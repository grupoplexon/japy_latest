<?php

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

date_default_timezone_set('America/Mexico_City');
$app = new Silex\Application();

$app['debug'] = true;

# CONFIGURACION
$app->register(
    new Silex\Provider\SecurityServiceProvider(),
    array(
        'security.firewalls' => array(
            'admin' => array(
                'pattern' => '^.*$',
                'http'    => true,
                'users'   => array(
                    'admin'  => array('ROLE_ADMIN', 'e964b65ef74b986ec46382cb2cbe1758e91f963a'), //Camaleon
                    'mobile' => array('ROLE_ADMIN', '9f047a8af6c607ca5cb24120890fe4e8f523c89d'), //mobile-pass-2016
                ),
            ),
        ),
    )
);

$app->register(new Silex\Provider\ValidatorServiceProvider());

$app['security.encoder.digest'] = $app->share(
    function ($app) {
        return new MessageDigestPasswordEncoder('sha1', false, 1);
    }
);

# CONEXION TO DATA BASE
$app->register(
    new Silex\Provider\DoctrineServiceProvider(),
    array(
        'db.options' => array(
            'driver'   => 'mysqli',
            'host'     => '127.0.0.1',
            'dbname'   => 'japy',
            'user'     => 'root',
            'password' => 'r9k-Yd&tr9k-Yd&t',
            'charset'  => 'utf8',
        ),
    )
);

$app->register(
    new Silex\Provider\TwigServiceProvider(),
    array(
        'twig.path' => __DIR__.'/templates',
    )
);

$app->register(
    new Silex\Provider\HttpCacheServiceProvider(),
    array(
        'http_cache.cache_dir' => __DIR__.'/cache/',
        'http_cache.esi'       => null,
    )
);

# GESTION DE ERRORES
$app->error(
    function (\Exception $exception, $code) use ($app) {

        return $app->json(
            array(
                'success' => 0,
                'data'    => array(
                    'success' => false,
                    'data'    =>
                        array(
                            'file'    => $exception->getFile(),
                            'line'    => $exception->getLine(),
                            'message' => $exception->getMessage(),
                            'trace'   => $exception->getTraceAsString(),
                            'code'    => $code,
                        ),
                ),
            ),
            403
        );
    }
);

//Symfony\Component\Debug\ErrorHandler::register();
#CORREO SERVICE
#CORREO 

function getServiceMail()
{
    $mail = new PHPMailer;
    $mail->isSMTP(); // Set mailer to use SMTP
    $mail->Host       = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth   = true; // Enable SMTP authentication
    $mail->Username   = 'comunicate.evolutel@gmail.com'; // SMTP username
    $mail->Password   = 'Camaleon97'; // SMTP password
    $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
    $mail->Port       = 587; // TCP port to connect to
    $mail->isHTML(true); // Set email format to HTML

    return $mail;
}

function randomLN($len)
{
    $str = '';
    $a   = "abcdefghijklmnopqrstuvwxyz0123456789";
    $b   = str_split($a);
    for ($i = 1; $i <= $len; $i++) {
        $str .= $b[rand(0, strlen($a) - 1)];
    }

    return $str;
}

if ($app["debug"]) {
    $app["correo"] = "f.eli.x.lopez.aguirre@gmail.com";
} else {
    $app["correo"] = "f.eli.x.lopez.aguirre@gmail.com";
}

//librerias
include_once './library/CodeQR.php';
include_once './library/Valid.php';
include_once './library/Token.php';
include_once './library/Filter.php';
//
#MODELOS
include_once './model/Model.php';
include_once './model/Usuario.php';
include_once './model/Cliente.php';

include_once './model/Novias/Tarea.php';
include_once './model/Novias/Correo.php';
include_once './model/Novias/Boda.php';
include_once './model/Novias/Proveedor.php';
include_once './model/Novias/Presupuesto.php';
include_once './model/Novias/Permisos.php';
include_once './model/Novias/MiPortal.php';

include_once './model/Novias/Comunidad/Comunidad.php';
include_once './model/Novias/Comunidad/Forum.php';
include_once './model/Novias/Comunidad/Picture.php';
include_once './model/Novias/Comunidad/Video.php';
include_once './model/Novias/Comunidad/Group.php';
include_once './model/Novias/Comunidad/Profile.php';

include_once './model/Proveedores/VerTelefono.php';
include_once './model/Proveedores/Promocion.php';
//include_once './model/Proveedores/Tipo.php';

#MODEL INVITADOS
include_once './model/Novias/Invitados/Grupo.php';
include_once './model/Novias/Invitados/Menu.php';
include_once './model/Novias/Invitados/Mesa.php';
include_once './model/Novias/Invitados/Invitado.php';

include_once './model/Blog/Blogs.php';
include_once './model/Blog/Comentarios.php';

include_once './model/Vestidos/Vestidos.php';
include_once './model/Vestidos/TipoVestidos.php';

#models para app expo
include_once './model/Expo/Expo.php';
include_once './model/Expo/Programa.php';
include_once './model/Expo/Patrocinador.php';
include_once './model/Expo/Expositor.php';
include_once './model/Expo/Nosotros.php';
include_once './model/ViveGDL.php';
include_once './model/Pais.php';
include_once './model/Expo/Ganador.php';
include_once './model/Mensajes.php';
//include_once './model/DefaultPorcentaje.php';

#RUTAS
include_once './rutas/usuario.php';
include_once './rutas/nosotros.php';
include_once './rutas/Novias/tareas.php';
include_once './rutas/Novias/proveedores.php';
include_once './rutas/Novias/categorias.php';
include_once './rutas/Novias/presupuesto.php';
include_once './rutas/Novias/permisos.php';
include_once './rutas/Novias/tendencia.php';

include_once './rutas/Novias/Comunidad/comunidad.php';
include_once './rutas/Novias/Comunidad/forum.php';
include_once './rutas/Novias/Comunidad/picture.php';
include_once './rutas/Novias/Comunidad/video.php';
include_once './rutas/Novias/Comunidad/group.php';
include_once './rutas/Novias/Comunidad/profile.php';

include_once './rutas/Blog/blogs.php';
include_once './rutas/Vestidos/vestidos.php';

include_once './rutas/Proveedores/proveedor.php';

#RUTA INVITADOS
include_once './rutas/Novias/Invitados/grupo.php';
include_once './rutas/Novias/Invitados/menu.php';
include_once './rutas/Novias/Invitados/mesa.php';
include_once './rutas/Novias/Invitados/invitado.php';

#rutas para app expo
include_once './rutas/Expo/expo.php';
include_once './rutas/Expo/agenda.php';
include_once './rutas/Expo/patrocinador.php';
include_once './rutas/Expo/expositor.php';
include_once './rutas/nosotros.php';
include_once './rutas/vivegdl.php';
include_once './rutas/Expo/ganadores.php';
include_once './rutas/buzon.php';

include_once './rutas/pais.php';

include_once './rutas/Authentication/Authentication.php';


$app->after(
    function (Request $request, Response $response) {
        $response->headers->set('Content-type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'Authorization');
    }
);

$app->boot();
$app->run();
