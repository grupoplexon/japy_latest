<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Presupuesto extends Model
{

    public function __construct(\Silex\Application $app)
    {
        parent::__construct($app);
        $this->id_tabla = "id_presupuesto";
        $this->tabla = "presupuesto";
    }

    protected function getColumsDefault($columnas = "*")
    {
        if ($columnas == "*") {
            return "id_presupuesto as id,categoria,nombre,costo_aproximado,costo_final,nota";
        }
        return $columnas;
    }

    public function getAllFromUsuario($id_usuario, $filter = "")
    {
        $colums = $this->getColumsDefault();
        if ($filter != "") {
            $filter = " and $filter ";
        }
        $sql = "SELECT $colums FROM presupuesto inner join boda using(id_boda) inner join cliente using(id_boda) where id_usuario=$id_usuario $filter ";
        $total = $this->app['db']->fetchAll($sql);
        return $total;
    }

    public function getPresupuesto($id_usuario, $categoria)
    {
        $colums = $this->getColumsDefault();
        $sql = "SELECT
                id_presupuesto AS id,
                categoria,
                nombre,
                CASE WHEN costo_aproximado != (presupuesto * porcentaje) AND costo_aproximado > 0 THEN costo_aproximado ELSE (porcentaje * boda.presupuesto) / 100 END AS costoaproximado,
                costo_final AS costofinal,
                nota,
                (
                SELECT
                    SUM(monto)
                FROM
                    presupuesto
                INNER JOIN pago USING (id_presupuesto)
                WHERE
                    id_presupuesto = P.id_presupuesto
                ) AS pago
            FROM
                presupuesto P
            INNER JOIN boda USING (id_boda)
            INNER JOIN cliente USING (id_boda)
            WHERE
                id_usuario = $id_usuario
            AND categoria = $categoria";
        $total = $this->app['db']->fetchAll($sql);
        return $total;
    }

    public function getAllCategorias($id_usuario)
    {
        $sql = "SELECT
               categoria as id,
               nombre,
               SUM((presupuesto * porcentaje) / 100) as presupuesto
           FROM
               presupuesto P
           INNER JOIN boda USING (id_boda)
           INNER JOIN cliente USING (id_boda)
           WHERE
               id_usuario = $id_usuario
           AND categoria IS NOT NULL
           GROUP BY
               categoria
           ORDER BY
               categoria ASC";
        $categorias = $this->app['db']->fetchAll($sql);
        return $categorias;
    }

    public function getAllCategoriasPorcentaje($id_usuario)
    {
        $sql = "SELECT
                p.categoria as id,
                dp.nombre ,
                SUM((presupuesto * p.porcentaje) / 100) AS presupuesto
            FROM
                presupuesto p
            INNER JOIN default_porcentaje dp ON(
                p.categoria = id_default_porcentaje
            )
            INNER JOIN boda USING(id_boda)
            INNER JOIN cliente USING(id_boda)
            WHERE
                id_usuario = $id_usuario
            GROUP BY
                categoria;";
        $categorias = $this->app['db']->fetchAll($sql);
        return $categorias;
    }

    public function getPresupuestoUpdate($id_usuario, $id_presupuesto)
    {
        $sql = "SELECT
                id_presupuesto AS id,
                categoria,
                nombre,
                CASE WHEN costo_aproximado != (presupuesto * porcentaje) AND costo_aproximado > 0 THEN costo_aproximado ELSE (porcentaje * boda.presupuesto) / 100 END AS costoaproximado,
                costo_final AS costofinal,
                nota,
                (
                SELECT
                    SUM(monto)
                FROM
                    presupuesto
                INNER JOIN pago USING (id_presupuesto)
                WHERE
                    id_presupuesto = P.id_presupuesto
                ) AS pago
            FROM
                presupuesto P
            INNER JOIN boda USING (id_boda)
            INNER JOIN cliente USING (id_boda)
            WHERE
                id_usuario = $id_usuario
            AND id_presupuesto = $id_presupuesto";
        $n = $this->app['db']->fetchAll($sql);
        if ($n) {
            return $n[0];
        }
        return null;
    }

}
