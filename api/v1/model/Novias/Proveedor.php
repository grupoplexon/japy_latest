<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Proveedor extends Model
{

    public function __construct(\Silex\Application $app)
    {
        parent::__construct($app);
        $this->id_tabla = "id_proveedor";
        $this->tabla    = "proveedor";
    }

    protected function getColumsDefault($colums = "*")
    {
        if ($colums == "*") {
            return "id_proveedor as id,nombre,contacto_telefono,contacto_pag_web,localizacion_latitud,localizacion_longitud,descuento,tipo_cuenta";
        }

        return $colums;
    }

    public function getPagePoveedores($page = 0, $categoria = null, $search)
    {
        $limit  = parent::limit($page);
        $colums = $this->getColumsDefault();
        $s      = " and (";
        foreach ($search as $key => $v) {
            $s .= " nombre like '%$v%' or localizacion_poblacion like '%$v%' or localizacion_estado like '%$v%' or nombre_tipo_proveedor like '%$v%' or";
        }
        $s .= " 1=0 )";
        if ($categoria) {
            $sql = "SELECT $colums,localizacion_poblacion,localizacion_estado,SUBSTRING(proveedor.descripcion,1,100) as descripcion,"
                    ." tag_categoria,nombre_tipo_proveedor FROM proveedor inner join tipo_proveedor using(id_tipo_proveedor) "
                    ." where tag_categoria='$categoria' and acceso=1 $s $limit ";
        } else {
            $sql = "SELECT $colums,localizacion_poblacion,localizacion_estado,SUBSTRING(proveedor.descripcion,1,100) as descripcion,tag_categoria,nombre_tipo_proveedor FROM proveedor inner join tipo_proveedor using(id_tipo_proveedor) where acceso=1 $s $limit ";
        }
        $total = $this->app['db']->fetchAll($sql);

        return $total;
    }


    public function getFAQIDbyName($servucio, $titulo)
    {
        $sql = 'SELECT id_faq FROM faq WHERE id_tipo_proveedor=:TIPO AND titulo=:TITULO';

        return $this->query($sql, [':TIPO' => $servucio, ':TITULO' => $titulo], true);
    }


    public function getPagePoveedoresFiltro($pagina = 1, $categoria = null, $search = null, $filtros = [])
    {
        $filter = "";
        foreach ($filtros as $key => $value) {
            $faq = $this->getFAQIDbyName($categoria, $key);
            if ($faq) {
                $value  = str_replace('_', ' ', $value);
                $filter .= " and filtrar_proveedor(proveedor.id_proveedor,$faq->id_faq,'$value')=1 ";
            }
        }
        $c = "";
        if ($categoria) {
            $c = " and ( tipo_proveedor.id_tipo_proveedor =$categoria  or rel_tipo_proveedor.id_tipo_proveedor =$categoria ) ";
        }
        $e = "";
        $s = '';
        $l = ' limit '.(50 * (($pagina <= 1 ? 1 : $pagina) - 1)).',50';

        if ($categoria == 'BANQUETES' || $categoria == 'NOVIOS' || $categoria == 'NOVIAS' || $categoria == 'PROVEEDORES') {
            $sql = 'SELECT proveedor.*,SUBSTRING(proveedor.descripcion,1,100) as descripcion,nombre_tipo_proveedor,grupo,producto,CASE WHEN id_boda IS NULL THEN 0 ELSE 1 END AS favorito '
                    .' FROM proveedor '
                    .' INNER JOIN tipo_proveedor USING(id_tipo_proveedor)  '
                    ." LEFT JOIN proveedor_boda ON(proveedor.id_proveedor=proveedor_boda.id_proveedor) "
                    .' LEFT JOIN rel_tipo_proveedor on(rel_tipo_proveedor.id_proveedor=proveedor.id_proveedor) '
                    ."  WHERE acceso=1 $c $s $e $filter group by proveedor.id_proveedor  $l";
        } else {
            $sql = "SELECT proveedor.*,nombre_tipo_proveedor,grupo,producto, SUBSTRING(proveedor.descripcion,1,100) as descripcion,
				CASE WHEN id_boda IS NULL THEN 0 ELSE 1 END AS favorito
				FROM proveedor INNER JOIN tipo_proveedor USING(id_tipo_proveedor)
				LEFT JOIN proveedor_boda ON(proveedor.id_proveedor=proveedor_boda.id_proveedor
				AND id_boda=0) WHERE acceso=1 AND proveedor.descripcion LIKE '%$categoria%' $e $l;";
        }
        $result = $this->query($sql, []);

        return $result;
    }

    public function getCountPromociones($id_proveedor)
    {
        $sql = "select count(id_promocion) as total from proveedor inner join promocion using(id_proveedor) where acceso=1 and id_proveedor=$id_proveedor and fecha_fin > NOW() ";
        $res = $this->app['db']->fetchAll($sql);
        if ($res) {
            $r = $res[0];

            return $r["total"];
        }

        return null;
    }

    public function getCountVideos($id_proveedor)
    {
        $sql = "select count(id_galeria) as total from proveedor inner join galeria using(id_proveedor) where acceso=1 and galeria.tipo='VIDEO' and id_proveedor=$id_proveedor ";
        $res = $this->app['db']->fetchAll($sql);
        if ($res) {
            $r = $res[0];

            return $r["total"];
        }

        return null;
    }

    public function getCountFotos($id_proveedor)
    {
        $sql = "select count(id_galeria) as total from proveedor inner join galeria using(id_proveedor) where acceso=1 and galeria.tipo='IMAGEN' and id_proveedor=$id_proveedor ";
        $res = $this->app['db']->fetchAll($sql);
        if ($res) {
            $r = $res[0];

            return $r["total"];
        }

        return null;
    }

    public function getGaleria($tipo, $proveedor)
    {
        $sql = "SELECT id_galeria as id,nombre FROM galeria where tipo='$tipo' and id_proveedor=$proveedor ; ";
        $res = $this->app['db']->fetchAll($sql);

        return $res;
    }

    public function getPrecio($id)
    {
        $sql = "select respuesta from faq inner join respuesta_faq using(id_faq) where id_proveedor=$id and tipo='RANGE' and valores is null";
        $res = $this->app['db']->fetchAll($sql);
        if ($res) {
            $r = $res[0]["respuesta"];
            if ($r) {
                $r = explode("|", $r);
                if (count($r) == 1) {
                    return "Desde ".$r[0];
                } else {
                    return $r[0]." a ".$r[1];
                }
            }
        }

        return null;
    }

    public function getPersonas($id)
    {
        $sql = "select respuesta from faq inner join respuesta_faq using(id_faq) where id_proveedor=$id and tipo='RANGE' and valores='PERSONAS' ";
        $res = $this->app['db']->fetchAll($sql);
        if ($res) {
            $r = $res[0]["respuesta"];
            if ($r) {
                $r = explode("|", $r);
                if (count($r) == 1) {
                    return "Hasta ".$r[0];
                } else {
                    return $r[0]." a ".$r[1];
                }
            }
        }

        return null;
    }

    public function getFaqs($proveedor)
    {
        $sql   = "SELECT faq.pregunta,respuesta FROM respuesta_faq inner join faq using(id_faq) where id_proveedor=$proveedor ";
        $total = $this->app['db']->fetchAll($sql);

        return $total;
    }

    public function getRecomendaciones($proveedor, $limit = 10)
    {
        $sql   = "SELECT resena as mensaje,usuario.nombre,fecha_recomendacion as fecha,id_usuario as usuario from proveedor_boda inner join cliente using(id_boda) inner join usuario using(id_usuario) where id_proveedor=$proveedor and resena is not null and visible=1 limit $limit";
        $total = $this->app['db']->fetchAll($sql);

        return $total;
    }

    public function getPromociones($proveedor)
    {
        $sql   = "SELECT id_promocion as id,nombre,descripcion as mensaje,tipo,fecha_fin from promocion where id_proveedor=$proveedor and fecha_fin > NOW() ";
        $total = $this->app['db']->fetchAll($sql);

        return $total;
    }

    public function getCountValoracion($proveedor)
    {
        $sql   = "SELECT sum(calificacion) as valoracion,count(id_proveedor) as total from proveedor_boda where id_proveedor=$proveedor ";
        $total = $this->app['db']->fetchAll($sql);
        if ($total) {
            $t = $total[0];
            if ($t["total"] > 0) {
                return $t["valoracion"] / $t["total"];
            }

            return 0;
        }

        return 0;
    }

    public function isLike($id_proveedor, $usuario)
    {
        $sql   = "SELECT id_proveedor FROM proveedor_boda inner join cliente using(id_boda) where id_proveedor=$id_proveedor and id_usuario=$usuario ;";
        $total = $this->app['db']->fetchAll($sql);
        if ($total) {
            return true;
        }

        return false;
    }

    public function insertLike($id_proveedor, $id_boda)
    {
        $sql  = "INSERT INTO proveedor_boda(id_proveedor,id_boda) VALUES($id_proveedor,$id_boda);";
        $stmt = $this->app['db']->prepare($sql);

        return $stmt->execute();
    }

    public function deleteLike($id_proveedor, $id_boda)
    {
        $sql  = "DELETE FROM proveedor_boda where id_proveedor=$id_proveedor and id_boda=$id_boda ;";
        $stmt = $this->app['db']->prepare($sql);

        return $stmt->execute();
    }

    public function getAllCategoriaFromUsuario($id_usuario, $categoria, $filter = "", $limit = "")
    {
        $colums = $this->getColumsDefault();
        if ($filter != "") {
            $filter = " and $filter ";
        }
        $sql   = "SELECT $colums, proveedor.id_usuario, tag_categoria,nombre_tipo_proveedor,proveedor_boda.estado, calificacion,resena,precio FROM proveedor inner join proveedor_boda using(id_proveedor) inner join cliente using (id_boda) inner join tipo_proveedor using(id_tipo_proveedor) where cliente.id_usuario=$id_usuario and tag_categoria='$categoria' $filter $limit ";
        $total = $this->app['db']->fetchAll($sql);
        foreach ($total as $key => &$value) {
            $sql  = "SELECT mime, data FROM galeria g INNER JOIN proveedor p ON(p.id_proveedor=g.id_proveedor AND g.logo=1) WHERE p.id_proveedor=".$value['id'];
            $logo = $this->app['db']->fetchAll($sql);
            if (isset($logo) && count($logo) > 0) {
                $value['foto'] = "data:".$logo[0]['mime'].";base64,".base64_encode($logo[0]['data']);
            } else {
                $value['foto'] = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAArlBMVEXg4OD////e3t7d3d3f39/h4eHc3Nzl5eXq6urn5+f7+/vi4uLo6Ojz8/P+/v7r6+vv7+/x8fH6+vrk5OTm5ubp6ens7Ozy8vL29vb39/fj4+Pu7u7w8PD19fX5+fn8/Pzb29v09PTc3dzh4OHi4+Lt7e3v8O/y8/L4+Pjd3N3d3t3e397f4N/h4uLi4uPk5eXr7Ovr7Ozx8fD19vX3+Pj4+fn5+vn7/Pv9/f39/v1OrysmAAACa0lEQVR42u2ZZ2/bQAyGxXGyLNtyPOQVryTN7N7t//9jdQMUqBGNI49XoIDeb/r0gORxKkk6derUqdN/I/dbMQGMlKbH4zFNCTkOAHGQTdbTAor9elu607c1g7h3D+fazphMEW8WBbxUnrGZNUwfoUZFeW0THNrkUK8vby2MwTE0axgeGVxCmy5DbeEVtGsSRkEfRiCFluCnS31caAy+6mtt4Rn4yynzBQ8CyGeKkyAGDuNExICfGgguZBAYKigoZEAujwrPpRDYSB+YS3diyN2ji+0tgKnUX9yTQ+Cd0F+0UECE78ulSwVkQbKg0EoBmQiDgnsF5EqYjpwrICtpzv9QQB6Er4sPCshaCpn+g5jQTgG5IGGebBWQTJonYwVE2hxFQ8QfDaS1nuSMb9dCxk16pYi7i1/rB/LRS+yvaaoYJDIhZK6YIZmFw4pqGhaaMteN3E9fBYyRcnngVwJIot2CsYztrOdnfOfJ+BCyAdMo9jb3TLmPzzhRLloZ4/BrAQ1bGD2Du4cjbOz3fTKA0KYl+IeSAi9FmEw8ylYvBMPkWb92idZnDp3/YKQ9SJFobZyoDqwk7Fp7llPwU/wmjyPF4LWRhR8169zJltfxGRKPObpVMqDwhmAf1Fp75ouot7/Q1o+iWkqFC4RHmwoeXFSbyfkI1m4K5aGQ9l4prViVbQxtz5vVKtHyTluXks0QBhNlFCnXz3pLkyn03QbS9MD4vRGjIVdcOrKCNKQ9mzHqj55WYW8q+TfBpfFvoeUlrfZ9VQeFB4aMugsb9iwhD2nlOQdvLSHAIX8ufTWrpGBhCqnOFDRlnP0e+AV36CL/cgXEFAAAAABJRU5ErkJggg==";
            }
        }

        return $total;
    }

    public function calificar($id_boda, $id_proveedor, $datos)
    {
        $set = "";
        if (is_array($datos)) {

            foreach ($datos as $key => $value) {
                $set .= " $key=:$key ,";
            }
            
            $set  = substr($set, 0, strlen($set) - 1);
            $sql  = "UPDATE proveedor_boda set $set where id_boda=$id_boda and id_proveedor=$id_proveedor ";
            $stmt = $this->app['db']->prepare($sql);
            try {

                foreach ($datos as $key => $value) {
                    $params[":$key"] = $value;
                }

                $stmt->execute($params);

                return true;
            } catch (Exception $e) {
                var_dump($e->getMessage());
            }
        }

        return false;
    }

    public function insertVisitaEscaparate($usuario, $proveedor)
    {
        $sql     = "SELECT * FROM cliente where id_usuario=$usuario";
        $cliente = $this->app['db']->fetchAll($sql);
        try {
            $sql2 = "insert into visita_escaparate (id_proveedor,id_cliente) values ($proveedor,$usuario) ";
            $stmt = $this->app['db']->prepare($sql2);
            $stmt->execute();
        } catch (Exception $e) {

        }
    }

    public function delete_proveedor_boda($id_boda, $id_proveedor)
    {
        $sql  = "DELETE FROM proveedor_boda where id_boda=$id_boda and id_proveedor=$id_proveedor ;";
        $stmt = $this->app['db']->prepare($sql);
        try {
            $stmt->execute();

            return true;
        } catch (Exception $e) {

        }

        return false;
    }

    public function getPrincipal($id_proveedor)
    {
        $sql   = "SELECT id_galeria from galeria g inner join proveedor p on(p.id_proveedor=g.id_proveedor and g.principal=1) where p.id_proveedor=$id_proveedor";
        $total = $this->app['db']->fetchAll($sql);
        if ($total) {
            return $total[0];
        }

        return null;
    }

    public function getLogo($id_proveedor)
    {
        $sql   = "SELECT id_galeria from galeria g inner join proveedor p on(p.id_proveedor=g.id_proveedor and g.logo=1) where p.id_proveedor=$id_proveedor";
        $total = $this->app['db']->fetchAll($sql);
        if ($total) {
            return $total[0];
        }

        return null;
    }

    public function getCountCategoriaFromUsuario($id_usuario)
    {
        $sql   = "SELECT
   tag_categoria,
   sum(
    CASE
    WHEN cliente.id_usuario IS NOT NULL THEN
     1
    ELSE
     0
    END
   ) AS total,
   sum(
    CASE
    WHEN proveedor_boda.estado=5 THEN
     1
    ELSE
     0
    END
   ) AS reservado
  FROM
   proveedor
  LEFT JOIN proveedor_boda USING (id_proveedor)
  LEFT JOIN cliente USING (id_boda)
  RIGHT JOIN tipo_proveedor USING (id_tipo_proveedor)
  WHERE
   cliente.id_usuario = $id_usuario
  OR cliente.id_usuario IS NULL
  GROUP BY
   tag_categoria;";
        $total = $this->app['db']->fetchAll($sql);

        return $total;
    }

}
