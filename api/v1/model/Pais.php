<?php use Silex\Application as Application;

class Pais extends Model
{
    public function __construct(\Silex\Application $app)
    {
        parent::__construct($app);
        $this->tabla = "pais";
        $this->id_tabla = "id_pais";
    }

    protected function getColumsDefault($colums = "*")
    {
        return "id_pais as id,nombre";
    }
}

class Estado extends Model
{
    public function __construct(\Silex\Application $app)
    {
        parent::__construct($app);
        $this->tabla = "estado";
        $this->id_tabla = "id_estado";
    }

    protected function getColumsDefault($colums = "*")
    {
        return "id_estado as id,id_pais as pais,estado as nombre";
    }
}

class Ciudad extends Model
{
    public function __construct(\Silex\Application $app)
    {
        parent::__construct($app);
        $this->tabla = "ciudad";
        $this->id_tabla = "id_ciudad";
    }

    protected function getColumsDefault($colums = "*")
    {
        return "id_ciudad as id,id_estado as estado,ciudad as nombre";
    }
}
