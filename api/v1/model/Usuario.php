<?php use Silex\Application as Application;

class Usuario extends Model
{
    public function __construct(\Silex\Application $app)
    {
        parent::__construct($app);
        $this->tabla = "usuario";
        $this->id_tabla = "id_usuario";
    }

    protected function getColumsDefault($colums = "*")
    {
        return "id_usuario as id,usuario,contrasena,rol,activo,nombre,apellido,correo";
    }

    public function valid($user)
    {
        foreach ($user as $key => $value) {
            if ($key != "foto" && (!$value || $value == "")) {
                return false;
            }
        }
        if (!filter_var($user["correo"], FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    public function existEmail($correo)
    {
        try {
            $sql = "SELECT id_usuario from $this->tabla where usuario='$correo'";
            $n = $this->app['db']->fetchAll($sql);
            if (count($n) > 0) {
                return true;
            }
            return false;
        } catch (Exception $e) {
        }
        return false;
    }

    public function getUserFromToken($token)
    {
        $colums = $this->getColumsDefault();
        $sql = "SELECT $colums from $this->tabla where sha(concat(usuario,'" . Token::$SALT . "',contrasena))='$token';";
        $n = $this->app['db']->fetchAll($sql);
        if ($n) return $n[0];
        return false;
    }

    public function getFotoFromToken($token)
    {
        $sql = "SELECT mime,foto from $this->tabla where sha(concat(usuario,'" . Token::$SALT . "',contrasena))='$token';";
        $n = $this->app['db']->fetchAll($sql);
        if ($n) return $n[0];
        return false;
    }

    public function getFacebook($usuario, $id_facebook)
    {
        $colums = $this->getColumsDefault();
        $sql = "SELECT $colums FROM usuario INNER JOIN cliente USING(id_usuario) WHERE usuario ='$usuario' AND id_facebook='$id_facebook' AND activo=1 AND rol=2 LIMIT 1 ";
        $n = $this->app['db']->fetchAll($sql);
        if ($n) return $n[0];
        return false;
    }

    public function getFromEmail($correo)
    {
        $sql = "SELECT id_usuario as id,correo,nombre,apellido FROM $this->tabla WHERE correo='$correo' AND activo=1 LIMIT 1";
        $n = $this->app['db']->fetchAll($sql);
        if ($n) return $n[0];
        return false;
    }

    public function getUser($correo, $password)
    {
        $sql = "SELECT id_usuario as id,correo,nombre,apellido,pais,estado,ciudad,foto FROM $this->tabla where correo='$correo' and password=md5('$password') and activo=1 ";
        $n = $this->app['db']->fetchAll($sql);
        if ($n) return $n[0];
        return false;
    }

    public function getUserCodigo($codigo)
    {
        $sql = "SELECT id_usuario as id,correo,nombre,apellido,pais,estado,ciudad,foto,password FROM $this->tabla where codigo_activacion= BINARY '$codigo' ;";
        $n = $this->app['db']->fetchAll($sql);
        if ($n) return $n[0];
        return false;
    }
}
