<?php use Silex\Application as Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;

$app->get('blog/categorias/{id_categoria}', function (Application $app, Request $request, $id_categoria) {
    try {
        $blogsDB = new Blogs($app);
        $blogs   = $blogsDB->getCategoria($id_categoria, filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('/blog/categorias', function (Application $app, Request $request) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $blogs   = $blogsDB->getAllFromCategorias(filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts', function (Application $app, Request $request) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $blogs   = $blogsDB->getAllFromPosts(filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts/masLeidos', function (Application $app, Request $request) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $blogs   = $blogsDB->getDestacadosFromPosts(filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts/masComentados', function (Application $app, Request $request) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $blogs   = $blogsDB->getComentadosFromPosts(filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts/{id_post}', function (Application $app, Request $request, $id_post) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $blogs   = $blogsDB->getPost($id_post, filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts/{id_post}/comentarios', function (Application $app, Request $request, $id_post) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $url     = "http://".$request->getHttpHost().str_replace("api/v1", "", $request->getBasePath());
        $user    = $userDB->getUserFromToken($request->get("token"));
        $blogs   = $blogsDB->getComentariosPost($id_post, $user["id"], $url, filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts/{id_post}/comentarios/{id_comentario}', function (Application $app, Request $request, $id_post, $id_comentario) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $user    = $userDB->getUserFromToken($request->get("token"));
        $url     = "http://".$request->getHttpHost().str_replace("api/v1", "", $request->getBasePath());
        $blogs   = $blogsDB->getComentario($id_post, $id_comentario, $user['id'], $url, filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts/{id_post}/comentarios/{id_comentario}/comentarios', function (Application $app, Request $request, $id_post, $id_comentario) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $url     = "http://".$request->getHttpHost().str_replace("api/v1", "", $request->getBasePath());
        $blogs   = $blogsDB->getComentariosComentarios($id_post, $id_comentario, $url, filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts/{id_post}/comentarios/{id_comentario}/comentarios/{id_comentario2}', function (Application $app, Request $request, $id_post, $id_comentario, $id_comentario2) {
    try {
        $userDB  = new Usuario($app);
        $blogsDB = new Blogs($app);
        $url     = "http://".$request->getHttpHost().str_replace("api/v1", "", $request->getBasePath());
        $blogs   = $blogsDB->getComentarioComen($id_post, $id_comentario, $id_comentario2, $url, filter_by($request));
        if ($blogs && count($blogs) > 0) {
            return $app->json(array("success" => true, "data" => $blogs), 202);
        } else {
            return $app->json(array("success" => false, "data" => []), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->post('blog/posts/{id_post}/comentario', function (Application $app, Request $request, $id_post) {
    try {
        $userDB     = new Usuario($app);
        $comentario = $request->get("comentario");
        $id_usuario = $userDB->getUserFromToken($request->get("token"));
        $app['db']->insert('blog_comentario', array("mensaje" => $comentario, "id_blog_post" => $id_post, "id_usuario" => $id_usuario['id'], "nuevo" => 0));
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->post('blog/posts/{id_post}/comentario/{id_comentario}', function (Application $app, Request $request, $id_post, $id_comentario) {
    try {
        $userDB     = new Usuario($app);
        $comentario = $request->get("comentario");
        $id_usuario = $userDB->getUserFromToken($request->get("token"));
        $app['db']->insert('blog_comentario', array("parent" => $id_comentario, "mensaje" => $comentario, "id_blog_post" => $id_post, "id_usuario" => $id_usuario['id'], "nuevo" => 0));
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/posts/{id_post}/favorito', function (Application $app, Request $request, $id_post) {
    try {
        $userDB     = new Usuario($app);
        $blogsDB    = new Blogs($app);
        $id_usuario = $userDB->getUserFromToken($request->get("token"));
        $fav        = $blogsDB->getFavorito($id_post, $id_usuario['id']);
        if (count($fav) == 0) {
            $app['db']->insert('blog_favorito', array("id_usuario" => $id_usuario['id'], "id_blog_post" => $id_post));
        } else {
            $app['db']->delete('blog_favorito', array("id_usuario" => $id_usuario['id'], "id_blog_post" => $id_post));
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('blog/comentario/{id_comentario}/favorito', function (Application $app, Request $request, $id_comentario) {
    try {
        $userDB     = new Usuario($app);
        $blogsDB    = new Blogs($app);
        $id_usuario = $userDB->getUserFromToken($request->get("token"));
        $fav        = $blogsDB->getComentarioFavorito($id_comentario, $id_usuario['id']);
        if (count($fav) == 0) {
            $app['db']->insert('blog_comentario_fav', array("id_usuario" => $id_usuario['id'], "id_blog_comentario" => $id_comentario));
        } else {
            $app['db']->delete('blog_comentario_fav', array("id_usuario" => $id_usuario['id'], "id_blog_comentario" => $id_comentario));
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
