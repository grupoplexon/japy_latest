<?php

use Silex\Application as Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;

$app->get('/me/presupuesto/default', function( Application $app, Request $request) {
    try {
        $porDB = new DefaultPorcentaje($app);
        $porcentajes = $porDB->getAll();
        if ($porcentajes) {
            return $app->json(array("success" => true, "data" => $porcentajes), 202);
        }
        return $app->json(array("success" => false, "data" => []), 202);
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }
    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});



$app->post('/me/presupuesto_default', function( Application $app, Request $request) {
    try {
        $userDB = new Usuario($app);
        $user = $userDB->getUserFromToken($request->get("token"));
        if ($user) {
            $porDB = new DefaultPorcentaje($app);
            $bodaDB = new Boda($app);
            $boda = $bodaDB->getBodaFromUsuario($user["id"]);
            $porcentajes = $porDB->getAllList();
            if ($porcentajes) {
                foreach ($porcentajes as $porcentaje) {
                    $porDB->updatePorcentaje($boda['id'], $porcentaje['nombre'], $porcentaje['porcentaje']);
                }
                $bodaDB->update($boda['id'], array('first' => 1));
                return $app->json(array("success" => true, "data" => true), 202);
            }
        }
        return $app->json(array("success" => false, "data" => FALSE), 202);
    } catch (Exception $e) {
        return $app->json(array("success" => false, "mensaje" => $e->getFile(), 'line' => $e->getLine(), 'msj' => $e->getMessage()), 403);
    }
    return $app->json(array("success" => false, "mensaje" => false), 403);
});


$app->get('/me/presupuesto', function( Application $app, Request $request) {
    try {
        $userDB = new Usuario($app);
        $tareasDB = new Presupuesto($app);
        $user = $userDB->getUserFromToken($request->get("token"));
        if ($user) {
            $tareas = $tareasDB->getAllFromUsuario($user["id"], filter_by($request));
            if ($tareas && count($tareas) > 0) {
                return $app->json(array("success" => true, "data" => $tareas), 202);
            } else {
                return $app->json(array("success" => false, "data" => []), 202);
            }
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }
    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});

$app->put('/me/presupuesto/', function( Application $app, Request $request) {
    try {
        $userDB = new Usuario($app);
        $bodaDB = new Boda($app);
        $presupuestoDB = new Presupuesto($app);
        $user = $userDB->getUserFromToken($request->get("token"));

        $pres = array(
            "categoria" => exists_categoria($request->get("categoria")),
            "nombre" => $request->get("nombre"),
            "costo_aproximado" => $request->get("costo_aproximado"),
            "costo_final" => $request->get("costo_final"),
            "nota" => $request->get("nota"),
        );
        if ($user) {
            $boda = $bodaDB->getBodaFromUsuario($user["id"]);
//            $presupuesto = $presupuestoDB->get(array("id_boda" => $boda["id"], "id_presupuesto" => $id_presupuesto));
            $pres["id_boda"] = $boda["id"];
            if ($presupuesto && count($presupuesto) > 0) {
                if ($presupuestoDB->insert($pres)) {
                    return $app->json(array("success" => true, "data" => "ok"), 202);
                }
            }
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }
    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});

$app->get('/me/presupuesto/{id_presupuesto}', function( Application $app, Request $request, $id_presupuesto) {
    try {
        $userDB = new Usuario($app);
        $bodaDB = new Boda($app);
        $presupuestoDB = new Presupuesto($app);
        $user = $userDB->getUserFromToken($request->get("token"));
        if ($user) {
            $boda = $bodaDB->getBodaFromUsuario($user["id"]);
            $presupuesto = $presupuestoDB->get(array("id_boda" => $boda["id"], "id_presupuesto" => $id_presupuesto));
            if ($presupuesto && count($presupuesto) > 0) {
                return $app->json(array("success" => true, "data" => $presupuesto), 202);
            }
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }
    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});


$app->post('/me/presupuesto/{id_presupuesto}', function( Application $app, Request $request, $id_presupuesto) {
    try {
        $userDB = new Usuario($app);
        $bodaDB = new Boda($app);
        $presupuestoDB = new Presupuesto($app);
        $user = $userDB->getUserFromToken($request->get("token"));
        $pres = array();
        if ($request->get("nombre")) {
            $pres["nombre"] = $request->get("nombre");
        }
        if ($request->get("costo_aproximado")) {
            $pres["costo_aproximado"] = $request->get("costo_aproximado");
        }
        if ($request->get("costo_aproximado")) {
            $pres["costo_final"] = $request->get("costo_final");
        }
        if ($request->get("costo_aproximado")) {
            $pres["nota"] = $request->get("nota");
        }
        if ($user) {
            $boda = $bodaDB->getBodaFromUsuario($user["id"]);
            $presupuesto = $presupuestoDB->get(array("id_boda" => $boda["id"], "id_presupuesto" => $id_presupuesto));
            if ($presupuesto && count($presupuesto) > 0) {
                if ($presupuestoDB->update($id_presupuesto, $pres)) {
                    return $app->json(array("success" => true, "data" => "ok"), 202);
                }
            }
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }
    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});



$app->delete('/me/presupuesto/{id_presupuesto}', function( Application $app, Request $request, $id_presupuesto) {
    try {
        $userDB = new Usuario($app);
        $bodaDB = new Boda($app);
        $presupuestoDB = new Presupuesto($app);
        $user = $userDB->getUserFromToken($request->get("token"));
        if ($user) {
            $boda = $bodaDB->getBodaFromUsuario($user["id"]);
            $presupuesto = $presupuestoDB->get(array("id_boda" => $boda["id"], "id_presupuesto" => $id_presupuesto));
            if ($presupuesto && count($presupuesto) > 0) {
                $presupuestoDB->delete($presupuesto["id"], array("completada" => 0));
                return $app->json(array("success" => true, "data" => "ok"), 202);
            }
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }
    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});

$app->get('/me/presupuesto-categoria', function( Application $app, Request $request) {
    try {
        $userDB = new Usuario($app);
        $tareasDB = new Presupuesto($app);
        $bodaDB = new Boda($app);
        $user = $userDB->getUserFromToken($request->get("token"));
        $categoria = $request->get("categoria");
        if ($user) {
            $boda = $bodaDB->getBodaFromUsuario($user["id"]);
            $tareas = $tareasDB->getPresupuesto($user["id"], $categoria);
            if ($tareas && count($tareas) > 0) {
                return $app->json(array("success" => true, "data" => $tareas), 202);
            } else {
                return $app->json(array("success" => false, "data" => []), 202);
            }
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }
    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});

$app->post('/me/update-presupuesto', function( Application $app, Request $request) {
    try {
        $userDB = new Usuario($app);
        $tareasDB = new Presupuesto($app);
        $bodaDB = new Boda($app);
        $user = $userDB->getUserFromToken($request->get("token"));
        $data = array();
        $data['nombre'] = $request->get('nombre');
        $data['costo_aproximado'] = $request->get('costo_aproximado');
        $data['costo_final'] = $request->get('costo_final');
        if ($user) {
            $bandera = $tareasDB->update($request->get("id_presupuesto"), $data);
            if ($bandera) {
                return $app->json(array("success" => true, "data" => "ok"), 202);
            }
        }
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }
    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});

$app->get('/me/todas-las-categorias', function( Application $app, Request $request) {
    try {
        $userDB = new Usuario($app);
        $tareasDB = new Presupuesto($app);
        $bodaDB = new Boda($app);
        $user = $userDB->getUserFromToken($request->get('token'));
        if ($user) {
            $tareas = $tareasDB->getAllCategoriasPorcentaje($user['id']);
            if ($tareas && count($tareas) > 0) {
                foreach ($tareas as &$value) {
                    $value['nombre'] = ucfirst($value['nombre']);
                    $value['presupuesto'] = round($value['presupuesto'], 2);
                }
                return $app->json(array('success' => true, 'data' => $tareas), 202);
            } else {
                return $app->json(array('success' => false, 'data' => []), 202);
            }
        }
    } catch (Exception $e) {
        return $app->json(array('success' => 0, 'mensaje' => $e->getMessage()), 403);
    }
    return $app->json(array('success' => 0, "mensaje" => 'error'), 403);
});
