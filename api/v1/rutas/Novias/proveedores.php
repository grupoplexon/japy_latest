<?php

use Silex\Application as Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;

/**
 * @api               {get} /me/proveedores mis Tareas
 * @apiVersion        0.1.0
 * @apiName           GetProveedores
 * @apiGroup          Proveedores
 *
 * @apiSuccess {Boolean} status  success TRUE conforme todo ha ido bien.
 * @apiSuccess {Array} arreglo de las tareas del usuario.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "success": true,
 *        "data": []
 *     }
 *
 * @apiError          UserBadResponse token incorrecto o el usuario no tiene tareas
 *
 * @apiError          BaD-Response:
 *     HTTP/1.1 400 Bad Response
 *     {
 *       "success": 0
 *     }
 */
$app->get('/me/proveedores', function (Application $app, Request $request) {
    try {
        $userDB   = new Usuario($app);
        $tareasDB = new Proveedor($app);
        $user     = $userDB->getUserFromToken($request->get("token"));
        if ($user) {
            $proveedores = $tareasDB->getCountCategoriaFromUsuario($user["id"]);
            if ($proveedores && count($proveedores) > 0) {
                return $app->json(["success" => true, "data" => $proveedores], 202);
            } else {
                return $app->json(["success" => false, "data" => []], 202);
            }
        }
    } catch (Exception $e) {
        return $app->json(["success" => 0, "mensaje" => $e->getMessage()], 403);
    }

    return $app->json(["success" => 0, "mensaje" => "error"], 403);
});


$app->get('/me/proveedores/{categoria}', function (Application $app, Request $request, $categoria) {
    try {
        $userDB   = new Usuario($app);
        $tareasDB = new Proveedor($app);
        $user     = $userDB->getUserFromToken($request->get("token"));
        if ($user) {
            $proveedores = $tareasDB->getAllCategoriaFromUsuario($user["id"], str_replace("_", " ", $categoria));
            if ($proveedores && count($proveedores) > 0) {
                $url = "http://".$request->getHttpHost().str_replace("api/v1", "index.php", $request->getBasePath());
                foreach ($proveedores as &$value) {
                    $principal = $tareasDB->getPrincipal($value["id"]);
                    if ($principal) {
                        $value["imagen"] = $url."/home/imagen_proveedor/".$principal["id_galeria"];
                        $value["foto"]   = $url."/home/imagen_proveedor/".$principal["id_galeria"];
                    }
                }

                return $app->json(["success" => true, "data" => $proveedores], 202);
            } else {
                return $app->json(["success" => false, "data" => []], 202);
            }
        }
    } catch (Exception $e) {
        return $app->json(["success" => 0, "mensaje" => $e->getMessage()], 403);
    }

    return $app->json(["success" => 0, "mensaje" => "error"], 403);
});

$app->post('/me/buzon/proveedor/{id_proveedor}', function (Application $app, Request $request, $id_proveedor) {
    $userDB      = new Usuario($app);
    $proveedorDB = new Proveedor($app);
    $user        = $userDB->getUserFromToken($request->get("token"));
    $proveedor   = $proveedorDB->get($id_proveedor, "id_proveedor as id,id_usuario");
    if ($user && $proveedor) {
        $correoDB = new Correo($app);
        $correo   = [
                "from"    => $user["id"],
                "to"      => $proveedor["id_usuario"],
                "mensaje" => $request->get("mensaje"),
                "asunto"  => $request->get("asunto"),
        ];
        $correoDB->new_correo($proveedor["id_usuario"], $user["id"], $request->get("asunto"), $request->get("mensaje"));
        $correos = $correoDB->get($correoDB->last_id());
        if ($correos) {
            return $app->json(["success" => true, "data" => $correos], 202);
        }
    }

    return $app->json(["success" => 0, "mensaje" => "error"], 404);
});


$app->get('/me/buzon/proveedor/{id_proveedor}', function (Application $app, Request $request, $id_proveedor) {
    $userDB      = new Usuario($app);
    $proveedorDB = new Proveedor($app);
    $user        = $userDB->getUserFromToken($request->get("token"));
    $proveedor   = $proveedorDB->get($id_proveedor, "id_proveedor as id,id_usuario");
    if ($user && $proveedor) {
        $correoDB = new Correo($app);
        $correoDB->mark_leidos($user["id"], $proveedor["id_usuario"]);
        $correos = $correoDB->getListWhere(" (correo.from=".$user["id"]." and correo.to=".$proveedor["id_usuario"]." ) or (correo.to=".$user["id"]." and correo.from=".$proveedor["id_usuario"]." ) and visible=1 ",
                "*", " order by fecha_creacion asc");

        return $app->json(["success" => true, "data" => $correos], 202);
    }

    return $app->json(["success" => 0, "mensaje" => "error"], 404);
});


$app->post('/me/proveedor/{id_proveedor}/calificacion', function (Application $app, Request $request, $id_proveedor) {
    $userDB      = new Usuario($app);
    $bodaDB      = new Boda($app);
    $proveedorDB = new Proveedor($app);
    $user        = $userDB->getUserFromToken($request->get("token"));
    $boda        = $bodaDB->getBodaFromUsuario($user["id"]);
    $proveedor   = $proveedorDB->get($id_proveedor, "id_proveedor as id,id_usuario");

    if ($user && $proveedor && $boda) {
        $datos = [
                "resena"       => $request->get("resena"),
                "calificacion" => $request->get("calificacion"),
                "precio"       => $request->get("precio"),
                "estado"       => $request->get("estado"),
        ];

        $b = $proveedorDB->calificar($boda["id"], $id_proveedor, $datos);
        if ($b) {
            return $app->json(["success" => true, "data" => null], 202);
        }
    }

    return $app->json(["success" => 0, "mensaje" => "error"], 404);
});


$app->delete('/me/proveedor/{id_proveedor}', function (Application $app, Request $request, $id_proveedor) {
    $userDB      = new Usuario($app);
    $bodaDB      = new Boda($app);
    $proveedorDB = new Proveedor($app);
    $user        = $userDB->getUserFromToken($request->get("token"));
    $boda        = $bodaDB->getBodaFromUsuario($user["id"]);
    $proveedor   = $proveedorDB->get($id_proveedor, "id_proveedor as id,id_usuario");
    if ($user && $proveedor && $boda) {
        $b = $proveedorDB->delete_proveedor_boda($boda["id"], $id_proveedor);
        if ($b) {
            return $app->json(["success" => true, "data" => true], 202);
        }
    }

    return $app->json(["success" => 0, "mensaje" => false], 404);
});


$app->post('/me/buzon/proveedor/{id_proveedor}', function (Application $app, Request $request, $id_proveedor) {
    $userDB      = new Usuario($app);
    $proveedorDB = new Proveedor($app);
    $user        = $userDB->getUserFromToken($request->get("token"));
    $proveedor   = $proveedorDB->get($id_proveedor, "id_proveedor as id,id_usuario");
    if ($user && $proveedor) {
        $correoDB = new Correo($app);
        $correo   = [
                "from"    => $user["id"],
                "to"      => $proveedor["id_usuario"],
                "mensaje" => $request->get("mensaje"),
                "asunto"  => $request->get("asunto"),
        ];
        $correoDB->new_correo($proveedor["id_usuario"], $user["id"], $request->get("asunto"), $request->get("mensaje"));
        $correos = $correoDB->get($correoDB->last_id());
        if ($correos) {
            return $app->json(["success" => true, "data" => $correos], 202);
        }
    }

    return $app->json(["success" => 0, "mensaje" => "error"], 404);
});




