<?php use Silex\Application as Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;

$app->get('me/buzon/', function (Application $app, Request $request) {
    try {
        $userDB   = new Usuario($app);
        $mensajes = new Mensajes($app);
        $user     = $userDB->getUserFromToken($request->get("token"));
        $url      = "http://".$request->getHttpHost().str_replace("api/v1", "", $request->getBasePath());
        $bandeja  = $mensajes->getBuzon($user["id"], $url);

        return $app->json(array("success" => true, "data" => $bandeja), 202);
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->get('mensajes/{id_chat}', function (Application $app, Request $request, $id_chat) {
    try {
        $userDB   = new Usuario($app);
        $mensajes = new Mensajes($app);
        $user     = $userDB->getUserFromToken($request->get("token"));
        $url      = "http://".$request->getHttpHost().str_replace("api/v1", "", $request->getBasePath());
        $bandeja  = $mensajes->getChat($user["id"], $id_chat, $url);

        return $app->json(array("success" => true, "data" => $bandeja), 202);
    } catch (Exception $e) {
        return $app->json(array("success" => 0, "mensaje" => $e->getMessage()), 403);
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 403);
});
$app->post('/sendMsj/{id_proveedor}', function (Application $app, Request $request, $id_proveedor) {
    $userDB      = new Usuario($app);
    $proveedorDB = new Proveedor($app);
    $user        = $userDB->getUserFromToken($request->get("token"));
    if ($user) {
        $correoDB = new Correo($app);
        $correo   = array("from" => $user["id"], "to" => $id_proveedor, "mensaje" => $request->get("mensaje"), "asunto" => $request->get("asunto"));
        $correos  = $correoDB->new_correo($id_proveedor, $user["id"], $request->get("asunto"), $request->get("mensaje"));
        if ($correos) {
            return $app->json(array("success" => true, "data" => $correos), 202);
        }
    }

    return $app->json(array("success" => 0, "mensaje" => "error"), 404);
});
