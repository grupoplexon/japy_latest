<?php use Silex\Application as Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;

$app->get('/pais/', function (Application $app, Request $request) {
    $paisDB = new Pais($app);
    $paises = $paisDB->getAll();
    if ($paises && count($paises) > 0) {
        return $app->json(array('success' => true, 'data' => $paises), 202);
    }
    return $app->json(array('success' => false, "status_code" => 1, 'mensaje' => 'Datos no encontrados'), 202);
});

$app->get('/pais/{id_pais}/estado/', function (Application $app, Request $request, $id_pais) {
    $paisDB = new Pais($app);
    $estadoDB = new Estado($app);
    $paises = $paisDB->get($id_pais);
    if ($paises && count($paises) > 0) {
        $estados = $estadoDB->getListWhere("id_pais=$id_pais");
        if ($estados) {
            foreach ($estados as $key => &$p) {
                $p['id'] = intval($p['id']);
                $p['pais'] = intval($p['pais']);
            }
            return $app->json(array('success' => true, 'data' => $estados), 202);
        }
    }
    return $app->json(array('success' => false, 'status_code' => 1, "mensaje" => "Datos no encontrados"), 202);
});

$app->get('/pais/{id_pais}/estado/{id_estado}/ciudad/', function (Application $app, Request $request, $id_pais, $id_estado) {
    $estadoDB = new Estado($app);
    $ciudadDB = new Ciudad($app);
    $estado = $estadoDB->get($id_estado);
    if ($estado && count($estado) > 0) {
        $ciudades = $ciudadDB->getListWhere("id_estado=$id_estado");
        if ($ciudades) {
            return $app->json(array('success' => true, 'data' => $ciudades), 202);
        }
    }
    return $app->json(array('success' => false, 'status_code' => 1, 'mensaje' => 'Datos no encontrados'), 202);
});
