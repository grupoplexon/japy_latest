<?php use Silex\Application as Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;

$app->post('/usuario/recuperar', function (Application $app, Request $request) {
    try {
        $userDB  = new Usuario($app);
        $usuario = $userDB->getFromEmail($request->get("correo"));
        if ($usuario && count($usuario) > 0) {
            $pass = randomLN(8);
            $userDB->update($usuario['id'], array("contrasena" => sha1(md5(sha1($pass)))));
            $mail = getServiceMail();
            $mail->addAddress($request->get("correo"));
            $mail->Subject = 'Clubnupcial.com ';
            $app['twig']->addFilter(new Twig_SimpleFilter('ebase64', 'base64_encode'));
            $mail->Body  = $app['twig']->render('email_recuperacion.php', array("nombre" => $usuario["nombre"]." ".$usuario["apellido"], "password" => $pass));
            $mail->Debug = true;
            $mail->send();

            return $app->json(array("success" => true, "data" => ""), 202);
        } else {
        }
    } catch (Exception $e) {
        echo 'eee';
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
$app->post('/usuario/login', function (Application $app, Request $request) {
    try {
        $userDB  = new Usuario($app);
        $usuario = $userDB->get(array("usuario" => $request->get("usuario"), "contrasena" => sha1(md5(sha1($request->get("password")))), "rol" => 2, "activo" => 1,));
        if ($usuario && count($usuario) > 0) {
            $usuario["token"] = Token::generate($usuario["usuario"], $usuario["contrasena"]);
            unset($usuario["contrasena"]);

            return $app->json(array("success" => true, "data" => $usuario), 202);
        }
    } catch (Exception $e) {
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
$app->post('/usuario/login/facebook', function (Application $app, Request $request) {
    try {
        $userDB  = new Usuario($app);
        $usuario = $userDB->getFacebook($request->get("usuario"), $request->get("id_facebook"));
        if ($usuario && count($usuario) > 0) {
            $usuario["token"] = Token::generate($usuario["usuario"], $usuario["contrasena"]);
            unset($usuario["contrasena"]);

            return $app->json(array("success" => true, "data" => $usuario), 202);
        } elseif ($userDB->existEmail($request->get('usuario'))) {
            return $app->json(array("success" => false, "status_code" => 2, "mensaje" => "2"), 202);
        }
    } catch (Exception $e) {
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "1"), 202);
});
$app->post('/usuario/register/facebook', function (Application $app, Request $request) {
    try {
        $invitadoDB = new Invitado($app);
        $userDB     = new Usuario($app);
        if ($userDB->existEmail($request->get('correo'))) {
            return $app->json(array("success" => false, "status_code" => 2, "mensaje" => "Este correo ya existe,intente con otro."), 202);
        }
        $data    = array("fecha_boda" => $request->get("fecha_boda"), "fecha_creacion" => date('Y-m-d H:i:s'),);
        $id_boda = (new Boda($app))->insert_id($data);
        $image   = file_get_contents("https://graph.facebook.com/".$request->get("id_facebook")."/picture?width=125");
        $o       = new stdClass();
        if (empty($image)) {
            $o->mime = null;
            $o->file = null;
        } else {
            $o->mime = "image/jpg";
            $o->file = $image;
        }
        $data        = array(
            "usuario"        => $request->get('correo'),
            "contrasena"     => sha1(md5(sha1($request->get('id_facebook')))),
            "rol"            => 2,
            "mime"           => $o->mime,
            "foto"           => $o->file,
            "activo"         => 1,
            "nombre"         => $request->get('nombre'),
            "apellido"       => $request->get('apellido'),
            "correo"         => $request->get('correo'),
            "fecha_creacion" => date('Y-m-d H:i:s'),
        );
        $id_usuario  = $userDB->insert_id($data);
        $id_permisos = (new Permisos($app))->insert_id(array(
            'enviar_mensaje'        => 1,
            "participacion_debates" => 1,
            "valorar_publicaciones" => 1,
            "anadir_amigo"          => 1,
            "aceptar_solicitud"     => 1,
            "mension_posts"         => 1,
            "email_diario"          => 1,
            "email_semanal"         => 1,
            "invitaciones"          => 1,
            "concursos"             => 1,
            "visibilidad_todos"     => 1,
            "email_debate"          => 1,
        ));
        $data        = array(
            "genero"           => ($request->get('tipo') == "NOVIO") ? 1 : 2,
            "fecha_nacimiento" => empty($request->get('fecha_nacimiento')) ? "" : date_create_from_format('m/d/Y', $request->get('fecha_nacimiento'))->format('Y-m-d'),
            "id_facebook"      => $request->get('id_facebook'),
            "id_permisos"      => $id_permisos,
            "id_boda"          => $id_boda,
            "id_usuario"       => $id_usuario,
        );
        $id_cliente  = (new Cliente($app))->insert_id($data);
        $id_mesa     = (new Mesa($app))->getMesaPrincipal($id_boda)[0]['id_mesa'];
        $id_grupo    = (new Grupo($app))->getGrupoNovios($id_boda)[0]['id_grupo'];
        $id_menu     = (new Menu($app))->getMenuNombre($id_boda)[0]['id_menu'];
        $rm          = array('nosotros' => $request->get('nombre').' '.$request->get('apellido'), 'fecha_boda' => $request->get('fecha_boda'),);
        (new MiPortal($app))->updateBoda($rm, $id_boda);
        $data = array(
            "nombre"     => $request->get('nombre'),
            "apellido"   => $request->get('apellido'),
            "sexo"       => ($request->get('tipo') == "NOVIO") ? 1 : 2,
            "edad"       => 1,
            "correo"     => $request->get('correo'),
            "confirmado" => 2,
            "id_grupo"   => $id_grupo,
            "id_menu"    => $id_menu,
            "id_mesa"    => $id_mesa,
            "silla"      => 2,
            "id_boda"    => $id_boda,
        );
        $invitadoDB->insert($data);
        $data = array("nombre" => ($request->get('tipo') == "NOVIO") ? 'NOVIA' : 'NOVIO', "sexo" => ($request->get('tipo') == "NOVIO") ? 2 : 1, "edad" => 1, "confirmado" => 1, "id_grupo" => $id_grupo, "id_menu" => $id_menu, "id_mesa" => $id_mesa, "silla" => 3, "id_boda" => $id_boda,);
        $invitadoDB->insert($data);
        $usuario = $userDB->get(array("usuario" => $request->get("correo"), "contrasena" => sha1(md5(sha1($request->get("id_facebook")))), "rol" => 2, "activo" => 1,));
        if ($usuario && count($usuario) > 0) {
            $usuario["token"] = Token::generate($usuario["usuario"], $usuario["contrasena"]);
            unset($usuario["contrasena"]);

            return $app->json(array("success" => true, "data" => $usuario), 202);
        }
    } catch (Exception $e) {
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Intentelo de nuevo más tarde"), 202);
});
$app->post('/usuario/register', function (Application $app, Request $request) {
    try {
        $invitadoDB = new Invitado($app);
        $userDB     = new Usuario($app);
        if ($userDB->existEmail($request->get('correo'))) {
            return $app->json(array("success" => false, "status_code" => 2, "mensaje" => "Este correo ya existe,intente con otro."), 202);
        }
        $data        = array("fecha_boda" => $request->get("fecha_boda"), "fecha_creacion" => date('Y-m-d H:i:s'),);
        $id_boda     = (new Boda($app))->insert_id($data);
        $data        = array(
            "usuario"        => $request->get('correo'),
            "contrasena"     => sha1(md5(sha1($request->get('contrasena')))),
            "rol"            => 2,
            "activo"         => 1,
            "nombre"         => $request->get('nombre'),
            "apellido"       => $request->get('apellido'),
            "correo"         => $request->get('correo'),
            "fecha_creacion" => date('Y-m-d H:i:s'),
        );
        $id_usuario  = $userDB->insert_id($data);
        $id_permisos = (new Permisos($app))->insert_id(array(
            'enviar_mensaje'        => 1,
            "participacion_debates" => 1,
            "valorar_publicaciones" => 1,
            "anadir_amigo"          => 1,
            "aceptar_solicitud"     => 1,
            "mension_posts"         => 1,
            "email_diario"          => 1,
            "email_semanal"         => 1,
            "invitaciones"          => 1,
            "concursos"             => 1,
            "visibilidad_todos"     => 1,
            "email_debate"          => 1,
        ));
        $data        = array("genero" => ($request->get('tipo') == "NOVIO") ? 1 : 2, "pais" => $request->get('pais'), "estado" => $request->get('estado'), "poblacion" => $request->get('poblacion'), "id_permisos" => $id_permisos, "id_boda" => $id_boda, "id_usuario" => $id_usuario,);
        $id_cliente  = (new Cliente($app))->insert_id($data);
        $id_mesa     = (new Mesa($app))->getMesaPrincipal($id_boda)[0]['id_mesa'];
        $id_grupo    = (new Grupo($app))->getGrupoNovios($id_boda)[0]['id_grupo'];
        $id_menu     = (new Menu($app))->getMenuNombre($id_boda)[0]['id_menu'];
        $rm          = array('nosotros' => $request->get('nombre').' '.$request->get('apellido'), 'fecha_boda' => $request->get('fecha_boda'),);
        (new MiPortal($app))->updateBoda($rm, $id_boda);
        $data = array(
            "nombre"     => $request->get('nombre'),
            "apellido"   => $request->get('apellido'),
            "sexo"       => ($request->get('tipo') == "NOVIO") ? 1 : 2,
            "edad"       => 1,
            "correo"     => $request->get('correo'),
            "confirmado" => 2,
            "id_grupo"   => $id_grupo,
            "id_menu"    => $id_menu,
            "id_mesa"    => $id_mesa,
            "silla"      => 2,
            "id_boda"    => $id_boda,
        );
        $invitadoDB->insert($data);
        $data = array("nombre" => ($request->get('tipo') == "NOVIO") ? 'NOVIA' : 'NOVIO', "sexo" => ($request->get('tipo') == "NOVIO") ? 2 : 1, "edad" => 1, "confirmado" => 1, "id_grupo" => $id_grupo, "id_menu" => $id_menu, "id_mesa" => $id_mesa, "silla" => 3, "id_boda" => $id_boda,);
        $invitadoDB->insert($data);
        $usuario = $userDB->get(array("usuario" => $request->get("correo"), "contrasena" => sha1(md5(sha1($request->get("contrasena")))), "rol" => 2, "activo" => 1,));
        if ($usuario && count($usuario) > 0) {
            $usuario["token"] = Token::generate($usuario["usuario"], $usuario["contrasena"]);
            unset($usuario["contrasena"]);

            return $app->json(array("success" => true, "data" => $usuario), 202);
        }
    } catch (Exception $e) {
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Intentelo de nuevo más tarde"), 202);
});
$app->post('/usuario/newregister', function (Application $app, Request $request) {
    try {
        $invitadoDB = new Invitado($app);
        $userDB     = new Usuario($app);
        $bodaDB     = new Boda($app);
        if ($userDB->existEmail($request->get('correo'))) {
            return $app->json(array("success" => false, "status_code" => 2, "mensaje" => "Este correo ya existe,intente con otro."), 202);
        }
        $data        = array("fecha_boda" => $request->get("fecha_boda"), "fecha_creacion" => date('Y-m-d H:i:s'), "presupuesto" => $request->get("presupuesto"), "no_invitado" => (( ! empty($request->get("no_invitados"))) ? intval($request->get("no_invitados")) : 0));
        $id_boda     = $bodaDB->insert_id($data);
        $data        = array(
            "usuario"        => $request->get('correo'),
            "contrasena"     => sha1(md5(sha1($request->get('contrasena')))),
            "rol"            => 2,
            "activo"         => 1,
            "nombre"         => $request->get('nombre'),
            "apellido"       => $request->get('apellido'),
            "correo"         => $request->get('correo'),
            "fecha_creacion" => date('Y-m-d H:i:s'),
        );
        $id_usuario  = $userDB->insert_id($data);
        $id_permisos = (new Permisos($app))->insert_id(array(
            'enviar_mensaje'        => 1,
            "participacion_debates" => 1,
            "valorar_publicaciones" => 1,
            "anadir_amigo"          => 1,
            "aceptar_solicitud"     => 1,
            "mension_posts"         => 1,
            "email_diario"          => 1,
            "email_semanal"         => 1,
            "invitaciones"          => 1,
            "concursos"             => 1,
            "visibilidad_todos"     => 1,
            "email_debate"          => 1,
        ));
        $data        = array("genero" => ($request->get('tipo') == "NOVIO") ? 1 : 2, "pais" => $request->get('pais'), "estado" => $request->get('estado'), "poblacion" => $request->get('poblacion'), "id_permisos" => $id_permisos, "id_boda" => $id_boda, "id_usuario" => $id_usuario,);
        $id_cliente  = (new Cliente($app))->insert_id($data);
        $id_mesa     = (new Mesa($app))->getMesaPrincipal($id_boda)[0]['id_mesa'];
        $id_grupo    = (new Grupo($app))->getGrupoNovios($id_boda)[0]['id_grupo'];
        $id_menu     = (new Menu($app))->getMenuNombre($id_boda)[0]['id_menu'];
        $rm          = array('fecha_boda' => $request->get('fecha_boda'),);
        $nombre1     = $request->get("nombre").' '.$request->get('apellido');
        $nombre2     = $request->get("nombre_invitado").' '.$request->get('apellido_invitado');
        $genero      = $request->get("tipo");
        if ( ! empty($nombre1) && ! empty($nombre2)) {
            $rm['nosotros'] = $nombre1.' & '.$nombre2;
        } elseif (empty ($nombre2)) {
            $rm['nosotros'] = $nombre1;
        } elseif (empty ($nombre1)) {
            $rm['nosotros'] = $nombre2;
        }
        (new MiPortal($app))->updateBoda($rm, $id_boda);
        $data = array(
            "nombre"     => $request->get('nombre'),
            "apellido"   => $request->get('apellido'),
            "sexo"       => ($request->get('tipo') == "NOVIO") ? 1 : 2,
            "edad"       => 1,
            "correo"     => $request->get('correo'),
            "confirmado" => 2,
            "id_grupo"   => $id_grupo,
            "id_menu"    => $id_menu,
            "id_mesa"    => $id_mesa,
            "silla"      => 2,
            "id_boda"    => $id_boda,
        );
        $invitadoDB->insert($data);
        $data = array("nombre" => ($request->get('tipo') == "NOVIO") ? 'NOVIA' : 'NOVIO', "sexo" => ($request->get('tipo') == "NOVIO") ? 2 : 1, "edad" => 1, "confirmado" => 1, "id_grupo" => $id_grupo, "id_menu" => $id_menu, "id_mesa" => $id_mesa, "silla" => 3, "id_boda" => $id_boda,);
        if ( ! empty($request->get('nombre_invitado'))) {
            $data['nombre'] = $request->get('nombre_invitado');
        }
        if ( ! empty($request->get('apellido_invitado'))) {
            $data['apellido'] = $request->get('apellido_invitado');
        }
        $invitadoDB->insert($data);
        $usuario = $userDB->get(array("usuario" => $request->get("correo"), "contrasena" => sha1(md5(sha1($request->get("contrasena")))), "rol" => 2, "activo" => 1,));
        if ($usuario && count($usuario) > 0) {
            $usuario["token"] = Token::generate($usuario["usuario"], $usuario["contrasena"]);
            unset($usuario["contrasena"]);

            return $app->json(array("success" => true, "data" => $usuario), 202);
        }
    } catch (Exception $e) {
        return $app->json(array("success" => false, "mensaje" => $e->getMessage()), 404);
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Intentelo de nuevo más tarde"), 202);
});
$app->get('/me/boda/', function (Application $app, Request $request) {
    $userDB  = new Usuario($app);
    $usuario = $userDB->getUserFromToken($request->get("token"));
    if ($usuario && count($usuario) > 0) {
        $bodaDB = new Boda($app);
        $boda   = $bodaDB->getBodaFromUsuario($usuario["id"]);

        return $app->json(array("success" => true, "data" => $boda), 202);
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
$app->get('/me/cliente/', function (Application $app, Request $request) {
    $userDB  = new Usuario($app);
    $usuario = $userDB->getUserFromToken($request->get("token"));
    if ($usuario && count($usuario) > 0) {
        $clienteBD = new Cliente($app);

        return $app->json(array("success" => true, "data" => $clienteBD->getUser($usuario["id"])), 202);
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
$app->get('/me/photo/', function (Application $app, Request $request) {
    $userDB  = new Usuario($app);
    $usuario = $userDB->getFotoFromToken($request->get("token"));
    if ($usuario && count($usuario) > 0) {
        if (empty($usuario['mime'])) {
            $url     = '.././img/user-novia.png';
            $picture = file_get_contents($url);
            $size    = getimagesize($url);
            header("Content-type: ".$size['mime']);
            echo $picture;
            exit;
        } else {
            header("Content-type: ".$usuario['mime']);
            echo $usuario['foto'];
            exit;
        }
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
$app->post('/me/deleteAcount/', function (Application $app, Request $request) {
    $userDB  = new Usuario($app);
    $usuario = $userDB->getUserFromToken($request->get("token"));
    if ($usuario && count($usuario) > 0) {
        $userDB->update($usuario['id'], array("activo" => 0));

        return $app->json(array("success" => true, "mensaje" => "Cuenta eliminada"), 202);
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
$app->post('/me/usuario/', function (Application $app, Request $request) {
    $token   = "Success";
    $userDB  = new Usuario($app);
    $usuario = $userDB->getUserFromToken($request->get("token"));
    if ($usuario && count($usuario) > 0) {
        if ( ! empty($request->get("contrasena"))) {
            $data['contrasena'] = sha1(md5(sha1($request->get("contrasena"))));
            $token              = Token::generate($usuario["usuario"], $data["contrasena"]);
        }
        if ( ! empty($request->get("correo"))) {
            $data['correo'] = $request->get("correo");
        }
        $userDB->update($usuario['id'], $data);

        return $app->json(array("success" => true, "mensaje" => $token), 202);
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
$app->post('/me/photo/', function (Application $app, Request $request) {
    $userDB  = new Usuario($app);
    $usuario = $userDB->getUserFromToken($request->get("token"));
    if ($usuario && count($usuario) > 0) {
        $data = array("foto" => base64_decode($request->get("base")), "mime" => $request->get("mime"),);
        $userDB->update($usuario['id'], $data);

        return $app->json(array("success" => true, "mensaje" => "Actualizacion correcta"), 202);
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
$app->post('/me/miperfil/', function (Application $app, Request $request) {
    $userDB  = new Usuario($app);
    $usuario = $userDB->getUserFromToken($request->get("token"));
    if ($usuario && count($usuario) > 0) {
        $clienteBD = new Cliente($app);
        $cliente   = $clienteBD->getUser($usuario["id"]);
        $bodaDB    = new Boda($app);
        $boda      = $bodaDB->getBodaFromUsuario($usuario["id"]);
        if ($cliente && count($cliente) > 0 && $boda && count($boda) > 0) {
            $data = array("pais" => $request->get("pais"), "estado" => $request->get("estado"), "poblacion" => $request->get("poblacion"), "telefono" => $request->get("telefono"), "sobre_mi" => $request->get("sobre_mi"),);
            $clienteBD->update($cliente['id'], $data);
            $data = array("nombre" => $request->get("nombre"), "apellido" => $request->get("apellido"),);
            $userDB->update($usuario['id'], $data);
            $data = array("fecha_boda" => $request->get("fecha_boda"), "no_invitado" => $request->get("no_invitado"), "sobre_boda" => $request->get("sobre_boda"),);
            $bodaDB->update($boda['id'], $data);

            return $app->json(array("success" => true, "mensaje" => "Actualizacion correcta"), 202);
        }
    }

    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});
