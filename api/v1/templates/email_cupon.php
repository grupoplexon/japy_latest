<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script type="colorScheme" class="swatch active">
            {
            "name":"Default",
            "bgBody":"ffffff",
            "link":"fff",
            "color":"555555",
            "bgItem":"ffffff",
            "title":"181818"
            }
        </script>
    </head>
    <body>
        <table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class='bgBody'>
            <tr>
                <td>
                    <table cellpadding="0" width="620" class="container" align="center" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                    <tr>
                                        <td class='movableContentContainer bgItem'>

                                            <div class='movableContent'>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                    <tr height="40">
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="200" valign="top">&nbsp;</td>
                                                        <td width="200" valign="top" align="center">
                                                            <div class="contentEditableContainer contentImageEditable">
                                                                <div class="contentEditable" align='center' >

                                                                    {% if not promo.imagen %}
                                                                    <img class="responsive-img"
                                                                         style="width: 100%;max-width: inherit;"
                                                                         src="{{ proveedor.logo}}"/>
                                                                    {% else %}
                                                                    <img class="responsive-img"
                                                                         style="width: 100%;max-width: inherit;"
                                                                         src="data:{{ promo.mime_imagen}};base64,{{ promo.imagen|ebase64 }}"/>
                                                                    {% endif %}


                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td width="200" valign="top">&nbsp;</td>
                                                    </tr>
                                                    <tr height="25">
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="section-print">

                                                <div class="divider"></div>
                                                <br />
                                                <div class="card-panel">
                                                    <div class="row">
                                                        <div class="col m8 s8">

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class='movableContent'>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                    <tr>
                                                        <td width="100%" colspan="3" align="center" style="padding-bottom:10px;padding-top:25px;">
                                                            <div class="contentEditableContainer contentTextEditable">
                                                                <div class="contentEditable" align='center' >
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50">&nbsp;</td>
                                                        <td width="500" align="center">
                                                            <div class="contentEditableContainer contentTextEditable">
                                                                <div class="contentEditable" align='left' >
                                                                    <h2 style="color: #e91e63">{{promo.nombre }}</h2>
                                                                    <b>{{ proveedor.nombre }}</b><br/>
                                                                    <label>Direccion:</label><br />
                                                                    {{ proveedor.localizacion_direccion }}, {{ proveedor.localizacion_poblacion }} ({{ proveedor.localizacion_estado }})
                                                                    <br/>
                                                                    <div class="divider"></div>
                                                                    <br/>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td width="50">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50">
                                                        </td>

                                                        <td width="275" >
                                                            <i class="fa fa-calendar-o"></i> {{promo.finaliza}}
                                                        </td>
                                                        <td width="275" >
                                                            <img class="" src="http://clubnupcial.com/dist/img/clubnupcial.png" style="width: 150px;float: right">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="100%" colspan="3" style="padding-top:35px;">
                                                            <hr style="height:1px;border:none;color:#333;background-color:#ddd;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50">

                                                        </td>
                                                        <td width="275" >
                                                            <h3>Detalle de la oferta</h3>
                                                            <div class="divider"></div>
                                                            <h4>{{ proveedor.nombre}}</h4>
                                                            <p>
                                                                {{promo.mensaje }}
                                                            </p>
                                                        </td>
                                                        <td width="275">

                                                            <div class="card-panel">
                                                                {% if not promo.imagen %}

                                                                <img  class="responsive-img mapa" style="width: 100%"
                                                                      src="http://maps.googleapis.com/maps/api/staticmap?center={{ proveedor.localizacion_latitud }},{{ proveedor.localizacion_longitud }}&zoom=13&scale=false&size=600x300&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7C{{ proveedor.localizacion_latitud }},{{ proveedor.localizacion_longitud }}&key=AIzaSyBy21BRpM9xBRkfK24c3mOUt-0QTuc1GY4" />
                                                                {% endif %}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                    <tr>
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200" align="center" style="padding-top:25px;">
                                                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="200" height="50">

                                                            </table>
                                                        </td>
                                                        <td width="200">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="instrucciones" class='movableContent'>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                    <tr>
                                                        <td width="100%" colspan="2" style="padding-top:65px;">
                                                            <hr style="height:1px;border:none;color:#333;background-color:#ddd;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- End of wrapper table -->

    </body>
</html>