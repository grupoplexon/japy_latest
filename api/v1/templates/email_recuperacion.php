<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script type="colorScheme" class="swatch active">
            {
            "name":"Default",
            "bgBody":"ffffff",
            "link":"fff",
            "color":"555555",
            "bgItem":"ffffff",
            "title":"181818"
            }
        </script>
    </head>
    <body>
        <table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class='bgBody'>
            <tr>
                <td>
                    <table cellpadding="0" width="620" class="container" align="center" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                    <tr>
                                        <td class='movableContentContainer bgItem'>
                                            
                                            
                                            
                                            <div class='movableContent'>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                    <tr height="40">
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="200" valign="top">&nbsp;</td>
                                                        <td width="200" valign="top" align="center">
                                                            <div class="contentEditableContainer contentImageEditable">
                                                                <div class="contentEditable" align='center' >
                                                                    <img src="http://clubnupcial.com/dist/img/clubnupcial.png" style="width: 170%;"  alt='Logo'  data-default="placeholder" />
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td width="200" valign="top">&nbsp;</td>
                                                    </tr>
                                                    <tr height="25">
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200">&nbsp;</td>
                                                        <td width="200">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            
                                            <div id="instrucciones" class='movableContent'>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                    <tr>
                                                        <td width="100%" colspan="2" style="padding-bottom: 20px;">
                                                            <hr style="height:1px;border:none;color:#333;background-color:#ddd;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            
                                            <div class='movableContent'>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                    <tr>
                                                        <td width="100">&nbsp;</td>
                                                        <td width="400" align="center">
                                                            <div class="contentEditableContainer contentTextEditable">
                                                                <div class="contentEditable" align='left' >
                                                                    Hola <b>{{ nombre }}</b><br/>
                                                                    <p>
                                                                        Tu nueva contrase&ntilde;a es:<br/>
                                                                        <b>{{ password }}</b>
                                                                        <br/>
                                                                        Te recomendamos por tu seguridad cambiar tu contrase&ntilde;as
                                                                    </p>
                                                                    <p>

                                                                    </p>
                                                                    <br/>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td width="100">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </div>


                                            <div class='movableContent'>
                                                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                    <tr>
                                                        <td width="100%" colspan="2" style="padding-top:25px; padding-bottom: 25px;">
                                                            <hr style="height:1px;border:none;color:#333;background-color:#ddd;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%" height="70" align="right" valign="top" align='right' style="padding-bottom:20px;">
                                                            <img src="http://clubnupcial.com/dist/img/clubnupcial.png" style="width: 100%;" alt="clubnupcial.com"/>
                                                        </td>
                                                        <td width="60%" height="70" valign="middle" style="padding-bottom:20px; padding-left: 20px;">
                                                            <div class="contentEditableContainer contentTextEditable">
                                                                <div class="contentEditable" align='left' >
                                                                    <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;">Enviado a <?php echo $correo ?> por japy.mx</span>
                                                                    <br/>
                                                                    <span style="font-size:11px;color:#555;font-family:Helvetica, Arial, sans-serif;line-height:200%;">japy.mx | </span>
                                                                    <br/>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
