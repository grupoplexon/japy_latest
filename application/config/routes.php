<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route["default_controller"]   = "home";
$route["404_override"]         = "errors/error_404";
$route["translate_uri_dashes"] = true;

$route["eventos"] = "home/expo_eventos";
$route["inicio"] = "home";

$route["(boda-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)$"]                     = "proveedores/perfil/$2";
$route["(boda-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)(\/faqs)?$"]            = "escaparate/faqs/$2";
$route["(boda-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)(\/videos)?(\/\d+)?$"]  = "escaparate/videos/$2/$4";
$route["(boda-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)(\/mapa)?$"]            = "escaparate/mapa/$2";
$route["(boda-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)(\/recomendaciones)?$"] = "escaparate/recomendaciones/$2";

$route["(mx-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)(\/([\p{L}]*))?(\?.+)?$"] = "proveedores/categoria/$2";
$route["(proveedores)(\?.+)?$"]                                      = "proveedores/sector";


// $route["(promociones)(\?.+)?$"]                                      = "promociones/sector";
$route["(promociones-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)(\/([\p{L}]*))?(\?.+)?$"] = "promociones/categoria/$2";


$route["victoria_samano"] = "StaticBlogs/victoria_samano";
$route["victoria-samano"] = "StaticBlogs/victoria_samano";
$route["pablo_saracho"] = "StaticBlogs/pablo_saracho";
$route["manuel_diaz"] = "StaticBlogs/manuel_diaz";
$route["melissa_cueva"] = "StaticBlogs/melissa_cueva";
$route["bride-w"] = "StaticBlogs/bride_w";
$route["tendencia-oscars"] = "StaticBlogs/tendencia_oscars";

$route["ciudad/{city}"] = "Brideweekend/ciudad";