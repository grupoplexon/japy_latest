<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("ADGaleria/GaleryAlbum_model", "album");
        $this->load->model("ADGaleria/GaleryPhoto_model", "photo");
        $this->load->model("ADGaleria/GaleryComment_model", "coments");
    }

    public function index()
    {
        if (isset($_GET['pagina'])) {
            $inicio = ($_GET['pagina'] - 1) * 15;
            $pagina = $_GET['pagina'];
        } else {
            $inicio = 0;
            $pagina = 1;
        }
        $totalAlbumes    = $this->album->countAlbumes($this->session->userdata('id_usuario'));
        $data['paginas'] = ceil($totalAlbumes / 15);
        $data['pagina']  = $pagina;
        $data["albumes"] = $this->album->getAlbumes($this->session->userdata('id_usuario'), $inicio);
        if ($data["albumes"]) {
            foreach ($data["albumes"] as $key => $value) {
                $value->foto_principal = $this->album->getFotoPrincipal($value->id_album);
            }
            foreach ($data["albumes"] as $key => $value) {
                if (isset($value->foto_principal->id_photo)) {
                    $principal = $value->foto_principal->id_photo;
                } else {
                    $principal = 0;
                }
                $value->fotos = $this->album->getFotos($value->id_album, $principal);
            }
        }
        //print_r($data);
        $this->load->view('admingaleria/index', $data);
    }

    public function addAlbum()
    {
        $this->load->view('admingaleria/addAlbum');
    }

    public function addComenario()
    {
        if ($_POST) {
            $datos['album']   = $this->input->post('album', true);
            $datos['mensaje'] = $this->input->post('mensaje');
            $result           = $this->coments->addComenario($datos['album'], $datos['mensaje'],
                    $this->session->userdata('id_usuario'));
            if ($result) {
                $datos['foto'] = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$this->session->userdata('id_usuario');

                $datos['activo']         = $result->activo;
                $datos['fecha_creacion'] = $result->fecha;
                $datos['usuario']        = $result->usuario;
                $datos['id_comentario']  = $result->id_comentario;
                if ( ! empty($result->mime)) {
                    $datos['foto_usuario'] = base_url()."index.php/novios/comunidad/Home/foto_usuario/$result->id_usuario";
                } else {
                    $datos['foto_usuario'] = base_url().'dist/img/blog/perfil.png';
                }
                if ( ! empty($datos['tipo_novia'])) {
                    $datos['tipo_novia'] = $this->validarTipo_usuario($datos['tipo_novia'], $result->id_usuario);
                }
            }

            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(202)
                    ->set_output(json_encode([
                                            'success' => true,
                                            'data'    => $datos,
                                    ]
                            )
                    );

        }

        return $this->output
                ->set_content_type('application/json')
                ->set_status_header(404)
                ->set_output(json_encode([
                        'success' => false,
                        'data'    => 'error',
                ]));
    }

    public function respuestaComentario()
    {
        if ($_POST) {
            $datos['album']     = $this->input->post('album', true);
            $datos['mensaje']   = $this->input->post('mensaje');
            $datos['respuesta'] = $this->input->post('respuesta');
            $result             = $this->coments->respuestaComentario($datos['album'], $datos['mensaje'],
                    $this->session->userdata('id_usuario'), $datos['respuesta']);
            if ($result) {
                $datos['foto'] = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$this->session->userdata('id_usuario');

                $datos['activo']         = $result->activo;
                $datos['mensaje']        = $result->parent;
                $datos['fecha_creacion'] = $result->fecha;
                $datos['usuario']        = $result->usuario;
                $datos['id_comentario']  = $result->id_comentario;
                if ( ! empty($result->mime)) {
                    $datos['foto_usuario'] = base_url()."index.php/novios/comunidad/Home/foto_usuario/$result->id_usuario";
                } else {
                    $datos['foto_usuario'] = base_url().'dist/img/blog/perfil.png';
                }
                if ( ! empty($datos['tipo_novia'])) {
                    $datos['tipo_novia'] = $this->validarTipo_usuario($datos['tipo_novia'], $result->id_usuario);
                }
            }

            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(202)
                    ->set_output(json_encode([
                                            'success' => true,
                                            'data'    => $datos,
                                    ]
                            )
                    );

        }

        return $this->output
                ->set_content_type('application/json')
                ->set_status_header(404)
                ->set_output(json_encode([
                        'success' => false,
                        'data'    => 'error',
                ]));
    }

    public function editAlbum($id)
    {
        if (isset($_GET['pagina'])) {
            $inicio = ($_GET['pagina'] - 1) * 10;
            $pagina = $_GET['pagina'];
        } else {
            $inicio = 0;
            $pagina = 1;
        }
        $totalAlbumes    = $this->coments->countComents($id);
        $data['paginas'] = ceil($totalAlbumes / 10);
        $data['pagina']  = $pagina;
        $data["album"]   = $this->album->getAlbum($id, $this->session->userdata('id_usuario'));
        if ($data['album']) {
            $data["fotos"]        = $this->album->getAllFotos($id);
            $data["comentarios"]  = $this->coments->getAllComents($id, $inicio);
            $data["comentarios2"] = $this->coments->getAllComentsComents($id);
            $data["foto_logeado"] = base_url()."index.php/perfil/foto/".$this->session->userdata('id_usuario');
            //print_r($data["comentarios"]);
            foreach ($data["comentarios"] as $key => $value) {
                if ( ! empty($value->mime)) {
                    $value->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$value->id_usuario";
                } else {
                    $value->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }

            }
            $this->load->view('admingaleria/editAlbum', $data);
        } else {
            show_404();
        }
    }

    public function desactivarComentario()
    {
        if ($_POST) {
            $id_album   = $this->input->post("album", true);
            $value      = $this->input->post("value", true);
            $comentario = $this->input->post("id_comentario", true);
            $result     = $this->coments->desactivarComentario($id_album, $value, $comentario);
            if ($result) {
                return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(202)
                        ->set_output(json_encode([
                                                'success' => true,
                                        ]
                                )
                        );
            }

            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(404)
                    ->set_output(json_encode([
                                            'success' => false,
                                    ]
                            )
                    );
        }
    }

    function getComent()
    {
        if ($_POST) {
            $id_comentario = $this->input->post('comentario', true);
            $result        = $this->coments->getComent($id_comentario);
            if ($result) {
                $i = 0;

                foreach ($result as $respuesta) {
                    if ( ! empty($respuesta->mime)) {
                        $respuesta->url_foto = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$respuesta->id_usuario;
                    } else {
                        $respuesta->url_foto = base_url().'dist/img/blog/perfil.png';
                    }
                    //print_r($respuesta);
                    $respuesta2[$i] = $respuesta;
                    $i++;
                }
                $datos = ['respuestas' => $respuesta2];

                return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(202)
                        ->set_output(json_encode([
                                                'success' => true,
                                                'data'    => $datos,
                                        ]
                                )
                        );
            }
            $datos = ['respuestas' => []];

            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(202)
                    ->set_output(json_encode([
                                            'success' => true,
                                            'data'    => $datos,
                                    ]
                            )
                    );
        }

        return $this->output
                ->set_content_type('application/json')
                ->set_status_header(404)
                ->set_output(json_encode([
                        'success' => false,
                        'data'    => 'error',
                ]));
    }

    public function agregarAlbum()
    {
        $id_usuario  = $this->session->userdata('id_usuario');
        $nombre      = $this->input->post('nombre', true);
        $descripcion = $this->input->post('descripcion', true);
        $result      = $this->album->addAlbum($id_usuario, $nombre, $descripcion);
        if ($result) {
            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(202)
                    ->set_output(json_encode([
                                            'success' => true,
                                            'data'    => $result,
                                    ]
                            )
                    );
        } else {
            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(404)
                    ->set_output(json_encode([
                            'success' => false,
                            'data'    => 'error',
                    ]));
        }
    }

    public function editarAlbum()
    {
        $id          = $this->input->post('id', true);
        $nombre      = $this->input->post('nombre', true);
        $descripcion = $this->input->post('descripcion', true);
        $result      = $this->album->editAlbum($id, $nombre, $descripcion);
        if ($result) {
            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(202)
                    ->set_output(json_encode([
                                            'success' => true,
                                            'data'    => $result,
                                    ]
                            )
                    );
        } else {
            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(404)
                    ->set_output(json_encode([
                            'success' => false,
                            'data'    => 'error',
                    ]));
        }
    }

    public function fotos()
    {
        $album = $_GET['id_album'];
        if ($_FILES) {
            $this->output->set_content_type('application/json');
            $file = $_FILES["files"];
            if ( ! is_array($file["name"]) && ! empty($file["tmp_name"])) {
                $data    = file_get_contents($file["tmp_name"]);
                $galeria = [
                        "id_album" => $album,
                        "nombre"   => $file["name"],
                        "imagen"   => $data,
                        "tipo"     => 0,
                        "mime"     => $file["type"],
                ];
                $b       = $this->photo->insert($galeria);
                if ($b) {
                    $id = $this->photo->last_id();

                    return $this->output
                            ->set_status_header(202)
                            ->set_output(json_encode([
                                    'success' => true,
                                    'data'    => [
                                            "url" => $this->config->base_url()."index.php/home/imagen_proveedor/".$id,
                                            "id"  => $id,
                                    ],
                            ]));
                }
            }

            return $this->output->set_status_header(404);
        } elseif ($_POST) {
            $this->output->set_content_type('application/json');
            $method = strtoupper($this->input->post("method", true, "String", ["required" => true]));
            $id     = $this->input->post("id", true, "Integer", ["required" => true, "min" => 1]);
            if ($method == "DELETE") {
                if ($this->input->isValid()) {
                    if ($this->photo->delete($id)) {
                        return $this->output
                                ->set_status_header(202)
                                ->set_output(json_encode([
                                                'success' => true,
                                                'data'    => "ok",
                                        ]
                                ));
                    }
                }
            } elseif ($method == "UPDATE") {
                $galeria = [
                        "nombre" => $this->input->post("nombre", true, "String", ["required" => true]),
                ];
                if ($this->input->isValid()) {
                    if ($this->photo->update($id, $galeria)) {
                        return $this->output
                                ->set_status_header(202)
                                ->set_output(json_encode([
                                        'success' => true,
                                        'data'    => [
                                                "url" => $this->config->base_url()."index.php/home/imagen_proveedor/".$id,
                                                "id"  => $id,
                                        ],
                                ]));
                    }
                }
            } elseif ($method == "UPDATE_PRINCIPAL" && $this->input->isValid()) {
                if ($this->photo->setNewPrincipal($id, $album)) {
                    return $this->output
                            ->set_status_header(202)
                            ->set_output(json_encode([
                                    'success' => true,
                                    'data'    => "ok",
                            ]));
                }
            }

            return $this->output->set_status_header(404);
        } else {
            $data["tipo"]    = $this->tipo->get($empresa->id_tipo_proveedor);
            $data["galeria"] = $this->galeria->getImagenesProveedor($empresa->id_proveedor);
            $data["has_lp"]  = $this->galeria->hasLP($empresa->id_proveedor);
            $this->load->view("proveedor/escaparate/fotos", $data);
        }
    }

}