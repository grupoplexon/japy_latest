<?php
use Carbon\Carbon;
use Restserver\Libraries\REST_Controller;
require_once APPPATH . '/libraries/REST_Controller.php';
require_once APPPATH . '/libraries/Format.php';
use Illuminate\Database\Capsule\Manager as DB;

class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Usuario_model", "usuario");
        $this->load->helper("formats_helper");
        $this->load->library("checker");
        $this->load->library("task");
        $this->load->helper("formats");
        $this->load->library("categoria");
        $this->load->model("Tarea_model", "tarea");
        $this->load->model("Presupuesto_model", "presupuesto");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Boda_model", "boda");
        $this->load->model("Login_model");
        $this->load->model("Proveedor/Evento_model", "evento");
        $this->load->model("Correo/Correo_model", "correo");
        $this->load->model("Invitados/Invitado_model", "Invitado_model");
        $this->load->library("My_PHPMailer");
        $this->load->helper("slug");
        $this->load->helper("collection_paginate");
        $this->load->model("Cliente_model", "cliente");
        $this->mailer = $this->my_phpmailer;
        $this->load->helper("cadena");
        header('Access-Control-Allow-Origin: *');
    }

    public function createUser_post() {
        if(User::where("correo", $this->input->post("correo"))->exists()) {
            $this->response(false);
        } else {
            $states              = ["cdmx", "guanajuato", "jalisco", "nuevo_leon", "puebla", "queretaro", "sinaloa", "veracruz"];
            $registeredFromState = $this->input->post("come_from");

            // INSERT BODA
            $data = [
                "fecha_boda"     => $this->input->post("fecha"),
                "fecha_creacion" => date("Y-m-d H:i:s"),
                "presupuesto"    => 0,
                "no_invitado"    => 0,
            ];

            $id_boda = $this->Boda_model->insertBoda($data);

            // INSERT USUARIO
            $genero = empty($this->input->post("genero")) ? 2 : $this->input->post("genero");

            if ($genero == 1) {
                $data["nombre"]   = $this->input->post("nombre");
                $data["apellido"] = "XXX";
            } elseif ($genero == 2) {
                $data["nombre"]   = $this->input->post("nombre");
                $data["apellido"] = "XXX";
            }

            $id_usuario = User::create([
                "usuario"         => $this->input->post("correo"),
                "correo"          => $this->input->post("correo"),
                "contrasena"      => sha1(md5(sha1($this->input->post("contrasena")))),
                "rol"             => 2,
                "activo"          => 1,
                "nombre"          => $data["nombre"],
                "apellido"        => "XXX",
                "registered_from" => $registeredFromState ? $registeredFromState : null,
            ])->id_usuario;

            $id_permisos = Profile::create([
                "enviar_mensaje"        => 1,
                "participacion_debates" => 1,
                "valorar_publicaciones" => 1,
                "anadir_amigo"          => 1,
                "aceptar_solicitud"     => 1,
                "mension_posts"         => 1,
                "email_diario"          => 1,
                "email_semanal"         => 1,
                "invitaciones"          => 1,
                "concursos"             => 1,
                "visibilidad_todos"     => 1,
                "email_debate"          => 1,
            ])->id_permisos;

            // INSERT CLIENTE
            $data = [
                "genero"      => $genero,
                "pais"        => null,
                "estado"      => null,
                "poblacion"   => null,
                "id_permisos" => $id_permisos,
                "id_boda"     => $id_boda,
                "id_usuario"  => $id_usuario,
                "telefono"  => $telefono = $this->input->post("telefono"),
            ];

            $id_cliente = $this->Cliente_model->insertCliente($data);

            $id_mesa = Table::create([
                'tipo'        => 3,
                'nombre'      => 'PRINCIPAL',
                'sillas'      => 4,
                'x'           => 425,
                'y'           => 75,
                'orientacion' => 90,
                'id_boda'     => $id_boda,
            ])->id_mesa;

            //$id_mesa = $this->Mesa_model->getMesaPrincipal($id_boda)->id_mesa;
            $id_grupo = Group::create([
                'grupo'   => 'Novios',
                'id_boda' => $id_boda,
            ])->id_grupo;

            $id_menu = Menu::create([
                'id_boda'     => $id_boda,
                'nombre'      => 'Adultos',
                'descripcion' => 'Menu para adultos',
            ])->id_menu;

            //SETUP DEFAULT
            Group::insert([
                ["grupo" => "Familia", "id_boda" => $id_boda],
                ["grupo" => "Amigos", "id_boda" => $id_boda],
            ]);

            Menu::create([
                "id_boda"     => $id_boda,
                "nombre"      => "Ni���os",
                "descripcion" => "Menu para ni���os",
            ]);

            $baseChores = Chore::whereNull("id_boda")->get();

            foreach ($baseChores as $chore) {
                $newChore          = $chore->replicate();
                $newChore->id_boda = $id_boda;
                $newChore->save();
            }

            if ($genero == 1) {
                $data = [
                    "nombre"     => $this->input->post("nombre"),
                    "apellido"   => "XXX",
                    "sexo"       => 1,
                    "edad"       => 1,
                    "correo"     => $this->input->post("correo"),
                    "confirmado" => 2,
                    "id_grupo"   => $id_grupo,
                    "id_menu"    => $id_menu,
                    "id_mesa"    => $id_mesa,
                    "silla"      => 2,
                    "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);

                $data = [
                    "nombre"     => 'XXX',
                    "apellido"   => 'XXX',
                    "sexo"       => 2,
                    "edad"       => 1,
                    "confirmado" => 1,
                    "id_grupo"   => $id_grupo,
                    "id_menu"    => $id_menu,
                    "id_mesa"    => $id_mesa,
                    "silla"      => 3,
                    "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);

            } elseif ($genero == 2) {
                $data = [
                    "nombre"     => 'XXX',
                    "apellido"   => 'XXX',
                    "sexo"       => 1,
                    "edad"       => 1,
                    "correo"     => $this->input->post("correo"),
                    "confirmado" => 2,
                    "id_grupo"   => $id_grupo,
                    "id_menu"    => $id_menu,
                    "id_mesa"    => $id_mesa,
                    "silla"      => 2,
                    "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);

                $data = [
                    "nombre"     => $this->input->post("nombre"),
                    "apellido"   => "XXX",
                    "sexo"       => 2,
                    "edad"       => 1,
                    "confirmado" => 1,
                    "id_grupo"   => $id_grupo,
                    "id_menu"    => $id_menu,
                    "id_mesa"    => $id_mesa,
                    "silla"      => 3,
                    "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);
            }

            $data = [
                "usuario"    => $id_usuario,
                //"nombre" => $this->input->post('nombre') . ' ' . $this->input->post('apellido'),
                "rol"        => 2,
                "confirmado" => 1,
                "encryption" => generarCadena(),
                "id_boda"    => $id_boda,
                "id_cliente" => $id_cliente,
                "id_usuario" => $id_usuario,
                "genero"     => (($this->input->post("genero") == 1) ? "Novio" : "Novia"),
            ];

            $data["nombre"] = $this->input->post("nombre");
            $estado = User::find($data['usuario']);
            // $data["passsword"] = $contrasena;
            // $data["email"] = $this->input->post("correo");

            $this->session->set_userdata($data);

            $this->mailer->to($this->input->post("correo"), $data["nombre"]);
            $this->mailer->setSubject("Bienvenido a Japy!");

            $template = $this->load->view("templates/email_brideweekend", [
                "email" => $this->input->post("correo"),
                "password" => $this->input->post("contrasena"),
            ], true);

            $this->mailer->setSubject("Bienvenido a Japy");
            $this->mailer->send($template);
            ///////////////////// MAIL DE REGISTRO EN BRIDE WEEKEND //////////////////////////
            $this->mailer->to($this->input->post("correo"), $data["nombre"]);
            $this->mailer->setSubject("Gracias por registrarte en Bride Weekend");

            $template = $this->load->view("templates/email_brideweekendEvent", [
                "nombre" => $this->input->post("nombre"),
                "estado" => $estado['original']['registered_from'],
            ], true);

            $this->mailer->setSubject("Gracias por registrarte en Bride Weekend");
            $this->mailer->send($template);
            
            $content["id_usuario"] = $id_usuario;
            $content["id_boda"] = $id_boda;

            $this->response($content);
        }
    }
    
    public function createTask_post() {
        if ($_POST) {

            $tarea = [
                "id_presupuesto" => null,
                "id_boda"        => $this->input->post("id_boda"),
                "nombre"         => $this->input->post("nombre", true, "String", ["min" => 1, "max" => 255]),
                "tiempo"         => $this->input->post("tiempo", true, "Integer",
                    ["required" => true, "min" => 1, "max" => 9]),
                "categoria"      => $this->input->post("categoria", true, "Integer", ["min" => 1]),
                "descripcion"    => $this->input->post("descripcion", true, "String", ["min" => 0, "max" => 260]),
                "completada"     => 0,
            ];

            if ($this->input->isValid()) {
                $b = $this->tarea->insert($tarea);
                if ($b) {
                    $id   = $this->tarea->last_id();
                    $pago = $this->tarea->get($id);

                    return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $pago,
                        ]));
                }
            }
        }
    }

    public function addNota_post()
    {
        if ($_POST) {
            $boda = $this->input->post('id_boda');
            $id   = $this->input->post('tarea', true, 'Integer', ['min' => 1]);
            $nota = $this->input->post('nota', true, 'String', ['min' => 0, 'max' => 320]);
            if ($this->input->isValid()) {
                $tarea = [
                    'nota' => $nota,
                ];
                $b     = $this->tarea->updateTarea($id, $boda, $tarea);
                if ($b) {
                    return $this->output
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $b,
                        ]));
                }
            }
        }
    }

    public function contact_post()
    {
        $response = [
            "code" => 200,
            "data" => [],
        ];

        $data       = [
            "name"    => "",
            "email"   => "",
            "message" => $this->input->post("message"),
        ];
        $provider   = Provider::find($this->input->post("providerId"));
        $user       = User::find($this->input->post("id_usuario"));
        $userExists = User::where("correo", $data["email"])->first();

        if ( ! $provider) {
            $response["code"]    = 404;
            $response["message"] = "Proveedor inexistente";
        } elseif ($user && $user->provider) {
            $response["code"]    = 403;
            $response["message"] = "Esta funcion es solo para usuarios";
        } elseif ( ! $user && $userExists) {
            //If the user is not logged in but there's a user with the email he's entering
            $response["code"]    = 403;
            $response["message"] = "Ese correo ya ha sido registrado";
        } elseif ( ! $user && ! $userExists) {
            //If the user is not logged in and there's no user with that email
            $data["password"] = base64_encode(random_bytes(10));
            $data["gender"]   = 2;

            $userId = $this->usuario->register($data);

            if ( ! $userId) {
                $response["code"]    = 500;
                $response["message"] = "Oops ocurrio un error intentalo de nuevo mas tarde";
            }

            $user = User::find($userId);
        }

        if ($response["code"] == 200) {
            $provider->indicators()->attach($this->input->post("providerId"), ['type' => 'contact_request']);
            $indicator = $provider->indicators()->latest()->first()->pivot;

            $to   = $this->input->post("providerId");
            $from = $user->id_usuario;
            $asunto = $this->input->post("asunto");
            $parent = $this->input->post("id_correo");
            if(empty($asunto)) {
                $asunto = "Un usuario te esta intentando contactar";
            }

            $correo = [
                "mensaje"       => $this->input->post("message"),
                "asunto"        => $asunto,
                "to"            => $to,
                "from"          => $from,
                "indicator_id"  => $indicator->id,
                "leida_usuario" => 1,
                "parent"    => $parent,
            ];

            $correo = $this->correo->insert($correo);
            $this->correo->sendNotification($from, $provider, "request_info");
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function contactClient_post() {
        if ($_POST) {
            $id_proveedor      = $this->input->post('id_proveedor');
            $id_usuario        = $this->input->post('id_usuario');
            $id        = $this->input->post('id_correo');
            $data['solicitud'] = $this->correo->get(['id_correo' => $id, 'to' => $id_usuario, 'visible_proveedor' => 1]);
            $provider          = Provider::find($id_proveedor);

            $files             = "";
            $correo['asunto']  = $this->input->post("asunto", true, "String", ["required" => true, "minSize" => 1, "maxSize" => 255]);
            $correo['mensaje'] = $this->input->post("mensaje", false, "String", ["required" => true]);

            if ($this->input->isValid()) {
                $correo['parent'] = $id;
                $correo['from']   = $id_proveedor;
                $correo['to']     = $id_usuario;

                // $mail                   = Mail::find($id);
                // $correo['indicator_id'] = $mail->replies()->count() ? $mail->replies->last()->indicator_id : $mail->indicator_id;

                // if ($this->input->post("guardar", true) == "on") {
                //     $id_archivo = $this->savePlantilla();
                // }

                if ( ! empty($id_archivo)) {

                    if (is_numeric($id_archivo)) {
                        $archivo = $this->archivo->get([
                            "id_archivo"   => $id_archivo,
                            "id_proveedor" => $id_proveedor,
                        ]);

                        if ($archivo) {
                            $adjunto = [
                                "id_archivo" => $id_archivo,
                                "nombre"     => $archivo->nombre,
                            ];
                        }
                    } else {
                        $file    = parseBase64($id_archivo);
                        $adjunto = [
                            "mime"   => $file->mime,
                            "data"   => $file->file,
                            "nombre" => "",
                        ];
                    }

                    if ($adjunto && $this->adjunto->insert($adjunto)) {
                        $id_adjunto = $this->adjunto->last_id();
                    }

                }

                if ($this->correo->insert($correo)) {
                    $this->correo->sendEmail();

                    $id_correo = $this->correo->last_id();
                    $correo    = $this->correo->get(["id_correo" => $id_correo]);

                    $provider->indicators()->where('id', $correo->indicator_id)->update(['handled_at' => Carbon::now()]);

                    // if ($id_adjunto && $id_correo) {
                    //     $this->correo->insertArchivo($id_adjunto, $id_correo);
                    // }

                    // if ($data["solicitud"]->estado == 0) {
                    //     $this->correo->update($data["solicitud"]->id_correo, ["estado" => 1]);
                    // }

                    return $this->output
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                        ]));
                }
            }
        }
    }

    public function sugeridosTask_post()
    {
        $page = $this->input->post("page");
        $categoryId = $this->input->post("categoryId");
        $isOnMobile = 0;
        if ( ! is_numeric($categoryId) || ! is_numeric($this->input->post("id_boda"))) {
            return $this->output
                ->set_content_type("application/json")
                ->set_status_header(406)
                ->set_output(
                    json_encode(
                        [
                            "success" => false,
                            "data"    => "",
                        ]
                    )
                );
        }

        $wedding = Wedding::find($this->input->post("id_boda"));

        //This returns the active providers that have already set a price for their service
        $category = Category::with([
            "providers"     => function ($query) use ($wedding) {
                return $query->whereHas("user", function ($query) {
                    return $query->where("activo", "2");
                })->whereHas("FAQ", function ($query) {
                    return $query->where("pregunta", "like", "%¿Cuál es el precio de tu servicio?%");
                })->with([
                    "FAQ"                     => function ($query) {
                        return $query->where("pregunta", "like", "%¿Cuál es el precio de tu servicio?%");
                    },
                    "imagePrincipalMiniature" => function ($query) {
                        return $query->select(["id_galeria", "id_proveedor", "nombre"]);
                    },
                ])->with([
                    "weddings" => function ($query) use ($wedding) {
                        return $query->where("proveedor_boda.id_boda", $wedding->id_boda);
                    },
                ]);
            },
            "subcategories" => function ($query) use ($wedding) {
                return $query->with([
                    "providers" => function ($query) use ($wedding) {
                        return $query->whereHas("user", function ($query) {
                            return $query->where("activo", "2");
                        })->whereHas("FAQ", function ($query) {
                            return $query->where("pregunta", "like", "%¿Cuál es el precio de tu servicio?%");
                        })->with([
                            "FAQ"                     => function ($query) {
                                return $query->where("pregunta", "like",
                                    "%¿Cuál es el precio de tu servicio?%");
                            },
                            "imagePrincipalMiniature" => function ($query) {
                                return $query->select(["id_galeria", "id_proveedor", "nombre"]);
                            },
                        ])->with([
                            "weddings" => function ($query) use ($wedding) {
                                return $query->where("proveedor_boda.id_boda", $wedding->id_boda);
                            },
                        ]);
                    },
                ]);
            },
        ]);

        $category = $category->find($categoryId);

        $budgets = Budget::where("id_boda", $this->input->post("id_boda"));

        //where("category_id", $categoryId)
        if ($category->subcategories->count()) {
            $categoriesId    = $category->subcategories()->pluck("id")->toArray();
            $categoriesId [] = $category->id;
            $budgets         = $budgets->whereIn("category_id", $categoriesId);
        } else {
            $budgets = $budgets->where("category_id", $category->id);
        }

        $budgets     = $budgets->get();
        $budgetPrice = 0;

        foreach ($budgets as $budget) {
            //If that category doesn't have an estimate
            if ($budget->costo_aproximado == 0) {
                //That's probably not a category, it's a custom budget
                if ($budget->percentage == 0) {
                    $budgetPrice += $wedding->presupuesto;
                } else {
                    //It's a category and we'll estimate
                    $budgetPrice += $budget->percentage * $wedding->presupuesto;
                }
            } else {
                $budgetPrice += $budget->costo_aproximado;
            }
        }

        $providers = $category->providers->count() ? $category->providers : new \Illuminate\Support\Collection();

        foreach ($category->subcategories as $subcategory) {
            if ($subcategory->providers->count()) {
                foreach ($subcategory->providers as $provider) {
                    $providers->push($provider);
                }
            }
        }

        $providers = $providers->unique('id_proveedor');

        //Check if the providers are below the weddings budget
        foreach ($providers as $key => $provider) {
            $providerPriceRange = explode("|", $provider->FAQ->first()->getOriginal('pivot_respuesta'));

            if (is_null($providerPriceRange) || count($providerPriceRange) != 2) {
                $providers->forget($key);
                continue;
            }

            $providerPriceRange[0] = str_replace(",", "", $providerPriceRange[0]);
            $providerPriceRange[1] = str_replace(",", "", $providerPriceRange[1]);

            //If my budget is zero, if its greater than the provider max price or if its in between the provider price range
            if ($budgetPrice == 0 || $budgetPrice >= $providerPriceRange[1] || ($providerPriceRange[0] <= $budgetPrice && $budgetPrice <= $providerPriceRange[1])) {
                continue;
            } else {
                $providers->forget($key);
            }
        }

        $providers = $providers->unique('id_proveedor')->sortByDesc('tipo_cuenta');
        $providers = collection_paginate($providers, $isOnMobile ? 1 : 3, $page);
        
        for($i=0; $i<sizeOf($providers); $i++) {
            $providers[$i]->descripcion = strip_tags(str_replace("\r\n", " ", $providers[0]->descripcion));
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(
                json_encode(
                    [
                        'success'  => true,
                        'category' => $category->name,
                        'data'     => $providers,
                    ]
                )
            );
    }
    
    public function validateLoginUser_post() {
        $email    = $this->input->post("user", true, "Email");
        $password = $this->input->post("password", true, "String");
        $result   = $this->Login_model->validar_usuarioApi($email, $password);
        if ($result=='user not exist') {
            $this->response('user not exist');
        } elseif($result=='password incorrect') {
            $this->response('password incorrect');
        } else {
            $data["id_user"] = $result->id_usuario;
            $data["name"] = $result->nombre;
            // $data["apellido"] = $result->apellido;
            $data["email"] = $result->correo;
            $a = Client::where('id_usuario', $result->id_usuario)->first();
            $data["phone"] = $a->telefono;
            $data["isBride"] = $a->genero;
            $boda = DB::table('boda')->where('id_boda', $a->id_boda)->first();
            $date = str_replace(" 00:00:00", "", $boda->fecha_boda);
            $data["weddingDate"] = $date;
            $data["id_boda"] = $a->id_boda;
            $this->response($data);
        }
    }

    public function extractChats_post() {
        $userId      = $this->input->post("id_usuario");
        $currentPage = $this->input->post("pagina") == null ? 1 : $this->input->post("pagina");

        $mails = Mail::with([
            "replies" => function ($query) {
                return $query->visible("user", true);
            },
            "userTo",
        ])->from($userId)->main()->visible("user", true)->get();

        //Just for ordering
        foreach ($mails as $mail) {
            if ($mail->latestReply) {
                $mail->fecha_creacion = $mail->latestReply->fecha_creacion;
            }
        }

        $mails = $mails->sortByDesc("fecha_creacion")->values();

        $data["mails"] = collection_paginate($mails, 10, $currentPage);

        $this->response($data);
    }

    public function chatPrivate_post()
    {
        $mail = Mail::with([
            "replies" => function ($query) {
                return $query->with("attachments")->visible("user", 1);
            },
            "userTo",
        ])->find($this->input->post('id_mail'));

        if (is_null($mail)) {
            show_404();
        }

        $user = User::find($this->input->post("id_usuario"));

        if ($user->id_usuario != $mail->from) {
            return $this->load->view("novios/buzon");
        }

        //Update seen on the emails that are for me
        $mail->replies()->where("to", $this->input->post("id_usuario"))->update(["leida_usuario" => 1]);

        $data["novio"]    = $this->usuario->get($mail->from);
        $data["cliente"]  = $this->cliente->get(["id_usuario" => $this->input->post("id_usuario")]);
        $data["boda"]     = $this->boda->get($data["cliente"]->id_boda);
        $data["titulo"]   = "Solicitud: ".strip_tags(substr($mail->mensaje, 0, 15))."...";
        $data["mail"]     = $mail;
        $data["provider"] = User::find($this->input->post('id_provider'))->provider;
        $id_usuario = DB::table("proveedor")->where("id_usuario", $this->input->post('id_provider'))->first();
        $data["provider"]["image_logo"] = DB::table("galeria")->where("id_proveedor", $id_usuario->id_proveedor)->where("logo", 1)->first();
        // $data["provider"]->image_logo->nombre = base_url()."uploads/images/".$data["provider"]->image_logo->nombre;
        $data["provider"]["image_principal"] = DB::table("galeria")->where("id_proveedor", $id_usuario->id_proveedor)->where("principal", 1)->first();
        // $data["provider"]->image_principal->nombre = base_url()."uploads/images/".$data["provider"]->image_principal->nombre;

        $data["provider"]["descripcion"]  = strip_tags(str_replace("\r\n", " ", $data["provider"]["descripcion"]));

        $this->response($data);
    }







    public function searchProvider_post(){
        
        $search      = is_null($this->input->post("buscar")) ? null : $this->input->post("buscar");
        $page        = is_null($this->input->post('pagina')) ? 1 : $this->input->post('pagina');
        $filterState = is_null($this->input->post('state')) ? 'Jalisco' : $this->input->post('state');
        $estado      = ($this->input->post('state') == "Todos") ? null : $filterState;
        $ciudad      = ($this->input->post("ciudad") == "Todos") ? null : $this->input->post("ciudad");
        $ver         = is_null($this->input->get('ver')) ? 'list' : $this->input->get('ver');
        $idBoda = null;
        $category    = Category::where("name", $search)->first();

        if ( ! $category) {

            $providers = Provider::has("categories")
            ->whereHas("user", function ($query) use ($search) {
                return $query->where("activo", 2);
            })->when($search, function ($query) use ($search) {
                return $query->where(function ($query) use ($search) {
                    return $query->where("nombre", "LIKE", "%$search%")
                        ->orWhere("descripcion", "  LIKE", "%$search%");
                });
            })->where("id_proveedor", "<>", 103)
            ->when($estado, function ($query) use ($estado) {
                return $query->where("localizacion_estado", $estado);
            }, function ($query) {
                return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
            })
            ->when($ciudad, function ($query) use ($ciudad) {
                return $query->where("localizacion_poblacion", $ciudad);
            })
            ->with("FAQ")
            ->orderBy(DB::raw("rand(".date("Ymd").") "))
            ->get();

            if($providers->count() < 1 && empty($search)){ ///Traer todos los proveedores
                $estado = null;
                $providers = Provider::has("categories")
                ->whereHas("user", function ($query) use ($search) {
                    return $query->where("activo", 2);
                })->when($search, function ($query) use ($search) {
                    return $query->where(function ($query) use ($search) {
                        return $query->where("nombre", "LIKE", "%$search%")
                            ->orWhere("descripcion", "LIKE", "%$search%");
                    });
                })->where("id_proveedor", "<>", 103)
                ->with("FAQ")
                ->orderBy(DB::raw("rand(".date("Ymd").") "))
                ->get();
            }
            $providers = $providers->sortByDesc("tipo_cuenta");

            $data["page"]      = $page;
            $data["providers"] = collection_paginate($providers, 10, $page);

            foreach ($data["providers"]->items() as $provider) {
                $userId = $this->session->userdata("id_usuario") ? $this->session->userdata("id_usuario") : [null];
                $type   = $search ? "appearance_search" : "appearance_category";
                $provider->indicators()->attach($userId, ["type" => $type]);
                $provider->load("imagePrincipal");

            }
            
            for($i=0; $i<sizeOf($data["providers"]); $i++) {
                $data["providers"][$i]["descripcion"] = html_entity_decode(strip_tags(str_replace("\r\n", " ", $data["providers"][$i]["descripcion"])));
            }
            
            $data["providers"][0]["images"] = DB::table("galeria")->where("id_proveedor", $data["providers"][0]->id_proveedor)->get();

            for($i=0; $i<sizeOf($data["providers"][0]["images"]); $i++) {
                $data["providers"][0]["images"][$i]->nombre = base_url()."uploads/images/".$data["providers"][0]["images"][$i]->nombre;
            }
            
            //$data["providers"] = $providers;

            $this->response($data["providers"]);
        } else {
            $categoryIds[] = $category->id;

            $search = null;  
            
            if ($category->subcategories->count()) {
                $categoryIds = array_merge($categoryIds, $category->subcategories->pluck("id")->toArray());
            }
    
            $providers = new \Illuminate\Support\Collection();
    
            foreach ($categoryIds as $categoryId) {
                $categoryProviders = Category::with([
                    "providers" => function ($query) use ($search, $estado, $ciudad) {
                        return $query->when($search, function ($query) use ($search) {
                            return $query->where(function ($query) use ($search) {
                                return $query->where("nombre", "LIKE", "%$search%")
                                    ->orWhere("descripcion", "LIKE", "%$search%");
                            });
                        })->whereHas("user", function ($query) use ($search) {
                            return $query->where("activo", 2);
                        })->where("id_proveedor", "<>", 103)
                            ->when($estado, function ($query) use ($estado) {
                                return $query->where("localizacion_estado", $estado);
                            }, function ($query) {
                                return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
                            })
                            ->when($ciudad, function ($query) use ($ciudad) {
                                return $query->where("localizacion_poblacion", $ciudad);
                            })
                            ->orderBy(DB::raw("rand(".date("Ymd").") "));
                    },
                ])->find($categoryId)->providers;
    
                foreach ($categoryProviders as $provider) {
                    $providers->push($provider);
                }
            }
    
            // dd($providers->count());
            $data["state"]     = $estado;
            if($providers->count() < 1 && empty($search)){ ///Traer todos los proveedores
                $estado = null;
                foreach ($categoryIds as $categoryId) {
                    $categoryProviders = Category::with([
                        "providers" => function ($query) use ($search, $estado, $ciudad) {
                            return $query->when($search, function ($query) use ($search) {
                                return $query->where(function ($query) use ($search) {
                                    return $query->where("nombre", "LIKE", "%$search%")
                                        ->orWhere("descripcion", "LIKE", "%$search%");
                                });
                            })->whereHas("user", function ($query) use ($search) {
                                return $query->where("activo", 2);
                            })->where("id_proveedor", "<>", 103)
                                ->when($estado, function ($query) use ($estado) {
                                    return $query->where("localizacion_estado", $estado);
                                }, function ($query) {
                                    return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
                                })
                                ->when($ciudad, function ($query) use ($ciudad) {
                                    return $query->where("localizacion_poblacion", $ciudad);
                                })
                                ->orderBy(DB::raw("rand(".date("Ymd").") "));
                        },
                    ])->find($categoryId)->providers;
        
                    foreach ($categoryProviders as $provider) {
                        $providers->push($provider);
                    }
                }
            }
    
            $providers = $providers->unique('id_proveedor');
    
            $providers = $providers->sortByDesc('tipo_cuenta');
    
            /// por foreach contar cuantas cuentas son de pago...
            $cuentas_pago = 0;
            $providers_basic = clone($providers);
            $providers_vip = [];
            foreach ($providers_basic as $k => $v) {
                if($v->tipo_cuenta > 0){
                    $cuentas_pago+=1;
                    // $providers_vip[$k]=$providers[$k];
                    unset($providers_basic[$k]);
                }
                else{}
                    // $providers_basic[$k]=$providers[$k];
            }
            $data["providers"] = collection_paginate($providers_basic, $ver == "list" ? 10 : 12, $page);
            $data["lastpage"] = $data["providers"]->lastpage()+1;
            unset($data["providers"]);
            /// y condicionar cuantos va a enviar en la pagina = 1 para
            // y la pagina = 2 empieze en su lugar
            $data["category"]  = $category;
            if($cuentas_pago > 0 && $page==1){///hay vips y es la primer pagina
                $data["providers"] = collection_paginate($providers, $ver == "list" ? $cuentas_pago : $cuentas_pago, $page);
            }elseif($cuentas_pago > 0 && $page>1){////hay vips y es otra pagina
                $data["providers"] = collection_paginate($providers_basic, $ver == "list" ? 10 : 12, $page-1);
            }else
                $data["providers"] = collection_paginate($providers, $ver == "list" ? 10 : 12, $page);
    
            // $data["category"]  = $category;
            // $data["providers"] = collection_paginate($providers, $ver == "list" ? 10 : 12, $page);
            $data["page"]      = $page;
            // $data["state"]     = $estado;
    
            foreach ($data["providers"]->items() as $provider) {
                $userId = $this->session->userdata("id_usuario") ? $this->session->userdata("id_usuario") : [null];
                $type   = $search ? "appearance_search" : "appearance_category";
                $provider->indicators()->attach($userId, ["type" => $type]);
                $provider->load("imagePrincipal", "images");
    
                foreach ($provider->FAQ as $FAQ) {
                    $provider->capacidad = ($FAQ->tipo == 'RANGE' && $FAQ->valores == 'PERSONAS') ? explode("|", $FAQ->pivot->respuesta) : '';
                    $provider->precio    = ($FAQ->tipo == 'RANGE' && $FAQ->valores == null) ? explode("|", $FAQ->pivot->respuesta) : '';
                }
    
                if ($provider->weddings->count() && $idBoda) {
                    $provider->favorito = $provider->weddings->keyBy('id_boda')->get($idBoda) ? 1 : 0;
                }
            }
    
            $data["servicio"]          = "ALL";
            $data["sector"]            = "ALL";
            $data["estado"]            = "ALL";
            $data["ciudad"]            = "ALL";
            $data["total_proveedores"] = 0;
    
            $sector  = $data["sector"] == "ALL" ? null : $data["sector"];
            $filtros = $this->input->get();
    
            $data["promociones"] = Promotion::whereHas("provider", function ($query) {
                return $query->has("categories");
            })->where("fecha_fin", ">", Carbon::now())->get();
            $data["total"]       = $providers->count();
    
            $data["ver"]               = $ver;
            // $data["tipos"]             = $this->tipo->getAll();
            $data["total_proveedores"] = $this->proveedor->count();
            // $data["estados"]           = $this->tipo_proveedor->getEstados();

            for($i=0; $i<sizeOf($data["providers"]); $i++) {
                $data["providers"][$i]["descripcion"] = html_entity_decode(strip_tags(str_replace("\r\n", " ", $data["providers"][$i]["descripcion"])));
            }

            //$data["providers"] = $providers;

            $this->response($data["providers"]);


        }

    }

    public function favoritesList_post(){
        $id_usuario = is_null($this->input->post("id_usuario")) ? null : $this->input->post("id_usuario");

        $usuario = DB::table('cliente')->where("id_usuario", $id_usuario)->first();
        $idBoda = $usuario->id_boda;

        $data["totales"] = Category::whereHas('subcategories', function ($query) use ($idBoda) {
            return $query->hasProvidersAndWedding($idBoda);
        })->with([
                'subcategories' => function ($query) use ($idBoda) {
                    return $query->hasProvidersAndWedding($idBoda)->withProvidersAndWedding($idBoda);
                },
        ])->get();

        $id_boda             = $usuario->id_boda;
        $data["proveedores"] = $this->boda->getProveedores($id_boda);

        $subcategories = "";
        if(!empty($data["totales"]))
        foreach($data["totales"] as $total){
            $data["subcategories"] = $total->subcategories;
            if(!empty($data["subcategories"]))
            foreach($data["subcategories"] as $total){
                $data["providers"] = $total->providers;
                if(empty($data["providers"][0]) == false)// Saber si es collection
                {
                    foreach ($data["providers"] as $k => $v) {
                        if(!empty($data["providers"][$k]->imagePrincipal->first()))
                            $data["providers"][$k]->imagePrincipal->first()->nombre;
                        if(!empty($data["providers"][$k]['imagePrincipal'][0]['nombre']))
                            $data["providers"][$k]['image_provider'] = base_url()."uploads/images/".$data["providers"][$k]['imagePrincipal'][0]['nombre'];
                        if(!empty($data["providers"][$k]['imagePrincipal']))
                        unset($data["providers"][$k]['imagePrincipal']);
                    }
                }
                else{
                }
            }
        }
        
        for($i=0; $i<sizeOf($data["totales"]); $i++) {
            $faqs = $this->getData($data["totales"][$i]->subcategories[0]->providers[0]->slug);
            $data["totales"][$i]->subcategories[0]->providers[0]["faq"] = $faqs;
            //$slug[$i] = $data["totales"][$i]->subcategories[0]->providers[0]->slug;
        }
        //$data["totales"][0]->subcategories[0]->providers[0]->slug
        
        $this->response( $data["totales"]);
    }



    private function getData($slug)
    {
        $provider = Provider::with([
            "images"     => function ($query) {
                return $query->where("parent", null);
            },
            "videos",
            "promotions" => function ($query) {
                return $query->where("fecha_fin", ">", Carbon::now());
            },
            "collaborators",
            "FAQ",
            "categories",
        ])->where("slug", $slug)->first();

        $data            = $provider->FAQ;

        return $data;
    }




    ////// Guardar Proveedor a Favoritos
    public function saveFavoriteProvider_post()
    {
        $id_user        = $this->input->post("id_usuario");
        $id_proveedor   = $this->input->post("id_proveedor");
        if(empty($id_user) || empty($id_proveedor)){
            return 0;
        }
        $aux = $this->usuario->getCliente($id_user);
        $id_boda = $aux->id_boda;

        if (is_numeric($id_boda) && $id_boda > 0) {
            $isFavorito = $this->proveedor->isFavorito($id_proveedor, $id_boda);

            if ($isFavorito) {
                $this->proveedor->no_favorito($id_proveedor, $id_boda);
            } else {
                $this->proveedor->favorito($id_proveedor, $id_boda);
            }

            $isFavorito = $this->proveedor->isFavorito($id_proveedor, $id_boda);

            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(
                    json_encode(
                        [
                            'success' => true,
                            'data'    => ["favorito" => $isFavorito],
                        ]
                    )
                );
        }
        return $this->output->set_status_header(404);
    }
    ////// Lista de Actividades de Usuario
    public function activityList_post()
    {
        $id_user   = $this->input->post("id_usuario");

        if(empty($id_user)){    
            $this->response($id_user);
            return 0;
        }

        $aux = $this->usuario->getCliente($id_user);
        $id_boda = $aux->id_boda;
        $estado = $this->input->post("estado", true, "Integer", ["min" => 0, "max" => 1]);
        $group  = $this->input->post("grupo");
        $boda   = $id_boda;
        $cat    = $this->input->post("cat", true, "Integer", ["min" => 0]);
        $s      = ["id_boda" => $id_boda];

        // dd($estado,$group,$boda,$cat,$s);

        if ($estado !== null) {
            $s["completada"] = $estado;
        }

        if ($cat) {
            $s["categoria"] = $cat;
        }

        $data["estado"]    = $estado;
        $data["categoria"] = $cat;

        if (strtolower($group) == "categoria") {
            $g = "categoria";
        } else {
            $g = "tiempo";
        }

        $data["grupo"]             = $g;
        $fecha2                    = Wedding::where('id_boda', $boda)->get(['fecha_creacion'])->first();
        $data["fecha_creacion"]    = $fecha2->fecha_creacion;
        $data["fecha_boda"]        = $this->tarea->getFechaBoda($boda);
        $data["total_completadas"] = $this->tarea->getTotalCompletadas($boda);
        $data["tareas"]            = $this->tarea->getListOrderBy($s, $g, "asc");
        $data['categorias']        = Categoria::getCategorias();
        
        if ($data["tareas"]) {
            foreach ($data["tareas"] as $key => &$tarea) {
                if ($cat && ($tarea->categoria != $cat)) {
                    unset($data['tareas'][$key]);
                    continue;
                }

                $tarea->presupuesto = $this->presupuesto->get($tarea->id_presupuesto);

                foreach ($data['categorias'] as $categoria) {
                    if ($tarea->categoria != $categoria->id) {
                        continue;
                    } else {
                        $tarea->categoria_titulo = $categoria->nombre;
                        break;
                    }
                }

                if ( ! is_null($tarea->id_proveedor)) {
                    $provider            = Provider::with(['imagePrincipal', 'categories'])->find($tarea->id_proveedor);
                    $provider->principal = $provider->imagePrincipal->pluck('nombre')->first();
                    unset($provider->imagePrincipal);
                    $tarea->proveedor = $provider;
                }
            }
        }

        $data['tareas'] = is_array($data['tareas']) ? array_values($data['tareas']) : [];

        foreach ($data["categorias"] as $key => &$cat) {
            $c              = str_sin_acentos(strtoupper($cat->nombre));
            $cat->total     = $this->boda->countCategoria($boda, $c);
            $cat->proveedor = $this->boda->getProveedorCategoria($boda, $c);
            if ($cat->proveedor) {
                $cat->proveedor->principal = $this->proveedor->getPrincipal($cat->proveedor->id_proveedor);
            }
        }

        if ($boda) {
            $data["weddingProviders"] = Wedding::with([
                "providers" => function ($query) {
                    return $query->whereHas("user", function ($query) {
                        return $query->where("activo", 1);
                    });
                },
            ])->find($boda)->providers;
        }

        $this->response($data);
    }
    ////// Cambiar Estatus de Actividad
    public function changeActivityStatus_post()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $id         = $this->input->post("tarea", true, "Integer", ["min" => 1]);
            $completada = $this->input->post("completada", true, "Integer", ["min" => 0, "max" => 1]);
            if ($this->input->isValid()) {
                $tarea = ["completada" => $completada];
                $b     = $this->tarea->update($id, $tarea);
                if ($b) {
                    return $this->output
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $b,
                        ]));
                }
            }
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => null,
            ]));
    }
    ////// Eliminar Actividad del Usuario
    public function deleteActivity_post()
    {
        $this->output->set_content_type('application/json');
        $boda  = $this->session->userdata("id_boda");
        $tarea = $this->input->post("tarea", true, "Integer", ["min" => 1]);
        if ($this->input->isValid()) {
            $b = $this->tarea->delete(["id_tarea" => $tarea]);

            return $this->output
                ->set_status_header(202)
                ->set_output(json_encode([
                    'success' => true,
                    'data'    => $b,
                ]));
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => null,
            ]));
    }
    
    
    public function teamBride_get(){
        $this->output->set_content_type('application/javascript');
        $team = DB::table('teamBride')->get();
        
        // return $team;
        // dd($team);
        $team = $team->count();
            return $this->output
                ->set_status_header(202)
                ->set_output(json_encode([
                    'success' => true,
                    'data'    => $team,
                ]));
    
    }

}