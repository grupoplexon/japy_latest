<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADDtSeccion extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADDtSeccion_model', 'ADDtSeccion_model');
        $this->load->model('App/ADSeccion_model', 'ADSeccion_model');
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
    }

    public function editar($id_dt_seccion)
    {
        if ($_POST) {
            $datos = [
                    "nombre"      => $this->input->post("nombre"),
                    "descripcion" => $this->input->post("descripcion"),
                    "pag_web"     => $this->input->post("pag_web"),
                    "red_social"  => $this->input->post("red_social"),
                    "direccion"   => $this->input->post("direccion"),
                    "horario"     => $this->input->post("horario"),
                    "latitud"     => $this->input->post("latitud"),
                    "longitud"    => $this->input->post("longitud"),
                    "correo"      => $this->input->post("correo"),
                    "telefono"    => $this->input->post("telefono"),
                    "fax"         => $this->input->post("fax"),
                    "giro"        => $this->input->post("giro"),
                    "imagen"      => $this->input->post("imagen"),
            ];
            $this->ADDtSeccion_model->updateDtSeccion($datos, $this->input->post("id_dt_seccion"));
            redirect("App/ADExpo/expo/".$this->input->post("id_expo"), 'refresh');
        }
        $result['dt_seccion'] = $this->ADDtSeccion_model->getDtSeccion($id_dt_seccion);
        $result['seccion']    = $this->ADSeccion_model->getSeccion($result['dt_seccion']->id_seccion);
        $result['expo']       = $this->ADExpo_model->getExpo($result['dt_seccion']->id_expo);
        $this->load->view('app/expo/seccion/formulario_dt', $result);
    }

    public function alta($id_seccion, $id_expo)
    {
        if ($_POST) {
            $datos = [
                    "nombre"      => $this->input->post("nombre"),
                    "descripcion" => $this->input->post("descripcion"),
                    "pag_web"     => $this->input->post("pag_web"),
                    "red_social"  => $this->input->post("red_social"),
                    "direccion"   => $this->input->post("direccion"),
                    "horario"     => $this->input->post("horario"),
                    "latitud"     => $this->input->post("latitud"),
                    "longitud"    => $this->input->post("longitud"),
                    "correo"      => $this->input->post("correo"),
                    "telefono"    => $this->input->post("telefono"),
                    "fax"         => $this->input->post("fax"),
                    "giro"        => $this->input->post("giro"),
                    "imagen"      => $this->input->post("imagen"),
                    "id_seccion"  => $this->input->post("id_seccion"),
                    "id_expo"     => $this->input->post("id_expo"),
            ];
            $this->ADDtSeccion_model->insertDtSeccion($datos);
            redirect("App/ADExpo/expo/$id_expo", 'refresh');
        }

        $result['seccion'] = $this->ADSeccion_model->getSeccion($id_seccion);
        $result['expo']    = $this->ADExpo_model->getExpo($id_expo);
        $this->load->view('app/expo/seccion/formulario_dt', $result);
    }

    public function eliminar($id_dt_seccion)
    {
        $this->ADDtSeccion_model->deleteDtSeccion($id_dt_seccion);
        redirect_back();
    }
}