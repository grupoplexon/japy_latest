<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADExpo extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
        $this->load->model('App/ADPublicidad_model', 'ADPublicidad_model');
        $this->load->model('App/ADLugar_model', 'ADLugar_model');
        $this->load->model('App/ADNotificacion_model', 'ADNotificacion_model');
        $this->load->model('App/ADPrograma_model', 'ADPrograma_model');
        $this->load->model('App/ADPatrocinador_model', 'ADPatrocinador_model');
        $this->load->model('App/ADGanador_model', 'ADGanador_model');
    }

    public function index()
    {
        $data['titulo'] = 'Expo tu boda';
        $data['expos']  = $this->ADExpo_model->getAllExpos();
        date_default_timezone_set('America/Mexico_City');
        $this->load->view('app/expo/inicial/lista', $data);
    }

    public function expo($id_expo)
    {
        $return['expo']       = $this->ADExpo_model->getExpo($id_expo);
        $return['titulo']     = strtoupper($return['expo']['nombre']);
        $return['publicidad'] = $this->ADPublicidad_model->getAllExpo($id_expo);
        //$return['notificaciones'] = $this->ADNotificacion_model->getAllExpo($id_expo);
        $return['programa'] = $this->ADPrograma_model->getAllExpo($id_expo);
        $return['lugares']  = $this->ADLugar_model->getAllLugar($id_expo);
        //print_r($return['lugares']):
        $return['expositores']    = $this->ADExpo_model->getExpositores($id_expo);
        $return['patrocinadores'] = $this->ADPatrocinador_model->getPatrocinadores($id_expo);
        $return['ganadores']      = $this->ADGanador_model->getGanadores($id_expo);
        $this->load->view('app/expo/inicial/dt_expo', $return);
    }

    public function alta()
    {
        $return['titulo'] = 'Expo tu boda';
        if ($_POST) {
            $data = [
                'nombre'    => $this->input->post('nombre'),
                'costo'     => $this->input->post('costo'),
                'ubicacion' => $this->input->post('ubicacion'),
                'Home'      => $this->input->post('Home'),
                'fin'       => $this->input->post('fin'),
                'latitud'   => $this->input->post('latitud'),
                'longitud'  => $this->input->post('longitud'),
                'imagen'    => $this->input->post('imagen'),
                'plano'     => $this->input->post('plano'),
                'activo'    => 1,
            ];
            if (empty($data['Home']) || empty($data['fin']) || empty($data['latitud']) || empty($data['longitud'])) {
                $return['mensaje'] = 'Faltan datos por llenar';
                $return['datos']   = $data;
            } else {
                $this->ADExpo_model->insertExpo($data);
                redirect('App/ADExpo', 'refresh');
            }
        }
        $this->load->view('app/expo/inicial/formulario', $return);
    }

    public function editar($id)
    {
        $return['datos']  = $this->ADExpo_model->getExpo($id);
        $return['titulo'] = 'EXPO';
        if ($this->input->post('activo') && $this->input->post('activo') == "on") {
            $activo = 1;
        } else {
            $activo = 0;
        }
        if ($_POST) {
//            dd($this->input->post());

            $data = [
                'nombre'    => $this->input->post('nombre'),
                'costo'     => $this->input->post('costo'),
                'ubicacion' => $this->input->post('ubicacion'),
                'Home'      => $this->input->post('Home'),
                'fin'       => $this->input->post('fin'),
                'latitud'   => $this->input->post('latitud'),
                'longitud'  => $this->input->post('longitud'),
                'imagen'    => $this->input->post('imagen'),
                'plano'     => $this->input->post('plano'),
                'activo'    => $activo,
            ];

            if (empty($data['Home']) || empty($data['fin']) || empty($data['latitud']) || empty($data['longitud'])) {
                $return['mensaje'] = 'Faltan datos por llenar';
                $return['datos']   = $data;
            } else {
                $this->ADExpo_model->updateExpo($data, $id);
                redirect('App/ADExpo/editar/'.$id, 'refresh');
            }
        }
        $this->load->view('app/expo/inicial/formulario', $return);
    }

    public function eliminar($id)
    {
        $this->ADExpo_model->deleteExpo($id);
        redirect_back();
    }

}
