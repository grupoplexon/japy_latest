<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADExpositor extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
        $this->load->model('App/ADExpositor_model', 'ADExpositor_model');
    }

    public function index($id_expo = null)
    {
        $return['expositores'] = $this->ADExpositor_model->getAllExpo($id_expo);
        $return['titulo']      = "Expositores";
        $return['id_expo']     = $id_expo;
        $this->load->view('app/expo/expositor/lista', $return);
    }

    public function alta($id_expo = null)
    {
        if ($_POST) {
            $data = [
                    "nombre"    => $this->input->post('nombre'),
                    "direccion" => $this->input->post('direccion'),
                    "telefono"  => $this->input->post('telefono'),
                    "facebook"  => $this->input->post('facebook'),
                    "web"       => $this->input->post('web'),
                    "correo"    => $this->input->post('correo'),
                    "giro"      => $this->input->post('giro'),
                    'foto'      => $this->input->post('imagen'),
                    "id_expo"   => $this->input->post('id_expo'),
            ];
            $this->ADExpositor_model->insertExpositor($data);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['expos']   = $this->ADExpo_model->getAllExpos();
        $return['id_expo'] = $id_expo;
        $this->load->view('app/expo/expositor/formulario', $return);
    }

    public function editar($id_expositor)
    {
        if ($_POST) {
            $data = [
                    "nombre"    => $this->input->post('nombre'),
                    "direccion" => $this->input->post('direccion'),
                    "telefono"  => $this->input->post('telefono'),
                    "facebook"  => $this->input->post('facebook'),
                    "web"       => $this->input->post('web'),
                    "correo"    => $this->input->post('correo'),
                    "giro"      => $this->input->post('giro'),
                    'foto'      => $this->input->post('imagen'),
                    "id_expo"   => $this->input->post('id_expo'),
            ];
            $this->ADExpositor_model->updateExpositor($data, $id_expositor);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['expositor'] = $this->ADExpositor_model->getExpositor($id_expositor);
        $return['expos']     = $this->ADExpo_model->getAllExpos();
        $return['id_expo']   = $return['expositor']->id_expo;
        $this->load->view('app/expo/expositor/formulario', $return);
    }

    public function eliminar($id)
    {
        $this->ADExpositor_model->deleteExpositor($id);
        redirect_back();
    }
}