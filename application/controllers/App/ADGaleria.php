<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADGaleria extends MY_Controller
{

    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADPublicidad_model', 'ADPublicidad_model');
        $this->load->model('App/ADGaleria_model', 'ADGaleria_model');
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
    }

    public function index()
    {
        $data['albums'] = $this->ADGaleria_model->getAll();
        $this->load->view('app/galeria/index', $data);
    }

    public function alta()
    {
        if ($_POST) {
            $data            = [
                    "fecha_inicio"  => $this->input->post("fecha_inicio"),
                    "fecha_termino" => $this->input->post("fecha_termino"),
                    "tipo"          => $this->input->post("tipo"),
                    "id_expo"       => $this->input->post("id_expo"),
                    "imagen"        => $this->input->post("imagen"),
            ];
            $data['id_expo'] = ($data['id_expo'] == 0) ? null : $data['id_expo'];
            $this->ADPublicidad_model->insertPublicidad($data);
            redirect('App/ADGaleria', 'refresh');
        }
        $result['expos'] = $this->ADExpo_model->getAllExpos();
        $this->load->view('app/galeria/formulario', $result);
    }

    public function editar($id_publicidad)
    {
        if ($_POST) {
            $data            = [
                    "fecha_inicio"  => $this->input->post("fecha_inicio"),
                    "fecha_termino" => $this->input->post("fecha_termino"),
                    "tipo"          => $this->input->post("tipo"),
                    "id_expo"       => $this->input->post("id_expo"),
                    "imagen"        => $this->input->post("imagen"),
            ];
            $data['id_expo'] = empty($data['id_expo']) ? null : $data['id_expo'];
            $this->ADPublicidad_model->updatePublicidad($data, $id_publicidad);
            redirect('App/ADPublicidad', 'refresh');
        }
        $result['publicidad'] = $this->ADPublicidad_model->getPublicidad($id_publicidad);
        $result['expos']      = $this->ADExpo_model->getAllExpos();
        $this->load->view('app/publicidad/formulario', $result);
    }

    public function eliminar($id_album)
    {
        $this->ADGaleria_model->deleteAlbum($id_album);
        redirect_back();
    }

}
