<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADGanador extends MY_Controller
{

    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
        $this->load->model('App/ADGanador_model', 'ADGanador_model');
    }

    public function index($id_expo = null)
    {
        $return['ganadores'] = $this->ADGanador_model->getAllGanadores($id_expo);
        $return['titulo']    = "Ganador";
        $return['id_expo']   = $id_expo;
        $this->load->view('app/expo/ganador/lista', $return);
    }

    public function alta($id_expo = null)
    {
        if ($_POST) {
            $data = [
                    "nombre"  => $this->input->post('nombre'),
                    "premio"  => $this->input->post('premio'),
                    "id_expo" => $this->input->post('id_expo'),
            ];
            $this->ADGanador_model->insertGanador($data);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['expos']   = $this->ADExpo_model->getAllExpos();
        $return['id_expo'] = $id_expo;
        $this->load->view('app/expo/ganador/formulario', $return);
    }

    public function editar($id_ganador)
    {
        if ($_POST) {
            $data = [
                    "nombre"  => $this->input->post('nombre'),
                    "premio"  => $this->input->post('premio'),
                    "id_expo" => $this->input->post('id_expo'),
            ];
            $this->ADGanador_model->updateGanador($data, $id_ganador);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['ganador'] = $this->ADGanador_model->getGanador($id_ganador);
        $return['expos']   = $this->ADExpo_model->getAllExpos();
        $return['id_expo'] = $return['ganador']->id_expo;
        $this->load->view('app/expo/ganador/formulario', $return);
    }

    public function eliminar($id)
    {
        $this->ADGanador_model->deleteGanador($id);
        redirect_back();
    }
}