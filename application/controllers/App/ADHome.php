<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADHome extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Home/Home_model', 'Home_model');
        $this->load->model('Home/Home_seccion_model', 'Home_seccion_model');
    }

    public function index()
    {
        $data['titulo']   = 'Panel principal > Slider';
        $data['imagenes'] = $this->Home_model->get();
        $this->load->view('app/panel_principal/index', $data);
    }

    public function footer()
    {
        $data['titulo']  = 'Panel principal > Footer';
        $data['seccion'] = $this->Home_seccion_model->get();
        $this->load->view('app/panel_principal/footer', $data);
    }

    public function subir_imagen()
    {
        if ($_FILES) {
            $this->output->set_content_type('application/json');
            $file = $_FILES["uploadfile"];
            if ( ! is_array($file["name"]) && ! empty($file["tmp_name"])) {
                $datos     = file_get_contents($file["tmp_name"]);
                $data      = [
                        "mime" => $file["type"],
                        "base" => $datos,
                ];
                $resultado = $this->Home_model->insert($data);

                if ($resultado != false) {
                    header('Content-Type: application/json');
                    http_response_code(202);
                    echo json_encode(["success" => 0, "message" => "Success", "id" => $resultado]);
                    exit;
                }
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(["success" => 1, "message" => "Error intentelo de nuevo mas tarde"]);
        exit;
    }

    public function delete_imagen()
    {
        if ($_POST) {
            $resultado = $this->Home_model->delete($this->input->post('id'));

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(["success" => 0, "message" => "Success"]);
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(["success" => 1, "message" => "Error intentelo de nuevo mas tarde"]);
        exit;
    }

    public function update_seccion($id)
    {
        if ($_POST) {
            $datos     = [
                    "titulo" => $this->input->post('titulo'),
                    "url"    => $this->input->post('url'),
                    "icono"  => $this->input->post('icono'),
            ];
            $resultado = $this->Home_seccion_model->update($datos, $id);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(["success" => 0, "message" => "Success"]);
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(["success" => 1, "message" => "Error intentelo de nuevo mas tarde"]);
        exit;
    }

    public function insert_seccion()
    {
        if ($_POST) {
            $datos     = [
                    "titulo" => $this->input->post('titulo'),
                    "url"    => $this->input->post('url'),
                    "tipo"   => $this->input->post('tipo'),
                    "icono"  => $this->input->post('icono'),
            ];
            $resultado = $this->Home_seccion_model->insert($datos);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(["success" => 0, "id" => $resultado]);
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(["success" => 1, "message" => "Error intentelo de nuevo mas tarde"]);
        exit;
    }

    public function delete_seccion($id)
    {
        if ($_GET) {
            $resultado = $this->Home_seccion_model->delete($id);
            echo $resultado;
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(["success" => 0, "message" => "Success"]);
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(["success" => 1, "message" => "Error intentelo de nuevo mas tarde"]);
        exit;
    }

    private function resize_image($file, $w, $h, $crop = false)
    {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth  = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth  = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth  = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }

    private function parseBase64($strBase64)
    {
        $o = new stdClass();
        if (empty($strBase64)) {
            $o->mime   = null;
            $o->imagen = null;
        } else {
            $mime      = explode(",", $strBase64);
            $o->imagen = base64_decode($mime[1]);
            $mime      = explode(";", $mime[0]);
            $mime      = explode(":", $mime[0]);
            $o->mime   = $mime[1];
        }

        return $o;
    }

}
