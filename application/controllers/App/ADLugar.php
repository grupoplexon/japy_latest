<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADLugar extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
        $this->load->model('App/ADLugar_model', 'ADLugar_model');
    }

    public function index($id_expo = null)
    {
        $return['lugares'] = $this->ADLugar_model->getAll($id_expo);
        $return['titulo']  = "Lugares";
        $return['id_expo'] = $id_expo;
        $this->load->view('app/expo/lugar/lista', $return);
    }

    public function lugar($id_lugar = null)
    {

        $return['lugares']   = $this->ADLugar_model->getAllByLugar($id_lugar);
        $return['titulo']    = $this->ADLugar_model->getTitle($id_lugar);
        $return['id_expo']   = $this->ADLugar_model->getId_expo($id_lugar);
        $return['id_parent'] = $id_lugar;
        $lugar               = $this->ADLugar_model->getLugar($id_lugar);
        $lugar2              = $this->ADLugar_model->getLugar($lugar->parent);
        if (isset($lugar2) && count($lugar2) > 0) {

            $return['id_back'] = $lugar->parent;
            $return['back']    = 0;
        } else {
            $return['id_back'] = $lugar->id_expo;
            $return['back']    = 1;
        }
        $this->load->view('app/expo/lugar/lista2', $return);

    }

    public function alta($id_expo = null)
    {
        if ($_POST) {
            $data = [
                    "titulo_es" => $this->input->post('titulo_es'),
                    "titulo_en" => $this->input->post('titulo_en'),
                    "imagen"    => $this->input->post('imagen'),
                    "id_expo"   => $this->input->post('id_expo'),
            ];
            $this->ADLugar_model->insertLugar($data);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['expos']   = $this->ADExpo_model->getAllExpos();
        $return['id_expo'] = $id_expo;
        $this->load->view('app/expo/lugar/formulario', $return);
    }

    public function altaLugar($id_lugar = null)
    {
        $expo = $this->ADLugar_model->getLugar($id_lugar);
        if (isset($expo) && count($expo) > 0) {
            $id_expo = $expo->id_expo;
        }
        if ($_POST) {
            $data = [
                    "titulo_es"      => $this->input->post('titulo_es'),
                    "titulo_en"      => $this->input->post('titulo_en'),
                    "descripcion_es" => $this->input->post('descripcion_es'),
                    "descripcion_en" => $this->input->post('descripcion_en'),
                    "imagen"         => $this->input->post('imagen'),
                    "parent"         => $id_lugar,
                    "id_expo"        => $id_expo,
            ];
            $this->ADLugar_model->insertLugar($data);
            redirect("App/ADLugar/lugar/".$id_lugar, 'refresh');
        }
        $return['titulo'] = $this->ADLugar_model->getTitle($id_lugar);
        $this->load->view('app/expo/lugar/formulario2', $return);
    }

    public function editar($id_lugar)
    {
        if ($_POST) {
            $data = [
                    "titulo_es" => $this->input->post('titulo_es'),
                    "titulo_en" => $this->input->post('titulo_en'),
                    "imagen"    => $this->input->post('imagen'),
                    "id_expo"   => $this->input->post('id_expo'),
            ];
            $this->ADLugar_model->updateLugar($data, $id_lugar);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['lugar']   = $this->ADLugar_model->getLugar($id_lugar);
        $return['expos']   = $this->ADExpo_model->getAllExpos();
        $return['id_expo'] = $return['lugar']->id_expo;
        $this->load->view('app/expo/lugar/formulario', $return);
    }

    public function editarLugar($id_lugar)
    {
        $return['lugar'] = $this->ADLugar_model->getLugar($id_lugar);
        if ($_POST) {
            $data = [
                    "titulo_es"      => $this->input->post('titulo_es'),
                    "titulo_en"      => $this->input->post('titulo_en'),
                    "descripcion_es" => $this->input->post('descripcion_es'),
                    "descripcion_en" => $this->input->post('descripcion_en'),
                    "imagen"         => $this->input->post('imagen'),
            ];
            $this->ADLugar_model->updateLugar($data, $id_lugar);
            redirect("App/ADLugar/lugar/".$return['lugar']->parent, 'refresh');
        }

        //$return['expos'] = $this->ADExpo_model->getAllExpos();
        //$return['id_expo'] = $return['lugar']->id_expo;
        $return['titulo'] = $this->ADLugar_model->getTitle($return['lugar']->parent);

        //print_r($return['lugar']->parent);
        $this->load->view('app/expo/lugar/formulario2', $return);
    }

    public function eliminar($id)
    {
        $this->ADLugar_model->deleteLugar($id);
        redirect_back();
    }
}