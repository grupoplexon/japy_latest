<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADNosotros extends MY_Controller
{

    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADNosotros_model', 'ADNosotros_model');
    }

    public function index()
    {
        if ($_POST) {
            $data = [
                    "nombre"      => $this->input->post('nombre'),
                    "direccion"   => $this->input->post('ubicacion'),
                    'descripcion' => $this->input->post('descripcion'),
                    "telefono"    => $this->input->post('telefono'),
                    "latitud"     => $this->input->post('latitud'),
                    "longitud"    => $this->input->post('longitud'),
                    "web"         => $this->input->post('web'),
                    "imagen"      => $this->input->post('imagen'),
            ];
            $this->ADNosotros_model->updateNosotros($data);
            redirect("App/ADNosotros", 'refresh');
        }
        $data['titulo']   = 'Nosotros';
        $data['nosotros'] = $this->ADNosotros_model->getNosotros();
        $this->load->view('app/nosotros/index', $data);
    }

}