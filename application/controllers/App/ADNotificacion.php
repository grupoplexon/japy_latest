<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADNotificacion extends MY_Controller
{

    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADNotificacion_model', 'ADNotificacion_model');
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
    }

    public function index()
    {
        $data['titulo']         = 'Notificaciones';
        $data['notificaciones'] = $this->ADNotificacion_model->getAll();
        $this->load->view('app/notificacion/index', $data);
    }

    public function alta()
    {
        if ($_POST) {
            $data            = [
                    "titulo"         => $this->input->post("titulo"),
                    "descripcion"    => $this->input->post("descripcion"),
                    "fecha_creacion" => date("Y-m-d H:i:s"),
                    "id_expo"        => $this->input->post("id_expo"),
            ];
            $data['id_expo'] = empty($data['id_expo']) ? null : $data['id_expo'];
            $this->ADNotificacion_model->insertNotificacion($data);
            /*
             * 
             * FALTA AGREGAR EL ENVIO DE NOTIFICACIONES A LOS USUARIOS.
             */
            redirect('App/ADNotificacion', 'refresh');
        }
        $result['expos'] = $this->ADExpo_model->getAllExpos();
        $this->load->view('app/notificacion/formulario', $result);
    }

}