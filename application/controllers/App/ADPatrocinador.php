<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADPatrocinador extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
        $this->load->model('App/ADPatrocinador_model', 'ADPatrocinador_model');
    }

    public function index($id_expo = null)
    {
        $return['patrocinadores'] = $this->ADPatrocinador_model->getAllPatrocinadores($id_expo);
        $return['titulo']         = "Patrocinador";
        $return['id_expo']        = $id_expo;
        $this->load->view('app/expo/patrocinador/lista', $return);
    }

    public function alta($id_expo = null)
    {
        if ($_POST) {
            $data = [
                    "nombre"  => $this->input->post('nombre'),
                    "web"     => $this->input->post('web'),
                    'imagen'  => $this->input->post('imagen'),
                    "id_expo" => $this->input->post('id_expo'),
            ];
            $this->ADPatrocinador_model->insertPatrocinador($data);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['expos']   = $this->ADExpo_model->getAllExpos();
        $return['id_expo'] = $id_expo;
        $this->load->view('app/expo/patrocinador/formulario', $return);
    }

    public function editar($id_patrocinador)
    {
        if ($_POST) {
            $data = [
                    "nombre"  => $this->input->post('nombre'),
                    "web"     => $this->input->post('web'),
                    'imagen'  => $this->input->post('imagen'),
                    "id_expo" => $this->input->post('id_expo'),
            ];
            $this->ADPatrocinador_model->updatePatrocinador($data, $id_patrocinador);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['patrocinador'] = $this->ADPatrocinador_model->getPatrocinador($id_patrocinador);
        $return['expos']        = $this->ADExpo_model->getAllExpos();
        $return['id_expo']      = $return['patrocinador']->id_expo;
        $this->load->view('app/expo/patrocinador/formulario', $return);
    }

    public function eliminar($id)
    {
        $this->ADPatrocinador_model->deletePatrocinador($id);
        redirect_back();
    }
}