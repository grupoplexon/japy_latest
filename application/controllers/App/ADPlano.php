<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADPlano extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
    }

    public function index($id_expo)
    {
        $result['expo']   = $this->ADExpo_model->getExpo($id_expo);
        $result['titulo'] = $result['expo']['nombre'];
        $this->load->view('app/expo/plano/formulario', $result);
    }

    public function subirPlano()
    {
        if ($_POST) {
            $data = [
                    'plano' => $this->input->post('plano'),
            ];
            $this->ADExpo_model->updateExpo($data, $this->input->post('id_expo'));
            redirect('App/ADExpo/expo/'.$this->input->post('id_expo'), 'refresh');
        }
    }

}
