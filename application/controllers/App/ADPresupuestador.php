<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ADPresupuestador extends MY_Controller
{

    public $input;

    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->input = new MY_Input();
        $this->load->model("DefaultPorcentaje_model", 'default_porcentaje');
        $this->load->helper("slug");
    }

    public function index()
    {
        $result['defualts']   = $this->default_porcentaje->getDefaults();
        $result['titulo']     = 'Presupuestador Maestro';
        $result['categories'] = Category::with('subcategories')
                ->main()
                ->withCount('subcategories')
                ->orderBy('subcategories_count', 'desc')
                ->get();
        $this->load->view('app/presupuesto/index', $result);
    }

    public function add()
    {
        $this->session->set_userdata('mensaje', 'Ocurrio un error al agregar el campo');
        $this->session->set_userdata('tipo_mensaje', 'danger');
        if ($_POST) {
            $nombre     = $this->input->post('nombre', true, 'String', ['required' => true, 'minSize' => 3]);
            $porcentaje = $this->input->post('porcentaje', true, 'Float',
                    ['required' => true, 'min' => 0, 'max' => 100]);
            $categoria  = $this->input->post('categoria', true, 'Integer', ['required' => true, 'min' => 1]);

            if ($this->input->isValid()) {

                $totalPercentage = Category::sum('percentage');

                if ($totalPercentage + $porcentaje > 100) {
                    $this->session->set_userdata('mensaje', 'El total sobrepasa el 100%');
                    $this->session->set_userdata('tipo_mensaje', 'danger');
                } else {
                    $subcategory = Category::create([
                            'name'       => $nombre,
                            'parent_id'  => $categoria,
                            'percentage' => $porcentaje,
                    ]);

//                    $data = array(
//                        'id_parent' => $categoria,
//                        'nombre' => $nombre,
//                        'porcentaje' => $porcentaje
//                    );
//
//                    $b = $this->default_porcentaje->insert($data);
//
                    if ($subcategory) {
                        $this->session->set_userdata('mensaje', 'Campo agregado correctamente');
                        $this->session->set_userdata('tipo_mensaje', 'success');
                    }
                }
            } else {
                $this->session->set_userdata('mensaje', $this->input->getErrors());
            }
        }
        redirect('App/ADPresupuestador/', 'refresh');
    }

    public function delete()
    {
        if ($_POST) {
            $id = $this->input->post('id', true, 'Integer', ['required' => true, 'min' => 1]);

            $category = Category::find($id);

            if ($category) {
                $category->providers()->detach();
                $category->delete();
            }

            return $this->output->set_status_header(200)
                    ->set_output(json_encode([
                                    'success' => true,
                                    'data'    => 'ok',
                            ])
                    );
        }

        return $this->output->set_status_header(404)
                ->set_output(json_encode([
                                'success' => false,
                                'data'    => "error",
                        ])
                );
    }

    public function update()
    {
        $response = [
                "code"    => 404,
                "data"    => [],
                "message" => "Error",
        ];

        $id          = $this->input->post("id", true, "Integer", ["required" => true, "min" => 0]);
        $value       = $this->input->post("value", true, "Float", ["required" => true, "min" => 0]);
        $name        = $this->input->post("name");
        $description = $this->input->post("description");

        $category = Category::find($id);

        if ($_POST && ! is_null($category)) {
            $totalPercentage = Category::where("id", "<>", $category->id)->sum("percentage");

            if ($totalPercentage + $value > 100) {
                $response["code"]    = 412;
                $response["message"] = "El total sobrepasa el 100%";
            } else {
                ! is_null($value) ? $request["percentage"] = $value : "";
                isset($description) ? $request["description"] = $description : "";
                isset($name) ? $request["name"] = $name : "";
                isset($name) ? $request["slug"] = create_category_slug($category->id, $request["name"]) : "";
                
                $category->update($request);

                $b = $this->default_porcentaje->update($id, ["porcentaje" => $value]);

                if ($b) {
                    $response["code"]    = 200;
                    $response["message"] = "Categoria editada exitosamente";
                }
            }
        }

        return $this->output->set_status_header($response["code"])
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
    }

}
