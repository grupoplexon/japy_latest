<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADPrograma extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADExpo_model', 'ADExpo_model');
        $this->load->model('App/ADPrograma_model', 'ADPrograma_model');
    }

    public function index($id_expo = null)
    {
        $return['programas'] = $this->ADPrograma_model->getAllExpo($id_expo);
        $return['titulo']    = "Programa";
        $return['id_expo']   = $id_expo;
        $this->load->view('app/expo/programa/lista', $return);
    }

    public function alta($id_expo = null)
    {
        if ($_POST) {
            $data = [
                    "titulo"         => $this->input->post('titulo'),
                    "descripcion"    => $this->input->post('descripcion'),
                    "ponente"        => $this->input->post('ponente'),
                    "lugar"          => $this->input->post('lugar'),
                    "fecha_i"        => $this->input->post('fecha_i'),
                    "fecha_t"        => $this->input->post('fecha_t'),
                    "id_expo"        => $this->input->post('id_expo'),
                    "fecha_creacion" => date("Y-m-d H:i:s"),
            ];
            $this->ADPrograma_model->insertPrograma($data);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['expos']   = $this->ADExpo_model->getAllExpos();
        $return['id_expo'] = $id_expo;
        $this->load->view('app/expo/programa/formulario', $return);
    }

    public function editar($id_programa)
    {
        if ($_POST) {
            $data = [
                    "titulo"      => $this->input->post('titulo'),
                    "descripcion" => $this->input->post('descripcion'),
                    "ponente"     => $this->input->post('ponente'),
                    "lugar"       => $this->input->post('lugar'),
                    "fecha_i"     => $this->input->post('fecha_i'),
                    "fecha_t"     => $this->input->post('fecha_t'),
                    "id_expo"     => $this->input->post('id_expo'),
            ];
            $this->ADPrograma_model->updatePrograma($data, $id_programa);
            redirect("App/ADExpo/expo/".$data['id_expo'], 'refresh');
        }
        $return['programa'] = $this->ADPrograma_model->getPrograma($id_programa);
        $return['expos']    = $this->ADExpo_model->getAllExpos();
        $return['id_expo']  = $return['programa']->id_expo;
        $this->load->view('app/expo/programa/formulario', $return);
    }

    public function eliminar($id)
    {
        $this->ADPrograma_model->deletePrograma($id);
        redirect_back();
    }
}