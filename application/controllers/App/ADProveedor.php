<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ADProveedor extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model("proveedor_model", "proveedor");
        $this->load->model("Usuario_model", "usuario");
        $this->load->helper("formats");
        $this->load->library("My_PHPMailer");
        $this->mailer = $this->my_phpmailer;
    }

    public function index()
    {
        $data["titulo"]      = "Proveedores";
        $data["proveedores"] = $this->proveedor->getAllAdmin();
        $this->load->view("app/proveedor/index", $data);
    }

    public function cambiar_cuenta()
    {
        $this->output->set_content_type("application/json");
        if ($_POST) {
            $p      = $this->input->post("proveedor", true, "Integer", ["required" => true, "min" => 1]);
            $cuenta = $this->input->post("tipo", true, "String", ["required" => true]);

            if ($this->proveedor->get($p)) {
                $proveedor = [
                        "tipo_cuenta" => $this->proveedor->RlabelTipoCuenta($cuenta),
                ];
                if ($this->proveedor->update($p, $proveedor)) {
                    return $this->output->set_status_header(202)
                            ->set_output(json_encode([
                                    "success" => true,
                                    "data"    => "ok",
                            ]));
                }
            }
        }

        return $this->output->set_status_header(404);
    }

    public function toggle_activar()
    {
        $this->output->set_content_type("application/json");
        if ($_POST) {
            $p   = $this->input->post("proveedor", true, "Integer", ["required" => true, "min" => 1]);
            $pro = $this->proveedor->get($p);
            if ($pro) {
                $proveedor = ["acceso" => $pro->acceso == 1 ? 0 : 1,];
                if ($this->proveedor->update($p, $proveedor)) {
                    $user = User::find($pro->id_usuario);
                    $user->update(["activo" => $pro->acceso == 1 ? 0 : 1]);

                    $this->mailer->to($user->correo, $user->nombre);

                    if ($pro->acceso == 1) {
                        $temp = "templates/proveedor/email_desactivacion";
                    } else {
                        $temp = "templates/proveedor/email_activacion";
                    }

                    $template = $this->load->view($temp, [
                            "correo" => $user->correo,
                            "nombre" => $user->nombre,
                    ], true);

                    $this->mailer->send($template);

                    return $this->output->set_status_header(202)
                            ->set_output(json_encode([
                                    "success" => true,
                                    "data"    => "ok",
                            ]));
                }
            }
        }

        return $this->output->set_status_header(404);
    }

}
