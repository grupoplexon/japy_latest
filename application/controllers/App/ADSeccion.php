<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ADSeccion extends MY_Controller
{

    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App/ADSeccion_model', 'ADSeccion_model');
    }

    public function editar($id)
    {
        if ($_POST) {
            $datos = [
                    "descripcion" => $this->input->post("descripcion"),
            ];
            $this->ADSeccion_model->updateSeccion($datos, $this->input->post("id_seccion"));
            redirect('App/ADExpo', 'refresh');
        }
        $result['seccion'] = $this->ADSeccion_model->getSeccion($id);
        $this->load->view('app/expo/seccion/formulario', $result);
    }

}
