<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Cliente_model", "cliente");
        $this->load->model("Vestido/Vestido_model", "tendencia");
    }

    function index()
    {
        $data['titulo']      = 'Administrador';
        $data["proveedores"] = $this->proveedor->count();
        $data["novios"]      = $this->cliente->count();
        $data["vestidos"]    = $this->tendencia->count();
        $this->load->view('app/index', $data);
    }

    public function est_clientes()
    {
        $visitas = $this->estvisitas($this->cliente->clientes12meses(), "clientes");

//        $visitas = $this->cliente->clientes12meses();
        return $this->output
                ->set_content_type('application/json')
                ->set_status_header(202)
                ->set_output(
                        json_encode(
                                [
                                        'success' => true,
                                        'data'    => [
                                                "clientes" => $visitas,
                                        ],
                                ]
                        )
                );
    }

    public function est_proveedores()
    {
        $visitas = $this->estvisitas($this->cliente->proveedores12meses(), "proveedores");

        return $this->output
                ->set_content_type('application/json')
                ->set_status_header(202)
                ->set_output(
                        json_encode(
                                [
                                        'success' => true,
                                        'data'    => [
                                                "proveedores" => $visitas,
                                        ],
                                ]
                        )
                );
    }

    private function estvisitas($visitas, $campo = "visitas")
    {
        $est = [];
        $now = new DateTime("now");
        $now->sub(DateInterval::createFromDateString('12 month'));
        for ($mes = 11; $mes >= 0; $mes--) {
            $now->add(DateInterval::createFromDateString('1 month'));
            $c = 0;
            foreach ($visitas as $key => $v) {
                if ($v->mes == $now->format("m-Y")) {
                    $c = (int)$v->{$campo};
                }
            }
            $est[] = [
                    "fecha"  => $now->format("m-Y"),
                    "$campo" => $c,
            ];
        }

        return $est;
    }

}
