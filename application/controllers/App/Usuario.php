<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends MY_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Usuario_model");
    }

    function index()
    {
        $datos['titulo'] = 'Usuario';
        $users           = User::where('rol', '<>', '2')
                ->where('rol', '<>', '3')
                ->active()
                ->get();

        $users->map(function ($user) {
            $user->rol = $this->getRol($user->rol);
        });

        //$datos["usuarios"] = $this->Usuario_model->getUsuarios_nonovio_noproveedor();
        $datos['usuarios'] = $users;
        $this->load->view('app/usuario/index', $datos);
    }

    function alta()
    {
        $datos['titulo'] = 'Usuario';
        if ($_POST) {
            $imagen = $this->parseBase64($this->input->post('imagen'));
            $data   = [
                    "usuario"    => $this->input->post('usuario'),
                    "nombre"     => $this->input->post('nombre'),
                    "apellido"   => $this->input->post('apellido'),
                    "mime"       => $imagen->mime,
                    "foto"       => $imagen->imagen,
                    "contrasena" => sha1(md5(sha1($this->input->post('contrasena')))),
                    "rol"        => $this->input->post('rol'),
            ];

            if ($this->Usuario_model->existeCorreo($data['usuario'])) {
                $datos['error'] = $data;
            } else {
                $this->Usuario_model->insert($data);
                redirect('App/Usuario');
            }
        }
        $this->load->view('app/usuario/edit', $datos);
    }

    function editar($id = null)
    {
        $datos['titulo'] = 'Usuario';
        if ($_POST) {
            $imagen = $this->parseBase64($this->input->post('imagen'));

            $data = [
                    "nombre"   => $this->input->post('nombre'),
                    "apellido" => $this->input->post('apellido'),
                    "mime"     => $imagen->mime,
                    "foto"     => $imagen->imagen,
            ];

            if ( ! empty($this->input->post('contrasena'))) {
                $data['contrasena'] = sha1(md5(sha1($this->input->post('contrasena'))));
            }

            if ( ! empty($this->input->post('rol'))) {
                $data['rol'] = $this->input->post('rol');
            }

            User::find($this->input->post('id_usuario'))->update($data);
            // $this->Usuario_model->update($this->input->post('id_usuario'), $data);
            redirect('App/Usuario');
        }
        //$datos["info"] = $this->Usuario_model->get($id);
        $datos['info'] = User::find($id);
        $this->load->view('app/usuario/edit', $datos);
    }

    function delete()
    {
        if ($_POST) {
            $id = $this->input->post('id');
            if ($this->session->userdata('id_usuario') != $id) {
                $this->Usuario_model->update($id, ["activo" => 0]);
                User::find($id)->update(['activo' => 0]);

                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(["success" => 1]);
                exit;
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(["success" => 1, "message" => "Error"]);
        exit;
    }

    private function getRol($rol)
    {
        switch ($rol) {
            case 1:
                return "Administrador";
            case 2:
                return "Novio";
            case 3:
                return "Proveedor";
            case 4:
                return "Redactor";
            case 5:
                return "Admin App";
            case 6:
                return "Moderador";
            case 7:
                return "Galeria";
            case 8:
                return "Blog";
        }
    }

    private function parseBase64($strBase64)
    {
        $o = new stdClass();
        if (empty($strBase64)) {
            $o->mime   = null;
            $o->imagen = null;
        } else {
            $mime      = explode(",", $strBase64);
            $o->imagen = base64_decode(str_replace(' ', '+', $mime[1]));
            $mime      = explode(";", $mime[0]);
            $mime      = explode(":", $mime[0]);
            $o->mime   = $mime[1];
        }

        return $o;
    }
}