<?php

use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class Brideweekend extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Cliente_model");
        $this->load->model("Boda_model");
        $this->load->model("Invitados/Invitado_model", "Invitado_model");
        $this->load->library("checker");
        $this->load->library("My_PHPMailer");
        $this->load->helper("slug");
        $this->mailer = $this->my_phpmailer;
        date_default_timezone_set('America/Mexico_City');
    }

    public function index()
    {

        $data["promotions"]= DB::table('promotions_brideweekend')->where('status',1)->get();
        $data["ciudades"] = DB::table('brideweekend')->get();
 
        $this->load->view("brideweekend/index", $data);

        // $data["email"] = "e.e@.com";
        // $data["password"] = uniqid();
        // $this->load->view("templates/email_brideweekend",$data);
    }

    public function ciudades(){
        $data["ciudades"] = DB::table('brideweekend')->orderBy('initial_date')->where('initial_date',  '>=', DB::raw('curdate()'))->get();

        foreach($data["ciudades"] as $ciudad) {
            $mes = new Datetime($ciudad->initial_date);
            $ciudad->mes_initial = ucfirst(strftime("%B", $mes->getTimestamp()));
            $ciudad->dia_initial = strftime("%d", $mes->getTimestamp());
            if($ciudad->final_date != null){
                $mes = new Datetime($ciudad->final_date);
                $ciudad->mes_final = ucfirst(strftime("%B", $mes->getTimestamp()));
                $ciudad->dia_final = strftime("%d", $mes->getTimestamp());
            }
        }
 
        // dd($data["ciudades"]);
        $this->load->view("brideweekend/ciudades", $data);

    }


    public function promocion(){

        $promo = $this->input->get('name');

        $data["promotion"] = DB::table('promotions_brideweekend')->where('id_promotion',$promo)->first();

        $this->load->view("brideweekend/promo", $data);
    }

    public function prueba(){
        $this->load->view("brideweekend/prueba");
    }
    public function brideweekend()
    {
        $this->load->view("brideweekend/brideweekend");
    }
    public function sede()
    {
        $this->load->view("brideweekend/sede");
    }
    public function comprador()
    {
        $this->load->view("brideweekend/comprador");
    }
    public function contacto()
    {
        $this->load->view("brideweekend/contacto");
    }
    public function blog()
    {
        $this->load->view("brideweekend/blog");
    }
    public function galeria()
    {
        $this->load->view("brideweekend/galeria");
    }
    public function boletos()
    {
        $this->load->view("brideweekend/boletos");
    }
    public function registro()
    {
        if ($_POST) {

            BrideWeekendRegistro::create([
                "nombre" => $this->input->post("nombre"),
                "correo" => $this->input->post("correo", true, "Email", ["required" => true]),
                "telefono" => $this->input->post("telefono"),
                "fecha_boda" => $this->input->post("fecha"),
            ]);
        }
        redirect("brideweekend/index");
    }
    public function register() 
    {
        if($_POST) {
            $contrasena = uniqid();
            // $nombre = $this->input->post('nombre');
            // $correo = $this->input->post("correo");
            // $registeredFromState = $this->input->post("come_from");
            // $genero = $this->input->post("genero");
            // $telefono = $this->input->post("telefono");
            // $fecha = $this->input->post("fecha");
            // dd($nombre.''.$correo.''.$registeredFromState.''.$genero.''.$telefono.''.$fecha);
            if(User::where("correo", $this->input->post("correo"))->exists()) {
                return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['validate' => 'user_exist']));
            } else {

                $states              = ["cdmx", "guanajuato", "jalisco", "nuevo_leon", "puebla", "queretaro", "sinaloa", "veracruz"];
                $registeredFromState = $this->input->post("come_from");

                // INSERT BODA
                $data = [
                    "fecha_boda"     => $this->input->post("fecha"),
                    "fecha_creacion" => date("Y-m-d H:i:s"),
                    "presupuesto"    => 0,
                    "no_invitado"    => 0,
                ];

                $id_boda = $this->Boda_model->insertBoda($data);

                // INSERT USUARIO
                $genero = empty($this->input->post("genero")) ? 2 : $this->input->post("genero");

                if ($genero == 1) {
                    $data["nombre"]   = $this->input->post("nombre");
                    $data["apellido"] = "XXX";
                } elseif ($genero == 2) {
                    $data["nombre"]   = $this->input->post("nombre");
                    $data["apellido"] = "XXX";
                }

                $id_usuario = User::create([
                    "usuario"         => $this->input->post("correo"),
                    "correo"          => $this->input->post("correo"),
                    "contrasena"      => sha1(md5(sha1($contrasena))),
                    "rol"             => 2,
                    "activo"          => 1,
                    "nombre"          => $data["nombre"],
                    "apellido"        => "XXX",
                    "registered_from" => $registeredFromState ? $registeredFromState : null,
                ])->id_usuario;

                $id_permisos = Profile::create([
                    "enviar_mensaje"        => 1,
                    "participacion_debates" => 1,
                    "valorar_publicaciones" => 1,
                    "anadir_amigo"          => 1,
                    "aceptar_solicitud"     => 1,
                    "mension_posts"         => 1,
                    "email_diario"          => 1,
                    "email_semanal"         => 1,
                    "invitaciones"          => 1,
                    "concursos"             => 1,
                    "visibilidad_todos"     => 1,
                    "email_debate"          => 1,
                ])->id_permisos;

                // INSERT CLIENTE
                $data = [
                    "genero"      => $genero,
                    "pais"        => null,
                    "estado"      => null,
                    "poblacion"   => null,
                    "id_permisos" => $id_permisos,
                    "id_boda"     => $id_boda,
                    "id_usuario"  => $id_usuario,
                    "telefono"  => $telefono = $this->input->post("telefono"),
                ];

                $id_cliente = $this->Cliente_model->insertCliente($data);

                $id_mesa = Table::create([
                    'tipo'        => 3,
                    'nombre'      => 'PRINCIPAL',
                    'sillas'      => 4,
                    'x'           => 425,
                    'y'           => 75,
                    'orientacion' => 90,
                    'id_boda'     => $id_boda,
                ])->id_mesa;

                //$id_mesa = $this->Mesa_model->getMesaPrincipal($id_boda)->id_mesa;
                $id_grupo = Group::create([
                    'grupo'   => 'Novios',
                    'id_boda' => $id_boda,
                ])->id_grupo;

                $id_menu = Menu::create([
                    'id_boda'     => $id_boda,
                    'nombre'      => 'Adultos',
                    'descripcion' => 'Menu para adultos',
                ])->id_menu;

                //SETUP DEFAULT
                Group::insert([
                    ["grupo" => "Familia", "id_boda" => $id_boda],
                    ["grupo" => "Amigos", "id_boda" => $id_boda],
                ]);

                Menu::create([
                    "id_boda"     => $id_boda,
                    "nombre"      => "Niños",
                    "descripcion" => "Menu para niños",
                ]);

                $baseChores = Chore::whereNull("id_boda")->get();

                foreach ($baseChores as $chore) {
                    $newChore          = $chore->replicate();
                    $newChore->id_boda = $id_boda;
                    $newChore->save();
                }
                if ($genero == 1) {
                    $data = [
                        "nombre"     => $this->input->post("nombre"),
                        "apellido"   => "XXX",
                        "sexo"       => 1,
                        "edad"       => 1,
                        "correo"     => $this->input->post("correo"),
                        "confirmado" => 2,
                        "id_grupo"   => $id_grupo,
                        "id_menu"    => $id_menu,
                        "id_mesa"    => $id_mesa,
                        "silla"      => 2,
                        "id_boda"    => $id_boda,
                    ];

                    $this->Invitado_model->insertInvitado($data);

                    $data = [
                        "nombre"     => 'XXX',
                        "apellido"   => 'XXX',
                        "sexo"       => 2,
                        "edad"       => 1,
                        "confirmado" => 1,
                        "id_grupo"   => $id_grupo,
                        "id_menu"    => $id_menu,
                        "id_mesa"    => $id_mesa,
                        "silla"      => 3,
                        "id_boda"    => $id_boda,
                    ];

                    $this->Invitado_model->insertInvitado($data);

                } elseif ($genero == 2) {
                    $data = [
                        "nombre"     => 'XXX',
                        "apellido"   => 'XXX',
                        "sexo"       => 1,
                        "edad"       => 1,
                        "correo"     => $this->input->post("correo"),
                        "confirmado" => 2,
                        "id_grupo"   => $id_grupo,
                        "id_menu"    => $id_menu,
                        "id_mesa"    => $id_mesa,
                        "silla"      => 2,
                        "id_boda"    => $id_boda,
                    ];

                    $this->Invitado_model->insertInvitado($data);

                    $data = [
                        "nombre"     => $this->input->post("nombre"),
                        "apellido"   => "XXX",
                        "sexo"       => 2,
                        "edad"       => 1,
                        "confirmado" => 1,
                        "id_grupo"   => $id_grupo,
                        "id_menu"    => $id_menu,
                        "id_mesa"    => $id_mesa,
                        "silla"      => 3,
                        "id_boda"    => $id_boda,
                    ];

                    $this->Invitado_model->insertInvitado($data);
                }

                $data = [
                    "usuario"    => $id_usuario,
                    //"nombre" => $this->input->post('nombre') . ' ' . $this->input->post('apellido'),
                    "rol"        => 2,
                    "confirmado" => 1,
                    "encryption" => generarCadena(),
                    "id_boda"    => $id_boda,
                    "id_cliente" => $id_cliente,
                    "id_usuario" => $id_usuario,
                    "genero"     => (($this->input->post("genero") == 1) ? "Novio" : "Novia"),
                ];

                $data["nombre"] = $this->input->post("nombre");
                // $data["passsword"] = $contrasena;
                // $data["email"] = $this->input->post("correo");

                $this->session->set_userdata($data);

                $this->mailer->to($this->input->post("correo"), $data["nombre"]);
                $this->mailer->setSubject("Bienvenido a Bride Advisor!");

                $template = $this->load->view("templates/email_brideweekend", [
                    "email" => $this->input->post("correo"),
                    "password" => $contrasena,
                ], true);

                $this->mailer->setSubject("Bienvenido a Bride Advisor");
                $this->mailer->send($template);
                ///////////////////// MAIL DE REGISTRO EN BRIDE WEEKEND //////////////////////////
                $this->mailer->to($this->input->post("correo"), $data["nombre"]);
                $this->mailer->setSubject("Gracias por registrarte en Bride Weekend");

                // $template = $this->load->view("templates/email_brideweekendEvent", [
                //     "nombre" => $this->input->post("nombre"),
                // ], true);
                
                $registro = is_null($this->input->post('registro')) ? null : $this->input->post('registro');
                
                if($registro =="BW"){
                    $template = $this->load->view("templates/email_brideweekendEvent", [
                        "nombre" => $this->input->post("nombre"),
                        "estado" => $registeredFromState,
                    ], true);

                }else{
                    $template = $this->load->view("templates/email_brideweekendEvent", [
                        "nombre" => $this->input->post("nombre"),
                        "estado" => $registeredFromState,
                    ], true);
                }

                $this->mailer->setSubject("Gracias por registrarte en Bride Weekend");
                $this->mailer->send($template);

                return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['validate' => 'success']));
            }
        }
    }
    public function teams() 
    {
        if($_POST) {
            $team = DB::table('teamBride')->insert([
                'nameTeam' => $this->input->post('nameTeam'),
                'nameBride' => $this->input->post('nameNovia'),
                'phone' => $this->input->post('phoneTeam'),
                'email' => $this->input->post('mailTeam'),
                'date' => $this->input->post('dateTeam'),
                'member1' => $this->input->post('integrante1'),
                'member2' => $this->input->post('integrante2'),
                'member3' => $this->input->post('integrante3'),
                'member4' => $this->input->post('integrante4'),
                'member5' => $this->input->post('integrante5'),
            ]);
            
            $this->mailer->to($this->input->post('mailTeam'), 'Team Bride');
            $this->mailer->setSubject("Gracias por registrar tu equipo");
            
            $template = $this->load->view("templates/teamBride", [
                "nombre" => $this->input->post('nameNovia')
            ], true);

            //$this->mailer->setSubject("Gracias por registrarte tu equipo");
            $this->mailer->send($template);

            if($team) {
                return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['status' => 'saved']));
            } else {
                return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['status' => 'error']));
            }
        }
    }
    public function registerActivity() 
    {
        if($_POST) {
            $correo = $this->input->post("correo");
            $telefono = $this->input->post("telefono");
            $novia = $this->input->post("nomNovia");
            $novio = $this->input->post("nomNovio");
            $ciudad = $this->input->post("ciudad");
            $fechaB = $this->input->post("fecha");
            $actividad = $this->input->post("actividad");
            $dia = $this->input->post("dia");

            // $count = DB::table('registro_actividades')->where('actividad',$actividad)->where('dia', $dia)->where('ciudad_evento', 'Culiacan')->count();
            // $limite=30;
            // if($actividad=='mixologia') {
            //     $limite=10;
            // }
            // if($actividad=='sushi' && $dia=='sabado') {
            //     $limite=10;
            // }
            // if(($actividad=='cata_vino' && $dia=='sabado') || ($actividad=='cata_vino' && $dia=='domigo'))  {
            //     $limite=10;
            // }
            // if($count<$limite) {
                $registro = DB::table('registro_actividades')->where('correo', $correo)->
                where('telefono', $telefono)->where('actividad', $actividad)->
                where('dia', $dia)->count();
                if($registro==0) {

                    $fecha= date('Y-m-d');

                    $res = DB::table('registro_actividades')->insert(
                        ['novia' => $novia, 'novio' => $novio, 'fecha' => $fechaB,
                        'correo' => $correo, 'ciudad' => $ciudad, 'telefono' => $telefono,
                        'actividad' => $actividad, 'dia' => $dia, 
                        'fecha_registro' => $fecha]
                    );

                    $this->mailer->to($correo, $novia);
                    $this->mailer->setSubject("Gracias por registrarte en la actividad");

                    $template = $this->load->view("templates/email_brideweekend_activity", [
                        "email" => $novia,
                    ], true);

                    $this->mailer->setSubject("Gracias por registrarte en la actividad");
                    $this->mailer->send($template);

                    return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['validate' => true]));
                } else {
                    return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['validate' => false]));
                }
            // } else {
            //     return $this->output->set_content_type("application/json")
            //             ->set_status_header(200)
            //             ->set_output(json_encode(['validate' => 'llena']));
            // }
        }
    }
    public function email_expositores()
    {
        if($_POST) {
            $email = $this->input->post("email");
            $name = $this->input->post("name");
            $msg = $this->input->post("msg");
            $telefono = $this->input->post("telefono");
            
            $recaptcha = $_POST["g-recaptcha-response"];
            
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = array(
                'secret' => '6Lexi6kUAAAAAM_c6O5f2RK1DnnuYfL7IIgH1s9y',
                'response' => $recaptcha
            );
        
            $options = array(
                'http' => array (
                    'method' => 'POST',
                    'content' => http_build_query($data)
                )
            );
            
            $context  = stream_context_create($options);
            $verify = file_get_contents($url, false, $context);
            $captcha_success = json_decode($verify);
            
            if ($captcha_success->success) {
                echo 'Se envía el formulario';
                $this->mailer->to("contacto@brideweekend.com", "Contacto");
                $this->mailer->setSubject("Un expositor quiere contactarte");
    
                $template = $this->load->view("templates/email_expositores", [
                    "email" => $this->input->post("email"),
                    "name" => $this->input->post("name"),
                    "msg" => $this->input->post("msg"),
                    "telefono" => $this->input->post("telefono"),
                ], true);
    
                $this->mailer->send($template);
                
                $this->mailer->to("contacto@brideadvisor.mx", "Contacto");
                $this->mailer->setSubject("Un expositor quiere contactarte");
    
                $template = $this->load->view("templates/email_expositores", [
                    "email" => $this->input->post("email"),
                    "name" => $this->input->post("name"),
                    "msg" => $this->input->post("msg"),
                    "telefono" => $this->input->post("telefono"),
                ], true);
    
                $this->mailer->send($template);
                
                redirect("brideweekend");
            } else {
                echo 'No se envía el formulario';
                // header("location:".base_url()."brideweekend?mensaje=errorCaptcha");
                redirect("brideweekend");
            }
        }
    } 

    public function registerUSA() {
        $this->mailer->to("mmota@corporativonupcial.com", "Contacto");
        $this->mailer->setSubject("Contacto BrideWeekend");

        $template = $this->load->view("templates/email_contact", [
            "email" => $this->input->post("email"),
            "name" => $this->input->post("name"),
            "msg" => $this->input->post("msg"),
            "telefono" => $this->input->post("telefono"),
        ], true);

        $this->mailer->send($template);
        return $this->output->set_content_type("application/json")
        ->set_status_header(200)
        ->set_output(json_encode(['status' => 'success']));
    }

    public function ciudad($city) {
        $city = str_replace("%20", " ", $city);
        $data["city"] = DB::table('brideweekend')->where('city', 'LIKE', '%'.$city.'%')->first();
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); 
        $fechaIni = Carbon::parse($data["city"]->initial_date);
        $fechaFin = Carbon::parse($data["city"]->final_date);
        $mes = $meses[($fechaIni->format('n')) - 1];
        $mestwo = $meses[($fechaFin->format('n')) - 1];
        if($mes==$mestwo) {
            $data['date'] = $mes.' '.$fechaIni->format('d').' y '. $fechaFin->format('d') . ' | ' . $fechaIni->format('Y');
        } else {
            $data['date'] = $mes.' '.$fechaIni->format('d').' y '. $mestwo .' '. $fechaFin->format('d') . ' ' . $fechaIni->format('Y');
        }
        $entrySat = date("g:i a",strtotime(substr($data["city"]->entry_expo_sat,0,5)));
        $exitSat = date("g:i a",strtotime(substr($data["city"]->entry_expo_sat,-5,5)));
        $horarioSat = $entrySat.' - '.$exitSat;
        $entrySun = date("g:i a",strtotime(substr($data["city"]->entry_expo_sun,0,5)));
        $exitSun = date("g:i a",strtotime(substr($data["city"]->entry_expo_sun,-5,5)));
        $horarioSun = $entrySun.' - '.$exitSun;
        if($horarioSat==$horarioSun) {
            $data["schedule"] = 'Sábado y Domingo '.$horarioSat;
        } else {
            $data["schedule"] = 'Sábado '.$entrySat.' - '.$exitSat.' Domingo '.$entrySun.' - '.$exitSun;
        }
        $this->load->view('brideweekend/ciudad', $data);
    }

    public function expositor() {
        $this->load->view('brideweekend/expositor');
    }
    
    public function exposiciones() 
    {
        $this->load->view("brideweekend/exposiciones");
    }
    public function concepto() 
    {
        $cities["cities"] = DB::table('brideweekend')->where('initial_date',  '>=', DB::raw('curdate()'))->orderBy('initial_date')->get();
        foreach ($cities["cities"] as $key => $value) {
            $mesStart = new Datetime($value->initial_date);
            $mesStart = strftime("%B", $mesStart->getTimestamp());
            $mesEnd = new Datetime($value->final_date);
            $mesEnd = strftime("%B", $mesEnd->getTimestamp());
            $dayStart = new Datetime($value->initial_date);
            $dayEnd = new Datetime($value->final_date);
            if($mesStart == $mesEnd) {
                $date = ucfirst($mesStart).' '.strftime("%d", $dayStart->getTimestamp()).' y '.strftime("%d", $dayEnd->getTimestamp());
            } else {
                $date = ucfirst($mesStart).' '.strftime("%d", $dayStart->getTimestamp()).' y '.ucfirst($mesEnd).' '.strftime("%d", $dayEnd->getTimestamp());
            }
            $value->dateFormat = $date;
        }
        $this->load->view("brideweekend/concepto", $cities);
    }

    public function expo_gdl() 
    {
        $this->load->view("brideweekend/expo_gdl");
    }
    public function expo_cdmx() 
    {
        $this->load->view("brideweekend/expo_cdmx");
    }
    public function expo_puebla() 
    {
        $this->load->view("brideweekend/expo_puebla");
    }
    public function expo_qro() 
    {
        $this->load->view("brideweekend/expo_qro");
    }
    public function expo_leon() 
    {
        $this->load->view("brideweekend/expo_leon");
    }
    public function new_york() {
        $this->load->view("brideweekend/new_york");
    }
    public function chicago() {
        $this->load->view("brideweekend/chicago");
    }
    public function phoenix() {
        $this->load->view("brideweekend/phoenix");
    }
    public function san_diego() {
        $this->load->view("brideweekend/sandiego");
    }
    public function houston() {
        $this->load->view("brideweekend/houston");
    }
    public function atlanta() {
        $this->load->view("brideweekend/atlanta");
    }
    public function los_angeles() {
        $this->load->view("brideweekend/los_angeles");
    }
    public function san_antonio() {
        $this->load->view("brideweekend/sanantonio");
    }
    public function monterrey() {
        $this->load->view("brideweekend/sanantonio");
    }
}