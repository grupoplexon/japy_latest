<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Cuenta extends CI_Controller
{

    public $input;

    public function __construct()
    {
        parent::__construct();
        $this->input = new MY_Input();
        $this->load->library("checker");
        $this->load->library("task");
        $this->load->library("facebook");
        $this->load->model("Login_model");
        $this->load->model("Usuario_model");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("MiPortal/MiPortal_model", "MiPortal_model");
    }
    
    public function index() {
        $this->load->view("registro/index");
    }

    public function novia()
    {
        $response = [
            "code"    => 500,
            "data"    => [],
            "message" => "",
        ];

        $wantsJson = $this->input->get_request_header('accept', true) === "application/json";

        if ($_POST) {
            $inicio = $this->input->post("inicio", true);
            $type   = $this->input->post("type");

            if ($inicio) {
                $data["email"] = $this->input->post("email", true, "Email");
            } else {
                $email    = $this->input->post("primaryEmail", true, "Email");
                $password = $this->input->post("primaryPassword", true, "String");
                $result   = $this->Login_model->validar_usuario($email, $password);

                if ( ! $result) {
                    $data["mensaje"]     = "Usuario/Contrase&ntilde;a invalida";
                    $response["code"]    = 401;
                    $response["message"] = $data["mensaje"];
                } else {
                    $session = [
                        "intento"    => null,
                        "id_usuario" => $result->id_usuario,
                        "usuario"    => $result->id_usuario,
                        "correo"     => $result->correo,
                        "nombre"     => $result->nombre." ".$result->apellido,
                        "rol"        => $result->rol,
                        "encryption" => generarCadena(),
                    ];

                    SessionLog::create(["user_id" => $session["id_usuario"], "type" => "LOGIN", "ip" => $_SERVER["REMOTE_ADDR"]]);

                    if ($result->rol == 2) {
                        $info_user             = $this->Usuario_model->getCliente($result->id_usuario);
                        $session["id_boda"]    = $info_user->id_boda;
                        $session["id_cliente"] = $info_user->id_cliente;
                        $session["genero"]     = (($info_user->genero == 1) ? "Novio" : "Novia");
                    } elseif ($result->rol == Checker::$PROVEEDOR) {
                        $provider = User::with("provider")->find($session["id_usuario"])->provider;;
                        $session["has_payment_method"] = $provider->hasPaymentMethod();
                        $session["nombre_proveedor"]   = $provider->nombre;
                        $session["id_proveedor"]       = $provider->id_proveedor;
                        $session["slug"]               = $provider->slug;
                        $session["plan"]               = $provider->tipo_cuenta;
                        $session["plan_months"]        = $provider->plan_months;
                        $session["has_trial"]          = $provider->trial_expiration_date
                            ? Carbon::createFromFormat("Y-m-d H:i:s", $provider->trial_expiration_date)->isFuture()
                            : true;
                    }

                    $this->session->set_userdata($session);
                    $response["data"] = $session;
                    $response["code"] = 200;

                    if ( ! $wantsJson) {
                        if ($this->session->userdata("redireccion")) {
                            $callback = $this->session->userdata("redireccion");
                            $this->session->unset_userdata("redireccion");
                            return redirect($callback);
                        }
                        $this->redireccionar();
                    }
                }
            }
        }

        if ($wantsJson) {
            return $this->output->set_status_header($response["code"])
                ->set_output(json_encode($response))
                ->set_content_type("application/json");
        }

        $callback = $this->input->get("callback", true, "String", ["required" => false]);

        $this->session->set_userdata("redireccion", $callback);

        if ($this->checker->isLogin()) {
            $this->redireccionar();
        }

        $data["login"] = "";

        $this->load->view("login/novia", $data);
    }

    public function empresa()
    {
        $response = [
            "code"    => 500,
            "data"    => [],
            "message" => "",
        ];

        $wantsJson = $this->input->get_request_header('accept', true) === "application/json";

        if ($_POST) {
            $inicio = $this->input->post("inicio", true);
            $type   = $this->input->post("type");

            if ($inicio) {
                $data["email"] = $this->input->post("email", true, "Email");
            } else {
                $email    = $this->input->post("primaryEmail", true, "Email");
                $password = $this->input->post("primaryPassword", true, "String");
                $result   = $this->Login_model->validar_usuario($email, $password);

                if ( ! $result) {
                    $data["mensaje"]     = "Usuario/Contrase&ntilde;a invalida";
                    $response["code"]    = 401;
                    $response["message"] = $data["mensaje"];
                } else {
                    $session = [
                        "intento"    => null,
                        "id_usuario" => $result->id_usuario,
                        "usuario"    => $result->id_usuario,
                        "correo"     => $result->correo,
                        "nombre"     => $result->nombre." ".$result->apellido,
                        "rol"        => $result->rol,
                        "encryption" => generarCadena(),
                    ];

                    SessionLog::create(["user_id" => $session["id_usuario"], "type" => "LOGIN", "ip" => $_SERVER["REMOTE_ADDR"]]);

                    if ($result->rol == 2) {
                        $info_user             = $this->Usuario_model->getCliente($result->id_usuario);
                        $session["id_boda"]    = $info_user->id_boda;
                        $session["id_cliente"] = $info_user->id_cliente;
                        $session["genero"]     = (($info_user->genero == 1) ? "Novio" : "Novia");
                    } elseif ($result->rol == Checker::$PROVEEDOR) {
                        $provider = User::with("provider")->find($session["id_usuario"])->provider;;
                        $session["has_payment_method"] = $provider->hasPaymentMethod();
                        $session["nombre_proveedor"]   = $provider->nombre;
                        $session["id_proveedor"]       = $provider->id_proveedor;
                        $session["slug"]               = $provider->slug;
                        $session["plan"]               = $provider->tipo_cuenta;
                        $session["plan_months"]        = $provider->plan_months;
                        $session["has_trial"]          = $provider->trial_expiration_date
                            ? Carbon::createFromFormat("Y-m-d H:i:s", $provider->trial_expiration_date)->isFuture()
                            : true;
                    }

                    $this->session->set_userdata($session);
                    $response["data"] = $session;
                    $response["code"] = 200;

                    if ( ! $wantsJson) {
                        if ($this->session->userdata("redireccion")) {
                            $callback = $this->session->userdata("redireccion");
                            $this->session->unset_userdata("redireccion");
                            return redirect($callback);
                        }
                        $this->redireccionar();
                    }
                }
            }
        }

        if ($wantsJson) {
            return $this->output->set_status_header($response["code"])
                ->set_output(json_encode($response))
                ->set_content_type("application/json");
        }

        $callback = $this->input->get("callback", true, "String", ["required" => false]);

        $this->session->set_userdata("redireccion", $callback);

        if ($this->checker->isLogin()) {
            $this->redireccionar();
        }

        $data["login"] = "";

        $this->load->view("login/empresa", $data);
    }

    public function logout()
    {
        $session = $this->session->userdata();

        if (count($session)) {
            SessionLog::create(["user_id" => $session["id_usuario"], "type" => "LOGOUT", "ip" => $_SERVER["REMOTE_ADDR"]]);
        }

        session_destroy();
        $this->session->set_userdata([]);
        redirect("/", "refresh");
    }

    public function facebook()
    {
        $this->output->set_content_type("application/json");
        $response = [
            "code" => 404,
            "data" => [],
        ];

        $email = $this->input->post("email", true, "Email");
        $user  = User::where("correo", $email)->where("activo", 1)->first();

        if ($user) {
            $session = [
                "intento"    => null,
                "id_usuario" => $user->id_usuario,
                "usuario"    => $user->id_usuario,
                "correo"     => $user->correo,
                "nombre"     => $user->nombre." ".$user->apellido,
                "rol"        => $user->rol,
                "encryption" => generarCadena(),
            ];

            if ($user->rol == 2) {
                $info_user             = $this->Usuario_model->getCliente($user->id_usuario);
                $session["id_boda"]    = $info_user->id_boda;
                $session["id_cliente"] = $info_user->id_cliente;
                $session["genero"]     = (($info_user->genero == 1) ? "Novio" : "Novia");
            } elseif ($user->rol == 3) {
                $empresa                     = $this->proveedor->get(["id_usuario" => $session["id_usuario"]]);
                $session["id_proveedor"]     = $empresa->id_proveedor;
                $session["nombre_proveedor"] = $empresa->nombre;
                $session["slug"]             = $empresa->slug;
            }

            $this->session->set_userdata($session);

            $response["code"] = 200;
            $response["data"] = $user;
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    private function redireccionar()
    {
        $id_rol = $this->session->userdata("rol");
        switch ($id_rol) {
            case Checker::$ADMIN:
            case Checker::$ADMINAPP:
                redirect("admin/");
                break;
            case Checker::$NOVIO:
                if (isset($_GET['callback'])) {
                    $callback = $this->input->get("callback");
                    redirect($callback, "refresh");
                } else {
                    redirect("novios/presupuesto", "refresh");
                }
                break;
            case Checker::$REDACTOR:
                redirect("blog/");
                break;
            case Checker::$PROVEEDOR:
                if (isset($_GET['callback'])) {
                    $callback = $this->input->get("callback");
                    redirect($callback, "refresh");
                } else {
                    /////Incremento en inicio de sesion
                    if($this->session->userdata("rol") == 3){
                        DB::table('indicators')->insert(
                            ['provider_id' => $this->session->userdata("id_proveedor"), 'type' => 'login', 'created_at' => (new \DateTime()), 'updated_at' => (new \DateTime())]
                        );
                    }
                    redirect("proveedor/");
                }
                break;
            case Checker::$MODERADOR:
                redirect("");
                break;
            case Checker::$GALERIA:
                redirect("ADGaleria/");
                break;
        }
    }
}
