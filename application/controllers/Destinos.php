<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;

class Destinos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
    }

    public function index() {
        $data['magazine'] = DB::table('blog_post as a')
        ->join('blog_post_categoria as b', 'a.tipo', '=', 'b.id_categoria')
        ->select('b.name_category', 'a.*')
        ->where('a.tipo', 3)
        ->orderBy('a.fecha_creacion', 'DESC')
        ->take(3)->get();

        $data['destinations']= DB::table('destinations')->get();

        $this->load->view("destinos/index", $data);
    }
    public function destino()
    {
        $destino = $this->input->get("destino");
        $data['destination'] = DB::table('destinations')->where('name', $destino)->first();
        $data['locations'] = DB::table('locations_attractives')->
            where('id_destinations', $data['destination']->id)
            ->where('type', 'Locacion')->get();
        $data['attractives'] = DB::table('locations_attractives')->
            where('id_destinations', $data['destination']->id)
            ->where('type', 'Atractivo')->get();
        
        $data["recommendations"] = DB::table('destinations')->where('id', '!=', $data['destination']->id )->take(4)->get();

        // dd($data['attractives']);

        $this->load->view("destinos/destino", $data);
    }
    public function autocomplete()
    {
        $term = $this->input->get("term");

        $categories = DB::table('destinations')->select('name')->where('name', 'like', '%'.$term.'%')->get();
        $aux = [];
        // foreach ($proveedores as $key => $v) {
        //     $aux[$key] = $v['nombre'];

        // }
        foreach ($categories as $key => $v) {
            $aux[$key] = $v->name;
        }
        $busqueda = $aux;
        unset($aux);

        return $this->output->set_status_header(200)
            ->set_output(json_encode($busqueda));
    }
}