<?php

class Errors extends CI_Controller
{
    public $input;

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Comunidad/Comunidad_model", "comunidad");
        $this->load->model("Comunidad/Imagen_model", "imagen");
        $this->load->model("Comunidad/Perfil_model", "perfil");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url()."index.php/novios/comunidad/Home/imagen/";
        $this->load->helper("formats_helper");
    }

    public function error_404()
    {
        $this->load->view("errors/html/error_general");
    }

}

