<?php

use Carbon\Carbon;

class Escaparate extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Usuario_model");
        $this->load->model("Cliente_model");
        $this->load->model("Boda_model");
        $this->load->model("Invitados/Menu_model", "Menu_model");
        $this->load->model("Invitados/Mesa_model", "Mesa_model");
        $this->load->model("Invitados/Invitado_model", "Invitado_model");
        $this->load->model("Invitados/Grupo_model", "Grupo_model");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->model("Proveedor/Promocion_model", "promocion");
        $this->load->model("Proveedor/Colaborador_model", "colaborador");
        $this->load->model("Proveedor/FAQ_model", "faq");
        $this->load->model("Proveedor/RespuestaFAQ_model", "respuesta");
        $this->load->model("Proveedor/TipoProveedor_model", "tipo");
        $this->load->model("Proveedor/Solicitud_model", "solicitud");
        $this->load->model("Proveedor/Evento_model", "evento");
        $this->load->model("Proveedor/ProveedorBoda_model", "ProveedorBoda_model");
        $this->load->model("Correo/Correo_model", "correo");
        $this->load->model("MiPortal/MiPortal_model", "MiPortal_model");
        $this->load->model("Producto_model", "producto");
        $this->load->model("Cliente_model", "cliente");
        $this->load->model("Boda_model", "boda");
        $this->load->model("Usuario_model", "usuario");
        $this->load->helper("slug");
        $this->load->library("Checker");
        $this->load->library("My_PHPMailer");

        $this->mailer = $this->my_phpmailer;
        $this->load->helper("formats");
    }

    public function proveedor($slug)
    {
        $data["proveedor"] = Provider::where("slug", $slug)->first();
        if ($data["proveedor"]) {
            $data = $this->getData($slug);

            if ($this->checker->isLogin() && $this->checker->isNovio()) {
                $data["favorito"] = $this->proveedor->isFavorito($data["proveedor"]->id_proveedor, $this->session->userdata("id_boda"));
            }

            return $this->load->view("principal/proveedor/escaparate", $data);
        }
        show_404();
    }

    public function recomendaciones($slug, $id_boda = null)
    {
        if ($slug) {
            $data["proveedor"] = Provider::where("slug", $slug)->first();
            if ($data["proveedor"]) {
                $data = $this->getData($data["proveedor"]->slug);

                $data["titulo"]          = "Recomendacion en japy.mx";
                $p                       = $this->input->get("pagina", true, "String", ["required" => false]);
                $page                    = $p ? $p : 1;
                $data["page"]            = $page;
                $data["allowToReview"]   = true;
                $data["recomendaciones"] = Review::where("proveedor_id", $data["proveedor"]->id_proveedor)
                    ->where("activo", 1)->orderBy('id_recomendacion', 'desc')->get();
                $data["starsReviews"]    = $this->ProveedorBoda_model->getCalification($data["proveedor"]->id_proveedor);
                foreach ($data["recomendaciones"] as $key => $r) {
                    if ($r->usuario_id === $this->session->userdata("id_usuario")):
                        $data["allowToReview"] = false;
                    endif;
                }

                $data["total"] = ($this->proveedor->totalRecomendaciones($data["proveedor"]->id_proveedor)/ 10) + 1;
                $data["id"]    = $data["proveedor"]->id_proveedor;

                if ($this->checker->isNovio()) {
                    $this->proveedor->registrarVisita($data["proveedor"]->id_proveedor,
                        $this->session->userdata("id_cliente"));
                    $data["favorito"] = $this->proveedor->isFavorito($data["proveedor"]->id_proveedor,
                        $this->session->userdata("id_boda"));
                }

                return $this->load->view("principal/proveedor/recomendaciones", $data);
            }
        }
        show_404();
    }

    public function faqs($slug)
    {
        $data["proveedor"] = Provider::where("slug", $slug)->first();
        if ($data["proveedor"]) {
            $data = $this->getData($slug);

            return $this->load->view("principal/proveedor/faqs", $data);
        }
        show_404();
    }

    public function mapa($slug)
    {
        $data["proveedor"] = Provider::where("slug", $slug)->first();
        if ($data["proveedor"]) {
            $data = $this->getData($slug);

            return $this->load->view("principal/proveedor/mapa", $data);
        }
        show_404();
    }

    public function fotos($id)
    {
        if (is_numeric($id) && $id > 0) {
            $data["proveedor"] = $this->proveedor->get($id);
            if ($data["proveedor"]) {
                $data = $this->getData($id);

                return $this->load->view("principal/proveedor/fotos", $data);
            }
        }
        show_404();
    }

    public function videos($slug, $id_video = 0)
    {
        $data["proveedor"] = Provider::where("slug", $slug)->first();
        if ($data["proveedor"]) {
            $data   = $this->getData($slug);

            if ($data["videos"]->count() == 0) {
                redirect($slug);
            }
            return $this->load->view("principal/proveedor/videos", $data);
        }
        show_404();
    }

    public function productos($id, $id_producto = null)
    {
        if (is_numeric($id)) {
            $data = $this->getData();
            if ($id_producto === null) {
                $data["productos"]
                    = $this->producto->getList(["id_proveedor" => $id]);
            } else {
                $data["productos"] = $this->producto->getList(
                    ["id_proveedor" => $id, "id_producto" => $id_producto]
                );
            }

            return $this->load->view("principal/proveedor/productos", $data);
        }
        show_404();
    }

    public function eventos($id, $id_evento = null)
    {
        if (is_numeric($id)) {
            $data = $this->getData($id);
            if ($id_evento == null) {
                $data["eventos"] = $this->evento->getEventosFuturos($id);
            } else {
                $data["eventos"] = [$this->evento->get($id_evento)];
            }

            return $this->load->view("principal/proveedor/eventos", $data);
        }
        show_404();
    }

    public function promociones($slug)
    {
        // $data["proveedor"] = Provider::where("slug", $slug)->first();
        // if ($data["proveedor"]) {
        //     $data = $this->getData($slug);

        //     return $this->load->view("principal/proveedor/promociones", $data);
        // }
        // show_404();
    
       // $data["proveedor"] = Provider::where("slug", $slug)->first();
        $data["promociones"] = $this->promocion->get($slug);

        $id_prov = $data["promociones"]->id_proveedor;

        $data["proveedor"] = Provider::where("id_proveedor", $id_prov)->first();

       // if ($data["promociones"]) {
           // $data = $this->getData($slug);

            return $this->load->view("principal/proveedor/promociones", $data);
       // }
        show_404();
 
    }    

    /*public function promociones($id, $id_promocion = null)
    {
        if (is_numeric($id) && $id > 0) {
            $data["proveedor"] = $this->proveedor->get($id);
            if ($data["proveedor"]) {
                $data = $this->getData($id);
                if ($id_promocion == null) {
                    return $this->load->view("principal/proveedor/promociones",
                        $data);
                } else {
                    if ($id_promocion == 0 && $data["proveedor"]->descuento) {
                        $temp               = new stdClass();
                        $temp->nombre       = $data["proveedor"]->descuento
                            ."% de descuento para novi@s de japy.mx";
                        $temp->descripcion
                                            = "Si vienes de parte de japy.mx te haremos un "
                            .$data["proveedor"]->descuento
                            ."% de descuento en los servicios contratados. No olvides presentar tu cupón cuando vengas a vernos.";
                        $temp->fecha_fin    = null;
                        $temp->imagen       = null;
                        $temp->mime_imagen  = null;
                        $temp->id_proveedor = $data["proveedor"]->id_proveedor;
                        $temp->id_promocion = 0;
                        $temp->tipo         = "DESCUENTO";
                        $data["promocion"]  = $temp;
                    } else {
                        $data["promocion"] = $this->promocion->get(
                            [
                                "id_proveedor" => $data["proveedor"]->id_proveedor,
                                "id_promocion" => $id_promocion,
                            ]
                        );
                    }
                    $data["descargas"] = $this->promocion->getDescargas(
                        $data["promocion"]->id_proveedor,
                        $data["promocion"]->id_promocion
                    );
                    if ($data["promocion"]) {
                        return $this->load->view("principal/proveedor/promocion",
                            $data);
                    }
                }
            }
        }
        show_404();
    }*/

    public function enviar_solicitud_informacion($id_proveedor)
    {
        $this->output->set_content_type("application/json");

        $response = [
            "code"    => 200,
            "data"    => [],
            "message" => 'Ok',
        ];

        $this->input->post("comentario", true, "String", ["required" => true, "min" => 0, "max" => 500]);
        $user     = User::find($this->session->userdata("id_usuario"));
        $provider = Provider::find($id_proveedor);
        $isCoupon = $this->input->post('isCoupon');

        if ($user->provider) {
            $response['code']    = 403;
            $response['message'] = "Esta funcion es solo para usuarios";
        } elseif ( ! $user) {
            $response['code']    = 401;
            $response['message'] = "Necesitas iniciar sesion";
        } elseif ( ! $provider) {
            $response['code']    = 404;
            $response['message'] = "Proveedor inexistente";
        } elseif ($this->input->isValid()) {
            $id_usuario = $this->session->userdata("id_usuario");
            $id_cliente = $this->session->userdata("id_cliente");
            $id_boda    = $this->session->userdata("id_boda");

            $provider->indicators()
                ->attach($this->session->userdata('id_usuario'),
                    ['type' => 'contact_request']);

            $indicator = $provider->indicators()->latest()->first()->pivot;

            if ($isCoupon) {
                $to   = $id_usuario;
                $from = $provider->user->id_usuario;
            } else {
                $to   = $provider->user->id_usuario;
                $from = $id_usuario;
            }

            $correo = [
                "mensaje"       => $this->input->post("comentario"),
                "asunto"        => $this->input->post("asunto", true, "String", ["max" => 140]),
                "to"            => $to,
                "from"          => $from,
                "indicator_id"  => $indicator->id,
                "leida_usuario" => 1,
            ];

            $correo = $this->correo->insert($correo);
            $this->correo->sendEmail();

        } else {
            $response['code']    = 400;
            $response['message'] = "Tu peticion no es valida";
        }

        return $this->output->set_status_header($response['code'])
            ->set_output(json_encode($response));
    }

    private function solicitud($id_proveedor)
    {
        $id_boda    = $this->session->userdata("id_boda");
        $id_cliente = $this->session->userdata("id_cliente");
        if ($_POST && $id_cliente) {
            $usuario = [
                "nombre"    => $this->input->post("nombre", true, "String",
                    ["required" => true]),
                "correo"    => $this->input->post("correo", true, "Email",
                    ["required" => true]),
                "telefono"  => $this->input->post("telefono", true, "String"),
                "invitados" => $this->input->post("invitados", true, "Integer",
                    ["min" => 1]),
            ];
            if ($id_boda) {
                /* $solicitud = array(
                  "mensaje" => $this->input->post("comentario", true, "String", array("required" => true, "max" => 250)),
                  "asunto" => $this->input->post("asunto", true, "String", array("max" => 140)),
                  "id_boda" => $id_boda,
                  "id_cliente" => $this->session->userdata("id_boda"),
                  ); */
                $correo = [
                    "mensaje" => $this->input->post(
                        "comentario",
                        true,
                        "String",
                        ["required" => true, "max" => 250]
                    ),
                    "asunto"  => $this->input->post("asunto", true, "String",
                        ["max" => 140]),
                    "to"      => $id_proveedor,
                    "from"    => $id_cliente,
                ];
                if ($this->input->isValid()) {
                    $b = $this->correo->insert($correo);
                    if ($b) {
                    }
                }
            }
        }
    }

    public function contact()
    {
        $this->output->set_content_type("application/json");
        $response = [
            "code" => 200,
            "data" => [],
        ];

        $data       = [
            "name"    => $this->input->post("name"),
            "email"   => $this->input->post("email"),
            "message" => $this->input->post("message"),
        ];
        $provider   = Provider::find($this->input->post("providerId"));
        $user       = User::find($this->session->userdata("id_usuario"));
        $userExists = User::where("correo", $data["email"])->first();

        if ( ! $provider) {
            $response["code"]    = 404;
            $response["message"] = "Proveedor inexistente";
        } elseif ($user && $user->provider) {
            $response["code"]    = 403;
            $response["message"] = "Esta funcion es solo para usuarios";
        } elseif ( ! $user && $userExists) {
            //If the user is not logged in but there's a user with the email he's entering
            $response["code"]    = 403;
            $response["message"] = "Ese correo ya ha sido registrado";
        } elseif ( ! $user && ! $userExists) {
            //If the user is not logged in and there's no user with that email
            $data["password"] = base64_encode(random_bytes(10));
            $data["gender"]   = 2;

            $userId = $this->usuario->register($data);

            if ( ! $userId) {
                $response["code"]    = 500;
                $response["message"] = "Oops ocurrio un error intentalo de nuevo mas tarde";
            }

            $user = User::find($userId);
        }

        if ($response["code"] == 200) {
            $provider->indicators()->attach($this->session->userdata("id_usuario"), ['type' => 'contact_request']);
            $indicator = $provider->indicators()->latest()->first()->pivot;

            $to   = $provider->user->id_usuario;
            $from = $user->id_usuario;

            $correo = [
                "mensaje"       => $this->input->post("message"),
                "asunto"        => "Un usuario te esta intentando contactar",
                "to"            => $to,
                "from"          => $from,
                "indicator_id"  => $indicator->id,
                "leida_usuario" => 1,
            ];

            $correo = $this->correo->insert($correo);
            $this->correo->sendNotification($from, $provider, "request_info");
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function descarga_cupon($id_proveedor)
    {
        if ($_POST && is_numeric($id_proveedor) && $id_proveedor > 0) {
            $promocion = $this->input->post("promocion", true, "Integer",
                ["required" => true, "min" => 0]);
            if ($this->input->isValid()) {
                $this->proveedor->registrarDescargaCupon(
                    $id_proveedor,
                    $promocion,
                    $this->session->userdata("id_cliente")
                );
            }

            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(
                    json_encode(
                        [
                            'success' => true,
                            'data'    => "ok",
                        ]
                    )
                );
        }

        return $this->output->set_status_header(404);
    }

    public function telefono($id)
    {
        if ($this->checker->isNovio()) {
            if (is_numeric($id) && $id > 0) {
                $proveedor = $this->proveedor->get($id);
                if ($proveedor) {
                    if ($this->checker->isNovio()) {
                        $this->proveedor->registrarVerTel($id,
                            $this->session->userdata("id_cliente"));
                    }

                    $provider = Provider::find($id);
                    $provider->indicators()
                        ->attach($this->session->userdata('id_usuario'),
                            ['type' => 'phone']);

                    return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output(
                            json_encode(
                                [
                                    'success' => true,
                                    'data'    => ["telefono" => $proveedor->contacto_telefono],
                                ]
                            )
                        );
                }
            }
        }

        return $this->output->set_status_header(404);
    }

    public function info($id_proveedor)
    {
        if ($this->checker->isNovio()) {
            if (is_numeric($id_proveedor) && $id_proveedor > 0) {

                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(
                        json_encode(
                            [
                                'success' => true,
                            ]
                        )
                    );
            }
        }

        return $this->output->set_status_header(404);
    }

    public function guardarlo($id)
    {
        if ($this->checker->isNovio()) {
            if (is_numeric($id) && $id > 0) {
                $id_boda    = $this->session->userdata("id_boda");
                $isFavorito = $this->proveedor->isFavorito($id, $id_boda);

                if ($isFavorito) {
                    $this->proveedor->no_favorito($id, $id_boda);
                } else {
                    $this->proveedor->favorito($id, $id_boda);
                }

                $isFavorito = $this->proveedor->isFavorito($id, $id_boda);

                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(
                        json_encode(
                            [
                                'success' => true,
                                'data'    => ["favorito" => $isFavorito],
                            ]
                        )
                    );
            }
        }

        return $this->output->set_status_header(404);
    }

    public function calificar()
    {
        $this->output->set_content_type('application/json');
        $stars    = is_null($this->input->post('stars')) ? 0 : $this->input->post('stars');
        $provider = Provider::find($this->input->post("providerId"));

        if ($provider) {
            $wedding = Wedding::with([
                'providers' => function ($query) use ($provider) {
                    return $query->where('proveedor_boda.id_proveedor',
                        $provider->id_proveedor);
                },
            ])->find($this->session->userdata("id_boda"));

            if ($_POST && $provider && $wedding->providers->count()) {
                $wedding->providers()
                    ->updateExistingPivot($provider->id_proveedor,
                        ['calificacion' => $stars]);
                $provider->indicators()
                    ->attach($this->session->userdata('id_usuario'),
                        ['type' => 'rating']);

                return $this->output->set_status_header(200)
                    ->set_output(
                        json_encode([
                            'success' => true,
                        ])
                    );
            }
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode([
                    'success' => false,
                    'data'    => "Recurso no entrado",
                ])
            );
    }

    private function getData($slug)
    {
        $provider = Provider::with([
            "images"     => function ($query) {
                return $query->where("parent", null);
            },
            "videos",
            "promotions" => function ($query) {
                return $query->where("fecha_fin", ">", Carbon::now());
            },
            "collaborators",
            "FAQ",
            "categories",
        ])->where("slug", $slug)->first();

        $userId = $this->session->userdata("id_usuario")
            ? $this->session->userdata("id_usuario") : [null];

        if ($this->checker->isNovio()) {

            //If it's the first time the user visits the provider

            $firstTimeVisit = $provider
                ->indicators()
                ->where("user_id", $userId)
                ->where("type", "visit")
                ->count();

            if ( ! $firstTimeVisit) {
                $this->correo->sendNotification($this->session->userdata("id_usuario"), $provider, "first_visit");
            }

            $data["favorito"]
                = $this->proveedor->isFavorito($provider->id_proveedor,
                $this->session->userdata("id_boda"));
        }

        $provider->indicators()->attach($userId, ["type" => "visit"]);

        $base                         = base_url()."uploads/images/";
        $data["proveedor"]            = $this->proveedor->get($provider->id_proveedor);
        $data["proveedor"]->logo      = $provider->imageLogoMiniature()->first() ? $base.$provider->imageLogoMiniature()->first()->nombre : null;
        $data["proveedor"]->principal = $provider->imagePrincipalMiniature()->first() ? $base.$provider->imagePrincipalMiniature()->first()->nombre : null;

        $data["faqs"]            = $provider->FAQ;
        $data["tipo"]            = $provider->categories->first();
        $data["videos"]          = $provider->videos;
        $data["eventos"]
                                 = $this->evento->hasEventos($provider->id_proveedor);
        $data["galeria"]
                                 = $provider->images->take($provider->allowedImagesQuantity());
        $data["favorito"]        = false;
        $data["provider"]        = Provider::with("categories","weddings")->find($provider->id_proveedor);
        $data["promociones"]     = $provider->promotions;
        $data["colaboradores"]   = $provider->collaborators;
        $data["recomendaciones"] = $this->proveedor->hasRecomendaciones($data["proveedor"]->id_proveedor);

        foreach ($data["faqs"] as $faq) {
            $faq->respuesta      = $faq->pivot->respuesta;
            $data["preguntas"][] = $faq;
        }

        return $data;
    }

    public function saveProviderReview()
    {

        if (isset($_POST)) {

            Review::create([
                'servicio'        => $this->input->post('service'),
                'calidad_precio'  => $this->input->post('price'),
                'profesionalidad' => $this->input->post('professional'),
                'respuesta'       => $this->input->post('answer'),
                'flexibilidad'    => $this->input->post('flex'),
                'boda_id'         => Client::where("id_usuario", $this->session->userdata("id_usuario"))->first()->id_boda,
                'proveedor_id'    => $this->input->post("provider"),
                'activo'          => 1,
                'asunto'          => $this->input->post('title'),
                'mensaje'         => $this->input->post('message'),
                'usuario_id'      => $this->session->userdata('id_usuario'),
            ])->id_recomendacion;

            return $this->output->set_status_header(200)
                ->set_output(
                    json_encode([
                        'success' => true,
                        'data'    => "Guardado",
                    ])
                );
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode([
                    'success' => false,
                    'data'    => "Error, no se guardado la opinión",
                ])
            );
    }
}
