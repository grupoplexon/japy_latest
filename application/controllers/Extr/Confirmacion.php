<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Confirmacion extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model('Invitados/Confirmacion_model');
    }

    public function index($codigo)
    {
        $id_invitado = $this->Confirmacion_model->get($codigo);
        if (empty($id_invitado)) {
            echo 'NO EXISTE INVITACIÓN';
        } else {
            $this->Confirmacion_model->delete($codigo);
            $this->Confirmacion_model->updateInvitado(array('confirmado' => 2), $id_invitado->id_invitado);
            echo 'GRACIAS POR CONFIRMAR TU ASISTENCIA';
        }
    }
}