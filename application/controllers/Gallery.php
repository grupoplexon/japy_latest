<?php

use Illuminate\Database\Capsule\Manager as DB;

use Models\Gallery as GalleryModel;
use Models\Tag as Tag;

class Gallery extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->output->set_content_type("application/json");
    }


    public function autocomplete()
    {
        $response["code"] = 200;

        $searchString = $this->input->post("search");

        $response["data"] = Tag::has('images')->where("title", "like", "%$searchString%")->get()->pluck("title")->toArray();

        return $this->output->set_status_header($response['code'])
            ->set_output(json_encode($response));
    }


    public function search()
    {
        $response["code"] = 200;

        $searchTags = array_map('strtolower', $this->input->post("tags"));

        $query = Tag::has('images')->with('images');

        foreach ($searchTags as $key => $searchTag) {
            if ($key == 0) {
                $query = $query->where('title', 'like', '%'.$searchTag.'%');
            } else {
                $query = $query->orWhere('title', 'like', '%'.$searchTag.'%');
            }
        }

        $tags = $query->get();

        //We get the images from the tag
        $images = GalleryModel::whereIn('id_galeria', DB::table('taggables')
            ->select('taggable_id')
            ->whereIn('tag_id', $tags->pluck('id'))
            ->groupBy('taggable_id')
            ->get()->pluck('taggable_id'))->get();

        $availableTags = DB::table('taggables')
            ->select('tags.title')
            ->join('tags', 'taggables.tag_id', '=', 'tags.id')
            ->whereIn('taggables.taggable_id', $images->pluck('id_galeria'))
            ->whereNotIn('taggables.tag_id', $tags->pluck('id'))
            ->groupBy('taggables.tag_id')->pluck('title')->unique()->values();

        $response["data"]["images"]         = $images;
        $response["data"]["available_tags"] = $availableTags;

        return $this->output->set_status_header($response['code'])
            ->set_output(json_encode($response));
    }

}