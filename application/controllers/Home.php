<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Blog/Post_model", "post");
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->helper("formats");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->model('Home/Home_model', 'Home_model');
        $this->load->model('MiPortal/MiPlantilla_model', 'MiPlantilla_model');
        $this->load->model('MiPortal/MiPortal_model', 'MiPortal_model');
        $this->load->model('MiPortal/MiPortalSeccion_model', 'MiPortalSeccion_model');
        $this->load->model('MiPortal/MiPortalLibro_model', 'MiPortalLibro_model');
        $this->load->model('MiPortal/MiPortalEvento_model', 'MiPortalEvento_model');
        $this->load->model('MiPortal/MiPortalEncuesta_model', 'MiPortalEncuesta_model');
        $this->load->model('MiPortal/MiPortalBlog_model', 'MiPortalBlog_model');
        $this->load->model('MiPortal/MiPortalDireccion_model', 'MiPortalDireccion_model');
        $this->load->model('MiPortal/MiPortalVideo_model', 'MiPortalVideo_model');
        $this->load->model('MiPortal/MiPortalAlbum_model', 'MiPortalAlbum_model');
        $this->load->model('MiPortal/MiPortalTest_model', 'MiPortalTest_model');
        $this->load->model('MiPortal/MiPortalBlog_model', 'MiPortalBlog_model');
        $this->load->model('MiPortal/MiPortalSeccion_Imagen_model', 'MiPortalSeccionImagen_model');
        $this->load->model('Vestido/Vestido_model', 'vestido');
        $this->load->model('Vestido/Tipo_model', 'tipo_vestido');
        $this->load->model("Correo/Correo_model", "correo");
        $this->load->library("My_PHPMailer");
        $this->mailer = $this->my_phpmailer;
    }

    public function proximamente()
    {
        $this->load->view("proximamente/index");
    }

    public function email()
    {
        $mailer                    = $this->my_phpmailer;
        $data["bride"]             = "María Salcedo";
        $data["user"]["telephone"] = "3366220011";
        $data["user"]["email"]     = "brianoop3@gmail.com";
        $data["user"]["subject"]   = "asuntote";
        $data["mail"]["sentAt"]    = "hoy";
        $data["mail"]["message"]   = "Mensaje de Prueba faiki novia";
        $data["mail"]["parent"]    = "101";
        $data["correo"]            = "lirarararara@gmail.com";
        $data["user"]["name"]      = "Prueba 3";
        $data["rol"]               = "2";
        $data["sentTo"]            = "brianoop3@gmail.com";

        $mailer->to($data["sentTo"], $data["sentTo"]);
        $mailer->send($this->load->view("templates/email_template", $data, true));
    }

    public function index()
    {
        if (isset($this->session->userdata()["id_usuario"])) {
            $this->redireccionar();
        }

        $data["providers"] = Provider::whereHas("user", function ($query) {
            return $query->where("activo", "!=", 1);
        })->where("tipo_cuenta", 3)->where("localizacion_estado", "Jalisco")->where('id_proveedor', '<>',
            103)->inRandomOrder()->get()->take(3);
            
        foreach ($data["providers"] as &$prov) {
            $proveedor   = DB::table('average_reviews')->where('id_proveedor',$prov->id_proveedor)->first();
            if($proveedor!=null){
                $prov->promedio = $proveedor->promedio;
            } else{
                $prov->promedio = 0;
            }
        }

        $data["proveedores"] = Provider::whereHas("user", function ($query) {
            return $query->where("activo", "!=", 1);
        })->get();

        $data["posts"]       = $this->post->getListCategoria(0, 3, 0);
        $data["pais"]        = "Mexico";
        $data["home_imagen"] = $this->Home_model->get();

        $data["categories"] = Category::with('subcategories')->main()->get();

        
        // $this->load->view("home/index", $data);
        $this->load->view("japy/prueba/index", $data);
    }
    
    public function index_prueba()
    {
        if (isset($this->session->userdata()["id_usuario"])) {
            $this->redireccionar();
        }

        $data["providers"] = Provider::whereHas("user", function ($query) {
            return $query->where("activo", "!=", 1);
        })->where("tipo_cuenta", 3)->where("localizacion_estado", "Jalisco")->where('id_proveedor', '<>',
            103)->inRandomOrder()->get()->take(6); 

        foreach ($data["providers"] as &$prov) {
            $proveedor   = DB::table('average_reviews')->where('id_proveedor',$prov->id_proveedor)->first();
            if($proveedor!=null){
                $prov->promedio = $proveedor->promedio;
            } else{
                $prov->promedio = 0;
            }
        }            

        $data["proveedores"] = Provider::whereHas("user", function ($query) {
            return $query->where("activo", "!=", 1);
        })->get();

        $data["posts"]       = $this->post->getListCategoria(0, 3, 0);
        $data["pais"]        = "Mexico";
        $data["home_imagen"] = $this->Home_model->get();

        $data["categories"] = Category::with('subcategories')->main()->get();

        
        // $this->load->view("home/index", $data);
        $this->load->view("japy/prueba/index", $data);
    }

    public function suscripcion(){
        /////falta poner validacion si ya existe el correo

        $email = $this->input->post("email");

        $registro = DB::table('suscripciones')->where('email',$email)->first();
        if(empty($registro)){
            $res = DB::table('suscripciones')->insert(
                ['email' => $email]
            );
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(202)
                ->set_output($res);
        }
        else{
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(202)
                ->set_output($res);
        }
    }

    public function promociones()
    {
        $this->load->view("principal/promocion/promociones");
    }

    public function expo_eventos()
    {
        $this->load->view("principal/expo_bodas/eventos");
    }

    public function expo()
    {
        if (isset($_GET['estado'])) {
            $state = $_GET['estado'];
        } else {
            $state = "jalisco";
        }
        $fexpo       = new DateTime("2018-09-22 12:00:00");
        $state       = strtolower($state);
        
        $data["now"] = new DateTime("now");
        $data['estado'] = $state;
        $eventsDate = [
            [
                "state"   => "queretaro",
                "date"    => "2019-09-07 12:00:00",
                "hasInfo" => true,
            ],
            [
                "state"   => "jalisco",
                "date"    => "2019-06-29 12:00:00",
                "hasInfo" => true,
            ],
            [
                "state"   => "guanajuato",
                "date"    => "2019-02-16 12:00:00",
                "hasInfo" => false,
            ],
            [
                "state"   => "puebla",
                "date"    => "2019-08-24 12:00:00",
                "hasInfo" => false,
            ],
            [
                "state"   => "cdmx",
                "date"    => "2019-02-08 12:00:00",
                "hasInfo" => false,
            ],
            [
                "state"   => "nuevo_leon",
                "date"    => "2019-10-27 12:00:00",
                "hasInfo" => false,
            ],
            [
                "state"   => "sinaloa",
                "date"    => "2019-04-06 12:00:00",
                "hasInfo" => false,
            ],
            [
                "state"   => "veracruz",
                "date"    => "2019-02-23 12:00:00",
                "hasInfo" => false,
            ],
        ];

        foreach ($eventsDate as $event => $eventVal) {
            if ($eventVal["state"] === $state) {
                $data["fexpo"]     = new DateTime($eventVal["date"]);
                $data["eventDate"] = $eventVal["date"];
                break;
            }
        }

        $this->load->view("principal/expo_bodas/expo", $data);
    }

    public function correo()
    {
        $this->load->view("templates/reminders/1_weeks");
    }

    public function busquedaDonde()
    {
        $this->output->set_content_type('application/json');
        $busqueda = $this->input->post("busqueda", true, "String", ["required" => true]);
        $estados  = $this->tipo_proveedor->busquedaDondeEstados($busqueda);
        $ciudades = $this->tipo_proveedor->busquedaDondeCiudades($busqueda);

        if ($estados || $ciudades) {
            return $this->output->set_status_header(202)
                ->set_output(
                    json_encode(
                        [
                            'success'  => true,
                            'estados'  => $estados,
                            'ciudades' => $ciudades,
                        ]
                    )
                );
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        'success' => false,
                    ]
                )
            );
    }

    public function busquedaQue()
    {
        $this->output->set_content_type('application/json');
        $busqueda    = $this->input->post("busqueda", true, "String", ["required" => true]);
        $grupos      = $this->tipo_proveedor->busquedaQueGrupos($busqueda);
        $categorias  = $this->tipo_proveedor->busquedaQueCategorias($busqueda, count($grupos));
        $proveedores = $this->tipo_proveedor->busquedaQueProveedores(
            $busqueda,
            count($grupos) + count($categorias)
        );
        if ($grupos || $categorias || $proveedores) {
            return $this->output->set_status_header(202)
                ->set_output(
                    json_encode(
                        [
                            'success'     => true,
                            'grupos'      => $grupos,
                            'categorias'  => $categorias,
                            'proveedores' => $proveedores,
                            'cantidad'    => count($grupos) + count($categorias),
                        ]
                    )
                );
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        'success' => false,
                    ]
                )
            );
    }

    public function getEstados()
    {
        $this->output->set_content_type('application/json');
        $estados = $this->tipo_proveedor->getEstados();
        if ($estados) {
            return $this->output->set_status_header(202)
                ->set_output(
                    json_encode(
                        [
                            'success' => true,
                            'estados' => $estados,
                        ]
                    )
                );
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(['success' => false])
            );
    }

    public function getTipos()
    {
        $this->output->set_content_type('application/json');
        $tipos = $this->tipo_proveedor->getGrupos();
        foreach ($tipos as &$value) {
            $value->tipos = $this->tipo_proveedor->getGrupo($value->nombre);
        }
        if ($tipos) {
            return $this->output->set_status_header(202)
                ->set_output(
                    json_encode(
                        [
                            'success' => true,
                            'tipos'   => $tipos,
                        ]
                    )
                );
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        'success' => false,
                    ]
                )
            );
    }

    public function search()
    {
        $estado = $this->input->post("estado", true, "String", ["required" => true]);
        $ciudad = $this->input->post("ciudad", true, "String", ["required" => true]);
        $que    = $this->input->post("que", true, "String", ["required" => true]);
        $que2   = $que;
        $que    = str_replace(" ", "-", $que);
        $estado = str_replace(" ", "-", $estado);
        $ciudad = str_replace(" ", "-", $ciudad);
        if ($que == "PROVEEDORES" || $que == "BANQUETES" || $que == "NOVIAS" || $que == "NOVIOS") {
            redirect("/proveedores/sector/$que/$estado/$ciudad");
        }
        $que = $this->tipo_proveedor->busquedaRelacionadas($que2);
        //$que[0]->nombre_tipo_proveedor);
        $que = str_replace(" ", "-", $que);
        redirect("/proveedores/categoria/$que/$estado/$ciudad");
    }

    public function novia()
    {
        $this->load->view("principal/newHeader");
        $this->load->view("principal/novia/index");
        $this->load->view("principal/footer");
    }

    public function imagen_proveedor($id_imagen)
    {
        $image = Gallery::find($id_imagen);

        if ($image) {
            $output = $image->data;

            if ( ! is_null($image->parent)) {
                $output = base64_decode(str_replace("data:image/jpeg;base64,", "", $output));
            }

            if (is_null($image->data)) {
                $output = base_url()."uploads/images/".$image->nombre;
            }

            return $this->output
                ->set_content_type($image->mime)
                ->set_status_header(202)
                ->set_output($output);
        }

        return $this->output
            ->set_content_type("application/json")
            ->set_status_header(404);
    }

    public function imagen($id_imagen)
    {
        $id_imagen = explode(".jpg", $id_imagen);
        $id_imagen = $id_imagen[0];
        $imagen    = $this->Home_model->get($id_imagen);
        $this->output->cache(60 * 24);
        if ($imagen) {
            return $this->output
                ->set_content_type($imagen->mime)
                ->set_status_header(202)
                ->set_output($imagen->base);
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404);
    }

    public function imagen_disenador($id_disenador)
    {
        $imagen = $this->tipo_vestido->get($id_disenador);
        $this->output->cache(60 * 24);
        if ($imagen) {
            return $this->output
                ->set_content_type($imagen->mime)
                ->set_status_header(202)
                ->set_output($imagen->base);
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404);
    }

    public function imagen_expo($id_expo)
    {
        $this->load->model("App/ADExpo_model", "expo");
        $imagen = $this->expo->getExpoA(encrypt($id_expo));
//        $this->output->cache(60 * 24);
        if ($imagen) {
            $file = $this->parseBase64($imagen["imagen"]);

            return $this->output
                ->set_content_type($file->mime)
                ->set_status_header(202)
                ->set_output($file->imagen);
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404);
    }

    private function parseBase64($strBase64)
    {
        $o = new stdClass();
        if (empty($strBase64)) {
            $o->mime   = null;
            $o->imagen = null;
        } else {
            $mime      = explode(",", $strBase64);
            $o->imagen = base64_decode($mime[1]);
            $mime      = explode(";", $mime[0]);
            $mime      = explode(":", $mime[0]);
            $o->mime   = $mime[1];
        }

        return $o;
    }

    public function contacto()
    {
        if ($_POST) {
            $data["from"]    = $this->input->post("email");
            $data["name"]    = $this->input->post("nombre");
            $data["rol"]     = $this->input->post("soy");
            $data["reason"]  = $this->input->post("motivo") == 1 ? "" : $this->input->post("motivo");
            $data["comment"] = $this->input->post("comentario");
            $data["correo"]  = "info@japy.mx";

            $this->mailer->to("info@japy.mx", "Informacion");
            $this->mailer->setSubject("Contacto");
            $template = $this->load->view("templates/email_usuario_contacto", $data, true);

            if ($this->mailer->send($template)) {
                $this->session->set_flashdata("tipo_mensaje", "success");
                $this->session->set_flashdata("mensaje", "El correo se envió con éxito");
            } else {
                $this->session->set_flashdata("tipo_mensaje", "danger");
                $this->session->set_flashdata("mensaje", "No fue posible enviar el correo");
            }

            redirect("inicio/contacto");
        }

        $this->load->view("principal/newHeader");
        $this->load->view("contacto/index");
        $this->load->view("principal/footer");
    }

    public function condicionesLegales()
    {
        $this->load->view('principal/newHeader');
        $this->load->view('condiciones/index');
        $this->load->view('principal/footer');
    }

    public function altaEmpresas()
    {
        $data['active'] = 1;
        $this->load->view('japy/header');
        $this->load->view('alta/empresas');
        $this->load->view('japy/footer');
    }

    public function visitaGuiada()
    {
        $data['active'] = 2;
        $this->load->view('alta/header', $data);
        $this->load->view('alta/visitaGuiada');
        $this->load->view('principal/footer');
    }

    public function mi_escaparate()
    {
        $data['active'] = 2;
        $this->load->view('alta/header', $data);
        $this->load->view('alta/miEscaparate');
        $this->load->view('principal/footer');
    }

    public function mis_recomendaciones()
    {
        $data['active'] = 2;
        $this->load->view('alta/header', $data);
        $this->load->view('alta/misRecomendaciones');
        $this->load->view('principal/footer');
    }

    public function misPromociones()
    {
        $data['active'] = 2;
        $this->load->view('alta/header', $data);
        $this->load->view('alta/misPromociones');
        $this->load->view('principal/footer');
    }

    public function misEventos()
    {
        $data['active'] = 2;
        $this->load->view('alta/header', $data);
        $this->load->view('alta/misEventos');
        $this->load->view('principal/footer');
    }

    public function misSolicitudes()
    {
        $data['active'] = 2;
        $this->load->view('alta/header', $data);
        $this->load->view('alta/misSolicitudes');
        $this->load->view('principal/footer');
    }

    public function misEstadisticas()
    {
        $data['active'] = 2;
        $this->load->view('alta/header', $data);
        $this->load->view('alta/misEstadisticas');
        $this->load->view('principal/footer');
    }

    public function serviciosPremium()
    {
        $data['active'] = 3;
        $this->load->view('alta/header', $data);
        $this->load->view('alta/serviciosPremium');
        $this->load->view('principal/footer');
    }

    /*
     *
     * SECCIONES INFORMATIVAS. para NOVIA
     *
     */

   public function planeador_bodas() 
    {
        $this->load->view('conocenos/conocenos.php');
    }
    public function planeador()
    {
        $this->load->view('conocenos/planeador.php');
    }

    public function agenda()
    {
        $this->load->view('conocenos/agenda.php');
    }

    public function invitados()
    {
        $this->load->view('conocenos/invitados.php');
    }

    public function control_mesas()
    {
        $this->load->view('conocenos/control_mesas.php');
    }

    public function proveedores()
    {
        $this->load->view('conocenos/proveedores.php');
    }

    public function presupuestador()
    {
        $this->load->view('conocenos/presupuestador.php');
    }

    public function vestidos()
    {
        $this->load->view('conocenos/vestidos.php');
    }

    public function portal_web()
    {
        $this->load->view('principal/novia/informativa/portal_web.php');
    }

    private function redireccionar()
    {
        $id_rol = $this->session->userdata("rol");
        $user   = User::find($this->session->userdata("id_usuario"));

        switch ($id_rol) {
            case Checker::$ADMIN:
                redirect("App/");
                break;
            case Checker::$NOVIO:
                if (isset($_GET['callback'])) {
                    $callback = $this->input->get("callback");
                    redirect($callback, "refresh");
                } else {
                    // redirect("novios/presupuesto", "refresh");
                }
                break;
            case Checker::$REDACTOR:
                redirect("blog/");
                break;
            case Checker::$PROVEEDOR:
                if (isset($_GET['callback'])) {
                    $callback = $this->input->get("callback");
                    redirect($callback, "refresh");
                } else {
                    redirect("proveedor/");
                }
                break;
            case Checker::$ADMINAPP:
                redirect("App/");
                break;
            case Checker::$MODERADOR:
                redirect("");
                break;
            case Checker::$GALERIA:
                redirect("ADGaleria/");
                break;
        }
    }
}
