<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;
use Models\Tag;
use Models\Gallery;

class Inspiracion extends MY_Controller
{

    public $input;

    public function __construct()
    {
        parent::__construct();
        // $this->input = new MY_Input();
        $this->load->library("checker");
        // $this->load->library("task");
    }

    public function index()
    {
        // $prueba = self::saveInspirations();
        $this->load->view("inspiracion/index");
    }

    public function start(){
        $inspirations['new'] = Inspiration::latest("created_at")->take(3)->get();
        foreach ($inspirations['new'] as $k => $v) {
            $inspirations['new'][$k]['hover'] = false;
            $inspirations['new'][$k]['comments'] = Coment::where('id_inspiracion',$v['id_inspiracion'])->count();
        }
        $inspirations['last'] = Inspiration::inRandomOrder()->take(6)->get();
        foreach ($inspirations['last'] as $k => $v) {
            $inspirations['last'][$k]['hover'] = false;
            $inspirations['last'][$k]['comments'] = Coment::where('id_inspiracion',$v['id_inspiracion'])->count();
        }
        return $this->output->set_status_header(200)
            ->set_output(json_encode($inspirations));
    }

    public function tags(){
        $term = $this->input->get("term");
        $tags = Tag::select('title')->where('title', 'like', '%'.$term.'%')->get();
        $aux = [];
        foreach ($tags as $k => $v) {
            $aux[$k] = $v['title'];
        }
        $tags = $aux;
        unset($aux);
        return $this->output->set_status_header(200)
            ->set_output(json_encode($tags));
    }

    public function getInspirations(){
        $search = $this->input->get("search");
        $inspirations=Inspiration::whereHas('tags',function($query) use ($search)
        {
            $query->where('title',$search);
        })->get();
        foreach ($inspirations as $k => $v) {
            $inspirations[$k]['hover'] = false;
            $inspirations[$k]['comments'] = Coment::where('id_inspiracion',$v['id_inspiracion'])->count();
        }
        return $this->output->set_status_header(200)
            ->set_output(json_encode($inspirations));
    }
    
    public function modalInfo() {
        if($_POST) {
            $id = $this->input->post("id_ins");
            $views = $this->input->post("views");
            $info = Inspiration::with('tags')->where('id_inspiracion', $id)->first();
            if($views==1) {
                $info->vistas = $info->vistas + 1;
                $info->save();
                $info = Inspiration::with('tags')->where('id_inspiracion', $id)->first();
            }
            $comentarios = Coment::where('id_inspiracion',$id)->get();
            $data="";
            $data2="";
            $tam = sizeOf($comentarios);
            $i=0;
            for($i; $i < $tam; $i++) {
                $data[$i]["names"] = $comentarios[$i]['nombre_cliente'];
                $data[$i]["texto"] = $comentarios[$i]['comentario'];
            }
            $tam2 = sizeOf($info['relations']['tags']);
            for($i=0; $i < $tam2; $i++) {
                $data2[$i]['tags'] = $info['relations']['tags'][$i]['title'];
            }
            $foto = Gallery::where('id_proveedor', $info->id_proveedor)->where('logo', 1)
            ->where('principal', 0)->first();
            $nomProveedor = Provider::where('id_proveedor', $info->id_proveedor)->first();
            
            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['inspiracion' => $info, 'comentarios' => $data,
                     'fotoInfo' => $foto->nombre, 'nomproInfo' => $nomProveedor->nombre,
                     'tags' => $data2]));
        }
    }

    public function saveComent() {
        $id = $this->input->get("id");
        $coment = $this->input->get("coment");
        $id_user = $this->input->get("id_user");
        $name = $this->input->get("name");
        $comentexist = Coment::where('id_cliente', $id_user)->where('id_inspiracion', $id)->first();
        if(sizeOf($comentexist)==0) {
            $newComent = Coment::create([
                'id_inspiracion' =>  $id,
                'id_cliente' => $id_user,
                'nombre_cliente' => $name,
                'comentario' => $coment
            ]);
            $validar = 'no coment';
        } else {
            $validar = 'coment';
        }
        return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['validar' => $validar]));
    }

    public function saveInspirations() {
        $iduser = $this->input->get("id_usuario");
        // $iduser = 8203;
        $idinspiration = $this->input->get("id_inspiracion");
        // $idinspiration = 113;
        DB::table('inspiracion_usuarios')->insert ([
            ['id_usuario' => $iduser, 'id_inspiracion' => $idinspiration]
        ]);
    }
}
