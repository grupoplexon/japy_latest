<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Models\Blog\Post as BlogPost;
use Illuminate\Database\Capsule\Manager as DB;
use Models\Tag;

class Magazine extends MY_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Blog/Categoria_model", "categoria");
        $this->load->model("Blog/Post_model", "post");
        $this->load->model("Blog/Comentario_model", "comentario");
        $this->load->model("Blog/Favorito_model", "fav");
        $this->load->model("Login_model", "usuario");
        $this->load->helper("formats");
        $this->input = new MY_Input();
    }

    public function index() {

        $tipo = is_null($this->input->get('tipo')) ? null : $this->input->get('tipo');     
        
        if($tipo!=null){
            $c=  DB::table('blog_post_categoria')->where('slug',$tipo)->first();
            $tipo=$c->id_categoria;
            // dd($tipo);
        }else{
            $tipo=0;
        }

        $cat = 0;
        $page = 1;

        $wantsJson    = $this->input->get_request_header('accept', true) === "application/json";
        $data["cat"]  = $cat;
        $data["page"] = $page;
        // $data["grupo_cat"]  = $this->grupo->getAll();
        $data["categorias"] = $this->categoria->getAll();

        if ($cat != 0) {
            $data["categoria"] = $this->categoria->get($cat);
        }

        $total_paginas = ceil($this->post->countCat($tipo) / 5);

        if ($page <= $total_paginas) {
            $limit1 = 5;
            $limit2 = ($page > 0 ? $page - 1 : $page) * 5;

            $data["total_paginas"] = $total_paginas;
            $data["posts"]         = $this->post->getListCategoria($tipo, $limit1, $limit2);

            foreach ($data["posts"] as &$post) {
                $post->autor       = $this->post->getAutor($post->id_autor);
                $post->tags        = BlogPost::with("tags")->find($post->id_blog_post);//$this->post->getTags($post->id_blog_post);
                $post->fav         = $this->fav->countPost($post->id_blog_post);
                $post->comentarios = $this->comentario->countPost($post->id_blog_post);
                if($post->tipo!=null){
                    $categoria   = DB::table('blog_post_categoria')->where('id_categoria',$post->tipo)->first();
                    $post->categoria = $categoria->name_category;
                }else{
                    $post->categoria=null;
                }
                
            }
        } else {
            $data["post"] = null;
        }
        if($tipo==0){
            $data["destacados"]       = $this->post->getListCategoria(0, 6, 0);
        }

        $data["categories"]=DB::table('blog_post_categoria')->get();

        // $this->load->view('magazine/index', $data);
        $this->load->view('magazine/magazine', $data);
    }

    public function blog($id_post = null, $social = null)
    {
        $data["id_post"]     = $id_post;
        $data["post"]        = $this->post->get($id_post);
        $data["post"]->tags  = BlogPost::with("tags")->find($id_post);
        $data["post"]->autor = $this->post->getAutor($data["post"]->id_autor);

        $data["destacados"] = DB::table('blog_post')->where('id_blog_post', '!=', $data['post']->id_blog_post )->where('tipo', $data['post']->tipo)->take(3)->get();
        
        $data["categories"]=DB::table('blog_post_categoria')->get();

        $this->load->view('magazine/blog', $data);
    }


    public function maquetar()
    {
        $this->load->view('magazine/maquetar');
    }

    public function tags(){
        $term = $this->input->get("term");
        $tags = Tag::select('title')->where('title', 'like', '%'.$term.'%')->get();
        $aux = [];
        foreach ($tags as $k => $v) {
            $aux[$k] = $v['title'];
        }
        $tags = $aux;
        unset($aux);
        return $this->output->set_status_header(200)
            ->set_output(json_encode($tags));
    }

    // public function getInspirations(){
    //     $search = $this->input->get("search");
    //     $inspirations=Inspiration::whereHas('tags',function($query) use ($search)
    //     {
    //         $query->where('title',$search);
    //     })->get();
    //     foreach ($inspirations as $k => $v) {
    //         $inspirations[$k]['hover'] = false;
    //         $inspirations[$k]['comments'] = Coment::where('id_inspiracion',$v['id_inspiracion'])->count();
    //     }
    //     return $this->output->set_status_header(200)
    //         ->set_output(json_encode($inspirations));
    // }

    public function buscar(){

        $search = $this->input->get("buscar");

        $data["total_paginas"] = 0;
        $data["cat"]  = 0;

        $data["posts"]=BlogPost::whereHas('tags',function($query) use ($search)
        {
            $query->where('title',$search);
        })->get();

        foreach ($data["posts"] as &$post) {
            $post->autor       = $this->post->getAutor($post->id_autor);
            $post->tags        = BlogPost::with("tags")->find($post->id_blog_post);//$this->post->getTags($post->id_blog_post);
            $post->fav         = $this->fav->countPost($post->id_blog_post);
            $post->comentarios = $this->comentario->countPost($post->id_blog_post);
        }

        $this->load->view('magazine/index', $data);
    }
}