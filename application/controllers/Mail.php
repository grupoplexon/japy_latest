<?php
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;
class Mail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("My_PHPMailer");
        $this->mailer = $this->my_phpmailer;
        $this->load->model("Correo/Correo_model", "correo");
    }
    //Sends and email to providers with their weekly indicators
    public function indicators()
    {
        return 0;
        $providersIndicators = [];

        $providers = Provider::with([
            'indicators' => function ($query) {
                return $query->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()]);
            },
            'user',
        ])->whereHas('user', function ($query) {
            return $query->where('activo', 1);
        })->get();
        $providers = DB::table("proveedor")
            ->whereExists(function ($query) {
                return $query->select(DB::raw(1))
                    ->from("usuario")
                    ->whereRaw("proveedor.id_usuario = usuario.id_usuario")
                    ->where("activo", 1);
            })
            ->join("indicators", "proveedor.id_proveedor", "indicators.provider_id")
            ->get();
        dd(Provider::get());
        dd($providers->first());
        $indicators = DB::table("indicators")
            ->whereBetween("created_at", [Carbon::now()->subWeek(), Carbon::now()])
            ->whereExists(function ($query) {
                return $query->select(DB::raw(1))
                    ->from("proveedor")
                    ->whereRaw("indicators.provider_id = proveedor.id_proveedor")
                    ->whereExists(function ($query) {
                        return $query->select(DB::raw(1))
                            ->from("usuario")
                            ->whereRaw("proveedor.id_usuario = usuario.id_usuario")
                            ->where("activo", 1);
                    });
            })->get();
        dd($indicators);
        foreach ($providers as $provider) {
            $providerIndicator = null;
            foreach ($provider->indicators as $user) {
                $providerIndicator['email'] = $provider->user->correo;
                $providerIndicator['name']  = $provider->user->nombre;
                $indicatorType              = $user->pivot->type;
                $indicatorPivot             = $user->pivot;
                $contactRequestCount        = 0;

                isset($providerIndicator['indicators'][$indicatorType]['amount'])
                    ? $providerIndicator['indicators'][$indicatorType]['amount']++
                    :
                    $providerIndicator['indicators'][$indicatorType]['amount'] = 1;

                if ($indicatorType == 'contact_request') {
                    $contactRequestCount++;
                    if ( ! is_null($indicatorPivot->handled_at)) {
                        $handledAt = Carbon::createFromFormat('Y-m-d H:i:s', $indicatorPivot->handled_at);
                        $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $indicatorPivot->created_at);
                        $timeTaken = $handledAt->diffInMinutes($createdAt);
                        if (isset($providerIndicator['indicators']['contact_average_time_request']['amount'])) {
                            $averageTime = $providerIndicator['indicators']['contact_average_time_request']['amount'];
                            $averageTime = ($averageTime + $timeTaken) / $contactRequestCount;
                        } else {
                            $averageTime = $timeTaken;
                        }
                        $providerIndicator['indicators']['contact_average_time_request']['amount'] = $averageTime;
                    } else {
                        isset($providerIndicator['indicators']['contact_pending_request']['amount'])
                            ? $providerIndicator['indicators']['contact_pending_request']['amount']++
                            :
                            $providerIndicator['indicators']['contact_pending_request']['amount'] = 1;
                    }
                }
            }
            if ( ! is_null($providerIndicator) && $providerIndicator['email'] != '') {
                $providersIndicators[] = $providerIndicator;
            }
        }
        foreach ($providersIndicators as $providerIndicator) {
            $this->mailer->to($providerIndicator['email'], $providerIndicator['name']);
            $providerIndicator['indicators'] = $this->buildIndicator($providerIndicator['indicators']);
            $template                        = $this->load->view("templates/email_indicators", [
                'indicators' => $providerIndicator['indicators'],
            ], true);

            $this->mailer->send($template);
        }
    }
    private function buildIndicator($indicators)
    {
        foreach ($indicators as $key => $indicator) {
            switch ($key) {
                case 'contact_request':
                    $indicator['image']   = 'mail.png';
                    $indicator['message'] = 'Solicitudes recibidas';
                    break;
                case 'contact_pending_request':
                    $indicator['image']   = 'mail.png';
                    $indicator['message'] = 'Solicitudes pendientes';
                    break;
                case 'contact_average_time_request':
                    $indicator['image']   = 'mail.png';
                    $indicator['message'] = 'Tu tiempo promedio de respuesta (minutos)';
                    break;
                case 'visit':
                    $indicator['image']   = 'group-profile-users.png';
                    $indicator['message'] = 'Visitas';
                    break;
                case 'phone':
                    $indicator['image']   = 'phone-receiver.png';
                    $indicator['message'] = 'Personas vieron tu telefono';
                    break;
                case 'recommendation':
                    $indicator['image']   = 'thumbs-up.png';
                    $indicator['message'] = 'Personas te recomendaron';
                    break;
                case 'rating':
                    $indicator['image']   = 'star.png';
                    $indicator['message'] = 'Personas te calificaron';
                    break;
                case 'appearance_category':
                    $indicator['image']   = 'search.png';
                    $indicator['message'] = 'Apariciones en categoria';
                    break;
                case 'appearance_search':
                    $indicator['image']   = 'search.png';
                    $indicator['message'] = 'Apariciones en busqueda';
                    break;
            }
            $indicators[$key] = $indicator;
        }
        return $indicators;
    }
    //Notifications for future weddings
    public function notifications()
    {
        $weddings = [];
        //12meses,1semana,2meses,2semanas,4meses,7meses,
        $wedding = Wedding::with([
            "client.user" => function ($query) {
                return $query->select(["id_usuario"]);
            },
        ])->where("id_boda", 194);
        $weddings = [
            "months" => [
                "2"  => (clone $wedding)->whereDate("fecha_boda", Carbon::now()->addMonths(2)->toDateString())->get(),
                "4"  => (clone $wedding)->whereDate("fecha_boda", Carbon::now()->addMonths(4)->toDateString())->get(),
                "7"  => (clone $wedding)->whereDate("fecha_boda", Carbon::now()->addMonths(7)->toDateString())->get(),
                "12" => (clone $wedding)->whereDate("fecha_boda", Carbon::now()->addMonths(12)->toDateString())->get(),
            ],
            "weeks"  => [
                "1" => (clone $wedding)->whereDate("fecha_boda", Carbon::now()->addWeeks(1)->toDateString())->get(),
                "2" => (clone $wedding)->whereDate("fecha_boda", Carbon::now()->addWeeks(2)->toDateString())->get(),
            ],
        ];
        //Key is the month
        foreach ($weddings["months"] as $key => $month) {
            foreach ($month as $wedding) {
                $this->correo->sendWeddingReminder($wedding->client->user->id_usuario, $key."_months");
            }
        }
        foreach ($weddings["weeks"] as $key => $month) {
            foreach ($month as $wedding) {
                $this->correo->sendWeddingReminder($wedding->client->user->id_usuario, $key."_weeks");
            }
        }
    }

}