<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Novia extends MY_Controller
{
    protected function middleware()
    {
        return ['user_auth|except:updatePorcentajes'];
    }
    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->library("task");
        $this->load->library("Categoria");
        $this->load->helper("formats");
        $this->load->model("Tarea_model", "tarea");
        $this->load->model("Boda_model", "boda");
        $this->load->model("Invitados/Mesa_model", "mesa");
        $this->load->model("Proveedor/ProveedorBoda_model", "ProveedorBoda_model");
        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Invitados/Invitado_model", "invitado");
        $this->load->model("Cliente_model", "cliente");
        $this->load->model("Presupuesto_model", "porcentaje");
        $this->load->model("Comunidad/Comunidad_model", "comunidad");
        $this->load->model("DefaultPorcentaje_model", 'default_porcentaje');
    }
    public function index()
    {
        $id_boda = $this->session->userdata("id_boda");
        $data["boda"] = $this->boda->get($id_boda);
        $data["usuario"] = $this->usuario->get($this->session->userdata("id_usuario"));
        $data["cliente"] = $this->cliente->get($this->session->userdata("id_cliente"));
        $data["presupuesto"] = $this->boda->getPresupuesto($id_boda);
        $data["pendientes"] = $this->boda->getTareasPendientes($id_boda);
        $data["tareas_prox"] = $this->tarea->getProximas($id_boda);
        $data["confirmados"] = $this->invitado->getConfirmados();
        $data["proveedores"] = $this->ProveedorBoda_model->proveedores_boda()->proveedores;
        $data["mesas"] = is_null($this->mesa->getCountMesa()) ? 0 : $this->mesa->getCountMesa()->total;
        $data['debates'] = $this->comunidad->debates_perfil();
        $grupo = $this->invitado->getGrupoNovios($id_boda);

        if ($grupo) {
            $data["novios"] = $this->invitado->getList(["id_boda" => $id_boda, "id_grupo" => $grupo->id_grupo]);
        }
        $this->load->view("principal/newheadernovia");
        $this->load->view("principal/novia/menu");
        $this->load->view("principal/novia/miperfil/index", $data);
        $this->load->view("japy/prueba/footer");
    }

    public function updatePorcentajes()
    {
        if ($_POST) {
            $categorias = Category::where('parent_id', 1)->get();
            $category = $this->input->post('categorias');
            $presupuesto = $this->input->post('presupuesto', true, 'Decimal', ['min' => 0]);
            $invitados = $this->input->post('invitados', true, 'Integer', ['min' => 1]);
            
            // dd($categorias);
            //Cambiar el envio de categorias de nombre a ID
            if ($this->input->isValid()) {
                $id_boda = $this->session->userdata('id_boda');

                $categories = json_decode($categorias);
                
                $wedding = Wedding::find($id_boda);
                if ($wedding) {
                    $wedding->update(['presupuesto' => $presupuesto, 'no_invitado' => $invitados, 'first' => 1]);

                    foreach ($categories as $category) {
                       
                        dd($wedding->budgets()->where('category_id', $category->id)->get());
                        $wedding->budgets()->updateOrCreate(
                            ['category_id' => $category->id, 'nombre' => $category->name],
                            [
                                'nombre' => $category->name,
                                'category_id' => $category->id,
                                'porcentaje' => $category->percentage,
                                'costo_aproximado' => $presupuesto * ($category->percentage / 100),
                            ]);
                    }
                }
                return $this->output->set_content_type('application/json')
                    ->set_status_header(202)
                    ->set_output(json_encode(['success' => true, 'data' => 'ok']));
            }
        }
        return $this->output->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(['text' => 'Error 404', 'type' => 'danger']));
    }

    public function firstDate()
    {
        if ($_POST) {
            $id = $this->session->userdata("id_boda");
            $fecha = $this->input->post("fecha");
            $datos = [
                "nombre" => $this->input->post("nombre"),
                "apellido" => $this->input->post("apellido"),
            ];
            if ($datos && $fecha) {
                Wedding::where('id_boda', $id)->update(['fecha_boda' => $fecha]);
                $this->invitado->setDateFiance($id, $datos);

                return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(["success" => true, "data" => "ok"]));
            }
        }
        return $this->output->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(['text' => 'Error 404', 'type' => 'danger']));
    }

}