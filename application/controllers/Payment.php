<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Payment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->output->set_content_type("application/json");
    }

    public function pay()
    {
        $error                 = false;
        $user                  = User::with("provider")->find($this->input->post("user_id"));
        $token                 = $this->input->post("token");
        $plan                  = $this->input->post("plan");
        $period                = $this->input->post("period");
        $paymentMethod         = $this->input->post("payment_method");
        $monthsWithoutInterest = $this->input->post("months_without_interest");

        if ( ! $user) {
            $response["code"]    = 401;
            $response["message"] = "Necesitar iniciar sesion";
            $error               = true;
        }

        if ( ! $user->provider) {
            $response["code"]    = 403;
            $response["message"] = "Esta funcion es solo para proveedores";
            $error               = true;
        }

        $provider = $user->provider;

        //Creating conekta customer
        if ( ! $error && empty($provider->conekta_customer_id)) {
            $conektaResponse = $provider->createCustomer();

            if ( ! $conektaResponse["success"]) {
                $response["code"]    = 500;
                $response["message"] = $conektaResponse["message"];

                $error = true;
            }
        }

        if ($error) {
            return $this->output->set_status_header($response["code"])
                ->set_output(json_encode($response));
        } else {
            $providerCanPay = true;
            if ($paymentMethod == "card") {
                if ( ! $provider->hasPaymentMethod()) {
                    $conektaResponse = $provider->addCustomerCard($token);
                    $providerCanPay  = $conektaResponse["success"];
                } elseif ($token) {
                    //We delete the current card and add a new one
                    $conektaResponse = $provider->deleteCustomerCard();
                    $conektaResponse = $provider->addCustomerCard($token);
                    $providerCanPay  = $conektaResponse["success"];
                }
            }

            if ($providerCanPay) {
                $conektaResponse = $provider->buyPlan($plan, $period, $paymentMethod, $monthsWithoutInterest);
            }
        }

        $response["message"] = $conektaResponse["message"];
        $response["code"]    = $conektaResponse["success"] ? 200 : 500;
        $response["data"]    = isset($conektaResponse["data"]) ? $conektaResponse["data"] : "";

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function webhook()
    {
        $body        = @file_get_contents("php://input");
        $conektaData = json_decode($body);
        file_put_contents("conekta.json", json_encode($conektaData));

        switch ($conektaData->type) {
            case "order.paid":
                if (isset($conektaData->data)) {
                    $metadata     = $conektaData->data->object->metadata;
                    $customerInfo = $conektaData->data->object->customer_info;

                    $planId                = $metadata->planId;
                    $period                = $metadata->period;
                    $monthsWithoutInterest = $metadata->monthsWithoutInterest;
                    $customerId            = $customerInfo->customer_id;
                    $provider              = Provider::where("conekta_customer_id", $customerId)->first();

                    if ($provider) {
                        $provider->updatePlan($planId, $period, $monthsWithoutInterest);
                    }
                }
                break;
            case "charge.chargeback.created":
            case "order.refunded":
                if (isset($conektaData->data)) {
                    $metadata     = $conektaData->data->object->metadata;
                    $customerInfo = $conektaData->data->object->customer_info;

                    $customerId            = $customerInfo->customer_id;
                    $monthsWithoutInterest = $metadata->monthsWithoutInterest;
                    $provider              = Provider::where("conekta_customer_id", $customerId)->first();

                    if ($provider) {
                        $provider->updatePlan(0, null, $monthsWithoutInterest);
                    }
                }
                break;
        }
        http_response_code(200);
    }

    public function cronjob()
    {
        $providers = Provider::whereNotNull("plan_expiration_date")
            ->where("plan_expiration_date", "<=", Carbon::now())
            ->get()
            ->take(20);

        $plans                      = [1 => "silver", 2 => "gold", 3 => "diamond"];
        $conektaResponse["success"] = false;

        foreach ($providers as $provider) {
            $planExpirationDate  = Carbon::createFromFormat("Y-m-d H:i:s", $provider->plan_expiration_date);
            $trialExpirationDate = Carbon::createFromFormat("Y-m-d H:i:s", $provider->trial_expiration_date);

            if ($planExpirationDate->isPast() && $trialExpirationDate->isPast()) {
                if ($provider->hasPaymentMethod() && $provider->tipo_cuenta > 0) {
                    $conektaResponse = $provider->buyPlan($plans[$provider->tipo_cuenta], $provider->plan_months, "card");
                }

                if ( ! $conektaResponse["success"]) {
                    $provider->updatePlan(0, null);
                }
            }
        }
    }

    public function getQuote()
    {
        $response["code"] = 200;

        $user    = User::with("provider")->find($this->input->post("user_id"));
        $period  = $this->input->post("period");
        $plan    = $this->input->post("plan");
        $plans   = ["silver", "gold", "diamond"];
        $periods = [6, 12];

        if ( ! $user) {
            $response["message"] = "Necesitar iniciar sesion";
            $response["code"]    = 401;
        }

        if ( ! $user->provider) {
            $response["message"] = "Esta funcion es solo para proveedores";
            $response["code"]    = 403;
        }

        if ( ! in_array($period, $periods)) {
            $response["message"] = "Seleccionaste un periodo inexistente";
            $response["code"]    = 422;
        }

        if ( ! in_array($plan, $plans)) {
            $response["message"] = "Seleccionaste un plan inexistente";
            $response["code"]    = 422;
        }

        $provider = $user->provider;

        if ($response["code"] == 200) {
            $response["data"]["amount"] = $provider->calculatePlanTotal($plan, $period);
            $response["data"]["remaining_months"] = $provider->getPlanRemaining()["remaining_months"];
            $response["data"]["remaining_days"] = $provider->getPlanRemaining()["remaining_days"];
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

}