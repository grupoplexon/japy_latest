<?php

class Perfil extends CI_Controller
{
    public $input;

    public function __construct()
    {
        $this->input = new MY_Input();
        parent::__construct();
        $this->load->library("checker");
        $this->load->library("session");
        $this->load->library("facebook");
        $this->load->helper("url");
        $this->load->model("Login_model", "usuario");
        $this->load->model("Cliente_model", "cliente");
        $this->load->model("Proveedor/Archivo_model", "archivo");
        $this->load->model("Proveedor_model", "proveedor");
    }

    public function foto($id_usuario)
    {
        $usuario = $this->usuario->getUsuario($id_usuario);
        if ($usuario) {
            if ($usuario->rol == Checker::$PROVEEDOR) {
                $p = $this->proveedor->get(array("id_usuario" => $usuario->id_usuario));
                $logo = $this->proveedor->getLogo($p->id_proveedor);
                if ($logo) {
                    return $this->output->set_content_type($logo->mime)
                        ->set_status_header(200)
                        ->set_output($logo->data);
                }
                return redirect($this->config->base_url() . "dist/img/blog/perfil.png");
            } else if ($usuario->foto) {
                return $this->output->set_content_type("image/jpg")
                    ->set_status_header(200)
                    ->set_output($usuario->foto);
            } else {
                $c = $this->cliente->get(array("id_usuario" => $id_usuario));
                if ($c->id_facebook != null) {
                    return redirect($this->facebook->get_profile_pic($c->id_facebook));
                } else {
                    return redirect($this->config->base_url() . "dist/img/blog/perfil.png");
                }
            }

        }
        return $this->output->set_status_header(404);
    }

}
