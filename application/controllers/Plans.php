<?php

class Plans extends CI_Controller
{
    public $input;
    protected $openpay;

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");

        Openpay::setId('mmlgnvqylu1kdtdgflj7');
        Openpay::setApiKey('sk_195885c4387e424fa023b7bddda8d2f8');
        Openpay::setProductionMode(false);
        $this->openpay = Openpay::getInstance('mmlgnvqylu1kdtdgflj7', 'sk_195885c4387e424fa023b7bddda8d2f8');
    }

    public function index()
    {
        $this->load->view("plans/index");
    }

    public function pay()
    {
        $this->output->set_content_type("application/json");

        $response = [
            "code"    => 500,
            "data"    => [],
            "message" => "error",
        ];

        $user = User::find($this->session->userdata("id_usuario"));

        $plans = [
            1 => 500,
            2 => 1000,
            3 => 2000,
        ];

        $customer = [
            'name'  => $this->input->post("cardHolder"),
            'email' => User::find($this->session->userdata("id_usuario"))->correo,
        ];

        $chargeData = [
            'method'            => 'card',
            'source_id'         => $this->input->post("token"),
            'amount'            => (float)($plans[$this->input->post("plan")["id"]] * $this->input->post("plan")["months"]),
            'description'       => "Plan de Japy",
            'device_session_id' => $this->input->post("deviceId"),
            'customer'          => $customer,
        ];

        $charge = $this->openpay->charges->create($chargeData);

        $user->payments()->create([
            "reference" => $charge->id,
        ]);

        return $this->output->set_status_header($response['code'])
            ->set_output(json_encode($response));
    }

}

