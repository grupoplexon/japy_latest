<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class Promociones extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->helper("formats");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->library("My_PHPMailer");
        $this->mailer = $this->my_phpmailer;
    }
    public function index()
    {
        $data['promociones'] = Promotion::whereHas('provider', function ($query) {
            return $query->has('categories');
        })->where('fecha_fin', '>', Carbon::now())->get();
        
        $data["providers"]= $this->proveedor->getAll();

        $this->load->view("principal/promocion/promociones",$data);
    }

    public function sector($sector = null, $estado = null, $ciudad = null)
    {
        $filterState = is_null($this->input->get('estado')) ? 'Jalisco' : $this->input->get('estado');
        $estado = ($this->input->get('estado') == "Todos") ? null : $filterState;
        $search      = is_null($this->input->get('categoria')) ? null : $this->input->get('categoria');

        $category    = Category::where("slug", $search)->first();

        if ( ! $category) {
            if($estado != null){

                $providers = DB::table('promocion')
                ->join('proveedor', 'promocion.id_proveedor', '=', 'proveedor.id_proveedor')
                ->select('promocion.*', 'proveedor.nombre AS nombre_prov', 'proveedor.tipo_cuenta')
                ->where('promocion.fecha_fin','>',Carbon::now())
                ->where('proveedor.localizacion_estado','=', $estado )
                ->get();

                $data['promociones'] = $providers->sortByDesc('tipo_cuenta');
            }else{
                $providers = DB::table('promocion')
                ->join('proveedor', 'promocion.id_proveedor', '=', 'proveedor.id_proveedor')
                ->select('promocion.*', 'proveedor.nombre AS nombre_prov', 'proveedor.tipo_cuenta')
                ->where('promocion.fecha_fin','>',Carbon::now())
                ->get();

                $data['promociones'] = $providers->sortByDesc('tipo_cuenta');
            }
        }else{
            if($estado != null){

                $data['promociones'] = DB::table('proveedor_categoria')
                ->join('categoria_proveedor', 'categoria_proveedor.categoria_id' , 'proveedor_categoria.id')
                ->join('proveedor', 'categoria_proveedor.proveedor_id', '=', 'proveedor.id_proveedor')
                ->join('promocion', 'promocion.id_proveedor', '=', 'proveedor.id_proveedor')
                ->select('promocion.*', 'proveedor.nombre AS nombre_prov', 'proveedor.tipo_cuenta')
                ->where('proveedor.localizacion_estado','=', $estado )
                ->where('proveedor_categoria.parent_id','=', $category->id )
                ->where('promocion.fecha_fin','>',Carbon::now())
                ->get();
    
            }else{
    
                $data['promociones'] = DB::table('proveedor_categoria')
                ->join('categoria_proveedor', 'categoria_proveedor.categoria_id' , 'proveedor_categoria.id')
                ->join('proveedor', 'categoria_proveedor.proveedor_id', '=', 'proveedor.id_proveedor')
                ->join('promocion', 'promocion.id_proveedor', '=', 'proveedor.id_proveedor')
                ->select('promocion.*', 'proveedor.nombre AS nombre_prov', 'proveedor.tipo_cuenta')
                ->where('proveedor_categoria.parent_id','=', $category->id )
                ->where('promocion.fecha_fin','>',Carbon::now())
                ->get();
            }
        }

        $this->load->view("principal/promocion/promociones",$data);
    }

    public function categoria($slug = null, $estado = null, $ciudad = null)
    {
        $filterState = is_null($this->input->get('estado')) ? 'Jalisco' : $this->input->get('estado');
        $estado = ($this->input->get('estado') == "Todos") ? null : $filterState;
    

        $category = Category::where('slug', $slug)->first();

        if($estado != null){

            $data['promociones'] = DB::table('proveedor_categoria')
            ->join('categoria_proveedor', 'categoria_proveedor.categoria_id' , 'proveedor_categoria.id')
            ->join('proveedor', 'categoria_proveedor.proveedor_id', '=', 'proveedor.id_proveedor')
            ->join('promocion', 'promocion.id_proveedor', '=', 'proveedor.id_proveedor')
            ->select('promocion.*', 'proveedor.nombre AS nombre_prov', 'proveedor.tipo_cuenta')
            ->where('proveedor.localizacion_estado','=', $estado )
            ->where('proveedor_categoria.parent_id','=', $category->id )
            ->where('promocion.fecha_fin','>',Carbon::now())
            ->get();

        }else{

            $data['promociones'] = DB::table('proveedor_categoria')
            ->join('categoria_proveedor', 'categoria_proveedor.categoria_id' , 'proveedor_categoria.id')
            ->join('proveedor', 'categoria_proveedor.proveedor_id', '=', 'proveedor.id_proveedor')
            ->join('promocion', 'promocion.id_proveedor', '=', 'proveedor.id_proveedor')
            ->select('promocion.*', 'proveedor.nombre AS nombre_prov', 'proveedor.tipo_cuenta')
            ->where('proveedor_categoria.parent_id','=', $category->id )
            ->where('promocion.fecha_fin','>',Carbon::now())
            ->get();
        }
        $this->load->view("principal/promocion/promociones",$data);
    }
}