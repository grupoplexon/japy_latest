<?php

use PhpOffice\PhpSpreadsheet\IOFactory;
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Proveedores extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->model("Proveedor/Promocion_model", "promocion");
        $this->load->model("Proveedor/Colaborador_model", "colaborador");
        $this->load->model("Proveedor/FAQ_model", "faq");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->model("Proveedor/TipoProveedor_model", "tipo");
        $this->load->model("Proveedor/Solicitud_model", "solicitud");
        $this->load->model("Proveedor/Archivo_model", "archivo");
        $this->load->model("Correo/Adjunto_model", "adjunto");
        $this->load->model("Correo/Correo_model", "correo");
        $this->load->model('Cliente_model');
        $this->load->model('Boda_model');
        $this->load->library("Checker");
        $this->load->library("Categoria");
        $this->load->helper("formats");
        $this->load->helper("collection_paginate");
        ini_set("memory_limit", "-1");
    }
    public function index()
    {
        // $category = DB::table('proveedor_categoria')->inRandomOrder()->limit(3)->get();
        // $categories['name'] = DB::table('proveedor_categoria')->inRandomOrder()->limit(3)->get();
        // $cont = 0;
        // foreach ($category as $key => $value) {
        //     $providers[$cont] = DB::table('proveedor')->where('id_tipo_proveedor', $categories['name'][$cont]->id)->where('localizacion_estado', 'Jalisco')->inRandomOrder()->limit(3)->get();
        //     for($i=0; $i<sizeof($providers[$cont]); $i++) {
        //         $image = DB::table('galeria')->where('id_proveedor', $providers[$cont][$i]->id_proveedor)->where('principal', 1)->first();
        //         if($image) {
        //             $providers[$cont][$i]->image = $image->nombre;
        //         }
        //     }
        //     $cont+=1;
        // }
        // $categories['providers'] = $providers;
        $this->load->view("brideadvisor/index");
    }
    public function extraerProviders() {
        if($_POST) {
            $category = DB::table('proveedor_categoria')->where('parent_id', '!=', '')->orderBy('name')->skip($this->input->post('page'))->take(3)->get();
            $categories['name'] = DB::table('proveedor_categoria')->where('parent_id', '!=', '')->orderBy('name')->skip($this->input->post('page'))->take(3)->get();
            $cont = 0;
            foreach ($category as $key => $value) {
                if(!empty($this->input->post('state')) && $this->input->post('state')!='Todos') {
                    $providers[$cont] = DB::table('categoria_proveedor')
                    ->join('proveedor', 'categoria_proveedor.proveedor_id', '=', 'proveedor.id_proveedor')
                    ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
                    ->leftjoin('average_reviews', 'proveedor.id_proveedor', '=', 'average_reviews.id_proveedor')
                    ->select('average_reviews.promedio', 'categoria_proveedor.categoria_id', 'proveedor.*')
                    ->where('categoria_proveedor.categoria_id', $value->id)
                    ->where('proveedor.localizacion_estado', $this->input->post('state'))
                    ->where('usuario.activo', '2')
                    ->groupBy('proveedor.id_proveedor')
                    ->orderBy('proveedor.tipo_cuenta', 'DESC')->limit(3)->get();
                } else {
                    $providers[$cont] = DB::table('categoria_proveedor')
                    ->join('proveedor', 'categoria_proveedor.proveedor_id', '=', 'proveedor.id_proveedor')
                    ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
                    ->leftjoin('average_reviews', 'proveedor.id_proveedor', '=', 'average_reviews.id_proveedor')
                    ->select('average_reviews.promedio', 'categoria_proveedor.categoria_id', 'proveedor.*')
                    ->where('categoria_proveedor.categoria_id', $value->id)
                    ->where('usuario.activo', '2')
                    ->groupBy('proveedor.id_proveedor')
                    ->orderBy('proveedor.tipo_cuenta', 'DESC')->limit(3)->get();
                }
                for($i=0; $i<sizeof($providers[$cont]); $i++) {
                    $image = DB::table('galeria')->where('id_proveedor', $providers[$cont][$i]->id_proveedor)->where('principal', 1)->first();
                    if($image) {
                        $providers[$cont][$i]->image = $image->nombre;
                    }
                }
                $cont+=1;
            }
            $categories['providers'] = $providers;
            // dd($categories);
            $categoria = DB::table('proveedor_categoria')->where('parent_id', '!=', '')->get();
            $cat = array();
            foreach ($categoria as $key => $value) {
                array_push($cat, $value->name);
            }
            $categories['category'] = $cat;
            return $this->output->set_content_type('application/json')
            ->set_status_header(202)
            ->set_output(json_encode($categories));
        }
    }
    public function saveReview()
    {
        if($_POST) {

            $id = $this->input->post('proveedor');

            $review = DB::table('reviews')->insert([
                'calificacion' => $this->input->post('rating'),
                'mensaje' => $this->input->post('mensaje'),
                'id_boda' => $this->session->userdata("id_boda"),
                'id_usuario' => $this->session->userdata("id_usuario"),
                'id_proveedor' => $this->input->post('proveedor'),
                'activo' => 1,
            ]);
            $reviews = DB::table('reviews')->where('id_proveedor', $this->input->post('proveedor'))->get();
            $cont = 0;
            $suma = 0;
            foreach ($reviews as $key => $value) {
                $suma = $suma + $value->calificacion;
                $cont+=1;
            }
            $prom = intval(round($suma / $cont));
            $promExist = DB::table('average_reviews')->where('id_proveedor', $this->input->post('proveedor'))->first();
            if(!empty($promExist)) {
                DB::table('average_reviews')
                                ->where('id_proveedor', $this->input->post('proveedor'))
                                ->update(['promedio' => $prom]);
            } else {
                $save = DB::table('average_reviews')->insert([
                    'id_proveedor' => $this->input->post('proveedor'),
                    'promedio' => $prom
                ]);
            }
            $response = [
                "code"    => 200,
                "data"    => [],
                "message" => 'ok',
            ];
            return $this->output->set_status_header($response['code'])
                ->set_output(json_encode($response));
        }
    }
    public function like_proveedor()
    {
        $this->output->set_content_type("application/json");
        $id_cliente = $this->session->userdata("id_cliente");
        $proveedor =  $this->input->post("proveedor");
        if ($_POST && $id_cliente) {
            
            $registro = DB::table('favorite_provider')->where('id_client', $id_cliente)->where('id_provider', $proveedor)->first();
            if($registro){
                return $this->output->set_status_header(202)
                    ->set_output(json_encode(array("success" => true, "data" => "ok")));
            }else{
                return $this->output->set_status_header(202)
                    ->set_output(json_encode(array("success" => true, "data" => "error")));
            }
        }
        return $this->output->set_status_header(404);
    }
    public function create_like(){
        $this->output->set_content_type("application/json");
        
        $id_cliente = $this->session->userdata("id_cliente");
        $proveedor =  $this->input->post("proveedor");
        
        if ($_POST && $id_cliente) {
            $category=DB::table('categoria_proveedor')->where('proveedor_id', $proveedor)->select('categoria_id')->first();
            
            $registro = DB::table('favorite_provider')->insert([
               'id_client' => $id_cliente,
               'id_provider' => $proveedor,
               'id_category' => $category->categoria_id]);
             
            return $this->output->set_status_header(202)
                ->set_output(json_encode(array("success" => true, "data" => "ok")));
        }
        return $this->output->set_status_header(404);
    }
    public function delete_like(){
         $this->output->set_content_type("application/json");
         
        $id_cliente = $this->session->userdata("id_cliente");
        $proveedor =  $this->input->post("proveedor");
        
        if ($_POST && $id_cliente) {
            DB::table('favorite_provider')->where('id_client', $id_cliente)->where('id_provider', $proveedor)->delete();
            
            return $this->output->set_status_header(202)
                ->set_output(json_encode(array("success" => true, "data" => "ok")));
        }
        return $this->output->set_status_header(404);
    }
    public function providerForCategory() {
        if($_POST) {
            if($this->input->post('location')!='Todos' && !empty($this->input->post('name')) && !empty($this->input->post('rating'))) {
                //Todos los filtros
                $category = DB::table('proveedor_categoria')->where('name', $this->input->post('name'))->first();
                $providers['providers'] = DB::table('proveedor')
                ->join('average_reviews', 'proveedor.id_proveedor', '=', 'average_reviews.id_proveedor')
                ->join('categoria_proveedor', 'proveedor.id_proveedor', '=', 'categoria_proveedor.proveedor_id')
                ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
                ->select('proveedor.*', 'average_reviews.promedio' )
                ->where('average_reviews.promedio', $this->input->post('rating'))
                ->where('proveedor.localizacion_estado', $this->input->post('location'))
                ->where('categoria_proveedor.categoria_id', $category->id)
                ->where('usuario.activo', 2)
                ->orderBy('proveedor.tipo_cuenta', 'DESC')
                ->skip($this->input->post('page'))->take(12)
                ->get();
            } elseif($this->input->post('location')!='Todos' && empty($this->input->post('name')) && !empty($this->input->post('rating'))) {
                //Por estado y por calificación
                $providers['providers'] = DB::table('proveedor')
                ->join('average_reviews', 'proveedor.id_proveedor', '=', 'average_reviews.id_proveedor')
                ->select('proveedor.*', 'average_reviews.promedio' )
                ->where('average_reviews.promedio', $this->input->post('rating'))
                ->where('proveedor.localizacion_estado', $this->input->post('location'))
                ->skip($this->input->post('page'))->take(12)
                ->orderBy('proveedor.tipo_cuenta', 'DESC')
                ->get();
            } elseif($this->input->post('location')!='Todos' && !empty($this->input->post('name')) && empty($this->input->post('rating'))) {
                //Por estado y por categoría
                $category = DB::table('proveedor_categoria')->where('name', $this->input->post('name'))->first();
                $providers['providers'] = DB::table('proveedor')
                ->leftJoin('average_reviews', 'proveedor.id_proveedor', '=', 'average_reviews.id_proveedor')
                ->join('categoria_proveedor', 'proveedor.id_proveedor', '=', 'categoria_proveedor.proveedor_id')
                ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
                ->select('proveedor.*', 'average_reviews.promedio' )
                ->where('proveedor.localizacion_estado', $this->input->post('location'))
                ->where('categoria_proveedor.categoria_id', $category->id)
                ->where('usuario.activo', 2)
                ->groupBy('proveedor.id_usuario')
                ->skip($this->input->post('page'))->take(12)
                ->orderBy('proveedor.tipo_cuenta', 'DESC')
                ->get();
            } elseif($this->input->post('location')=='Todos' && !empty($this->input->post('name')) && !empty($this->input->post('rating'))) {
                //Por categoria y por calificación
                $category = DB::table('proveedor_categoria')->where('name', $this->input->post('name'))->first();
                $providers['providers'] = DB::table('proveedor')
                ->join('average_reviews', 'proveedor.id_proveedor', '=', 'average_reviews.id_proveedor')
                ->join('categoria_proveedor', 'proveedor.id_proveedor', '=', 'categoria_proveedor.proveedor_id')
                ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
                ->select('proveedor.*', 'average_reviews.promedio' )
                ->where('average_reviews.promedio', $this->input->post('rating'))
                ->where('categoria_proveedor.categoria_id', $category->id)
                ->where('usuario.activo', 2)
                ->skip($this->input->post('page'))->take(12)
                ->orderBy('proveedor.tipo_cuenta', 'DESC')
                ->get();
            } elseif($this->input->post('location')=='Todos' && !empty($this->input->post('name')) && empty($this->input->post('rating'))) {
                //Por categoría --PENDIENTE
                $category = DB::table('proveedor_categoria')->where('name', $this->input->post('name'))->first();
                $providers['providers'] = DB::table('proveedor')
                ->leftJoin('average_reviews', 'proveedor.id_proveedor', '=', 'average_reviews.id_proveedor')
                ->join('categoria_proveedor', 'proveedor.id_proveedor', '=', 'categoria_proveedor.proveedor_id')
                ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
                ->select('proveedor.*', 'average_reviews.promedio' )
                ->where('categoria_proveedor.categoria_id', $category->id)
                ->where('usuario.activo', 2)
                ->groupBy('proveedor.id_usuario')
                ->skip($this->input->post('page'))->take(12)
                ->orderBy('proveedor.tipo_cuenta', 'DESC')
                ->get();
            } elseif($this->input->post('location')=='Todos' && empty($this->input->post('name')) && !empty($this->input->post('rating'))) {
                //Por calificación
                $providers['providers'] = DB::table('proveedor')
                ->join('average_reviews', 'proveedor.id_proveedor', '=', 'average_reviews.id_proveedor')
                ->join('categoria_proveedor', 'proveedor.id_proveedor', '=', 'categoria_proveedor.proveedor_id')
                ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
                ->select('proveedor.*', 'average_reviews.promedio' )
                ->where('average_reviews.promedio', $this->input->post('rating'))
                ->where('usuario.activo', 2)
                ->skip($this->input->post('page'))->take(12)
                ->orderBy('proveedor.tipo_cuenta', 'DESC')
                ->get();
            }
            for ($i=0; $i<sizeof($providers['providers']); $i++) {
                $image = DB::table('galeria')->where('id_proveedor', $providers['providers'][$i]->id_proveedor)->where('Principal', 1)->first();
                if(!empty($image->nombre)) {
                    $providers['providers'][$i]->image = $image->nombre;
                } else {
                    $providers['providers'][$i]->image = null;
                }
            }
            $providers['category'] = $this->input->post('name');
            // dd($providers);
            return $this->output->set_content_type('application/json')
            ->set_status_header(202)
            ->set_output(json_encode($providers));
        }
    }
    public function perfil($slug) {
        $data['info'] = DB::table('proveedor')->where('slug', $slug)->first();
        $data['galeria'] = DB::table('galeria')->where('id_proveedor', $data['info']->id_proveedor)->orderBy('principal', 'DESC')->get();
        $data['video'] = DB::table('galeria')->where('tipo', 'VIDEO')->where('id_proveedor', $data['info']->id_proveedor)->first();
        $data['promociones'] = DB::table('promocion')->where('id_proveedor', $data['info']->id_proveedor )->get();
        // echo($data['promociones']);
        $data['reviews'] = DB::table('reviews')
                ->join('usuario', 'usuario.id_usuario', '=', 'reviews.id_usuario')
                ->select('reviews.*', 'usuario.nombre' )
                ->where('reviews.id_proveedor', $data['info']->id_proveedor )
                ->get();
        $this->load->view("brideadvisor/profile", $data);
    }
    public function providers() {
        $term = $this->input->get("term");
        //  $proveedores = Provider::select('nombre')->where('nombre', 'like', '%'.$term.'%')->whereHas("activo", function ($query){
        //     return $query->where("activo", 2);
        // })->get();
        $categories = Category::select('name')->where('name', 'like', '%'.$term.'%')->where('parent_id', '!=', '')->get();
        $aux = [];
        // foreach ($proveedores as $key => $v) {
        //     $aux[$key] = $v['nombre'];

        // }
        foreach ($categories as $key => $v) {
            $aux[$key] = $v['name'];
        }
        $busqueda = $aux;
        unset($aux);

        return $this->output->set_status_header(200)
            ->set_output(json_encode($busqueda));
    }

    public function searchProvider() {
        if($this->input->post('state')!='Todos' && !empty($this->input->post('state'))) {
            $provider = DB::table('proveedor')
            ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
            ->join('galeria', 'proveedor.id_proveedor', '=', 'galeria.id_proveedor')
            ->select('proveedor.*', 'usuario.activo', 'galeria.nombre as namephoto')
            ->where('proveedor.nombre', 'like', '%'.$this->input->post('name').'%')
            ->where('proveedor.localizacion_estado', $this->input->post('state'))
            ->whereIn('galeria.principal', [1])
            ->groupBy('proveedor.id_proveedor')
            ->where('usuario.activo', 2)->get();
        } else {
            $provider = DB::table('proveedor')
            ->join('usuario', 'proveedor.id_usuario', '=', 'usuario.id_usuario')
            ->join('galeria', 'proveedor.id_proveedor', '=', 'galeria.id_proveedor')
            ->select('proveedor.*', 'usuario.activo', 'galeria.nombre as namephoto')
            ->where('proveedor.nombre',  'like', '%'.$this->input->post('name').'%')
            ->whereIn('galeria.principal', [1])
            ->groupBy('proveedor.id_proveedor')
            ->where('usuario.activo', 2)->get();
        }
        if(!empty($provider)) {
            // dd($provider);
            if(sizeof($provider)<2) {
                $image = DB::table('galeria')->where('id_proveedor', $provider[0]->id_proveedor)->where('principal', 1)->first();
                $categories = DB::table('categoria_proveedor')->where('proveedor_id', $provider[0]->id_proveedor)->first();
            } else {
                $image = DB::table('galeria')->where('id_proveedor', $provider[0]->id_proveedor)->where('principal', 1)->first();
                $categories = DB::table('categoria_proveedor')->where('proveedor_id', $provider[0]->id_proveedor)->first();
            }
            $category = DB::table('proveedor_categoria')->where('id', $categories->categoria_id)->first();
            $data['info'] = $provider;
            $data['image'] = $image->nombre;
            $data['category'] = $category->name;
            // dd($data);
            return $this->output->set_content_type('application/json')
                ->set_status_header(202)
                ->set_output(json_encode($data));
        } else {
            $data['info'] = 'not found';
            return $this->output->set_content_type('application/json')
                ->set_status_header(202)
                ->set_output(json_encode($data));
        }
    }
    public function mensaje()
    {
        if($_POST) {
            $this->output->set_content_type("application/json");
            $response = [
                "code"    => 200,
                "data"    => [],
                "message" => 'Ok',
            ];
            $this->input->post("message", true, "String", ["required" => true, "min" => 0, "max" => 500]);
            $user     = User::find($this->session->userdata("id_usuario"));
            $id_proveedor = $this->input->post('id_proveedor');
            $provider = Provider::find($id_proveedor);
            // $isCoupon = $this->input->post('isCoupon');

            $id_usuario = $this->session->userdata("id_usuario");
            $id_cliente = $this->session->userdata("id_cliente");
            $id_boda    = $this->session->userdata("id_boda");

            $provider->indicators()
                ->attach($this->session->userdata('id_usuario'),
                    ['type' => 'contact_request']);
            $indicator = $provider->indicators()->latest()->first()->pivot;

            $to   = $provider->user->id_usuario;
            $from = $id_usuario;

            $correo = [
                "mensaje"       => $this->input->post("message"),
                "asunto"        => $this->input->post("asunto", true, "String", ["max" => 140]),
                "to"            => $to,
                "from"          => $from,
                "indicator_id"  => $indicator->id,
                "leida_usuario" => 1,
            ];
            $correo = $this->correo->insert($correo);
            $this->correo->sendEmail();

            $response['code']    = 200;
            $response['message'] = "ok";

            return $this->output->set_status_header($response['code'])
                ->set_output(json_encode($response));
        }
    }

    public function categoria($slug = null, $estado = '', $ciudad = '')
    {
        $wantsJson   = $this->input->get_request_header('accept', true) === "application/json";
        $ver         = is_null($this->input->get("ver")) ? "list" : $this->input->get("ver");
        $page        = is_null($this->input->get("pagina")) ? 1 : $this->input->get("pagina");
        $ciudad      = ($this->input->get("ciudad") == "Todos") ? null : $this->input->get("ciudad");
        $search      = is_null($this->input->get("buscar")) ? null : $this->input->get("buscar");
        $filterState = is_null($this->input->get('estado')) ? "Jalisco" : $this->input->get('estado');
        $estado      = ($this->input->get('estado') == "Todos") ? null : $filterState;
        $idBoda      = $this->session->userdata("id_boda");
        $category    = Category::where("slug", $slug)->first();

        if ( ! $category) {
            show_404();
        }
        $categoryIds[] = $category->id;

        if ($category->subcategories->count()) {
            $categoryIds = array_merge($categoryIds, $category->subcategories->pluck("id")->toArray());
        }
        $providers = new \Illuminate\Support\Collection();

        foreach ($categoryIds as $categoryId) {
            $categoryProviders = Category::with([
                "providers" => function ($query) use ($search, $estado, $ciudad) {
                    return $query->when($search, function ($query) use ($search) {
                        return $query->where(function ($query) use ($search) {
                            return $query->where("nombre", "LIKE", "%$search%")
                                ->orWhere("descripcion", "LIKE", "%$search%");
                        });
                    })->whereHas("user", function ($query) use ($search) {
                        return $query->where("activo", 2);
                    })->where("id_proveedor", "<>", 103)
                        ->when($estado, function ($query) use ($estado) {
                            return $query->where("localizacion_estado", $estado);
                        }, function ($query) {
                            return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
                        })
                        ->when($ciudad, function ($query) use ($ciudad) {
                            return $query->where("localizacion_poblacion", $ciudad);
                        })
                        ->orderBy(DB::raw("rand(".date("Ymd").") "));
                },
            ])->find($categoryId)->providers;
            foreach ($categoryProviders as $provider) {
                $providers->push($provider);
            }
        }
        // dd($providers->count());
        $data["state"]     = $estado;
        if($providers->count() < 1 && empty($search)){ ///Traer todos los proveedores
            $estado = null;
            foreach ($categoryIds as $categoryId) {
                $categoryProviders = Category::with([
                    "providers" => function ($query) use ($search, $estado, $ciudad) {
                        return $query->when($search, function ($query) use ($search) {
                            return $query->where(function ($query) use ($search) {
                                return $query->where("nombre", "LIKE", "%$search%")
                                    ->orWhere("descripcion", "LIKE", "%$search%");
                            });
                        })->whereHas("user", function ($query) use ($search) {
                            return $query->where("activo", 2);
                        })->where("id_proveedor", "<>", 103)
                            ->when($estado, function ($query) use ($estado) {
                                return $query->where("localizacion_estado", $estado);
                            }, function ($query) {
                                return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
                            })
                            ->when($ciudad, function ($query) use ($ciudad) {
                                return $query->where("localizacion_poblacion", $ciudad);
                            })
                            ->orderBy(DB::raw("rand(".date("Ymd").") "));
                    },
                ])->find($categoryId)->providers;
    
                foreach ($categoryProviders as $provider) {
                    $providers->push($provider);
                }
            }
        }
        $providers = $providers->unique('id_proveedor');

        $providers = $providers->sortByDesc('tipo_cuenta');

        /// por foreach contar cuantas cuentas son de pago...
        $cuentas_pago = 0;
        $providers_basic = clone($providers);
        $providers_vip = [];
        foreach ($providers_basic as $k => $v) {
            if($v->tipo_cuenta > 0){
                $cuentas_pago+=1;
                // $providers_vip[$k]=$providers[$k];
                unset($providers_basic[$k]);
            }
            else{}
                // $providers_basic[$k]=$providers[$k];
        }
        $data["providers"] = collection_paginate($providers_basic, $ver == "list" ? 10 : 12, $page);
        $data["lastpage"] = $data["providers"]->lastpage()+1;
        unset($data["providers"]);
        /// y condicionar cuantos va a enviar en la pagina = 1 para
        // y la pagina = 2 empieze en su lugar
        $data["category"]  = $category;
        if($cuentas_pago > 0 && $page==1){///hay vips y es la primer pagina
            $data["providers"] = collection_paginate($providers, $ver == "list" ? $cuentas_pago : $cuentas_pago, $page);
        }elseif($cuentas_pago > 0 && $page>1){////hay vips y es otra pagina
            $data["providers"] = collection_paginate($providers_basic, $ver == "list" ? 10 : 12, $page-1);
        }else
            $data["providers"] = collection_paginate($providers, $ver == "list" ? 10 : 12, $page);

        // $data["category"]  = $category;
        // $data["providers"] = collection_paginate($providers, $ver == "list" ? 10 : 12, $page);
        $data["page"]      = $page;
        // $data["state"]     = $estado;
        foreach ($data["providers"]->items() as $provider) {
            $userId = $this->session->userdata("id_usuario") ? $this->session->userdata("id_usuario") : [null];
            $type   = $search ? "appearance_search" : "appearance_category";
            $provider->indicators()->attach($userId, ["type" => $type]);
            $provider->load("imagePrincipal", "images");
            foreach ($provider->FAQ as $FAQ) {
                $provider->capacidad = ($FAQ->tipo == 'RANGE' && $FAQ->valores == 'PERSONAS') ? explode("|", $FAQ->pivot->respuesta) : '';
                $provider->precio    = ($FAQ->tipo == 'RANGE' && $FAQ->valores == null) ? explode("|", $FAQ->pivot->respuesta) : '';
            }

            if ($provider->weddings->count() && $idBoda) {
                $provider->favorito = $provider->weddings->keyBy('id_boda')->get($idBoda) ? 1 : 0;
            }
        }
        $data["servicio"]          = "ALL";
        $data["sector"]            = "ALL";
        $data["estado"]            = "ALL";
        $data["ciudad"]            = "ALL";
        $data["total_proveedores"] = 0;

        $sector  = $data["sector"] == "ALL" ? null : $data["sector"];
        $filtros = $this->input->get();

        $data["promociones"] = Promotion::whereHas("provider", function ($query) {
            return $query->has("categories");
        })->where("fecha_fin", ">", Carbon::now())->get();
        $data["total"]       = $providers->count();

        $data["ver"]               = $ver;
        $data["tipos"]             = $this->tipo->getAll();
        $data["total_proveedores"] = $this->proveedor->count();
        $data["estados"]           = $this->tipo_proveedor->getEstados();

        if ($wantsJson) {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(
                    json_encode($data["providers"])
                );
        } else {
            $this->load->view("proveedor/lista_proveedores", $data);
        }
    }

    public function sector($sector = null, $estado = null, $ciudad = null)
    {
        $wantsJson   = $this->input->get_request_header('accept', true) === "application/json";
        $ver         = is_null($this->input->get('ver')) ? 'list' : $this->input->get('ver');
        $page        = is_null($this->input->get('pagina')) ? 1 : $this->input->get('pagina');
        $search      = is_null($this->input->get('buscar')) ? null : $this->input->get('buscar');
        $filterState = is_null($this->input->get('estado')) ? 'Jalisco' : $this->input->get('estado');
        $estado      = ($this->input->get('estado') == "Todos") ? null : $filterState;
        $ciudad      = ($this->input->get("ciudad") == "Todos") ? null : $this->input->get("ciudad");
        $idBoda      = $this->session->userdata('id_boda');
        $nombre      = null;
        $category    = Category::where("name", $search)->first();

        if ( ! $category) {
            $providers = Provider::has("categories")
            ->whereHas("user", function ($query) use ($search) {
                return $query->where("activo", 2);
            })->when($search, function ($query) use ($search) {
                return $query->where(function ($query) use ($search) {
                    return $query->where("nombre", "LIKE", "%$search%")
                        ->orWhere("descripcion", "  LIKE", "%$search%");
                });
            })->where("id_proveedor", "<>", 103)
            ->when($estado, function ($query) use ($estado) {
                return $query->where("localizacion_estado", $estado);
            }, function ($query) {
                return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
            })
            ->when($ciudad, function ($query) use ($ciudad) {
                return $query->where("localizacion_poblacion", $ciudad);
            })
            ->with("FAQ")
            ->orderBy(DB::raw("rand(".date("Ymd").") "))
            ->get();
            // dd(empty($search));
            // dd($providers->count());
            // dd(isset($providers));
            $data["state"]     = $estado;
            if($providers->count() < 1 && empty($search)){ ///Traer todos los proveedores
                $estado = null;
                $providers = Provider::has("categories")
                ->whereHas("user", function ($query) use ($search) {
                    return $query->where("activo", 2);
                })->when($search, function ($query) use ($search) {
                    return $query->where(function ($query) use ($search) {
                        return $query->where("nombre", "LIKE", "%$search%")
                            ->orWhere("descripcion", "LIKE", "%$search%");
                    });
                })->where("id_proveedor", "<>", 103)
                ->when($estado, function ($query) use ($estado) {
                    return $query->where("localizacion_estado", $estado);
                }, function ($query) {
                    return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
                })
                ->when($ciudad, function ($query) use ($ciudad) {
                    return $query->where("localizacion_poblacion", $ciudad);
                })
                ->with("FAQ")
                ->orderBy(DB::raw("rand(".date("Ymd").") "))
                ->get();
            }
            $providers = $providers->sortByDesc("tipo_cuenta");
            /// por foreach contar cuantas cuentas son de pago...
            $cuentas_pago = 0;
            $providers_basic = clone($providers);
            foreach ($providers_basic as $k => $v) {
                if($v->tipo_cuenta > 0){
                    $cuentas_pago+=1;
                    unset($providers_basic[$k]);
                }
            }
            $data["providers"] = collection_paginate($providers_basic, $ver == "list" ? 10 : 12, $page);
            $data["lastpage"] = $data["providers"]->lastpage()+1;
            unset($data["providers"]);

            if($cuentas_pago > 0 && $page==1){///hay vips y es la primer pagina
                $data["providers"] = collection_paginate($providers, $ver == "list" ? $cuentas_pago : $cuentas_pago, $page);
            }elseif($cuentas_pago > 0 && $page>1){////hay vips y es otra pagina
                $data["providers"] = collection_paginate($providers_basic, $ver == "list" ? 10 : 12, $page-1);
            }else
                $data["providers"] = collection_paginate($providers, $ver == "list" ? 10 : 12, $page);
            $data["page"]      = $page;
            $data["state"]     = $estado;
            // $data["providers"] = collection_paginate($providers, $ver == "list" ? 10 : 12, $page);
            foreach ($data["providers"]->items() as $provider) {
                $userId = $this->session->userdata("id_usuario") ? $this->session->userdata("id_usuario") : [null];
                $type   = $search ? "appearance_search" : "appearance_category";
                $provider->indicators()->attach($userId, ["type" => $type]);
                $provider->load("imagePrincipal");
                foreach ($provider->FAQ as $FAQ) {
                    $provider->capacidad = ($FAQ->tipo == 'RANGE' && $FAQ->valores == 'PERSONAS') ? explode("|", $FAQ->pivot->respuesta) : '';
                    $provider->precio    = ($FAQ->tipo == 'RANGE' && $FAQ->valores == null) ? explode("|", $FAQ->pivot->respuesta) : '';
                }
                if ($provider->weddings->count() && $idBoda) {
                    $provider->favorito = $provider->weddings->keyBy('id_boda')->get($idBoda) ? 1 : 0;
                }
            }
            $data["allProviders"] = $providers;

            if ($this->categoria->exists(str_replace('-', ' ', $nombre))) {
                $tipo = $this->tipo->get(['tag_categoria' => urldecode(str_replace('-', ' ', strtoupper($nombre)))]);
            } else {
                $tipo = $this->tipo->get(['nombre_tipo_proveedor' => urldecode(str_replace('-', ' ', $nombre))]);
            }
            $data['servicio']          = 'ALL';
            $data['sector']            = 'ALL';
            $data['estado']            = 'ALL';
            $data['ciudad']            = 'ALL';
            $data['total_proveedores'] = 0;
            if ($tipo) {
                $data['sector']   = $tipo->grupo;
                $data['servicio'] = $tipo;
                $data['filtros']  = $this->tipo->getFilters($tipo->id_tipo_proveedor, $this->input);
            }
            if ($estado) {
                $data['estado'] = str_replace('-', ' ', $estado);
            }
            if ($ciudad) {
                $data['ciudad'] = str_replace('-', ' ', $ciudad);
            }
            $categoria = $data['servicio'] == 'ALL' ? $nombre : $data['servicio']->id_tipo_proveedor;
            $sector    = $data['sector'] == 'ALL' ? null : $data['sector'];
            $estado    = $data['estado'] == 'ALL' ? null : $data['estado'];
            $ciudad    = $data['ciudad'] == 'ALL' ? null : $data['ciudad'];
            $filtros   = $this->input->get();

            $data['proveedores'] = $this->proveedor->search_menu($page, $categoria, $sector, $estado, $ciudad, 0, $filtros);
            $data['promociones'] = Promotion::whereHas('provider', function ($query) {
                return $query->has('categories');
            })->where('fecha_fin', '>', Carbon::now())->get();
            $data['total']       = $this->proveedor->total_search_menu($categoria, $sector, $estado, $ciudad);

            $data['ver']               = $ver;
            $data['tipos']             = $this->tipo->getAll();
            $data['total_proveedores'] = $this->proveedor->count();
            $data['estados']           = $this->tipo_proveedor->getEstados();
            $data['isSector']          = 1;

            if ($wantsJson) {
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(
                        json_encode($data["providers"])
                    );
            } else {
                $this->load->view('proveedor/lista_proveedores', $data);
            }
        }else{
            $categoryIds[] = $category->id;
            $search = null;  
            if ($category->subcategories->count()) {
                $categoryIds = array_merge($categoryIds, $category->subcategories->pluck("id")->toArray());
            }
            $providers = new \Illuminate\Support\Collection();
    
            foreach ($categoryIds as $categoryId) {
                $categoryProviders = Category::with([
                    "providers" => function ($query) use ($search, $estado, $ciudad) {
                        return $query->when($search, function ($query) use ($search) {
                            return $query->where(function ($query) use ($search) {
                                return $query->where("nombre", "LIKE", "%$search%")
                                    ->orWhere("descripcion", "LIKE", "%$search%");
                            });
                        })->whereHas("user", function ($query) use ($search) {
                            return $query->where("activo", 2);
                        })->where("id_proveedor", "<>", 103)
                            ->when($estado, function ($query) use ($estado) {
                                return $query->where("localizacion_estado", $estado);
                            }, function ($query) {
                                return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
                            })
                            ->when($ciudad, function ($query) use ($ciudad) {
                                return $query->where("localizacion_poblacion", $ciudad);
                            })
                            ->orderBy(DB::raw("rand(".date("Ymd").") "));
                    },
                ])->find($categoryId)->providers;
    
                foreach ($categoryProviders as $provider) {
                    $providers->push($provider);
                }
            }
            // dd($providers->count());
            $data["state"]     = $estado;
            if($providers->count() < 1 && empty($search)){ ///Traer todos los proveedores
                $estado = null;
                foreach ($categoryIds as $categoryId) {
                    $categoryProviders = Category::with([
                        "providers" => function ($query) use ($search, $estado, $ciudad) {
                            return $query->when($search, function ($query) use ($search) {
                                return $query->where(function ($query) use ($search) {
                                    return $query->where("nombre", "LIKE", "%$search%")
                                        ->orWhere("descripcion", "LIKE", "%$search%");
                                });
                            })->whereHas("user", function ($query) use ($search) {
                                return $query->where("activo", 2);
                            })->where("id_proveedor", "<>", 103)
                                ->when($estado, function ($query) use ($estado) {
                                    return $query->where("localizacion_estado", $estado);
                                }, function ($query) {
                                    return $query->whereIn("localizacion_estado", ['Jalisco', 'Queretaro', 'Puebla', 'Sinaloa']);
                                })
                                ->when($ciudad, function ($query) use ($ciudad) {
                                    return $query->where("localizacion_poblacion", $ciudad);
                                })
                                ->orderBy(DB::raw("rand(".date("Ymd").") "));
                        },
                    ])->find($categoryId)->providers;
        
                    foreach ($categoryProviders as $provider) {
                        $providers->push($provider);
                    }
                }
            }
            $providers = $providers->unique('id_proveedor');
            $providers = $providers->sortByDesc('tipo_cuenta');
            /// por foreach contar cuantas cuentas son de pago...
            $cuentas_pago = 0;
            $providers_basic = clone($providers);
            $providers_vip = [];
            foreach ($providers_basic as $k => $v) {
                if($v->tipo_cuenta > 0){
                    $cuentas_pago+=1;
                    // $providers_vip[$k]=$providers[$k];
                    unset($providers_basic[$k]);
                }
                else{}
                    // $providers_basic[$k]=$providers[$k];
            }
            $data["providers"] = collection_paginate($providers_basic, $ver == "list" ? 10 : 12, $page);
            $data["lastpage"] = $data["providers"]->lastpage()+1;
            unset($data["providers"]);
            /// y condicionar cuantos va a enviar en la pagina = 1 para
            // y la pagina = 2 empieze en su lugar
            $data["category"]  = $category;
            if($cuentas_pago > 0 && $page==1){///hay vips y es la primer pagina
                $data["providers"] = collection_paginate($providers, $ver == "list" ? $cuentas_pago : $cuentas_pago, $page);
            }elseif($cuentas_pago > 0 && $page>1){////hay vips y es otra pagina
                $data["providers"] = collection_paginate($providers_basic, $ver == "list" ? 10 : 12, $page-1);
            }else
                $data["providers"] = collection_paginate($providers, $ver == "list" ? 10 : 12, $page);
            // $data["category"]  = $category;
            // $data["providers"] = collection_paginate($providers, $ver == "list" ? 10 : 12, $page);
            $data["page"]      = $page;
            // $data["state"]     = $estado;
            foreach ($data["providers"]->items() as $provider) {
                $userId = $this->session->userdata("id_usuario") ? $this->session->userdata("id_usuario") : [null];
                $type   = $search ? "appearance_search" : "appearance_category";
                $provider->indicators()->attach($userId, ["type" => $type]);
                $provider->load("imagePrincipal", "images");
                foreach ($provider->FAQ as $FAQ) {
                    $provider->capacidad = ($FAQ->tipo == 'RANGE' && $FAQ->valores == 'PERSONAS') ? explode("|", $FAQ->pivot->respuesta) : '';
                    $provider->precio    = ($FAQ->tipo == 'RANGE' && $FAQ->valores == null) ? explode("|", $FAQ->pivot->respuesta) : '';
                }
                if ($provider->weddings->count() && $idBoda) {
                    $provider->favorito = $provider->weddings->keyBy('id_boda')->get($idBoda) ? 1 : 0;
                }
            }
            $data["servicio"]          = "ALL";
            $data["sector"]            = "ALL";
            $data["estado"]            = "ALL";
            $data["ciudad"]            = "ALL";
            $data["total_proveedores"] = 0;
            $sector  = $data["sector"] == "ALL" ? null : $data["sector"];
            $filtros = $this->input->get();
    
            $data["promociones"] = Promotion::whereHas("provider", function ($query) {
                return $query->has("categories");
            })->where("fecha_fin", ">", Carbon::now())->get();
            $data["total"]       = $providers->count();
    
            $data["ver"]               = $ver;
            $data["tipos"]             = $this->tipo->getAll();
            $data["total_proveedores"] = $this->proveedor->count();
            $data["estados"]           = $this->tipo_proveedor->getEstados();
    
            if ($wantsJson) {
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(
                        json_encode($data["providers"])
                    );
            } else {
                $this->load->view("proveedor/lista_proveedores", $data);
            } 
        }
        // go for all the providers in the state
    }

    public function archivo($id_adjunto, $nombre = null)
    {
        $id_usuario = $this->session->id_usuario;
        if ($id_usuario) {
            $archivo = $this->adjunto->get($id_adjunto);
            if ($archivo) {
                if ($archivo->id_archivo) {
                    $archivo = $this->archivo->get($archivo->id_archivo);
                }
                if ($archivo) {
                    header('Content-Disposition: attachment; filename="'.basename($archivo->nombre).'"');

                    return $this->output
                        ->set_content_type($archivo->mime)
                        ->set_status_header(202)
                        ->set_output(base64_decode(str_replace("data:$archivo->mime;base64,", "", $archivo->data)));
                }
            }
        }
        show_404();
    }
    public function archivo_plantilla($id_archivo, $nombre = null)
    {
        $id_cliente   = $this->session->userdata("id_cliente");
        $id_proveedor = $this->session->userdata("id_proveedor");
        if ($id_cliente || $id_proveedor) {
            $archivo = $this->archivo->get($id_archivo);
            if ($archivo) {

                return $this->output
                    ->set_content_type($archivo->mime)
                    ->set_status_header(202)
                    ->set_output(base64_decode(str_replace("data:$archivo->mime;base64,", "", preg_replace('/\s*/m', '', $archivo->data))));
            }
        }
        show_404();
    }
    public function sugeridos($page = 0, $categoryId = 112, $isOnMobile = 0)
    {
        if ( ! is_numeric($categoryId) || ! is_numeric($this->session->userdata("id_boda"))) {
            return $this->output
                ->set_content_type("application/json")
                ->set_status_header(406)
                ->set_output(
                    json_encode(
                        [
                            "success" => false,
                            "data"    => "",
                        ]
                    )
                );
        }
        $wedding = Wedding::find($this->session->userdata("id_boda"));
        //This returns the active providers that have already set a price for their service
        $category = Category::with([
            "providers"     => function ($query) use ($wedding) {
                return $query->whereHas("user", function ($query) {
                    return $query->where("activo", "2");
                })->whereHas("FAQ", function ($query) {
                    return $query->where("pregunta", "like", "%¿Cuál es el precio de tu servicio?%");
                })->with([
                    "FAQ"                     => function ($query) {
                        return $query->where("pregunta", "like", "%¿Cuál es el precio de tu servicio?%");
                    },
                    "imagePrincipalMiniature" => function ($query) {
                        return $query->select(["id_galeria", "id_proveedor", "nombre"]);
                    },
                ])->with([
                    "weddings" => function ($query) use ($wedding) {
                        return $query->where("proveedor_boda.id_boda", $wedding->id_boda);
                    },
                ]);
            },
            "subcategories" => function ($query) use ($wedding) {
                return $query->with([
                    "providers" => function ($query) use ($wedding) {
                        return $query->whereHas("user", function ($query) {
                            return $query->where("activo", "2");
                        })->whereHas("FAQ", function ($query) {
                            return $query->where("pregunta", "like", "%¿Cuál es el precio de tu servicio?%");
                        })->with([
                            "FAQ"                     => function ($query) {
                                return $query->where("pregunta", "like",
                                    "%¿Cuál es el precio de tu servicio?%");
                            },
                            "imagePrincipalMiniature" => function ($query) {
                                return $query->select(["id_galeria", "id_proveedor", "nombre"]);
                            },
                        ])->with([
                            "weddings" => function ($query) use ($wedding) {
                                return $query->where("proveedor_boda.id_boda", $wedding->id_boda);
                            },
                        ]);
                    },
                ]);
            },
        ]);
        $category = $category->find($categoryId);
        $budgets = Budget::where("id_boda", $this->session->userdata("id_boda"));
        //where("category_id", $categoryId)
        if ($category->subcategories->count()) {
            $categoriesId    = $category->subcategories()->pluck("id")->toArray();
            $categoriesId [] = $category->id;
            $budgets         = $budgets->whereIn("category_id", $categoriesId);
        } else {
            $budgets = $budgets->where("category_id", $category->id);
        }
        $budgets     = $budgets->get();
        $budgetPrice = 0;
        foreach ($budgets as $budget) {
            //If that category doesn't have an estimate
            if ($budget->costo_aproximado == 0) {
                //That's probably not a category, it's a custom budget
                if ($budget->percentage == 0) {
                    $budgetPrice += $wedding->presupuesto;
                } else {
                    //It's a category and we'll estimate
                    $budgetPrice += $budget->percentage * $wedding->presupuesto;
                }
            } else {
                $budgetPrice += $budget->costo_aproximado;
            }
        }
        $providers = $category->providers->count() ? $category->providers : new \Illuminate\Support\Collection();

        foreach ($category->subcategories as $subcategory) {
            if ($subcategory->providers->count()) {
                foreach ($subcategory->providers as $provider) {
                    $providers->push($provider);
                }
            }
        }
        $providers = $providers->unique('id_proveedor');
        //Check if the providers are below the weddings budget
        foreach ($providers as $key => $provider) {
            $providerPriceRange = explode("|", $provider->FAQ->first()->getOriginal('pivot_respuesta'));
            if (is_null($providerPriceRange) || count($providerPriceRange) != 2) {
                $providers->forget($key);
                continue;
            }
            $providerPriceRange[0] = str_replace(",", "", $providerPriceRange[0]);
            $providerPriceRange[1] = str_replace(",", "", $providerPriceRange[1]);
            //If my budget is zero, if its greater than the provider max price or if its in between the provider price range
            if ($budgetPrice == 0 || $budgetPrice >= $providerPriceRange[1] || ($providerPriceRange[0] <= $budgetPrice && $budgetPrice <= $providerPriceRange[1])) {
                continue;
            } else {
                $providers->forget($key);
            }
        }
        $providers = $providers->unique('id_proveedor')->sortByDesc('tipo_cuenta');
        $providers = collection_paginate($providers, $isOnMobile ? 1 : 3, $page);
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(
                json_encode(
                    [
                        'success'  => true,
                        'category' => $category->name,
                        'data'     => $providers,
                    ]
                )
            );
    }
    public function excel_upload()
    {
        if ($_FILES) {
            /* INFORMATION ABOUT THE FILE */
            $path_parts = pathinfo($_FILES["file"]["name"]);
            /* LOADING UPLOAD LIBRARY */
            $config["upload_path"]   = "./uploads/excel/";
            $config["allowed_types"] = "xls|xlsx";
            $config["max_size"]      = "20480";
            $config["file_name"]     = $path_parts["filename"]."_".uniqid();
            $this->load->library("upload", $config);
            /* UPLOADING FILE */
            if ($this->upload->do_upload("file")) {
                $file        = $this->upload->data()["full_path"];
                $spreadsheet = IOFactory::load($file);
                $spreadsheet->setActiveSheetIndex(2);
                $worksheet = $spreadsheet->getActiveSheet();
                foreach ($worksheet->toArray() as $key => $row) {
                    //We start from the 2nd row because the first and second are title only
                    if ($key < 2) {
                        continue;
                    }
                    //We ignore this values because they're null by default (Empty spaces)
                    unset($row[5]);
                    unset($row[13]);
                    unset($row[16]);
                    //If one of the values is null we ignore the whole row
                    $row = array_values($row);
                    foreach ($row as $item) {
                        if (is_null($item)) {
                            // we break the outter foreach
                            continue 2;
                        }
                    }
                    $country = Country::where('nombre', 'like', $row[7]);
                    $state   = State::where('estado', 'like', $row[8]);
                    $city    = City::where('ciudad', 'like', $row[9]);
                    $country = $country->count() ? $country->first() : $row[7];
                    if ($country instanceof Country) {
                        $state   = $state->where('id_pais', $country->id_pais);
                        $country = $country->nombre;
                        $state   = $state->count() ? $state->first() : $row[8];
                    } else {
                        $state = $row[8];
                    }
                    if ($state instanceof State) {
                        $city  = $city->where('id_estado', $state->id_estado);
                        $state = $state->estado;
                        $city  = $city->count() ? $city->first()->ciudad : $row[9];
                    } else {
                        $city = $row[9];
                    }
                    $categories = Category::whereNotNull('parent_id')->find(explode(",", $row[14]));
                    $user = User::create([
                        'usuario'    => $row[12],
                        'contrasena' => sha1(md5(sha1($row[13]))),
                        'rol'        => 3,
                        'activo'     => 1,
                        'nombre'     => $row[0],
                        'correo'     => $row[1],
                    ]);
                    $provider = Provider::create([
                        'id_usuario'               => $user->id_usuario,
                        'nombre'                   => $user->nombre,
                        'descripcion'              => $row[6],
                        'contacto_telefono'        => $row[2],
                        'contacto_celular'         => $row[3],
                        'contacto_pag_web'         => $row[4],
                        'localizacion_pais'        => $country,
                        'localizacion_cp'          => $row[10],
                        'localizacion_estado'      => $state,
                        'localizacion_poblacion'   => $city,
                        'localizacion_direccion'   => $row[11],
                        'id_tipo_proveedor'        => $categories->first()->id,
                        'acceso'                   => 1,
                        'notificacion_informacion' => 1,
                        'notificacion_emails'      => 1,
                        'notificacion_alertas'     => 1,
                    ]);
                    $user->provider()->save($provider);
                    $provider->categories()->sync($categories);
                }
            }
            dd("Done");
        }
        $this->load->view('proveedor/excel_upload');
    }
    public function faq_upload()
    {
        if ($_FILES) {
            /* INFORMATION ABOUT THE FILE */
            $path_parts = pathinfo($_FILES["file"]["name"]);
            /* LOADING UPLOAD LIBRARY */
            $config['upload_path']   = './uploads/excel/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']      = '20480';
            $config["file_name"]     = $path_parts['filename'].'_'.uniqid();
            $this->load->library('upload', $config);
            /* UPLOADING FILE */
            if ($this->upload->do_upload("file")) {
                $file        = $this->upload->data()['full_path'];
                $spreadsheet = IOFactory::load($file);
                $spreadsheet->setActiveSheetIndex(1);
                $worksheet = $spreadsheet->getActiveSheet();
                foreach ($worksheet->toArray() as $key => $row) {
                    if ($key < 1) {
                        continue;
                    }
                    $row = array_values($row);

                    $categories = [];
                    foreach ($row as $key2 => $cell) {
                        if ($key2 < 2) {
                            continue;
                        }
                        if ( ! is_null($cell)) {
                            $categories[] = $cell;
                        }
                    }
                    if (count($categories)) {
                        $question              = FAQ::limit($key)->get()->last();
                        $question->category_id = $categories[0];
                        $question->save();

                        if (count($categories) > 1) {
                            unset($categories[0]);
                            $categories = array_values($categories);

                            foreach ($categories as $category) {
                                $newQuestion              = $question->replicate();
                                $newQuestion->category_id = $category;
                                $newQuestion->save();
                            }
                        }
                    }
                }
            }
            dd("Done");
        }
        $this->load->view('proveedor/faq_upload');
    }
    public function registerHubSpot()
    {
        //EXPORT THIS TO EXCEL AND IMPORT INTO HUBSPOT
        $providers = Provider::with('user')->get();
        foreach ($providers as $provider) {
            if (isset($provider->user)) {
                echo(/*$provider->user->correo.' '.$provider->nombre.' '.*/
                    $provider->contacto_telefono."<br>");
            }
        }
        die();
    }
    public function firstLocation()
    {
        if ($_POST) {
            $id_user = $this->session->userdata("id_usuario");
            $data    = [
                "pais"      => $this->input->post("country"),
                "estado"    => $this->input->post("state"),
                "poblacion" => $this->input->post("city"),
            ];
            if ($id_user) {
                $user = User::find($id_user);
                if ($user->client) {
                    if ($data) {
                        $user->client()->update($data);
                    }
                }
                return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(["success" => true, "data" => "ok"]));
            }
        }
        return $this->output->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(['text' => 'Error 404', 'type' => 'danger']));
    }
    public function getCities()
    {
        $state = ($this->input->post('state') === "Todos") ? " " : $this->input->post('state');
        if ($state) {
            $cities = Provider::groupBy("localizacion_poblacion")->having("localizacion_estado", "=", $state)->get();
            return $this->output->set_content_type("application/json")
                ->set_status_header(200)
                ->set_output(json_encode(["success" => true, "data" => $cities]));
        }
        return $this->output->set_content_type("application/json")
            ->set_status_header(404)
            ->set_output(json_encode(["text" => "Error 404", "type" => "danger"]));
    }
    public function existeCategoria()
    {
        $categoriaId = $this->input->get('categoria');
        $category = Category::find($categoriaId);
        return $category ? $this->output->set_content_type("application/json")
                                        ->set_status_header(200)
                                        ->set_output(json_encode(["status" => "Success"]))
                         : $this->output->set_content_type("application/json")
                                        ->set_status_header(404)
                                        ->set_output(json_encode(["status" => "failed"]));
    }
}