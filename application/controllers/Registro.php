<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;
class Registro extends CI_Controller
{
    public $input;

    public function __construct()
    {
        parent::__construct();
        $this->input = new MY_Input();
        $this->load->model("Login_model", "login");
        $this->load->model("Usuario_model", "user");
        $this->load->model("Cliente_model");
        $this->load->model("Boda_model");
        $this->load->model("Invitados/Menu_model", "Menu_model");
        $this->load->model("Invitados/Mesa_model", "Mesa_model");
        $this->load->model("Invitados/Invitado_model", "Invitado_model");
        $this->load->model("Invitados/Grupo_model");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->model("Perfil_model");
        $this->load->model("Proveedor_model");
        $this->load->model("MiPortal/MiPortal_model", "MiPortal_model");
        $this->load->library("checker");
        $this->load->library("My_PHPMailer");
        $this->load->helper("slug");
        $this->mailer = $this->my_phpmailer;
    }

    public function index() {
        $this->load->view("registro/index");
    }

    public function registroNovia()
    {
        if (isset($this->session->userdata()["id_usuario"])) {
            $this->redireccionar();
        }

        if ($_POST) {
            $states              = ["cdmx", "Guanajuato", "Jalisco", "nuevo_leon", "Puebla", "Queretaro", "Sinaloa", "Veracruz"];
            $registeredFromState = $this->input->post("come_from");

            // INSERT BODA
            $data = [
                "fecha_boda"     => $this->input->post("date"),
                "fecha_creacion" => date("Y-m-d H:i:s"),
                "presupuesto"    => 0,
                "no_invitado"    => 0,
            ];

            $id_boda = $this->Boda_model->insertBoda($data);

            // INSERT USUARIO
            $genero = empty($this->input->post("genero")) ? 2 : $this->input->post("genero");

            if ($genero == 1) {
                $data["nombre"]   = $this->input->post("nombre");
                $data["apellido"] = $this->input->post("last_nombre");
            } elseif ($genero == 2) {
                $data["nombre"]   = $this->input->post("nombre");
                $data["apellido"] = $this->input->post("last_nombre");
            }

            $id_usuario = User::create([
                "usuario"         => $this->input->post("correo"),
                "correo"          => $this->input->post("correo"),
                "contrasena"      => sha1(md5(sha1($this->input->post("contrasena")))),
                "rol"             => 2,
                "activo"          => 1,
                "nombre"          => $data["nombre"],
                "apellido"        => $data["apellido"],
                "registered_from" => $registeredFromState ? $registeredFromState : null,
            ])->id_usuario;

            $id_permisos = Profile::create([
                "enviar_mensaje"        => 1,
                "participacion_debates" => 1,
                "valorar_publicaciones" => 1,
                "anadir_amigo"          => 1,
                "aceptar_solicitud"     => 1,
                "mension_posts"         => 1,
                "email_diario"          => 1,
                "email_semanal"         => 1,
                "invitaciones"          => 1,
                "concursos"             => 1,
                "visibilidad_todos"     => 1,
                "email_debate"          => 1,
            ])->id_permisos;

            // INSERT CLIENTE
            $data = [
                "genero"      => $genero,
                "pais"        => null,
                "estado"      => null,
                "poblacion"   => null,
                "telefono"    => $this->input->post("phone"),
                "id_permisos" => $id_permisos,
                "id_boda"     => $id_boda,
                "id_usuario"  => $id_usuario,
            ];

            $id_cliente = $this->Cliente_model->insertCliente($data);

            $id_mesa = Table::create([
                'tipo'        => 3,
                'nombre'      => 'PRINCIPAL',
                'sillas'      => 4,
                'x'           => 425,
                'y'           => 75,
                'orientacion' => 90,
                'id_boda'     => $id_boda,
            ])->id_mesa;

            //$id_mesa = $this->Mesa_model->getMesaPrincipal($id_boda)->id_mesa;
            $id_grupo = Group::create([
                'grupo'   => 'Novios',
                'id_boda' => $id_boda,
            ])->id_grupo;

            $id_menu = Menu::create([
                'id_boda'     => $id_boda,
                'nombre'      => 'Adultos',
                'descripcion' => 'Menu para adultos',
            ])->id_menu;

            //SETUP DEFAULT
            Group::insert([
                ["grupo" => "Familia", "id_boda" => $id_boda],
                ["grupo" => "Amigos", "id_boda" => $id_boda],
            ]);

            Menu::create([
                "id_boda"     => $id_boda,
                "nombre"      => "Niños",
                "descripcion" => "Menu para niños",
            ]);

            $baseChores = Chore::whereNull("id_boda")->get();

            foreach ($baseChores as $chore) {
                $newChore          = $chore->replicate();
                $newChore->id_boda = $id_boda;
                $newChore->save();
            }

            if ($genero == 1) {
                $data = [
                    "nombre"     => $this->input->post("nombre"),
                    "apellido"   => $this->input->post("last_nombre"),
                    "sexo"       => 1,
                    "edad"       => 1,
                    "correo"     => $this->input->post("correo"),
                    "confirmado" => 2,
                    "id_grupo"   => $id_grupo,
                    "id_menu"    => $id_menu,
                    "id_mesa"    => $id_mesa,
                    "silla"      => 2,
                    "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);

                $data = [
                    "nombre"     => 'XXX',
                    "apellido"   => 'XXX',
                    "sexo"       => 2,
                    "edad"       => 1,
                    "confirmado" => 1,
                    "id_grupo"   => $id_grupo,
                    "id_menu"    => $id_menu,
                    "id_mesa"    => $id_mesa,
                    "silla"      => 3,
                    "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);

            } elseif ($genero == 2) {
                $data = [
                    "nombre"     => 'XXX',
                    "apellido"   => 'XXX',
                    "sexo"       => 1,
                    "edad"       => 1,
                    "correo"     => $this->input->post("correo"),
                    "confirmado" => 2,
                    "id_grupo"   => $id_grupo,
                    "id_menu"    => $id_menu,
                    "id_mesa"    => $id_mesa,
                    "silla"      => 2,
                    "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);

                $data = [
                    "nombre"     => $this->input->post("nombre"),
                    "apellido"   => $this->input->post("last_nombre"),
                    "sexo"       => 2,
                    "edad"       => 1,
                    "confirmado" => 1,
                    "id_grupo"   => $id_grupo,
                    "id_menu"    => $id_menu,
                    "id_mesa"    => $id_mesa,
                    "silla"      => 3,
                    "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);
            }

            $data = [
                "usuario"    => $id_usuario,
                //"nombre" => $this->input->post('nombre') . ' ' . $this->input->post('apellido'),
                "rol"        => 2,
                "confirmado" => 1,
                "encryption" => generarCadena(),
                "id_boda"    => $id_boda,
                "id_cliente" => $id_cliente,
                "id_usuario" => $id_usuario,
                "genero"     => (($this->input->post("genero") == 1) ? "Novio" : "Novia"),
            ];

            $data["nombre"] = $this->input->post("nombre");
            $estado = User::find($data['usuario']);

            $this->session->set_userdata($data);

            $this->mailer->to($this->input->post("correo"), $data["nombre"]);
            $this->mailer->setSubject("Bienvenido a BrideAdvisor!");

            $template = $this->load->view("templates/email_welcome", [
                "correo" => $this->input->post("correo"),
                "nombre" => $data["nombre"],
                "estado" => $estado['original']['registered_from'],
            ], true);

            $this->mailer->setSubject("Bienvenido a BrideAdvisor");
            $this->mailer->send($template);

            if (in_array($registeredFromState, $states)) {
                // redirect("home/expo?estado=".$registeredFromState, "refresh");
                redirect("novios/perfil", "refresh");
            } elseif ($registeredFromState == "japy") {
                redirect("novios/perfil", "refresh");
            } else {
                redirect("novios/perfil", "refresh");
            }
        }
        $this->load->view("registro/registroNovia");
    }

    public function facebook()
    {
        $this->output->set_content_type("application/json");
        $response = [
            "code" => 404,
            "data" => [],
        ];

        if ($_POST) {
            $postData = [
                "email"    => $this->input->post("email", true),
                "name"     => $this->input->post("name", true),
                "gender"   => $this->input->post("Gender", true),
                "estado"   => $this->input->post("State", true),
                "password" => base64_encode(random_bytes(10)),
            ];
            //dd($postData);

            $user = User::where("correo", $postData["email"])->where("activo", 1)->first();

            if ($user) {
                $session = [
                    "intento"    => null,
                    "id_usuario" => $user->id_usuario,
                    "usuario"    => $user->id_usuario,
                    "correo"     => $user->correo,
                    "nombre"     => $user->nombre." ".$user->apellido,
                    "rol"        => $user->rol,
                    "encryption" => generarCadena(),
                ];

                if ($user->rol == 2) {
                    $session["id_boda"]    = $user->client->wedding->id_boda;
                    $session["id_cliente"] = $user->client->id_boda;
                    $session["genero"]     = ($user->client->genero == 1 ? "Novio" : "Novia");
                } elseif ($user->rol == 3) {
                    $empresa                     = $this->proveedor->get(["id_usuario" => $session["id_usuario"]]);
                    $session["id_proveedor"]     = $empresa->id_proveedor;
                    $session["nombre_proveedor"] = $empresa->nombre;
                    $session["slug"]             = $empresa->slug;
                }

                $this->session->set_userdata($session);

                $response["code"] = 200;
                $response["data"] = $user;
            } else {
                $user = $this->user->register($postData);

                $response["code"] = 200;
                $response["data"] = $user;
            }
        }
        dd($this->output->set_status_header($response["code"])->set_output(json_encode($response)));
        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function activar($codigo)
    {
        $usuario = $this->user->get(["codigo_activacion" => $codigo]);
        if ($usuario) {
            $this->user->update($usuario->id_usuario, ["codigo_activacion" => null, "activo" => 1]);
            $this->load->view("register/activacion", ["usuario" => $usuario]);

            return;
        }
        show_404();
    }

    public function proveedor()
    {
        if ($_POST) {
            $data["tipo_mensaje"] = "danger";
            $repass               = $this->input->post("repassword", true, "String", ["required" => true, "min" => 8]);

            $usuario = [
                "usuario"    => $this->input->post("usuario", true, "String", ["required" => true, "min" => 6]),
                "contrasena" => $this->input->post("password", true, "String", ["required" => true, "min" => 8]),
                "activo"     => 0,
                "rol"        => Checker::$PROVEEDOR,
                "nombre"     => $this->input->post("nombre", true, "String", ["required" => true]),
                "correo"     => $this->input->post("correo", true, "Email", ["required" => true]),
            ];

            $proveedor = [
                "nombre"                 => $this->input->post("nombre_empresa", true, "String", ["required" => true]),
                "descripcion"            => $this->input->post("descripcion_empresa", true, "String", ["required" => true, "max" => 900]),
                "contacto_telefono"      => $this->input->post("telefono", true, "String", ["required" => true]),
                "contacto_celular"       => $this->input->post("celular", true, "String", ["required" => true]),
                "contacto_pag_web"       => $this->input->post("pagina_web", true, "String", ["required" => true]),
                "localizacion_pais"      => $this->input->post("pais_empresa", true, "String", ["required" => true]),
                "localizacion_cp"        => $this->input->post("cp", true, "Integer", ["required" => true, "min" => 0]),
                "localizacion_estado"    => $this->input->post("estado_empresa", true, "String", ["required" => true]),
                "localizacion_poblacion" => $this->input->post("estado_poblacion", true, "String", ["required" => true]),
                "localizacion_direccion" => $this->input->post("direccion", true, "String", ["required" => true]),
            ];

            $tipos_proveedor = $this->input->post("categories", true, "Array", ["required" => true, "min" => 1, "max" => 4, "display" => "Sector de Actividad"]);

            $exists = $this->Proveedor_model->exitsUserName($usuario["usuario"]);
            if ($exists) {
                $data["mensaje"] = "El nombre de usuario ya existe";
            } else {
                if ($repass == $usuario["contrasena"]) {
                    $usuario["contrasena"] = sha1(md5(sha1($usuario["contrasena"])));
                    if ($this->input->isValid()) {
                        $proveedor["id_tipo_proveedor"] = $tipos_proveedor[0];
                        $newProvider                    = $this->Proveedor_model->insertUsuarioProveedor($usuario,
                            $proveedor, $tipos_proveedor);

                        if ($newProvider) {
                            $user       = User::where("usuario", $usuario["usuario"])->first();
                            $categories = Category::find($this->input->post("categories"));
                            $user->provider->update([
                                "slug" => create_slug($user->provider->id_proveedor, $user->provider->nombre),
                            ]);

                            $user->provider->categories()->attach($categories);
                            $user->provider->registerHubSpot();

                            $this->mailer->to($user->correo, $user->nombre);
                            $this->mailer->setSubject("Bienvenido a Bride Advisor");
                            $template = $this->load->view("templates/proveedor/email_nuevo", ["nombre" => $user->nombre, "correo" => $user->correo], true);
                            $this->mailer->send($template);

                            $this->mailer->to("avazquez@japy.mx", $user->provider->nombre);
                            $this->mailer->setSubject("Nuevo proveedor");
                            $template = $this->load->view("templates/email_proveedor_nuevo", ["correo" => "avazquez@japy.mx", "user" => $user], true);
                            $this->mailer->send($template);

                            $data["titulo"] = "Registro Proveedor";
                            $this->load->view("register/proveedor_ok", $data);

                            return;
                        } else {
                            $data["mensaje"] = "Ocurrio un error intente más tarde";
                        }
                    } else {
                        $data["mensaje"] = $this->input->getErrors();
                    }
                } else {
                    $data["mensaje"] = "Las contraseñas son distintas";
                }
            }
            $data = array_merge($data, $this->input->post());
        }

        $data["titulo"]     = "Registro Proveedor";
        $data["tipos"]      = $this->tipo_proveedor->getAll();
        $data["categories"] = Category::where('parent_id', 1)->get();
        $data["countries"] = $this->getCountries();
        $this->load->view("register/proveedor", $data);
    }

    public function getCountries(){
        $countries = DB::table('pais')->get();
        return $countries;
        // $response['code'] = 202;
        // return $this->output->set_status_header($response["code"])
        //     ->set_output(json_encode($response))
        //     ->set_content_type("application/json");
    }

    public function getStates($id_pais = null, $actual_state = null){
        if(!is_null($actual_state)){
            $response['state'] = DB::table('estado')
                ->where('estado', $actual_state)->first();
            $response['cities'] = $this->getCities_in($response['state']->id_estado);
        }
        if(!is_null($id_pais)){
            $response['states'] = DB::table('estado')
                ->where('id_pais', $id_pais)->get();
            $response['code'] = 202;
            return $this->output->set_status_header($response["code"])
                ->set_output(json_encode($response))
                ->set_content_type("application/json");
        }
        $response['code'] = 203;
        return $this->output->set_status_header($response["code"])
                ->set_output(json_encode($response))
                ->set_content_type("application/json");
    }

    public function getCities_in($id_estado = null){
        if(!is_null($id_estado)){
            $response['cities'] = DB::table('ciudad')
                ->where('id_estado', $id_estado)->get();
            return $response['cities'];
        }
    }

    public function getCities($id_estado = null){
        if(!is_null($id_estado)){
            $response['cities'] = DB::table('ciudad')
                ->where('id_estado', $id_estado)->get();
                return $this->output->set_status_header(202)
                ->set_output(json_encode($response))
                ->set_content_type("application/json");
        }
    }

    public function privacidad()
    {
        $data["titulo"] = "Privacidad";
        $this->load->view("register/privacidad");
    }

    public function ajaxValidateFieldName()
    {
        $fieldId = $this->input->get("fieldId", true, "String", ["required" => true]);
        $value   = $this->input->get("fieldValue", true, "String", ["required" => true]);
        if ($this->input->isValid()) {
            $exits = $this->Proveedor_model->exitsUserName($value);
            if ( ! $exits) {
                echo '["'.$fieldId.'", true, "ok"]';

                return;
            }
        }
        echo '["'.$fieldId.'", false, "El usuario ya existe"]';
    }

    public function checkUsuario()
    {
        return $this->output
            ->set_content_type("application/json")
            ->set_status_header(202)
            ->set_output(json_encode(
                    [
                        "success" => true,
                        "data"    => User::where("correo", $_POST["correo"])->exists(),
                    ]
                )
            );
    }

    public function expo()
    {
        $this->load->view("expo_registro/index");
    }

    /*    public function finishRegistration()
        {
            //THE USERS GO FROM 1622 TO 2672 WITH ROL 2!!!!

            //DEACTIVATE ALL THE USERS WHEN REGISTERING THEM BECAUSE THEY COULD LEAVE IN THE MIDDLE OF THE REGISTRATION
            $response = [];
            $email = $this->input->get("email");

        $user = User::where("activo", 0)
                ->where("correo", $email)
                ->doesntHave("client")
                ->where("rol", 2)
                ->first();

        if ($_POST && $user) {
            // INSERT BODA
            $data = [
                    "fecha_boda"     => $this->input->post("fecha_boda"),
                    "fecha_creacion" => date("Y-m-d H:i:s"),
                    "presupuesto"    => $this->input->post("presupuesto"),
                    "no_invitado"    => ( ! empty($this->input->post("no_invitados"))) ? $this->input->post("no_invitados") :0,
            ];

                $id_boda = $this->Boda_model->insertBoda($data);

                // INSERT USUARIO
                $genero = empty($this->input->post("tipo")) ? 1 : $this->input->post("tipo");

                if ($genero == 1) {
                    $data["nombre"] = $this->input->post("nombre_novio");
                    $data["apellido"] = $this->input->post("apellido_novio");
                } elseif ($genero == 2) {
                    $data["nombre"] = $this->input->post("nombre_novia");
                    $data["apellido"] = $this->input->post("apellido_novia");
                }

            User::where("correo", $this->input->post("correo"))->update([
                    "contrasena" => sha1(md5(sha1($this->input->post("contrasena")))),
                    "rol"        => 2,
                    "activo"     => 1,
                    "nombre"     => $data["nombre"],
                    "apellido"   => $data["apellido"],
            ]);

                $id_usuario = User::where("correo", $this->input->post("correo"))->first()->id_usuario;

            $id_permisos = Profile::create([
                    "enviar_mensaje"        => 1,
                    "participacion_debates" => 1,
                    "valorar_publicaciones" => 1,
                    "anadir_amigo"          => 1,
                    "aceptar_solicitud"     => 1,
                    "mension_posts"         => 1,
                    "email_diario"          => 1,
                    "email_semanal"         => 1,
                    "invitaciones"          => 1,
                    "concursos"             => 1,
                    "visibilidad_todos"     => 1,
                    "email_debate"          => 1,
            ])->id_permisos;

                // INSERT CLIENTE
                $data = [
                        "genero"      => $this->input->post("tipo"),
                        "pais"        => $this->input->post("country"),
                        "estado"      => $this->input->post("state"),
                        "poblacion"   => $this->input->post("city"),
                        "id_permisos" => $id_permisos,
                        "id_boda"     => $id_boda,
                        "id_usuario"  => $id_usuario,
                ];
            // INSERT CLIENTE
            $data = [
                "genero" => $this->input->post("genero"),
                "pais" => null,
                "estado" => null,
                "poblacion" => null,
                "id_permisos" => $id_permisos,
                "id_boda" => $id_boda,
                "id_usuario" => $id_usuario,
            ];

                $id_cliente = $this->Cliente_model->insertCliente($data);

                $id_mesa = Table::create([
                        "tipo"        => 3,
                        "nombre"      => "PRINCIPAL",
                        "sillas"      => 4,
                        "x"           => 425,
                        "y"           => 75,
                        "orientacion" => 90,
                        "id_boda"     => $id_boda,
                ])->id_mesa;
            $id_mesa = Table::create([
                "tipo" => 3,
                "nombre" => "PRINCIPAL",
                "sillas" => 4,
                "x" => 425,
                "y" => 75,
                "orientacion" => 90,
                "id_boda" => $id_boda,
            ])->id_mesa;

                $id_grupo = Group::create([
                        "grupo"   => "Novios",
                        "id_boda" => $id_boda,
                ])->id_grupo;
            $id_grupo = Group::create([
                "grupo" => "Novios",
                "id_boda" => $id_boda,
            ])->id_grupo;

                $id_menu = Menu::create([
                        "id_boda"     => $id_boda,
                        "nombre"      => "Adultos",
                        "descripcion" => "Menu para adultos",
                ])->id_menu;
            $id_menu = Menu::create([
                "id_boda" => $id_boda,
                "nombre" => "Adultos",
                "descripcion" => "Menu para adultos",
            ])->id_menu;

            Menu::create([
                "id_boda" => $id_boda,
                "nombre" => "Niños",
                "descripcion" => "Menu para niños",
            ]);
                Menu::create([
                        "id_boda"     => $id_boda,
                        "nombre"      => "Niños",
                        "descripcion" => "Menu para niños",
                ]);

            //SETUP DEFAULT
            Group::insert([
                    ["grupo" => "Familia", "id_boda" => $id_boda],
                    ["grupo" => "Amigos", "id_boda" => $id_boda],
            ]);

            // UPDATE MIPORTAL
            $rm = [
                //"nosotros" => $this->input->post("nombre") . " " . $this->input->post("apellido"),
                    "fecha_boda" => $this->input->post("fecha_boda"),
            ];

                $baseChores = Chore::whereNull("id_boda")->get();

                foreach ($baseChores as $chore) {
                    $newChore = $chore->replicate();
                    $newChore->id_boda = $id_boda;
                    $newChore->save();
                }

                $novia  = $this->input->post("nombre_novia")." ".$this->input->post("apellido_novia");
                $novio  = $this->input->post("nombre_novio")." ".$this->input->post("apellido_novio");
                $genero = $this->input->post("tipo");

                $rm["nosotros"] = "$novio & $novia";

             $this->MiPortal_model->update($rm, $id_boda);

                $data = [
                    "nombre"     =>

                $this->input->post("nombre_novio"),
                    "apellido" => $this->input->post("apellido_novio"),
                    "sexo" => 1,
                    "edad" => 1,
                    "correo" => $this->input->post("correo"),
                    "confirmado" => 2,
                    "id_grupo" => $id_grupo,
                    "id_menu" => $id_menu,
                    "id_mesa" => $id_mesa,
                    "silla" => 2,
                    "id_boda" => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);

                $data = [
                        "nombre"     => $this->input->post("nombre_novia"),
                        "apellido"   => $this->input->post("apellido_novia"),
                        "sexo"       => 2,
                        "edad"       => 1,
                        "confirmado" => 1,
                        "id_grupo"   => $id_grupo,
                        "id_menu"    => $id_menu,
                        "id_mesa"    => $id_mesa,
                        "silla"      => 3,
                        "id_boda"    => $id_boda,
                ];

                $this->Invitado_model->insertInvitado($data);

            $data = [
                    "usuario"     => $id_usuario,
                //"nombre" => $this->input->post('nombre') . ' ' . $this->input->post('apellido'),
                    "rol"         => 2,
                    "confirmado"  => 1,
                    "encryption"  => generarCadena(),
                    "id_boda"     => $id_boda,
                    "id_cliente"  => $id_cliente,
                    "id_usuario"  => $id_usuario,
                    "id_miportal" => is_object($this->MiPortal_model->getIDPortal($id_boda)) ? $this->MiPortal_model->getIDPortal($id_boda)->id_miportal : false,
                    "genero"      => (($this->input->post('tipo') == 1) ? 'Novio' : 'Novia'),
            ];

                $nombre = "";
                $genero = $this->input->post("tipo");

                if ($genero == 1) {
                    $data['nombre'] = $this->input->post("nombre_novio") . ' ' . $this->input->post('apellido_novio');
                    $nombre = $this->input->post("nombre_novio");
                } elseif ($genero == 2) {
                    $data['nombre'] = $this->input->post("nombre_novia") . ' ' . $this->input->post('apellido_novia');
                    $nombre = $this->input->post("nombre_novia");
                }

                $this->session->set_userdata($data);

                $this->mailer->to($this->input->post("correo"), $nombre);

            $template = $this->load->view("templates/email_usuario_nuevo", [
                    "correo" => $this->input->post('correo'),
                    "nombre" => $nombre,
            ], true);

                $this->mailer->send($template);

                redirect("novios/presupuesto", "refresh");
            } elseif ($user) {
                $response["user"] = $user;

                return $this->load->view("register/finishRegistration", $response);
            }

            redirect("register", "refresh");
        }*/

    private function redireccionar()
    {
        $id_rol = $this->session->userdata("rol");

        switch ($id_rol) {
            case Checker::$ADMIN:
                redirect("App/");
                break;
            case Checker::$NOVIO:
                redirect("novios/presupuesto");
                break;
            case Checker::$REDACTOR:
                redirect("blog/");
                break;
            case Checker::$PROVEEDOR:
                redirect("proveedor/");
                break;
            case Checker::$ADMINAPP:
                redirect("App/");
                break;
            case Checker::$MODERADOR:
                redirect("");
                break;
            case Checker::$GALERIA:
                redirect("ADGaleria/");
                break;
        }
    }

}
