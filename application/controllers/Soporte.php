<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Soporte extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("Checker");
        $this->load->model("Usuario_model", "usuario");
        $this->load->library("My_PHPMailer");

        $this->mailer = $this->my_phpmailer;
    }

    public function politica_de_datos()
    {
        $this->load->view("sopport/politica");
    }

    public function declaracion_derechos_responsabilidades()
    {
        $this->load->view("sopport/declaracion_derechos");
    }

    public function contrasena()
    {
        if ($_POST) {
            $correo = $this->input->post("correo", true, "Email", ["required" => true]);
            if ($this->input->isValid()) {
                $data['error'] = null;
                $user          = $this->usuario->getFromCorreo($correo);

                if (is_null($user)) {
                    $data['error'] = "No existe un usuario con ese correo";
                } else {
                    if ( ! $user->activo) {
                        $data['error'] = "Tu usuario aun no ha sido activado";
                    } else {
                        $pass = $this->checker->rand(5);

                        $this->usuario->update($user->id_usuario, ["contrasena" => sha1(md5(sha1($pass)))]);

                        $this->mailer->to($correo, $user->nombre);

                        $template = $this->load->view("templates/change_password", [
                                "correo"   => $correo,
                                "password" => $pass,
                                "nombre"   => $user->nombre,
                        ], true);
                        $this->mailer->setSubject("Recuperacion de contraseña a Japy");
                        $this->mailer->send($template);
                    }
                }
                $this->mailer->setSubject("Soporte de Japy");
                $this->load->view("sopport/recover_ok", $data);

                return 1;
            }
        }

        $this->load->view("sopport/recuperar");
    }

    public function recuperar_ok($data)
    {
        $this->load->view("sopport/recuperar_ok", $data);
    }


}
