<?php

class StaticBlogs extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Cliente_model");
        $this->load->model("Boda_model");
        $this->load->model("Invitados/Invitado_model", "Invitado_model");
        $this->load->library("checker");
        $this->load->library("My_PHPMailer");
        $this->load->helper("slug");
        $this->mailer = $this->my_phpmailer;
    }

    public function bengy_olivares()
    {
        $this->load->view("static_blogs/bengy_olivares");
    }
    public function melissa_cueva()
    {
        $this->load->view("static_blogs/melissa_cueva");
    }
    public function victoria_samano()
    {
        $this->load->view("static_blogs/victoria_samano");
    }
    public function pablo_saracho()
    {
        $this->load->view("static_blogs/pablo_saracho");
    }
    public function manuel_diaz() {
        $this->load->view("static_blogs/manuel_diaz");
    }
    public function bw() {
        $this->load->view("static_blogs/bride_weekend");
    }
    public function bride_w() {
        $this->load->view("static_blogs/bride_weekend");
    }
    public function tendencia_oscars() {
        $this->load->view("static_blogs/oscars");
    }

}