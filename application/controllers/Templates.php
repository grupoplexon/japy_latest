<?php
/**
 * Created by PhpStorm.
 * User: raulm
 * Date: 22/06/2018
 * Time: 04:33 PM
 */

class Templates extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view("templates/email_confirmacion");
    }

}