<?php
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Database\Capsule\Manager as DB;

class Tendencia extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("Checker");
        $this->load->model("Vestido/Tipo_model", "tipo");
        $this->load->model("Vestido/Vestido_model", "vestido");
        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Vestido/VestidoUsuario_model", "like");
        $this->load->model("Vestido/VestidoComentario_model", "comentario");
        $this->load->helper("formats");
    }
    public function index($tipo = null)
    {
        $data["vestido"] = DB::table('tipo_vestido')->select('id_tipo_vestido','nombre', 'slug')->get();
        $tipo_id = DB::table('tipo_vestido')->select('id_tipo_vestido')->where('slug', $tipo)->first();

        if($tipo_id){
            $tipo_id =  $tipo_id->id_tipo_vestido;
            $data['value'] = $tipo_id;
        }else{
            $tipo_id = 1;
            $data['value'] = 1;
        }
        $data["tendencias"] = DB::table('vestido')->where('tipo_vestido', $tipo_id ? $tipo_id : 1)->get(); 
        $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('disenador')->select('disenador')->get();
        $data["disenador"] = DB::table('vestido')->where('tipo_vestido', $tipo_id ? $tipo_id : 1)->groupBy('disenador')->select('disenador')->get();
        $data["temporada"] = DB::table('vestido')->where('tipo_vestido', $tipo_id ? $tipo_id : 1)->groupBy('temporada')->select('temporada')->get();
        $data["corte"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('corte')->select('corte')->get();
        $data["escote"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('escote')->select('escote')->get();
        $data["largo"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('largo')->select('largo')->get();
        $data["tipo"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('tipo')->select('tipo')->get();
        $data["estilo"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('estilo')->select('estilo')->get();
        $data["categoria"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('categoria')->select('categoria')->get();
        
        $this->load->view("tendencia/index", $data);
    }
    public function categoria()
    {
        // $tipo = null, $disenador = null
        $tipo = $this->input->get("tipo");
        $disenador = $this->input->get("disenador");
        $corte = $this->input->get("corte");
        $escote = $this->input->get("escote");
        $largo = $this->input->get("largo");
        $tipo1 = $this->input->get("tipo1");
        $estilo = $this->input->get("estilo");
        $category = $this->input->get("category");
        $temporada = $this->input->get("temporada");
        
        $data["vestido"] = DB::table('tipo_vestido')->select('id_tipo_vestido','nombre', 'slug')->get();
        $tipo_id = DB::table('tipo_vestido')->select('id_tipo_vestido')->where('slug', $tipo)->first();
        if($tipo_id){
            $tipo_id =  $tipo_id->id_tipo_vestido;
            $data['value'] = $tipo_id;
        }else{
            $tipo_id = 1;
            $data['value'] = 1;
        }
        $data['value_dis'] = $disenador;
        $data['value_corte'] = $corte;
        $data['value_escote'] = $escote;
        $data['value_largo'] = $largo;
        $data['value_estilo'] = $estilo;
        $data['value_tipo'] = $tipo1;
        $data['value_category'] = $category;
        $data['value_temporada'] = $temporada;
        
        $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('disenador')->select('disenador')->get();
        ///// VESTIDOS DE NOVIA
        if($tipo=="vestidos-novia"){
            $data["tendencias"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($corte, function ($query) use ($corte) {
                    return $query->where('corte', $corte);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('corte');
                })
            ->when($escote, function ($query) use ($escote) {
                return $query->where('escote', $escote);
            }, function ($query) use ($escote){
                 return $query->whereNotNull('escote');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->get();
            
            $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($corte, function ($query) use ($corte) {
                    return $query->where('corte', $corte);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('corte');
                })
            ->when($escote, function ($query) use ($escote) {
                return $query->where('escote', $escote);
            }, function ($query) use ($escote){
                 return $query->whereNotNull('escote');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->groupBy('disenador')->select('disenador')->get();
        }///// TRAJES DE NOVIO
        else if($tipo=="trajes-novio"){
            $data["tendencias"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($estilo, function ($query) use ($estilo) {
                return $query->where('estilo', $estilo);
            }, function ($query) use ($estilo){
                 return $query->whereNotNull('estilo');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->get();
            
            $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($estilo, function ($query) use ($estilo) {
                return $query->where('estilo', $estilo);
            }, function ($query) use ($estilo){
                 return $query->whereNotNull('estilo');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->groupBy('disenador')->select('disenador')->get();
            
        }///// VESTIDOS DE FIESTA
        else if($tipo=="vestidos-fiesta"){
            $data["tendencias"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($largo, function ($query) use ($largo) {
                return $query->where('largo', $largo);
            }, function ($query) use ($largo){
                 return $query->whereNotNull('largo');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->get();
            
            $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($largo, function ($query) use ($largo) {
                return $query->where('largo', $largo);
            }, function ($query) use ($largo){
                 return $query->whereNotNull('largo');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->groupBy('disenador')->select('disenador')->get();
            
        }///// ZAPATOS
        else if($tipo=="zapatos"){
            $data["tendencias"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($category, function ($query) use ($category) {
                return $query->where('categoria', $category);
            }, function ($query) use ($category){
                 return $query->whereNotNull('categoria');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->get();
            
            $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($category, function ($query) use ($category) {
                return $query->where('categoria', $category);
            }, function ($query) use ($category){
                 return $query->whereNotNull('categoria');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->groupBy('disenador')->select('disenador')->get();
            
            
        }///// JOYERIA
        else if($tipo=="joyeria"){
            $data["tendencias"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($tipo1, function ($query) use ($tipo1) {
                return $query->where('tipo', $tipo1);
            }, function ($query) use ($tipo1){
                 return $query->whereNotNull('tipo');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->get();
            
            $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($tipo1, function ($query) use ($tipo1) {
                return $query->where('tipo', $tipo1);
            }, function ($query) use ($tipo1){
                 return $query->whereNotNull('tipo');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->groupBy('disenador')->select('disenador')->get();
            
            
        }///// LENCERIA
        else if($tipo=="lenceria"){
            $data["tendencias"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($tipo1, function ($query) use ($tipo1) {
                return $query->where('tipo', $tipo1);
            }, function ($query) use ($tipo1){
                 return $query->whereNotNull('tipo');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->get();
            
            $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($tipo1, function ($query) use ($tipo1) {
                return $query->where('tipo', $tipo1);
            }, function ($query) use ($tipo1){
                 return $query->whereNotNull('tipo');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->groupBy('disenador')->select('disenador')->get();
            
            
        }///// COMPLEMENTOS
        else if($tipo=="complementos"){
            $data["tendencias"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($tipo1, function ($query) use ($tipo1) {
                return $query->where('tipo', $tipo1);
            }, function ($query) use ($tipo1){
                 return $query->whereNotNull('tipo');
            })
             ->when($category, function ($query) use ($category) {
                return $query->where('categoria', $category);
            }, function ($query) use ($category){
                 return $query->whereNotNull('categoria');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->get();
            
            $data["disenadores"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )
            ->when($disenador, function ($query) use ($disenador) {
                    return $query->where('disenador', $disenador);
                }, function ($query) use ($corte){
                     return $query->whereNotNull('disenador');
                })
            ->when($tipo1, function ($query) use ($tipo1) {
                return $query->where('tipo', $tipo1);
            }, function ($query) use ($tipo1){
                 return $query->whereNotNull('tipo');
            })
             ->when($category, function ($query) use ($category) {
                return $query->where('categoria', $category);
            }, function ($query) use ($category){
                 return $query->whereNotNull('categoria');
            })
            ->when($temporada, function ($query) use ($temporada) {
                    return $query->where('temporada', $temporada);
                }, function ($query) use ($temporada){
                     return $query->whereNotNull('temporada');
                })
            ->groupBy('disenador')->select('disenador')->get();
            
            
        }
        $data["disenador"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('disenador')->select('disenador')->get();
        $data["temporada"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('temporada')->select('temporada')->get();
        $data["corte"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('corte')->select('corte')->get();
        $data["escote"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('escote')->select('escote')->get(); 
        $data["largo"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('largo')->select('largo')->get();
        $data["tipo"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('tipo')->select('tipo')->get();
        $data["estilo"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('estilo')->select('estilo')->get();
        $data["categoria"] = DB::table('vestido')->where('tipo_vestido', $tipo_id )->groupBy('categoria')->select('categoria')->get();
        $this->load->view("tendencia/index", $data);
        // show_404();
    }
    public function post()
    {
        $id = $this->input->get("modelo");
        $data['post'] = DB::table('vestido')->where('nombre', $id)->first();
        $this->load->view("tendencia/post", $data);
    }
    public function like_vestido()
    {
        $this->output->set_content_type("application/json");
        $id_cliente = $this->session->userdata("id_cliente");
        $vestido =  $this->input->post("vestido");
        if ($_POST && $id_cliente) {
            $registro = DB::table('vestido_usuario')->where('id_cliente', $id_cliente)->where('id_vestido', $vestido)->first();
            if($registro){
                return $this->output->set_status_header(202)
                    ->set_output(json_encode(array("success" => true, "data" => "ok")));
            }else{
                return $this->output->set_status_header(202)
                    ->set_output(json_encode(array("success" => true, "data" => "error")));
            }
        }
        return $this->output->set_status_header(202)
                    ->set_output(json_encode(array("success" => true, "data" => "error")));
    }
    public function create_like(){
        $this->output->set_content_type("application/json");
        
        $id_cliente = $this->session->userdata("id_cliente");
        $vestido =  $this->input->post("vestido");
        
        if ($_POST && $id_cliente) {
            $registro = DB::table('vestido_usuario')->insert([
               'id_cliente' =>$id_cliente,
               'id_vestido' =>$vestido]);
            return $this->output->set_status_header(202)
                ->set_output(json_encode(array("success" => true, "data" => "ok")));
        }
        return $this->output->set_status_header(404);
    }
    public function delete_like(){
        $this->output->set_content_type("application/json");
         
        $id_cliente = $this->session->userdata("id_cliente");
        $vestido =  $this->input->post("vestido");
        
        if ($_POST && $id_cliente) {
            DB::table('vestido_usuario')->where('id_cliente', $id_cliente)->where('id_vestido', $vestido)->delete();
            
            return $this->output->set_status_header(202)
                ->set_output(json_encode(array("success" => true, "data" => "ok")));
        }
        return $this->output->set_status_header(404);
    }
    public function saveComment(){
        $this->output->set_content_type("application/json");
         
        $id_cliente = $this->session->userdata("id_usuario");
        $vestido =  $this->input->post("vestido");
        $comentario =  $this->input->post("comment");
        if ($_POST && $id_cliente) {
            $registro = DB::table('vestido_comentario')->insert([
               'id_usuario' =>$id_cliente,
               'mensaje' => $comentario,
               'id_vestido' =>$vestido]);
            
            return $this->output->set_status_header(202)
                ->set_output(json_encode(array("success" => true, "data" => "ok")));
        }
        return $this->output->set_status_header(404);
    }
    public function imagen_vestido($otro_dato, $id_vestido = null)
    {
        if (is_numeric($otro_dato)) {
            $id_vestido = $otro_dato;
        }
        $expl    = explode(".", $id_vestido);
        $ves     = $expl[0];
        $vestido = $this->vestido->getVestido($ves);
        if ($vestido) {
            if ($vestido->imagen) {
//                $this->output->cache(604800);
                ob_end_clean();
                header('Content-Length:	'.strlen($vestido->imagen));
                header("Last-Modified: Tue, 25 Oct 2016 21:27:04 GMT");
                header("Expires: Tue, 25 Oct 2022 21:27:04 GMT");
                header("Accept-Ranges: bytes");
                header("Content-Type: image/jpeg");
                header("Pragma:	cache");
                header("Cache-Control: max-age=172800, public, must-revalidate");
                $timestamp = date("now");
                $tsstring  = gmdate('D, d M Y H:i:s ', $timestamp).'GMT';
                $etag      = "es_ES".$timestamp;

                $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false;
                $if_none_match     = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'] : false;
                if ((($if_none_match && $if_none_match == $etag) || ( ! $if_none_match))
                    && ($if_modified_since && $if_modified_since == $tsstring)
                ) {
                    header('HTTP/1.1 304 Not Modified');
                    exit();
                } else {
//                    header("Last-Modified: $tsstring");
                    header("ETag: \"{$etag}\"");
                }
                header('Content-Disposition: inline; filename='.basename($id_vestido).';');
//                $img = imagecreate(100, 100); //Create an image 100px x 100px
//                imagecolorallocate($img, 255, 0, 0); //Fill the image red
//                header('Content-type: image/jpeg'); //Set the content type to image/jpg
//                imagejpeg($img); //Output the iamge
//                imagedestroy($img); //Destroy the image
                echo($vestido->imagen);
                die();
                exit;
                $this->output->set_content_type("image/jpeg")
                    ->set_status_header(200)
                    ->set_output($vestido->imagen)
                    ->_display();
                exit;
            }
            return redirect($this->config->base_url()."dist/img/blog/perfil.png");
        }
        return redirect($this->config->base_url()."dist/img/blog/perfil.png");
    }
    public function disenadores($tipo = null)
    {
        $tipo                    = urldecode($tipo);
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"]        = $this->tipo->existsProducto($tipo ? $tipo : "Vestidos de Novia", $data["tipos_productos"]);
        if ($data["producto"]) {
            $data["title"]       = $data["producto"]->nombre;
            $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
            $data["temporadas"]  = $this->vestido->getTemporadas($data["producto"]->id_tipo_vestido);
            $data["destacados"]  = $this->vestido->getDisenadoresAll($data["producto"]->id_tipo_vestido);
            $this->load->view("principal/vestidos/disenadores", $data);
            return;
        }
        show_404();
    }
    private function getParamsSearch($sortable)
    {
        $param = array();
        foreach ($sortable as $key => $s) {
            $temp = $this->input->get($s->nombre, true);
            if ($temp) {
                $param[$s->nombre] = $temp;
            }
        }
        $temp = $this->input->get("disenador", true);
        if ($temp) {
            $param["disenador"] = $temp;
        }
        return $param;
    }
    public function catalogo($tipo = null)
    {
        $tipo                    = urldecode($tipo);
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"]        = $this->tipo->existsProducto($tipo ? $tipo : "Vestidos de Novia", $data["tipos_productos"]);

        if ($data["producto"]) {
            $data["param"]       = $this->getParamsSearch($data["producto"]->sortable);
            $data["title"]       = $data["producto"]->nombre;
            $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
            $data["temporadas"]  = $this->vestido->getTemporadas($data["producto"]->id_tipo_vestido);
            $page                = $this->input->get("pag", true, "Integer", ["required" => true, "min" => 1]);
            if ( ! $page) {
                $page = 1;
            }
            $data["page"]     = $page;
            $data["total"]    = $this->vestido->getTotalCatalogo($data["producto"]->id_tipo_vestido, $data["param"]);
            $data["catalogo"] = $this->vestido->getCatalogo($data["producto"]->id_tipo_vestido, $data["param"], $page);
            if ($data["catalogo"]) {
                foreach ($data["catalogo"] as $key => &$value) {
                    $value->like     = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                    $value->likes    = $this->vestido->countLikes($value->id_vestido);
                    $value->comments = $this->vestido->countComentarios($value->id_vestido);
                }
            }
            $data["destacados"] = $this->vestido->getDisenadoresDestacados($data["producto"]->id_tipo_vestido);

            $this->load->view("principal/vestidos/catalogo", $data);
            return;
        }
        show_404();
    }
    public function disenador($tipo, $nombre)
    {
        $tipo                    = urldecode($tipo);
        $nombre                  = urldecode($nombre);
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"]        = $this->tipo->existsProducto($tipo, $data["tipos_productos"]);
        if ($data["producto"]) {
            if ($this->vestido->existsDisenador($nombre)) {
                $pag = $this->input->get("pag", true, "Integer", array("required" => false));
                if ( ! $pag) {
                    $pag = 1;
                }
                $data["pag"]         = $pag;
                $data["disenador"]   = urldecode(str_replace("-", " ", $nombre));
                $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
                $data["title"]       = "Disenador: ".$data["disenador"];
                $data["vestidos"]    = $this->vestido->getAllDisenador($data["disenador"], $pag);
                $data["param"]       = array();
                if ($data["vestidos"]) {
                    foreach ($data["vestidos"] as $key => &$value) {
                        $value->like     = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                        $value->likes    = $this->vestido->countLikes($value->id_vestido);
                        $value->comments = $this->vestido->countComentarios($value->id_vestido);
                    }
                }
                $data["total"]      = $this->vestido->getTotalAllDisenador($data["disenador"]);
                $data["temporadas"] = $this->vestido->getTemporadasDisenador($data["disenador"]);

                return $this->load->view("principal/vestidos/disenador", $data);
            }
        }
        show_404();
    }
    public function filtrar($tipo, $nombre)
    {
        $tipo                    = urldecode($tipo);
        $nombre                  = urldecode($nombre);
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"]        = $this->tipo->existsProducto($tipo, $data["tipos_productos"]);
        if ($data["producto"]) {
            if ($this->vestido->existsDisenador($nombre)) {
                $pag = $this->input->get("pag", true, "Integer", array("required" => false));
                if ( ! $pag) {
                    $pag = 1;
                }
                $data["pag"]         = $pag;
                $data["disenador"]   = urldecode(str_replace("-", " ", $nombre));
                $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
                $data["title"]       = "Disenador: ".$data["disenador"];
                $data["vestidos"]    = $this->vestido->getAllDisenador($data["disenador"], $pag);
                if ($data["vestidos"]) {
                    foreach ($data["vestidos"] as $key => &$value) {
                        $value->like = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                    }
                }
                $data["total"]      = $this->vestido->getTotalAllDisenador($data["disenador"]);
                $data["temporadas"] = $this->vestido->getTemporadasDisenador($data["disenador"]);

                return $this->load->view("principal/vestidos/disenador", $data);
            }
        }
        show_404();
    }
    public function articulo($disenador, $modelo)
    {
        $data["disenador"] = $disenador;
        $data["modelo"]    = $modelo;
        $data["vestido"]   = $this->vestido->get(array(
            "nombre"    => urldecode(str_replace("-", " ", $modelo)),
            "disenador" => urldecode(str_replace("-", " ", $disenador)),
        ));
        if ($data["vestido"]) {
            $data["producto"] = $this->tipo->get($data["vestido"]->tipo_vestido);
            if ($data["producto"]) {
                $data["title"]         = $data["producto"]->nombre.": ".$data["vestido"]->nombre;
                $data["vestido"]->like = $this->vestido->isLikeForMe($this->session->id_cliente, $data["vestido"]->id_vestido);
                $data["total"]         = $this->vestido->countDisenador($data["vestido"]->disenador);
                $data["vestidos"]      = $this->vestido->getListOrderBy(array(
                    "tipo_vestido" => $data["vestido"]->tipo_vestido,
                    "disenador"    => urldecode(str_replace("-", " ", $disenador)),
                ), " nombre ");
                $data["posicion"] = 0;
                foreach ($data["vestidos"] as $key => $value) {
                    $data["posicion"]++;
                    if ($value->id_vestido == $data["vestido"]->id_vestido) {
                        break;
                    }
                }
                $data["comentarios"] = $this->comentario->getComentariosArticulosInit($data["vestido"]->id_vestido);
                foreach ($data["comentarios"] as $key => &$value) {
                    $value->usuario = $this->usuario->get($value->id_usuario);
                    if ( ! $value->usuario) {
                        unset($data['comentarios'][$key]);
                    }
                }
                $data['comentarios'] = array_values($data['comentarios']);
                return $this->load->view("principal/vestidos/articulo", $data);
            }
        }
        show_404();
    }
    public function like()
    {
        $this->output->set_content_type("application/json");
        $id_cliente = $this->session->userdata("id_cliente");
        if ($_POST && $id_cliente) {
            $vestido = $this->input->post("vestido", true, "Integer", array("required" => true, "min" => 1));
            $v       = $this->vestido->get($vestido);
            if ($v && $this->input->isValid()) {
                $like = $this->like->get(array(
                    "id_vestido" => $v->id_vestido,
                    "id_cliente" => $id_cliente,
                ));
                if ( ! $like) {
                    $this->like->insert(array(
                        "id_vestido" => $v->id_vestido,
                        "id_cliente" => $id_cliente,
                    ));
                } else {
                    $this->like->delete($like->id_vestido_usuario);
                }
                return $this->output->set_status_header(202)
                    ->set_output(json_encode(array("success" => true, "data" => "ok")));
            }
        }
        return $this->output->set_status_header(404);
    }
    public function comentar()
    {
        $this->output->set_content_type("application/json");
        $id_cliente = $this->session->userdata("id_cliente");
        if ($_POST && $id_cliente) {
            $c = array(
                "id_usuario" => $this->session->userdata("id_usuario"),
                "id_vestido" => $this->input->post("vestido", true, "Integer", array("required" => true, "min" => 1)),
                "parent"     => $this->input->post("id_comment", true, "Integer", array("required" => false, "min" => 1)),
                "mensaje"    => $this->input->post("comment", true, "String", array("required" => true)),
            );
            $v = $this->vestido->get($c["id_vestido"]);
            if ($v && $this->input->isValid()) {
                $b = $this->comentario->insert($c);
                if ($b) {
                    return $this->output->set_status_header(202)
                        ->set_output(json_encode(array("success" => true, "data" => "ok")));
                }
            }
        }
        return $this->output->set_status_header(404);
    }
    public function comentarios()
    {
        $this->output->set_content_type("application/json");
        if ($_POST) {
            $parent = $this->input->post("comment", true, "Integer", array("required" => true, "min" => 1));
            if ($this->input->isValid()) {
                $comentarios = $this->comentario->getList(array("parent" => $parent));
                foreach ($comentarios as $key => &$value) {
                    $value->usuario = $this->comentario->getUsuario($value->id_usuario);
                }
                return $this->output->set_status_header(202)
                    ->set_output(json_encode(array("success" => true, "data" => $comentarios)));
            }
        }
        return $this->output->set_status_header(404);
    }
}