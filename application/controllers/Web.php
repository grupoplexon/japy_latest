<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model('Blog/Post_model', 'post');
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->helper("formats");
        $this->load->model('Correo/Correo_model', 'Correo_model');
        $this->load->model('Proveedor/Solicitud_model', 'Solicitud_model');
        $this->load->model('MiPortal/MiPlantilla_model', 'MiPlantilla_model');
        $this->load->model('MiPortal/MiPortal_model', 'MiPortal_model');
        $this->load->model('MiPortal/MiPortalSeccion_model', 'MiPortalSeccion_model');
        $this->load->model('MiPortal/MiPortalLibro_model', 'MiPortalLibro_model');
        $this->load->model('MiPortal/MiPortalEvento_model', 'MiPortalEvento_model');
        $this->load->model('MiPortal/MiPortalEncuesta_model', 'MiPortalEncuesta_model');
        $this->load->model('MiPortal/MiPortalBlog_model', 'MiPortalBlog_model');
        $this->load->model('MiPortal/MiPortalDireccion_model', 'MiPortalDireccion_model');
        $this->load->model('MiPortal/MiPortalVideo_model', 'MiPortalVideo_model');
        $this->load->model('MiPortal/MiPortalAlbum_model', 'MiPortalAlbum_model');
        $this->load->model('MiPortal/MiPortalTest_model', 'MiPortalTest_model');
        $this->load->model('MiPortal/MiPortalBlog_model', 'MiPortalBlog_model');
        $this->load->model('MiPortal/MiPortalVisita_model', 'MiPortalVisita_model');
        $this->load->model('MiPortal/MiPortalContactar_model', 'MiPortalContactar_model');
        $this->load->model('MiPortal/MiPortalSeccion_Imagen_model', 'MiPortalSeccionImagen_model');
    }

    public function index($id_web, $seccion = null)
    {
        if (empty($id_web)) {
            redirect('Home');
        }

        $seccion = ($seccion == null) ? 'bienvenido' : $seccion;
        $seccion = strtolower($seccion);

        $return = $this->information_default($id_web, $seccion);

        switch ($seccion) {
            case 'bienvenido':
                $return['bienvenido'] = $this->MiPortalSeccion_model->getImagen('bienvenido');
                $this->load->view('portalweb_visitante/index', $return);
                break;
            case 'libro':
                $return['comentarios'] = $this->MiPortalLibro_model->getAll($id_web);
                $this->load->view('portalweb_visitante/libro', $return);
                break;
            case 'blog':
                $return['blogs'] = $this->MiPortalBlog_model->getAll($id_web);
                $this->load->view('portalweb_visitante/blog', $return);
                break;
            case 'direccion':
                $return['direcciones'] = $this->MiPortalDireccion_model->getAll($id_web);
                $this->load->view('portalweb_visitante/direccion', $return);
                break;
            case 'album':
                $return['albums'] = $this->MiPortalAlbum_model->getAll($id_web);
                $this->load->view('portalweb_visitante/album', $return);
                break;
            case 'encuesta':
                $return['encuestas'] = $this->MiPortalEncuesta_model->getAll($id_web);
                $this->load->view('portalweb_visitante/encuesta', $return);
                break;
            case 'evento':
                $return['eventos'] = $this->MiPortalEvento_model->getAll($id_web);
                $this->load->view('portalweb_visitante/evento', $return);
                break;
            case 'video':
                $return['videos'] = $this->MiPortalVideo_model->getAll($id_web);
                $this->load->view('portalweb_visitante/video', $return);
                break;
            case 'test':
                $return['tests'] = $this->MiPortalTest_model->getAnswer($id_web, $this->session->userdata('id_visita'));
                $this->load->view('portalweb_visitante/test', $return);
                break;
            case 'asistencia':
                $this->load->view('portalweb_visitante/asistencia', $return);
                break;
            case 'contactar':
                $this->load->view('portalweb_visitante/contactar', $return);
                break;
        }
    }

    public function test($id = null)
    {
        if ($_POST) {
            if ($id != null) {
                $id           = decrypt($id);
                $comprobacion = $this->MiPortalTest_model->comprobar($id, $this->session->userdata('bodas_visit'));
                if ($comprobacion != false) {
                    $data = array(
                        "id_miportal_test"   => $id,
                        "respuesta"          => $this->input->post('respuesta', true),
                        "fecha"              => date('Y-m-d H:i:s'),
                        "id_miportal_visita" => $this->session->userdata('id_visita'),
                    );

                    $resultado = $this->MiPortalTest_model->insertAnswer($data);
                    if ($resultado != false) {
                        header('Content-Type: application/json');
                        http_response_code(202);
                        echo json_encode(array("success" => 0, "resultado" => $comprobacion));
                        exit;
                    }
                }
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function encuesta($id = null)
    {
        if ($_POST) {
            if ($id != null) {
                $id           = decrypt($id);
                $comprobacion = $this->MiPortalEncuesta_model->comprobar($id, $this->session->userdata('bodas_visit'));
                if ($comprobacion != false) {
                    $data = array(
                        "id_miportal_encuesta_opcion" => $this->input->post('respuesta', true),
                        "fecha"                       => date('Y-m-d H:i:s'),
                        "id_miportal_visita"          => $this->session->userdata('id_visita'),
                    );

                    $resultado = $this->MiPortalEncuesta_model->insertAnswer($data, $id);

                    if ($resultado != false) {
                        $comprobacion = $this->MiPortalEncuesta_model->getAnswer($id);

                        header('Content-Type: application/json');
                        http_response_code(202);
                        echo json_encode(array("success" => 0, "resultado" => $comprobacion));
                        exit;
                    }
                }
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function album($id_web = null, $id_album = null)
    {
        if ($_POST) {
            if ($this->MiPortalAlbum_model->comprobar(decrypt($id_album), $this->session->userdata('bodas_visit'))) {
                $data      = array(
                    "id_miportal_album" => decrypt($id_album),
                    "nombre"            => $this->input->post('nombre', true),
                    "correo"            => $this->input->post('correo', true),
                    "fecha"             => date('Y-m-d H:i:s'),
                    "comentario"        => $this->input->post('comentario', true),
                );
                $resultado = $this->MiPortalAlbum_model->insertComentario($data);
                if ($resultado != false) {
                    header('Content-Type: application/json');
                    http_response_code(202);
                    echo json_encode(array("success" => 0, "message" => "Success"));
                    exit;
                }
            }
            header('Content-Type: application/json');
            http_response_code(404);
            echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
            exit;
        } else {
            if ($id_web == null) {
                redirect('Home');
            } elseif ($id_album == null) {
                redirect("Web/index/$id_web");
            }

            $return             = $this->information_default($id_web, 'album');
            $return['id_album'] = decrypt($id_album);
            $return['album']    = $this->MiPortalAlbum_model->get($id_web, $return['id_album']);

            if (empty($return['album'])) {
                redirect("Web/index/$id_web/album");
            } else {
                $this->load->view('portalweb_visitante/albumcomment', $return);
            }
        }
    }

    public function blog($id_web = null, $id_blog = null)
    {
        if ($_POST) {
            if ($this->MiPortalBlog_model->comprobar(decrypt($id_blog), $this->session->userdata('bodas_visit'))) {
                $data      = array(
                    "id_miportal_blog" => decrypt($id_blog),
                    "nombre"           => $this->input->post('nombre', true),
                    "email"            => $this->input->post('email', true),
                    "fecha"            => date('Y-m-d H:i:s'),
                    "comentario"       => $this->input->post('comentario', true),
                );
                $resultado = $this->MiPortalBlog_model->insertComentario($data);
                if ($resultado != false) {
                    header('Content-Type: application/json');
                    http_response_code(202);
                    echo json_encode(array("success" => 0, "message" => "Success"));
                    exit;
                }
            }
            header('Content-Type: application/json');
            http_response_code(404);
            echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
            exit;
        } else {
            if ($id_web == null) {
                redirect('Home');
            } elseif ($id_blog == null) {
                redirect("Web/index/$id_web");
            }

            $return            = $this->information_default($id_web, 'blog');
            $return['id_blog'] = decrypt($id_blog);
            $return['blog']    = $this->MiPortalBlog_model->get($id_web, $return['id_blog']);

            if (empty($return['blog'])) {
                redirect("Web/index/$id_web/blog");
            } else {
                $this->load->view('portalweb_visitante/blogcomment', $return);
            }
        }
    }

    public function contactar()
    {
        if ($_POST) {
            $id = $this->MiPortalContactar_model->comprobar($this->session->userdata('bodas_visit'));
            if ($id != false) {
                $data      = array(
                    "id_miportal_seccion" => $id,
                    "nombre"              => $this->input->post('nombre', true),
                    "email"               => $this->input->post('email', true),
                    "asunto"              => $this->input->post('asunto', true),
                    "fecha"               => date('Y-m-d H:i:s'),
                    "comentario"          => $this->input->post('comentario', true),
                );
                $resultado = $this->MiPortalContactar_model->insertComentario($data);
                if ($resultado != false) {
                    header('Content-Type: application/json');
                    http_response_code(202);
                    echo json_encode(array("success" => 0, "message" => "Success"));
                    exit;
                }
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function libro()
    {
        if ($_POST) {
            $id = $this->MiPortalLibro_model->comprobar($this->session->userdata('bodas_visit'));
            if ($id != false) {
                $data      = array(
                    "id_miportal_seccion" => $id,
                    "nombre"              => $this->input->post('nombre', true),
                    "email"               => $this->input->post('email', true),
                    "fecha"               => date('Y-m-d H:i:s'),
                    "comentario"          => $this->input->post('comentario', true),
                );
                $resultado = $this->MiPortalLibro_model->insert($data);
                if ($resultado != false) {
                    header('Content-Type: application/json');
                    http_response_code(202);
                    echo json_encode(array("success" => 0, "message" => "Success"));
                    exit;
                }
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    private function information_default($id_web, $seccion)
    {
        $return['seccion'] = $seccion;
        $return['id_web']  = $id_web;
        if (empty($this->session->userdata('bodas_visit')) || empty($this->session->userdata('miportal')) || $id_web != $this->session->userdata('bodas_visit') || (($this->session->userdata('fecha_actualizacion') + 1200) < time())) {
            if ($id_web != $this->session->userdata('bodas_visit')) {
                $this->session->set_userdata("encryption", generarCadena());
                $this->session->set_userdata("contrasenia_visita", "");
            }

            $return['miportal']         = $this->MiPortal_model->getPortal($id_web);
            $return['image_plantilla']  = $this->MiPlantilla_model->getImagen($return['miportal']->id_plantillaportal);
            $return['plantilla']        = $this->MiPlantilla_model->getPlantillaWeb($id_web);
            $return['miportal_seccion'] = $this->MiPortalSeccion_model->getAllShort($id_web);

            $return['informacion'] = $this->MiPortalSeccion_model->getSeccion($id_web, $seccion);
            $return['titulo']      = $return['informacion']->titulo;

            $datos = array(
                "bodas_visit"           => $id_web,
                "fecha_actualizacion"   => time(),
                "miportal"              => json_encode($return['miportal']),
                "image_plantilla"       => json_encode($return['image_plantilla']),
                "plantilla"             => json_encode($return['plantilla']),
                "miportal_seccion"      => json_encode($return['miportal_seccion']),
                $seccion."_informacion" => json_encode($return['informacion']),
                $seccion."_titulo"      => $return['titulo'],
            );
            $this->session->set_userdata($datos);
        } else {
            $return['miportal']         = json_decode($this->session->userdata('miportal'));
            $return['image_plantilla']  = json_decode($this->session->userdata('image_plantilla'));
            $return['plantilla']        = json_decode($this->session->userdata('plantilla'));
            $return['miportal_seccion'] = json_decode($this->session->userdata('miportal_seccion'));

            if (empty($this->session->userdata($seccion.'_informacion'))) {
                $return['informacion'] = $this->MiPortalSeccion_model->getSeccion($id_web, $seccion);
                $return['titulo']      = $return['informacion']->titulo;

                $datos = array(
                    $seccion."_informacion" => json_encode($return['informacion']),
                    $seccion."_titulo"      => $return['titulo'],
                );
                $this->session->set_userdata($datos);
            } else {
                $return['informacion'] = json_decode($this->session->userdata($seccion.'_informacion'));
                $return['titulo']      = $this->session->userdata($seccion.'_titulo');
            }
        }
        // ESTA SECCION ESTA DESACTIVADA O ESTA COMO NO VISIBLE 
        if ($return['informacion']->activo == 2 || $return['informacion']->visible == 3) {
            redirect("Web/index/$id_web");
        }

        return $return;
    }

    public function login()
    {
        if ($_POST) {
            //ENVIA EMAIL A NOVIOS SOLICITANDO LA CONTRASENIA
            if ( ! empty($this->input->post('nombre-send'))) {
                $miportal = $this->MiPortal_model->getBoda($this->session->userdata('bodas_visit'));
                foreach ($miportal as $key => $value) {
                    $data   = array(
                        "to"      => $value->id_usuario,
                        "asunto"  => $this->input->post('nombre-send').' te pide la contraseña de la web de tu boda.',
                        "mensaje" => $this->input->post('nombre-send').' ha intentado acceder a la web de tu boda pero no tiene la contrase&ntilde;a.¿Se la env&iacute;as?<br>Email:'
                            .$this->input->post('email-send')
                            .' <center><a style="background-color: #eeeeee !important; box-shadow: none; background-color: transparent; color: #343434; cursor: pointer; border: none; border-radius: 2px; display: inline-block; height: 36px; line-height: 36px; outline: 0; padding: 0 2rem; text-transform: uppercase; vertical-align: middle; -webkit-tap-highlight-color: transparent;" href="mailto:'
                            .$this->input->post('email-send').'" >Enviar contrase&ntilde;a</a></center>',
                        "reply"   => 0,
                    );
                    $return = $this->Correo_model->insert($data);
                }

                if ($return != false) {
                    header('Content-Type: application/json');
                    http_response_code(202);
                    echo json_encode(array("success" => 0, "message" => "Success"));
                    exit;
                }
                header('Content-Type: application/json');
                http_response_code(404);
                echo json_encode(array("success" => 2, "message" => "Intentelo de nuevo m&aacute;s tarde."));
                exit;
            } else {
                // COMPRUEBA CONTRASENIA PARA ENTRAR AL PORTAL
                if ( ! empty($this->input->post('password'))) {
                    $dt = $this->MiPortal_model->validar($this->session->userdata('bodas_visit'), $this->input->post('password'));
                    if ($dt) {
                        $data = array(
                            'contrasenia_visita' => 'acceso_correcto',
                            'id_web'             => $this->input->post('ip'),
                        );
                        $this->session->set_userdata($data);
                        if (empty($this->input->post('nombre'))) {
                            header('Content-Type: application/json');
                            http_response_code(202);
                            echo json_encode(array("success" => 0, "message" => "Success"));
                            exit;
                        }
                    } else {
                        header('Content-Type: application/json');
                        http_response_code(404);
                        echo json_encode(array("success" => 3, "message" => "Password erroneo."));
                        exit;
                    }
                }

                // DATOS DE VISITANTE
                if ( ! empty($this->input->post('nombre'))) {
                    $data      = array(
                        "nombre"    => $this->input->post('nombre', true),
                        "apellidos" => $this->input->post('apellido', true),
                        "email"     => $this->input->post('email', true),
                        "fecha"     => date('Y-m-d H:i:s'),
                    );
                    $resultado = $this->MiPortalVisita_model->insert($data);
                    if ($resultado != false) {
                        $miportal = $this->MiPortal_model->getBoda($this->session->userdata('bodas_visit'));
                        foreach ($miportal as $key => $value) {
                            $datos = array(
                                "id_boda"      => $value->id_boda,
                                "id_cliente"   => $value->id_cliente,
                                "id_proveedor" => null,
                                "asunto"       => 'Nueva visita en tu web de boda',
                                "mensaje"      => '¡Has tenido una nueva visita en tu web de boda! '.$data['nombre'].' '.$data['apellidos'].' acaba de entrar.¿Quieres enviarle un mensaje?<br>Email:'
                                    .$data['email']
                                    .' <center><a style="background-color: #eeeeee !important; box-shadow: none; background-color: transparent; color: #343434; cursor: pointer; border: none; border-radius: 2px; display: inline-block; height: 36px; line-height: 36px; outline: 0; padding: 0 2rem; text-transform: uppercase; vertical-align: middle; -webkit-tap-highlight-color: transparent;" href="mailto:'
                                    .$data['email'].'" >Enviar contrase&ntilde;a</a></center>',
                            );
                            $this->Solicitud_model->insert($datos);
                        }

                        $return = array(
                            "nombre_visita" => $data['nombre'].' '.$data['apellidos'],
                            "email_visita"  => $data['email'],
                            "id_visita"     => $resultado,
                        );
                        $this->session->set_userdata($return);
                        header('Content-Type: application/json');
                        http_response_code(202);
                        echo json_encode(array("success" => 0, "message" => "Success"));
                        exit;
                    }
                }
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function moveImages()
    {
        $images = Gallery::get();

        foreach ($images as $image) {

        }

    }

}
