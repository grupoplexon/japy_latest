<?php

use Models\Blog\Post as Post;
use Illuminate\Database\Capsule\Manager as DB;

class Blog extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Blog/Post_model', 'post');
    }

    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function index()
    {
        $data["posts"] = $this->post->getAll("id_blog_post,titulo,fecha_creacion,id_autor,publicado,SUBSTRING(contenido,0,50) as contenido");
        foreach ($data["posts"] as &$value) {
            $value->titulo = strip_tags($value->titulo);
        }
        //Se envia a la vista los datos de los post existentes
        $this->load->view("admin/blog/index", $data);
    }

    public function create()
    {
        $data = [];
        $data["category"] = DB::table('blog_post_categoria')->get();
        $this->load->view("admin/blog/post", $data);
    }

    public function edit($id_post)
    {
        $data["post"] = Post::find($id_post);
        $data["category"] = DB::table('blog_post_categoria')->get();

        if ($data["post"]) {
            $data["id_post"] = $id_post;

            return $this->load->view("admin/blog/post", $data);
        }

        return show_404();
    }

    public function destroy()
    {
        Post::destroy($this->input->post('id'));
    }

    public function store()
    {
        $this->output->set_content_type('application/json');
        $tags     = $this->input->post("tags");
        $response = [
            "code"    => 404,
            "message" => "error",
            "data"    => [],
        ];

        $post = [
            'titulo'              => $this->input->post("titulo"),
            'contenido'           => $this->input->post("contenido"),
            'id_autor'            => $this->session->userdata("usuario"),
            'id_imagen_destacada' => $this->input->post("id_imagen_destacada"),
            'publicado'           => $this->input->post('estado'),
            'tipo'           => $this->input->post('tipoPost'),
        ];

        $post = Post::create($post);

        if ($tags) {
            $post->tags()->sync($tags);
        }

        $response["code"]    = 200;
        $response["message"] = "ok";

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function update($id)
    {
        $this->output->set_content_type('application/json');
        $tags     = $this->input->post("tags");
        $response = [
            "code"    => 404,
            "message" => "error",
            "data"    => [],
        ];

        $post = Post::find($id);

        if ($post) {
            $data = [
                'titulo'              => $this->input->post("titulo"),
                'contenido'           => $this->input->post("contenido"),
                'id_autor'            => $this->session->userdata("usuario"),
                'id_imagen_destacada' => $this->input->post("id_imagen_destacada"),
                'publicado'           => $this->input->post('estado'),
                'tipo'           => $this->input->post('tipoPost'),
            ];

            $post->update($data);

            if ($tags) {
                $post->tags()->sync($tags);
            }

            $response["code"]    = 200;
            $response["message"] = "ok";
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function publish()
    {
        Post::where("id_blog_post", $this->input->post("id"))->update(["publicado" => $this->input->post("estado")]);
    }

    public function upload()
    {
        $data["imagen"] = $_POST["imagen_principal"];
        $this->load->view("admin/blog/post", $data);
    }

}
