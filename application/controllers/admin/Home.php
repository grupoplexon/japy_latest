<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Destinos/Destino_model", "destino");
        $this->load->model("Destinos/Locations_model", "location");
        $this->load->helper("storage");
        $this->load->model("Brideweekend/Brideweekend_model", "brideweekend");
        date_default_timezone_set('America/Mexico_City');
    }

    protected function middleware()
    {
        return ["admin_auth"];
    }

    public function index()
    {
        if($this->session->userdata('correo')!='flori@admin.com') {
            $data = [];
            $this->load->view("admin/index", $data);
        } else {
            $this->load->view("admin/trends/index");
        }
    }

    public function destinations() {
        $this->load->view('admin/destinos/index');
    }

    public function nuevoDestino() {
        $this->load->view('admin/destinos/nuevo');
    }

    public function saveDestination() {
        $tmp_name = $_FILES['archivo']['tmp_name'];
        $img_file = $_FILES['archivo']['name'];
        $img_type = $_FILES['archivo']['type'];
        $directorio_destino = 'uploads/images/destinos';
        if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") ||
        strpos($img_type, "jpg")) || strpos($img_type, "png"))) {
            $new_name = uniqid().'.png';
            if (move_uploaded_file($tmp_name, $directorio_destino . '/' .$new_name))
            {
                $save = DB::table('destinations')->insert([
                    'name' => $this->input->post('name'),
                    'image' => $new_name,
                    'description' => $this->input->post('desc1'),
                    'location_description' => $this->input->post('desc2'),
                    'attractive_description' => $this->input->post('desc3'),
                ]);
                if($save) {
                    redirect('admin/home/destinations');
                } else {
                    dd('error');
                }
            } else {
                dd('error');
            }
        } else {
            dd('error');
        }
    }

    public function extractDestinations() {
        $fetch_data = $this->destino->make_datatables();  
        $data = array();  
        foreach($fetch_data as $row)  
        {   
                $sub_array = array();  
                $sub_array[] = $row->id;  
                $sub_array[] = $row->name;  
                $sub_array[] = $row->description;

                $data[] = $sub_array;  
        }  
        $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->destino->get_all_data(),  
                "recordsFiltered"     =>     $this->destino->get_filtered_data(),  
                "data"                    =>     $data  
        );  
        return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($output));
    }

    public function add($id, $success=null) {
        $data['id'] = $id;
        $data['saved'] = $success;
        $this->load->view('admin/destinos/add', $data);
    }

    public function addLocation() {

        $tmp_name = $_FILES['archivo']['tmp_name'];
        $img_file = $_FILES['archivo']['name'];
        $img_type = $_FILES['archivo']['type'];

        $id = $this->input->post('edit');

        if($id==0){

            $directorio_destino = 'uploads/images/destinos';
            if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") ||
            strpos($img_type, "jpg")) || strpos($img_type, "png"))) {
                $new_name = uniqid().'.png';
                if (move_uploaded_file($tmp_name, $directorio_destino . '/' .$new_name))
                {
                    
                    $save = DB::table('locations_attractives')->insert([
                        'id_destinations' => $this->input->post('idDestino'),
                        'name' => $this->input->post('name'),
                        'type' => $this->input->post('tipo'),
                        'image' => $new_name,
                        'description' => $this->input->post('desc1'),
                    ]);
                    if($save) {
                        redirect('admin/home/add/'.$this->input->post('idDestino').'/success');
                    } else {
                        redirect('admin/home/add/'.$this->input->post('idDestino').'/error');
                    }
                } else {
                    redirect('admin/home/add/'.$this->input->post('idDestino').'/error');
                }
            } else {
                redirect('admin/home/add/'.$this->input->post('idDestino').'/error');
            }
        }else{
            if(!$img_file){
                $save = DB::table('locations_attractives')
                ->where('id', $id)
                ->update(['id_destinations' => $this->input->post('idDestino'),
                            'name' => $this->input->post('name'),
                            'type' => $this->input->post('tipo'),
                            'description' => $this->input->post('desc1'),
                        ]);
            }else{
                $directorio_destino = 'uploads/images/destinos';
                if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") ||
                strpos($img_type, "jpg")) || strpos($img_type, "png"))) {
                    $new_name = uniqid().'.png';
                    if (move_uploaded_file($tmp_name, $directorio_destino . '/' .$new_name))
                    {
                        $save = DB::table('locations_attractives')
                        ->where('id', $id)
                        ->update(['id_destinations' => $this->input->post('idDestino'),
                                    'name' => $this->input->post('name'),
                                    'type' => $this->input->post('tipo'),
                                    'image' => $new_name,
                                    'description' => $this->input->post('desc1'),
                                ]);
                    } else {
                        redirect('admin/home/add/'.$this->input->post('idDestino').'/error');
                    }
                } else {
                    redirect('admin/home/add/'.$this->input->post('idDestino').'/error');
                }
            }

            if($save) {
                redirect('admin/home/add/'.$this->input->post('idDestino').'/success');
            } else {
                redirect('admin/home/add/'.$this->input->post('idDestino').'/error');
            }
        }
    }

    public function edit($id) {
        $data['destinos'] = DB::table('destinations')->where('id', $id)->first();
        $this->load->view('admin/destinos/edit', $data);
    }

    public function saveEdition() {
        if($_FILES) {
            $destino = DB::table('destinations')->where('id', $this->input->post('id'))->first();
            if($destino) {
                unlink('uploads/images/destinos/'.$destino->image);
                $tmp_name = $_FILES['archivo']['tmp_name'];
                $img_file = $_FILES['archivo']['name'];
                $img_type = $_FILES['archivo']['type'];
                $directorio_destino = 'uploads/images/destinos';
                if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") ||
                strpos($img_type, "jpg")) || strpos($img_type, "png"))) {
                    $new_name = uniqid().'.png';
                    if (move_uploaded_file($tmp_name, $directorio_destino . '/' .$new_name))
                    {
                        $save = DB::table('destinations')
                        ->where('id', $this->input->post('id'))
                        ->update([
                            'name' => $this->input->post('name'),
                            'image' => $new_name,
                            'description' => $this->input->post('desc1'),
                            'location_description' => $this->input->post('desc2'),
                            'attractive_description' => $this->input->post('desc3'),
                        ]);
                        if($save) {
                            redirect('admin/home/destinations');
                        } else {
                            dd('error');
                        }
                    } else {
                        dd('error');
                    }
                }
            }
        } else {
            $save = DB::table('destinations')
            ->where('id', $this->input->post('id'))
            ->update([
                'name' => $this->input->post('name'),
                'description' => $this->input->post('desc1'),
                'location_description' => $this->input->post('desc2'),
                'attractive_description' => $this->input->post('desc3'),
            ]);
            if($save) {
                redirect('admin/home/destinations');
            } else {
                redirect('admin/home/edit/'.$this->input->post('id'));
            }
        }
    }

    public function extractLocations() {
        $_POST['idDestino'] = $this->input->post('id');
        $fetch_data = $this->location->make_datatables();  
        $data = array();
        foreach($fetch_data as $row)  
        {   
                $sub_array = array();  
                $sub_array[] = $row->id;  
                $sub_array[] = $row->name;  
                $sub_array[] = $row->type;
                $sub_array[] = $row->image;
                $sub_array[] = $row->description;

                $data[] = $sub_array;  
        }  
        $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->destino->get_all_data(),  
                "recordsFiltered"     =>     $this->destino->get_filtered_data(),  
                "data"                    =>     $data  
        );  
        return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($output));
    }

    public function usersRegisteredWeek()
    {
        $this->output->set_content_type("application/json");
        $response = [
            "code"    => 404,
            "data"    => [],
            "message" => "error",
        ];

        $states = ["cdmx", "guanajuato", "jalisco", "nuevo_leon", "puebla", "queretaro", "sinaloa", "veracruz"];
        $users  = User::whereDate("fecha_creacion", ">=", Carbon::now()->subWeek())
            ->groupBy(DB::raw("(registered_from),(date(fecha_creacion))"))
            ->get([DB::raw("date(fecha_creacion) as day, registered_from, count(*) as total")]);

        if ($users->count()) {
            $data = [];
            foreach ($users as $user) {
                foreach ($states as $state) {
                    $data[$user->day][$state] = 0;
                }
                $data[$user->day]["desconocido"] = 0;
            }

            foreach ($users as $user) {
                $user->registered_from                    = is_null($user->registered_from) ? "desconocido" : $user->registered_from;
                $data[$user->day][$user->registered_from] = +$user->total;
                $data[$user->day]["day"]                  = $user->day;
            }

            $response["data"]    = array_values($data);
            $response["code"]    = 200;
            $response["message"] = "ok";
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function brideweekend() {
        $this->load->view('admin/brideweekend/index');
    }

    public function brideweekendNew() {
        $this->load->view('admin/brideweekend/nuevo');
    }

    public function saveCity() {
        if($_POST) {
            // dd($this->input->post('hourSunPasStart').', '.$this->input->post('hourSunPasEnd'));
            $destination = 'uploads/images/brideweekend';
            $quality     = 20;
            $pngQuality  = 9;
            
            $file = $_FILES['img_r'];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["img_r"]["name"])["extension"];
            $fileName  = store_file($data, $extension, "images/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
            $img_r = $fileName;
            
            $file = $_FILES['img_c'];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["img_c"]["name"])["extension"];
            $fileName  = store_file($data, $extension, "images/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
            $img_c = $fileName;

            $file = $_FILES['img_l'];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["img_l"]["name"])["extension"];
            $fileName  = store_file($data, $extension, "images/brideweekend");
            $new_name_image = $file = $destination."/".$fileName;
            $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
            $img_l = $fileName;

            $entryESa = $this->input->post('hourSatExpoStart').'-'.$this->input->post('hourSatExpoEnd');
            $entryESu = $this->input->post('hourSunExpoStart').'-'.$this->input->post('hourSunExpoEnd');

            if(!empty($this->input->post('hourSatPasStart')) && !empty($this->input->post('hourSatPasEnd'))) {
                // $entryPSa = date("g:ia",strtotime($this->input->post('hourSatPasStart'))).' y '.date("g:ia",strtotime($this->input->post('hourSatPasEnd')));
                $entryPSa = $this->input->post('hourSatPasStart').'-'.$this->input->post('hourSatPasEnd');
            } elseif(!empty($this->input->post('hourSatPasStart')) && empty($this->input->post('hourSatPasEnd'))) {
                // $entryPSa = date("g:ia",strtotime($this->input->post('hourSatPasStart')));
                $entryPSa = $this->input->post('hourSatPasStart');
            }

            if(!empty($this->input->post('hourSunPasStart')) && !empty($this->input->post('hourSunPasEnd'))) {
                // $entryPSu = date("g:ia",strtotime($this->input->post('hourSunPasStart'))).' y '.date("g:ia",strtotime($this->input->post('hourSunPasEnd')));
                $entryPSu = $this->input->post('hourSunPasStart').'-'.$this->input->post('hourSunPasEnd');
            } elseif(!empty($this->input->post('hourSunPasStart')) && empty($this->input->post('hourSunPasEnd'))) {
                // $entryPSu = date("g:ia",strtotime($this->input->post('hourSunPasStart')));
                $entryPSu = $this->input->post('hourSunPasStart');
            }

            $save = DB::table('brideweekend')->insert([
                'city' => $this->input->post('city'),
                'initial_date' => $this->input->post('date_i'),
                'final_date' => $this->input->post('date_f'),
                'enclosure' => $this->input->post('recinto'),
                'enclosure_address' => $this->input->post('address'),
                'enclosure_description' => $this->input->post('description'),
                'map' => $this->input->post('map'),
                'img_enclosure' => $img_r,
                'img_head' => $img_c,
                'logo_enclosure' => $img_l,
                'organized_by' => $this->input->post('organized_by'),
                'entry_expo_sat' => $entryESa,
                'entry_expo_sun' => $entryESu,
                'runway_sat' => $entryPSa,
                'runway_sun' => $entryPSu,
            ]);
            redirect('admin/home/brideweekend');
        }
    }

    function fetch_trend() {
        $fetch_data = $this->brideweekend->make_datatables();  
        $data = array();  
        foreach($fetch_data as $row)  
        {   
                $sub_array = array();  
                $sub_array[] = $row->id;  
                $sub_array[] = $row->city;  
                $sub_array[] = $row->initial_date;  
                $sub_array[] = $row->final_date;  
                // $sub_array[] = $row->temporada;  
                // $sub_array[] = '';

                $data[] = $sub_array;  
        }  
        $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->brideweekend->get_all_data(),  
                "recordsFiltered"     =>     $this->brideweekend->get_filtered_data(),  
                "data"                    =>     $data  
        );  
        return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($output));
    } 

    public function editBW($id) {
        $city['city'] = DB::table('brideweekend')->where('id', $id)->first();
        $city['hourOne'] = substr($city['city']->entry_expo_sat, 0, 5);
        $city['hourTwo'] = substr($city['city']->entry_expo_sat, 6, 5);
        $city['hourTree'] = substr($city['city']->entry_expo_sun, 0, 5);
        $city['hourFour'] = substr($city['city']->entry_expo_sun, 6, 5);
        $city['hourFive'] = substr($city['city']->runway_sat, 0, 5);
        $city['hourSix'] = substr($city['city']->runway_sat, 6, 5);
        $city['hourSeven'] = substr($city['city']->runway_sun, 0, 5);
        $city['hourEight'] = substr($city['city']->runway_sun, 6, 5);
        // dd($city['hourTwo']);
        $this->load->view('admin/brideweekend/edit', $city);
    }

    public function editCity() {
        if($_POST) {
            $images = DB::table('brideweekend')->where('id', $this->input->post('ID'))->first();
            $destination = 'uploads/images/brideweekend';
            $quality     = 20;
            $pngQuality  = 9;
            if(!empty($_FILES['img_l']['name'])) {
                if(!empty($images->logo_enclosure)) {
                    unlink('uploads/images/brideweekend/'.$images->logo_enclosure);
                }
                $file = $_FILES['img_l'];
                $data     = file_get_contents($file["tmp_name"]);
                $extension = pathinfo($_FILES["img_l"]["name"])["extension"];
                $fileName  = store_file($data, $extension, "images/brideweekend");
                $new_name_image = $file = $destination."/".$fileName;
                $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
                $img_l = $fileName;
                $city = DB::table('brideweekend')
                ->where('id', $this->input->post('ID'))
                ->update([
                    'logo_enclosure' => $img_l,
                ]);
            }
            if(!empty($_FILES['img_r']['name'])) {
                if(!empty($images->img_enclosure)) {
                    unlink('uploads/images/brideweekend/'.$images->img_enclosure);
                }
                $file = $_FILES['img_r'];
                $data     = file_get_contents($file["tmp_name"]);
                $extension = pathinfo($_FILES["img_r"]["name"])["extension"];
                $fileName  = store_file($data, $extension, "images/brideweekend");
                $new_name_image = $file = $destination."/".$fileName;
                $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
                $img_r = $fileName;
                $city = DB::table('brideweekend')
                ->where('id', $this->input->post('ID'))
                ->update([
                    'img_enclosure' => $img_r,
                ]);
            }
            if(!empty($_FILES['img_c']['name'])) {
                if(!empty($images->img_head)) {
                    unlink('uploads/images/brideweekend/'.$images->img_head);
                }
                $file = $_FILES['img_c'];
                $data     = file_get_contents($file["tmp_name"]);
                $extension = pathinfo($_FILES["img_c"]["name"])["extension"];
                $fileName  = store_file($data, $extension, "images/brideweekend");
                $new_name_image = $file = $destination."/".$fileName;
                $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
                $img_c = $fileName;
                $city = DB::table('brideweekend')
                ->where('id', $this->input->post('ID'))
                ->update([
                    'img_head' => $img_c,
                ]);
            }
            $entryESa = $this->input->post('hourSatExpoStart').'-'.$this->input->post('hourSatExpoEnd');
            $entryESu = $this->input->post('hourSunExpoStart').'-'.$this->input->post('hourSunExpoEnd');

            if(!empty($this->input->post('hourSatPasStart')) && !empty($this->input->post('hourSatPasEnd'))) {
                // $entryPSa = date("g:ia",strtotime($this->input->post('hourSatPasStart'))).' y '.date("g:ia",strtotime($this->input->post('hourSatPasEnd')));
                $entryPSa = $this->input->post('hourSatPasStart').'-'.$this->input->post('hourSatPasEnd');
            } elseif(!empty($this->input->post('hourSatPasStart')) && empty($this->input->post('hourSatPasEnd'))) {
                // $entryPSa = date("g:ia",strtotime($this->input->post('hourSatPasStart')));
                $entryPSa = $this->input->post('hourSatPasStart');
            }

            if(!empty($this->input->post('hourSunPasStart')) && !empty($this->input->post('hourSunPasEnd'))) {
                // $entryPSu = date("g:ia",strtotime($this->input->post('hourSunPasStart'))).' y '.date("g:ia",strtotime($this->input->post('hourSunPasEnd')));
                $entryPSu = $this->input->post('hourSunPasStart').'-'.$this->input->post('hourSunPasEnd');
            } elseif(!empty($this->input->post('hourSunPasStart')) && empty($this->input->post('hourSunPasEnd'))) {
                // $entryPSu = date("g:ia",strtotime($this->input->post('hourSunPasStart')));
                $entryPSu = $this->input->post('hourSunPasStart');
            }
            $city = DB::table('brideweekend')
            ->where('id', $this->input->post('ID'))
            ->update([
                'city' => $this->input->post('city'),
                'initial_date' => $this->input->post('date_i'),
                'final_date' => $this->input->post('date_f'),
                'enclosure' => $this->input->post('recinto'),
                'enclosure_address' => $this->input->post('address'),
                'enclosure_description' => $this->input->post('description'),
                'map' => $this->input->post('map'),
                'entry_expo_sat' => $entryESa,
                'entry_expo_sun' => $entryESu,
                'runway_sat' => $entryPSa,
                'runway_sun' => $entryPSu,
            ]);
            redirect('admin/home/brideweekend');
        }
    }
}
