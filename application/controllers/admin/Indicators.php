<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Indicators extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("proveedor_model", "proveedor");
    }

    public function index()
    {
        $this->load->view("admin/indicators/indicadores");
    }

    public function fetch_user() {
        if($_POST) {
            $fechaIni = $this->input->post('fechaI');
            $fechaFin = $this->input->post('fechaF');
            $proveedores = $this->proveedor->make_datatables2();
            
            $prueba = self::calcula($proveedores, $fechaIni, $fechaFin);
            
            $temporal = array();
            $data = array();
            $total=0;
            $tam = sizeOf($prueba);
            $total=$tam;
            if($tam<10) {
                while($total < 10) {
                    $_POST['start'] = $_POST['start'] + $_POST['length'];
                    $_POST['length'] = (10-$total);
                    $proveedores2 = $this->proveedor->make_datatables2();
                    $prueba2 = self::calcula($proveedores2, $fechaIni, $fechaFin);
                    $tam2= sizeOf($prueba2);
                    $total = $total + $tam2;
                    if($tam2!=0 && $tam2 <= (10-$tam)) {
                        $temporal[] = $prueba2;
                    }
                }
            }
            for($x=0; $x<sizeOf($temporal); $x++) {
                if($x<1) {
                    $data []= array_merge($prueba, $temporal[$x]);
                } else {
                    $data [0]= array_merge($data[0], $temporal[$x]);
                }
            }
            $data[] = sort($data[0]);
            $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->proveedor->get_all_data2(),  
                "recordsFiltered"     =>     $this->proveedor->get_filtered_data2(),  
                "data"                    =>     $data[0]  
            );

            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($output));
        }
    }

    public function calcula($proveedores, $fechaIni, $fechaFin) {
        $data = array();
        foreach($proveedores as $row) {
            $sub_array = array();  
            $sub_array[] = $row->nombre;  
            $sub_array[] = DB::table('indicators')
                                    ->where('provider_id', $row->id_proveedor)
                                    ->where('type', 'login')
                                    ->whereBetween('created_at', [$fechaIni, $fechaFin])
                                    ->count();  
            $sub_array[] = DB::table('indicators')
                                    ->where('provider_id', $row->id_proveedor)
                                    ->where('type', 'phone')
                                    ->whereBetween('created_at', [$fechaIni, $fechaFin])
                                    ->count();  
            $sub_array[] = DB::table('indicators')
                                    ->where('provider_id', $row->id_proveedor)
                                    ->where('type', 'visit')
                                    ->whereBetween('created_at', [$fechaIni, $fechaFin])
                                    ->count();  
            $sub_array[] = DB::table('indicators')
                                    ->where('provider_id', $row->id_proveedor)
                                    ->where('type', 'contact_request')
                                    ->whereBetween('created_at', [$fechaIni, $fechaFin])
                                    ->count();
            if($sub_array[1]>0 || $sub_array[2]>0 || $sub_array[3]>0 || $sub_array[4]>0) {
                $data[] = $sub_array;
            }
        }
        return $data;
    }

    public function muestraIndicadores() {
        if($_POST) {
            $fechaIni = $this->input->post('fechaI');
            $fechaFin = $this->input->post('fechaF');

            $nuevafecha = strtotime ( '+1 day' , strtotime ( $fechaFin ) ) ;
            $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
            $fechaFin = $nuevafecha;

            $proveedores = Provider::get();
            $cont=0;
            // $indicators = array();
            
            foreach ($proveedores as $key => $value) {
                $visit = DB::table('indicators')
                                    ->where('provider_id', $value['id_proveedor'])
                                    ->where('type', 'visit')
                                    ->whereBetween('created_at', [$fechaIni, $fechaFin])
                                    ->count();
                $phone = DB::table('indicators')
                                    ->where('provider_id', $value['id_proveedor'])
                                    ->where('type', 'phone')
                                    ->whereBetween('created_at', [$fechaIni, $fechaFin])
                                    ->count();
                $mensajes = DB::table('indicators')
                                    ->where('provider_id', $value['id_proveedor'])
                                    ->where('type', 'contact_request')
                                    ->whereBetween('created_at', [$fechaIni, $fechaFin])
                                    ->count();
                $login = DB::table('indicators')
                                    ->where('provider_id', $value['id_proveedor'])
                                    ->where('type', 'login')
                                    ->whereBetween('created_at', [$fechaIni, $fechaFin])
                                    ->count();
                if($visit>0 || $phone>0 || $mensajes>0 || $login>0) {
                    $indicators[$cont]['nombres'] = $value['nombre'];
                    $indicators[$cont]['visit'] = $visit;
                    $indicators[$cont]['phone'] = $phone;
                    $indicators[$cont]['mensajes'] = $mensajes;
                    $indicators[$cont]['login'] = $login;
                    $cont+=1;
                }
            }
            // dd($indicators);
            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['indicadores' => $indicators]));
        }
    }
}