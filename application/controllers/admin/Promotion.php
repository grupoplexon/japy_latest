<?php
use Illuminate\Database\Capsule\Manager as DB;

use Dompdf\Dompdf;

class Promotion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("storage");
        $this->load->library("My_PHPMailer");
        $this->load->helper("slug");
        $this->mailer = $this->my_phpmailer;
    }

    public function index(){

        $data["promotions"]= DB::table('promotions_brideweekend')->where('status', 1)->get();

        $this->load->view('admin/promociones/index' , $data);
    }
    public function newPromotion(){
        $this->load->view('admin/promociones/newPromotion');
    }
    public function upload() {
        $directorio_destino  = 'uploads/promociones/images';
        
        $tmp_name = $_FILES['principal']['tmp_name'];
        $img_file = $_FILES['principal']['name'];
        $img_type = $_FILES['principal']['type'];
        $tmp_name2 = $_FILES['cupon']['tmp_name'];
        $img_file2 = $_FILES['cupon']['name'];
        $img_type2 = $_FILES['cupon']['type'];

        if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") ||
        strpos($img_type, "jpg")) || strpos($img_type, "png"))) {
            $new_name = uniqid().'.png';
            $new_name2 = uniqid().'.png';
            if (move_uploaded_file($tmp_name, $directorio_destino . '/' .$new_name) && move_uploaded_file($tmp_name2, $directorio_destino . '/' .$new_name2))
            {

                $res = DB::table('promotions_brideweekend')->insert([
                    'name_provider' => $this->input->post("provider"),
                    'description' => $this->input->post("descripcion"),
                    'image' => $new_name,
                    'cupon' => $new_name2,
                    'profile_provider' => $this->input->post("perfil"),
                    'status' => 1,
                ]);
                $status = "ok";

                if($res) {
                    redirect('admin/promotion/index');
                } else {
                    dd('error');
                }
            }
        }
    }
    public function deletePromotion() {
        if ($_POST) {
            $id= $this->input->post("id_promotion");

            $promo = DB::table('promotions_brideweekend')
              ->where('id_promotion', $id)
              ->update(['status' => 0]);
        }
    }
    public function editPromotion()
    {
        $promo = $this->input->get('id_promotion');

        $data["promotion"] = DB::table('promotions_brideweekend')->where('id_promotion', $promo)->first();
        // dd($data["promotion"]);

        $this->load->view('admin/promociones/editPromotion' , $data);
    }
    public function edit(){
        $directorio_destino  = 'uploads/promociones/images';
        $tmp_name = $_FILES['principal']['tmp_name'];
        $img_file = $_FILES['principal']['name'];
        $img_type = $_FILES['principal']['type'];
        $tmp_name2 = $_FILES['cupon']['tmp_name'];
        $img_file2 = $_FILES['cupon']['name'];
        $img_type2 = $_FILES['cupon']['type'];

        if($tmp_name == null && $tmp_name2 == null){

            // dd($this->input->post("descripcion"));

            $res = DB::table('promotions_brideweekend')
                ->where('id_promotion', $this->input->post("id_promotion"))
                ->update([
                    'name_provider' => $this->input->post("provider"),
                    'description' => $this->input->post("descripcion"),
                    'profile_provider' => $this->input->post("perfil"),
                    ]);
            // dd($res);
            if($res) {
                redirect('admin/promotion/index');
            } else {
                dd('error');
            }
        }else if($tmp_name != null && $tmp_name2 == null){
            
            if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") ||
                strpos($img_type, "jpg")) || strpos($img_type, "png"))) {
                    $new_name = uniqid().'.png';
                    if (move_uploaded_file($tmp_name, $directorio_destino . '/' .$new_name))
                    {
                        $res = DB::table('promotions_brideweekend')
                        ->where('id_promotion', $this->input->post("id_promotion"))
                        ->update([
                            'name_provider' => $this->input->post("provider"),
                            'description' => $this->input->post("descripcion"),
                            'profile_provider' => $this->input->post("perfil"),
                            'image' => $new_name,
                        ]);
                        $status = "ok";

                        if($res) {
                            redirect('admin/promotion/index');
                        } else {
                            dd('error');
                        }
                    }
                }
        }else if($tmp_name2 != null && $tmp_name == null){

            if (((strpos($img_type2, "gif") || strpos($img_type2, "jpeg") ||
                strpos($img_type2, "jpg")) || strpos($img_type2, "png"))) {
                    $new_name2 = uniqid().'.png';
                    if (move_uploaded_file($tmp_name2, $directorio_destino . '/' .$new_name2))
                    {
                        $res = DB::table('promotions_brideweekend')
                        ->where('id_promotion', $this->input->post("id_promotion"))
                        ->update([
                            'name_provider' => $this->input->post("provider"),
                            'description' => $this->input->post("descripcion"),
                            'profile_provider' => $this->input->post("perfil"),
                            'cupon' => $new_name2,
                        ]);
                        $status = "ok";
                        if($res) {
                            redirect('admin/promotion/index');
                        } else {
                            dd('error');
                        }
                    }
                }
        }else{
             if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") ||
                strpos($img_type, "jpg")) || strpos($img_type, "png"))) {
                    $new_name = uniqid().'.png';
                    $new_name2 = uniqid().'.png';
                    if (move_uploaded_file($tmp_name, $directorio_destino . '/' .$new_name) && move_uploaded_file($tmp_name2, $directorio_destino . '/' .$new_name2))
                    {

                        $res = DB::table('promotions_brideweekend')
                        ->where('id_promotion', $this->input->post("id_promotion"))
                        ->update([
                            'name_provider' => $this->input->post("provider"),
                            'description' => $this->input->post("descripcion"),
                            'image' => $new_name,
                            'cupon' => $new_name2,
                            'profile_provider' => $this->input->post("perfil"),
                        ]);
                        $status = "ok";

                        if($res) {
                            redirect('admin/promotion/index');
                        } else {
                            dd('error');
                        }
                    }
                }
        }
        // if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") ||
        // strpos($img_type, "jpg")) || strpos($img_type, "png"))) {
        //     $new_name = uniqid().'.png';
        //     $new_name2 = uniqid().'.png';
        //     if (move_uploaded_file($tmp_name, $directorio_destino . '/' .$new_name) && move_uploaded_file($tmp_name2, $directorio_destino . '/' .$new_name2))
        //     {

        //         $res = DB::table('promotions_brideweekend')->insert([
        //             'name_provider' => $this->input->post("provider"),
        //             'description' => $this->input->post("descripcion"),
        //             'image' => $new_name,
        //             'cupon' => $new_name2,
        //             'status' => 1,
        //         ]);
        //         $status = "ok";

        //         if($res) {
        //             redirect('admin/promotion/index');
        //         } else {
        //             dd('error');
        //         }
        //     }
        // }
    }
    public function createPDF(){
        // if($_POST){

            $promos = json_decode($this->input->get("id_promotion"));
            
            $data = array();
            for($i=0; $i<count($promos); $i++){

                $p = DB::table('promotions_brideweekend')->where('id_promotion', $promos[$i])->select('cupon', 'downloads')->get();
                $data[] = $p;

                $num = $data[$i][0]->downloads;
                $num = $num + 1;

                $promo = DB::table('promotions_brideweekend')
                    ->where('id_promotion', $promos[$i])
                    ->update(['downloads' => $num]);

            }
            // dd($data);
            $dompdf = new Dompdf();

            $html = $this->load->view('templates/pdf_brideweekend', [
                "cupon" => $data,
            ], true);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $dompdf->stream("cupones_bridewekeend.pdf");

            // $output = $dompdf->output();
            
            // $folder = 'uploads/promociones/pdf/';
            // $file_save=  $folder.$name;

            // file_put_contents($file_save, $output);
        
    }
    public function sendPDF(){
        if($_POST){

            $promos = json_decode($this->input->post("id_promotion"));
            
            $data = array();
            for($i=0; $i<count($promos); $i++){

                $p = DB::table('promotions_brideweekend')->where('id_promotion', $promos[$i])->select('cupon', 'downloads')->get();
                $data[] = $p;

                $num = $data[$i][0]->downloads;
                $num = $num + 1;

                $promo = DB::table('promotions_brideweekend')
                    ->where('id_promotion', $promos[$i])
                    ->update(['downloads' => $num]);

            }
            // dd($data);
            $dompdf = new Dompdf();

            $html = $this->load->view('templates/pdf_brideweekend', [
                "cupon" => $data,
            ], true);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            // $dompdf->stream("cupones_bridewekeend.pdf");

            $output = $dompdf->output();
            
            $folder = 'uploads/promociones/pdf/';
            $file_save=  $folder.'cupones.pdf';

            $mail = $this->input->post("mail");

            file_put_contents($file_save, $output);

            $this->mailer->to($mail, $mail);

            $template = "DESCARGA TUS CUPONES BRIDEADVISOR";

            $this->mailer->setSubject("Cupones BrideAdvisor");
            $this->mailer->setAttachment($file_save,'cupones.pdf');
            $this->mailer->send($template);

            return $this->output->set_content_type("application/json")
                ->set_status_header(200)
                ->set_output(json_encode(['validate' => true]));
            
        }
        return $this->output->set_content_type("application/json")
            ->set_status_header(200)
            ->set_output(json_encode(['validate' => false]));
    }

}