<?php
use Illuminate\Database\Capsule\Manager as DB;
class Proveedor extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("proveedor_model", "proveedor");
    }


    public function index()
    {
        $data["countries"] = $this->getCountries();
        $this->load->view("admin/proveedores/proveedor", $data);
    }

    public function extraerProveedores() {
        if($_POST) {
            $data = $this->proveedor->getAllAdmin();

            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['success' => true, 'provider' => $data]));
        }
    }

    function fetch_user(){  
        // dd($_POST);
        // $this->load->model("proveedor");  
        // dd($this->proveedor->make_datatables());
        $fetch_data = $this->proveedor->make_datatables();  
        $data = array();
        $category = '';
        $categories['categorias'] = DB::table('proveedor_categoria')->where('parent_id', '!=', null)->select('id', 'name')->get();
        // dd($categories['categoriaProvider']);
        foreach($fetch_data as $row)  
        {   
                // $relation = DB::table('categoria_proveedor')->where('proveedor_id', $row->id_proveedor)->first();
                // if(!empty($relation)) {
                //     $temp = DB::table('proveedor_categoria', $relation->categoria_id)->first();
                //     $category = $temp->name;
                // } else {
                //     $category = 'Seleccione';
                // }

                $categories['category'] = DB::table('categoria_proveedor')->where('proveedor_id', $row->id_proveedor)->select('categoria_id')->first();
                if(empty($categories['category'])) {
                    $categories['category'] = '';
                }
                
                $sub_array = array();  
                $sub_array[] = $row->nombre;  
                $sub_array[] = $row->tipo_cuenta;  
                $sub_array[] = '';
                
                if(!empty($row->plan_expiration_date)){
                    $createDate = new DateTime($row->plan_expiration_date);
                    $strip = $createDate->format('Y-m-d');
                    $sub_array[] = $strip;
                }else{
                    $sub_array[] = $row->plan_expiration_date;
                }

                $sub_array[] = $row->activo;
                $sub_array[] = $categories;
                $sub_array[] = $row->usuario;  
                $sub_array[] = $row->contacto_celular;  
                $sub_array[] = $row->localizacion_direccion.', '.$row->localizacion_estado.', '.$row->localizacion_pais;
                $sub_array[] = $row->fecha_creacion;

                $sub_array[] = $row->id_usuario;

                $sub_array[] = $row->localizacion_pais;
                $sub_array[] = $row->localizacion_estado;
                $sub_array[] = $row->localizacion_poblacion;

                $data[] = $sub_array;  
        }  
        $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->proveedor->get_all_data(),  
                "recordsFiltered"     =>     $this->proveedor->get_filtered_data(),  
                "data"                    =>     $data  
        );  
        // dd($output);
        // echo json_encode($output);  
        return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($output));
    }

    public function tipoEstado() {
        if($_POST) {
            $table = $this->input->post('table');
            $dato = $this->input->post('date');
            $iduser = $this->input->post('id_user');
            if($table == "categoria") {
                $idProveedor = DB::table('proveedor')->where('id_usuario', $iduser)->select('id_proveedor')->first();
                // dd($idProveedor->id_proveedor);
                $validate = DB::table('categoria_proveedor')->where('proveedor_id', $idProveedor->id_proveedor)->first();
                if(empty($validate)) {
                    $categoria_proveedor = DB::table('categoria_proveedor')->insert([
                        'categoria_id' => $dato,
                        'proveedor_id' => $idProveedor->id_proveedor
                    ]);
                } else {
                    DB::table('categoria_proveedor')
                    ->where('proveedor_id', $idProveedor->id_proveedor)
                    ->update(['categoria_id' => $dato]);
                }
            } elseif($table == "tiempo") { ////Agregar meses a la fecha de expiracion
                // dd(date('Y/m/d', strtotime('+'.$dato.' months')));
                $cuenta = Provider::where("id_usuario", $iduser)->first();
                $cuenta->plan_expiration_date  = date('Y/m/d', strtotime('+'.$dato.' months'));
                $cuenta->save();
                return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['tiempo' => true]));
            }else{
                if($table == "tipo") {
                    $cuenta = Provider::where("id_usuario", $iduser)->first();
                    $cuenta->tipo_cuenta = $dato;
                    $cuenta->save();
                } else {
                    $user = User::find($iduser);
                    $user->activo = $dato;
                    $user->save();
                }
            }
        }
    }

    public function getCountries(){
        $countries = DB::table('pais')->get();
        return $countries;
    }

    public function getStates($id_pais = null, $actual_state = null){
        if(!is_null($actual_state)){
            $response['state'] = DB::table('estado')
                ->where('estado', $actual_state)->first();
            $response['cities'] = $this->getCities_in($response['state']->id_estado);
        }
        if(!is_null($id_pais)){
            $response['states'] = DB::table('estado')
                ->where('id_pais', $id_pais)->get();
            $response['code'] = 202;
            return $this->output->set_status_header($response["code"])
                ->set_output(json_encode($response))
                ->set_content_type("application/json");
        }
        $response['code'] = 203;
        return $this->output->set_status_header($response["code"])
                ->set_output(json_encode($response))
                ->set_content_type("application/json");
    }

    public function getCities_in($id_estado = null){
        if(!is_null($id_estado)){
            $response['cities'] = DB::table('ciudad')
                ->where('id_estado', $id_estado)->get();
            return $response['cities'];
        }
    } 

    public function modificarLocalizacion() {
        if($_POST) {
            $id = $this->input->post('Id');
            $pais = $this->input->post('Pais');
            $estado = $this->input->post('Estado');
            $ciudad = $this->input->post('Ciudad');

            $modificar = Provider::where("id_usuario", $id)->first();
            $modificar->localizacion_pais = $pais;
            $modificar->localizacion_estado = $estado;
            $modificar->localizacion_poblacion = $ciudad;
            $modificar->save();
            return $this->output->set_status_header(200);
        }
    }
}