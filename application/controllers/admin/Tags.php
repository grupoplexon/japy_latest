<?php
use Models\Tag;
use Illuminate\Database\Capsule\Manager as DB;

class Tags extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function middleware()
    {
        return ["admin_auth"];
    }

    public function index()
    {
        $data = [];
        $data['tags'] = Tag::with('subcategoria')->get();
        $data['subcategorias'] = DB::table('proveedor_categoria')->where('parent_id', '!=', null)->get();
        $this->load->view("admin/tags/index", $data);
    }

    public function new()
    {
        if($_POST){
            $title = $this->input->post('title');
            $subcategoria_id = $this->input->post('subcategoria');
            if(!empty($title))
                $tag = Tag::create([
                    'title' =>  $title,
                    'subcategoria_id' => $subcategoria_id
                ]);

            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($tag));

        }
    }

    public function edit()
    {
        if($_POST){
            $id = $this->input->post('id');
            $title = $this->input->post('title');
            $subcategoria_id = $this->input->post('subcategoria');
            $tag = Tag::find($id);
            $tag->title = $title;
            $tag->subcategoria_id = $subcategoria_id;
            $tag->save();

            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($tag));

        }
    }

    public function delete()
    {
        if($_POST){
            $id = $this->input->post('id');

            $tag = Tag::find($id);
            $tag->delete();

            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($tag));

        }
    }
}
