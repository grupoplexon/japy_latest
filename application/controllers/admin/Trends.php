<?php
use Illuminate\Database\Capsule\Manager as DB;
class Trends extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Vestido/Vestido_model", "vestido");
        $this->load->model("Vestido/Tipo_model", "tipo");
        $this->load->helper("storage");
    }


    public function index()
    {
        // $this->getTrends();
        // $this->fetch_trend();
        // $data["countries"] = $this->getCountries();
        // $this->load->view("admin/proveedores/proveedor", $data);
        $this->load->view("admin/trends/index");
    }

    function fetch_trend(){  

        $fetch_data = $this->vestido->make_datatables();  
        $data = array();  
        foreach($fetch_data as $row)  
        {   
                $sub_array = array();  
                $sub_array[] = $row->id_vestido;  
                $sub_array[] = $row->nombre;  
                $sub_array[] = $row->disenador;  
                $sub_array[] = $row->estilo;  
                $sub_array[] = $row->temporada;  
                $sub_array[] = '';

                $data[] = $sub_array;  
        }  
        $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->vestido->get_all_data(),  
                "recordsFiltered"     =>     $this->vestido->get_filtered_data(),  
                "data"                    =>     $data  
        );  
        return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($output));

    } 

    public function nueva()
    {
        $this->load->view("admin/trends/nuevo");
    }

    public function edit($id) {
        $data["tendencia"] = DB::table("vestido")->where("id_vestido", $id)->first();
        $name = DB::table("tipo_vestido")->where("id_tipo_vestido", $data["tendencia"]->tipo_vestido)->first();
        $data["tendencia"]->nombre_vestido = $name->nombre;
        $data["tipos"] = $this->tipo->getAll();

        foreach ($data["tipos"] as $key => &$t) {
            $sort        = explode(",", $t->sortable);
            $t->sortable = [];
            foreach ($sort as $key => $value) {
                $temp          = new stdClass();
                $temp->nombre  = $value;
                $temp->valores = $this->tipo->getValues($value, $t->id_tipo_vestido);
                $t->sortable[] = $temp;
            }
        }
        // dd($data["tipos"][0]->sortable[0]->valores[0]->nombre);
        // dd($data["tendencia"]);
        $this->load->view("admin/trends/edit", $data);
    }

    public function extractTypes() {
        $data["tipos"] = $this->tipo->getAll();

        foreach ($data["tipos"] as $key => &$t) {
            $sort        = explode(",", $t->sortable);
            $t->sortable = [];
            foreach ($sort as $key => $value) {
                $temp          = new stdClass();
                $temp->nombre  = $value;
                $temp->valores = $this->tipo->getValues($value, $t->id_tipo_vestido);
                $t->sortable[] = $temp;
            }
        }
        return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['data' => $data["tipos"]]));
    }

    public function upload() {
        if ($_FILES) {
            $destination = 'uploads/images/tendencia';
            $quality     = 20;
            $pngQuality  = 9;
            $file = $_FILES["files"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["files"]["name"])["extension"];
            $fileName  = store_file($data, $extension, "images/tendencia");
            $new_name_image = $file = $destination."/".$fileName;

            $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
            $status = "error";

            if($this->input->post("detalle")=="Vestidos de Novia") {
                $res = DB::table('vestido')->insert([
                    'nombre' => $this->input->post("modelo"),
                    'detalle' => $this->input->post("descripcion"),
                    'image' => $fileName,
                    'escote' => $this->input->post("titleOne"),
                    'corte' => $this->input->post("titleTwo"),
                    'disenador' => $this->input->post("diseñador"),
                    'temporada' => $this->input->post("temporada"),
                    'tipo_vestido' => $this->input->post("idVestido")
                ]);
                $status = "ok";
            } elseif($this->input->post("detalle")=="Trajes de Novio") {
                $res = DB::table('vestido')->insert([
                    'nombre' => $this->input->post("modelo"),
                    'detalle' => $this->input->post("descripcion"),
                    'image' => $fileName,
                    'estilo' => $this->input->post("titleOne"),
                    'disenador' => $this->input->post("diseñador"),
                    'temporada' => $this->input->post("temporada"),
                    'tipo_vestido' => $this->input->post("idVestido")
                ]);
                $status = "ok";
            } elseif($this->input->post("detalle")=="Vestidos de Fiesta") {
                $res = DB::table('vestido')->insert([
                    'nombre' => $this->input->post("modelo"),
                    'detalle' => $this->input->post("descripcion"),
                    'image' => $fileName,
                    'largo' => $this->input->post("titleOne"),
                    'disenador' => $this->input->post("diseñador"),
                    'temporada' => $this->input->post("temporada"),
                    'tipo_vestido' => $this->input->post("idVestido")
                ]);
                $status = "ok";
            } elseif($this->input->post("detalle")=="Joyeria") {
                $res = DB::table('vestido')->insert([
                    'nombre' => $this->input->post("modelo"),
                    'detalle' => $this->input->post("descripcion"),
                    'image' => $fileName,
                    'tipo' => $this->input->post("titleOne"),
                    'disenador' => $this->input->post("diseñador"),
                    'temporada' => $this->input->post("temporada"),
                    'tipo_vestido' => $this->input->post("idVestido")
                ]);
                $status = "ok";
            } elseif($this->input->post("detalle")=="Zapatos") {
                $res = DB::table('vestido')->insert([
                    'nombre' => $this->input->post("modelo"),
                    'detalle' => $this->input->post("descripcion"),
                    'image' => $fileName,
                    'categoria' => $this->input->post("titleOne"),
                    'disenador' => $this->input->post("diseñador"),
                    'temporada' => $this->input->post("temporada"),
                    'tipo_vestido' => $this->input->post("idVestido")
                ]);
                $status = "ok";
            } elseif($this->input->post("detalle")=="Lencería") {
                $res = DB::table('vestido')->insert([
                    'nombre' => $this->input->post("modelo"),
                    'detalle' => $this->input->post("descripcion"),
                    'image' => $fileName,
                    'tipo' => $this->input->post("titleOne"),
                    'disenador' => $this->input->post("diseñador"),
                    'temporada' => $this->input->post("temporada"),
                    'tipo_vestido' => $this->input->post("idVestido")
                ]);
                $status = "ok";
            } elseif($this->input->post("detalle")=="Complementos") {
                $res = DB::table('vestido')->insert([
                    'nombre' => $this->input->post("modelo"),
                    'detalle' => $this->input->post("descripcion"),
                    'image' => $fileName,
                    'categoria' => $this->input->post("titleOne"),
                    'tipo' => $this->input->post("titleTwo"),
                    'disenador' => $this->input->post("diseñador"),
                    'temporada' => $this->input->post("temporada"),
                    'tipo_vestido' => $this->input->post("idVestido")
                ]);
                $status = "ok";
            }

            return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['state' => $status]));
        }
    }

    public function editUpload() {
        if($_POST) {
            $result = DB::table('vestido')->where('id_vestido', $this->input->post("id"))->first();
            $status = "error";
            // dd($result->id_vestido);
            if($this->input->post("indicator")==0 || $_FILES) {
                if($this->input->post("detalle")=="Vestidos de Novia") {
                    if($this->input->post("titleOne")=='Selecciona una opcion') {
                        $var = $this->input->post("actualOne");
                    } else {
                        $var = $this->input->post("titleOne");
                    }
                    if($this->input->post("titleTwo")=='Selecciona una opcion') {
                        $var2 = $this->input->post("actualTwo");
                    } else {
                        $var2 = $this->input->post("titleTwo");
                    }
                    $update = DB::table("vestido")->where("id_vestido", $result->id_vestido)->update(
                    array(
                        'escote' => $var,
                        'corte' => $var2,
                        'nombre' => $this->input->post("modelo"),
                        'disenador' => $this->input->post("disenador"),
                        'temporada' => $this->input->post("temporada"),
                        'detalle' => $this->input->post("descripcion"),
                    ));
                    $status = "ok";
                } elseif($this->input->post("detalle")=="Trajes de Novio") {
                    if($this->input->post("titleOne")=='Selecciona una opcion') {
                        $var = $this->input->post("actualOne");
                    } else {
                        $var = $this->input->post("titleOne");
                    }
                    $update = DB::table("vestido")->where("id_vestido", $result->id_vestido)->update(
                    array(
                        'estilo' => $var,
                        'nombre' => $this->input->post("modelo"),
                        'disenador' => $this->input->post("disenador"),
                        'temporada' => $this->input->post("temporada"),
                        'detalle' => $this->input->post("descripcion"),
                    ));
                    $status = "ok";
                } elseif($this->input->post("detalle")=="Vestidos de Fiesta") {
                    if($this->input->post("titleOne")=='Selecciona una opcion') {
                        $var = $this->input->post("actualOne");
                    } else {
                        $var = $this->input->post("titleOne");
                    }
                    $update = DB::table("vestido")->where("id_vestido", $result->id_vestido)->update(
                    array(
                        'largo' => $var,
                        'nombre' => $this->input->post("modelo"),
                        'disenador' => $this->input->post("disenador"),
                        'temporada' => $this->input->post("temporada"),
                        'detalle' => $this->input->post("descripcion"),
                    ));
                    $status = "ok";
                } elseif($this->input->post("detalle")=="Joyeria") {
                    if($this->input->post("titleOne")=='Selecciona una opcion') {
                        $var = $this->input->post("actualOne");
                    } else {
                        $var = $this->input->post("titleOne");
                    }
                    $update = DB::table("vestido")->where("id_vestido", $result->id_vestido)->update(
                    array(
                        'tipo' => $var,
                        'nombre' => $this->input->post("modelo"),
                        'disenador' => $this->input->post("disenador"),
                        'temporada' => $this->input->post("temporada"),
                        'detalle' => $this->input->post("descripcion"),
                    ));
                    $status = "ok";
                } elseif($this->input->post("detalle")=="Zapatos") {
                    if($this->input->post("titleOne")=='Selecciona una opcion') {
                        $var = $this->input->post("actualOne");
                    } else {
                        $var = $this->input->post("titleOne");
                    }
                    $update = DB::table("vestido")->where("id_vestido", $result->id_vestido)->update(
                    array(
                        'categoria' => $var,
                        'nombre' => $this->input->post("modelo"),
                        'disenador' => $this->input->post("disenador"),
                        'temporada' => $this->input->post("temporada"),
                        'detalle' => $this->input->post("descripcion"),
                    ));
                    $status = "ok";
                } elseif($this->input->post("detalle")=="Lencer├¡a") {
                    if($this->input->post("titleOne")=='Selecciona una opcion') {
                        $var = $this->input->post("actualOne");
                    } else {
                        $var = $this->input->post("titleOne");
                    }
                    $update = DB::table("vestido")->where("id_vestido", $result->id_vestido)->update(
                    array(
                        'tipo' => $var,
                        'nombre' => $this->input->post("modelo"),
                        'disenador' => $this->input->post("disenador"),
                        'temporada' => $this->input->post("temporada"),
                        'detalle' => $this->input->post("descripcion"),
                    ));
                    $status = "ok";
                } elseif($this->input->post("detalle")=="Complementos") {
                    if($this->input->post("titleOne")=='Selecciona una opcion') {
                        $var = $this->input->post("actualOne");
                    } else {
                        $var = $this->input->post("titleOne");
                    }
                    if($this->input->post("titleTwo")=='Selecciona una opcion') {
                        $var2 = $this->input->post("actualTwo");
                    } else {
                        $var2 = $this->input->post("titleTwo");
                    }
                    $update = DB::table("vestido")->where("id_vestido", $result->id_vestido)->update(
                    array(
                        'categoria' => $var,
                        'tipo' => $var2,
                        'nombre' => $this->input->post("modelo"),
                        'disenador' => $this->input->post("disenador"),
                        'temporada' => $this->input->post("temporada"),
                        'detalle' => $this->input->post("descripcion"),
                    ));
                    $status = "ok";
                }
            }
            if($this->input->post("indicator")==1) {
                $imagesPaths [] = "uploads/images/tendencia/$result->image";

                foreach ($imagesPaths as $imagePath) {
                    if (file_exists($imagePath)) {
                        unlink($imagePath);
                    }
                }

                $destination = 'uploads/images/tendencia';
                $quality     = 20;
                $pngQuality  = 9;
                $file = $_FILES["files"];
                $data     = file_get_contents($file["tmp_name"]);
                $extension = pathinfo($_FILES["files"]["name"])["extension"];
                $fileName  = store_file($data, $extension, "images/tendencia");
                $new_name_image = $file = $destination."/".$fileName;

                $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);

                $update = DB::table("vestido")->where("id_vestido", $result->id_vestido)->update(
                array(
                    'image' => $fileName,
                ));
                $status = "ok";
            }

            return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['state' => $status]));
        }
    }

    public function eliminar() {
        if($_POST) {
            $image = DB::table('vestido')->where('id_vestido', $this->input->post("id_tendencia"))->first();
            $imagesPaths [] = "uploads/images/tendencia/$image->image";

            foreach ($imagesPaths as $imagePath) {
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
            $result = DB::table('vestido')->where('id_vestido', $this->input->post("id_tendencia"))->delete();
            return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['state' => 'ok']));
        }
    }
}