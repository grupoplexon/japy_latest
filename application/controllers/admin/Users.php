<?php
use Illuminate\Database\Capsule\Manager as DB;
class Users extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Users/Usuarios_model", "usuarios");
    }

    public function index() {
        $this->load->view("admin/users/user");
    }

    public function edit($userId)
    {
        $data['user'] = User::find($userId);

        if ( ! $data['user']) {
            show_404('/');
        }

        switch ($data['user']->rol) {
            case 1:
                $data['user']->rol = "Administrador";
                break;
            case 2:
                $data['user']->rol = "Novio";
                break;
            case 3:
                $data['user']->rol = "Proveedor";
                $data['provider']  = User::with("provider")->find($userId)->Provider;
                break;
            case 4:
                $data['user']->rol = "Redactor";
                break;
            case 5:
                $data['user']->rol = "Administrador aplicacion";
                break;
            case 6:
                $data['user']->rol = "Moderador";
                break;
            case 7:
                $data['user']->rol = "Galeria";
                break;
            case 8:
                $data['user']->rol = "Blog";
                break;
        }
        $this->load->view("admin/users/edit", $data);
    }

    public function fetch_trend() {
        $fetch_data = $this->usuarios->make_datatables();  
        $data = array();  
        foreach($fetch_data as $row)  
        {   
                $sub_array = array();  
                $sub_array[] = $row->id_usuario;  
                $sub_array[] = $row->nombre;  
                $sub_array[] = $row->usuario;  
                $sub_array[] = $row->rol;  
                $sub_array[] = $row->activo;  
                $sub_array[] = '';

                $data[] = $sub_array;  
        }  
        $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->usuarios->get_all_data(),  
                "recordsFiltered"     =>     $this->usuarios->get_filtered_data(),  
                "data"                    =>     $data  
        );  
        return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode($output));
    }

    public function editUser() {
        if($_POST) {
            $id = $this->input->post("id");
            $status = $this->input->post("status");
            $name = $this->input->post("name");
            $lastname = $this->input->post("lastName");
            $username = $this->input->post("username");
            $email = $this->input->post("email");
            $password = $this->input->post("password");

            $data = User::find($id);
            // dd($data);
            if(!empty($data)) {
                $data->nombre = $name;
                $data->apellido = $lastname;
                $data->correo = $email;
                $data->usuario = $username;
                if($status=="true") {
                    $data->activo = 2;
                } else {
                    $data->activo = 0;
                }
                if(!empty($password)) {
                    $data->contrasena = sha1(md5(sha1($password)));
                }
                // dd($data);
                $data->save();
            }

            if($data->rol==3) {
                $provider = DB::table("proveedor")->where("id_usuario", $id)->update(array('nombre' => $name));
            }
            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['validate' => true]));
        }
    }

    public function editInfo() {
        if($_POST) {
            $id = $this->input->post("id");
            $cel = $this->input->post("celular");
            $phone = $this->input->post("telefono");
            $pweb = $this->input->post("pWeb");
            $contacto = $this->input->post("email");
            $provider = DB::table("proveedor")->where("id_usuario", $id)->update(
                array(
                    'contacto_telefono' => $phone,
                    'contacto_celular' => $cel,
                    'contacto_pag_web' => $pweb,
                    'contacto_correos' => $contacto
                ));
            if($provider) {
                return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['validate' => true]));
            } else {
                return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['validate' => false]));
            }
        }
    }
}