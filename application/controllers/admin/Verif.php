<?php

use Illuminate\Database\Eloquent;

class Verif extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->model("Proveedor/FAQ_model", "faq");
        $this->load->model("Proveedor/RespuestaFAQ_model", "respuesta");
        $this->load->helper("slug");
    }

    public function index()
    {
        $data["verification"]= Verification::where("status", 1)->get();
        $this->load->view("admin/verification/index", $data);
    }

    public function updateVerification()
    {  
        $id= $this->input->post("id2");

        $proveedor = Verification::where('id',$id)->get();

        $prov = Verification::find($id);

        $prov->status = 0;
        $prov->aprobacion = 1;
        $prov->save();

        $this->updateDescripcion($prov->id_proveedor, $prov->nombre_prov,$prov->descripcion);

       
    }



    public function updateVerificationDenegada()
    {  
        $id= $this->input->post("id2");

        $proveedor = Verification::where('id',$id)->get();

        $prov = Verification::find($id);

        $prov->status = 0;
        $prov->aprobacion = 0;
        $prov->save();

        if ($_POST) {

            dd($prov);
        }

    }

    public function updateDescripcion($id,$nombre,$descripcion)
    {
         if ($_POST) {
            $e       = $this->proveedor->get(["id_usuario" => $id]);
            $empresa = [
                    "descripcion" => $descripcion,
                    "nombre"      => $nombre,
            ];
            if ($this->input->isValid()) {
                $empresa["slug"] = create_slug($e->id_proveedor, $empresa["nombre"]);
                $b               = $this->proveedor->update($e->id_proveedor, $empresa);
                if ($b) {
                    
                    $this->session->set_userdata("tipo_mensaje", "success");
                    $this->session->set_userdata("mensaje", "Descripcion actualizada");
                }
            }
        }
        
    }

    public function updatePrecios(){

        $id= $this->input->post("id2");

        $proveedor = Verification::where('id',$id)->get();

        $prov = Verification::find($id);

        $idprov= $prov->id_proveedor;
        $idfaq= $prov->id_faq;
        $precio = $prov->precios;


        $prov->status = 0;
        $prov->aprobacion = 1;
        $prov->save();
    
        $this->respuesta->delete(["id_proveedor" => $idprov]);

        
        $respuestas[] = [
            "id_proveedor" => $idprov,
            "id_faq"       => $idfaq,
            "respuesta"    => $precio,
        ];

        $this->respuesta->insert_batch($respuestas);

    }

    /* public function extraerDatos() {
        if($_POST) {
            $alerts = Alert::where('status', 1)->where('id_temp', 1)->get();
            
            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['success' => true, 'alerts' => $alerts, 'tamaño' => sizeof($alerts)]));
        }
    } */
    
    public function extraerDatos() {
        if($_POST) {
            $a['galeria'] = Alert::where('status', 1)->where('id_temp', 1)->get();
            // dd($alerts[0]['attributes']['id_user']);
            // $alerts = array_merge($a, $images);
            
            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['success' => true, 'alerts' => $a, 'tamaño' => sizeof($a)]));
        }
    }

    public function cambiarEstados() {
        if($_POST) {
            $id = $this->input->post("id_user");

            $alert = Alert::find($id);
            $alert->status = 0;
            $alert->id_temp = 0;
            $alert->save();

            $user = User::find($alert["id_user"]);
            $user->activo = 2;
            $user->save();
        }
    }
    
    public function mostrarFotos() {
        if($_POST) {
            $id = $this->input->post("id_user");
            $id_provider = Provider::where('id_usuario', $id)->first();
            $images   = $this->galeria->getImagenesProveedor($id_provider['attributes']['id_proveedor']);
            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['success' => true, 'images' => $images]));
        }
    }
}
