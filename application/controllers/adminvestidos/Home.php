<?php

use Intervention\Image\ImageManagerStatic as Image;

class Home extends My_Controller
{
    protected function middleware()
    {
        return ['admin_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Vestido/Tipo_model", "tipo");
        $this->load->model("Vestido/Vestido_model", "vestido");
    }

    public function index()
    {
        $data["tipos"] = $this->tipo->countArticulos();
        $this->load->view("app/vestidos/index", $data);
    }

    public function nuevo()
    {
        if ($_POST) {
            $this->session->set_userdata("tipo_mensaje", "danger");
            $this->session->set_userdata("mensaje", "Ocurrio un error al crear el artículo");
            $imagen = $this->input->post("file");
            $file   = $this->parseBase64($imagen);

            if ($this->input->post("file")) {
                $imageBase64 = (string)Image::make($this->input->post("file"))->encode('data-url', 60);
            }

            if ( ! $file->mime) {
                $this->session->set_userdata("mensaje", "La imagen no es válida");
            } else {
                $id_tipo        = $this->input->post("tipo_vestido", true, "Integer", ["required" => true, "min" => 0]);
                $tipo           = $this->tipo->get($id_tipo);
                $vestido        = [
                    "tipo_vestido" => $id_tipo,
                    "detalle"      => $this->input->post("detalle", true, "String", ["required" => true, "minSize" => 10]),
                    "nombre"       => $this->clearNameVestido($this->input->post("nombre", true, "String", ["required" => true])),
                    "imagen"       => isset($imageBase64) ? $imageBase64 : null,
                    "mime_imagen"  => isset($imageBase64) ? $file->mime : null,
                ];
                $tipo->sortable = explode(",", $tipo->sortable);
                foreach ($tipo->sortable as $key => $sort) {
                    if ($sort == "diseñador") {
                        $vestido["disenador"] = $this->input->post($sort, true, "String", ["required" => true]);
                    } else {
                        $vestido[$sort] = $this->input->post($sort, true, "String", ["required" => true]);
                    }
                }
                if ($this->input->isValid()) {
                    if ($this->vestido->insert($vestido)) {
                        $this->session->set_userdata("tipo_mensaje", "success");
                        $this->session->set_userdata("mensaje", "Articulo creado  correctamente");
                        redirect("adminvestidos/home/index");
                    }
                } else {
                    $this->session->set_userdata("mensaje", $this->input->getErrors());
                }
            }
        }

        $data["tipos"] = $this->tipo->getAll();

        foreach ($data["tipos"] as $key => &$t) {
            $sort        = explode(",", $t->sortable);
            $t->sortable = [];
            foreach ($sort as $key => $value) {
                $temp          = new stdClass();
                $temp->nombre  = $value;
                $temp->valores = $this->tipo->getValues($value, $t->id_tipo_vestido);
                $t->sortable[] = $temp;
            }
        }

        $this->load->view("app/vestidos/nuevo", $data);
    }

    public function editar($id_vestido)
    {
        $data["vestido"] = $this->vestido->get($id_vestido);
        if ($data["vestido"]) {
            $tipo = $this->tipo->get($data["vestido"]->tipo_vestido);
            if ($_POST) {
                $this->session->set_userdata("tipo_mensaje", "danger");
                $this->session->set_userdata("mensaje", "Ocurrio un error al crear el artículo");
                $imagen = $this->input->post("file");
                $file   = $this->parseBase64($imagen);

                if ($this->input->post("file") && ($data["vestido"]->imagen != $this->input->post("file"))) {
                    $imageBase64 = (string)Image::make($this->input->post("file"))->encode('data-url', 60);
                }

                if ( ! $file->mime) {
                    $this->session->set_userdata("mensaje", "La imagen no es válida");
                } else {
                    $vestido = [
                        "detalle"     => $this->input->post("detalle", true, "String", ["required" => true, "minSize" => 10]),
                        "nombre"      => str_replace("-", "", $this->input->post("nombre", true, "String", ["required" => true])),
                        "imagen"      => isset($imageBase64) ? $imageBase64 : null,
                        "mime_imagen" => isset($imageBase64) ? $file->mime : null,
                    ];

                    $tipo->sortable = explode(",", $tipo->sortable);

                    foreach ($tipo->sortable as $key => $sort) {
                        if ($sort == "diseñador") {
                            $vestido["disenador"] = $this->input->post($sort, true, "String", ["required" => true]);
                        } else {
                            $vestido[$sort] = $this->input->post($sort, true, "String", ["required" => true]);
                        }
                    }

                    if ($this->input->isValid()) {
                        if ($this->vestido->update($id_vestido, $vestido)) {
                            $this->session->set_userdata("tipo_mensaje", "success");
                            $this->session->set_userdata("mensaje", "Articulo actualizado correctamente");
                            redirect("adminvestidos/home/articulos/".str_replace(" ", "-", $tipo->nombre));
                        }
                    } else {
                        $this->session->set_userdata("mensaje", $this->input->getErrors());
                    }
                }
            }

            $data["tipo"]           = $tipo;
            $sort                   = explode(",", $data["tipo"]->sortable);
            $data["tipo"]->sortable = [];

            foreach ($sort as $key => $value) {
                $temp                     = new stdClass();
                $temp->nombre             = $value;
                $temp->valores            = $this->tipo->getValues($value, $data["tipo"]->id_tipo_vestido);
                $data["tipo"]->sortable[] = $temp;
            }

            return $this->load->view("app/vestidos/edit", $data);
        }
        show_404();
    }

    private function clearNameVestido($nombre)
    {
        $nombre = str_replace(".", "", str_replace("-", "", $nombre));
        $nombre = str_replace("/", "", str_replace("%", "", $nombre));
        $nombre = str_replace("+", "", str_replace(":", "", $nombre));

        return $nombre;
    }

    public function articulos($nombre)
    {
        $data["tipo"] = $this->tipo->get(["nombre" => str_replace("-", " ", $nombre)]);
        if ($data["tipo"]) {
            $data["articulos"] = $this->vestido->getList(["tipo_vestido" => $data["tipo"]->id_tipo_vestido]);

            return $this->load->view("app/vestidos/list", $data);
        }
        show_404();
    }

    public function delete($id_vestido)
    {
        $this->vestido->delete($id_vestido);
        redirect($_SERVER['HTTP_REFERER']);
    }

    private function parseBase64($strBase64)
    {
        $o = new stdClass();
        if (empty($strBase64)) {
            $o->mime   = null;
            $o->imagen = null;
        } else {
            $mime      = explode(",", $strBase64);
            $o->imagen = base64_decode($mime[1]);
            $mime      = explode(";", $mime[0]);
            $mime      = explode(":", $mime[0]);
            $o->mime   = $mime[1];
        }

        return $o;
    }

}
