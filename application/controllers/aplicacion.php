<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as DB;

class Aplicacion extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
    }

    public function index()
    {
        
        $this->load->view("aplicacion");
    }
}