<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Categorias extends CI_Controller {

    public $input;

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model('Blog/Post_model', 'post');
        $this->load->model("Blog/Comentario_model", "comentario");
        $this->load->model('Blog/Categoria_model', 'categoria');
        $this->load->model('Blog/Grupo_model', 'grupo');
        $this->load->helper('formats');
        $this->input = new MY_Input();
    }

    public function index() {
        $data['grupo'] = $this->grupo->getAll();
        foreach ($data['grupo'] as &$value) {
            $value->total = count($this->categoria->getList(array('id_blog_grupo' => $value->id_blog_grupo))) - 1;
        }
        $this->load->view('blog/grupo/index', $data);
    }

    public function agregar($id) {
        $data["titulo"] = "Agregar Categoria";
        $data['grupo'] = $this->grupo->get($id);
        if ($_POST) {
            $this->session->set_userdata('mensaje', 'Ocurrio un error al agregar la categoria');
            $this->session->set_userdata('tipo_mensaje', 'danger');
            $nombre = $this->input->post('nombre', TRUE, "String", array("required" => true));
            $descripcion = $this->input->post('descripcion', TRUE, "String", array("required" => true, 'maxSize' => 255));
            if ($this->input->isValid()) {
                $b = $this->categoria->insert(array("nombre" => $nombre, "descripcion" => $descripcion, 'id_blog_grupo' => $id));
                if ($b) {
                    $this->session->set_userdata('mensaje', 'Categoria Agregada');
                    $this->session->set_userdata('tipo_mensaje', 'success');
                    redirect('blog/categorias/grupo/' . $id);
                }
            } else {
                $this->session->set_userdata('mensaje', $this->input->getErrors());
            }
        }
        $this->load->view('blog/categorias/agregar', $data);
    }

    public function grupo_agregar() {
        $data["titulo"] = "Agregar Grupo";
        if ($_POST) {
            $this->session->set_userdata('mensaje', 'Ocurrio un error al agregar el grupo');
            $this->session->set_userdata('tipo_mensaje', 'danger');
            $nombre = $this->input->post('nombre', TRUE, "String", array("required" => true));
            if ($this->input->isValid()) {
                $b = $this->grupo->insert(array("nombre" => $nombre));
                if ($b) {
                    $this->session->set_userdata('mensaje', 'Grupo Agregado');
                    $this->session->set_userdata('tipo_mensaje', 'success');
                    redirect('blog/categorias/');
                }
            } else {
                $this->session->set_userdata('mensaje', $this->input->getErrors());
            }
        }
        $this->load->view('blog/grupo/agregar', $data);
    }

    public function grupo($id) {
        $data['grupo'] = $this->grupo->get($id);
        $data['categorias'] = $this->categoria->getList(array('id_blog_grupo' => $id));
        foreach ($data['categorias'] as &$value) {
            $value->total = $this->post->countCat($value->id_blog_categoria);
        }
        $this->load->view('blog/categorias/index', $data);
    }

    public function editar($id) {
        $data['categoria'] = $this->categoria->get($id);
        if ($data['categoria']) {
            if ($_POST) {
                $this->session->set_userdata('mensaje', 'Ocurrio un error al agregar la categoria');
                $this->session->set_userdata('tipo_mensaje', 'danger');
                $nombre = $this->input->post('nombre', TRUE, "String", array("required" => true));
                $descripcion = $this->input->post('descripcion', TRUE, "String", array("required" => true, 'maxSize' => 255));
                if ($this->input->isValid()) {
                    $b = $this->categoria->update($id, array('nombre' => $nombre, "descripcion" => $descripcion));
                    if ($b) {
                        $this->session->set_userdata('mensaje', 'Categoria Agregada');
                        $this->session->set_userdata('tipo_mensaje', 'success');
                        redirect('blog/categorias/grupo/' . $data['categoria']->id_blog_grupo);
                    }
                } else {
                    $this->session->set_userdata('mensaje', $this->input->getErrors());
                }
            }
            $this->load->view('blog/categorias/editar', $data);
        }
    }

}
