<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Comentario
 *
 * @author Evolutel
 */
class Comentario extends CI_Controller
{

    public $input;

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model('Blog/Categoria_model', 'categoria');
        $this->load->model('Blog/Post_model', 'post');
        $this->load->model("Blog/Grupo_model", "grupo");
        $this->load->model("Blog/Comentario_model", "comentario");
        $this->load->model("Blog/Favorito_model", "fav");
        $this->load->model("Login_model", "usuario");
        $this->load->helper('formats');
        $this->input = new MY_Input();
    }

    public function index()
    {
        $pagina = $this->input->get("pag", true);

        if ($pagina == 0) {
            $pagina = 1;
        }

        $total_paginas = ceil($this->post->count() / 50);

        if ($pagina <= $total_paginas) {
            $limit1 = ($pagina - 1) * 50;
            $limit2 = $pagina * 50;

            $data["posts"] = $this->comentario->getPostNewComentado($this->session->userdata("rol") == Checker::$ADMIN,
                    $this->session->userdata("usuario"));

            foreach ($data["posts"] as $key => &$value) {
                $value->autor = $this->post->getAutor($value->id_autor);
            }

            $data["total_paginas"] = $total_paginas;
            $data["pagina"]        = $pagina;

            $this->load->view("blog/comentarios/index", $data);

            return;
        }
        $this->output->set_status_header(404);
    }

    public function favorito($id_blog_comentario)
    {
        $this->output->set_content_type('application/json');
        if ($_POST && $this->session->userdata("usuario")) {
            $id_post = $this->input->post("post", true, "Integer", ["required" => true, "min" => 0]);
            $id_comm = $this->input->post("comment", true, "Integer", ["required" => true, "min" => 0]);
            if ($this->input->isValid() && $id_comm == $id_blog_comentario) {
                $b = $this->comentario->changeFav($id_post, $id_comm, $this->session->userdata("usuario"));
                if ($b) {
                    return $this->output->set_status_header(200)
                            ->set_output(json_encode([
                                    'success' => true,
                                    'data'    => true,
                            ]));
                }
            }
        }

        return $this->output->set_status_header(404)
                ->set_output(json_encode([
                        'success' => false,
                        'data'    => 'error',
                ]));
    }

}
