<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Models\Blog\Post;

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->helper("formats");
        $this->load->model("Blog/Categoria_model", "categoria");
        $this->load->model("Blog/Comentario_model", "comentario");
        $this->load->model("Blog/Favorito_model", "fav");
        $this->load->model("Blog/Post_model", "post");
        //    $this->load->model("Blog/Grupo_model", "grupo");
    }

    public function index($cat = 0, $page = 1)
    {
        $wantsJson    = $this->input->get_request_header('accept', true) === "application/json";
        $data["cat"]  = $cat;
        $data["page"] = $page;
        // $data["grupo_cat"]  = $this->grupo->getAll();
        $data["categorias"] = $this->categoria->getAll();

        if ($cat != 0) {
            $data["categoria"] = $this->categoria->get($cat);
        }

        $total_paginas = ceil($this->post->countCat($cat) / 5);

        if ($page <= $total_paginas) {
            $limit1 = 5;
            $limit2 = ($page > 0 ? $page - 1 : $page) * 5;

            $data["total_paginas"] = $total_paginas;
            $data["posts"]         = $this->post->getListCategoria($cat, $limit1, $limit2);

            foreach ($data["posts"] as &$post) {
                $post->autor       = $this->post->getAutor($post->id_autor);
                $post->tags        = Post::with("tags")->find($post->id_blog_post);//$this->post->getTags($post->id_blog_post);
                $post->fav         = $this->fav->countPost($post->id_blog_post);
                $post->comentarios = $this->comentario->countPost($post->id_blog_post);
            }
        } else {
            $data["post"] = null;
        }

        $data["destacados"] = $this->post->getDestacados();
        $data["comentados"] = $this->post->getMasComentado();
        // dd($data["posts"]->tags->tags->title);
        if ($wantsJson) {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(
                    json_encode($data['posts'])
                );
        } else {
            $this->load->view("blog/index", $data);
        }
    }

    public function info()
    {
        if ($this->checker->isLogin()) {
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(
                    json_encode(
                        [
                            'success' => true,
                        ]
                    )
                );
        }

        return $this->output->set_status_header(404);
    }

}
