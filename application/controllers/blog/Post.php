<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Models\Blog\Post as BlogPost;
use Models\Tag;

class Post extends CI_Controller
{

    public $input;

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Blog/Categoria_model", "categoria");
        $this->load->model("Blog/Post_model", "post");
        //$this->load->model("Blog/Grupo_model", "grupo");
        $this->load->model("Blog/Comentario_model", "comentario");
        $this->load->model("Blog/Favorito_model", "fav");
        $this->load->model("Login_model", "usuario");
        $this->load->helper("formats");
        $this->input = new MY_Input();
    }

    public function index()
    {
        $pagina = $this->input->get("pag", true);

        if ($pagina == 0) {
            $pagina = 1;
        }

        $total_paginas = ceil($this->post->count() / 50);

        if ($pagina <= $total_paginas) {
            $limit1 = ($pagina - 1) * 50;
            $limit2 = $pagina * 50;
            if ($this->session->userdata("rol") == Checker::$ADMIN) {
                $data["posts"] = $this->post->getAll("id_blog_post,titulo,fecha_creacion,id_autor,publicado,SUBSTRING(contenido,0,50) as contenido", $limit1, $limit2);
            } else {
                $data["posts"] = $this->post->getAllRedactor($this->session->userdata("usuario"), $limit1, $limit2);
            }
            foreach ($data["posts"] as &$value) {
                $value->autor       = $this->post->getAutor($value->id_autor);
                $value->tags        = $this->post->getTags($value->id_blog_post);
                $value->fav         = $this->fav->countPost($value->id_blog_post);
                $value->comentarios = $this->comentario->countPost($value->id_blog_post);
            }

            $data["total_paginas"] = $total_paginas;
            $data["pagina"]        = $pagina;

            $this->load->view("blog/posts/index", $data);

            return;
        }
        $this->output->set_status_header(404);
    }

    public function publish()
    {
        $this->output->set_content_type('application/json');
        if ($_POST && ($this->session->userdata("rol") == Checker::$ADMIN || $this->session->userdata("rol") == Checker::$REDACTOR)) {
            $post   = $this->input->post("post", true, "Integer", ["required" => true, "min" => 0]);
            $estado = $this->input->post("estado", true, "Intger", ["required" => true, "min" => 0]);
            if ($this->input->isValid()) {
                if ($this->session->userdata("rol") == Checker::$ADMIN) {
                    $p = $this->post->get($post);
                } else {
                    $p = $this->post->get(["id_blog_post" => $post, "id_autor" => $this->session->userdata("usuario")]);
                }
                if ($p) {
                    if ($this->post->update($post, ["publicado" => ($estado == 1 ? 0 : 1)])) {
                        return $this->output->set_status_header(200)
                            ->set_output(
                                json_encode(
                                    [
                                        'success' => true,
                                        'data'    => "ok",
                                    ]
                                )
                            );
                    }
                }
            }
        }

        return $this->output->set_status_header(404);
    }

    public function ver($id_post, $social = null)
    {
        $data["id_post"]     = $id_post;
        $data["categorias"]  = $this->categoria->getAll();
        $data["post"]        = $this->post->get($id_post);
        $data["post"]->tags  = BlogPost::with("tags")->find($id_post);
        $data["post"]->autor = $this->post->getAutor($data["post"]->id_autor);
        $data["post"]->fav   = $this->fav->countPost($data["post"]->id_blog_post);
        $data["comentarios"] = $this->comentario->getComentarios(["parent" => null, "id_blog_post" => $id_post]);

        if ($data["comentarios"]) {
            foreach ($data["comentarios"] as $key => &$c) {
                $c->usuario     = $this->usuario->getUsuario($c->id_usuario);
                $c->favorito    = $this->comentario->isFavorito(
                    $this->session->userdata("usuario"),
                    $c->id_blog_comentario
                );
                $c->favoritos   = $this->comentario->countFavoritos($c->id_blog_comentario);
                $c->comentarios = $this->comentario->countComentarios($c->id_blog_comentario);
            }
        }

        if ($this->checker->isLogin()) {
            $data["favorito"] = $this->fav->isFavorito($this->session->userdata("usuario"), $id_post);
        }

        $data["titulo"] = $data["post"]->titulo;

        $data["destacados"] = $this->post->getDestacados();
        $data["comentados"] = $this->post->getMasComentado();
        if ($this->checker->isBlogger()) {
            $this->comentario->setComentariosLeidos($id_post);
        }

        $data["posts"]       = $this->post->getListCategoria(0, 3, 0);

        $data["providers"] = Provider::inRandomOrder()->limit(3)->get();

        $this->load->view("blog/posts/ver", $data);
    }

    public function comentar($id_blog)
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $comentario = $this->input->post(
                "comentario",
                true,
                "String",
                ["required" => true, "min" => 1, "max" => 400]
            );
            if ($this->input->isValid()) {
                $blog_comment = [
                    "id_blog_post" => $id_blog,
                    "id_usuario"   => $this->session->userdata("usuario"),
                    "mensaje"      => $comentario,
                    "nuevo"        => $this->checker->isBlogger() ? 0 : 1,
                ];
                $b            = $this->comentario->insert($blog_comment);
                if ($b) {
                    return $this->output->set_status_header(200)
                        ->set_output(
                            json_encode(
                                [
                                    'success' => true,
                                    'data'    => $this->comentario->last_id(),
                                ]
                            )
                        );
                }
            }
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        'success' => false,
                        'data'    => 'error',
                    ]
                )
            );
    }

    public function comentarios($id_blog_comentario)
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $comentario = $this->input->post('comment', true, "Integer", ["required" => true, "min" => 1]);
            if ($this->input->isValid() && $id_blog_comentario == $comentario) {
                $b = $this->comentario->getListOrderBy(["parent" => $comentario], "fecha", "desc");
                if ($b) {
                    $comentarios = [];

                    foreach ($b as $key => $c) {
                        $user                       = $this->usuario->getUsuario($c->id_usuario);
                        $comment                    = new stdClass();
                        $comment->mensaje           = $c->mensaje;
                        $comment->id                = $c->id_blog_comentario;
                        $comment->fecha             = relativeTimeFormat($c->fecha);
                        $comment->favorito          = $this->comentario->isFavorito(
                            $this->session->userdata("usuario"),
                            $c->id_blog_comentario
                        );
                        $comment->usuario           = new stdClass();
                        $comment->usuario->id       = $user->id_usuario;
                        $comment->usuario->apellido = $user->apellido;
                        $comment->usuario->nombre   = $user->nombre;
                        $comment->favoritoCount     = $this->comentario->countFavoritos($c->id_blog_comentario);
                        $comentarios[]              = $comment;
                    }

                    return $this->output->set_status_header(200)
                        ->set_output(
                            json_encode(
                                [
                                    'success' => true,
                                    'data'    => $comentarios,
                                ]
                            )
                        );
                }
            }
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        'success' => false,
                        'data'    => 'error',
                    ]
                )
            );
    }

    public function comentar_comment($id_blog)
    {
        $this->output->set_content_type('application/json');
        if ($_POST && $this->session->userdata("usuario")) {
            $comentario = $this->input->post(
                "comentario",
                true,
                "String",
                ["required" => true, "minSize" => 1, "maxSize" => 400]
            );
            $parent     = $this->input->post("parent", true, "Integer", ["required" => true, "min" => 1, "max" => 400]);
            if ($this->input->isValid()) {
                $blog_comment = [
                    "id_blog_post" => $id_blog,
                    "id_usuario"   => $this->session->userdata("usuario"),
                    "mensaje"      => $comentario,
                    "parent"       => $parent,
                    "nuevo"        => $this->checker->isBlogger() ? 0 : 1,
                ];
                $b            = $this->comentario->insert($blog_comment);
                if ($b) {
                    return $this->output->set_status_header(200)
                        ->set_output(
                            json_encode(
                                [
                                    'success' => true,
                                    'data'    => $this->comentario->last_id(),
                                ]
                            )
                        );
                }
            }
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        'success' => false,
                        'data'    => 'error',
                    ]
                )
            );
    }

    public function favorito($id_blog)
    {
        $this->output->set_content_type('application/json');
        if ($_POST && $this->session->userdata("usuario")) {
            if ($this->input->isValid()) {
                $favorito = [
                    "id_blog_post" => $id_blog,
                    "id_usuario"   => $this->session->userdata("usuario"),
                ];
                $f        = $this->fav->get($favorito);
                if ( ! $f) {
                    $b = $this->fav->insert($favorito);
                } else {
                    $b = $this->fav->delete($f->id_blog_favorito);
                }
                if ($b) {
                    return $this->output->set_status_header(200)
                        ->set_output(
                            json_encode(
                                [
                                    'success' => true,
                                    'data'    => $favorito,
                                ]
                            )
                        );
                }
            }
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        "success" => false,
                        "data"    => "error",
                    ]
                )
            );
    }


    public function nuevo()
    {
        $data["tags"] = Tag::get();
        $this->load->view("blog/posts/redactar", $data);
    }

    public function editar($id)
    {
        $data["categorias"] = $this->categoria->getAll();
        $data["id_post"]    = $id;
        $data["post"]       = $this->post->get($id);
        $data["tags"]       = BlogPost::with("tags")->find($data["post"]->id_blog_post);

        $this->load->view("blog/posts/redactar", $data);
    }

    public function getTagsById()
    {
        $response = [
            "success" => false,
            "data"    => [],
            "code"    => 404,
        ];

        if ($_GET) {
            $id   = $this->input->get("id");
            $post = BlogPost::with("tags")->find($id);

            if ($post) {
                $response["success"] = true;
                $response["data"]    = $post->tags;
                $response["code"]    = 200;
            }
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function eliminar($id)
    {
        if ($_POST && $this->session->userdata("usuario")) {
            $post = BlogPost::find($id);
            if ($post) {
                $post->tags()->detach();
            }
            $b = $this->post->delete($id);
            $this->session->set_userdata('mensaje', 'Entrada eliminada correctamente');
            $this->session->set_userdata('tipo_mensaje', 'success');
            redirect('blog/post');
        } else {
            $data['id_post']    = $id;
            $data['post']       = $this->post->get($id);
            $data['post']->tags = $this->post->getTags($data['post']->id_blog_post);
            $this->load->view("blog/posts/eliminar", $data);
        }
    }

    public function guardar()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $id   = $this->input->post("id", true);
            $tags = $this->input->post("tags");
            $b    = false;

            $post = [
                'titulo'              => $this->input->post("titulo"),
                'contenido'           => $this->input->post("contenido"),
                'id_autor'            => $this->session->userdata("usuario"),
                'id_imagen_destacada' => $this->input->post("imagen_destacada", true, "Integer"),
            ];

            if ($id && (bool)$id) {
                $b = $this->post->updatePOST($id, $post);
            } else {
                //$b = $this->post->insertPOST($post);
                $b = false;
                if ($id == "" || $id == null || $id == 0) {
                    $b = $this->post->insertPOST($post);
                } else {
                    $id = $this->input->post("id", true, 'Integer', ['min' => 0]);
                    if ($this->input->isValid()) {
                        $b = $this->post->updatePOST($id, $post);
                    }
                }

                if ($tags) {
                    $post = BlogPost::find($b);
                    $post->tags()->sync($tags);
                }

                return $this->output->set_status_header(200)
                    ->set_output(json_encode(['success' => true, 'data' => $b]));
            }

            return $this->output->set_status_header(404)
                ->set_output(json_encode(['success' => false, 'data' => 'error']));
        }
    }


}
