<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Post
 *
 * @author Evolutel
 */
class Resource extends CI_Controller {

    private $URL_IMAGENES = "";

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model('Blog/Categoria_model', 'categoria');
        $this->load->model('Blog/Post_model', 'post');
        $this->load->model('Blog/Imagen_model', 'imagen');
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/blog/resource/imagen/";
    }

    public function imagenes($page = 1) {
        $this->output->set_content_type('application/json');
        $total_paginas = ($this->imagen->count() / 50) + 1;
        if ($page <= $total_paginas) {
            $limit1 = ($page - 1) * 50;
            $limit2 = $page * 50;
            $imagenes = $this->imagen->getAll("id_blog_imagen", $limit1, $limit2);
            foreach ($imagenes as $key => &$value) {
                $value->url = $this->URL_IMAGENES . $value->id_blog_imagen;
            }
            return $this->output
                            ->set_status_header(200)
                            ->set_output(json_encode(array(
                                'success' => true,
                                'data' => $imagenes,
                                "paginas" => round($total_paginas),
                                    ))
            );
        }
        return $this->output
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
                                ))
        );
    }

    public function imagen($id) {
        if ($id == 0) {
            redirect(base_url() . "/dist/img/blog/default.png");
            return;
        }
        $imagen = $this->imagen->get($id);
        if ($imagen) {
            return $this->output
                            ->set_content_type($imagen->mime)
                            ->set_status_header(200)
                            ->set_output($imagen->imagen);
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }

    public function subir($type) {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            switch (strtoupper($type)) {
                case "IMAGEN":
                    $imagen = $this->input->post("archivo");
                    $mime = explode(",", $imagen);
                    $imagen = base64_decode($mime[1]);
                    $mime = explode(";", $mime[0]);
                    $mime = explode(":", $mime[0]);
                    $mime = $mime[1];
                    $datos = array("imagen" => $imagen, "mime" => $mime);
                    $b = $this->imagen->insert($datos);
                    if ($b) {
                        return $this->output
                                        ->set_status_header(200)
                                        ->set_output(json_encode(array(
                                            'success' => true,
                                            'data' => $this->config->base_url() . "blog/resource/imagen/" . $this->imagen->last_id(),
                                            'id' => $this->imagen->last_id()
                        )));
                    }
                default:
                    break;
            }
        }
        return $this->output
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }

}
