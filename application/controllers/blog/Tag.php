<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Models\Tag as BlogTag;

class Tag extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function store()
    {
        $response["code"] = 500;
        $response["data"] = [];
        $request          = $this->input->post();

        if (isset($request["tags"])) {
            foreach ($request["tags"] as $key => $tag) {
                $response["data"][] = BlogTag::updateOrCreate(["title" => $tag]);
            }

            $response["code"] = 200;
        }

        $response["data"] = json_encode(array_values(array_unique($response["data"])));

        return $this->output->set_content_type("application/json")
            ->set_status_header($response["code"])
            ->set_output($response["data"]);
    }

    public function show()
    {
        $response["code"] = 404;
        $search           = $this->input->get("term")["term"];

        if ($search) {
            $response["data"] = BlogTag::where("title", "like", "%$search%")->get();
            $response["code"] = 200;
        }

        return $this->output->set_content_type("application/json")
            ->set_status_header($response["code"])
            ->set_output(json_encode($response["data"]));
    }


}
