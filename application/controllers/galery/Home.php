<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Vestido/Vestido_model", "vestido");
        $this->load->model("Vestido/Tipo_model", "tipo");
        $this->load->model("Galeria/Album", "album");        
        $this->load->helper("formats");
    }

    public function index($page = 1) {
        
        $data["page"] = $page;
        
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"] = $this->tipo->existsProducto($tipo ? $tipo : "Vestidos de Novia", $data["tipos_productos"]);
        if ($data["producto"]) {
            $data["title"] = $data["producto"]->nombre;
//            $data["albums"] = $this->album->getAlbum($data["fotos"]->id_fotos);
            $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
            $data["temporadas"] = $this->vestido->getTemporadas($data["producto"]->id_tipo_vestido);
            $data["catalogo"] = $this->vestido->getCatalogo($data["producto"]->id_tipo_vestido);
            $data["destacados"] = $this->vestido->getDisenadoresDestacados($data["producto"]->id_tipo_vestido);
            $data["param"] = array();
            if ($data["catalogo"]) {
                foreach ($data["catalogo"] as $key => &$value) {
                    $value->like = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                    $value->likes = $this->vestido->countLikes($value->id_vestido);
                    $value->comments = $this->vestido->countComentarios($value->id_vestido);
                }
            }
                   $this->load->view('galery/index', $data);
            return;
        }
        show_404();
    }
    
    
    	 public function imagen($id_photo) {
        $foto = $this->foto->getAlbum($id_vestido);
        if ($foto) {
            $this->output->cache(60 * 24);
            if ($foto->imagen) {
                return $this->output->set_content_type("image/jpg")
                                ->set_status_header(200)
                                ->set_output($foto->imagen);
            }
			return redirect($this->config->base_url() . "dist/img/blog/perfil.png");
        }
		return redirect($this->config->base_url() . "dist/img/blog/perfil.png");
    }
    
     
}
