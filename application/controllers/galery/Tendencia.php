<?php

class Tendencia extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("Checker");
        $this->load->model("Vestido/Tipo_model", "tipo");
        $this->load->model("Vestido/Vestido_model", "vestido");
        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Vestido/VestidoUsuario_model", "like");
        $this->load->model("Vestido/VestidoComentario_model", "comentario");
        $this->load->helper("formats");
    }

    public function index($tipo = NULL) {
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"] = $this->tipo->existsProducto($tipo ? $tipo : "Vestidos de Novia", $data["tipos_productos"]);
        if ($data["producto"]) {
            $data["title"] = $data["producto"]->nombre;
            $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
            $data["temporadas"] = $this->vestido->getTemporadas($data["producto"]->id_tipo_vestido);
            $data["catalogo"] = $this->vestido->getCatalogo($data["producto"]->id_tipo_vestido);
            $data["destacados"] = $this->vestido->getDisenadoresDestacados($data["producto"]->id_tipo_vestido);
            $data["param"] = array();
            if ($data["catalogo"]) {
                foreach ($data["catalogo"] as $key => &$value) {
                    $value->like = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                    $value->likes = $this->vestido->countLikes($value->id_vestido);
                    $value->comments = $this->vestido->countComentarios($value->id_vestido);
                }
            }
            $this->load->view("principal/vestidos/index", $data);
            return;
        }
        show_404();
    }

	public function imagen_vestido($id_vestido) {
        $vestido = $this->vestido->getVestido($id_vestido);
        if ($vestido) {
            $this->output->cache(60 * 24);
            if ($vestido->imagen) {
                return $this->output->set_content_type("image/jpg")
                                ->set_status_header(200)
                                ->set_output($vestido->imagen);
            }
			return redirect($this->config->base_url() . "dist/img/blog/perfil.png");
        }
		return redirect($this->config->base_url() . "dist/img/blog/perfil.png");
    }
	
    public function disenadores($tipo = NULL) {
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"] = $this->tipo->existsProducto($tipo ? $tipo : "Vestidos de Novia", $data["tipos_productos"]);
        if ($data["producto"]) {
            $data["title"] = $data["producto"]->nombre;
            $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
            $data["temporadas"] = $this->vestido->getTemporadas($data["producto"]->id_tipo_vestido);
            $data["destacados"] = $this->vestido->getDisenadoresAll($data["producto"]->id_tipo_vestido);
            $this->load->view("principal/vestidos/disenadores", $data);
            return;
        }
        show_404();
    }

    private function getParamsSearch($sortable) {
        $param = array();
        foreach ($sortable as $key => $s) {
            $temp = $this->input->get($s->nombre, TRUE);
            if ($temp) {
                $param[$s->nombre] = $temp;
            }
        }
        $temp = $this->input->get("disenador", TRUE);
        if ($temp) {
            $param["disenador"] = $temp;
        }
        return $param;
    }

    public function catalogo($tipo = NULL) {
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"] = $this->tipo->existsProducto($tipo ? $tipo : "Vestidos de Novia", $data["tipos_productos"]);
        if ($data["producto"]) {
            $data["param"] = $this->getParamsSearch($data["producto"]->sortable);
            $data["title"] = $data["producto"]->nombre;
            $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
            $data["temporadas"] = $this->vestido->getTemporadas($data["producto"]->id_tipo_vestido);
            $page = $this->input->get("pag", true, "Integer", array("required" => true, "min" => 1));
            if (!$page) {
                $page = 1;
            }
            $data["page"] = $page;
            $data["total"] = $this->vestido->getTotalCatalogo($data["producto"]->id_tipo_vestido, $data["param"]);
            $data["catalogo"] = $this->vestido->getCatalogo($data["producto"]->id_tipo_vestido, $data["param"], $page);
            if ($data["catalogo"]) {
                foreach ($data["catalogo"] as $key => &$value) {
                    $value->like = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                    $value->likes = $this->vestido->countLikes($value->id_vestido);
                    $value->comments = $this->vestido->countComentarios($value->id_vestido);
                }
            }

            $data["destacados"] = $this->vestido->getDisenadoresDestacados($data["producto"]->id_tipo_vestido);
            $this->load->view("principal/vestidos/catalogo", $data);
            return;
        }
        show_404();
    }

    public function disenador($tipo, $nombre) {
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"] = $this->tipo->existsProducto($tipo, $data["tipos_productos"]);
        if ($data["producto"]) {
            if ($this->vestido->existsDisenador($nombre)) {
                $pag = $this->input->get("pag", true, "Integer", array("required" => false));
                if (!$pag) {
                    $pag = 1;
                }
                $data["pag"] = $pag;
                $data["disenador"] = urldecode(str_replace("-", " ", $nombre));
                $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
                $data["title"] = "Disenador: " . $data["disenador"];
                $data["vestidos"] = $this->vestido->getAllDisenador($data["disenador"], $pag);
                $data["param"] = array();
                if ($data["vestidos"]) {
                    foreach ($data["vestidos"] as $key => &$value) {
                        $value->like = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                        $value->likes = $this->vestido->countLikes($value->id_vestido);
                        $value->comments = $this->vestido->countComentarios($value->id_vestido);
                    }
                }
                $data["total"] = $this->vestido->getTotalAllDisenador($data["disenador"]);
                $data["temporadas"] = $this->vestido->getTemporadasDisenador($data["disenador"]);
                return $this->load->view("principal/vestidos/disenador", $data);
            }
        }
        show_404();
    }

    public function filtrar($tipo, $nombre) {
        $data["tipos_productos"] = $this->tipo->getAll();
        $data["producto"] = $this->tipo->existsProducto($tipo, $data["tipos_productos"]);
        if ($data["producto"]) {
            if ($this->vestido->existsDisenador($nombre)) {
                $pag = $this->input->get("pag", true, "Integer", array("required" => false));
                if (!$pag) {
                    $pag = 1;
                }
                $data["pag"] = $pag;
                $data["disenador"] = urldecode(str_replace("-", " ", $nombre));
                $data["disenadores"] = $this->vestido->getDisenadores($data["producto"]->id_tipo_vestido);
                $data["title"] = "Disenador: " . $data["disenador"];
                $data["vestidos"] = $this->vestido->getAllDisenador($data["disenador"], $pag);
                if ($data["vestidos"]) {
                    foreach ($data["vestidos"] as $key => &$value) {
                        $value->like = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                    }
                }
                $data["total"] = $this->vestido->getTotalAllDisenador($data["disenador"]);
                $data["temporadas"] = $this->vestido->getTemporadasDisenador($data["disenador"]);
                return $this->load->view("principal/vestidos/disenador", $data);
            }
        }
        show_404();
    }

    public function articulo($disenador, $modelo) {
        $data["disenador"] = $disenador;
        $data["modelo"] = $modelo;
        $data["vestido"] = $this->vestido->get(array(
            "nombre" => urldecode(str_replace("-", " ", $modelo)),
            "disenador" => urldecode(str_replace("-", " ", $disenador))
        ));
        if ($data["vestido"]) {
            $data["producto"] = $this->tipo->get($data["vestido"]->tipo_vestido);
            if ($data["producto"]) {
                $data["title"] = $data["producto"]->nombre . ": " . $data["vestido"]->nombre;
                $data["vestido"]->like = $this->vestido->isLikeForMe($this->session->id_cliente, $data["vestido"]->id_vestido);
                $data["total"] = $this->vestido->countDisenador($data["vestido"]->disenador);
                $data["vestidos"] = $this->vestido->getListOrderBy(array(
                    "tipo_vestido" => $data["vestido"]->tipo_vestido,
                    "disenador" => urldecode(str_replace("-", " ", $disenador))
                        ), " nombre ");
                $data["posicion"] = 0;
                foreach ($data["vestidos"] as $key => $value) {
                    $data["posicion"] ++;
                    if ($value->id_vestido == $data["vestido"]->id_vestido) {
                        break;
                    }
                }
                $data["comentarios"] = $this->comentario->getComentariosArticulosInit($data["vestido"]->id_vestido);
                foreach ($data["comentarios"] as $key => &$value) {
                    $value->usuario = $this->usuario->get($value->id_usuario);
                }
                return $this->load->view("principal/vestidos/articulo", $data);
            }
        }
        show_404();
    }

    public function like() {
        $this->output->set_content_type("application/json");
        $id_cliente = $this->session->userdata("id_cliente");
        if ($_POST && $id_cliente) {
            $vestido = $this->input->post("vestido", true, "Integer", array("required" => true, "min" => 1));
            $v = $this->vestido->get($vestido);
            if ($v && $this->input->isValid()) {
                $like = $this->like->get(array(
                    "id_vestido" => $v->id_vestido,
                    "id_cliente" => $id_cliente
                ));
                if (!$like) {
                    $this->like->insert(array(
                        "id_vestido" => $v->id_vestido,
                        "id_cliente" => $id_cliente
                    ));
                } else {
                    $this->like->delete($like->id_vestido_usuario);
                }
                return $this->output->set_status_header(202)
                                ->set_output(json_encode(array("success" => true, "data" => "ok")));
            }
        }
        return $this->output->set_status_header(404);
    }

    public function comentar() {
        $this->output->set_content_type("application/json");
        $id_cliente = $this->session->userdata("id_cliente");
        if ($_POST && $id_cliente) {
            $c = array(
                "id_usuario" => $this->session->userdata("id_usuario"),
                "id_vestido" => $this->input->post("vestido", true, "Integer", array("required" => true, "min" => 1)),
                "parent" => $this->input->post("id_comment", true, "Integer", array("required" => false, "min" => 1)),
                "mensaje" => $this->input->post("comment", true, "String", array("required" => true)),
            );
            $v = $this->vestido->get($c["id_vestido"]);
            if ($v && $this->input->isValid()) {
                $b = $this->comentario->insert($c);
                if ($b) {
                    return $this->output->set_status_header(202)
                                    ->set_output(json_encode(array("success" => true, "data" => "ok")));
                }
            }
        }
        return $this->output->set_status_header(404);
    }

    public function comentarios() {
        $this->output->set_content_type("application/json");
        if ($_POST) {
            $parent = $this->input->post("comment", true, "Integer", array("required" => true, "min" => 1));
            if ($this->input->isValid()) {
                $comentarios = $this->comentario->getList(array("parent" => $parent));
                foreach ($comentarios as $key => &$value) {
                    $value->usuario = $this->comentario->getUsuario($value->id_usuario);
                }
                return $this->output->set_status_header(202)
                                ->set_output(json_encode(array("success" => true, "data" => $comentarios)));
            }
        }
        return $this->output->set_status_header(404);
    }

}
