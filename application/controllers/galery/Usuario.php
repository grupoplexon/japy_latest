<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Evolutel
 */
class Usuario extends CI_Controller {

    public $input;

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Blog/Comentario_model", "comentario");
        $this->load->model("Blog/Usuario_model", "usuario");
        $this->load->helper('formats');
        $this->input = new MY_Input();
    }

    public function index() {
        $data["usuarios"] = $this->usuario->getAll();
        $this->load->view("blog/usuarios/index", $data);
    }

    public function nuevo() {
        if ($_POST) {
            $this->session->set_userdata("tipo_mensaje", "danger");
            $pass = $this->input->post('password', true);
            $repass = $this->input->post("re-password", true);
            if ($pass == $repass) {
                $usuario = array(
                    'usuario' => $this->input->post('correo', true, "Email", array("required" => true)),
                    'correo' => $this->input->post("correo", true, "Email", array("required" => true)),
                    'nombre' => $this->input->post("nombre", true, "String", array("requires" => true, "min" => 2, "max" => 32)),
                    'apellido' => $this->input->post("apellido", true, "String", array("requires" => true, "min" => 2, "max" => 40)),
                    'activo' => 1,
                    'contrasena' => sha1(md5(sha1($pass))),
                );
                if ($this->input->isValid()) {
                    if ($this->usuario->insert($usuario)) {
                        $this->session->set_userdata("mensaje", "Datos registrados correctamente");
                        $this->session->set_userdata("tipo_mensaje", "success");
                        redirect("blog/usuario");
                    } else {
                        $this->session->set_userdata("mensaje", "Ocurrio un error al registrar los datos intente mas tarde, si el problema persiste contacte con el administrador");
                    }
                } else {
                    $this->session->set_userdata("mensaje", $this->input->getErrors());
                }
            } else {
                $this->session->set_userdata("mensaje", "Las contraseñas no coinciden");
            }
        }
        $this->load->view("blog/usuarios/nuevo");
    }

    public function activar($id_usuario) {
        $u = $this->usuario->get(array("id_usuario" => $id_usuario, "rol" => Checker::$REDACTOR));
        if ($u) {
            if ($_POST) {
                $this->session->set_userdata("mensaje", "Ocurrio un error intente más tarde");
                $this->session->set_userdata("tipo_mensaje", "danger");
                if ($this->usuario->update($id_usuario, array("activo" => $u->activo == 1 ? 0 : 1))) {
                    if ($u->activo == 1) {
                        $this->session->set_userdata("mensaje", "El usuario $u->nombre $u->apellido ya no se encuentra activo");
                    } else {
                        $this->session->set_userdata("mensaje", "El usuario $u->nombre $u->apellido se activo correctamente");
                    }
                    $this->session->set_userdata("tipo_mensaje", "success");
                    redirect("blog/usuario");
                }
            }
            $data["usuario"] = $u;
            $this->load->view("blog/usuarios/activar", $data);
        }
        return "show404";
    }

}
