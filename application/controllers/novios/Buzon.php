<?php

use Models\Attachment;

class Buzon extends MY_Controller
{
    protected function middleware()
    {
        return ['user_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->library("Checker");
        $this->load->helper("formats");
        $this->load->helper("collection_paginate");
        $this->load->helper("extension_helper");
        $this->load->model("Correo/Correo_model", "correo");
        $this->load->model("Correo/Adjunto_model", "adjunto");
        $this->load->model("Correo/AmistadUsuarios_model", "amistad");
        $this->load->model("Boda_model", "boda");
        $this->load->model("Cliente_model", "cliente");
        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/Respuesta_model", "respuesta");
    }

    public function index()
    {
        $userId      = $this->session->userdata("id_usuario");
        $currentPage = $this->input->get("pagina") == null ? 1 : $this->input->get("pagina");

        $mails = Mail::with([
            "replies" => function ($query) {
                return $query->visible("user", true);
            },
            "userTo",
        ])->from($userId)->main()->visible("user", true)->get();

        //Just for ordering
        foreach ($mails as $mail) {
            if ($mail->latestReply) {
                $mail->fecha_creacion = $mail->latestReply->fecha_creacion;
            }
        }

        $mails = $mails->sortByDesc("fecha_creacion")->values();

        $data["mails"] = collection_paginate($mails, 10, $currentPage);

        $this->load->view("principal/novia/buzon/index", $data);
    }

    private function getAmigos()
    {
        $amigos = $this->amistad->getList(["id_usuario_envio" => $this->session->id_usuario, "amistad" => 1]);
        if ($amigos) {
            foreach ($amigos as &$value) {
                $value->usuario = $this->usuario->get($value->id_usuario_confirmacion);
            }
        }

        return $amigos;
    }

    public function enviados()
    {
        $page                = $this->input->get("pagina", true, "Integer", ["min" => 1]);
        $data["page"]        = $page ? $page : 1;
        $data["solicitudes"] = $this->correo->getEnviadosCliente($this->session->userdata("id_usuario"), $data["page"]);
        $data["total"]       = $this->correo->getCountEnviadosCliente($this->session->userdata("id_usuario"));
        $data["boda"]        = $this->boda->get($this->session->id_boda);
        $data["menu"]        = "enviados";
        $data["amigos"]      = $this->getAmigos();
        foreach ($data["solicitudes"] as &$correos) {
            $id          = $correos->to;
            $correos->to = $this->proveedor->getProveedor($id);
            if ( ! $correos->to) {
                $correos->to = $this->usuario->get($id);
            }
        }
        $this->load->view("principal/novia/buzon/enviados", $data);
    }

    public function nuevo()
    {
        $usuario = $this->input->get("usuario", true, "Integer", ["min" => 1]);
        if ($_POST) {
            $this->session->set_userdata("tipo_mensaje", "danger");
            $this->session->set_userdata("mensaje", "Ocurrio un error al enviar el mensaje");
            $msj = [
                "to"      => $this->input->post("to", true, "String", ["required" => true]),
                "from"    => $this->session->userdata("id_usuario"),
                "asunto"  => $this->input->post("asunto", true, "String",
                    ["required" => true, "minSize" => 2, "maxSize" => 160]),
                "mensaje" => $this->input->post("mensaje", false, "String", ["required" => true]),
            ];
            if ($this->input->isValid()) {
                if ($this->correo->insert($msj)) {
                    $this->session->set_userdata("tipo_mensaje", "success");
                    $this->session->set_userdata("mensaje", "Mensaje enviado correctamente ");
                    $this->correo->mendallaExtrovertida($this->session->userdata("id_usuario"));

                    return redirect("novios/buzon");
                }
            } else {
                $this->session->set_userdata("mensaje", $this->input->getErrors());
            }
            $usuario = $this->input->post("to", true, "String", ["required" => true]);
        }
        $user = $this->usuario->get($usuario);
        if ($user) {
            $data["amigos"] = $this->getAmigos();
            $data["menu"]   = "buzon";
            $data["to"]     = $user;
            $data["from"]   = $this->usuario->get($this->session->userdata("id_usuario"));

            return $this->load->view("principal/novia/buzon/nuevo", $data);
        }
        show_404();
    }

    public function ver($to, $id)
    {
        $mail = Mail::with([
            "replies" => function ($query) {
                return $query->with("attachments")->visible("user", 1);
            },
            "userTo",
        ])->find($id);

        if (is_null($mail)) {
            show_404();
        }

        $user = User::find($this->session->userdata("id_usuario"));

        if ($user->id_usuario != $mail->from) {
            return $this->load->view("novios/buzon");
        }

        //Update seen on the emails that are for me
        $mail->replies()->where("to", $this->session->id_usuario)->update(["leida_usuario" => 1]);

        if ($_POST) {
            $respuesta = [
                "asunto"  =>
                    $this->input->post("asunto", true, "String",
                        ["required" => true, "minSize" => 1, "maxSize" => 255]),
                "mensaje" => $this->input->post("mensaje", false, "String", ["required" => true]),
                "from"    => $this->session->id_usuario,
                "to"      => $to,
                "parent"  => $id,
            ];

            if ($this->input->isValid()) {
                $provider   = User::find($mail->to)->provider;
                $indicators = $provider->load([
                    "indicators" => function ($query) use ($mail) {
                        return $query->whereNull("handled_at")
                            ->whereIn("id", $mail->replies->pluck("indicator_id"));
                    },
                ])->indicators;

                if ($mail->replies()->count() == 0) {
                    $respuesta["indicator_id"] = $mail->indicator_id;
                } elseif ( ! $indicators->count()) {
                    $provider->indicators()->attach($this->session->userdata("id_usuario"),
                        ["type" => "contact_request"]);
                    $indicator                 = $provider->indicators()->latest()->first()->pivot;
                    $respuesta["indicator_id"] = $indicator->id;
                } else {
                    $respuesta['indicator_id'] = $indicators->last()->pivot->id;
                }

                $response = Mail::create($respuesta);

                if ($response) {
                    $this->correo->sendEmail();

                    if (count($this->input->post("files")) && count($this->input->post("files_names"))) {
                        $replyFile = [];
                        $replyFilesNames = $this->input->post("files_names");

                        $replyFiles = array_map(function ($item) {
                            $newItem["data"] = $item;

                            $pos             = strpos($item, ';');
                            $newItem["mime"] = explode(':', substr($item, 0, $pos))[1];

                            return $newItem;
                        }, $this->input->post("files"));

                        //Attach the files to the reply
                        foreach ($replyFiles as $key => $replyFile) {
                            $response->attachments()->create(["data" => $replyFile["data"], "nombre" => $replyFilesNames[$key], "mime" => $replyFile["mime"]]);
                        }
                    }

                    $response->update(["leida_usuario" => 1, "leida_proveedor" => 0]);

                    return redirect("novios/buzon/ver/$to/$id");
                }
            }
        }

        $data["novio"]    = $this->usuario->get($mail->from);
        $data["cliente"]  = $this->cliente->get(["id_usuario" => $this->session->id_usuario]);
        $data["boda"]     = $this->boda->get($data["cliente"]->id_boda);
        $data["titulo"]   = "Solicitud: ".strip_tags(substr($mail->mensaje, 0, 15))."...";
        $data["mail"]     = $mail;
        $data["provider"] = User::with([
            "provider" => function ($query) {
                return $query->with("imageLogo", "imagePrincipal");
            },
        ])->find($to)->provider;

        return $this->load->view("principal/novia/buzon/ver", $data);
    }

    public function eliminar()
    {
        $this->output->set_content_type("application / json");
        $userId   = $this->session->id_usuario;
        $response = [
            'success' => false,
            'data'    => 'error',
            'code'    => 404,
        ];

        if ($_POST) {
            $solicitudes = $this->input->post('solicitudes');

            //If it's an array we get the mail and the replies, else we get the mail
            $update = Mail::fromOrTo($userId)
                ->visible('user', true)
                ->when(is_array($solicitudes), function ($query) use ($solicitudes) {
                    return $query->whereIn('id_correo', $solicitudes)
                        ->orWhereIn('parent', $solicitudes);
                }, function ($query) use ($solicitudes) {
                    return $query->where('id_correo', $solicitudes);
                })->update(['visible_usuario' => 0]);

            if ($update) {
                $response['success'] = true;
                $response['data']    = $update;
                $response['code']    = 200;
            }
        }

        return $this->output->set_status_header($response['code'])->set_output(json_encode($response));
    }
}
