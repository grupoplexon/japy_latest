<?php

class Mesa extends MY_Controller
{
    protected function middleware()
    {
        return ['user_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Boda_model");
        $this->load->model('Invitados/Grupo_model', 'Grupo_model');
        $this->load->model('Invitados/Menu_model', 'Menu_model');
//        $this->load->model('Invitados/Invitado_model', 'Invitado_model');
        $this->load->model('Mesas/Mesa_model', 'mesa');
        $this->load->model('Mesas/Invitado_model', 'invitado');
        $this->load->library('encrypt');
    }

    public function index()
    {
        $return['titulo']    = "Organizador de mesas";
        $return["grupos"]    = $this->Grupo_model->getGrupos();
        $return["invitados"] = $this->invitado->getInvitadosGrupoOrder('nombre');
        $return['menus']     = $this->Menu_model->getMenus();
        $return["mesas"]     = $this->mesa->getList(["id_boda" => $this->session->userdata("id_boda")]);
        $this->load->view("principal/novia/mesas/index", $return);
    }

    public function table()
    {
        $return['titulo'] = "Mi lista de invitados";
        $return["mesas"]  = $this->mesa->getList(["id_boda" => $this->session->userdata("id_boda")]);
        foreach ($return["mesas"] as &$mesa) {
            $mesa->invitados = $this->invitado->getList([
                    "id_mesa" => $mesa->id_mesa,
                    "id_boda" => $this->session->userdata("id_boda"),
            ]);
        }
        $this->load->view("principal/novia/mesas/table", $return);
    }

    public function addMesa()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            //id, tipo, sillas, nombre, x, y, orientation
            $mesa = [
                    "tipo"        => $this->input->post("tipo", true, "Integer",
                            ["required" => true, "min" => 1, "max" => 3]),
                    "sillas"      => $this->input->post("sillas", true, "Integer",
                            ["required" => true, "min" => 1, "max" => 15]),
                    "nombre"      => $this->input->post("nombre", true, "String",
                            ["required" => true, "maxSize" => 60]),
                    "x"           => $this->input->post("x", true, "Decimal", ["required" => true]),
                    "y"           => $this->input->post("y", true, "Decimal", ["required" => true]),
                    "orientacion" => $this->input->post("orientation", true, "Integer",
                            ["required" => true, "min" => 0, "max" => 360]),
                    "id_boda"     => $this->session->userdata("id_boda"),
            ];

            if ($this->input->isValid()) {
                $b = $this->mesa->insert($mesa);
                if ($b) {
                    $id = $this->mesa->last_id();

                    return $this->output->set_status_header(202)
                            ->set_output(json_encode([
                                    'success' => true,
                                    'data'    => $this->mesa->get($id),
                            ]));
                }
            } else {
                return $this->output->set_status_header(202)
                        ->set_output(json_encode([
                                'success' => false,
                                'data'    => $this->input->getErrors(),
                        ]));
            }
        }

        return $this->output->set_status_header(404);
    }

    public function updateMesa()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $id_mesa = $this->input->post("mesa", true, "Integer", ["required" => true, "min" => 1]);
            $mesa    = [
                    "sillas"      => $this->input->post("sillas", true, "Integer",
                            ["required" => true, "min" => 1, "max" => 15]),
                    "nombre"      => $this->input->post("nombre", true, "String",
                            ["required" => true, "maxSize" => 60]),
                    "x"           => $this->input->post("x", true, "Decimal", ["required" => true]),
                    "y"           => $this->input->post("y", true, "Decimal", ["required" => true]),
                    "orientacion" => $this->input->post("orientation", true, "Integer",
                            ["required" => true, "min" => 0, "max" => 360]),
            ];
            if ($this->input->isValid()) {
                if ($this->mesa->get(["id_mesa" => $id_mesa, "id_boda" => $this->session->userdata("id_boda")])) {
                    $b = $this->mesa->update($id_mesa, $mesa);
                    if ($b) {
                        $this->mesa->removeInvitadosOverFlow($id_mesa, $mesa["sillas"]);

                        return $this->output->set_status_header(200)
                                ->set_output(json_encode([
                                                'success' => true,
                                                'data'    => "ok",
                                        ])
                                );
                    }
                }
            } else {
                return $this->output->set_status_header(202)
                        ->set_output(json_encode([
                                        'success' => false,
                                        'data'    => $this->input->getErrors(),
                                ])
                        );
            }
        }

        return $this->output->set_status_header(404);
    }

    public function deleteMesa()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $id_mesa = $this->input->post("mesa", true, "Integer", ["required" => true, "min" => 1]);
            if ($this->input->isValid()) {
                if ($this->mesa->get(["id_mesa" => $id_mesa, "id_boda" => $this->session->userdata("id_boda")])) {
                    $b = $this->mesa->delete($id_mesa);
                    if ($b) {
                        return $this->output->set_status_header(202)
                                ->set_output(json_encode([
                                                'success' => true,
                                                'data'    => "ok",
                                        ])
                                );
                    }
                }
            }
        }

        return $this->output->set_status_header(404);
    }

    public function deleteInvitado()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $id = $this->input->post("invitado");
            $id_invitado = decrypt($this->input->post("invitado", true, "String", ["required" => true, "min" => 1]));
            if ($this->input->isValid()) {
                if ($this->invitado->get([
                        "id_invitado" => $id_invitado,
                        "id_boda"     => $this->session->userdata("id_boda"),
                ])
                ) {
                    $b = $this->invitado->update($id_invitado, ["id_mesa" => null, "silla" => null]);
                    if ($b) {
                        return $this->output->set_status_header(202)
                                ->set_output(json_encode([
                                                'success' => true,
                                                'data'    => "ok",
                                                'id'    => $id,
                                        ])
                                );
                    }
                }
            }
        }

        return $this->output->set_status_header(404);
    }

    public function setInvitado()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $id_mesa     = $this->input->post('mesa', true, 'Integer', ['required' => true, 'min' => 1]);
            $id = $this->input->post('invitado');
            $id_invitado = decrypt($this->input->post('invitado', true, 'String', ['required' => true, 'min' => 1]));
            $silla       = $this->input->post('silla', true, 'Integer', ['required' => true, 'min' => 1]);
            if ($this->input->isValid()) {
                if ($this->mesa->get(["id_mesa" => $id_mesa, "id_boda" => $this->session->userdata("id_boda")])) {
                    if ($this->invitado->get($id_invitado)) {
                        $b = $this->invitado->update($id_invitado, [
                                "id_mesa" => $id_mesa,
                                "silla"   => $silla,
                        ]);
                        if ($b) {
                            return $this->output->set_status_header(202)
                                    ->set_output(json_encode([
                                                    'success' => true,
                                                    'data'    => "ok",
                                                    'id'    => $id,
                                            ])
                                    );
                        }
                    } else {
                        echo "no hay invitado";
                    }
                } else {
                    echo "no hay mesa";
                }
            } else {
                echo "no fue valido";
            }
        }

        return $this->output->set_status_header(404);
    }

}
