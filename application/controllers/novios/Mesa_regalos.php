<?php

use Illuminate\Database\Capsule\Manager as DB;

class Mesa_regalos extends MY_controller
{
    protected function middleware()
    {
        return ['user_auth'];
    }
    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->library('encrypt');
        $this->load->model("Perfil_model", "perfil");
        $this->load->model("Boda_model", "boda");
        $this->load->model("Invitados/Mesa_model", "mesa");
        $this->load->model("Proveedor/ProveedorBoda_model", "ProveedorBoda_model");
        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Invitados/Invitado_model", "invitado");
        $this->load->model("Cliente_model", "cliente");
        $this->load->helper("storage");
    }
    public function index()
    {
        $id_boda = $this->session->userdata("id_boda");
        $data["boda"] = $this->boda->get($id_boda);
        $data["mesa"] = DB::table("mesa_regalos")->where("id_boda", $id_boda)->first();
        $grupo = $this->invitado->getGrupoNovios($id_boda);
        if ($grupo) {
            $data["novios"] = $this->invitado->getList(["id_boda" => $id_boda, "id_grupo" => $grupo->id_grupo]);
        }

        if($data["mesa"]!=null){

            $data["regalos"] = DB::table("regalos")->where("id_mesa", $data["mesa"]->id_mesa)->orderBy( 'precio','asc')->get();
            
            $this->load->view("principal/novia/mesa_regalos/index", $data);

        }else{

            DB::table("mesa_regalos")->insert([
                'id_boda' =>$id_boda
            ]);
            $data["mesa"] =DB::table("mesa_regalos")->where("id_boda", $id_boda)->first();

            $data["regalos"] = null;

            $this->load->view("principal/novia/mesa_regalos/index", $data);
        }
    }

    public function updateRegalo()
    {

        if($this->input->post("tipo")=="update"){

            $id_boda = $this->session->userdata("id_boda");
            $id_mesa = DB::table("mesa_regalos")->select('id_mesa')->where("id_boda", $id_boda)->first();

            $save = DB::table('regalos')->where("id_regalo", $this->input->post("id"))->update([
                'nombre_producto' => $this->input->post("producto"),
                'precio' => $this->input->post("precio"),
                'descripcion' => $this->input->post("descripcion"),
                'categoria' => $this->input->post("categoria")
            ]);

        }
        return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['success' => 'ok']));
    }

    public function saveRegalo()
    {
        if ($_FILES) {

            // dd($_FILES);
            $id_boda = $this->session->userdata("id_boda");
            $id_mesa = DB::table("mesa_regalos")->select('id_mesa')->where("id_boda", $id_boda)->first();
            $destination = 'uploads/regalos';
            $quality     = 20;
            $pngQuality  = 9;
            $file = $_FILES["files"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["files"]["name"])["extension"];
            $fileName  = store_file($data, $extension, "/regalos");
            $new_name_image = $file = $destination."/".$fileName;

            $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
            $status = "ok";

            if($this->input->post("tipo")=="save"){

                $save = DB::table('regalos')->insert([
                    'nombre_producto' => $this->input->post("producto"),
                    'precio' => $this->input->post("precio"),
                    'descripcion' => $this->input->post("descripcion"),
                    'categoria' => $this->input->post("categoria"),
                    'imagen' => $fileName, 
                    'id_mesa' => $id_mesa->id_mesa
                ]);
            }else if($this->input->post("tipo")=="update"){

                $save = DB::table('regalos')->where("id_regalo", $this->input->post("id"))->update([
                    'nombre_producto' => $this->input->post("producto"),
                    'precio' => $this->input->post("precio"),
                    'descripcion' => $this->input->post("descripcion"),
                    'categoria' => $this->input->post("categoria"),
                    'imagen' => $fileName, 
                    'id_mesa' => $id_mesa->id_mesa
                ]);

            }
            
            return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(['state' => $status]));
        }
    }
    public function autocomplete()
    {
        $term = $this->input->get("term");
        $id_boda = $this->session->userdata("id_boda");
        $id_mesa = DB::table("mesa_regalos")->select('id_mesa')->where("id_boda", $id_boda)->first();
        $categories = DB::table('regalos')->select('nombre_producto')->where('nombre_producto', 'like', '%'.$term.'%')->where('id_mesa', $id_mesa->id_mesa)->get();
        $aux = [];
        foreach ($categories as $key => $v) {
            $aux[$key] = $v->nombre_producto;
        }
        $busqueda = $aux;
        unset($aux);

        return $this->output->set_status_header(200)
            ->set_output(json_encode($busqueda));
    }
    public function regalos()
    {
        $term = $this->input->get("nombre");
        $id_boda = $this->session->userdata("id_boda");
        $data["boda"] = $this->boda->get($id_boda);
        $data["usuario"] = $this->session->userdata("id_usuario");
        $data["cliente"] = $this->session->userdata("id_cliente");    
        $data["mesa"] = DB::table("mesa_regalos")->where("id_boda", $id_boda)->first();
        $data["regalos"] = DB::table("regalos")->where("id_mesa", $data["mesa"]->id_mesa)->where("nombre_producto", $term)->orderBy( 'precio','asc')->get();

        $grupo = $this->invitado->getGrupoNovios($id_boda);
        if ($grupo) {
            $data["novios"] = $this->invitado->getList(["id_boda" => $id_boda, "id_grupo" => $grupo->id_grupo]);
        }
        $this->load->view("principal/novia/mesa_regalos/index", $data);
    }
    public function regalo()
    {   
        $id_boda = $this->session->userdata("id_boda");
        $data["boda"] = $this->boda->get($id_boda);
        $data["usuario"] = $this->session->userdata("id_usuario");
        $data["cliente"] = $this->session->userdata("id_cliente");    
        $data["mesa"] = DB::table("mesa_regalos")->where("id_boda", $id_boda)->first();

        $grupo = $this->invitado->getGrupoNovios($id_boda);
        if ($grupo) {
            $data["novios"] = $this->invitado->getList(["id_boda" => $id_boda, "id_grupo" => $grupo->id_grupo]);
        }

        $categoria = json_decode($this->input->get("categoria"));
        $precio = $this->input->get("precio");
        $array = explode(",", $precio);
        $orden = $this->input->get("orden");

        // dd($array);

        $data["regalos"] = DB::table("regalos")->where("id_mesa", $data["mesa"]->id_mesa)
            ->when($categoria, function ($query) use ($categoria) {
                return $query =  $query->whereIn('categoria', $categoria);

            })
            ->when($array, function($query) use ($array){
               return  $query->whereBetween('precio', [$array[0], $array[1]]);
            })
            ->orderBy( 'precio', $orden)->get();

        // dd($data["regalos"]);
        
        $this->load->view("principal/novia/mesa_regalos/index", $data);
    }
    public function imagenPortada()
    {
        if ($_FILES) {

            $id_boda = $this->session->userdata("id_boda");
            $destination = 'uploads/regalos';
            $quality     = 20;
            $pngQuality  = 9;
            $file = $_FILES["files"];
            $data     = file_get_contents($file["tmp_name"]);
            $extension = pathinfo($_FILES["files"]["name"])["extension"];
            $fileName  = store_file($data, $extension, "/regalos");
            $new_name_image = $file = $destination."/".$fileName;

            $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
            $status = "ok";

            $save = DB::table('mesa_regalos')->where('id_boda', $id_boda)
            ->update(['imagen_fondo' => $fileName]);
        }
    }
    public function deleteRegalo()
    {
        $id = $this->input->post("id_regalo");

        DB::table("regalos")->where('id_regalo', $id)->delete();
    }
    public function editRegalo()
    {
        $id_boda = $this->session->userdata("id_boda");
        $data["boda"] = $this->boda->get($id_boda);
        $data["mesa"] = DB::table("mesa_regalos")->where("id_boda", $id_boda)->first();
        $grupo = $this->invitado->getGrupoNovios($id_boda);
        if ($grupo) {
            $data["novios"] = $this->invitado->getList(["id_boda" => $id_boda, "id_grupo" => $grupo->id_grupo]);
        }
        $data["regalos"] = DB::table("regalos")->where("id_mesa", $data["mesa"]->id_mesa)->orderBy( 'precio','asc')->get();
        $data["update"] = 1;

        $id = $this->input->get("regalo");

        $data["regalo"] = DB::table("regalos")->where('id_regalo', $id)->first();
        // dd($data["regalo"]);
        $this->load->view("principal/novia/mesa_regalos/index", $data);
    }
}