<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Miscalificaciones extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->helper("formats");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Boda_model", "boda");
    }

    public function index() {
        $data["calificaciones"] = $this->proveedor->misRecomendaciones($this->session->id_boda);
        $data["boda"] = $this->boda->get($this->session->id_boda);
        $this->load->view("principal/novia/miscalificaciones/index", $data);
    }

}
