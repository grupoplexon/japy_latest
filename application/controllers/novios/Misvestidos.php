<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Misvestidos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('checker');
        $this->load->model('Vestido/Vestido_model', 'vestido');
        $this->load->model('Vestido/Tipo_model', 'tipo');
    }

    public function index() {
        $data['grupo'] = $this->vestido->misVestidoGroup($this->session->id_cliente);
        foreach ($data['grupo'] as &$value) {
            $value->vestidos = $this->vestido->misVestidos($this->session->id_cliente, $value->id_tipo_vestido, 3);
        }
        $this->load->view('principal/novia/misvestidos/index', $data);
    }

    public function tipo($nombre) {
        $data['grupo'] = $this->vestido->misVestidoGroup($this->session->id_cliente);
        $data['tipo'] = $this->tipo->get(array('nombre' => urldecode(str_replace('-', ' ', $nombre))));
        $data['vestidos'] = $this->vestido->misVestidos($this->session->id_cliente, $data['tipo']->id_tipo_vestido);
        if ($data['vestidos']) {
            foreach ($data['vestidos'] as &$value) {
                $value->like = $this->vestido->isLikeForMe($this->session->id_cliente, $value->id_vestido);
                $value->likes = $this->vestido->countLikes($value->id_vestido);
                $value->comments = $this->vestido->countComentarios($value->id_vestido);
            }
        }
        $this->load->view('principal/novia/misvestidos/tipo', $data);
    }

}
