<?php

class Pago extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('checker');
        $this->load->helper('formats');
        $this->load->library('categoria');
        $this->load->model('Presupuesto_model', 'presupuesto');
        $this->load->model('Pago_model', 'pagos');
    }

    public function index()
    {

    }

    public function presupuesto($id_presupuesto)
    {
        $pagos = $this->pagos->getList(array('id_presupuesto' => $id_presupuesto));
        if ($pagos) {
            return $this->output->set_content_type('application/json')
                ->set_status_header(202)
                ->set_output(json_encode(array(
                    'success' => true,
                    'data' => $pagos
                )));
        }
        return $this->output->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(array(
                'success' => false,
                'data' => 'error'
            )));
    }

    public function nuevo()
    {
        $this->output->set_content_type('application/json');

        if ($_POST) {
            $pago = ['id_presupuesto' => $this->input->post('presupuesto', true, 'Integer', ['min' => 1]),];
            $b = $this->pagos->insert($pago);
            if ($b) {
                $id = $this->pagos->last_id();
                $pago = $this->pagos->get($id);
                return $this->output->set_status_header(202)
                    ->set_output(json_encode(array(
                        'success' => true,
                        'data' => $pago
                    )));
            }
        }

        return $this->output->set_status_header(404)
            ->set_output(json_encode(array(
                'success' => false,
                'data' => 'error'
            )));
    }

    public function getAll()
    {
        if ($_POST) {
            $id_boda = $this->session->userdata('id_boda');
            $pagos = $this->pagos->getAllBoda($id_boda);
            if ($pagos) {
                return $this->output->set_content_type('application/json')
                    ->set_status_header(202)
                    ->set_output(json_encode(array(
                        'success' => true,
                        'data' => $pagos
                    )));
            }
        }
        return $this->output->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(array(
                'success' => false,
                'data' => ""
            )));
    }

    public function update()
    {
        // todo: check this out
        $error = "error";
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $tipo = $this->input->post('type', true, 'String', array('min' => 1));
            $id = $this->input->post('pago', true, 'Integer', array('min' => 1));
            $data = $this->input->post('data', true, 'String', array('min' => 1));

            $presupuesto = ["$tipo" => $data == "" ? null : $data];

            if ($this->input->isValid()) {
                $b = $this->pagos->update($id, $presupuesto);
                if ($b) {
                    return $this->output->set_status_header(202)
                        ->set_output(json_encode(array('success' => true, 'data' => 'ok')));
                }
            }
            $error = $this->input->getErrors();
        }
        return $this->output->set_status_header(404)
            ->set_output(json_encode(array('success' => false, 'data' => $error
            )));
    }

    public function eliminar()
    {
        $error = "error";
        if ($_POST) {
            $id = $this->input->post('pago', true, 'Integer', array('min' => 1));
            if ($this->input->isValid()) {
                if ($this->pagos->delete($id)) {
                    return $this->output->set_content_type('application/json')
                        ->set_status_header(202)
                        ->set_output(json_encode(array('success' => true, 'data' => 'ok')));
                }
            }
            $error = $this->input->getErrors();
        }
        return $this->output->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode(array('success' => false, 'data' => $error)));
    }

}
