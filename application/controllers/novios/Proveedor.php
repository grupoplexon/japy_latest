<?php
use Models\Gallery;
use Illuminate\Database\Capsule\Manager as DB;
class Proveedor extends MY_Controller
{
    protected function middleware()
    {
        return ['user_auth'];
    }
    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->helper("formats");
        $this->load->helper("collection_paginate");
        $this->load->library("task");
        // $this->load->library("categoria");
        $this->load->helper("cadena");
        $this->load->model("Boda_model", "boda");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Presupuesto_model", "presupuesto");
        $this->load->model('DefaultPorcentaje_model', 'defaults');
        $this->load->model("Proveedor/Inspiracion_model", "inspiracion");
    }
    function index()
    {
        $categories = Category::all();
        $id_boda             = $this->session->userdata("id_boda");
        $data["proveedores"] = DB::table('proveedor')
                                ->join('favorite_provider', 'favorite_provider.id_provider', '=', 'proveedor.id_proveedor')
                                ->where('favorite_provider.id_client', $this->session->userdata('id_cliente'))
                                ->join('galeria', 'galeria.id_proveedor', '=', 'proveedor.id_proveedor')
                                ->where('galeria.principal', 1)
                                ->select('favorite_provider.id_category', 'proveedor.*', 'galeria.nombre AS image')
                                ->groupBy('proveedor.id_proveedor')->get();

        $data["categories"] = DB::table('proveedor_categoria')
                                ->join('favorite_provider', 'proveedor_categoria.id', '=', 'favorite_provider.id_category')
                                ->groupBy('proveedor_categoria.id')
                                ->select('proveedor_categoria.*')->get();

        $iduser = $this->session->userdata("id_usuario");
        $guardadas = Saveinsp::where('id_usuario', $iduser)->get();
        for($i=0; $i<sizeOf($guardadas); $i++) {
            $data['inspiracion'][$i] = Inspiration::find($guardadas[$i]['id_inspiracion']);
        }
        $this->load->view("principal/novia/proveedor/index", $data);
    }
    function sugeridos($categoria = null)
    {
        $id_boda      = $this->session->userdata('id_boda');
        $data         = [];
        $data['boda'] = $this->boda->get($id_boda);
        if ($categoria == null) {
            $data['categorias'] = $this->defaults->getDefaults();
            $data['title']      = 'Proveedores sugeridos';
            foreach ($data['categorias'] as &$cat) {
                $cat->presupuestos = $this->presupuesto->getAllCategoria($id_boda, $cat->id_default_porcentaje);
                $a_favor           = 0;
                if ($cat->presupuestos) {
                    foreach ($cat->presupuestos as $p) {
                        $a_favor += $data['boda']->presupuesto * ($p->porcentaje / 100);
                    }
                }
                $cat->presupuesto = $a_favor;
                $proveedores      = [];
                $ids              = [];
                foreach ($cat->categorias as $value) {
                    $r = $this->proveedor->getProveedoresByCategoriaPresupuesto($value->enlace_proveedores,
                            $cat->presupuesto, $data['boda']->no_invitado);
                    if ($r) {
                        $uniq = [];
                        foreach ($r as $value) {
                            if ( ! key_exists($value->id_proveedor, $ids)) {
                                $ids[$value->id_proveedor] = true;
                                $uniq[]                    = $value;
                            }
                        }
                        $proveedores = array_merge($proveedores, $uniq);
                    }
                }
                $cat->proveedores = $proveedores;
                if ($cat->proveedores) {
                    foreach ($cat->proveedores as $p) {
                        $p->principal = $this->proveedor->getPrincipal($p->id_proveedor);
                        $p->logo      = $this->proveedor->getLogo($p->id_proveedor);
                        $p->precio    = $this->proveedor->precioDesde($p->id_proveedor);
                        $p->favorito  = $this->proveedor->isFavorito($p->id_proveedor, $id_boda);
                    }
                }
            }
            return $this->load->view('principal/novia/proveedor/sugeridos', $data);

        } else {
            $cat = $this->defaults->getCategoriaPrincipal(urldecode($categoria));
            if ($cat) {
                $data['page']     = $this->input->get('pag', true, 'Integer') ? $this->input->get('pag', true,
                        'Integer') : 1;
                $data['title']    = 'Proveedores sugeridos de '.urldecode($categoria);
                $cat->presupuesto = $data['boda']->presupuesto * ($cat->porcentaje / 100);
                $cat->proveedores = $this->proveedor->getProveedoresByCategoriaPresupuesto($cat->nombre,
                        $cat->presupuesto, $data['boda']->no_invitado, $data['page']);
                $proveedores      = [];
                $ids              = [];
                foreach ($cat->categorias as $value) {
                    $r = $this->proveedor->getProveedoresByCategoriaPresupuesto($value->enlace_proveedores,
                            $cat->presupuesto, $data['boda']->no_invitado);
                    if ($r) {
                        $uniq = [];
                        foreach ($r as $value) {
                            if ( ! key_exists($value->id_proveedor, $ids)) {
                                $ids[$value->id_proveedor] = true;
                                $uniq[]                    = $value;
                            }
                        }
                        $proveedores = array_merge($proveedores, $uniq);
                    }
                }
                $cat->proveedores = $proveedores;
                if ($cat->proveedores) {
                    foreach ($cat->proveedores as $p) {
                        $p->principal = $this->proveedor->getPrincipal($p->id_proveedor);
                        $p->logo      = $this->proveedor->getLogo($p->id_proveedor);
                        $p->precio    = $this->proveedor->precioDesde($p->id_proveedor);
                        $p->favorito  = $this->proveedor->isFavorito($p->id_proveedor, $id_boda);
                    }
                }
                $data['total']     = $this->proveedor->getCountCategoriaPresupuesto($cat->nombre, $cat->presupuesto,
                        $data['boda']->no_invitado);
                $data['categoria'] = $cat;

                return $this->load->view('principal/novia/proveedor/sugeridos_categoria', $data);
            }
            show_404();
        }
    }
    function categoria($categoria)
    {
        $id_boda             = $this->session->userdata("id_boda");
        $categoria           = urldecode(str_replace("-", " ", $categoria));
        $totales             = new stdClass();
        $totales->categorias = Categoria::getCategorias();
        foreach ($totales->categorias as $key => &$cat) {
            $c          = str_sin_acentos(strtoupper($cat->nombre));
            $cat->total = $this->boda->countCategoria($id_boda, $c);
        }
        $data["totales"] = $totales;
        if ($this->categoria->exists($categoria)) {
            $data["boda"]              = $this->boda->get($id_boda);
            $data["categoria_acentos"] = ($categoria);
            $data["categoria"]         = str_sin_acentos($categoria);
            $data["proveedores"]       = $this->boda->getProveedores($id_boda, $data["categoria"]);
            foreach ($data["proveedores"] as $key => $p) {
                $p->principal = $this->proveedor->getPrincipal($p->id_proveedor);
                $p->logo      = $this->proveedor->getLogo($p->id_proveedor);
            }
            $this->load->view("principal/novia/proveedor/categoria", $data);
        } else {
            show_404();
        }
    }
    public function getCategoria()
    {
        $this->output->set_content_type('application/json');
        $cat = $this->input->get("categoria", true, "String", ["required" => true]);
        $c   = str_sin_acentos(strtoupper($cat));
        if ($this->categoria->exists($cat)) {
            $id_boda   = $this->session->userdata("id_boda");
            $proveedor = $this->boda->getProveedorCategoriaMini($id_boda, $cat);
            if ($proveedor) {
                $proveedor->principal = $this->proveedor->getPrincipalMini($proveedor->id_proveedor);
                $proveedor->total     = $this->boda->countCategoria($id_boda, $c);

                return $this->output->set_status_header(202)->set_output(json_encode([
                        'success' => true,
                        'data'    => $proveedor,
                ]));
            }
        }
        return $this->output->set_status_header(404)->set_output(json_encode([
                'success' => false,
                'data'    => "error",
        ]));
    }
    public function getProvider()
    {
        $this->output->set_content_type('application/json');
        $id_boda  = $this->session->userdata("id_boda");
        $provider = Provider::with([
                'imagePrincipal',
                'weddings' => function ($query) use ($id_boda) {
                    return $query->where('proveedor_boda.id_boda', $id_boda);
                },
        ])->find($this->input->get('providerId'));

        if ($provider) {
            $provider->principal = $provider->imagePrincipal->pluck('id_galeria');
            unset($provider->imagePrincipal);

            return $this->output->set_status_header(200)->set_output(json_encode([
                    'success' => true,
                    'data'    => $provider->toArray(),
            ]));
        }
        return $this->output->set_status_header(404)->set_output(json_encode([
                'success' => false,
                'data'    => "error",
        ]));
    }
    function change_estado($proveedor)
    {
        if ($_POST) {
            if (is_numeric($proveedor) && $proveedor > 0) {
                $id_boda = $this->session->userdata("id_boda");
                $estado  = $this->input->post("estado", true, "Integer", ["required" => true, "min" => 0, "max" => 5]);
                $p       = $this->proveedor->get($proveedor);
                if ($this->input->isValid() && $p) {
                    $b = $this->boda->updateProveedor($id_boda, $p->id_proveedor, $estado);
                    if ($b) {
                        return $this->output->set_content_type('application/json')->set_status_header(202)->set_output(json_encode([
                                'success' => true,
                                'data'    => "ok",
                        ]));
                    }
                }
            }
        }
        return $this->output->set_content_type('application/json')->set_status_header(404)->set_output(json_encode([
                'success' => false,
                'data'    => "error",
        ]));
    }
    function change_precio($proveedor)
    {
        if ($_POST) {
            if (is_numeric($proveedor) && $proveedor > 0) {
                $id_boda = $this->session->userdata("id_boda");
                $estado  = $this->input->post("precio", true, "Decimal", ["required" => true, "min" => 0]);
                $p       = $this->proveedor->get($proveedor);
                if ($this->input->isValid() && $p) {
                    $b = $this->boda->updateProveedorPrecio($id_boda, $p->id_proveedor, $estado);
                    if ($b) {
                        return $this->output->set_content_type('application/json')->set_status_header(202)->set_output(json_encode([
                                'success' => true,
                                'data'    => "ok",
                        ]));
                    }
                }
            }
        }
        return $this->output->set_content_type('application/json')->set_status_header(404)->set_output(json_encode([
                'success' => false,
                'data'    => "error",
        ]));
    }
    function calificar($proveedor)
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            if (is_numeric($proveedor) && $proveedor > 0) {
                $id_boda = $this->session->userdata("id_boda");
                $cal     = $this->input->post("cal", true, "Integer", ["required" => true, "min" => 0, "max" => 5]);
                $p       = $this->proveedor->get($proveedor);
                if ($this->input->isValid() && $p) {
                    $b = $this->boda->updateProveedorCal($id_boda, $p->id_proveedor, $cal);
                    if ($b) {
                        return $this->output->set_status_header(202)->set_output(json_encode([
                                'success' => true,
                                'data'    => "ok",
                        ]));
                    }
                }
            }
        }
        return $this->output->set_status_header(404)->set_output(json_encode([
                'success' => false,
                'data'    => "error",
        ]));
    }
    function eliminar($proveedor)
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            if (is_numeric($proveedor) && $proveedor > 0) {
                $id_boda = $this->session->userdata("id_boda");
                $pro     = $this->input->post("proveedor", true, "Integer", ["required" => true, "min" => 0]);
                if ($this->input->isValid() && $proveedor == $pro) {
                    $p = $this->proveedor->get($proveedor);
                    if ($p) {
                        $b = $this->boda->removeProveedor($id_boda, $p->id_proveedor);
                        if ($b) {
                            return $this->output->set_status_header(202)->set_output(json_encode([
                                    'success' => true,
                                    'data'    => "ok",
                            ]));
                        }
                    }
                }
            }
        }
        return $this->output->set_status_header(404)->set_output(json_encode([
                'success' => false,
                'data'    => "error",
        ]));
    }
    function search()
    {
        $this->output->set_content_type("application/json");
        if ($_POST) {
            $search    = $this->input->post("search");
            $page      = $this->input->post("page");
            $weddingId = $this->session->userdata("id_boda");

            $providers = Provider::where(function ($query) use ($search) {
                return $query->where("nombre", "like", "%".$search."%")
                        ->orWhere("localizacion_estado", "like", "%".$search."%");
            })->whereHas("user", function ($query) {
                return $query->where("activo", "1");
            })->whereHas("categories", function ($query) {
                return $query->notMain();
            })->with([
                    "categories",
                    "imagePrincipal" => function ($query) {
                        return $query->select(["id_galeria", "id_proveedor", "nombre"]);
                    },
            ])->with([
                    "weddings" => function ($query) use ($weddingId) {
                        return $query->where("proveedor_boda.id_boda", $weddingId);
                    },
            ])->get()->sortByDesc("tipo_cuenta");
            $providers = collection_paginate($providers, 3, $page);

            foreach ($providers as $provider) {
                $provider->indicators()->attach($this->session->userdata("id_usuario"),
                        ["type" => "appearance_search"]);
            }
            return $this->output->set_status_header(202)->set_output(json_encode($providers));
        }
        return $this->output->set_status_header(404)->set_output(json_encode([
                "success" => false,
                "data"    => "error",
        ]));
    }
    function agregar()
    {
        $this->output->set_content_type("application/json");
        if ($_POST) {
            $proveedor = $this->input->post("proveedor", true, "Integer", ["required" => true, "min" => 1]);
            $p         = $this->proveedor->get($proveedor);
            if ($p) {
                $id_boda = $this->session->userdata("id_boda");

                $wedding = Wedding::whereHas('providers', function ($query) use ($p) {
                    $query->where('proveedor_boda.id_proveedor', $p->id_proveedor);
                })->find($id_boda);
                if ($wedding) {
                    return $this->output->set_status_header(404)->set_output(json_encode([
                            'success' => false,
                            'data'    => "error",
                    ]));
                }
                if ($this->boda->insertProveedorBoda($id_boda, $p->id_proveedor)) {
                    return $this->output->set_status_header(202)->set_output(json_encode([
                            'success' => true,
                            'data'    => "ok",
                    ]));
                }
            }
        }
        return $this->output->set_status_header(404)->set_output(json_encode([
                'success' => false,
                'data'    => "error",
        ]));
    }
    function recomendar()
    {
        $this->output->set_content_type('application/json');
        $id_boda = $this->session->userdata("id_boda");
        if ($_POST && $id_boda) {
            $id_proveedor = $this->input->post("proveedor", true, "String", ["required" => true]);
            $resena       = [
                    "calidad_servicio"    => $this->input->post("calidad", true, "Integer", [
                            "required" => true,
                            "min"      => 1,
                            "max"      => 5,
                    ]),
                    "respuesta"           => $this->input->post("respuesta", true, "Integer", [
                            "required" => true,
                            "min"      => 1,
                            "max"      => 5,
                    ]),
                    "relacion_calidad"    => $this->input->post("relacion_calidad", true, "Integer", [
                            "required" => true,
                            "min"      => 1,
                            "max"      => 5,
                    ]),
                    "flexibilidad"        => $this->input->post("flexibilidad", true, "Integer", [
                            "required" => true,
                            "min"      => 1,
                            "max"      => 5,
                    ]),
                    "profesionalismo"     => $this->input->post("profesionalismo", true, "Integer", [
                            "required" => true,
                            "min"      => 1,
                            "max"      => 5,
                    ]),
                    "resena"              => $this->input->post("resena", true, "String", ["required" => true]),
                    "fecha_recomendacion" => (new DateTime())->format('Y-m-d H:i:s'),
            ];
            if ($this->input->isValid()) {
                $b = $this->boda->updateProveedorResena($id_boda, $id_proveedor, $resena);
                if ($b) {
                    return $this->output->set_status_header(202)->set_output(json_encode([
                            'success' => true,
                            'data'    => "ok",
                    ]));
                }
            }
        }
        return $this->output->set_status_header(404)->set_output(json_encode([
                'success' => false,
                'data'    => $this->input->getErrors(),
        ]));
    }
    public function modalInfo() {
        if($_POST) {
            $id = $this->input->post("id_ins");
            $info = Inspiration::with('tags')->where('id_inspiracion', $id)->first();
            $comentarios = Coment::where('id_inspiracion',$id)->get();
            $data="";
            $data2="";
            $tam = sizeOf($comentarios);
            $i=0;
            for($i; $i < $tam; $i++) {
                $data[$i]["names"] = $comentarios[$i]['nombre_cliente'];
                $data[$i]["texto"] = $comentarios[$i]['comentario'];
            }
            $tam2 = sizeOf($info['relations']['tags']);
            for($i=0; $i < $tam2; $i++) {
                $data2[$i]['tags'] = $info['relations']['tags'][$i]['title'];
            }
            $foto = Gallery::where('id_proveedor', $info->id_proveedor)->where('logo', 1)
            ->where('principal', 0)->first();
            $nomProveedor = Provider::where('id_proveedor', $info->id_proveedor)->first();
            // dd($info);
            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['inspiracion' => $info, 'comentarios' => $data,
                     'fotoInfo' => $foto->nombre, 'nomproInfo' => $nomProveedor->nombre,
                     'tags' => $data2]));
        }
    }
    public function deleteInspiration() {
        if($_POST) {
            $id = $this->input->post("id");
            $delete = Saveinsp::where('id_inspiracion', $id)->delete();
            return $this->output->set_status_header(200);
        }
    }
}
