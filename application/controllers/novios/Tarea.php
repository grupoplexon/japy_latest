<?php

class Tarea extends MY_Controller
{
    protected function middleware()
    {
        return ['user_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->library("task");
        $this->load->helper("formats");
        $this->load->library("categoria");
        $this->load->model("Tarea_model", "tarea");
        $this->load->model("Presupuesto_model", "presupuesto");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Boda_model", "boda");
    }

    public function index()
    {
        $estado = $this->input->get("estado", true, "Integer", ["min" => 0, "max" => 1]);
        $group  = $this->input->get("grupo");
        $boda   = $this->session->userdata("id_boda");
        $cat    = $this->input->get("cat", true, "Integer", ["min" => 0]);
        $s      = ["id_boda" => $this->session->userdata("id_boda")];
        if ($estado !== null) {
            $s["completada"] = $estado;
        }

        if ($cat) {
            $s["categoria"] = $cat;
        }

        $data["estado"]    = $estado;
        $data["categoria"] = $cat;

        if (strtolower($group) == "categoria") {
            $g = "categoria";
        } else {
            $g = "tiempo";
        }

        $data["grupo"]             = $g;
        $fecha2                    = Wedding::where('id_boda', $boda)->get(['fecha_creacion'])->first();
        $data["fecha_creacion"]    = $fecha2->fecha_creacion;
        $data["fecha_boda"]        = $this->tarea->getFechaBoda($boda);
        $data["total_completadas"] = $this->task->getTotalCompletadas();
        $data["tareas"]            = $this->tarea->getListOrderBy($s, $g, "asc");
        if($data["tareas"] == false)
            $data["tareas"] = [];
        if(!empty($s["categoria"])){
            $res = Category::select('id')->where("parent_id", $s["categoria"])->get();

            foreach ($res as $k => $v) {
                $aux = $s;
                $aux['categoria'] = $v["id"];
                $aux = $this->tarea->getListOrderBy($aux, $g, "asc");

                if(!empty($aux))
                    $data["tareas"] = array_merge($data["tareas"], $aux);
            }

            $cat = $v["id"];

            $data['categorias'] = Categoria::getCategorias();

            if ($data["tareas"]) {
                foreach ($data["tareas"] as $key => &$tarea) {
                    if ($cat && ($tarea->categoria != $cat)) {
                        continue;
                    }

                    $tarea->presupuesto = $this->presupuesto->get($tarea->id_presupuesto);

                    foreach ($data['categorias'] as $categoria) {
                        if ($tarea->categoria != $categoria->id) {
                            continue;
                        } else {
                            $tarea->categoria_titulo = $categoria->nombre;
                            break;
                        }
                    }

                    if ( ! is_null($tarea->id_proveedor)) {
                        $provider            = Provider::with(['imagePrincipal', 'categories'])->find($tarea->id_proveedor);
                        $provider->principal = $provider->imagePrincipal->pluck('nombre')->first();
                        unset($provider->imagePrincipal);
                        $tarea->proveedor = $provider;
                    }
                }
            }

        }

        // dd($cat);
        // dd($data["tareas"]);////NO BORRRAR CODIGO DE ABAJO ES EL ORIGINAL

        $data['categorias']        = Category::where('parent_id',1)->get();

        // dd($data['categorias']);
        // if ($data["tareas"]) {
        //     foreach ($data["tareas"] as $key => &$tarea) {
        //         if ($cat && ($tarea->categoria != $cat)) {
        //             unset($data['tareas'][$key]);
        //             continue;
        //         }

        //         $tarea->presupuesto = $this->presupuesto->get($tarea->id_presupuesto);

        //         foreach ($data['categorias'] as $categoria) {
        //             if ($tarea->categoria != $categoria->id) {
        //                 continue;
        //             } else {
        //                 $tarea->categoria_titulo = $categoria->nombre;
        //                 break;
        //             }
        //         }

        //         if ( ! is_null($tarea->id_proveedor)) {
        //             $provider            = Provider::with(['imagePrincipal', 'categories'])->find($tarea->id_proveedor);
        //             $provider->principal = $provider->imagePrincipal->pluck('nombre')->first();
        //             unset($provider->imagePrincipal);
        //             $tarea->proveedor = $provider;
        //         }
        //     }
        // }

        $data['tareas'] = is_array($data['tareas']) ? array_values($data['tareas']) : [];
        // dd($data['tareas']);

        foreach ($data["categorias"] as $key => &$cat) {
            $c              = str_sin_acentos(strtoupper($cat->nombre));
            $cat->total     = $this->boda->countCategoria($boda, $c);
            $cat->proveedor = $this->boda->getProveedorCategoria($boda, $c);
            if ($cat->proveedor) {
                $cat->proveedor->principal = $this->proveedor->getPrincipal($cat->proveedor->id_proveedor);
            }
        }
        if ($boda) {
            $data["weddingProviders"] = Wedding::with([
                "providers" => function ($query) {
                    return $query->whereHas("user", function ($query) {
                        return $query->where("activo", 1);
                    });
                },
            ])->find($boda)->providers;
        }
        $this->load->view("principal/newheadernovia");
        $this->load->view("principal/novia/menu");
        $this->load->view("principal/novia/tareas/index", $data);
        $this->load->view("japy/prueba/footer");
    }
    public function nueva()
    {
        if ($_POST) {

            $tarea = [
                "id_presupuesto" => null,
                "id_boda"        => $this->session->userdata("id_boda"),
                "nombre"         => $this->input->post("nombre", true, "String", ["min" => 1, "max" => 255]),
                "tiempo"         => $this->input->post("tiempo", true, "Integer",
                    ["required" => true, "min" => 1, "max" => 9]),
                "categoria"      => $this->input->post("categoria", true, "Integer", ["min" => 1]),
                "descripcion"    => $this->input->post("descripcion", true, "String", ["min" => 0, "max" => 260]),
                "completada"     => 0,
            ];

            if ($this->input->isValid()) {
                $b = $this->tarea->insert($tarea);
                if ($b) {
                    $id   = $this->tarea->last_id();
                    $pago = $this->tarea->get($id);

                    return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $pago,
                        ]));
                }
            }
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => 'error',
            ]));
    }

    public function addNota()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $boda = $this->session->userdata('id_boda');
            $id   = $this->input->post('tarea', true, 'Integer', ['min' => 1]);
            $nota = $this->input->post('nota', true, 'String', ['min' => 0, 'max' => 320]);
            if ($this->input->isValid()) {
                $tarea = [
                    'nota' => $nota,
                ];
                $b     = $this->tarea->updateTarea($id, $boda, $tarea);
                if ($b) {
                    return $this->output
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $b,
                        ]));
                }
            }
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => null,
            ]));
    }

    public function updateProvider()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $wedding    = $this->session->userdata("id_boda");
            $taskId     = $this->input->post("taskId", true, "Integer", ["min" => 1]);
            $providerId = $this->input->post("providerId", true, "Integer", ["min" => 1]);

            if ($this->input->isValid()) {
                $task = $this->tarea->get($taskId);

                if ($task && ($task->id_proveedor != null && $task->id_proveedor == $providerId)) {
                    $task = ["id_proveedor" => null];
                } else {
                    $task = ["id_proveedor" => $providerId];
                }

                $this->tarea->update($taskId, $task);

                $task = $this->tarea->get($taskId);

                if ($task) {
                    return $this->output
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $task,
                        ]));
                }
            }
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => null,
            ]));
    }

    public function presupuestos()
    {
        $this->output->set_content_type('application/json');
        //$boda = $this->session->userdata("id_boda");
        //$p = $this->presupuesto->getAllCategoria($boda, $cat);
        $budgets = Budget::with('payments')->where('id_boda', $this->session->userdata('id_boda'))->get();

        $budgets->map(function ($budget) {
            if ($budget->payments->count()) {
                $budget->pagado = $budget->payments->first()->monto;
            }
        });

        if ($budgets) {
            return $this->output
                ->set_status_header(202)
                ->set_output(json_encode([
                    'success' => true,
                    'data'    => $budgets,
                ]));
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => null,
            ]));
    }

    public function estado()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $id         = $this->input->post("tarea", true, "Integer", ["min" => 1]);
            $completada = $this->input->post("completada", true, "Integer", ["min" => 0, "max" => 1]);
            if ($this->input->isValid()) {
                $tarea = ["completada" => $completada];
                $b     = $this->tarea->update($id, $tarea);

                if ($completada == '1') {
                    $tarea = [
                        'fecha_asignada' => null,
                    ];

                    $b = $this->tarea->update($id, $tarea);
                }

                if ($b) {
                    return $this->output
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $b,
                        ]));
                }
            }
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => null,
            ]));
    }

    public function updatePresupuesto()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $presupuesto = $this->input->post("presupuesto", true, "Integer", ["min" => 1]);
            $id          = $this->input->post("tarea", true, "Integer", ["min" => 0]);
            if ($this->input->isValid()) {
                $tarea = ["id_presupuesto" => $presupuesto];
                $b     = $this->tarea->update($id, $tarea);
                if ($b) {
                    return $this->output
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $b,
                        ]));
                }
            }
        }

        return $this->output->set_status_header(404);
    }

    public function eliminar()
    {
        $this->output->set_content_type('application/json');
        $boda  = $this->session->userdata("id_boda");
        $tarea = $this->input->post("tarea", true, "Integer", ["min" => 1]);
        if ($this->input->isValid()) {
            $b = $this->tarea->delete(["id_tarea" => $tarea, "id_boda" => $boda]);

            return $this->output
                ->set_status_header(202)
                ->set_output(json_encode([
                    'success' => true,
                    'data'    => $b,
                ]));
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => null,
            ]));
    }

    public function eventDates()
    {
        $this->output->set_content_type("application/json");
        $clientId = $this->session->userdata("id_cliente");

        if ($_POST && $clientId) {
            $tasks = $this->input->post("tasks");
            $tasksWithDate = $this->tarea->getTareasConFecha($tasks);

            return $this->output
                        ->set_status_header(200)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $tasksWithDate,
                        ]));
        }

        return $this->output
                    ->set_status_header(500)
                    ->set_output(json_encode([
                        'success' => false,
                        'data'    => null,
                    ]));
    }

    public function addEventDate()
    {
        $this->output->set_content_type("application/json");
        $clientId = $this->session->userdata("id_cliente");

        if ($_POST && $clientId) {
            $taskId = $this->input->post("task_id");
            $assignedDate = $this->input->post("date");
            $tasks = $this->input->post("tasks");

            $this->tarea->updateFechaTarea($taskId, $assignedDate);
            $tasksWithDate = $this->tarea->getTareasConFecha($tasks);

            return $this->output
                        ->set_status_header(200)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $tasksWithDate,
                        ]));
        }

        return $this->output
                    ->set_status_header(500)
                    ->set_output(json_encode([
                        'success' => false,
                        'data'    => null,
                    ]));
    }

}
