<?php
class Forum extends CI_Controller {

    public $input;    

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Comunidad/Comunidad_model", "comunidad");
        $this->load->model("Comunidad/Imagen_model", "imagen");
        $this->load->model("Comunidad/Foto_model", "foto");
        $this->load->model("Comunidad/Video_model", "video");
        $this->load->model("Comunidad/Foro_model", "foro");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/";
        $this->load->helper('formats_helper');
    }
    
    public function formatearDebates(){
        $ultimos_debates2 = "";
        $ultimos_debates = $this->comunidad->getUltimosDebates();
        $i = 0;
        if($ultimos_debates){
            foreach ($ultimos_debates as $debate){
                $debate->fecha_creacion = relativeTimeFormat($debate->fecha_creacion, $mini=FALSE);
                if(strlen($debate->fecha_creacion) <= 10 && strlen($debate->fecha_creacion) > 4){
                    $debate->fecha_creacion = "Hace $debate->fecha_creacion";
                }else{
                    $debate->fecha_creacion = "El $debate->fecha_creacion";
                }
                if(!empty($debate->mime)){
                    $debate->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$debate->id_usuario";
                }else{
                    $debate->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                if(empty($debate->comentarios)){
                    $debate->comentarios = 0;
                }
                $debate->url_debate = base_url()."index.php/novios/comunidad/Home/debatePublicado/$debate->id_debate";
                $ultimos_debates2[$i] = $debate;
                $i++;
            }
        }
        return $ultimos_debates2;
    }
    
    public function foratearDebatesComentados(){
        $debates_comentados2 = "";
        $debates_comentados = $this->comunidad->getDebatesComentados();
        $i = 0;
        if($debates_comentados){
            foreach ($debates_comentados as $debate){
                $datos = $this->comunidad->datosDebates($debate->id_debate);
                $debate->fecha = relativeTimeFormat($debate->fecha, $mini=FALSE);
                if(strlen($debate->fecha) <= 10 && strlen($debate->fecha) > 4){
                    $debate->fecha = "Hace $debate->fecha";
                }else{
                    $debate->fecha = "El $debate->fecha";
                }
                if(!empty($datos->mime)){
                    $debate->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$datos->id_usuario";
                }else{
                    $debate->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                $debate->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$datos->id_usuario";
                $debate->titulo_debate = $datos->titulo_debate;
                $debate->usuario = $datos->usuario;
                $debate->fecha_creacion = relativeTimeFormat($datos->fecha_creacion, $mini=FALSE);
                if(strlen($debate->fecha_creacion) <= 10 && strlen($debate->fecha_creacion) > 4){
                    $debate->fecha_creacion = "Hace $debate->fecha_creacion";
                }else{
                    $debate->fecha_creacion = "El $debate->fecha_creacion";
                }
                $debate->debate = substr($datos->debate,0,255);
                $debate->debate = "$debate->debate ...";
                $debates_comentados2[$i] = $debate;
                $i++;
            }
        }
        return $debates_comentados2;
    }
    
    public function formatearMasActivos(){
        $mas_activos = $this->comunidad->usuariosActivos();
        $mas_activos2 = "";
        $i = 0;
        if($mas_activos){
            foreach ($mas_activos as $activo) {
                $activo = $this->comunidad->getDatosActivos($activo->id_usuario);
                if($activo){
                    $activo->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$activo->id_usuario";
                    if(!empty($activo->mime)){
                        $activo->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$activo->id_usuario";
                    }else{
                        $activo->foto_usuario = base_url()."dist/img/blog/perfil.png";
                    }
                    $activo->fecha_boda = new DateTime($activo->fecha_boda);
                    $mes = $this->formatearMes($activo->fecha_boda);
                    $anio = $activo->fecha_boda->format('Y');
                    $activo->fecha_Boda = "Me caso en $mes de $anio, $activo->poblacion";
                    $mas_activos2[$i] = $activo;
                    $i++;
                }
            }
        }
        return $mas_activos2;
    }
    
    public function formatearVisitas(){
        $visitas_perfil = $this->comunidad->getVisitas();
        $visitas_perfil2 = "";
        $i = 0;
        if($visitas_perfil){
            foreach ($visitas_perfil as $visita){
                $visita->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$visita->id_usuario";
                if(!empty($visita->mime)){
                    $visita->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$visita->id_usuario";
                }else{
                    $visita->foto_usuario = base_url()."dist/img/blog/perfil.png";
                }
                $usuario = $this->session->userdata('id_usuario');
                $visita->todos = base_url()."index.php/novios/comunidad/perfil/visitas/$usuario";
                $visitas_perfil2[$i] = $visita;
                $i++;
            }
        }
        return $visitas_perfil2;
    }
    
    public function formatearCoincidencia(){
        $usuarios_boda = $this->comunidad->usuariosBoda();
        if($usuarios_boda){
            if(!empty($usuarios_boda->fecha_boda)){
                $usuarios_boda->fecha_boda = new DateTime($usuarios_boda->fecha_boda);
                $mes = $this->formatearMes($usuarios_boda->fecha_boda);
                $anio = $usuarios_boda->fecha_boda->format('Y');
                $dia = $usuarios_boda->fecha_boda->format('d');
                $usuarios_boda->fecha_boda = "$dia de $mes de $anio";
            }
            if(!empty($usuarios_boda->mime)){
                $usuarios_boda->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/".$this->session->userdata('id_usuario');
            }else{
                $usuarios_boda->foto_usuario = base_url()."dist/img/blog/perfil.png";
            }
            return $usuarios_boda;
        }
    }
    
    public function formatearMes($fecha){
//        echo date('Y-m-d h:i:s');
//        $result = $this->comunidad->conocerCompaneros();
//        $fecha = new DateTime($result->fecha_boda);
//        echo $fecha->format("F Y");
        $mes = $fecha->format("F");
        switch ($mes){
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return  $mes;
    }
    
    public function formatearDebatesForo($debates){
        $debates2 = "";
        $i = 0;
        if($debates){
            foreach ($debates as $debate){
                $debate->fecha_creacion = relativeTimeFormat($debate->fecha_creacion, $mini=FALSE);
                if(strlen($debate->fecha_creacion) <= 10 && strlen($debate->fecha_creacion) > 4){
                    $debate->fecha_creacion = "Hace $debate->fecha_creacion";
                }else{
                    $debate->fecha_creacion = "El $debate->fecha_creacion";
                }
                if(!empty($debate->mime)){
                    $debate->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$debate->id_usuario";
                }else{
                    $debate->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                $debate->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$debate->id_usuario";
                $debate->url_debate = base_url()."index.php/novios/comunidad/Home/debatePublicado/$debate->id_debate";
                $debate->debate = substr($debate->debate, 0, 255);
                $debate->debate = "$debate->debate ...";
                $debates2[$i] = $debate;
                $i++;
            }
        }
        return $debates2;
    }
    
    public function debates($ordenamiento = "",$pagina = 1){
        $datos = "";
        $bandera = true;
        $grupos = $this->comunidad->getGrupos();
        $visitas_perfil = $this->formatearVisitas();
        $usuarios_boda = $this->formatearCoincidencia();
        $grupos_miembro = $this->comunidad->getGruposMiembro();
        if(empty($ordenamiento)){
            $ultimos_debates = $this->foro->getUltimosDebates();
            $debates_populares = $this->foro->getDebatesPopulares();
            $ultimos_debates = $this->formatearDebatesForo($ultimos_debates);
            $debates_populares = $this->formatearDebatesForo($debates_populares);
            $datos = array(
                'grupos' => $grupos,
                'ultimos_debates' => $ultimos_debates,
                'debates_populares' => $debates_populares,
                'visitas_perfil' => $visitas_perfil,
                'usuarios_boda' => $usuarios_boda,
                'grupos_miembro' => $grupos_miembro 
            );
            $this->load->view('principal/novia/comunidad/Foro/foro_debates',$datos);
        }else{
            switch ($ordenamiento){
                case "recientes":
                    $contador = $this->foro->getTotalDebates();
                    $paginas = $this->obtenerPaginas($pagina, $contador);
                    if(!empty($paginas)){
                        $debates = $this->foro->filtroDebatesRecientes($paginas['Home']);
                        $debates = $this->formatearDebatesForo($debates);
                        $datos = array(
                            'grupos' => $grupos,
                            'debates' => $debates,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'total_paginas' => $paginas['total_paginas'],
                            'pagina' => $pagina,
                            'ordenamiento' => $ordenamiento
                        );
                    }else{
                        $bandera = false;
                    }
                    break;
                case "populares":
                    $contador = $this->foro->getTotalDebates();
                    $paginas = $this->obtenerPaginas($pagina, $contador);
                    if(!empty($paginas)){
                        $debates = $this->foro->filtroDebatesPopulares($paginas['Home']);
                        $debates = $this->formatearDebatesForo($debates);
                        $datos = array(
                            'grupos' => $grupos,
                            'debates' => $debates,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'total_paginas' => $paginas['total_paginas'],
                            'pagina' => $pagina,
                            'ordenamiento' => $ordenamiento
                        );
                    }else{
                        $bandera = false;
                    }
                    break;
                default :
                    $bandera = false;
                    $this->error_404();
            }
            if($bandera){
                $this->load->view('principal/novia/comunidad/Foro/filtro_debates',$datos);
            }
        }
    }
    
    public function fotos($ordenamiento = "", $pagina = 1){
        $datos = "";
        $bandera = true;
        $grupos = $this->comunidad->getGrupos();
        $visitas_perfil = $this->formatearVisitas();
        $usuarios_boda = $this->formatearCoincidencia();
        $grupos_miembro = $this->comunidad->getGruposMiembro();
        if(empty($ordenamiento)){
            $ultimas_fotos = $this->foro->getUltimasFotos();
            $fotos_vistas = $this->foro->getFotosMasVistas();
            $ultimas_fotos2 = $this->formatearFotos($ultimas_fotos);
            $fotos_vistas2 = $this->formatearFotos($fotos_vistas);
            $datos = array(
                'grupos' => $grupos,
                'ultimas_fotos' => $ultimas_fotos2,
                'fotos_vistas' => $fotos_vistas2,
                'visitas_perfil' => $visitas_perfil,
                'usuarios_boda' => $usuarios_boda,
                'grupos_miembro' => $grupos_miembro 
            );
            $this->load->view('principal/novia/comunidad/Foro/foro_fotos',$datos);
        }else{
            switch ($ordenamiento){
                case "recientes":
                    $contador = $this->foro->getTotalFotos();
                    $paginas = $this->obtenerPaginas($pagina,$contador);
                    if(!empty($paginas)){
                        $fotos = $this->foro->filtroFotosFecha($paginas['Home']);
                        $fotos = $this->formatearFotos($fotos);
                        $datos = array(
                            'grupos' => $grupos,
                            'fotos' => $fotos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'total_paginas' => $paginas['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina
                        );
                    }else{
                        $bandera = FALSE;
                    }
                    break;
                case "visitadas":
                    $contador = $this->foro->getTotalFotos();
                    $paginas = $this->obtenerPaginas($pagina,$contador);
                    if(!empty($paginas)){
                        $fotos = $this->foro->filtroFotosVistas($paginas['Home']);
                        $fotos = $this->formatearFotos($fotos);
                        $datos = array(
                            'grupos' => $grupos,
                            'fotos' => $fotos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'total_paginas' => $paginas['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina

                        );
                    }else{
                        $bandera = false;
                    }
                    break;
                case "comentadas":
                    $contador = $this->foro->getTotalFotos();
                    $paginas = $this->obtenerPaginas($pagina,$contador);
                    if(!empty($paginas)){
                        $fotos = $this->foro->filtroFotosComentarios($paginas['Home']);
                        $fotos = $this->formatearFotos($fotos);
                        $datos = array(
                            'grupos' => $grupos,
                            'fotos' => $fotos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'total_paginas' => $paginas['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina
                        );
                    }else{
                        $bandera = false;
                    }
                    break;
                default :
                    $bandera = false;
                    $this->error_404();
            }
            if($bandera){
                $this->load->view('principal/novia/comunidad/Foro/filtro_fotos',$datos);
            }
        }
    }
    
    public function formatearFotos($fotos){
        $fotos2 = "";
        $i = 0;
        if($fotos){
            foreach ($fotos as $foto){
                if(!empty($foto->mime)){
                    $foto->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$foto->id_usuario";
                }else{
                    $foto->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                $fotos2[$i] = $foto;
                $i++;
            }
        }
        return $fotos2;
    }
    
    public function obtenerPaginas($pagina,$contador){
        $total_paginas = (int)($contador / 16);
        if($total_paginas < ($contador/16)){
            $total_paginas++;
        }
        if($total_paginas == 0){
                $total_paginas = 1;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 16;
            $datos = array(
                'total_paginas' => $total_paginas,
                'Home'          => $inicio
            );
            return $datos;
        }else{
            $this->error_404();
        }
    }
    
    public function formatearVideos($videos){
        $videos2 = "";
        $i = 0;
        if($videos){
            foreach ($videos as $video){
                if(!empty($video->mime)){
                    $video->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$video->id_usuario";
                }else{
                    $video->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                $videos2[$i] = $video;
                $i++;
            }
        }
        return $videos2;
    }
    
    public function videos($ordenamiento = "", $pagina = 1){
        $datos = "";
        $bandera = true;
        $grupos = $this->comunidad->getGrupos();
        $visitas_perfil = $this->formatearVisitas();
        $usuarios_boda = $this->formatearCoincidencia();
        $grupos_miembro = $this->comunidad->getGruposMiembro();
        if(empty($ordenamiento)){
            $ultimos_videos = $this->foro->getUltimosVideos();
            $videos_vistos = $this->foro->getVideosMasVistos();
            $ultimos_videos2 = $this->formatearVideos($ultimos_videos);
            $videos_vistos2 = $this->formatearVideos($videos_vistos);
            $datos = array(
                'grupos' => $grupos,
                'videos_recientes' => $ultimos_videos2,
                'videos_vistos' => $videos_vistos2,
                'visitas_perfil' => $visitas_perfil,
                'usuarios_boda' => $usuarios_boda,
                'grupos_miembro' => $grupos_miembro 
            );
            $this->load->view('principal/novia/comunidad/Foro/foro_videos',$datos);
        }else{
            switch ($ordenamiento){
                case "recientes":
                    $contador = $this->foro->getTotalVideos();
                    $paginas = $this->obtenerPaginas($pagina,$contador);
                    if(!empty($paginas)){
                        $videos = $this->foro->filtroVideosFecha($paginas['Home']);
                        $videos = $this->formatearVideos($videos);
                        $datos = array(
                            'grupos' => $grupos,
                            'videos' => $videos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'total_paginas' => $paginas['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina
                        );
                    }else{
                        $bandera = false;
                    }
                    break;
                case "visitados":
                    $contador = $this->foro->getTotalVideos();
                    $paginas = $this->obtenerPaginas($pagina,$contador);
                    if(!empty($paginas)){
                        $videos = $this->foro->filtroVideosVistas($paginas['Home']);
                        $videos = $this->formatearVideos($videos);
                        $datos = array(
                            'grupos' => $grupos,
                            'videos' => $videos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'total_paginas' => $paginas['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina

                        );
                    }else{
                        $bandera = false;
                    }
                    break;
                case "comentados":
                    $contador = $this->foro->getTotalVideos();
                    $paginas = $this->obtenerPaginas($pagina,$contador);
                    if(!empty($paginas)){
                        $videos = $this->foro->filtroVideosComentarios($paginas['Home']);
                        $videos = $this->formatearVideos($videos);
                        $datos = array(
                            'grupos' => $grupos,
                            'videos' => $videos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'total_paginas' => $paginas['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina
                        );
                    }else{
                        $bandera = false;
                    }
                    break;
                default :
                    $bandera = false;
                    $this->error_404();
            }
            if($bandera){
                $this->load->view('principal/novia/comunidad/Foro/filtro_videos',$datos);
            }
        }
    }
    
    public function grupos(){
        $datos = "";
        $grupos = $this->comunidad->getGrupos();
        $visitas_perfil = $this->formatearVisitas();
        $usuarios_boda = $this->formatearCoincidencia();
        $grupos_miembro = $this->comunidad->getGruposMiembro();
        $datos = array(
            'grupos' => $grupos,
            'visitas_perfil' => $visitas_perfil,
            'usuario_boda' => $usuarios_boda,
            'grupos_miembro' => $grupos_miembro
        );
        $this->load->view('principal/novia/comunidad/Foro/foro_grupos',$datos);
    }
    
    public function usuarios($ordenamiento = "",$pagina = 1){
        if(!empty($ordenamiento)){
            $grupos = $this->comunidad->getGrupos();
            $visitas_perfil = $this->formatearVisitas();
            $usuarios_boda = $this->formatearCoincidencia();
            $grupos_miembro = $this->comunidad->getGruposMiembro();
            $usuarios = $this->filtros($ordenamiento,$pagina);
            if(!empty($usuarios['miembros'])){
                $usuarios['miembros'] = $this->formatearMiembros($usuarios['miembros']);
            }else{
                $usuarios['miembros'] = "";
            }
            if(!empty($usuarios['pagina']) && $usuarios['pagina' ] != 'error'){
                $datos = array(
                    'grupos' => $grupos,
                    'visitas_perfil' => $visitas_perfil,
                    'usuario_boda' => $usuarios_boda,
                    'grupos_miembro' => $grupos_miembro,
                    'ordenamiento' => $ordenamiento,
                    'usuarios' => $usuarios['miembros'],
                    'total_paginas' => $usuarios['total_paginas'],
                    'contador' => $usuarios['contador'],
                    'pagina' => $pagina,
                    'tipo' => $usuarios['tipo']
                );
                $this->load->view('principal/novia/comunidad/Foro/foro_usuarios',$datos);
            }else{
                $this->load->view('errors/html/error_404');
            }
        }else{
            $this->load->view('errors/html/error_404');
        }
    }
    
    public function validarMiembros($color = "Color",$temporada = "Temporada",$estilo = "Estilo",$estado = "Estado",$fecha = "fecha_boda",$pagina = 1){
        $color = str_replace("-", "_", $color);
        $ordenamiento = "$color-$temporada-$estilo-$estado-$fecha";
        redirect("novios/comunidad/forum/usuarios/$ordenamiento/$pagina");
    }
    
    public function validarFecha($fecha){
        $token = explode("_", $fecha);
        if(count($token) == 3){
            $dia = $token[0];
            $mes = $token[1];
            $anio = $token[2];
            if($anio % 4 == 0){
                if($mes == 02){
                    return $dia >= 1 && $dia <= 29;
                }
            }
            if($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11){
                return $dia >= 1 && $dia <= 30;
            }else{
                if($mes == 1 || $mes == 3 || $mes == 5 || $mes == 7 || $mes == 8 || $mes == 10 || $mes == 12){
                    return $dia >= 1 && $dia <= 31;
                }
            }
            $this->load->view('errors/html/error_404');
        }
        $this->load->view('errors/html/error_404');
    }
    
    public function filtros($ordenamiento,$pagina){
        $token = explode("-", $ordenamiento);
        $bandera = true;
        if(count($token) == 5){
            $token[3] = str_replace("_", " ", $token[3]);
            if(!empty($token[4]) && $token[4] != "fecha_boda"){
                if($this->validarFecha($token[4])){
                    $token[4] = str_replace("_", "/", $token[4]);
                }else{
                    $bandera = false;
                }
            }else if(empty ($token[4])){
                $bandera = false;
            }
            $color = $token[0];
            $temporada = $token[1];
            $estilo = $token[2];
            $estado = $token[3];
            $fecha = $token[4];
            if($bandera){
                if($color != "color" && $color != "" && $temporada != "temporada" && $temporada != "" && $estilo != "estilo" && $estilo != "" && $estado != "estado" && $estado != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro1($token,$pagina);
                }else if($color != "color" && $color != "" && $temporada != "temporada" && $temporada != "" && $estilo != "estilo" && $estilo != "" && $estado != "estado" && $estado != ""){
                    $result = $this->foro->filtro2($color,$temporada,$estilo,$estado,$pagina);
                }else if($color != "color" && $color != "" && $temporada != "temporada" && $temporada != "" && $estilo != "estilo" && $estilo != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro3($color,$temporada,$estilo,$fecha,$pagina);
                }else if($color != "color" && $color != "" && $estilo != "estilo" && $estilo != "" && $estado != "estado" && $estado != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro4($color,$estilo,$estado,$fecha,$pagina);
                }else if($color != "color" && $color != "" && $temporada != "temporada" && $temporada != "" && $estado != "estado" && $estado != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro5($color,$temporada,$estado,$fecha,$pagina);
                }else if($temporada != "temporada" && $temporada != "" && $estilo != "estilo" && $estilo != "" && $estado != "estado" && $estado != "" && $fecha != "fecha_boda" && $fecha != ""){    
                    $result = $this->foro->filtro6($temporada,$estilo,$estado,$fecha,$pagina);
                }else if($color != "color" && $color != "" && $temporada != "temporada" && $temporada != "" && $estilo != "estilo" && $estilo != ""){
                    $result = $this->foro->filtro7($color,$temporada,$estilo,$pagina);
                }else if($color != "color" && $color != "" && $temporada != "temporada" && $temporada != "" && $estado != "estado" && $estado != ""){ 
                    $result = $this->foro->filtro8($color,$temporada,$estado,$pagina);
                }else if($color != "color" && $color != "" && $temporada != "temporada" && $temporada != "" && $fecha != "fecha_boda" && $fecha != ""){    
                    $result = $this->foro->filtro9($color,$temporada,$fecha,$pagina);
                }else if($color != "color" && $color != "" && $estilo != "estilo" && $estilo != "" && $estado != "estado" && $estado != ""){
                    $result = $this->foro->filtro10($color,$estilo,$estado,$pagina);
                }else if($color != "color" && $color != "" && $estilo != "estilo" && $estilo != "" && $fecha != "fecha_boda" && $fecha != ""){    
                    $result = $this->foro->filtro11($color,$estilo,$fecha,$pagina);
                }else if($temporada != "temporada" && $temporada != "" && $estilo != "estilo" && $estilo != "" && $estado != "estado" && $estilo != ""){
                    $result = $this->foro->filtro12($temporada,$estilo,$estado,$pagina);
                }else if($temporada != "temporada" && $temporada != "" && $estilo != "estilo" && $estilo != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro13($temporada,$estilo,$fecha,$pagina);
                }else if($estilo != "estilo" && $estilo != "" && $estado != "estado" && $estado != "" && $fecha != "fecha_boda" && $fecha != ""){    
                    $result = $this->foro->filtro14($estilo,$estado,$fecha,$pagina);
                }else if($color != "color" && $color != "" && $temporada != "temporada" && $temporada != ""){
                    $result = $this->foro->filtro15($color,$temporada,$pagina);
                }else if($color != "color" && $color != "" && $estilo != "estilo" && $estilo != ""){
                    $result = $this->foro->filtro16($color,$estilo,$pagina);
                }else if($color != "color" && $color != "" && $estado != "estado" && $estado != ""){
                    $result = $this->foro->filtro17($color,$estado,$pagina);
                }else if($color != "color" && $color != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro18($color,$fecha,$pagina);
                }else if($temporada != "temporada" && $temporada != "" && $estilo != "estilo" && $estilo != ""){
                    $result = $this->foro->filtro19($temporada,$estilo,$pagina);
                }else if($temporada != "temporada" && $temporada != "" && $estado != "estado" && $estado != ""){
                    $result = $this->foro->filtro20($temporada,$estado,$pagina);
                }else if($temporada != "temporada" && $temporada != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro21($temporada,$fecha,$pagina);
                }else if($estilo != "estilo" && $estilo != "" && $estado != "estado" && $estado != ""){
                    $result = $this->foro->filtro22($estilo,$estado,$pagina);
                }else if($estilo != "estilo" && $estilo != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro23($estilo,$fecha,$pagina);
                }else if($estado != "estado" && $estado != "" && $fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro24($estado,$fecha,$pagina);
                }else if($color != "color" && $color != ""){
                    $result = $this->foro->filtro25($color,$pagina);
                }else if($temporada != "temporada" && $temporada != ""){
                    $result = $this->foro->filtro26($temporada,$pagina);
                }else if($estilo != "estilo" && $estilo != ""){
                    $result = $this->foro->filtro27($estilo,$pagina);
                }else if($estado != "estado" && $estado != ""){
                    $result = $this->foro->filtro28($estado,$pagina);
                }else if($fecha != "fecha_boda" && $fecha != ""){
                    $result = $this->foro->filtro29($fecha,$pagina);
                }else if($color == "color" && $temporada == "temporada" && $estilo == "estilo" && $estado == "estado" && $fecha == "fecha_boda"){
                    $result = $this->foro->filtro30($pagina);
                }
                $datos = "";
                if(!empty($result) && $result['pagina'] != 'error'){
                    $datos = array(
                        'total_paginas' => $result['total_paginas'],
                        'contador' => $result['contador'],
                        'miembros' => $result['miembros'],
                        'pagina' => $result['pagina'],
                        'tipo' => 'validarMiembros'
                    );
                    return $datos;
                }else{
                    $datos = "";
                    $datos = array('pagina' => $result['pagina']);
                    return $datos;
                }
            }
        }else if(count($token) == 1){
            $usuario = "";
            $token = explode(".", $ordenamiento);
            if(count($token) > 1){
                for($i = 0; $i < count($token); $i++){
                    $usuario = $usuario." ".$token[$i];
                }
            }else{
                $usuario = $ordenamiento;
            }
            $result = $this->foro->buscarMiembro($usuario,$pagina);
            if($result && $result['pagina'] != 'error'){
                $datos = array(
                    'total_paginas' => $result['total_paginas'],
                    'contador' => $result['contador'],
                    'miembros' => $result['miembros'],
                    'pagina' => $result['pagina'],
                    'tipo' => 'buscarMiembro'
                );
                return $datos;
            }else{
                $datos = "";
                $datos = array('pagina' => $result['pagina']);
                return $datos;
            }
        }else{
            $this->load->view('errors/html/error_404');
        }
    }
    
    public  function formatearMiembros($usuarios){
        $usuarios2 = "";
        $i = 0;
        if($usuarios){
            foreach ($usuarios as $usuario){
                $amistad = $this->foro->getAmistad($usuario->id_usuario);
                if(!empty($amistad) && $amistad->amistad == 1){
                    $usuario->estado_usuario = "Eliminar Amistad";
                }else if(!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $usuario->id_usuario){
                    $usuario->estado_usuario = "Aceptar Solicitud";
                }else if(!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_confirmacion == $usuario->id_usuario){
                    $usuario->estado_usuario = "Solicitud Enviada";
                }else{
                    $usuario->estado_usuario = "Enviar Solicitud";
                }
                $usuario->mensajes = $this->foro->getTotalMensajes($usuario->id_usuario);
                $usuario->debates = $this->foro->getTotalDebates($usuario->id_usuario);
                $usuario->fotos = $this->foro->getTotalFotos($usuario->id_usuario);
                $usuario->amigos = $this->foro->getTotalAmigos($usuario->id_usuario);
                $usuario->url_debates_participacion = "participacion-$usuario->id_usuario";
                $usuario->url_debates = "misdebates-$usuario->id_usuario";
                $usuario->url_fotos = "fotos-$usuario->id_usuario";
                $usuario->url_amigos = "amigos-$usuario->id_usuario";
                if(!empty($usuario->foto)){
                    $usuario->url_foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$usuario->id_usuario";
                }else{
                    $usuario->url_foto_usuario =  base_url().'dist/img/blog/perfil.png';
                }
                $usuario->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$usuario->id_usuario";
                $usuario->id_usuario = $usuario->id_usuario;
                $usuario->url_agregar_amigo = "agregar-$usuario->id_usuario";
                $usuario->agregar = "";
                $usuario->lugar = "";
                if(!empty($usuario->ciudad_boda) && !empty($usuario->estado_boda)){
                    $usuario->lugar = "$usuario->ciudad_boda, $usuario->estado_boda";
                }else if(!empty($usuario->ciudad)){
                    $usuario->lugar = $usuario->ciudad_boda;
                }else if(!empty($usuario->estado_boda)){
                    $usuario->lugar = $usuario->estado_boda;
                }
                $usuario->fecha_boda = new DateTime($usuario->fecha_boda);
                $mes = $this->formatearMes($usuario->fecha_boda);
                $anio = $usuario->fecha_boda->format('Y');
                $fecha_amistad = "Me caso en $mes de $anio";
                $usuario->fecha_creacion = $fecha_amistad;
                $usuarios2[$i] = $usuario;
                $i++;
            } 
        }
        return $usuarios2;
    }
    
    public function buscarMiembro(){
        $pagina = $this->input->get("pagina",TRUE);
        $usuario = $this->input->get("nombre",TRUE);
        if(!empty($usuario) && $usuario != ""){
            $usuario = str_replace(" ", ".", $usuario);
            redirect("novios/comunidad/forum/usuarios/$usuario/$pagina");
        }
        $usuario = "color-temporada-estilo-estado-fecha_boda";
        redirect("novios/comunidad/forum/usuarios/$usuario/$pagina");
    }
    
    public function buscar(){
        if($_GET){
            $buscar = $this->input->get("buscar", TRUE);
            $pagina = $this->input->get("pagina", TRUE);
            if(!empty($pagina) && isset($_GET['buscar'])){
                $contador = $this->foro->num_debates_encontrados($buscar);
                $contador = $this->obtenerPaginas($pagina, $contador);
                if($contador){
                    $debates = $this->foro->buscar($buscar,$contador['Home']);
                    $debates2 = "";
                    if($debates){
                        $i = 0;
                        foreach ($debates as $debate){
                            if(empty($debate->mime)){
                                $debate->foto_usuario = base_url().'dist/img/blog/perfil.png';
                            }else{
                                $debate->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$debate->id_usuario";
                            }
                            $debate->fecha_creacion = new DateTime($debate->fecha_creacion);
                            $debate->fecha_creacion = "El ".$debate->fecha_creacion->format('d/m/Y');
                            $debate->debate = substr($debate->debate, 0, 255)." ...";
                            $debate->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$debate->id_usuario";
                            $debates2[$i] = $debate;
                            $i++;
                        }
                    }
                    $grupos = $this->comunidad->getGrupos();
                    $visitas_perfil = $this->formatearVisitas();
                    $usuarios_boda = $this->formatearCoincidencia();
                    $grupos_miembro = $this->comunidad->getGruposMiembro();
                    $datos = array(
                        'grupos' => $grupos,
                        'visitas_perfil' => $visitas_perfil,
                        'usuarios_boda' => $usuarios_boda,
                        'grupos_miembro' => $grupos_miembro,
                        'total_paginas' => $contador['total_paginas'],
                        'pagina' => $pagina,
                        'debates' => $debates2,
                        'buscar' => $buscar
                    );
                    $this->load->view('principal/novia/comunidad/Foro/buscar_debates',$datos);
                }
            }else{
                $this->load->view('errors/html/error_404');
            }
        }
    }
    
    public function error_404(){
        $data["pais"] =  "Mexico" ;
        $data["tipos"] = $this->tipo_proveedor->getGrupos();
        foreach ($data["tipos"] as $key => $value) {
            $value->tipos = $this->tipo_proveedor->getGrupo($value->nombre);
        }
        $this->load->view('errors/html/error_404.php',$data);
    }
}

