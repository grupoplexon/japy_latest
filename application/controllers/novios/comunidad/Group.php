<?php

class Group extends CI_Controller
{
    public $input;

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Comunidad/Comunidad_model", "comunidad");
        $this->load->model("Comunidad/Imagen_model", "imagen");
        $this->load->model('Comunidad/Grupo_model', 'grupo');
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/";
        $this->load->helper('formats_helper');

    }

    public function grupo($id_grupo = 0, $seccion = 1, $ordenamiento = 1, $pagina = 1)
    {
        if ($id_grupo > 48 || $id_grupo < 1) {
            $this->error_404();
        }
        if ($id_grupo != 0 && $seccion != 1) {
            $grupos = $this->comunidad->getGrupos();
            $visitas_perfil = $this->formatearVisitas();
            $usuarios_boda = $this->formatearCoincidencia();
            $grupos_miembro = $this->comunidad->getGruposMiembro();
            $grupo = $this->grupo->getNombreGrupo($id_grupo);
            $nombre_grupo = $grupo->nombre;
            $imagen_grupo = $grupo->imagen;
            $result = $this->grupo->validarUnionGrupo($id_grupo);
            if (!isset($result->activo) || $result->activo == 0) {
                $unirse = "Unirse Al Grupo";
            } else {
                $unirse = "Salir Del Grupo";
            }
            $datos = "";
            switch ($seccion) {
                case "todo":
                    $todo = $this->todo($id_grupo);
                    if ($todo) {
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            'unirse' => $unirse,
                            'debates_recientes' => $todo['debates'],
                            'fotos_recientes' => $todo['fotos'],
                            'videos_recientes' => $todo['videos'],
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                case "fotos":
                    $fotos = $this->fotos($id_grupo, $ordenamiento, $pagina);
                    if ($fotos) {
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            'unirse' => $unirse,
                            'titulo' => $fotos['titulo'],
                            'fotos' => $fotos['fotos'],
                            'total_paginas' => $fotos['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                case "debates":
                    $debates = $this->debates($id_grupo, $ordenamiento, $pagina);
                    if ($debates) {
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            'unirse' => $unirse,
                            'debates' => $debates['debates'],
                            'total_paginas' => $debates['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                case "videos":
                    $videos = $this->videos($id_grupo, $ordenamiento, $pagina);
                    if ($videos) {
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            'unirse' => $unirse,
                            'titulo' => $videos['titulo'],
                            'videos' => $videos['videos'],
                            'total_paginas' => $videos['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                case "miembros":
                    $miembros = $this->miembros($id_grupo, $ordenamiento, $pagina);
                    if (empty($miembros['contador'])) {
                        $miembros['contador'] = 0;
                    }
                    if (!empty($miembros['pagina']) && $miembros['pagina'] != 'error') {
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            'unirse' => $unirse,
                            'miembros' => $miembros['miembros'],
                            'total_paginas' => $miembros['total_paginas'],
                            'contador' => $miembros['contador'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'tipo' => $miembros['tipo']
                        );
                    } else {
                        $this->error_404();
                    }
                    break;
                case "nuevos_integrantes":
                    $nuevos_integrantes = $this->nuevos_integrantes($id_grupo, $ordenamiento, $pagina);
                    if ($nuevos_integrantes) {
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            'unirse' => $unirse,
                            'nuevos_integrantes' => $nuevos_integrantes['nuevos_integrantes'],
                            'total_paginas' => $nuevos_integrantes['total_paginas'],
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                case "proximas_bodas":
                    $proximas_bodas = $this->proximas_bodas($id_grupo, $ordenamiento, $pagina);
                    if ($proximas_bodas) {
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            'unirse' => $unirse,
                            'proximas_bodas' => $proximas_bodas['proximas_bodas'],
                            'total_paginas' => $proximas_bodas['total_paginas'],
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                default :
                    $this->error_404();
            }
            if (!empty($datos)) {
                $this->load->view('principal/novia/comunidad/grupos.php', $datos);
            }
        } else {
            $this->error_404();
        }
    }

    public function formatearVisitas()
    {
        $visitas_perfil = $this->comunidad->getVisitas();
        $visitas_perfil2 = "";
        $i = 0;
        if ($visitas_perfil) {
            foreach ($visitas_perfil as $visita) {
                $visita->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$visita->id_usuario";
                if (!empty($visita->mime)) {
                    $visita->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$visita->id_usuario";
                } else {
                    $visita->foto_usuario = base_url() . "dist/img/blog/perfil.png";
                }
                $usuario = $this->session->userdata('id_usuario');
                $visita->todos = base_url() . "index.php/novios/comunidad/perfil/visitas/$usuario";
                $visitas_perfil2[$i] = $visita;
                $i++;
            }
        }
        return $visitas_perfil2;
    }

    public function formatearCoincidencia()
    {
        $usuarios_boda = $this->comunidad->usuariosBoda();
        if ($usuarios_boda) {
            if (!empty($usuarios_boda->fecha_boda)) {
                $usuarios_boda->fecha_boda = new DateTime($usuarios_boda->fecha_boda);
                $mes = $this->formatearMes($usuarios_boda->fecha_boda);
                $anio = $usuarios_boda->fecha_boda->format('Y');
                $dia = $usuarios_boda->fecha_boda->format('d');
                $usuarios_boda->fecha_boda = "$dia de $mes de $anio";
            }
            if (!empty($usuarios_boda->mime)) {
                $usuarios_boda->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/" . $this->session->userdata('id_usuario');
            } else {
                $usuarios_boda->foto_usuario = base_url() . "dist/img/blog/perfil.png";
            }
            return $usuarios_boda;
        }
    }

    public function todo($id_grupo)
    {
        $debates2 = "";
        $fotos = "";
        $videos = "";
        $debates = $this->grupo->getDebatesGrupo($id_grupo);
        $fotos = $this->grupo->getFotosRecientesGrupo($id_grupo);
        $videos = $this->grupo->getVideosRecientesGrupo($id_grupo);
        $debates = $this->formatearFotoUsuario($debates);
        $fotos = $this->formatearFotoUsuario($fotos);
        $videos = $this->formatearFotoUsuario($videos);
        $datos = array(
            'debates' => $debates,
            'fotos' => $fotos,
            'videos' => $videos
        );
        return $datos;
    }

    public function formatearFotoUsuario($datos)
    {
        $datos2 = "";
        $i = 0;
        if (!empty($datos)) {
            foreach ($datos as $dato) {
                if (empty($dato->mime)) {
                    $dato->foto_usuario = base_url() . "dist/img/blog/perfil.png";
                } else {
                    $dato->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/$dato->id_usuario";
                }
                $datos2[$i] = $dato;
                $i++;
            }
            return $datos2;
        }
        return $datos;
    }

    public function fotos($id_grupo, $ordenamiento, $pagina)
    {
        $fotos = "";
        $inicio = 0;
        $titulo = "";
        $contador = $this->grupo->totalFotos($id_grupo);
        if ($ordenamiento > 1) {
            $pagina = $ordenamiento;
        }
        $total_paginas = (int)($contador / 16);
        if ($total_paginas == 0) {
            $total_paginas = 1;
        }
        if ($ordenamiento != 1 && $ordenamiento != "comentarios" && $ordenamiento != "visitas" && $ordenamiento != "fecha") {
            $pagina = $total_paginas + 1;
        }
        if ($pagina <= $total_paginas && $pagina > 0) {
            $inicio = ($pagina - 1) * 16;
            if ($ordenamiento == 1 || $ordenamiento == "comentarios") {
                $fotos = $this->grupo->getComentadas($id_grupo, $inicio);
                $fotos = $this->formatearFotoUsuario($fotos);
                $titulo = "Fotos Mas Comentadas";
            } else if ($ordenamiento == "fecha") {
                $fotos = $this->grupo->getRecientes($id_grupo, $inicio);
                $fotos = $this->formatearFotoUsuario($fotos);
                $titulo = "Fotos Mas Recientes";
            } else if ($ordenamiento == "visitas") {
                $fotos = $this->grupo->getVisitadas($id_grupo, $inicio);
                $fotos = $this->formatearFotoUsuario($fotos);
                $titulo = "Fotos Mas Visitadas";
            } else {
                $this->load->view('errors/html/error_404');
            }
            $datos = array(
                'titulo' => $titulo,
                'fotos' => $fotos,
                'total_paginas' => $total_paginas
            );
            return $datos;
        } else {
            $this->load->view('errors/html/error_404');
        }
    }

    public function debates($id_grupo, $ordenamiento, $pagina)
    {
        $debates = "";
        $inicio = 0;
        $titulo = "";
        $contador = $this->grupo->totalDebatesUltimoMensaje($id_grupo);
        if ($ordenamiento > 1) {
            $pagina = $ordenamiento;
        }
        $total_paginas = (int)($contador / 16);
        if ($total_paginas == 0) {
            $total_paginas = 1;
        }
        if ($ordenamiento != 1 && $ordenamiento != "mensajes" && $ordenamiento != "visitas" && $ordenamiento != "ultimo_mensaje" && $ordenamiento != "fecha" && $ordenamiento != "sin_respuesta") {
            $pagina = $total_paginas + 1;
        }
        if ($pagina <= $total_paginas && $pagina > 0) {
            $inicio = ($pagina - 1) * 16;
            switch ($ordenamiento) {
                case 1:
                    $debates = $this->grupo->getUltimoMensaje($id_grupo, $inicio);
                    break;
                case "mensajes":
                    $debates = $this->grupo->getMensaje($id_grupo, $inicio);
                    $debates = $this->formatearFotoUsuario($debates);
                    break;
                case "visitas":
                    $debates = $this->grupo->getVisitasDebate($id_grupo, $inicio);
                    $debates = $this->formatearFotoUsuario($debates);
                    break;
                case "ultimo_mensaje":
                    $debates = $this->grupo->getUltimoMensaje($id_grupo, $inicio);
                    break;
                case "fecha":
                    $debates = $this->grupo->getDebatesRecientes($id_grupo, $inicio);
                    $debates = $this->formatearFotoUsuario($debates);
                    break;
                case "sin_respuesta":
                    $debates = $this->grupo->getDebateSinRespuesta($id_grupo, $inicio);
                    $debates = $this->formatearFotoUsuario($debates);
                    break;
            }
            $datos = array(
                'debates' => $debates,
                'total_paginas' => $total_paginas
            );
            return $datos;
        } else {
            $this->load->view('errors/html/error_404');
        }
    }

    public function videos($id_grupo, $ordenamiento, $pagina)
    {
        $videos = "";
        $inicio = 0;
        $titulo = "";
        $contador = $this->grupo->totalVideos($id_grupo);
        if ($ordenamiento > 1) {
            $pagina = $ordenamiento;
        }
        $total_paginas = (int)($contador / 16);
        if ($total_paginas == 0) {
            $total_paginas = 1;
        }
        if ($ordenamiento != 1 && $ordenamiento != "comentarios" && $ordenamiento != "visitas" && $ordenamiento != "fecha") {
            $pagina = $total_paginas + 1;
        }
        if ($pagina <= $total_paginas && $pagina > 0) {
            $inicio = ($pagina - 1) * 16;
            if ($ordenamiento == 1 || $ordenamiento == "comentarios") {
                $videos = $this->grupo->getComentados($id_grupo, $inicio);
                $videos = $this->formatearFotoUsuario($videos);
                $titulo = "Videos Mas Comentados";
            } else if ($ordenamiento == "fecha") {
                $videos = $this->grupo->getVideosRecientes($id_grupo, $inicio);
                $videos = $this->formatearFotoUsuario($videos);
                $titulo = "Videos Mas Recientes";
            } else if ($ordenamiento == "visitas") {
                $videos = $this->grupo->getVisitados($id_grupo, $inicio);
                $videos = $this->formatearFotoUsuario($videos);
                $titulo = "Videos Mas Visitados";
            } else {
                $this->load->view('errors/html/error_404');
            }
            $datos = array(
                'titulo' => $titulo,
                'videos' => $videos,
                'total_paginas' => $total_paginas
            );
            return $datos;
        } else {
            $this->load->view('errors/html/error_404');
        }
    }

    public function miembros($id_grupo, $ordenamiento, $pagina)
    {
        $miembros = $this->getMiembrosFiltro($id_grupo, $ordenamiento, $pagina);
        $i = 0;
        $miembros2 = "";
        if (!empty($miembros['miembros']) && $miembros['pagina'] != 'error') {
            foreach ($miembros['miembros'] as $miembro) {
                $fecha_creacion = new DateTime($miembro->fecha_creacion);
                $mes = $this->formatearMes($fecha_creacion);
                $anio = $fecha_creacion->format("Y");
                $dia = $fecha_creacion->format("d");
                $miembro->fecha_creacion = "Fecha de Registro: " . $dia . " de " . $mes . " de " . $anio;
                $fecha_boda = new DateTime($miembro->fecha_boda);
                $mes = $this->formatearMes($fecha_boda);
                $anio = $fecha_boda->format("Y");
                $miembro->fecha_boda = "Fecha boda: " . $mes . " de " . $anio;
                if (!empty($miembro->ciudad_boda) && !empty($miembro->estado_boda)) {
                    $miembro->lugar_boda = $miembro->ciudad_boda . ', ' . $miembro->estado_boda;
                } else if (!empty ($miembro->ciudad_boda)) {
                    $miembro->lugar_boda = $miembro->ciudad_boda;
                } else if (!empty($miembro->estado_boda)) {
                    $miembro->lugar_boda = $miembro->estado_boda;
                } else {
                    $miembro->lugar_boda = "Lugar: Sin Eleccion";
                }
                if (!empty($miembro->foto)) {
                    $miembro->url_foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $miembro->id_usuario;
                } else {
                    $miembro->url_foto_usuario = base_url() . "dist/img/blog/perfil.png";
                }
                $amistad = $this->grupo->getAmistad($miembro->id_usuario);
                if (!empty($amistad) && $amistad->amistad == 1) {
                    $miembro->estado_usuario = "Eliminar Amistad";
                } else if (!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $miembro->id_usuario) {
                    $miembro->estado_usuario = "Aceptar Solicitud";
                } else if (!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_confirmacion == $miembro->id_usuario) {
                    $miembro->estado_usuario = "Solicitud Enviada";
                } else {
                    $miembro->estado_usuario = "Enviar Solicitud";
                }
                $miembro->url_usuario = base_url() . "index.php/novios/comunidad/Perfil/usuario/" . $miembro->id_usuario;
                $miembro->url_debates = "misdebates-" . $miembro->id_usuario;
                $miembro->url_debates_participacion = "participacion-" . $miembro->id_usuario;
                $miembro->url_fotos = "fotos-" . $miembro->id_usuario;
                $miembro->url_amigos = "amigos-" . $miembro->id_usuario;
                $miembro->url_agregar_amigo = "agregar-" . $miembro->id_usuario;
                $miembro->url_comentario = base_url() . "index.php/comunidad/Perfil/comentario/" . $miembro->id_usuario;
                $miembro->num_debates = $this->grupo->getTotalDebates($miembro->id_usuario);
                $miembro->num_mensajes = $this->grupo->getTotalMensajes($miembro->id_usuario);
                $miembro->num_fotos = $this->grupo->getTotalFotosUsuario($miembro->id_usuario);
                $miembro->num_amigos = $this->grupo->getTotalAmigos($miembro->id_usuario);
                $miembros2[$i] = $miembro;
                $i++;
            }
            $datos = array(
                'miembros' => $miembros2,
                'total_paginas' => $miembros['total_paginas'],
                'contador' => $miembros['contador'],
                'pagina' => $pagina,
                'tipo' => $miembros['tipo']
            );
            return $datos;
        }
        if ($miembros['pagina'] == 'error') {
            $datos = array('pagina' => 'error');
        } else {
            $datos = array(
                'miembros' => "",
                'total_paginas' => "",
                'pagina' => $pagina,
                'tipo' => $miembros['tipo']
            );
        }
        return $datos;
    }

    public function validarMiembros($id_grupo = 0, $color = "Color", $temporada = "Temporada", $estilo = "Estilo", $estado = "Estado", $fecha = "Fecha Boda", $pagina = 1)
    {
        $color = str_replace("-", "_", $color);
        $ordenamiento = "$color-$temporada-$estilo-$estado-$fecha";
        redirect("novios/comunidad/Group/grupo/$id_grupo/miembros/$ordenamiento/$pagina");
    }

    public function validarFecha($fecha)
    {
        $token = explode("_", $fecha);
        if (count($token) == 3) {
            $dia = $token[0];
            $mes = $token[1];
            $anio = $token[2];
            if ($anio % 4 == 0) {
                if ($mes == 02) {
                    return $dia >= 1 && $dia <= 29;
                }
            }
            if ($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11) {
                return $dia >= 1 && $dia <= 30;
            } else {
                if ($mes == 1 || $mes == 3 || $mes == 5 || $mes == 7 || $mes == 8 || $mes == 10 || $mes == 12) {
                    return $dia >= 1 && $dia <= 31;
                }
            }
            return false;
        }
        return false;
    }

    public function getMiembrosFiltro($id_grupo, $ordenamiento, $pagina)
    {
        $token = explode("-", $ordenamiento);
        $result = "";
        $bandera = true;
        if (count($token) == 5) {
            $token[3] = str_replace("_", " ", $token[3]);
            if (!empty($token[4]) && $token[4] != "fecha_boda") {
                if ($this->validarFecha($token[4])) {
                    $token[4] = str_replace("_", "/", $token[4]);
                } else {
                    $bandera = false;
                }
            } else if (empty ($token[4])) {
                $bandera = false;
            }
            if ($bandera) {
                if ($token[0] != "color" && $token[0] != 1 && $token[1] != "temporada" && $token[1] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[3] != "estado" && $token[3] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro1($id_grupo, $token, $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[1] != "temporada" && $token[1] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[3] != "estado" && $token[3] != 1) {
                    $result = $this->grupo->filtro2($id_grupo, $token[0], $token[1], $token[2], $token[3], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[1] != "temporada" && $token[1] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro3($id_grupo, $token[0], $token[1], $token[2], $token[4], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[3] != "estado" && $token[3] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro4($id_grupo, $token[0], $token[2], $token[3], $token[4], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[1] != "temporada" && $token[1] != 1 && $token[3] != "estado" && $token[3] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro5($id_grupo, $token[0], $token[1], $token[3], $token[4], $pagina);
                } else if ($token[1] != "temporada" && $token[1] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[3] != "estado" && $token[3] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro6($id_grupo, $token[1], $token[2], $token[3], $token[4], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[1] != "temporada" && $token[1] != 1 && $token[2] != "estilo" && $token[2] != 1) {
                    $result = $this->grupo->filtro7($id_grupo, $token[0], $token[1], $token[2], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[1] != "temporada" && $token[1] != 1 && $token[3] != "estado" && $token[3] != 1) {
                    $result = $this->grupo->filtro8($id_grupo, $token[0], $token[1], $token[3], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[1] != "temporada" && $token[1] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro9($id_grupo, $token[0], $token[1], $token[4], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[3] != "estado" && $token[3] != 1) {
                    $result = $this->grupo->filtro10($id_grupo, $token[0], $token[2], $token[3], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro11($id_grupo, $token[0], $token[2], $token[4], $pagina);
                } else if ($token[1] != "temporada" && $token[1] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[3] != "estado" && $token[2] != 1) {
                    $result = $this->grupo->filtro12($id_grupo, $token[1], $token[2], $token[3], $pagina);
                } else if ($token[1] != "temporada" && $token[1] != 1 && $token[2] != "estilo" && $token[2] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro13($id_grupo, $token[1], $token[2], $token[4], $pagina);
                } else if ($token[2] != "estilo" && $token[2] != 1 && $token[3] != "estado" && $token[3] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro14($id_grupo, $token[2], $token[3], $token[4], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[1] != "temporada" && $token[1] != 1) {
                    $result = $this->grupo->filtro15($id_grupo, $token[0], $token[1], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[2] != "estilo" && $token[2] != 1) {
                    $result = $this->grupo->filtro16($id_grupo, $token[0], $token[2], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[3] != "estado" && $token[3] != 1) {
                    $result = $this->grupo->filtro17($id_grupo, $token[0], $token[3], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro18($id_grupo, $token[0], $token[4], $pagina);
                } else if ($token[1] != "temporada" && $token[1] != 1 && $token[2] != "estilo" && $token[2] != 1) {
                    $result = $this->grupo->filtro19($id_grupo, $token[1], $token[2], $pagina);
                } else if ($token[1] != "temporada" && $token[1] != 1 && $token[3] != "estado" && $token[3] != 1) {
                    $result = $this->grupo->filtro20($id_grupo, $token[1], $token[3], $pagina);
                } else if ($token[1] != "temporada" && $token[1] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro21($id_grupo, $token[1], $token[4], $pagina);
                } else if ($token[2] != "estilo" && $token[2] != 1 && $token[3] != "estado" && $token[3] != 1) {
                    $result = $this->grupo->filtro22($id_grupo, $token[2], $token[3], $pagina);
                } else if ($token[2] != "estilo" && $token[2] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro23($id_grupo, $token[2], $token[4], $pagina);
                } else if ($token[3] != "estado" && $token[3] != 1 && $token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro24($id_grupo, $token[3], $token[4], $pagina);
                } else if ($token[0] != "color" && $token[0] != 1) {
                    $result = $this->grupo->filtro25($id_grupo, $token[0], $pagina);
                } else if ($token[1] != "temporada" && $token[1] != 1) {
                    $result = $this->grupo->filtro26($id_grupo, $token[1], $pagina);
                } else if ($token[2] != "estilo" && $token[2] != 1) {
                    $result = $this->grupo->filtro27($id_grupo, $token[2], $pagina);
                } else if ($token[3] != "estado" && $token[3] != 1) {
                    $result = $this->grupo->filtro28($id_grupo, $token[3], $pagina);
                } else if ($token[4] != "fecha_boda" && $token[4] != 1) {
                    $result = $this->grupo->filtro29($id_grupo, $token[4], $pagina);
                } else {
                    $result = $this->grupo->filtro30($id_grupo, $pagina);
                }
                $datos = "";
                if (!empty($result) && $result['pagina'] != 'error') {
                    $datos = array(
                        'total_paginas' => $result['total_paginas'],
                        'contador' => $result['contador'],
                        'miembros' => $result['miembros'],
                        'pagina' => $result['pagina'],
                        'tipo' => 'validarMiembros'
                    );
                    return $datos;
                } else {
                    $datos = "";
                    $datos = array('pagina' => $result['pagina']);
                    return $datos;
                }
            }
        } else if (count($token) == 1) {
            $usuario = "";
            $token = explode(".", $ordenamiento);
            if (count($token) > 1) {
                for ($i = 0; $i < count($token); $i++) {
                    $usuario = $usuario . " " . $token[$i];
                }
            } else {
                $usuario = $ordenamiento;
            }
            $result = $this->grupo->buscarMiembro($id_grupo, $usuario, $pagina);
            if ($result && $result['pagina'] != 'error') {
                $datos = array(
                    'total_paginas' => $result['total_paginas'],
                    'contador' => $result['contador'],
                    'miembros' => $result['miembros'],
                    'pagina' => $result['pagina'],
                    'tipo' => 'buscarMiembro'
                );
                return $datos;
            } else {
                $datos = "";
                $datos = array('pagina' => $result['pagina']);
                return $datos;
            }
        } else {
            $this->load->view('errors/html/error_404');
        }
    }

    public function buscarMiembro()
    {
        $id_grupo = $this->input->get("grupo", TRUE);
        $pagina = $this->input->get("pagina", TRUE);
        $usuario = $this->input->get("usuario", TRUE);
        if (!empty($usuario) && $usuario != "") {
            $usuario = str_replace(" ", ".", $usuario);
            redirect("novios/comunidad/Group/grupo/$id_grupo/miembros/$usuario/$pagina");
        }
        $usuario = "color-temporada-estilo-estado-fecha_boda";
        redirect("novios/comunidad/Group/grupo/$id_grupo/miembros/$usuario/$pagina");
    }

    public function nuevos_integrantes($id_grupo, $ordenamiento, $pagina)
    {
        $fecha_actual = date('Y');
        $fecha_actual = $fecha_actual . "-01-01";
        $contador = $this->grupo->totalUsuariosGrupo($id_grupo, $fecha_actual);
        $total_paginas = (int)($contador / 12);
        if ($total_paginas == 0) {
            $total_paginas = 1;
        }
        if ($ordenamiento > 1) {
            $pagina = $ordenamiento;
        } else if (!is_numeric($ordenamiento)) {
            $pagina = $total_paginas + 1;
        }
        if ($pagina <= $total_paginas && $pagina > 0) {
            $inicio = ($pagina - 1) * 12;
            $nuevos_integrantes = $this->grupo->nuevos_integrantes($id_grupo, $fecha_actual, $inicio);
            $i = 0;
            $integrantes = "";
            foreach ($nuevos_integrantes as $integrante) {
                $fecha_creacion = new DateTime($integrante->fecha_creacion);
                $mes = $this->formatearMes($fecha_creacion);
                $anio = $fecha_creacion->format("Y");
                $dia = $fecha_creacion->format("d");
                $integrante->fecha_creacion = "Fecha de Registro: " . $dia . " de " . $mes . " de " . $anio;
                $fecha_boda = new DateTime($integrante->fecha_boda);
                $mes = $this->formatearMes($fecha_boda);
                $anio = $fecha_boda->format("Y");
                $integrante->fecha_boda = "Fecha boda: " . $mes . " de " . $anio;
                if (!empty($integrante->ciudad_boda) && !empty($integrante->estado_boda)) {
                    $integrante->lugar_boda = $integrante->ciudad_boda . ', ' . $integrante->estado_boda;
                } else if (!empty ($integrante->ciudad_boda)) {
                    $integrante->lugar_boda = $integrante->ciudad_boda;
                } else if (!empty($integrante->estado_boda)) {
                    $integrante->lugar_boda = $integrante->estado_boda;
                } else {
                    $integrante->lugar_boda = "Lugar: Sin Eleccion";
                }
                if (!empty($integrante->foto)) {
                    $integrante->url_foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $integrante->id_usuario;
                } else {
                    $integrante->url_foto_usuario = base_url() . "dist/img/blog/perfil.png";
                }
                $amistad = $this->grupo->getAmistad($integrante->id_usuario);
                if (!empty($amistad) && $amistad->amistad == 1) {
                    $integrante->estado_usuario = "Eliminar Amistad";
                } else if (!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $integrante->id_usuario) {
                    $integrante->estado_usuario = "Aceptar Solicitud";
                } else if (!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_confirmacion == $integrante->id_usuario) {
                    $integrante->estado_usuario = "Solicitud Enviada";
                } else {
                    $integrante->estado_usuario = "Enviar Solicitud";
                }
                $integrante->url_usuario = base_url() . "index.php/novios/comunidad/Perfil/usuario/" . $integrante->id_usuario;
                $integrante->url_debates = "misdebates-" . $integrante->id_usuario;
                $integrante->url_debates_participacion = "participacion-" . $integrante->id_usuario;
                $integrante->url_fotos = "fotos-" . $integrante->id_usuario;
                $integrante->url_amigos = "amigos-" . $integrante->id_usuario;
                $integrante->url_agregar_amigo = "agregar-" . $integrante->id_usuario;
                $integrante->url_comentario = base_url() . "index.php/comunidad/Perfil/comentario/" . $integrante->id_usuario;
                $integrante->num_debates = $this->grupo->getTotalDebates($integrante->id_usuario);
                $integrante->num_mensajes = $this->grupo->getTotalMensajes($integrante->id_usuario);
                $integrante->num_fotos = $this->grupo->getTotalFotosUsuario($integrante->id_usuario);
                $integrante->num_amigos = $this->grupo->getTotalAmigos($integrante->id_usuario);
                $integrantes[$i] = $integrante;
                $i++;
            }
            $datos = array(
                'nuevos_integrantes' => $integrantes,
                'total_paginas' => $total_paginas
            );
            return $datos;
        } else {
            $this->load->view('errors/html/error_404');
        }
    }

    public function proximas_bodas($id_grupo = "", $ordenamiento, $pagina = "")
    {
        $fecha_actual = date('Y-m');
        $fecha_actual = $fecha_actual . "-01";
        $contador = $this->grupo->totalUsuariosBoda($id_grupo, $fecha_actual);
        $total_paginas = (int)($contador / 12);
        if ($total_paginas == 0) {
            $total_paginas = 1;
        }
        if ($ordenamiento > 1) {
            $pagina = $ordenamiento;
        } else if (!is_numeric($ordenamiento)) {
            $pagina = $total_paginas + 1;
        }
        if ($pagina <= $total_paginas && $pagina > 0) {
            $inicio = ($pagina - 1) * 12;
            $proximas_bodas = $this->grupo->proximas_bodas($id_grupo, $fecha_actual, $inicio);
            $i = 0;
            $bodas = "";
            foreach ($proximas_bodas as $boda) {
                $fecha_creacion = new DateTime($boda->fecha_creacion);
                $mes = $this->formatearMes($fecha_creacion);
                $anio = $fecha_creacion->format("Y");
                $dia = $fecha_creacion->format("d");
                $boda->fecha_creacion = "Fecha de Registro: " . $dia . " de " . $mes . " de " . $anio;
                $fecha_boda = new DateTime($boda->fecha_boda);
                $mes = $this->formatearMes($fecha_boda);
                $anio = $fecha_boda->format("Y");
                $boda->fecha_boda = "Fecha boda: " . $mes . " de " . $anio;
                if (!empty($boda->ciudad_boda) && !empty($boda->estado_boda)) {
                    $boda->lugar_boda = $boda->ciudad_boda . ', ' . $boda->estado_boda;
                } else if (!empty ($boda->ciudad_boda)) {
                    $boda->lugar_boda = $boda->ciudad_boda;
                } else if (!empty($boda->estado_boda)) {
                    $boda->lugar_boda = $boda->estado_boda;
                } else {
                    $boda->lugar_boda = "Lugar: Sin Eleccion";
                }
                if (!empty($boda->foto)) {
                    $boda->url_foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $boda->id_usuario;
                } else {
                    $boda->url_foto_usuario = base_url() . "dist/img/blog/perfil.png";
                }
                $amistad = $this->grupo->getAmistad($boda->id_usuario);
                if (!empty($amistad) && $amistad->amistad == 1) {
                    $boda->estado_usuario = "Eliminar Amistad";
                } else if (!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $boda->id_usuario) {
                    $boda->estado_usuario = "Aceptar Solicitud";
                } else if (!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_confirmacion == $boda->id_usuario) {
                    $boda->estado_usuario = "Solicitud Enviada";
                } else {
                    $boda->estado_usuario = "Enviar Solicitud";
                }
                $boda->url_usuario = base_url() . "index.php/novios/comunidad/Perfil/usuario/" . $boda->id_usuario;
                $boda->url_debates = "misdebates-" . $boda->id_usuario;
                $boda->url_debates_participacion = "participacion-" . $boda->id_usuario;
                $boda->url_fotos = "fotos-" . $boda->id_usuario;
                $boda->url_amigos = "amigos-" . $boda->id_usuario;
                $boda->url_agregar_amigo = "agregar-" . $boda->id_usuario;
                $boda->url_comentario = base_url() . "index.php/comunidad/Perfil/comentario/" . $boda->id_usuario;
                $boda->num_debates = $this->grupo->getTotalDebates($boda->id_usuario);
                $boda->num_mensajes = $this->grupo->getTotalMensajes($boda->id_usuario);
                $boda->num_fotos = $this->grupo->getTotalFotosUsuario($boda->id_usuario);
                $boda->num_amigos = $this->grupo->getTotalAmigos($boda->id_usuario);
                $bodas[$i] = $boda;
                $i++;
            }
            $datos = array(
                'proximas_bodas' => $bodas,
                'total_paginas' => $total_paginas
            );
            return $datos;
        } else {
            $this->load->view('errors/html/error_404');
        }
    }

    public function unirse_al_grupo()
    {
        if ($_POST) {
            $id_grupo = $this->input->post('grupo');
            $result = $this->grupo->validarUnionGrupo($id_grupo);
            if (isset($result->activo) && $result->activo == 0) {
                $result = $this->grupo->updateUnionGrupo($id_grupo, 1, $result->id_miembro, 1);
                $data = "Salir Del Grupo";
            } else if (isset ($result->activo) && $result->activo == 1) {
                $result = $this->grupo->updateUnionGrupo($id_grupo, 0, $result->id_miembro, -1);
                $data = "Unirse Al Grupo";
            } else {
                $result = $result = $this->grupo->setUnionGrupo($id_grupo);
                $data = "Salir Del Grupo";
            }
            if ($result) {
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(202)
                    ->set_output(json_encode(array(
                                'success' => TRUE,
                                'data' => $data,
                            )
                        )
                    );
            }
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(404)
                ->set_output(json_encode(array(
                            'success' => FALSE,
                            'data' => 'error'
                        )
                    )
                );
        }
    }

    public function formatearMes($fecha)
    {
        $mes = $fecha->format("F");
        $anio = $fecha->format("Y");
        switch ($mes) {
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return $mes;
    }

    public function error_404()
    {
        $data["pais"] = "Mexico";
        $data["tipos"] = $this->tipo_proveedor->getGrupos();
        foreach ($data["tipos"] as $key => $value) {
            $value->tipos = $this->tipo_proveedor->getGrupo($value->nombre);
        }
        $this->load->view('errors/html/error_404.php', $data);
    }
}

