<?php

class Home extends CI_Controller {

    public $input;

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Comunidad/Comunidad_model", "comunidad");
        $this->load->model("Comunidad/Imagen_model", "imagen");
        $this->load->model("Comunidad/Foto_model", "foto");
        $this->load->model("Comunidad/Video_model", "video");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/";
        $this->load->helper('formats_helper');
    }

    public function index() {
        $ultimos_debates = $this->formatearDebates();
        $grupos = $this->comunidad->getGrupos();
        $debates_comentados = $this->formatearDebatesComentados();
        $fotos = $this->comunidad->getFotos();
        $fotos = $this->formatearFotoUsuario($fotos);
        $videos = $this->comunidad->getVideos();
        $videos = $this->formatearFotoUsuario($videos);
        $mas_activos = $this->formatearMasActivos();
        $visitas_perfil = $this->formatearVisitas();
        $usuarios_boda = $this->formatearCoincidencia();
        $grupos_miembro = $this->comunidad->getGruposMiembro();
        $datos = array(
            'ultimos_debates' => $ultimos_debates,
            'grupos' => $grupos,
            'debates_comentados' => $debates_comentados,
            'fotos' => $fotos,
            'videos' => $videos,
            'mas_activos' => $mas_activos,
            'visitas_perfil' => $visitas_perfil,
            'usuarios_boda' => $usuarios_boda,
            'grupos_miembro' => $grupos_miembro
        );
        $this->load->view('principal/novia/comunidad/index', $datos);
    }

    public function formatearFotoUsuario($datos) {
        $datos2 = "";
        $i = 0;
        foreach ($datos as $dato) {
            if (!empty($dato->mime)) {
                $dato->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/$dato->id_usuario";
            } else {
                $dato->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
            }
            $datos2[$i] = $dato;
            $i++;
        }
        return $datos2;
    }

    public function formatearDebates() {
        $ultimos_debates2 = "";
        $ultimos_debates = $this->comunidad->getUltimosDebates();
        $i = 0;
        foreach ($ultimos_debates as $debate) {
            $debate->fecha_creacion = relativeTimeFormat($debate->fecha_creacion, $mini = FALSE);
            if (strlen($debate->fecha_creacion) <= 10 && strlen($debate->fecha_creacion) > 4) {
                $debate->fecha_creacion = "Hace $debate->fecha_creacion";
            } else {
                $debate->fecha_creacion = "El $debate->fecha_creacion";
            }
            if (!empty($debate->mime)) {
                $debate->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/$debate->id_usuario";
            } else {
                $debate->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
            }
            $debate->url_debate = base_url() . "index.php/novios/comunidad/Home/debatePublicado/$debate->id_debate";
            $ultimos_debates2[$i] = $debate;
            $i++;
        }
        return $ultimos_debates2;
    }

    public function formatearDebatesComentados() {
        $debates_comentados2 = "";
        $debates_comentados = $this->comunidad->getDebatesComentados();
        $i = 0;
        foreach ($debates_comentados as $debate) {
            $datos = $this->comunidad->datosDebates($debate->id_debate);
            $debate->fecha = relativeTimeFormat($debate->fecha, $mini = FALSE);
            if (strlen($debate->fecha) <= 10 && strlen($debate->fecha) > 4) {
                $debate->fecha = "Hace $debate->fecha";
            } else {
                $debate->fecha = "El $debate->fecha";
            }
            if (!empty($datos->mime)) {
                $debate->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/$datos->id_usuario";
            } else {
                $debate->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
            }
            if(!empty($datos)){
                $debate->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$datos->id_usuario";
                $debate->titulo_debate = $datos->titulo_debate;
                $debate->usuario = $datos->usuario;
                $debate->fecha_creacion = relativeTimeFormat($datos->fecha_creacion, $mini = FALSE);
                if (strlen($debate->fecha_creacion) <= 10 && strlen($debate->fecha_creacion) > 4) {
                    $debate->fecha_creacion = "Hace $debate->fecha_creacion";
                } else {
                    $debate->fecha_creacion = "El $debate->fecha_creacion";
                }
            }
            $debate->debate = substr($datos->debate, 0, 255);
            $debate->debate = "$debate->debate ...";
            $debate->num_comentarios = $datos->comentarios;
            $debates_comentados2[$i] = $debate;
            $i++;
        }
        return $debates_comentados2;
    }

    public function formatearMasActivos() {
        $mas_activos = $this->comunidad->usuariosActivos();
        $mas_activos2 = "";
        $i = 0;
        if ($mas_activos) {
            foreach ($mas_activos as $activo) {
                $activo = $this->comunidad->getDatosActivos($activo->id_usuario);
                if ($activo) {
                    $activo->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$activo->id_usuario";
                    if (!empty($activo->mime)) {
                        $activo->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/$activo->id_usuario";
                    } else {
                        $activo->foto_usuario = base_url() . "dist/img/blog/perfil.png";
                    }
                    $activo->fecha_boda = new DateTime($activo->fecha_boda);
                    $mes = $this->formatearMes($activo->fecha_boda);
                    $anio = $activo->fecha_boda->format('Y');
                    $activo->fecha_boda = "Me caso en $mes de $anio, $activo->poblacion";
                    $mas_activos2[$i] = $activo;
                    $i++;
                }
            }
        }
        return $mas_activos2;
    }

    public function formatearVisitas() {
        $visitas_perfil = $this->comunidad->getVisitas();
        $visitas_perfil2 = "";
        $i = 0;
        if ($visitas_perfil) {
            foreach ($visitas_perfil as $visita) {
                $visita->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$visita->id_usuario";
                if (!empty($visita->mime)) {
                    $visita->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$visita->id_usuario";
                } else {
                    $visita->foto_usuario = base_url() . "dist/img/blog/perfil.png";
                }
                $usuario = $this->session->userdata('id_usuario');
                $visita->todos = base_url() . "index.php/novios/comunidad/perfil/visitas/$usuario";
                $visitas_perfil2[$i] = $visita;
                $i++;
            }
        }
        return $visitas_perfil2;
    }

    public function formatearCoincidencia() {
        $usuarios_boda = $this->comunidad->usuariosBoda();
        if ($usuarios_boda) {
            if (!empty($usuarios_boda->fecha_boda)) {
                $usuarios_boda->fecha_boda = new DateTime($usuarios_boda->fecha_boda);
                $mes = $this->formatearMes($usuarios_boda->fecha_boda);
                $anio = $usuarios_boda->fecha_boda->format('Y');
                $dia = $usuarios_boda->fecha_boda->format('d');
                $usuarios_boda->fecha_boda = "$dia de $mes de $anio";
            }
            if (!empty($usuarios_boda->mime)) {
                $usuarios_boda->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/" . $this->session->userdata('id_usuario');
            } else {
                $usuarios_boda->foto_usuario = base_url() . "dist/img/blog/perfil.png";
            }
            return $usuarios_boda;
        }
    }

//    public function formatearUsuariosOnline(){
//        $usuarios_logeados = $this->comunidad->getUsuariosLogeados();
//        $usuarios_logeados2 = "";
//        $i = 0;
//        if($usuarios_logeados){
//            foreach ($usuarios_logeados as $usuario){
//                if(!empty($usuario->mime)){
//                    $usuario->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$usuario->id_usuario";
//                }else{
//                    $usuario->foto_usuario = base_url()."dist/img/blog/perfil.png";
//                }
//                $usuario->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$usuario->id_usuario";
//                $usuarios_logeados2[$i] = $usuario;
//                $i++;
//            }
//        }
//        return $usuarios_logeados2;
//    }

    public function debates() {
        $this->load->view('principal/novia/comunidad/debates');
    }

    public function nuevoDebate($id_grupo = "") {
        $dudas = "";
        if ($_GET) {
            $dudas = $this->input->get('dudas');
        }
        $datos = $this->comunidad->getGrupos();
        $grupos = array();
        $grupo = array(
            'grupos' => $datos,
            'dudas' => $dudas,
            'id_grupo' => $id_grupo
        );
        $this->load->view('principal/novia/comunidad/nuevo_debate', $grupo);
    }

    public function imagen($id) {
        if ($id == 0) {
            redirect(base_url() . "/dist/img/blog/default.png");
            return;
        }
        $imagen = $this->imagen->get($id);
        if ($imagen) {
            return $this->output
                            ->set_content_type($imagen->mime)
                            ->set_status_header(200)
                            ->set_output($imagen->imagen);
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }

    public function subir($type) {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            switch (strtoupper($type)) {
                case "IMAGEN":
                    $imagen = $this->input->post("archivo");
                    $mime = explode(",", $imagen);
                    $imagen = base64_decode($mime[1]);
                    $mime = explode(";", $mime[0]);
                    $mime = explode(":", $mime[0]);
                    $mime = $mime[1];
                    $datos = array("imagen" => $imagen, "mime" => $mime);
                    $b = $this->imagen->insert($datos);
                    if ($b) {
                        return $this->output
                                        ->set_status_header(200)
                                        ->set_output(json_encode(array(
                                            'success' => true,
                                            'data' => $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/" . $this->imagen->last_id()
                        )));
                    }
                default:
                    break;
            }
        }
        return $this->output
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }

    public function publicarDebate() {
        if ($_POST) {
            $datos['grupos'] = $this->input->post('grupos', TRUE);
            $datos['titulo'] = $this->input->post('titulo', TRUE);
            $datos['contenido'] = $this->input->post('contenido');
            $result = $this->comunidad->publicarDebate($datos);
            $debates_publicados = $this->comunidad->numero_debates();
            switch ($debates_publicados) {
                case 100:
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 1);
                    break;
                case 50:
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 4);
                    break;
                case 20:
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 3);
                    break;
                case 10:
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 2);
                    break;
                case 1:
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 24);
                    break;
            }
            $fotos_publicadas = $this->foto->numero_fotos();
            $videos_publicados = $this->video->numero_videos();
            $contador = $debates_publicados + $fotos_publicadas + $videos_publicados;
            switch ($contador) {
                case 10:
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 20);
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 23);
                    break;
                case 20:
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 21);
                    break;
                case 50:
                    $this->comunidad->set_medallas($this->session->userdata('id_usuario'), 22);
                    break;
            }
        }
        redirect("novios/comunidad/Home/debatePublicado/$result");
    }

    public function debatePublicado($id_debate = "", $pagina = 1) {
        $grupos = $this->comunidad->getGrupos();
        $visitas_perfil = $this->formatearVisitas();
        $usuarios_boda = $this->formatearCoincidencia();
        $grupos_miembro = $this->comunidad->getGruposMiembro();
        $datos = $this->comunidad->getDebate($id_debate);
        $suscripcion = $this->comunidad->validarSuscripcion($id_debate);
        if ($datos) {
            if (isset($suscripcion->activo) && $suscripcion->activo == 1) {
                $suscripcion = "Cancelar Suscripcion";
            } else {
                $suscripcion = "Suscribirse";
            }
            $fecha_creacion2 = relativeTimeFormat($datos->fecha_creacion, $mini = FALSE);
            if (strlen($fecha_creacion2) <= 10 && strlen($fecha_creacion2) > 4) {
                $fecha_creacion2 = "Hace $fecha_creacion2";
            } else if (strlen($fecha_creacion2) > 10) {
                $fecha_creacion2 = "El $fecha_creacion2";
            }
            if (!empty($datos->tipo_novia)) {
                $datos->tipo_novia = $this->validarTipo_usuario($datos->tipo_novia, $datos->id_usuario);
            }
            $this->setVistaDebate($id_debate);
            $contador = $this->comunidad->getContadorRespuestas($id_debate);
            $fecha_bodaD = new DateTime($datos->fecha_boda);
            $mes = $this->formatearMes($fecha_bodaD);
            $anio = $fecha_bodaD->format("Y");
            $dia = $fecha_bodaD->format("d");
            $fecha_bodaD = $mes . " " . $anio;
            $fecha_bodaD2 = $dia . " de " . $mes . " de " . $anio;
            $foto_usuario = $this->comunidad->getFoto($this->session->userdata('id_usuario'));
            if (!empty($foto_usuario)) {
                $id_usuario = $this->session->userdata('id_usuario');
                $foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$id_usuario";
            } else {
                $foto_usuario = base_url() . 'dist/img/blog/perfil.png';
            }
            if (!empty($datos->foto)) {
                $datos->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$datos->id_usuario";
            } else {
                $datos->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
            }
            $foto_logeado = $this->comunidad->getFoto($this->session->userdata('id_usuario'));
            if (!empty($foto_logeado->mime)) {
                $foto_logeado = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $this->session->userdata('id_usuario');
            } else {
                $foto_logeado = base_url() . 'dist/img/blog/perfil.png';
            }
            $comentarios2 = "";
            $comentarios3 = "";
            if (isset($pagina) || $contador > 0 && $contador <= 20) {
                $total_paginas = (int) ($contador / 20);
                if ($total_paginas == 0) {
                    $total_paginas = 1;
                }
                if ($pagina <= $total_paginas) {
                    $inicio = ($pagina - 1) * 20;
                    $limite = $pagina * 20;
                    $limite = $contador - $limite;
                    $comentarios = $this->comunidad->getRespuestas($id_debate, $inicio, $limite);
                    $respuestaComentarios = $this->comunidad->getRespuestaComentario($id_debate);
                    $i = 0;
                    $fecha_creacion = "";
                    $fecha_boda = "";
                    foreach ($comentarios as $comentario) {
                        if ($comentario->fecha_boda != "" && $comentario->fecha_boda != NULL) {
                            $fecha_boda = new DateTime($comentario->fecha_boda);
                            $mes = $this->formatearMes($fecha_boda);
                            $anio = $fecha_boda->format("Y");
                            $fecha_boda = $mes . " " . $anio;
                        }
                        if ($comentario->fecha_creacion) {
                            $fecha_creacion = relativeTimeFormat($comentario->fecha_creacion, $mini = FALSE);
                            if (strlen($fecha_creacion) <= 10) {
                                $fecha_creacion = "Hace $fecha_creacion";
                            } else {
                                $fecha_creacion = "El $fecha_creacion";
                            }
                        }
                        if (!empty($comentario->mime)) {
                            $comentario->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$comentario->id_usuario";
                        } else {
                            $comentario->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                        }
                        if (!empty($comentario->tipo_novia)) {
                            $comentario->tipo_novia = $this->validarTipo_usuario($comentario->tipo_novia, $comentario->id_usuario);
                        }
                        $comentario->fecha_boda = $fecha_boda;
                        $comentario->fecha_creacion = $fecha_creacion;
                        $comentarios2[$i] = $comentario;
                        $i++;
                    }
                    $i = 0;
                    foreach ($respuestaComentarios as $comentario) {
                        if ($comentario->fecha_boda != "" && $comentario->fecha_boda != NULL) {
                            $fecha_boda = new DateTime($comentario->fecha_boda);
                            $mes = $this->formatearMes($fecha_boda);
                            $anio = $fecha_boda->format("Y");
                            $fecha_boda = $mes . " " . $anio;
                        }
                        if ($comentario->fecha_creacion) {
                            $fecha_creacion = relativeTimeFormat($comentario->fecha_creacion, $mini = FALSE);
                            if (strlen($fecha_creacion) <= 10) {
                                $fecha_creacion = "Hace $fecha_creacion";
                            } else {
                                $fecha_creacion = "El $fecha_creacion";
                            }
                        }
                        if (!empty($comentario->mime)) {
                            $comentario->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$comentario->id_usuario";
                        } else {
                            $comentario->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                        }
                        $comentario->fecha_boda = $fecha_boda;
                        $comentario->fecha_creacion = $fecha_creacion;
                        $comentarios3[$i] = $comentario;
                        $i++;
                    }
                }
            }
            $id_denuncia = "";
            if(!empty($datos->id_denuncia)){
                $id_denuncia = $datos->id_denuncia;
            }
            $data = array(
                'grupo' => $datos->nombre,
                'imagen' => $datos->imagen,
                'id_grupo' => $datos->id_grupos_comunidad,
                'id_usuario' => $datos->id_usuario,
                'titulo_debate' => $datos->titulo_debate,
                'id_debate' => $datos->id_debate,
                'contenido' => $datos->debate,
                'usuario' => $datos->usuario,
                'foto' => $datos->foto_usuario,
                'tipo_novia' => $datos->tipo_novia,
                'foto_usuario' => $foto_usuario,
                'foto_logeado' => $foto_logeado,
                'fecha_boda' => $fecha_bodaD,
                'fecha_boda2' => $fecha_bodaD2,
                'estado_boda' => $datos->estado_boda,
                'fecha_creacion' => $fecha_creacion2,
                'contador' => $contador,
                'total_paginas' => $total_paginas,
                'pagina' => $pagina,
                'relacion' => '0',
                'visita_perfil' => '',
                'suscripcion' => $suscripcion,
                'comentarios' => $comentarios2,
                'comentarios2' => $comentarios3,
                'grupos' => $grupos,
                'visitas_perfil' => $visitas_perfil,
                'usuarios_boda' => $usuarios_boda,
                'grupos_miembro' => $grupos_miembro,
                'id_denuncia' => $id_denuncia
            );
            $this->load->view('principal/novia/comunidad/debate_publicado', $data);
        } else {
            $this->load->view('errors/html/error_404');
        }
    }

    public function comentariosDebate() {
        if ($_POST) {
            $datos['debate'] = $this->input->post('debate', TRUE);
            $datos['mensaje'] = $this->input->post('mensaje');
            $datos['permiso_notificacion'] = $this->input->post('permiso_notificacion', TRUE);
            $datos['fecha_creacion'] = date('Y-m-d H:i:s');
            if ($datos['permiso_notificacion'] != NULL) {
                $datos['permiso_notificacion'] = 1;
            } else {
                $datos['permiso_notificacion'] = 0;
            }
            $result = $this->comunidad->comentariosDebate($datos);
            if ($result) {
                $datos['foto'] = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $this->session->userdata('id_usuario');
                $fecha = new DateTime($result->fecha_boda);
                $mes = $this->formatearMes($fecha);
                $anio = $fecha->format("Y");
                $fecha_creacion = relativeTimeFormat($datos['fecha_creacion'], $mini = FALSE);
                $fecha = $mes." ". $anio;
                $datos['fecha_boda'] = $fecha; 
                $datos['estado_boda'] = $result->estado_boda;
                $datos['fecha_creacion'] = $fecha_creacion;
                $datos['usuario'] = $result->usuario;
                $datos['id_comentario'] = $result->id_comentario;
                if (!empty($result->tipo_novia)) {
                    $datos['tipo_novia'] = $this->validarTipo_usuario($result->tipo_novia, $result->id_usuario);
                }
                if (!empty($result->mime)) {
                    $datos['foto_usuario'] = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$result->id_usuario";
                } else {
                    $datos['foto_usuario'] = base_url() . 'dist/img/blog/perfil.png';
                }
                if(!empty($result->id_boda)){
                    $datos['id_boda'] = $result->id_boda;
                }
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(202)
                            ->set_output(json_encode(array(
                                'success' => true,
                                'data' => $datos,
                                            )
                                    )
            );
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }

    public function respuestaComentario() {
        if ($_POST) {
            $datos['debate'] = $this->input->post('debate', TRUE);
            $datos['mensaje'] = $this->input->post('mensaje');
            $datos['respuesta'] = $this->input->post('respuesta', TRUE);
            $datos['fecha_creacion'] = date('Y-m-d H:i:s');
            $result = $this->comunidad->respuestaComentario($datos);
            if ($result) {
                if (!empty($result->mime)) {
                    $foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $result->usuarioCO;
                } else {
                    $foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                }
                $fecha = new DateTime($result->fecha_boda);
                $mes = $this->formatearMes($fecha);
                $anio = $fecha->format("Y");
                $fecha_creacion = relativeTimeFormat($datos['fecha_creacion'], $mini = FALSE);
                $fecha = $mes . " " . $anio;
                $fecha_boda = $fecha;
                $estado_boda = $result->estado_boda;
                $fecha_creacion2 = $fecha_creacion;
                $usuario = $result->usuario;
                $id_comentario = $result->id_comentario;
                $tipo_novia = "";
                if (!empty($result->tipo_novia)) {
                    $tipo_novia = $this->validarTipo_usuario($result->tipo_novia, $result->id_usuario);
                }
                $data = array(
                    'mensaje' => $datos['mensaje'],
                    'url_foto' => $foto_usuario,
                    'fecha_boda' => $fecha_boda,
                    'estado_boda' => $estado_boda,
                    'fecha_creacion' => $fecha_creacion2,
                    'usuario' => $usuario,
                    'mensaje' => $id_comentario,
                    'comentario' => $datos['mensaje'],
                    'tipo_novia' => $tipo_novia,
                    'id_comentario' => $result->id_comentario
                );
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(202)
                            ->set_output(json_encode(array(
                                'success' => true,
                                'data' => $data,
                                            )
                                    )
            );
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }

    function getRespuesta() {
        if ($_POST) {
            $id_comentario = $this->input->post('comentario', TRUE);
            $result = $this->comunidad->getRespuesta($id_comentario);
            if ($result) {
                $i = 0;
                foreach ($result as $respuesta) {
                    $fecha_boda = "";
                    if ($respuesta->fecha_boda != "" && $respuesta->fecha_boda != NULL) {
                        $fecha_boda = new DateTime($respuesta->fecha_boda);
                        $mes = $this->formatearMes($fecha_boda);
                        $anio = $fecha_boda->format("Y");
                        $fecha_boda = $mes . " " . $anio;
                    }
                    if ($respuesta->fecha_creacion) {
                        $fecha_creacion = relativeTimeFormat($respuesta->fecha_creacion, $mini = FALSE);
                        if (strlen($fecha_creacion) <= 10) {
                            $fecha_creacion = "Hace $fecha_creacion";
                        } else {
                            $fecha_creacion = "El $fecha_creacion";
                        }
                    }
                    if (!empty($respuesta->tipo_novia)) {
                        $respuesta->tipo_novia = $this->validarTipo_usuario($respuesta->tipo_novia, $respuesta->id_usuario);
                    }
                    if (!empty($respuesta->mime)) {
                        $respuesta->url_foto = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $respuesta->usuarioCO;
                    } else {
                        $respuesta->url_foto = base_url() . 'dist/img/blog/perfil.png';
                    }
                    $denunciado = $this->comunidad->comentario_denunciado($respuesta->id_comentario,1);
                    if(!empty($denunciado->id_comentario) && $denunciado->id_comentario == $respuesta->id_comentario){
                        $respuesta->id_denuncia = $denunciado->id_comentario;
                    }else{
                        $respuesta->id_denuncia = "";
                    }
                    $respuesta->fecha_boda = $fecha_boda;
                    $respuesta->fecha_creacion = $fecha_creacion;
                    $respuesta2[$i] = $respuesta;
                    $i++;
                }
                $datos = array('respuestas' => $respuesta2);
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $datos,
                                                )
                                        )
                );
            }
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }

    public function suscripcionDebate() {
        if ($_POST) {
            $id_debate = $this->input->post("debate", TRUE);
            $result = $this->comunidad->validarSuscripcion($id_debate);
            if (isset($result->activo) && $result->activo == 0) {
                $result = $this->comunidad->updateSuscripcion($id_debate, 1, $result->id_suscripcion);
                $data = "Cancelar Suscripcion";
            } else if (isset($result->activo) && $result->activo == 1) {
                $result = $this->comunidad->updateSuscripcion($id_debate, 0, $result->id_suscripcion);
                $data = "Suscribirse";
            } else {
                $result = $this->comunidad->setSuscripcion($id_debate);
                $data = "Cancelar Suscripcion";
            }
            if ($result) {
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => TRUE,
                                    'data' => $data,
                                                )
                                        )
                );
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(404)
                            ->set_output(json_encode(array(
                                'success' => FALSE,
                                'data' => 'error'
                                            )
                                    )
            );
        }
    }

    public function responderComentario() {
        if ($_POST) {
            $autores = "";
            $id = $this->input->post("id", TRUE);
            $result = $this->comunidad->getComentario($id);
            if ($result) {
                $tokenizar = explode('class="autor"', $result->mensaje);
                $i = count($tokenizar);
                if ($i > 1) {
                    $j = 0;
                    for ($i = 0; $i < count($tokenizar); $i++) {
                        if ($i % 2 != 0) {
                            $token = explode("</p><p>mensaje citado</p>", $tokenizar[$i]);
                            $autor[$j] = $token[0];
                            $j++;
                        }
                    }
                    for ($i = 0; $i < count($autores); $i++) {
                        $autores = $autores . "<p class='autor'>$autor[$i]</p> <p>Mensaje citado</p></div>";
                    }
                    $mensaje = "<div class='mensaje' contenteditable='false'>" . $autores;
                } else {
                    $mensaje = "<div class='mensaje' contenteditable='false'>"
                            . "<p class='autor'><b>Escrito por: " . $result->usuario . "</b></p><p>Mensaje Citado</p></div><br/>";
                }
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => TRUE,
                                    'data' => $tokenizar
                                                )
                                        )
                );
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(404)
                            ->set_output(json_encode(array(
                                'success' => FALSE,
                                'data' => $result
                                            )
                                    )
            );
        }
    }

    public function conocerCompaneros() {
        $result = $this->comunidad->conocerCompaneros();
        $companeros = "";
        if ($result) {
            $result['fecha_boda'] = new DateTime($result['fecha_boda']);
            $anio = $result['fecha_boda']->format("Y");
            $dia = $result['fecha_boda']->format("d");
            $mes = $this->formatearMes($result['fecha_boda']);
            $fecha_boda = $dia . " de " . $mes . " de " . $anio;
            $i = 0;
            foreach ($result['companeros'] as $companero) {
                if (!empty($companero->mime)) {
                    $companero->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$companero->id_usuario";
                } else {
                    $companero->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                }
                $companero->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$companero->id_usuario";
                $companeros[$i] = $companero;
                $i++;
            }
            $datos = array(
                'fecha_boda' => $fecha_boda,
                'companeros' => $companeros,
                'dia' => $dia,
                'mes' => $mes,
                'anio' => $anio
            );
        } else {
            show_404();
        }
        $this->load->view('principal/novia/comunidad/conocer_companeros', $datos);
    }

    public function debateBoda() {
        if ($_POST) {
            $debate = $this->input->post("contenido", TRUE);
            $fecha_boda = $this->input->post("fecha_boda", TRUE);
            $titulo = $this->input->post("titulo", TRUE);
            $datos = array(
                'debate' => $debate,
                'fecha_boda' => $fecha_boda,
                'titulo' => $titulo
            );
            $this->comunidad->publicarDebate($datos);
            $this->load->view('principal/novia/comunidad/debate_publicado', $datos);
        }
    }

    public function formatearMes($fecha) {
//        echo date('Y-m-d h:i:s');
//        $result = $this->comunidad->conocerCompaneros();
//        $fecha = new DateTime($result->fecha_boda);
//        echo $fecha->format("F Y");
        $mes = $fecha->format("F");
        switch ($mes) {
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return $mes;
    }

    public function foto_usuario($id) {
        if ($id == 0) {
            redirect(base_url() . "/dist/img/blog/default.png");
            return;
        }
        $imagen = $this->comunidad->getFoto($id);
        if ($imagen) {
            return $this->output
                            ->set_content_type($imagen->mime)
                            ->set_status_header(200)
                            ->set_output($imagen->foto);
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }

    public function setVistaDebate($id_debate) {
        $id_usuario = $this->session->userdata('id_usuario');
        $sql = "SELECT
            id_vista
        FROM
            vistas_debates VD
        INNER JOIN debates D ON (D.id_debate = VD.id_debate)
        INNER JOIN usuario U ON (U.id_usuario = VD.id_usuario)
        WHERE
            D.activo = 1
        AND U.activo = 1
        AND VD.id_debate = $id_debate
        AND VD.id_usuario = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        if (empty($result)) {
            $data = array(
                'id_debate' => $id_debate,
                'id_usuario' => $id_usuario
            );
            $result = $this->db->insert("vistas_debates", $data);
            $result = $this->db->query("select vistas from debates where id_debate=$id_debate")->row();
            if (empty($result)) {
                $result->vistas = 0;
            }
            $this->db->set($data = array('vistas' => $result->vistas + 1));
            $this->db->where("id_debate", $id_debate);
            $this->db->where("activo", 1);
            $this->db->update("debates");
        }
    }

    public function denuncia() {
        if ($_POST) {
            $comentario = $this->input->post("id_comentario", TRUE);
            $razon = $this->input->post("razon", TRUE);
            $tipo = $this->input->post("tipo", TRUE);
            $result = $this->comunidad->denuncia($comentario, $razon, $tipo);
            if ($result) {
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(200)
                                ->set_output('true');
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(404);
        }
    }

    public function denuncia_debate() {
        if ($_POST) {
            $debate = $this->input->post("debate", TRUE);
            $razon = $this->input->post("razon", TRUE);
            $result = $this->comunidad->denuncia_debate($debate, $razon);
            if ($result) {
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(200)
                                ->set_output('true');
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(404);
        }
    }

    public function buscar() {
        if ($_POST) {
            $titulo_debate = $this->input->post("titulo_debate", TRUE);
            $result = $this->comunidad->buscar($titulo_debate);
            $debates = "";
            if ($result) {
                $i = 0;
                foreach ($result as $debate) {
                    if (!empty($debate->mime)) {
                        $debate->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/$debate->id_usuario";
                    } else {
                        $debate->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                    }
                    $debate->enlace_debate = base_url() . "index.php/novios/comunidad/home/debatePublicado/$debate->id_debate";
                    $debate->fecha_creacion = new DateTime($debate->fecha_creacion);
                    $debate->fecha_creacion = $debate->fecha_creacion->format('d/m/Y');
                    $debates[$i] = $debate;
                    $i++;
                }
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(200)
                                ->set_output(json_encode(array(
                                    'success' => TRUE,
                                    'data' => $debates
                                                )
                                        )
                );
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(200)
                            ->set_output(json_encode(array(
                                'success' => FALSE
                                            )
                                    )
            );
        }
    }

    public function getGrupos() {
        $grupos = $this->comunidad->getGrupos();
        $grupos2 = "";
        $i = 0;
        if ($grupos) {
            foreach ($grupos as $grupo) {
                $grupo->debates = $this->comunidad->num_debates($grupo->id_grupos_comunidad);
                $grupo->enlace_grupo = base_url() . "index.php/novios/comunidad/group/grupo/$grupo->id_grupos_comunidad/debates";
                $grupo->imagen = base_url() . "dist/images/comunidad/grupos/$grupo->imagen";
                $grupos2[$i] = $grupo;
                $i++;
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(200)
                            ->set_output(json_encode(array(
                                'success' => TRUE,
                                'data' => $grupos2
                                            )
                                    )
            );
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }

    public function validarTipo_usuario($tipo, $id_usuario) {
        $novio = "";
        switch ($tipo) {
            case 1:
                $genero = $this->db->query("SELECT genero FROM usuario U INNER JOIN cliente C USING (id_usuario) WHERE id_usuario = $id_usuario")->row();
                if (!empty($genero) && $genero->genero == 1) {
                    $novio = "Nuevo";
                } else if (!empty($genero) && $genero->genero == 2) {
                    $novio = "Nueva";
                }
                break;
            case 2:
                $novio = "Principiante";
                break;
            case 3:
                $novio = "Habitual";
                break;
            case 4:
                $novio = "Top";
                break;
            case 5:
                $novio = "Pro";
                break;
            case 6:
                $novio = "VIP";
                break;
            case 7:
                $novio = "Super";
                break;
            case 8:
                $genero = $this->db->query("SELECT genero FROM usuario U INNER JOIN cliente C USING (id_usuario) WHERE id_usuario = $id_usuario")->row();
                if (!empty($genero) && $genero->genero == 1) {
                    $novio = "Destacado";
                } else if (!empty($genero) && $genero == 2) {
                    $novio = "Destacada";
                }
                break;
            case 9:
                $novio = "Leyenda";
                break;
        }
        return $novio;
    }

}
