<?php
class Perfil extends CI_Controller {
    public $input;    

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Comunidad/Comunidad_model", "comunidad");
        $this->load->model("Comunidad/Imagen_model", "imagen");
        $this->load->model("Comunidad/Perfil_model", "perfil");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/";
        $this->load->helper('formats_helper');
    }
    
    //EL USUARIO SERA PARA LA VISTA DE MI PERFIL
    
    public function usuario($id_usuario = ""){
        if(empty($id_usuario)){
            $this->error_404();
        }else{
            $mi_id_usuario = $this->session->userdata('id_usuario');
            if($mi_id_usuario != $id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerDatosPerfil($id_usuario);
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Perfil/index',$datos);
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerDatosPerfil($id_usuario);
                    $datos['medalla'] = 'mi_perfil';
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Miperfil/index',$datos);
                    }
                }else{
                    $this->error_404();
                }
            }
        }
    }
    
    public function obtenerDatosPerfil($id_usuario){
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            if(!empty($datos_usuario->color)){
                $datos_usuario->color2 = $this->formatearColor($datos_usuario->color);
            }
            if(!empty($datos_usuario->estacion)){
                $datos_usuario->estacion2 = $this->formatearEstacion($datos_usuario->estacion);
            }
            if(!empty($datos_usuario->estilo)){
                $datos_usuario->estilo2 = $this->formatearEstilo($datos_usuario->estilo);
            }
            $actividades = $this->perfil->getActividadPerfil($id_usuario);
            $actividades2 = "";
            $i = 0;
            if($actividades){
                foreach ($actividades as $actividad){
                    $actividad = $this->perfil->formatearDatosActividad($actividad);
                    $actividad->fecha_creacion = relativeTimeFormat($actividad->fecha_creacion, $mini=FALSE);
                    if(strlen($actividad->fecha_creacion) <= 10 && strlen($actividad->fecha_creacion) > 4){
                        $actividad->fecha_creacion = "Hace $actividad->fecha_creacion";
                    }else{
                        $actividad->fecha_creacion = "El $actividad->fecha_creacion";
                    }
                    $actividades2[$i] = $actividad;
                    $i++;
                }
            }
            $grupos = $this->comunidad->getGrupos();
            $visitas_perfil = $this->formatearVisitas();
            $usuarios_boda = $this->formatearCoincidencia();
            $grupos_miembro = $this->comunidad->getGruposMiembro();
            $debates = $this->perfil->getDebatesPerfil($id_usuario);
            $fotos = $this->perfil->getFotosPerfil($id_usuario);
            $videos = $this->perfil->getVideosPerfil($id_usuario);
            $amigos = $this->perfil->getAmigosPerfil($id_usuario);
            $comentarios = $this->perfil->getComentariosPerfil($id_usuario);
            $visita = $this->perfil->validarVisita($id_usuario);
            $amistad = $this->getAmistad($id_usuario);
            $datos = array(
                'datos_usuario' => $datos_usuario,
                'debates' => $debates,
                'fotos' => $fotos,
                'videos' => $videos,
                'ultimas_actividades' => $actividades2,
                'amigos' => $amigos,
                'comentarios' => $comentarios,
                'seccion' => 'perfil',
                'id_usuario' => $id_usuario,
                'grupos' => $grupos,
                'visitas_perfil' => $visitas_perfil,
                'usuarios_boda' => $usuarios_boda,
                'grupos_miembro' => $grupos_miembro,
                'amistad' => $amistad
            );
            return $datos;
        }
    }
    
    public function mi_muro($id_usuario = "",$pagina = 1){
        if(empty($id_usuario)){
            $this->error_404();
        }else{
            $mi_id_usuario = $this->session->userdata('id_usuario');
            if($mi_id_usuario != $id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerDatosMuro($id_usuario, $pagina);
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Perfil/mi_muro',$datos);
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerDatosMuro($id_usuario, $pagina);
                    $datos['medalla'] = 'mi_perfil';
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Miperfil/mi_muro',$datos);
                    }
                }else{
                    $this->error_404();
                }
            }
        }
    }
    
    public function obtenerDatosMuro($id_usuario,$pagina){
        $inicio = 0;
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            $contador = $this->perfil->getTotalComentariosMuro($id_usuario);
            $total_paginas = (int)($contador / 16);
            if($total_paginas < ($contador/16)){
                $total_paginas++;
            }
            if($total_paginas == 0){
                    $total_paginas = 1;
            }
            if($pagina <= $total_paginas && $pagina > 0){
                $inicio = ($pagina - 1) * 16;
                $comentarios = $this->perfil->getComentariosMuro($id_usuario,$inicio);
                $comentarios2 = "";
                $i = 0;
                if($comentarios){
                    foreach ($comentarios as $comentario){
                        $comentario->fecha_creacion = new DateTime($comentario->fecha_creacion);
                        $comentario->fecha_creacion = "El ".$comentario->fecha_creacion->format('d/m/Y');
                        if(!empty($comentario->mime)){
                            $comentario->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$comentario->id_usuario";
                        }else{
                            $comentario->foto_usuario = base_url().'dist/img/blog/perfil.png';
                        }
                        $comentario->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$comentario->id_usuario";
                        $comentarios2[$i] = $comentario; 
                        $i++;
                    }
                }
                $grupos = $this->comunidad->getGrupos();
                $visitas_perfil = $this->formatearVisitas();
                $usuarios_boda = $this->formatearCoincidencia();
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $visita = $this->perfil->validarVisita($id_usuario);
                $amistad = $this->getAmistad($id_usuario);
                $datos = array(
                    'seccion' => 'muro',
                    'datos_usuario' => $datos_usuario,
                    'comentarios' => $comentarios2,
                    'total_paginas' => $total_paginas,
                    'contador' => $contador,
                    'pagina' => $pagina,
                    'id_usuario' => $id_usuario,
                    'grupos' => $grupos,
                    'visitas_perfil' => $visitas_perfil,
                    'usuarios_boda' => $usuarios_boda,
                    'grupos_miembro' => $grupos_miembro,
                    'amistad' => $amistad
                );
                return $datos;
            }else{
                $this->load->view('errors/html/error_404');
            }
        }
    }

    public function  setComentarioMuro(){
        $id_usuario = $this->input->post('usuario', TRUE);
        $id_muro = $this->input->post('muro', TRUE);
        $comentario = $this->input->post('comentario', TRUE);
        $result = $this->perfil->setComentarioMuro($id_usuario,$id_muro,$comentario);
        if($result){
            if(!empty($result->mime)){
                $result->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$result->id_usuario";
            }else{
                $result->foto_usuario = base_url().'dist/img/blog/perfil.png';
            }
            $result->fecha_creacion = new DateTime($result->fecha_creacion);
            $result->fecha_creacion = "El ".$result->fecha_creacion->format('d/m/Y');
            $result->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$result->id_usuario";
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(202)
                            ->set_output(json_encode(array(
                                'success' => true,
                                'data' => $result
                            )
                    )
            );
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => true,
                            'data' => 'error'
                        )
                )
        );
    }

    public function debates($ordenamiento,$id_usuario = "",$pagina = 1){
        if(empty($id_usuario)){
            $this->error_404();
        }else{
            $mi_id_usuario = $this->session->userdata('id_usuario');
            if($mi_id_usuario != $id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerDebates($ordenamiento,$id_usuario, $pagina);
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Perfil/debates',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerDebates($ordenamiento,$id_usuario, $pagina);
                    $datos['medalla'] = 'mi_perfil';
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Miperfil/debates',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }
        }
    }
    
    public function obtenerDebates($ordenamiento,$id_usuario,$pagina){
        $inicio = 0;
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            if($ordenamiento == "participacion"){
                $contador = $this->perfil->getTotalDebatesParticipacion($id_usuario);
                $total_paginas = (int)($contador / 16);
                if($total_paginas < ($contador/16)){
                    $total_paginas++;
                }
                if($total_paginas == 0){
                        $total_paginas = 1;
                }
                if($pagina <= $total_paginas && $pagina > 0){
                    $inicio = ($pagina - 1) * 16;
                    $debates = $this->perfil->getDebatesParticipacion($id_usuario,$inicio);
                }else{
                    $debates = $this->perfil->getDebatesParticipacion($id_usuario,$inicio);
                }
            }else if($ordenamiento == "misdebates"){
                $contador = $this->perfil->getTotalMisDebates($id_usuario);
                $total_paginas = (int)($contador / 16);
                if($total_paginas < ($contador/16)){
                    $total_paginas++;
                }
                if($total_paginas == 0){
                        $total_paginas = 1;
                }
                if($pagina <= $total_paginas && $pagina > 0){
                    $inicio = ($pagina - 1) * 16;
                    $debates = $this->perfil->getMisDebates($id_usuario,$inicio);
                }else{
                    $this->error_404();
                }
            }else{
                $this->error_404();
            }
            $debates2 = "";
            $i = 0;
            if($debates){
                foreach ($debates as $debate){
                    $debate->fecha_creacion = relativeTimeFormat($debate->fecha_creacion, $mini=FALSE);
                    if(strlen($debate->fecha_creacion) <= 10 && strlen($debate->fecha_creacion) > 4){
                        $debate->fecha_creacion = "Hace $debate->fecha_creacion";
                    }else{
                        $debate->fecha_creacion = "El $debate->fecha_creacion";
                    }
                    if(!empty($debate->mime)){
                        $debate->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$debate->id_usuario";
                    }else{
                        $debate->foto_usuario = base_url().'dist/img/blog/perfil.png';
                    }
                    $debate->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$debate->id_usuario";
                    $debate->debate = substr($debate->debate, 0, 250);
                    $debate->debate = "$debate->debate ...";
                    $debates2[$i] = $debate;
                    $i++;
                }
            }
            $grupos = $this->comunidad->getGrupos();
            $visitas_perfil = $this->formatearVisitas();
            $usuarios_boda = $this->formatearCoincidencia();
            $grupos_miembro = $this->comunidad->getGruposMiembro();
            $visita = $this->perfil->validarVisita($id_usuario);
            $amistad = $this->getAmistad($id_usuario);
            $datos = array(
                'seccion' => 'debates',
                'datos_usuario' => $datos_usuario,
                'debates' => $debates2,
                'ordenamiento' => $ordenamiento,
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'pagina' => $pagina,
                'id_usuario' => $id_usuario,
                'grupos' => $grupos,
                'visitas_perfil' => $visitas_perfil,
                'usuarios_boda' => $usuarios_boda,
                'grupos_miembro' => $grupos_miembro,
                'amistad' => $amistad
            );
            return $datos;
        }
    }
    
    public function amigos($id_usuario = "", $pagina = 1){
        if(empty($id_usuario)){
            $this->error_404();
        }else{
            $mi_id_usuario = $this->session->userdata('id_usuario');
            if($mi_id_usuario != $id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerAmigos($id_usuario, $pagina, "miperfil");
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Perfil/amigos',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerAmigos($id_usuario, $pagina, "perfil");
                    $datos['medalla'] = 'mi_perfil';
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Miperfil/amigos',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }
        }
    }
    
    public function obtenerAmigos($id_usuario,$pagina){
        $inicio = 0;
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            $contador = $this->perfil->getTotalAmigos($id_usuario);
            $total_paginas = (int)($contador / 16);
            if($total_paginas < ($contador/16)){
                $total_paginas++;
            }
            if($total_paginas == 0){
                    $total_paginas = 1;
            }
            if($pagina <= $total_paginas && $pagina > 0){
                $inicio = ($pagina - 1) * 16;
                $grupos = $this->comunidad->getGrupos();
                $visitas_perfil = $this->formatearVisitas();
                $usuarios_boda = $this->formatearCoincidencia();
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $amistad = $this->getAmistad($id_usuario);
                $visita = $this->perfil->validarVisita($id_usuario);
                $amigos = $this->perfil->getAmigos($id_usuario,$inicio);
                $amistad = $this->getAmistad($id_usuario);
                $datos = array(
                    'seccion' => 'amigos',
                    'datos_usuario' => $datos_usuario,
                    'amigos' => $amigos,
                    'total_paginas' => $total_paginas,
                    'contador' => $contador,
                    'pagina' => $pagina,
                    'id_usuario' => $id_usuario,
                    'amistad' => $amistad,
                    'grupos' => $grupos,
                    'visitas_perfil' => $visitas_perfil,
                    'usuarios_boda' => $usuarios_boda,
                    'grupos_miembro' => $grupos_miembro,
                    'amistad' => $amistad
                );
                return $datos;
            }
        }
    }
    
    public function getAmistad($id_usuario){
        $amistad = $this->perfil->getAmistad($id_usuario);
        if(!empty($amistad) && $amistad->amistad == 1){
            $amistad = "Eliminar Amistad";
        }else if(!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $id_usuario){
            $amistad = "Aceptar Solicitud";
        }else if(!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $this->session->userdata('id_usuario')){
            $amistad = "Solicitud Enviada";
        }else{
            $amistad = "Enviar Solicitud";
        }
        return $amistad;
    }

    public function fotos($id_usuario = "", $pagina = 1){
        if(empty($id_usuario)){
            $this->error_404();
        }else{
            $mi_id_usuario = $this->session->userdata('id_usuario');
            if($mi_id_usuario != $id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerFotos($id_usuario, $pagina);
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Perfil/fotos',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerFotos($id_usuario, $pagina);
                    $datos['medalla'] = 'mi_perfil';
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Miperfil/fotos',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }
        }
    }
    
    public function obtenerFotos($id_usuario,$pagina){
        $inicio = 0;
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            $contador = $this->perfil->getTotalFotos($id_usuario);
            $total_paginas = (int)($contador / 16);
            if($total_paginas < ($contador/16)){
                $total_paginas++;
            }
            if($total_paginas == 0){
                    $total_paginas = 1;
            }
            if($pagina <= $total_paginas && $pagina > 0){
                $inicio = ($pagina - 1) * 16;
                $fotos = $this->perfil->getFotos($id_usuario,$inicio);
                if($fotos){
                    $fotos = $this->formatearFoto($fotos);
                }
                $grupos = $this->comunidad->getGrupos();
                $visitas_perfil = $this->formatearVisitas();
                $usuarios_boda = $this->formatearCoincidencia();
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $visita = $this->perfil->validarVisita($id_usuario);
                $amistad = $this->getAmistad($id_usuario);
                $datos = array(
                    'seccion' => 'fotos',
                    'datos_usuario' => $datos_usuario,
                    'fotos' => $fotos,
                    'total_paginas' => $total_paginas,
                    'contador' => $contador,
                    'pagina' => $pagina,
                    'id_usuario' => $id_usuario,
                    'grupos' => $grupos,
                    'visitas_perfil' => $visitas_perfil,
                    'usuarios_boda' => $usuarios_boda,
                    'grupos_miembro' => $grupos_miembro,
                    'amistad' => $amistad
                );
                return $datos;
            }
        }
    }
    
    public function formatearFoto($fotos){
        $fotos2 = "";
        $i = 0;
        if($fotos){
            foreach ($fotos as $foto){
                if(!empty($foto->mime)){
                    $foto->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$foto->id_usuario";
                }else{
                    $foto->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                $fotos2[$i] = $foto;
                $i++;
            }
        }
        return $fotos2;
    }
    
    public function videos($id_usuario = "",$pagina = 1){
        if(empty($id_usuario)){
            $this->error_404();
        }else{
            $mi_id_usuario = $this->session->userdata('id_usuario');
            if($mi_id_usuario != $id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obetnerVideos($id_usuario, $pagina);
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Perfil/videos',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obetnerVideos($id_usuario, $pagina);
                    $datos['medalla'] = 'mi_perfil';
                    if($datos){
                        $this->load->view('principal/novia/comunidad/Miperfil/videos',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }
        }
    }
    
    public function obetnerVideos($id_usuario,$pagina){
        $inicio = 0;
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            $contador = $this->perfil->getTotalVideos($id_usuario);
            $total_paginas = (int)($contador / 16);
            if($total_paginas < ($contador/16)){
                $total_paginas++;
            }
            if($total_paginas == 0){
                    $total_paginas = 1;
            }
            if($pagina <= $total_paginas && $pagina > 0){
                $inicio = ($pagina - 1) * 16;
                $videos = $this->perfil->getVideos($id_usuario,$inicio);
                if($videos){
                    $videos = $this->formatearVideo($videos);
                }
                $grupos = $this->comunidad->getGrupos();
                $visitas_perfil = $this->formatearVisitas();
                $usuarios_boda = $this->formatearCoincidencia();
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $visita = $this->perfil->validarVisita($id_usuario);
                $amistad = $this->getAmistad($id_usuario);
                $datos = array(
                    'seccion' => 'videos',
                    'datos_usuario' => $datos_usuario,
                    'videos' => $videos,
                    'total_paginas' => $total_paginas,
                    'contador' => $contador,
                    'pagina' => $pagina,
                    'id_usuario' => $id_usuario,
                    'grupos' => $grupos,
                    'visitas_perfil' => $visitas_perfil,
                    'usuarios_boda' => $usuarios_boda,
                    'grupos_miembro' => $grupos_miembro,
                    'amistad' => $amistad
                );
                return $datos;
            }
        }
    }
    
    public function formatearVideo($videos){
        $videos2 = "";
        $i = 0;
        if($videos){
            foreach ($videos as $video){
                if(!empty($video->mime)){
                    $video->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$video->id_usuario";
                }else{
                    $video->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                $direccion_img = str_replace("https://www.youtube.com", "https://i.ytimg.com/", $video->direccion_web);
                $direccion_img = str_replace("embed", "vi", $direccion_img);
                $direccion_img = $direccion_img."/default.jpg";
                $video->direccion_img = $direccion_img;
                $videos2[$i] = $video;
                $i++;
            }
            return $videos2;
        }
    }
    
    public function actividad($id_usuario = "",$pagina = 1){
        if(empty($id_usuario)){
            $this->load->view('errors/html/error_404');
        }else{
            $mi_id_usuario = $this->session->userdata('id_usuario');
            if($mi_id_usuario != $id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerActividad($id_usuario, $pagina);
                    if($datos){
                        $this->load->view('principal/novia/comunidad/perfil/actividad',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerActividad($id_usuario, $pagina);
                    $datos['medalla'] = 'mi_perfil';
                    if($datos){
                        $this->load->view('principal/novia/comunidad/miperfil/actividad',$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }
        }
    }
    
    public function obtenerActividad($id_usuario,$pagina){
        $inicio = 0;
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            $contador = $this->perfil->getTotalActividades($id_usuario);
            $total_paginas = (int)($contador / 16);
            if($total_paginas < ($contador/16)){
                $total_paginas++;
            }
            if($total_paginas == 0){
                    $total_paginas = 1;
            }
            if($pagina <= $total_paginas && $pagina > 0){
                $inicio = ($pagina - 1) * 16;
                $actividades2 = "";
                $i = 0;
                $actividades = $this->perfil->getActividades($id_usuario,$inicio);
                if($actividades){
                    foreach ($actividades as $actividad){
                        if($actividad){
                            $actividad = $this->perfil->formatearDatosActividad($actividad);
                            if(!empty($actividad->fecha_creacion)){
                                $actividad->fecha_creacion = relativeTimeFormat($actividad->fecha_creacion, $mini=FALSE);
                                if(strlen($actividad->fecha_creacion) <= 10 && strlen($actividad->fecha_creacion) > 4){
                                    $actividad->fecha_creacion = "Hace $actividad->fecha_creacion";
                                }else{
                                    $actividad->fecha_creacion = "El $actividad->fecha_creacion";
                                }
                            }
                            $actividades2[$i] = $actividad;
                            $i++;
                        }
                    }
                }
                $grupos = $this->comunidad->getGrupos();
                $visitas_perfil = $this->formatearVisitas();
                $usuarios_boda = $this->formatearCoincidencia();
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $visita = $this->perfil->validarVisita($id_usuario);
                $amistad = $this->getAmistad($id_usuario);
                $datos = array(
                    'seccion' => 'actividad',
                    'datos_usuario' => $datos_usuario,
                    'actividades' => $actividades2,
                    'total_paginas' => $total_paginas,
                    'contador' => $contador,
                    'pagina' => $pagina,
                    'id_usuario' => $id_usuario,
                    'grupos' => $grupos,
                    'visitas_perfil' => $visitas_perfil,
                    'usuarios_boda' => $usuarios_boda,
                    'grupos_miembro' => $grupos_miembro,
                    'amistad' => $amistad
                );
                return $datos;
            }
        }
    }


    public function medallas($id_usuario){
        if(empty($id_usuario)){
            $this->error_404();
        }
        $mi_id_usuario = $this->session->userdata('id_usuario');
        if($mi_id_usuario != $id_usuario){
            $validar = $this->perfil->validarUsuario($id_usuario);
            if($validar){
                $datos = $this->obtenerMedallas($id_usuario);
                if($datos){
                    $this->load->view('principal/novia/comunidad/Perfil/medallas',$datos);
                }else{
                    $this->error_404();
                }
            }else{
                $this->error_404();
            }
        }else{
            $validar = $this->perfil->validarUsuario($id_usuario);
            if($validar){
                $datos = $this->obtenerMedallas($id_usuario);
                $datos['medalla'] = 'mi_perfil';
                if($datos){
                    $this->load->view('principal/novia/comunidad/Miperfil/medallas',$datos);
                }else{
                    $this->error_404();
                }
            }else{
                $this->error_404();
            }
        }
    }
    
    public function obtenerMedallas($id_usuario){
        $inicio = 0;
        $grupos = $this->comunidad->getGrupos();
        $visitas_perfil = $this->formatearVisitas();
        $usuarios_boda = $this->formatearCoincidencia();
        $grupos_miembro = $this->comunidad->getGruposMiembro();
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        $amistad = $this->getAmistad($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            $medallas = $this->perfil->getMedallas($id_usuario);
            $medallas3 = "";
            $medallas34 = "";
            $i = 0;
            $consulta = "";
            foreach ($medallas as $medalla){
                if(!empty($medalla->url_imagen)){
                    $medalla->url_imagen = base_url()."dist/images/comunidad/medallas/$medalla->url_imagen";
                }
                $consulta = "AND M.id_medalla != $medalla->id_nueva_medalla $consulta";
                $medallas3[$i] = $medalla;
                $i++;
            }
            $medallas2 = $this->perfil->getMedallasNoObtenidas($id_usuario,$consulta);
            if(empty($medallas2)){
                $medallas2 = $this->perfil->getTodasMedallas();
            }
            $i = 0;
            foreach ($medallas2 as $medalla){
                if(!empty($medalla->url_imagen)){
                    $medalla->url_imagen = base_url()."dist/images/comunidad/medallas/$medalla->url_imagen";
                }
                $medallas4[$i] = $medalla;
                $i++;
            }
            $datos = array(
                'seccion' => 'medallas',
                'datos_usuario' => $datos_usuario,
                'medallas' => $medallas3,
                'medallas2' => $medallas4,
                'id_usuario' => $id_usuario,
                'grupos' => $grupos,
                'visitas_perfil' => $visitas_perfil,
                'usuarios_boda' => $usuarios_boda,
                'grupos_miembro' => $grupos_miembro,
                'amistad' => $amistad
            );
            return $datos;
        }
    }
    
    public function proveedores($id_usuario){
        if(empty($id_usuario)){
            $this->error_404();
        }else{
            $mi_id_usuario = $this->session->userdata('id_usuario');
            if($mi_id_usuario != $id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerProveedores($id_usuario);
                    $this->load->view('principal/novia/comunidad/Perfil/proveedores',$datos);
                }else{
                    $this->error_404();
                }
            }else{
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerProveedores($id_usuario);
                    $datos['medalla'] = 'mi_perfil';
                    $this->load->view('principal/novia/comunidad/Miperfil/proveedores',$datos);
                }else{
                    $this->error_404();
                }
            }
        }
    }
    
    public function obtenerProveedores($id_usuario){
        $inicio = 0;
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            $proveedores = $this->perfil->getProveedores($id_usuario);
            $proveedores2 = "";
            $i = 0;
            if($proveedores){
                foreach ($proveedores as $proveedor){
                    if(!empty($proveedor->localizacion_estado) && !empty($proveedor->localizacion_poblacion)){
                        $proveedor->localizacion = "$proveedor->localizacion_poblacion, $proveedor->localizacion_estado";
                    }else if(!empty($proveedor->localizacion_estado)){
                        $proveedor->localizacion = $proveedor->localizacion_estado;
                    }else if(!empty($proveedor->localizacion_poblacion)){
                        $proveedor->localizacion = $proveedor->localizacion_poblacion;
                    }
                    $proveedor->imagen = base_url()."dist/img/slider1.png";
                    $proveedor->url = base_url()."index.php/novios/proveedor/categoria/$proveedor->tag_categoria";
                    $proveedores2[$i] = $proveedor;
                    $i++;
                }
            }
            $grupos = $this->comunidad->getGrupos();
            $visitas_perfil = $this->formatearVisitas();
            $usuarios_boda = $this->formatearCoincidencia();
            $grupos_miembro = $this->comunidad->getGruposMiembro();
            $visita = $this->perfil->validarVisita($id_usuario);
            $amistad = $this->getAmistad($id_usuario);
            $datos = array(
                'seccion' => 'proveedores',
                'datos_usuario' => $datos_usuario,
                'proveedores' => $proveedores2,
                'id_usuario' => $id_usuario,
                'grupos' => $grupos,
                'visitas_perfil' => $visitas_perfil,
                'usuarios_boda' => $usuarios_boda,
                'grupos_miembro' => $grupos_miembro,
                'amistad' => $amistad
            );
            return $datos;
        }
    }
    
    public function visitas($id_usuario = "", $pagina = 1){
        if(empty($id_usuario)){
            $this->error_404();
        }else{
            $mi_id_usuario = $this->session->userdata("id_usuario");
            if($id_usuario == $mi_id_usuario){
                $validar = $this->perfil->validarUsuario($id_usuario);
                if($validar){
                    $datos = $this->obtenerVisitas($id_usuario, $pagina);
                    $datos['medalla'] = 'mi_perfil';
                    if($datos){
                        $this->load->view("principal/novia/comunidad/Miperfil/visitas.php",$datos);
                    }else{
                        $this->error_404();
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $this->error_404();
            }
        }
    }
    public function obtenerVisitas($id_usuario, $pagina){
        $inicio = 0;
        $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
        if($datos_usuario){
            if(!empty($datos_usuario->fecha_creacion)){
                $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                $anio = $datos_usuario->fecha_creacion->format('Y');
                $datos_usuario->fecha_creacion = "Desde $mes de $anio";
            }
            $contador = $this->perfil->getTotalVisitas($id_usuario);
            $total_paginas = (int)($contador / 16);
            if($total_paginas < ($contador/16)){
                $total_paginas++;
            }
            if($total_paginas == 0){
                    $total_paginas = 1;
            }
            if($pagina <= $total_paginas && $pagina > 0){
                $inicio = ($pagina - 1) * 16;
                $visita = $this->perfil->validarVisita($id_usuario);
                $visitas = $this->perfil->getVisitas($id_usuario,$inicio);
                $visitas2 = "";
                $i = 0;
                foreach ($visitas as $visita){
                    $amistad = $this->perfil->getAmistadVisitas($id_usuario,$visita->id_usuario_visita);
                    if(!empty($amistad) && $amistad->amistad == 1){
                        $visita->estado_usuario = "Eliminar Amistad";
                    }else if(!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $visita->id_usuario_visita){
                        $visita->estado_usuario = "Aceptar Solicitud";
                    }else if(!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_confirmacion == $visita->id_usuario_visita){
                        $visita->estado_usuario = "Solicitud Enviada";
                    }else{
                        $visita->estado_usuario = "Enviar Solicitud";
                    }
                    $visita->mensajes = $this->perfil->getTotalMensajes($visita->id_usuario_visita);
                    $visita->debates = $this->perfil->getTotalDebates($visita->id_usuario_visita);
                    $visita->fotos = $this->perfil->getTotalFotos($visita->id_usuario_visita);
                    $visita->amigos = $this->perfil->getTotalAmigos($visita->id_usuario_visita);
                    $visita->url_debates_participacion = "participacion-$visita->id_usuario_visita";
                    $visita->url_debates = "misdebates-$visita->id_usuario_visita";
                    $visita->url_fotos = "fotos-$visita->id_usuario_visita";
                    $visita->url_amigos = "amigos-$visita->id_usuario_visita";
                    if(!empty($visita->mime)){
                        $visita->url_foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$visita->id_usuario_visita";
                    }else{
                        $visita->url_foto_usuario =  base_url().'dist/img/blog/perfil.png';
                    }
                    $visita->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$visita->id_usuario_visita";
                    $visita->id_usuario = $visita->id_usuario_visita;
                    $visita->url_agregar_amigo = "agregar-$visita->id_usuario_visita";
                    $visita->agregar = "";
                    $visita->lugar = "";
                    if(!empty($visita->poblacion) && !empty($amigo->estado)){
                        $visita->lugar = "$visita->poblacion, $visita->estado";
                    }else if(!empty($visita->poblacion)){
                        $visita->lugar = $visita->poblacion;
                    }else if(!empty($visita->estado)){
                        $visita->lugar = $visita->estado;
                    }
                    $visita->fecha_creacion = new DateTime($visita->fecha_creacion);
                    $mes = $this->formatearMes($visita->fecha_creacion);
                    $anio = $visita->fecha_creacion->format('Y');
                    $fecha_amistad = "Desde $mes de $anio";
                    $visita->fecha_creacion = $fecha_amistad;
                    $visitas2[$i] = $visita;
                    $i++;
                }
                $grupos = $this->comunidad->getGrupos();
                $visitas_perfil = $this->formatearVisitas();
                $usuarios_boda = $this->formatearCoincidencia();
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $amistad = $this->getAmistad($id_usuario);
                $datos = array(
                    'seccion' => 'visitas',
                    'datos_usuario' => $datos_usuario,
                    'visitas' => $visitas2,
                    'total_paginas' => $total_paginas,
                    'contador' => $contador,
                    'pagina' => $pagina,
                    'id_usuario' => $id_usuario,
                    'grupos' => $grupos,
                    'visitas_perfil' => $visitas_perfil,
                    'usuarios_boda' => $usuarios_boda,
                    'grupos_miembro' => $grupos_miembro,
                    'amistad' => $amistad
                ); 
                return $datos;
            }
        }
    }
    
    public function favoritos($id_usuario,$pagina = 1){
        $mi_id_usuario = $this->session->userdata('id_usuario');
        if($id_usuario == $mi_id_usuario){
            $validar = $this->perfil->validarUsuario($id_usuario);
            $i = 0;
            $favoritos2 = "";
            if($validar){
                $inicio = 0;
                $datos_usuario = $this->perfil->getDatosUsuario($id_usuario);
                if($datos_usuario){
                    if(!empty($datos_usuario->fecha_creacion)){
                        $datos_usuario->fecha_creacion = new DateTime($datos_usuario->fecha_creacion);
                        $mes = $this->formatearMes($datos_usuario->fecha_creacion);
                        $anio = $datos_usuario->fecha_creacion->format('Y');
                        $datos_usuario->fecha_creacion = "Desde $mes de $anio";
                    }
                    $contador = $this->perfil->getTotalFavoritos($id_usuario);
                    $total_paginas = (int)($contador / 16);
                    if($total_paginas < ($contador/16)){
                        $total_paginas++;
                    }
                    if($total_paginas == 0){
                            $total_paginas = 1;
                    }
                    if($pagina <= $total_paginas && $pagina > 0){
                        $inicio = ($pagina - 1) * 16;
                        $favoritos = $this->perfil->getFavoritos($id_usuario,$inicio);
                        if($favoritos){
                            foreach ($favoritos as $favorito){
                                $favorito = $this->perfil->formatearDatosActividad($favorito);
                                $favorito->fecha_creacion = relativeTimeFormat($favorito->fecha_creacion, $mini=FALSE);
                                if(strlen($favorito->fecha_creacion) <= 10 && strlen($favorito->fecha_creacion) > 4){
                                    $favorito->fecha_creacion = "Hace $favorito->fecha_creacion";
                                }else{
                                    $favorito->fecha_creacion = "El $favorito->fecha_creacion";
                                }
                                if(empty($favorito->foto_usuario)){
                                    $favorito->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/$favorito->id_usuario";
                                }
                                $favoritos2[$i] = $favorito;
                                $i++;
                            }
                        }
                        $grupos = $this->comunidad->getGrupos();
                        $visitas_perfil = $this->formatearVisitas();
                        $usuarios_boda = $this->formatearCoincidencia();
                        $grupos_miembro = $this->comunidad->getGruposMiembro();
                        $amistad = $this->getAmistad($id_usuario);
                        $datos = array(
                            'seccion' => 'favoritos',
                            'datos_usuario' => $datos_usuario,
                            'favoritos' => $favoritos2,
                            'id_usuario' => $id_usuario,
                            'pagina' => $pagina,
                            'contador' => $contador,
                            'total_paginas' => $total_paginas,
                            'grupos' => $grupos,
                            'visitas_perfil' => $visitas_perfil,
                            'usuarios_boda' => $usuarios_boda,
                            'grupos_miembro' => $grupos_miembro,
                            'amistad' => $amistad,
                            'medalla' => 'mi_perfil'
                        ); 
                        $this->load->view('principal/novia/comunidad/Miperfil/favoritos',$datos);
                    }
                }
            }else{
                $this->error_404();
            }
        }else{
            $this->error_404();
        }
    }
    
    public function formatearVisitas(){
        $visitas_perfil = $this->comunidad->getVisitas();
        $visitas_perfil2 = "";
        $i = 0;
        if($visitas_perfil){
            foreach ($visitas_perfil as $visita){
                $visita->url_usuario = base_url()."index.php/novios/comunidad/Perfil/usuario/$visita->id_usuario";
                if(!empty($visita->mime)){
                    $visita->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$visita->id_usuario";
                }else{
                    $visita->foto_usuario = base_url()."dist/img/blog/perfil.png";
                }
                $usuario = $this->session->userdata('id_usuario');
                $visita->todos = base_url()."index.php/novios/comunidad/Perfil/visitas/$usuario";
                $visitas_perfil2[$i] = $visita;
                $i++;
            }
        }
        return $visitas_perfil2;
    }
    
    public function formatearCoincidencia(){
        $usuarios_boda = $this->comunidad->usuariosBoda();
        if($usuarios_boda){
            if(!empty($usuarios_boda->fecha_boda)){
                $usuarios_boda->fecha_boda = new DateTime($usuarios_boda->fecha_boda);
                $mes = $this->formatearMes($usuarios_boda->fecha_boda);
                $anio = $usuarios_boda->fecha_boda->format('Y');
                $dia = $usuarios_boda->fecha_boda->format('d');
                $usuarios_boda->fecha_boda = "$dia de $mes de $anio";
            }
            if(!empty($usuarios_boda->mime)){
                $usuarios_boda->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/".$this->session->userdata('id_usuario');
            }else{
                $usuarios_boda->foto_usuario = base_url()."dist/img/blog/perfil.png";
            }
            return $usuarios_boda;
        }
    }

    public function solicitudAmistad(){
        $id_usuario_confirmacion = $this->input->post("id_usuario_confirmacion", TRUE);
        $tipo = $this->input->post("tipo", TRUE);
        $result = "";
        switch ($tipo){
            case "Enviar Solicitud":
                $result = $this->perfil->validarAmistad($id_usuario_confirmacion);
                if($result){
                    $data = "Solicitud Enviada";
                }else{
                    $data = $tipo;
                }
                break;
            case "Solicitud Enviada":
                $result = $this->perfil->updateAmistad($id_usuario_confirmacion,0,0);
                if($result){
                    $data = "Enviar Solicitud";
                }else{
                    $data = $tipo;
                }
                break;
            case "Eliminar Amistad":
                $result = $this->perfil->updateAmistad($id_usuario_confirmacion,0,0);
                if($result){
                    $data = "Enviar Solicitud";
                }else{
                    $data = $tipo;
                }
                break;
            case "Aceptar Solicitud":
                $result = $this->perfil->updateAmistad($id_usuario_confirmacion,0,1);
                if($result){
                    $data = "Eliminar Amistad";
                }else{
                    $data = $tipo;
                }
                break;
        }
        if($result){
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(202)
                            ->set_output(json_encode(array(
                                'success' => TRUE,
                                'data' => $data
                            )
                    )
            );
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' =>FALSE,
                            'data' => 'Error'
                        )
                )
        );
    }
    
    public function comentario(){
        $comentario = $this->input->post("comentario", TRUE);
        $id_muro = $this->input->post("muro_usuario", TRUE);
        $result = $this->perfil->setComentario($id_muro,$comentario);
        if($result){
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(202)
                            ->set_output(json_encode(array(
                                'success' => TRUE,
                                'data' => 'Se ha publicado correctamente'
                            )
                    )
            );
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' =>FALSE,
                            'data' => 'Error'
                        )
                )
        );
    }
    
    public function formatearMes($fecha){
        $mes = $fecha->format("F");
        switch ($mes){
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return  $mes;
    }
    
    public function formatearColor($color){
        switch ($color) {
            case 'yellow':
                $color = 'Amarillo';
                break;
            case 'orange':
                $color = 'Anaranjado';
                break;
            case 'blue':
                $color = 'Azul';
                break;
            case 'beige':
                $color = 'Beige';
                break;
            case 'white':
                $color = 'Blanco';
                break;
            case 'white-black':
                $color = 'Blanco y Negro';
                break;
            case 'brown':
                $color = 'Cafe';
                break;
            case 'golden':
                $color = 'Dorado';
                break;
            case 'fucsia':
                $color = 'Fucsia';
                break;
            case 'grey':
                $color = 'Gris';
                break;
            case 'purple':
                $color = 'Morado';
                break;
            case 'black':
                $color = 'Negro';
                break;
            case 'silver':
                $color = 'Plateado';
                break;
            case 'red':
                $color = 'Rojo';
                break;
            case 'pink':
                $color = 'Rosa';
                break;
            case 'green':
                $color = 'Verde';
                break;
            case 'wine':
                $color = 'Vino';
                break;
        }
        return $color;
    }
    
    public function formatearEstacion($estacion){
        switch ($estacion) {
            case 'invierno':
                $estacion = 'Invierno';
                break;
            case 'otono':
                $estacion = 'Oto&ntilde;o';
                break;
            case 'primavera':
                $estacion = 'Primavera';
                break;
            case 'verano':
                $estacion = 'Verano';
                break;
        }
        return $estacion;
    }
    
    public function formatearEstilo($estilo){
        switch ($estilo) {
            case 'aire_libre':
                $estilo = 'Al aire libre';
                break;
            case 'campo':
                $estilo = 'En el campo';
                break;
            case 'de_noche':
                $estilo = 'De noche';
                break;
            case 'elegante':
                $estilo = 'Elegante';
                break;
            case 'playa':
                $estilo = 'En la playa';
                break;
            case 'moderna':
                $estilo = 'Modernas';
                break;
            case 'rustica':
                $estilo = 'R&uacute;sticas';
                break;
            case 'vintage':
                $estilo = 'Vintage';
                break;
        }
        return $estilo;
    }
    
    public function error_404(){
        $data["pais"] =  "Mexico" ;
        $data["tipos"] = $this->tipo_proveedor->getGrupos();
        foreach ($data["tipos"] as $key => $value) {
            $value->tipos = $this->tipo_proveedor->getGrupo($value->nombre);
        }
        $this->load->view('errors/html/error_404.php',$data);
    }
}