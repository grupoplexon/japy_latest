<?php

class Picture extends CI_Controller {

    public $input;

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Comunidad/Comunidad_model", "comunidad");
        $this->load->model("Comunidad/Imagen_model", "imagen");
        $this->load->model("Comunidad/Foto_model", "foto");
        $this->load->model("Comunidad/Video_model", "video");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/";
        $this->load->helper('formats_helper');
    }

//    public function index(){
//        $fotos_recientes = $this->foto->getFotosRecientes();
//        $fotos_vistas = $this->foto->getFotosVistas();
//        $datos = array(
//            'fotos_recientes' => $fotos_recientes,
//            'fotos_vistas' => $fotos_vistas
//        );
//        $this->load->view('principal/novia/comunidad/fotos',$datos);
//    }

    public function foto($id) {
        if ($id == 0) {
            redirect(base_url() . "/dist/img/blog/default.png");
            return;
        }
        $imagen = $this->foto->foto($id);
        if ($imagen) {
            return $this->output
                            ->set_content_type($imagen->mime)
                            ->set_status_header(200)
                            ->set_output($imagen->imagen);
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }

    public function nuevaFoto($mensaje = "") {
        $datos = $this->comunidad->getGrupos();
        $grupo = array(
            'grupos' => $datos
        );
        if (!empty($mensaje) && $mensaje == "archivo_danado") {
            $data = array(
                'grupos' => $datos,
                'mensaje' => $mensaje
            );
            $this->load->view('principal/novia/comunidad/nueva_foto', $data);
        }
        if(!empty($mensaje) && $mensaje > 0 && $mensaje <= 32){
            $data = array('grupos' => $datos, 'id_grupo' => $mensaje);
            $this->load->view('principal/novia/comunidad/nueva_foto', $data);
        }
        $this->load->view('principal/novia/comunidad/nueva_foto', $grupo);
    }

    public function publicarFoto() {
        if ($_POST) {
            $datos['grupos'] = $this->input->post('grupos', TRUE);
            $datos['titulo'] = $this->input->post('titulo', TRUE);
            $datos['imagen'] = $this->input->post('foto');
            $datos['descripcion'] = $this->input->post('contenido');
            $mime = explode(",", $datos['imagen']);
            $foto = base64_decode($mime[1]);
            $mime = explode(";", $mime[0]);
            $mime = explode(":", $mime[0]);
            $mime = $mime[1];
            $bandera = false;
            switch ($mime) {
                case "image/png":
                    $bandera = true;
                    break;
                case "image/gif":
                    $bandera = true;
                    break;
                case "image/jpg":
                    $bandera = true;
                    break;
                case "image/jpeg":
                    $bandera = true;
                    break;
                case "image/bmp":
                    $bandera = true;
                    break;
                case "image/tiff":
                    $bandera = true;
                    break;
            }
            if ($bandera) {
                $datos['imagen'] = $foto;
                $datos['mime'] = $mime;
                $result = $this->foto->publicarFoto($datos);
                $result = "foto" . $datos['grupos'] . "-g" . $result;
                $debates_publicados = $this->comunidad->numero_debates();
                $fotos_publicadas = $this->foto->numero_fotos();
                $videos_publicados = $this->video->numero_videos();
                $contador = $debates_publicados + $fotos_publicadas + $videos_publicados;
                switch ($contador) {
                    case 10:
                        $this->foto->set_medallas($this->session->userdata('id_usuario'), 20);
                        $this->foto->set_medallas($this->session->userdata('id_usuario'), 23);
                        break;
                    case 20:
                        $this->foto->set_medallas($this->session->userdata('id_usuario'), 21);
                        break;
                    case 50:
                        $this->foto->set_medallas($this->session->userdata('id_usuario'), 22);
                        break;
                }
                redirect("novios/comunidad/Picture/fotoPublicada/$result");
            } else {
                redirect("novios/comunidad/Picture/nuevaFoto/archivo_danado");
            }
        }
    }

    public function formatearVisitas() {
        $visitas_perfil = $this->comunidad->getVisitas();
        $visitas_perfil2 = "";
        $i = 0;
        if ($visitas_perfil) {
            foreach ($visitas_perfil as $visita) {
                $visita->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$visita->id_usuario";
                if (!empty($visita->mime)) {
                    $visita->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$visita->id_usuario";
                } else {
                    $visita->foto_usuario = base_url() . "dist/img/blog/perfil.png";
                }
                $usuario = $this->session->userdata('id_usuario');
                $visita->todos = base_url() . "index.php/novios/comunidad/perfil/visitas/$usuario";
                $visitas_perfil2[$i] = $visita;
                $i++;
            }
        }
        return $visitas_perfil2;
    }

    public function formatearCoincidencia() {
        $usuarios_boda = $this->comunidad->usuariosBoda();
        if ($usuarios_boda) {
            if (!empty($usuarios_boda->fecha_boda)) {
                $usuarios_boda->fecha_boda = new DateTime($usuarios_boda->fecha_boda);
                $mes = $this->formatearMes($usuarios_boda->fecha_boda);
                $anio = $usuarios_boda->fecha_boda->format('Y');
                $dia = $usuarios_boda->fecha_boda->format('d');
                $usuarios_boda->fecha_boda = "$dia de $mes de $anio";
            }
            if (!empty($usuarios_boda->mime)) {
                $usuarios_boda->foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/" . $this->session->userdata('id_usuario');
            } else {
                $usuarios_boda->foto_usuario = base_url() . "dist/img/blog/perfil.png";
            }
            return $usuarios_boda;
        }
    }

    public function fotoPublicada($id_foto = "", $pagina = 1) {
        $token = explode("-g", $id_foto);
        if (count($token) == 2) {
            $id_grupo = explode("foto", $token[0]);
            if (count($id_grupo) == 2) {
                $grupos = $this->comunidad->getGrupos();
                $visitas_perfil = $this->formatearVisitas();
                $usuarios_boda = $this->formatearCoincidencia();
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $publicacion = $this->foto->getFotoPublicada($id_grupo[1], $token[1]);
                $like = $this->foto->validarLikeFoto($token[1]);
                $foto_usuario = $this->comunidad->getFoto($this->session->userdata('id_usuario'));
                if ($foto_usuario->mime) {
                    $foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $this->session->userdata('id_usuario');
                } else {
                    $foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                }
                $fecha_bodaD2 = "";
                if ($publicacion) {
                    if (isset($like->activo) && $like->activo == 1) {
                        $like = "Cancelar Me Gusta";
                    } else {
                        $like = "Me Gusta";
                    }
                    $fecha_creacion2 = relativeTimeFormat($publicacion->fecha_creacion, $mini = FALSE);
                    if (strlen($fecha_creacion2) <= 10 && strlen($fecha_creacion2) > 4) {
                        $fecha_creacion2 = "Hace $fecha_creacion2";
                    } else if (strlen($fecha_creacion2) > 10) {
                        $fecha_creacion2 = "El $fecha_creacion2";
                    }
                    if (!empty($publicacion->mime_user)) {
                        $publicacion->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$publicacion->id_usuario";
                    } else {
                        $publicacion->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                    }
                    $tipo_novia = "";
                    if (!empty($publicacion->tipo_novia)) {
                        $tipo_novia = $this->validarTipo_usuario($publicacion->tipo_novia, $publicacion->id_usuario);
                    }
                    $contadorR = $this->foto->getContadorFotos($token[1]);
                    $contador = $contadorR->comentarios;
                    $boda_user = $this->comunidad->getFechaBoda();
                    if (isset($boda_user->fecha_boda) && $boda_user->fecha_boda != null) {
                        $fecha_bodaD = new DateTime($boda_user->fecha_boda);
                        $mes = $this->formatearMes($fecha_bodaD);
                        $anio = $fecha_bodaD->format("Y");
                        $dia = $fecha_bodaD->format("d");
                        $fecha_bodaD = $mes . " " . $anio;
                        $fecha_bodaD2 = $dia . " de " . $mes . " de " . $anio;
                    }
                    $url_siguiente = $this->foto->getId_siguiente($id_grupo[1], $token[1]);
                    $url_anterior = $this->foto->getId_anterior($id_grupo[1], $token[1]);
                    if (empty($url_siguiente)) {
                        $url_siguiente = "";
                    } else {
                        $url_siguiente = base_url() . "index.php/novios/comunidad/Picture/fotoPublicada/foto" . $publicacion->id_grupo . "-g" . $url_siguiente->id_foto;
                    }
                    if (empty($url_anterior)) {
                        $url_anterior = "";
                    } else {
                        $url_anterior = base_url() . "index.php/novios/comunidad/Picture/fotoPublicada/foto" . $publicacion->id_grupo . "-g" . $url_anterior->id_foto;
                    }
                    $this->foto->setVista($token[1]);
                    $vistas = $this->foto->getVistasFotos($token[1]);
                    $comentarios2 = "";
                    $comentarios3 = "";
                    if (isset($pagina) || $contador > 0 && $contador <= 10) {
                        $total_paginas = (int) ($contador / 16);
                        if ($total_paginas < ($contador / 16)) {
                            $total_paginas++;
                        }
                        if ($total_paginas == 0) {
                            $total_paginas = 1;
                        }
                        if (!empty($pagina) && !is_numeric($pagina)) {
                            $pagina = $total_paginas + 1;
                        }
                        if ($pagina <= $total_paginas && $pagina > 0) {
                            $inicio = ($pagina - 1) * 16;
                            $limite = $pagina * 16;
                            $limite = $contador - $limite;
                            $comentarios = $this->foto->getComentariosFoto($token[1], $inicio, $limite);
                            $respuestaComentarios = $this->foto->getRespuestaComentario($token[1]);
                            $i = 0;
                            $fecha_creacion = "";
                            $fecha_boda = "";
                            foreach ($comentarios as $comentario) {
                                if ($comentario->fecha_boda != "" && $comentario->fecha_boda != NULL) {
                                    $fecha_boda = new DateTime($comentario->fecha_boda);
                                    $mes = $this->formatearMes($fecha_boda);
                                    $anio = $fecha_boda->format("Y");
                                    $fecha_boda = $mes . " " . $anio;
                                }
                                if ($comentario->fecha_creacion) {
                                    $fecha_creacion = relativeTimeFormat($comentario->fecha_creacion, $mini = FALSE);
                                    if (strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4) {
                                        $fecha_creacion = "Hace $fecha_creacion";
                                    } else {
                                        $fecha_creacion = "El $fecha_creacion";
                                    }
                                }
                                if (!empty($comentario->tipo_novia)) {
                                    $comentario->tipo_novia = $this->validarTipo_usuario($comentario->tipo_novia, $comentario->id_usuario);
                                }
                                $comentario->fecha_boda = $fecha_boda;
                                $comentario->fecha_creacion = $fecha_creacion;
                                if (!empty($comentario->mime)) {
                                    $comentario->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$comentario->id_usuario";
                                } else {
                                    $comentario->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                                }
                                $comentarios2[$i] = $comentario;
                                $i++;
                            }
                            foreach ($respuestaComentarios as $comentario) {
                                if ($comentario->fecha_boda != "" && $comentario->fecha_boda != NULL) {
                                    $fecha_boda = new DateTime($comentario->fecha_boda);
                                    $mes = $this->formatearMes($fecha_boda);
                                    $anio = $fecha_boda->format("Y");
                                    $fecha_boda = $mes . " " . $anio;
                                }
                                if ($comentario->fecha_creacion) {
                                    $fecha_creacion = relativeTimeFormat($comentario->fecha_creacion, $mini = FALSE);
                                    if (strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4) {
                                        $fecha_creacion = "Hace $fecha_creacion";
                                    } else {
                                        $fecha_creacion = "El $fecha_creacion";
                                    }
                                }
                                if (!empty($comentario->mime)) {
                                    $comentario->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$comen->id_usuario";
                                } else {
                                    $comentario->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                                }
                                $comentario->fecha_boda = $fecha_boda;
                                $comentario->fecha_creacion = $fecha_creacion;
                                $comentarios3[$i] = $comentario;
                                $i++;
                            }
                            $id_denuncia = "";
                            if(!empty($publicacion->id_denuncia)){
                                $id_denuncia = $publicacion->id_denuncia;
                            }
                            $data = array(
                                'grupo' => $publicacion->nombre,
                                'imagen' => $publicacion->imagen,
                                'id_grupo' => $publicacion->id_grupos_comunidad,
                                'url_grupo' => base_url() . "index.php/novios/comunidad/Group/grupo/$publicacion->id_grupo/todo",
                                'url_perfil' => base_url() . "index.php/novios/comunidad/perfil/usuario/" . $publicacion->id_usuario,
                                'id_usuario' => $publicacion->id_usuario,
                                'url_siguiente' => $url_siguiente,
                                'url_anterior' => $url_anterior,
                                'titulo_foto' => $publicacion->titulo,
                                'id_foto' => $token[1],
                                'descripcion' => $publicacion->descripcion,
                                'usuario' => $publicacion->usuario,
                                'mime' => $publicacion->mime,
                                'imagen' => $publicacion->imagen,
                                'foto_usuario' => $publicacion->foto_usuario,
                                'foto_logeado' => $foto_usuario,
                                'fecha_boda2' => $fecha_bodaD2,
                                'estado_boda' => $boda_user->estado_boda,
                                'fecha_creacion' => $fecha_creacion2,
                                'contador' => $contador,
                                'total_paginas' => $total_paginas,
                                'pagina' => $pagina,
                                'vistas' => $vistas->vistas,
                                'relacion' => '0',
                                'visita_perfil' => '',
                                'like' => $like,
                                'url_foto' => $id_foto,
                                'comentarios' => $comentarios2,
                                'comentarios2' => $comentarios3,
                                'grupos' => $grupos,
                                'visitas_perfil' => $visitas_perfil,
                                'usuarios_boda' => $usuarios_boda,
                                'grupos_miembro' => $grupos_miembro,
                                'tipo_novia' => $tipo_novia,
                                'id_denuncia' => $id_denuncia
                            );
                            $this->load->view('principal/novia/comunidad/foto_publicada', $data);
                        } else {
                            $this->error_404();
                        }
                    }
                } else {
                    $this->error_404();
                }
            } else {
                $this->error_404();
            }
        } else {
            $this->error_404();
        }
    }

    public function likeFoto() {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $id_foto = $this->input->post("foto", TRUE);
            $result = $this->foto->validarLikeFoto($id_foto);
            if (isset($result->activo) && $result->activo == 0) {
                $result = $this->foto->updateLikeFoto($id_foto, 1, $result->id_me_gusta);
                $data = "Cancelar Me Gusta";
            } else if (isset($result->activo) && $result->activo == 1) {
                $result = $this->foto->updateLikeFoto($id_foto, 0, $result->id_me_gusta);
                $data = "Me Gusta";
            } else {
                $result = $this->foto->setLikeFoto($id_foto);
                $data = "Cancelar Me Gusta";
            }
            if ($result) {
                return $this->output
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => TRUE,
                                    'data' => $data,
                                        ))
                );
            }
        }
        return $this->output
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => FALSE,
                            'data' => 'error'
                                ))
        );
    }

//    public function recientes($pagina = 1){
//        $contador = $this->foto->getTotalFotos();
//        if(isset($pagina) || $contador > 0 && $contador <= 16){
//            $total_paginas = (int)($contador / 16);
//            if($total_paginas == 0){
//                $total_paginas = 1;
//            }
//            if($pagina <= $total_paginas){
//                $inicio = ($pagina - 1) * 16;
//                $limite = $pagina * 16;
//                $limite = $contador - $limite;
//                $result = $this->foto->getRecientes($inicio,$limite);
//            }
//        }
//        $dato = array(
//            'titulo' => 'Ultimas Fotos Publicadas',
//            'pagina' => $pagina,
//            'contador' => $contador,
//            'total_paginas' => $total_paginas,
//            'ordenamiento' => 'recientes',
//            'fotos' => $result
//        );
//        $this->load->view('principal/novia/comunidad/filtro_fotos',$dato);
//    }
//    
//    public function vistas($pagina = 1){
//        $contador = $this->foto->getTotalFotos();
//        if(isset($pagina) || $contador > 0 && $contador <= 16){
//            $total_paginas = (int)($contador / 16);
//            if($total_paginas == 0){
//                $total_paginas = 1;
//            }
//            if($pagina <= $total_paginas){
//                $inicio = ($pagina - 1) * 16;
//                $limite = $pagina * 16;
//                $limite = $contador - $limite;
//                $result = $this->foto->getVistas($inicio,$limite);
//            }
//        }
//        $dato = array(
//            'titulo' => 'Fotos Mas Visitadas',
//            'pagina' => $pagina,
//            'contador' => $contador,
//            'total_paginas' => $total_paginas,
//            'ordenamiento' => 'vistas',
//            'fotos' => $result
//        );
//        $this->load->view('principal/novia/comunidad/filtro_fotos',$dato);
//    }
//    
//    public function comentadas($pagina = 1){
//        $contador = $this->foto->getTotalFotos();
//        if(isset($pagina) || $contador > 0 && $contador <= 16){
//            $total_paginas = (int)($contador / 16);
//            if($total_paginas == 0){
//                $total_paginas = 1;
//            }
//            if($pagina <= $total_paginas){
//                $inicio = ($pagina - 1) * 16;
//                $limite = $pagina * 16;
//                $limite = $contador - $limite;
//                $result = $this->foto->getComentadas($inicio,$limite);
//            }
//        }
//        $dato = array(
//            'titulo' => 'Fotos Mas Comentadas',
//            'pagina' => $pagina,
//            'contador' => $contador,
//            'total_paginas' => $total_paginas,
//            'ordenamiento' => 'comentados',
//            'fotos' => $result
//        );
//        $this->load->view('principal/novia/comunidad/filtro_fotos',$dato);
//    }

    public function formatearMes($fecha) {
//        echo date('Y-m-d h:i:s');
//        $result = $this->comunidad->conocerCompaneros();
//        $fecha = new DateTime($result->fecha_boda);
//        echo $fecha->format("F Y");
        $mes = $fecha->format("F");
        $anio = $fecha->format("Y");
        switch ($mes) {
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return $mes;
    }

    public function comentariosFoto() {
        if ($_POST) {
            $datos['foto'] = $this->input->post('foto', TRUE);
            $datos['mensaje'] = $this->input->post('mensaje');
            $datos['permiso_notificacion'] = $this->input->post('permiso_notificacion', TRUE);
            $datos['fecha_creacion'] = date('Y-m-d H:i:s');
            if ($datos['permiso_notificacion'] != NULL) {
                $datos['permiso_notificacion'] = 1;
            } else {
                $datos['permiso_notificacion'] = 0;
            }
            $result = $this->foto->comentariosFotos($datos);
            if ($result) {
                if (!empty($result->mime)) {
                    $datos['foto_usuario'] = base_url() . "index.php/novios/comunidad/home/foto_usuario/" . $this->session->userdata('id_usuario');
                } else {
                    $datos['foto_usuario'] = base_url() . 'dist/img/blog/perfil.png';
                }
                if (!empty($result->tipo_novia)) {
                    $datos['tipo_novia'] = $this->validarTipo_usuario($result->tipo_novia, $result->id_usuario);
                }
                $fecha = new DateTime($result->fecha_boda);
                $mes = $this->formatearMes($fecha);
                $anio = $fecha->format("Y");
                $fecha_creacion = relativeTimeFormat($datos['fecha_creacion'], $mini = FALSE);
                $fecha = $mes . " " . $anio;
                $datos['fecha_boda'] = $fecha;
                $datos['estado_boda'] = $result->estado_boda;
                $datos['fecha_creacion'] = $fecha_creacion;
                $datos['usuario'] = $result->usuario;
                $datos['id_comentario'] = $result->id_comentario;
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $datos,
                                                )
                                        )
                );
            }
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }

    public function respuestaComentario() {
        if ($_POST) {
            $datos['foto'] = $this->input->post('foto', TRUE);
            $datos['mensaje'] = $this->input->post('mensaje');
            $datos['respuesta'] = $this->input->post('respuesta', TRUE);
            $datos['fecha_creacion'] = date('Y-m-d H:i:s');
            $result = $this->foto->respuestaComentario($datos);
            if ($result) {
                if (!empty($result->mime)) {
                    $foto_usuario = base_url() . "index.php/novios/comunidad/home/foto_usuario/$result->usuarioCO";
                } else {
                    $foto_usuario = base_url() . 'dist/img/blog/perfil.png';
                }
                $fecha = new DateTime($result->fecha_boda);
                $mes = $this->formatearMes($fecha);
                $anio = $fecha->format("Y");
                $fecha_creacion = relativeTimeFormat($datos['fecha_creacion'], $mini = FALSE);
                $fecha = $mes . " " . $anio;
                $fecha_boda = $fecha;
                $estado_boda = $result->estado_boda;
                $fecha_creacion2 = $fecha_creacion;
                $usuario = $result->usuario;
                $id_comentario = $result->id_comentario;
                if (!empty($result->tipo_novia)) {
                    $tipo_novia = $this->validarTipo_usuario($result->tipo_novia, $result->id_usuario);
                }
                $data = array(
                    'mensaje' => $datos['mensaje'],
                    'url_foto' => $foto_usuario,
                    'fecha_boda' => $fecha_boda,
                    'estado_boda' => $estado_boda,
                    'fecha_creacion' => $fecha_creacion2,
                    'usuario' => $usuario,
                    'mensaje' => $id_comentario,
                    'comentario' => $datos['mensaje'],
                    'tipo_novia' => $tipo_novia,
                    'id_comentario' => $result->id_comentario
                );
            }
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(202)
                            ->set_output(json_encode(array(
                                'success' => true,
                                'data' => $data,
                                            )
                                    )
            );
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }

    function getRespuesta() {
        if ($_POST) {
            $id_comentario = $this->input->post('comentario', TRUE);
            $result = $this->foto->getRespuesta($id_comentario);
            if ($result) {
                $i = 0;
                foreach ($result as $respuesta) {
                    $fecha_boda = "";
                    if ($respuesta->fecha_boda != "" && $respuesta->fecha_boda != NULL) {
                        $fecha_boda = new DateTime($respuesta->fecha_boda);
                        $mes = $this->formatearMes($fecha_boda);
                        $anio = $fecha_boda->format("Y");
                        $fecha_boda = $mes . " " . $anio;
                    }
                    if ($respuesta->fecha_creacion) {
                        $fecha_creacion = relativeTimeFormat($respuesta->fecha_creacion, $mini = FALSE);
                        if (strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4) {
                            $fecha_creacion = "Hace $fecha_creacion";
                        } else {
                            $fecha_creacion = "El $fecha_creacion";
                        }
                    }
                    if (!empty($respuesta->mime)) {
                        $respuesta->url_foto = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $respuesta->usuarioCO;
                    } else {
                        $respuesta->url_foto = base_url() . 'dist/img/blog/perfil.png';
                    }
                    $denunciado = $this->comunidad->comentario_denunciado($respuesta->id_comentario,2);
                    if(!empty($denunciado->id_comentario) && $denunciado->id_comentario == $respuesta->id_comentario){
                        $respuesta->id_denuncia = $denunciado->id_comentario;
                    }else{
                        $respuesta->id_denuncia = "";
                    }
                    $respuesta->fecha_boda = $fecha_boda;
                    $respuesta->fecha_creacion = $fecha_creacion;
                    if (!empty($respuesta->tipo_novia)) {
                        $respuesta->tipo_novia = $this->validarTipo_usuario($respuesta->tipo_novia, $respuesta->id_usuario);
                    }
                    $respuesta2[$i] = $respuesta;
                    $i++;
                }
                $datos = array('respuestas' => $respuesta2);
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $datos,
                                                )
                                        )
                );
            }
            $datos = array('respuestas' => $respuesta2);
            return $this->output
                            ->set_content_type('application/json')
                            ->set_status_header(202)
                            ->set_output(json_encode(array(
                                'success' => true,
                                'data' => $datos,
                                            )
                                    )
            );
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }

    public function preview($id_foto) {
        $imagen = $this->foto->getFoto($id_foto);
        if ($imagen) {
            return $this->output
                            ->set_content_type($imagen->mime)
                            ->set_status_header(200)
                            ->set_output($imagen->imagen);
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }

    public function denuncia() {
        if ($_POST) {
            $foto = $this->input->post('foto', true);
            $razon = $this->input->post('razon', true);
            $result = $this->foto->denuncia($foto, $razon);
            if ($result) {
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(200)
                                ->set_output("true");
            }
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }

    public function validarTipo_usuario($tipo, $id_usuario) {
        $novio = "";
        switch ($tipo) {
            case 1:
                $genero = $this->db->query("SELECT genero FROM usuario U INNER JOIN cliente C USING (id_usuario) WHERE id_usuario = $id_usuario")->row();
                if (!empty($genero) && $genero->genero == 1) {
                    $novio = "Nuevo";
                } else if (!empty($genero) && $genero->genero == 2) {
                    $novio = "Nueva";
                }
                break;
            case 2:
                $novio = "Principiante";
                break;
            case 3:
                $novio = "Habitual";
                break;
            case 4:
                $novio = "Top";
                break;
            case 5:
                $novio = "Pro";
                break;
            case 6:
                $novio = "VIP";
                break;
            case 7:
                $novio = "Super";
                break;
            case 8:
                $genero = $this->db->query("SELECT genero FROM usuario U INNER JOIN cliente C USING (id_usuario) WHERE id_usuario = $id_usuario")->row();
                if (!empty($genero) && $genero->genero == 1) {
                    $novio = "Destacado";
                } else if (!empty($genero) && $genero == 2) {
                    $novio = "Destacada";
                }
                break;
            case 9:
                $novio = "Leyenda";
                break;
        }
        return $novio;
    }

    public function error_404() {
        $data["pais"] = "Mexico";
        $data["tipos"] = $this->tipo_proveedor->getGrupos();
        foreach ($data["tipos"] as $key => $value) {
            $value->tipos = $this->tipo_proveedor->getGrupo($value->nombre);
        }
        $this->load->view('errors/html/error_404.php', $data);
    }

}
