<?php
class Resource extends CI_Controller {

    public $input;    

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Comunidad_model", "comunidad");
        $this->load->model("Comunidad/Imagen_model", "imagen");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/resource/imagen/";
    }

    public function imagen($id) {
        if ($id == 0) {
            redirect(base_url() . "/dist/img/blog/default.png");
            return;
        }
        $imagen = $this->imagen->get($id);
        if ($imagen) {
            return $this->output
                            ->set_content_type($imagen->mime)
                            ->set_status_header(200)
                            ->set_output($imagen->imagen);
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }

    public function subir($type) {
        if ($_POST) {
            switch (strtoupper($type)) {
                case "IMAGEN":
                    $imagen = $this->input->post("archivo");
                    $mime = explode(",", $imagen);
                    $imagen = base64_decode($mime[1]);
                    $mime = explode(";", $mime[0]);
                    $mime = explode(":", $mime[0]);
                    $mime = $mime[1];
                    $datos = array("imagen" => $imagen, "mime" => $mime);
                    $b = $this->imagen->insert($datos);
                    if ($b) {
                        return $this->output
                                        ->set_content_type('application/json')
                                        ->set_status_header(200)
                                        ->set_output(json_encode(array(
                                            'success' => true,
                                            'data' => $this->config->base_url() . "index.php/novios/comunidad/resource/imagen/" . $this->imagen->last_id()
                        )));
                    }
                default:
                    break;
            }
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }
}
