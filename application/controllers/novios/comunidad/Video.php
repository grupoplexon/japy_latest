<?php

class Video extends CI_Controller {
    public $input;    

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Comunidad/Comunidad_model", "comunidad");
        $this->load->model("Comunidad/Imagen_model", "imagen");
        $this->load->model("Comunidad/Video_model", "video");
        $this->load->model("Comunidad/Foto_model", "foto");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/";
        $this->load->helper('formats_helper');
    }
    
//    public function index(){
//        $videos_recientes = $this->video->getVideosRecientes();
//        $videos_vistos = $this->video->getVideosVistos();
//        $datos = array(
//            'videos_recientes' => $videos_recientes,
//            'videos_vistos' => $videos_vistos
//        );
//        $this->load->view('principal/novia/comunidad/videos',$datos);
//    }
    
    public function nuevoVideo($id_grupo = ""){
        $datos = $this->comunidad->getGrupos();
        if(!empty($id_grupo) && $id_grupo > 0 && $id_grupo <= 32){
            $data = array(
                'grupos' => $datos,
                'id_grupo' => $id_grupo
            );
            $this->load->view('principal/novia/comunidad/nuevo_video',$data);
        }else{
            $grupo = array(
                'grupos' => $datos
            );
            $this->load->view('principal/novia/comunidad/nuevo_video',$grupo);
        }
    }
    
    public function publicarVideo(){
        if($_POST){
            $dato['grupo'] = $this->input->post('grupos',true);
            $dato['titulo'] = $this->input->post('titulo',true);
            $dato['descripcion'] = $this->input->post('descripcion');
            $dato['direccion_video'] = $this->input->post('direccion_video',TRUE);
            $dato['direccion_video'] = str_replace("watch?v=","embed/",$dato['direccion_video']);
            $result = $this->video->publicarVideo($dato);
            $result = "video".$dato['grupo']."-g".$result;
            $debates_publicados = $this->comunidad->numero_debates();
            $fotos_publicadas = $this->foto->numero_fotos();
            $videos_publicados = $this->video->numero_videos();
            $contador = $debates_publicados + $fotos_publicadas + $videos_publicados;
            switch ($contador){
                case 10:
                    $this->video->set_medallas($this->session->userdata('id_usuario'),20);
                    $this->video->set_medallas($this->session->userdata('id_usuario'),23);
                    break;
                case 20:
                    $this->video->set_medallas($this->session->userdata('id_usuario'),21);
                    break;
                case 50:
                    $this->video->set_medallas($this->session->userdata('id_usuario'),22);
                    break;
            }
            redirect("novios/comunidad/Video/videoPublicado/$result");
        }
    }
    
    public function formatearVisitas(){
        $visitas_perfil = $this->comunidad->getVisitas();
        $visitas_perfil2 = "";
        $i = 0;
        if($visitas_perfil){
            foreach ($visitas_perfil as $visita){
                $visita->url_usuario = base_url()."index.php/novios/comunidad/perfil/usuario/$visita->id_usuario";
                if(!empty($visita->mime)){
                    $visita->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$visita->id_usuario";
                }else{
                    $visita->foto_usuario = base_url()."dist/img/blog/perfil.png";
                }
                $usuario = $this->session->userdata('id_usuario');
                $visita->todos = base_url()."index.php/novios/comunidad/perfil/visitas/$usuario";
                $visitas_perfil2[$i] = $visita;
                $i++;
            }
        }
        return $visitas_perfil2;
    }
    
    public function formatearCoincidencia(){
        $usuarios_boda = $this->comunidad->usuariosBoda();
        if($usuarios_boda){
            if(!empty($usuarios_boda->fecha_boda)){
                $usuarios_boda->fecha_boda = new DateTime($usuarios_boda->fecha_boda);
                $mes = $this->formatearMes($usuarios_boda->fecha_boda);
                $anio = $usuarios_boda->fecha_boda->format('Y');
                $dia = $usuarios_boda->fecha_boda->format('d');
                $usuarios_boda->fecha_boda = "$dia de $mes de $anio";
            }
            if(!empty($usuarios_boda->mime)){
                $usuarios_boda->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/".$this->session->userdata('id_usuario');
            }else{
                $usuarios_boda->foto_usuario = base_url()."dist/img/blog/perfil.png";
            }
            return $usuarios_boda;
        }
    }
    
    public function videoPublicado($id_video = "", $pagina = 1){
        $token = explode("-g", $id_video);
        if(count($token) == 2){
            $id_grupo = explode("video", $token[0]);
            if(count($id_grupo) == 2){
                $grupos = $this->comunidad->getGrupos();
                $visitas_perfil = $this->formatearVisitas();
                $usuarios_boda = $this->formatearCoincidencia();
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $publicacion = $this->video->getVideoPublicado($id_grupo[1],$token[1]);
                $like = $this->video->validarLikeVideo($token[1]);
                $foto_usuario = $this->comunidad->getFoto($this->session->userdata('id_usuario'));
                if(!empty($foto_usuario->mime)){
                    $foto_logeado = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$this->session->userdata('id_usuario');
                }else{
                    $foto_logeado = base_url().'dist/img/blog/perfil.png';
                }
                if($publicacion){
                    if(isset($like->activo) && $like->activo == 1){
                        $like = "Cancelar Me Gusta";
                    }else{
                        $like = "Me Gusta";
                    }
                    $fecha_creacion2 = relativeTimeFormat($publicacion->fecha_creacion,$mini=FALSE);
                    if(strlen($fecha_creacion2) <= 10 && strlen($fecha_creacion2) > 4){
                        $fecha_creacion2 = "Hace $fecha_creacion2";
                    }else if(strlen($fecha_creacion2) > 10){
                        $fecha_creacion2 = "El $fecha_creacion2";
                    }
                    if(!empty($publicacion->mime_user)){
                        $publicacion->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$publicacion->id_usuario";
                    }else{
                        $publicacion->foto_usuario = base_url().'dist/img/blog/perfil.png';
                    }
                    $contador2 = $this->video->getContadorVideos($token[1]);
                    $contador = $contador2->comentarios;
                    $boda_user = $this->comunidad->getFechaBoda();
                    if(isset($boda_user->fecha_boda) && $boda_user->fecha_boda != null){
                        $fecha_bodaD = new DateTime($boda_user->fecha_boda);
                        $mes = $this->formatearMes($fecha_bodaD);
                        $anio = $fecha_bodaD->format("Y");
                        $dia = $fecha_bodaD->format("d");
                        $fecha_bodaD = $mes." ".$anio;
                        $fecha_bodaD2 = $dia." de ".$mes." de ".$anio;
                    }
                    $this->video->setVistaVideo($token[1]);
                    $vistas = $this->video->getVistasVideos($token[1]);
                    $comentarios2 = "";
                    $comentarios3 = "";
                    $fecha_bodaD2 = "";
                    if(isset($pagina) || $contador > 0 && $contador <= 10){
                        $total_paginas = (int)($contador / 16);
                        if($total_paginas < ($contador/16)){
                            $total_paginas++;
                        }
                        if($total_paginas == 0){
                                $total_paginas = 1;
                        }
                        if(!empty($pagina) && !is_numeric($pagina)){
                            $pagina = $total_paginas + 1;
                        }
                        if($pagina <= $total_paginas && $pagina > 0){
                            $inicio = ($pagina - 1) * 10;
                            $limite = $pagina * 10;
                            $limite = $contador - $limite;
                            $comentarios = $this->video->getComentariosVideo($token[1],$inicio,$limite);
                            $respuestaComentarios = $this->video->getRespuestaComentarioVideo($token[1]);
                            $i = 0;
                            $fecha_creacion = "";
                            $fecha_boda = "";
                            foreach ($comentarios as $comentario){
                                if($comentario->fecha_boda != "" && $comentario->fecha_boda != NULL){
                                    $fecha_boda = new DateTime($comentario->fecha_boda);
                                    $mes = $this->formatearMes($fecha_boda);
                                    $anio = $fecha_boda->format("Y");
                                    $fecha_boda = $mes." ".$anio;
                                }
                                if($comentario->fecha_creacion){
                                    $fecha_creacion = relativeTimeFormat($comentario->fecha_creacion,$mini=FALSE);
                                    if(strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4){
                                        $fecha_creacion = "Hace $fecha_creacion";
                                    }else if(strlen($fecha_creacion2) > 10){
                                        $fecha_creacion = "El $fecha_creacion";
                                    }
                                }
                                if(!empty($comentario->mime)){
                                    $comentario->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$comentario->id_usuario";
                                }else{
                                    $comentario->foto_usuario = base_url().'dist/img/blog/perfil.png';
                                }
                                if(!empty($comentario->tipo_novia)){
                                    $comentario->tipo_novia = $this->validarTipo_usuario($comentario->tipo_novia,$comentario->id_usuario);
                                }
                                $comentario->fecha_boda = $fecha_boda;
                                $comentario->fecha_creacion = $fecha_creacion;
                                $comentarios2[$i] = $comentario;
                                $i++;
                            }
                            foreach ($respuestaComentarios as $comentario){
                                if($comentario->fecha_boda != "" && $comentario->fecha_boda != NULL){
                                    $fecha_boda = new DateTime($comentario->fecha_boda);
                                    $mes = $this->formatearMes($fecha_boda);
                                    $anio = $fecha_boda->format("Y");
                                    $fecha_boda = $mes." ".$anio;
                                }
                                if($comentario->fecha_creacion){
                                    $fecha_creacion = relativeTimeFormat($comentario->fecha_creacion,$mini=FALSE);
                                    if(strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4){
                                        $fecha_creacion = "Hace $fecha_creacion";
                                    }else{
                                        $fecha_creacion = "El $fecha_creacion";
                                    }
                                }
                                $comentario->fecha_boda = $fecha_boda;
                                $comentario->fecha_creacion = $fecha_creacion;
                                $comentarios3[$i] = $comentario;
                                $i++;
                            }
                            $id_denuncia = "";
                            if(!empty($publicacion->id_denuncia)){
                                $id_denuncia = $publicacion->id_denuncia;
                            }
                            $data = array(
                                'grupo' => $publicacion->nombre,
                                'url_grupo' => base_url()."index.php/novios/comunidad/group/grupo/$publicacion->id_grupo/todo",
                                'url_perfil' => base_url()."index.php/novios/comunidad/perfil/usuario/".$publicacion->id_usuario,
                                'id_usuario' => $publicacion->id_usuario,
                                'titulo_video' => $publicacion->titulo,
                                'id_video' => $token[1],
                                'descripcion' => $publicacion->descripcion,
                                'direccion_web' => $publicacion->direccion_web,
                                'usuario' => $publicacion->usuario,
                                'foto_usuario' => $publicacion->foto_usuario,
                                'foto_logeado' => $foto_logeado,
                                'fecha_boda2' => $fecha_bodaD2,
                                'estado_boda' => $boda_user->estado_boda,
                                'fecha_creacion' => $fecha_creacion2,
                                'contador' => $contador,
                                'total_paginas' => $total_paginas,
                                'pagina' => $pagina,
                                'vistas' => $vistas->vistas,
                                'url_video' => $id_video,
                                'relacion' => '0',
                                'visita_perfil' => '',
                                'like' => $like,
                                'comentarios' => $comentarios2,
                                'comentarios2' => $comentarios3,
                                'grupos' => $grupos,
                                'visitas_perfil' =>$visitas_perfil,
                                'usuarios_boda' => $usuarios_boda,
                                'grupos_miembro' => $grupos_miembro,
                                'id_denuncia' => $id_denuncia
                            ); 
                            $this->load->view('principal/novia/comunidad/video_publicado',$data);
                        }else{
                            $this->error_404();
                        }
                    }
                }else{
                    $this->error_404();
                }
            }else{
                $this->error_404();
            }
        }else{
            $this->error_404();
        }
    }
    
    public function likeVideo(){
        if($_POST){
            $id_video = $this->input->post("video", TRUE);
            $result = $this->video->validarLikeVideo($id_video);
            if(isset($result->activo) && $result->activo == 0){
                $result = $this->video->updateLikeVideo($id_video,1,$result->id_me_gusta);
                $data = "Cancelar Me Gusta";
            }else if(isset ($result->activo) && $result->activo == 1){
                $result = $this->video->updateLikeVideo($id_video,0,$result->id_me_gusta);
                $data = "Me Gusta";
            }else{
                $result = $this->video->setLikeVideo($id_video);
                $data = "Cancelar Me Gusta";
            }
            if($result){
                return $this->output
                                    ->set_content_type('application/json')
                                    ->set_status_header(202)
                                    ->set_output(json_encode(array(
                                        'success' => TRUE,
                                        'data' => $data,
                                    )
                            )
                );
            }
            return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(404)
                                ->set_output(json_encode(array(
                                    'success' => FALSE,
                                    'data' => 'error'
                                )
                        )
            );
        }
    }
    
    public function formatearMes($fecha){
//        echo date('Y-m-d h:i:s');
//        $result = $this->comunidad->conocerCompaneros();
//        $fecha = new DateTime($result->fecha_boda);
//        echo $fecha->format("F Y");
        $mes = $fecha->format("F");
        $anio = $fecha->format("Y");
        switch ($mes){
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return  $mes;
    }
    
    public function comentariosVideo(){
        if($_POST){
            $datos['video'] = $this->input->post('video', TRUE);
            $datos['mensaje'] = $this->input->post('mensaje');
            $datos['permiso_notificacion'] = $this->input->post('permiso_notificacion', TRUE);
            $datos['fecha_creacion'] = date('Y-m-d H:i:s');
            if($datos['permiso_notificacion'] != NULL){
                $datos['permiso_notificacion'] = 1;
            }else{
                $datos['permiso_notificacion'] = 0;
            }
            $result = $this->video->comentariosVideo($datos);
            if($result){
                if(!empty($result->mime)){
                    $datos['foto_usuario'] = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$this->session->userdata('id_usuario');
                }else{
                    $datos['foto_usuario'] = base_url().'dist/img/blog/perfil.png';
                }
                $tipo_novia = "";
                if(!empty($result->tipo_novia)){
                    $tipo_novia = $this->validarTipo_usuario($result->tipo_novia, $result->id_usuario);
                }
                $fecha = new DateTime($result->fecha_boda);
                $mes = $this->formatearMes($fecha);
                $anio = $fecha->format("Y");
                $fecha_creacion = relativeTimeFormat($datos['fecha_creacion'],$mini=FALSE);
                $fecha = $mes." ".$anio;
                $datos['fecha_boda'] = $fecha;
                $datos['estado_boda'] = $result->estado_boda;
                $datos['fecha_creacion'] = $fecha_creacion;
                $datos['usuario'] = $result->usuario;
                $datos['id_comentario'] = $result->id_comentario;
                $datos['tipo_novia'] = $tipo_novia;
            }
            return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $datos,
                                )
                        )
                );
            
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }
    
    public function respuestaComentario(){
        if($_POST){
            $datos['video'] = $this->input->post('video', TRUE);
            $datos['mensaje'] = $this->input->post('mensaje');
            $datos['respuesta'] = $this->input->post('respuesta', TRUE);
            $datos['fecha_creacion'] = date('Y-m-d H:i:s');
            $result = $this->video->respuestaComentarioVideo($datos);
            if($result){
                //$foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$result->usuarioCO;
                $fecha = new DateTime($result->fecha_boda);
                $mes = $this->formatearMes($fecha);
                $anio = $fecha->format("Y");
                $fecha_creacion = relativeTimeFormat($datos['fecha_creacion'],$mini=FALSE);
                $fecha = $mes." ".$anio;
                $fecha_boda = $fecha;
                $estado_boda = $result->estado_boda;
                $fecha_creacion2 = $fecha_creacion;
                $usuario = $result->usuario;
                $id_comentario = $result->id_comentario;
                if(!empty($result->mime)){
                    $foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$result->usuarioCO;
                }else{
                    $foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                $tipo_novia = "";
                if(!empty($result->tipo_novia)){
                    $tipo_novia = $this->validarTipo_usuario($result->tipo_novia, $result->id_usuario);
                }
                $data = array(
                    'mensaje' => $datos['mensaje'],
                    'url_foto' => $foto_usuario,
                    'fecha_boda' => $fecha_boda,
                    'estado_boda' => $estado_boda,
                    'fecha_creacion' => $fecha_creacion2,
                    'usuario' => $usuario,
                    'mensaje' => $id_comentario,
                    'comentario' => $datos['mensaje'],
                    'tipo_novia' => $tipo_novia,
                    'id_comentario' => $result->id_comentario
                );
            }
            return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $data,
                                )
                        )
                );
            
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }
    
    function getRespuesta(){
        if($_POST){
            $id_comentario = $this->input->post('comentario', TRUE);
            $result = $this->video->getRespuestaVideo($id_comentario);
            if ($result){
                $i = 0;
                foreach ($result as $respuesta){
                    $fecha_boda = "";
                    if(!empty($respuesta->fecha_boda)){
                        $fecha_boda = new DateTime($respuesta->fecha_boda);
                        $mes = $this->formatearMes($fecha_boda);
                        $anio = $fecha_boda->format("Y");
                        $fecha_boda = $mes." ".$anio;
                    }
                    if(!empty($respuesta->fecha_creacion)){
                        $fecha_creacion = relativeTimeFormat($respuesta->fecha_creacion,$mini=FALSE);
                        if(strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4){
                            $fecha_creacion = "Hace $fecha_creacion";
                        }else{
                            $fecha_creacion = "El $fecha_creacion";
                        }
                    }
                    if(!empty($respuesta->mime)){
                        $respuesta->url_foto = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$respuesta->usuarioCO;
                    }else{
                        $respuesta->url_foto = base_url().'dist/img/blog/perfil.png';
                    }
                    $tipo_novia = "";
                    if(!empty($respuesta->tipo_novia)){
                        $respuesta->tipo_novia = $this->validarTipo_usuario($respuesta->tipo_novia, $respuesta->id_usuario);
                    }
                    $denunciado = $this->comunidad->comentario_denunciado($respuesta->id_comentario,3);
                    if(!empty($denunciado->id_comentario) && $denunciado->id_comentario == $respuesta->id_comentario){
                        $respuesta->id_denuncia = $denunciado->id_comentario;
                    }else{
                        $respuesta->id_denuncia = "";
                    }
                    $respuesta->fecha_boda = $fecha_boda;
                    $respuesta->fecha_creacion = $fecha_creacion;
                    $respuesta2[$i] = $respuesta;
                    $i++;
                }
                $datos = array('respuestas' => $respuesta2);
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $datos,
                                )
                        )
                );
            }
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }
    
//    public function recientes($pagina = 1){
//        $contador = $this->video->getTotalVideos();
//        if(isset($pagina) || $contador > 0 && $contador <= 16){
//            $total_paginas = (int)($contador / 16);
//            if($total_paginas == 0){
//                $total_paginas = 1;
//            }
//            if($pagina <= $total_paginas){
//                $inicio = ($pagina - 1) * 16;
//                $limite = $pagina * 16;
//                $limite = $contador - $limite;
//                $result = $this->video->getRecientes($inicio,$limite);
//            }
//        }
//        $dato = array(
//            'titulo' => 'Ultimos Videos Publicadas',
//            'pagina' => $pagina,
//            'contador' => $contador,
//            'total_paginas' => $total_paginas,
//            'ordenamiento' => 'recientes',
//            'videos' => $result
//        );
//        $this->load->view('principal/novia/comunidad/filtro_videos',$dato);
//    }
//    
//    public function vistos($pagina = 1){
//        $contador = $this->video->getTotalVideos();
//        if(isset($pagina) || $contador > 0 && $contador <= 16){
//            $total_paginas = (int)($contador / 16);
//            if($total_paginas == 0){
//                $total_paginas = 1;
//            }
//            if($pagina <= $total_paginas){
//                $inicio = ($pagina - 1) * 16;
//                $limite = $pagina * 16;
//                $limite = $contador - $limite;
//                $result = $this->video->getVistos($inicio,$limite);
//            }
//        }
//        $dato = array(
//            'titulo' => 'Videos Mas Visitadas',
//            'pagina' => $pagina,
//            'contador' => $contador,
//            'total_paginas' => $total_paginas,
//            'ordenamiento' => 'vistos',
//            'videos' => $result
//        );
//        $this->load->view('principal/novia/comunidad/filtro_videos',$dato);
//    }
//    
//    public function comentados($pagina = 1){
//        $contador = $this->video->getTotalVideos();
//        if(isset($pagina) || $contador > 0 && $contador <= 16){
//            $total_paginas = (int)($contador / 16);
//            if($total_paginas == 0){
//                $total_paginas = 1;
//            }
//            if($pagina <= $total_paginas){
//                $inicio = ($pagina - 1) * 16;
//                $limite = $pagina * 16;
//                $limite = $contador - $limite;
//                $result = $this->video->getComentados($inicio,$limite);
//            }
//        }
//        $dato = array(
//            'titulo' => 'Fotos Mas Comentadas',
//            'pagina' => $pagina,
//            'contador' => $contador,
//            'total_paginas' => $total_paginas,
//            'ordenamiento' => 'comentados',
//            'videos' => $result
//        );
//        $this->load->view('principal/novia/comunidad/filtro_videos',$dato);
//    }
    
    public function denuncia(){
        if($_POST){
            $video = $this->input->post('video',true);
            $razon = $this->input->post('razon',true);
            $result = $this->video->denuncia($video,$razon);
            if($result){
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(200)
                                ->set_output("true");
            }
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }
    
    public function validarTipo_usuario($tipo,$id_usuario){
        $novio = "";
        switch ($tipo){
            case 1:
                $genero = $this->db->query("SELECT genero FROM usuario U INNER JOIN cliente C USING (id_usuario) WHERE id_usuario = $id_usuario")->row();
                if(!empty($genero) && $genero->genero == 1){
                    $novio = "Nuevo";
                }else if(!empty($genero) && $genero->genero == 2){
                    $novio = "Nueva";
                }
                break;
            case 2:
                $novio = "Principiante";
                break;
            case 3:
                $novio = "Habitual";
                break;
            case 4:
                $novio = "Top";
                break;
            case 5:
                $novio = "Pro";
                break;
            case 6:
                $novio = "VIP";
                break;
            case 7:
                $novio = "Super";
                break;
            case 8:
                $genero = $this->db->query("SELECT genero FROM usuario U INNER JOIN cliente C USING (id_usuario) WHERE id_usuario = $id_usuario")->row();
                if(!empty($genero) && $genero->genero == 1){
                    $novio = "Destacado";
                }else if(!empty($genero) && $genero == 2){
                    $novio = "Destacada";
                }
                break;
            case 9:
                $novio = "Leyenda";
                break;
        }
        return $novio;
    }
    
    public function comprobar_url(){
        if($_POST){
            $url = $this->input->post('video', true);
            $id = @fopen($url, "r");
            if($id){
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(200)
                                ->set_output("true");
            }else{
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(200)
                                ->set_output("false");
            }
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404);
    }
    
    public function error_404(){
        $data["pais"] =  "Mexico" ;
        $data["tipos"] = $this->tipo_proveedor->getGrupos();
        foreach ($data["tipos"] as $key => $value) {
            $value->tipos = $this->tipo_proveedor->getGrupo($value->nombre);
        }
        $this->load->view('errors/html/error_404.php',$data);
    }
}

