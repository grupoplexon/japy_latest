<?php

use Carbon\Carbon;

class Home extends MY_Controller
{
    protected function middleware()
    {
        return ["user_auth"];
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $wantsJson = $this->input->get_request_header('accept', true) === "application/json";
        $response  = ["code" => 200, "data" => []];

        if ($wantsJson) {
            return $this->output->set_content_type('application/json')
                ->set_status_header($response['code'])
                ->set_output(json_encode($response));
        }

        $this->load->view("principal/newHeader");
        $this->load->view("principal/novia/menu");
        $this->load->view("principal/novia/gallery/index");
        $this->load->view("principal/footer");
    }


}