<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Grupo extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model('Invitados/Grupo_model', 'Grupo_model');
    }

    public function index()
    {

    }

    public function add_grupo()
    {
        if ($_POST) {
            $data = [
                    "grupo"   => $this->input->post('nombre_grupo'),
                    "id_boda" => $this->session->userdata('id_boda'),
            ];

            $group = Group::where("grupo", $data["grupo"])
                    ->where("id_boda", $data["id_boda"])
                    ->get();

            if ( !$group->count() ) {
                $this->Grupo_model->insertGrupo($data);
            } else {
                $this->session->set_flashdata("tipo_mensaje", "danger");
                $this->session->set_flashdata("mensaje", "El grupo ya existe");
            }

            redirect_back();
        }
    }

    public function update_grupo()
    {
        if ($_POST) {
            $data = [
                    "grupo" => $this->input->post('nombre_grupo_update'),
            ];
            $this->Grupo_model->updateGrupo($data, $this->input->post('id_grupo'));
            redirect_back();
        }
    }

    public function delete_grupo($id_grupo)
    {
        $this->Grupo_model->deleteGrupo($id_grupo);
        redirect_back();
    }

}
