<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{
    protected function middleware()
    {
        return ['user_auth'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model('Invitados/Invitado_model', 'Invitado_model');
        $this->load->model('Invitados/Grupo_model', 'Grupo_model');
        $this->load->model('Invitados/Mesa_model', 'Mesa_model');
        $this->load->model('Invitados/Menu_model', 'Menu_model');
        $this->load->model('Invitados/Confirmacion_model', 'Confirmacion_model');
        $this->load->model('Invitados/Invitacion_model', 'Invitacion_model');
        $this->load->model('Invitados/Acompanante_model', 'Acompanante_model');
        $this->load->model('Boda_model');
    }

    public function index()
    {
        $return["boda"]                      = $this->Boda_model->get($this->session->userdata("id_boda"));
        $return["menus"]                     = $this->Menu_model->getMenus();
        $return["grupos"]                    = $this->Grupo_model->getGrupos();
        $return["mesas"]                     = $this->Mesa_model->getMesas();
        $return["invitados"]                 = $this->Invitado_model->getInvitadosGrupoOrder("nombre");
        $return["asistencia"]["confirmados"] = 0;
        $return["asistencia"]["pendientes"]  = 0;
        $return["asistencia"]["cancelados"]  = 0;

        $return["sin_direccion"] = 0;
        $return["no_enviados"]   = 0;

        $return["sentados"] = 0;

        $return["sexo"]["hombre"] = 0;
        $return["sexo"]["mujer"]  = 0;
        $return["sexo"]["nino"]   = 0;
        $return["sexo"]["bebe"]   = 0;

        foreach ($return["invitados"] as $key => $value) {
            switch ((int)$value->edad) {
                case 1: //ADULTO
                    switch ($value->sexo) {
                        case 1: // HOMBRE
                            $return["sexo"]["hombre"] += 1;
                            break;
                        case 2: // MUJER
                            $return["sexo"]["mujer"] += 1;
                            break;
                    }
                    break;
                case 2: //NINO
                    $return["sexo"]["nino"] += 1;
                    break;
                case 3: //BEBE
                    $return["sexo"]["bebe"] += 1;
                    break;
            }

            switch ($value->confirmado) {
                case 1: // pendiente
                    $return["asistencia"]["pendientes"] += 1;
                    break;
                case 2: // confirmado
                    $return["asistencia"]["confirmados"] += 1;
                    break;
                case 3: //cancelado
                    $return["asistencia"]["cancelados"] += 1;
                    break;
            }

            if ($value->email_enviado == 2) {
                $return["no_enviados"] += 1;
            }

            if (empty($value->direccion)) {
                $return["sin_direccion"] += 1;
            }

            if ( ! empty(decrypt($value->id_mesa))) {
                $return["sentados"] += 1;
            }
        }

        $hosts  = [];
        $guests = [];

        foreach ($return["invitados"] as $invitado) {
            if ($invitado->invitado_por) {
                $guests[] = $invitado;
            } else {
                $hosts[] = $invitado;
            }
        }
        
        $this->load->view("principal/novia/invitados/invitados", $return);
    }

    public function importar()
    {
        if ($_POST) {
            $nombre    = $this->input->post('nombre');
            $apellidos = $this->input->post('apellidos');
            $email     = $this->input->post('email');
            $telefono  = $this->input->post('telefono');
            $celular   = $this->input->post('celular');
            $direccion = $this->input->post('direccion');
            $cp        = $this->input->post('cp');
            $grupo     = $this->input->post('grupo');
            $menu      = $this->input->post('menu');
            $sexo      = $this->input->post('sexo');
            $edad      = $this->input->post('edad');

            foreach ($nombre as $key => $value) {
                $data      = [
                        'nombre'        => $value,
                        'apellido'      => $apellidos[$key],
                        'sexo'          => $sexo[$key],
                        'edad'          => $edad[$key],
                        'correo'        => strtolower($email[$key]),
                        'telefono'      => $telefono[$key],
                        'celular'       => $celular[$key],
                        'direccion'     => $direccion[$key],
                        'codigo_postal' => $cp[$key],
                        'email_enviado' => 2,
                        'confirmado'    => 1,
                        'id_grupo'      => decrypt($grupo[$key]),
                        'id_menu'       => decrypt($menu[$key]),
                        'id_mesa'       => null,
                        'id_boda'       => $this->session->userdata('id_boda'),
                ];
                $resultado = $this->Invitado_model->insertInvitado($data);
            }
            if ($resultado != false) {
                $return["mensaje"] = "success";
            } else {
                $return["mensaje"] = "error";
            }
        }
        $return["grupos"] = $this->Grupo_model->getGrupos();
        $return['menus']  = $this->Menu_model->getNumeroMenus();
        $this->load->view('principal/novia/invitados/importar_contactos', $return);
    }

    public function import_excel($nombre)
    {
        $file_url = base_url().'dist/archivo/formato.xlsx';
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"".basename($file_url)."\"");
        readfile($file_url);
    }

    public function grupos()
    {
        $return["grupos"]    = $this->Grupo_model->getNumeroGrupo();
        $return['mesas']     = $this->Mesa_model->getMesas();
        $return['menus']     = $this->Menu_model->getMenus();
        $return["invitados"] = $this->Invitado_model->getInvitadosGrupoOrder('id_grupo');
        $this->load->view('principal/novia/invitados/grupos', $return);
    }

    public function menus()
    {
        $return["grupos"]    = $this->Grupo_model->getGrupos();
        $return["mesas"]     = $this->Mesa_model->getMesas();
        $return["menus"]     = $this->Menu_model->getNumeroMenus();
        $return["invitados"] = $this->Invitado_model->getInvitadosGrupoOrder('nombre');

        $this->load->view('principal/novia/invitados/menus', $return);
    }

    public function contactos()
    {
        $return["grupos"]    = $this->Grupo_model->getGrupos();
        $return['mesas']     = $this->Mesa_model->getMesas();
        $return['menus']     = $this->Menu_model->getMenus();
        $return["invitados"] = $this->Invitado_model->getInvitadosGrupoOrder('nombre');
        $this->load->view('principal/novia/invitados/contactos', $return);
    }

    public function invitaciones($id = null)
    {
        if ($this->session->userdata('mensaje') != null) {
            $return['mensaje'] = $this->session->userdata('mensaje');
            $this->session->unset_userdata('mensaje');
        }

        if ($id != null) {
            $return["invitacion"] = $this->Invitacion_model->getInvitacion($id);
            if ($this->session->userdata('mensaje') != null) {
                $return['mensaje'] = $this->session->userdata('mensaje');
                $this->session->unset_userdata('mensaje');
            }
        }
        $return["boda"]         = $this->Boda_model->get($this->session->userdata("id_boda"));
        $return["novios"]       = $this->Invitado_model->getInvitadosGrupoNovio();
        $return["invitaciones"] = $this->Invitacion_model->getInvitaciones();
        $this->load->view('principal/novia/invitados/invitaciones', $return);
    }

    public function estadisticas()
    {
        $return["grupos"]    = $this->Grupo_model->getGrupoCount();
        $return['mesas']     = $this->Mesa_model->getMesas();
        $return['menus']     = $this->Menu_model->getCountMenu();
        $return["invitados"] = $this->Invitado_model->getInvitados();

        $return["asistencia"]['confirmados'] = 0;
        $return["asistencia"]['pendientes']  = 0;
        $return["asistencia"]['cancelados']  = 0;

        $return["enviado"]['si'] = 0;
        $return["enviado"]['no'] = 0;

        $return["sexo"]['hombre'] = 0;
        $return["sexo"]['mujer']  = 0;
        $return["sexo"]['nino']   = 0;
        $return["sexo"]['bebe']   = 0;

        foreach ($return['invitados'] as $key => $value) {
            switch ((int)$value->edad) {
                case 1: //ADULTO
                    switch ($value->sexo) {
                        case 1: // HOMBRE
                            $return["sexo"]['hombre'] += 1;
                            break;
                        case 2: // MUJER
                            $return["sexo"]['mujer'] += 1;
                            break;
                    }
                    break;
                case 2: //NINO
                    $return["sexo"]['nino'] += 1;
                    break;
                case 3: //BEBE
                    $return["sexo"]['bebe'] += 1;
                    break;
            }

            switch ($value->confirmado) {
                case 1: // pendiente
                    $return["asistencia"]['pendientes'] += 1;
                    break;
                case 2: // confirmado
                    $return["asistencia"]['confirmados'] += 1;
                    break;
                case 3: //cancelado
                    $return["asistencia"]['cancelados'] += 1;
                    break;
            }

            switch ($value->email_enviado) {
                case 1:
                    $return["enviado"]['si'] += 1;
                    break;
                case 2:
                    $return["enviado"]['no'] += 1;
                    break;
            }
        }
        $this->load->view('principal/novia/invitados/estadisticas', $return);
    }

    public function add_invitado()
    {
        $data        = [
                "nombre"        => $this->input->post('nombre'),
                "apellido"      => $this->input->post('apellido'),
                "correo"        => empty($this->input->post('correo')) ? null : $this->input->post('correo'),
                "telefono"      => empty($this->input->post('telefono')) ? null : $this->input->post('telefono'),
                "sexo"          => (int)$this->input->post('sexo'),
                "edad"          => (int)$this->input->post('edad'),
                "celular"       => empty($this->input->post('celular')) ? null : $this->input->post('celular'),
                "pais"          => $this->input->post('country'),
                "estado"        => $this->input->post('state'),
                "poblado"       => $this->input->post('city'),
                "direccion"     => empty($this->input->post('direccion')) ? null : $this->input->post('direccion'),
                "codigo_postal" => empty($this->input->post('cp')) ? null : $this->input->post('cp'),
                "confirmado"    => 1,
                "email_enviado" => 2,
                "id_grupo"      => (int)decrypt($this->input->post('grupo')),
                "id_menu"       => (int)decrypt($this->input->post('menu')),
                "id_boda"       => (int)$this->session->userdata("id_boda"),
        ];
        $id_invitado = $this->Invitado_model->insertInvitado($data);
        /*
         *      -       -       OBTIENE DATOS DE LA VISTA        -       -
         */

        $acompanante = [
                "nombre"   => $this->input->post('nombre_acompanante'),
                "apellido" => $this->input->post('apellido_acompanante'),
                "sexo"     => $this->input->post('sexo_acompanante'),
                "edad"     => $this->input->post('edad_acompanante'),
                "id_grupo" => $this->input->post('grupo_acompanante'),
                "id_menu"  => $this->input->post('menu_acompanante'),
        ];


        /*
         *      -       -       INSERT ACOMPANANTE        -       -
         */
        for ($i = 0; $i < count($acompanante['nombre']); $i++) {
            $acom       = [
                    "nombre"        => $acompanante['nombre'][$i],
                    "apellido"      => $acompanante['apellido'][$i],
                    "sexo"          => (int)$acompanante['sexo'][$i],
                    "edad"          => (int)$acompanante['edad'][$i],
                    "confirmado"    => 1,
                    "email_enviado" => 2,
                    "id_grupo"      => (int)decrypt($acompanante['id_grupo'][$i]),
                    "id_menu"       => (int)decrypt($acompanante['id_menu'][$i]),
                    "id_boda"       => (int)$this->session->userdata("id_boda"),
            ];
            $acompanant = [
                    'id_invitado'    => $id_invitado,
                    'id_acompanante' => $this->Invitado_model->insertInvitado($acom),
            ];
            $this->Acompanante_model->insertAcompanante($acompanant);
        }

        /*
         *      -   -   DELETE/INSERT INVITADO CAMBIA A ACOMPANANTE  -   -
         */
        $invi_create = $this->input->post('id_acompanante');
        if ( ! empty($invi_create)) {
            for ($i = 0; $i < count($invi_create); $i++) {
                $value = $invi_create[$i];
                $this->Acompanante_model->deleteAcompanante($value);
                $dat = [
                        'id_invitado'    => $id_invitado,
                        'id_acompanante' => $value,
                ];
                $this->Acompanante_model->insertAcompanante($dat);
            }
        }

        redirect_back();
    }

    public function delete_invitado($id_invitado)
    {
        $this->Invitado_model->deleteInvitado($id_invitado);
        redirect_back();
    }

    public function datos_invitado($id)
    {
        $resultado = $this->Invitado_model->getInvitado($id);
        if ($resultado != false) {
            header('Content-Type: application/json');
            http_response_code(202);
            echo json_encode($resultado);
            exit;
        } else {
            header('Content-Type: application/json');
            http_response_code(404);
            echo json_encode(["success" => 1, "message" => "Invitado no encontrado"]);
            exit;
        }
    }

    public function info_boda()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            $info = [];
            if ( ! empty($this->input->post('color'))) {
                $info = ['color' => $this->input->post('color')];
            } elseif ( ! empty($this->input->post('estacion'))) {
                $info = ['estacion' => $this->input->post('estacion')];
            } elseif ( ! empty($this->input->post('estilo'))) {
                $info = ['estilo' => $this->input->post('estilo')];
            }
            $result = $this->Boda_model->update($this->session->userdata('id_boda'), $info);
            if ($result != false) {
                http_response_code(202);
                echo json_encode(["success" => 0, "message" => "Actualizacion correcta"]);
                exit;
            }
        }
        http_response_code(404);
        echo json_encode(["success" => 1, "message" => "Error"]);
        exit;
    }

    public function updateInvitado()
    {
        if ($_POST) {
            switch ($this->input->post("tipo")) {
                case "confirmacion":
                    $data = [
                            "confirmado" => $this->input->post("confirmado"),
                    ];
                    break;
                case "grupo":
                    $data = [
                            "id_grupo" => $this->input->post("id_grupo"),
                    ];
                    break;
                case "menu":
                    $data = [
                            "id_menu" => $this->input->post("id_menu"),
                    ];
                    break;
                case "mesa":
                    $silla = $this->Mesa_model->getDisponible(decrypt($this->input->post("id_mesa")));
                    if ( ! empty($silla)) {
                        $data = [
                                "id_mesa" => $this->input->post("id_mesa"),
                                "silla"   => $silla,
                        ];
                    } else {
                        $data = [];
                    }
                    break;
                case "correo":
                    $data = [
                            "correo" => $this->input->post("correo"),
                    ];
                    break;
                case "email_enviado":
                    $data = [
                            "email_enviado" => $this->input->post("email_enviado"),
                    ];
                    break;
            }

            $resultado = $this->Invitado_model->updateInvitado($data, $this->input->post("id_invitado"));
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode($resultado);
                exit;
            } else {
                header('Content-Type: application/json');
                http_response_code(404);
                echo json_encode(["success" => 1, "message" => "Usuario invalido"]);
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(["success" => 0, "mensaje" => "Ha ocurrido un error."]);
        exit;
    }

    public function update_invitado()
    {
        $data = [
                "nombre"        => $this->input->post('nombre'),
                "apellido"      => $this->input->post('apellido'),
                "correo"        => $this->input->post('correo'),
                "telefono"      => $this->input->post('telefono'),
                "sexo"          => $this->input->post('sexo'),
                "edad"          => $this->input->post('edad'),
                "celular"       => $this->input->post('celular'),
                "pais"          => $this->input->post('country'),
                "estado"        => $this->input->post('state'),
                "poblado"       => $this->input->post('city'),
                "direccion"     => $this->input->post('direccion'),
                "codigo_postal" => $this->input->post('cp'),
                "id_grupo"      => $this->input->post('grupo'),
                "id_menu"       => $this->input->post('menu'),
                "id_boda"       => $this->session->userdata("id_boda"),
        ];

        if (empty($data['id_grupo'])) {
            unset($data['id_grupo']);
        }

        $id_invitado = $this->input->post('id_invitado');
        $this->Invitado_model->updateInvitado($data, $id_invitado);
        /*
         *      -       -       OBTIENE DATOS DE LA VISTA        -       -
         */

        $acompanante = [
                "nombre"   => $this->input->post('nombre_acompanante'),
                "apellido" => $this->input->post('apellido_acompanante'),
                "sexo"     => $this->input->post('sexo_acompanante'),
                "edad"     => $this->input->post('edad_acompanante'),
                "id_grupo" => $this->input->post('grupo_acompanante'),
                "id_menu"  => $this->input->post('menu_acompanante'),
        ];
        /*
         * -        -  DELETE ACOMPANATE  -       -
         */
        $remove_acomp = $this->input->post('acompanante_remove');
        if ( ! empty($remove_acomp)) {
            for ($i = 0; $i < count($remove_acomp); $i++) {
                $value = $remove_acomp[$i];
                $this->Acompanante_model->deleteAcompanante($value);
            }
        }
        /*
         *      -       -       INSERT ACOMPANANTE        -       -
         */
        for ($i = 0; $i < count($acompanante['nombre']); $i++) {
            $acom = [
                    "nombre"        => $acompanante['nombre'][$i],
                    "apellido"      => $acompanante['apellido'][$i],
                    "sexo"          => $acompanante['sexo'][$i],
                    "edad"          => $acompanante['edad'][$i],
                    "confirmado"    => 1,
                    "email_enviado" => 2,
                    "id_grupo"      => decrypt($acompanante['id_grupo'][$i]),
                    "id_menu"       => decrypt($acompanante['id_menu'][$i]),
                    "id_boda"       => $this->session->userdata("id_boda"),
            ];

            $acompanant = [
                    'id_invitado'    => $id_invitado,
                    'id_acompanante' => $this->Invitado_model->insertInvitado($acom),
            ];
            $this->Acompanante_model->insertAcompanante($acompanant);
        }


        /*
         *      -   -   DELETE/INSERT INVITADO CAMBIA A ACOMPANANTE  -   -
         */
        $invi_create = $this->input->post('id_acompanante');
        if ( ! empty($invi_create)) {
            for ($i = 0; $i < count($invi_create); $i++) {
                $value = $invi_create[$i];
                $this->Acompanante_model->deleteAcompanante($value);
                $dat = [
                        'id_invitado'    => $id_invitado,
                        'id_acompanante' => $value,
                ];
                $this->Acompanante_model->insertAcompanante($dat);
            }
        }


        redirect_back();
    }

    public function delete_acompanante()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            $resultado = $this->Acompanante_model->deleteAcompananteinvitado($this->input->post('id_invitado'),
                    $this->input->post('id_acompanante'));
            if ($resultado != false) {
                http_response_code(202);
                echo json_encode($resultado);
                exit;
            } else {
                http_response_code(404);
                echo json_encode(["success" => 1, "message" => "Error en los datos"]);
                exit;
            }
        }
        http_response_code(404);
        echo json_encode(["success" => 0, "mensaje" => "Ha ocurrido un error."]);
        exit;
    }

}
