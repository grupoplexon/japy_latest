<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invitacion extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('My_PHPMailer');
        $this->load->library("checker");
        $this->load->model('Invitados/Invitacion_model', 'Invitacion_model');
        $this->load->model('Invitados/Invitado_model', 'Invitado_model');
        $this->load->model('Invitados/Confirmacion_model', 'Confirmacion_model');
    }

    public function send($id_invitacion)
    {
        $data['id'] = $id_invitacion;
        $data['invitacion'] = $this->Invitacion_model->getInvitacion($id_invitacion);
        $data['invitados'] = $this->Invitado_model->getInvitados();
        $this->load->view('principal/novia/invitados/send_email', $data);
    }

    public function sendEmail($id_invitacion)
    {
        if ($_POST) {
            $invitados = $this->input->post('id_invitados');
            $invitados = $this->Invitado_model->getInvitadoArray($invitados);
            $invitacion = $this->Invitacion_model->getInvitacion($id_invitacion);
            foreach ($invitados as $key => $value) {
                $this->Invitado_model->updateInvitado(array("email_enviado" => 1), encrypt($value->id_invitado));
                if ($invitacion->confirmacion == 1) {
                    $data = array(
                        "codigo" => generarCadenaTamano(100),
                        "estado" => 0,
                        "id_invitado" => $value->id_invitado,
                    );
                    $this->Confirmacion_model->insert($data);
                    $invitacion->codigo = $data['codigo'];
                }
                $this->my_phpmailer->to($value->correo, $value->nombre . '' . $value->apellido);
                $this->my_phpmailer->send($this->load->view("templates/email_solicitud_confirmacion", $invitacion, TRUE));
            }
            $this->session->set_userdata('mensaje', 'Correos enviados satisfactoriamente');
            redirect('novios/invitados/Home/invitaciones');
        }
    }

    public function add_invitacion()
    {
        if ($_POST) {
            $img = $this->parseBase64($this->input->post('imagen'));
            $data = array(
                "novia" => $this->input->post('novia'),
                "novio" => $this->input->post('novio'),
                "fecha" => $this->input->post('fecha'),
                "lugar" => $this->input->post('lugar'),
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                "confirmacion" => $this->input->post('confirmacion'),
                "mime" => $img->mime,
                "base" => $img->imagen,
                "id_boda" => $this->session->userdata('id_boda'),
            );
            $id_invitacion = $this->Invitacion_model->insertInvitacion($data);
            switch ($this->input->post('boton')) {
                case 1:
                    // Guardar
                    redirect_back();
                    break;
                case 2:
                    // Guardar y enviar
                    redirect('novios/invitados/Invitacion/send/' . $id_invitacion);
                    break;
            }
        }
    }

    public function update_invitacion()
    {
        if ($_POST) {
            $img = $this->parseBase64($this->input->post('imagen'));
            $data = array(
                "novia" => $this->input->post('novia'),
                "novio" => $this->input->post('novio'),
                "fecha" => $this->input->post('fecha'),
                "lugar" => $this->input->post('lugar'),
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                "confirmacion" => $this->input->post('confirmacion'),
                "mime" => $img->mime,
                "base" => $img->imagen,
            );
            $this->Invitacion_model->updateInvitacion($data, $this->input->post('id_invitacion'));
            switch ($this->input->post('boton')) {
                case 1:
                    // Guardar
                    $this->session->set_userdata('mensaje', 'Las actualizaciones se realizaron con exito..');
                    redirect_back();
                    break;
                case 2:
                    // Guardar y enviar
                    redirect('novios/invitados/Invitacion/send/' . $this->input->post('id_invitacion'));
                    break;
            }
        }
    }

    public function delete_invitacion($id_invitacion)
    {
        $this->Invitacion_model->deleteInvitacion($id_invitacion);
        redirect_back();
    }

    private function parseBase64($strBase64)
    {
        $o = new stdClass();
        if (empty($strBase64)) {
            $o->mime = NULL;
            $o->imagen = NULL;
        } else {
            $mime = explode(",", $strBase64);
            $o->imagen = base64_decode($mime[1]);
            $mime = explode(";", $mime[0]);
            $mime = explode(":", $mime[0]);
            $o->mime = $mime[1];
        }
        return $o;
    }

}
