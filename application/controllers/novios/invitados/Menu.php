<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model('Invitados/Menu_model', 'Menu_model');
    }

    public function index()
    {

    }

    public function add_menu()
    {
        if ($_POST) {
            $data = [
                    "nombre"      => $this->input->post('nombre_menu'),
                    "descripcion" => $this->input->post('descripcion_menu'),
                    "id_boda"     => $this->session->userdata('id_boda'),
            ];

            $menu = $this->Menu_model->getSameMenu($data["nombre"],$data["id_boda"]);

            if ( !count($menu)) {
                     $this->Menu_model->insertMenu($data);
            } else {
                $this->session->set_flashdata("tipo_mensaje", "danger");
                $this->session->set_flashdata("mensaje", "El Menú ya existe.");
            }

            redirect_back();
        }
    }

    public function update_menu()
    {
        if ($_POST) {
            $data = [
                    "nombre"      => $this->input->post('nombre_menu_update'),
                    "descripcion" => $this->input->post('descripcion_menu_update'),
            ];
            $this->Menu_model->updateMenu($data, $this->input->post('id_menu_update'));
            redirect_back();
        }
    }

    public function delete_menu($id_menu)
    {
        $this->Menu_model->deleteMenu($id_menu);
        redirect_back();
    }

}