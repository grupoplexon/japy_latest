<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("formats");
        $this->load->library('My_PHPMailer');
        $this->load->model('Invitados/Invitado_model', 'Invitado_model');
        $this->load->model('MiPortal/MiPlantilla_model', 'MiPlantilla_model');
        $this->load->model('MiPortal/MiPortal_model', 'MiPortal_model');
        $this->load->model('MiPortal/MiPortalSeccion_model', 'MiPortalSeccion_model');
        $this->load->model('MiPortal/MiPortalLibro_model', 'MiPortalLibro_model');
        $this->load->model('MiPortal/MiPortalEvento_model', 'MiPortalEvento_model');
        $this->load->model('MiPortal/MiPortalEncuesta_model', 'MiPortalEncuesta_model');
        $this->load->model('MiPortal/MiPortalBlog_model', 'MiPortalBlog_model');
        $this->load->model('MiPortal/MiPortalDireccion_model', 'MiPortalDireccion_model');
        $this->load->model('MiPortal/MiPortalVideo_model', 'MiPortalVideo_model');
        $this->load->model('MiPortal/MiPortalAlbum_model', 'MiPortalAlbum_model');
        $this->load->model('MiPortal/MiPortalTest_model', 'MiPortalTest_model');
        $this->load->model('MiPortal/MiPortalBlog_model', 'MiPortalBlog_model');
        $this->load->model('MiPortal/MiPortalSeccion_Imagen_model', 'MiPortalSeccionImagen_model');
    }

    public function index()
    {
        if ($_POST) {
            $id_seccion = decrypt($this->input->post('id-bienvenido'));
            $data = array(
                "subtitulo" => $this->input->post('subtitle-bienvenido'),
                "descripcion" => $this->input->post('descripcion-bienvenida'),
            );
            $this->MiPortalSeccion_model->update_seccion($data, $id_seccion);
            $base = $this->input->post('image-bienvenido');
            $id_image = $this->input->post('image-bienvenido-id');
            $id_image = empty($id_image) ? '' : decrypt($id_image);
            if (!empty($base)) {
                $img = $this->parseBase64($base);
                $data = array(
                    "mime" => $img->mime,
                    "base" => $img->imagen,
                    "miportal" => "BIENVENIDO",
                );
                if (empty($id_image)) {
                    $this->MiPortalSeccionImagen_model->insert($data, $id_seccion);
                } else {
                    $this->MiPortalSeccionImagen_model->update($data, $id_image);
                }
            } else {
                if (!empty($id_image)) {
                    $this->MiPortalSeccionImagen_model->delete($id_image);
                }
            }
        }
        $return = $this->info_global('bienvenido');
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function bienvenido()
    {
        redirect('novios/miweb');
    }

    public function libro()
    {
        if ($_POST) {
            $id_seccion = decrypt($this->input->post('id-libro'));
            $data = array(
                "subtitulo" => $this->input->post('subtitle-libro'),
                "descripcion" => $this->input->post('descripcion-libro'),
            );
            $this->MiPortalSeccion_model->update_seccion($data, $id_seccion);
        }
        $return = $this->info_global('libro');
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function asistencia()
    {
        if ($_POST) {
            $id_seccion = decrypt($this->input->post('id-confirma'));
            $data = array(
                "subtitulo" => $this->input->post('subtitle-confirma'),
                "descripcion" => $this->input->post('descripcion-confirma'),
            );
            $this->MiPortalSeccion_model->update_seccion($data, $id_seccion);
        }
        $return = $this->info_global('asistencia');
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function contactar()
    {
        if ($_POST) {
            $id_seccion = decrypt($this->input->post('id-contactar'));
            $data = array(
                "subtitulo" => $this->input->post('subtitle-contactar'),
                "descripcion" => $this->input->post('descripcion-contactar'),
                "email" => $this->input->post('email-contactar'),
                "alerta" => $this->input->post('alert-contactar'),
                "telefono" => $this->input->post('telefono-contactar'),
                "direccion" => $this->input->post('direccion-contactar'),
            );
            $this->MiPortalSeccion_model->update_seccion($data, $id_seccion);
        }
        $return = $this->info_global('contactar');
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function encuesta()
    {
        $return = $this->info_global('encuesta');
        $return['encuestas'] = $this->MiPortalEncuesta_model->getAll();
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function test()
    {
        $return = $this->info_global('test');
        $return['tests'] = $this->MiPortalTest_model->getAll();
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function evento()
    {
        $return = $this->info_global('evento');
        $return['eventos'] = $this->MiPortalEvento_model->getAll();
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function video()
    {
        $return = $this->info_global('video');
        $return['videos'] = $this->MiPortalVideo_model->getAll();
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function album()
    {
        $return = $this->info_global('album');
        $return['albums'] = $this->MiPortalAlbum_model->getAll();
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function direccion()
    {
        $return = $this->info_global('direccion');
        $return['direcciones'] = $this->MiPortalDireccion_model->getAll();
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function blog()
    {
        $return = $this->info_global('blog');
        $return['blogs'] = $this->MiPortalBlog_model->getAll();
        $this->load->view('principal/novia/miportal/index', $return);
    }

    public function info_global($seccion)
    {
        $return['miportal'] = $this->MiPortal_model->get();
        $return['otherplantilla'] = $this->MiPlantilla_model->getAll();
        $return['miportal_seccion'] = $this->MiPortalSeccion_model->getAll();
        foreach ($return['miportal_seccion'] as $key => $value) {
            if ($value->tipo == $seccion) {
                $return['seccion'] = $value;
            }
        }
        if (isset($return['seccion']) && $return['seccion']->visible == 3 || $return['seccion']->activo == 2) {
            redirect(base_url().'index.php/novios/miweb');
        }

        $return['comentarios'] = $this->MiPortalLibro_model->getAll();
        $return['plantilla'] = $this->MiPlantilla_model->getPlantilla();
        $return['invitados'] = $this->Invitado_model->getCorreoInvitados();
        $return['image_plantilla'] = $this->MiPlantilla_model->getImagen($return['miportal']->id_plantillaportal);

        return $return;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *         -     -       E       N       C       U       E       S       T       A          -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function insert_encuesta()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post("titulo"),
                "descripcion" => $this->input->post("descripcion"),
                "fecha" => date("Y-m-d H:i:s"),
                "id_miportal_seccion" => decrypt($this->input->post("s")),
            );
            $result = $this->MiPortalEncuesta_model->insertEncuesta($data);
            if ($result != false) {
                $opt = $this->input->post("opciones");
                $ids = array();
                foreach ($opt as $key => $value) {
                    $value['id_miportal_encuesta'] = $result;
                    $id = $this->MiPortalEncuesta_model->insertOpcion($value);
                    array_push($ids, $id);
                }
            }
            if ($result != false) {
                http_response_code(202);
                echo json_encode(array("success" => 0, "id_encuesta" => encrypt($result), "id_new" => $ids));
                exit;
            }
        }
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function update_encuesta()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post("titulo"),
                "descripcion" => $this->input->post("descripcion"),
            );
            $resultado = $this->MiPortalEncuesta_model->updateEncuesta($data, decrypt($this->input->post("i_e")));
            $id = array();
            $opt = $this->input->post("opciones");
            foreach ($opt as $key => $value) {
                if ($value['id'] != null) {
                    $result = $this->MiPortalEncuesta_model->updateOpcion(
                        array("texto" => $value['texto']),
                        decrypt($value['id'])
                    );
                } else {
                    $result = $this->MiPortalEncuesta_model->insertOpcion(
                        array("texto" => $value['texto'], "id_miportal_encuesta" => decrypt($this->input->post("i_e")))
                    );
                    array_push($id, $result);
                }
            }
            if ($resultado != false) {
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id_new" => $id));
                exit;
            }
        }
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_encuesta()
    {
        if ($_POST) {
            $resultado = $this->MiPortalEncuesta_model->deleteEncuesta(decrypt($this->input->post('id-encuesta')));
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_encuesta_opcion()
    {
        if ($_POST) {
            $resultado = $this->MiPortalEncuesta_model->deleteOption(
                decrypt($this->input->post('id-opcion')),
                decrypt($this->input->post('id-enc'))
            );
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    function send_email()
    {
        if ($_POST) {
            $data['miportal'] = $this->MiPortal_model->get();
            $data['correos'] = explode(',', $this->input->post('correos'));
            $data['asunto'] = $this->input->post('asunto');
            $data['mensaje'] = $this->input->post('mensaje');
            foreach ($data['correos'] as $key => $value) {
                $this->my_phpmailer->to($value, $value);
                $this->my_phpmailer->send($this->load->view("templates/email_solicitud_confirmacion", $data, true));
            }
            header('Content-Type: application/json');
            http_response_code(202);
            echo json_encode(array("success" => 0, "message" => "Success"));
            exit;
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -       -     -       L       I       B       R       O           -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function deleteComent()
    {
        if ($_POST) {
            $resultado = $this->MiPortalLibro_model->delete(decrypt($this->input->post('id_comment')));
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -       -     -       E       V       E       N       T       O           S    -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function update_headerevento()
    {
        if ($_POST) {
            $id_seccion = decrypt($this->input->post('id-evento'));
            $data = array(
                "titulo" => $this->input->post('titulo-evento'),
                "descripcion" => $this->input->post('descripcion-evento'),
            );
            $resultado = $this->MiPortalSeccion_model->update_seccion($data, $id_seccion);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function update_evento()
    {
        header('Content-Type: application/json');
        if ($_POST) {
            $id_evento = decrypt($this->input->post('i'));
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'telefono' => $this->input->post('telefono'),
                'fecha' => $this->input->post('fecha'),
                'pais' => $this->input->post('pais'),
                'estado' => $this->input->post('estado'),
                'poblado' => $this->input->post('poblado'),
                'codigo_postal' => $this->input->post('codigo_postal'),
                'direccion' => $this->input->post('direccion'),
                'latitud' => $this->input->post('latitud'),
                'longitud' => $this->input->post('longitud'),
            );
            $resultado = $this->MiPortalEvento_model->update($data, $id_evento);
            if ($resultado != false) {
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function insert_evento()
    {
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'telefono' => $this->input->post('telefono'),
                'fecha' => $this->input->post('fecha'),
                'pais' => $this->input->post('pais'),
                'estado' => $this->input->post('estado'),
                'poblado' => $this->input->post('poblado'),
                'codigo_postal' => $this->input->post('codigo_postal'),
                'direccion' => $this->input->post('direccion'),
                'latitud' => $this->input->post('latitud'),
                'longitud' => $this->input->post('longitud'),
                'id_miportal_seccion' => decrypt($this->input->post('s')),
            );

            $id_evento = $resultado = $this->MiPortalEvento_model->insert($data);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id" => $id_evento));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_evento()
    {
        if ($_POST) {
            $id_seccion = decrypt($this->input->post('id-evento'));
            $resultado = $this->MiPortalEvento_model->delete($id_seccion);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -            -        -        A      L       B       U       M           -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function update_album()
    {
        if ($_POST) {
            $id_album = decrypt($this->input->post('i'));

            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
            );
            $resultado = $this->MiPortalAlbum_model->update($data, $id_album);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function insert_album()
    {
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'fecha' => date('Y-m-d H:i:s'),
                'id_miportal_seccion' => decrypt($this->input->post('s')),
            );
            $id_album = $this->MiPortalAlbum_model->insert($data);
            foreach ($this->input->post('imagenes') as $key => $value) {
                $dt = array(
                    "id_miportal_album" => $id_album,
                    "id_miportal_imagen" => decrypt($value),
                );
                $this->MiPortalAlbum_model->insertImagen($dt);
            }

            if ($id_album != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id" => encrypt($id_album)));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_album()
    {
        if ($_POST) {
            $id_album = decrypt($this->input->post('id-album'));
            $resultado = $this->MiPortalAlbum_model->deleteComentario($id_album);
            $resultado = $this->MiPortalAlbum_model->deleteImg($id_album);
            $resultado = $this->MiPortalAlbum_model->delete($id_album);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_comentario()
    {
        if ($_POST) {
            $id_album = decrypt($this->input->post('id_comment'));
            $resultado = $this->MiPortalAlbum_model->deleteComentario($id_album);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function insert_relacionimage()
    {
        if ($_POST) {
            $dt = array(
                "id_miportal_album" => decrypt($this->input->post('id_album')),
                "id_miportal_imagen" => decrypt($this->input->post('id_imagen')),
            );
            $resultado = $this->MiPortalAlbum_model->insertImagen($dt);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function upload_image($id_album)
    {
        header('Content-Type: application/json');
        if ($_FILES) {
            $file = $_FILES["uploadfile"];
            if (!is_array($file["name"]) && !empty($file["tmp_name"])) {
                $datos = file_get_contents($file["tmp_name"]);
                $data = array(
                    "mime" => $file["type"],
                    "base" => $datos,
                    "miportal" => 0,
                );
                $resultado = $this->MiPortalAlbum_model->insertImagenPortal($data, decrypt($id_album));
            }
            if ($resultado != false) {
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id" => encrypt($resultado)));
                exit;
            }
        } else {
            if ($_POST) {
                $id = $this->input->post('id');
                $resultado = $this->MiPortalAlbum_model->deleteImagenPortal(decrypt($id));
                if ($resultado != false) {
                    http_response_code(202);
                    echo json_encode(array("success" => 0, "message" => "Success", "id" => encrypt($resultado)));
                    exit;
                }
            }
        }
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -            -        -        B           L           O        G             -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function update_blog()
    {
        if ($_POST) {
            $id_blog = decrypt($this->input->post('i'));

            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
            );
            $resultado = $this->MiPortalBlog_model->update($data, $id_blog);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function insert_blog()
    {
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'fecha' => date('Y-m-d H:i:s'),
                'id_miportal_seccion' => decrypt($this->input->post('s')),
            );
            $id_blog = $this->MiPortalBlog_model->insert($data);
            foreach ($this->input->post('imagenes') as $key => $value) {
                $dt = array(
                    "id_miportal_blog" => $id_blog,
                    "id_miportal_imagen" => decrypt($value),
                );
                $this->MiPortalBlog_model->insertImagen($dt);
            }

            if ($id_blog != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id" => encrypt($id_blog)));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_blog()
    {
        if ($_POST) {
            $id_blog = decrypt($this->input->post('id-blog'));
            $resultado = $this->MiPortalBlog_model->deleteComentario($id_blog);
            $resultado = $this->MiPortalBlog_model->deleteImg($id_blog);
            $resultado = $this->MiPortalBlog_model->delete($id_blog);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_comentario_blog()
    {
        if ($_POST) {
            $id_blog = decrypt($this->input->post('id_comment'));
            $resultado = $this->MiPortalBlog_model->deleteComentario($id_blog);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function insert_relacionimage_blog()
    {
        if ($_POST) {
            $dt = array(
                "id_miportal_blog" => decrypt($this->input->post('id_blog')),
                "id_miportal_imagen" => decrypt($this->input->post('id_imagen')),
            );
            $resultado = $this->MiPortalBlog_model->insertImagen($dt);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function upload_image_blog($id_blog)
    {
        if ($_FILES) {
            $file = $_FILES["uploadfile"];
            if (!is_array($file["name"]) && !empty($file["tmp_name"])) {
                $datos = file_get_contents($file["tmp_name"]);
                $data = array(
                    "mime" => $file["type"],
                    "base" => $datos,
                    "miportal" => 0,
                );
                $resultado = $this->MiPortalBlog_model->insertImagenPortal($data, decrypt($id_blog));
            }

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id" => encrypt($resultado)));
                exit;
            }
        } else {
            if ($_POST) {
                $id = $this->input->post('id');

                $resultado = $this->MiPortalBlog_model->deleteImagenPortal(decrypt($id));
                if ($resultado != false) {
                    header('Content-Type: application/json');
                    http_response_code(202);
                    echo json_encode(array("success" => 0, "message" => "Success", "id" => encrypt($resultado)));
                    exit;
                }
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -            -        -         V        I       D       E       O       S           -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function update_video()
    {
        if ($_POST) {
            $id_video = decrypt($this->input->post('i'));

            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'direccion_web' => $this->input->post('direccion_web'),
            );
            $resultado = $this->MiPortalVideo_model->update($data, $id_video);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function insert_video()
    {
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'direccion_web' => $this->input->post('direccion_web'),
                'fecha' => date('Y-m-d H:i:s'),
                'id_miportal_seccion' => decrypt($this->input->post('s')),
            );

            $id_video = $this->MiPortalVideo_model->insert($data);

            if ($id_video != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id" => $id_video));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_video()
    {
        if ($_POST) {
            $id_video = decrypt($this->input->post('id-video'));
            $resultado = $this->MiPortalVideo_model->delete($id_video);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -     -          -          E    N   C   U   E   S   T   A       -         -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function update_test()
    {
        if ($_POST) {
            $id_test = decrypt($this->input->post('i_e'));

            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'answer1' => $this->input->post('answer1'),
                'answer2' => $this->input->post('answer2'),
                'answer3' => $this->input->post('answer3'),
                'answer4' => $this->input->post('answer4'),
                'respuesta' => $this->input->post('respuesta'),
            );
            $resultado = $this->MiPortalTest_model->update($data, $id_test);

            if (empty($data['answer3'])) {
                $this->MiPortalTest_model->deleteAnswer($id_test, '3');
            }
            if (empty($data['answer4'])) {
                $this->MiPortalTest_model->deleteAnswer($id_test, '4');
            }


            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function insert_test()
    {
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'answer1' => $this->input->post('answer1'),
                'answer2' => $this->input->post('answer2'),
                'answer3' => $this->input->post('answer3'),
                'answer4' => $this->input->post('answer4'),
                'respuesta' => $this->input->post('respuesta'),
                'id_miportal_seccion' => decrypt($this->input->post('s')),
            );

            $id_test = $this->MiPortalTest_model->insert($data);

            if ($id_test != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id" => $id_test));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_test()
    {
        if ($_POST) {
            $id_test = decrypt($this->input->post('id-test'));
            $resultado = $this->MiPortalTest_model->delete($id_test);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -        D     I       R       E       C       C       I       O       N       E       S    -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function update_direccion()
    {
        if ($_POST) {
            $id_direccion = decrypt($this->input->post('i'));

            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'telefono' => $this->input->post('telefono'),
                'pais' => $this->input->post('pais'),
                'estado' => $this->input->post('estado'),
                'poblado' => $this->input->post('poblado'),
                'codigo_postal' => $this->input->post('codigo_postal'),
                'direccion' => $this->input->post('direccion'),
                'latitud' => $this->input->post('latitud'),
                'longitud' => $this->input->post('longitud'),
            );
            $resultado = $this->MiPortalDireccion_model->update($data, $id_direccion);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function insert_direccion()
    {
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                'telefono' => $this->input->post('telefono'),
                'pais' => $this->input->post('pais'),
                'estado' => $this->input->post('estado'),
                'poblado' => $this->input->post('poblado'),
                'codigo_postal' => $this->input->post('codigo_postal'),
                'direccion' => $this->input->post('direccion'),
                'latitud' => $this->input->post('latitud'),
                'longitud' => $this->input->post('longitud'),
                'id_miportal_seccion' => decrypt($this->input->post('s')),
            );

            $id_direccion = $resultado = $this->MiPortalDireccion_model->insert($data);

            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success", "id" => $id_direccion));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function delete_direccion()
    {
        if ($_POST) {
            $id_direccion = decrypt($this->input->post('id-direccion'));
            $resultado = $this->MiPortalDireccion_model->delete($id_direccion);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -       -     -       M        I        -   P       O       R       T       A       L    -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function restart_miportal()
    {
        $datos = array(
            "titleColor" => '',
            "titleFont" => '',
            "menuBackground" => '',
            "menuColor" => '',
            "menuFont" => '',
            "cuentaBackground" => '',
            "cuentaColor" => '',
            "bodyBackground" => '',
            "subtitleColor" => '',
            "subtitleFont" => '',
            "textColor" => '',
            "linkColor" => '',
            "textFont" => '',
            "img_table" => "plantillaportal_imagen",
            "img_id" => null,
        );
        $resultado = $this->MiPortal_model->update($datos, $this->session->userdata("id_boda"));
        //$value = $this->MiPortal_model->getPlantilla();
        if ($resultado != false) {
            header('Content-Type: application/json');
            http_response_code(202);
            echo json_encode(array("success" => 0, "message" => "Success"));
            //echo json_encode($value);
            exit;
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function change_theme()
    {
        if ($_POST) {
            $datos = array(
                "titleColor" => '',
                "titleFont" => '',
                "menuBackground" => '',
                "menuColor" => '',
                "menuFont" => '',
                "cuentaBackground" => '',
                "cuentaColor" => '',
                "bodyBackground" => '',
                "subtitleColor" => '',
                "subtitleFont" => '',
                "textColor" => '',
                "linkColor" => '',
                "textFont" => '',
                "img_table" => "plantillaportal_imagen",
                "img_id" => null,
                "id_plantillaportal" => decrypt($this->input->post('id_plantillaportal')),
            );

            $resultado = $this->MiPortal_model->update($datos, $this->session->userdata("id_boda"));

            $return['info_plantilla'] = $this->MiPlantilla_model->getPlantilla();
            $return['image_plantilla'] = $this->MiPlantilla_model->getImagen(
                decrypt($this->input->post('id_plantillaportal'))
            );
            foreach ($return['image_plantilla'] as $key => $value) {
                $value->base64 = base64_encode($value->base64);
            }


            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode($return);
                exit;
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function update_miportal()
    {
        switch ($this->input->post('tipo')) {
            case 'contrasena':
                $datos = array(
                    "contrasena" => empty($this->input->post('contrasena')) ? '' : md5(
                        $this->input->post('contrasena')
                    ),
                );
                if ($this->input->post('aplicarseccion') == 'true') {
                    $this->MiPortalSeccion_model->updatePrivado();
                }
                break;
            case 'cuenta_atras':
                $datos = array(
                    "cuenta_atras" => $this->input->post('cuenta_atras'),
                );
                break;
            case 'informacion':
                $datos = array(
                    "titulo" => $this->input->post('titulo'),
                    "nosotros" => $this->input->post('nosotros'),
                    "fecha_boda" => $this->input->post('fecha_boda'),
                );
                break;
            case 'img_fondo':
                $datos = array(
                    "img_table" => $this->input->post('img_table'),
                    "img_id" => $this->input->post('img_id'),
                );
                break;
            case 'titleColor':
                $datos = array(
                    "titleColor" => $this->input->post('valor'),
                );
                break;
            case 'titleFont':
                $datos = array(
                    "titleFont" => $this->input->post('valor'),
                );
                break;
            case 'menuBackground':
                $datos = array(
                    "menuBackground" => $this->input->post('valor'),
                );
                break;
            case 'menuColor':
                $datos = array(
                    "menuColor" => $this->input->post('valor'),
                );
                break;
            case 'menuFont':
                $datos = array(
                    "menuFont" => $this->input->post('valor'),
                );
                break;
            case 'cuentaBackground':
                $datos = array(
                    "cuentaBackground" => $this->input->post('valor'),
                );
                break;
            case 'cuentaColor':
                $datos = array(
                    "cuentaColor" => $this->input->post('valor'),
                );
                break;
            case 'bodyBackground':
                $datos = array(
                    "bodyBackground" => $this->input->post('valor'),
                );
                break;
            case 'subtitleColor':
                $datos = array(
                    "subtitleColor" => $this->input->post('valor'),
                );
                break;
            case 'subtitleFont':
                $datos = array(
                    "subtitleFont" => $this->input->post('valor'),
                );
                break;
            case 'textColor':
                $datos = array(
                    "textColor" => $this->input->post('valor'),
                );
                break;
            case 'linkColor':
                $datos = array(
                    "linkColor" => $this->input->post('valor'),
                );
                break;
            case 'textFont':
                $datos = array(
                    "textFont" => $this->input->post('valor'),
                );
                break;
            default:
                header('Content-Type: application/json');
                http_response_code(404);
                echo json_encode(array("success" => 1, "message" => "Campo no existe"));
                exit;
        }

        $resultado = $this->MiPortal_model->update($datos, $this->session->userdata("id_boda"));

        if ($resultado != false) {
            header('Content-Type: application/json');
            http_response_code(202);
            echo json_encode(array("success" => 0, "message" => "Success"));
            exit;
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function upload_img()
    {
        if ($_POST) {
            $img = $this->parseBase64($this->input->post('base64'));
            $data = array(
                "mime" => $img->mime,
                "base" => $img->imagen,
                "miportal" => encrypt($this->session->userdata('id_miportal')),
            );
            $resultado = $this->MiPortalSeccion_model->insertImg($data);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function update_seccion()
    {
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "visible" => $this->input->post('visible'),
            );
            if ($this->input->post('tipo') == 'bienvenido' && $data['visible'] == 3) {
                // BIENVENIDO NO PUEDE SER DE TIPO INVISIBLE
            } else {
                $resultado = $this->MiPortalSeccion_model->update($data, $this->input->post('id'));
                if ($resultado != false) {
                    header('Content-Type: application/json');
                    http_response_code(202);
                    echo json_encode(array("success" => 0, "message" => "Success"));
                    exit;
                }
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function activar_seccion()
    {
        if ($_POST) {
            $data = array(
                "titulo" => $this->input->post('titulo'),
                "descripcion" => $this->input->post('descripcion'),
                "activo" => 1,
            );
            $resultado = $this->MiPortalSeccion_model->update($data, $this->input->post('id'));
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 0, "message" => "Success"));
                exit;
            }
        }

        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function restaurar_seccion()
    {
        $resultado = $this->MiPortalSeccion_model->restaurar_seccion(
            $this->input->post('tipo'),
            $this->input->post('id')
        );
        if ($resultado != false) {
            header('Content-Type: application/json');
            http_response_code(202);
            echo json_encode(array("success" => 0, "message" => "Success"));
            exit;
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    public function enviar_invitaciones()
    {
        /* if ($_POST) {
          try {
          $mail = new PHPMailer();
          $mail->isSMTP();
          $mail->SMTPAuth = true;
          $mail->SMTPSecure = "ssl";
          $mail->Host = "smtp.gmail.com";
          $mail->Port = 465;
          $mail->Username = "comunicate.evolutel@gmail.com";
          $mail->Password = "Camaleon97";
          $mail->setFrom('comunicate.evolutel@gmail.com', 'Japy');
          $mail->addAddress('', '');
          $cuerpo = '<body style="font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;"></body>';
          $mail->Subject = "Japy";
          $mail->msgHTML($cuerpo);
          $mail->send();
          } catch (phpmailerException $e) {
          echo $e->errorMessage();
          exit();
          } catch (Exception $e) {
          echo $e->getMessage();
          exit();
          }
          } */
        redirect_back();
    }

    public function getMiPortalSeccion($id)
    {
        $resultado = $this->MiPortalSeccion_model->get($id);
        if ($resultado != false) {
            header('Content-Type: application/json');
            http_response_code(202);
            echo json_encode($resultado);
            exit;
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 1, "message" => "Error intentelo de nuevo mas tarde"));
        exit;
    }

    private function parseBase64($strBase64)
    {
        $o = new stdClass();
        if (empty($strBase64)) {
            $o->mime = null;
            $o->imagen = null;
        } else {
            $mime = explode(",", $strBase64);
            $o->imagen = base64_decode($mime[1]);
            $mime = explode(";", $mime[0]);
            $mime = explode(":", $mime[0]);
            $o->mime = $mime[1];
        }

        return $o;
    }

}
