<?php
class Group extends CI_Controller {
    public $input;    

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Moderador/Comunidad_model", "comunidad");
        $this->load->model("Moderador/Imagen_model", "imagen");
        $this->load->model("Moderador/Grupo_model", "grupo");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/";
        $this->load->helper('formats_helper');
    }
    
    public function grupo($id_grupo = 0, $seccion = 1, $ordenamiento = 1, $pagina = 1){
        if($this->checker->isLogin()){
			if (!$this->checker->isModerador()) {
				$this->load->view('errors/html/error_404.php');
			}
			}
        else{
            redirect("login");
        }
		if($id_grupo > 48 || $id_grupo < 1){
            $this->load->view('errors/html/error_404.php'); 
        }
        if($id_grupo != 0 && $seccion != 1){
            $grupos = $this->comunidad->getGrupos();
            
            
            $grupos_miembro = $this->comunidad->getGruposMiembro();
            $grupo = $this->grupo->getNombreGrupo($id_grupo);
            $nombre_grupo = $grupo->nombre;
            $imagen_grupo = $grupo->imagen;
            $datos = "";
            switch ($seccion){
                case "todo":
                    $todo = $this->todo($id_grupo);
                    if($todo){
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            
                            'debates_recientes' => $todo['debates'],
                            'fotos_recientes' => $todo['fotos'],
                            'videos_recientes' => $todo['videos'],
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,                                             
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                case "fotos":
                    $fotos = $this->fotos($id_grupo,$ordenamiento,$pagina);
                    if($fotos){
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            
                            'titulo' => $fotos['titulo'],
                            'fotos' => $fotos['fotos'],
                            'total_paginas' => $fotos['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            
                            
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                case "debates":
                    $debates = $this->debates($id_grupo,$ordenamiento,$pagina);
					
                    if($debates){
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            
                            'debates' => $debates['debates'],
                            'total_paginas' => $debates['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            
                            
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                case "videos":
                    $videos = $this->videos($id_grupo,$ordenamiento,$pagina);
                    if($videos){
                        $datos = array(
                            'nombre_grupo' => $nombre_grupo,
                            'imagen_grupo' => $imagen_grupo,
                            
                            'titulo' => $videos['titulo'],
                            'videos' => $videos['videos'],
                            'total_paginas' => $videos['total_paginas'],
                            'ordenamiento' => $ordenamiento,
                            'pagina' => $pagina,
                            'seccion' => $seccion,
                            'id_grupo' => $id_grupo,
                            'grupos' => $grupos,
                            
                            
                            'grupos_miembro' => $grupos_miembro
                        );
                    }
                    break;
                
                default :
                    $this->load->view('errors/html/error_404.php');
            }
            $this->load->view('principal/novia/moderador/grupos',$datos);
        }else{
            $this->load->view('errors/html/error_404.php');
        }
    }
    
    public function todo($id_grupo){
        $debates = $this->grupo->getDebatesGrupo($id_grupo);
		$debates = $this->formatearFotos($debates);
        $fotos = $this->grupo->getFotosRecientesGrupo($id_grupo);
		$fotos = $this->formatearFotos($fotos);
        $videos = $this->grupo->getVideosRecientesGrupo($id_grupo);
		$videos = $this->formatearFotos($videos);
        $datos = array(
            'debates' => $debates,
            'fotos' => $fotos,
            'videos' => $videos
        );
        return $datos;
    }
    
	public function formatearFotos($fotos){
        $fotos2 = "";
        $i = 0;
        if($fotos){
            foreach ($fotos as $foto){
                if(!empty($foto->mime)){
                    $foto->foto_usuario = base_url()."index.php/novios/moderador/home/foto_usuario/$foto->id_usuario";
                }else{
                    $foto->foto_usuario = base_url().'dist/img/blog/perfil.png';
                }
                $fotos2[$i] = $foto;
                $i++;
            }
        }
        return $fotos2;
    }
	
    public function fotos($id_grupo,$ordenamiento,$pagina){
		if($this->checker->isLogin()){
			if (!$this->checker->isModerador()) {
				$this->load->view('errors/html/error_404.php');
			}
			}
        else{
            redirect("login");
        }
        $fotos = "";
        $inicio = 0;
        $titulo = "";
        $contador = $this->grupo->totalFotos($id_grupo);
        $total_paginas = (int)($contador / 16);
        if($total_paginas == 0){
                $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 16;
        }
        if($ordenamiento == 1 || $ordenamiento == "comentarios"){
            $fotos = $this->grupo->getComentadas($id_grupo,$inicio);
            $titulo = "Fotos Mas Comentadas";
        }else if($ordenamiento == "fecha"){
            $fotos = $this->grupo->getRecientes($id_grupo,$inicio);
            $titulo = "Fotos Mas Recientes";
        }else if($ordenamiento == "visitas"){
            $fotos = $this->grupo->getVisitadas($id_grupo,$inicio);
            $titulo = "Fotos Mas Visitadas";
        }else{
            $this->load->view('principal/novia/moderador/debate_eliminado');
        }
		$fotos = $this->formatearFotos($fotos);
        $datos = array(
            'titulo' => $titulo,
            'fotos' => $fotos,
            'total_paginas' => $total_paginas
        );
        return $datos;
    }
    
    public function debates($id_grupo,$ordenamiento,$pagina){
        $debates = "";
        $inicio = 0;
        $titulo = "";
        $contador = $this->grupo->totalDebatesUltimoMensaje($id_grupo);
        $total_paginas = (int)($contador / 16);
        if($total_paginas == 0){
                $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 16;
        }
        switch ($ordenamiento){
            case 1:
                $debates = $this->grupo->getUltimoMensaje($id_grupo,$inicio);
                break;
            case "mensajes":
                $debates = $this->grupo->getMensaje($id_grupo,$inicio);
                break;
            case "visitas":
                $debates = $this->grupo->getVisitasDebate($id_grupo,$inicio);
                break;
            case "ultimo_mensaje":
                $debates = $this->grupo->getUltimoMensaje($id_grupo,$inicio);
                break;
            case "fecha":
                $debates = $this->grupo->getDebatesRecientes($id_grupo,$inicio);
                break;
            case "sin_respuesta":
                $debates = $this->grupo->getDebateSinRespuesta($id_grupo,$inicio);
                break;
        }
		$debates = $this->formatearFotos($debates);
		
        $datos = array(
            'debates' => $debates,
            'total_paginas' => $total_paginas
        );
        return $datos;
    }
    
    public function videos($id_grupo,$ordenamiento,$pagina){
		if($this->checker->isLogin()){
			if (!$this->checker->isModerador()) {
				$this->load->view('errors/html/error_404.php');
			}
			}
        else{
            redirect("login");
        }
        $videos = "";
        $inicio = 0;
        $titulo = "";
        $contador = $this->grupo->totalVideos($id_grupo);
        $total_paginas = (int)($contador / 16);
        if($total_paginas == 0){
                $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 16;
        }
        if($ordenamiento == 1 || $ordenamiento == "comentarios"){
            $videos = $this->grupo->getComentados($id_grupo,$inicio);
            $titulo = "Videos Mas Comentados";
        }else if($ordenamiento == "fecha"){
            $videos = $this->grupo->getVideosRecientes($id_grupo,$inicio);
            $titulo = "Videos Mas Recientes";
        }else if($ordenamiento == "visitas"){
            $videos = $this->grupo->getVisitados($id_grupo,$inicio);
            $titulo = "Videos Mas Visitados";
        }else{
            $this->load->view('principal/novia/moderador/debate_eliminado');
        }
		$videos = $this->formatearFotos($videos);
        $datos = array(
            'titulo' => $titulo,
            'videos' => $videos,
            'total_paginas' => $total_paginas
        );
		
        return $datos;
    }
    
    public function validarFecha($fecha){
        $token = explode("_", $fecha);
        if(count($token) == 3){
            $dia = $token[0];
            $mes = $token[1];
            $anio = $token[2];
            if($anio % 4 == 0){
                if($mes == 02){
                    return $dia >= 1 && $dia <= 29;
                }
            }
            if($mes == 4 || $mes == 6 || $mes == 9 || $mes == 11){
                return $dia >= 1 && $dia <= 30;
            }else{
                if($mes == 1 || $mes == 3 || $mes == 5 || $mes == 7 || $mes == 8 || $mes == 10 || $mes == 12){
                    return $dia >= 1 && $dia <= 31;
                }
            }
            $this->load->view('errors/html/error_404.php');
        }
        $this->load->view('errors/html/error_404.php');
    }
    
    
    public function formatearMes($fecha){
        $mes = $fecha->format("F");
        $anio = $fecha->format("Y");
        switch ($mes){
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return  $mes;
    }
}

