<?php

class Video extends CI_Controller {
    public $input;    

    public function __construct() {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Moderador/Comunidad_model", "comunidad");
        $this->load->model("Moderador/Imagen_model", "imagen");
        $this->load->model("Moderador/Video_model", "video");
        $this->load->model("Moderador/Foto_model", "foto");
        $this->load->library("facebook");
        $this->URL_IMAGENES = $this->config->base_url() . "index.php/novios/comunidad/Home/imagen/";
        $this->load->helper('formats_helper');
    }
    
//    public function index(){
//        $videos_recientes = $this->video->getVideosRecientes();
//        $videos_vistos = $this->video->getVideosVistos();
//        $datos = array(
//            'videos_recientes' => $videos_recientes,
//            'videos_vistos' => $videos_vistos
//        );
//        $this->load->view('principal/novia/moderador/videos',$datos);
//    }
    
    public function nuevoVideo($grupo=""){
		if($this->checker->isLogin()){
			if (!$this->checker->isModerador()) {
				$this->load->view('errors/html/error_404.php');
			}
			}
        else{
            redirect("login");
        }
        $datos = $this->comunidad->getGrupos();
        $grupos = array(
            'grupos' => $datos,
			'selectGrupo' => str_replace("_", " ", $grupo)
        );
        $this->load->view('principal/novia/moderador/nuevo_video',$grupos);
    }
    
    public function publicarVideo(){
        if($_POST){
            $dato['grupo'] = $this->input->post('grupos',true);
            $dato['titulo'] = $this->input->post('titulo',true);
            $dato['descripcion'] = $this->input->post('descripcion');
            $dato['direccion_video'] = $this->input->post('direccion_video',TRUE);
            $dato['direccion_video'] = str_replace("watch?v=","embed/",$dato['direccion_video']);
            $result = $this->video->publicarVideo($dato);
            $result = "video".$dato['grupo']."-g".$result;
            $debates_publicados = $this->comunidad->numero_debates();
            $fotos_publicadas = $this->foto->numero_fotos();
            $videos_publicados = $this->video->numero_videos();
            $contador = $debates_publicados + $fotos_publicadas + $videos_publicados;
            switch ($contador){
                case 10:
                    $this->video->set_medallas($this->session->userdata('id_usuario'),20);
                    $this->video->set_medallas($this->session->userdata('id_usuario'),23);
                    break;
                case 20:
                    $this->video->set_medallas($this->session->userdata('id_usuario'),21);
                    break;
                case 50:
                    $this->video->set_medallas($this->session->userdata('id_usuario'),22);
                    break;
            }
            redirect("novios/moderador/Video/videoPublicado/$result");
        }
    }
    
   
    
    
    
    public function videoPublicado($id_video, $pagina = 1, $id_comentario=0){
		if($this->checker->isLogin()){
			if (!$this->checker->isModerador()) {
				$this->load->view('errors/html/error_404.php');
			}
			}
        else{
            redirect("login");
        }
        $token = explode("-g", $id_video);
        if(count($token) == 2){
            $id_grupo = explode("video", $token[0]);
            if(count($id_grupo) == 2){
                $grupos = $this->comunidad->getGrupos();
                
                
                $grupos_miembro = $this->comunidad->getGruposMiembro();
                $publicacion = $this->video->getVideoPublicado($id_grupo[1],$token[1]);
                $like = $this->video->validarLikeVideo($token[1]);
                $foto_usuario = $this->comunidad->getFoto($this->session->userdata('id_usuario'));
                if(!empty($foto_usuario->mime)){
                    $foto_logeado = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$this->session->userdata('id_usuario');
                }else{
                    $foto_logeado = base_url().'dist/img/blog/perfil.png';
                }
                if($publicacion){
                    if(isset($like->activo) && $like->activo == 1){
                        $like = "Cancelar Me Gusta";
                    }else{
                        $like = "Me Gusta";
                    }
                    $fecha_creacion2 = relativeTimeFormat($publicacion->fecha_creacion,$mini=FALSE);
                    if(strlen($fecha_creacion2) <= 10 && strlen($fecha_creacion2) > 4){
                        $fecha_creacion2 = "Hace $fecha_creacion2";
                    }else if(strlen($fecha_creacion2) > 10){
                        $fecha_creacion2 = "El $fecha_creacion2";
                    }
                    if(!empty($publicacion->mime_user)){
                        $publicacion->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$publicacion->id_usuario";
                    }else{
                        $publicacion->foto_usuario = base_url().'dist/img/blog/perfil.png';
                    }
                    $contador2 = $this->video->getContadorVideos($token[1]);
                    $contador = $contador2->comentarios;
                    $boda_user = $this->comunidad->getFechaBoda();
                    if(isset($boda_user->fecha_boda) && $boda_user->fecha_boda != null){
                        $fecha_bodaD = new DateTime($boda_user->fecha_boda);
                        $mes = $this->formatearMes($fecha_bodaD);
                        $anio = $fecha_bodaD->format("Y");
                        $dia = $fecha_bodaD->format("d");
                        $fecha_bodaD = $mes." ".$anio;
                        $fecha_bodaD2 = $dia." de ".$mes." de ".$anio;
                    }
                    $this->video->setVistaVideo($token[1]);
                    $vistas = $this->video->getVistasVideos($token[1]);
                    $comentarios2 = "";
                    $comentarios3 = "";
					$comentarioo = "";
                    if(isset($pagina) || $contador > 0 && $contador <= 10){
                        $total_paginas = (int)($contador / 16);
                        if($total_paginas < ($contador/16)){
                            $total_paginas++;
                        }
                        if($total_paginas == 0){
                                $total_paginas = 1;
                        }
                        if($pagina <= $total_paginas){
                            $inicio = ($pagina - 1) * 10;
                            $limite = $pagina * 10;
                            $limite = $contador - $limite;
							if($id_comentario != 0 && $pagina == 1){
								$comentarioo = $this->video->getRespuestaa($token[1],$id_comentario);
								
								if(isset($comentarioo->comentado) && $comentarioo->comentado==0){
									$comentarioo2 = $comentarioo;
									$comentarios = $this->video->getComentariosVideo2($token[1],$inicio,19,$id_comentario);
									}
								else if(isset($comentarioo->comentado)){
									$comentarioo2 = $this->video->getRespuestaa2($comentarioo->comentado);
									if(isset($comentarioo2)){
										$comentarios = $this->video->getComentariosVideo2($token[1],$inicio,19,$comentarioo->comentado);
									}	else{
										$id_comentario = 0;
										$comentarios = $this->video->getComentariosVideo($token[1],$inicio,$limite);
									}
									
								}
								else{
									$id_comentario = 0;
									 $comentarios = $this->video->getComentariosVideo($token[1],$inicio,$limite);
								}
							}
							else{
								$id_comentario = 0;
                             $comentarios = $this->video->getComentariosVideo($token[1],$inicio,$limite);}
                           
                            $respuestaComentarios = $this->video->getRespuestaComentarioVideo($token[1]);
                            $i = 0;
                            $fecha_creacion = "";
                            $fecha_boda = "";
							if(isset($comentarioo2) && $id_comentario !=0 && $pagina == 1){
							if($comentarioo2->fecha_boda != "" && $comentarioo2->fecha_boda != NULL){
                                    $fecha_boda = new DateTime($comentarioo2->fecha_boda);
                                    $mes = $this->formatearMes($fecha_boda);
                                    $anio = $fecha_boda->format("Y");
                                    $fecha_boda = $mes." ".$anio;
                                }
                                if($comentarioo2->fecha_creacion){
                                    $fecha_creacion = relativeTimeFormat($comentarioo2->fecha_creacion,$mini=FALSE);
                                    if(strlen($fecha_creacion) <= 10){
                                        $fecha_creacion = "Hace $fecha_creacion";
                                    }else{
                                        $fecha_creacion = "El $fecha_creacion";
                                    }
                                }
                                $comentarioo2->fecha_boda = $fecha_boda;
                                $comentarioo2->fecha_creacion = $fecha_creacion;
                                if(!empty($comentarioo2->mime)){
                                    $comentarioo2->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$comentarioo2->id_usuario";
                                }else{
                                    $comentarioo2->foto_usuario = base_url().'dist/img/blog/perfil.png';
                                }
                                $comentarios2[$i] = $comentarioo2;
							$i++;}
                            foreach ($comentarios as $comentario){
                                if($comentario->fecha_boda != "" && $comentario->fecha_boda != NULL){
                                    $fecha_boda = new DateTime($comentario->fecha_boda);
                                    $mes = $this->formatearMes($fecha_boda);
                                    $anio = $fecha_boda->format("Y");
                                    $fecha_boda = $mes." ".$anio;
                                }
                                if($comentario->fecha_creacion){
                                    $fecha_creacion = relativeTimeFormat($comentario->fecha_creacion,$mini=FALSE);
                                    if(strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4){
                                        $fecha_creacion = "Hace $fecha_creacion";
                                    }else if(strlen($fecha_creacion2) > 10){
                                        $fecha_creacion = "El $fecha_creacion";
                                    }
                                }
                                if(!empty($comentario->mime)){
                                    $comentario->foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/$comentario->id_usuario";
                                }else{
                                    $comentario->foto_usuario = base_url().'dist/img/blog/perfil.png';
                                }
                                $comentario->fecha_boda = $fecha_boda;
                                $comentario->fecha_creacion = $fecha_creacion;
                                $comentarios2[$i] = $comentario;
                                $i++;
                            }
                            foreach ($respuestaComentarios as $comentario){
                                if($comentario->fecha_boda != "" && $comentario->fecha_boda != NULL){
                                    $fecha_boda = new DateTime($comentario->fecha_boda);
                                    $mes = $this->formatearMes($fecha_boda);
                                    $anio = $fecha_boda->format("Y");
                                    $fecha_boda = $mes." ".$anio;
                                }
                                if($comentario->fecha_creacion){
                                    $fecha_creacion = relativeTimeFormat($comentario->fecha_creacion,$mini=FALSE);
                                    if(strlen($fecha_creacion) <= 10){
                                        $fecha_creacion = "Hace $fecha_creacion";
                                    }else{
                                        $fecha_creacion = "El $fecha_creacion";
                                    }
                                }
                                $comentario->fecha_boda = $fecha_boda;
                                $comentario->fecha_creacion = $fecha_creacion;
                                $comentarios3[$i] = $comentario;
                                $i++;
                            }
                        }
                    }
                    $data = array(
                        'grupo' => $publicacion->nombre,
						'activo' => $publicacion->activo,
                        'url_grupo' => base_url()."index.php/novios/comunidad/group/grupo/$publicacion->id_grupo/todo",
                        'url_perfil' => base_url()."index.php/novios/comunidad/perfil/usuario/".$publicacion->id_usuario,
                        'id_usuario' => $publicacion->id_usuario,
                        'titulo_video' => $publicacion->titulo,
                        'id_video' => $token[1],
                        'descripcion' => $publicacion->descripcion,
                        'direccion_web' => $publicacion->direccion_web,
                        'usuario' => $publicacion->usuario,
						'id_denuncia' => $id_comentario,
                        'foto_usuario' => $publicacion->foto_usuario,
                        'foto_logeado' => $foto_logeado,
                        'fecha_creacion' => $fecha_creacion2,
                        'contador' => $contador,
                        'total_paginas' => $total_paginas,
                        'pagina' => $pagina,
                        'vistas' => $vistas->vistas,
                        'url_video' => $id_video,
                        'relacion' => '0',
                        'visita_perfil' => '',
                        'like' => $like,
                        'comentarios' => $comentarios2,
                        'comentarios2' => $comentarios3,
                        'grupos' => $grupos,
                        'grupos_miembro' => $grupos_miembro,
						'comentarioo' => $comentarioo						
                    ); 
                    $this->load->view('principal/novia/moderador/video_publicado',$data);
                }else{
                    $this->load->view('errors/html/error_404.php');
                }
            }else{
                $this->load->view('errors/html/error_404.php');
            }
        }else{
            $this->load->view('errors/html/error_404.php');
        }
    }
	
	public function desactivarVideo(){
        if($_POST){
            $id_video = $this->input->post("video", TRUE);
            $value = $this->input->post("value", TRUE);
            $result = $this->comunidad->desactivarVideo($id_video, $value);
            if($result){
                return $this->output
                                    ->set_content_type('application/json')
                                    ->set_status_header(202)
                                    ->set_output(json_encode(array(
                                        'success' => TRUE
                                    )
                            )
                );
            }
            return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(404)
                                ->set_output(json_encode(array(
                                    'success' => FALSE
                                )
                        )
            );
        }
    }
    
    public function likeVideo(){
        if($_POST){
            $id_video = $this->input->post("video", TRUE);
            $result = $this->video->validarLikeVideo($id_video);
            if(isset($result->activo) && $result->activo == 0){
                $result = $this->video->updateLikeVideo($id_video,1,$result->id_me_gusta);
                $data = "Cancelar Me Gusta";
            }else if(isset ($result->activo) && $result->activo == 1){
                $result = $this->video->updateLikeVideo($id_video,0,$result->id_me_gusta);
                $data = "Me Gusta";
            }else{
                $result = $this->video->setLikeVideo($id_video);
                $data = "Cancelar Me Gusta";
            }
            if($result){
                return $this->output
                                    ->set_content_type('application/json')
                                    ->set_status_header(202)
                                    ->set_output(json_encode(array(
                                        'success' => TRUE,
                                        'data' => $data,
                                    )
                            )
                );
            }
            return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(404)
                                ->set_output(json_encode(array(
                                    'success' => FALSE,
                                    'data' => 'error'
                                )
                        )
            );
        }
    }
    
    public function formatearMes($fecha){
//        echo date('Y-m-d h:i:s');
//        $result = $this->comunidad->conocerCompaneros();
//        $fecha = new DateTime($result->fecha_boda);
//        echo $fecha->format("F Y");
        $mes = $fecha->format("F");
        $anio = $fecha->format("Y");
        switch ($mes){
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return  $mes;
    }
    
    public function comentariosVideo(){
        if($_POST){
            $datos['video'] = $this->input->post('video', TRUE);
            $datos['mensaje'] = $this->input->post('mensaje');
            $datos['permiso_notificacion'] = $this->input->post('permiso_notificacion', TRUE);
            $datos['fecha_creacion'] = date('Y-m-d H:i:s');
            if($datos['permiso_notificacion'] != NULL){
                $datos['permiso_notificacion'] = 1;
            }else{
                $datos['permiso_notificacion'] = 0;
            }
            $result = $this->video->comentariosVideo($datos);
            if($result){
				$fecha="";
                if(!empty($result->mime)){
                    $datos['foto_usuario'] = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$this->session->userdata('id_usuario');
                }else{
                    $datos['foto_usuario'] = base_url().'dist/img/blog/perfil.png';
                }
                $fecha = new DateTime($result->fecha_boda);
                $mes = $this->formatearMes($fecha);
                $anio = $fecha->format("Y");
                $fecha_creacion = relativeTimeFormat($datos['fecha_creacion'],$mini=FALSE);
                $fecha = $mes." ".$anio;
                $datos['fecha_boda'] = $fecha;
                $datos['estado_boda'] = $result->estado_boda;
                $datos['fecha_creacion'] = $fecha_creacion;
                $datos['usuario'] = $result->usuario;
                $datos['id_comentario'] = $result->id_comentario;
                $datos['activo'] = $result->activo;
                $datos['mensaje'] = $result->comentario;
            }
            return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $datos,
                                )
                        )
                );
            
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }
    
    public function respuestaComentario(){
        if($_POST){
            $datos['video'] = $this->input->post('video', TRUE);
            $datos['mensaje'] = $this->input->post('mensaje');
            $datos['respuesta'] = $this->input->post('respuesta', TRUE);
            $datos['fecha_creacion'] = date('Y-m-d H:i:s');
            $result = $this->video->respuestaComentarioVideo($datos);
            if($result){
				$fecha_boda="";
                if(!empty($respuesta->mime)){
                            $foto_usuario = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$respuesta->usuarioCO;
                        }else{
                            $foto_usuario = base_url().'dist/img/blog/perfil.png';
                        }
						$fecha = new DateTime($result->fecha_boda);
                $mes = $this->formatearMes($fecha);
                $anio = $fecha->format("Y");
                $fecha_creacion = relativeTimeFormat($datos['fecha_creacion'],$mini=FALSE);
                $fecha = $mes." ".$anio;
                $fecha_boda = $fecha;
                $estado_boda = $result->estado_boda;
                $fecha_creacion2 = $fecha_creacion;
                $usuario = $result->usuario;
                $id_comentario = $result->comentado;
                $data = array(
                    'url_foto' => $foto_usuario,
                    'fecha_boda' => $fecha_boda,
                    'estado_boda' => $estado_boda,
                    'fecha_creacion' => $fecha_creacion2,
                    'usuario' => $usuario,
                    'mensaje' => $id_comentario,
                    'comentario' => $datos['mensaje'],
                );
            }
            return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $data,
                                )
                        )
                );
            
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }
    
    function getRespuesta(){
        if($_POST){
            $id_comentario = $this->input->post('comentario', TRUE);
            $result = $this->video->getRespuestaVideo($id_comentario);
            if ($result){
                $i = 0;
                foreach ($result as $respuesta){
					$fecha_boda ="";
                    if($respuesta->fecha_boda != "" && $respuesta->fecha_boda != NULL){
                        $fecha_boda = new DateTime($respuesta->fecha_boda);
                        $mes = $this->formatearMes($fecha_boda);
                        $anio = $fecha_boda->format("Y");
                        $fecha_boda = $mes." ".$anio;
                    }
                    if($respuesta->fecha_creacion){
                        $fecha_creacion = relativeTimeFormat($respuesta->fecha_creacion,$mini=FALSE);
                        if(strlen($fecha_creacion) <= 10){
                            $fecha_creacion = "Hace $fecha_creacion";
                        }else{
                            $fecha_creacion = "El $fecha_creacion";
                        }
                    }
					if(!empty($respuesta->mime)){
                            $respuesta->url_foto = base_url()."index.php/novios/comunidad/Home/foto_usuario/".$respuesta->usuarioCO;
                        }else{
                            $respuesta->url_foto = base_url().'dist/img/blog/perfil.png';
                        }
                    $respuesta->fecha_boda = $fecha_boda;
                    $respuesta->fecha_creacion = $fecha_creacion;
                    $respuesta2[$i] = $respuesta;
                    $i++;
                }
                $datos = array('respuestas' => $respuesta2);
                return $this->output
                                ->set_content_type('application/json')
                                ->set_status_header(202)
                                ->set_output(json_encode(array(
                                    'success' => true,
                                    'data' => $datos,
                                )
                        )
                );
            }
        }
        return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(404)
                        ->set_output(json_encode(array(
                            'success' => false,
                            'data' => 'error'
        )));
    }
    
 }

