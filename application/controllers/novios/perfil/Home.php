<?php
use Illuminate\Database\Capsule\Manager as DB;
class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->model("Perfil_model", "perfil");
        $this->load->library("facebook");
    }

    protected function middleware()
    {
        return ["user_auth"];
    }

    public function index()
    {
        $datos               = $this->recuperar_datos();
        $datos["id_usuario"] = $this->session->userdata("id_usuario");
        $datos["countries"] = $this->getCountries();
        $this->load->view("principal/novia/perfil/index", $datos);
    }

    public function getCountries(){
        $countries = DB::table('pais')->get();
        return $countries;
    }

    public function update()
    {
        if ($_POST) {
            $datos["usuario"]     = $this->input->post("usuario", true, "String", ["maxSize" => 190]);
            $datos["telefono"]    = $this->input->post("telefono", true, "String", ["maxSize" => 20]);
            $datos["sobre_mi"]    = $this->input->post("sobre_mi", true, "String", ["maxSize" => 250]);
            $datos["nombre"]      = $this->input->post("nombre", true, "String", ["maxSize" => 32]);
            $datos["apellido"]    = $this->input->post("apellido", true, "String", ["maxSize" => 50]);
            $datos["foto"]        = $this->input->post("file");
            $datos["fecha_boda"]  = $this->input->post("fecha_boda");
            $datos["no_invitado"] = $this->input->post("no_invitado", true, "String", ["maxSize" => 11]);
            $datos["sobre_boda"]  = $this->input->post("sobre_boda", true, "String", ["maxSize" => 250]);
            $datos["pais_boda"]   = $this->input->post("country", true, "String", ["maxSize" => 30]);
            $datos["estado_boda"] = $this->input->post("state", true, "String", ["maxSize" => 30]);
            $datos["ciudad_boda"] = $this->input->post("city", true, "String", ["maxSize" => 30]);
            $datos["color"]       = $this->input->post("seleccion-color", true, "String", ["maxSize" => 30]);
            $datos["estacion"]    = $this->input->post("seleccion-estacion", true, "String", ["maxSize" => 30]);
            $datos["estilo"]      = $this->input->post("seleccion-estilo", true, "String", ["maxSize" => 30]);
            $datos["genero"]      = $this->input->post("genero");

            if ($datos && $this->input->isValid()) {
                $this->perfil->update($datos);
                $this->session->set_userdata("tipo_mensaje", "success");
                $this->session->set_userdata("mensaje", "Datos de perfil actualizados");
                $this->session->set_userdata("nombre", $datos["nombre"]." ".$datos["apellido"]);
                redirect("novios/perfil");
            }

            $mensaje = $this->input->getErrors();
            $this->session->set_userdata("tipo_mensaje", "danger");
            $this->session->set_userdata("mensaje", $mensaje);
        }
        redirect("novios/perfil");
    }

    public function recuperar_datos()
    {
        $data                 = $this->perfil->getCliente();
        $datos["id_cliente"]  = $data->id_cliente;
        $datos["id_usuario"]  = $data->id_usuario;
        $datos["id_boda"]     = $data->id_boda;
        $datos["usuario"]     = $data->usuario;
        $datos["tipo"]        = $data->genero;
        $datos["telefono"]    = $data->telefono;
        $datos["sobre_mi"]    = $data->sobre_mi;
        $datos["nombre"]      = $data->nombre;
        $datos["apellido"]    = $data->apellido;
        $datos["mime"]        = $data->mime;
        $datos["foto"]        = $data->foto;
        $datos["fecha_boda"]  = $data->fecha_boda;
        $datos["color"]       = $data->color;
        $datos["estacion"]    = $data->estacion;
        $datos["estilo"]      = $data->estilo;
        $datos["no_invitado"] = $data->no_invitado;
        $datos["sobre_boda"]  = $data->sobre_boda;
        $datos["pais_boda"]   = $data->pais_boda;
        $datos["estado_boda"] = $data->estado_boda;
        $datos["ciudad_boda"] = $data->ciudad_boda;

        //dd($datos);
        return $datos;
    }

    public function configuracion()
    {
        $data  = $this->perfil->datosConfiguracion();
        $datos = "";
        if ($data) {
            if ( ! empty($data->id_facebook)) {
                $datos["facebook"] = $data->id_facebook;
            }
            if ( ! empty($data->visibilidad_todos)) {
                $datos["visibilidad_todos"] = $data->visibilidad_todos;
            }
            if ( ! empty($data->correo)) {
                $datos["correo"] = $data->correo;
            }
        }
        $this->load->view("principal/novia/perfil/configuracion", $datos);
    }

    public function notificaciones()
    {
        $data  = $this->perfil->datosNotificaciones();
        $datos = "";
        if ($data) {
            $datos["enviar_mensaje"]        = $data->enviar_mensaje;
            $datos["participacion_debates"] = $data->participacion_debates;
            $datos["valorar_publicaciones"] = $data->valorar_publicaciones;
            $datos["anadir_amigo"]          = $data->anadir_amigo;
            $datos["aceptar_solicitud"]     = $data->aceptar_solicitud;
            $datos["mension_posts"]         = $data->mension_posts;
            $datos["email_diario"]          = $data->email_diario;
            $datos["email_semanal"]         = $data->email_semanal;
            $datos["invitaciones"]          = $data->invitaciones;
            $datos["concursos"]             = $data->concursos;
        }
        $this->load->view("principal/novia/perfil/notificaciones", $datos);
    }

    public function vincularFacebook()
    {
        $user    = $this->facebook->get_user_configuracion();
        $user_fb = $user["id"];
        $this->perfil->vincularFacebook($user_fb);
        redirect("novios/perfil/Home/configuracion");
    }

    public function validarPassword()
    {
        $password = $this->perfil->validarPassword();
        $password = $password->contrasena;
        $password = sha1(md5(sha1($password)));
        $vacio    = sha1(md5(sha1("")));
        if ($password != $vacio && $password != null) {
            $password = 202;
            $this->perfil->desvincularFacebook();
        } else {
            $password = 203;
        }
        print_r($password);
    }

    public function updateEmail()
    {
        if ($_POST) {
            $datos["correo"] = $this->input->post("nuevo_correo", true);
            if ($datos) {
                $this->perfil->updateEmail($datos);
            }
        }
        redirect("novios/perfil/Home/configuracion");
    }

    public function updatePassword()
    {
        if ($_POST) {
            $datos["contrasena"] = $this->input->post("nueva_contrasena", true);
            if ($datos) {
                $password = sha1(md5(sha1($datos["contrasena"])));
                $this->perfil->updatePassword($password);
            }
        }
        redirect("novios/perfil/Home/configuracion");
    }

    public function permisoVisibilidad()
    {
        if ($_POST) {
            $permiso = $this->input->post("visibilidad_todos");
            if ($permiso == "aceptar") {
                $permiso = 1;
            } else {
                $permiso = 0;
            }
            $this->perfil->permisoVisibilidad($permiso);
            redirect("novios/perfil/Home/configuracion");
        }
        show_404();
    }

    public function deleteCount()
    {
        $result = $this->perfil->deleteCount();
        if ($result) {
            return $this->output
                ->set_content_type("application/json")
                ->set_status_header(200)
                ->set_output(json_encode(["success" => true, "data" => true,]));
        }

        return $this->output
            ->set_content_type("application/json")
            ->set_status_header(404)
            ->set_output(json_encode(["success" => false, "data" => "error",]));

    }

    public function permisosNotificaciones()
    {
        if ($_POST) {
            $datos["enviar_mensaje"]        = $this->input->post("enviar_mensaje", true);
            $datos["participacion_debates"] = $this->input->post("participacion_debates", true);
            $datos["valorar_publicaciones"] = $this->input->post("valorar_publicaciones", true);
            $datos["anadir_amigo"]          = $this->input->post("anadir_amigo", true);
            $datos["aceptar_solicitud"]     = $this->input->post("aceptar_solicitud", true);
            $datos["mension_posts"]         = $this->input->post("mension_posts", true);
            $datos["email_diario"]          = $this->input->post("email_diario", true);
            $datos["email_semanal"]         = $this->input->post("email_semanal", true);
            $datos["invitaciones"]          = $this->input->post("invitaciones", true);
            $datos["concursos"]             = $this->input->post("concursos", true);

            if ($datos) {
                if ($datos["enviar_mensaje"] == "aceptar") {
                    $datos["enviar_mensaje"] = 1;
                } else {
                    $datos["enviar_mensaje"] = 0;
                }
                if ($datos["participacion_debates"] == "aceptar") {
                    $datos["participacion_debates"] = 1;
                } else {
                    $datos["participacion_debates"] = 0;
                }
                if ($datos["valorar_publicaciones"] == "aceptar") {
                    $datos["valorar_publicaciones"] = 1;
                } else {
                    $datos["valorar_publicaciones"] = 0;
                }
                if ($datos["anadir_amigo"] == "aceptar") {
                    $datos["anadir_amigo"] = 1;
                } else {
                    $datos["anadir_amigo"] = 0;
                }
                if ($datos["aceptar_solicitud"] == "aceptar") {
                    $datos["aceptar_solicitud"] = 1;
                } else {
                    $datos["aceptar_solicitud"] = 0;
                }
                if ($datos["mension_posts"] == "aceptar") {
                    $datos["mension_posts"] = 1;
                } else {
                    $datos["mension_posts"] = 0;
                }
                if ($datos["email_diario"] == "aceptar") {
                    $datos["email_diario"] = 1;
                } else {
                    $datos["email_diario"] = 0;
                }
                if ($datos["email_semanal"] == "aceptar") {
                    $datos["email_semanal"] = 1;
                } else {
                    $datos["email_semanal"] = 0;
                }
                if ($datos["invitaciones"] == "aceptar") {
                    $datos["invitaciones"] = 1;
                } else {
                    $datos["invitaciones"] = 0;
                }
                if ($datos["concursos"] == "aceptar") {
                    $datos["concursos"] = 1;
                } else {
                    $datos["concursos"] = 0;
                }
                $this->perfil->permisosNotificaciones($datos);
            }
        }
        redirect("novios/perfil/Home/notificaciones");
    }

    public function deleteImage()
    {
        $this->output->set_content_type("application/json");

        $user = User::find($this->input->post("userId"));

        if ( ! $user) {
            return $this->output
                ->set_status_header(404)
                ->set_output(json_encode([
                    "success" => false,
                ]));
        }

        $user->foto = null;
        $user->save();

        return $this->output
            ->set_status_header(202)
            ->set_output(json_encode([
                "success" => true,
                "data"    => $user,
            ]));
    }

    public function checkUser()
    {
        if (isset($_POST["usuario"])) {
            return $this->output
                ->set_content_type("application/json")
                ->set_status_header(202)
                ->set_output(json_encode(
                        [
                            "success" => true,
                            "data"    => User::where(function ($query) {
                                return $query->where("usuario", $_POST["usuario"])
                                    ->orWhere("correo", $_POST["usuario"]);
                            })
                                ->where("id_usuario", "!=", $this->session->userdata("id_usuario"))
                                ->exists(),
                        ]
                    )
                );
        }

        return $this->output
            ->set_content_type("application/json")
            ->set_status_header(404)
            ->set_output(json_encode(["success" => false, "data" => "error",]));

    }

}
