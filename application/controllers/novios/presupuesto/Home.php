<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;
class Home extends MY_Controller
{
    protected function middleware()
    {
        return ["user_auth"];
    }

    public function __construct()
    {
        parent::__construct();
        $this->load->library("checker");
        $this->load->helper("formats_helper");
        $this->load->library("task");
        $this->load->library("categoria");
        $this->load->model("Presupuesto_model", "presupuesto");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Tipo_proveedor_model", "tipo_proveedor");
        $this->load->model("Boda_model", "boda");
        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Cliente_model", "cliente");
        $this->load->model("Invitados/Invitado_model", "invitado");
        $this->load->model("Proveedor/ProveedorBoda_model", "ProveedorBoda_model");
        $this->load->model("DefaultPorcentaje_model", "default_porcentaje");
    }

    public function index()
    {
        $idBoda                 = $this->session->userdata("id_boda");
        $data["boda"]           = $this->boda->get($idBoda);
        $categorias_presupuesto = $this->default_porcentaje->getCategoriasFull();
        $wedding                = Wedding::with("budgets.payments")->find($idBoda);

        $categories = Category::with([
            "budgets"       => function ($query) use ($idBoda) {
                return $query->where("id_boda", $idBoda);
            },
            "subcategories" => function ($query) use ($idBoda) {
                return $query->whereHas("budgets", function ($query) use ($idBoda) {
                    return $query->where("id_boda", $idBoda);
                })->with([
                    "budgets" => function ($query) use ($idBoda) {
                        return $query->where("id_boda", $idBoda);
                    },
                ]);
            },
        ])->get();

        $data["categories"]  = $categories;
        $data["presupuesto"] = $presupuesto = $this->boda->get($idBoda)->presupuesto;

        foreach ($categories as $category) {
            $category->total_approximate = $category->total_final = $category->total_paid = 0;

            /*foreach ($category->subcategories as $key => $subcategory) {
                $budget = $subcategory->budgets->first();
                $subcategory->status =  $budget->status;
                $subcategory->id_presupuesto =  $budget->id_presupuesto;
                if (is_null($budget)) {
                    $subcategory->paid           = "0.00";
                    $subcategory->cost_type      = "approximate";
                    $subcategory->cost           = "0.00";
                    $category->total_approximate += $subcategory->cost;
                } else {
                    if (is_null($budget->costo_final)) {
                        $subcategory->cost_type      = "approximate";
                        $subcategory->cost           = $budget->costo_aproximado == 0 ? $presupuesto * ($budget->porcentaje / 100) : $budget->costo_aproximado;
                        $category->total_approximate += $subcategory->cost;
                    } else {
                        $subcategory->cost_type = "final";
                        $subcategory->cost      = $budget->costo_final;
                        $category->total_final  += $subcategory->cost;
                    }
                    $subcategory->paid = $budget->payments()->where("pagado", 1)->get()->sum("monto");
                }

                $category->total_paid += $subcategory->paid;
            }*/

            foreach ($category->budgets as $key => $budget) {
                $budget->cost = is_null($budget->costo_final) ? $budget->costo_aproximado : $budget->costo_final;
                $budget->paid = $budget->payments()->where("pagado", 1)->get()->sum("monto");
            }
        }
        $data["usuario"] = $this->usuario->get($this->session->userdata("id_usuario"));
        // dd($data['usuario']->id_usuario);

        //////////////////////CODE QR////////////////////////////////////
        // $verifQR = User::find($data['usuario']->id_usuario);
        // $dir = 'uploads/images/QR/';
        // // dd($verifQR['original']['codigo_qr']);
        // if($verifQR['original']['codigo_qr']==null) {
        //     $uniqid = uniqid();
        //     $filename = $dir.$uniqid.'.png';
        //     // dd($filename);
        //     if(!file_exists(base_url().$filename)) {
        //         // dd("ENTRE");
        //         $verifQR->codigo_qr = $uniqid.'.png';
        //         $verifQR->save();
        //         $tamanio = 10;
        //         $level = 'H';
        //         $frameSize = 0;
        //         $contenido = $data['usuario']->id_usuario;

        //         QRcode::png($contenido, $filename, $level, $tamanio, $frameSize);
        //         $data['rutaQR'] = base_url().$filename;
        //     }
        // } else {
        //     $filename = $dir.$verifQR->codigo_qr;
        //     if(file_exists($filename)) {
        //         $data['rutaQR'] = base_url().$filename;
        //     }
            
        // }
        // dd($data['rutaQR']);
        // dd($data['categories'][0]);
        // $this->load->view("principal/newHeader");
        $this->load->view("principal/newheadernovia");
        $this->load->view("principal/novia/menu");
        $this->load->view("principal/novia/presupuesto/index", $data);
        $this->load->view("japy/prueba/footer");
    }

    public function nota()
    {
        $this->output->set_content_type("application/json");
        $error = "error";
        if ($_POST) {
            $presupuesto = [
                "nota" => $this->input->post("nota", true, "String", ["min" => 0, "max" => 255]),
            ];
            $id          = $this->input->post("presupuesto", true, "Integer", ["min" => 1]);
            if ($this->input->isValid()) {
                $b = $this->presupuesto->update($id, $presupuesto);
                if ($b) {
                    return $this->output
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => 'ok',
                        ]));
                }
            }
            $error = $this->input->getErrors();
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => $error,
            ]));
    }
    
    public function update()
    {
        $this->output->set_content_type('application/json');
        $error = "error";

        if ($_POST) {
            $tipo = $this->input->post("type", true, "String", ["min" => 1]);
            $id   = $this->input->post("presupuesto", true, "Integer", ["min" => 1]);

            $presupuesto = [
                "$tipo" => $this->input->post("data", true, "String", ["min" => 1]),
            ];

            if ($this->input->isValid()) {
                $b = $this->presupuesto->update($id, $presupuesto);
                if ($b) {
                    return $this->output->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => 'ok',
                        ]));
                }
            }
            $error = $this->input->getErrors();
        }

        return $this->output->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => $error,
            ]));
    }

    public function crear()
    {
        $error = "error";
        if ($_POST) {
            $categoria   = $this->input->post("categoria", true, "String", ["min" => 1]);
            $presupuesto = [
                "categoria"   => $categoria,
                "id_boda"     => $this->session->userdata("id_boda"),
                "category_id" => $categoria,
            ];
            if ($this->input->isValid()) {
                $b = $this->presupuesto->insert($presupuesto);
                if ($b) {
                    $id = $this->presupuesto->last_id();

                    return $this->output->set_content_type('application/json')
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $this->presupuesto->get($id),
                        ]));
                }
            }
            $error = $this->input->getErrors();
        }

        return $this->output->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => $error,
            ]));
    }

    public function nuevo_tarea()
    {
        $error = "error";
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $tarea       = $this->input->post("tarea", true, "Integer", ["min" => 1]);
            $presupuesto = [
                'categoria'           => $this->input->post('categoria', true, "Integer", ['min' => 1]),
                'categoria_proveedor' => $this->input->post("categoria", true, "Integer", ["min" => 1]),
                'category_id'         => $this->input->post("categoria"),
                "nombre"              => $this->input->post("nombre", true, "String", ["min" => 1]),
                "id_boda"             => $this->session->userdata("id_boda"),
                "costo_aproximado"    => is_numeric($this->input->post("costo_aproximado")) ? $this->input->post("costo_aproximado") : "",
                "costo_final"         => is_numeric($this->input->post("costo_final")) ? $this->input->post("costo_final") : "",
            ];

            if ($this->input->isValid() && ! is_null($presupuesto['categoria'])) {
                $presupuesto['categoria'] = $this->default_porcentaje->getCategoriaPresupuesto($presupuesto['categoria']);
                $b                        = $this->presupuesto->insert($presupuesto);
                if ($b) {
                    $id = $this->presupuesto->last_id();
                    $this->tarea->update($tarea, ["id_presupuesto" => $id]);

                    return $this->output->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => $this->presupuesto->get($id),
                        ]));
                }
            }
            $error = $this->input->getErrors();
        }

        return $this->output->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => $error,
            ]));
    }

    public function eliminar()
    {
        $error = "error";
        if ($_POST) {
            $id = $this->input->post("presupuesto", true, "Integer", ["min" => 1]);
            if ($this->input->isValid()) {
                $b = $this->presupuesto->delete($id);
                if ($b) {
                    return $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(202)
                        ->set_output(json_encode([
                            'success' => true,
                            'data'    => 'ok',
                        ]));
                }
            }
            $error = $this->input->getErrors();
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(404)
            ->set_output(json_encode([
                'success' => false,
                'data'    => $error,
            ]));
    }

    public function editar()
    {
        $id_boda             = $this->session->userdata("id_boda");
        $data["boda"]        = $this->boda->get($id_boda);
        $data["usuario"]     = $this->usuario->get($this->session->userdata('id_usuario'));
        $data["cliente"]     = $this->cliente->get($this->session->userdata('id_cliente'));
        $data["presupuesto"] = $this->boda->getPresupuesto($id_boda);
        $data["pendientes"]  = $this->boda->getTareasPendientes($id_boda);
        $data["tareas_prox"] = $this->tarea->getProximas($id_boda);
        $data["confirmados"] = $this->invitado->getConfirmados();
        $data["proveedores"] = $this->ProveedorBoda_model->proveedores_boda()->proveedores;
        $grupo               = $this->invitado->getGrupoNovios($id_boda);

        if ($grupo) {
            $data["novios"] = $this->invitado->getList(["id_boda" => $id_boda, "id_grupo" => $grupo->id_grupo]);
        }

        if ( ! $data["novios"]) {
            $data["novios"] = $this->invitado->getName($this->session->userdata('id_usuario'));
        }

        $data["presupuesto"] = $this->boda->get($id_boda)->presupuesto;
        /*     $data["categorias"] = $this->default_porcentaje->getCategorias();
                     foreach ($data["categorias"] as $key => &$c) {
                 foreach ($c as $key => $p) {
                     $cat = $this->presupuesto->getCategoria($id_boda, $key);
                     if ($cat) {
                         $p = $cat->porcentaje;
                     }
                 }
             }*/

        $data['categories'] = Category::where('parent_id', 1)->get();
        $data['editar']     = "editar";

        $this->load->view("principal/newheadernovia");
        $this->load->view("principal/novia/menu");
        $this->load->view("principal/novia/miperfil/presupuestador", $data);
        $this->load->view("japy/prueba/footer");
    }

    public function budget()
    {
        $response = [
            "code"    => 404,
            "data"    => [],
            "message" => "Concepto inexistente",
        ];

        $wedding = Wedding::find($this->session->id_boda);

        if ($this->input->post("type") && $this->input->post("type") == "create") {
            $category = Category::find($this->input->post("category_id"));

            if ($category) {

                $newBudget = [
                    "categoria"           => $category->id,
                    "nombre"              => $this->input->post("concept") ? $this->input->post("concept") : $category->name,
                    "costo_aproximado"    => $this->input->post("approximateCost") ? $this->input->post("approximateCost") : 0,
                    "costo_final"         => $this->input->post("finalCost") ? $this->input->post("finalCost") : 0,
                    "nota"                => $this->input->post("notes"),
                    "id_boda"             => $wedding->id_boda,
                    "porcentaje"          => 0,
                    "categoria_proveedor" => 0,
                    "category_id"         => $category->id,
                    "fecha_asignada"      => $this->input->post("date"),
                ];

                $budget = Budget::create($newBudget);
            }
        } else {
            $budget = Budget::with(["category", "payments"])->find($this->input->post("id"));

            if ($budget && $wedding) {
                //Update
                if ($this->input->post("type") == "update") {
                    $budgetUpdate = [
                        "nombre" => $this->input->post("concept"),
                        "nota"   => $this->input->post("notes"),
                        "fecha_asignada" => $this->input->post("date"),
                    ];

                    $this->input->post("approximateCost") ? $budgetUpdate["costo_aproximado"] = $this->input->post("approximateCost") : "";
                    $this->input->post("finalCost") ? $budgetUpdate["costo_final"] = $this->input->post("finalCost") : "";
                    $this->input->post("date") ? $budgetUpdate["fecha_asignada"] = $this->input->post("date") : "";

                    $budget->update($budgetUpdate);

                    if ($this->input->post("newPayment")) {
                        $budget->payments()->create([
                            "monto"  => $this->input->post("newPayment"),
                            "pagado" => 1,
                        ]);
                    }
                } elseif ($this->input->post("type") == "remove") {
                    if ($budget->delete()) {
                        $response["code"]    = 200;
                        $response["message"] = "Ok";
                    } else {
                        $response["code"]    = 500;
                        $response["message"] = "Error";
                    }
                }
            }
        }

        if ($budget) {
            $response["data"]["id"]              = $budget->id_presupuesto;
            $response["data"]["category"]        = $budget->category->subcategories->count() ? $budget->category->name : $budget->category->parent->name;
            $response["data"]["category_id"]     = $budget->category->subcategories->count() ? $budget->category->id : $budget->category->parent->id;
            $response["data"]["subcategory_id"]  = $budget->category->subcategories->count() ? 0 : $budget->category->id;
            $response["data"]["concept"]         = $budget->nombre ? $budget->nombre : $budget->category->name;
            $response["data"]["approximateCost"] = number_format($budget->costo_aproximado, 2);
            $response["data"]["finalCost"]       = number_format($budget->costo_final, 2);
            $response["data"]["notes"]           = $budget->nota;
            $response["data"]["date"]           = $budget->fecha_asignada;
            $response["data"]["totalPaid"]       = number_format($budget->payments()->where("pagado",
                1)->get()->sum("monto"), 2);

            $response["code"]    = 200;
            $response["message"] = "Ok";

        }

        return $this->output
            ->set_content_type("application/json")
            ->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function firstBudget()
    {
        if ($_POST) {
            $data["id_boda"] = $this->session->userdata("id_boda");
            $data["budget"]  = $this->input->post("budget");
            $data["invited"] = $this->input->post("invited");

            $categories = Category::notMain()->get();
            $wedding    = Wedding::find($data["id_boda"]);

            if ($data && $this->input->isValid() && $wedding) {
                $wedding->budgets()->delete();
                $wedding->update([
                    "presupuesto" => $data["budget"],
                    "no_invitado" => $data["invited"],
                ]);

                if ($wedding) {
                    $wedding->update(["presupuesto" => $data["budget"], "no_invitado" => $data["invited"], "first" => 1]);

                    foreach ($categories as $category) {
                        $wedding->budgets()->updateOrCreate(
                            ["category_id" => $category->id, "nombre" => $category->name],
                            [
                                "nombre"           => $category->name,
                                "category_id"      => $category->id,
                                "porcentaje"       => $category->percentage,
                                "costo_aproximado" => $data["budget"] * ($category->percentage / 100),
                            ]);
                    }

                    return $this->output->set_content_type("application/json")
                        ->set_status_header(200)
                        ->set_output(json_encode(["success" => true, "data" => "ok"]));
                }
            }
        }

        return $this->output->set_content_type("application/json")
            ->set_status_header(404)
            ->set_output(json_encode(['text' => 'Error 404', 'type' => 'danger']));
    }

    public function updateTutorialState()
    {
        $user                    = User::find($this->session->userdata("id_usuario"));
        $user->has_seen_tutorial = 1;
        $user->save();

        return $this->output->set_content_type("application/json")
            ->set_status_header(200)
            ->set_output(json_encode(['success' => true, 'data' => 'ok']));
    }

    public function updateDate(){
        $this->output->set_content_type("application/json");

        if ($_POST) {

            $id  = $this->input->post("id");
            $fecha = $this->input->post("date");
            $pres = $this->input->post("id_presupuesto");

            $date = DB::table("presupuesto")->where("id_presupuesto", $id)->update(
                array(
                    'fecha_asignada' => $fecha,
                ));

            $presupuestos = $this->presupuesto->getPresupuestoConFecha($pres);

            return $this->output
                    ->set_status_header(200)
                    ->set_output(json_encode([
                        'success' => true,
                        'data'    => $presupuestos,
                    ]));

        }
        return $this->output
                    ->set_status_header(500)
                    ->set_output(json_encode([
                        'success' => false,
                        'data'    => null,
                    ]));
    }

    public function eventDates(){
        $this->output->set_content_type("application/json");

        if ($_POST) {

            $pres = $this->input->post("id_presupuesto");
            $presupuestos = $this->presupuesto->getPresupuestoConFecha($pres);

            return $this->output
                    ->set_status_header(200)
                    ->set_output(json_encode([
                        'success' => true,
                        'data'    => $presupuestos,
                    ]));
        }
        return $this->output
                    ->set_status_header(500)
                    ->set_output(json_encode([
                        'success' => false,
                        'data'    => null,
                    ]));
    }

    // public function eventDates()
    // {
    //     $this->output->set_content_type("application/json");

    //     if ($_POST) {
    //         $tasks = $this->input->post("id_presupuesto");
    //         $tasksWithDate = $this->presupuesto->getPresupuestoConFecha($tasks);

    //         return $this->output
    //                     ->set_status_header(200)
    //                     ->set_output(json_encode([
    //                         'success' => true,
    //                         'data'    => $tasksWithDate,
    //                     ]));
    //     }

    //     return $this->output
    //                 ->set_status_header(500)
    //                 ->set_output(json_encode([
    //                     'success' => false,
    //                     'data'    => null,
    //                 ]));
    // }

}
