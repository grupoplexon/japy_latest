<?php

use Models\Gallery;
use Illuminate\Database\Capsule\Manager as DB;
class Escaparate extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/FAQ_model", "faq");
        $this->load->model("Proveedor/RespuestaFAQ_model", "respuesta");
        $this->load->model("Proveedor/Respuesta_model", "plantilla");
        $this->load->model("Proveedor/Archivo_model", "archivo");
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->model("Proveedor/Inspiracion_model", "inspiracion");
        $this->load->model("Proveedor/Promocion_model", "promocion");
        $this->load->model("Proveedor/Producto_model", "producto");
        $this->load->model("Proveedor/TipoProveedor_model", "tipo");
        $this->load->model("Proveedor/Calendario_model", "calendario");
        $this->load->model("Proveedor/Colaborador_model", "colaborador");
        $this->load->model("Proveedor/Evento_model", "evento");
        $this->load->helper("formats");
        $this->load->helper("slug");
        $this->load->helper("storage");
        $this->load->model("Login_model", "usuario");
        $this->load->library("Checker");
    }

    protected function middleware()
    {
        return ["provider_auth"];
    }

    public function index()
    {
        $data["empresa"]    = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        $data["contacto"]   = $this->usuario->getUsuario($this->session->userdata("id_usuario"));
        $data["tipo"]       = $this->tipo->get($data["empresa"]->id_tipo_proveedor);
        $data["categories"] = Category::with("subcategories")->get();
        $data['categoryProvider'] = DB::table('categoria_proveedor')->where('proveedor_id', $data['empresa']->id_proveedor)->first();
        $this->load->view("proveedor/escaparate/index", $data);
    }

    public function updateAcceso()
    {
        if ($_POST) {

            $usuario = [
                "usuario"    => $this->session->userdata("id_usuario"),
                "contrasena" => $this->input->post("password", true, "String", ["min" => 1]),
                "correo"     => $this->session->userdata("correo"),
            ];

            $user = User::find($this->session->userdata("id_usuario"));

            $this->session->set_userdata("tipo_mensaje", "danger");
            $actual = $this->input->post("pass_actual", true, "String", ["min" => 1, "required" => true]);
            $nueva  = $this->input->post("pass_nueva", true, "String", ["required" => true, "min" => 1]);

            if ($this->usuario->validar_usuario($usuario["correo"], $actual)) {
                if ($nueva == $usuario["contrasena"]) {
                    $user->update(["contrasena" => sha1(md5(sha1($nueva)))]);
                    $this->session->set_userdata("tipo_mensaje", "success");
                    $this->session->set_userdata("mensaje", "Contraseña actualizada");
                } else {
                    $this->session->set_userdata("mensaje", "Las contraseñas no coinciden");
                }
            } else {
                $this->session->set_userdata("mensaje", "Contraseña inválida");
            }
        }
        redirect("proveedor/escaparate");
    }

    public function updateDescripcion()
    {
        $this->session->set_userdata("tipo_mensaje", "danger");
        $this->session->set_userdata("mensaje", "Ocurrio un error al actualizar intente más tarde");
        if ($_POST) {
            $e       = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
            $empresa = [
                "descripcion" => $this->input->post("descripcion", false),
                "nombre"      => $this->input->post(
                    "nombre_empresa",
                    true,
                    "String",
                    ["required" => true, "maxSize" => 200]
                ),
            ];
            if ($this->input->isValid()) {
                $empresa["slug"] = create_slug($e->id_proveedor, $empresa["nombre"]);
                $b               = $this->proveedor->update($e->id_proveedor, $empresa);
                if ($b) {
                    $this->session->set_userdata("nombre_proveedor", $empresa["nombre"]);
                    $this->session->set_userdata("slug", $empresa["slug"]);
                    $this->session->set_userdata("tipo_mensaje", "success");
                    $this->session->set_userdata("mensaje", "Descripcion actualizada");
                }
            }
        }
        redirect("proveedor/escaparate");
    }

    public function createVerification()
    {
        if ($_POST) {

            $id = $this->session->userdata("id_usuario");
            $usuario= $this->input->post("nombre_empresa");
            $descripcion = $this->input->post("descripcion");

            $verification = [
                'id_proveedor' =>$this->session->userdata("id_usuario"),
                'nombre_prov'  =>$this->input->post("nombre_empresa"),
                'descripcion'  =>$this->input->post("descripcion"),
                'tipo'         => 0,
                'status'       => 1,
            ];

            $verification = Verification::create($verification);

            $this->session->set_userdata("tipo_mensaje", "success");
            $this->session->set_userdata("mensaje", "La descripción se encuentra en proceso de aprobación");

        } else {
            dd("NO ENTRO");
        }
        redirect("proveedor/escaparate");
    }

    public function updateContacto()
    {
        if ($_POST) {
            $id = $this->session->userdata("id_usuario");
            $e  = $this->proveedor->get(["id_usuario" => $id]);
            $this->session->set_userdata("tipo_mensaje", "danger");

            $usuario = [
                "nombre" => $this->input->post("nombre", true, "String", ["required" => true, "min" => 1]),
                "correo" => $this->input->post("correo", true, "Email", ["required" => true]),
            ];

            $empresa = [
                "contacto_telefono" => $this->input->post(
                    "telefono",
                    true,
                    "String",
                    ["required" => true, "min" => 3]
                ),
                "contacto_pag_web"  => $this->input->post("pagina_web", true, "String"),
                "contacto_celular"  => $this->input->post("celular", true, "String", ["required" => true]),
            ];

            if ($this->input->isValid()) {
                $emailExists = User::where('correo', $usuario['correo'])
                    ->where('id_usuario', '<>', $this->session->id_usuario)
                    ->count();

                if ($emailExists) {
                    $this->session->set_userdata("mensaje", "Ya existe un usuario con ese correo");
                } else {
                    $b = $this->proveedor->updateUsuarioProveedor($id, $usuario, $empresa);
                    if ($b) {
                        $this->session->set_userdata("tipo_mensaje", "success");
                        $this->session->set_userdata("mensaje", "Datos de contacto actualizados");
                    } else {
                        $this->session->set_userdata("mensaje", "Ocurrio un error al actualizar los datos");
                    }
                }
            } else {
                $this->session->set_userdata("mensaje", $this->input->getErrors());
            }
        }
        redirect("proveedor/escaparate");
    }

    public function updateCategorias()
    {
        $user = User::find($this->session->id_usuario);

        if ($_POST) {
            $categories = Category::find($this->input->post("categories"));
            $provider = DB::table('proveedor')->where('id_usuario', $user->id_usuario)->first();
            $update = DB::table('categoria_proveedor')
            ->where('proveedor_id', $provider->id_proveedor)
            ->update(['categoria_id' => $this->input->post("categories")[0]]);
            if (!$update) {
                $this->session->set_userdata("tipo_mensaje", "error");
                $this->session->set_userdata("mensaje", "Ocurrio un error al actualizar los datos");
            } else {
                // if ( ! $categories->count()) {
                //     $user->provider->categories()->detach();
                // } else {
                //     $user->provider->categories()->sync($categories);
                // }
                $this->session->set_userdata("tipo_mensaje", "success");
                $this->session->set_userdata("mensaje", "Tus datos de categoría han sido actualizados");
            }
        } else {
            // $user->provider->categories()->detach();
            $this->session->set_userdata("tipo_mensaje", "success");
            $this->session->set_userdata("mensaje", "Tus datos de categoría han sido actualizados");
        }

        redirect("proveedor/escaparate");
    }

    public function localizacion()
    {
        $data["empresa"] = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        if ($_POST) {
            $this->session->set_userdata("tipo_mensaje", "danger");
            $empresa = [
                // "localizacion_pais"      => $this->input->post("pais", true, "String", ["required" => true]),
                "localizacion_pais"      => $this->input->post("nameCountry", true, "String", ["required" => true]),
                "localizacion_cp"        => $this->input->post("cp", true, "Integer",
                    ["required" => true, "min" => 0]),
                // "localizacion_estado"    => $this->input->post("estado", true, "String", ["required" => true]),
                "localizacion_estado"    => $this->input->post("nameState", true, "String", ["required" => true]),
                "localizacion_direccion" => $this->input->post("direccion", true, "String", ["required" => true]),
                "localizacion_poblacion" => $this->input->post("poblacion", true, "String", ["required" => true]),
                "localizacion_latitud"   => $this->input->post(
                    "latitud",
                    true,
                    "Double",
                    ["required" => true, "min" => -180, "max" => 180]
                ),
                "localizacion_longitud"  => $this->input->post(
                    "longitud",
                    true,
                    "Double",
                    ["required" => true, "min" => -180, "max" => 180]
                ),
            ];
            if ($this->input->isValid()) {
                $b = $this->proveedor->update($data["empresa"]->id_proveedor, $empresa);
                if ($b) {
                    $this->session->set_userdata("tipo_mensaje", "success");
                    $this->session->set_userdata("mensaje", "Datos de localizacion actualizados");
                    redirect("proveedor/escaparate/localizacion");
                } else {
                    $this->session->set_userdata("mensaje", "Ocurrio un error al actualizar los datos");
                }
            } else {
                $this->session->set_userdata("mensaje", $this->input->getErrors());
            }
        }
        $data["tipo"] = $this->tipo->get($data["empresa"]->id_tipo_proveedor);
        $data["countries"] = $this->getCountries();
        $this->load->view("proveedor/escaparate/localizacion", $data);
    }

    public function getCountries(){
        $countries = DB::table('pais')->get();
        return $countries;
    }

    public function faq()
    {
        $data["empresa"] = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        $id_proveedor    = $data["empresa"]->id_proveedor;

        $provider = Provider::with([
            "categories" => function ($query) use ($id_proveedor) {
                return $query->with([
                    "FAQ" => function ($query) use ($id_proveedor) {
                        return $query->with([
                            "providers" => function ($query) use ($id_proveedor) {
                                return $query->where("respuesta_faq.id_proveedor", $id_proveedor);
                            },
                        ]);
                    },
                ]);
            },
        ])->where("id_usuario", $this->session->userdata("id_usuario"))->first();

        $faqs = new \Illuminate\Support\Collection();
        foreach ($provider->categories as $category) {
            foreach ($category->FAQ as $faq) {
                $faqs->push($faq);
            }
        }

        $faqs = $faqs->unique("pregunta")->values();

        $faqs->map(function ($faq) {
            $faq->pregunta = trim($faq->pregunta, " ");
            if ($faq->providers->count()) {
                return $faq->respuesta = $faq->providers->count() ? $faq->providers->first()->pivot->respuesta : [];
            }
        });

        $faqs = $faqs->unique("pregunta")->values();

        $data["faqs"] = $faqs;

        if ($_POST) {
            $this->session->set_userdata("tipo_mensaje", "danger");
            
            $respuestas = [];

            foreach ($data["faqs"] as $key => $faq) {
                $t = $this->input->post($faq->id_faq);
                if ( ! $this->isRespuestaEmpty($t)) {

                    if ($faq->tipo == "RANGE") {

                        $r = $this->respuesta->get(["id_proveedor" => $id_proveedor,"id_faq"=>$faq->id_faq]);
                        $this->respuesta->delete(["id_proveedor" => $id_proveedor]);

                        $temp = $t[0];
                        if ($t[1] != "") {
                            $t[0] = str_replace(",", "", $t[0]);
                            $t[1] = str_replace(",", "", $t[1]);

                            if ($t[0] <= $t[1]) {
                                $t[0] = number_format($t[0], 2, ".", ",");
                                $t[1] = number_format($t[1], 2, ".", ",");
                                $temp = "$t[0]|$t[1]";
                            } else {
                                $this->session->set_userdata("mensaje", "Ocurrio un error al actualizar los datos");
                                redirect("proveedor/escaparate/faq");
                            }
                        }

                        $this->createPrecio($id_proveedor,$faq->id_faq,$temp);



                        $respuestas[] = [
                            "id_proveedor" => $id_proveedor,
                            "id_faq"       => $faq->id_faq,
                            "respuesta"    => $r->respuesta,
                        ];
                    } else {
                        if ($faq->tipo == "CHECKBOX") {

                            $this->respuesta->delete(["id_proveedor" => $id_proveedor]);

                            $temp = "";

                            foreach ($t as $key => $check) {
                                if ($check != "OTRO") {

                                    

                                    $temp .= $check.",";
                                }
                            }

                            $respuestas[] = [
                                "id_proveedor" => $id_proveedor,
                                "id_faq"       => $faq->id_faq,
                                "respuesta"    => substr($temp, 0, -1),
                            ];
                        } else {

                            $this->respuesta->delete(["id_proveedor" => $id_proveedor]);

                            $respuestas[] = [
                                "id_proveedor" => $id_proveedor,
                                "id_faq"       => $faq->id_faq,
                                "respuesta"    => $t,
                            ];
                        }
                    }
                }
            }

            $b = $this->respuesta->insert_batch($respuestas);

            if ($b) {
                $this->session->set_userdata("tipo_mensaje", "success");
                $this->session->set_userdata("mensaje", "FAQ actualizados");
                $this->session->set_userdata("mensaje", "Los precios deben ser aprobados por el administrador");
            } else {
                $this->session->set_userdata("mensaje", "Ocurrio un error al actualizar los datos");
            }

            redirect("proveedor/escaparate/faq");
        }

        $data["tipo"] = $this->tipo->get($data["empresa"]->id_tipo_proveedor);

        $this->load->view("proveedor/escaparate/faq", $data);
    }

    public function createPrecio($id_prov, $id_faq, $precio)
    {

        $provider = Provider::where("id_proveedor", $id_prov)->first();

        if ($_POST) {

            $verification = [
                'id_proveedor' =>$id_prov,
                'nombre_prov'  =>$provider->nombre,
                'id_faq'      =>$id_faq,
                'precios'      =>$precio,
                'tipo'         => 1,
                'status'       => 1,
            ];

            $verification = Verification::create($verification);


        } else {
            dd("NO ENTRO");
        }
    }

    private function isRespuestaEmpty($res)
    {
        if ($res == null) {
            return true;
        }
        if (isset($res)) {
            if (is_string($res)) {
                if ($res == "" || $res == null) {
                    return true;
                }
            } else {
                if (is_array($res)) {
                    foreach ($res as $key => $value) {
                        if ( ! $this->isRespuestaEmpty($value)) {
                            return false;
                        }
                    }

                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public function promociones($method = null, $id = 0)
    {
        $empresa = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        if ($_POST && $method != null) {
            $this->session->set_userdata("tipo_mensaje", "danger");
            $this->session->set_userdata("mensaje", "Ocurrio un error al actualizar");
            switch (strtolower($method)) {
                case "update_desc":
                    $proveedor = [
                        "descuento" => $this->input->post(
                            "desc",
                            true,
                            "Integer",
                            ["required" => true, "min" => 0]
                        ),
                    ];
                    if ($this->input->isValid()) {
                        if ($this->proveedor->update($empresa->id_proveedor, $proveedor)) {
                            $this->session->set_userdata("tipo_mensaje", "success");
                            $this->session->set_userdata("mensaje", "Descuento actualizado correctamente ");
                        }
                    }
                    break;
                case "new":
                    $imagen    = $this->input->post("file");

                    $img       = parseBase64($imagen);
                    $promocion = [
                        "nombre"       => $this->input->post("nombre", true, "String", ["required" => true]),
                        "descripcion"  => $this->input->post("descripcion", false, "String", ["required" => true]),
                        "precio_original" => $this->input->post("precior"),
                        "precio_desc"     => $this->input->post("preciod"),
                        "tipo"         => $this->input->post("tipo", true, "String", ["required" => true]),
                        "fecha_fin"    => $this->input->post(
                            "fecha",
                            true,
                            "Date",
                            ["required" => true, "format" => "Y/m/d"]
                        ),
                        "imagen"        => $img->file,
                        "mime_imagen"   => $img->mime,
                        "id_proveedor" => $empresa->id_proveedor,
                    ];
                    if ($this->input->isValid()) {
                        if ($this->promocion->insert($promocion)) {
                            $this->session->set_userdata("tipo_mensaje", "success");
                            $this->session->set_userdata("mensaje", "Promocion creada correctamente ");
                        }
                    }
                    break;
                case "update":
                    if ($id !== 0) {
                        if ($this->promocion->get(["id_promocion" => $id, "id_proveedor" => $empresa->id_proveedor])) {
                            $imagen    = $this->input->post("file");
                            $img       = parseBase64($imagen);
                            $promocion = [
                                "nombre"      => $this->input->post("nombre", true, "String", ["required" => true]),
                                "descripcion" => $this->input->post(
                                    "descripcion",
                                    false,
                                    "String",
                                    ["required" => true]
                                ),
                                "precio_original" => $this->input->post("precior"),
                                "precio_desc"     => $this->input->post("preciod"),
                                "tipo"        => $this->input->post("tipo", true, "String", ["required" => true]),
                                "fecha_fin"   => $this->input->post(
                                    "fecha",
                                    true,
                                    "Date",
                                    ["required" => true, "format" => "Y/m/d"]
                                ),
                                "imagen"      => $img->file,
                                "mime_imagen" => $img->mime,
                            ];
                            if ($this->input->isValid()) {
                                if ($this->promocion->update($id, $promocion)) {
                                    $this->session->set_userdata("tipo_mensaje", "success");
                                    $this->session->set_userdata("mensaje", "Promocion actualizada correctamente ");
                                }
                            }
                        }
                    }
                    break;
            }
            redirect("proveedor/escaparate/promociones");
        }
        $data["empresa"]     = $empresa;
        $data["promociones"] = $this->promocion->getList(["id_proveedor" => $empresa->id_proveedor]);
        if ($data["promociones"]) {
            foreach ($data["promociones"] as $key => &$value) {
                $value->descargas = $this->promocion->getDescargas($value->id_proveedor, $value->id_promocion);
            }
        }

        $data["tipo"] = $this->tipo->get($data["empresa"]->id_tipo_proveedor);
        $this->load->view("proveedor/escaparate/promociones", $data);
    }

    public function fotos()
    {
        $empresa     = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        $destination = 'uploads/images';
        $quality     = 20;
        $pngQuality  = 9;

        $response = [
            "code"    => 401,
            "success" => false,
            "data"    => [],
        ];

        if ($_FILES) {
            $this->output->set_content_type('application/json');
            $file = $_FILES["files"];

            if ( ! is_array($file["name"]) && ! empty($file["tmp_name"])) {
                $data     = file_get_contents($file["tmp_name"]);
                $provider = Provider::where("id_usuario", $this->session->userdata("id_usuario"))->first();

                if (isset($provider) && $provider->canUploadImages()) {
                    $extension = pathinfo($_FILES["files"]["name"])["extension"];
                    $fileName  = store_file($data, $extension, "images");
                    $finfo     = finfo_open(FILEINFO_MIME_TYPE);
                    $mime      = finfo_file($finfo, $_FILES["files"]["tmp_name"]);

                    $new_name_image = $file = $destination."/".$fileName;

                    $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);

                    $galeria = [
                        "id_proveedor" => $empresa->id_proveedor,
                        "nombre"       => $fileName,
                        "tipo"         => Galeria_model::$TIPO_IMAGEN,
                        "mime"         => $mime,
                    ];

                    if ($this->galeria->insert($galeria)) {
                        $id = $this->galeria->last_id();

                        return $this->output
                            ->set_status_header(202)
                            ->set_output(
                                json_encode(
                                    [
                                        'success' => true,
                                        'data'    => [
                                            "url"       => base_url()."uploads/images/$fileName",
                                            "id"        => $id,
                                            "file_name" => $fileName,
                                        ],
                                    ]
                                )
                            );
                    }
                }
                $response["code"] = 403;
            }

            return $this->output->set_status_header($response["code"])->set_output(json_encode($response));
        } elseif ($_POST) {
            $this->output->set_content_type("application/json");
            $method = strtoupper($this->input->post("method", true, "String", ["required" => true]));
            $id     = $this->input->post("id", true, "Integer", ["required" => true, "min" => 1]);

            switch ($method) {
                case "DELETE":
                    $image = Gallery::find($id);
                    if ($image) {
                        $imagesPaths = $image->miniatures->map(function ($image) {
                            return "uploads/images/$image->nombre";
                        });

                        $imagesPaths [] = "uploads/images/$image->nombre";

                        foreach ($imagesPaths as $imagePath) {
                            unlink($imagePath);
                        }

                        $image->miniatures()->delete();
                        $image->delete();

                        $response["code"]    = 202;
                        $response["success"] = true;
                        $response["data"]    = "ok";
                    }
                    break;
                case "UPDATE":
                    $galeria = $this->input->post("nombre");

                    if ($this->galeria->update($id, $galeria)) {
                        $response["code"]    = 202;
                        $response["success"] = true;
                        $response["data"]    = [
                            "url" => $this->config->base_url()."home/imagen_proveedor/".$id,
                            "id"  => $id,
                        ];
                    }
                    break;
                case "UPDATE_LOGO":
                    $data  = $this->input->post("data");
                    $image = Gallery::with([
                        "miniatures" => function ($query) {
                            return $query->where("logo", 1);
                        },
                    ])->find($id);

                    if ($image) {
                        //Delete other miniatures that don't belong to this image
                        $images = Gallery::where("logo", 1)
                            ->where("id_proveedor", $empresa->id_proveedor)
                            ->where("id_galeria", "<>", $id)
                            ->where("parent", null)
                            ->get();

                        Gallery::whereIn("parent", $images->pluck("id_galeria"))
                            ->where("logo", 1)
                            ->delete();

                        $data     = parseBase64($data);
                        $fileName = store_file($data->file, $data->extension, "images");
                        $mime     = $data->mime;

                        $new_name_image = $file = $destination."/".$fileName;

                        $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);

                        if ($image->miniatures->count()) {
                            $image->miniatures->first()->update([
                                "nombre" => $fileName,
                                "mime"   => $mime,
                            ]);
                        } else {
                            $image = Gallery::create([
                                "id_proveedor" => $empresa->id_proveedor,
                                "nombre"       => $fileName,
                                "tipo"         => Galeria_model::$TIPO_IMAGEN,
                                "mime"         => $mime,
                                "logo"         => 1,
                                "parent"       => $id,
                            ]);
                        }

                        if ($this->galeria->setNewLogo($id, $empresa->id_proveedor) && $image) {
                            $response["code"]    = 202;
                            $response["success"] = true;
                            $response["data"]    = "ok";
                        }
                    }
                    break;
                case "UPDATE_PRINCIPAL":
                    $data = $this->input->post("data");

                    $image = Gallery::with([
                        "miniatures" => function ($query) {
                            return $query->where("principal", 1);
                        },
                    ])->find($id);

                    if ($image) {

                        //Delete other miniatures that don't belong to this image
                        $images = Gallery::where("principal", 1)
                            ->where("id_proveedor", $empresa->id_proveedor)
                            ->where("id_galeria", "<>", $id)
                            ->where("parent", null)
                            ->get();

                        Gallery::whereIn("parent", $images->pluck("id_galeria"))
                            ->where("principal", 1)
                            ->delete();

                        $data     = parseBase64($data);
                        $fileName = store_file($data->file, $data->extension, "images");
                        $mime     = $data->mime;

                        $new_name_image = $file = $destination."/".$fileName;

                        $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);

                        if ($image->miniatures->count()) {
                            $image->miniatures->first()->update([
                                "nombre" => $fileName,
                                "mime"   => $mime,
                            ]);
                        } else {
                            $image = Gallery::create([
                                "id_proveedor" => $empresa->id_proveedor,
                                "nombre"       => $fileName,
                                "tipo"         => Galeria_model::$TIPO_IMAGEN,
                                "mime"         => $mime,
                                "principal"    => 1,
                                "parent"       => $id,
                            ]);
                        }

                        if ($this->galeria->setNewPrincipal($id, $empresa->id_proveedor) && $image) {
                            $response["code"]    = 202;
                            $response["success"] = true;
                            $response["data"]    = "ok";
                        }
                    }
                    break;
            }

            return $this->output->set_status_header($response["code"])->set_output(json_encode($response));
        } else {
            $data["tipo"]      = $this->tipo->get($empresa->id_tipo_proveedor);
            $data["galeria"]   = $this->galeria->getImagenesProveedor($empresa->id_proveedor);
            $data["has_lp"]    = $this->galeria->hasLP($empresa->id_proveedor);
            $data["proveedor"] = $empresa;
            $data["video"]     = false;
            $data["inspiracion"]   = $this->inspiracion->getImagenesProveedor($empresa->id_proveedor);
            $this->load->view("proveedor/escaparate/fotos", $data);
        }
    }

    public function videos()
    {
        $empresa = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);

        $response = [
            "code"    => 401,
            "success" => false,
            "data"    => [],
        ];

        if ($empresa->tipo_cuenta != 3) {
            redirect("proveedor/escaparate");
        }

        $message = "Error uploading file";

        if ($_FILES) {
            $this->output->set_content_type('application/json');
            $file = $_FILES["files"];
            if ( ! is_array($file["name"]) && ! empty($file["tmp_name"])) {
                $data     = file_get_contents($file["tmp_name"]);
                $provider = Provider::where("id_usuario", $this->session->userdata("id_usuario"))->first();
                if ($provider->canUploadVideo()) {
                    $extension = pathinfo($_FILES["files"]["name"])["extension"];
                    $fileName  = store_file($data, $extension, "videos");
                    $finfo     = finfo_open(FILEINFO_MIME_TYPE);
                    $mime      = finfo_file($finfo, $_FILES["files"]["tmp_name"]);

                    $galeria = [
                        "id_proveedor" => $empresa->id_proveedor,
                        "nombre"       => $fileName,
                        "tipo"         => Galeria_model::$TIPO_VIDEO,
                        "mime"         => $file["type"],
                    ];
                    if ($this->galeria->insert($galeria)) {
                        $id                      = $this->galeria->last_id();
                        $config['upload_path']   = './uploads/videos/';
                        $config['allowed_types'] = 'mp4|mov';
                        $config['max_size']      = '510000';
                        $config['file_name']     = md5(time().uniqid()).'.'.str_replace("video/", "", $file["type"]);

                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload("files")) {
                            $this->galeria->update($id, ["nombre" => $config["file_name"]]);

                            return $this->output
                                ->set_status_header(202)
                                ->set_output(
                                    json_encode(
                                        [
                                            'success' => true,
                                            'data'    => [
                                                "url" => $this->config->base_url()."/uploads/videos/".$config["file_name"],
                                                "id"  => $id,
                                            ],
                                        ]
                                    )
                                );
                        } else {
                            $this->galeria->delete($id);
                        }
                    }
                } else {
                    $response["code"] = 403;
                }
            }

            return $this->output->set_status_header($response["code"])->set_output(json_encode($response));
        } else {
            if ($_POST) {
                $this->output->set_content_type('application/json');
                $method = strtoupper($this->input->post("method", true, "String", ["required" => true]));
                $id     = $this->input->post("id", true, "Integer", ["required" => true, "min" => 1]);
                if ($method == "DELETE") {
                    if ($this->input->isValid()) {
                        unlink('./uploads/videos/'.$this->galeria->get($id)->nombre);
                        $ok = $this->galeria->delete($id);
                    }
                } else {
                    if ($method == "UPDATE") {
                        $galeria = [
                            "nombre" => $this->input->post("nombre", true, "String", ["required" => true]),
                        ];
                        if ($this->input->isValid()) {
                            $ok = $this->galeria->update($id, $galeria);
                        }
                    }
                }
                if ($ok) {
                    return $this->output
                        ->set_status_header(202)
                        ->set_output(
                            json_encode(
                                [
                                    'success' => true,
                                    'data'    => "ok",
                                ]
                            )
                        );
                }

                return $this->output->set_status_header($response["code"])->set_output(json_encode($response));
            } else {
                $data["tipo"]    = $this->tipo->get($empresa->id_tipo_proveedor);
                $data["galeria"] = $this->galeria->getVideosProveedor($empresa->id_proveedor);
                $data["video"]   = true;
                $this->load->view("proveedor/escaparate/videos", $data);
            }
        }
    }

    public function reportajes()
    {

    }

    public function publicar_recomendacion()
    {
        $this->output->set_content_type('application/json');
        if ($_POST) {
            $this->load->model("Boda_model", "boda");
            $empresa = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
            $rec     = [
                "visible" => $this->input->post(
                    "estado",
                    true,
                    "Integer",
                    ["required" => true, "min" => 0, "max" => 1]
                ),
            ];
            $boda    = $this->input->post("boda", true, "Integer", ["required" => true]);
            if ($this->input->isValid()) {
                $b = $this->boda->updateProveedorResena($boda, $empresa->id_proveedor, $rec);
                if ($b) {
                    return $this->output->set_status_header(202)
                        ->set_output(
                            json_encode(
                                [
                                    'success' => true,
                                    'data'    => "ok",
                                ]
                            )
                        );
                }
            }
        }

        return $this->output->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        'success' => false,
                        'data'    => "error",
                    ]
                )
            );
    }

    public function productos($action = null, $id = null)
    {
        $empresa      = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        $data["tipo"] = $this->tipo->get($empresa->id_tipo_proveedor);
        if ($_POST) {
            $this->session->set_userdata("tipo_mensaje", "danger");
            $this->session->set_userdata("mensaje", "Ocurrio un error intente mas tarde");
            $action = strtoupper($action);
            if ($action == "NUEVO" || $action == "EDITAR") {
                $producto = [
                    "nombre"       => $this->input->post("nombre", true, "String", ["required" => true]),
                    "detalle"      => $this->input->post("detalle", true, "String", ["required" => true]),
                    "precio"       => $this->input->post("precio", true, "String"),
                    "id_proveedor" => $empresa->id_proveedor,
                ];
                $file     = true;
                if ($data["tipo"]->producto_imagen) {
                    $file = parseBase64($this->input->post("file"));
                    if ($file->mime != null) {
                        $producto["mime"]   = $file->mime;
                        $producto["imagen"] = $file->file;
                    } else {
                        $file = false;
                    }
                }
                if ($file && $this->input->isValid()) {
                    if ($action === "NUEVO") {
                        if ($this->producto->insert($producto)) {
                            $this->session->set_userdata("tipo_mensaje", "success");
                            $this->session->set_userdata("mensaje", "Producto agregado correctamente");
                        }
                    } else {
                        if ($action === "EDITAR") {
                            $id_producto = $this->input->post("producto", true, "Integer", ["required" => true]);
                            if ($id == $id_producto) {
                                if ($this->producto->update($id, $producto)) {
                                    $this->session->set_userdata("tipo_mensaje", "success");
                                    $this->session->set_userdata("mensaje", "Producto actualizado correctamente");
                                }
                            }
                        }
                    }
                } else {
                    $this->session->set_userdata("mensaje", $this->input->getErrors());
                }
            } else {
                if ($action === "ELIMINAR") {
                    $id_producto = $this->input->post("producto", true, "Integer", ["required" => true]);
                    if ($id == $id_producto && $this->input->isValid()) {
                        if ($this->producto->delete($id)) {
                            $this->session->set_userdata("tipo_mensaje", "success");
                            $this->session->set_userdata("mensaje", "Producto eliminado correctamente");
                        }
                    }
                }
            }
            redirect("proveedor/escaparate/productos");
        }
        $data["productos"] = $this->producto->getList(["id_proveedor" => $empresa->id_proveedor]);
        $this->load->view("proveedor/escaparate/productos", $data);
    }

    public function auto($tipo, $producto)
    {
        $term = $this->input->get("term", true, "String", ["required" => true]);
        $this->output->set_content_type('application/json');
        $res = [];
        //        foreach ($val as $key => $value) {
        //            $res[] = array(
        //                "id" => $value->valor,
        //                "label" => $value->valor,
        //                "value" => $value->valor,
        //            );
        //        }
        return $this->output->set_status_header(202)
            ->set_output(json_encode($res));
    }

    public function calendario()
    {
        $empresa = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        if ($_POST) {
            $this->output->set_content_type('application/json');
            $cal = [
                "id_proveedor" => $empresa->id_proveedor,
                "dia"          => DateTime::createFromFormat(
                    "U",
                    $this->input->post("dia", true, "Date", ["format" => "U"])
                )->format("Y-m-d"),
                "estado"       => $this->input->post("ocupado", true) == "true" ? 1 : 0,
            ];
            if ($this->input->isValid()) {
                $b = $this->calendario->get(["id_proveedor" => $empresa->id_proveedor, "dia" => $cal["dia"]]);
                if ( ! $b) {
                    $ok = $this->calendario->insert($cal);
                } else {
                    $ok = $this->calendario->update($b->id_calendario, $cal);
                }
            }
            if ($ok) {
                return $this->output->set_status_header(202)
                    ->set_output(
                        json_encode(
                            [
                                'success' => true,
                                'data'    => "ok",
                            ]
                        )
                    );
            }

            return $this->output->set_status_header(404);
        }
        $year = $this->input->get("year", true, "Integer");
        if ( ! $year) {
            $year = date("Y");
        }
        $data["year"]       = $year;
        $data["tipo"]       = $this->tipo->get($empresa->id_tipo_proveedor);
        $data["calendario"] = $this->calendario->getList(["id_proveedor" => $empresa->id_proveedor]);
        $this->load->view("proveedor/escaparate/calendario", $data);
    }

    public function eventos($action = null, $id = null)
    {
        $empresa = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        if ($_POST) {

            if ( ! empty($this->input->post('delete'))) {
                $this->evento->delete($id);
            } else {
                if (strtoupper($action) == "NUEVO") {
                    $this->session->set_userdata("tipo_mensaje", "danger");
                    $this->session->set_userdata("mensaje", "Ocurrio un error verifique que los datos sean correctos");
                    $imagen = $this->input->post("file");
                    $img    = parseBase64($imagen);
                    $evento = [
                        "id_proveedor"  => $empresa->id_proveedor,
                        "nombre"        => $this->input->post("nombre", true, "String",
                            ["min" => 1, "required" => true]),
                        "descripcion"   => $this->input->post("descripcion", false, "String", ["min" => 1]),
                        "fecha_inicio"  => $this->input->post(
                                "fecha_inicio_submit",
                                true,
                                "String",
                                ["required" => true]
                            )." ".$this->input->post("hora_inicio", true, "String", ["required" => true]),
                        "fecha_termino" => $this->input->post(
                                "fecha_termino_submit",
                                true,
                                "String",
                                ["required" => true]
                            )." ".$this->input->post("hora_termino", true, "String", ["required" => true]),
                        "latitud"       => $this->input->post(
                            "latitud",
                            true,
                            "String",
                            ["required" => true, "min" => -360, "max" => 360]
                        ),
                        "longitud"      => $this->input->post(
                            "longitud",
                            true,
                            "String",
                            ["required" => true, "min" => -360, "max" => 360]
                        ),
                        "estado"        => $this->input->post("estado", true, "String", ["required" => true]),
                        "poblacion"     => $this->input->post("poblacion", true, "String", ["required" => true]),
                        "direccion"     => $this->input->post("direccion", true, "String", ["required" => true]),
                        "tipo_evento"   => $this->input->post("tipo", true, "Integer", ["required" => true]),
                        "imagen"        => $img->file,
                        "mime_imagen"   => $img->mime,
                    ];
                    if ($this->input->isValid()) {
                        if ($this->evento->insert($evento)) {
                            $this->session->set_userdata("tipo_mensaje", "success");
                            $this->session->set_userdata("mensaje", "Evento agregado correctamente");
                            redirect("proveedor/escaparate/eventos");
                        }
                    } else {
                        $this->session->set_userdata("mensaje", $this->input->getErrors());
                    }
                } else {
                    if (strtoupper($action) == "EDIT") {

                        $event = $this->evento->get($id);
                        if ($event) {
                            $this->session->set_userdata("tipo_mensaje", "danger");
                            $this->session->set_userdata(
                                "mensaje",
                                "Ocurrio un error verifique que los datos sean correctos"
                            );
                            $imagen = $this->input->post("file");
                            $img    = parseBase64($imagen);
                            $evento = [
                                "id_proveedor"  => $empresa->id_proveedor,
                                "nombre"        => $this->input->post(
                                    "nombre",
                                    true,
                                    "String",
                                    ["min" => 1, "required" => true]
                                ),
                                "descripcion"   => $this->input->post("descripcion", false, "String", ["min" => 1]),
                                "fecha_inicio"  => $this->input->post(
                                        "fecha_inicio_submit",
                                        true,
                                        "String",
                                        ["required" => true]
                                    )." ".$this->input->post("hora_inicio", true, "String",
                                        ["required" => true]),
                                "fecha_termino" => $this->input->post(
                                        "fecha_termino_submit",
                                        true,
                                        "String",
                                        ["required" => true]
                                    )." ".$this->input->post("hora_termino", true, "String",
                                        ["required" => true]),
                                "latitud"       => $this->input->post(
                                    "latitud",
                                    true,
                                    "String",
                                    ["required" => true, "min" => -360, "max" => 360]
                                ),
                                "longitud"      => $this->input->post(
                                    "longitud",
                                    true,
                                    "String",
                                    ["required" => true, "min" => -360, "max" => 360]
                                ),
                                "estado"        => $this->input->post("estado", true, "String",
                                    ["required" => true]),
                                "poblacion"     => $this->input->post(
                                    "poblacion",
                                    true,
                                    "String",
                                    ["required" => true]
                                ),
                                "direccion"     => $this->input->post(
                                    "direccion",
                                    true,
                                    "String",
                                    ["required" => true]
                                ),
                                "tipo_evento"   => $this->input->post("tipo", true, "Integer",
                                    ["required" => true]),
                                "imagen"        => $img->file,
                                "mime_imagen"   => $img->mime,
                            ];
                            if ($this->input->isValid()) {
                                if ($this->evento->update($id, $evento)) {
                                    $this->session->set_userdata("tipo_mensaje", "success");
                                    $this->session->set_userdata("mensaje", "Evento agregado correctamente");
                                    redirect("proveedor/escaparate/eventos");
                                }
                            } else {
                                $this->session->set_userdata("mensaje", $this->input->getErrors());
                            }
                        }
                    } else {
                        if (strtoupper($action) == "BORRAR") {
                            $this->output->set_content_type('application/json');
                            $e = $this->input->post("evento", true, "Integer", ["required" => true, "min" => 1]);
                            if ($this->input->isValid()) {
                                if ($this->evento->delete(
                                    ["id_evento" => $e, "id_proveedor" => $empresa->id_proveedor]
                                )
                                ) {
                                    return $this->output
                                        ->set_status_header(202)
                                        ->set_output(json_encode(["ok" => true]));
                                }
                            }

                            return $this->output->set_status_header(400);
                        }
                    }
                }
            }
        }
        $data["eventos"] = $this->evento->getList(["id_proveedor" => $empresa->id_proveedor]);
        $data["empresa"] = $empresa;
        $data["tipo"]    = $this->tipo->get($empresa->id_tipo_proveedor);
        $this->load->view("proveedor/escaparate/eventos", $data);
    }

    public function colaboradores()
    {
        $empresa = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);

        if ($_POST) {
            $action = $this->input->post("action", true, "String");

            if ($action == "delete") {
                $this->output->set_content_type('application/json');
                $co = [
                    "id_proveedor"   => $empresa->id_proveedor,
                    "id_colaborador" => $this->input->post("colaborador", true, "Integer", ["min" => 1]),
                ];
                if ($this->input->isValid()) {
                    if ($this->colaborador->delete($co)) {
                        return $this->output->set_status_header(202)
                            ->set_output(json_encode($co));
                    }
                }

                return $this->output->set_status_header(400);
            } else {
                $this->session->set_userdata("tipo_mensaje", "danger");
                $this->session->set_userdata("mensaje", "Ocurrio un error verifique que los datos sean correctos");

                $datos          = $this->input->post("datos", true, "String", ["required" => true]);
                $collaboratorId = $this->input->post("id", true, "Integer", ["required" => true, "min" => 1]);

                if ($this->input->isValid()) {
                    $colaborador = $this->proveedor->get($collaboratorId);
                    if ($colaborador) {
                        $collaborator = [
                            "id_proveedor"   => $empresa->id_proveedor,
                            "id_colaborador" => $colaborador->id_proveedor,
                        ];

                        //If the provider doesn't have that collaborator
                        if (Provider::whereHas(
                                'collaborators',
                                function ($query) use ($collaborator) {
                                    return $query->where('id_colaborador', $collaborator['id_colaborador']);
                                }
                            )->find($empresa->id_proveedor) == null
                        ) {
                            if ($this->colaborador->insert($collaborator)) {
                                $this->session->set_userdata("tipo_mensaje", "success");
                                $this->session->set_userdata("mensaje", "Colaborador agregado correctamente");
                                redirect("proveedor/escaparate/colaboradores");
                            }
                        }

                        $this->session->set_userdata("mensaje", "Usted ya esta colaborando con ese proveedor");
                        redirect("proveedor/escaparate/colaboradores");
                    }
                }
            }
        } else {
            if ($_GET) {
                $term = $this->input->get("term", true, "String");

                if ($term) {
                    $search = $this->proveedor->search($term, $empresa->id_proveedor);

                    return $this->output->set_content_type('application/json')
                        ->set_status_header(202)
                        ->set_output(json_encode($search));
                }
            }
        }

        $data["colaboradores"] = $this->colaborador->getColaboradores($empresa->id_proveedor);
        $data['collaborators'] = Provider::with(
            [
                'collaborators' => function ($query) {
                    return $query->with(['imageLogo']);
                },
            ]
        )->find($empresa->id_proveedor)->collaborators;
        foreach ($data["colaboradores"] as $key => &$c) {
            $c->logo = $this->proveedor->getLogo($c->id_proveedor);
        }
        $data["tipo"] = $this->tipo->get($empresa->id_tipo_proveedor);
        $this->load->view("proveedor/escaparate/coolaboradores", $data);
    }

    public function deleteImagesTemplate()
    {
        $response = [
            "code"    => 404,
            "message" => "error",
            "data"    => [],
        ];

        $template = $this->plantilla->get($this->input->post("templateId"));

        if ($template) {
            $file = $this->archivo->get($this->input->post("fileId"));

            if ($file) {
                $this->plantilla->deleteArchivo($file->id_archivo, $template->id_respuesta, $template->id_proveedor);
            }

            $response["code"]    = 200;
            $response["message"] = "ok";
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function uploadFilesTemplate()
    {
        $response = [
            "code"    => 404,
            "message" => "error",
            "data"    => [],
        ];
        $template = $this->plantilla->get($this->input->post("templateId"));

        if ($template) {
            //Setting the file array
            for ($i = 0; $i < count($_FILES['files']['tmp_name']); $i++) {
                $imagedata = file_get_contents($_FILES['files']['tmp_name'][$i]);
                $base64    = base64_encode($imagedata);
                $src       = 'data:'.mime_content_type($_FILES['files']['tmp_name'][$i]).';base64,'.$base64;
                if ($this->archivo->insert(["data" => $src, "nombre" => $_FILES['files']['name'][$i], "mime" => $_FILES['files']['type'][$i]])) {
                    $fileIds[] = $this->archivo->last_id();
                    $this->plantilla->insertArchivo($this->archivo->last_id(), $template->id_respuesta);
                }
            }

            $response["data"]    = [
                "fileIds"    => $fileIds,
                "templateId" => $this->input->post("templateId"),
            ];
            $response["code"]    = 200;
            $response["message"] = "ok";
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    function updateDescriptionImageAndVideo()
    {
        $response = [
            "code"    => 404,
            "message" => "error",
            "data"    => [],
        ];

        $id          = $this->input->post("id");
        $descripcion = $this->input->post("data");

        if (isset($id) && isset($descripcion)) {
            Gallery::where("id_galeria", $id)->update(["descripcion" => $descripcion]);
            $response = [
                "code"    => 200,
                "message" => "Exito",
                "data"    => [],
            ];
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    function getDescriptionImageAndVideo()
    {
        $response = [
            "code"    => 404,
            "message" => "error",
            "data"    => [],
        ];

        $id = $this->input->post("id");

        if (isset($id)) {
            $response = [
                "code"    => 200,
                "message" => "Exito",
                "data"    => Gallery::where("id_galeria", $id)->select("descripcion")->first(),
            ];
        }

        return $this->output->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    function fotosCargadas() {
        if ($_POST) {

            $data["id_user"] = $this->session->userdata("id_usuario");
            $data["name"] = $this->session->userdata("nombre");
            $data["status"] = $this->input->post("status");
            $data["type"] = $this->input->post("type");
            $data["msj"] = $this->input->post("msj");

            $user = User::find($data["id_user"]);

            if($user["activo"]!=2) {
                $empresa     = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
                $cuenta["galeria"]   = $this->galeria->getImagenesProveedor($empresa->id_proveedor);
                if($cuenta >= 8) {
                    $validauser = Alert::where('id_user', $data["id_user"])->first();
                    if(empty($validauser)) {
                        $guardar = Alert::create([
                            'id_user'    => $data["id_user"],
                            'id_temp'   => 1,
                            'status'    => $data["status"],
                            'name'     => $data["name"],
                            'msj'     => $data["msj"],
                            'type'     => $data["type"],
                        ]);
                    }
                }
            }
        }
    }

    function deleteInsp() {
        if($_POST) {
            $id = $this->input->post("id");
            $delete = Inspiration::find($id)->delete();
            return $this->output->set_status_header(200);
        }
    }
}
