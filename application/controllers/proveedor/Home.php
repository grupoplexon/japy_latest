<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->model("Proveedor/Promocion_model", "promocion");
        $this->load->model("Proveedor/Colaborador_model", "colaborador");
        $this->load->model("Proveedor/FAQ_model", "faq");
        $this->load->model("Proveedor/RespuestaFAQ_model", "respuesta");
        $this->load->model("Proveedor/TipoProveedor_model", "tipo");
        $this->load->model("Proveedor/Solicitud_model", "solicitud");
        $this->load->model("Correo/Correo_model", "correo");
        $this->load->library("Checker");
        $this->load->helper("formats");
    }

    protected function middleware()
    {
        return ["provider_auth"];
    }

    public function index()
    {
        $data["proveedor"] = Provider::where("id_usuario", $this->session->userdata("id_usuario"))->first();
        if ($data["proveedor"]) {
            $id = $data["proveedor"]->id_proveedor;

            $foto = $this->galeria->getList([
                "id_proveedor" => $data["proveedor"]->id_proveedor,
                "tipo"         => Galeria_model::$TIPO_IMAGEN,
            ], 8);

            $data["comp_perfil"] = [
                "logo"          => $this->proveedor->getLogo($id) ? true : false,
                "principal"     => $this->proveedor->getPrincipal($id) ? true : false,
                "fotos"         => count($foto) >= 8,
                "promociones"   => $this->promocion->hasPromocion($id),
                "colaboradores" => $this->colaborador->hasColaboradores($id),
                "preguntas"     => $this->respuesta->hasPreguntas($id),
                "localizacion"  => $data["proveedor"]->localizacion_latitud ? true : false,
            ];

            $data["porcentaje"]      = $this->porcentajePerfil($data["comp_perfil"], $foto);
            $data["tipo"]            = $this->tipo->get($data["proveedor"]->id_tipo_proveedor);
            $data["tiempo"]          = $this->proveedor->timepoRespuestaSolicitudes($id);
            $data["recomendaciones"] = $this->proveedor->lastRecomendaciones($id);

            $this->load->view("proveedor/index", $data);
        }
    }

    private function porcentajePerfil($comp_perfil, $foto)
    {
        $porcentaje_perfil = 0;
        $redirectTo        = ['fotos', 'fotos', 'fotos', 'promociones', 'colaboradores', 'faq', 'localizacion'];
        if ($comp_perfil["logo"]) {
            unset($redirectTo[0]);
            $porcentaje_perfil += 25;
        }
        if ($comp_perfil["principal"]) {
            unset($redirectTo[1]);
            $porcentaje_perfil +=25;
        }
        if (count($foto) >= 8) {
            unset($redirectTo[2]);
            $porcentaje_perfil += 20;
        }
        if ($comp_perfil["promociones"]) {
            unset($redirectTo[3]);
            $porcentaje_perfil += 15;
        }
        // if ($comp_perfil["colaboradores"]) {
        //     unset($redirectTo[4]);
        //     $porcentaje_perfil += 16;
        // }{}
        // if ($comp_perfil["preguntas"]) {
        //     unset($redirectTo[5]);
        //     $porcentaje_perfil += 16;
        // }
        if ($comp_perfil["localizacion"]) {
            unset($redirectTo[6]);
            $porcentaje_perfil += 15;
        }

        if (empty($redirectTo)) {
            $redirectTo = '';
        } else {
            $redirectTo = array_values($redirectTo);
        }

        return ['porcentaje' => $porcentaje_perfil, 'redireccionar' => $redirectTo];
    }

    public function visitas($id)
    {
        $visitas = $this->estvisitas($this->proveedor->visitas12meses($id));

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(202)
            ->set_output(
                json_encode(
                    [
                        'success' => true,
                        'data'    => $visitas,
                    ]
                )
            );
    }

    public function telefono($id)
    {
        $visitas = $this->estvisitas($this->proveedor->telefono12meses($id));

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(202)
            ->set_output(
                json_encode(
                    [
                        'success' => true,
                        'data'    => $visitas,
                    ]
                )
            );
    }

    public function solicitudes($id)
    {
        $visitas = $this->estvisitas($this->proveedor->solicitudes12meses($id));

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(202)
            ->set_output(
                json_encode(
                    [
                        'success' => true,
                        'data'    => [
                            "solicitudes"      => $visitas,
                            "total_pendientes" => $this->proveedor->totalSolicitudesPendientes12meses($id),
                            "tiempo_respuesta" => "12 Hrs",
                        ],
                    ]
                )
            );
    }

    private function estvisitas($visitas, $campo = "visitas")
    {
        $est = [];
        $now = new DateTime("now");
        $now->sub(DateInterval::createFromDateString('12 month'));
        for ($mes = 11; $mes >= 0; $mes--) {
            $now->add(DateInterval::createFromDateString('1 month'));
            $c = 0;
            foreach ($visitas as $key => $v) {
                if ($v->mes == $now->format("m-Y")) {
                    $c = (int)$v->visitas;
                    break;
                }
            }
            $est[] = [
                "fecha"  => $now->format("m-Y"),
                "$campo" => $c,
            ];
        }

        return $est;
    }

    public function indicadores($providerId)
    {
        $response = [
            'code'    => 200,
            'data'    => [],
            'message' => 'Ok',
        ];

        $startOfYear = Carbon::now()->startOfYear();
        $endOfYear   = Carbon::now()->endOfYear();

        while ($startOfYear < $endOfYear) {
            $response['data']['year']['visit'][$startOfYear->month]           = 0;
            $response['data']['year']['contact_request'][$startOfYear->month] = 0;
            $response['data']['year']['phone'][$startOfYear->month]           = 0;
            $startOfYear->addMonth();
        }

        $indicatorsWeekly = DB::table("indicators")
            ->where("provider_id", $providerId)
            ->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()])
            ->get();

        $indicatorsYearly = DB::table("indicators")
            ->where("provider_id", $providerId)
            ->where(function ($query) {
                return $query->where("type", "visit")
                    ->orWhere("type", "contact_request")
                    ->orWhere("type", "phone");
            })
            ->whereBetween("created_at", [Carbon::now()->subYear(), Carbon::now()])
            ->get();

        $contactRequestCount = 0;

        foreach ($indicatorsWeekly as $indicator) {
            isset($response['data']['week'][$indicator->type])
                ?
                $response['data']['week'][$indicator->type]++
                :
                $response['data']['week'][$indicator->type] = 1;

            if ($indicator->type == 'contact_request') {
                if ( ! is_null($indicator->handled_at)) {
                    $handledAt = Carbon::createFromFormat('Y-m-d H:i:s', $indicator->handled_at);
                    $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $indicator->created_at);
                    $timeTaken = $handledAt->diffInMinutes($createdAt);
                    $contactRequestCount++;

                    if (isset($response['data']['week']['contact_average_time_request'])) {
                        $averageTime = $response['data']['week']['contact_average_time_request'];
                        $averageTime += $timeTaken;
                    } else {
                        $averageTime = $timeTaken;
                    }

                    $response['data']['week']['contact_average_time_request'] = $averageTime;

                } else {
                    $test[] = $indicator->id;
                    isset($response['data']['week']['contact_pending_request'])
                        ?
                        $response['data']['week']['contact_pending_request']++
                        :
                        $response['data']['week']['contact_pending_request'] = 1;
                }
            }
        }

        if (isset($response['data']['week']['contact_average_time_request'])) {
            $response['data']['week']['contact_average_time_request'] = (int)($response['data']['week']['contact_average_time_request'] / $contactRequestCount);
        }

        $startOfYear = Carbon::now()->startOfYear();

        foreach ($indicatorsYearly as $indicator) {
            $indicatorMonth = Carbon::createFromFormat("Y-m-d H:i:s", $indicator->created_at)->month;
            $response["data"]["year"][$indicator->type][$indicatorMonth]++;
        }
        if(isset($response['data']['week']['login']))
            unset($response['data']['week']['login']);
        return $this->output
            ->set_content_type("application/json")
            ->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

}
