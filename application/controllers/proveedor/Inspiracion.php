<?php

use Models\Gallery;
use Illuminate\Database\Capsule\Manager as DB;
class Inspiracion extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/FAQ_model", "faq");
        $this->load->model("Proveedor/RespuestaFAQ_model", "respuesta");
        $this->load->model("Proveedor/Respuesta_model", "plantilla");
        $this->load->model("Proveedor/Archivo_model", "archivo");
        $this->load->model("Proveedor/Inspiracion_model", "inspiracion");
        $this->load->model("Proveedor/Promocion_model", "promocion");
        $this->load->model("Proveedor/Producto_model", "producto");
        $this->load->model("Proveedor/TipoProveedor_model", "tipo");
        $this->load->model("Proveedor/Calendario_model", "calendario");
        $this->load->model("Proveedor/Colaborador_model", "colaborador");
        $this->load->model("Proveedor/Evento_model", "evento");
        $this->load->helper("formats");
        $this->load->helper("slug");
        $this->load->helper("storage");
        $this->load->model("Login_model", "usuario");
        $this->load->library("Checker");
    }

    protected function middleware()
    {
        return ["provider_auth"];
    }

    public function index() {
        $empresa     = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        $data["inspiracion"]   = $this->inspiracion->getImagenesProveedor($empresa->id_proveedor);
        // $data = Inspiration::with('tags')->find(3); //PARA TRAER INSPIRACION CON TAGS
        // dd($data);
        $this->load->view("proveedor/inspiration/inspiration", $data);
    }

    public function fotos()
    {
        // dd($this->session->userdata("id_usuario"));
        $empresa     = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        // dd("PASE");
        $destination = 'uploads/images/inspiracion';
        $quality     = 20;
        $pngQuality  = 9;

        $response = [
            "code"    => 401,
            "success" => false,
            "data"    => [],
        ];

        if ($_FILES) {
            // dd($_FILES);
            $validar = self::validarInspiracion();

            // dd($validar);
            if($validar<10) {
                // dd("ENTRE");
                $this->output->set_content_type('application/json');
                $file = $_FILES["files"];
                if ( ! is_array($file["name"]) && ! empty($file["tmp_name"])) {
                    $data     = file_get_contents($file["tmp_name"]);
                    $provider = Provider::where("id_usuario", $this->session->userdata("id_usuario"))->first();

                    if (isset($provider)) {
                        $extension = pathinfo($_FILES["files"]["name"])["extension"];
                        $fileName  = store_file($data, $extension, "images/inspiracion");
                        $finfo     = finfo_open(FILEINFO_MIME_TYPE);
                        $mime      = finfo_file($finfo, $_FILES["files"]["tmp_name"]);

                        $new_name_image = $file = $destination."/".$fileName;

                        $image_compress = new Eihror\Compress\Compress($file, $new_name_image, $quality, $pngQuality, $destination);
                        // dd($image_compress);

                        $galeria = [
                            "id_proveedor" => $empresa->id_proveedor,
                            "nombre"       => $fileName,
                            "tipo"         => Inspiracion_model::$TIPO_IMAGEN,
                            "mime"         => $mime,
                        ];
                        // dd($galeria);
                        if ($this->inspiracion->insert($galeria)) {
                            // dd("ENTRE");
                            $id = $this->inspiracion->last_id();

                            return $this->output
                                ->set_status_header(202)
                                ->set_output(
                                    json_encode(
                                        [
                                            'success' => true,
                                            'data'    => [
                                                "url"       => base_url()."uploads/images/inspiracion/$fileName",
                                                "id"        => $id,
                                                "file_name" => $fileName,
                                            ],
                                        ]
                                    )
                                );
                        }
                    }
                    $response["code"] = 403;
                }

                return $this->output->set_status_header($response["code"])->set_output(json_encode($response));
            } else {
                return $this->output
                ->set_content_type("application/json")
                ->set_status_header(406)
                ->set_output(
                    json_encode(
                        [
                            "success" => false,
                            "data"    => "",
                        ]
                    )
                );
            }
        } elseif ($_POST) {
            $this->output->set_content_type("application/json");
            $method = strtoupper($this->input->post("method", true, "String", ["required" => true]));
            $id     = $this->input->post("id", true, "Integer", ["required" => true, "min" => 1]);

            switch ($method) {
                case "DELETE":
                    $image = Inspiration::find($id);
                    // dd($image['original']['nombre']);
                    $exist= 'uploads/images/inspiracion/'.$image['original']['nombre'];
                    // dd($exist);
                    if(!file_exists($exist)) {
                        // dd("NO EXISTE");
                        $eliminar = Inspiration::destroy($id);
                        // $eliminar->destroy();

                        $response["code"]    = 202;
                        $response["success"] = true;
                        $response["data"]    = "ok";
                    } else {
                        // dd("EXISTE");
                        if ($image) {
                            $imagesPaths [] = "uploads/images/inspiracion/$image->nombre";
    
                            foreach ($imagesPaths as $imagePath) {
                                unlink($imagePath);
                            }
    
                            $eliminar = Inspiration::destroy($id);
                            // $eliminar->destroy();
    
                            $response["code"]    = 202;
                            $response["success"] = true;
                            $response["data"]    = "ok";
                        }
                    }
                    DB::table('taggables')->where('taggable_type','Inspiration')->where('taggable_id',$id)->delete();
                    // dd("STOP");
                    break;
            }

            return $this->output->set_status_header($response["code"])->set_output(json_encode($response));
        } else {
            $data["tipo"]      = $this->tipo->get($empresa->id_tipo_proveedor);
            $data["galeria"]   = $this->galeria->getImagenesProveedor($empresa->id_proveedor);
            $data["has_lp"]    = $this->galeria->hasLP($empresa->id_proveedor);
            $data["proveedor"] = $empresa;
            $data["video"]     = false;
            $this->load->view("proveedor/escaparate/fotos", $data);
        }
    }

    public function guardarDatos() {
        if($_POST) {

            $validar = self::validarInspiracion();
            if($validar<10) {
                $empresa     = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
                    // $id = $this->input->post('Id');
                    // $title = $this->input->post('Titulo');
                    // $descripcion = $this->input->post('Descripcion');
                    // $tags = $this->input->post('Tags');
                    // $guardar = Inspiration::find($id);
                    // $guardar->titulo = $title;
                    // $guardar->descripcion = $descripcion;
                    /// guardar las tags

                $galeria  =  Gallery::find($this->input->post('Id'));

                $tags =  $this->input->post('Tags');

                $inspiracion = Inspiration::create([ 
                    "id_proveedor" => $empresa->id_proveedor, 
                    "id_galeria" => $this->input->post('Id'), 
                    "titulo" => $this->input->post('Titulo'), 
                    "descripcion" => $this->input->post('Descripcion'), 
                    "mime" => $galeria->mime, 
                    "tipo" => $galeria->tipo, 
                    "nombre" => $galeria->nombre, 

                ]); 
                $guardar = Inspiration::find($inspiracion->id_inspiracion); 

                if ($tags) {
                    $guardar->tags()->sync($tags);
                    DB::table('taggables')->where('tag_id',0)->delete();
                }

                $guardar->save();
                return $this->output->set_status_header(200);
            } else {
                return $this->output
                ->set_content_type("application/json")
                ->set_status_header(406)
                ->set_output(
                    json_encode(
                        [
                            "success" => false,
                            "data"    => "",
                        ]
                    )
                );
            }
        }
    }

    public function validarInspiracion() {
        $empresa = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        $inspiraciones = Inspiration::where('id_proveedor', $empresa->id_proveedor)
                                ->count();

        return $inspiraciones;
    }

    public function cuentaPublicacion() {
        if($_POST) {
            $empresa = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
            $inspiraciones = Inspiration::where('id_proveedor', $empresa->id_proveedor)
                                    ->count();

            return $this->output->set_content_type("application/json")
            ->set_status_header(200)
            ->set_output(json_encode(['publicaciones' => $inspiraciones]));
        }
    }

    public function modalInfo() {
        if($_POST) {
            $id = $this->input->post("id_ins");
            $info = Inspiration::with('tags')->where('id_inspiracion', $id)->first();
            $comentarios = Coment::where('id_inspiracion',$id)->get();
            $data="";
            $data2="";
            $tam = sizeOf($comentarios);
            $i=0;
            for($i; $i < $tam; $i++) {
                $data[$i]["names"] = $comentarios[$i]['nombre_cliente'];
                $data[$i]["texto"] = $comentarios[$i]['comentario'];
            }
            $tam2 = sizeOf($info['relations']['tags']);
            for($i=0; $i < $tam2; $i++) {
                $data2[$i]['tags'] = $info['relations']['tags'][$i]['title'];
            }
            $foto = Gallery::where('id_proveedor', $info->id_proveedor)->where('logo', 1)
            ->where('principal', 0)->first();
            $nomProveedor = Provider::where('id_proveedor', $info->id_proveedor)->first();
            
            return $this->output->set_content_type("application/json")
                    ->set_status_header(200)
                    ->set_output(json_encode(['inspiracion' => $info, 'comentarios' => $data,
                     'fotoInfo' => $foto->nombre, 'nomproInfo' => $nomProveedor->nombre,
                     'tags' => $data2]));
        }
    }
}
