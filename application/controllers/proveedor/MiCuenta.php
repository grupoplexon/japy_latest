<?php

class MiCuenta extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("session");
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->library("Checker");
    }

    protected function middleware()
    {
        return ['provider_auth'];
    }

    public function index()
    {
        $id                = $this->session->userdata("id_proveedor");
        $data["proveedor"] = $this->proveedor->get($id);
        if ($_POST) {
            $this->session->set_userdata("tipo_mensaje", "danger");
            $this->session->set_userdata("mensaje", "Ocurrio un error al actualizar la información");

            $notify = [
                    "notificacion_informacion" => $this->input->post("informacion", true, "Boolean",
                            ["true" => "on"]) ? 1 : 0,
                    "notificacion_emails"      => $this->input->post("emails", true, "Boolean",
                            ["true" => "on"]) ? 1 : 0,
                    "notificacion_alertas"     => $this->input->post("alertas", true, "Boolean",
                            ["true" => "on"]) ? 1 : 0,
            ];

            if ($this->input->isValid()) {
                if ($this->proveedor->update($id, $notify)) {
                    $this->session->set_userdata("tipo_mensaje", "success");
                    $this->session->set_userdata("mensaje", "Información actualizada correctamente");
                    redirect("proveedor/MiCuenta");
                }
            }
        }

        $this->load->view("proveedor/micuenta", $data);
    }

}
