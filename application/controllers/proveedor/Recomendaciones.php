<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Recomendaciones extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Proveedor/FAQ_model", "faq");
        $this->load->model("Proveedor/RespuestaFAQ_model", "respuesta");
        $this->load->model("Proveedor/Galeria_model", "galeria");
        $this->load->model("Proveedor/Promocion_model", "promocion");
        $this->load->model("Proveedor/Producto_model", "producto");
        $this->load->model("Proveedor/TipoProveedor_model", "tipo");
        $this->load->model("Proveedor/Calendario_model", "calendario");
        $this->load->model("Proveedor/Colaborador_model", "colaborador");
        $this->load->model("Proveedor/ProveedorBoda_model", "ProveedorBoda_model");
        $this->load->model("Proveedor/Evento_model", "evento");
        $this->load->model("TipoProducto_model", "tipo_producto");
        $this->load->model("PropProducto_model", "prop_producto");
        $this->load->helper("formats");
        $this->load->model("Login_model", "usuario");
        $this->load->library("Checker");
    }

    protected function middleware()
    {
        return ['provider_auth'];
    }

    public function index()
    {
        $p                       = $this->input->get("pagina", true, "String", ["required" => false]);
        $page                    = $p ? $p : 1;
        $data["page"]            = $page;
        $empresa                 = $this->proveedor->get(["id_usuario" => $this->session->userdata("id_usuario")]);
        $data["tipo"]            = $this->tipo->get($empresa->id_tipo_proveedor);
        $data["recomendaciones"] = Review::where('proveedor_id',$empresa->id_proveedor)->get();//$this->proveedor->pageRecomendaciones($empresa->id_proveedor, $page);
        $data["provider"]        = Provider::with("weddings")->find($empresa->id_proveedor);
        $data["starsReviews"]    = $this->ProveedorBoda_model->getCalification($empresa->id_proveedor);
        $data["total"]           = ($this->proveedor->totalRecomendaciones($empresa->id_proveedor) / 10) + 1;

        $this->load->view("proveedor/recomendaciones/index", $data);
    }

}
