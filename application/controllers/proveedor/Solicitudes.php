<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

class Solicitudes extends MY_Controller
{
    //TODO: Rebuild the whole mail system because it's a mess
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Proveedor_model", "proveedor");
        $this->load->model("Usuario_model", "usuario");
        $this->load->model("Cliente_model", "cliente");
        $this->load->model("Boda_model", "boda");
        $this->load->model("Proveedor/Solicitud_model", "solicitud");
        $this->load->model("Correo/Correo_model", "correo");
        $this->load->model("Correo/Adjunto_model", "adjunto");
        $this->load->model("Proveedor/Archivo_model", "archivo");
        $this->load->model("Proveedor/Respuesta_model", "respuesta");
        $this->load->library("My_PHPMailer");
        $this->load->helper("formats");
        $this->load->helper("extension_helper");
        $this->load->helper("collection_paginate");
        $this->load->library("Checker");
    }

    protected function middleware()
    {
        return ['provider_auth'];
    }

    //0:pendiente,1:Atendida,2:Contratada,3:Descartada
    public function index()
    {
        $inicio = $fin = $search = $tipo = null;

        $currentState = $this->input->get("estado", true, "Integer", ["min" => 0, "required" => true]);
        $page         = $this->input->get("pagina", true, "Integer", ["min" => 1, "required" => true]);
        $accion       = $this->input->get("accion", true, "String", ["required" => true]);
        $id_usuario   = $this->session->userdata("id_usuario");
        $leido        = is_null($this->input->get('leido')) ? 0 : $this->input->get('leido');
        $parentMails  = [];

        $provider = User::find($this->session->id_usuario)->provider;

        $data["titulo"] = $currentState ? "Mis solicitudes ".Solicitudes::labelEstado($currentState)."s" : "Mis solicitudes";
        $data["leido"]  = $leido;

        ! $page ? $page = 1 : '';
        ! $provider ? show_404() : '';

        if ($accion == 'search') {
            $inicio = $this->input->get("inicio_submit", true, "Date", ["format" => 'Y-m-d']);
            $fin    = $this->input->get("fin_submit", true, "Date", ["format" => 'Y-m-d']);
            $search = $this->input->get("search", true, "String", ["minSize" => 1]);
            $tipo   = is_null($this->input->get("tipo")) ? "solicitud" : $this->input->get("tipo");  //Type is for searching dates between wedding or when it was received

            $this->session->set_userdata("tipo_mensaje", "info");

            if ($inicio && $fin) {
                $this->session->set_userdata("mensaje", "Resultado de la Busqueda: '$search' entre el $inicio y el $fin");
            } else {
                $this->session->set_userdata("mensaje", "Resultado de la Busqueda: '$search'");
            }
        }

        //The filters are applied to all the filters even the replies
        $searchMails = Mail::with('user')
            ->to($provider->user->id_usuario)
            ->visible('provider', true)
            //When searching between two dates (type indicates between wedding date or mail date)
            ->when($inicio && $fin, function ($query) use ($inicio, $fin, $tipo) {
                return $query->dateFilter($inicio, $fin, $tipo);
            })
            //When search
            ->when($search, function ($query) use ($search) {
                return $query->searchFilter($search);
            })
            //When state is set we search by state else by seen
            ->when(isset($currentState), function ($query) use ($currentState) {
                return $query->status($currentState);
            }, function ($query) use ($leido) {
                return $query->seen("provider", $leido);
            })->get();

        foreach ($searchMails as $mail) {
            if ($mail->parent) {
                $parentMails[] = $mail->parent;
            } else {
                $parentMails[] = $mail->id_correo;
            }
        }

        $parentMails = array_unique($parentMails);

        $mails = Mail::with([
            "user",
            "replies",
            "latestPendingReply",
        ])->find($parentMails);

        //For ordering
        foreach ($mails as $key => $mail) {
            if ($mail->latestReply) {
                $mail->fecha_creacion = $mail->latestReply->fecha_creacion;
            }
            if ($leido) {
                if ($mail->hasUnseenReplies($this->session->id_usuario)) {
                    $mails->forget($key);
                }
            }
        }

        $mails = $mails->sortByDesc("fecha_creacion")->values();

        $data["estado"]      = $currentState;
        $data["totales"]     = $this->totales($id_usuario);
        $data["page"]        = $page;
        $data["solicitudes"] = collection_paginate($mails, 10, $page);

        $this->load->view("proveedor/solicitudes/index", $data);
    }

    public function next()
    {
        $id     = $this->session->userdata("id_proveedor");
        $page   = $this->input->get("pagina", true, "Integer", ["min" => 1, "required" => true]);
        $estado = $this->input->get("estado", true, "Integer", ["min" => 0, "required" => true]);
        $leido  = $this->input->get("leido", true, "Integer", ["min" => 0, "required" => true]);

        if ( ! $page) {
            $page = 1;
        }

        if ($estado === null && $leido === null || ($estado === null && $leido == 0)) {
            $data["solicitudes"] = $this->correo->getEntrada($id, $page);
        } else {
            if ($estado !== null && $estado >= 0 && $estado < 4) {
                $data["solicitudes"] = $this->correo->getEntradaEstado($id, $page, $estado);
            } else {
                $data["solicitudes"] = $this->correo->getEntrada($id, $page, 1);
            }
        }

        return $this->output->set_content_type('application/json')->set_status_header(202)
            ->set_output(json_encode([
                'success' => true,
                'data'    => $data["solicitudes"],
            ]));
    }

    public function mover()
    {
        $this->output->set_content_type('application/json');
        $estado    = $this->input->post("estado", true, "Integer", ["min" => 0]);
        $solicitud = $this->input->post("solicitudes", true);

        if ($this->input->isValid()) {
            Mail::whereIn('id_correo', $solicitud)
                ->orWhereIn('parent', $solicitud)
                ->update(['estado' => $estado]);

            return $this->output
                ->set_status_header(202)
                ->set_output(
                    json_encode([
                        'success' => true,
                        'data'    => "ok",
                    ]));
        }

        return $this->output
            ->set_status_header(400)
            ->set_output(
                json_encode([
                    'success' => false,
                    'data'    => "error",
                ]));
    }

    public function marcar()
    {
        $this->output->set_content_type('application/json');

        $estado    = $this->input->post("leida", true, "Integer", ["min" => 0]);
        $solicitud = $this->input->post("solicitudes", true);

        if ($this->input->isValid()) {
            Mail::whereIn('id_correo', $solicitud)
                ->orWhereIn('parent', $solicitud)
                ->update(['leida_proveedor' => $estado]);

            return $this->output
                ->set_status_header(202)
                ->set_output(json_encode([
                        'success' => true,
                        'data'    => "ok",
                    ])
                );
        }

        return $this->output
            ->set_status_header(400)
            ->set_output(json_encode([
                    'success' => false,
                    'data'    => "error",
                ])
            );
    }

    public static function labelEstado($estado)
    {
        switch ($estado) {
            case 0:
                return "pendiente";
            case 1:
                return "atendida";
            case 2:
                return "contratada";
            case 3:
                return "descartada";
        }
    }

    private function totales($id)
    {
        $totales             = new stdClass();
        $totales->no_leidos  = Mail::getCount($id, 0);
        $totales->leidos     = Mail::getCount($id, 1);
        $totales->pendiente  = $this->correo->getCountEstado($id, 0);
        $totales->atendida   = $this->correo->getCountEstado($id, 1);
        $totales->contratada = $this->correo->getCountEstado($id, 2);
        $totales->descartada = $this->correo->getCountEstado($id, 3);

        return $totales;
    }

    public function ver($id)
    {
        $id_proveedor      = $this->session->userdata('id_proveedor');
        $id_usuario        = $this->session->userdata('id_usuario');
        $data['solicitud'] = $this->correo->get(['id_correo' => $id, 'to' => $id_usuario, 'visible_proveedor' => 1]);
        $provider          = Provider::find($id_proveedor);

        if ($data['solicitud'] && $provider) {
            if ($_POST) {
                $files             = $this->input->post("file");
                $correo['asunto']  = $this->input->post("asunto", true, "String", ["required" => true, "minSize" => 1, "maxSize" => 255]);
                $correo['mensaje'] = $this->input->post("mensaje", false, "String", ["required" => true]);

                if ($this->input->isValid()) {
                    $correo['parent'] = $data["solicitud"]->id_correo;
                    $correo['from']   = $id_usuario;
                    $correo['to']     = $data["solicitud"]->from;

                    $mail                   = Mail::find($id);
                    $correo['indicator_id'] = $mail->replies()->count() ? $mail->replies->last()->indicator_id : $mail->indicator_id;

                    if ($this->input->post("guardar", true) == "on") {
                        $id_archivo = $this->savePlantilla();
                    }

                    if ( ! empty($id_archivo)) {

                        if (is_numeric($id_archivo)) {
                            $archivo = $this->archivo->get([
                                "id_archivo"   => $id_archivo,
                                "id_proveedor" => $id_proveedor,
                            ]);

                            if ($archivo) {
                                $adjunto = [
                                    "id_archivo" => $id_archivo,
                                    "nombre"     => $archivo->nombre,
                                ];
                            }
                        } else {
                            $file    = parseBase64($id_archivo);
                            $adjunto = [
                                "mime"   => $file->mime,
                                "data"   => $file->file,
                                "nombre" => $this->input->post('file_name'),
                            ];
                        }

                        if ($adjunto && $this->adjunto->insert($adjunto)) {
                            $id_adjunto = $this->adjunto->last_id();
                        }

                    }

                    if ($this->correo->insert($correo)) {
                        $this->correo->sendEmail();

                        $id_correo = $this->correo->last_id();
                        $correo    = $this->correo->get(["id_correo" => $id_correo]);

                        $provider->indicators()->where('id', $correo->indicator_id)->update(['handled_at' => Carbon::now()]);

                        if ($id_adjunto && $id_correo) {
                            $this->correo->insertArchivo($id_adjunto, $id_correo);
                        }

                        if ($data["solicitud"]->estado == 0) {
                            $this->correo->update($data["solicitud"]->id_correo, ["estado" => 1]);
                        }

                        return redirect("proveedor/solicitudes/ver/".$id);
                    }
                }
            }

            Mail::where('id_correo', $id)
                ->orWhere('parent', $id)
                ->update(['leida_proveedor' => 1]);

            $data["estado"]    = $data["solicitud"]->estado;
            $data["leido"]     = $data["solicitud"]->leida_proveedor;
            $data["novio"]     = $this->usuario->get($data["solicitud"]->from);
            $data["cliente"]   = $this->cliente->get(["id_usuario" => $data["solicitud"]->from]);
            $data["boda"]      = $this->boda->get($data["cliente"]->id_boda);
            $data["titulo"]    = "Solicitud: ".substr($data["solicitud"]->mensaje, 0, 15)."...";
            $data["contacto"]  = $this->usuario->get($id_usuario);
            $data["proveedor"] = $this->proveedor->get($id_proveedor);
            $data["mensajes"]  = $this->correo->getMensajes($id);

            foreach ($data["mensajes"] as $key => &$msj) {
                $msj->archivos = $this->adjunto->getAdjuntos($msj->id_correo);
            }

            $data["plantillas"] = $this->respuesta->getPlantillasMin($id_proveedor);
            $data["totales"]    = $this->totales($id_usuario);

            return $this->load->view("proveedor/solicitudes/ver", $data);
        }
        show_404();
    }

    private function savePlantilla()
    {
        $id_archivo   = $this->input->post("file");
        $id_proveedor = $this->session->userdata("id_proveedor");
        $respuesta    = [
            "asunto"           => $this->input->post("asunto", true, "String", ["required" => true, "minSize" => 1, "maxSize" => 255]),
            "mensaje"          => $this->input->post("mensaje", false, "String", ["required" => true]),
            "id_proveedor"     => $id_proveedor,
            "plantilla"        => true,
            "nombre_plantilla" => $this->input->post("nombre_plantilla", true),
        ];

        if ($this->input->isValid()) {

            if (is_numeric($id_archivo) && $this->archivo->get(["id_archivo" => $id_archivo, "id_proveedor" => $id_proveedor])) {
                $id_archivo = $this->input->post("file");
            } else {
                $file = parseBase64($id_archivo);

                $archivo = [
                    "id_proveedor" => $id_proveedor,
                    "mime"         => $file->mime,
                    "data"         => $file->file,
                    "nombre"       => $this->input->post("file_name", true, "String", ["required" => true]),
                ];

                if ($this->archivo->insert($archivo)) {
                    $id_archivo = $this->archivo->last_id();
                }
            }

            if ($this->respuesta->insert($respuesta)) {
                $id_respuesta = $this->respuesta->last_id();
                if ($id_archivo && $id_respuesta) {
                    $this->respuesta->insertArchivo($id_archivo, $id_respuesta);

                    return $id_archivo;
                }
            }
        }

        return null;
    }

    public function imprimir($id)
    {
        $id_proveedor      = $this->session->userdata("id_proveedor");
        $data["solicitud"] = $this->solicitud->get(["id_solicitud" => $id, "id_proveedor" => $id_proveedor]);
        if ($data["solicitud"]) {
            $data["boda"]     = $this->boda->get($data["solicitud"]->id_boda);
            $data["cliente"]  = $this->cliente->get($data["solicitud"]->id_cliente);
            $data["novio"]    = $this->usuario->get($data["cliente"]->id_usuario);
            $data["mensajes"] = $this->solicitud->getMensajes($id_proveedor, $id);
            $data["contacto"] = $this->usuario->get($this->session->userdata("id_usuario"));
            foreach ($data["mensajes"] as $key => &$msj) {
                $msj->archivos = $this->respuesta->getArchivos($id_proveedor, $msj->id_respuesta);
            }

            return $this->load->view("proveedor/solicitudes/imprimir", $data);
        }
        show_404();
    }

    public function reenviar($id)
    {
        if ($_POST) {
            $correos    = $this->input->post("correos", true, "String", ["required" => true]);
            $id_usuario = $this->session->userdata("id_usuario");
            $solicitud  = $this->correo->get(["id_correo" => $id, "to" => $id_usuario]);
            if ($this->input->isValid() && $solicitud) {
                $correos = explode(",", $correos);
                if (count($correos) > 0) {
                    $mailer            = $this->my_phpmailer;
                    $data["solicitud"] = $this->correo->get(["id_correo" => $id, "to" => $id_usuario]);
                    $data["proveedor"] = $this->proveedor->get($this->session->id_proveedor);
                    $data["cliente"]   = $this->cliente->get(["id_usuario" => $data["solicitud"]->from]);
                    $data["boda"]      = $this->boda->get($data["cliente"]->id_boda);
                    $data["novio"]     = $this->usuario->get($data["cliente"]->id_usuario);
                    foreach ($correos as $key => $correo) {
                        if (filter_var($correo, FILTER_VALIDATE_EMAIL)) {
                            $data["correo"] = $correo;
                            $mailer->to($correo, $correo);
                            $mailer->send($this->load->view("templates/email_solicitud", $data, true));
                        }
                    }
                    $this->session->set_userdata("mensaje", "Solicitud reenviada");
                    $this->session->set_userdata("tipo_mensaje", "success");
                    redirect("proveedor/solicitudes/ver/".$id);
                }
            }
        }
        show_404();
    }

    public function plantilla($id)
    {
        $this->output->set_content_type('application/json');
        $plantilla = $this->respuesta->get($id);
        if ($plantilla) {
            $id_proveedor        = $this->session->userdata("id_proveedor");
            $plantilla->archivos = $this->respuesta->getArchivos($id_proveedor, $plantilla->id_respuesta);

            return $this->output
                ->set_status_header(200)
                ->set_output(
                    json_encode(
                        [
                            'success' => true,
                            'data'    => $plantilla,
                        ]
                    )
                );
        }

        return $this->output
            ->set_status_header(404)
            ->set_output(
                json_encode(
                    [
                        'success' => false,
                        'data'    => "error",
                    ]
                )
            );
    }

    public function updatenota($id_correo)
    {
        $this->output->set_content_type('application/json');
        if ($_POST && is_numeric($id_correo)) {
            $id_usuario = $this->session->id_usuario;
            $solicitud  = $this->correo->get(["id_correo" => $id_correo, "to" => $id_usuario]);
            $nota       = $this->input->post("nota", true, "String", ["required" => true, "maxSize" => 500]);
            if ($this->input->isValid() && $solicitud) {
                $b = $this->correo->update($id_correo, ["nota" => $nota]);
                if ($b) {
                    return $this->output->set_status_header(200)
                        ->set_output(
                            json_encode(
                                [
                                    'success' => true,
                                    'data'    => "ok",
                                ]
                            )
                        );
                }
            }
        }

        return $this->output->set_status_header(404);
    }

    public function configuracion()
    {
        $id_proveedor = $this->session->userdata("id_proveedor");

        if ($_POST) {
            $this->session->set_userdata("mensaje", "Ocurrio un problema al actualizar los correos intente más tarde");
            $this->session->set_userdata("tipo_mensaje", "error");
            $principal = $this->input->post("correo_principal", true, "Email", ["required" => true]);
            $correos   = $this->input->post("correo", true);
            if ($this->input->isValid()) {
                $emailExists = User::where('correo', $principal)
                    ->where('id_usuario', '<>', $this->session->id_usuario)
                    ->count();

                if ($emailExists) {
                    $this->session->set_userdata("mensaje", "Ya existe un usuario con ese correo");
                } else {
                    $contacto_correos = null;
                    foreach ($correos as $key => $c) {
                        if ($c && $c != "") {
                            $contacto_correos .= "$c,";
                        }
                    }
                    $this->proveedor->update($id_proveedor, ["contacto_correos" => $contacto_correos]);
                    $b = $this->usuario->update($this->session->userdata("id_usuario"), ["correo" => $principal]);
                    if ($b) {
                        $this->session->set_userdata("mensaje", "Correos actualizados exitosamente");
                        $this->session->set_userdata("tipo_mensaje", "success");
                    }
                }
            }
        }

        $data["titulo"]    = "Configuración";
        $data["proveedor"] = $this->proveedor->get($id_proveedor);
        $data["contacto"]  = $this->usuario->get($this->session->userdata("id_usuario"));
        $data["totales"]   = $this->totales($this->session->userdata("id_usuario"));

        $this->load->view("proveedor/solicitudes/opciones", $data);
    }

    public function plantillas()
    {
        $id_proveedor = $this->session->userdata("id_proveedor");
        if ($_POST) {
            $this->session->set_userdata("mensaje", "Ocurrio un error intente mas tarde");
            $this->session->set_userdata("tipo_mensaje", "danger");

            $plantilla = [
                "nombre_plantilla" => $this->input->post("nombre_plantilla", true, "String", ["required" => true, "minSize" => 3]),
                "asunto"           => $this->input->post("asunto", true, "String", ["required" => true, "minSize" => 2]),
                "mensaje"          => $this->input->post("mensaje", true, "String", ["required" => true, "minSize" => 2]),
            ];

            $id         = $this->input->post("plantilla", true, "Integer", ["required" => true, "min" => 1]);
            $id_archivo = $this->input->post("id_archivo", true, "Integer", ["min" => 1]);
            $file       = $this->input->post("file");
            $file_name  = $this->input->post("file_name");

            if ($this->input->isValid()) {
                $res = $this->respuesta->get(["id_respuesta" => $id, "id_proveedor" => $id_proveedor]);

                if ($res) {

                    if (strpos($file, "http") === -1 || $file == "") {
                        if ($id_archivo) {
                            $this->respuesta->deleteArchivo($id_archivo, $id, $id_proveedor);
                        }
                    }
                    if ( ! strpos($file, "http")) {
                        $file = parseBase64($file);
                        if ($file->mime != null) {
                            $archivo = [
                                "id_proveedor" => $id_proveedor,
                                "mime"         => $file->mime,
                                "data"         => $file->file,
                                "nombre"       => $file_name,
                            ];
                            if ($archivo["nombre"]) {
                                if ($this->archivo->insert($archivo)) {
                                    $id_archivo = $this->archivo->last_id();
                                    if ($id_archivo && $id) {
                                        $this->respuesta->insertArchivo($id_archivo, $id);
                                    }
                                }
                            }
                        }
                    }
                    $this->respuesta->update($id, $plantilla);
                    $this->session->set_userdata("mensaje", "<i class='fa fa-check' ></i> plantilla actualizada correctamente");
                    $this->session->set_userdata("tipo_mensaje", "success");

                    return redirect("proveedor/solicitudes/plantillas");
                }
            }
        }
        $data["proveedor"]  = $this->proveedor->get($id_proveedor);
        $data["contacto"]   = $this->usuario->get($this->session->userdata("id_usuario"));
        $data["totales"]    = $this->totales($this->session->userdata("id_usuario"));
        $data["plantillas"] = $this->respuesta->getPlantillas($id_proveedor);
        foreach ($data["plantillas"] as $key => &$msj) {
            $msj->archivos = $this->respuesta->getArchivos($id_proveedor, $msj->id_respuesta);
        }

        $this->load->view("proveedor/solicitudes/plantillas", $data);
    }

    public function exportar($inicio, $fin, $nombre)
    {
        $inicio = DateTime::createFromFormat('d-m-Y', $inicio);
        $fin    = DateTime::createFromFormat('d-m-Y', $fin);
        if ($inicio && $fin) {
            $id_proveedor        = $this->session->userdata("id_proveedor");
            $data["solicitudes"] = $this->solicitud->getSolicitudesTime(
                $inicio->format("Y - m - d"),
                $fin->format("Y - m - d"),
                $id_proveedor
            );
            $this->load->view("proveedor/solicitudes/exportar", $data);
        }
    }

    public function disableTemplate()
    {
        $id = $_POST["idTemplate"];
        if ($this->respuesta->updateEnableTemplate($id)) {
            return $this->output
                ->set_content_type("application/json")
                ->set_status_header(200)
                ->set_output(
                    json_encode(
                        [
                            "success" => true,
                            "data"    => "ok",
                        ]
                    )
                );
        } else {
            return $this->output
                ->set_content_type("application/json")
                ->set_status_header(404)
                ->set_output(
                    json_encode(
                        [
                            "success" => false,
                            "data"    => "error",
                        ]
                    )
                );
        }
    }

    public function sendMail($mailId)
    {
        $this->output->set_content_type("application/json");
        $response = [
            "code"    => 404,
            "message" => "error",
            "data"    => [],
        ];

        $provider = Provider::find($this->session->id_proveedor);
        $user     = User::find($this->session->id_usuario);
        $mail     = Mail::find($mailId);

        if ($provider && $user && $mail) {
            $reply = [
                "asunto"       => $this->input->post("asunto"),
                "mensaje"      => $this->input->post("mensaje"),
                "parent"       => $mail->id_correo,
                "from"         => $user->id_usuario,
                "to"           => $mail->from,
                "indicator_id" => $mail->replies()->count() ? $mail->replies->last()->indicator_id : $mail->indicator_id,
            ];

            $replyFiles      = $this->input->post("files");
            $replyFilesNames = $this->input->post("files_names");

            $reply = Mail::create($reply);

            $this->correo->sendEmail();

            if (isset($replyFiles) && count($replyFiles)) {
                //Set up the file array with data and name

                $replyFiles = array_map(function ($item) {
                    $newItem["data"] = $item;

                    $pos             = strpos($item, ';');
                    $newItem["mime"] = explode(':', substr($item, 0, $pos))[1];

                    return $newItem;
                }, $replyFiles);

                //Attach the files to the reply
                foreach ($replyFiles as $key => $replyFile) {
                    $reply->attachments()->create(["data" => $replyFile["data"], "nombre" => $replyFilesNames[$key], "mime" => $replyFile["mime"]]);
                }
            }

            //If the user wants to save the template
            if ($this->input->post("guardar") == "on") {
                $replyTemplate = [
                    "asunto"           => $reply["asunto"],
                    "mensaje"          => $reply["mensaje"],
                    "id_proveedor"     => $provider->id_proveedor,
                    "plantilla"        => true,
                    "nombre_plantilla" => $this->input->post("nombre_plantilla"),
                ];

                $replyIdFiles = [];

                if (isset($replyFiles) && count($replyFiles)) {
                    foreach ($replyFiles as $key => $replyFile) {
                        $this->archivo->insert(["data" => $replyFile["data"], "nombre" => $replyFilesNames[$key], "mime" => $replyFile["mime"]]);
                        $replyIdFiles [] = $this->archivo->last_id();
                    }
                }

                if ($this->respuesta->insert($replyTemplate)) {
                    $id_respuesta = $this->respuesta->last_id();
                    if ($id_respuesta && isset($replyIdFiles) && count($replyIdFiles)) {
                        foreach ($replyIdFiles as $replyIdFile) {
                            $this->respuesta->insertArchivo($replyIdFile, $id_respuesta);
                        }
                    }
                }
            }

            $response["code"]    = 200;
            $response["message"] = "ok";
        }

        return $this->output->set_content_type("application/json")
            ->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }

    public function updateTemplate($templateId)
    {
        $this->output->set_content_type("application/json");
        $response        = [
            "code"    => 404,
            "message" => "error",
            "data"    => [],
        ];
        $replyFiles      = $this->input->post("files");
        $replyFilesNames = $this->input->post("files_names");
        $template        = DB::table("respuesta")->where("id_respuesta", $templateId);

        if ($template->exists()) {
            $template->update([
                "asunto"  => $this->input->post("asunto"),
                "mensaje" => $this->input->post("mensaje"),
            ]);

            //Delete all current files
            DB::table("respuesta_archivo")->where("id_respuesta", $templateId)->delete();

            if (isset($replyFiles)) {
                //Set up the file array with data and name
                $replyFiles = array_map(function ($item) {
                    $newItem["data"] = $item;

                    $pos             = strpos($item, ';');
                    $newItem["mime"] = explode(':', substr($item, 0, $pos))[1];

                    return $newItem;
                }, $replyFiles);

                $replyIdFiles = [];

                if (isset($replyFiles) && count($replyFiles)) {
                    foreach ($replyFiles as $key => $replyFile) {
                        $this->archivo->insert(["data" => $replyFile["data"], "nombre" => $replyFilesNames[$key], "mime" => $replyFile["mime"]]);
                        $replyIdFiles [] = $this->archivo->last_id();
                    }
                }

                if ($templateId && isset($replyIdFiles) && count($replyIdFiles)) {
                    foreach ($replyIdFiles as $replyIdFile) {
                        $this->respuesta->insertArchivo($replyIdFile, $templateId);
                    }
                }
            }

            $response["code"]    = 200;
            $response["message"] = "ok";
        }

        return $this->output->set_content_type("application/json")
            ->set_status_header($response["code"])
            ->set_output(json_encode($response));
    }
}
