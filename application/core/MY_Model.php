<?php class MY_Model extends CI_Model
{
    protected $tabla;
    protected $id_tabla;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get($id)
    {
        if (is_array($id)) {
            $query = $this->db->get_where($this->tabla, $id);
        } else {
            $query = $this->db->get_where($this->tabla, array($this->id_tabla => $id));
        }
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }

    public function getList($id, $limit1 = Null, $limit2 = NULL)
    {
        if (is_array($id)) {
            $query = $this->db->get_where($this->tabla, $id, $limit1, $limit2);
        } else {
            $query = $this->db->get_where($this->tabla, array($this->id_tabla => $id), $limit1, $limit2);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function getListOrderBy($id, $order_by, $desc = "asc")
    {
        if (is_array($id)) {
            $query = $this->db->select("*")->from($this->tabla)->where($id)->order_by($order_by, $desc)->get();
        } else {
            $query = $this->db->select("*")->from($this->tabla)->where(array($this->id_tabla => $id))->order_by($order_by, $desc)->get();
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function getAll($columns = "*", $limit1 = Null, $limit2 = NULL)
    {
        if ($limit2 == NULL && $limit1 != NULL) {
            $this->db->select($columns)->from($this->tabla)->limit($limit1);
        } else if ($limit1 != NULL && $limit2 != NULL) {
            $this->db->select($columns)->from($this->tabla)->limit($limit1, $limit2);
        } else {
            $this->db->select($columns)->from($this->tabla);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return array();
    }

    public function count($column = "*")
    {
        if ($column == "*") {
            $column = $this->id_tabla;
        }
        $sql = "select COUNT(DISTINCT $column ) as total from $this->tabla";
        $result = $this->db->query($sql)->row();
        return $result->total;
    }

    public function update($id, $datos)
    {
        try {
            $this->db->where(array($this->id_tabla => $id));
            return $this->db->update($this->tabla, $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    public function insert($datos)
    {
        try {
            return $this->db->insert($this->tabla, $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    public function insert_batch($datos)
    {
        try {
            return $this->db->insert_batch($this->tabla, $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    public function last_id()
    {
        return $this->db->insert_id();
    }

    public function delete($id)
    {
        try {
            if (is_array($id)) {
                return $this->db->delete($this->tabla, $id);
            } else {
                return $this->db->delete($this->tabla, array($this->id_tabla => $id));
            }
        } catch (Exception $e) {
            return false;
        }
    }
}
