<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! function_exists("parseBase64")) {

    function parseBase64($strBase64)
    {
        $o = new stdClass();

        if (empty($strBase64)) {
            $o->mime = null;
            $o->file = null;
        } else {
            $img          = explode(',', $strBase64);
            $ini          = substr($img[0], 11);
            $o->extension = explode(';', $ini)[0];

            $mime    = explode(",", $strBase64);
            $o->file = base64_decode($mime[1]);

            $mime    = explode(";", $mime[0]);
            $mime    = explode(":", $mime[0]);
            $o->mime = $mime[1];
        }

        return $o;
    }

}
