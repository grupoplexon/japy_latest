<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ( ! function_exists('generarCadena')) {

    function generarCadena()
    {
        $caracteres     = "abcdefghijklmnopqrstuvwxyz1234567890";
        $numerodeletras = 56;
        $cadena         = "";
        for ($i = 0; $i < $numerodeletras; $i++) {
            $cadena = $cadena.substr($caracteres, rand(0, strlen($caracteres)), 1);
        }

        return $cadena;
    }

}

if ( ! function_exists('generarCadenaTamano')) {

    function generarCadenaTamano($numerodeletras)
    {
        $caracteres = "abcdefghijklmnopqrstuvwxyz1234567890";
        $cadena     = "";
        for ($i = 0; $i < $numerodeletras; $i++) {
            $cadena = $cadena.substr($caracteres, rand(0, strlen($caracteres)), 1);
        }

        return $cadena;
    }

}

if ( ! function_exists('sanear_string')) {

    function sanear_string($string)
    {

        $string = trim($string);

        $string = str_replace(
                ['á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'], ['a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'], $string
        );

        $string = str_replace(
                ['é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'], ['e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'], $string
        );

        $string = str_replace(
                ['í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'], ['i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'], $string
        );

        $string = str_replace(
                ['ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'], ['o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'], $string
        );

        $string = str_replace(
                ['ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'], ['u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'], $string
        );

        $string = str_replace(
                ['ñ', 'Ñ', 'ç', 'Ç'], ['n', 'N', 'c', 'C',], $string
        );
        $string = str_replace(
                [
                        "",
                        "¨",
                        "º",
                        "-",
                        "~",
                        "#",
                        "@",
                        "|",
                        "!",
                        "&#34;",
                        "·",
                        "$",
                        "%",
                        "&",
                        "/",
                        "(",
                        ")",
                        "?",
                        "'",
                        "¡",
                        "¿",
                        "[",
                        "^",
                        "`",
                        "]",
                        "+",
                        "}",
                        "{",
                        "¨",
                        "´",
                        ">",
                        "< ",
                        ";",
                        ",",
                        ":",
                        ".",
                        " ",
                ], ' ', $string
        );

        return $string;
    }

}

if ( ! function_exists('limpiar_javascript')) {

    function limpiar_javascript($string)
    {
        preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $string);
    }

}

function getFooter()
{
    $CI = get_instance();
    if (empty($CI->session->userdata('home_seccion'))) {
        $CI->load->model('Home/Home_seccion_model', 'Home_seccion_model');
        $info = $CI->Home_seccion_model->get();
        $CI->session->set_userdata('home_seccion', $info);

        return $info;
    } else {
        return $CI->session->userdata('home_seccion');
    }
}

function productos()
{
    $prod                = [];
    $p                   = new stdClass();
    $p->nombre           = "Vestidos de Novia";
    $p->sortable         = ["escote" => true, "corte" => true, "diseñador" => true, "temporada" => true];
    $p->tipo_proveedores = [22];
    $p->descripcion      = "Tu vestido de novia es el más bonito que te pondrás en tu vida. Busca dentro de nuestro catálogo, es el más grande en moda nupcial y seguro que encuentras ese diseño único que te hace brillar con luz propia. Tenemos más de 6.000 vestidos de más de 50 diseñadores. Podrás filtrar por diseñador, corte, temporada y/o escote.";
    $prod[]              = $p;
    $p                   = new stdClass();
    $p->nombre           = "Trajes de Novio";
    $p->sortable         = ["estilo" => true, "diseñador" => true, "temporada" => true];
    $p->tipo_proveedores = [27];
    $p->descripcion      = "El traje de novio ofrece un montón de opciones para elegir con estilos muy diferentes. Adéntrate en nuestro catálogo y encuentra tu diseño ideal entre los más de 300 trajes que tenemos. Podrás filtrar por diseñador, temporada y estilo, y descubrirás las últimas propuestas y tendencias en moda para novios.";
    $prod[]              = $p;
    $p                   = new stdClass();
    $p->nombre           = "Vestidos de Fiesta";
    $p->sortable         = ["largo" => true, "diseñador" => true, "temporada" => true];
    $p->tipo_proveedores = [26];
    $p->descripcion      = "A todas nos encanta estrenar vestido en las bodas y sentirnos muy guapas. En nuestro catálogo tenemos de todo, curiosea un poco y ya verás como encuentras tu diseño ideal para esta boda. Elige entre más de 2.000 vestidos de fiesta por diseñador, temporada, largo, y descubre las últimas propuestas y tendencias en moda fiesta.";
    $prod[]              = $p;
    $p                   = new stdClass();
    $p->nombre           = "Joyeria";
    $p->sortable         = ["tipo" => true, "diseñador" => true, "temporada" => true];
    $p->tipo_proveedores = [24];
    $p->descripcion      = "Encuentra la joyería perfecta para tu vestido de novia en nuestro catálogo nupcial y ¡brilla! Descubre las alianzas perfectas entre miles de diseños, conoce los diseñadores más reconocidos y escoge las joyas claves para complementar tu look entre aretes, collares, pulseras, diademas, tiaras, tocados... ¡y mucho más!";
    $prod[]              = $p;
    $p                   = new stdClass();
    $p->nombre           = "Zapatos";
    $p->sortable         = ["categoria" => true, "diseñador" => true, "temporada" => true];
    $p->tipo_proveedores = [23, 28];
    $p->descripcion      = "¿Cómo los quieres? Altos, bajos, con tacón, de plataforma, de color, con pedrería, abiertos, cerrados... en nuestro catálogo nupcial encontrarás miles de opciones para caminar hacia el altar con estilo. Conoce los diseñadores más reconocidos y las últimas tendencias. Tenemos diseños para todos los gustos ¡empieza la búsqueda!";
    $prod[]              = $p;
    $p                   = new stdClass();
    $p->nombre           = "Lenceria";
    $p->sortable         = ["tipo" => true, "diseñador" => true, "temporada" => true];
    $p->tipo_proveedores = [23];
    $p->descripcion      = "La noche de bodas será la más especial de tus noches y necesitas la lencería ideal. En nuestro catálogo nupcial tenemos miles de opciones para todas las figuras y gustos: lencería de fantasía, bodys, sostenes con o sin tirantes, panties etc. Encuentra el modelo que encaje perfectamente con el corte de tu vestido y déjalo boquiabierto :)";
    $prod[]              = $p;
    $p                   = new stdClass();
    $p->nombre           = "Complementos";
    $p->sortable         = ["categoria" => true, "tipo" => true, "diseñador" => true, "temporada" => true];
    $p->tipo_proveedores = [23, 28];
    $p->descripcion      = "Un gran vestido o un buen traje necesitan unos complementos a la altura para verte espectacular en la boda. En nuestro catálogo encontrarás todo lo que necesitas. Busca entre más de 1.000 complementos por diseñador, temporada, tipo, y descubre las últimas propuestas y tendencias en moda y complementos.";
    $prod[]              = $p;

    return $prod;
}
