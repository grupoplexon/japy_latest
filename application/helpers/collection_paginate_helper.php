<?php
if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if ( ! function_exists('collection_paginate')) {
    function collection_paginate($items, $per_page, $page = 1)
    {
        $offset = ($page * $per_page) - $per_page;

        return new Illuminate\Pagination\LengthAwarePaginator(
                $items->forPage($page, $per_page)->values(),
                $items->count(),
                $per_page,
                $page,
                ['path' => Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
    }
}

