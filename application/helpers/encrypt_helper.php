<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ( ! function_exists('decrypt')) {

    function decrypt($q)
    {
        $CI =& get_instance();
        $CI->load->library('session');
        $dirty    = ["+", "/", "="];
        $clean    = ["_P_", "_S_", "_E_"];
        $q        = str_replace($clean, $dirty, $q);
        $cryptKey = $CI->session->userdata('encryption');
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC,
                md5(md5($cryptKey))), "\0");
        $qDecoded = is_numeric($qDecoded) ? $qDecoded : 0;

        return (int)$qDecoded;
    }

}

if ( ! function_exists('encrypt')) {

    function encrypt($q)
    {
        $CI =& get_instance();
        $CI->load->library('session');
        $cryptKey = $CI->session->userdata('encryption');
        $dirty    = ["+", "/", "="];
        $clean    = ["_P_", "_S_", "_E_"];
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC,
                md5(md5($cryptKey))));

        return str_replace($dirty, $clean, $qEncoded);
    }

}