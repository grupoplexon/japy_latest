<?php

defined('BASEPATH') OR exit('No direct script access allowed');

setlocale(LC_TIME, 'es_ES.UTF-8');
setlocale(LC_MONETARY, 'en_US');
date_default_timezone_set('America/Mexico_City');


if ( ! function_exists('timeFormat')) {

    function timeFormat($time, $format = "%A %d de %B del %Y %I:%M %p")
    {
        $transf = strtotime($time);

        return strftime($format, $transf);
    }

}


if ( ! function_exists("money_format")) {

    function money_format($formato, $valor)
    {
        if (setlocale(LC_MONETARY, 0) == 'C') {
            return number_format($valor, 2);
        }

        // Obter dados da localidade 
        $locale = localeconv();

        // Extraindo opcoes do formato 
        $regex = '/^'. // Inicio da Expressao
                '%'. // Caractere %
                '(?:'. // Inicio das Flags opcionais
                '\=([\w\040])'. // Flag =f
                '|'.
                '([\^])'. // Flag ^
                '|'.
                '(\+|\()'. // Flag + ou (
                '|'.
                '(!)'. // Flag !
                '|'.
                '(-)'. // Flag -
                ')*'. // Fim das flags opcionais
                '(?:([\d]+)?)'. // W  Largura de campos
                '(?:#([\d]+))?'. // #n Precisao esquerda
                '(?:\.([\d]+))?'. // .p Precisao direita
                '([in%])'. // Caractere de conversao
                '$/';             // Fim da Expressao 

        if ( ! preg_match($regex, $formato, $matches)) {
            trigger_error('Formato invalido: '.$formato, E_USER_WARNING);

            return $valor;
        }

        // Recolhendo opcoes do formato 
        $opcoes = [
                'preenchimento'   => ($matches[1] !== '') ? $matches[1] : ' ',
                'nao_agrupar'     => ($matches[2] == '^'),
                'usar_sinal'      => ($matches[3] == '+'),
                'usar_parenteses' => ($matches[3] == '('),
                'ignorar_simbolo' => ($matches[4] == '!'),
                'alinhamento_esq' => ($matches[5] == '-'),
                'largura_campo'   => ($matches[6] !== '') ? (int)$matches[6] : 0,
                'precisao_esq'    => ($matches[7] !== '') ? (int)$matches[7] : false,
                'precisao_dir'    => ($matches[8] !== '') ? (int)$matches[8] : $locale['int_frac_digits'],
                'conversao'       => $matches[9],
        ];

        // Sobrescrever $locale 
        if ($opcoes['usar_sinal'] && $locale['n_sign_posn'] == 0) {
            $locale['n_sign_posn'] = 1;
        } elseif ($opcoes['usar_parenteses']) {
            $locale['n_sign_posn'] = 0;
        }
        if ($opcoes['precisao_dir']) {
            $locale['frac_digits'] = $opcoes['precisao_dir'];
        }
        if ($opcoes['nao_agrupar']) {
            $locale['mon_thousands_sep'] = '';
        }

        // Processar formatacao 
        $tipo_sinal = $valor >= 0 ? 'p' : 'n';
        if ($opcoes['ignorar_simbolo']) {
            $simbolo = '';
        } else {
            $simbolo = $opcoes['conversao'] == 'n' ? $locale['currency_symbol'] : $locale['int_curr_symbol'];
        }
        $numero = number_format(abs($valor), $locale['frac_digits'], $locale['mon_decimal_point'],
                $locale['mon_thousands_sep']);

        /*
          //TODO: dar suporte a todas as flags
          list($inteiro, $fracao) = explode($locale['mon_decimal_point'], $numero);
          $tam_inteiro = strlen($inteiro);
          if ($opcoes['precisao_esq'] && $tam_inteiro < $opcoes['precisao_esq']) {
          $alinhamento = $opcoes['alinhamento_esq'] ? STR_PAD_RIGHT : STR_PAD_LEFT;
          $numero = str_pad($inteiro, $opcoes['precisao_esq'] - $tam_inteiro, $opcoes['preenchimento'], $alinhamento).
          $locale['mon_decimal_point'].
          $fracao;
          }
         */

        $sinal         = $valor >= 0 ? $locale['positive_sign'] : $locale['negative_sign'];
        $simbolo_antes = $locale[$tipo_sinal.'_cs_precedes'];

        // Espaco entre o simbolo e o numero 
        $espaco1 = $locale[$tipo_sinal.'_sep_by_space'] == 1 ? ' ' : '';

        // Espaco entre o simbolo e o sinal 
        $espaco2 = $locale[$tipo_sinal.'_sep_by_space'] == 2 ? ' ' : '';

        $formatado = '';
        switch ($locale[$tipo_sinal.'_sign_posn']) {
            case 0:
                if ($simbolo_antes) {
                    $formatado = '('.$simbolo.$espaco1.$numero.')';
                } else {
                    $formatado = '('.$numero.$espaco1.$simbolo.')';
                }
                break;
            case 1:
                if ($simbolo_antes) {
                    $formatado = $sinal.$espaco2.$simbolo.$espaco1.$numero;
                } else {
                    $formatado = $sinal.$numero.$espaco1.$simbolo;
                }
                break;
            case 2:
                if ($simbolo_antes) {
                    $formatado = $simbolo.$espaco1.$numero.$sinal;
                } else {
                    $formatado = $numero.$espaco1.$simbolo.$espaco2.$sinal;
                }
                break;
            case 3:
                if ($simbolo_antes) {
                    $formatado = $sinal.$espaco2.$simbolo.$espaco1.$numero;
                } else {
                    $formatado = $numero.$espaco1.$sinal.$espaco2.$simbolo;
                }
                break;
            case 4:
                if ($simbolo_antes) {
                    $formatado = $simbolo.$espaco2.$sinal.$espaco1.$numero;
                } else {
                    $formatado = $numero.$espaco1.$simbolo.$espaco2.$sinal;
                }
                break;
        }

        // Se a string nao tem o tamanho minimo 
        if ($opcoes['largura_campo'] > 0 && strlen($formatado) < $opcoes['largura_campo']) {
            $alinhamento = $opcoes['alinhamento_esq'] ? STR_PAD_RIGHT : STR_PAD_LEFT;
            $formatado   = str_pad($formatado, $opcoes['largura_campo'], $opcoes['preenchimento'], $alinhamento);
        }

        return $formatado;
    }

}


if ( ! function_exists("moneyFormat")) {

    function moneyFormat($val, $simbol = false)
    {
        return number_format($val, 2, ".", ",");
    }

}


if ( ! function_exists('relativeTimeFormat')) {

    function relativeTimeFormat($time, $mini = false)
    {
        $transf          = strtotime($time);
        $segundos        = strtotime('now') - $transf;
        $diferencia_dias = intval($segundos / 60 / 60 / 24);
        if ($diferencia_dias > 0) {//mas de un dia
            if ($diferencia_dias == 1) {
                return "ayer";
            }
            if ($mini === false) {
                return strftime("%d de %B del %Y %I:%M %p", $transf);
            } else {
                return strftime("%d-%m-%y", $transf);
            }
        }
        $horas = intval($segundos / 60 / 60);
        if ($horas > 0) {
            return $horas." horas";
        }
        $minutos = intval($segundos / 60);
        if ($minutos > 5) {
            return $minutos." minutos";
        }

        return "Hace un momento";
    }

}


if ( ! function_exists('relativeTimeFormat_mini')) {

    function relativeTimeFormat_mini($time, $mini = false)
    {
        $transf          = strtotime($time);
        $segundos        = strtotime('now') - $transf;
        $diferencia_dias = intval($segundos / 60 / 60 / 24);
        if ($diferencia_dias > 0) {//mas de un dia
            if ($diferencia_dias == 1) {
                return "ayer";
            }
            if ($mini === false) {
                return strftime("%d de %B del %Y %I:%M %p", $transf);
            } else {
                return strftime("%d-%m-%y", $transf);
            }
        }
        $horas = intval($segundos / 60 / 60);
        if ($horas > 0) {
            return $horas." hrs";
        }
        $minutos = intval($segundos / 60);
        if ($minutos > 5) {
            return $minutos." min";
        }

        return "ahora";
    }

}


if ( ! function_exists('dateMiniFormat')) {

    function dateMiniFormat($time, $format = "%d-%B-%Y")
    {
        $transf = strtotime($time);

        return strftime($format, $transf);
    }

}

if ( ! function_exists('dateFormat')) {

    function dateFormat($time, $format = "%A %d de %B del %Y ")
    {
        $transf = strtotime($time);

        return strftime($format, $transf);
    }

}

if ( ! function_exists('estado_promocion')) {

    function estado_promocion($fecha)
    {
        $now   = new DateTime("now");
        $promo = new DateTime($fecha);
        if ($now > $promo) {
            return "Finalizada";
        } elseif ($now < $promo) {
            return "Finaliza el ".dateMiniFormat($fecha, "%d-%h-%g");
        } else {
            return "Hoy finaliza";
        }
    }

}

if ( ! function_exists('icon_tipo_promocion')) {

    function icon_tipo_promocion($tipo)
    {
        switch (strtoupper($tipo)) {
            case "OFERTA":
                return "<i class='fa fa-star'></i>";
            case "REGALO":
                return "<i class='fa fa-gift'></i>";
            case "DESCUENTO":
                return "<i class='fa fa-tags'></i>";
        }
    }

}

if ( ! function_exists('color_tipo_promocion')) {

    function color_tipo_promocion($tipo)
    {
        switch (strtoupper($tipo)) {
            case "OFERTA":
            case "REGALO":
                return " yellow darken-3 white-text ";
            case "DESCUENTO":
                return " orange darken-3 white-text ";
        }
    }

}

if ( ! function_exists('icon_categoria')) {

    function icon_categoria($nombre)
    {
        switch (str_sin_acentos(strtolower($nombre))) {
            case "catering":
                return "proveedor-cat-catering";
            case "ceremonia":
                return "proveedor-cat-ceremonia";
            case "salon":
                return "proveedor-cat-salon";
            case "foto y video":
                return "proveedor-cat-fotografia";
            case "musica":
                return "proveedor-cat-musica";
            case "autos para boda":
                return "proveedor-cat-autos";
            case "invitaciones":
                return "proveedor-cat-invitaciones";
            case "recuerdos":
                return "proveedor-cat-recuerdos";
            case "flores y decoracion":
                return "proveedor-cat-arreglos";
            case "animacion":
                return "proveedor-cat-animacion";
            case "planeacion":
                return "proveedor-cat-planner";
            case "pasteles":
                return "proveedor-cat-pasteles";
            case "novia y complementos":
                return "proveedor-cat-novia-complementos";
            case "novio y accesorios":
                return "proveedor-cat-novio";
            case "joyeria":
                return "proveedor-cat-joyeria";
            case "luna de miel":
                return "proveedor-cat-luna-miel";
            case "belleza y salud":
                return "proveedor-cat-salud-belleza";
        }
    }

}


if ( ! function_exists('icon_categoria_presupuesto')) {

    function icon_categoria_presupuesto($nombre)
    {
        switch (str_sin_acentos(strtolower($nombre))) {
            case "novia":
                return "proveedor-cat-catering";
            case "novio":
                return "proveedor-cat-catering";
            case "complementos de novios":
                return "proveedor-cat-ceremonia";
            case "invitaciones":
                return "proveedor-cat-recepcion";
            case "foto y video":
                return "proveedor-cat-salon";
            case "ceremonia":
                return "proveedor-cat-fotografia";
            case "recepcion":
                return "proveedor-cat-musica";
            case "decoracion y flores":
                return "proveedor-cat-autos";
            case "extras":
                return "proveedor-cat-invitaciones";
            case "mesa de regalos":
                return "proveedor-cat-recuerdos";
            case "luna de miel":
                return "proveedor-cat-arreglos";
            case "hogar":
                return "proveedor-cat-animacion";
            case "financiamiento":
                return "proveedor-cat-planner";
        }
    }
}

if ( ! function_exists('icon_img_proveedor')) {

    function icon_img_proveedor($nombre)
    {
        return str_replace(" ", "%20", (str_sin_acentos(strtolower($nombre))));
    }

}

if ( ! function_exists("str_sin_acentos")) {

    function str_sin_acentos($text)
    {
        $text   = htmlentities($text, ENT_QUOTES, 'UTF-8');
        $text   = strtolower($text);
        $patron = [
            // Espacios, puntos y comas por guion
            //'/[\., ]+/' => ' ',
            // Vocales
                '/\+/'       => '',
                '/&agrave;/' => 'a',
                '/&egrave;/' => 'e',
                '/&igrave;/' => 'i',
                '/&ograve;/' => 'o',
                '/&ugrave;/' => 'u',
                '/&aacute;/' => 'a',
                '/&eacute;/' => 'e',
                '/&iacute;/' => 'i',
                '/&oacute;/' => 'o',
                '/&uacute;/' => 'u',
                '/&acirc;/'  => 'a',
                '/&ecirc;/'  => 'e',
                '/&icirc;/'  => 'i',
                '/&ocirc;/'  => 'o',
                '/&ucirc;/'  => 'u',
                '/&atilde;/' => 'a',
                '/&etilde;/' => 'e',
                '/&itilde;/' => 'i',
                '/&otilde;/' => 'o',
                '/&utilde;/' => 'u',
                '/&auml;/'   => 'a',
                '/&euml;/'   => 'e',
                '/&iuml;/'   => 'i',
                '/&ouml;/'   => 'o',
                '/&uuml;/'   => 'u',
                '/&auml;/'   => 'a',
                '/&euml;/'   => 'e',
                '/&iuml;/'   => 'i',
                '/&ouml;/'   => 'o',
                '/&uuml;/'   => 'u',
            // Otras letras y caracteres especiales
                '/&aring;/'  => 'a',
                '/&ntilde;/' => 'n',
            // Agregar aqui mas caracteres si es necesario
        ];

        $text = preg_replace(array_keys($patron), array_values($patron), $text);

        return $text;
    }

}