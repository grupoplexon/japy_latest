<?php
if ( ! defined("BASEPATH")) {
    exit("No direct script access allowed");
}

if ( ! function_exists("create_slug")) {
    function create_slug($id, $string)
    {
        $slug = str_slug($string);
        if (Provider::where("slug", $slug)->where("id_proveedor", "<>", $id)->get()->count()) {
            for ($i = 1; $i < 1000; $i++) {
                if ( ! Provider::where("slug", $slug."-".$i)->where("id_proveedor", "<>", $id)->get()->count()) {
                    break;
                }
            }
        }

        return isset($i) ? $slug."-".$i : $slug;
    }
}

if ( ! function_exists("create_category_slug")) {
    function create_category_slug($id, $string)
    {
        $slug = str_slug($string);
        if (Category::where("slug", $slug)->where("id", "<>", $id)->get()->count()) {
            for ($i = 1; $i < 1000; $i++) {
                if ( ! Category::where("slug", $slug."-".$i)->where("id", "<>", $id)->get()->count()) {
                    break;
                }
            }
        }

        return isset($i) ? $slug."-".$i : $slug;
    }
}

