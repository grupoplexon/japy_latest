<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! function_exists("store_file")) {

    function store_file($file, $extension, $folder)
    {
        $fileName = uniqid().".".$extension;
        file_put_contents("uploads/$folder/$fileName", $file);

        return $fileName;
    }

}
