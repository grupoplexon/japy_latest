<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categoria
 *
 * @author Evolutel
 */
class Categoria
{

    public $nombre;
    public $id;

    public function __construct($id = null, $nombre = null)
    {
        $this->nombre = $nombre;
        $this->id = $id;
    }

//    public function __construct() {
//        $args = func_get_args();
//        $nargs = func_num_args();
//        switch($nargs)
//        case 1:
//        self::__construct1($args[0]);
//        break;
//        case 2:
//        self::__construct2($args[0], $args[1]);
//        break;
//    }

    public static function getCategorias()
    {
        /*        $categorias = [
                    ['nombre' => 'Novia',
                        'servicios' => ['Vestido', 'Zapatos', 'Tocado', 'Peinado', 'Maquillaje', 'Joyería', 'Uñas', 'Spa', 'Lenceria']
                    ],
                    ['nombre' => 'Novio',
                        'servicios' => ['Renta de traje', 'Camisa', 'Zapatos', 'Cinturón']
                    ],
                    ['nombre' => 'Complementos de novios',
                        'servicios' => ['Carro de novios', 'Joyería', 'Hoteles']
                    ],
                    ['nombre' => 'Invitaciones',
                        'servicios' => ['Invitaciones']
                    ],
                    ['nombre' => 'Foto y video',
                        'servicios' => ['Fotografía', 'Video']
                    ],
                    ['nombre' => 'Ceremonia',
                        'servicios' => ['Música ceremonia']
                    ],
                    ['nombre' => 'Recepcion',
                        'servicios' => ['Banquete', 'Salón', 'Mobiliario', 'Música', 'Vino', 'Wedding planner', 'Pastel']
                    ],
                    ['nombre' => 'Decoracion y flores',
                        'servicios' => ['Florerías y decoracion']
                    ],
                    ['nombre' => 'Extras',
                        'servicios' => ['Cena desvelados', 'Pirotecnia', 'Cabina de fotos', 'Recuerditos', 'Termos,sombreros y accesorios', 'Pantuflas', 'Letras gigantes', 'Cortesía baños', 'Salas lounge', 'Mobiliario ingreso', 'Iluminación arquitectónica', 'Shows', 'Sombrillas', 'Pérgolas', 'Calentones', 'Abanicos', 'Candy bar', 'Barra shots', 'Toldos', 'Extras']
                    ],
                    ['nombre' => 'Mesa de regalos',
                        'servicios' => ['Mesa de regalos']
                    ],
                    ['nombre' => 'Luna de miel',
                        'servicios' => ['Agencias de viajes', 'Hoteles', 'Líneas aéreas', 'Cruceros', 'Transporte', 'Tours', 'Restaurantes', 'Otros luna de miel']
                    ],
                    ['nombre' => 'Hogar',
                        'servicios' => ['Muebles', 'Automóviles', 'Electrodomesticos']
                    ],
                    ['nombre' => 'Financiamiento',
                        'servicios' => ['Financiamiento', 'Tarjetas de crédito']
                    ],
                ];*/

        // $categories = Category::main()->with('subcategories')->get();
        $categories = Category::get();

        foreach ($categories as $category) {
            $servicios = [];
            foreach ($category->subcategories as $subcategory) {
                $servicios[] = ['id' => $subcategory->id, 'nombre' => $subcategory->name];
            }
            $categoria = ['id' => $category->id, 'nombre' => $category->name, 'servicios' => $servicios];
            $categorias[] = (object)$categoria;
        }

//      $categoria[] = new Categoria(1, "Salón");
//      $categoria[] = new Categoria(2, "Catering");
//      $categoria[] = new Categoria(3, "Foto y video");
//      $categoria[] = new Categoria(4, "Música");
//      $categoria[] = new Categoria(5, "Autos para Boda");
//      $categoria[] = new Categoria(6, "Invitaciones");
//      $categoria[] = new Categoria(7, "Recuerdos");
//      $categoria[] = new Categoria(8, "Flores y Decoración");
//      $categoria[] = new Categoria(9, "Animación");
//      $categoria[] = new Categoria(10, "Planeación");
//      $categoria[] = new Categoria(11, "Pasteles");
//      $categoria[] = new Categoria(12, "Novia y Complementos");
//      $categoria[] = new Categoria(13, "Novio y Accesorios");
//      $categoria[] = new Categoria(14, "Belleza y Salud");
//      $categoria[] = new Categoria(15, "Joyería");
//      $categoria[] = new Categoria(16, "Luna de Miel");
//      $categoria[] = new Categoria(17, "Otros");
//      $categoria[] = new Categoria(18, "Ceremonia");

        return $categorias;
    }

    public static function getCategoriaName($id_categoria)
    {
        $categorias = Categoria::getCategorias();
        foreach ($categorias as $value) {
            if ($value->id == $id_categoria) {
                return $value->nombre;
            }
        }
        return false;
    }

    public static function getCategoria($nombre)
    {
        $cats = Categoria::getCategorias();
        foreach ($cats as $key => $value) {
            if ($value->nombre == $nombre) {
                return $value;
            }
        }
        return false;
    }

    public function descripcionGrupo($grupo)
    {
        switch (strtoupper($grupo)) {
            case "PROVEEDORES":
                return "<b>Guía de Proveedores de bodas {{ESTADO}}:</b> Aquí encontrarás todo los servicios que necesitas para organizar tu boda {{ESTADO}}, fotógrafos, invitaciones, recuerdos de boda, empresas de animación y música hasta pastelerías para tu pastel de bodas.";
            case "BANQUETES":
                return "<b>Guía de Banquetes para bodas {{ESTADO}}:</b> Encuentra todo tipo de lugares para celebrar tu boda {{ESTADO}} restaurantes, hoteles, haciendas, jardines y empresas de catering. Consulta precio y pide presupuesto.";
            case "NOVIAS":
                return "<b>Guía para la Novia {{ESTADO}}:</b> Todo lo que necesitas para estar hermosa el día de tu boda. Tiendas de vestidos de novia, zapatos, complementos, centros de belleza, etc.";
            case "NOVIO":
                return "Guía para el Novio {{ESTADO}}: Todo lo que necesita el novio para verse guapo el día de su boda. Aquí encontrarás tiendas de trajes y complementos.";
            default:
                return "";
                break;
        }
    }

    public function exists($nombre)
    {
        foreach (Categoria::getCategorias() as $key => $c) {
            if (strtolower(str_sin_acentos($c->nombre)) == strtolower(str_sin_acentos($nombre)))
                return true;
        }
        return false;
    }

//0:No disponible,1:Descartado,2:Revisado;3:Presele4:Neg;5:Reserv
    public static function label_select_estado($estado)
    {
        switch ($estado) {
            case 0:
                return "No disponible";
            case 1:
                return "Descartado";
            case 2:
                return "Revisado";
            case 3:
                return "Preseleccionado";
            case 4:
                return "Negociando";
            case 5:
                return "Reservado";
        }
    }

}
