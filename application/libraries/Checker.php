<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cheker
 *
 * @author eli
 */
class Checker
{

    protected $CI;
    public static $ADMIN = 1;
    public static $NOVIO = 2;
    public static $PROVEEDOR = 3;
    public static $REDACTOR = 4;
    public static $ADMINAPP = 5;
    public static $MODERADOR = 6;
    public static $GALERIA = 7;
    public static $BLOG = 8;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('session');
        date_default_timezone_set('America/Mexico_City');
        setlocale(LC_TIME, 'es_ES.UTF-8');
        setlocale(LC_MONETARY, 'en_US');
        date_default_timezone_set('America/Mexico_City');

    }

    public function isLogin()
    {
        if ($this->CI->session->userdata("usuario")) {
            return true;
        }

        return false;
    }

    public function getUserName()
    {
        return $this->CI->session->userdata("nombre");
    }

    public function getIdRol()
    {
        return $this->CI->session->userdata("rol");
    }

    public function isBlogger()
    {
        switch ($this->CI->session->userdata("rol")) {
            case 1:
            case 4:
                return true;
        }

        return false;
    }

    public function isAdmin()
    {
        return $this->CI->session->userdata("rol") == 1;
    }

    public function isProveedor()
    {
        if ($this->CI->session->userdata("rol") == Checker::$PROVEEDOR) {
            return true;
        }

        return false;
    }

    public function isNovio()
    {
        if ($this->CI->session->userdata("rol") == Checker::$NOVIO) {
            return true;
        }

        return false;
    }

    public function isModerador()
    {
        if ($this->CI->session->userdata("rol") == Checker::$MODERADOR) {
            return true;
        }

        return false;
    }

    public function isGaleria()
    {
        if ($this->CI->session->userdata("rol") == Checker::$GALERIA) {
            return true;
        }

        return false;
    }

    public function isBlog()
    {
        if ($this->CI->session->userdata("rol") == Checker::$BLOG) {
            return true;
        }

        return false;
    }

    public function getMensajesNuevo()
    {
        $this->CI->load->model("Comentario_model", "comentario");
        $isAdmin = $this->CI->session->userdata("rol") == Checker::$ADMIN;

        return $this->CI->comentario->getCountNuevos(0, $isAdmin ? 0 : $this->CI->session->userdata("usuario"));
    }

    public function tipoProveedor()
    {
        $this->CI->load->model("proveedor/TipoProveedor_model", "tipo");
        $this->CI->load->model("proveedor/Proveedor_model", "proveedor");
        $empresa = $this->CI->proveedor->get(["id_usuario" => $this->CI->session->userdata("id_usuario")]);

        return $this->CI->tipo->get($empresa->id_tipo_proveedor);
    }

    public function getCliente()
    {
        if ($this->isNovio()) {
            $this->CI->load->model("Cliente_model", "cliente");

            return $this->CI->cliente->get(["id_usuario" => $this->CI->session->userdata("id_usuario")]);
        }

        return null;
    }

    public function getBoda()
    {
        if ($this->isNovio()) {
            $this->CI->load->model("Boda_model", "boda");

            return $this->CI->boda->get(["id_boda" => $this->CI->session->userdata("id_boda")]);
        }

        return null;
    }

    public function getTareasCompletadas()
    {
        if ($this->isNovio()) {
            $this->CI->load->model('Tarea_model', 'tarea');

            return $this->CI->tarea->getTotalCompletadas($this->CI->session->userdata('id_boda'));
        }

        return null;
    }

    public function getInvitadosConfirmados()
    {
        if ($this->isNovio()) {
            $this->CI->load->model('Invitados/Invitado_model', 'invitado_checker');

            return $this->CI->invitado_checker->getConfirmados();
        }

        return null;
    }

    public function getProveedoresReservados()
    {
        if ($this->isNovio()) {
            $this->CI->load->model('Proveedor_model', 'proveedor');

            return $this->CI->proveedor->getCountReservados($this->CI->session->userdata('id_boda'));
        }

        return null;
    }

    public function numSolicitudes()
    {
        $this->CI->load->model("Correo/Correo_model", "correo");
        $id = $this->CI->session->id_usuario;

        if ($this->isNovio() || $this->isProveedor()) {
            return $this->CI->correo->getCount($id, 0);
        }

        return 0;
    }

    public function hasFb()
    {
        if ($this->isLogin()) {
            if ($this->isProveedor()) {
                $this->CI->load->model("Proveedor_model", "proveedor");

                return $this->CI->proveedor->get($this->CI->session->userdata("id_proveedor"))->id_facebook ? true : false;
            } elseif ($this->isNovio()) {
                $this->CI->load->model("Cliente_model", "cliente");

                return $this->CI->proveedor->get($this->CI->session->userdata("id_cliente"))->id_facebook ? true : false;
            }
        }

        return false;
    }

    public function rand($len)
    {
        $str = '';
        $a   = "abcdefghijklmnopqrstuvwxyz0123456789";
        $b   = str_split($a);
        for ($i = 1; $i <= $len; $i++) {
            $str .= $b[rand(0, strlen($a) - 1)];
        }

        return $str;
    }

    public function getRol()
    {
        switch ($this->CI->session->userdata("rol")) {
            case 1:
                return "Administrador";
            case 2:
                return "Novio";
            case 3:
                return "Proveedor";
            case 4:
                return "Redactor";
            case 5:
                return "Admin App";
            case 6:
                return "Moderador";
            case 7:
                return "Galeria";
            case 8:
                return "Blog";
        }
    }

}
