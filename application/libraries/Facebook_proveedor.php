<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Autoload the required files
require_once( APPPATH . '../vendor/facebook/php-sdk-v4/autoload.php' );

// Make sure to load the Facebook SDK for PHP via composer or manually

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;

//use Facebook\ as FB;
// add other classes you plan to use, e.g.:
// use Facebook\FacebookRequest;
// use Facebook\GraphUser;
// use Facebook\FacebookRequestException;

class Facebook_proveedor {

    var $ci;
    var $session = false;
    var $fb;

    public function __construct() {
        // Get CI object.
        $this->ci = & get_instance();
        // Initialize the SDK
//        $this->fb = new FacebookFB([
//            'app_id' => $this->ci->config->item('api_id', 'facebook'), // Replace {app-id} with your app id
//            'app_secret' => $this->ci->config->item('app_secret', 'facebook'),
//            'default_graph_version' => 'v2.2',
//        ]);
        FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook_proveedor'), $this->ci->config->item('app_secret', 'facebook_proveedor'));
    }

    /**
     * Returns the login URL.
     */
    public function login_url() {
        $helper = new FacebookRedirectLoginHelper($this->ci->config->item('redirect_url', 'facebook_proveedor'));
        return $helper->getLoginUrl($this->ci->config->item('permissions', 'facebook'));
    }

    public function link_url() {
        $helper = new FacebookRedirectLoginHelper($this->ci->config->item('link_facebook', 'facebook_proveedor'));
        return $helper->getLoginUrl($this->ci->config->item('permissions', 'facebook'));
    }

    public function getAccessTokenLink() {
        $t = $this->ci->session->userdata('fb_token');
        if (!$t) {
            $helper = new FacebookRedirectLoginHelper($this->ci->config->item('link_facebook', 'facebook_proveedor'));
            try {
                $token = $helper->getSessionFromRedirect()->getToken();
                $this->ci->session->set_userdata('fb_token', $token);
                return $token;
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            return null;
        }
    }

    public function getAccessToken() {
        $t = $this->ci->session->userdata('fb_token');
        if (!$t) {
            $helper = new FacebookRedirectLoginHelper($this->ci->config->item('redirect_url', 'facebook_proveedor'));
            try {
                $token = $helper->getSessionFromRedirect()->getToken();
                $this->ci->session->set_userdata('fb_token', $token);
                return $token;
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            return null;
        }
    }

    public function getLocation($id) {
//        $this->get_session();
//        try {
        $request = ( new FacebookRequest($this->session, 'GET', "/$id?fields=location,name"))->execute();
        $loc = $request->getGraphObject()->asArray();
        return $loc;
//        } catch (Exception $e) {
//            return false;
//        }
    }

    /**
     * Get FB session.
     */
    public function get_session() {
        if ($this->ci->session->userdata('fb_token')) {
            $this->session = new FacebookSession($this->ci->session->userdata('fb_token'));

//            try {
            if (!$this->session->validate()) {
                $this->session = false;
            }
//            } catch (Exception $e) {
//                var_dump($e);
//                $this->session = false;
//            }
        } else {
            // Add `use Facebook\FacebookRedirectLoginHelper;` to top of file
            $helper = new FacebookRedirectLoginHelper($this->ci->config->item('redirect_url', 'facebook_proveedor'));
            try {
                $this->session = $helper->getSessionFromRedirect();
            } catch (FacebookRequestException $ex) {
                print_r($ex->getResponse());
            } catch (\Exception $ex) {
                print_r($ex->getResponse());
            }
        }
    }

    //GET SESSION PARA OBTENER USUARIO
    public function get_session_user() {
        if ($this->ci->session->userdata('fb_token')) {
            $this->session = new FacebookSession($this->ci->session->userdata('fb_token'));

//            try {
            if (!$this->session->validate()) {
                $this->session = false;
            }
//            } catch (Exception $e) {
//                var_dump($e);
//                $this->session = false;
//            }
        } else {
            // Add `use Facebook\FacebookRedirectLoginHelper;` to top of file
            $helper = new FacebookRedirectLoginHelper($this->ci->config->item('link_facebook', 'facebook_proveedor'));
            try {
                $this->session = $helper->getSessionFromRedirect();
            } catch (FacebookRequestException $ex) {
                print_r($ex->getResponse());
            } catch (\Exception $ex) {
                print_r($ex->getResponse());
            }
        }
    }

    /**
     * Login functionality.
     */
    public function login() {
        $this->get_session();
        if ($this->session) {
            $this->ci->session->set_userdata('fb_token', $this->session->getToken());
            $user = $this->get_user();
            if ($user && !empty($user['email'])) {
                $result = $this->ci->user_model->get_user($user['email']);
                if (!$result) {
                    $this->ci->session->set_flashdata('fb_user', $user);
                    redirect(base_url('Registro'));
                } else {
                    if ($this->ci->user_model->sign_in($result->username, $result->password)) {
                        redirect(base_url('Home'));
                    } else {
                        die('ERROR1');
                        redirect(base_url('Cuenta'));
                    }
                }
            } else {
                die('ERROR2');
            }
        }
    }

    /**
     * Returns the current user's info as an array.
     */
    public function get_user() {
        $this->get_session();
        if ($this->session) {
            $request = ( new FacebookRequest($this->session, 'GET', '/me?fields=id,name,email,gender'))->execute();
            $user = $request->getGraphObject()->asArray();
            return $user;
        }
        return false;
    }

    /**
     * Get user's profile picture.
     */
    public function get_profile_pic($user_id) {
        $this->get_session();
        if ($this->session) {
            $request = ( new FacebookRequest($this->session, 'GET', '/' . $user_id . '/picture?redirect=false&type=large'))->execute();
            $pic = $request->getGraphObject()->asArray();

            if (!empty($pic) && !$pic['is_silhouette']) {
                return $pic['url'];
            }
        }
        return false;
    }

}
