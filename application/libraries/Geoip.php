<?php

(defined('BASEPATH') OR defined('SYSPATH')) or die('No direct access allowed.');

class Geoip
{

    //CI
    protected $CI;
    protected $_gi;
    //CLASS
    protected $_Ip;
    protected $_Data;
    protected $_key = "48e9a12e7c98df764fca3b69ec790aa799bcf34dc14e42aa2925ef870fd5d22b";

    function __construct()
    {
        if ( ! isset($this->CI)) {
            $this->CI = &get_instance();
        }
    }

    function __destruct()
    {
        if (isset($this->_gi)) {
            geoip_close($this->_gi);
        }
    }

    function geolocalization($ip = null)
    {
        if ($this->_Set_IP($ip)) {
            $this->_Data = json_decode(file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key=$this->_key&ip=$this->_Ip&format=json"));

            return $this->_Data;
        } else {
            return false;
        }
    }

    function info()
    {
        return $this->_Data;
    }

    private function _Set_IP($ip = null)
    {
        if ($ip == null) {
            $ip = $this->CI->input->ip_address();
        }
        $this->_Ip = $ip;

        return $this->CI->input->valid_ip($this->_Ip);
    }

}
