<?php

if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class My_PHPMailer
{

    private $CI;
    private $to;

    public function __construct()
    {
        require_once('PHPMailer/PHPMailerAutoload.php');
        $this->CI = &get_instance();
    }

    public function to($email, $nombre)
    {
        $this->to = [
            "email"  => $email,
            "nombre" => $nombre,
        ];
    }

    public function setSubject($subject)
    {
        setlocale(LC_ALL, "en_US.utf8");
        $this->subject = iconv('UTF-8', 'ASCII//TRANSLIT', $subject);

    }

    public function setAttachment($attach, $name)
    {

        $this->attach = $attach;
        $this->name_file = $name;
    }

    public function send($mensaje)
    {
        $config = $this->CI->config->item('email');
        try {
            $mail = new PHPMailer();
            $mail->isSMTP();
            if (key_exists("SMTPAuth", $config)) {
                $mail->SMTPAuth = $config["SMTPAuth"];
            }
            if (key_exists("SMTPSecure", $config)) {
                $mail->SMTPSecure = $config["SMTPSecure"];
            }
            //$mail->SMTPDebug = 3;
            $mail->Host     = $config["Host"];
            $mail->Port     = $config["Port"];
            $mail->Username = $config["Username"];
            $mail->Password = $config["Password"];
            $mail->setFrom($config["Username"], 'BrideAdvisor');
            if (is_array($this->to["email"])) {
                for ($i = 0; $i < count($this->to["email"]); $i++) {
                    $mail->addAddress($this->to["email"][$i], $this->to["nombre"][$i]);
                }
            } else {
                $mail->addAddress($this->to["email"], $this->to["nombre"]);
            }
            if(isset($this->attach)){
                $mail->addAttachment($this->attach,$this->name_file);
            }
            $mail->Subject = $this->subject;
            $mail->msgHTML($mensaje);

            return $mail->send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
