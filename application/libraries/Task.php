<?php

class Task
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('session');
        $this->CI->load->model("Tarea_model", "tarea");
        setlocale(LC_TIME, 'es_ES.UTF-8');
    }

    public function getTotal()
    {
        $boda = $this->CI->session->userdata("id_boda");
        return $this->CI->tarea->getTotal($boda);
    }

    public function getTotalCompletadas()
    {
        $boda = $this->CI->session->userdata("id_boda");
        return $this->CI->tarea->getTotalCompletadas($boda);
    }

    public function categoriaNombre($categoria)
    {
        if ($categoria) {
            try {
                $c = Categoria::getCategorias();
                if (count($c) >= $categoria) {
                    return $c[$categoria - 1]->nombre;
                }
            } catch (Exception $e) {

            }
        }
        return "Por Definir";
    }

    public function timepoToString($timepo)
    {
        switch ($timepo) {
            case 1:
                return 'De 10 a 12 meses';
            case 2:
                return 'De 7 a 9 meses';
            case 3:
                return 'De 4 a 6 meses';
            case 4:
                return 'De 2 a 3 meses';
            case 5:
                return 'Ultimo mes';
            case 6:
                return 'Ultimas 2 semana';
            case 7:
                return 'Ultima semana';
            case 8:
                return 'Ultimo Día';
            case 9:
                return 'Despues de la boda';
        }
        return 'S/N';
    }

    public function timepoRelativo($tiempo)
    {
        $boda = $this->CI->session->userdata("id_boda");
        $fecha = $this->CI->tarea->getFechaBoda($boda);
        $d = new DateTime(($fecha));
        $i = new DateInterval('P0D');
        switch ($tiempo) {
            case 1:
                $i = new DateInterval('P10M');
                break;
            case 2:
                $i = new DateInterval('P7M');
                break;
            case 3:
                $i = new DateInterval('P4M');
                break;
            case 4:
                $i = new DateInterval('P2M');
                break;
            case 5:
                $i = new DateInterval('P1M');
                break;
            case 6:
                $i = new DateInterval('P14D');
                break;
            case 7:
                $i = new DateInterval('P7D');
                break;
            case 8:
                $i = new DateInterval('P1D');
                break;
        }
        $d->sub($i);
        return "Antes de " . strftime("%B", $d->getTimestamp());
    }

}
