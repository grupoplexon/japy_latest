<?php

class AdminAuthMiddleware
{
    protected $controller;
    protected $ci;

    public function __construct($controller, $ci)
    {
        $this->controller = $controller;
        $this->ci         = $ci;
        $controller->load->helper('url');
    }

    public function run()
    {
        if ( ! isset($this->ci->session->userdata()['rol']) || $this->ci->session->userdata()['rol'] != 1) {
            session_destroy();
            redirect('/', 'refresh');
        }
    }
}