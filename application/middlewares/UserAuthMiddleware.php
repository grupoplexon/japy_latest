<?php

class UserAuthMiddleware
{
    protected $controller;
    protected $ci;

    public function __construct($controller, $ci)
    {
        $this->controller = $controller;
        $this->ci         = $ci;
        $controller->load->helper("url");
        $controller->load->model("Usuario_model", "Usuario");
        $controller->load->model("Presupuesto_model", "Presupuesto");
    }

    public function run()
    {
        if (isset($this->ci->session->userdata()["id_usuario"])) {
            $user = $this->controller->Usuario->get($this->ci->session->userdata()["id_usuario"]);

            if ( ! $user || $user->rol != 2 || ! $user->activo) {
                session_destroy();
                redirect("/cuenta", "refresh");
            }

        } else {
            session_destroy();
            redirect("/cuenta", "refresh");
        }
    }
}