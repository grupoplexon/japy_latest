<?php

class GaleryAlbum_model extends MY_Model {
    public function __construct() {
        parent::__construct();
        $this->tabla = "galery_album";
        $this->id_tabla = "id_album";
        $this->load->database();
        $this->load->library('encrypt');
    }
	
	function countAlbumes($id) {
        $sql = "SELECT * FROM galery_album WHERE id_usuario = $id";
        $result = $this->db->query($sql);
        $result = $result->result();        
        return count($result);
    }
	
	function getAlbumes($id, $inicio) {
        $sql = "SELECT * FROM galery_album WHERE id_usuario = $id order by id_album desc LIMIT $inicio, 15";
        $result = $this->db->query($sql);
        $result = $result->result();        
        return $result;
    }
	
	function getAllFotos($id) {
        $sql = "SELECT * FROM galery_photo WHERE id_album=$id";
        $result = $this->db->query($sql);
        $result = $result->result();
		
			return $result;
    }
	
	function getAlbum($id, $id_usuario) {
        $sql = "SELECT * FROM galery_album WHERE id_album=$id and id_usuario = $id_usuario";
        $result = $this->db->query($sql);
        $result = $result->result();
		if(isset($result[0])){			
        return $result[0];}
		else{
			return $result;
		}
    }
	
	function getFotoPrincipal($id_album) {
        $sql = "SELECT * FROM galery_photo WHERE id_album = $id_album AND tipo=1";
        $result = $this->db->query($sql);
        $result = $result->result();
		if(isset($result[0])){			
        return $result[0];}
		else{
			$sql = "SELECT * FROM galery_photo WHERE id_album = $id_album";
			$result = $this->db->query($sql);
			$result = $result->result();
			if(isset($result[0])){			
				return $result[0];
			}
			else{
				return $result;
			}
		}
    }
	
	function getFotos($id_album, $principal) {
        $sql = "SELECT * FROM galery_photo WHERE id_album = $id_album AND tipo=0 and id_photo!=$principal order by id_photo desc limit 3";
        $result = $this->db->query($sql);
        $result = $result->result();        
        return $result;
    }
	
	function addAlbum($id_usuario, $nombre, $descripcion) {
        $sql = "INSERT INTO galery_album set nombrealbum='$nombre', descripcion='$descripcion', id_usuario=$id_usuario";
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();      
        return $insert_id;
    }
	
	function editAlbum($id, $nombre, $descripcion) {
        $sql = "UPDATE galery_album set nombrealbum='$nombre', descripcion='$descripcion' WHERE id_album=$id";
        $result = $this->db->query($sql);
              
        return $result;
    }
}