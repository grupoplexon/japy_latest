<?php

class GaleryComment_model extends MY_Model {
    public function __construct() {
        parent::__construct();
        $this->tabla = "galery_coment";
        $this->id_tabla = "id_comentario";
        $this->load->database();
        $this->load->library('encrypt');
    }
	
	function getAllComents($id,$inicio) {
        $sql = "SELECT galery_coment.*, usuario.id_usuario, usuario.mime, usuario.usuario FROM galery_coment join usuario on usuario.id_usuario = galery_coment.id_usuario WHERE id_album = $id and parent is null order by id_comentario desc limit $inicio, 10";
        $result = $this->db->query($sql);
        $result = $result->result();        
        return $result;
    }
	
	function countComents($id) {
        $sql = "SELECT galery_coment.*, usuario.id_usuario, usuario.mime, usuario.usuario FROM galery_coment join usuario on usuario.id_usuario = galery_coment.id_usuario WHERE id_album = $id and parent is null";
        $result = $this->db->query($sql);
        $result = $result->result();        
        return count($result);
    }
	
	function getAllComentsComents($id) {
        $sql = "SELECT galery_coment.*, usuario.id_usuario, usuario.mime, usuario.usuario FROM galery_coment join usuario on usuario.id_usuario = galery_coment.id_usuario WHERE id_album = $id and parent is not null";
        $result = $this->db->query($sql);
        $result = $result->result();        
        return $result;
    }
	
	function getComent($id) {
        $sql = "SELECT galery_coment.*, usuario.id_usuario, usuario.mime, usuario.usuario FROM galery_coment join usuario on usuario.id_usuario = galery_coment.id_usuario WHERE parent = $id";
        $result = $this->db->query($sql);
        $result = $result->result();        
        return $result;
    }
	
	function desactivarComentario($id_album, $value, $comentario){
			if($value==1){
				$result = $this->db->query("UPDATE galery_coment SET activo = 0 WHERE id_album = $id_album AND id_comentario = $comentario");
            }
			else{
				$result = $this->db->query("UPDATE galery_coment SET activo = 1 WHERE id_album = $id_album AND id_comentario = $comentario");
            }
			return $result;
        }
		
	function addComenario($album,$mensaje,$id_usuario){
			$sql = "INSERT INTO galery_coment set id_album='$album', id_usuario='$id_usuario', comentario='$mensaje'";
			$result2 =$this->db->query($sql);             
            $id_comentario = $this->db->insert_id();
            if($result2){
                 $sql = "SELECT galery_coment.*, usuario.id_usuario, usuario.mime, usuario.usuario FROM galery_coment join usuario on usuario.id_usuario = galery_coment.id_usuario WHERE id_comentario = $id_comentario
                    ";
            $result = $this->db->query($sql)->row();}
            return $result;
        }
		
		function respuestaComentario($album,$mensaje,$id_usuario, $respuesta){
			$sql = "INSERT INTO galery_coment set id_album='$album', id_usuario='$id_usuario', comentario='$mensaje', parent='$respuesta'";
			$result2 =$this->db->query($sql);             
            $id_comentario = $this->db->insert_id();
            if($result2){
                 $sql = "SELECT galery_coment.*, usuario.id_usuario, usuario.mime, usuario.usuario FROM galery_coment join usuario on usuario.id_usuario = galery_coment.id_usuario WHERE id_comentario = $id_comentario
                    ";
            $result = $this->db->query($sql)->row();}
            return $result;
        }
}