<?php

class GaleyLike_model extends MY_Model {
    public function __construct() {
        parent::__construct();
        $this->tabla = "galery_like";
        $this->id_tabla = "id_like";
        $this->load->database();
        $this->load->library('encrypt');
    }
}