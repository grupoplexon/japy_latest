<?php

class GaleryPhoto_model extends MY_Model {
    public function __construct() {
        parent::__construct();
        $this->tabla = "galery_photo";
        $this->id_tabla = "id_photo";
        $this->load->database();
        $this->load->library('encrypt');
    }
	
	public function setNewPrincipal($id, $id_album) {
        $logo = $this->get(array("id_album" => $id_album, "tipo" => 1));
        if ($logo) {
            $this->update($logo->id_photo, array("tipo" => 0));
        }
        return $this->update($id, array("tipo" => 1));
    }
}