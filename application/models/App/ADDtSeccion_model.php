<?php

class ADDtSeccion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getDtSeccion($id) {
        $id = decrypt($id);
        $this->db->where('id_dt_seccion', $id);
        $this->db->select('*');
        $datos = $this->db->get("dt_seccion");
        $datos = $datos->row();
        if(!empty($datos)){
            $datos->id_dt_seccion = encrypt($datos->id_dt_seccion);
            $datos->id_seccion = encrypt($datos->id_seccion);
            $datos->id_expo = encrypt($datos->id_expo);
        }
        return $datos;
    }

    function insertDtSeccion($data) {
        $this->db->trans_start();
        $data['id_expo'] = decrypt($data['id_expo']);
        $data['id_seccion'] = decrypt($data['id_seccion']);
        $this->db->insert('dt_seccion', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }

    function updateDtSeccion($data, $id) {
        $id = decrypt($id);
        $data['id_expo'] = decrypt($data['id_expo']);
        $data['id_seccion'] = decrypt($data['id_seccion']);
        $this->db->where('id_dt_seccion', $id);
        $this->db->update('dt_seccion', $data);
        return true;
    }

    function deleteDtSeccion($id_dt_seccion) {
        $id_dt_seccion = decrypt($id_dt_seccion);
        $this->db->where('id_dt_seccion', $id_dt_seccion);
        $this->db->delete('dt_seccion');
        return true;
    }

}
