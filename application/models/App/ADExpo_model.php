<?php

class ADExpo_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getAllExpos()
    {
        $sql = "SELECT * FROM expo";
        $result = $this->db->query($sql);
        $result = $result->result();
        if (count($result) > 0) {
            foreach ($result as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
            }
        }

        return $result;
    }

    function getExpo($id)
    {
        $id = decrypt($id);
        $sql = "SELECT * FROM expo  WHERE id_expo = $id";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $result = $result->row_array(0);
            $result['id_expo'] = encrypt($result['id_expo']);
            return $result;
        }
        return false;
    }

    function getExpositores($id)
    {
        $id = decrypt($id);
        $sql = "SELECT * FROM expositor  WHERE id_expo = $id limit 10";
        $result = $this->db->query($sql)->result();
        //return $result;
        if ($result) {
            foreach ($result as $key => $value) {
                $value->id_expositor = encrypt($value->id_expositor);
            }
            return $result;
        }
        return false;
    }

    function getCountExpositores($id)
    {
        $id = decrypt($id);
        $sql = "SELECT * FROM expositor  WHERE id_expo = $id";
        $result = $this->db->query($sql)->result();
        //return $result;

        return $result;

    }

    function getExpoA($id)
    {
        $id = decrypt($id);
        $sql = "SELECT * FROM expo  WHERE id_expo = $id";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $result = $result->row_array(0);
            $result['id_expo'] = encrypt($result['id_expo']);
            return $result;
        }
        return false;
    }


    function updateExpo($data, $id)
    {
        $id = decrypt($id);
        $this->db->where('id_expo', $id);
        $this->db->update('expo', $data);
        return true;
    }

    function deleteExpo($id)
    {
        $id = decrypt($id);
        $sql = "UPDATE expo SET activo = 0 WHERE id_expo = $id";
        $result = $this->db->query($sql);
        return true;
    }

    function insertExpo($data)
    {
        $this->db->trans_start();
        $this->db->insert('expo', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }

}
