<?php

class ADExpositor_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getExpositor($id)
    {
        $id = decrypt($id);
        $this->db->where('id_expositor', $id);
        $this->db->select('*');
        $datos = $this->db->get("expositor");
        $datos = $datos->row();
        if (!empty($datos)) {
            $datos->id_expositor = encrypt($datos->id_expositor);
            $datos->id_expo = encrypt($datos->id_expo);
        }
        return $datos;
    }

    function getAllExpo($id_expo)
    {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT expositor.* "
            . "FROM expositor "
            . "WHERE id_expo = $id_expo "
            . "ORDER BY id_expositor ASC";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if (count($datos) > 0) {
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_expositor = encrypt($value->id_expositor);
            }
        }

        return $datos;
    }

    function getAll()
    {
        $sql = "SELECT programa.*, expo.nombre "
            . "FROM programa LEFT JOIN expo USING(id_expo)";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if (count($datos) > 0) {
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_programa = encrypt($value->id_programa);
            }
        }

        return $datos;
    }

    function insertExpositor($data)
    {
        $this->db->trans_start();
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->insert('expositor', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }

    function updateExpositor($data, $id)
    {
        $id = decrypt($id);
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->where('id_expositor', $id);
        $this->db->update('expositor', $data);
        return true;
    }

    function deleteExpositor($id_expositor)
    {
        $id_expositor = decrypt($id_expositor);
        $this->db->where('id_expositor', $id_expositor);
        $this->db->delete('expositor');
        return true;
    }
}