<?php

class ADGanador_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function getGanador($id) {
        $id = decrypt($id);
        $this->db->where('id_ganador', $id);
        $this->db->select('*');
        $datos = $this->db->get("ganador");
        $datos = $datos->row();
        if(!empty($datos)){
            $datos->id_ganador = encrypt($datos->id_ganador);
            $datos->id_expo = encrypt($datos->id_expo);
        }
        return $datos;
    }
    
    function getGanadores($id_expo) {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT ganador.* "
                . "FROM ganador "
                . "WHERE id_expo = $id_expo "
                . "ORDER BY id_ganador ASC limit 10";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_ganador = encrypt($value->id_ganador);
            }
        }
        
        return $datos;
    }
    
    function getAllGanadores($id_expo) {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT ganador.* "
                . "FROM ganador where id_expo = $id_expo ";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_ganador = encrypt($value->id_ganador);
            }
        }
        
        return $datos;
    }
    
    function insertGanador($data) {
        $this->db->trans_start();
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->insert('ganador', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
    
    function updateGanador($data, $id) {
        $id = decrypt($id);
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->where('id_ganador', $id);
        $this->db->update('ganador', $data);
        return true;
    }

    function deleteGanador($id) {
        $id = decrypt($id);
        $this->db->where('id_ganador', $id);
        $this->db->delete('ganador');
        return true;
    }
}