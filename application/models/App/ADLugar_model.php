<?php

class ADLugar_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function getLugar($id) {
        $id = decrypt($id);
        $this->db->where('id_vivegdl', $id);
        $this->db->select('*');
        $datos = $this->db->get("vivegdl");
        $datos = $datos->row();
        if(count($datos)>0){
            $datos->id_vivegdl = encrypt($datos->id_vivegdl);
            $datos->id_expo = encrypt($datos->id_expo);
            $datos->parent = encrypt($datos->parent);
        }
        
        return $datos;
    }
    
    function getAllLugar($id_expo) {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT * "
                . "FROM vivegdl "
                . "WHERE parent is null and id_expo = $id_expo "
                . "ORDER BY id_vivegdl ASC limit 10";
        //print_r($sql);
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_vivegdl = encrypt($value->id_vivegdl);
                $value->parent = encrypt($value->parent);
            }
        }
        
        return $datos;
    }
    
    function getAll($id_expo) {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT * "
                . "FROM vivegdl "
                . "WHERE parent is null and id_expo = $id_expo "
                . "ORDER BY id_vivegdl ASC";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->hijos =  $this->getHijos($value->id_vivegdl);
                $value->id_expo = encrypt($value->id_expo);
                $value->id_vivegdl = encrypt($value->id_vivegdl);
                $value->parent = encrypt($value->parent);
            }
        }
        
        return $datos;
    }
    
    function getAllByLugar($id_lugar) {
        $id_lugar = decrypt($id_lugar);
        $sql = "SELECT * "
                . "FROM vivegdl "
                . "WHERE parent = $id_lugar "
                . "ORDER BY id_vivegdl ASC";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->hijos =  $this->getHijos($value->id_vivegdl);
                $value->id_expo = encrypt($value->id_expo);
                $value->id_vivegdl = encrypt($value->id_vivegdl);
                $value->parent = encrypt($value->parent);
            }
        }
        
        return $datos;
    }
    
    function getTitle($id_lugar) {
        $id_lugar = decrypt($id_lugar);
        $sql = "SELECT titulo_es "
                . "FROM vivegdl "
                . "WHERE id_vivegdl = $id_lugar "
                . "ORDER BY id_vivegdl ASC";
        //print_r($sql);
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            return $datos[0]->titulo_es;
        }
        
        return "";
    }
    
    function getId_expo($id_lugar) {
        $id_lugar = decrypt($id_lugar);
        $sql = "SELECT id_expo "
                . "FROM vivegdl "
                . "WHERE id_vivegdl = $id_lugar "
                . "ORDER BY id_vivegdl ASC";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            return encrypt($datos[0]->id_expo);
        }
        
        return "";
    }
    
    function getHijos($id){
        $sql = "SELECT * "
                . "FROM vivegdl "
                . "WHERE parent = $id "
                . "ORDER BY id_vivegdl ASC";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        return count($datos);
    }

    function insertLugar($data) {
        $this->db->trans_start();
        if(isset($data['id_expo']))
        $data['id_expo'] = decrypt($data['id_expo']);
        if(isset($data['parent']))
        $data['parent'] = decrypt($data['parent']);
        $this->db->insert('vivegdl', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
    
    function updateLugar($data, $id) {
        $id = decrypt($id);
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->where('id_vivegdl', $id);
        $this->db->update('vivegdl', $data);
        return true;
    }

    function deleteLugar($id) {
        $id = decrypt($id);
        $this->db->where('id_vivegdl', $id);
        $this->db->delete('vivegdl');
        return true;
    }
}