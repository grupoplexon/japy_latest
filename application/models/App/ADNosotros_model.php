<?php

class ADNosotros_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function getNosotros() {
        $sql = "SELECT * FROM nosotros  WHERE id_nosotros = 1";
        $result = $this->db->query($sql)->result();
        //return $result;
        if (isset($result) && count($result)>0) {            
            return $result[0];
        }
        return false;
    }

    function updateNosotros($data) {
        $this->db->where('id_nosotros', 1);
        $this->db->update('nosotros', $data);
        return true;
    }

}
