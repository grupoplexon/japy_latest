<?php

class ADNotificacion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function getNotificacion($id) {
        $id = decrypt($id);
        $this->db->where('id_notificacion', $id);
        $this->db->select('*');
        $datos = $this->db->get("notificacion");
        $datos = $datos->row();
        if(!empty($datos)){
            $datos->id_notificacion = encrypt($datos->id_notificacion);
            $datos->id_expo = encrypt($datos->id_expo);
        }
        return $datos;
    }
    
    function getAllExpo($id_expo) {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT notificacion.*, expo.nombre "
                . "FROM notificacion LEFT JOIN expo USING(id_expo) "
                . "WHERE id_expo = $id_expo or id_expo is null";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_notificacion = encrypt($value->id_notificacion);
                $value->id_expo = encrypt($value->id_expo);
            }
        }
        
        return $datos;
    }
    
    function getAll() {
        $sql = "SELECT notificacion.*, expo.nombre "
                . "FROM notificacion LEFT JOIN expo USING(id_expo)";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_notificacion = encrypt($value->id_notificacion);
                $value->id_expo = encrypt($value->id_expo);
            }
        }
        
        return $datos;
    }
    
    function insertNotificacion($data) {
        $this->db->trans_start();
        $data['id_expo'] = empty($data['id_expo'])? null:decrypt($data['id_expo']);
        $this->db->insert('notificacion', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }

}