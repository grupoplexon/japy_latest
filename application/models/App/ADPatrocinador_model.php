<?php

class ADPatrocinador_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function getPatrocinador($id) {
        $id = decrypt($id);
        $this->db->where('id_patrocinador', $id);
        $this->db->select('*');
        $datos = $this->db->get("patrocinador");
        $datos = $datos->row();
        if(!empty($datos)){
            $datos->id_patrocinador = encrypt($datos->id_patrocinador);
            $datos->id_expo = encrypt($datos->id_expo);
        }
        return $datos;
    }
    
    function getPatrocinadores($id_expo) {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT patrocinador.* "
                . "FROM patrocinador "
                . "WHERE id_expo = $id_expo "
                . "ORDER BY id_patrocinador ASC limit 10";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_patrocinador = encrypt($value->id_patrocinador);
            }
        }
        
        return $datos;
    }
    
    function getAllPatrocinadores($id_expo) {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT patrocinador.* "
                . "FROM patrocinador where id_expo = $id_expo";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_patrocinador = encrypt($value->id_patrocinador);
            }
        }
        
        return $datos;
    }
    
    function insertPatrocinador($data) {
        $this->db->trans_start();
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->insert('patrocinador', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
    
    function updatePatrocinador($data, $id) {
        $id = decrypt($id);
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->where('id_patrocinador', $id);
        $this->db->update('patrocinador', $data);
        return true;
    }

    function deletePatrocinador($id) {
        $id = decrypt($id);
        $this->db->where('id_patrocinador', $id);
        $this->db->delete('patrocinador');
        return true;
    }
}