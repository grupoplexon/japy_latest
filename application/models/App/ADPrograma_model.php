<?php

class ADPrograma_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function getPrograma($id) {
        $id = decrypt($id);
        $this->db->where('id_programa', $id);
        $this->db->select('*');
        $datos = $this->db->get("programa");
        $datos = $datos->row();
        if(!empty($datos)){
            $datos->id_programa = encrypt($datos->id_programa);
            $datos->id_expo = encrypt($datos->id_expo);
        }
        return $datos;
    }
    
    function getAllExpo($id_expo) {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT programa.*, expo.nombre "
                . "FROM programa LEFT JOIN expo USING(id_expo) "
                . "WHERE id_expo = $id_expo "
                . "ORDER BY fecha_i ASC limit 10";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_programa = encrypt($value->id_programa);
            }
        }
        
        return $datos;
    }
    
    function getAll() {
        $sql = "SELECT programa.*, expo.nombre "
                . "FROM programa LEFT JOIN expo USING(id_expo)";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_programa = encrypt($value->id_programa);
            }
        }
        
        return $datos;
    }
    
    function insertPrograma($data) {
        $this->db->trans_start();
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->insert('programa', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
    
    function updatePrograma($data, $id) {
        $id = decrypt($id);
        $data['id_expo'] = decrypt($data['id_expo']);
        $this->db->where('id_programa', $id);
        $this->db->update('programa', $data);
        return true;
    }

    function deletePrograma($id_programa) {
        $id_programa = decrypt($id_programa);
        $this->db->where('id_programa', $id_programa);
        $this->db->delete('programa');
        return true;
    }
}