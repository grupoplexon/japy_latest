<?php

class ADPublicidad_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getPublicidad($id)
    {
        $id = decrypt($id);
        $this->db->where('id_publicidad', $id);
        $this->db->select('*');
        $datos = $this->db->get("publicidad");
        $datos = $datos->row();
        if (!empty($datos)) {
            $datos->id_publicidad = encrypt($datos->id_publicidad);
        }
        return $datos;
    }

    function getCountPublicidad($id)
    {
        $id = decrypt($id);
        $sql = "SELECT * FROM publicidad WHERE id_expo = $id";
        $result = $this->db->query($sql)->result();
        //return $result;

        return $result;

    }

    function getAllExpo($id_expo)
    {
        $id_expo = decrypt($id_expo);
        $sql = "SELECT publicidad.*, expo.nombre "
            . "FROM publicidad LEFT JOIN expo USING(id_expo) "
            . "WHERE id_expo = $id_expo or id_expo is null limit 10";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if (count($datos) > 0) {
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_publicidad = encrypt($value->id_publicidad);
            }
        }

        return $datos;
    }

    function getAll()
    {
        $sql = "SELECT publicidad.*, expo.nombre "
            . "FROM publicidad LEFT JOIN expo USING(id_expo)";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if (count($datos) > 0) {
            foreach ($datos as $key => $value) {
                $value->id_expo = encrypt($value->id_expo);
                $value->id_publicidad = encrypt($value->id_publicidad);
            }
        }

        return $datos;
    }

    function insertPublicidad($data)
    {
        $this->db->trans_start();
        $data['id_expo'] = ($data['id_expo'] == '') ? '' : decrypt($data['id_expo']);
        $this->db->insert('publicidad', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }

    function updatePublicidad($data, $id_publicidad)
    {
        $id_publicidad = decrypt($id_publicidad);
        $data['id_expo'] = empty($data['id_expo']) ? null : decrypt($data['id_expo']);
        $this->db->where('id_publicidad', $id_publicidad);
        $this->db->update('publicidad', $data);
        return true;
    }

    function deletePublicidad($id_publicidad)
    {
        $id_publicidad = decrypt($id_publicidad);
        $this->db->where('id_publicidad', $id_publicidad);
        $this->db->delete('publicidad');
        return true;
    }
}