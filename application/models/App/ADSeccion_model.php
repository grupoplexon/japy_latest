<?php

class ADSeccion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function getSeccionesNombre($id_expo,$id_seccion){
        $id_expo = decrypt($id_expo);
        $id_seccion = decrypt($id_seccion);
        $sql = "SELECT dt_seccion.id_dt_seccion,dt_seccion.nombre "
                . "FROM dt_seccion INNER JOIN seccion USING(id_seccion) "
                . "WHERE id_expo = $id_expo AND id_seccion = $id_seccion";
        $datos = $this->db->query($sql);
        $datos = $datos->result();
        if(count($datos) > 0){
            foreach ($datos as $key => $value) {
                $value->id_dt_seccion = encrypt($value->id_dt_seccion);
            }
        }
        
        return $datos;
    }
    
    function getSeccion($id) {
        $id = decrypt($id);
        $this->db->where('id_seccion', $id);
        $this->db->select('*');
        $datos = $this->db->get("seccion");
        $datos = $datos->row();
        if(!empty($datos)){
            $datos->id_seccion = encrypt($datos->id_seccion);
        }
        return $datos;
    }
    
    function updateSeccion($data,$id){
        $id = decrypt($id);
        $this->db->where('id_seccion', $id);
        $this->db->update('seccion', $data);
        return true;
    }
}