<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Comentario_model
 *
 * @author Evolutel
 */
class Comentario_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_blog_comentario";
        $this->tabla    = "blog_comentario";
    }

    public function countPost($id_post)
    {
        $sql   = "select COUNT( DISTINCT id_blog_comentario) as total from blog_post inner join blog_comentario using(id_blog_post) where id_blog_post=$id_post ";
        $datos = $this->db->query($sql);

        return $datos->row()->total;
    }

    public function getComentarios($id, $limit1 = null, $limit2 = null)
    {
        if (is_array($id)) {
            $this->db->where($id, $limit1, $limit2);
        } else {
            $this->db->where([$this->id_tabla => $id], $limit1, $limit2);
        }
        $this->db->order_by("id_blog_comentario", "desc");
        $query = $this->db->get($this->tabla);
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    public function getPostNewComentado($all = true, $autor)
    {
        if ($all) {
            $sql = " SELECT  blog_post.*,count(id_blog_comentario) AS comentarios
	FROM blog_post  INNER  JOIN blog_comentario USING(id_blog_post) 
	WHERE publicado=1 AND nuevo=1
	GROUP BY id_blog_post
	ORDER BY comentarios DESC";
        } else {
            $sql = " select  blog_post.*,count(id_blog_comentario) as comentarios
	from blog_post  inner  join blog_comentario using(id_blog_post) 
	where publicado=1 and nuevo=1 and blog_post.id_autor=$autor
	group by id_blog_post
	ORDER BY comentarios desc";
        }
        $datos = $this->db->query($sql);

        return $datos->result();
    }

    public function setComentariosLeidos($id_blog_post)
    {
        $sql = "update blog_comentario set nuevo=0 where id_blog_post=$id_blog_post";

        return $this->db->query($sql);
    }

    public function getCountNuevos($id_post = 0, $id_autor = 0)
    {
        $s = "";
        if ($id_autor != 0) {
            $s = " and id_autor=$id_autor ";
        }
        if ($id_post == 0) {
            $sql = "select COUNT( id_blog_comentario) as total from  blog_comentario inner join blog_post using(id_blog_post) where nuevo=1 $s ";
        } else {
            $sql = "select COUNT( id_blog_comentario) as total from  blog_comentario inner join blog_post using(id_blog_post) where nuevo=1 and id_blog_post=$id_post $s ";
        }
        $datos = $this->db->query($sql);

        return $datos->row()->total;
    }

    public function changeFav($id_post, $id_comment, $id_usuario)
    {
        try {
            $sql = "SELECT * FROM blog_comentario_fav f INNER JOIN blog_comentario c USING(id_blog_comentario)"
                    ." WHERE f.id_usuario=? AND id_blog_post=? AND id_blog_comentario=? ";
            $fav = $this->db->query($sql, [$id_usuario, $id_post, $id_comment])->row();
            if ($fav) {
                $this->db->delete('blog_comentario_fav',
                        ['id_usuario' => $id_usuario, "id_blog_comentario" => $id_comment]);
            } else {
                $this->db->insert("blog_comentario_fav",
                        ['id_usuario' => $id_usuario, "id_blog_comentario" => $id_comment]);
            }

            return true;
        } catch (Exception $ee) {
            return false;
        }
    }

    public function isFavorito($id_usuario, $id_blog_comentario)
    {
        $sql = "SELECT * FROM blog_comentario_fav WHERE id_usuario=? AND id_blog_comentario=? ;";

        return $this->db->query($sql, [$id_usuario, $id_blog_comentario])->row() ? true : false;
    }

    public function countFavoritos($id_blog_comentario)
    {
        $sql = "SELECT count(*) AS total FROM blog_comentario_fav WHERE id_blog_comentario=? ;";

        return $this->db->query($sql, [$id_blog_comentario])->row()->total;
    }

    public function countComentarios($id_blog_comentario)
    {
        $sql = "SELECT count(*) AS total FROM blog_comentario WHERE parent=? ;";

        return $this->db->query($sql, [$id_blog_comentario])->row()->total;
    }

}
