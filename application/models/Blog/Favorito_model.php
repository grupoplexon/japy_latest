<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Favorito_model
 *
 * @author Evolutel
 */
class Favorito_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->id_tabla = "id_blog_favorito";
        $this->tabla = "blog_favorito";
    }

    public function isFavorito($id, $id_blog) {
        return $this->get(array("id_usuario" => $id, "id_blog_post" => $id_blog));
    }

    public function countPost($id_post) {
        $sql = "select COUNT( DISTINCT id_blog_favorito) as total from blog_post inner join blog_favorito using(id_blog_post) where id_blog_post=$id_post ";
        $datos = $this->db->query($sql);
        return $datos->row()->total;
    }

}
