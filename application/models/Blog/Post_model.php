<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Post_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_blog_post";
        $this->tabla    = "blog_post";
    }

    public function countCat($id_categoria = 0)
    {
        if ($id_categoria != 0) {
            $sql = "SELECT count(*) as total FROM blog_post 
                    -- inner join blog_post_categoria using(id_blog_post) 
                    where tipo=$id_categoria";
        } else {
            $sql = "SELECT count(*) AS total FROM blog_post WHERE publicado=1";
        }
        $datos = $this->db->query($sql);

        return $datos->row()->total;
    }

    public function getAllRedactor($id_autor, $limit1, $limit2)
    {
        $sql   = "SELECT id_blog_post,titulo,fecha_creacion,id_autor,publicado,SUBSTRING(contenido,0,50) as contenido"
                ."  FROM blog_post where id_autor=$id_autor limit $limit1,$limit2";
        $datos = $this->db->query($sql);

        return $datos->result();
    }

    public function getListCategoria($cat, $limit1, $limit2)
    {
        if ($cat != 0) {
            $sql = "SELECT * FROM blog_post 
                    -- inner join blog_post_categoria using(id_blog_post) 
                    where tipo=$cat and publicado=1  
                    order by fecha_creacion desc  
                    limit $limit1 offset $limit2";
        } else {
            $sql = "SELECT * FROM blog_post 
                    where publicado = 1  
                    order by fecha_creacion desc 
                    limit $limit1 offset $limit2 ";
        }

        $datos = $this->db->query($sql);

        return $datos->result();
    }

    public function getAutor($id_autor)
    {
        $this->db->where('id_usuario', $id_autor);
        $this->db->select('nombre,apellido,usuario');
        $datos = $this->db->get("usuario");

        return $datos->row();
    }

    public function getTags($id_post)
    {
        $sql   = "SELECT * FROM blog_post_categoria as pc inner join blog_categoria using(id_blog_categoria) where id_blog_post=$id_post";
        $datos = $this->db->query($sql);

        return $datos->result();
    }

    public function insertPOST($post)
    {
        $this->db->trans_begin();
        try {
            $b = $this->db->insert($this->tabla, $post);
        } catch (Exception $e) {
            $b = false;
        }
        if ($b) {
            $id = $this->last_id();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();

                return false;
            } else {
                $this->db->trans_commit();

                return $id;
            }
        } else {
            $this->db->trans_rollback();

            return false;
        }
    }

    public function getDestacados($limit = 10)
    {
        $sql        = "select  id_blog_post,id_autor,sum(fav) as fav,sum(comentarios) as comentarios,sum(total) as total from (
                        (select  id_blog_post,id_autor,count(id_blog_comentario) as comentarios, 0 as fav,count(id_blog_comentario) as total
                        from blog_post  left  join blog_comentario using(id_blog_post) 
                        where publicado=1
                        group by id_blog_post
                        ORDER BY total desc
                        limit $limit)
                        UNION
                        (select 
                        id_blog_post,id_autor,0  as comentarios,count(id_blog_favorito) as fav, count(id_blog_favorito) as total
                        from blog_post 
                         left   join blog_favorito using(id_blog_post)
                        where publicado=1
                        group by id_blog_post,id_autor
                        ORDER BY total desc
                        limit $limit)
                ) as totales group by id_blog_post, id_autor order by total desc, id_blog_post desc";
        $datos      = $this->db->query($sql);
        $destacados = $datos->result();
        if ($destacados) {
            foreach ($destacados as &$d) {
                $d->autor = $this->getAutor($d->id_autor);
                $d->post  = $this->get($d->id_blog_post);
            }
        }

        return $destacados;
    }

    public function getMasComentado()
    {
        $sql   = " SELECT  blog_post.*,count(id_blog_comentario) AS total
	FROM blog_post  INNER  JOIN blog_comentario USING(id_blog_post) 
	WHERE publicado=1
	GROUP BY id_blog_post
	ORDER BY total DESC
	LIMIT 10 ";
        $datos = $this->db->query($sql);

        return $datos->result();
    }

    public function updatePOST($id, $post)
    {
        $this->db->trans_begin();
        try {
            $this->db->where([$this->id_tabla => $id]);
            $b = $this->db->update($this->tabla, $post);
        } catch (Exception $e) {
            $b = false;
        }
        if ($b) {
            $this->db->query("DELETE FROM blog_post_categoria WHERE id_blog_post=$id");
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();

                return false;
            } else {
                $this->db->trans_commit();

                return $id;
            }
        } else {
            $this->db->trans_rollback();

            return false;
        }
    }

}
