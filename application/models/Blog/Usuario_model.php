<?php

class Usuario_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_usuario";
        $this->tabla    = "usuario";
    }

    public function get($id)
    {
        if (is_array($id)) {
            $id["rol"] = 4;
            $query     = $this->db->get_where($this->tabla, $id);
        } else {
            $query = $this->db->get_where($this->tabla, [$this->id_tabla => $id]);
        }
        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    public function exits($id)
    {
        if (is_array($id)) {
            $query = $this->db->get_where($this->tabla, $id);
        } else {
            $query = $this->db->get_where($this->tabla, [$this->id_tabla => $id]);
        }
        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return false;
    }

    public function getList($id, $limit1 = null, $limit2 = null)
    {
        if (is_array($id)) {
            $id["rol"] = 4;
            $query     = $this->db->get_where($this->tabla, $id, $limit1, $limit2);
        } else {
            $query = $this->db->get_where($this->tabla, [$this->id_tabla => $id, "rol" => 4], $limit1, $limit2);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    public function getAll($columns = "*", $limit1 = null, $limit2 = null)
    {
        if ($limit2 == null && $limit1 != null) {
            $this->db->select($columns)->from($this->tabla)->where(["rol" => 4])->limit($limit1);
        } elseif ($limit1 != null && $limit2 != null) {
            $this->db->select($columns)->from($this->tabla)->where(["rol" => 4])->limit($limit1, $limit2);
        } else {
            $this->db->select($columns)->from($this->tabla)->where(["rol" => 4]);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return [];
    }

    public function count($column = "*")
    {
        if ($column == "*") {
            $column = $this->id_tabla;
        }
        $sql    = "select COUNT(DISTINCT $column ) as total from $this->tabla where rol=4";
        $result = $this->db->query($sql)->row();

        return $result->total;
    }

    public function update($id, $datos)
    {
        try {
            $this->db->where([$this->id_tabla => $id, "rol" => 4]);

            return $this->db->update($this->tabla, $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    public function insert($datos)
    {
        $datos["rol"] = 4;

        return parent::insert($datos);
    }

    public function getCliente($id_usuario)
    {
        try {
            $sql    = "SELECT cliente.* FROM cliente WHERE id_usuario = $id_usuario";
            $result = $this->db->query($sql);

            return $result->row();
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        return false;
    }

    function getClienteFB($id_fb, $correo)
    {
        $sql    = "SELECT cliente.* FROM cliente inner join usuario using(id_usuario) WHERE id_facebook = $id_fb or correo='$correo'; ";
        $result = $this->db->query($sql);

        return $result->row();
    }

    function getClienteFacebook($id_fb)
    {
        $sql    = "SELECT cliente.* FROM cliente WHERE id_facebook = $id_fb ; ";
        $result = $this->db->query($sql);

        return $result->row();
    }

    function existeCorreo($correo)
    {
        return $this->get(["usuario" => $correo]) ? true : false;
    }

}
