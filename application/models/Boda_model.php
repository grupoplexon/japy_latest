<?php

class Boda_model extends MY_Model
{
    //0:No disponible,1:Descartado,2:Revisado;3:Presele4:Neg;5:Reserv
    public static $ESTADO_DISPONIBLE = 0;
    public static $ESTADO_DESCARTADO = 1;
    public static $ESTADO_REVISADO = 2;
    public static $ESTADO_PRESELECCIONADO = 3;
    public static $ESTADO_NEGOCIANDO = 4;
    public static $ESTADO_RESERVADO = 5;

    public function __construct()
    {
        parent::__construct();
        $this->tabla = "boda";
        $this->id_tabla = "id_boda";
        $this->load->database();
        $this->load->library('encrypt');
    }

    function insertBoda($data)
    {
        $this->db->trans_start();
        $this->db->insert('boda', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }

    function getBudget($id_boda){
        $sql = "SELECT presupuesto FROM boda where id_boda=$id_boda";
        return $this->db->query($sql)->row()->presupuesto;
    }

    function getPresupuesto($id_boda)
    {
        $sql = "SELECT sum(costo_aproximado) as presupuesto FROM boda inner join presupuesto using(id_boda) where id_boda=$id_boda ;";
        return $this->db->query($sql)->row()->presupuesto;
    }

    function getTareasPendientes($id_boda)
    {
        $sql = "SELECT count(id_tarea) as pendientes FROM boda inner join tarea using(id_boda) where id_boda=$id_boda and completada=0 ;";
        return $this->db->query($sql)->row()->pendientes;
    }

    function getProveedores($id_boda, $cat = NULL)
    {
        if ($cat == NULL) {
            $sql = "SELECT * FROM proveedor_boda INNER JOIN proveedor USING(id_proveedor) INNER JOIN tipo_proveedor USING(id_tipo_proveedor) WHERE id_boda=?";
            return $this->db->query($sql, array($id_boda))->result();
        } else {
            $sql = "SELECT * FROM proveedor_boda INNER JOIN proveedor USING(id_proveedor) INNER JOIN tipo_proveedor USING(id_tipo_proveedor) WHERE id_boda=? AND tag_categoria=? ";
            return $this->db->query($sql, array($id_boda, $cat))->result();
        }
    }

    function updateProveedor($id_boda, $id_proveedor, $estado)
    {
        $sql = "UPDATE  proveedor_boda SET estado=? WHERE id_boda=? AND id_proveedor=? ;";
        return $this->db->query($sql, array($estado, $id_boda, $id_proveedor));
    }

    function updateProveedorPrecio($id_boda, $id_proveedor, $precio)
    {
        $sql = "UPDATE  proveedor_boda SET precio=? WHERE id_boda=? AND id_proveedor=? ;";
        return $this->db->query($sql, array($precio, $id_boda, $id_proveedor));
    }

    function updateProveedorCal($id_boda, $id_proveedor, $cal)
    {
        $sql = "UPDATE  proveedor_boda SET calificacion=? WHERE id_boda=? AND id_proveedor=? ;";
        return $this->db->query($sql, array($cal, $id_boda, $id_proveedor));
    }

    function updateProveedorResena($id_boda, $id_proveedor, $data)
    {
        $this->db->where('id_boda', $id_boda);
        $this->db->where('id_proveedor', $id_proveedor);
        return $this->db->update('proveedor_boda', $data);
    }

    function removeProveedor($id_boda, $id_proveedor)
    {
        $sql = "DELETE FROM proveedor_boda WHERE id_boda=? AND id_proveedor=? ;";
        return $this->db->query($sql, array($id_boda, $id_proveedor));
    }

    function insertProveedorBoda($id_boda, $id_proveedor)
    {
        return $this->db->insert('proveedor_boda', array("id_boda" => $id_boda, "id_proveedor" => $id_proveedor));
    }

    function countProveedores($id_boda)
    {
        $sql = "SELECT count(*) AS total FROM proveedor_boda WHERE id_boda=? ;";
        return $this->db->query($sql, array($id_boda))->row()->total;
    }

    function countProveedoresEstado($id_boda, $estado)
    {
        $sql = "SELECT count(*) AS total FROM proveedor_boda WHERE estado=? AND id_boda=? ;";
        return $this->db->query($sql, array($estado, $id_boda))->row()->total;
    }

    function countCategoria($id_boda, $categoria)
    {
        $sql = "SELECT COUNT(*) AS total FROM proveedor_boda"
            . " INNER JOIN proveedor USING(id_proveedor)"
            . " INNER JOIN tipo_proveedor USING(id_tipo_proveedor)"
            . " WHERE id_boda=? AND tag_categoria = ? ;";
        return $this->db->query($sql, array($id_boda, $categoria))->row()->total;
    }

    function getProveedorCategoria($id_boda, $categoria)
    {
        $sql = "SELECT proveedor.*,proveedor_boda.estado, tipo_proveedor.nombre_tipo_proveedor,tag_categoria
                FROM  proveedor_boda
                INNER JOIN proveedor USING (id_proveedor)
                INNER JOIN tipo_proveedor USING (id_tipo_proveedor)
                WHERE tag_categoria = '$categoria'
                  AND id_boda = $id_boda
                ORDER BY estado DESC
                LIMIT 1";
        return $this->db->query($sql)->row();
    }

    function getProveedorCategoriaMini($id_boda, $categoria)
    {
        $sql = "SELECT id_proveedor,nombre,proveedor_boda.estado, nombre_tipo_proveedor
                FROM  proveedor_boda
                INNER JOIN proveedor USING (id_proveedor)
                INNER JOIN tipo_proveedor USING (id_tipo_proveedor)
                WHERE tag_categoria = ?
                  AND id_boda = ?
                ORDER BY estado DESC
                LIMIT 1";
        return $this->db->query($sql, array($categoria, $id_boda))->row();
    }

}
