<?php

class Cliente_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla    = "cliente";
        $this->id_tabla = "id_cliente";
        $this->load->database();
        $this->load->library('encrypt');
    }

    function insertCliente($data)
    {
        $this->db->trans_start();
        $this->db->insert('cliente', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();

        return $id;
    }

    public function clientes12meses()
    {
        $sql = "select CONCAT(LPAD(MONTH(fecha_creacion), 2, '0'),'-',YEAR(fecha_creacion)) as mes,count(id_usuario) as clientes ".
                "from usuario ".
                "where rol=2 and fecha_creacion >= date_sub(curdate(), interval 12 month)  ".
                "group by CONCAT(YEAR(fecha_creacion),MONTH(fecha_creacion));";

        return $this->db->query($sql)->result();
    }

    public function proveedores12meses()
    {
        $sql = "select CONCAT(LPAD(MONTH(fecha_creacion), 2, '0'),'-',YEAR(fecha_creacion)) as mes,count(id_usuario) as proveedores ".
                "from usuario ".
                "where rol=3 and fecha_creacion >= date_sub(curdate(), interval 12 month)  ".
                "group by CONCAT(YEAR(fecha_creacion),MONTH(fecha_creacion));";

        return $this->db->query($sql)->result();
    }

    public function setLocation($id_user,$data){
        try {
            $sql = "SELECT id_cliente FROM cliente where id_usuario = $id_user";
            $client = $this->db->query($sql)->result();
            $this->db->where('id_cliente',$client[0]->id_cliente);
            return $this->db->update('cliente', $data);
        } catch (Exception $e) {
            return false;
        }
    }

}
