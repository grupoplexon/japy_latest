<?php
    class Comunidad_model extends CI_Model{
        
        public function __construct() {
            parent::__construct();
            $this->load->database();
        }
        
        function getDebate($id_debate){
            $result = "";
            if(!empty($id_debate)){
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT
                            G.nombre,
                            G.imagen,
                            G.id_grupos_comunidad,
                            D.id_usuario,
                            D.titulo_debate,
                            D.id_debate,
                            D.fecha_creacion,
                            D.debate,
                            D.vistas,
                            U.usuario,
                            U.foto,
                            U.mime,
                            U.id_usuario,
                            U.tipo_novia,
                            B.fecha_boda,
                            B.estado_boda
                        FROM
                            debates D
                        INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                        INNER JOIN cliente C ON (C.id_usuario = U.id_usuario)
                        INNER JOIN boda B USING (id_boda)
                        INNER JOIN grupos_comunidad G ON (D.id_grupo = G.id_grupos_comunidad)
                        WHERE
                            D.id_debate = $id_debate
                        AND D.activo = 1";$id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT
                        G.nombre,
                        G.imagen,
                        G.id_grupos_comunidad,
                        D.id_usuario,
                        D.titulo_debate,
                        D.id_debate,
                        D.fecha_creacion,
                        D.debate,
                        D.vistas,
                        U.usuario,
                        U.foto,
                        U.mime,
                        U.id_usuario,
                        U.tipo_novia,
                  	D.activo,
			(SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= D.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= D.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= D.id_usuario) AS id_boda,
                        (SELECT id_debate FROM denuncias_debates WHERE id_debate = $id_debate AND id_usuario = $id_usuario) AS id_denuncia
                    FROM
                        debates D
                    INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                    INNER JOIN grupos_comunidad G ON (D.id_grupo = G.id_grupos_comunidad)
                    WHERE
                        D.id_debate = $id_debate
                    AND D.activo = 1";
                $result = $this->db->query($sql)->row();
                $primer_debate = $this->db->query("SELECT id_debate FROM debates WHERE id_debate = $id_debate AND primero_del_dia = 1")->row();
                if(!empty($primer_debate)){
                    $this->set_medallas($id_usuario, 29);
                }
            }
            return $result;
        }
        
        function getContadorRespuestas($id){
            $sql = "SELECT id_debate FROM comentarios where id_debate=".$id. " and activo=1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function getRespuestas($id,$inicio,$limite){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                    CD.id_usuario,
                    CD.fecha_creacion,
                    CD.mensaje,
                    CD.id_comentario,
                    U.usuario,
                    U.id_usuario,
                    U.mime,
                    U.tipo_novia,
                    (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CD.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CD.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CD.id_usuario) AS id_boda,
                    (SELECT id_comentario FROM denuncia_comentarios
                            WHERE id_comentario = CD.id_comentario AND tipo = 1 AND id_usuario = $id_usuario
                    ) AS id_denuncia                            
                    FROM
                        debates D
                    INNER JOIN comentarios CD USING (id_debate)
                    INNER JOIN usuario U on (U.id_usuario=CD.id_usuario)
                    WHERE
                        CD.id_debate =$id
                    AND CD.activo = 1
                    AND CD.comentado = 0
                    ORDER BY
                            CD.fecha_creacion DESC
                    LIMIT 20 OFFSET ". $inicio.";";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getGrupos(){
            $result = $this->db->get('grupos_comunidad');
            return $result->result();
        }
        
        function publicarDebate($datos){
            $id_debate = "";
            $fecha_actual = date('Y-m-d H:i:s');
            if(date('H:i') >= '00:00' && date('H:i') <= '06:00'){
                $this->set_medallas($this->session->userdata('id_usuario'), 31);
            }
            $fecha = date('Y-m-d');
            $sql = "SELECT fecha_creacion FROM debates WHERE DATE_FORMAT(fecha_creacion,'%Y-%m-%d')  = '$fecha' AND activo = 1 ";
            $primer = $this->db->query($sql)->row();
            if(empty($primer)){
                $data = array(
                    'id_grupo' => $datos['grupos'],
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'titulo_debate' => $datos['titulo'],
                    'fecha_creacion' => $fecha_actual,
                    'fecha_actualizacion' => $fecha_actual,
                    'debate' => $datos['contenido'],
                    'primero_del_dia' => '1',
                    'activo' => '1'
                );
                $result = $this->db->insert("debates",$data);
                $id_debate = $this->db->insert_id();
            }else{
                $data = array(
                    'id_grupo' => $datos['grupos'],
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'titulo_debate' => $datos['titulo'],
                    'fecha_creacion' => $fecha_actual,
                    'fecha_actualizacion' => $fecha_actual,
                    'debate' => $datos['contenido'],
                    'primero_del_dia' => '0',
                    'activo' => '1'
                );
                $result = $this->db->insert("debates",$data);
                $id_debate = $this->db->insert_id();
            }
            if($result){
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 3;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $id_debate,
                    'tipo_actividad' => 1,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            return $id_debate;
        }
        
        function comentariosDebate($datos){
            $fecha_actual = date('Y-m-d H:i:s');
            $usuario = $this->session->userdata('id_usuario');
            $validar_comentario = $this->db->query("SELECT id_debate FROM comentarios WHERE id_debate =".$datos['debate'])->row();
            if(empty($validar_comentario)){
                $data = array(
                    'id_debate' => $datos['debate'],
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'mensaje' => $datos['mensaje'],
                    'activo' => 1,
                    'fecha_creacion' => $datos['fecha_creacion'],
                    'fecha_actualizacion' => $datos['fecha_creacion'],
                    'comentado' => 0,
                    'primer_comentario' => 1
                );
            }else{
                $data = array(
                    'id_debate' => $datos['debate'],
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'mensaje' => $datos['mensaje'],
                    'activo' => 1,
                    'fecha_creacion' => $datos['fecha_creacion'],
                    'fecha_actualizacion' => $datos['fecha_creacion'],
                    'comentado' => 0,
                    'primer_comentario' => 0
                );
            }
            
            //-------------INSERTAR COMENTARIO EN DB-------------------------------------------
            $result2 = $this->db->insert("comentarios",$data);
            $id_comentario = $this->db->insert_id();
            $id_usuario = $this->session->userdata('id_usuario');
            $comentarios_debate = $this->db->query("SELECT id_debate FROM comentarios WHERE activo = 1 AND id_usuario = $id_usuario")->num_rows();
            $comentarios_foto = $this->db->query("SELECT id_foto FROM comentarios_foto WHERE activo = 1 AND id_usuario = $id_usuario")->num_rows();
            $comentarios_video = $this->db->query("SELECT id_video FROM comentarios_video WHERE activo = 1 AND id_usuario = $id_usuario")->num_rows();
            $num_comentarios = $comentarios_debate + $comentarios_foto + $comentarios_video;
            switch ($num_comentarios){
                case 10:
                    $this->set_medallas($id_usuario, 8);
                    $this->set_medallas($id_usuario, 25);
                    break;
                case 20:
                    $this->set_medallas($id_usuario, 32);
                    $this->set_medallas($id_usuario, 26);
                    break;
                case 50:
                    $this->set_medallas($id_usuario, 9);
                    $this->set_medallas($id_usuario, 27);
                    break;
            }
            $sql = "SELECT id_debate FROM comentarios WHERE id_usuario = $id_usuario AND primer_comentario = 1";
            $fuga = $this->db->query($sql)->num_rows();
            switch ($fuga){
                case 1:
                    $this->set_medallas($id_usuario, 5);
                    break;
                case 20:
                    $this->set_medallas($id_usuario, 6);
                    break;
                case 50:
                    $this->set_medallas($id_usuario, 7);
                    break;
            }
            $aventurero = $this->db->query("SELECT id_debate FROM comentarios WHERE id_usuario = $id_usuario")->num_rows();
            switch ($aventurero){
                case 10:
                    $this->set_medallas($id_usuario, 13);
                    break;
                case 20:
                    $this->set_medallas($id_usuario, 14);
                    break;
                case 50:
                    $this->set_medallas($id_usuario, 15);
                    break;
            }
            if($result2){
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 3;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $id_comentario,
                    'tipo_actividad' => 4,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            //ACTUALIZACION CAMPO PARA ENVIAR EMAIL DEL DEBATE TABLA DEBATES
            $this->db->set($data = array('email_debate' => $datos['permiso_notificacion']));
            $this->db->where("id_debate", $datos['debate']);
            $this->db->where("id_usuario" , $this->session->userdata('id_usuario'));
            $this->db->update("comentarios");
            
            //---------PEDIR DATOS PARA PUBLICAR COMENTARIO-------------------------------------
            $sql = "SELECT
                    CD.fecha_creacion,
                    CD.id_comentario,
                    U.usuario,
                    U.id_usuario,
                    U.mime,
                    U.tipo_novia,
                    (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
                            cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
                            = CD.id_usuario) AS fecha_boda,
                            (SELECT boda.estado_boda FROM boda JOIN cliente ON 
                            cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
                            = CD.id_usuario) AS estado_boda,
                            (SELECT boda.id_boda FROM boda JOIN cliente ON 
                            cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
                            = CD.id_usuario) AS id_boda
                                                
                    FROM
                        debates D
                    INNER JOIN comentarios CD USING (id_debate)
                    INNER JOIN usuario U ON (U.id_usuario = CD.id_usuario)
                    WHERE
                        CD.id_comentario = $id_comentario
                    AND CD.activo = 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function respuestaComentario($datos){
            $fecha_actual = date('Y-m-d H:i:s');
            $data = array(
                'id_debate' => $datos['debate'],
                'id_usuario' => $this->session->userdata('id_usuario'),
                'mensaje' => $datos['mensaje'],
                'comentado' => $datos['respuesta'],
                'activo' => 1,
                'fecha_creacion' => $datos['fecha_creacion'],
                'fecha_actualizacion' => $datos['fecha_creacion']
            );
            $result2 = $this->db->insert("comentarios",$data);
            $id_comentario = $this->db->insert_id();
            if($result2){
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 3;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $id_comentario,
                    'tipo_actividad' => 4,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            $sql = "SELECT
                        CO.id_usuario as usuarioCO,
                        CO.fecha_creacion,
                        CO.id_comentario,
                        U.usuario,
                        U.id_usuario,
                        U.mime,
                        U.tipo_novia,
                        (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS id_boda
                    FROM
                        debates D
                    INNER JOIN comentarios CO USING (id_debate)
                    INNER JOIN usuario U ON (U.id_usuario = CO.id_usuario)
                    WHERE
                        CO.id_comentario = $id_comentario
                    AND CO.activo = 1
                    AND CO.comentado != 0 LIMIT 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getRespuesta($id_comentario){
            $sql = "SELECT
                        CO.id_usuario AS usuarioCO,
                        CO.fecha_creacion,
                        CO.id_comentario,
                        CO.comentado,
                        CO.mensaje,
                        U.usuario,
                        U.id_usuario,
                        U.mime,
                        U.tipo_novia,
                        (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS id_boda
                    FROM
                        comentarios CO
                    INNER JOIN usuario U ON (U.id_usuario = CO.id_usuario)
                    INNER JOIN debates D ON (D.id_debate = CO.id_debate)
                    WHERE
                        CO.activo = 1
                    AND CO.comentado = $id_comentario
                    ORDER BY
                        CO.fecha_creacion DESC";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function comentario_denunciado($id_comentario,$tipo){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT id_comentario FROM denuncia_comentarios WHERE id_comentario = $id_comentario AND id_usuario = $id_usuario AND tipo = $tipo";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getBoda($id_boda){
            $sql = "Select fecha_boda, estado_boda FROM boda WHERE id_boda = $id_boda";
            $resutl = $this->db->query($sql)->row();
            return $resutl;
        }
        
        function getRespuestaComentario($id_debate){
            $sql = "SELECT
                    CO.id_usuario,
                    CO.fecha_creacion,
                    CO.mensaje,
                    CO.id_comentario,
                    CO.comentado,
                    U.usuario,
                    U.id_usuario,
                    U.mime,
                    U.tipo_novia,
                    (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CO.id_usuario) AS id_boda
                    FROM
                        debates D
                    INNER JOIN comentarios CO USING (id_debate)
                    INNER JOIN usuario U on (U.id_usuario=CO.id_usuario)
                    WHERE
                        CO.id_debate = $id_debate
                    AND CO.activo = 1
                    AND CO.comentado > 0
                    ORDER BY
                        CO.fecha_creacion DESC";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function conocerCompaneros(){
            $id_usuario = $this->session->userdata('id_usuario');
            $result = "";
            $sql =  "SELECT
                        B.fecha_boda
                    FROM 
                        cliente C
                    INNER JOIN usuario USING (id_usuario)
                    INNER JOIN boda B ON (B.id_boda = C.id_boda)
                    WHERE 
                        C.id_usuario = $id_usuario";
            $fecha_boda = $this->db->query($sql)->row();
            if($fecha_boda){
                $sql =  "SELECT
                            U.id_usuario,
                            U.usuario,
                            U.mime,
                            B.ciudad_boda,
                            C.id_boda
                        FROM
                            usuario U 
                        INNER JOIN cliente C ON (C.id_usuario = U.id_usuario)
                        INNER JOIN boda B ON (B.id_boda = C.id_boda)
                        WHERE
                            B.fecha_boda = '$fecha_boda->fecha_boda'";
                $result = $this->db->query($sql)->result();
            }
            $datos = array(
                'fecha_boda' => $fecha_boda->fecha_boda,
                'companeros' => $result
            ); 
            return $datos;
        }
        
        function getFoto($id){
            $sql = "select U.foto,U.mime from usuario U where U.id_usuario=".$id;
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function setSuscripcion($id_debate){
            $data = array(
                'id_debate' => $id_debate,
                'id_usuario' => $this->session->userdata('id_usuario'),
                'activo' => 1
            );
            $result = $this->db->insert('suscripcion_debates',$data);
            return $result;
        }
        
        function validarSuscripcion($id_debate){
            $result = "";
            if(!empty($id_debate)){
                $id_usuario = $this->session->userdata('id_usuario');
                $result = $this->db->query("select activo, id_suscripcion from suscripcion_debates where id_debate=$id_debate and id_usuario=$id_usuario")->row();
            }
            return $result;
        }
        
        function updateSuscripcion($id_debate,$activo,$id_suscripcion){
            $id_usuario = $this->session->userdata('id_usuario');
            $data = array(
                'id_debate' => $id_debate,
                'id_usuario' => $id_usuario,
                'activo' => $activo
            );
            $this->db->set($data);
            $this->db->where('id_suscripcion',$id_suscripcion);
            $result = $this->db->update('suscripcion_debates');
            return $result;
        }
        
        function getComentario($id){
            $sql = "SELECT
                    CD.id_comentario,
                    CD.mensaje,
                    U.usuario
                    FROM
                            comentarios CD
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        CD.id_comentario = $id
                    AND	U.id_usuario = CD.id_usuario
                    AND CD.activo = 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getFechaBoda(){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                    B.fecha_boda,
                    B.estado_boda,
                    B.ciudad_boda
            FROM
                    cliente C
            INNER JOIN boda B USING (id_boda)
            INNER JOIN usuario U USING (id_usuario)
            WHERE
                C.id_usuario = $id_usuario
            AND	U.activo = 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getUltimosDebates(){
            $sql = "SELECT 
                        D.id_usuario,
                        D.titulo_debate,
                        D.id_debate,
                        D.fecha_creacion,
                        U.usuario,
                        U.mime
                    FROM
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        D.activo = 1
                    ORDER BY
                        D.fecha_creacion DESC
                    LIMIT 6";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getDebatesComentados(){
            $sql = "SELECT 
                        id_debate,
                        max(fecha_creacion) as fecha
                    FROM
                        comentarios
                    WHERE
                        activo = 1
                    GROUP BY
                        id_debate
                    ORDER BY
                        fecha DESC
                    LIMIT 5";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function datosDebates($id_debate){
            $sql = "SELECT 
                        D.titulo_debate,
                        D.debate,
                        D.fecha_creacion,
                        D.comentarios,
                        G.nombre,
                        U.mime,
                        U.usuario,
                        U.id_usuario
                    FROM
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    INNER JOIN grupos_comunidad G ON (G.id_grupos_comunidad = D.id_grupo)
                    WHERE
                        D.id_debate = $id_debate
                    AND	D.activo = 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getFotos(){
            $sql = "SELECT 
                        F.id_foto,
                        F.titulo,
                        F.id_usuario,
                        F.id_grupo,
                        U.mime,
                        U.usuario
                    FROM
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        F.activo = 1
                    ORDER BY
                        F.fecha_creacion DESC
                    LIMIT 7";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getVideos(){
            $sql = "SELECT 
                        V.id_video,
                        V.titulo,
                        V.id_usuario,
                        V.direccion_web,
                        V.id_grupo,
                        U.mime,
                        U.usuario
                    FROM
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        V.activo = 1
                    ORDER BY
                        V.fecha_creacion DESC
                    LIMIT 7";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function usuariosActivos(){
            $sql = "SELECT
                        id_usuario
                    FROM 
                        actividad_usuarios 
                    GROUP BY
                        id_usuario
                    LIMIT 5";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getDatosActivos($id_usuario){
            $sql = "SELECT
                        U.id_usuario,
                        U.usuario,
                        U.mime,
                        C.poblacion,
                        B.fecha_boda
                    FROM 
                        usuario U
                    INNER JOIN cliente C USING (id_usuario)
                    INNER JOIN boda B ON (B.id_boda = C.id_boda)
                    WHERE 
                        U.id_usuario = $id_usuario
                    AND U.activo = 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getUsuariosLogeados(){
            $sql = "SELECT
                        U.id_usuario,
                        U.mime
                    FROM 
                        usuario U
                    WHERE
                        U.activo = 1 
                    LIMIT 8";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getVisitas(){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                        U.mime,
                        U.id_usuario,
                        U.usuario
                    FROM 
                        visitas_perfil VP
                    INNER JOIN usuario U ON (U.id_usuario = VP.id_usuario_visita)
                    WHERE 
                        VP.id_usuario_perfil = $id_usuario
                    AND U.activo = 1 
                    LIMIT 4";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function usuariosBoda(){
            $usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                        B.fecha_boda,
                        B.estado_boda,
                        B.id_boda,
                        U.mime
                    FROM 
                        cliente C
                    INNER JOIN  usuario U ON (U.id_usuario = C.id_usuario)
                    INNER JOIN boda B ON (B.id_boda = C.id_boda) 
                    WHERE 
                        U.id_usuario = $usuario
                    AND U.activo = 1";
            $result = $this->db->query($sql)->row();
            if($result){
                $fecha_boda = new DateTime($result->fecha_boda);
                $fecha_boda = $fecha_boda->format('Y-m-d');
                $sql = "SELECT
                            B.id_boda
                        FROM
                            boda B
                        INNER JOIN cliente C USING (id_boda)
                        INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                        WHERE
                            B.fecha_boda = '$fecha_boda' 
                        AND U.activo = 1 
                        AND B.id_boda != $result->id_boda";
                $coincidencia = $this->db->query($sql)->num_rows();
                $result->novios = $coincidencia;
            }
            return $result;
        }
        
        function getGruposMiembro(){
            $usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                        G.nombre,
                        G.id_grupos_comunidad,
                        G.imagen
                    FROM 
                        miembros_grupo MG
                    INNER JOIN usuario U ON (U.id_usuario = MG.id_usuario)
                    INNER JOIN grupos_comunidad G ON (G.id_grupos_comunidad = MG.id_grupo)
                    WHERE 
                        U.id_usuario = $usuario
                    AND U.activo = 1 
                    AND MG.activo = 1";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function denuncia($comentario,$razon,$tipo){
            $fecha_creacion = date("Y-m-d H:i:s");
            $usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT id_usuario FROM denuncia_comentarios WHERE id_usuario = $usuario AND id_comentario = $comentario AND tipo = $tipo";
            $result = $this->db->query($sql)->row();
            if(empty($result)){
                $data = array(
                    'id_comentario' => $comentario,
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'razon' => $razon,
                    'tipo' => $tipo,
                    'fecha_creacion' => $fecha_creacion
                );
                $result = $this->db->insert("denuncia_comentarios",$data);
            }
            return $result;
        }
        
        function denuncia_debate($id_debate,$razon){
            $fecha_creacion = date("Y-m-d H:i:s");
            $usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT id_usuario FROM denuncias_debates WHERE id_usuario = $usuario AND id_debate = $id_debate";
            $result = $this->db->query($sql)->row();
            if(empty($result)){
                $data = array(
                    'id_debate' => $id_debate,
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'razon' => $razon,
                    'fecha_creacion' => $fecha_creacion
                );
                $result = $this->db->insert("denuncias_debates",$data);
            }
            return $result;
        }
        
        function set_medallas($id_usuario,$id_medalla){
            $sql = "SELECT id_medalla FROM medallas_usuarios WHERE id_usuario = $id_usuario AND id_nueva_medalla = $id_medalla";
            $validar = $this->db->query($sql)->row();
            if(empty($validar)){
                $datos = array(
                    'id_usuario' => $id_usuario,
                    'id_nueva_medalla' => $id_medalla
                );
                $this->db->insert("medallas_usuarios",$datos);
            }
        }
        
        function numero_debates(){
            $id_usuario = $this->session->userdata('id_usuario');
            $result = $this->db->query("SELECT id_debate FROM debates WHERE id_usuario = $id_usuario")->num_rows();
            return $result;
        }
        
        function buscar($titulo_debate){
            $sql =  "SELECT
                        D.id_debate,
                        D.titulo_debate,
                        D.fecha_creacion,
                        U.mime,
                        U.id_usuario
                    FROM 
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE 
                        D.activo
                    AND D.titulo_debate LIKE '%$titulo_debate%'";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function num_debates($id_grupo){
            $sql = "SELECT id_debate FROM debates WHERE activo = 1 AND id_grupo = $id_grupo";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function validarPuntos($puntos){
            if(!empty($puntos)){
                $id_usuario = $this->session->userdata('id_usuario');
                $fecha_actual = date('Y-m-d');
                $sql =  "SELECT 
                            B.fecha_boda 
                        FROM 
                            usuario U 
                        INNER JOIN cliente C USING (id_usuario) 
                        INNER JOIN boda B ON(B.id_boda = C.id_boda)
                        WHERE 
                            U.id_usuario = $id_usuario 
                        AND B.fecha_boda > '$fecha_actual'";
                $fecha_boda = $this->db->query($sql)->row();
                if($puntos >= 0 && $puntos <= 20){//NUEV@ NOVI@
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 1){
                        $this->db->set($data = array('tipo_novia' => 1));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 1));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 21 && $puntos <= 50){//NOVI@ PRINCIPIANTE
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 2){
                        $this->db->set($data = array('tipo_novia' => 2));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 2));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 51 && $puntos <= 100){//NOVI@ HABITUAL
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 3){
                        $this->db->set($data = array('tipo_novia' => 3));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 3));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 101 && $puntos <= 200){//NOVI@ TOP
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 4){
                        $this->db->set($data = array('tipo_novia' => 4));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 4));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 201 && $puntos <= 1000){//NOVI@ PRO
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 5){
                        $this->db->set($data = array('tipo_novia' => 5));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 5));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 1001 && $puntos <= 2000 && empty ($fecha_boda->fecha_boda)){//NOVI@ VIP
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 6){
                        $this->db->set($data = array('tipo_novia' => 6));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 6));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 2001 && $puntos <= 5000 && empty ($fecha_boda->fecha_boda)){//SUPER NOVI@ 
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 7){
                        $this->db->set($data = array('tipo_novia' => 7));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 7));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 5001 && $puntos <= 10000 && empty ($fecha_boda->fecha_boda)){//DESTACAD@
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 8){
                        $this->db->set($data = array('tipo_novia' => 8));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 8));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos > 1000 && !empty ($fecha_boda->fecha_boda)){//NOVI@ LEYENDA
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 9){
                        $this->db->set($data = array('tipo_novia' => 9));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }else{
                        $this->db->set($data = array('tipo_novia' => 9));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }
            }
        }
        
        function debates_perfil(){
            $sql =  "SELECT
                        D.id_debate,
                        D.titulo_debate,
                        D.debate,
                        D.fecha_creacion,
                        U.id_usuario,
                        U.mime,
                        U.usuario
                    FROM
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        D.activo = 1
                    AND U.activo = 1
                    ORDER BY D.fecha_creacion DESC 
                    LIMIT 3";
            $result = $this->db->query($sql)->result();
            return $result;
        }
    }
