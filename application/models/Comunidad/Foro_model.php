<?php
    class Foro_model extends CI_Model{
        
        public function __construct() {
            parent::__construct();
            $this->load->database();
        }
        
        function getUltimasFotos(){
            $sql =  "SELECT
                        F.id_foto,
                        F.id_grupo,
                        F.titulo,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE 
                        F.activo = 1
                    ORDER BY 
                        F.fecha_creacion DESC
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getFotosMasVistas(){
            $sql =  "SELECT 
                        F.id_foto,
                        F.id_grupo,
                        F.titulo,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        F.activo = 1
                    ORDER BY
                        F.vistas DESC 
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroFotosFecha($inicio){
            $sql =  "SELECT
                        F.id_foto,
                        F.id_grupo,
                        F.titulo,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE 
                        F.activo = 1
                    ORDER BY 
                        F.fecha_creacion DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroFotosVistas($inicio){
            $sql =  "SELECT
                        F.id_foto,
                        F.id_grupo,
                        F.titulo,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        F.activo = 1
                    ORDER BY
                        F.vistas DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroFotosComentarios($inicio){
            $sql =  "SELECT 
                        F.id_foto,
                        F.id_grupo,
                        F.titulo,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        F.activo = 1
                    ORDER BY
                        F.comentarios DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }

        function getTotalFotos(){
            $sql =  "SELECT
                        id_foto
                    FROM
                        foto_publicada
                    WHERE 
                        activo = 1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function getUltimosVideos(){
            $sql =  "SELECT
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE 
                        V.activo = 1
                    ORDER BY 
                        V.fecha_creacion DESC
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getVideosMasVistos(){
            $sql =  "SELECT 
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        V.activo = 1
                    ORDER BY
                        V.vistas DESC 
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroVideosFecha($inicio){
            $sql =  "SELECT
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE 
                        V.activo = 1
                    ORDER BY 
                        V.fecha_creacion DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroVideosVistas($inicio){
            $sql =  "SELECT
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        V.activo = 1
                    ORDER BY
                        V.vistas DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroVideosComentarios($inicio){
            $sql =  "SELECT 
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        V.activo = 1
                    ORDER BY
                        V.comentarios DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getTotalVideos(){
            $sql =  "SELECT
                        id_video
                    FROM
                        video_publicado
                    WHERE 
                        activo = 1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function getUltimosDebates(){
            $sql =  "SELECT 
                        D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime
                    FROM 
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        D.activo = 1
                    ORDER BY 
                        D.fecha_creacion DESC
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getDebatesPopulares(){
            $sql =  "SELECT
                        D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime
                    FROM
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        D.activo = 1
                    ORDER BY
                        D.vistas DESC
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroDebatesRecientes($inicio){
            $sql =  "SELECT 
                        D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime
                    FROM 
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        D.activo = 1
                    ORDER BY 
                        D.fecha_creacion DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroDebatesPopulares($inicio){
            $sql =  "SELECT 
                        D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime
                    FROM 
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        D.activo = 1
                    ORDER BY 
                        D.vistas DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getTotalDebates(){
            $sql =  "SELECT 
                        id_debate
                    FROM 
                        debates
                    WHERE 
                        activo = 1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function getAmistad($id_usuario){
            $id_usuario_logeado = $this->session->userdata('id_usuario');
            $sql =  "SELECT 
                        amistad, 
                        solicitud,
                        id_usuario_envio,
                        id_usuario_confirmacion
                    FROM 
                        amistad_usuarios 
                    WHERE 
                        id_usuario_envio = $id_usuario 
                    AND id_usuario_confirmacion = $id_usuario_logeado
                    OR id_usuario_envio = $id_usuario_logeado 
                    AND id_usuario_confirmacion = $id_usuario";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getTotalAmigos($id_usuario){
            $sql = "SELECT
                        id_usuario_confirmacion
                    FROM
                        amistad_usuarios 
                    WHERE
                        amistad = 1
                    AND id_usuario_envio = $id_usuario
                    OR id_usuario_confirmacion = $id_usuario
                    AND amistad = 1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
    
        function getTotalMensajes($id_usuario){
            $sql = "SELECT activo FROM comentarios WHERE activo = 1 AND id_usuario = $id_usuario";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }

        function getTotalDebatesTarjeta($id_usuario){
            $sql = "SELECT activo FROM debates WHERE activo = 1 AND id_usuario = $id_usuario";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }

        function getTotalFotosTarjeta($id_usuario){
            $sql = "SELECT activo FROM foto_publicada WHERE activo = 1 AND id_usuario = $id_usuario";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function filtro1($token,$pagina){ 
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $token[4]);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$token[2]'
            AND B.color = '$token[0]'
            AND B.estacion = '$token[1]'
            AND B.estado_boda = '$token[3]'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$token[2]'
                AND B.color = '$token[0]'
                AND B.estacion = '$token[1]'
                AND B.estado_boda = '$token[3]'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro2($color,$temporada,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.color = '$color'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro3($color,$temporada,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.color = '$color'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro4($color,$estilo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro5($color,$temporada,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.color = '$color'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada' 
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.color = '$color'
                AND B.estado_boda = '$estado'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro6($temporada,$estilo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'    
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro7($color,$temporada,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color= '$color'
            AND B.estilo = '$estilo'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.color = '$color'
                AND B.estilo = '$estilo'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro8($color,$temporada,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.color = '$color'
                AND B.estacion = '$temporada'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro9($color,$temporada,$fecha,$pagina){
         $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.color = '$color'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro10($color,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.color = '$color'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro11($color,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.color = '$color'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro12($temporada,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.estacion = '$temporada'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro13($temporada,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro14($estilo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro15($color,$temporada,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.color = '$color'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro16($color,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.color = '$color'
                AND B.estilo = '$estilo'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro17($color,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                    AND B.color = '$color'
                    AND B.estado_boda = '$estado'
                    ORDER BY
                        B.fecha_boda ASC
                    LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro18($color,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.color = '$color'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro19($temporada,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.estacion = '$temporada'
                AND B.estilo = '$estilo'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro20($temporada,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.estacion = '$temporada'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro21($temporada,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro22($estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro23($estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro24($estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro25($color,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($color))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.color = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($color))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro26($temporada,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($temporada))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.estacion = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($temporada))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro27($estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estilo = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($estilo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.estilo = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($estilo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro28($estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estado_boda = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($estado))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND B.estado_boda = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($estado))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro29($fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND DATE(B.fecha_boda) = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($fecha))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND DATE(B.fecha_boda) = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($fecha))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro30($pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            
            WHERE
                U.activo = 1
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function buscarMiembro($usuario,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND U.usuario LIKE '%$usuario%'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 1);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 1){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 1;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND U.usuario LIKE '%$usuario%'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 1 OFFSET $inicio";
            $miembros = $this->db->query($sql)->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function num_debates_encontrados($debate){
        $sql =  "SELECT 
                    id_debate
                FROM
                    debates
                WHERE
                    activo = 1
                AND titulo_debate LIKE '%$debate%'";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
    
    function buscar($debate,$inicio){
        $sql =  "SELECT 
                    D.id_debate,
                    D.titulo_debate,
                    D.comentarios,
                    D.vistas,
                    D.fecha_creacion,
                    D.debate,
                    U.id_usuario,
                    U.mime,
                    U.usuario
                FROM
                    debates D
                INNER JOIN usuario U USING (id_usuario)
                WHERE
                    D.activo = 1
                AND D.titulo_debate LIKE '%$debate%'
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }
    
}