<?php
    class Foto_model extends CI_Model{
        
        public function __construct() {
            parent::__construct();
            $this->load->database();
        }
        
        function publicarFoto($datos){
            $id_debate = "";
            $fecha_actual = date('Y-m-d H:i:s');
            $data = array(
                'id_usuario' => $this->session->userdata('id_usuario'),
                'id_grupo' => $datos['grupos'],
                'mime' => $datos['mime'],
                'imagen' => $datos['imagen'],
                'titulo' => $datos['titulo'],
                'descripcion' => $datos['descripcion'],
                'fecha_creacion' => $fecha_actual,
                'fecha_actualizacion' => $fecha_actual,
                'activo' => 1
            );
            $this->db->insert("foto_publicada",$data);
            $result = $this->db->insert_id();
            $this->set_medallas($this->session->userdata('id_usuario'), 30);
            if($result){
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 3;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $result,
                    'tipo_actividad' => 2,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            return $result;
        }
        
        function getFotoPublicada($id_grupo, $id_foto){
            $result = "";
            $id_usuario = $this->session->userdata('id_usuario');
            if(!empty($id_foto) && !empty($id_grupo)){
                $sql = "SELECT
                            G.nombre,
                            G.id_grupos_comunidad,
                            G.imagen,
                            F.id_grupo,
                            F.imagen,
                            F.mime,
                            F.fecha_creacion,
                            F.titulo,
                            F.descripcion,
                            U.usuario,
                            U.foto,
                            U.mime as mime_user,
                            U.id_usuario,
                            (SELECT id_foto FROM denuncias_fotos WHERE id_foto = $id_foto AND id_usuario = $id_usuario) AS id_denuncia
                        FROM
                            foto_publicada F
                            INNER JOIN usuario U ON (F.id_usuario = U.id_usuario)
                            INNER JOIN grupos_comunidad G ON (G.id_grupos_comunidad = F.id_grupo)
                        WHERE
                            F.activo = 1
                        AND F.id_foto = $id_foto
                        AND F.id_grupo = $id_grupo;
                       ";
                $result = $this->db->query($sql)->row();
            }
            return $result;
        }
                
        function getId_siguiente($id_grupo,$id_foto){
            if($id_foto >= 2){
                $id_foto = $id_foto--;
            }
            $sql = "SELECT
                        id_foto
                    from 
                        foto_publicada
                    WHERE 
                        id_foto < $id_foto
                    AND activo = 1
                    AND id_grupo=$id_grupo
                    ORDER BY id_foto DESC
                    LIMIT 1;";
            $result = $this->db->query($sql)->row();
            if($result == NULL){
                $sql = "SELECT
                        id_foto
                    from 
                        foto_publicada
                    WHERE 
                        id_foto > $id_foto
                    AND activo = 1
                    AND id_grupo=$id_grupo
                    ORDER BY id_foto DESC
                    LIMIT 1;";
                $result = $this->db->query($sql)->row();
            }
            return $result;
        }
        
        function getId_anterior($id_grupo,$id_foto){
            $id_foto = $id_foto++;
            $sql = "SELECT
                        id_foto
                    from 
                        foto_publicada
                    WHERE 
                        id_foto > $id_foto
                    AND activo = 1
                    AND id_grupo=$id_grupo
                    ORDER BY id_foto ASC
                    LIMIT 1;";
            $result = $this->db->query($sql)->row();
            if($result == NULL){
                $sql = "SELECT
                        id_foto
                    from 
                        foto_publicada
                    WHERE 
                        id_foto < $id_foto
                    AND activo = 1
                    AND id_grupo=$id_grupo
                    ORDER BY id_foto ASC
                    LIMIT 1;";
                $result = $this->db->query($sql)->row();
            }
            return $result;
        }
        
        function setLikeFoto($id_foto){
            $data = array(
                'id_foto' => $id_foto,
                'id_usuario' => $this->session->userdata('id_usuario'),
                'activo' => 1
            );
            $result = $this->db->insert('me_gusta_foto',$data);
            $id_me_gusta = $this->db->insert_id();
            if($result){
                $fecha_actual = date("Y-m-d H:i:s");
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 1;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $id_me_gusta,
                    'tipo_actividad' => 7,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            return $result;
        }
        
        function updateLikeFoto($id_foto,$activo,$id_like){
            $id_usuario = $this->session->userdata('id_usuario');
            $data = array(
                'id_foto' => $id_foto,
                'id_usuario' => $id_usuario,
                'activo' => $activo
            );
            $this->db->set($data);
            $this->db->where('id_me_gusta',$id_like);
            $result = $this->db->update('me_gusta_foto');
            return $result;
        }
        
        function validarLikeFoto($id_foto){
            $result = "";
            if(!empty($id_foto)){
                $sql = "SELECT
                            M.activo,
                            M.id_me_gusta
                        FROM
                            foto_publicada F
                            INNER JOIN me_gusta_foto M ON (M.id_usuario = F.id_usuario)
                        WHERE
                            F.activo = 1
                        AND M.id_foto = $id_foto
                        AND M.id_usuario = F.id_usuario;
                       ";
                $result = $this->db->query($sql)->row();
            }
            return $result;
        }
        
        function setVista($id_foto){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                vistas
            FROM
                vistas_fotos V
            INNER JOIN foto_publicada F ON (F.id_foto = V.id_foto)
            INNER JOIN usuario U ON (U.id_usuario = V.id_usuario)
            WHERE
                F.activo = 1
            AND U.activo = 1
            AND V.id_foto = $id_foto
            AND V.id_usuario = $id_usuario";
            $result = $this->db->query($sql)->num_rows();
            if(empty($result)){
                $data = array(
                    'id_foto' => $id_foto,
                    'id_usuario' => $id_usuario
                );
                $result = $this->db->insert("vistas_fotos",$data);
                $result = $this->db->query("select vistas from foto_publicada where id_foto=$id_foto")->row();
                if (empty($result)){
                    $result->vistas = 0;
                }
                $this->db->set($data = array('vistas' => $result->vistas + 1));
                $this->db->where("id_foto",$id_foto);
                $this->db->where("activo",1);
                $this->db->update("foto_publicada");
            }
        }
        
        function getVistasFotos($id_foto){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                vistas
            FROM
                foto_publicada
            WHERE
                activo = 1
            AND id_foto = $id_foto;";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function comentariosFotos($datos){
            $fecha_actual = date("Y-m-d H:i:s");
            $data = array(
                'id_foto' => $datos['foto'],
                'id_usuario' => $this->session->userdata('id_usuario'),
                'comentario' => $datos['mensaje'],
                'comentado' => 0,
                'activo' => 1,
                'fecha_creacion' => $datos['fecha_creacion'],
                'fecha_actualizacion' => $datos['fecha_creacion']
            );
            
            //-------------INSERTAR COMENTARIO EN DB-------------------------------------------
            $result = $this->db->insert("comentarios_foto",$data);
            $id_comentario = $this->db->insert_id();
            $id_usuario = $this->session->userdata('id_usuario');
            if($result){
                $fecha_actual = date("Y-m-d H:i:s");
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 1;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $id_comentario,
                    'tipo_actividad' => 5,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            $paparazzi = $this->db->query("SELECT id_foto FROM comentarios_foto WHERE id_usuario = $id_usuario")->num_rows();
            switch ($paparazzi){
                case 10:
                    $this->set_medallas($id_usuario, 17);
                    break;
                case 20:
                    $this->set_medallas($id_usuario, 18);
                    break;
                case 50:
                    $this->set_medallas($id_usuario, 19);
                    break;
            }
            $comentarios_debate = $this->db->query("SELECT id_debate FROM comentarios WHERE id_usuario = $id_usuario")->num_rows();
            $comentarios_foto = $this->db->query("SELECT id_foto FROM comentarios_foto WHERE id_usuario = $id_usuario")->num_rows();
            $comentarios_video = $this->db->query("SELECT id_video FROM comentarios_video WHERE id_usuario = $id_usuario")->num_rows();
            $num_comentarios = $comentarios_debate + $comentarios_foto + $comentarios_video;
            switch ($num_comentarios){
                case 10:
                    $this->set_medallas($id_usuario, 8);
                    $this->set_medallas($id_usuario, 25);
                    break;
                case 20:
                    $this->set_medallas($id_usuario, 32);
                    $this->set_medallas($id_usuario, 26);
                    break;
                case 50:
                    $this->set_medallas($id_usuario, 9);
                    $this->set_medallas($id_usuario, 27);
                    break;
            }
            //---------ACTUALIZACION CAMPO COMENTARIOS DE FOTO PUBLICADA-----------------------
            $sql = "select comentarios from foto_publicada where activo=1 and id_foto=".$datos['foto'];
            $result = $this->db->query($sql)->row();
            if (empty($result->comentarios)){
                $result->comentarios = 0;
            }
            $this->db->set($data = array('comentarios' => $result->comentarios + 1));
            $this->db->where("id_foto", $datos['foto']);
            $this->db->update("foto_publicada");
            //---------PEDIR DATOS PARA PUBLICAR COMENTARIO-------------------------------------
            $sql = "SELECT
                        CF.id_usuario,
                        CF.fecha_creacion,
                        CF.id_comentario,
                        U.usuario,
                        U.id_usuario,
                        U.mime,
                        U.tipo_novia,
                        (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS id_boda
                    FROM
                        foto_publicada F
                    INNER JOIN comentarios_foto CF USING (id_foto)
                    INNER JOIN usuario U ON (U.id_usuario = CF.id_usuario)
                    WHERE
                        CF.id_comentario = $id_comentario
                    AND CF.activo = 1
                    AND CF.comentado = 0";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function respuestaComentario($datos){
            $data = array(
                'id_foto' => $datos['foto'],
                'id_usuario' => $this->session->userdata('id_usuario'),
                'comentario' => $datos['mensaje'],
                'comentado' => $datos['respuesta'],
                'activo' => 1,
                'fecha_creacion' => $datos['fecha_creacion'],
                'fecha_actualizacion' => $datos['fecha_creacion']
            );
            $this->db->insert("comentarios_foto",$data);
            $id_comentario = $this->db->insert_id();
            $sql = "SELECT
                        CF.id_usuario as usuarioCO,
                        CF.fecha_creacion,
                        CF.id_comentario,
                        U.usuario,
                        U.id_usuario,
                        U.mime,
                        U.tipo_novia,
                        (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS id_boda
                    FROM
                        foto_publicada F
                    INNER JOIN comentarios_foto CF USING (id_foto)
                    INNER JOIN usuario U ON (U.id_usuario = CF.id_usuario)
                    WHERE
                        CF.id_comentario = $id_comentario
                    AND CF.activo = 1
                    AND CF.comentado != 0 LIMIT 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getRespuesta($id_comentario){
            $sql = "SELECT
                        CF.id_usuario as usuarioCO,
                        CF.fecha_creacion,
                        CF.id_comentario,
                        CF.comentado,
                        CF.comentario,
                        U.usuario,
                        U.id_usuario,
                        U.mime,
                        U.tipo_novia,
                        (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS id_boda
                    FROM
                        comentarios_foto CF
                    INNER JOIN usuario U ON (U.id_usuario = CF.id_usuario)
                    WHERE
                        CF.activo = 1
                    AND CF.comentado = $id_comentario
                    ORDER BY CF.fecha_creacion DESC";
            $result = $this->db->query($sql)->result();
            return $result;
        }
                
        function getContadorFotos($id_foto){
            $sql = "SELECT comentarios FROM foto_publicada where id_foto=".$id_foto. " and activo=1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getComentariosFoto($id_foto,$inicio,$limite){
            $id_usuario = $this->session->userdata('id_usuario');
            $result = "";
            if(!empty($id_foto)){
                $sql = "SELECT
                        CF.id_usuario,
                        CF.fecha_creacion,
                        CF.comentario,
                        CF.id_comentario,
                        U.usuario,
                        U.id_usuario,
                        U.mime,
                        U.tipo_novia,
                        (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS id_boda,
                        (SELECT id_comentario FROM denuncia_comentarios
                            WHERE id_comentario = CF.id_comentario AND tipo = 2 AND id_usuario = $id_usuario
                        ) AS id_denuncia
                        FROM
                            foto_publicada D
                        INNER JOIN comentarios_foto CF USING (id_foto)
                        INNER JOIN usuario U on (U.id_usuario=CF.id_usuario)
                        WHERE
                            CF.id_foto =$id_foto
                        AND CF.activo = 1
                        AND CF.comentado = 0
                        ORDER BY
                            CF.fecha_creacion DESC
                        LIMIT 16 OFFSET  $inicio";
                $result = $this->db->query($sql)->result();
            }
            return $result;
        }
        
        function getRespuestaComentario($id_foto){
            $sql = "SELECT
                    CF.id_usuario,
                    CF.fecha_creacion,
                    CF.comentario,
                    CF.id_comentario,
                    CF.comentado,
                    U.usuario,
                    U.id_usuario,
                    (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CF.id_usuario) AS id_boda
                    FROM
                        foto_publicada D
                    INNER JOIN comentarios_foto CF USING (id_foto)
                    INNER JOIN usuario U on (U.id_usuario=CF.id_usuario)
                    WHERE
                        CF.id_foto = $id_foto
                    AND CF.activo = 1
                    AND CF.comentado > 0
                    ORDER BY
                        CF.fecha_creacion DESC";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getFotosRecientes(){
            $sql = "SELECT
                        F.id_foto,
                        F.id_usuario,
                        F.id_grupo,
                        F.titulo,
                        U.usuario
                    FROM
                        foto_publicada F
                        INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
                    WHERE
                        F.activo=1
                    ORDER BY
                        F.fecha_creacion DESC
                    LIMIT 11;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getFotosVistas(){
            $sql = "SELECT
                F.id_foto,
                F.id_usuario,
                F.id_grupo,
                F.titulo,
                U.usuario
            FROM
                foto_publicada F
                INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
            WHERE
                F.activo = 1
            AND	F.vistas > 0
            ORDER BY
                F.vistas DESC
            LIMIT
                11;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function foto($id_foto){
            $sql = "select mime, imagen from foto_publicada where activo=1 and id_foto=$id_foto";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getTotalFotos(){
            $sql = "select activo from foto_publicada where activo=1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function getRecientes($inicio,$limite){
            $sql = "SELECT
                        F.id_foto,
                        F.id_usuario,
                        F.id_grupo,
                        F.titulo,
                        U.usuario
                    FROM
                        foto_publicada F
                        INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
                    WHERE
                        F.activo=1
                    ORDER BY
                        F.fecha_creacion DESC
                    LIMIT 16 OFFSET  $inicio;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function  getVistas($inicio,$limite){
            $sql = "SELECT
                F.id_foto,
                F.id_usuario,
                F.id_grupo,
                F.titulo,
                U.usuario
            FROM
                foto_publicada F
                INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
            WHERE
                F.activo = 1
            AND	F.vistas > 0
            ORDER BY
                F.vistas DESC
            LIMIT
                16 OFFSET  $inicio;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function  getComentadas($inicio,$limite){
            $sql = "SELECT
                F.id_foto,
                F.id_usuario,
                F.id_grupo,
                F.titulo,
                U.usuario
            FROM
                foto_publicada F
                INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
            WHERE
                F.activo = 1
            ORDER BY
                F.comentarios DESC
            LIMIT
                16 OFFSET  $inicio;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getFoto($id_foto){
            $sql = "SELECT imagen,mime FROM foto_publicada WHERE id_foto = $id_foto AND activo = 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function denuncia($foto,$razon){
            $fecha_creacion = date("Y-m-d H:i:s");
            $usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT id_usuario FROM denuncias_fotos WHERE id_usuario = $usuario AND id_foto = $foto";
            $result = $this->db->query($sql)->row();
            if(empty($result)){
                $data = array(
                    'id_foto' => $foto,
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'razon' => $razon,
                    'fecha_creacion' => $fecha_creacion
                );
                $result = $this->db->insert("denuncias_fotos",$data);
            }
            return $result;
        }
        
        function set_medallas($id_usuario,$id_medalla){
            $sql = "SELECT id_medalla FROM medallas_usuarios WHERE id_usuario = $id_usuario AND id_nueva_medalla = $id_medalla";
            $validar = $this->db->query($sql)->row();
            if(empty($validar)){
                $datos = array(
                    'id_usuario' => $id_usuario,
                    'id_nueva_medalla' => $id_medalla
                );
                $this->db->insert("medallas_usuarios",$datos);
            }
        }
        
        function numero_fotos(){
            $id_usuario = $this->session->userdata('id_usuario');
            $resurlt = $this->db->query("SELECT id_foto FROM foto_publicada WHERE activo = 1 AND id_usuario = $id_usuario")->num_rows();
            return $resurlt;
        }
        
        function validarPuntos($puntos){
            if(!empty($puntos)){
                $id_usuario = $this->session->userdata('id_usuario');
                $fecha_actual = date('Y-m-d');
                $sql =  "SELECT 
                            B.fecha_boda 
                        FROM 
                            usuario U 
                        INNER JOIN cliente C USING (id_usuario) 
                        INNER JOIN boda B ON(B.id_boda = C.id_boda)
                        WHERE 
                            U.id_usuario = $id_usuario 
                        AND B.fecha_boda > '$fecha_actual'";
                $fecha_boda = $this->db->query($sql)->row();
                if($puntos >= 0 && $puntos <= 20){//NUEV@ NOVI@
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 1){
                        $this->db->set($data = array('tipo_novia' => 1));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 21 && $puntos <= 50){//NOVI@ PRINCIPIANTE
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 2){
                        $this->db->set($data = array('tipo_novia' => 2));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 51 && $puntos <= 100){//NOVI@ HABITUAL
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 3){
                        $this->db->set($data = array('tipo_novia' => 3));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 101 && $puntos <= 200){//NOVI@ TOP
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 4){
                        $this->db->set($data = array('tipo_novia' => 4));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 201 && $puntos <= 1000){//NOVI@ PRO
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 5){
                        $this->db->set($data = array('tipo_novia' => 5));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 1001 && $puntos <= 2000 && empty ($fecha_boda->fecha_boda)){//NOVI@ VIP
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 6){
                        $this->db->set($data = array('tipo_novia' => 6));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 2001 && $puntos <= 5000 && empty ($fecha_boda->fecha_boda)){//SUPER NOVI@ 
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 7){
                        $this->db->set($data = array('tipo_novia' => 7));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 5001 && $puntos <= 10000 && empty ($fecha_boda->fecha_boda)){//DESTACAD@
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 8){
                        $this->db->set($data = array('tipo_novia' => 8));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos > 1000 && !empty ($fecha_boda->fecha_boda)){//NOVI@ LEYENDA
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 9){
                        $this->db->set($data = array('tipo_novia' => 9));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }
            }
        }
    }