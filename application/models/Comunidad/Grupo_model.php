<?php
class Grupo_model extends CI_Model{

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function setUnionGrupo($id_grupo){
        $data = array(
            'id_grupo' => $id_grupo,
            'id_usuario' => $this->session->userdata('id_usuario'),
            'activo' => 1
        );
        $result = $this->db->insert('miembros_grupo',$data);
        $miembros = $this->db->query("SELECT miembros FROM grupos_comunidad WHERE id_grupos_comunidad = $id_grupo")->row();
        if(empty($miembros->miembros)){
            $miembros->miembros = 0;
        }
        $miembros->miembros = $miembros->miembros + 1;
        $data = array(
            'miembros' => $miembros->miembros
        );
        $this->db->set($data);
        $this->db->where('id_grupos_comunidad', $id_grupo);
        $this->db->update('grupos_comunidad');
        return $result;
    }

    function validarUnionGrupo($id_grupo){
        $id_usuario = $this->session->userdata('id_usuario');
        $result = $this->db->query("select activo, id_miembro from miembros_grupo where id_grupo=$id_grupo and id_usuario=$id_usuario")->row();
        return $result;
    }

    function updateUnionGrupo($id_grupo,$activo,$id_miembro,$valor_miembro){
        $id_usuario = $this->session->userdata('id_usuario');
        $miembro = $this->db->query("SELECT miembros FROM grupos_comunidad WHERE id_grupos_comunidad = $id_grupo")->row();
        $miembro->miembros = $miembro->miembros + $valor_miembro;
        $this->db->set($data = array('miembros' => $miembro->miembros));
        $this->db->where('id_grupos_comunidad',$id_grupo);
        $this->db->update('grupos_comunidad');
        $data = array(
            'id_grupo' => $id_grupo,
            'id_usuario' => $id_usuario,
            'activo' => $activo
        );
        $this->db->set($data);
        $this->db->where('id_miembro',$id_miembro);
        $result = $this->db->update('miembros_grupo');
        return $result;
    }

    function getNombreGrupo($id_grupo){
        $sql = "select nombre,imagen from grupos_comunidad where id_grupos_comunidad = $id_grupo";
        $result = $this->db->query($sql)->row();
        return $result;
    }
    
//    -----------------------SECCION TODO---------------------------------------
    
    function getDebatesGrupo($id_grupo){
        $sql = "SELECT
                    CO.id_debate,
                    max(CO.fecha_creacion) AS fecha,
                    D.id_grupo
                FROM
                    comentarios CO
                INNER JOIN debates D ON (D.id_debate = CO.id_debate)
                WHERE
                    CO.activo = 1
                AND D.id_grupo = ?
                GROUP BY
                    id_debate
                ORDER BY
                    fecha DESC
                LIMIT 5";
        $result = $this->db->query($sql, array($id_grupo))->result();
        $i = 0;
        $debates = "";
        foreach($result as $debate){
            $sql = "SELECT 
                        D.id_debate,
                        D.titulo_debate,
                        D.fecha_creacion,
                        D.debate,
                        D.id_usuario,
                        U.usuario,
                        U.mime
                    FROM
                        debates D
                    INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                    WHERE
                        D.activo = 1
                    AND D.id_debate = ?";
            $debates[$i] = $this->db->query($sql,array($debate->id_debate))->row();
            $sql = "SELECT activo FROM comentarios WHERE activo = 1 AND id_debate = $debate->id_debate";
            $num_comentarios = $this->db->query($sql)->num_rows();
            $debates[$i]->num_comentarios = $num_comentarios;
            $fecha_comentario = relativeTimeFormat($debate->fecha,$mini=FALSE);
            if(strlen($fecha_comentario) <= 10 && strlen($fecha_comentario) > 4){
                $fecha_comentario = "Hace $fecha_comentario";
            }else if(strlen($fecha_comentario) > 10){
                $fecha_comentario = "El $fecha_comentario";
            }
            $fecha_creacion = relativeTimeFormat($debates[$i]->fecha_creacion,$mini=FALSE);
            if(strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4){
                $fecha_creacion = "Hace $fecha_creacion";
            }else if(strlen($fecha_creacion) > 10){
                $fecha_creacion = "El $fecha_creacion";
            }
            $debates[$i]->fecha_creacion = $fecha_creacion;
            $debates[$i]->fecha_comentario = $fecha_comentario;
            $sub_debate = substr($debates[$i]->debate, 0, 255);
            $sub_debate = $sub_debate . '...';
            $debates[$i]->debate = $sub_debate;
            $debates[$i]->url_usuario = base_url()."index.php/novios/comunidad/Perfil/usuario/".$debates[$i]->id_usuario;
            $i++;
        }
        return $debates;
    }
    
    function getFotosRecientesGrupo($id_grupo){
        $sql = "SELECT
                    F.id_foto,
                    F.id_usuario,
                    F.id_grupo,
                    F.titulo,
                    U.usuario,
                    U.mime
                FROM
                    foto_publicada F
                    INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
                WHERE
                    F.activo=1
                AND F.id_grupo = $id_grupo    
                ORDER BY
                    F.fecha_creacion DESC
                LIMIT 11;";
        $result = $this->db->query($sql)->result();
        return $result;
    }
    
    function getVideosRecientesGrupo($id_grupo){
        $sql = "SELECT
                    V.id_video,
                    V.id_usuario,
                    V.id_grupo,
                    V.direccion_web,
                    V.descripcion,
                    V.titulo,
                    U.usuario,
                    U.mime
                FROM
                    video_publicado V
                    INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
                WHERE
                    V.activo=1
                AND V.id_grupo = ?
                ORDER BY
                    V.fecha_creacion DESC
                LIMIT 11;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
//    ------------------------SECCION FOTOS-------------------------------------
    
    function totalFotos($id_grupo){
        $sql = "SELECT activo FROM foto_publicada WHERE id_grupo = ? AND activo = 1";
        $result = $this->db->query($sql,array($id_grupo))->num_rows();
        return $result;
    }
    
    function getComentadas($id_grupo,$inicio){
        $sql = "SELECT
            F.id_foto,
            F.id_usuario,
            F.id_grupo,
            F.titulo,
            U.usuario,
            U.mime
        FROM
            foto_publicada F
            INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
        WHERE
            F.activo = 1
        AND F.id_grupo = ?
        ORDER BY
            F.comentarios DESC
        LIMIT
            16 OFFSET  $inicio;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
    function getRecientes($id_grupo,$inicio){
        $sql = "SELECT
                    F.id_foto,
                    F.id_usuario,
                    F.id_grupo,
                    F.titulo,
                    U.usuario,
                    U.mime
                FROM
                    foto_publicada F
                    INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
                WHERE
                    F.activo=1
                AND F.id_grupo = ?
                ORDER BY
                    F.fecha_creacion DESC
                LIMIT 16 OFFSET  $inicio;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
    function getVisitadas($id_grupo,$inicio){
        $sql = "SELECT
                F.id_foto,
                F.id_usuario,
                F.id_grupo,
                F.titulo,
                U.usuario,
                U.mime
            FROM
                foto_publicada F
                INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
            WHERE
                F.activo = 1
            AND	F.vistas > 0
            AND F.id_grupo = ?
            ORDER BY
                F.vistas DESC
            LIMIT
                16 OFFSET  $inicio;";
            $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
//    -------------------------SECCION VIDEOS-----------------------------------
    
    function totalVideos($id_grupo){
        $sql = "SELECT activo FROM video_publicado WHERE id_grupo = ? AND activo = 1";
        $result = $this->db->query($sql,array($id_grupo))->num_rows();
        return $result;
    }
        
    function getComentados($id_grupo,$inicio){
        $sql = "SELECT
            V.id_video,
            V.id_usuario,
            V.id_grupo,
            V.titulo,
            V.direccion_web,
            U.usuario,
            U.mime
        FROM
            video_publicado V
            INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
        WHERE
            V.activo = 1
        AND V.id_grupo = ?
        ORDER BY
            V.comentarios DESC
        LIMIT
            16 OFFSET  $inicio;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
    function getVideosRecientes($id_grupo,$inicio){
        $sql = "SELECT
                    V.id_video,
                    V.id_usuario,
                    V.id_grupo,
                    V.titulo,
                    V.direccion_web,
                    U.usuario,
                    U.mime
                FROM
                    video_publicado V
                    INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
                WHERE
                    V.activo=1
                AND V.id_grupo = ?
                ORDER BY
                    V.fecha_creacion DESC
                LIMIT 16 OFFSET  $inicio;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
    function getVisitados($id_grupo,$inicio){
        $sql = "SELECT
                V.id_video,
                V.id_usuario,
                V.id_grupo,
                V.titulo,
                V.direccion_web,
                U.usuario,
                U.mime
            FROM
                video_publicado V
                INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
            WHERE
                V.activo = 1
            AND	V.vistas > 0
            AND V.id_grupo = ?
            ORDER BY
                V.vistas DESC
            LIMIT
                16 OFFSET  $inicio;";
            $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
//    ----------------------------SECCION DEBATES-------------------------------
    
    function totalDebates($id_grupo){
        $sql = "SELECT activo FROM debates WHERE activo = 1 AND id_grupo = ?";
        $result = $this->db->query($sql,array($id_grupo))->num_rows();
        return $result;
    }

    function totalDebatesUltimoMensaje($id_grupo){
        $sql = "SELECT
                    CO.id_debate,
                    max(CO.fecha_creacion) AS fecha,
                    D.id_grupo
                FROM
                    comentarios CO
                INNER JOIN debates D ON (D.id_debate = CO.id_debate)
                WHERE
                    CO.activo = 1
                AND D.id_grupo = ?
                GROUP BY
                    id_debate";
        $result = $this->db->query($sql, array($id_grupo))->num_rows();
        return $result;
    }
    
    function getMensaje($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.id_usuario,
                    D.titulo_debate,
                    U.usuario,
                    U.mime
                FROM
                    debates D
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    D.activo = 1
                AND D.id_grupo = ?
                GROUP BY
                    D.id_debate
                ORDER BY
                    D.comentarios DESC
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql,array($id_grupo))->result();
        $debates = $this->formatearArray($result);
        return $debates;
    }
    
    function getVisitasDebate($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.id_usuario,
                    D.titulo_debate,
                    U.usuario,
                    U.mime
                FROM
                    debates D
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    D.activo = 1
                AND D.id_grupo = ?
                GROUP BY
                    D.id_debate
                ORDER BY
                    D.vistas DESC
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql,array($id_grupo))->result();
        $debates = $this->formatearArray($result);
        return $debates;
    }
    
    function getUltimoMensaje($id_grupo,$inicio){
        $sql = "SELECT
                    CO.id_debate,
                    max(CO.fecha_creacion) AS fecha,
                    D.id_grupo
                FROM
                    comentarios CO
                INNER JOIN debates D ON (D.id_debate = CO.id_debate)
                WHERE
                    CO.activo = 1
                AND D.id_grupo = ?
                GROUP BY
                    id_debate
                ORDER BY
                    fecha DESC
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql, array($id_grupo))->result();
        $i = 0;
        $debates = "";
        foreach($result as $debate){
            $sql = "SELECT 
                        D.id_debate,
                        D.titulo_debate,
                        D.fecha_creacion,
                        D.debate,
                        D.id_usuario,
                        U.usuario,
                        U.mime
                    FROM
                        debates D
                    INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                    WHERE
                        D.activo = 1
                    AND D.id_debate = ?";
            $debates[$i] = $this->db->query($sql,array($debate->id_debate))->row();
            $sql = "SELECT activo FROM comentarios WHERE activo = 1 AND id_debate = $debate->id_debate";
            $num_comentarios = $this->db->query($sql)->num_rows();
            $debates[$i]->num_comentarios = $num_comentarios;
            $fecha_comentario = relativeTimeFormat($debate->fecha,$mini=FALSE);
            if(strlen($fecha_comentario) <= 10 && strlen($fecha_comentario) > 4){
                $fecha_comentario = "Hace $fecha_comentario";
            }else if(strlen($fecha_comentario) > 10){
                $fecha_comentario = "El $fecha_comentario";
            }
            $fecha_creacion = relativeTimeFormat($debates[$i]->fecha_creacion,$mini=FALSE);
            if(strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4){
                $fecha_creacion = "Hace $fecha_creacion";
            }else if(strlen($fecha_creacion) > 10){
                $fecha_creacion = "El $fecha_creacion";
            }
            if(!empty($debates[$i]->mime)){
                $debates[$i]->foto_usuario = base_url()."index.php/novios/comunidad/home/foto_usuario/".$debates[$i]->id_usuario;
            }else{
                $debates[$i]->foto_usuario = base_url()."dist/img/blog/perfil.png";
            }
            $debates[$i]->fecha_creacion = $fecha_creacion;
            $debates[$i]->fecha_comentario = $fecha_comentario;
            $sub_debate = substr($debates[$i]->debate, 0, 255);
            $sub_debate = $sub_debate . '...';
            $debates[$i]->debate = $sub_debate;
            $debates[$i]->url_usuario = base_url()."index.php/novios/comunidad/Perfil/".$debates[$i]->id_usuario;
            $i++;
        }
        return $debates;
    }
    
    function getDebatesRecientes($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.id_usuario,
                    D.titulo_debate,
                    U.usuario,
                    U.mime
                FROM
                    debates D
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    D.activo = 1
                AND D.id_grupo = ?
                GROUP BY
                    D.id_debate
                ORDER BY
                    D.fecha_creacion DESC LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql,array($id_grupo))->result();
        $debates = $this->formatearArray($result);
        return $debates;
    }
    
    function getDebateSinRespuesta($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.id_usuario,
                    D.titulo_debate,
                    U.usuario,
                    U.mime
                FROM
                    debates D
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    D.activo = 1
                AND D.id_grupo = ?
                AND D.comentarios = 0
                GROUP BY
                    D.id_debate
                ORDER BY
                    D.fecha_creacion DESC LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql,array($id_grupo))->result();
        $debates = $this->formatearArray($result);
        return $debates;
    }
    
    function getTotalComentarios($id_debate){
        $sql = "SELECT activo FROM comentarios WHERE id_debate = $id_debate AND activo = 1";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
    
    function formatearFecha($fecha_creacion){
        $fecha_creacion2 = "";
        if(!empty($fecha_creacion)){
            $fecha_creacion2 = relativeTimeFormat($fecha_creacion,$mini=FALSE);
            if(strlen($fecha_creacion2) <= 10 && strlen($fecha_creacion2) > 4){
                $fecha_creacion2 = "Hace $fecha_creacion2";
            }else if(strlen($fecha_creacion2) > 10){
                $fecha_creacion2 = "El $fecha_creacion2";
            }
        }
        return $fecha_creacion2;
    }
    
    function formatearArray($result){
        $debates = "";
        $i = 0;
        foreach ($result as $debate){
            $sql = "SELECT
                        fecha_creacion
                    FROM
                        comentarios
                    WHERE
                        activo = 1
                    AND id_debate = ?
                    ORDER BY
                        fecha_creacion DESC
                    LIMIT 1";
            $comentario = $this->db->query($sql,array($debate->id_debate))->row();
            $num_comentarios = $this->getTotalComentarios($debate->id_debate);
            $debate->fecha_creacion = $this->formatearFecha($debate->fecha_creacion);
            if(!empty($comentario->fecha_creacion)){
                $debate->fecha_comentario = $this->formatearFecha($comentario->fecha_creacion);
            }else{
                $debate->fecha_comentario = "";
            }
            $sub_debate = substr($debate->debate, 0, 255);
            $sub_debate = $sub_debate . '...';
            $debate->debate = $sub_debate;
            $debate->url_usuario = base_url().'index.php/novios/comunidad/Home/foto_usuario'.$debate->id_usuario;
            $debate->num_comentarios = $num_comentarios;
            $debates[$i] = $debate;
            $i++;
        }
        return $debates;
    }
    
//    ---------------------------SECCION NUEVOS INTEGRANTES -----------------------
    function totalUsuariosGrupo($id_grupo,$fecha_actual){
        $sql = "SELECT
                U.activo
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.id_grupo = ?
            AND U.fecha_creacion > ?
            ORDER BY
                U.fecha_creacion DESC";
        $result = $this->db->query($sql,array($id_grupo,$fecha_actual))->num_rows();
        return $result;
    }
    
    function nuevos_integrantes($id_grupo,$fecha_actual,$inicio){
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto,
                U.puntos
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.id_grupo = ?
            AND U.fecha_creacion > ?
            AND M.activo = 1
            ORDER BY
                U.fecha_creacion DESC 
            LIMIT 12 OFFSET $inicio";
        $nuevos_integrantes = $this->db->query($sql,array($id_grupo,$fecha_actual))->result();
        return $nuevos_integrantes;
    }
    
    function getTotalFotosUsuario($id_usuario){
        $sql = "SELECT activo FROM foto_publicada WHERE activo = 1 AND id_usuario = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
    
    function getTotalAmigos($id_usuario_confirmacion){
        $id_usuario_envio = $this->session->userdata('id_usuario');
        $sql = "SELECT id_amistad FROM amistad_usuarios "
                . "WHERE amistad = 1 AND id_usuario_envio = $id_usuario_envio AND id_usuario_confirmacion = $id_usuario_confirmacion "
                . "OR amistad = 1 AND id_usuario_envio = $id_usuario_confirmacion AND id_usuario_confirmacion = $id_usuario_envio";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
    
    function getAmistad($id_usuario){
        $id_usuario_logeado = $this->session->userdata('id_usuario');
        $sql =  "SELECT 
                    amistad, 
                    solicitud,
                    id_usuario_envio,
                    id_usuario_confirmacion
                FROM 
                    amistad_usuarios 
                WHERE 
                    id_usuario_envio = $id_usuario 
                AND id_usuario_confirmacion = $id_usuario_logeado
                OR id_usuario_envio = $id_usuario_logeado 
                AND id_usuario_confirmacion = $id_usuario";
        $result = $this->db->query($sql)->row();
        return $result;
    }
    
    function getTotalMensajes($id_usuario){
        $sql = "SELECT activo FROM comentarios WHERE activo = 1 AND id_usuario = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
    
    function getTotalDebates($id_usuario){
        $sql = "SELECT activo FROM debates WHERE activo = 1 AND id_usuario = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
    
//     ---------------------------FIN SECCION NUEVOS INTEGRANTES -----------------------
    
//    ----------------------------SECCION PROXIMAS BODAS --------------------------------
    function totalUsuariosBoda($id_grupo,$fecha_boda){
        $sql = "SELECT
                U.activo
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.id_grupo = ?
            AND B.fecha_boda > ?";
        $result = $this->db->query($sql,array($id_grupo,$fecha_boda))->num_rows();
        return $result;
    }
    
    function proximas_bodas($id_grupo,$fecha_boda,$inicio){
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto,
                U.puntos
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.id_grupo = ?
            AND B.fecha_boda >= ?
            AND M.activo = 1
            ORDER BY
                B.fecha_boda ASC 
            LIMIT 12 OFFSET $inicio";
        $proximas_bodas = $this->db->query($sql,array($id_grupo,$fecha_boda))->result();
        return $proximas_bodas;
    }
//    ----------------------------FIN SECCION PROXIMAS BODAS --------------------------------
    
    
//    --------------------------------SECCION MIEMBROS--------------------------------------
    function filtro1($id_grupo,$token,$pagina){ 
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $token[4]);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$token[2]'
            AND B.color = '$token[0]'
            AND B.estacion = '$token[1]'
            AND B.estado_boda = '$token[3]'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $datos = "";
            $sql = "SELECT
                        U.id_usuario,
                        U.usuario,
                        U.fecha_creacion,
                        U.puntos,
                        B.ciudad_boda,
                        B.estado_boda,
                        B.fecha_boda,
                        U.foto
                    FROM
                        cliente C
                    INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                    INNER JOIN boda B ON (B.id_boda = C.id_boda)
                    INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                    WHERE
                        U.activo = 1
                    AND M.activo = 1
                    AND M.id_grupo = ?
                    AND DATE(B.fecha_boda) = '$fecha'
                    AND B.estilo = '$token[2]'
                    AND B.color = '$token[0]'
                    AND B.estacion = '$token[1]'
                    AND B.estado_boda = '$token[3]'
                    ORDER BY
                        B.fecha_boda ASC
                    LIMIT 12 OFFSET $inicio";
                $miembros = $this->db->query($sql,array($id_grupo))->result();
                $datos = array(
                    'total_paginas' => $total_paginas,
                    'contador' => $contador,
                    'miembros' => $miembros,
                    'pagina' => $pagina
                );
                return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro2($id_grupo,$color,$temporada,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.color = '$color'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro3($id_grupo,$color,$temporada,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.color = '$color'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro4($id_grupo,$color,$estilo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro5($id_grupo,$color,$temporada,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.color = '$color'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada' 
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.color = '$color'
                AND B.estado_boda = '$estado'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro6($id_grupo,$temporada,$estilo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'    
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.id_grupo = ?
                AND M.activo = 1
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro7($id_grupo,$color,$temporada,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.color= '$color'
            AND B.estilo = '$estilo'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.color = '$color'
                AND B.estilo = '$estilo'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro8($id_grupo,$color,$temporada,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.color = '$color'
                AND B.estacion = '$temporada'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro9($id_grupo,$color,$temporada,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = ?
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo,$fecha))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.color = '$color'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro10($id_grupo,$color,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.color = '$color'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro11($id_grupo,$color,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.color = '$color'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro12($id_grupo,$temporada,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.estacion = '$temporada'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.estacion = '$temporada'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro13($id_grupo,$temporada,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro14($id_grupo,$estilo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro15($id_grupo,$color,$temporada,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.color = '$color'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro16($id_grupo,$color,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.color = '$color'
                AND B.estilo = '$estilo'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro17($id_grupo,$color,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.color = '$color'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.color = '$color'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro18($id_grupo,$color,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.color = '$color'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro19($id_grupo,$temporada,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.estacion = '$temporada'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.estacion = '$temporada'
                AND B.estilo = '$estilo'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro20($id_grupo,$temporada,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.estacion = '$temporada'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro21($id_grupo,$temporada,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estacion = '$temporada'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro22($id_grupo,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.estilo = '$estilo'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro23($id_grupo,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estilo = '$estilo'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro24($id_grupo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = '$fecha'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND DATE(B.fecha_boda) = '$fecha'
                AND B.estado_boda = '$estado'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro25($id_grupo,$color,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.color = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo,$color))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.color = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo,$color))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro26($id_grupo,$temporada,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.estacion = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo,$temporada))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.estacion = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo,$temporada))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro27($id_grupo,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.estilo = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo,$estilo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.estilo = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo,$estilo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro28($id_grupo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND B.estado_boda = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo,$estado))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND B.estado_boda = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo,$estado))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro29($id_grupo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            AND DATE(B.fecha_boda) = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo,$fecha))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                        U.id_usuario,
                        U.usuario,
                        U.fecha_creacion,
                        U.puntos,
                        B.ciudad_boda,
                        B.estado_boda,
                        B.fecha_boda,
                        U.foto
                    FROM
                        cliente C
                    INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                    INNER JOIN boda B ON (B.id_boda = C.id_boda)
                    INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                    WHERE
                        U.activo = 1
                    AND M.activo = 1
                    AND M.id_grupo = ?
                    AND DATE(B.fecha_boda) = ?
                    ORDER BY
                        B.fecha_boda ASC
                    LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo,$fecha))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function filtro30($id_grupo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($id_grupo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
    
    function buscarMiembro($id_grupo,$usuario,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
            WHERE
                U.activo = 1
            AND M.activo = 1
            AND M.id_grupo = $id_grupo
            AND U.usuario LIKE '%$usuario%'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($total_paginas < $contador / 12){
            $total_paginas++;
        }
        if(!empty($pagina) && !is_numeric($pagina)){
            $pagina = $total_paginas + 1;
        }
        if($pagina <= $total_paginas && $pagina > 0){
            $inicio = ($pagina - 1) * 12;
            $sql = "SELECT
                    U.id_usuario,
                    U.usuario,
                    U.fecha_creacion,
                    U.puntos,
                    B.ciudad_boda,
                    B.estado_boda,
                    B.fecha_boda,
                    U.foto
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                INNER JOIN miembros_grupo M ON (M.id_usuario = U.id_usuario)
                WHERE
                    U.activo = 1
                AND M.activo = 1
                AND M.id_grupo = ?
                AND U.usuario LIKE '%$usuario%'
                ORDER BY
                    B.fecha_boda ASC
                LIMIT 12 OFFSET $inicio";
            $miembros = $this->db->query($sql,array($id_grupo))->result();
            $datos = array(
                'total_paginas' => $total_paginas,
                'contador' => $contador,
                'miembros' => $miembros,
                'pagina' => $pagina
            );
            return $datos;
        }
        return $datos = array('pagina' => 'error');
    }
//    -------------------------------FIN SECCION MIEMBROS--------------------------------------
}
