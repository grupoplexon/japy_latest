<?php
    class Video_model extends CI_Model{
        
        public function __construct() {
            parent::__construct();
            $this->load->database();
        }
        
        function publicarVideo($datos){
            $fecha_actual = date('Y-m-d H:i:s');
            $data = array(
                'id_usuario' => $this->session->userdata('id_usuario'),
                'id_grupo' => $datos['grupo'],
                'direccion_web' => $datos['direccion_video'],
                'titulo' => $datos['titulo'],
                'descripcion' => $datos['descripcion'],
                'fecha_creacion' => $fecha_actual,
                'fecha_actualizacion' => $fecha_actual,
                'activo' => 1,
                'comentarios' => 0,
                'vistas' => 0
            );
            $fecha = date('Y-m-d');
            $primer_video = $this->db->query("SELECT fecha_creacion FROM video_publicado WHERE DATE_FORMAT(fecha_creacion,'%Y-%m-%d') = '$fecha'")->row();
            if(empty($primer_video)){
                $this->set_medallas($this->session->userdata('id_usuario'), 28);
            }
            $this->db->insert('video_publicado',$data);
            $result = $this->db->insert_id();
            if($result){
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 3;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $result,
                    'tipo_actividad' => 3,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            return $result;
        }
        
        function getVideoPublicado($id_grupo,$id_video){
            $result = "";
            if(!empty($id_grupo) && !empty($id_video)){
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT
                            G.nombre,
                            V.id_grupo,
                            V.fecha_creacion,
                            V.titulo,
                            V.descripcion,
                            V.direccion_web,
                            U.usuario,
                            U.foto,
                            U.mime as mime_user,
                            U.id_usuario,
                            (SELECT id_video FROM denuncias_videos WHERE id_video = $id_video AND id_usuario = $id_usuario) AS id_denuncia
                        FROM
                            video_publicado V
                            INNER JOIN usuario U ON (V.id_usuario = U.id_usuario)
                            INNER JOIN grupos_comunidad G ON (G.id_grupos_comunidad = V.id_grupo)
                        WHERE
                            V.activo = 1
                        AND V.id_video = $id_video
                        AND V.id_grupo = $id_grupo;
                       ";
                $result = $this->db->query($sql)->row();
            }
            return $result;
        }
        
        function setLikeVideo($id_video){
            $data = array(
                'id_video' => $id_video,
                'id_usuario' => $this->session->userdata('id_usuario'),
                'activo' => 1
            );
            $result = $this->db->insert('me_gusta_video',$data);
            $id_me_gusta = $this->db->insert_id();
            if($result){
                $fecha_actual = date("Y-m-d H:i:s");
                $id_usuario = $this->session->userdata('id_usuario');
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 1;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $id_me_gusta,
                    'tipo_actividad' => 8,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            return $result;
        }
        
        function updateLikeVideo($id_video,$activo,$id_like){
            $id_usuario = $this->session->userdata('id_usuario');
            $data = array(
                'id_video' => $id_video,
                'id_usuario' => $id_usuario,
                'activo' => $activo
            );
            $this->db->set($data);
            $this->db->where('id_me_gusta',$id_like);
            $result = $this->db->update('me_gusta_video');
            return $result;
        }
        
        function validarLikeVideo($id_video){
            $result = "";
            if(!empty($id_video)){
                $sql = "SELECT
                            M.activo,
                            M.id_me_gusta
                        FROM
                            video_publicado V
                            INNER JOIN me_gusta_video M ON (M.id_usuario = V.id_usuario)
                        WHERE
                            V.activo = 1
                        AND M.id_video = $id_video
                        AND M.id_usuario = V.id_usuario;
                       ";
                $result = $this->db->query($sql)->row();
            }
            return $result;
        }
        
        function setVistaVideo($id_video){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                id_vista_video
            FROM
                vistas_videos V
            INNER JOIN video_publicado VP ON (VP.id_video = V.id_video)
            INNER JOIN usuario U ON (U.id_usuario = V.id_usuario)
            WHERE
                VP.activo = 1
            AND U.activo = 1
            AND V.id_video = $id_video
            AND V.id_usuario = $id_usuario";
            $result = $this->db->query($sql)->num_rows();
            if(empty($result)){
                $data = array(
                    'id_video' => $id_video,
                    'id_usuario' => $id_usuario
                );
                $result = $this->db->insert("vistas_videos",$data);
                $result = $this->db->query("select vistas from video_publicado where id_video=$id_video")->row();
                if (empty($result)){
                    $result->vistas = 0;
                }
                $this->db->set($data = array('vistas' => $result->vistas + 1));
                $this->db->where("id_video",$id_video);
                $this->db->where("activo",1);
                $this->db->update("video_publicado");
            }
        }
        
        function getVistasVideos($id_video){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                vistas
            FROM
                video_publicado
            WHERE
                activo = 1
            AND id_video = $id_video;";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getContadorVideos($id_video){
            $sql = "SELECT comentarios FROM video_publicado where id_video=".$id_video. " and activo=1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function comentariosVideo($datos){
            $fecha_actual = date("Y-m-d H:i:s");
            $data = array(
                'id_video' => $datos['video'],
                'id_usuario' => $this->session->userdata('id_usuario'),
                'comentario' => $datos['mensaje'],
                'comentado' => 0,
                'activo' => 1,
                'fecha_creacion' => $datos['fecha_creacion'],
                'fecha_actualizacion' => $datos['fecha_creacion']
            );
            
            //-------------INSERTAR COMENTARIO EN DB-------------------------------------------
            
            $result = $this->db->insert("comentarios_video",$data);
            $id_comentario = $this->db->insert_id();
            $id_usuario = $this->session->userdata('id_usuario');
            if($result){
                $sql = "SELECT puntos FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
                $puntos = $this->db->query($sql)->row();
                $puntos = ((int)$puntos->puntos) + 1;
                $this->validarPuntos($puntos);
                $data = array(
                    'puntos' => $puntos
                );
                $this->db->set($data);
                $this->db->where('id_usuario',$id_usuario);
                $this->db->where('activo',1);
                $this->db->update('usuario');
                $data = array(
                    'id_usuario' => $id_usuario,
                    'id_actividad_realizada' => $id_comentario,
                    'tipo_actividad' => 6,
                    'fecha_creacion' => $fecha_actual
                );
                $this->db->insert('actividad_usuarios',$data);
            }
            $comentarios_debate = $this->db->query("SELECT id_debate FROM comentarios WHERE id_usuario = $id_usuario")->num_rows();
            $comentarios_foto = $this->db->query("SELECT id_foto FROM comentarios_foto WHERE id_usuario = $id_usuario")->num_rows();
            $comentarios_video = $this->db->query("SELECT id_video FROM comentarios_video WHERE id_usuario = $id_usuario")->num_rows();
            $num_comentarios = $comentarios_debate + $comentarios_foto + $comentarios_video;
            switch ($num_comentarios){
                case 10:
                    $this->set_medallas($id_usuario, 8);
                    $this->set_medallas($id_usuario, 25);
                    break;
                case 20:
                    $this->set_medallas($id_usuario, 32);
                    $this->set_medallas($id_usuario, 26);
                    break;
                case 50:
                    $this->set_medallas($id_usuario, 9);
                    $this->set_medallas($id_usuario, 27);
                    break;
            }
            //---------ACTUALIZACION CAMPO COMENTARIOS DE FOTO PUBLICADA-----------------------
            $sql = "select comentarios from video_publicado where activo=1 and id_video=".$datos['video'];
            $result = $this->db->query($sql)->row();
            if (empty($result->comentarios)){
                $result->comentarios = 0;
            }
            $this->db->set($data = array('comentarios' => $result->comentarios + 1));
            $this->db->where("id_video", $datos['video']);
            $this->db->update("video_publicado");
            //---------PEDIR DATOS PARA PUBLICAR COMENTARIO-------------------------------------
            $sql = "SELECT
                        CV.id_usuario,
                        CV.fecha_creacion,
                        CV.id_comentario,
                        U.usuario,
                        U.id_usuario,
                        U.mime,
                        U.tipo_novia,
                        B.fecha_boda,
                        B.estado_boda,
                        B.id_boda,
                        C.id_usuario,
                        C.id_boda
                    FROM
                        video_publicado V
                    INNER JOIN comentarios_video CV USING (id_video)
                    INNER JOIN usuario U ON (U.id_usuario = CV.id_usuario)
                    INNER JOIN cliente C ON (C.id_usuario = U.id_usuario)
                    INNER JOIN boda B USING (id_boda)
                    WHERE
                        CV.id_comentario = $id_comentario
                    AND CV.activo = 1
                    AND CV.comentado = 0";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getComentariosVideo($id_video,$inicio,$limite){
            $id_usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT
                    CV.id_usuario,
                    CV.fecha_creacion,
                    CV.comentario,
                    CV.id_comentario,
                    U.usuario,
                    U.id_usuario,
                    U.mime,
                    U.tipo_novia,
                    (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS id_boda,
                    (SELECT id_comentario FROM denuncia_comentarios
                        WHERE id_comentario = CV.id_comentario AND tipo = 3 AND id_usuario = $id_usuario
                    ) AS id_denuncia
                    FROM
                        video_publicado V
                    INNER JOIN comentarios_video CV USING (id_video)
                    INNER JOIN usuario U on (U.id_usuario=CV.id_usuario)
                    WHERE
                        CV.id_video =$id_video
                    AND CV.activo = 1
                    AND CV.comentado = 0
                    ORDER BY
                        CV.fecha_creacion DESC
                    LIMIT 10 OFFSET  $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function respuestaComentarioVideo($datos){
            $data = array(
                'id_video' => $datos['video'],
                'id_usuario' => $this->session->userdata('id_usuario'),
                'comentario' => $datos['mensaje'],
                'comentado' => $datos['respuesta'],
                'activo' => 1,
                'fecha_creacion' => $datos['fecha_creacion'],
                'fecha_actualizacion' => $datos['fecha_creacion']
            );
            $this->db->insert("comentarios_video",$data);
            $id_comentario = $this->db->insert_id();
            $sql = "SELECT
                        CV.id_usuario as usuarioCO,
                        CV.fecha_creacion,
                        CV.id_comentario,
                        U.usuario,
                        U.id_usuario,
                        U.tipo_novia,
                        U.mime,
                        (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS id_boda
                    FROM
                        video_publicado V
                    INNER JOIN comentarios_video CV USING (id_video)
                    INNER JOIN usuario U ON (U.id_usuario = CV.id_usuario)
                    INNER JOIN cliente C ON (C.id_usuario = U.id_usuario)
                    INNER JOIN boda B USING (id_boda)
                    WHERE
                        CV.id_comentario = $id_comentario
                    AND CV.activo = 1
                    AND CV.comentado != 0 LIMIT 1";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getRespuestaVideo($id_comentario){
            $sql = "SELECT
                        CV.id_usuario as usuarioCO,
                        CV.fecha_creacion,
                        CV.id_comentario,
                        CV.comentado,
                        CV.comentario,
                        U.usuario,
                        U.id_usuario,
                        U.tipo_novia,
                        U.mime,
                        (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS id_boda
                    FROM
                        comentarios_video CV
                    INNER JOIN video_publicado V USING (id_video)
                    INNER JOIN usuario U on (U.id_usuario=CV.id_usuario)
                    WHERE
                        CV.activo = 1
                    AND CV.comentado = $id_comentario
                    ORDER BY CV.fecha_creacion DESC";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getRespuestaComentarioVideo($id_video){
            $sql = "SELECT
                    CV.id_usuario,
                    CV.fecha_creacion,
                    CV.comentario,
                    CV.id_comentario,
                    CV.comentado,
                    U.usuario,
                    U.id_usuario,
                    U.tipo_novia,
                    U.mime,
                    (SELECT boda.fecha_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS fecha_boda,
						(SELECT boda.estado_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS estado_boda,
						(SELECT boda.id_boda FROM boda JOIN cliente ON 
						cliente.id_boda = boda.id_boda WHERE cliente.id_usuario
						= CV.id_usuario) AS id_boda
                    FROM
                        comentarios_video CV
                    INNER JOIN video_publicado V USING (id_video)
                    INNER JOIN usuario U on (U.id_usuario=CV.id_usuario)
                    WHERE
                        CV.id_video = $id_video
                    AND CV.activo = 1
                    AND CV.comentado > 0
                    ORDER BY
                        CV.fecha_creacion DESC";
            $result = $this->db->query($sql)->result();
            return $result;
        }
                
        function getVideosRecientes(){
            $sql = "SELECT
                        V.id_video,
                        V.id_usuario,
                        V.id_grupo,
                        V.direccion_web,
                        V.descripcion,
                        V.titulo,
                        U.usuario
                    FROM
                        video_publicado V
                        INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
                    WHERE
                        V.activo=1
                    ORDER BY
                        V.fecha_creacion DESC
                    LIMIT 11;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getVideosVistos(){
            $sql = "SELECT
                V.id_video,
                V.id_usuario,
                V.id_grupo,
                V.direccion_web,
                V.descripcion,
                V.titulo,
                U.usuario
            FROM
                video_publicado V
                INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
            WHERE
                V.activo = 1
            AND	V.vistas > 0
            ORDER BY
                V.vistas DESC
            LIMIT
                11;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getVideosVistosGrupo($id_grupo){
            $sql = "SELECT
                V.id_video,
                V.id_usuario,
                V.id_grupo,
                V.direccion_web,
                V.descripcion,
                V.titulo,
                U.usuario
            FROM
                video_publicado V
                INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
            WHERE
                V.activo = 1
            AND	V.vistas > 0
            AND V.id_grupo = $id_grupo
            ORDER BY
                V.vistas DESC
            LIMIT
                11;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getTotalVideos(){
            $sql = "select activo from foto_publicada where activo=1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function getRecientes($inicio,$limite){
            $sql = "SELECT
                        V.id_video,
                        V.id_usuario,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.usuario
                    FROM
                        video_publicado V
                        INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
                    WHERE
                        V.activo=1
                    ORDER BY
                        V.fecha_creacion DESC
                    LIMIT 16 OFFSET  $inicio;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function  getVistos($inicio,$limite){
            $sql = "SELECT
                V.id_video,
                V.id_usuario,
                V.id_grupo,
                V.titulo,
                V.direccion_web,
                U.usuario
            FROM
                video_publicado V
                INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
            WHERE
                V.activo = 1
            AND	V.vistas > 0
            ORDER BY
                V.vistas DESC
            LIMIT
                16 OFFSET  $inicio;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function  getComentados($inicio,$limite){
            $sql = "SELECT
                V.id_video,
                V.id_usuario,
                V.id_grupo,
                V.titulo,
                V.direccion_web,
                U.usuario
            FROM
                video_publicado V
                INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
            WHERE
                V.activo = 1
            ORDER BY
                V.comentarios DESC
            LIMIT
                16 OFFSET  $inicio;";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function denuncia($video,$razon){
            $fecha_creacion = date("Y-m-d H:i:s");
            $usuario = $this->session->userdata('id_usuario');
            $sql = "SELECT id_usuario FROM denuncias_videos WHERE id_usuario = $usuario AND id_video = $video";
            $result = $this->db->query($sql)->row();
            if(empty($result)){
                $data = array(
                    'id_video' => $video,
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'razon' => $razon,
                    'fecha_creacion' => $fecha_creacion
                );
                $result = $this->db->insert("denuncias_videos",$data);
            }
            return $result;
        }
        
        function set_medallas($id_usuario,$id_medalla){
            $sql = "SELECT id_medalla FROM medallas_usuarios WHERE id_usuario = $id_usuario AND id_nueva_medalla = $id_medalla";
            $validar = $this->db->query($sql)->row();
            if(empty($validar)){
                $datos = array(
                    'id_usuario' => $id_usuario,
                    'id_nueva_medalla' => $id_medalla
                );
                $this->db->insert("medallas_usuarios",$datos);
            }
        }
        
        function numero_videos(){
            $id_usuario = $this->session->userdata('id_usuario');
            $resurlt = $this->db->query("SELECT id_video FROM video_publicado WHERE activo = 1 AND id_usuario = $id_usuario")->num_rows();
            return $resurlt;
        }
        
        function validarPuntos($puntos){
            if(!empty($puntos)){
                $id_usuario = $this->session->userdata('id_usuario');
                $fecha_actual = date('Y-m-d');
                $sql =  "SELECT 
                            B.fecha_boda 
                        FROM 
                            usuario U 
                        INNER JOIN cliente C USING (id_usuario) 
                        INNER JOIN boda B ON(B.id_boda = C.id_boda)
                        WHERE 
                            U.id_usuario = $id_usuario 
                        AND B.fecha_boda > '$fecha_actual'";
                $fecha_boda = $this->db->query($sql)->row();
                if($puntos >= 0 && $puntos <= 20){//NUEV@ NOVI@
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 1){
                        $this->db->set($data = array('tipo_novia' => 1));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 21 && $puntos <= 50){//NOVI@ PRINCIPIANTE
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 2){
                        $this->db->set($data = array('tipo_novia' => 2));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 51 && $puntos <= 100){//NOVI@ HABITUAL
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 3){
                        $this->db->set($data = array('tipo_novia' => 3));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 101 && $puntos <= 200){//NOVI@ TOP
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 4){
                        $this->db->set($data = array('tipo_novia' => 4));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 201 && $puntos <= 1000){//NOVI@ PRO
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 5){
                        $this->db->set($data = array('tipo_novia' => 5));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 1001 && $puntos <= 2000 && empty ($fecha_boda->fecha_boda)){//NOVI@ VIP
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 6){
                        $this->db->set($data = array('tipo_novia' => 6));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 2001 && $puntos <= 5000 && empty ($fecha_boda->fecha_boda)){//SUPER NOVI@ 
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 7){
                        $this->db->set($data = array('tipo_novia' => 7));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos >= 5001 && $puntos <= 10000 && empty ($fecha_boda->fecha_boda)){//DESTACAD@
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 8){
                        $this->db->set($data = array('tipo_novia' => 8));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }else if($puntos > 1000 && !empty ($fecha_boda->fecha_boda)){//NOVI@ LEYENDA
                    $result = $this->db->query("SELECT tipo_novia FROM usuario WHERE id_usuario = $id_usuario")->row();
                    if(!empty($result->tipo_novia) && $result->tipo_novia != 9){
                        $this->db->set($data = array('tipo_novia' => 9));
                        $this->db->where('id_usuario', $id_usuario);
                        $this->db->update('usuario');
                    }
                }
            }
        }
    }
