<?php

class Adjunto_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->id_tabla = "id_adjunto";
        $this->tabla = "adjunto";
    }

    public function getAdjuntos($id_correo) {
        $sql = "SELECT adjunto_correo.*,adjunto.nombre,adjunto.id_archivo
                FROM correo 
                inner join adjunto_correo using(id_correo)
                inner join adjunto using(id_adjunto) 
               where id_correo=?";
        return $this->db->query($sql, array($id_correo))->result();
    }

    public function has_permiso_download($id_adjunto, $id_usuario) {
        $sql = "SELECT *
                FROM correo
                inner join adjunto_correo using(id_correo)
                inner join adjunto using(id_adjunto)
               where id_adjunto=? and( `to`=? or `from`=?) limit 1";
        return $this->db->query($sql, array($id_adjunto, $id_usuario, $id_usuario))->row() ? true : false;
    }

}
