<?php

class Correo_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_correo";
        $this->tabla    = "correo";
        $ci             = get_instance();
        $ci->load->library("My_PHPMailer");
    }

    public function getEntrada($to, $who, $page = 1, $leido = 0, $palabra = null, $inicio = null, $fin = null, $tipo = null)
    {
        $limit = $this->limit($page);
        if ($palabra != null) {
            $palabra = " and (usuario.nombre like '%$palabra%' or  usuario.apellido like '%$palabra%' or correo.mensaje like '%$palabra%' ) ";
        } else {
            $palabra = "";
        }

        if ($inicio && $fin && $tipo) {
            if ($tipo == "boda") {
                $tipo = "boda.fecha_boda";
            } else {
                $tipo = "correo.fecha_creacion";
            }
            $fecha = " and ( $tipo BETWEEN '$inicio' and '$fin' ) ";
        } else {
            $fecha = "";
        }

        $sql = "SELECT usuario.nombre,usuario.apellido,boda.fecha_boda,boda.no_invitado,correo.*  FROM correo"
            ." INNER JOIN usuario ON (id_usuario =  correo.`from`)
                    INNER JOIN cliente USING (id_usuario)
                    INNER JOIN boda USING (id_boda)"
            ." where correo.`to`=$to and leida_proveedor=$leido $palabra $fecha "
            ." order by correo.fecha_creacion desc $limit";

        return $this->db->query($sql)->result();
    }

    public function getEntradaEstado(
        $to,
        $page = 1,
        $estado = 0,
        $palabra = null,
        $inicio = null,
        $fin = null,
        $tipo = null
    ) {
        $limit = $this->limit($page);
        if ($palabra != null) {
            $palabra = " and (usuario.nombre like '%$palabra%' or  usuario.apellido like '%$palabra%' or correo.mensaje like '%$palabra%' ) ";
        } else {
            $palabra = "";
        }
        if ($inicio && $fin && $tipo) {
            if ($tipo == "boda") {
                $tipo = "boda.fecha_boda";
            } else {
                $tipo = "correo.fecha_creacion";
            }
            $fecha = " and ( $tipo BETWEEN '$inicio' and '$fin' ) ";
        } else {
            $fecha = "";
        }
        $sql = "SELECT    usuario.nombre,usuario.apellido,boda.fecha_boda,boda.no_invitado,correo.* FROM correo"
            ." INNER JOIN usuario ON (id_usuario =  correo.`from`)"
            ." inner join cliente using(id_usuario)"
            ." inner join boda using(id_boda)"
            ." where correo.`to`=$to and correo.estado=$estado  $palabra $fecha "
            ." order by correo.fecha_creacion desc $limit";

        return $this->db->query($sql)->result();
    }

    private function limit($page)
    {
        if ($page == 1) {
            return " limit 15";
        } else {
            return " limit   ".(($page - 1) * 15).",15 ";
        }
    }

    public function getCount($id_usuario, $leido = 0)
    {
        $sql = "SELECT id_correo,parent FROM correo where `to`=$id_usuario and leida_proveedor=$leido";

        $mailIds = [];

        foreach ($this->db->query($sql)->result() as $row) {
            $mailIds[] = $row->parent != null ? $row->parent : $row->id_correo;
        }

        $mailIds = array_unique($mailIds);

        return count($mailIds);
    }

    public function getCountEstado($id_usuario, $estado = null)
    {
        if ($estado === null) {
            $sql = "SELECT count(id_correo) as total FROM correo where `to`=$id_usuario and parent is null";
        } elseif ($estado >= 0 && $estado < 4) {
            $sql = "SELECT count(id_correo) as total FROM correo where `to`=$id_usuario and estado=$estado and parent is null";
        }

        return $this->db->query($sql)->row()->total;
    }

    public function getPendientes($id_usuario, $numero)
    {
        $sql
            = "select nombre,apellido,correo.* from correo 
                inner join usuario on(correo.`from`=usuario.id_usuario)
                inner join cliente using(id_usuario)
                where `to`=$id_usuario and correo.estado=0 order by correo.fecha_creacion limit $numero ;";

        return $this->db->query($sql)->result();
    }

    public function updateEstado($ids, $estado)
    {
        try {
            $data = [];
            foreach ($ids as $key => $id) {
                $data[] = [
                    "id_correo" => $id,
                    "estado"    => $estado,
                ];
            }
            $this->db->update_batch($this->tabla, $data, $this->id_tabla);

            return true;
        } catch (Exception $e) {

        }

        return false;
    }

    public function updateLeida($ids, $who, $estado)
    {
        try {
            $data = [];
            foreach ($ids as $key => $id) {
                $data[] = [
                    "id_correo"                                          => $id,
                    $who == 'user' ? "leida_usuario" : "leida_proveedor" => $estado,
                ];
            }
            $this->db->update_batch($this->tabla, $data, $this->id_tabla);

            return true;
        } catch (Exception $e) {

        }

        return false;
    }

    public function getSolicitudesTime($inicio, $fin, $id_proveedor)
    {
        $sql
            = "SELECT solicitud.*, usuario.nombre,usuario.apellido,usuario.correo,boda.no_invitado,fecha_boda
                    FROM solicitud 
                    inner join boda using(id_boda) 
                    inner join cliente on(boda.id_boda=cliente.id_boda) 
                    inner join usuario on(cliente.id_usuario=usuario.id_usuario)
                    where solicitud.id_proveedor=? and solicitud.fecha_creacion between ? and ?";

        return $this->db->query($sql, [$id_proveedor, $inicio, $fin])->result();
    }

    public function getMensajes($id_correo)
    {
        $sql = "SELECT correo.* FROM correo where parent=?;";

        return $this->db->query($sql, [$id_correo])->result();
    }

    // SECCION DE SOLICITUDES NOVIOS;
    public function getEnviadosCliente($id_usuario, $page = 1)
    {
        $limit = $this->limit($page);
        $sql
               = "select correo.*,fecha_boda,no_invitado from correo
            inner join usuario on(id_usuario=`from`)
                inner join cliente using(id_usuario)
                inner join boda using(id_boda)
                where visible=1 and `from`=? and parent is null order by correo.fecha_creacion desc $limit ";

        return $this->db->query($sql, [$id_usuario])->result();
    }

    public function getCountEnviadosCliente($id_usuario)
    {
        $sql
            = "select count(id_correo) as total from correo
            inner join usuario on(id_usuario=`from`)
                where visible=1 and `from`=? and parent is null  ";

        return $this->db->query($sql, [$id_usuario])->row()->total;
    }

    public function getCountBandejaEntrada($who, $id_cliente)
    {
        if ($who == 'user') {
            $sql = "SELECT count(*) as total FROM correo where `to`=? and leida_usuario=0";
        } else {
            $sql = "SELECT count(*) as total FROM correo where `to`=? and leida_proveedor=0";
        }

        return $this->db->query($sql, [$id_cliente])->row()->total;
    }

    public function getBandejaEntrada($id_usuario, $page = 1)
    {
        $limit = $this->limit($page);

        $sql = "select *, correo.fecha_creacion as 'created_at' from correo left join usuario on(correo.from=id_usuario) where correo.`to`=? and visible=1 $limit";

        return $this->db->query($sql, [$id_usuario])->result();
    }

    public function insertArchivo($id_adjunto, $id_correo)
    {
        try {
            return $this->db->insert("adjunto_correo", ["id_adjunto" => $id_adjunto, "id_correo" => $id_correo]);
        } catch (Exception $e) {
            return false;
        }
    }

    public function mendallaExtrovertida($id_usuario)
    {
        try {
            return $this->db->insert('medallas_usuarios', [
                    'id_usuario'       => $id_usuario,
                    'id_nueva_medalla' => 16,
                ]
            );
        } catch (Exception $e) {
            return false;
        }
    }

    public function isMine($id_correo, $id_usuario)
    {
        $sql = 'SELECT * FROM correo WHERE id_correo=? and ( correo.`to`=? or correo.`from` =?) limit 1;';
        $t   = $this->db->query($sql, [$id_correo, $id_usuario, $id_usuario])->row();
        if ($t) {
            return true;
        }

        return false;
    }

    public function sendEmail()
    {
        $mailer = $this->my_phpmailer;

        $mail = Mail::get()->last();
        if ($mail) {
            $userTo   = User::find($mail->to);
            $userFrom = User::find($mail->from);
            if ($userTo && $userFrom) {
                //if the bride sent a message
                if ($userTo->provider) {
                    $data["bride"]                       = $userFrom->nombre." ".$userFrom->apellido;
                    $data["user"]["telephone"]           = $userFrom->client->telefono;
                    $data["user"]["email"]               = $userFrom->correo;
                    $data["user"]["wedding"]["date"]     = $userFrom->client->wedding->fecha_boda;
                    $data["user"]["wedding"]["noGuests"] = $userFrom->client->wedding->no_invitado;
                    $data["user"]["subject"]             = $mail->asunto;
                    $data["mail"]["sentAt"]              = $mail->fecha_creacion;
                    $data["mail"]["message"]             = $mail->mensaje;
                    $data["mail"]["parent"]              = $mail->parent ? $mail->parent : $mail->id_correo;
                    $data["correo"]                      = $userTo->correo;
                    $data["user"]["name"]                = $userTo->nombre;
                    $data["rol"]                         = $userFrom->rol;

//                    $providerContactEmails = $userTo->provider->contacto_correos;
//
//                    if ($providerContactEmails) {
//                        $providerContactEmails = explode("", "", preg_replace("/\s+/", "", $providerContactEmails));
//                    }

                    $providerContactEmails[] = $userTo->correo;

                    foreach ($providerContactEmails as $key => $email) {
                        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $data["sentTo"] = $email;
                            $mailer->to($email, $email);
                            $mailer->setSubject($mail->asunto);
                            $mailer->send($this->load->view("templates/email_template", $data, true));
                        }
                    }
                } else {
                    //if the provider sent a response
                    $data["user"]["name"]      = $userFrom->provider->nombre;
                    $data["user"]["email"]     = $userFrom->correo;
                    $data["user"]["telephone"] = $userFrom->provider->contacto_telefono;
                    $data["user"]["cellphone"] = $userFrom->provider->contacto_celular;
                    $data["user"]["id"]        = $userFrom->provider->id_usuario;
                    $data["mail"]["sentAt"]    = $mail->fecha_creacion;
                    $data["mail"]["message"]   = $mail->mensaje;
                    $data["mail"]["parent"]    = $mail->parent ? $mail->parent : $mail->id_correo;
                    $data["sentTo"]            = $userTo->correo;
                    $data["phone"]             = Client::where("id_usuario", $userTo->id_usuario)->first()->telefono;
                    $data["bride"]             = $userTo->nombre." ".$userTo->apellido;
                    $data["brideId"]           = $userTo->id_usuario;
                    $data["rol"]               = $userFrom->rol;

                    $mailer->to($data["sentTo"], $data["sentTo"]);
                    $mailer->setSubject($mail->asunto);
                    $mailer->send($this->load->view("templates/email_template", $data, true));
                }
            }
        }
    }

    public function sendNotification($userId, $provider, $template)
    {
        $templates = [
            "first_visit"  => ["path" => "templates/email_first_visit", "subject" => "Usuario ha visitado tu escaparate por primera vez"],
            "request_info" => ["path" => "templates/email_request_info", "subject" => "Un usuario ha solicitado informacion"],
        ];

        $mailer = $this->my_phpmailer;

        $bride    = User::with('client.wedding')->find($userId);
        $provider = User::find($provider->id_usuario);
        $mail     = Mail::get()->last();

        if ($bride && $provider && count($templates[$template])) {
            $data["name"]         = $bride->nombre." ".$bride->apellido;
            $data["email"]        = $bride->correo;
            $data["phone"]        = $bride->client->telefono;
            $data["sentTo"]       = $provider->correo;
            $data["providerName"] = $provider->nombre;
            $data["date"]         = $bride->client->wedding->fecha_boda;
            $data["guests"]       = $bride->client->wedding->no_invitado;
            $data["city"]         = $bride->client->wedding->ciudad_boda;
            $data["budget"]       = ($provider->tipo == 3)? $bride->client->wedding->presupuesto: " ";
            $data["mail"]         = $mail;

            $mailer->to($data["sentTo"], $data["sentTo"]);
            $mailer->setSubject($templates[$template]["subject"]);
            $mailer->send($this->load->view($templates[$template]["path"], $data, true));
        }
    }

    public function sendWeddingReminder($userId = null, $template = null)
    {
        $templates = [
            "2_months"  => ["path" => "templates/reminders/2_months", "subject" => "Faltan 2 meses para tu boda!"],
            "4_months"  => ["path" => "templates/reminders/4_months", "subject" => "Faltan 4 meses para tu boda!"],
            "7_months"  => ["path" => "templates/reminders/7_months", "subject" => "Faltan 7 meses para tu boda!"],
            "12_months" => ["path" => "templates/reminders/12_months", "subject" => "Faltan 12 meses para tu boda!"],
            "1_weeks"   => ["path" => "templates/reminders/1_weeks", "subject" => "Falta 1 semana para tu boda!"],
            "2_weeks"   => ["path" => "templates/reminders/2_weeks", "subject" => "Faltan 2 semanas para tu boda!"],
        ];

        $mailer = $this->my_phpmailer;

        $user = User::find($userId);

        if ($user && count($templates[$template])) {
            $data["name"]  = $user->nombre." ".$user->apellido;
            $data["email"] = $user->correo;

            $mailer->to($data["email"], $data["name"]);
            $mailer->setSubject($templates[$template]["subject"]);
            $mailer->send($this->load->view($templates[$template]["path"], $data, true));
        }
    }

}
