<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DefaultPorcentaje_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla = "default_porcentaje";
        $this->id_tabla = "id_default_porcentaje";
    }

    public function getCategorias()
    {
        $cate = $this->getList(array('id_parent' => null));
        $result = array();
        foreach ($cate as $parent) {
            $mas = array();
            $de = $this->getList(array('id_parent' => $parent->id_default_porcentaje));
            foreach ($de as $value) {
                $mas[$value->nombre] = $value->porcentaje;
            }
            $result[$parent->nombre] = $mas;
        }
        return $result;
    }

    public function getCategoriasFull()
    {
        $categorias = $this->getList(['id_parent' => null]);
        foreach ($categorias as $categoria) {
            $subcategorias = $this->getList(['id_parent' => $categoria->id_default_porcentaje]);
            $categoria->subcategorias = $subcategorias;
        }
        return $categorias;
    }

    public function getCategoria($nombre)
    {
        $sql = "SELECT * FROM default_porcentaje where nombre='$nombre' and id_parent is not null ";
        $t = $this->db->query($sql)->row();
        if ($t) {
            return $t->id_parent;
        }
//        var_dump($sql);
        return null;
    }

    public function getCategoriaPrincipal($nombre)
    {
        $sql = "SELECT * FROM default_porcentaje WHERE LOWER(nombre)=LOWER(?) AND id_parent IS NULL ";
        $t = $this->db->query($sql, $nombre)->row();
        if ($t) {
            $t->categorias = $this->getList(array('id_parent' => $t->id_default_porcentaje));
            return $t;
        }
        return null;
    }

    public function getDefaults()
    {
        $cate = $this->getList(array('id_parent' => null));
        foreach ($cate as &$parent) {
            $parent->categorias = $this->getList(array('id_parent' => $parent->id_default_porcentaje));
        }
        return $cate;
    }

    public function getCategoriaPresupuesto($id_categoria_proveedor)
    {
        $sql = "SELECT * FROM default_porcentaje where enlace_proveedores=$id_categoria_proveedor and id_parent is not null limit 1 ";
        $t = $this->db->query($sql)->row();
        if ($t) {
            return $t->id_parent;
        }
        return 6;
    }

}
