<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Destino_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id";
        $this->tabla    = "destinations";
        ini_set('memory_limit', '-1');
    }

    function make_query()  
    {  
        if(!empty($_POST["search"]["value"]))  
        {  
            if(strlen($_POST["search"]["value"]) < 4)
                return $sql = 'SELECT * FROM destinations WHERE (destinations.name LIKE \''.$_POST["search"]["value"].'%\' OR destinations.description LIKE \''.$_POST["search"]["value"].'%\') ;';
            else
                return $sql = 'SELECT * FROM destinations WHERE (destinations.name LIKE \'%'.$_POST["search"]["value"].'%\' OR destinations.description LIKE \'%'.$_POST["search"]["value"].'%\') ;';
        }  
        if(!empty($_POST['order']) && (!empty($_POST['order']['0']['column']) || $_POST['order']['0']['column'] >= 0 ) && !empty($_POST['order']['0']['dir']))  
        {  
            $order = 'destinations.name';    
            if($_POST['order']['0']['column'] == 0)
                $order = 'destinations.id';
            if($_POST['order']['0']['column'] == 1)
                $order = 'destinations.name';
            if($_POST['order']['0']['column'] == 2)
                $order = 'destinations.description';
            return $sql = 'SELECT * FROM destinations ORDER BY '.$order.' '.$_POST['order']['0']['dir'].' LIMIT '.$_POST['start'].','.$_POST['length'].';';
        }  
        else  
        {  
            return $sql = 'SELECT * FROM destinations LIMIT '.$_POST['start'].','.$_POST['length'].';';
        }  
    } 

    function make_datatables(){  
        $sql = $this->make_query();  
        if($_POST["length"] != -1)  
        {  
                $this->db->limit($_POST['length'], $_POST['start']);  
        }  
        $query = $this->db->query($sql); 
        return $query->result();  
    }  

    function get_all_data()  
    {  
        $this->db->select("*");  
        $this->db->from($this->tabla);  
        return $this->db->count_all_results();  
    }  

    function get_filtered_data(){  
        $this->make_query();  
        $sql = 'SELECT id,name FROM destinations ;';
        $query = $this->db->query($sql);
        return $query->num_rows();  
    }
}