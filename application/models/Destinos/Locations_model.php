<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Locations_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id";
        $this->tabla    = "locations_attractives";
        ini_set('memory_limit', '-1');
    }

    function make_query()  
    {  
        if(!empty($_POST["search"]["value"]))  
        {  
            if(strlen($_POST["search"]["value"]) < 4)
                return $sql = 'SELECT * FROM locations_attractives WHERE locations_attractives.id_destinations='.$_POST["idDestino"].' AND (locations_attractives.name LIKE \''.$_POST["search"]["value"].'%\' OR locations_attractives.type LIKE \''.$_POST["search"]["value"].'%\') ;';
            else
                return $sql = 'SELECT * FROM locations_attractives WHERE locations_attractives.id_destinations='.$_POST["idDestino"].' AND (locations_attractives.name LIKE \'%'.$_POST["search"]["value"].'%\' OR locations_attractives.type LIKE \'%'.$_POST["search"]["value"].'%\') ;';
        }  
        if(!empty($_POST['order']) && (!empty($_POST['order']['0']['column']) || $_POST['order']['0']['column'] >= 0 ) && !empty($_POST['order']['0']['dir']))  
        {  
            $order = 'locations_attractives.name';    
            if($_POST['order']['0']['column'] == 0)
                $order = 'locations_attractives.id';
            if($_POST['order']['0']['column'] == 1)
                $order = 'locations_attractives.name';
            if($_POST['order']['0']['column'] == 2)
                $order = 'locations_attractives.type';
            return $sql = 'SELECT * FROM locations_attractives WHERE locations_attractives.id_destinations='.$_POST["idDestino"].' ORDER BY '.$order.' '.$_POST['order']['0']['dir'].' LIMIT '.$_POST['start'].','.$_POST['length'].';';
        }  
        else  
        {  
            return $sql = 'SELECT * FROM locations_attractives WHERE locations_attractives.id_destinations='.$_POST["idDestino"].' LIMIT '.$_POST['start'].','.$_POST['length'].';';
        }  
    } 

    function make_datatables(){  
        $sql = $this->make_query();  
        if($_POST["length"] != -1)  
        {  
                $this->db->limit($_POST['length'], $_POST['start']);  
        }  
        $query = $this->db->query($sql); 
        return $query->result();  
    }  

    function get_all_data()  
    {  
        $this->db->select("*");  
        $this->db->from($this->tabla);  
        return $this->db->count_all_results();  
    }  

    function get_filtered_data(){  
        $this->make_query();  
        $sql = 'SELECT id,name FROM locations_attractives WHERE locations_attractives.id_destinations='.$_POST["idDestino"].';';
        $query = $this->db->query($sql);
        return $query->num_rows();  
    }
}