<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Album extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->id_tabla = "id_album";
        $this->tabla = "galery_album";
        
//        $this->id_tabla = "id_vestido";
//        $this->tabla = "vestido";
    }
    
    public function getFotoPrincipal($album){
        $query = "SELECT * FROM galery_photo as p join galery_album as a on a.id_album = p.id_album"
                ."where (a.id_album = $album) and (p.tipo = 0)";
        return $this->db->query($query, array($album))->result();
    }
    
    public function getAlbum($tipo_producto) {
        $query = "select nombrealbum, count(id_photo) as total from galery_album"
                . " where id_album = 1 group by nombrealbum";
        return $this->db->query($query, array($tipo_producto))->result();
    }
    
    public function getPhotos($id_photo) {
        $sql = "SELECT * FROM galery_photo where id_album = $id_photo";
        $r = $this->db->query($sql)->row();
        return $r;
    }
    
       public function getCatalogoAlbum($tipo_producto, $param = array(), $pag = 1) {
        $where = "";
        foreach ($param as $key => $value) {
            if ($key == "disenador" || $key == "diseñador") {
                $where.=" disenador='" . str_replace("-", " ", $value) . "' and ";
            } else {
                $where.=" $key='$value' and ";
            }
        }
        $where .= " tipo_vestido=$tipo_producto ";
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        $query = "SELECT vestido.* FROM  vestido  "
                . " where $where  $limit ;";
        return $this->db->query($query, array($tipo_producto))->result();
    }
    
        public function getAlbumDestacados($tipo_producto, $pag = 1) {
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        $sql = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)
                WHERE
                        tipo_vestido = $tipo_producto
                group by disenador
                order by likes desc,comentarios desc,temporada desc
                $limit ;";
        return $this->db->query($sql)->result();
    }
/*-----------------------------------------------------------------------------------
 */
    public function getDisenadores($tipo_producto) {
        $query = "SELECT disenador,count(id_vestido) as total FROM  vestido  "
                . " where tipo_vestido=? group by disenador";
        return $this->db->query($query, array($tipo_producto))->result();
    }

    public function getTemporadas($tipo_producto) {
        $query = "SELECT temporada,count(id_vestido) as total FROM  vestido  "
                . " where tipo_vestido=? group by temporada";
        return $this->db->query($query, array($tipo_producto))->result();
    }

    public function getCatalogo($tipo_producto, $param = array(), $pag = 1) {
        $where = "";
        foreach ($param as $key => $value) {
            if ($key == "disenador" || $key == "diseñador") {
                $where.=" disenador='" . str_replace("-", " ", $value) . "' and ";
            } else {
                $where.=" $key='$value' and ";
            }
        }
        $where .= " tipo_vestido=$tipo_producto ";
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        $query = "SELECT vestido.* FROM  vestido  "
                . " where $where  $limit ;";
        return $this->db->query($query, array($tipo_producto))->result();
    }

	    public function getVestido($id_vestido) {
        $sql = "SELECT * FROM vestido where id_vestido = $id_vestido";
        $r = $this->db->query($sql)->row();
        return $r;
    }
	
	
    public function getTotalCatalogo($tipo_producto, $param = array()) {
        $where = "";
        foreach ($param as $key => $value) {
            if ($key == "disenador" || $key == "diseñador") {
                $where.=" disenador='" . str_replace("-", " ", $value) . "' and ";
            } else {
                $where.=" $key='$value' and ";
            }
        }
        $where .= " tipo_vestido=$tipo_producto ";
        $query = "SELECT count(*) as total FROM  vestido   where $where  ;";
        $r = $this->db->query($query, array($tipo_producto))->row();
        if ($r) {
            return $r->total;
        }
        return 0;
    }

    public function getDestacadosMin($tipo_producto, $total = 3) {
        $limit = " limit $total";
        $sql = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)
                WHERE
                        tipo_vestido = $tipo_producto
                group by disenador
                order by likes desc,comentarios desc,temporada desc
                $limit ;";
        return $this->db->query($sql)->result();
    }

    public function getDestacados($tipo_producto, $pag = 1) {
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        $sql = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)
                WHERE
                        tipo_vestido = $tipo_producto
                group by id_vestido
                order by likes desc,comentarios desc,temporada desc
                $limit ;";
        return $this->db->query($sql)->result();
    }

    public function getDisenadoresDestacados($tipo_producto, $pag = 1) {
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        $sql = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)
                WHERE
                        tipo_vestido = $tipo_producto
                group by disenador
                order by likes desc,comentarios desc,temporada desc
                $limit ;";
        return $this->db->query($sql)->result();
    }

    public function getDisenadoresAll($tipo_producto, $pag = 1) {
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        $sql = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)
                WHERE
                        tipo_vestido = $tipo_producto
                group by disenador
                order by likes desc,comentarios desc,temporada desc
                $limit ;";
        return $this->db->query($sql)->result();
    }

    public function getAllDisenador($nombre, $pag = 1) {
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        $sql = "SELECT * From vestido where disenador=?  order by nombre asc $limit";
        return $this->db->query($sql, array($nombre))->result();
    }

    public function getTotalAllDisenador($nombre, $pag = 1) {
        $sql = "SELECT count(*) as total From vestido where disenador=? ";
        $r = $this->db->query($sql, array($nombre))->row();
        if ($r)
            return $r->total;
        return 0;
    }

    public function getTemporadasDisenador($nombre) {
        $sql = "SELECT temporada,count(id_vestido) as total From vestido where disenador=? group by temporada";
        return $this->db->query($sql, array($nombre))->result();
    }

    public function existsDisenador($nombre_disenador) {
        $nombre = urldecode($nombre_disenador);
        $nombre = str_replace("-", " ", $nombre);
        $sql = "SELECT disenador FROM vestido where disenador =? limit 1 ";
        return $this->db->query($sql, array($nombre))->row() ? true : false;
    }

    public function countDisenador($nombre_disenador) {
        $sql = "SELECT count(id_vestido) as total FROM vestido where disenador =? group by disenador ";
        $r = $this->db->query($sql, array($nombre_disenador))->row();
        return $r ? $r->total : 0;
    }

    public function countComentarios($id_vestido) {
        $sql = "SELECT count(id_vestido) as total FROM vestido inner join vestido_comentario using(id_vestido)
                where id_vestido=$id_vestido
                 group by id_vestido";
        $r = $this->db->query($sql)->row();
        if ($r)
            return $r->total;
        return 0;
    }

    public function countLikes($id_vestido) {
        $sql = "SELECT count(id_vestido) as total FROM vestido inner join vestido_usuario using(id_vestido)
                    where id_vestido=$id_vestido
                     group by id_vestido";
        $r = $this->db->query($sql)->row();
        if ($r)
            return $r->total;
        return 0;
    }

    public function isLikeForMe($id_cliente, $id_vestido) {
        $sql = "select * from  vestido_usuario where id_vestido=? and id_cliente=?;";
        $l = $this->db->query($sql, array($id_vestido, $id_cliente))->row();
        if ($l)
            return TRUE;
        return FALSE;
    }

    public function misVestidoGroup($id_cliente) {
        $sql = "SELECT
                        tipo_vestido.*, count(id_tipo_vestido) AS total
                FROM
                        vestido
                INNER JOIN vestido_usuario USING (id_vestido)
                INNER JOIN tipo_vestido ON (id_tipo_vestido = tipo_vestido)
                WHERE
                        id_cliente = $id_cliente
                GROUP BY
                        tipo_vestido";
        return $this->db->query($sql)->result();
    }

    public function misVestidos($id_cliente, $id_tipo = null, $l = null) {
        $limit = "";
        if ($l) {
            $limit = " limit 0,$l";
        }
        if ($id_tipo) {
            $sql = "SELECT * FROM $this->tabla inner join vestido_usuario using(id_vestido) where id_cliente=$id_cliente and tipo_vestido=$id_tipo $limit";
        } else {
            $sql = "SELECT * FROM $this->tabla inner join vestido_usuario using(id_vestido) where id_cliente=$id_cliente $limit";
        }
        return $this->db->query($sql)->result();
    }

}
