<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class VestidoComentario_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->id_tabla = "id_comentario";
        $this->tabla = "galery_coment";
    }

    public function getComentariosArticulosInit($id_fotografia) {
        $sql = "SELECT c.*,sum( case when t.id_comentario is null then 0 else 1 end) as comentarios"
                . " FROM $this->tabla as c left join $this->tabla as t on(c.id_comentario=t.parent and c.id_photo=$id_fotografia and  c.parent is null  ) "
                . " where c.parent is null and  c.id_photo=$id_fotografia group by c.id_comentario "
                . " order by c.fecha desc ";
        return $this->db->query($sql)->result();
    }

    public function getUsuario($id_usuario) {
        $sql = "SELECT id_usuario,nombre,apellido,correo,conectado FROM usuario where id_usuario=? ";
        return $this->db->query($sql,array($id_usuario))->row();
    }

}
