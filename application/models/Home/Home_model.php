<?php

class Home_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
    }

    function get($id_imagen = NULL) {
        try {
            if ($id_imagen == NULL) {
                $result = $this->db->query("SELECT * FROM home_imagen")->result();
                if (count($result) > 0) {
                    foreach ($result as $key => $value) {
                        $value->id_imagen = encrypt($value->id_imagen);
                    }
                }
                return $result;
            } else {
                $sql = "SELECT * FROM home_imagen where id_imagen=?";
                $result = $this->db->query($sql, array(decrypt($id_imagen)))->row();
                if ($result) {
                    $result->id_imagen = encrypt($result->id_imagen);
                    return $result;
                }
            }
        } catch (Exception $e) {
            
        }
        return false;
    }

    function insert($data) {
        try {
            $this->db->trans_start();
            $this->db->insert('home_imagen', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            return encrypt($id);
        } catch (Exception $e) {
            return false;
        }
    }

    function delete($id) {
        try {
            $this->db->where('id_imagen', decrypt($id));
            $this->db->delete('home_imagen');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}
