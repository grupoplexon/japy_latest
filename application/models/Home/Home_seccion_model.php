<?php

class Home_seccion_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
    }

    function get()
    {
        try {
            $sql    = "SELECT * FROM home_seccion ORDER BY tipo,id_home_seccion";
            $result = $this->db->query($sql)->result();
            if (count($result) > 0) {
                foreach ($result as $key => $value) {
                    $value->id_home_seccion = encrypt($value->id_home_seccion);
                }
            }

            return $result;
        } catch (Exception $e) {
            return false;
        }
    }

    function getTipo($tipo)
    {
        try {
            $sql    = "SELECT * FROM home_seccion WHERE tipo == $tipo";
            $result = $this->db->query($sql)->result();
            if (count($result) > 0) {
                foreach ($result as $key => $value) {
                    $value->id_home_seccion = encrypt($value->id_home_seccion);
                }
            }

            return $result;
        } catch (Exception $e) {
            return false;
        }
    }

    function insert($data)
    {
        try {
            $this->db->trans_start();
            $this->db->insert('home_seccion', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();

            return encrypt($id);
        } catch (Exception $e) {
            return false;
        }
    }

    function update($datos, $id)
    {
        try {
            $this->db->where('id_home_seccion', decrypt($id));

            return $this->db->update('home_seccion', $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    function delete($id)
    {
        try {
            $this->db->where('id_home_seccion', decrypt($id));
            $this->db->delete('home_seccion');

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}