<?php

class Acompanante_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
    }
    
    function insertAcompananteBatch($data) {
        $this->db->insert_batch('acompanante', $data);
        return true;
    }
    
    function insertAcompanante($data) {
        $this->db->trans_start();
        $data['id_invitado'] = decrypt($data['id_invitado']);
        $data['id_acompanante'] = decrypt($data['id_acompanante']);
        $this->db->insert('acompanante', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return $id;
    }
    
    function deleteAcompanante($id_invitado){
        $id_invitado = decrypt($id_invitado);
        $this->db->where('id_invitado ', $id_invitado);
        $this->db->or_where('id_acompanante', $id_invitado);
        $this->db->delete('acompanante'); 
        return TRUE;
    }
    
    function deleteAcompananteinvitado($id_invitado,$id_acompanante){
        $id_invitado = decrypt($id_invitado);
        $id_acompanante = decrypt($id_acompanante);
        $this->db->where('id_invitado ', $id_invitado);
        $this->db->where('id_acompanante', $id_acompanante);
        $this->db->delete('acompanante'); 
        return true;
    }
    
}