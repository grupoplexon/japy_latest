<?php

class Confirmacion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insert($data) {
        $this->db->insert('confirmacion', $data);
        return true;
    }

    function delete($codigo) {
        $this->db->where('codigo', $codigo);
        $this->db->delete('confirmacion');
    }

    function get($codigo) {
        $sql = "SELECT id_invitado FROM confirmacion WHERE codigo = '$codigo'";
        $result = $this->db->query($sql);
        $result = $result->row();
        return $result;
    }

    function updateInvitado($datos, $id) {
        try {
            $this->db->where('id_invitado', $id);
            return $this->db->update('invitado', $datos);
        } catch (Exception $e) {
            return false;
        }
    }

}
