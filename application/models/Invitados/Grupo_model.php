<?php

class Grupo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insertGrupoBatch($data)
    {
        $this->db->insert_batch('grupo', $data);
        return true;
    }

    function insertGrupo($data)
    {
        $this->db->insert('grupo', $data);
        return true;
    }

    public function updateGrupo($datos, $id)
    {
        try {
            $id = decrypt($id);
            $this->db->where('id_grupo', $id);
            return $this->db->update('grupo', $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteGrupo($id)
    {
        try {
            $id = decrypt($id);
            $this->db->where('id_grupo', $id);
            $this->db->update('invitado', array("id_grupo" => null));

            $this->db->where('id_grupo', $id);
            return $this->db->delete('grupo');
        } catch (Exception $e) {
            return false;
        }
    }

    function getGrupoCount()
    {
        $sql = "SELECT grupo.id_grupo,grupo.grupo, count(invitado.id_grupo) AS total
                FROM grupo LEFT JOIN invitado USING(id_grupo)
                WHERE grupo.id_boda = " . $this->session->userdata("id_boda") . "
                GROUP BY id_grupo";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_grupo = encrypt($value->id_grupo);
        }
        return $result;
    }

    function getGrupos()
    {
        $sql = "SELECT * FROM grupo WHERE id_boda = " . $this->session->userdata("id_boda");
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_grupo = encrypt($value->id_grupo);
            $value->id_boda = encrypt($value->id_boda);
        }
        return $result;
    }

    function getGrupoNovios($id_boda)
    {
        $sql = "SELECT id_grupo FROM grupo WHERE grupo = 'Novios' AND id_boda = $id_boda";
        $result = $this->db->query($sql);
        $result = $result->row();
        return $result;
    }

    function getNumeroGrupo()
    {
        $sql = "SELECT grupo.*, count(id_invitado) as total "
            . "FROM grupo LEFT JOIN invitado USING(id_grupo) "
            . "WHERE grupo.id_boda = " . $this->session->userdata('id_boda') . " "
            . "GROUP BY id_grupo ORDER BY id_grupo;";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_grupo = encrypt($value->id_grupo);
            $value->id_boda = encrypt($value->id_boda);
        }
        return $result;
    }
}