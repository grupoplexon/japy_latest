<?php

class Invitacion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
    }
    function insertInvitacionBatch($data) {
        $this->db->insert_batch('invitacion', $data);
        return true;
    }
    
    function insertInvitacion($data) {
        $this->db->trans_start();
        $this->db->insert('invitacion', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return encrypt($id);
    }
    
    public function updateInvitacion($datos,$id) {
        try {
            $id = decrypt($id);
            $this->db->where('id_invitacion',$id);
            return $this->db->update('invitacion', $datos);
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function deleteInvitacion($id){
        try {
            $id = decrypt($id);
            $this->db->where('id_invitacion', $id);
            return $this->db->delete('invitacion');
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    function getInvitaciones(){
        $sql = "SELECT invitacion.* FROM invitacion WHERE id_boda = ".$this->session->userdata("id_boda");
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_invitacion = encrypt($value->id_invitacion);
            $value->id_boda = encrypt($value->id_boda);        
        }
        return $result;
    }
    
    function getInvitacion($id){
        $id = decrypt($id);
        $sql = "SELECT invitacion.* FROM invitacion WHERE id_invitacion = $id AND id_boda = ".$this->session->userdata("id_boda");
        $result = $this->db->query($sql);
        $result = $result->row();
        if(!empty($result)){
            $result->id_invitacion = encrypt($result->id_invitacion);
            $result->id_boda = encrypt($result->id_boda);        
        }
        return $result;
    }
}