<?php

class Invitado_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla    = "invitado";
        $this->id_tabla = "id_invitado";
        $this->load->database();
        $this->load->library('encrypt');
    }

    function insertInvitadoBatch($data)
    {
        $this->db->insert_batch('invitado', $data);

        return true;
    }

    function insertInvitado($data)
    {
        try {
            $this->db->trans_start();
            $this->db->insert('invitado', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();

            return encrypt($id);
        } catch (Exception $e) {
            return false;
        }
    }

    function deleteInvitado($id)
    {
        try {
            $id = decrypt($id);
            $this->db->where('id_invitado', $id);

            return $this->db->delete('invitado');
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateInvitado($datos, $id)
    {
        try {
            $id = decrypt($id);
            if (isset($datos['id_mesa'])) {
                $datos['id_mesa'] = decrypt($datos['id_mesa']);
            }

            if (isset($datos['id_grupo'])) {
                $datos['id_grupo'] = decrypt($datos['id_grupo']);
            }

            if (isset($datos['id_menu'])) {
                $datos['id_menu'] = decrypt($datos['id_menu']);
            }

            $this->db->where('id_invitado', $id);

            return $this->db->update('invitado', $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    function getSexo()
    {
        $sql    = "SELECT edad,count(edad) "
                ."FROM invitado "
                ."WHERE id_boda = ".$this->session->userdata("id_boda")." "
                ."GROUP BY edad";
        $result = $this->db->query($sql);

        return $result->result();
    }

    public function getConfirmados()
    {
        try {
            $sql    = "SELECT count(confirmado) AS total "
                    ."FROM invitado "
                    ."WHERE id_boda = ".$this->session->userdata("id_boda")." AND confirmado = 2 "
                    ."GROUP BY confirmado";
            $result = $this->db->query($sql)->row();
            if (empty($result)) {
                return 0;
            } else {
                return $result->total;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    function getInvitadosGrupoNovio()
    {
        $sql    = "SELECT invitado.*"
                ."FROM invitado INNER JOIN grupo USING(id_grupo) "
                ."WHERE grupo.grupo = 'Novios' AND invitado.id_boda = ".$this->session->userdata("id_boda")."";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_grupo    = encrypt($value->id_grupo);
            $value->id_menu     = encrypt($value->id_menu);
            $value->id_mesa     = encrypt($value->id_mesa);
            $value->id_boda     = encrypt($value->id_boda);
            $value->id_invitado = encrypt($value->id_invitado);
        }

        return $result;
    }

    function getInvitadoArray($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $ids[$i] = decrypt($ids[$i]);
        }
        $this->db->select('nombre, apellido, id_invitado,correo');
        $this->db->or_where_in('id_invitado', $ids);
        $query = $this->db->get('invitado');
        $query = $query->result();

        return $query;
    }

    function getInvitado($id)
    {
        $id     = decrypt($id);
        $sql    = "SELECT invitado.*, acompanante.id_invitado as invitado_por "
                ."FROM invitado LEFT JOIN acompanante ON(id_acompanante = invitado.id_invitado) "
                ."WHERE id_boda = ".$this->session->userdata("id_boda")." AND invitado.id_invitado = $id";
        $result = $this->db->query($sql);
        $value  = $result->row();
        if ( ! empty($value)) {
            $value->id_grupo    = encrypt($value->id_grupo);
            $value->id_menu     = encrypt($value->id_menu);
            $value->id_mesa     = encrypt($value->id_mesa);
            $value->id_boda     = encrypt($value->id_boda);
            $value->id_invitado = encrypt($value->id_invitado);
            if ($value->invitado_por != null) {
                $value->invitado_por = encrypt($value->invitado_por);
            } else {
                $respuesta          = $this->getAcompaniante($value->id_invitado);
                $value->acompanante = $respuesta;
            }

            return $value;
        } else {
            return false;
        }
    }

    function getAcompaniante($id_invitado)
    {
        $id_invitado = decrypt($id_invitado);
        $sql         = "SELECT invitado.*,acompanante.id_invitado as invitado_por  
                FROM acompanante INNER JOIN invitado ON(invitado.id_invitado = acompanante.id_acompanante) 
                WHERE acompanante.id_invitado = $id_invitado";
        $result      = $this->db->query($sql);
        $result      = $result->result();
        foreach ($result as $key => $value) {
            $value->id_grupo    = encrypt($value->id_grupo);
            $value->id_menu     = encrypt($value->id_menu);
            $value->id_mesa     = encrypt($value->id_mesa);
            $value->id_boda     = encrypt($value->id_boda);
            $value->id_invitado = encrypt($value->id_invitado);
            if ($value->invitado_por != null) {
                $value->invitado_por = encrypt($value->invitado_por);
            }
        }

        return $result;
    }

    function getInvitados()
    {
        $sql    = "SELECT invitado.*, acompanante.id_invitado as invitado_por "
                ."FROM invitado LEFT JOIN acompanante ON(id_acompanante = invitado.id_invitado) "
                ."WHERE id_boda = ".$this->session->userdata("id_boda")." "
                ."ORDER BY invitado_por ASC";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_grupo    = encrypt($value->id_grupo);
            $value->id_menu     = encrypt($value->id_menu);
            $value->id_mesa     = encrypt($value->id_mesa);
            $value->id_boda     = encrypt($value->id_boda);
            $value->id_invitado = encrypt($value->id_invitado);
            if ($value->invitado_por != null) {
                $value->invitado_por = encrypt($value->invitado_por);
            }
        }

        return $result;
    }

    function getCorreoInvitados()
    {
        $sql    = "SELECT invitado.correo "
                ."FROM invitado "
                ."WHERE correo IS NOT NULL AND id_boda = ".$this->session->userdata("id_boda");
        $result = $this->db->query($sql);
        $result = $result->result();

        return $result;
    }

    function getInvitadosOrder($order_by = 'id_invitado')
    {
        $sql    = "SELECT invitado.*, acompanante.id_invitado as invitado_por "
                ."FROM invitado LEFT JOIN acompanante ON(id_acompanante = invitado.id_invitado) "
                ."WHERE id_boda = ".$this->session->userdata("id_boda")." "
                ."ORDER BY $order_by ASC";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_grupo    = encrypt($value->id_grupo);
            $value->id_menu     = encrypt($value->id_menu);
            $value->id_mesa     = encrypt($value->id_mesa);
            $value->id_boda     = encrypt($value->id_boda);
            $value->id_invitado = encrypt($value->id_invitado);
            if ($value->invitado_por != null) {
                $value->invitado_por = encrypt($value->invitado_por);
            }
        }

        return $result;
    }

    function getInvitadosGrupoOrder($order_by = 'id_invitado')
    {
        $sql = "SELECT invitado.*, grupo.grupo, acompanante.id_invitado as invitado_por "
                ."FROM invitado "
                ."LEFT JOIN acompanante ON(id_acompanante = invitado.id_invitado) "
                ."LEFT JOIN grupo USING(id_grupo) "
                ."WHERE invitado.id_boda = ".$this->session->userdata("id_boda")." "
                ."ORDER BY $order_by ASC";

        $result = $this->db->query($sql)->result();

        foreach ($result as $key => $value) {
            $value->id_grupo    = empty($value->id_grupo) ? '' : encrypt($value->id_grupo);
            $value->id_menu     = empty($value->id_menu) ? '' : encrypt($value->id_menu);
            $value->id_mesa     = empty($value->id_mesa) ? '' : encrypt($value->id_mesa);
            $value->id_boda     = encrypt($value->id_boda);
            $value->id_invitado = encrypt($value->id_invitado);
            if ($value->invitado_por != null) {
                $sql                     = "SELECT nombre,apellido FROM invitado WHERE id_invitado = $value->invitado_por IS NOT NULL";
                $resultado               = $this->db->query($sql)->row();
                $value->invitado_por     = encrypt($value->invitado_por);
                $value->nombre_principal = $resultado->nombre.$resultado->apellido;
            }
        }

        return $result;
    }

    function getGrupoNovios($id_boda)
    {
        $this->db->select('id_grupo,grupo,color');
        $this->db->where('id_boda', $id_boda);
        $this->db->where('grupo', "Novios");
        $query = $this->db->get('grupo');

        return $query->row();
    }

    public function getList($id, $limit1 = null, $limit2 = null)
    {
        if (is_array($id)) {
            $query = $this->db->get_where("invitado", $id, $limit1, $limit2);
        } else {
            $query = $this->db->get_where("invitado", ["id_invitado" => $id], $limit1, $limit2);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    public function getName($id_usuario)
    {
        $sql    = "SELECT nombre,genero FROM usuario INNER JOIN cliente USING (id_usuario) WHERE id_usuario = $id_usuario";
        $result = $this->db->query($sql)->result();
        if ( ! empty($result)) {
            if ( ! empty($result[0]->genero) && $result[0]->genero == 1) {
                $result[0]->nombre = $result[0]->nombre." &amp NOVIA";

                return $result;
            } elseif ( ! empty($result[0]->genero) && $result[0]->genero == 2) {
                $result[0]->nombre = $result[0]->nombre." &amp NOVIO";

                return $result;
            }
        }

        return false;
    }

    public function setDateFiance($id_boda,$data){
        try {
            $this->db->where('id_boda', $id_boda);
            $this->db->where('nombre','XXX');
            return $this->db->update('invitado', $data);
        } catch (Exception $e) {
            return false;
        }

    }

}
