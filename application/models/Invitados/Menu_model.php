<?php

class Menu_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
    }

    function insertMenuBatch($data)
    {
        $this->db->insert_batch('menu', $data);

        return true;
    }

    function insertMenu($data)
    {
        $this->db->insert('menu', $data);

        return true;
    }

    public function updateMenu($datos, $id)
    {
        try {
            $id = decrypt($id);
            $this->db->where('id_menu', $id);

            return $this->db->update('menu', $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteMenu($id)
    {
        try {
            $id = decrypt($id);
            $this->db->where('id_menu', $id);
            $this->db->update('invitado', ["id_menu" => null]);

            $this->db->where('id_menu', $id);

            return $this->db->delete('menu');
        } catch (Exception $e) {
            return false;
        }
    }

    function getCountMenu()
    {
        $sql = "SELECT menu.id_menu,menu.nombre, count(invitado.id_menu) as total
                FROM menu LEFT JOIN invitado USING(id_menu)
                WHERE menu.id_boda = " . $this->session->userdata('id_boda') . "
                GROUP BY id_menu";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_menu = encrypt($value->id_menu);
        }

        return $result;
    }

    function getNumeroMenus()
    {
        $sql = "SELECT menu.*, count(id_invitado) as total "
            . "FROM menu LEFT JOIN invitado USING(id_menu) "
            . "WHERE menu.id_boda = " . $this->session->userdata('id_boda') . " "
            . "GROUP BY id_menu ORDER BY id_menu;";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_menu = encrypt($value->id_menu);
            $value->id_boda = encrypt($value->id_boda);
        }

        return $result;
    }

    function getMenus()
    {
        $sql = "SELECT * FROM menu WHERE id_boda = " . $this->session->userdata("id_boda");
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_menu = encrypt($value->id_menu);
            $value->id_boda = encrypt($value->id_boda);
        }

        return $result;
    }

    function getMenuNombre($id_boda)
    {
        $sql = "SELECT id_menu FROM menu WHERE nombre = 'Adultos' AND id_boda = $id_boda";
        $result = $this->db->query($sql);
        $result = $result->row();

        return $result;
    }

    function getSameMenu($nombre, $id_boda)
    {
        $sql = "SELECT id_menu FROM menu WHERE nombre = '$nombre' AND  id_boda = $id_boda";
        $result = $this->db->query($sql);
        $result = $result->row();

        return $result;
    }

}
