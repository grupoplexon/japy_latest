<?php

class Mesa_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla = "mesa";
        $this->id_tabla = "id_mesa";
        $this->load->database();
        $this->load->library('encrypt');
    }

    function insertMesasuBatch($data)
    {
        $this->db->insert_batch('mesa', $data);
        return true;
    }

    function insertMesas($data)
    {
        $this->db->insert('mesa', $data);
        return true;
    }

    function getCountMesa()
    {
        $idBoda = $this->session->userdata('id_boda');
        $sql = "SELECT count(id_boda) AS total FROM mesa WHERE id_boda = $idBoda GROUP BY id_boda";
        $result = $this->db->query($sql);
        return $result->row();
    }

    function getMesaPrincipal($id_boda)
    {
        $sql = "SELECT id_mesa FROM mesa WHERE id_boda = $id_boda AND nombre = 'PRINCIPAL'";
        $result = $this->db->query($sql);
        $result = $result->row();
        return $result;
    }

    /*function getMesas(){
        $sql = "SELECT * FROM mesa WHERE id_boda = ".$this->session->userdata("id_boda");
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_mesa = encrypt($value->id_mesa);
            $value->id_boda = encrypt($value->id_boda);        
        }
        return $result;
    }*/

    function getMesas()
    {
        $sql = "SELECT mesa.*,count(invitado.id_mesa) AS ocupado 
                FROM mesa LEFT JOIN invitado USING(id_mesa) 
                WHERE mesa.id_boda = " . $this->session->userdata("id_boda") . " 
                GROUP BY id_mesa";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_mesa = encrypt($value->id_mesa);
            $value->id_boda = encrypt($value->id_boda);
        }
        return $result;
    }

    function getDisponible($id_mesa)
    {
        $mesa = $this->getMesa($id_mesa);
        $ocupadas = $this->getOcupadas($id_mesa);
        for ($i = 1; $i <= $mesa->sillas; $i++) {
            $estado = true;
            foreach ($ocupadas as $key => $value) {
                if ($value->silla == $i) {
                    $estado = false;
                    break;
                }
            }

            if ($estado) {
                return $i;
            }

        }
        return false;
    }

    function getOcupadas($id_mesa)
    {
        $sql = "SELECT silla
                FROM invitado
                WHERE id_mesa = $id_mesa and invitado.id_boda = " . $this->session->userdata("id_boda");
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getMesa($id_mesa)
    {
        $sql = "SELECT * 
                FROM mesa 
                WHERE id_mesa = $id_mesa and id_boda = " . $this->session->userdata("id_boda");
        $result = $this->db->query($sql)->row();
        return $result;
    }
}