<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Alert extends Eloquent
{
    protected $table = 'alerts';
    protected $primaryKey = 'id';
    protected $fillable
        = [
            "id_user",
            "id_temp",
            "status",
            "name",
            "msj",
            "type",
        ];

    // $alert = Alert::create([
    //     'usuario'    => $row[12],
    //     'contrasena' => sha1(md5(sha1($row[13]))),
    //     'rol'        => 3,
    //     'activo'     => 1,
    //     'nombre'     => $row[0],
    //     'correo'     => $row[1],
    // ]);
    
}