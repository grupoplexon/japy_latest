<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Attachment extends Eloquent
{
    protected $table = 'adjunto';
    protected $primaryKey = 'id_adjunto';
    public $fillable = ['data', 'mime', 'nombre'];
    public $timestamps = false;

    // Relations

    //Scopes

}
