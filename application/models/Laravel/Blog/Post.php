<?php
namespace Models\Blog;

use \Illuminate\Database\Eloquent\Model as Eloquent;

use Models\Tag;

class Post extends Eloquent
{
    protected $table = "blog_post";
    protected $primaryKey = "id_blog_post";
    public $guarded = ["id_blog_post"];
    public $timestamps = false;

    // Relations
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

}
