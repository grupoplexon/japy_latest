<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Board extends Eloquent
{
    protected $table = "boards";
    protected $guarded = [];

    // Relations
    public function images()
    {
        return $this->belongsToMany(Gallery::class, "board_gallery", "board_id", "gallery_id");
    }

}
