<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class BrideWeekendRegistro extends Eloquent
{
    protected $table = 'brideweekend_registro';
    protected $primaryKey = 'id_registro';
    protected $fillable= [
        "id_registro",
        "nombre",
        "correo",
        "telefono",
        "fecha_boda",
    ];
}