<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Budget extends Eloquent
{
    protected $primaryKey = "id_presupuesto";
    protected $table = "presupuesto";
    protected $guarded = [];
    public $timestamps = false;

    public function wedding()
    {
        return $this->belongsTo(Wedding::class, "id_boda", "id_boda");
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, "id_presupuesto", "id_presupuesto");
    }

    public function category()
    {
        return $this->belongsTo(Category::class, "category_id", "id");
    }
}
