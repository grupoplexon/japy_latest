<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Category extends Eloquent
{
    protected $table = "proveedor_categoria";
    protected $primaryKey = "id";
    protected $fillable = ["id","name", "slug", "parent_id", "percentage", "description"];
    public $timestamps = false;

    // Relations
    public function subcategories()
    {
        return $this->hasMany(Category::class, "parent_id", "id");
    }

    public function providers()
    {
        return $this->belongsToMany(Provider::class, "categoria_proveedor", "categoria_id", "proveedor_id");
    }

    public function parent()
    {
        return $this->hasOne(Category::class, "id", "parent_id");
    }

    public function budgets()
    {
        return $this->hasMany(Budget::class, "category_id", "id");
    }

    public function FAQ()
    {
        return $this->hasMany(FAQ::class, "category_id", "id");
    }

    //Accessors & mutators
    public function getImageAttribute($value)
    {
        return "dist/img/iconos/proveedores/$value.png";
    }

    //Scopes
    public function scopeMain($query)
    {
        return $query->whereNull("parent_id")
                ->orWhere("parent_id", 0);
    }

    public function scopeNotMain($query)
    {
        return $query->whereNotNull("parent_id");
    }

    public function scopeHasProvidersAndWedding($query, $id)
    {
        return $query->whereHas("providers", function ($query) use ($id) {
            return $query->whereHas("weddings", function ($query) use ($id) {
                return $query->where("boda.id_boda", $id);
            });
        });
    }

    public function scopeWithProvidersAndWedding($query, $id)
    {
        return $query->with([
                "providers" => function ($query) use ($id) {
                    return $query->whereHas("weddings", function ($query) use ($id) {
                        return $query->where("boda.id_boda", $id);
                    });
                },
        ]);
    }

    public function scopeWithProvidersByLocation($query, $state, $city)
    {
        return $query->with([
                "providers" => function ($query) use ($state, $city) {
                    return $query->whereHas("user", function ($query) {
                        return $query->where("activo", "1");
                    })->when($state, function ($query) use ($state) {
                        return $query->where("localizacion_estado", $state);
                    })->when($city, function ($query) use ($city) {
                        return $query->where("localizacion_poblacion", $city);
                    })->with("imagePrincipal", "FAQ", "weddings")->withCount("images", "videos", "promotions");
                },
        ]);
    }

    //Custom

    public function getSecondaryImage()
    {
        return "dist/img/iconos/proveedores/".$this->getOriginal("image").".png";
    }

}
