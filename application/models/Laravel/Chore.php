<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Chore extends Eloquent
{
    protected $table = 'tarea';
    protected $primaryKey = 'id_tarea';
    protected $guarded = ['id_tarea'];
    public $timestamps = false;

    public function provider()
    {
        return $this->belongsTo(Provider::class, 'id_proveedor', 'id_proveedor');
    }

    public function wedding()
    {
        return $this->belongsTo(Wedding::class, 'id_boda', 'id_boda');
    }
}