<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class City extends Eloquent
{
    protected $table = 'ciudad';
    protected $primaryKey = 'id_ciudad';

    public function state()
    {
        return $this->belongsTo(State::class, 'id_estado', 'id_estado');
    }
}
