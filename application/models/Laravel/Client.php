<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Client extends Eloquent
{
    /*

    ONLY THE USER CAN SEND A MAIL TO START THE CONVERSATION
    THE PROVIDER CAN ONLY RESPOND TO HIS MAIL

    */

    protected $table = 'cliente';
    protected $primaryKey = 'id_cliente';
    protected $guarded = [];
    public $timestamps = false;

    // Relations
    public function user()
    {
        return $this->hasOne(User::class, "id_usuario", "id_usuario");
    }

    public function wedding()
    {
        return $this->hasOne(Wedding::class, 'id_boda', 'id_boda');
    }
}
