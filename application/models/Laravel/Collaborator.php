<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Collaborator extends Eloquent
{
    protected $table = 'colaborador';
    protected $primaryKey = 'id_colaborador';
}
