<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Coment extends Eloquent
{
    protected $table = 'inspiracion_comentarios';
    protected $primaryKey = 'id_comentario';
    protected $fillable
        = [
            "id_inspiracion",
            "id_cliente",
            "nombre_cliente",
            "comentario",
        ];

    // $alert = Alert::create([
    //     'usuario'    => $row[12],
    //     'contrasena' => sha1(md5(sha1($row[13]))),
    //     'rol'        => 3,
    //     'activo'     => 1,
    //     'nombre'     => $row[0],
    //     'correo'     => $row[1],
    // ]);
    
}