<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Country extends Eloquent
{
    protected $table = 'pais';
    protected $primaryKey = 'id_pais';

    public function states()
    {
        return $this->hasMany(State::class, 'id_pais', 'id_pais');
    }

}
