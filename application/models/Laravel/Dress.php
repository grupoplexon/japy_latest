<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Dress extends Eloquent
{
    protected $table = 'vestido';
    protected $primaryKey = 'id_vestido';
    protected $guarded = ['id_vestido'];
    public $timestamps = false;

}