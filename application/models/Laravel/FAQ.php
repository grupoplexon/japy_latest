<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class FAQ extends Eloquent
{
    protected $table = "faq";
    protected $primaryKey = "id_faq";
    protected $fillable = ["id_faq", "id_tipo_proveedor", "icon", "titulo", "pregunta", "tipo", "valores", "category_id"];
    public $timestamps = false;

    public function providers()
    {
        return $this->belongsToMany(Provider::class, "respuesta_faq", "id_faq", "id_proveedor")
            ->withPivot("respuesta");
    }
}
