<?php

namespace Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

use Models\Tag;
use Provider;

class Gallery extends Eloquent
{
    protected $table = "galeria";
    protected $primaryKey = "id_galeria";
    protected $fillable = ["id_proveedor", "nombre", "data", "tipo", "mime", "parent", "logo", "principal", "descripcion"];
    protected $hidden = ["data"];
    public $timestamps = false;


    public function provider()
    {
        return $this->belongsTo(Provider::class, "id_proveedor", "id_proveedor");
    }

    public function miniatures()
    {
        return $this->hasMany(Gallery::class, "parent", "id_galeria");
    }

    public function boards()
    {
        return $this->belongsToMany(Board::class, "board_gallery", "gallery_id", "board_id");
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, "taggable");
    }

}
