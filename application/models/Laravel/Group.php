<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Group extends Eloquent
{
    protected $table = 'grupo';
    protected $primaryKey = 'id_grupo';
    protected $fillable = ['grupo', 'id_boda'];
    public $timestamps = false;
}
