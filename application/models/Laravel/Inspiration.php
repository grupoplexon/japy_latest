<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

use Models\Tag;

class Inspiration extends Eloquent
{
    protected $table = 'inspiracion';
    protected $primaryKey = 'id_inspiracion';
    protected $fillable
        = [
            "id_inspiracion",
            "id_proveedor",
            "id_galeria",
            "nombre",
            "updated_at",
            "titulo",
            "descripcion",
            "vistas",
            "tipo",
            "mime",
        ];

    // Relations
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    // $alert = Alert::create([
    //     'usuario'    => $row[12],
    //     'contrasena' => sha1(md5(sha1($row[13]))),
    //     'rol'        => 3,
    //     'activo'     => 1,
    //     'nombre'     => $row[0],
    //     'correo'     => $row[1],
    // ]);
    
}