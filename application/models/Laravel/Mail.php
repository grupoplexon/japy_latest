<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;
use Models\Attachment;

class Mail extends Eloquent
{
    /*

    ONLY THE USER CAN SEND A MAIL TO START THE CONVERSATION
    THE PROVIDER CAN ONLY RESPOND TO HIS MAIL

    */

    protected $table = 'correo';
    protected $primaryKey = 'id_correo';
    public $fillable
        = [
            'asunto',
            'mensaje',
            'from',
            'to',
            'parent',
            'leida_usuario',
            'leida_proveedor',
            'visible_usuario',
            'visible_proveedor',
            'indicator_id',
        ];
    public $timestamps = false;

    //RELATIONS
    public function replies()
    {
        return $this->hasMany(Mail::class, 'parent', 'id_correo');
    }

    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'adjunto_correo', 'id_correo', 'id_adjunto');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'from', 'id_usuario');
    }

    public function userTo()
    {
        return $this->belongsTo(User::class, 'to', 'id_usuario');
    }

    public function latestReply()
    {
        return $this->hasOne(Mail::class, 'parent', 'id_correo')
            ->orderBy('id_correo', 'desc')
            ->limit(1);
    }

    public function latestPendingReply()
    {
        return $this->hasOne(Mail::class, 'parent', 'id_correo')->where('leida_proveedor', 0);
    }

    //SCOPES
    public function scopeMain($query)
    {
        return $query->whereNull('parent')
            ->orWhere('parent', 0);
    }

    public function scopeFrom($query, $value)
    {
        return $query->where('from', $value);
    }

    public function scopeTo($query, $value)
    {
        return $query->where('to', $value);
    }

    public function scopeFromOrTo($query, $value)
    {
        return $query->where('from', $value)->orWhere('to', $value);
    }

    public function scopeVisible($query, $who, $value)
    {
        if ($who == 'user') {
            return $query->where('visible_usuario', $value);
        } else {
            return $query->where('visible_proveedor', $value);
        }
    }

    public function scopeSeen($query, $who, $value)
    {
        if ($who == 'user') {
            return $query->where('leida_usuario', $value);
        } else {
            return $query->where('leida_proveedor', $value);
        }
    }

    public function scopeStatus($query, $value)
    {
        return $query->where('estado', $value);
    }

    public function scopeDateFilter($query, $inicio, $fin, $tipo)
    {
        return $query->when($tipo == 'solicitud', function ($query) use ($inicio, $fin) {
            return $query->whereRaw("CAST(fecha_creacion as DATE) BETWEEN '$inicio' AND '$fin'");
        }, function ($query) use ($inicio, $fin) {
            return $query->whereHas('user.client.wedding', function ($query) use ($inicio, $fin) {
                return $query->whereRaw("CAST(fecha_boda as DATE) BETWEEN '$inicio' AND '$fin'");
            });
        });
    }

    public function scopeSearchFilter($query, $value)
    {
        return $query->whereHas('user', function ($query) use ($value) {
            return $query->where('nombre', 'like', "%$value%")
                ->orWhere('apellido', 'like', "%$value%");
        })->orWhere('asunto', 'like', "%$value%")->orWhere('mensaje', 'like', "%$value%");
    }

    //MUTATORS & ACCESSORS
    //THIS WON'T WORK IF YOU'RE A PROVIDER
    public function getWeddingAttribute()
    {
        return $this->user->client->wedding;
    }

    //CUSTOM FUNCTIONS
    //Only for provider
    public function hasUnseenReplies($to)
    {
        return $this->replies()
            ->where('leida_proveedor', 0)
            ->where('to', $to)
            ->count();
    }

    //Only for provider
    public static function getCount($userId, $seen)
    {
        $mails = Mail::with('replies')->to($userId)->visible('provider', true)->main()->get();

        foreach ($mails as $key => $mail) {
            if ($mail->replies->count()) {
                foreach ($mail->replies as $reply) {
                    $latestReply = $mail->latestReply;

                    if ($latestReply->leida_proveedor != $seen) {
                        $mails->forget($key);
                        break;
                    }

                }
            } elseif ($mail->leida_proveedor != $seen) {
                $mails->forget($key);
            }
        }

        return $mails->count();
    }
}
