<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Menu extends Eloquent
{
    protected $table = 'menu';
    protected $primaryKey = 'id_menu';
    protected $fillable = ['nombre', 'descripcion', 'id_boda'];
    public $timestamps = false;
}
