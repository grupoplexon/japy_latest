<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Oferta extends Eloquent
{
    protected $table = 'deal';
    protected $primaryKey = 'id_oferta';
    protected $fillable= [
        "id_oferta",
        "porcentaje_desc",
        "vigencia",
        "presupuesto_min",
        "presupuesto_max",
        "id_proveedor",
        "tipo",
        "fecha_inicio",
        "fecha_fin",
    ];

}