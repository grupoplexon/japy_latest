<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Payment extends Eloquent
{
    protected $table = "pago";
    protected $primaryKey = "id_pago";
    protected $guarded = [];
    const CREATED_AT = "fecha_pago";

    public function setUpdatedAt($value)
    {
        //Do-nothing
    }

    public function getUpdatedAtColumn()
    {
        //Do-nothing
    }
}
