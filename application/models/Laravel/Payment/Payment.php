<?php

namespace Models\Payment;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Payment extends Eloquent
{
    protected $table = "payment_cards";
    protected $primaryKey = "id_usuario";
    protected $guarded = ["id"];

    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "id_usuario");
    }

    //Scopes
}
