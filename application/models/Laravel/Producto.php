<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Producto extends Eloquent
{
    protected $table = 'producto_oferta';
    protected $primaryKey = 'id';
    protected $fillable= [
        "id",
        "id_oferta",
        "id_producto",
    ];

}