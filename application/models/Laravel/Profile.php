<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Profile extends Eloquent
{
    protected $table = "permisos_usuarios";
    protected $primaryKey = "id_permisos";
    protected $fillable = [
            "enviar_mensaje",
            "participacion_debates",
            "valorar_publicaciones",
            "anadir_amigo",
            "aceptar_solicitud",
            "mension_posts",
            "email_diario",
            "email_semanal",
            "invitaciones",
            "concursos",
            "visibilidad_todos",
            "email_debate",
    ];
    public $timestamps = false;
}
