<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Promotion extends Eloquent
{
    protected $table = 'promocion';
    protected $primaryKey = 'id_promocion';

    public function provider()
    {
        return $this->belongsTo(Provider::class, 'id_proveedor', 'id_proveedor');
    }
}
