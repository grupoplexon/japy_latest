<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

use Models\Gallery;
use Carbon\Carbon;
use Conekta\Conekta as Conekta;

class Provider extends Eloquent
{
    protected $table = "proveedor";
    protected $primaryKey = "id_proveedor";
    protected $guarded = [];
    public $timestamps = false;

    public function __construct()
    {
        parent::__construct();
        Conekta::setApiKey("key_sQdMeJqixL8cAGoDA8c7qw");
        Conekta::setApiVersion("2.0.0");
        Conekta::setLocale("es");
    }

    //Relations
    public function user()
    {
        return $this->belongsTo(User::class, "id_usuario", "id_usuario");
    }

    public function activo()
    {
        return $this->hasMany(User::class, "id_usuario", "id_usuario");
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, "categoria_proveedor", "proveedor_id", "categoria_id");
    }

    public function weddings()
    {
        return $this->belongsToMany(Wedding::class, "proveedor_boda", "id_proveedor",
            "id_boda")->withPivot("calificacion");
    }

    public function imagePrincipal()
    {
        return $this->images()->where("principal", 1)->where("parent", null);
    }

    public function imagePrincipalMiniature()
    {
        return $this->images()->where("principal", 1)->where("parent", "<>", null);
    }

    public function imageLogo()
    {
        return $this->hasOne(Gallery::class, "id_proveedor", "id_proveedor")
            ->where("tipo", "IMAGEN")->where("logo", 1)->where("parent", null);
    }

    public function imageLogoMiniature()
    {
        return $this->hasOne(Gallery::class, "id_proveedor", "id_proveedor")
            ->where("tipo", "IMAGEN")->where("logo", 1)->where("parent", "<>", null);
    }

    public function images()
    {
        return $this->hasMany(Gallery::class, "id_proveedor", "id_proveedor")
            ->where("tipo", "IMAGEN");
    }

    public function videos()
    {
        return $this->hasMany(Gallery::class, "id_proveedor", "id_proveedor")
            ->where("tipo", "VIDEO");
    }

    public function FAQ()
    {
        return $this->belongsToMany(FAQ::class, "respuesta_faq", "id_proveedor", "id_faq")
            ->withPivot("respuesta");
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class, "id_proveedor", "id_proveedor");
    }

    public function collaborators()
    {
        return $this->belongsToMany(Provider::class, "colaborador", "id_proveedor", "id_colaborador");
    }

    public function tasks()
    {
        return $this->hasMany(Chore::class, "id_proveedor", "id_proveedor");
    }

    public function indicators()
    {
        return $this->belongsToMany(User::class, "indicators", "provider_id", "user_id")
            ->withPivot("id", "type", "created_at", "updated_at", "handled_at")->withTimestamps();
    }

    //Custom methods
    public function registerHubSpot()
    {
        $this->user->exists() && ! empty($this->user->correo) && $this->user->correo != "bienvenido@clubnupcial.com" ? $provider["properties"][] = ["property" => "email", "value" => $this->user->correo] : "";
        $this->user->exists() && ! empty($this->user->nombre) ? $provider["properties"][] = ["property" => "firstname", "value" => $this->user->nombre] : "";
        $this->contacto_pag_web && ! empty($this->contacto_pag_web) ? $provider["properties"][] = ["property" => "website", "value" => $this->contacto_pag_web] : "";
        $this->nombre && ! empty($this->nombre) ? $provider["properties"][] = ["property" => "company", "value" => $this->nombre] : "";
        $this->contacto_telefono && ! empty($this->contacto_telefono) ? $provider["properties"][] = ["property" => "phone", "value" => preg_replace("/\s/", "", $this->contacto_telefono)] : "";
        $this->contacto_celular && ! empty($this->contacto_celular) ? $provider["properties"][] = ["property" => "mobilephone", "value" => preg_replace("/\s/", "", $this->contacto_celular)] : "";
        $this->localizacion_poblacion && ! empty($this->localizacion_poblacion) ? $provider["properties"][] = ["property" => "city", "value" => $this->localizacion_poblacion] : "";
        $this->localizacion_estado && ! empty($this->localizacion_estado) ? $provider["properties"][] = ["property" => "state", "value" => $this->localizacion_estado] : "";
        $this->localizacion_pais && ! empty($this->localizacion_pais) ? $provider["properties"][] = ["property" => "country", "value" => $this->localizacion_pais] : "";
        $this->localizacion_cp && ! empty($this->localizacion_cp) ? $provider["properties"][] = ["property" => "zip", "value" => $this->localizacion_cp] : "";
        $this->localizacion_direccion && ! empty($this->localizacion_direccion) ? $provider["properties"][] = ["property" => "address", "value" => $this->localizacion_direccion] : "";
        $this->categories->count() ? $provider["properties"][] = ["property" => "categoria", "value" => $this->categories()->first()->name] : "";

        if ($provider) {
            $post_json = json_encode($provider);
            $hapikey   = "6f6f6ea6-e765-477e-b797-00c4ad961c88";
            $endpoint  = "https://api.hubapi.com/contacts/v1/contact?hapikey=".$hapikey;
            $ch        = @curl_init();
            @curl_setopt($ch, CURLOPT_POST, true);
            @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
            @curl_setopt($ch, CURLOPT_URL, $endpoint);
            @curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
            @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response    = @curl_exec($ch);
            $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $curl_errors = curl_error($ch);
            @curl_close($ch);

            return $response;
        }
    }

    public function canUploadVideo()
    {
        return ! $this->videos()->count();
    }

    public function canUploadImages()
    {
        $imagesCount              = $this->images()->where("parent", null)->count();
        $imagesQuantityConstraint = [
            0 => 8,
            1 => 10,
            2 => 15,
            3 => 20,
        ];

        return $imagesQuantityConstraint[$this->tipo_cuenta] > $imagesCount;
    }

    public function allowedImagesQuantity()
    {
        $imagesQuantityConstraint = [
            0 => 8,
            1 => 10,
            2 => 15,
            3 => 20,
        ];

        return $imagesQuantityConstraint[$this->tipo_cuenta];
    }

    //CONEKTA
    public function createCustomer()
    {
        $response = ["success" => false, "message" => "Oops ocurrio un error"];

        try {
            $customer = \Conekta\Customer::create(
                [
                    "name"  => $this->user->nombre." ".$this->user->apellido,
                    "email" => $this->user->correo,
                    "phone" => $this->contacto_telefono,
                ]
            );

            $this->update(["conekta_customer_id" => $customer->id]);
            $response["success"] = true;

        } catch (\Conekta\ProccessingError $error) {
            $response["message"] = $error->getMesage();
        } catch (\Conekta\ParameterValidationError $error) {
            $response["message"] = $error->getMessage();
        } catch (\Conekta\Handler $error) {
            $response["message"] = $error->getMessage();
        }

        return $response;
    }

    public function addCustomerCard($token)
    {
        $response = ["success" => false, "message" => "Oops ocurrio un error"];

        if ( ! empty($token)) {
            try {
                $customer = \Conekta\Customer::find($this->conekta_customer_id);
                $source   = $customer->createPaymentSource(array(
                    "token_id" => $token,
                    "type"     => "card",
                ));

                $response["success"] = true;
                $response["message"] = "Tarjeta actualizada exitosamente";

            } catch (\Conekta\ProcessingError $error) {
                $response["message"] = $error->getMessage();
            } catch (\Conekta\ParameterValidationError $error) {
                $response["message"] = $error->getMessage();
            } catch (\Conekta\Handler $error) {
                $response["message"] = $error->getMessage();
            }
        } else {
            $response["message"] = "Tu tarjeta no pudo ser procesada. Intenta ingresandola de nuevo";
        }

        return $response;
    }

    public function deleteCustomerCard()
    {
        $response = ["success" => false, "message" => "Oops ocurrio un error"];

        try {
            $customer = \Conekta\Customer::find($this->conekta_customer_id);
            if ($customer->payment_sources[0]) {
                $customer->payment_sources[0]->delete();
            }

            $response["success"] = true;
            $response["message"] = "Tarjeta eliminada exitosamente";

        } catch (\Conekta\ProcessingError $error) {
            $response["message"] = $error->getMessage();
        } catch (\Conekta\ParameterValidationError $error) {
            $response["message"] = $error->getMessage();
        } catch (\Conekta\Handler $error) {
            $response["message"] = $error->getMessage();
        }

        return $response;
    }

    public function hasPaymentMethod()
    {
        $response = false;

        if ( ! empty($this->conekta_customer_id)) {
            try {
                $customer = \Conekta\Customer::find($this->conekta_customer_id);
                $response = (bool)$customer->payment_sources[0];

            } catch (\Conekta\ProcessingError $error) {
                $response["message"] = $error->getMessage();
            } catch (\Conekta\ParameterValidationError $error) {
                $response["message"] = $error->getMessage();
            } catch (\Conekta\Handler $error) {
                $response["message"] = $error->getMessage();
            }
        }

        return $response;
    }

    public function getPlanRemaining()
    {
        $remaining          = ["remaining_months" => 0, "remaining_days" => 0];
        $planExpirationDate = $this->plan_expiration_date ? Carbon::createFromFormat("Y-m-d H:i:s", $this->plan_expiration_date) : null;
        if ($planExpirationDate && $planExpirationDate->isFuture()) {
            $remaining["remaining_months"] = $planExpirationDate->diffInMonths(Carbon::now());
            $remaining["remaining_days"]   = $planExpirationDate->subMonths($remaining["remaining_months"])->diffInDays(Carbon::now());
        }

        return $remaining;
    }

    public function calculatePlanTotal($plan, $period)
    {
        $plans              = [
            "silver"  => [
                6  => 6264,
                12 => 8352,
            ],
            "gold"    => [
                6  => 8352,
                12 => 11136,
            ],
            "diamond" => [
                6  => 10440,
                12 => 13920,
            ],
        ];
        $planExpirationDate = $this->plan_expiration_date ? Carbon::createFromFormat("Y-m-d H:i:s", $this->plan_expiration_date) : null;

        if ($this->tipo_cuenta > 0 && ! is_null($planExpirationDate) && $planExpirationDate->isFuture()) {
            //With startofmonth we include the current month
            $remainingMonths = $this->getPlanRemaining()["remaining_months"];
            $paidPlanPrice   = $plans[array_keys($plans)[$this->tipo_cuenta - 1]][$this->plan_months];
            $paidPlanMonths  = $this->plan_months;
            $pricePerMonth   = $paidPlanPrice / $paidPlanMonths;
            //Total is the plan selected minus the plan the user paid for and the percentage he has left
            $total = $plans[$plan][$period] - ($pricePerMonth * $remainingMonths);
        } else {
            $total = $plans[$plan][$period];
        }

        return $total;
    }

    public function buyPlan($plan, $period, $paymentMethod, $monthsWithoutInterest)
    {
        $response                       = ["success" => false, "message" => "Oops ocurrio un error"];
        $availablePlans                 = [
            "silver"  => [
                "translation" => "plata",
                "id"          => 1,
            ],
            "gold"    => [
                "translation" => "oro",
                "id"          => 2,
            ],
            "diamond" => [
                "translation" => "diamante",
                "id"          => 3,
            ],
        ];
        $availableMonthsWithoutInterest = [3, 6, 9, 12];
        $availablePeriods               = [6, 12];
        $paymentMethods                 = ["card" => "default", "oxxo" => "oxxo_cash"];

        if ( ! in_array($period, $availablePeriods)) {
            $response["message"] = "Seleccionaste un periodo inexistente";
        } elseif ( ! array_key_exists($plan, $availablePlans)) {
            $response["message"] = "Seleccionaste un plan inexistente";
        } elseif ( ! array_key_exists($paymentMethod, $paymentMethods)) {
            $response["message"] = "Metodo de pago inexistente";
        } elseif ( ! empty($monthsWithoutInterest) && ! in_array($monthsWithoutInterest, $availableMonthsWithoutInterest)) {
            $response["message"] = "Seleccionaste un periodo de meses sin intereses inexistente";
        } else {
            $total = $this->calculatePlanTotal($plan, $period);

            if ($total > 0) {
                try {
                    $orderData = [
                        "line_items"    => [
                            [
                                "name"       => "Plan ".$availablePlans[$plan]["translation"]." ".$period." meses Japy",
                                "unit_price" => $total * 10,
                                "quantity"   => 1,

                            ],
                        ],
                        "currency"      => "MXN",
                        "customer_info" => [
                            "customer_id" => $this->conekta_customer_id,
                        ],
                        "charges"       => [
                            [
                                "payment_method" => [
                                    "type" => $paymentMethods[$paymentMethod],
                                ],
                            ],
                        ],
                        "metadata"      => [
                            "planId"                => $availablePlans[$plan]["id"],
                            "period"                => $period,
                            "monthsWithoutInterest" => empty($monthsWithoutInterest) && $paymentMethod != "card" ? 0 : $monthsWithoutInterest,
                        ],
                    ];

                    if ( ! empty($monthsWithoutInterest) && $paymentMethod == "card") {
                        $orderData["charges"][0]["payment_method"]["monthly_installments"] = $monthsWithoutInterest;
                    }

                    $order = \Conekta\Order::create($orderData);

                } catch (\Conekta\ProcessingError $error) {
                    $response["message"] = $error->getMessage();
                } catch (\Conekta\ParameterValidationError $error) {
                    $response["message"] = $error->getMessage();
                } catch (\Conekta\Handler $error) {
                    $response["message"] = $error->getMessage();
                }
            }

            if ($total < 0) {
                $response["success"] = false;
                $response["message"] = "No puedes adquirir un plan de menor precio";
            } elseif ($total == 0) {
                $this->updatePlan($availablePlans[$plan]["id"], $period, empty($monthsWithoutInterest) ? null : $monthsWithoutInterest);
                $response["success"] = true;
                $response["message"] = "Se actualizo correctamente a un plan del mismo precio";
                $response["data"]    = ["needsPayment" => false,];
            } elseif (isset($order)) {
                if ($paymentMethod == "card" && $order->payment_status == "paid") {
                    $response["success"] = true;
                    $response["message"] = "Plan adquirido exitosamente";
                } elseif ($paymentMethod == "oxxo") {
                    $response["success"] = true;
                    $response["message"] = "ok";
                    $response["data"]    = [
                        "id"             => $order->id,
                        "payment_method" => $order->charges[0]->payment_method->service_name,
                        "reference"      => $order->charges[0]->payment_method->reference,
                        "amount"         => $order->amount / 10,
                        "needsPayment"   => true,
                    ];
                }
            }
        }

        if ( ! $response["success"] && $paymentMethod == "card") {
            $this->deleteCustomerCard();
        }

        return $response;
    }

    public function updatePlan($planId, $period, $monthsWithoutInterest = null)
    {
        $data["tipo_cuenta"]             = $planId;
        $data["plan_months"]             = $period;
        $data["months_without_interest"] = $monthsWithoutInterest;

        if (is_null($period)) {
            $data["plan_expiration_date"] = null;
        } else {
            $trialExpirationDate          = $this->trial_expiration_date ? Carbon::createFromFormat("Y-m-d H:i:s", $this->trial_expiration_date) : null;
            $planExpirationDate           = Carbon::now()->addMonths($period);
            $remainingTrialDays           = 0;
            $remainingPlanDays            = 0;
            $data["plan_expiration_date"] = $planExpirationDate;

            if (is_null($trialExpirationDate)) {
                //We add the months and then we convert them to days
                $remainingTrialDays = $planExpirationDate->diffInDays($planExpirationDate->copy()->addMonths(3));
            } else {
                if ($trialExpirationDate->isFuture()) {
                    if ($planExpirationDate->isFuture()) {
                        $remainingTrialDays = $planExpirationDate->diffInDays($trialExpirationDate);
                    } else {
                        $remainingTrialDays = Carbon::now()->diffInDays($trialExpirationDate);
                    }
                }
            }

            if ( ! is_null($this->plan_expiration_date)) {
                $remainingPlanDays = Carbon::createFromFormat("Y-m-d H:i:s", $this->plan_expiration_date)
                    ->diffInDays(Carbon::createFromFormat("Y-m-d H:i:s", $this->plan_expiration_date)->endOfMonth());
            }

            if ($remainingTrialDays > 0) {
                $data["trial_expiration_date"] = $planExpirationDate->copy()->addDays($remainingTrialDays);
            }

            if ($remainingPlanDays > 0) {
                $data["plan_expiration_date"] = $planExpirationDate->copy()->addDays($remainingPlanDays);
            }
        }

        $this->update($data);
    }
}

