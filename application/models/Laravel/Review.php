<?php
/**
 * Created by PhpStorm.
 * User: raulm
 * Date: 29/08/2018
 * Time: 11:19 AM
 */


use \Illuminate\Database\Eloquent\Model as Eloquent;

class Review extends Eloquent
{
    protected $table = 'recomendacion';
    protected $primaryKey = 'id_recomendacion';
    protected $fillable = [
        "id_recomendacion",
        "servicio",
        "calidad_precio",
        "profesionalidad",
        "respuesta",
        "flexibilidad",
        "boda_id",
        "proveedor_id",
        "fecha_creacion",
        "activo",
        "asunto",
        "mensaje",
        "usuario_id"
    ];

    const CREATED_AT = "fecha_creacion";

    public function setUpdatedAt($value)
    {
        //Do-nothing
    }

    public function getUpdatedAtColumn()
    {
        //Do-nothing
    }

    public function provider(){
        return $this->belongsTo(Provider::class,"proveedor_id","id_recomendacion");
    }
}