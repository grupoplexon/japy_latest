<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Saveinsp extends Eloquent
{
    protected $table = 'inspiracion_usuarios';
    protected $primaryKey = 'id';
    protected $fillable
        = [
            "id_usuario",
            "id_inspiracion",
            "created_at",
            "updated_at",
        ];
}