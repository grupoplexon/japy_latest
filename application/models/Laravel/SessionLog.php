<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class SessionLog extends Eloquent
{
    protected $primaryKey = "id";
    protected $fillable = ["user_id", "type",];
    const UPDATED_AT = null;

    public function user()
    {
        return $this->belongsTo(User::class, "id_usuario", "user_id");
    }
}
