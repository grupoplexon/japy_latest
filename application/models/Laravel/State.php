<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class State extends Eloquent
{
    protected $table = 'estado';
    protected $primaryKey = 'id_estado';

    public function cities()
    {
        return $this->hasMany(City::class, 'id_estado', 'id_estado');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'id_pais', 'id_pais');
    }
}
