<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Table extends Eloquent
{
    protected $table = 'mesa';
    protected $primaryKey = 'id_mesa';
    protected $fillable = ['id_boda','tipo','sillas','nombre','x','y','orientacion'];
    public $timestamps = false;
}