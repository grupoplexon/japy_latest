<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;
// use Illuminate\Database\Capsule\Manager as DB;
// DB::table('taggables')->where('tag_id',0)->delete();

use Models\Gallery;
use Models\Blog\Post;
use Models\Inspiration;
use Category;    

class Tag extends Eloquent
{
    protected $table = "tags";
    protected $guarded = [];

    // Relations
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'taggable');
    }

    // Relations
    public function inspiration_tags()
    {
        return $this->morphedByMany(Inspiration::class, 'taggable');
    }

    public function images()
    {
        return $this->morphedByMany(Gallery::class, 'taggable');
    }

    public function subcategoria(){
        return $this->hasOne(Category::class, 'id', 'subcategoria_id');
    }

}
