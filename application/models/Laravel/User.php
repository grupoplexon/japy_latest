<?php

use Models\Payment\Payment;
use \Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
    protected $table = "usuario";
    protected $primaryKey = "id_usuario";
    protected $fillable
        = [
            "id_usuario",
            "usuario",
            "nombre",
            "apellido",
            "mime",
            "foto",
            "contrasena",
            "rol",
            "activo",
            "correo",
            "fecha_creacion",
            "has_seen_tutorial",
            "registered_from",
        ];
    protected $hidden = ["mime", "foto"];

    const CREATED_AT = "fecha_creacion";

    public function setUpdatedAt($value)
    {
        //Do-nothing
    }

    public function getUpdatedAtColumn()
    {
        //Do-nothing
    }

    //Scopes
    public function scopeActive($query)
    {
        return $query->where("activo", 1);
    }

    //Relations
    //If the user is a provider himself
    public function provider()
    {
        return $this->hasOne(Provider::class, "id_usuario", "id_usuario");
    }

    public function client()
    {
        return $this->hasOne(Client::class, "id_usuario", "id_usuario");
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, "user_id", "id_usuario");
    }

    public function sessionHistory()
    {
        return $this->hasMany(SessionHistory::class, "user_id", "id_usuario");
    }

}
