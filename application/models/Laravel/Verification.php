<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;


class Verification extends Eloquent
{
    protected $table = "verification";
    protected $primaryKey = "id";
    protected $fillable= [
            "id",
            "id_proveedor",
            "id_faq",
            "nombre_prov",
            "descripcion",
            "precios",
            "tipo",
            "aprobacion",
            "status",
        ];

}