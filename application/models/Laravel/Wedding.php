<?php

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Wedding extends Eloquent
{
    protected $table = 'boda';
    protected $primaryKey = 'id_boda';
    protected $fillable = ['presupuesto', 'no_invitado', 'first'];
    public $timestamps = false;

    //Relations
    public function client()
    {
        return $this->hasOne(Client::class, 'id_boda', 'id_boda');
    }

    public function providers()
    {
        return $this->belongsToMany(Provider::class, 'proveedor_boda', 'id_boda', 'id_proveedor')
            ->withPivot('calificacion');
    }

    public function budgets()
    {
        return $this->hasMany(Budget::class, 'id_boda', 'id_boda');
    }

    public function chores()
    {
        return $this->hasMany(Chore::class, 'id_boda', 'id_boda');
    }
}
