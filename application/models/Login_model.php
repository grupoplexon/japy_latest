<?php

class Login_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
    }

    function validar_usuario($correo, $contrasenia)
    {
        $this->db->where('usuario', $correo);
        $this->db->or_where('correo', $correo);
        $this->db->where('activo', '!=', 0);
        $this->db->where('contrasena', sha1(md5(sha1($contrasenia))));
        $this->db->select('*');
        $datos = $this->db->get("usuario");

        return User::where(function ($query) use ($correo) {
            return $query->where('usuario', $correo)->orWhere('correo', $correo);
        })->where('contrasena', sha1(md5(sha1($contrasenia))))->where('activo', '!=', 0)->first();

        //return $datos->row();
    }

    function validar_usuarioApi($correo, $contrasenia)
    {
        $user = User::where('usuario', $correo)->first();
        $password = User::where('contrasena', sha1(md5(sha1($contrasenia))))->first();

        if(!empty($user)) {
            if(!empty($password)) {
                return User::where(function ($query) use ($correo) {
                    return $query->where('usuario', $correo)->orWhere('correo', $correo);
                })->where('contrasena', sha1(md5(sha1($contrasenia))))->where('activo', '!=', 0)->first();
            } else {
                return 'password incorrect';
            }
        } else {
            return 'user not exist';
        }
    }

    function getUsuario($id_usuario)
    {
        $this->db->where('id_usuario', $id_usuario);
        $this->db->select('*');
        $datos = $this->db->get("usuario");

        return $datos->row();
    }

}
