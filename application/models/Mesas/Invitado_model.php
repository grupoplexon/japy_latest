<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Invitado_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla    = "invitado";
        $this->id_tabla = "id_invitado";
    }

    function getInvitadosGrupoOrder($order_by = 'id_invitado')
    {
        $sql    = "SELECT invitado.*,grupo.grupo,acompanante.id_invitado as invitado_por "
                ."FROM invitado "
                ."LEFT JOIN acompanante ON(id_acompanante = invitado.id_invitado) "
                ."INNER JOIN grupo USING(id_grupo) "
                ."WHERE invitado.id_boda = ".$this->session->userdata("id_boda")." "
                ."ORDER BY $order_by ASC";
        $result = $this->db->query($sql)->result();
        foreach ($result as $key => $value) {
            $value->id_grupo    = encrypt($value->id_grupo);
            $value->id_menu     = encrypt($value->id_menu);
            $value->id_mesa     = encrypt($value->id_mesa);
            $value->id_boda     = encrypt($value->id_boda);
            $value->id_invitado = encrypt($value->id_invitado);
            if ($value->invitado_por != NULL) {
                $sql = "SELECT nombre,apellido FROM invitado WHERE id_invitado = $value->invitado_por IS NOT NULL";
                $resultado = $this->db->query($sql)->row();
                $value->invitado_por = encrypt($value->invitado_por);
                $value->nombre_principal = $resultado->nombre . $resultado->apellido;
            }
        }

        return $result;
    }

    public function getConfirmados()
    {
        try {
            $sql    = 'SELECT count(confirmado) AS total FROM invitado WHERE id_boda = ? AND confirmado = 2 GROUP BY confirmado';
            $result = $this->db->query($sql, [$this->session->userdata('id_boda')])->row();
            if (empty($result)) {
                return 0;
            } else {
                return $result->total;
            }
        } catch (Exception $e) {
            return false;
        }
    }

}
