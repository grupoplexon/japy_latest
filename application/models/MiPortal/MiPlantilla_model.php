<?php

class MiPlantilla_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function getAll(){
        $sql = "SELECT id_plantillaportal,nombre,img_demo FROM plantillaportal ";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_plantillaportal = encrypt($value->id_plantillaportal);
        }
        return $result;
    }
    
    function getPlantillaWeb($id_web) {
        $sql = "SELECT plantillaportal.*"
                . "FROM miportal INNER JOIN plantillaportal USING(id_plantillaportal) "
                . "WHERE id_miportal = $id_web";
        $result = $this->db->query($sql);
        $result = $result->row();
        return $result;
    }
    
    function getPlantilla() {
        $sql = "SELECT plantillaportal.*"
                . "FROM miportal INNER JOIN plantillaportal USING(id_plantillaportal) "
                . "WHERE id_boda = ".$this->session->userdata('id_boda');
        $result = $this->db->query($sql);
        $result = $result->row();
        return $result;
    }
    
    function getImagen($id_plantilla) {
        $sql = "SELECT plantillaportal_imagen.* FROM plantillaportal INNER JOIN plantillaportal_imagen USING(id_plantillaportal) WHERE id_plantillaportal = $id_plantilla";
        $result = $this->db->query($sql);
        $result = $result->result();
        return $result;
    }
    
    function updateImage($data,$id){
        try {
            $this->db->where('id_plantillaportal_imagen',$id);
            return $this->db->update('plantillaportal_imagen', $data);
        } catch (Exception $e) {
            return false;
        }
    }
}