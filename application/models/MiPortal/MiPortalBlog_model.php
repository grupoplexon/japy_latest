<?php

class MiPortalBlog_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function get($id_web,$id_blog) {
        try {
            $sql = "SELECT miportal_blog.* FROM miportal_seccion INNER JOIN miportal_blog USING(id_miportal_seccion) WHERE id_miportal_blog = $id_blog AND id_miportal = $id_web";
            $result = $this->db->query($sql)->row();
            if(!empty($result)){
                $sql = "SELECT miportal_imagen.* 
                        FROM miportal_blog_imagen INNER JOIN miportal_imagen 
                        ON(miportal_blog_imagen.id_miportal_imagen = miportal_imagen.id_miportal_imagen AND 
                            id_miportal_blog = $result->id_miportal_blog)";
                $resultado = $this->db->query($sql)->result();
                $result->imagenes = $resultado;
                
                $sql = "SELECT * 
                        FROM miportal_blog_comentario 
                        WHERE id_miportal_blog = $result->id_miportal_blog";
                $resultado = $this->db->query($sql)->result();
                $result->comentarios = $resultado;
                
                $result->id_miportal_blog = encrypt($result->id_miportal_blog);
                $result->id_miportal_seccion = encrypt($result->id_miportal_seccion);
            }
            return $result;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function getAll($id_web = NULL) {
        try {
            $id_web = ($id_web == NULL)? $this->session->userdata('id_miportal') : $id_web;
            
            $sql = "SELECT miportal_blog.* FROM miportal_seccion INNER JOIN miportal_blog USING(id_miportal_seccion) WHERE id_miportal =" . $id_web;
            $result = $this->db->query($sql);
            $result = $result->result();
            foreach ($result as $key => $value) {
                $sql = "SELECT miportal_imagen.* 
                        FROM miportal_blog_imagen INNER JOIN miportal_imagen 
                        ON(miportal_blog_imagen.id_miportal_imagen = miportal_imagen.id_miportal_imagen AND 
                            id_miportal_blog = $value->id_miportal_blog)";
                $resultado = $this->db->query($sql);
                $value->imagenes = $resultado->result();

                $sql = "SELECT * 
                        FROM miportal_blog_comentario 
                        WHERE id_miportal_blog = $value->id_miportal_blog";
                $resultado = $this->db->query($sql);
                $value->comentarios = $resultado->result();

                $value->id_miportal_blog = encrypt($value->id_miportal_blog);
                $value->id_miportal_seccion = encrypt($value->id_miportal_seccion);
            }
            return $result;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function insert($data) {
        try {
            $this->db->trans_start();
            $this->db->insert('miportal_blog', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();

            return $id;
        } catch (Exception $e) {
            return false;
        }
    }

    public function update($data, $id) {
        try {
            $this->db->where('id_miportal_blog', $id);
            return $this->db->update('miportal_blog', $data);
        } catch (Exception $e) {
            return false;
        }
    }

    public function delete($id) {
        try {
            $this->db->where('id_miportal_blog', $id);
            return $this->db->delete('miportal_blog');
        } catch (Exception $e) {
            return false;
        }
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -     -  C      O       M       E       N       T       A       R       I       O   -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */
    
    public function comprobar($id_miportal_blog,$id_miportal){
        $sql = "SELECT id_miportal "
                . "FROM miportal_blog INNER JOIN miportal_seccion USING(id_miportal_seccion) "
                . "WHERE id_miportal = $id_miportal AND id_miportal_blog = $id_miportal_blog";
        $result = $this->db->query($sql)->row();
        
        if(empty($result)){
            return false;
        }
        return true;
    }
    
    public function insertComentario($data) {
        try {
            $this->db->insert('miportal_blog_comentario', $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteComentario($id) {
        try {
            $this->db->where('id_miportal_blog_comentario', $id);
            return $this->db->delete('miportal_blog_comentario');
        } catch (Exception $e) {
            return false;
        }
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *    -       -      -         I          M       A       G       E       N         -         -        -   
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function insertImagen($data) {
        try {
            $this->db->insert('miportal_blog_imagen', $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteImagen($id) {
        try {
            $this->db->where('id_miportal_imagen', $id);
            return $this->db->delete('miportal_blog_imagen');
        } catch (Exception $e) {
            return false;
        }
    }

    /* ---------------------------------------------------------------------------------------------------------------
     * 
     * 
     * 
     *   P     O       R       T       A       L       -----     I          M       A       G       E       N    
     * 
     * 
     * ---------------------------------------------------------------------------------------------------------------
     */

    public function insertImagenPortal($datos, $id_blog) {
        try {
            $this->db->trans_start();
            $this->db->insert('miportal_imagen', $datos);
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            if ($id_blog != 0) {
                $data = array(
                    "id_miportal_imagen" => $id,
                    "id_miportal_blog" => $id_blog,
                );
                $this->insertImagen($data);
            }

            return $id;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteImagenPortal($id) {
        try {
            $this->db->where('id_miportal_imagen', $id);
            $this->db->delete('miportal_imagen');

            $this->db->where('id_miportal_imagen', $id);
            return $this->db->delete('miportal_blog_imagen');
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function deleteImg($id_blog){
        $sql = "SELECT miportal_imagen.id_miportal_imagen "
                . "FROM miportal_imagen INNER JOIN miportal_blog_imagen "
                . "ON(miportal_imagen.id_miportal_imagen = miportal_blog_imagen.id_miportal_imagen AND id_miportal_blog = $id_blog)";
        $resultado = $this->db->query($sql)->result();
        foreach ($resultado as $key => $value) {
            $this->deleteImagenPortal($value->id_miportal_imagen);
        }
    }
    
}
