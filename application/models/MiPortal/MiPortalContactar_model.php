<?php

class MiPortalContactar_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function comprobar($id_miportal){
        $sql = "SELECT id_miportal_seccion "
                . "FROM miportal_seccion "
                . "WHERE id_miportal = $id_miportal AND tipo = 'contactar'";
        $result = $this->db->query($sql)->row();
        
        if(empty($result)){
            return false;
        }
        return $result->id_miportal_seccion;
    }
    
    public function insertComentario($data) {
        try {
            $result = $this->db->insert('miportal_contactar', $data);
            return $result;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function delete($id){
        try {
            $this->db->where('id_miportal_contactar', $id);
            return $this->db->delete('miportal_contactar');
        } catch (Exception $ex) {
            return false;
        }
    }
}
