<?php

class MiPortalDireccion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getAll($id_web = NULL) {
        try {
            $id_web = ($id_web == NULL)? $this->session->userdata('id_miportal') : $id_web;
            
            $sql = "SELECT miportal_direccion.* FROM miportal_seccion INNER JOIN miportal_direccion USING(id_miportal_seccion) WHERE id_miportal =" . $id_web;
            $result = $this->db->query($sql);
            $result = $result->result();
            foreach ($result as $key => $value) {
                $value->id_miportal_direccion = encrypt($value->id_miportal_direccion);
                $value->id_miportal_seccion = encrypt($value->id_miportal_seccion);
            }
            return $result;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function insert($data) {
        try {
            $this->db->trans_start();
            $this->db->insert('miportal_direccion', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            
            return encrypt($id);
        } catch (Exception $e) {
            return false;
        }
    }

    public function update($data, $id) {
        try {
            $this->db->where('id_miportal_direccion', $id);
            return $this->db->update('miportal_direccion', $data);
        } catch (Exception $e) {
            return false;
        }
    }

    public function delete($id) {
        try {
            $this->db->where('id_miportal_direccion', $id);
            return $this->db->delete('miportal_direccion');
        } catch (Exception $e) {
            return false;
        }
    }

}