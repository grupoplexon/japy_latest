<?php

class MiPortalEncuesta_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function comprobar($id_miportal_encuesta, $id_miportal) {
        $sql = "SELECT id_miportal_encuesta "
                . "FROM miportal_encuesta INNER JOIN miportal_seccion USING(id_miportal_seccion) "
                . "WHERE id_miportal = $id_miportal AND id_miportal_encuesta = $id_miportal_encuesta";
        $result = $this->db->query($sql)->row();

        if (empty($result)) {
            return false;
        }
        return true;
    }

    public function getAll($id_web = NULL) {
        try {
            $id_web = ($id_web == NULL) ? $this->session->userdata('id_miportal') : $id_web;

            $sql = "SELECT miportal_encuesta.* "
                    . "FROM miportal_encuesta INNER JOIN miportal_seccion USING(id_miportal_seccion) "
                    . "WHERE id_miportal = " . $id_web;
            $result = $this->db->query($sql);
            $result = $result->result();
            foreach ($result as $key => $value) {
                $sql = "SELECT miportal_encuesta_opcion.*,count(miportal_encuesta_answer.id_miportal_encuesta_opcion) as contador "
                        . "FROM miportal_encuesta_opcion LEFT JOIN miportal_encuesta_answer USING(id_miportal_encuesta_opcion) "
                        . "WHERE id_miportal_encuesta = " . $value->id_miportal_encuesta
                        . " GROUP BY id_miportal_encuesta_opcion";
                $resultado = $this->db->query($sql)->result();
                foreach ($resultado as $nC => $opt) {
                    $opt->contador = empty($opt->contador) ? 0 : $opt->contador;
                }
                $value->opciones = $resultado;
                $value->id_miportal_encuesta = encrypt($value->id_miportal_encuesta);
                $value->id_miportal_seccion = encrypt($value->id_miportal_seccion);
            }
            return $result;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function getAnswer($id_encuesta) {
        try {
            $sql = "SELECT texto as name,count(miportal_encuesta_answer.id_miportal_encuesta_opcion) as y "
                    . "FROM miportal_encuesta_opcion LEFT JOIN miportal_encuesta_answer USING(id_miportal_encuesta_opcion) "
                    . "WHERE id_miportal_encuesta = $id_encuesta "
                    . " GROUP BY id_miportal_encuesta_opcion";
            $resultado = $this->db->query($sql)->result();
            foreach ($resultado as $nC => $opt) {
                $opt->y = (int)(empty($opt->y) ? 0 : $opt->y);
            }
            RETURN $resultado;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function insertAnswer($data,$id){
        try {
            $sql = "SELECT id_miportal_encuesta_answer "
                    . "FROM miportal_encuesta_answer INNER JOIN miportal_encuesta_opcion USING(id_miportal_encuesta_opcion) "
                    . "WHERE id_miportal_encuesta = $id AND id_miportal_visita =". $data['id_miportal_visita'];
            $resultado = $this->db->query($sql)->result();
            foreach ($resultado as $nC => $opt) {
                $this->db->where('id_miportal_encuesta_answer', $opt->id_miportal_encuesta_answer);
                $this->db->delete('miportal_encuesta_answer');
            }
            
            $this->db->insert('miportal_encuesta_answer', $data);
            if(count($resultado) > 0){
                return 'update';
            }else{
                return 'insert';
            }
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function insertEncuesta($data) {
        try {
            $this->db->trans_start();
            $this->db->insert('miportal_encuesta', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            return $id;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function insertOpcion($data) {
        try {
            $this->db->trans_start();
            $this->db->insert('miportal_encuesta_opcion', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();

            return encrypt($id);
        } catch (Exception $ex) {
            return false;
        }
    }

    public function insertOpcionBatch($data) {
        try {
            $this->db->insert_batch('miportal_encuesta_opcion', $data);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function updateOpcion($data, $id) {
        try {
            $this->db->where('id_miportal_encuesta_opcion', $id);
            return $this->db->update('miportal_encuesta_opcion', $data);
        } catch (Exception $ex) {
            return false;
        }
    }

    public function updateEncuesta($data, $id) {
        try {
            $this->db->where('id_miportal_encuesta', $id);
            return $this->db->update('miportal_encuesta', $data);
        } catch (Exception $ex) {
            return false;
        }
    }

    public function deleteOption($id, $id_encuesta) {
        try {
            $this->db->where('id_miportal_encuesta_opcion', $id);
            $this->db->where('id_miportal_encuesta', $id_encuesta);
            return $this->db->delete('miportal_encuesta_opcion');
        } catch (Exception $ex) {
            return false;
        }
    }

    public function deleteEncuesta($id) {
        try {
            $this->db->where('id_miportal_encuesta', $id);
            return $this->db->delete('miportal_encuesta');
        } catch (Exception $ex) {
            return false;
        }
    }

}
