<?php

class MiPortalLibro_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function comprobar($id_miportal){
        $sql = "SELECT id_miportal_seccion "
                . "FROM miportal_seccion "
                . "WHERE id_miportal = $id_miportal AND tipo = 'libro'";
        $result = $this->db->query($sql)->row();
        
        if(empty($result)){
            return false;
        }
        return $result->id_miportal_seccion;
    }
    
    public function getAll($id_web = NULL){
        try{
            $id_web = ($id_web == NULL)? $this->session->userdata('id_miportal') : $id_web;
            $sql = "SELECT miportal_libro.* FROM miportal_seccion INNER JOIN miportal_libro USING(id_miportal_seccion) WHERE estado = 1 AND id_miportal =" . $id_web;
            $result = $this->db->query($sql);
            $result = $result->result();
            foreach ($result as $key => $value) {
                $value->id_miportal_libro = encrypt($value->id_miportal_libro); 
                $value->id_miportal_seccion = encrypt($value->id_miportal_seccion); 
            }
            return $result;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function insert($data){
        try {
            return $this->db->insert('miportal_libro', $data); 
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    public function delete($id){
        try {
            $this->db->where('id_miportal_libro',$id);
            return $this->db->update('miportal_libro', array('estado' => 0));
        } catch (Exception $e) {
            return false;
        }
    }
}
