<?php

class MiPortalSeccion_Imagen_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function insert($data,$id_seccion){
        try{
            $this->db->trans_start();
            $this->db->insert('miportal_imagen', $data); 
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            
            
            $data = array(
                "id_miportal_imagen" => $id,
                "id_miportal_seccion" => $id_seccion,
            );
            return $this->db->insert('miportal_seccion_imagen', $data); 
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function update($data,$id_miportal_imagen){
        try {
            $this->db->where('id_miportal_imagen',$id_miportal_imagen);
            return $this->db->update('miportal_imagen', $data);
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function delete($id_imagen){
        try{
            $this->db->where('id_miportal_imagen', $id_imagen);
            $this->db->delete('miportal_seccion_imagen'); 
            
            $this->db->where('id_miportal_imagen', $id_imagen);
            return $this->db->delete('miportal_imagen'); 
        } catch (Exception $ex) {
            return false;
        }
    }
}