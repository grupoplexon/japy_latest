<?php

class MiPortalSeccion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function get($id) {
        $id = decrypt($id);
        $sql = "SELECT * FROM miportal_seccion WHERE id_miportal_seccion = $id";
        $result = $this->db->query($sql);
        $result = $result->row();
        if (!empty($result)) {
            $result->id_miportal_seccion = encrypt($result->id_miportal_seccion);
            $result->id_miportal = encrypt($result->id_miportal);
            return $result;
        } else {
            return false;
        }
    }
    
    function getSeccion($id,$tipo) {
        $sql = "SELECT * FROM miportal_seccion WHERE id_miportal = $id AND tipo = '$tipo'";
        $result = $this->db->query($sql);
        $result = $result->row();
        if (!empty($result)) {
            $result->id_miportal_seccion = encrypt($result->id_miportal_seccion);
            $result->id_miportal = encrypt($result->id_miportal);
            return $result;
        } else {
            return false;
        }
    }
    
    function update_seccion($datos,$id){
        try {
            $this->db->where('id_miportal',$this->session->userdata('id_miportal'));
            $this->db->where('id_miportal_seccion',$id);
            return $this->db->update('miportal_seccion', $datos);
        } catch (Exception $e) {
            return false;
        }
    }
    
    function insertImg($datos){
        try{
            $datos['miportal'] = decrypt($datos['miportal']);
            $this->db->trans_start();
            $this->db->insert('miportal_imagen', $datos);
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            $data = array(
                "img_id" => $id,
                "img_table" => 'miportal_imagen',
            );
            
            return $this->assignImg($data,$datos['miportal']);
        } catch (Exception $ex) {
            return false;
        }
        
    }
    
    function assignImg($data,$id){
        try{
            $this->db->where('id_miportal', $id);
            return $this->db->update('miportal', $data);
        } catch (Exception $ex) {
            return false;
        }
    }
    
    function getImagen($tipo){
        $sql = "SELECT miportal_imagen.* FROM miportal_seccion_imagen INNER JOIN miportal_imagen USING(id_miportal_imagen) INNER JOIN miportal_seccion USING(id_miportal_seccion) WHERE tipo = '$tipo'";
        $result = $this->db->query($sql);
        $result = $result->row();
        return $result;
    }
    
    function getAllShort($id_web) {
        $sql = "SELECT id_miportal_seccion,miportal_seccion.titulo,miportal_seccion.visible,miportal_seccion.activo,miportal_seccion.tipo
                FROM miportal INNER JOIN miportal_seccion USING(id_miportal)
                WHERE id_miportal = " . $id_web . " ORDER BY miportal_seccion.titulo DESC";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_miportal_seccion = encrypt($value->id_miportal_seccion);
        }
        return $result;
    }
    
    function getAll() {
        $sql = "SELECT miportal_seccion.*
                FROM miportal INNER JOIN miportal_seccion USING(id_miportal)
                WHERE id_boda = " . $this->session->userdata('id_boda') . " ORDER BY miportal_seccion.titulo DESC";
        $result = $this->db->query($sql);
        $result = $result->result();
        foreach ($result as $key => $value) {
            $value->id_miportal_seccion = encrypt($value->id_miportal_seccion);
            $value->id_miportal = encrypt($value->id_miportal);
        }
        return $result;
    }

    function restaurar_seccion($tipo, $id) {
        try {
            $id = decrypt($id);
            switch (strtolower($tipo)) {
                case 'blog':
                    $datos = array(
                        "titulo" => "BLOG DE BODA",
                        "subtitulo" => '',
                        "descripcion" => "¡Atención, atención! No te desconectes de esta sección porque aquí encontrarás las noticias más frescas alrededor de la organización de la boda.",
                        "activo" => 2,
                        "visible" => 1,
                    );
                    $this->db->delete('miportal_blog', array('id_miportal_seccion' => $id));
                    break;
                case 'direccion':
                    $datos = array(
                        "titulo" => "DIRECCIONES",
                        "subtitulo" => '',
                        "descripcion" => "No queremos que te pierdas la boda por nada del mundo. Aquí podrás consultar siempre la dirección de la ceremonia y de la recepción. ¡No hay excusas para llegar tarde!",
                        "activo" => 2,
                        "visible" => 1,
                    );
                    $this->db->delete('miportal_direccion', array('id_miportal_seccion' => $id));
                    break;
                case 'album':
                    $datos = array(
                        "titulo" => "NUESTRAS FOTOS",
                        "subtitulo" => '',
                        "descripcion" => "Estas son algunas de nuestras fotos favoritas, esas que nos emocionan cada vez que las miramos. Estamos muy felices de contar contigo y enseñarte los momentos que nos han llevado hasta aquí",
                        "activo" => 2,
                        "visible" => 1,
                    );
                    $this->db->delete('miportal_album', array('id_miportal_seccion' => $id));
                    break;
                case 'encuesta':
                    $datos = array(
                        "titulo" => "ENCUESTAS",
                        "subtitulo" => '',
                        "descripcion" => "Tenemos dudas sobre algunos detalles de la boda y tú eres de gran ayuda. Responde a la encuesta para echarnos una mano, gracias!",
                        "activo" => 2,
                        "visible" => 1,
                    );
                    $this->db->delete('miportal_encuesta', array('id_miportal_seccion' => $id));
                    break;
                case 'test':
                    $datos = array(
                        "titulo" => "TESTS",
                        "subtitulo" => '',
                        "descripcion" => "¿Crees que nos conoces? Hemos preparado algunas preguntas para ponerte a prueba, el que más respuestas correctas haga se llevará un premio el día de la boda ¿lo quieres?",
                        "activo" => 2,
                        "visible" => 1,
                    );
                    $this->db->delete('miportal_test', array('id_miportal_seccion' => $id));
                    break;
                case 'evento':
                    $datos = array(
                        "titulo" => "EVENTOS",
                        "subtitulo" => '',
                        "descripcion" => "Antes de la boda tenemos algunas citas importantes que no queremos que te pierdas. Así que libérate la agenda los días que toquen y prepárate, porque los días que faltan hasta la boda serán memorables.",
                        "activo" => 2,
                        "visible" => 1,
                    );
                    $this->db->delete('miportal_evento', array('id_miportal_seccion' => $id));
                    break;
                case 'video':
                    $datos = array(
                        "titulo" => "NUESTROS VIDEOS",
                        "subtitulo" => '',
                        "descripcion" => "Nuestra historia ha sido para nosotros más bonita que cualquier película romántica. Por eso queremos compartir contigo unos videos muy especiales para nosotros (también hemos puesto algunos de Youtube para reírnos un rato",
                        "activo" => 2,
                        "visible" => 1,
                    );
                    $this->db->delete('miportal_video', array('id_miportal_seccion' => $id));
                    break;
            }
            if (isset($datos) && !empty($datos)) {
                $this->db->where('id_miportal_seccion', $id);
                return $this->db->update('miportal_seccion', $datos);
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    function updatePrivado(){
        try {
            $sql = "SELECT id_miportal FROM miportal WHERE id_boda = ". $this->session->userdata('id_boda')." LIMIT 1";
            $result = $this->db->query($sql);
            $result = $result->row();
            
            $sql = "UPDATE miportal_seccion SET visible = 2 WHERE (activo = 0 OR activo = 1) AND id_miportal = ".$result->id_miportal;
            $result = $this->db->query($sql);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    function update($datos, $id_miportal_seccion) {
        try {
            $id_miportal_seccion = decrypt($id_miportal_seccion);
            $this->db->where('id_miportal_seccion', $id_miportal_seccion);
            return $this->db->update('miportal_seccion', $datos);
        } catch (Exception $e) {
            return false;
        }
    }

}
