<?php

class MiPortalTest_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function comprobar($id_miportal_test,$id_miportal){
        $sql = "SELECT respuesta "
                . "FROM miportal_test INNER JOIN miportal_seccion USING(id_miportal_seccion) "
                . "WHERE id_miportal = $id_miportal AND id_miportal_test = $id_miportal_test";
        $result = $this->db->query($sql)->row();
        
        if(empty($result)){
            return false;
        }
        return $result->respuesta;
    }
    
    public function getAnswer($id_web, $id_visita) {
        try {
            $id_visita = empty($id_visita)? 0 : $id_visita;
            
            $sql = "SELECT DISTINCT miportal_test.*,miportal_test_answer.respuesta as respuesta_visita "
                    . "FROM miportal_seccion INNER JOIN miportal_test USING(id_miportal_seccion) "
                    . "LEFT JOIN miportal_test_answer ON(miportal_test_answer.id_miportal_test = miportal_test.id_miportal_test AND id_miportal_visita = $id_visita) "
                    . "WHERE id_miportal = $id_web "
                    . "GROUP BY id_miportal_test";
            $result = $this->db->query($sql)->result();
            
            foreach ($result as $key => $value) {
                /*$sql = "SELECT miportal_test_answer.id_miportal_test,miportal_test_answer.respuesta, COUNT(miportal_test_answer.respuesta) AS total
                        FROM miportal_test INNER JOIN miportal_test_answer ON(miportal_test_answer.id_miportal_test = miportal_test.id_miportal_test AND miportal_test_answer.id_miportal_test = $value->id_miportal_test)
                        GROUP BY miportal_test_answer.respuesta";
                $respuestas = $this->db->query($sql);
                $respuestas = $respuestas->result();
                
                $value->respuestas = $respuestas;*/
                $value->id_miportal_test = encrypt($value->id_miportal_test);
                $value->id_miportal_seccion = encrypt($value->id_miportal_seccion);
            }
            return $result;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function getAll($id_web = NULL) {
        try {
            $id_web = ($id_web == NULL)? $this->session->userdata('id_miportal') : $id_web;
            
            $sql = "SELECT miportal_test.* FROM miportal_seccion INNER JOIN miportal_test USING(id_miportal_seccion) WHERE id_miportal =" . $id_web;
            $result = $this->db->query($sql);
            $result = $result->result();
            foreach ($result as $key => $value) {
                $sql = "SELECT miportal_test_answer.id_miportal_test,miportal_test_answer.respuesta, COUNT(miportal_test_answer.respuesta) AS total
                        FROM miportal_test INNER JOIN miportal_test_answer ON(miportal_test_answer.id_miportal_test = miportal_test.id_miportal_test AND miportal_test_answer.id_miportal_test = $value->id_miportal_test)
                        GROUP BY miportal_test_answer.respuesta";
                $respuestas = $this->db->query($sql);
                $respuestas = $respuestas->result();
                
                $value->respuestas = $respuestas;
                $value->id_miportal_test = encrypt($value->id_miportal_test);
                $value->id_miportal_seccion = encrypt($value->id_miportal_seccion);
            }
            return $result;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function insertAnswer($data){
        try {
            return $this->db->insert('miportal_test_answer',$data);
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function deleteAnswer($id_miportal_test,$respuesta){
        try {
            $this->db->where('respuesta',$respuesta);
            $this->db->where('id_miportal_test', $id_miportal_test);
            return $this->db->delete('miportal_test_answer');
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function insert($data) {
        try {
            
            $this->db->trans_start();
            $this->db->insert('miportal_test', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            
            return encrypt($id);
        } catch (Exception $e) {
            return false;
        }
    }

    public function update($data, $id) {
        try {
            $this->db->where('id_miportal_test', $id);
            return $this->db->update('miportal_test', $data);
        } catch (Exception $e) {
            return false;
        }
    }

    public function delete($id) {
        try {
            $this->db->where('id_miportal_test', $id);
            return $this->db->delete('miportal_test');
        } catch (Exception $e) {
            return false;
        }
    }

}