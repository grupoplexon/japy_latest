<?php

class MiPortalVisita_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data) {
        try {
            
            $this->db->trans_start();
            $this->db->insert('miportal_visita', $data);
            $id = $this->db->insert_id();
            $this->db->trans_complete();
            
            return $id;
        } catch (Exception $e) {
            return false;
        }
    }

}
