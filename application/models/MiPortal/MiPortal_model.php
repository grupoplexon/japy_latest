<?php

class MiPortal_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function validar($id, $contrasenia) {
        try {
            $contrasenia = md5($contrasenia);
            
            $sql = "SELECT id_miportal
                FROM miportal
                WHERE id_miportal = $id AND contrasena = '$contrasenia'";
            $result = $this->db->query($sql)->result();
            if (count($result) > 0) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    function getBoda($id) {
        try {
            $sql = "SELECT id_boda,id_cliente,id_usuario 
                FROM cliente INNER JOIN boda USING(id_boda) 
                    INNER JOIN miportal USING(id_boda) 
                WHERE id_miportal = $id";
            $result = $this->db->query($sql)->result();
            return $result;
        } catch (Exception $e) {
            return false;
        }
    }

    function getIDPortal($id) {
        try {
            $sql = "SELECT id_miportal
                FROM miportal
                WHERE id_boda = " . $id;
            $result = $this->db->query($sql)->row();
            return $result;
        } catch (Exception $e) {
            return false;
        }
    }

    function getPortal($id) {
        try {
            $sql = "SELECT miportal.*,
                (CASE miportal.img_table 
                WHEN 'plantillaportal_imagen' THEN (SELECT plantillaportal_imagen.url_img FROM plantillaportal_imagen WHERE plantillaportal_imagen.id_plantillaportal_imagen = miportal.img_id) 
                WHEN 'miportal_imagen' THEN (SELECT miportal_imagen.base FROM miportal_imagen WHERE miportal_imagen.id_miportal_imagen = miportal.img_id) END) as base,
                (CASE miportal.img_table 
                WHEN 'plantillaportal_imagen' THEN (SELECT '') 
                WHEN 'miportal_imagen' THEN (SELECT miportal_imagen.mime FROM miportal_imagen WHERE miportal_imagen.id_miportal_imagen = miportal.img_id) END) as mime
                FROM miportal 
                WHERE id_miportal = $id";
            $result = $this->db->query($sql)->row();
            if (!empty($result)) {
                $result->id_miportal = encrypt($result->id_miportal);
                if (isset($result) && empty($result->base)) {
                    $consulta = "SELECT url_img "
                            . "FROM miportal INNER JOIN plantillaportal ON(miportal.id_plantillaportal = plantillaportal.id_plantillaportal AND miportal.id_boda = $result->id_boda) "
                            . "INNER JOIN plantillaportal_imagen ON(plantillaportal_imagen.id_plantillaportal = plantillaportal.id_plantillaportal AND plantillaportal_imagen.default = 1)";
                    $respuesta = $this->db->query($consulta)->row();

                    $result->base = $respuesta->url_img;
                }
                return $result;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    function get($id = null) {
        try {
            $id = ($id == null) ? $this->session->userdata('id_boda') : $id;
            $sql = "SELECT miportal.*,
                (CASE miportal.img_table 
                WHEN 'plantillaportal_imagen' THEN (SELECT plantillaportal_imagen.url_img FROM plantillaportal_imagen WHERE plantillaportal_imagen.id_plantillaportal_imagen = miportal.img_id) 
                WHEN 'miportal_imagen' THEN (SELECT miportal_imagen.base FROM miportal_imagen WHERE miportal_imagen.id_miportal_imagen = miportal.img_id) END) as base,
                (CASE miportal.img_table 
                WHEN 'plantillaportal_imagen' THEN (SELECT '') 
                WHEN 'miportal_imagen' THEN (SELECT miportal_imagen.mime FROM miportal_imagen WHERE miportal_imagen.id_miportal_imagen = miportal.img_id) END) as mime
                FROM miportal 
                WHERE id_boda = " . $id;
            $result = $this->db->query($sql)->row();
            if (!empty($result)) {
                $result->id_miportal = encrypt($result->id_miportal);
                if (isset($result) && empty($result->base)) {
                    $consulta = "SELECT url_img "
                            . "FROM miportal INNER JOIN plantillaportal ON(miportal.id_plantillaportal = plantillaportal.id_plantillaportal AND miportal.id_boda = " . $this->session->userdata('id_boda') . ") "
                            . "INNER JOIN plantillaportal_imagen ON(plantillaportal_imagen.id_plantillaportal = plantillaportal.id_plantillaportal AND plantillaportal_imagen.default = 1)";
                    $respuesta = $this->db->query($consulta)->row();
                    $result->base = $respuesta->url_img;
                }
                return $result;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    function update($datos, $id_boda) {
        try {
            $this->db->where('id_boda', $id_boda);
            return $this->db->update('miportal', $datos);
        } catch (Exception $e) {
            return false;
        }
    }

}
