<?php
    class Foro_model extends CI_Model{
        
        public function __construct() {
            parent::__construct();
            $this->load->database();
        }
        
        function getUltimasFotos(){
            $sql =  "SELECT
                        f.id_foto,
                        f.id_grupo,
                        f.titulo,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    
                    ORDER BY 
                        f.fecha_creacion DESC
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
		function num_debates_encontrados($debate){
        $sql =  "SELECT 
                    id_debate
                FROM
                    debates
                WHERE
                    titulo_debate LIKE '%$debate%'";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
	
        function getFotosMasVistas(){
            $sql =  "SELECT 
                        f.id_foto,
                        f.id_grupo,
                        f.titulo,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    
                    ORDER BY
                        f.vistas DESC 
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroFotosFecha($inicio){
            $sql =  "SELECT
                        f.id_foto,
                        f.id_grupo,
                        f.titulo,
                        U.id_usuario,
                        U.usuario
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    
                    ORDER BY 
                        f.fecha_creacion DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroFotosVistas($inicio){
            $sql =  "SELECT
                        f.id_foto,
                        f.id_grupo,
                        f.titulo,
                        U.id_usuario,
                        U.usuario
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    
                    ORDER BY
                        f.vistas DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroFotosComentarios($inicio){
            $sql =  "SELECT 
                        f.id_foto,
                        f.id_grupo,
                        f.titulo,
                        U.id_usuario,
                        U.usuario
                    FROM 
                        foto_publicada F
                    INNER JOIN usuario U USING (id_usuario)
                    
                    ORDER BY
                        f.comentarios DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
		
		function filtroFotosDenuncias($inicio){
            $sql =  "SELECT 
                        f.id_foto,
                        f.id_grupo,
                        f.titulo,
                        U.id_usuario,
                        U.usuario,
						DF.razon
                    FROM 
                        denuncias_fotos DF
                     INNER JOIN foto_publicada F ON F.id_foto = DF.id_foto
                    INNER JOIN usuario U ON (U.id_usuario = F.id_usuario)
                    ORDER BY
                        DF.fecha_creacion DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
		
		function filtroVideosDenunciados($inicio){
            $sql =  "SELECT 
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime,
			DV.razon
                    FROM 
                        denuncias_videos DV
                     INNER JOIN video_publicado V ON V.id_video = DV.id_video
                    INNER JOIN usuario U ON (U.id_usuario = V.id_usuario)
                    ORDER BY
                        DV.fecha_creacion DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
		
		function filtroFotosDenunciasComentarios($inicio){
            $sql =  "SELECT 
                        f.id_foto,
                        f.id_grupo,
                        f.titulo,
                        U.id_usuario,
                        U.usuario,
						DC.razon,
						DC.id_comentario
                    FROM 
                        denuncia_comentarios DC
                      INNER JOIN comentarios_foto C ON C.id_comentario = DC.id_comentario
                     INNER JOIN foto_publicada F ON F.id_foto = C.id_foto
                    INNER JOIN usuario U ON (U.id_usuario = F.id_usuario)
                    WHERE
                        DC.tipo = 2
                    ORDER BY
                        f.comentarios DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
		
		function filtroVideosDenunciasComentarios($inicio){
            $sql =  "SELECT 
                       V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime,
						DC.razon,
						DC.id_comentario
                    FROM 
                        denuncia_comentarios DC
                      INNER JOIN comentarios_video C ON C.id_comentario = DC.id_comentario
                     INNER JOIN video_publicado V ON V.id_video = C.id_video
                    INNER JOIN usuario U ON (U.id_usuario = V.id_usuario)
                    WHERE
                        DC.tipo = 3
                    ORDER BY
                        DC.fecha_creacion DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }

			function getTotalFotosDenunciadas(){
				$sql =  "SELECT
							id_foto
						FROM
							denuncias_fotos";
				$result = $this->db->query($sql)->num_rows();
				return $result;
			}
			
			function getTotalVideosDenunciados(){
				$sql =  "SELECT
							id_video
						FROM
							denuncias_videos";
				$result = $this->db->query($sql)->num_rows();
				return $result;
			}
			
			function getTotalFotosDenunciadasComentarios(){
				$sql =  "SELECT
							id_comentario							
						FROM
							denuncia_comentarios
							WHERE tipo = 2";
				$result = $this->db->query($sql)->num_rows();
				return $result;
			}
			
			function getTotalVideosDenunciadasComentarios(){
				$sql =  "SELECT
							id_comentario							
						FROM
							denuncia_comentarios
							WHERE tipo = 3";
				$result = $this->db->query($sql)->num_rows();
				return $result;
			}

			function getTotalFotos(){
				$sql =  "SELECT
							id_foto
						FROM
							foto_publicada
						";
				$result = $this->db->query($sql)->num_rows();
				return $result;
			}
        
        function getUltimosVideos(){
            $sql =  "SELECT
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    
                    ORDER BY 
                        V.fecha_creacion DESC
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getVideosMasVistos(){
            $sql =  "SELECT 
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)

                    ORDER BY
                        V.vistas DESC 
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroVideosFecha($inicio){
            $sql =  "SELECT
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE 
                        V.activo = 1
                    ORDER BY 
                        V.fecha_creacion DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroVideosVistas($inicio){
            $sql =  "SELECT
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        V.activo = 1
                    ORDER BY
                        V.vistas DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroVideosComentarios($inicio){
            $sql =  "SELECT 
                        V.id_video,
                        V.id_grupo,
                        V.titulo,
                        V.direccion_web,
                        U.id_usuario,
                        U.usuario,
                        U.mime
                    FROM 
                        video_publicado V
                    INNER JOIN usuario U USING (id_usuario)
                    WHERE
                        V.activo = 1
                    ORDER BY
                        V.comentarios DESC 
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getTotalVideos(){
            $sql =  "SELECT
                        id_video
                    FROM
                        video_publicado
                    ";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function getUltimosDebates(){
            $sql =  "SELECT 
                        D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime
                    FROM 
                        debates D
                    INNER JOIN usuario U USING (id_usuario)
                    
                    ORDER BY 
                        D.fecha_creacion DESC
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getDebatesPopulares(){
            $sql =  "SELECT
                        D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime
                    FROM
                        debates D
                    INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                    
                    ORDER BY
                        D.vistas DESC
                    LIMIT 11";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroDebatesRecientes($inicio){
            $sql =  "SELECT 
                        D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime
                    FROM 
                        debates D
                    INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                    
                    ORDER BY 
                        D.fecha_creacion DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function filtroDebatesPopulares($inicio){
            $sql =  "SELECT 
                        D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime
                    FROM 
                        debates D
                    INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                    
                    ORDER BY 
                        D.vistas DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
		
		function filtroDebatesDenunciados($inicio){
            $sql =  "SELECT D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime,
						DD.razon
                        FROM denuncias_debates DD
			INNER JOIN debates D ON D.id_debate = DD.id_debate
            INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
			ORDER BY 
                        DD.fecha_creacion DESC
                    LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
		
		function filtroComentariosDebatesDenunciados($inicio){
            $sql =  "SELECT D.id_debate,
                        D.fecha_creacion,
                        D.titulo_debate,
                        D.debate,
                        D.id_usuario,
                        D.vistas,
                        D.comentarios,
                        U.usuario,
                        U.mime,
						DC.razon,
						DC.id_comentario
						FROM denuncia_comentarios DC 
						INNER JOIN comentarios C ON C.id_comentario = DC.id_comentario
						INNER JOIN debates D ON D.id_debate = C.id_debate
						INNER JOIN usuario U ON U.id_usuario = D.id_usuario
						WHERE DC.tipo=1
						ORDER BY 
                        DC.fecha_creacion DESC
						LIMIT 16 OFFSET $inicio";
            $result = $this->db->query($sql)->result();
            return $result;
        }
        
        function getTotalDebates(){
            $sql =  "SELECT 
                        id_debate
                    FROM 
                        debates
                    ";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
		
		function getTotalDenuncias(){
            $sql =  "SELECT 
                        id_debate
                    FROM 
                        denuncias_debates";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
		
		function getTotalDesactivadas($table){
            $sql =  "SELECT 
                        count(*) as total
                    FROM 
                        $table where activo = 0";
            $result = $this->db->query($sql)->row();
            return $result;
        }
		
		function getTotalComentariosDesactivados($table, $tipo){
            $sql =  "SELECT 
                        count(*) as total
                    FROM 
                        $table T inner join denuncia_comentarios DC on 
						T.id_comentario = DC.id_comentario
						where T.activo = 0 and DC.tipo = $tipo";
            $result = $this->db->query($sql)->row();
            return $result;
        }
		
		function getTotalDenunciasComentarios(){
            $sql =  "SELECT 
                        id_comentario
                    FROM 
                        denuncia_comentarios WHERE tipo = 1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function getAmistad($id_usuario){
            $id_usuario_logeado = $this->session->userdata('id_usuario');
            $sql =  "SELECT 
                        amistad, 
                        solicitud,
                        id_usuario_envio,
                        id_usuario_confirmacion
                    FROM 
                        amistad_usuarios 
                    WHERE 
                        id_usuario_envio = $id_usuario 
                    AND id_usuario_confirmacion = $id_usuario_logeado
                    OR id_usuario_envio = $id_usuario_logeado 
                    AND id_usuario_confirmacion = $id_usuario";
            $result = $this->db->query($sql)->row();
            return $result;
        }
        
        function getTotalAmigos($id_usuario){
            $sql = "SELECT
                        id_usuario_confirmacion
                    FROM
                        amistad_usuarios 
                    WHERE
                        amistad = 1
                    AND id_usuario_envio = $id_usuario
                    OR id_usuario_confirmacion = $id_usuario
                    AND amistad = 1";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
    
        function getTotalMensajes($id_usuario){
            $sql = "SELECT activo FROM comentarios WHERE id_usuario = $id_usuario";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }

        function getTotalDebatesTarjeta($id_usuario){
            $sql = "SELECT activo FROM debates WHERE id_usuario = $id_usuario";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }

        function getTotalFotosTarjeta($id_usuario){
            $sql = "SELECT activo FROM foto_publicada WHERE id_usuario = $id_usuario";
            $result = $this->db->query($sql)->num_rows();
            return $result;
        }
        
        function filtro1($color,$temporada,$estilo,$estado,$fecha,$pagina){ 
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro2($color,$temporada,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro3($color,$temporada,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro4($color,$estilo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro5($color,$temporada,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.color = '$color'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada' 
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.color = '$color'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro6($temporada,$estilo,$estado,$fecha,$pagina){
         $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'    
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro7($color,$temporada,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color= '$color'
            AND B.estilo = '$estilo'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro8($color,$temporada,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro9($color,$temporada,$fecha,$pagina){
         $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro10($color,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro11($color,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro12($temporada,$estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro13($temporada,$estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro14($estilo,$estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro15($color,$temporada,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro16($color,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro17($color,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = '$color'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro18($color,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.color = '$color'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro19($temporada,$estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro20($temporada,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = '$temporada'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro21($temporada,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estacion = '$temporada'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro22($estilo,$estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estilo = '$estilo'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro23($estilo,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estilo = '$estilo'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro24($estado,$fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = '$fecha'
            AND B.estado_boda = '$estado'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro25($color,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($color))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.color = ?
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql,array($color))->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro26($temporada,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($temporada))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estacion = ?
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql,array($temporada))->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro27($estilo,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estilo = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($estilo))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estilo = ?
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql,array($estilo))->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro28($estado,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estado_boda = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($estado))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.estado_boda = ?
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql,array($estado))->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro29($fecha,$pagina){
        $contador = 0;
        $fecha = DateTime::createFromFormat('j/m/Y', $fecha);
        $fecha = $fecha->format('Y-m-d');
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = ?
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql,array($fecha))->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND B.fecha_boda = ?
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql,array($fecha))->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
    function filtro30($pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            
            WHERE
                U.activo = 1
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    
	function buscar($debate,$inicio){
        $sql =  "SELECT 
                    D.id_debate,
                    D.titulo_debate,
                    D.comentarios,
                    D.vistas,
                    D.fecha_creacion,
                    D.debate,
                    U.id_usuario,
                    U.mime,
                    U.usuario
                FROM
                    debates D
                INNER JOIN usuario U USING (id_usuario)
                WHERE
                    D.titulo_debate LIKE '%$debate%'
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }
	
    function buscarMiembro($usuario,$pagina){
        $contador = 0;
        $sql = "SELECT
                U.id_usuario
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND U.usuario LIKE '%$usuario%'
            ORDER BY
                B.fecha_boda ASC";
        $contador = $this->db->query($sql)->num_rows();
        $total_paginas = (int)($contador / 12);
        if($total_paginas == 0){
            $total_paginas = 1;
        }
        if($pagina <= $total_paginas){
            $inicio = ($pagina - 1) * 12;
        }
        $sql = "SELECT
                U.id_usuario,
                U.usuario,
                U.fecha_creacion,
                U.puntos,
                B.ciudad_boda,
                B.estado_boda,
                B.fecha_boda,
                U.foto
            FROM
                cliente C
            INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
            INNER JOIN boda B ON (B.id_boda = C.id_boda)
            WHERE
                U.activo = 1
            AND U.usuario LIKE '%$usuario%'
            ORDER BY
                B.fecha_boda ASC
            LIMIT 12 OFFSET $inicio";
        $miembros = $this->db->query($sql)->result();
        $datos = array(
            'total_paginas' => $total_paginas,
            'contador' => $contador,
            'miembros' => $miembros
        );
        return $datos;
    }
    }