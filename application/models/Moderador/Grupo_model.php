<?php
class Grupo_model extends CI_Model{

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	function getNombreGrupo($id_grupo){
        $sql = "select nombre,imagen from grupos_comunidad where id_grupos_comunidad = $id_grupo";
        $result = $this->db->query($sql)->row();
        return $result;
    }
	
//    -----------------------SECCION TODO---------------------------------------
    
    function getDebatesGrupo($id_grupo){
        $sql = "SELECT
                    D.id_debate,
                    (SELECT MAX(CO.fecha_creacion) FROM comentarios CO WHERE D.id_debate = CO.id_debate) AS fecha,
                    
                    D.id_grupo
                FROM
                    debates D
                 WHERE
                    D.id_grupo = ?
                GROUP BY
                    id_debate
                ORDER BY
                    fecha DESC
                LIMIT 5";
        $result = $this->db->query($sql, array($id_grupo))->result();
        $i = 0;
        $debates = "";
        foreach($result as $debate){
            $sql = "SELECT 
                        D.id_debate,
                        D.titulo_debate,
                        D.fecha_creacion,
                        D.debate,
                        D.id_usuario,
						U.mime,
                        U.usuario
                    FROM
                        debates D
                    INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                    WHERE
                        D.id_debate = ?";
			$fecha_comentario= "";
            $debates[$i] = $this->db->query($sql,array($debate->id_debate))->row();
            $sql = "SELECT activo FROM comentarios WHERE id_debate = $debate->id_debate";
            $num_comentarios = $this->db->query($sql)->num_rows();
            $debates[$i]->num_comentarios = $num_comentarios;
			if(isset($debate->fecha) && $debate->fecha != NULL){
            $fecha_comentario = relativeTimeFormat($debate->fecha,$mini=FALSE);
			}
            if(strlen($fecha_comentario) <= 10 && strlen($fecha_comentario) > 4){
                $fecha_comentario = "Hace $fecha_comentario";
            }else if(strlen($fecha_comentario) > 10){
                $fecha_comentario = "El $fecha_comentario";
            }
            $fecha_creacion = relativeTimeFormat($debates[$i]->fecha_creacion,$mini=FALSE);
            if(strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4){
                $fecha_creacion = "Hace $fecha_creacion";
            }else if(strlen($fecha_creacion) > 10){
                $fecha_creacion = "El $fecha_creacion";
            }
            $debates[$i]->fecha_creacion = $fecha_creacion;
            $debates[$i]->fecha_comentario = $fecha_comentario;
            $sub_debate = substr($debates[$i]->debate, 0, 255);
            $sub_debate = $sub_debate . '...';
            $debates[$i]->debate = $sub_debate;
            $debates[$i]->url_usuario = base_url()."index.php/novios/comunidad/Perfil/".$debates[$i]->id_usuario;
            $i++;
        }
        return $debates;
    }
    
    function getFotosRecientesGrupo($id_grupo){
        $sql = "SELECT
                    F.id_foto,
                    F.id_usuario,
                    F.id_grupo,
                    F.titulo,
                    U.usuario,
					U.mime
                FROM
                    foto_publicada F
                    INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
                WHERE
                    F.id_grupo = $id_grupo    
                ORDER BY
                    F.fecha_creacion DESC
                LIMIT 11;";
        $result = $this->db->query($sql)->result();
        return $result;
    }
    
    function getVideosRecientesGrupo($id_grupo){
        $sql = "SELECT
                    V.id_video,
                    V.id_usuario,
                    V.id_grupo,
                    V.direccion_web,
                    V.descripcion,
                    V.titulo,
                    U.usuario,
					U.mime
                FROM
                    video_publicado V
                    INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
                WHERE
                    V.id_grupo = ?
                ORDER BY
                    V.fecha_creacion DESC
                LIMIT 11;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
//    ------------------------SECCION FOTOS-------------------------------------
    
    function totalFotos($id_grupo){
        $sql = "SELECT activo FROM foto_publicada WHERE id_grupo = ?";
        $result = $this->db->query($sql,array($id_grupo))->num_rows();
        return $result;
    }
    
    function getComentadas($id_grupo,$inicio){
        $sql = "SELECT
            F.id_foto,
            F.id_usuario,
            F.id_grupo,
            F.titulo,
            U.usuario,
			U.mime,
			(SELECT COUNT(*) FROM comentarios_foto CO WHERE CO.id_foto = F.id_foto) as total
        FROM
            foto_publicada F
            INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
        WHERE
            F.id_grupo = ?
        ORDER BY
            total DESC
        LIMIT
            16 OFFSET  $inicio;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
    function getRecientes($id_grupo,$inicio){
        $sql = "SELECT
                    F.id_foto,
                    F.id_usuario,
                    F.id_grupo,
                    F.titulo,
                    U.usuario,
					U.mime
                FROM
                    foto_publicada F
                    INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
                WHERE
                    F.id_grupo = ?
                ORDER BY
                    F.fecha_creacion DESC
                LIMIT 16 OFFSET  $inicio;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
    function getVisitadas($id_grupo,$inicio){
        $sql = "SELECT
                F.id_foto,
                F.id_usuario,
                F.id_grupo,
                F.titulo,
                U.usuario,
				U.mime
            FROM
                foto_publicada F
                INNER JOIN usuario U on (U.id_usuario = F.id_usuario)
            WHERE
                F.vistas > 0
            AND F.id_grupo = ?
            ORDER BY
                F.vistas DESC
            LIMIT
                16 OFFSET  $inicio;";
            $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
//    -------------------------SECCION VIDEOS-----------------------------------
    
    function totalVideos($id_grupo){
        $sql = "SELECT activo FROM video_publicado WHERE id_grupo = ?";
        $result = $this->db->query($sql,array($id_grupo))->num_rows();
        return $result;
    }
        
    function getComentados($id_grupo,$inicio){
        $sql = "SELECT
            V.id_video,
            V.id_usuario,
            V.id_grupo,
            V.titulo,
            V.direccion_web,
            U.usuario,
            U.mime,
            (SELECT COUNT(*) FROM comentarios_video CO WHERE CO.id_video = V.id_video) AS total
        FROM
            video_publicado V
            INNER JOIN usuario U ON (U.id_usuario = V.id_usuario)
        WHERE
            V.id_grupo = ?
        ORDER BY
            (SELECT COUNT(*) FROM comentarios_video CO WHERE CO.id_video = V.id_video) DESC
        LIMIT
            16 OFFSET  $inicio;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
    function getVideosRecientes($id_grupo,$inicio){
        $sql = "SELECT
                    V.id_video,
                    V.id_usuario,
                    V.id_grupo,
                    V.titulo,
                    V.direccion_web,
                    U.usuario,
					U.mime
                FROM
                    video_publicado V
                    INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
                WHERE
                    V.id_grupo = ?
                ORDER BY
                    V.fecha_creacion DESC
                LIMIT 16 OFFSET  $inicio;";
        $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
    function getVisitados($id_grupo,$inicio){
        $sql = "SELECT
                V.id_video,
                V.id_usuario,
                V.id_grupo,
                V.titulo,
                V.direccion_web,
                U.usuario,
				U.mime
            FROM
                video_publicado V
                INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
            WHERE
                V.vistas > 0
            AND V.id_grupo = ?
            ORDER BY
                V.vistas DESC
            LIMIT
                16 OFFSET  $inicio;";
            $result = $this->db->query($sql,array($id_grupo))->result();
        return $result;
    }
    
//    ----------------------------SECCION DEBATES-------------------------------
    
    function totalDebates($id_grupo){
        $sql = "SELECT activo FROM debates WHERE id_grupo = ?";
        $result = $this->db->query($sql,array($id_grupo))->num_rows();
        return $result;
    }

    function totalDebatesUltimoMensaje($id_grupo){
        $sql = "SELECT
                    CO.id_debate,
                    max(CO.fecha_creacion) AS fecha,
                    D.id_grupo
                FROM
                    comentarios CO
                INNER JOIN debates D ON (D.id_debate = CO.id_debate)
                WHERE
                    D.id_grupo = ?
                GROUP BY
                    id_debate";
        $result = $this->db->query($sql, array($id_grupo))->num_rows();
        return $result;
    }
    
    function getMensaje($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.id_usuario,
                    D.titulo_debate,
                    U.usuario,
		    U.mime,
		    (SELECT COUNT(*) FROM comentarios CO WHERE CO.id_debate = D.id_debate) AS total
                FROM
                    debates D
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    D.id_grupo = ?
                GROUP BY
                    D.id_debate
                ORDER BY
                    total DESC
				LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql,array($id_grupo))->result();
        $debates = $this->formatearArray($result);
        return $debates;
    }
    
    function getVisitasDebate($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.id_usuario,
                    D.titulo_debate,
                    U.usuario,
					U.mime
                FROM
                    debates D
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    D.id_grupo = ?
                GROUP BY
                    D.id_debate
                ORDER BY
                    D.vistas DESC
				LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql,array($id_grupo))->result();
        $debates = $this->formatearArray($result);
        return $debates;
    }
    
    function getUltimoMensaje($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    (SELECT MAX(CO.fecha_creacion) FROM comentarios CO WHERE D.id_debate = CO.id_debate) AS fecha,
                    D.id_grupo
                FROM
                    debates D
                WHERE
                    D.id_grupo = ?
                GROUP BY
                    id_debate
                ORDER BY
                    fecha DESC
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql, array($id_grupo))->result();
        $i = 0;
        $debates = "";
        foreach($result as $debate){
            $sql = "SELECT 
                        D.id_debate,
                        D.titulo_debate,
                        D.fecha_creacion,
                        D.debate,
                        D.id_usuario,
                        U.usuario,
						U.mime
                    FROM
                        debates D
                    INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                    WHERE
                        D.id_debate = ?";
			$fecha_comentario="";
            $debates[$i] = $this->db->query($sql,array($debate->id_debate))->row();
            $sql = "SELECT activo FROM comentarios WHERE id_debate = $debate->id_debate";
            $num_comentarios = $this->db->query($sql)->num_rows();
            $debates[$i]->num_comentarios = $num_comentarios;
			if(isset($debate->fecha))
            $fecha_comentario = relativeTimeFormat($debate->fecha,$mini=FALSE);
            if(strlen($fecha_comentario) <= 10 && strlen($fecha_comentario) > 4){
                $fecha_comentario = "Hace $fecha_comentario";
            }else if(strlen($fecha_comentario) > 10){
                $fecha_comentario = "El $fecha_comentario";
            }
            $fecha_creacion = relativeTimeFormat($debates[$i]->fecha_creacion,$mini=FALSE);
            if(strlen($fecha_creacion) <= 10 && strlen($fecha_creacion) > 4){
                $fecha_creacion = "Hace $fecha_creacion";
            }else if(strlen($fecha_creacion) > 10){
                $fecha_creacion = "El $fecha_creacion";
            }
            $debates[$i]->fecha_creacion = $fecha_creacion;
            $debates[$i]->fecha_comentario = $fecha_comentario;
            $sub_debate = substr($debates[$i]->debate, 0, 255);
            $sub_debate = $sub_debate . '...';
            $debates[$i]->debate = $sub_debate;
            $debates[$i]->url_usuario = base_url()."index.php/novios/comunidad/Perfil/".$debates[$i]->id_usuario;
            $i++;
        }
        return $debates;
    }
    
    function getDebatesRecientes($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.id_usuario,
                    D.titulo_debate,
                    U.usuario,
					U.mime
                FROM
                    debates D
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    D.id_grupo = ?
                GROUP BY
                    D.id_debate
                ORDER BY
                    D.fecha_creacion DESC";
        $result = $this->db->query($sql,array($id_grupo))->result();
        $debates = $this->formatearArray($result);
        return $debates;
    }
    
    function getDebateSinRespuesta($id_grupo,$inicio){
        $sql = "SELECT
                    D.id_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.id_usuario,
                    D.titulo_debate,
                    U.usuario,
					U.mime,
                    (SELECT COUNT(*) FROM comentarios CO WHERE CO.id_debate = D.id_debate) AS total
                FROM
                    debates D
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    D.id_grupo = ? AND
                    (SELECT COUNT(*) FROM comentarios CO WHERE CO.id_debate = D.id_debate) = 0
                GROUP BY
                    D.id_debate
                ORDER BY
                    D.fecha_creacion DESC
                   LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql,array($id_grupo))->result();
        $debates = $this->formatearArray($result);
        return $debates;
    }
    
    function getTotalComentarios($id_debate){
        $sql = "SELECT activo FROM comentarios WHERE id_debate = $id_debate";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
    
    function formatearFecha($fecha_creacion){
        $fecha_creacion2 = "";
        if(!empty($fecha_creacion)){
            $fecha_creacion2 = relativeTimeFormat($fecha_creacion,$mini=FALSE);
            if(strlen($fecha_creacion2) <= 10 && strlen($fecha_creacion2) > 4){
                $fecha_creacion2 = "Hace $fecha_creacion2";
            }else if(strlen($fecha_creacion2) > 10){
                $fecha_creacion2 = "El $fecha_creacion2";
            }
        }
        return $fecha_creacion2;
    }
    
    function formatearArray($result){
        $debates = "";
        $i = 0;
        foreach ($result as $debate){
            $sql = "SELECT
                        fecha_creacion
                    FROM
                        comentarios
                    WHERE
                        id_debate = ?
                    ORDER BY
                        fecha_creacion DESC
                    LIMIT 1";
            $comentario = $this->db->query($sql,array($debate->id_debate))->row();
            $num_comentarios = $this->getTotalComentarios($debate->id_debate);
            $debate->fecha_creacion = $this->formatearFecha($debate->fecha_creacion);
            if(!empty($comentario->fecha_creacion)){
                $debate->fecha_comentario = $this->formatearFecha($comentario->fecha_creacion);
            }else{
                $debate->fecha_comentario = "";
            }
            $sub_debate = substr($debate->debate, 0, 255);
            $sub_debate = $sub_debate . '...';
            $debate->debate = $sub_debate;
            $debate->url_usuario = base_url().'index.php/novios/comunidad/Home/foto_usuario'.$debate->id_usuario;
            $debate->num_comentarios = $num_comentarios;
            $debates[$i] = $debate;
            $i++;
        }
        return $debates;
    }
    
}
