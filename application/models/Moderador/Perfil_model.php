<?php

class Perfil_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function validarUsuario($id_usuario)
    {
        $sql = "SELECT activo FROM usuario WHERE id_usuario = $id_usuario AND activo = 1";
        $result = $this->db->query($sql)->row();
        return $result;
    }

    function getDatosUsuario($id_usuario)
    {
        $sql = "SELECT
                    U.usuario,
                    U.puntos,
                    U.fecha_creacion,
                    U.mime,
                    U.id_usuario,
                    C.poblacion,
                    C.estado,
                    C.sobre_mi,
                    B.fecha_boda,
                    B.sobre_boda,
                    B.color,
                    B.estacion,
                    B.estilo
                FROM
                    cliente C
                INNER JOIN usuario U ON (U.id_usuario = C.id_usuario)
                INNER JOIN boda B ON (B.id_boda = C.id_boda)
                WHERE
                    U.activo = 1
                AND U.id_usuario = $id_usuario";
        $result = $this->db->query($sql)->row();
        return $result;
    }

    function getAmistad($id_usuario)
    {
        $id_usuario_logeado = $this->session->userdata('id_usuario');
        $sql = "SELECT 
                    amistad, 
                    solicitud,
                    id_usuario_envio,
                    id_usuario_confirmacion
                FROM 
                    amistad_usuarios 
                WHERE 
                    id_usuario_envio = $id_usuario 
                AND id_usuario_confirmacion = $id_usuario_logeado
                OR id_usuario_envio = $id_usuario_logeado 
                AND id_usuario_confirmacion = $id_usuario";
        $result = $this->db->query($sql)->row();
        return $result;
    }

    function getDebatesPerfil($id_usuario)
    {
        $debates2 = "";
        $sql = "SELECT
                    D.id_debate,
                    D.titulo_debate,
                    D.fecha_creacion,
                    D.debate,
                    D.id_grupo,
                    D.comentarios,
                    D.vistas,
                    D.id_usuario as usuario_debate,
                    U.usuario,
                    U.mime,
                    G.nombre
                FROM
                    comentarios CO
                INNER JOIN debates D ON (D.id_debate = CO.id_debate)
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                INNER JOIN grupos_comunidad G ON (
                    G.id_grupos_comunidad = D.id_grupo
                )
                WHERE
                    D.activo = 1
                AND G.id_grupos_comunidad = D.id_grupo
                AND CO.id_usuario = $id_usuario
                GROUP BY
                    CO.id_debate
                ORDER BY
                    D.fecha_creacion DESC
                LIMIT 5";
        $debates = $this->db->query($sql)->result();
        $i = 0;
        foreach ($debates as $debate) {
            $debate->num_comentarios = $debate->comentarios;
            $fecha_debate = relativeTimeFormat($debate->fecha_creacion, $mini = FALSE);
            if (strlen($fecha_debate) <= 10 && strlen($fecha_debate) > 4) {
                $fecha_debate = "Hace $fecha_debate";
            } else if (strlen($fecha_debate) > 10) {
                $fecha_debate = "El $fecha_debate";
            }
            if (!empty($debate->mime)) {
                $debate->url_foto = base_url() . "index.php/novios/comunidad/Home/foto_usuario/" . $debate->usuario_debate;
            } else {
                $debate->url_foto = base_url() . "dist/img/blog/perfil.png";
            }
            $debate->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$id_usuario";
            $debate->fecha_creacion = $fecha_debate;
            $sub_debate = substr($debates[$i]->debate, 0, 255);
            $sub_debate = $sub_debate . '...';
            $debate->debate = $sub_debate;
            $debates2[$i] = $debate;
            $i++;
        }
        return $debates;
    }

    function getFotosPerfil($id_usuario)
    {
        $sql = "SELECT
                    F.id_foto,
                    F.id_usuario,
                    F.id_grupo,
                    F.titulo,
                    U.usuario,
                    U.mime
                FROM
                    foto_publicada F
                INNER JOIN usuario U ON (U.id_usuario = F.id_usuario)
                WHERE
                    F.activo = 1
                AND F.id_usuario = $id_usuario
                ORDER BY
                    F.fecha_creacion DESC
                LIMIT 7;";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getVideosPerfil($id_usuario)
    {
        $sql = "SELECT
                    V.id_video,
                    V.id_usuario,
                    V.id_grupo,
                    V.direccion_web,
                    V.descripcion,
                    V.titulo,
                    U.usuario,
                    U.mime
                FROM
                    video_publicado V
                    INNER JOIN usuario U on (U.id_usuario = V.id_usuario)
                WHERE
                    V.activo=1
                AND V.id_usuario = $id_usuario
                ORDER BY
                    V.fecha_creacion DESC
                LIMIT 7;";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getActividadPerfil($id_usuario)
    {
        $sql = "SELECT id_actividad, id_actividad_realizada, tipo_actividad FROM actividad_usuarios WHERE id_usuario = $id_usuario ORDER BY fecha_creacion DESC LIMIT 5";
        $actividades = $this->db->query($sql)->result();
        return $actividades;
    }

    function formatearDatosActividad($actividad)
    {
        $result = "";
        $actividades2 = "";
        $i = 0;
        $actividad->tipo_actividad = intval($actividad->tipo_actividad);
        if ($actividad) {
            switch ($actividad->tipo_actividad) {
                case 1:
                    $sql = "SELECT debate, titulo_debate, debate, fecha_creacion FROM debates WHERE id_debate = $actividad->id_actividad_realizada AND activo = 1";
                    $result = $this->db->query($sql)->row();
                    $result->puntos = 3;
                    $result->participacion = "Publique el debate";
                    $result->titulo = $result->titulo_debate;
                    $result->contenido = substr($result->debate, 0, 150);
                    $result->contenido = "$result->contenido ...";
                    $result->url_titulo = base_url() . "index.php/novios/comunidad/Home/debatePublicado/$actividad->id_actividad_realizada";
                    break;
                case 2:
                    $sql = "SELECT id_foto, id_grupo, fecha_creacion, mime, titulo FROM foto_publicada WHERE id_foto = $actividad->id_actividad_realizada AND activo = 1";
                    $result = $this->db->query($sql)->row();
                    $result->puntos = 3;
                    $result->participacion = "Publique la foto";
                    $result->url_titulo = base_url() . "index.php/novios/comunidad/picture/fotoPublicada/foto$result->id_grupo-g$actividad->id_actividad_realizada";
                    if (!empty($result->mime)) {
                        $result->url_imagen = base_url() . "index.php/novios/comunidad/picture/preview/$result->id_foto";
                    }
                    break;
                case 3:
                    $sql = "SELECT id_video, id_grupo, fecha_creacion, titulo, direccion_web FROM video_publicado WHERE id_video = $actividad->id_actividad_realizada AND activo = 1";
                    $result = $this->db->query($sql)->row();
                    $result->puntos = 3;
                    $result->participacion = "Publique el video";
                    $direccion_img = str_replace("https://www", "//img", $result->direccion_web);
                    $direccion_img = str_replace("embed", "vi", $direccion_img);
                    $direccion_img = $direccion_img . "/default.jpg";
                    $result->url_imagen = $direccion_img;
                    $result->url_titulo = base_url() . "index.php/novios/comunidad/video/videoPublicado/video$result->id_grupo-g$actividad->id_actividad_realizada";
                    break;
                case 4:
                    $sql = "SELECT 
                                CO.id_comentario, 
                                CO.fecha_creacion,
                                CO.mensaje,
                                D.titulo_debate,
                                D.id_debate
                            FROM 
                                comentarios CO 
                            INNER JOIN debates D ON (D.id_debate = CO.id_debate)
                            WHERE 
                                CO.id_comentario = $actividad->id_actividad_realizada 
                            AND CO.activo = 1";
                    $result = $this->db->query($sql)->row();
                    $result->puntos = 1;
                    $result->participacion = "Comente el debate";
                    $result->contenido = substr($result->mensaje, 0, 200);
                    $result->contenido = "$result->contenido ...";
                    $result->titulo = $result->titulo_debate;
                    $result->url_titulo = base_url() . "index.php/novios/comunidad/Home/debatePublicado/$result->id_debate";
                    break;
                case 5:
                    $sql = "SELECT 
                                CO.id_comentario, 
                                CO.fecha_creacion, 
                                F.titulo, 
                                CO.comentario,
                                F.id_grupo,
                                F.id_foto
                            FROM 
                                comentarios_foto CO
                            INNER JOIN foto_publicada  F ON (F.id_foto = CO.id_foto)
                            WHERE 
                                CO.id_comentario = $actividad->id_actividad_realizada 
                            AND CO.activo = 1";
                    $result = $this->db->query($sql)->row();
                    $result->puntos = 1;
                    $result->participacion = "Comente la foto";
                    $result->contenido = substr($result->comentario, 0, 150);
                    $result->contenido = "$result->contenido ...";
                    $result->url_titulo = base_url() . "index.php/novios/comunidad/picture/fotoPublicada/foto$result->id_grupo-g$result->id_foto";
                    break;
                case 6:
                    $sql = "SELECT 
                                CO.id_comentario, 
                                CO.fecha_creacion, 
                                V.titulo, 
                                CO.comentario,
                                V.id_grupo,
                                V.id_video
                            FROM 
                                comentarios_video CO
                            INNER JOIN video_publicado  V ON (V.id_video = CO.id_video)
                            WHERE 
                                CO.id_comentario = $actividad->id_actividad_realizada 
                            AND CO.activo = 1";
                    $result = $this->db->query($sql)->row();
                    $result->puntos = 1;
                    $result->participacion = "Comente el video";
                    $result->contenido = substr($result->comentario, 0, 150);
                    $result->contenido = "$result->contenido ...";
                    $result->url_titulo = base_url() . "index.php/novios/comunidad/video/videoPublicado/video$result->id_grupo-g$result->id_video";
                    break;
                case 7:
                    $sql = "SELECT 
                                F.id_foto, 
                                F.fecha_creacion,
                                F.mime,
                                F.titulo,
                                F.id_grupo,
                                F.id_usuario,
                                U.usuario
                            FROM 
                                me_gusta_foto M
                            INNER JOIN foto_publicada F ON (F.id_foto = M.id_foto)
                            INNER JOIN usuario U ON (U.id_usuario = F.id_usuario)
                            WHERE 
                                F.id_foto = $actividad->id_actividad_realizada 
                            AND F.activo = 1";
                    $result = $this->db->query($sql)->row();
                    $result->puntos = 1;
                    $result->participacion = "Me gusta la foto";
                    $result->url_titulo = base_url() . "index.php/novios/comunidad/picture/fotoPublicada/foto$result->id_grupo-g$result->id_foto";
                    if (!empty($result->mime)) {
                        $result->url_imagen = base_url() . "index.php/novios/comunidad/picture/preview/$result->id_foto";
                    }
                    break;
                case 8:
                    $sql = "SELECT 
                                V.id_video, 
                                V.fecha_creacion,
                                V.direccion_web, 
                                V.titulo,
                                V.id_grupo,
                                V.id_usuario,
                                U.usuario
                            FROM 
                                me_gusta_video M
                            INNER JOIN video_publicado V ON (V.id_video = M.id_video)
                            INNER JOIN usuario U ON (U.id_usuario = V.id_usuario)
                            WHERE 
                                V.id_video = $actividad->id_actividad_realizada 
                            AND V.activo = 1";
                    $result = $this->db->query($sql)->row();
                    $result->puntos = 1;
                    $result->participacion = "Me gusta el video";
                    $direccion_img = str_replace("https://www", "//img", $result->direccion_web);
                    $direccion_img = str_replace("embed", "vi", $direccion_img);
                    $direccion_img = $direccion_img . "/default.jpg";
                    $result->url_imagen = $direccion_img;
                    $result->url_titulo = base_url() . "index.php/novios/comunidad/video/videoPublicado/video$result->id_grupo-g$result->id_video";
                    break;
            }
        }
        return $result;
    }

    function getAmigosPerfil($id_usuario)
    {
        $amigos1 = "";
        $amigos2 = "";
        $sql = "SELECT
                    A.id_usuario_confirmacion,
                    A.fecha_creacion,
                    U.usuario,
                    C.poblacion,
                    C.estado
                FROM
                    amistad_usuarios A
                INNER JOIN usuario U ON (U.id_usuario = A.id_usuario_confirmacion)
                INNER JOIN cliente C ON (C.id_usuario = U.id_usuario)
                WHERE
                    A.amistad = 1
                AND A.id_usuario_envio = $id_usuario
                ORDER BY
                    A.fecha_creacion DESC
                LIMIT 4;";
        $amigos1 = $this->db->query($sql)->result();
        if (count($amigos1) < 4) {
            $sql = "SELECT
                        A.id_usuario_envio,
                        A.fecha_creacion,
                        U.usuario,
                        U.mime,
                        C.poblacion,
                        C.estado
                    FROM
                        amistad_usuarios A
                    INNER JOIN usuario U ON (U.id_usuario = A.id_usuario_envio)
                    INNER JOIN cliente C ON (C.id_usuario = U.id_usuario)
                    WHERE
                        A.amistad = 1
                    AND A.id_usuario_confirmacion = $id_usuario
                    ORDER BY
                        A.fecha_creacion DESC
                    LIMIT 4;";
            $amigos2 = $this->db->query($sql)->result();
        }
        return $this->formatearTarjetaAmigoPerfil($amigos1, $amigos2);
    }

    function getTotalAmigos($id_usuario)
    {
        $sql = "SELECT
                    id_usuario_confirmacion
                FROM
                    amistad_usuarios 
                WHERE
                    amistad = 1
                AND id_usuario_envio = $id_usuario
                OR id_usuario_confirmacion = $id_usuario
                AND amistad = 1";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getTotalMensajes($id_usuario)
    {
        $sql = "SELECT activo FROM comentarios WHERE activo = 1 AND id_usuario = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getTotalDebates($id_usuario)
    {
        $sql = "SELECT activo FROM debates WHERE activo = 1 AND id_usuario = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getTotalFotos($id_usuario)
    {
        $sql = "SELECT activo FROM foto_publicada WHERE activo = 1 AND id_usuario = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getComentariosPerfil($id_usuario)
    {
        $sql = "SELECT 
                    CM.id_usuario_comentario,
                    CM.fecha_creacion,
                    CM.comentario,
                    U.usuario,
                    U.mime
                FROM 
                    comentarios_muro CM
                INNER JOIN usuario U ON (U.id_usuario = CM.id_usuario_comentario)
                WHERE 
                    CM.id_muro = $id_usuario 
                ORDER BY 
                    CM.fecha_creacion DESC
                LIMIT 5";
        $result = $this->db->query($sql)->result();
        $comentarios = "";
        $i = 0;
        foreach ($result as $comentario) {
            $comentario->fecha_creacion = new DateTime($comentario->fecha_creacion);
            $comentario->fecha_creacion = $comentario->fecha_creacion->format("d/m/Y");
            $comentario->fecha_creacion = "El $comentario->fecha_creacion";
            $comentario->foto_usuario = "";
            if (!empty($comentario->mime)) {
                $comentario->foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$comentario->id_usuario_comentario";
            } else {
                $comentario->foto_usuario = base_url() . 'dist/img/blog/perfil.png';
            }
            $comentario->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$comentario->id_usuario_comentario";
            $comentarios[$i] = $comentario;
            $i++;
        }
        return $comentarios;
    }

    function formatearTarjetaAmigoPerfil($amigos1, $amigos2)
    {
        $amigos = "";
        $i = 0;
        foreach ($amigos1 as $amigo) {
            if ($i == 4) {
                break;
            }
            $amigo->mensajes = $this->perfil->getTotalMensajes($amigo->id_usuario_confirmacion);
            $amigo->debates = $this->perfil->getTotalDebates($amigo->id_usuario_confirmacion);
            $amigo->fotos = $this->perfil->getTotalFotos($amigo->id_usuario_confirmacion);
            $amigo->amigos = $this->perfil->getTotalAmigos($amigo->id_usuario_confirmacion);
            $amigo->url_debates_participacion = "participacion-$amigo->id_usuario_confirmacion";
            $amigo->url_debates = "misdebates-$amigo->id_usuario_confirmacion";
            $amigo->url_fotos = "fotos-$amigo->id_usuario_confirmacion";
            $amigo->url_amigos = "amigos-$amigo->id_usuario_confirmacion";
            if (!empty($amigo->mime)) {
                $amigo->url_foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$amigo->id_usuario_confirmacion";
            } else {
                $amigo->url_foto_usuario = base_url() . 'dist/img/blog/perfil.png';
            }
            $amigo->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$amigo->id_usuario_confirmacion";
            $amigo->id_usuario = $amigo->id_usuario_confirmacion;
            $amigo->url_agregar_amigo = "agregar-$amigo->id_usuario_confirmacion";
            $amigo->agregar = $this->ObtenerAmistad($amigo->id_usuario_confirmacion);
            $amigo->lugar = "";
            if (!empty($amigo->poblacion) && !empty($amigo->estado)) {
                $amigo->lugar = "$amigo->poblacion, $amigo->estado";
            } else if (!empty($amigo->poblacion)) {
                $amigo->lugar = $amigo->poblcacion;
            } else if (!empty($amigo->estado)) {
                $amigo->lugar = $amigo->estado;
            }
            $amigo->fecha_creacion = new DateTime($amigo->fecha_creacion);
            $mes = $this->formatearMes($amigo->fecha_creacion);
            $anio = $amigo->fecha_creacion->format('Y');
            $fecha_amistad = "Desde $mes de $anio";
            $amigo->fecha_creacion = $fecha_amistad;
            $amigos[$i] = $amigo;
            $i++;
        }
        foreach ($amigos2 as $amigo) {
            if ($i == 4) {
                break;
            }
            $amigo->mensajes = $this->perfil->getTotalMensajes($amigo->id_usuario_envio);
            $amigo->debates = $this->perfil->getTotalDebates($amigo->id_usuario_envio);
            $amigo->fotos = $this->perfil->getTotalFotos($amigo->id_usuario_envio);
            $amigo->amigos = $this->perfil->getTotalAmigos($amigo->id_usuario_envio);
            $amigo->url_debates_participacion = "participacion-$amigo->id_usuario_envio";
            $amigo->url_debates = "misdebates-$amigo->id_usuario_envio";
            $amigo->url_fotos = "fotos-$amigo->id_usuario_envio";
            $amigo->url_amigos = "amigos-$amigo->id_usuario_envio";
            if (!empty($amigo->mime)) {
                $amigo->url_foto_usuario = base_url() . "index.php/novios/comunidad/Home/foto_usuario/$amigo->id_usuario_envio";
            } else {
                $amigo->url_foto_usuario = base_url() . 'dist/img/blog/perfil.png';
            }
            $amigo->url_usuario = base_url() . "index.php/novios/comunidad/perfil/usuario/$amigo->id_usuario_envio";
            $amigo->id_usuario = $amigo->id_usuario_envio;
            $amigo->url_agregar_amigo = "agregar-$amigo->id_usuario_envio";
            $amigo->agregar = $this->ObtenerAmistad($amigo->id_usuario_envio);
            $amigo->lugar = "";
            if (!empty($amigo->poblacion) && !empty($amigo->estado)) {
                $amigo->lugar = "$amigo->poblacion, $amigo->estado";
            } else if (!empty($amigo->poblacion)) {
                $amigo->lugar = $amigo->poblcacion;
            } else if (!empty($amigo->estado)) {
                $amigo->lugar = $amigo->estado;
            }
            $amigo->fecha_creacion = new DateTime($amigo->fecha_creacion);
            $mes = $this->formatearMes($amigo->fecha_creacion);
            $anio = $amigo->fecha_creacion->format('Y');
            $fecha_amistad = "Desde $mes de $anio";
            $amigo->fecha_creacion = $fecha_amistad;
            $amigos[$i] = $amigo;
            $i++;
        }
        return $amigos;
    }

    public function ObtenerAmistad($id_usuario)
    {
        $amistad = $this->perfil->getAmistad($id_usuario);
        if (!empty($amistad) && $amistad->amistad == 1) {
            $amistad = "Eliminar Amistad";
        } else if (!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $id_usuario) {
            $amistad = "Aceptar Solicitud";
        } else if (!empty($amistad) && $amistad->solicitud == 1 && $amistad->id_usuario_envio == $this->session->userdata('id_usuario')) {
            $amistad = "Solicitud Enviada";
        } else {
            $amistad = "Enviar Solicitud";
        }
        return $amistad;
    }

    function getTotalComentariosMuro($id_usuario)
    {
        $sql = "SELECT id_muro FROM comentarios_muro WHERE id_muro = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getComentariosMuro($id_usuario, $inicio)
    {
        $sql = "SELECT
                    U.mime,
                    U.usuario,
                    U.id_usuario,
                    CM.comentario,
                    CM.fecha_creacion
                FROM
                    comentarios_muro CM
                INNER JOIN usuario U ON (U.id_usuario = CM.id_usuario_comentario)
                WHERE 
                    CM.id_muro = $id_usuario
                ORDER BY 
                    CM.fecha_creacion DESC
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function setComentarioMuro($id_usuario, $id_muro, $comentario)
    {
        $fecha_creacion = date("Y-m-d H:i:s");
        $validar_comentario = $this->db->query("SELECT id_usuario_comentario FROM comentarios_muro WHERE id_muro = $id_muro")->row();
        if (empty($validar_comentario)) {
            $data = array(
                'id_muro' => $id_muro,
                'id_usuario_comentario' => $id_usuario,
                'comentario' => $comentario,
                'fecha_creacion' => $fecha_creacion,
                'primer_comentario' => 1
            );
        } else {
            $data = array(
                'id_muro' => $id_muro,
                'id_usuario_comentario' => $id_usuario,
                'comentario' => $comentario,
                'fecha_creacion' => $fecha_creacion,
                'primer_comentario' => 0
            );
        }
        $result = $this->db->insert('comentarios_muro', $data);
        $id_comentario = $this->db->insert_id();
        $sql = "SELECT id_usuario_comentario FROM comentarios_muro WHERE id_muro = $id_muro AND id_usuario_comentario = $id_usuario AND primer_comentario = 1";
        $comentarios_muro = $this->db->query($sql)->num_rows();
        switch ($comentarios_muro) {
            case 1:
                $this->set_medallas($id_usuario, 10);
                break;
            case 20:
                $this->set_medallas($id_usuario, 11);
                break;
            case 50:
                $this->set_medallas($id_usuario, 12);
                break;
        }
        if ($result) {
            $sql = "SELECT
                        U.mime,
                        U.usuario,
                        U.id_usuario,
                        CM.comentario,
                        CM.fecha_creacion
                    FROM
                        comentarios_muro CM
                    INNER JOIN usuario U ON (U.id_usuario = CM.id_usuario_comentario)
                    WHERE 
                        CM.id_comentario = $id_comentario";
            $result = $this->db->query($sql)->row();
        }
        return $result;
    }

    function getFotos($id_usuario, $inicio)
    {
        $sql = "SELECT
                    F.id_foto,
                    F.titulo,
                    F.id_grupo,
                    U.id_usuario,
                    U.usuario,
                    U.mime
                FROM
                    foto_publicada F
                INNER JOIN usuario U ON (U.id_usuario = F.id_usuario)
                WHERE 
                    F.id_usuario = $id_usuario
                ORDER BY
                    F.fecha_creacion DESC
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getTotalVideos($id_usuario)
    {
        $sql = "SELECT activo FROM video_publicado WHERE id_usuario = $id_usuario AND activo = 1";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getVideos($id_usuario, $inicio)
    {
        $sql = "SELECT
                    V.id_video,
                    V.titulo,
                    V.id_grupo,
                    V.direccion_web,
                    U.id_usuario,
                    U.usuario,
                    U.mime
                FROM
                    video_publicado V
                INNER JOIN usuario U ON (U.id_usuario = V.id_usuario)
                WHERE 
                    V.id_usuario = $id_usuario
                ORDER BY
                    V.fecha_creacion DESC
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getTotalActividades($id_usuario)
    {
        $sql = "SELECT id_actividad FROM actividad_usuarios WHERE id_usuario = $id_usuario";
        $actividades = $this->db->query($sql)->num_rows();
        return $actividades;
    }

    function getActividades($id_usuario, $inicio)
    {
        $sql = "SELECT 
                    id_actividad, 
                    id_actividad_realizada, 
                    tipo_actividad 
                FROM 
                    actividad_usuarios 
                WHERE 
                    id_usuario = $id_usuario 
                ORDER BY 
                    fecha_creacion DESC 
                LIMIT 16 OFFSET $inicio";
        $actividades = $this->db->query($sql)->result();
        return $actividades;
    }

    function getTotalDebatesParticipacion($id_usuario)
    {
        $sql = "SELECT D.activo FROM debates D INNER JOIN comentarios CO USING(id_debate) WHERE D.activo = 1 AND CO.id_usuario = $id_usuario GROUP BY CO.id_debate";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getDebatesParticipacion($id_usuario, $inicio)
    {
        $sql = "SELECT
                    D.id_debate,
                    D.titulo_debate,
                    D.debate,
                    D.fecha_creacion,
                    D.comentarios,
                    D.vistas,
                    U.id_usuario,
                    U.usuario,
                    U.mime,
                    max(CO.fecha_creacion) as fecha
                FROM 
                    comentarios CO
                INNER JOIN debates D USING (id_debate)
                INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
                WHERE
                    CO.id_usuario = $id_usuario
                AND D.activo = 1
                GROUP BY
                    CO.id_debate
                ORDER BY
                    fecha DESC
                LIMIT 16  OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getTotalMisDebates($id_usuario)
    {
        $sql = "SELECT activo FROM debates WHERE id_usuario = $id_usuario AND activo = 1";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getMisDebates($id_usuario, $inicio)
    {
        $sql = "SELECT
            D.id_debate,
            D.titulo_debate,
            D.debate,
            D.comentarios,
            D.vistas,
            D.fecha_creacion,
            U.id_usuario,
            U.usuario,
            U.mime
        FROM 
            debates D
        INNER JOIN usuario U ON (U.id_usuario = D.id_usuario)
        WHERE
            D.id_usuario = $id_usuario
        AND D.activo = 1
        ORDER BY
            D.fecha_creacion DESC
        LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getAmigos($id_usuario, $inicio)
    {
        $amigos1 = "";
        $amigos2 = "";
        $sql = "SELECT
                    A.id_usuario_confirmacion,
                    A.fecha_creacion,
                    A.amistad,
                    A.solicitud,
                    U.usuario,
                    U.mime,
                    C.poblacion,
                    C.estado
                FROM
                    amistad_usuarios A
                INNER JOIN usuario U ON (U.id_usuario = A.id_usuario_confirmacion)
                INNER JOIN cliente C ON (C.id_usuario = U.id_usuario)
                WHERE
                    A.amistad = 1
                AND A.id_usuario_envio = $id_usuario
                ORDER BY
                    A.fecha_creacion DESC
                LIMIT 4;";
        $amigos1 = $this->db->query($sql)->result();
        if (count($amigos1) < 4) {
            $sql = "SELECT
                        A.amistad,
                        A.solicitud,
                        A.id_usuario_envio,
                        A.fecha_creacion,
                        U.usuario,
                        U.mime,
                        C.poblacion,
                        C.estado
                    FROM
                        amistad_usuarios A
                    INNER JOIN usuario U ON (U.id_usuario = A.id_usuario_envio)
                    INNER JOIN cliente C ON (C.id_usuario = U.id_usuario)
                    WHERE
                        A.amistad = 1
                    AND A.id_usuario_confirmacion = $id_usuario
                    ORDER BY
                        A.fecha_creacion DESC
                    LIMIT 4;";
            $amigos2 = $this->db->query($sql)->result();
        }
        return $this->formatearTarjetaAmigoPerfil($amigos1, $amigos2);
    }

    function getMedallas($id_usuario)
    {
        $sql = "SELECT
                    MU.id_nueva_medalla,
                    M.descripcion,
                    M.nombre,
                    M.url_imagen
                FROM 
                    medallas M
                INNER JOIN medallas_usuarios MU ON (MU.id_nueva_medalla = M.id_medalla)
                WHERE
                    MU.id_usuario = $id_usuario
                ORDER BY
                    fecha_creacion DESC";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getMedallasNoObtenidas($id_usuario, $consulta)
    {
        $sql = "SELECT
                    M.descripcion,
                    M.nombre,
                    M.url_imagen
                FROM 
                    medallas M
                INNER JOIN medallas_usuarios MU ON (MU.id_nueva_medalla != M.id_medalla)
                WHERE
                    MU.id_usuario = $id_usuario $consulta group by M.id_medalla";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getTodasMedallas()
    {
        $sql = "SELECT * FROM medallas";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getProveedores($id_usuario)
    {
        $sql = "SELECT 
                    P.id_proveedor,
                    P.nombre,
                    P.localizacion_estado,
                    P.localizacion_poblacion,
                    TP.tag_categoria
                FROM 
                    proveedor_boda PB
                INNER JOIN proveedor P USING (id_proveedor)
                INNER JOIN cliente C ON (C.id_boda = PB.id_boda)
                INNER JOIN tipo_proveedor TP ON (TP.id_tipo_proveedor = P.id_tipo_proveedor)
                WHERE 
                C.id_usuario = $id_usuario";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function validarVisita($id_usuario)
    {
        $id_usuario_visita = $this->session->userdata('id_usuario');
        if ($id_usuario != $id_usuario_visita) {
            $sql = "SELECT id_usuario_visita FROM visitas_perfil WHERE id_usuario_visita = $id_usuario_visita AND id_usuario_perfil = $id_usuario";
            $result = $this->db->query($sql)->row();
            if (empty($result)) {
                $fecha_creacion = date("Y-m-d H:i:s");
                $data = array(
                    'id_usuario_visita' => $id_usuario_visita,
                    'id_usuario_perfil' => $id_usuario,
                    'fecha_creacion' => $fecha_creacion
                );
                $visita = $this->db->insert("visitas_perfil", $data);
            }

        }
    }

    function getTotalVisitas($id_usuario)
    {
        $sql = "SELECT 
                    id_usuario_visita
                FROM 
                    visitas_perfil
                WHERE 
                    id_usuario_perfil = $id_usuario";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getVisitas($id_usuario, $inicio)
    {
        $sql = "SELECT 
                    VP.id_usuario_visita,
                    U.usuario,
                    U.mime,
                    U.fecha_creacion,
                    C.poblacion,
                    C.estado
                FROM 
                    visitas_perfil VP
                INNER JOIN usuario U ON (U.id_usuario = VP.id_usuario_visita)
                INNER JOIN cliente C ON (C.id_usuario = VP.id_usuario_visita)
                WHERE 
                    VP.id_usuario_perfil = $id_usuario
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function getAmistadVisitas($id_usuario, $id_usuario_visita)
    {
        $sql = "SELECT
                    amistad,
                    solicitud,
                    id_usuario_envio,
                    id_usuario_confirmacion
                FROM
                    amistad_usuarios
                WHERE
                    id_usuario_envio = $id_usuario
                AND id_usuario_confirmacion = $id_usuario_visita
                OR id_usuario_envio = $id_usuario_visita
                AND id_usuario_confirmacion = $id_usuario";
        $amistad = $this->db->query($sql)->row();
        return $amistad;
    }

    function getTotalFavoritos($id_usuario)
    {
        $sql = "SELECT
                    id_actividad
                FROM
                    actividad_usuarios
                WHERE
                    id_usuario = 36
                AND tipo_actividad = 7 
                OR id_usuario = 36
                AND tipo_actividad = 8";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    function getFavoritos($id_usuario, $inicio)
    {
        $sql = "SELECT
                    id_actividad,
                    tipo_actividad,
                    id_usuario,
                    id_actividad_realizada
                FROM
                    actividad_usuarios
                WHERE
                    id_usuario = $id_usuario
                AND tipo_actividad = 7 
                OR id_usuario = $id_usuario
                AND tipo_actividad = 8
                ORDER BY 
                    fecha_creacion DESC
                LIMIT 16 OFFSET $inicio";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    function validarAmistad($id_usuario_confirmacion)
    {
        $id_usuario = $this->session->userdata('id_usuario');
        $sql = "SELECT 
                    amistad, 
                    solicitud 
                FROM 
                    amistad_usuarios 
                WHERE 
                    id_usuario_confirmacion = $id_usuario_confirmacion 
                AND id_usuario_envio = $id_usuario
                AND solicitud = 0
                OR id_usuario_confirmacion = $id_usuario 
                AND id_usuario_envio = $id_usuario_confirmacion
                AND solicitud = 0";
        $result = $this->db->query($sql)->row();
        if (!empty($result)) {
            $result = $this->updateAmistad($id_usuario_confirmacion, 1, 0);
        } else {
            $fecha_actual = date("Y-m-d h:i:s");
            $data = array(
                'id_usuario_envio' => $id_usuario,
                'id_usuario_confirmacion' => $id_usuario_confirmacion,
                'amistad' => 0,
                'solicitud' => 1,
                'fecha_creacion' => $fecha_actual,
                'fecha_actualizacion' => $fecha_actual
            );
            $result = $this->db->insert("amistad_usuarios", $data);
        }
        return $result;
    }

    function updateAmistad($id_usuario_confirmacion, $solicitud, $amistad)
    {
        $id_usuario = $this->session->userdata('id_usuario');
        $fecha_actual = date("Y-m-d h:i:s");
        $data = array(
            'id_usuario_envio' => $id_usuario,
            'id_usuario_confirmacion' => $id_usuario_confirmacion,
            'amistad' => $amistad,
            'solicitud' => $solicitud,
            'fecha_actualizacion' => $fecha_actual
        );
        $this->db->set($data);
        $this->db->where("id_usuario_confirmacion", $id_usuario_confirmacion);
        $this->db->where("id_usuario_envio", $id_usuario);
        $this->db->or_where("id_usuario_confirmacion", $id_usuario);
        $this->db->where("id_usuario_envio", $id_usuario_confirmacion);
        $result = $this->db->update("amistad_usuarios");
        return $result;
    }

    function setComentario($id_muro, $comentario)
    {
        $id_usuario = $this->session->userdata('id_usuario');
        $fecha_actual = date("Y-m-d h:i:s");
        $primer_comentario = $this->db->query("SELECT id_usuario_comentario FROM comentarios_muro WHERE id_muro = $id_muro")->row();
        if (empty($primer_comentario)) {
            $data = array(
                'id_muro' => $id_muro,
                'id_usuario_comentario' => $id_usuario,
                'comentario' => $comentario,
                'primer_comentario' => 1,
                'fecha_creacion' => $fecha_actual
            );
            $num_comentarios = $this->db->query("SELECT id_usuario_comentario FROM comentarios_muro WHERE id_usuario_comentario = $id_usuario AND primer_comentario = 1")->num_rows();
            switch ($num_comentarios) {
                case 1:
                    $this->set_medallas($id_usuario, 10);
                    break;
                case 20:
                    $this->set_medallas($id_usuario, 11);
                    break;
                case 50:
                    $this->set_medallas($id_usuario, 12);
                    break;
            }
        } else {
            $data = array(
                'id_muro' => $id_muro,
                'id_usuario_comentario' => $id_usuario,
                'comentario' => $comentario,
                'primer_comentario' => 0,
                'fecha_creacion' => $fecha_actual
            );
        }
        $result = $this->db->insert("comentarios_muro", $data);
        return $result;
    }

    public function formatearMes($fecha)
    {
        $mes = $fecha->format("F");
        switch ($mes) {
            case "January":
                $mes = "Enero";
                break;
            case "February":
                $mes = "Febrero";
                break;
            case "March":
                $mes = "Marzo";
                break;
            case "April":
                $mes = "Abril";
                break;
            case "May":
                $mes = "Mayo";
                break;
            case "June":
                $mes = "Junio";
                break;
            case "July":
                $mes = "Julio";
                break;
            case "August":
                $mes = "Agosto";
                break;
            case "September":
                $mes = "Septiembre";
                break;
            case "October":
                $mes = "Octubre";
                break;
            case "November":
                $mes = "Noviembre";
                break;
            case "December":
                $mes = "Diciembre";
                break;
        }
        return $mes;
    }

    function set_medallas($id_usuario, $id_medalla)
    {
        $sql = "SELECT id_medalla FROM medallas_usuarios WHERE id_usuario = $id_usuario AND id_nueva_medalla = $id_medalla";
        $validar = $this->db->query($sql)->row();
        if (empty($validar)) {
            $datos = array(
                'id_usuario' => $id_usuario,
                'id_nueva_medalla' => $id_medalla
            );
            $this->db->insert("medallas_usuarios", $datos);
        }
    }
}

