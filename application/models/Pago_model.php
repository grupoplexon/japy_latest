<?php

class Pago_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_pago";
        $this->tabla = "pago";
    }

    public function getAllBoda($id_boda)
    {
        $sql = "SELECT pago.*,presupuesto.nombre as presupuesto from pago inner join presupuesto using(id_presupuesto) where id_boda=$id_boda";
        return $this->db->query($sql)->result();
    }

}
