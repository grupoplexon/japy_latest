<?php

class Perfil_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getCliente()
    {
        $id_user = $this->session->userdata('id_usuario');
        $sql     = "select C.id_usuario, C.id_boda, C.id_cliente, C.genero , C.telefono, C.sobre_mi, U.usuario, U.nombre, U.apellido,U.mime, U.foto,".
            "B.fecha_boda, B.color, B.estacion, B.estilo, B.no_invitado, B.sobre_boda, B.pais_boda, B.estado_boda, B.ciudad_boda  from cliente C, usuario U, boda B where C.id_usuario=".$id_user.
            " and U.id_usuario=".$id_user." and B.id_boda=C.id_boda and U.activo=1";
        $result  = $this->db->query($sql)->row();

        return $result;
    }

    function insert($data)
    {
        $this->db->trans_start();
        $this->db->insert('permisos_usuarios', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();

        return $id;
    }

    function update($datos)
    {
        $id_user    = $this->session->userdata('id_usuario');
        $mime       = "";
        $foto       = "";
        $decodifica = "";

        //if (isset($datos['fecha_boda']) && $datos['fecha_boda'] != "") {
        //    $fecha = DateTime::createFromFormat('j/m/Y', $datos['fecha_boda']);
        //    $fecha_boda = $fecha->format('Y-m-d');
        // }

        if (isset($datos['foto']) && $datos['foto'] != "") {
            $foto       = explode(",", $datos['foto']); //data:image/jpg;base64, esto es el mime_type
            $codigo_img = $foto[1];
            $foto       = explode(":", $foto[0]);
            $foto       = explode(";", $foto[1]);
            $mime       = $foto[0];
            $decodifica = base64_decode($codigo_img);
        }

        //datos usaurio
        $data = [
            'U.usuario'  => $datos['usuario'],
            'U.nombre'   => $datos['nombre'],
            'U.apellido' => $datos['apellido'],
            'U.mime'     => $mime,
            'U.foto'     => $decodifica,
        ];

        //actualizacion usuario
        $this->db->set($data);
        $this->db->where('U.id_usuario', $id_user);
        $this->db->update('usuario as U');

        //guest invite update
        $data = [
            'I.apellido' => $datos['apellido'],
            'I.nombre'   => $datos['nombre'],
            'I.sexo'     => $datos['genero'],
        ];

        $sql         = "SELECT I.id_invitado FROM invitado I inner join cliente C on I.id_boda = C.id_boda where id_usuario = $id_user";
        $id_invitado = $this->db->query($sql)->row();

        $this->db->set($data);
        $this->db->where('I.id_invitado', $id_invitado->id_invitado);
        $this->db->update('invitado as I');

        //datos cliente
        $data = [
            'C.telefono' => $datos['telefono'],
            'C.sobre_mi' => $datos['sobre_mi'],
            'C.genero'   => $datos['genero'],
        ];

        //actualizacion cliente
        $this->db->set($data);
        $this->db->where('C.id_usuario', $id_user);
        $this->db->update('cliente as C');

        //recuperar id boda para actualizacion
        $result = $this->db->query("SELECT B.id_boda FROM cliente C, boda B WHERE C.id_usuario=".$id_user." AND B.id_boda=C.id_boda")->row();

        //datos boda
        if (empty($datos['no_invitado'])) {
            $datos['no_invitado'] = 0;
        }

        $data = [
            'no_invitado' => $datos['no_invitado'],
            'sobre_boda'  => $datos['sobre_boda'],
            'pais_boda'   => $datos['pais_boda'],
            'estado_boda' => $datos['estado_boda'],
            'ciudad_boda' => $datos['ciudad_boda'],
            'color'       => $datos['color'],
            'estacion'    => $datos['estacion'],
            'estilo'      => $datos['estilo'],
        ];

        if (isset($datos['fecha_boda']) && ! empty($datos['fecha_boda'])) {
            $data['fecha_boda'] = $datos['fecha_boda'];
        }

        //actualizacion boda
        $this->db->set($data);
        $this->db->where('id_boda', $result->id_boda);
        $this->db->update('boda');
    }

    public function datosConfiguracion()
    {
        $sql    = "SELECT P.visibilidad_todos, C.id_facebook, U.correo FROM cliente C, permisos_usuarios P, usuario U WHERE C.id_usuario=".$this->session->userdata('id_usuario')." AND P.id_permisos=C.id_permisos AND U.id_usuario=".$this->session->userdata('id_usuario');
        $result = $this->db->query($sql)->row();

        return $result;
    }

    public function datosNotificaciones()
    {
        $sql      = "SELECT C.id_permisos FROM permisos_usuarios P, cliente C WHERE C.id_usuario=".$this->session->userdata("id_usuario");
        $result   = $this->db->query($sql)->row();
        $permisos = "";
        if ( ! empty($result->id_permisos)) {
            $sql      = "SELECT * FROM permisos_usuarios WHERE id_permisos=".$result->id_permisos;
            $permisos = $this->db->query($sql)->row();
        }

        return $permisos;
    }

    public function vincularFacebook($user_facebook)
    {
        $this->db->set("id_facebook", $user_facebook);
        $this->db->where("id_usuario", $this->session->userdata('id_usuario'));
        $this->db->update("cliente");
    }

    public function validarPassword()
    {
        $sql    = "SELECT U.contrasena FROM usuario U WHERE U.id_usuario=".$this->session->userdata('id_usuario');
        $result = $this->db->query($sql)->row();

        return $result;
    }

    public function desvincularFacebook()
    {
        $this->db->set("id_facebook", "");
        $this->db->where("id_usuario", $this->session->userdata('id_usuario'));
        $this->db->update("cliente");
    }

    public function updateEmail($datos)
    {
        $this->db->set("correo", $datos['correo']);
        $this->db->where("id_usuario", $this->session->userdata('id_usuario'));
        $this->db->where("activo", '1');
        $this->db->update("usuario");
    }

    public function updatePassword($password)
    {
        $this->db->set("contrasena", $password);
        $this->db->where("id_usuario", $this->session->userdata('id_usuario'));
        $this->db->where("activo", '1');
        $this->db->update("usuario");
    }

    public function permisoVisibilidad($permiso)
    {
        $sql    = "SELECT C.id_permisos FROM cliente C WHERE id_usuario=".$this->session->userdata('id_usuario')." AND C.id_permisos != NULL";
        $result = $this->db->query($sql)->row();
        if ($result) {
            $this->db->set("visibilidad_todos", $permiso);
            $this->db->where("id_permisos", $result->id_permisos);
            $this->db->update("permisos_usuarios");
        } else {
            $data = array(
                'enviar_mensaje'        => 0,
                'participacion_debates' => 0,
                'valorar_publicaciones' => 0,
                'anadir_amigo'          => 0,
                'aceptar_solicitud'     => 0,
                'mension_posts'         => 0,
                'email_diario'          => 0,
                'email_semanal'         => 0,
                'invitaciones'          => 0,
                'concursos'             => 0,
                'visibilidad_todos'     => $permiso,
                'email_debate'          => 0,
            );
            $this->db->insert("permisos_usuarios", $data);
            $id_permiso = $this->db->insert_id();
            $this->db->set("id_permisos", $id_permiso);
            $this->db->where("id_usuario", $this->session->userdata('id_usuario'));
            $this->db->update("cliente");
        }
    }

    function deleteCount()
    {
        $this->db->set("activo", 0);
        $this->db->where('id_usuario', $this->session->userdata('id_usuario'));

        return $this->db->update('usuario');
    }

    function permisosNotificaciones($datos)
    {
        $sql    = "SELECT C.id_permisos FROM cliente C WHERE C.id_usuario=".$this->session->userdata('id_usuario');
        $result = $this->db->query($sql)->row();
        $data   = array(
            'enviar_mensaje'        => $datos['enviar_mensaje'],
            'participacion_debates' => $datos['participacion_debates'],
            'valorar_publicaciones' => $datos['valorar_publicaciones'],
            'anadir_amigo'          => $datos['anadir_amigo'],
            'aceptar_solicitud'     => $datos['aceptar_solicitud'],
            'mension_posts'         => $datos['mension_posts'],
            'email_diario'          => $datos['email_diario'],
            'email_semanal'         => $datos['email_semanal'],
            'invitaciones'          => $datos['invitaciones'],
            'concursos'             => $datos['concursos'],
        );
        if ( ! empty($result->id_permisos)) {
            $this->db->set($data);
            $this->db->where('id_permisos', $result->id_permisos);
            $this->db->update('permisos_usuarios');
        } else {
            $id_usuario = $this->session->userdata("id_usuario");
            $id_cliente = $this->db->query("SELECT id_cliente FROM cliente WHERE id_usuario = $id_usuario")->row();
            $this->db->insert("permisos_usuarios", $data);
            $id_permisos = $this->db->insert_id();
            if ( ! empty($id_cliente->id_cliente)) {
                $this->db->set("id_permisos", $id_permisos);
                $this->db->where('id_cliente', $id_cliente->id_cliente);
                $this->db->update('cliente');
            }
        }
    }
}
