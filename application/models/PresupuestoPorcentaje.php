<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PresupuestoPorcentaje extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tabla = "presupuesto_porcentaje";
        $this->id_tabla = "id_presupuesto";
    }

      public function updatePorcentaje($id_boda, $categoria, $porcentaje) {

        $exists = $this->get(array("id_boda" => $id_boda, "categoria" => $categoria));
        if ($exists) {
            return $this->update($exists->id_presupuesto, array("porcentaje" => $porcentaje));
        } else {
            return $this->insert(array(
                        "id_boda" => $id_boda,
                        "categoria" => $categoria,
                        "porcentaje" => $porcentaje,
                        "id_categoria" => PresupuestoPorcentaje::categoriaToCategoriaProveedor($categoria)
            ));
        }
    }

    public static function categoriaToCategoriaProveedor($categoria) {
        /*
          $categoria[] = new Categoria(9, "Animación");
          $categoria[] = new Categoria(11, "Pasteles");
          $categoria[] = new Categoria(17, "Otros"); */
        switch (strtolower($categoria)) {
            //$categoria[] = new Categoria(12, "Novia y Complementos");
            case "vestido": case "zapatos": case "tocado": case "liga": case "lazo":
                return 12;
            // $categoria[] = new Categoria(15, "Joyería");
            case "joyeria": case "anillos": case "arras":
                return 15;
            //$categoria[] = new Categoria(14, "Belleza y Salud");
            case "maquillaje": case "peinado": case "uñas":
                return 14;
            //$categoria[] = new Categoria(13, "Novio y Accesorios");
            case "renta de traje": case "camisa": case "zapatos de novio": case "cinturon":
                return 13;
            //$categoria[] = new Categoria(5, "Autos para Boda");
            case "carro de novios":
                return 5;
            //$categoria[] = new Categoria(16, "Luna de Miel");
            case "noche de bodas":
                return 17;
            //$categoria[] = new Categoria(6, "Invitaciones");
            case "invitaciones": case "confirmaciones":
                return 6;
            //$categoria[] = new Categoria(3, "Foto y video");
            case "foto": case "video":
                return 3;
            //$categoria[] = new Categoria(18, "Ceremonia");
            case "donativo( iglesia o lugar)":
                return 18;
            //$categoria[] = new Categoria(4, "Música");
            case "musica ceremonia": case "musica":
                return 4;
            //$categoria[] = new Categoria(1, "Salón");
            case "salon":
                return 1;
            //$categoria[] = new Categoria(2, "Catering");
            case "banquete": case "vino": case "mobiliario":
                return 2;
            //$categoria[] = new Categoria(10, "Planeación");
            case "wedding planner":
                return 10;
            //$categoria[] = new Categoria(7, "Recuerdos");
            case "centros de mesa":
                return 7;
            //$categoria[] = new Categoria(8, "Flores y Decoración");
            case "decoracion iglesia": case "ramos botenier": case "decoracion coche novios":
                return 8;
        }
        return null;
    }

}
