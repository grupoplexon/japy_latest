<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Presupuesto_model
 *
 * @author Evolutel
 */
class Presupuesto_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = 'id_presupuesto';
        $this->tabla    = 'presupuesto';
    }

    public function updatePorcentaje($id_boda, $categoria, $porcentaje, $cat = null)
    {
        $id_categoria = Presupuesto_model::categoriaToCategoriaProveedor($categoria);
        $exists       = parent::get(['id_boda' => $id_boda, 'nombre' => $categoria]);
        if ($exists) {
            return $this->update($exists->id_presupuesto, [
                    'porcentaje'          => $porcentaje,
                    'costo_aproximado'    => null,
                    'categoria'           => $cat,
                    'categoria_proveedor' => $id_categoria,
            ]);
        } else {
            return $this->insert([
                    'id_boda'             => $id_boda,
                    'categoria'           => $cat,
                    'categoria_proveedor' => $id_categoria,
                    'porcentaje'          => $porcentaje,
                    'nombre'              => $categoria,
            ]);
        }
    }

    public function getAllCategoria($id_boda, $id_categoria)
    {
        $sql = "select presupuesto.*, sum(monto) as pagado 
                from presupuesto left join pago on(pago.id_presupuesto=presupuesto.id_presupuesto and pagado=1)
                where id_boda=$id_boda and categoria=$id_categoria
                group by id_presupuesto";

        return $this->db->query($sql)->result();
    }

    public function getAllCategoriaReal($id_boda, $id_categoria)
    {
        $sql = "select presupuesto.*,sum(monto) as pagado from presupuesto left join pago on(pago.id_presupuesto=presupuesto.id_presupuesto and pagado=1)
                    where id_boda=$id_boda and categoria_proveedor=$id_categoria
                    group by id_presupuesto ";

        return $this->db->query($sql)->result();
    }

    public function get($id)
    {
        if ($id != null) {
            $sql = "SELECT presupuesto.*, sum(monto) as pagado FROM presupuesto left join  pago using(id_presupuesto) where id_presupuesto=$id group by id_presupuesto ";

            return $this->db->query($sql)->row();
        }

        return false;
    }

    public function delete($id)
    {
        try {

            $this->db->where(["id_presupuesto" => $id]);
            $this->db->update("tarea", ["id_presupuesto" => null]);

            return parent::delete($id);
        } catch (Exception $e) {
            return false;
        }
    }

    public function getCategoria($id_boda, $nombre)
    {
        $sql = "SELECT porcentaje FROM presupuesto INNER JOIN boda USING (id_boda) WHERE id_boda = $id_boda AND nombre = '$nombre'";

        return $this->db->query($sql)->row();
    }

    public function boda($idBoda)
    {
        $sql = "SELECT * FROM presupuesto WHERE id_boda = $idBoda";
        return $this->db->query($sql)->result();
    }

    public static function categoriaToCategoriaProveedor($categoria)
    {
        switch (strtolower($categoria)) {
            case "vestido":
            case "zapatos":
            case "tocado":
            case "liga":
            case "lazo":
                return 12;
            case "joyeria":
            case "anillos":
            case "arras":
                return 15;
            case "maquillaje":
            case "peinado":
            case "uñas":
                return 14;
            case "renta de traje":
            case "camisa":
            case "zapatos de novio":
            case "cinturon":
                return 13;
            case "carro de novios":
                return 5;
            case "noche de bodas":
                return 17;
            case "invitaciones":
            case "confirmaciones":
                return 6;
            case "donativo (iglesia o lugar)":
                return 18;
            case "salon":
                return 1;
            case "banquete":
            case "vino":
            case "mobiliario":
                return 2;
            case "foto":
            case "video":
                return 3;
            case "musica ceremonia":
            case "musica":
                return 4;
            case "wedding planner":
                return 10;
            case "centros de mesa":
                return 7;
            case "decoracion iglesia":
            case "ramos botonier":
            case "decoracion coche novios":
                return 8;
        }

        return 17;
    }

    public function getPresupuestoConFecha($presupuesto)
    {
        $arregloPresupuesto = implode(',', $presupuesto);
        $sql = "SELECT id_presupuesto AS 'task_id', nombre AS 'title', fecha_asignada AS 'date' FROM presupuesto WHERE id_presupuesto IN ($arregloPresupuesto) AND fecha_asignada IS NOT NULL";
        return $this->db->query($sql)->result();
    }
}
