<?php

class Producto_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->id_tabla = "id_producto";
        $this->tabla = "producto";
    }

    public function getProveedoresDestacados($tipo_producto, $pag = 1) {
        $compl = $this->queryTipo($tipo_producto);
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        if (count($tipo_producto->tipo_proveedores) == 1) {
            $query = "select proveedor.*,
                        sum(case when id_like_producto is null then 0 else 1 end ) as likes,
                        sum(case when id_producto_comentario is null then 0 else 1 end ) as comentarios,
                        count(id_producto) as cantidad
                         from proveedor inner join  producto using(id_proveedor) left join producto_comentario USING(id_producto)
                        left join like_producto using(id_producto)
                        where id_tipo_proveedor=?  and $compl
                        group by id_proveedor
                        order by tipo_cuenta desc ,comentarios desc,likes desc,cantidad desc  $limit;";
            return $this->db->query($query, array($tipo_producto->tipo_proveedores[0]))->result();
        } else if (count($tipo_producto->tipo_proveedores) == 2) {
            $query = "select proveedor.*,
                        sum(case when id_like_producto is null then 0 else 1 end ) as likes,
                        sum(case when id_producto_comentario is null then 0 else 1 end ) as comentarios,
                        count(id_producto) as cantidad
                         from proveedor inner join  producto using(id_proveedor) left join producto_comentario USING(id_producto)
                        left join like_producto using(id_producto)
                        where (id_tipo_proveedor=? or id_tipo_proveedor=?) and $compl
                        group by id_proveedor
                        order by tipo_cuenta desc ,comentarios desc,likes desc,cantidad desc  $limit;";
            return $this->db->query($query, array($tipo_producto->tipo_proveedores[0], $tipo_producto->tipo_proveedores[1]))->result();
        }
        return null;
    }

    public function getCatalogo($tipo_producto, $pag = 1) {
        $compl = $this->queryTipo($tipo_producto);
        $limit = ($pag == 1 ? " limit 15" : "limit " . (($pag - 1) * 15) . "15");
        if (count($tipo_producto->tipo_proveedores) == 1) {
            $query = "SELECT producto.* FROM proveedor inner join tipo_proveedor using(id_tipo_proveedor) inner join producto using(id_proveedor) "
                    . " where id_tipo_proveedor=? and $compl $limit ;";
            return $this->db->query($query, array($tipo_producto->tipo_proveedores[0]))->result();
        } else if (count($tipo_producto->tipo_proveedores) == 2) {
            $query = "SELECT producto.* FROM proveedor inner join tipo_proveedor using(id_tipo_proveedor) inner join producto using(id_proveedor) "
                    . " where (id_tipo_proveedor=? or id_tipo_proveedor=?) and $compl $limit ;";
            return $this->db->query($query, array($tipo_producto->tipo_proveedores[0], $tipo_producto->tipo_proveedores[1]))->result();
        }
        return null;
    }

    private function queryTipo($tipo_producto) {
        $tipo = NULL;
//        switch (strtoupper($tipo_producto->nombre)) {
//            case "ZAPATOS":case "LENCERIA":case "COMPLEMENTOS":
//                $tipo = strtoupper($tipo_producto->nombre);
//                break;
//        }
//        if ($tipo === NULL) {
//            return "tipo_producto is null";
//        } else {
            return "tipo_producto='$tipo_producto->id_tipo_producto' ";
//        }
    }

    public function getDisenadores($tipo_producto) {
        $compl = $this->queryTipo($tipo_producto);
        if (count($tipo_producto->tipo_proveedores) == 1) {
            $query = "SELECT producto.disenador,count(id_producto) as total FROM proveedor inner join tipo_proveedor using(id_tipo_proveedor) inner join producto using(id_proveedor) "
                    . " where id_tipo_proveedor=? and $compl group by disenador";
            return $this->db->query($query, array($tipo_producto->tipo_proveedores[0]))->result();
        } else if (count($tipo_producto->tipo_proveedores) == 2) {
            $query = "SELECT producto.disenador,count(id_producto) as total FROM proveedor inner join tipo_proveedor using(id_tipo_proveedor) inner join producto using(id_proveedor) "
                    . " where (id_tipo_proveedor=? or id_tipo_proveedor=?) and $compl group by disenador";
            return $this->db->query($query, array($tipo_producto->tipo_proveedores[0], $tipo_producto->tipo_proveedores[1]))->result();
        }
        return null;
    }

    public function getTemporadas($tipo_producto) {
        $compl = $this->queryTipo($tipo_producto);
        if (count($tipo_producto->tipo_proveedores) == 1) {
            $query = "SELECT producto.temporada,count(id_producto) as total FROM proveedor inner join tipo_proveedor using(id_tipo_proveedor) inner join producto using(id_proveedor) "
                    . " where id_tipo_proveedor=? and $compl group by temporada";
            return $this->db->query($query, array($tipo_producto->tipo_proveedores[0]))->result();
        } else if (count($tipo_producto->tipo_proveedores) == 2) {
            $query = "SELECT producto.temporada,count(id_producto) as total FROM proveedor inner join tipo_proveedor using(id_tipo_proveedor) inner join producto using(id_proveedor) "
                    . " where (id_tipo_proveedor=? or id_tipo_proveedor=?) and $compl group by temporada";
            return $this->db->query($query, array($tipo_producto->tipo_proveedores[0], $tipo_producto->tipo_proveedores[1]))->result();
        }
        return null;
    }


}
