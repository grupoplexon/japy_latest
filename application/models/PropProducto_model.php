<?php

class PropProducto_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tabla = "prop_producto";
        $this->id_tabla = "id_propiedad_producto";
    }

}
