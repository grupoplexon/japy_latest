<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Archivo_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla    = "archivo";
        $this->id_tabla = "id_archivo";
    }

    public function has_permiso_download($id_cliente, $id_archivo)
    {
        $sql
            = "SELECT *
                FROM solicitud
                inner join respuesta using(id_solicitud)
                inner join respuesta_archivo using(id_respuesta) 
                where id_archivo=?";

        return $this->db->query($sql, [$id_archivo])->row() ? true : false;
    }


}
