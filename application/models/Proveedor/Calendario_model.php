<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Calendario_model
 *
 * @author Evolutel
 */
class Calendario_model extends MY_Model {

    //put your code here


    public function __construct() {
        parent::__construct();
        $this->tabla = "calendario";
        $this->id_tabla = "id_calendario";
    }

    public function getYear($id_proveedor, $year) {
        $sql = "SELECT * FROM calendario where id_proveedor=$id_proveedor and YEAR(dia)=$year and estado=1";
        return $this->db->query($sql)->result();
    }

}
