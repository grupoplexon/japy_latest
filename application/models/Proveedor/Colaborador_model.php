<?php

class Colaborador_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = NULL;
        $this->tabla = "colaborador";
    }

    public function getColaboradores($empresa)
    {
        $sql = "SELECT p.id_proveedor,id_colaborador,nombre,localizacion_estado,nombre_tipo_proveedor from proveedor p
             inner join tipo_proveedor using(id_tipo_proveedor)
             inner join colaborador c  on(c.id_proveedor=p.id_proveedor and c.id_colaborador=$empresa)  ";
        return $this->db->query($sql)->result();
    }

    public function hasColaboradores($id_proveedor)
    {
        $sql = "SELECT * FROM colaborador where id_proveedor=$id_proveedor limit 1 ;";
        return $this->db->query($sql)->row() ? true : false;
    }

}
