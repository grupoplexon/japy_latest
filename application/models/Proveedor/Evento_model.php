<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Evento_model
 *
 * @author Evolutel
 */
class Evento_model extends MY_Model {

    //put your code here

    public function __construct() {
        parent::__construct();
        $this->tabla = "evento";
        $this->id_tabla = "id_evento";
    }

    public function hasEventos($id_proveedor) {
        $sql = "SELECT count(*) as total FROM evento where id_proveedor=? group by id_proveedor";
        $r=$this->db->query($sql, array($id_proveedor))->row();
        if($r)
            return $r->total;
        else
            return 0;
    }

    public function getEventosFuturos($id_proveedor) {
        $sql = "SELECT * FROM evento where id_proveedor=? and fecha_termino>NOW() order by fecha_inicio asc";
        return $this->db->query($sql, array($id_proveedor))->result();
    }

}
