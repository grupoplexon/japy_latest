<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Galeria_model
 *
 * @author Evolutel
 */
class Inspiracion_model extends MY_Model
{

    //put your code here

    public static $TIPO_IMAGEN = "IMAGEN";
    public static $TIPO_VIDEO = "VIDEO";
    public static $URL_IMAGEN = "uploads/images/";

    public function __construct()
    {
        parent::__construct();
        $this->tabla    = "inspiracion";
        $this->id_tabla = "id_inspiracion";
    }

    public function getImagenesProveedor($id_proveedor)
    {
        $base = base_url().Inspiracion_model::$URL_IMAGEN;
        $this->db->select("id_inspiracion,nombre,CONCAT('$base',nombre) as url")
            ->from($this->tabla)
            ->where(["id_proveedor" => $id_proveedor, "tipo" => Inspiracion_model::$TIPO_IMAGEN, "parent" => null]);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return false;
    }

    public function getVideosProveedor($id_proveedor)
    {
        $base = base_url()."/uploads/videos/";
        $this->db->select("id_galeria,nombre,descripcion,logo,principal,CONCAT('$base',nombre) as url, nombre as filename")
            ->from($this->tabla)
            ->where(["id_proveedor" => $id_proveedor, "tipo" => Galeria_model::$TIPO_VIDEO]);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $videos = [];
            foreach ($query->result() as $result) {
                if ($file = $this->videoExists($result->filename)) {
                    $result->url = $base.'/'.$file;
                    $videos[]    = $result;
                }
            }

            return $videos;
        }

        return false;
    }

    public function getVideo($id_proveedor, $id)
    {
        $base = base_url()."/uploads/videos";
        $this->db->select("id_galeria,nombre,logo,principal,CONCAT('$base/',nombre) as url, CONCAT('video','-',id_proveedor,'-',id_galeria) as filename")
            ->from($this->tabla)
            ->where(["id_proveedor" => $id_proveedor, "tipo" => Galeria_model::$TIPO_VIDEO, "id_galeria" => $id]);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                if ($file = $this->videoExists($result->filename)) {
                    $result->url = $base.'/'.$file;

                    return $result;
                }
            }
        }

        return false;
    }

    public function setNewLogo($id, $id_proveedor)
    {
        $logo = $this->get(["id_proveedor" => $id_proveedor, "logo" => 1, "parent" => null]);
        if ($logo) {
            $this->update($logo->id_galeria, ["logo" => 0]);
        }

        return $this->update($id, ["logo" => 1]);
    }

    public function setNewPrincipal($id, $id_proveedor)
    {
        $logo = $this->get(["id_proveedor" => $id_proveedor, "principal" => 1, "parent" => null]);
        if ($logo) {
            $this->update($logo->id_galeria, ["principal" => 0]);
        }

        return $this->update($id, ["principal" => 1]);
    }

    public function hasVideos($id_proveedor)
    {
        $sql = "SELECT count(id_proveedor) as total FROM galeria where id_proveedor=$id_proveedor and tipo='".Galeria_model::$TIPO_VIDEO."' ;";

        return $this->db->query($sql)->row()->total;
    }

    public function hasGaleria($id_proveedor)
    {
        $sql = "SELECT count(id_proveedor) as total FROM galeria where id_proveedor=$id_proveedor and tipo='".Galeria_model::$TIPO_IMAGEN."' ;";

        return $this->db->query($sql)->row()->total;
    }

    public function hasLP($id_proveedor)
    {
        $sql    = "SELECT id_proveedor,logo,principal from $this->tabla where (id_proveedor=$id_proveedor and logo=1) or (id_proveedor=$id_proveedor and principal=1)";
        $query  = $this->db->query($sql);
        $result = $query->result();
        if ($query->num_rows() >= 2 || ($query->num_rows() == 1 && (bool)$result[0]->logo && (bool)$result[0]->principal)) {
            return true;
        }

        return false;
    }

    public function videoExists($file)
    {
        if (file_exists('./uploads/videos/'.$file)) {
            return $file;
        }

        return false;
    }

}
