<?php

class Promocion_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_promocion";
        $this->tabla = "promocion";
    }

    public function siguientes($id_proveedor)
    {
        $now = new DateTime();
        $now = $now->format("Y-m-d");
        $sql = "SELECT * FROM promocion where id_proveedor=$id_proveedor and fecha_fin >= '$now' ";
        return $this->db->query($sql)->result();
    }

    public function hasPromocion($id_proveedor)
    {
        $sql = "SELECT * FROM promocion where id_proveedor=$id_proveedor limit 1";
        return $this->db->query($sql)->row() ? true : false;
    }

    public function countPromociones($id_proveedor)
    {
        $sql = "SELECT count(id_promocion) AS total FROM promocion  WHERE id_proveedor=? ;";
        return $this->db->query($sql, array($id_proveedor))->row()->total;
    }

    public function getDescargas($id_proveedor, $id_promocion)
    {
        $sql = "SELECT count(*) as descargas FROM descarga_cupon where id_proveedor=$id_proveedor and id_promocion=$id_promocion ";
        return $this->db->query($sql)->row()->descargas;
    }

}
