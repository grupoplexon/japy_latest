<?php

class RespuestaFAQ_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_respuesta_faq";
        $this->tabla = "respuesta_faq";
    }

    public function getPreguntas($id_proveedor, $id_tipo_proveedor)
    {
        $query = "Select * from faq inner join respuesta_faq r on(faq.id_faq=r.id_faq and id_proveedor=$id_proveedor) where id_tipo_proveedor=$id_tipo_proveedor ";

        return $this->db->query($query)->result();
    }

    public function getAllPreguntas($id_proveedor, $id_tipo_proveedor)
    {
        /*        $query = "SELECT * FROM faq LEFT JOIN respuesta_faq using (id_faq) WHERE id_tipo_proveedor = ? AND (id_proveedor = ? or id_proveedor is null)"; */
        $query = "SELECT *,faq.id_faq AS id_faq FROM faq 
                  LEFT JOIN respuesta_faq ON (faq.id_faq=respuesta_faq.id_faq AND id_proveedor=? ) 
                  WHERE id_tipo_proveedor = ? AND (id_proveedor = ? OR id_proveedor IS NULL)";
        return $this->db->query($query, array($id_proveedor, $id_tipo_proveedor, $id_proveedor))->result();
    }

    public function hasPreguntas($id_proveedor)
    {
        $query = "Select id_proveedor FROM respuesta_faq where id_proveedor=$id_proveedor limit 1";

        return $this->db->query($query)->row() ? true : false;
    }

}
