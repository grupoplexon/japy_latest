<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Respuesta_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_respuesta";
        $this->tabla    = "respuesta";
    }

    public function insertArchivo($id_archivo, $id_respuesta)
    {
        try {
            return $this->db->insert("respuesta_archivo", ["id_archivo" => $id_archivo, "id_respuesta" => $id_respuesta]);
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteArchivo($id_archivo, $id_respuesta, $id_proveedor)
    {
        try {
            $p = $this->db->query("SELECT * FROM archivo WHERE id_archivo=?", [$id_archivo])->row();
            if ($p) {
                return $this->db->delete("respuesta_archivo", ["id_archivo" => $id_archivo, "id_respuesta" => $id_respuesta]);
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function getArchivos($id_proveedor, $id_respuesta)
    {
        $sql
            = "SELECT respuesta_archivo.*,archivo.*
                FROM respuesta 
                INNER JOIN respuesta_archivo USING(id_respuesta) 
               INNER JOIN archivo USING(id_archivo)
               WHERE respuesta.id_proveedor=? AND id_respuesta=?";

        return $this->db->query($sql, [$id_proveedor, $id_respuesta])->result();
    }

    public function getPlantillasMin($id_proveedor)
    {
        $sql = "SELECT id_respuesta,nombre_plantilla FROM respuesta WHERE respuesta.id_proveedor=? AND plantilla=1 AND enable = 1";

        return $this->db->query($sql, array($id_proveedor))->result();
    }

    public function getPlantillas($id_proveedor)
    {
        $sql = "SELECT * FROM respuesta  WHERE respuesta.id_proveedor=? AND plantilla=1 AND enable = 1";

        return $this->db->query($sql, array($id_proveedor))->result();
    }

    public function markLeidoRespuestaCliente($id_solicitud)
    {
        try {
            $sql = "update respuesta set leido=1 where  id_solicitud=$id_solicitud and id_proveedor not like 'client-%' ";
            $this->db->query($sql);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateEnableTemplate($id)
    {
        try {
            $sql = "UPDATE respuesta SET enable = 0 WHERE id_respuesta = $id";
            $this->db->query($sql);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}
