<?php

class Solicitud_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tabla = "solicitud";
        $this->id_tabla = "id_solicitud";
    }

    public function getEntrada($id_proveedor, $page = 1, $leido = 0, $palabra = null, $inicio = null, $fin = null, $tipo = null) {
        if ($page == 1) {
            $limit = " limit 15";
        } else {
            $limit = " limit   " . (($page - 1) * 15) . ",15 ";
        }
        if ($palabra != NULL) {
            $palabra = " and (usuario.nombre like '%$palabra%' or  usuario.apellido like '%$palabra%' or solicitud.mensaje like '%$palabra%' ) ";
        } else {
            $palabra = "";
        }
        if ($inicio && $fin && $tipo) {
            if ($tipo == "boda") {
                $tipo = "boda.fecha_boda";
            } else {
                $tipo = "solicitud.fecha_creacion";
            }
            $fecha = " and ( $tipo BETWEEN '$inicio' and '$fin' ) ";
        } else {
            $fecha = "";
        }
        $sql = "SELECT   usuario.nombre,usuario.apellido,boda.fecha_boda,boda.no_invitado,solicitud.*  FROM solicitud"
                . " inner join boda using(id_boda)"
                . " inner join cliente using(id_boda)"
                . " inner join usuario using(id_usuario)"
                . " where id_proveedor=$id_proveedor and leida=$leido $palabra $fecha "
                . " order by solicitud.fecha_creacion desc $limit";
        return $this->db->query($sql)->result();
    }

    public function getEntradaEstado($id_proveedor, $page = 1, $estado = 0, $palabra = null, $inicio = null, $fin = null, $tipo = null) {
        if ($page == 1) {
            $limit = " limit 15";
        } else {
            $limit = " limit   " . (($page - 1) * 15) . ",15 ";
        }


        if ($palabra != NULL) {
            $palabra = " and (usuario.nombre like '%$palabra%' or  usuario.apellido like '%$palabra%' or solicitud.mensaje like '%$palabra%' ) ";
        } else {
            $palabra = "";
        }

        if ($inicio && $fin && $tipo) {
            if ($tipo == "boda") {
                $tipo = "boda.fecha_boda";
            } else {
                $tipo = "solicitud.fecha_creacion";
            }
            $fecha = " and ( $tipo BETWEEN '$inicio' and '$fin' ) ";
        } else {
            $fecha = "";
        }
        $sql = "SELECT    usuario.nombre,usuario.apellido,boda.fecha_boda,boda.no_invitado,solicitud.* FROM solicitud"
                . " inner join boda using(id_boda)"
                . " inner join cliente using(id_boda)"
                . " inner join usuario using(id_usuario)"
                . " where id_proveedor=$id_proveedor and solicitud.estado=$estado  $palabra $fecha "
                . " order by solicitud.fecha_creacion desc $limit";
        return $this->db->query($sql)->result();
    }

    public function getCount($id_proveedor, $leido = 0) {
        $sql = "SELECT count(id_solicitud) as total FROM solicitud where id_proveedor=? and leida=?";
        return $this->db->query($sql, array($id_proveedor, $leido))->row()->total;
    }

    public function getCountEstado($id_proveedor, $estado = NULL) {
        if ($estado === NULL) {
            $sql = "SELECT count(id_solicitud) as total FROM solicitud where id_proveedor=$id_proveedor ";
        } else if ($estado >= 0 && $estado < 4) {
            $sql = "SELECT count(id_solicitud) as total FROM solicitud where id_proveedor=$id_proveedor and estado=$estado";
        }
        return $this->db->query($sql)->row()->total;
    }

    public function getPendientes($id_proveedor, $numero) {
        $sql = "select nombre,apellido,solicitud.* from solicitud 
                inner join cliente using(id_cliente)
                inner join usuario using(id_usuario)
                where id_proveedor=$id_proveedor and solicitud.estado=0 order by solicitud.fecha_creacion limit $numero ;";
        return $this->db->query($sql)->result();
    }

    public function updateEstado($ids, $estado) {
        try {
            $data = array();
            foreach ($ids as $key => $id) {
                $data[] = array(
                    "id_solicitud" => $id,
                    "estado" => $estado
                );
            }
            $this->db->update_batch($this->tabla, $data, $this->id_tabla);
            return TRUE;
        } catch (Exception $e) {
            
        }
        return false;
    }

    public function updateLeida($ids, $estado) {
        try {
            $data = array();
            foreach ($ids as $key => $id) {
                $data[] = array(
                    "id_solicitud" => $id,
                    "leida" => $estado
                );
            }
            $this->db->update_batch($this->tabla, $data, $this->id_tabla);
            return TRUE;
        } catch (Exception $e) {
            
        }
        return false;
    }

    public function getSolicitudesTime($inicio, $fin, $id_proveedor) {
        $sql = "SELECT solicitud.*, usuario.nombre,usuario.apellido,usuario.correo,boda.no_invitado,fecha_boda
                    FROM solicitud 
                    inner join boda using(id_boda) 
                    inner join cliente on(boda.id_boda=cliente.id_boda) 
                    inner join usuario on(cliente.id_usuario=usuario.id_usuario)
                    where solicitud.id_proveedor=? and solicitud.fecha_creacion between ? and ?";
        return $this->db->query($sql, array($id_proveedor, $inicio, $fin))->result();
    }

    public function getMensajes($id_proveedor, $id_solicitud) {
        $sql = "SELECT respuesta.* FROM respuesta inner join solicitud using(id_solicitud) where id_solicitud=? and solicitud.id_proveedor=? ;";
        return $this->db->query($sql, array($id_solicitud, $id_proveedor))->result();
    }

    // SECCION DE SOLICITUDES NOVIOS;
    public function getEnviadosCliente($id_cliente, $page = 1) {
        if ($page == 1) {
            $limit = " limit 15";
        } else {
            $limit = " limit   " . (($page - 1) * 15) . ",15 ";
        }
        $sql = "select solicitud.*,fecha_boda,no_invitado,proveedor.nombre from solicitud
                inner join boda using(id_boda)
                inner join cliente using(id_boda)
                inner join proveedor using(id_proveedor)
                where visible_client=1 and cliente.id_cliente=? $limit ";
        return $this->db->query($sql, array($id_cliente))->result();
    }

    public function getCountEnviadosCliente($id_cliente) {
        $sql = "select count(id_solicitud) as total from solicitud
                inner join boda using(id_boda)
                inner join cliente using(id_boda)
                where visible_client=1 and cliente.id_cliente=?  ";
        return $this->db->query($sql, array($id_cliente))->row()->total;
    }

    public function getCountBandejaEntrada($id_cliente) {
        $sql = "select count(id_respuesta) as total
                from respuesta
                inner join solicitud using(id_solicitud)
                where visible_client=1 and id_respuesta in (
                        select max(id_respuesta) id_respuesta from solicitud 
                        inner join cliente using(id_boda)
                        inner join respuesta using(id_solicitud)
                        where cliente.id_cliente=? and respuesta.id_proveedor is not null and respuesta.id_proveedor!=0
                        group by id_solicitud
                ) ;";
        return $this->db->query($sql, array($id_cliente))->row()->total;
    }

    public function getBandejaEntrada($id_cliente, $page = 1) {
        if ($page == 1) {
            $limit = " limit 15";
        } else {
            $limit = " limit   " . (($page - 1) * 15) . ",15 ";
        }
        $sql = "select respuesta.*,proveedor.nombre
                from respuesta
                inner join proveedor using(id_proveedor)
                inner join solicitud using(id_solicitud)
                where visible_client=1 and id_respuesta in (
                        select max(id_respuesta) id_respuesta from solicitud 
                        inner join cliente using(id_boda)
                        inner join respuesta using(id_solicitud)
                        where cliente.id_cliente=? and respuesta.id_proveedor is not null and respuesta.id_proveedor!=0
                        group by id_solicitud
                ) order by fecha_respuesta desc $limit;
";
        return $this->db->query($sql, array($id_cliente))->result();
    }

}
