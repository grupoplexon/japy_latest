<?php

class TipoProveedor_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla = "tipo_proveedor";
        $this->id_tabla = "id_tipo_proveedor";
    }

    public function getFilters($id_tipo_proveedor, MY_Input $input)
    {
        $sql = 'SELECT * FROM faq WHERE id_tipo_proveedor = ? AND titulo IS NOT NULL AND tipo != "TEXT";';
        $tipos = $this->db->query($sql, array($id_tipo_proveedor))->result();
        if ($tipos) {
            foreach ($tipos as $value) {
                $value->select = explode(',', str_replace('_', ' ', $input->get(implode('_', explode(' ', $value->titulo)))));
                if ($value->tipo == 'RANGE') {
                    $f = $this->getRangeFilter($value->id_faq);
                    if ($f->id_faq != null) {
                        $value->minimo = $f->minimo;
                        $value->maximo = $f->maximo;
                        $value->valores = $f->minimo;
                    }
                }
            }
        }
        return $tipos;
    }

    private function getRangeFilter($id_faq)
    {
        $sql = 'SELECT id_faq, min(REPLACE( substring_index(respuesta , "|" , 1) , "," , "" )+0) AS minimo, max(REPLACE( substring_index(respuesta , "|" , -1) , "," , "" )+0) AS maximo FROM respuesta_faq WHERE id_faq = ? ;';
        return $this->db->query($sql, $id_faq)->row();
    }

}
