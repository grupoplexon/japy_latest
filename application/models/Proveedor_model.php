<?php

class Proveedor_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla = "proveedor";
        $this->id_tabla = "id_proveedor";
    }

    public function getAllAdmin()
    {
        $sql = 'SELECT proveedor.* ,usuario.correo, usuario.usuario, usuario.activo ,fecha_creacion FROM proveedor INNER JOIN usuario USING(id_usuario) ;';
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function exitsUserName($username)
    {
        $sql = 'SELECT * FROM  usuario WHERE usuario =? ;';
        $query = $this->db->query($sql, [$username]);

        return $query->num_rows() > 0;
    }

    public function insertUsuarioProveedor($usuario, $proveedor, $tipos_proveedor)
    {
        try {
            $this->db->trans_begin();
            $this->db->insert("usuario", $usuario);
            $id = $this->db->insert_id();
            $proveedor["id_usuario"] = $id;
            $this->db->insert("proveedor", $proveedor);
            $id_proveedor = $this->db->insert_id();
            $prom = [
                'id_proveedor' => $id_proveedor,
                'promedio' => 5
            ];
            $this->db->insert('average_reviews', $prom);
            foreach ($tipos_proveedor as $value) {
                $this->db->insert("rel_tipo_proveedor",
                    ["id_proveedor" => $id_proveedor, "id_tipo_proveedor" => $value]);
            }
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();

                return false;
            } else {
                $this->db->trans_commit();

                return true;
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();

            return false;
        }
    }

    public function search($term, $excluir)
    {
        $sql = "SELECT id_proveedor as id,nombre,localizacion_estado as estado, nombre_tipo_proveedor as tipo FROM proveedor p left join tipo_proveedor t"
            . "  on(p.id_tipo_proveedor=t.id_tipo_proveedor ) "
            . " left join colaborador using(id_proveedor) "
            . "  where  id_colaborador is null and( nombre like '%$term%' or localizacion_estado like '%$term%' or  nombre_tipo_proveedor like '%$term%' ) and id_proveedor!=$excluir limit 10 ";

        return $this->db->query($sql)->result();
    }

    public function search_cat($term, $cat = 'ALL')
    {
        if ($cat == "ALL") {
            $sql = "SELECT id_proveedor as id,nombre,localizacion_estado as estado, nombre_tipo_proveedor as tipo FROM proveedor p inner join tipo_proveedor t"
                . "  on(p.id_tipo_proveedor=t.id_tipo_proveedor ) "
                . "  where   nombre like '$term%' or localizacion_estado like '$term%' or  nombre_tipo_proveedor like '$term%' limit 10 ";
        } else {
            $cat = urldecode($cat);
            $sql = 'SELECT id_proveedor as id,nombre,localizacion_estado as estado, nombre_tipo_proveedor as tipo FROM proveedor p inner join tipo_proveedor t'
                . "  on(p.id_tipo_proveedor=t.id_tipo_proveedor and tag_categoria='$cat') "
                . "  where nombre like '$term%' or localizacion_estado like '$term%' or  nombre_tipo_proveedor like '$term%' limit 10 ";
        }

        return $this->db->query($sql)->result();
    }

    public function getProveedor($id_usuario)
    {
        $sql = 'SELECT * FROM usuario INNER JOIN proveedor USING(id_usuario) WHERE id_usuario=? ;';

        return $this->db->query($sql, [$id_usuario])->row();
    }

    public function getLogo($id_proveedor)
    {
        $sql = 'SELECT * FROM galeria g INNER JOIN proveedor p ON(p.id_proveedor=g.id_proveedor AND g.logo=1) WHERE p.id_proveedor=? AND g.parent IS NOT NULL';

        return $this->db->query($sql, [$id_proveedor])->row();
    }

    public function getPrincipal($id_proveedor)
    {
        $sql = 'SELECT * FROM galeria g INNER JOIN proveedor p ON(p.id_proveedor=g.id_proveedor AND g.principal=1) WHERE p.id_proveedor=? ';

        return $this->db->query($sql, [$id_proveedor])->row();
    }

    public function getPrincipalMini($id_proveedor)
    {
        $sql = 'SELECT id_galeria,g.nombre FROM galeria g INNER JOIN proveedor p ON(p.id_proveedor=g.id_proveedor AND g.principal=1) WHERE p.id_proveedor=?';

        return $this->db->query($sql, [$id_proveedor])->row();
    }

    public function updateUsuarioProveedor($id, $usuario, $proveedor)
    {
        try {
            $p = $this->getProveedor($id);
            if ($p) {
                $this->db->trans_begin();
                $this->db->where(['id_usuario' => $id]);
                $this->db->update('usuario', $usuario);
                $this->db->where(['id_proveedor' => $p->id_proveedor]);
                $this->db->update('proveedor', $proveedor);
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();

                    return false;
                } else {
                    $this->db->trans_commit();

                    return true;
                }
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }

        return false;
    }

    public function getFAQIDbyName($servucio, $titulo)
    {
        $sql = 'SELECT id_faq FROM faq WHERE id_tipo_proveedor=? AND titulo=?';

        return $this->db->query($sql, [$servucio, $titulo])->row();
    }

    public function search_menu(
        $pagina = 1,
        $categoria = null,
        $sector = null,
        $estado = null,
        $ciudad = null,
        $boda_id = 0,
        $filtros = []
    )
    {
        $filtro = "";
        $categoriaQuery = "";
        $estadoQuery = "";
        $sectorQuery = '';

        foreach ($filtros as $key => $value) {
            $faq = $this->getFAQIDbyName($categoria, $key);
            if ($faq) {
                $value = str_replace('_', ' ', $value);
                $filtro .= " and filtrar_proveedor(proveedor.id_proveedor,$faq->id_faq,'$value')=1 ";
            }
        }

        if ($categoria) {
            $categoriaQuery = " and ( tipo_proveedor.id_tipo_proveedor = $categoria  or rel_tipo_proveedor.id_tipo_proveedor = $categoria ) ";
        }

        if ($estado && $ciudad) {
            $estadoQuery = " and localizacion_estado='$estado' and localizacion_poblacion='$ciudad' ";
        } elseif ($estado) {
            $estadoQuery = " and localizacion_estado='$estado'";
        }

        if ($sector) {
            $sectorQuery = " and grupo='" . strtoupper($sector) . "'";
        }

        $limite = ' limit ' . (50 * (($pagina <= 1 ? 1 : $pagina) - 1)) . ',50';

        if ($categoria == 'BANQUETES' || $categoria == 'NOVIOS' || $categoria == 'NOVIAS' || $categoria == 'PROVEEDORES' || $sector) {
            $sql =
                "SELECT proveedor.*, nombre_tipo_proveedor,grupo,producto,CASE WHEN id_boda IS NULL THEN 0 ELSE 1 END AS favorito 
                 FROM proveedor 
                 INNER JOIN tipo_proveedor USING(id_tipo_proveedor)  
                 LEFT JOIN proveedor_boda ON(proveedor.id_proveedor=proveedor_boda.id_proveedor AND id_boda=$boda_id) 
                 LEFT JOIN rel_tipo_proveedor ON(rel_tipo_proveedor.id_proveedor=proveedor.id_proveedor) 
                 WHERE acceso=1 $categoriaQuery $sectorQuery $estadoQuery $filtro group by proveedor.id_proveedor  $limite";
        } else {
            $sql = "SELECT proveedor.*,nombre_tipo_proveedor,grupo,producto,
				CASE WHEN id_boda IS NULL THEN 0 ELSE 1 END AS favorito
				FROM proveedor 
				INNER JOIN tipo_proveedor USING(id_tipo_proveedor)
				LEFT JOIN proveedor_boda ON(proveedor.id_proveedor=proveedor_boda.id_proveedor AND id_boda=0)
				WHERE acceso=1 AND proveedor.descripcion LIKE '%$categoria%' $estadoQuery $limite";
        }
        $result = $this->db->query($sql)->result();

        return $result;
    }

    public function total_search_menu($categoria = null, $sector = null, $estado = null, $ciudad = null, $boda = 0)
    {
        $c = "";
        if ($categoria) {
            $c = " and id_tipo_proveedor=$categoria ";
        }
        $e = "";
        if ($estado && $ciudad) {
            $e = " and localizacion_estado='$estado' and localizacion_poblacion='$ciudad' ";
        } elseif ($estado) {
            $e = " and localizacion_estado='$estado'";
        }
        $s = '';
        if ($sector) {
            $s = " and grupo='" . strtoupper($sector) . "'";
        }
        if ($categoria == 'BANQUETES' || $categoria == 'NOVIOS' || $categoria == 'NOVIAS' || $categoria == 'PROVEEDORES' || $sector) {
            $sql = 'SELECT count(id_proveedor) as total '
                . ' FROM proveedor '
                . ' inner join tipo_proveedor using(id_tipo_proveedor)'
                . "  where acceso=1 $c $s $e ";
        } else {
            $sql = "SELECT count(id_proveedor) as total "
                . ' FROM proveedor '
                . ' inner join tipo_proveedor using(id_tipo_proveedor)'
                . "  where acceso=1 and proveedor.descripcion like '%$categoria%' $e ";
        }
        $result = $this->db->query($sql)->row()->total;

        return $result;
    }

    public function search_menu_promociones($categoria = null, $sector = null, $estado = null, $ciudad = null)
    {
        $c = "";
        if ($categoria) {
            $c = " and id_tipo_proveedor=$categoria ";
        }
        $e = "";
        if ($estado && $ciudad) {
            $e = " and localizacion_estado='$estado' and localizacion_poblacion='$ciudad' ";
        } elseif ($estado) {
            $e = " and localizacion_estado='$estado'";
        }
        $s = "";
        if ($sector) {
            $s = " and grupo='" . strtoupper($sector) . "'";
        }
        $l = " limit 10";
        if ($categoria == 'BANQUETES' || $categoria == 'NOVIOS' || $categoria == 'NOVIAS' || $categoria == 'PROVEEDORES' || $sector) {
            $sql = "SELECT promocion.*,proveedor.nombre as nombre_proveedor, localizacion_estado, localizacion_poblacion,nombre_tipo_proveedor
                    FROM proveedor 
                inner join tipo_proveedor using(id_tipo_proveedor)  
                inner join promocion using(id_proveedor)
		where acceso=1 and fecha_fin > NOW() and fecha_inicio < NOW() $c $s $e $l ";
        } else {
            $sql = "SELECT promocion.*,proveedor.nombre as nombre_proveedor, localizacion_estado, localizacion_poblacion,nombre_tipo_proveedor
                    FROM proveedor 
                inner join tipo_proveedor using(id_tipo_proveedor)  
                inner join promocion using(id_proveedor)
		where acceso=1 and fecha_fin > NOW() and fecha_inicio < NOW() and proveedor.descripcion like '%$categoria%' $e $l ";
        }

        return $this->db->query($sql)->result();
    }

    public function getRelacionadosSolicitudes($id_proveedor, $id_boda)
    {
        $sql = "select DISTINCT proveedor.* from proveedor inner join solicitud using(id_proveedor) " .
            "where id_proveedor!=$id_proveedor and id_boda!=$id_boda and id_boda in ( " .
            "	select id_boda from boda inner join solicitud using(id_boda) where id_proveedor=$id_proveedor group by id_boda " .
            ") " .
            "limit 6;";

        $result = $this->db->query($sql)->result();

        if (count($result) < 6) {
            $p = $this->get($id_proveedor);
            $l = 6 - count($result);
            $sql = "SELECT proveedor.* FROM proveedor WHERE localizacion_estado='$p->localizacion_estado' and id_proveedor!=$id_proveedor limit $l";
            $re = $this->db->query($sql)->result();

            return array_merge($result, $re);
        }

        return $result;
    }

    public function isSector($variable)
    {
        switch ($variable) {
            case $variable:
                break;

            default:
                break;
        }
    }

    public function visitas12meses($id_proveedor)
    {
        $sql = "select CONCAT(LPAD(MONTH(fecha), 2, '0'),'-',YEAR(fecha)) as mes,count(id_proveedor) as visitas " .
            'from visita_escaparate ' .
            'where id_proveedor=? and fecha >= date_sub(curdate(), interval 12 month)  ' .
            'group by CONCAT(YEAR(fecha),MONTH(fecha));';

        return $this->db->query($sql, [$id_proveedor])->result();
    }

    public function telefono12meses($id_proveedor)
    {
        $sql = "select CONCAT(LPAD(MONTH(fecha), 2, '0'),'-',YEAR(fecha)) as mes,count(id_proveedor) as visitas
            from ver_telefono 
            where id_proveedor=$id_proveedor and fecha >= date_sub(curdate(), interval 12 month) 
            group by CONCAT(YEAR(fecha),MONTH(fecha));";

        return $this->db->query($sql)->result();
    }

    public function solicitudes12meses($id_proveedor)
    {
        $sql = "select CONCAT(LPAD(MONTH(fecha_creacion), 2, '0'),'-',YEAR(fecha_creacion)) as mes,count(id_proveedor) as visitas  from proveedor 
                        inner join solicitud using(id_proveedor)
                        where id_proveedor=$id_proveedor and fecha_creacion >= date_sub(curdate(), interval 12 month) 
                        group by CONCAT(YEAR(fecha_creacion),MONTH(fecha_creacion));";

        return $this->db->query($sql)->result();
    }

    public function totalSolicitudesPendientes12meses($id_proveedor)
    {
        $sql = "select count(id_proveedor) as total  from proveedor 
                inner join solicitud using(id_proveedor)
                where id_proveedor=$id_proveedor and solicitud.estado=0 and fecha_creacion >= date_sub(curdate(), interval 12 month) ";

        return $this->db->query($sql)->row()->total;
    }

    public function timepoRespuestaSolicitudes($id_usuario)
    {
        $sql = "SELECT
                    c.id_correo,
                    min(r.fecha_creacion) AS fecha_respuesta,
                    EXTRACT(
                        HOUR
                        FROM
                            TIMEDIFF(r.fecha_creacion, c.fecha_creacion)
                    ) AS horas,
                    EXTRACT(
                        MINUTE
                        FROM
                            TIMEDIFF(r.fecha_creacion, c.fecha_creacion)
                    ) AS minutos
                FROM
                    correo c
                INNER JOIN correo r on (r.parent=c.id_correo)
                WHERE
                    c.`to` = $id_usuario
                AND c.fecha_creacion >= date_sub(curdate(), INTERVAL 3 MONTH)
                GROUP BY
                    c.id_correo,horas,minutos";
        $result = $this->db->query($sql)->result();
        if ($result) {
            $total = 0;
            $count = 0;
            foreach ($result as $r) {
                $total += $r->horas + ((1 / 60) * $r->minutos);
                $count++;
            }
            $prom = $total / $count;
            $aux = (string)$prom;
            $min = substr($aux, strpos($aux, ".")) * 60;

            return intval($prom) . "H" . $min . "M";
        }

        return "0m";
    }

    public function registrarVisita($id_proveedor, $id_cliente)
    {
        return $this->db->insert('visita_escaparate', ['id_proveedor' => $id_proveedor, 'id_cliente' => $id_cliente]);
    }

    public function registrarVerTel($id_proveedor, $id_cliente)
    {
        return $this->db->insert('ver_telefono', ['id_proveedor' => $id_proveedor, 'id_cliente' => $id_cliente]);
    }

    public function registrarDescargaCupon($id_proveedor, $id_promocion, $id_cliente)
    {
        try {
            return $this->db->insert('descarga_cupon',
                ['id_proveedor' => $id_proveedor, 'id_cliente' => $id_cliente, 'id_promocion' => $id_promocion]);
        } catch (Exception $ex) {

        }
    }

    public function favorito($id_proveedor, $id_boda)
    {
        return $this->db->insert('proveedor_boda', ['id_proveedor' => $id_proveedor, 'id_boda' => $id_boda]);
    }

    public function no_favorito($id_proveedor, $id_boda)
    {
        return $this->db->delete("proveedor_boda", ["id_proveedor" => $id_proveedor, "id_boda" => $id_boda]);
    }

    public function isFavorito($id_proveedor, $id_boda)
    {
        $sql = "SELECT * from proveedor_boda where id_proveedor=$id_proveedor and id_boda=$id_boda";

        return $this->db->query($sql)->row() ? true : false;
    }

    public function precioDesde($id_proveedor)
    {
        $sql = "SELECT respuesta_faq.respuesta AS respuesta FROM proveedor 
                INNER JOIN respuesta_faq USING(id_proveedor)
                INNER JOIN faq USING(id_faq)
                WHERE id_proveedor=? AND faq.tipo='RANGE' AND faq.valores IS NULL LIMIT 1";
        $res = $this->db->query($sql, [$id_proveedor])->row();
        if ($res) {
            $res = $res->respuesta;

            return explode("|", $res);
        }

        return null;
    }

    public function capacidad($id_proveedor)
    {
        $sql = "SELECT respuesta FROM  respuesta_faq 
                    INNER JOIN faq USING(id_faq)
                    WHERE id_proveedor=? AND faq.tipo='RANGE' AND valores='PERSONAS'; ";
        $res = $this->db->query($sql, [$id_proveedor])->row();
        if ($res) {
            $res = $res->respuesta;

            return explode("|", $res);
        }

        return null;
    }

    public function labelTipoCuenta($tipo)
    {
        switch ($tipo) {
            case 0:
                return "Basico";
            case 1:
                return "Plata";
            case 2:
                return "Oro";
            case 3:
                return "Diamante";
        }
    }

    public function RlabelTipoCuenta($tipo)
    {
        switch (strtoupper($tipo)) {
            case "BASICO":
                return 0;
            case "PLATA":
                return 1;
            case "ORO":
                return 2;
            case "DIAMANTE":
                return 3;
        }

        return 0;
    }

    public function lastRecomendaciones($id_proveedor, $num = 5)
    {
        $sql = "SELECT
                        proveedor_boda.*,usuario.*,boda.fecha_boda
                FROM
                        proveedor_boda
                INNER JOIN boda USING (id_boda)
                INNER JOIN cliente USING (id_boda)
                INNER JOIN usuario USING (id_usuario)
                WHERE
                        id_proveedor = $id_proveedor
                AND fecha_recomendacion IS NOT NULL
                LIMIT $num ;";

        return $this->db->query($sql)->result();
    }

    public function pageRecomendaciones($id_proveedor, $page = 1)
    {
        $limit = "limit " . (($page - 1) * 10) . ",10";
        $sql = "SELECT
                        proveedor_boda.*,usuario.*,boda.fecha_boda
                FROM
                        proveedor_boda
                INNER JOIN boda USING (id_boda)
                INNER JOIN cliente USING (id_boda)
                INNER JOIN usuario USING (id_usuario)
                WHERE
                        id_proveedor = $id_proveedor
                AND fecha_recomendacion IS NOT NULL
                $limit; ";

        return $this->db->query($sql)->result();
    }

    public function getRecomendacion($id_proveedor, $id_boda)
    {

        $sql = "SELECT
                        proveedor_boda.*,usuario.*,boda.fecha_boda
                FROM
                        proveedor_boda
                INNER JOIN boda USING (id_boda)
                INNER JOIN cliente USING (id_boda)
                INNER JOIN usuario USING (id_usuario)
                WHERE
                        id_proveedor = $id_proveedor and id_boda=$id_boda
                AND fecha_recomendacion IS NOT NULL
                 ";

        return $this->db->query($sql)->result();
    }

    public function totalRecomendaciones($id_proveedor)
    {
        $sql = "SELECT count(*) AS total FROM proveedor_boda WHERE id_proveedor=?";

        return $this->db->query($sql, [$id_proveedor])->row()->total;
    }

    public function hasRecomendaciones($id_proveedor)
    {
        $sql = "select id_proveedor from proveedor_boda where id_proveedor=$id_proveedor limit 1";

        return $this->db->query($sql)->row() ? true : false;
    }

    public function misRecomendaciones($id_boda)
    {
        $sql = "SELECT * FROM proveedor_boda inner join proveedor using(id_proveedor) where id_boda=$id_boda  AND fecha_recomendacion IS NOT NULL ;";

        return $this->db->query($sql)->result();
    }

    public function getByCategoriaPresupuesto($categoria, $presupuesto, $invitados = 400, $page = 1)
    {
        $cat = $this->getCategoriasbyTAG($categoria);
        $result = [];
        $sql = 'SELECT  proveedor.*, likes_proveedor(id_proveedor) AS likes, recomendaciones_proveedor(id_proveedor) AS recomendaciones , rel_tipo_proveedor.id_tipo_proveedor,nombre_tipo_proveedor  ' .
            'FROM proveedor ' .
            'INNER JOIN tipo_proveedor USING(id_tipo_proveedor) ' .
            'LEFT JOIN rel_tipo_proveedor USING(id_proveedor) ' .
            'WHERE ' .
            '( ' .
            '	proveedor.id_tipo_proveedor = ? ' .
            ') ' .
            ' and entra_en_presupuesto(id_proveedor,tipo_proveedor.id_tipo_proveedor,?,?) > 0  order by tipo_cuenta desc,likes desc,recomendaciones desc  limit ' . (($page - 1) * 8) . ',8';
        $ids = [];
        foreach ($cat as $c) {
            $r = $this->db->query($sql, [$c->id_tipo_proveedor, $presupuesto, $invitados])->result();
            if ($r) {
                $uniq = [];
                foreach ($r as $value) {
                    if (!key_exists($value->id_proveedor, $ids)) {
                        $ids[$value->id_proveedor] = true;
                        $uniq[] = $value;
                    }
                }
                $result = array_merge($result, $uniq);
            }
        }
        if (count($result) > 0) {
            return $result;
        }

        return null;
    }

    public function getProveedoresByCategoriaPresupuesto($categoria, $presupuesto, $invitados = 400, $page = 1)
    {
        if ($categoria) {
            $sql = 'SELECT  proveedor.*, likes_proveedor(id_proveedor) AS likes, recomendaciones_proveedor(id_proveedor) AS recomendaciones , rel_tipo_proveedor.id_tipo_proveedor,nombre_tipo_proveedor  ' .
                'FROM proveedor ' .
                'INNER JOIN tipo_proveedor USING(id_tipo_proveedor) ' .
                'LEFT JOIN rel_tipo_proveedor USING(id_proveedor) ' .
                'WHERE ' .
                '( ' .
                '	proveedor.id_tipo_proveedor = ? ' .
                ') ' .
                ' and entra_en_presupuesto(id_proveedor,tipo_proveedor.id_tipo_proveedor,?,?) > 0  order by tipo_cuenta desc,likes desc,recomendaciones desc  limit ' . (($page - 1) * 8) . ',8';
            $result = $this->db->query($sql, [$categoria, $presupuesto, $invitados])->result();
            if (count($result) > 0) {
                return $result;
            }
        }

        return null;
    }

    public function getCountCategoriaPresupuesto($categoria, $presupuesto, $invitados = 400)
    {
        $cat = $this->getCategoriasbyTAG($categoria);
        $total = 0;
        $sql = 'SELECT count(id_proveedor) as total  ' .
            'FROM proveedor ' .
            ' WHERE ' .
            ' proveedor.id_tipo_proveedor = ? ' .
            ' and entra_en_presupuesto(id_proveedor,proveedor.id_tipo_proveedor,?,?) > 0  ';
        foreach ($cat as $c) {
            $r = $this->db->query($sql, [$c->id_tipo_proveedor, $presupuesto, $invitados])->row();
            if ($r) {
                $total += $r->total;
            }
        }

        return $total;
    }

    public function getByAllPresupuesto($presupuesto, $invitados = 400, $page = 1)
    {
        $result = [];
        $sql = 'SELECT  *, likes_proveedor(id_proveedor) AS likes, recomendaciones_proveedor(id_proveedor) AS recomendaciones , rel_tipo_proveedor.id_tipo_proveedor,nombre_tipo_proveedor  ' .
            'FROM proveedor ' .
            'INNER JOIN tipo_proveedor USING(id_tipo_proveedor) ' .
            'LEFT JOIN rel_tipo_proveedor USING(id_proveedor) ' .
            'WHERE entra_en_presupuesto(id_proveedor,tipo_proveedor.id_tipo_proveedor,?,?) > 0  order by tipo_cuenta desc,likes desc,recomendaciones desc  limit 16';
        $r = $this->db->query($sql, [$presupuesto, $invitados])->result();
        if ($r) {
            $result = array_merge($result, $r);
        }
        if (count($result) > 0) {
            return $result;
        }

        return null;
    }

    public function getCategoriasbyTAG($tag)
    {
        $sql_c = 'SELECT * FROM tipo_proveedor WHERE tag_categoria=? ;';

        return $this->db->query($sql_c, [$tag])->result();
    }

    public function esPorPersona($id_tipo_proveedor)
    {
        $sql = 'SELECT id_tipo_proveedor FROM  tipo_proveedor INNER JOIN faq USING(id_tipo_proveedor) WHERE tipo="RANGE" AND valores="PERSONAS" AND id_tipo_proveedor=? ';
        $t = $this->db->query($sql, [$id_tipo_proveedor])->row();
        if ($t) {
            return true;
        }

        return false;
    }

    public function getCountReservados($id_boda)
    {
        $sql = 'SELECT count(*) AS total FROM proveedor INNER JOIN proveedor_boda USING(id_proveedor) WHERE id_boda=?';
        $t = $this->db->query($sql, [$id_boda])->row();
        if ($t) {
            return $t->total;
        }

        return 0;
    }


//////////////////datatable dinamico (Indicadores)////////////////////////
function make_query2()  
{  
    if(!empty($_POST["search"]["value"]))  
    {  
        if(strlen($_POST["search"]["value"]) < 4) {
            // return $sql = 'SELECT proveedor.* ,usuario.correo, usuario.usuario, usuario.activo ,fecha_creacion FROM proveedor INNER JOIN usuario USING(id_usuario) WHERE (usuario.correo LIKE \''.$_POST["search"]["value"].'%\' OR usuario.usuario LIKE \''.$_POST["search"]["value"].'%\' OR proveedor.nombre LIKE \''.$_POST["search"]["value"].'%\') ;';
            return $sql = 'SELECT nombre FROM proveedor INNER JOIN indicators USING(id_proveedor=provider_id) WHERE (proveedor.nombre LIKE \''.$_POST["search"]["value"].'%\') ;';
        } else {
            // return $sql = 'SELECT proveedor.* ,usuario.correo, usuario.usuario, usuario.activo ,fecha_creacion FROM proveedor INNER JOIN usuario USING(id_usuario) WHERE (usuario.correo LIKE \'%'.$_POST["search"]["value"].'%\' OR usuario.usuario LIKE \'%'.$_POST["search"]["value"].'%\' OR proveedor.nombre LIKE \'%'.$_POST["search"]["value"].'%\') ;';
            print_r(" ELSE ");
        }
    }  
    if((!empty($_POST['order']['0']['column']) || $_POST['order']['0']['column'] >= 0 ) && !empty($_POST['order']['0']['dir']))  
    {  
        // $order = 'proveedor.nombre';    
        // if($_POST['order']['0']['column'] == 0)
        //     $order = 'proveedor.nombre';
        // if($_POST['order']['0']['column'] == 1)
        //     $order = 'proveedor.tipo_cuenta';
        // if($_POST['order']['0']['column'] == 2)
        //     $order = 'usuario.activo';
        // if($_POST['order']['0']['column'] == 3)
        //     $order = 'usuario.usuario';
        // if($_POST['order']['0']['column'] == 4)
        //     $order = 'usuario.correo';
        // if($_POST['order']['0']['column'] == 5)
        //     $order = 'proveedor.contacto_celular';
        // if($_POST['order']['0']['column'] == 6)
        //     $order = 'proveedor.localizacion_estado';
        return $sql = 'SELECT id_proveedor, nombre FROM proveedor LIMIT '.$_POST['start'].','.$_POST['length'].';';
    }  
    else  
    {  
        return $sql = 'SELECT id_proveedor FROM proveedor LIMIT '.$_POST['start'].','.$_POST['length'].';';
    }  
}  
    function make_datatables2(){  
        $sql = $this->make_query2();
        if($_POST["length"] != -1)  
        {  
                $this->db->limit($_POST['length'], $_POST['start']);  
        }  
        $query = $this->db->query($sql);
        return $query->result();  
    }  
    function get_filtered_data2(){  
        $this->make_query();  
        $sql = 'SELECT id_proveedor FROM proveedor';
        $query = $this->db->query($sql);
        return $query->num_rows();  
    }       
    function get_all_data2()  
    {  
        $this->db->select("*");  
        $this->db->from($this->tabla);  
        return $this->db->count_all_results();  
    }
//////////////////datatable dinamico (Proveedores)////////////////////////
    function make_query()  
    {  
        if(!empty($_POST["search"]["value"]))  
        {  
            if(strlen($_POST["search"]["value"]) < 4)
                return $sql = 'SELECT proveedor.* ,usuario.correo, usuario.usuario, usuario.activo ,fecha_creacion FROM proveedor INNER JOIN usuario USING(id_usuario) WHERE (usuario.correo LIKE \''.$_POST["search"]["value"].'%\' OR usuario.usuario LIKE \''.$_POST["search"]["value"].'%\' OR proveedor.nombre LIKE \''.$_POST["search"]["value"].'%\') ;';
            else
                return $sql = 'SELECT proveedor.* ,usuario.correo, usuario.usuario, usuario.activo ,fecha_creacion FROM proveedor INNER JOIN usuario USING(id_usuario) WHERE (usuario.correo LIKE \'%'.$_POST["search"]["value"].'%\' OR usuario.usuario LIKE \'%'.$_POST["search"]["value"].'%\' OR proveedor.nombre LIKE \'%'.$_POST["search"]["value"].'%\') ;';
        }  
        if((!empty($_POST['order']['0']['column']) || $_POST['order']['0']['column'] >= 0 ) && !empty($_POST['order']['0']['dir']))  
        {  
            $order = 'proveedor.nombre';    
            if($_POST['order']['0']['column'] == 0)
                $order = 'proveedor.nombre';
            if($_POST['order']['0']['column'] == 1)
                $order = 'proveedor.tipo_cuenta';
            if($_POST['order']['0']['column'] == 2)
                $order = 'usuario.activo';
            if($_POST['order']['0']['column'] == 3)
                $order = 'usuario.usuario';
            if($_POST['order']['0']['column'] == 4)
                $order = 'usuario.correo';
            if($_POST['order']['0']['column'] == 5)
                $order = 'proveedor.contacto_celular';
            if($_POST['order']['0']['column'] == 6)
                $order = 'proveedor.localizacion_estado';
            return $sql = 'SELECT proveedor.* ,usuario.correo, usuario.usuario, usuario.activo ,fecha_creacion FROM proveedor INNER JOIN usuario USING(id_usuario) ORDER BY '.$order.' '.$_POST['order']['0']['dir'].' LIMIT '.$_POST['start'].','.$_POST['length'].';';
        }  
        else  
        {  
            return $sql = 'SELECT proveedor.* ,usuario.correo, usuario.usuario, usuario.activo ,fecha_creacion FROM proveedor INNER JOIN usuario USING(id_usuario) LIMIT '.$_POST['start'].','.$_POST['length'].';';
        }  
    }  
    function make_datatables(){  
        $sql = $this->make_query();  
        if($_POST["length"] != -1)  
        {  
                $this->db->limit($_POST['length'], $_POST['start']);  
        }  
        $query = $this->db->query($sql); 
        return $query->result();  
    }  
    function get_filtered_data(){  
        $this->make_query();  
        $sql = 'SELECT proveedor.* ,usuario.correo, usuario.usuario, usuario.activo ,fecha_creacion FROM proveedor INNER JOIN usuario USING(id_usuario);';
        $query = $this->db->query($sql);
        return $query->num_rows();  
    }       
    function get_all_data()  
    {  
        $this->db->select("*");  
        $this->db->from($this->tabla);  
        return $this->db->count_all_results();  
    }  





}
