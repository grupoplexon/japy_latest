<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tarea_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_tarea";
        $this->tabla = "tarea";
    }

    public function getFechaBoda($boda)
    {
        $sql = "SELECT fecha_boda from boda where id_boda=$boda";
        return $this->db->query($sql)->row()->fecha_boda;
    }

    public function getTotal($id_boda)
    {
        $sql = "SELECT COUNT(*) as total from tarea where id_boda=$id_boda";
        return $this->db->query($sql)->row()->total;
    }

    public function getTotalCompletadas($id_boda)
    {
        $sql = "SELECT COUNT(*) as total from tarea where id_boda=$id_boda and completada=1";
        return $this->db->query($sql)->row()->total;
    }

    public function getProximas($id_boda, $total = 5)
    {
        $sql = "SELECT * from tarea where id_boda=$id_boda and completada=0 order by tiempo asc  limit $total ";
        return $this->db->query($sql)->result();
    }

    public function getTareasConFecha($arregloTareas)
    {
        $arregloTareas = implode(',', $arregloTareas);
        $sql = "SELECT id_tarea AS 'task_id', nombre AS 'title', fecha_asignada AS 'date' FROM tarea WHERE id_tarea IN ($arregloTareas) AND fecha_asignada IS NOT NULL";
        return $this->db->query($sql)->result();
    }

    public function updateFechaTarea($id, $fecha)
    {
        try {
            $this->db->where([$this->id_tabla => $id]);
            $this->db->set('fecha_asignada', $fecha);
            return $this->db->update($this->tabla);
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateTarea($id, $id_boda, $data)
    {
        try {
            $this->db->where([$this->id_tabla => $id, "id_boda" => $id_boda]);
            return $this->db->update($this->tabla, $data);
        } catch (Exception $e) {
            return false;
        }
    }

}
