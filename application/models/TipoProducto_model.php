<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TipoProducto_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->tabla = "tipo_producto";
        $this->id_tabla = "id_tipo_producto";
    }

    public function getAll($columns = "*", $limit1 = Null, $limit2 = NULL) {
        $result = parent::getAll($columns, $limit1, $limit2);
        foreach ($result as $key => &$value) {
            $value->sortable = explode(",", $value->sortable);
            $value->tipo_proveedores = explode(",", $value->tipo_proveedores);
        }
        return $result;
    }

    public function existsProducto($p, $productos) {
        $pro = str_replace("-", " ", $p);
        foreach ($productos as $key => $producto) {
            if (strtolower($pro) == strtolower($producto->nombre)) {
                foreach ($producto->sortable as $key => &$value) {
                    $nom = $value;
                    $value = new stdClass();
                    $value->nombre = $nom;
                    $value->valores = $this->getValoresSorteable($producto->id_tipo_producto, strtoupper($nom));
                }
                return $producto;
            }
        }
        return FALSE;
    }

    public function getTipos($id_proveedor) {
        $sql = "SELECT * FROM $this->tabla where tipo_proveedores like '%$id_proveedor%' ";
        $res = $this->db->query($sql)->result();
        foreach ($res as $key => &$tipo) {
            $tipo->sortable = explode(",", $tipo->sortable);
            foreach ($tipo->sortable as $key => &$value) {
                $var = $value;
                $value = new stdClass();
                $value->nombre = $var;
                $value->valores = $this->getValoresSorteable($tipo->id_tipo_producto, strtoupper($var));
            }
        }
        return $res;
    }

    public function getValores($tipo_producto, $prop, $search) {
        if (strtolower($prop) == "temporada") {
            $query = "SELECT temporada as valor from producto where tipo_producto=$tipo_producto and temporada like '%$search%' group by temporada";
        } else if (strtolower($prop) == "disenador" || strtolower($prop) == "diseñador") {
            $query = "SELECT disenador as valor from producto where tipo_producto=$tipo_producto and disenador like '%$search%'  group by disenador";
        }
        return $this->db->query($query)->result();
    }

    public function getValoresSorteable($id_tipo_producto, $grupo) {
        $sql = "select nombre,imagen,grupo from prop_producto inner join tipo_producto_propiedad using(id_prop_producto) where id_tipo_producto=$id_tipo_producto and grupo='$grupo';";
        return $this->db->query($sql)->result();
    }

}
