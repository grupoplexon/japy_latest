<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tipo_proveedor_model
 *
 * @author Evolutel
 */
class Tipo_proveedor_model extends MY_Model {

    //put your code here

    public function __construct() {
        parent::__construct();
        $this->tabla = "tipo_proveedor";
        $this->id_tabla = "id_tipo_proveedor";
    }

    public function getGrupos() {
        $sql = "SELECT
                        grupo as nombre,
                   count(id_proveedor) as total
                FROM
                        tipo_proveedor
                LEFT JOIN proveedor USING (id_tipo_proveedor)
                where  proveedor.acceso=1  or proveedor.acceso is null
                GROUP BY
                        grupo  order by nombre asc";
        return $this->db->query($sql)->result();
    }

    public function busquedaDondeEstados($busqueda) {
        $sql = "SELECT
                       *
                FROM
                        estado
               where estado like '$busqueda%' and id_pais = 142";
        return $this->db->query($sql)->result();
    }

    public function busquedaDondeCiudades($busqueda) {
        $sql = "SELECT * FROM ciudad
JOIN estado ON estado.id_estado = ciudad.id_estado
WHERE ciudad.ciudad LIKE '%$busqueda%' AND estado.id_pais = 142 LIMIT 20";
        return $this->db->query($sql)->result();
    }

    public function busquedaQueGrupos($busqueda) {
        $sql = "SELECT tipo_proveedor.grupo FROM tipo_proveedor
WHERE tipo_proveedor.grupo LIKE '%$busqueda%' GROUP BY grupo LIMIT 25;";
        return $this->db->query($sql)->result();
    }

    public function busquedaQueCategorias($busqueda, $limit) {
        $limit = 25 - $limit;
        $sql = "SELECT tipo_proveedor.nombre_tipo_proveedor FROM tipo_proveedor
WHERE tipo_proveedor.nombre_tipo_proveedor LIKE '%$busqueda%' LIMIT $limit;";
        return $this->db->query($sql)->result();
    }

    public function busquedaQueProveedores($busqueda, $limit) {
        //echo '<script>console.log('.$limit.')</script>';
        $limit = 25 - $limit;
        $sql = "SELECT proveedor.*, tipo_proveedor.nombre_tipo_proveedor FROM proveedor
				JOIN tipo_proveedor ON tipo_proveedor.id_tipo_proveedor = proveedor.id_tipo_proveedor
				WHERE proveedor.nombre LIKE '%$busqueda%' and proveedor.acceso=1 LIMIT $limit;";
        return $this->db->query($sql)->result();
    }

    public function busquedaRelacionadas($busqueda) {
        $sql = "SELECT tipo_proveedor.nombre_tipo_proveedor FROM tipo_proveedor_relaciones
JOIN tipo_proveedor ON tipo_proveedor.id_tipo_proveedor = tipo_proveedor_relaciones.id_tipo_proveedor
WHERE tipo_proveedor_relaciones.nombre = '$busqueda'";
        if ($this->db->query($sql)->num_rows() > 0) {
            $result = $this->db->query($sql)->result();
            return $result[0]->nombre_tipo_proveedor;
        } else {
            return $busqueda;
        }
    }

    public function getEstados() {
        $sql = "SELECT
                        *
                   from estado where id_pais=142";
        return $this->db->query($sql)->result();
    }

    public function getGrupo($grupo) {
        $sql = "SELECT
                    $this->tabla.*,
                count(id_proveedor) as total
                FROM
                    $this->tabla
                LEFT JOIN proveedor USING (id_tipo_proveedor)
                WHERE
                    tipo_proveedor.grupo = '" . strtoupper($grupo) . "'
                GROUP BY id_tipo_proveedor; ";
        return $this->db->query($sql)->result();
    }



}
