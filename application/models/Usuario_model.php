<?php

class Usuario_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
        $this->load->model("Boda_model");
        $this->load->model("Cliente_model");
        $this->load->model("Invitados/Invitado_model", "Invitado_model");
        $this->load->library("checker");
        $this->load->library("My_PHPMailer");
        $this->tabla    = "usuario";
        $this->id_tabla = "id_usuario";
    }

    public function getCliente($id_usuario)
    {
        try {
            $sql    = "SELECT cliente.* FROM cliente WHERE id_usuario = $id_usuario";
            $result = $this->db->query($sql);

            return $result->row();
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        return false;
    }

    function getClienteFB($id_fb, $correo)
    {
        $sql    = "SELECT cliente.* FROM cliente inner join usuario using(id_usuario) WHERE id_facebook = $id_fb or correo='$correo'; ";
        $result = $this->db->query($sql);

        return $result->row();
    }
    function getClienteFacebook($id_fb)
    {
        $sql    = "SELECT cliente.* FROM cliente WHERE id_facebook = $id_fb ; ";
        $result = $this->db->query($sql);

        return $result->row();
    }
    function insertUsuario($data)
    {
        $this->db->trans_start();
        $this->db->insert('usuario', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();

        return $id;
    }
    function existeCorreo($correo)
    {
        return $this->get(array("usuario" => $correo)) ? true : false;
    }
    public function getFromCorreo($correo)
    {
        $sql = "Select * from usuario where usuario='$correo'";
        $u   = $this->db->query($sql)->row();
        if ($u) {
            return $u;
        } else {
            $sql = "Select * from usuario where correo='$correo'";
            $u   = $this->db->query($sql)->row();
            if ($u) {
                return $u;
            }
        }
    }
    public function getUsuarios_nonovio_noproveedor()
    {
        try {
            $sql    = "SELECT * FROM usuario WHERE rol != 2 AND rol != 3 AND activo = 1";
            $result = $this->db->query($sql);

            return $result->result();
        } catch (Exception $e) {
            return false;
        }
    }
    public function register($data)
    {
        // INSERT USUARIO
        $id_usuario = User::create([
            "usuario"    => $data["email"],
            "correo"     => $data["email"],
            "contrasena" => sha1(md5(sha1($data["password"]))),
            "rol"        => 2,
            "activo"     => 1,
            "nombre"     => $data["name"],
            "apellido"   => "XXX",
            "registered_from"   =>  $data["estado"],
        ])->id_usuario;

        // INSERT BODA
        $weddingData = [
            "fecha_boda"     => date("Y-m-d H:i:s"),
            "fecha_creacion" => date("Y-m-d H:i:s"),
            "presupuesto"    => 0,
            "no_invitado"    => 0,
        ];

        $id_boda = $this->Boda_model->insertBoda($weddingData);

        //Create profile permissions
        $id_permisos = Profile::create([
            "enviar_mensaje"        => 1,
            "participacion_debates" => 1,
            "valorar_publicaciones" => 1,
            "anadir_amigo"          => 1,
            "aceptar_solicitud"     => 1,
            "mension_posts"         => 1,
            "email_diario"          => 1,
            "email_semanal"         => 1,
            "invitaciones"          => 1,
            "concursos"             => 1,
            "visibilidad_todos"     => 1,
            "email_debate"          => 1,
        ])->id_permisos;

        // INSERT CLIENTE
        $clientData = [
            "genero"      => $data["gender"],
            "pais"        => null,
            "estado"      => null,
            "poblacion"   => null,
            "id_permisos" => $id_permisos,
            "id_boda"     => $id_boda,
            "id_usuario"  => $id_usuario,
        ];

        $id_cliente = $this->Cliente_model->insertCliente($clientData);

        $id_mesa = Table::create([
            "tipo"        => 3,
            "nombre"      => "PRINCIPAL",
            "sillas"      => 4,
            "x"           => 425,
            "y"           => 75,
            "orientacion" => 90,
            "id_boda"     => $id_boda,
        ])->id_mesa;

        $id_grupo = Group::create([
            "grupo"   => "Novios",
            "id_boda" => $id_boda,
        ])->id_grupo;

        $id_menu = Menu::create([
            "id_boda"     => $id_boda,
            "nombre"      => "Adultos",
            "descripcion" => "Menu para adultos",
        ])->id_menu;

        //SETUP DEFAULT
        Group::insert([
            ["grupo" => "Familia", "id_boda" => $id_boda],
            ["grupo" => "Amigos", "id_boda" => $id_boda],
        ]);

        Menu::create([
            "id_boda"     => $id_boda,
            "nombre"      => "Ni単os",
            "descripcion" => "Menu para ni単os",
        ]);

        $baseChores = Chore::whereNull("id_boda")->get();

        foreach ($baseChores as $chore) {
            $newChore          = $chore->replicate();
            $newChore->id_boda = $id_boda;
            $newChore->save();
        }

        //INSERT USER AND PARTNER IN GUESTS
        $userData = [
            "nombre"     => $data["name"],
            "apellido"   => "XXX",
            "sexo"       => $data["gender"],
            "edad"       => 1,
            "correo"     => $data["email"],
            "confirmado" => 2,
            "id_grupo"   => $id_grupo,
            "id_menu"    => $id_menu,
            "id_mesa"    => $id_mesa,
            "silla"      => 2,
            "id_boda"    => $id_boda,
        ];

        $this->Invitado_model->insertInvitado($userData);

        $partnerData = [
            "nombre"     => "XXX",
            "apellido"   => "XXX",
            "sexo"       => 2,
            "edad"       => 1,
            "confirmado" => 1,
            "id_grupo"   => $id_grupo,
            "id_menu"    => $id_menu,
            "id_mesa"    => $id_mesa,
            "silla"      => 3,
            "id_boda"    => $id_boda,
        ];

        $this->Invitado_model->insertInvitado($partnerData);

        $data = [
            "usuario"    => $id_usuario,
            "nombre"     => $data["name"],
            "email"      => $data["email"],
            "rol"        => 2,
            "confirmado" => 1,
            "encryption" => generarCadena(),
            "id_boda"    => $id_boda,
            "id_cliente" => $id_cliente,
            "id_usuario" => $id_usuario,
            "genero"     => ($data["gender"] == 1 ? "Novio" : "Novia"),
        ];

        $this->session->set_userdata($data);

        $this->mailer->to($data["email"], $data["nombre"]);

        $template = $this->load->view("templates/email_usuario_nuevo", [
            "correo" => $data["email"],
            "nombre" => $data["nombre"],
        ], true);

        $this->mailer->send($template);

        $dataFb['id_usuario'] = $id_usuario;
        $dataFb['name'] = $data['name'];
        $dataFb['email'] = $data['email'];
        $dataFb['phone'] = '';
        $dataFb['isBride'] = 0;
        $dataFb['weddingDate'] = date("Y-m-d");
        $dataFb['id_boda'] = $id_boda;

        return $dataFb;
    }

}
