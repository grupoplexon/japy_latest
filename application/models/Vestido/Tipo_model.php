<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tipo_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->tabla    = 'tipo_vestido';
        $this->id_tabla = 'id_tipo_vestido';
    }

    public function getValues($grupo, $id_tipo_vestido)
    {
        $sql = "SELECT * FROM descriptor_tipo_vestido inner join descriptor_vestido using(id_descriptor_vestido) where grupo='$grupo' and id_tipo_vestido=$id_tipo_vestido";

        return $this->db->query($sql)->result();
    }

    public function countArticulos()
    {
        $sql
            = "SELECT
                    tipo_vestido.*, sum(
                            CASE
                            WHEN id_vestido IS NULL THEN
                                    0
                            ELSE
                                    1
                            END
                    ) as total
            FROM
                    tipo_vestido
            LEFT JOIN vestido ON (tipo_vestido = id_tipo_vestido)
            GROUP BY
                    id_tipo_vestido;";

        return $this->db->query($sql)->result();
    }

    public function existsProducto($p, $productos)
    {
        $pro = str_replace("-", " ", $p);
        foreach ($productos as $key => $producto) {
            if (strtolower($pro) == strtolower($producto->nombre)) {
                $producto->sortable = explode(",", $producto->sortable);
                foreach ($producto->sortable as $key => &$value) {
                    $nom            = $value;
                    $value          = new stdClass();
                    $value->nombre  = $nom;
                    $value->valores = $this->getValues(strtoupper($nom), $producto->id_tipo_vestido);
                }

                return $producto;
            }
        }

        return false;
    }

}
