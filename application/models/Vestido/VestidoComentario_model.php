<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class VestidoComentario_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->id_tabla = "id_vestido_comentario";
        $this->tabla = "vestido_comentario";
    }

    public function getComentariosArticulosInit($id_articulo) {
        $sql = "SELECT c.*,sum( case when t.id_vestido_comentario is null then 0 else 1 end) as comentarios"
                . " FROM $this->tabla as c left join $this->tabla as t on(c.id_vestido_comentario=t.parent and c.id_vestido=$id_articulo and  c.parent is null  ) "
                . " where c.parent is null and  c.id_vestido=$id_articulo group by c.id_vestido_comentario "
                . " order by c.fecha_creacion desc ";
        return $this->db->query($sql)->result();
    }

    public function getUsuario($id_usuario) {
        $sql = "SELECT id_usuario,nombre,apellido,correo,conectado FROM usuario where id_usuario=? ";
        return $this->db->query($sql,array($id_usuario))->row();
    }

}
