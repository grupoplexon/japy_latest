<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Vestido_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->id_tabla = "id_vestido";
        $this->tabla    = "vestido";
        ini_set('memory_limit', '-1');
    }

    public function getDisenadores($tipo_producto)
    {
        $query = "SELECT disenador,count(id_vestido) AS total FROM  vestido  "
            ." WHERE tipo_vestido=? GROUP BY disenador";

        return $this->db->query($query, array($tipo_producto))->result();
    }

    public function getTemporadas($tipo_producto)
    {
        $query = "SELECT temporada,count(id_vestido) AS total FROM  vestido  "
            ." WHERE tipo_vestido=? GROUP BY temporada";

        return $this->db->query($query, array($tipo_producto))->result();
    }

    public function getCatalogo($tipo_producto, $param = array(), $pag = 1)
    {
        $where = "";
        foreach ($param as $key => $value) {
            if ($key == "disenador" || $key == "diseñador") {
                $where .= " disenador='".str_replace("-", " ", $value)."' and ";
            } else {
                $where .= " $key='$value' and ";
            }
        }
        $where .= " tipo_vestido=$tipo_producto ";
        $limit = ($pag == 1 ? " limit 15" : "limit ".(($pag - 1) * 15)."15");
        $query = "SELECT vestido.* FROM  vestido  "." where $where;";
        return $this->db->query($query, array($tipo_producto))->result();
    }

    public function getVestido($id_vestido)
    {
        $sql = "SELECT * FROM vestido where id_vestido = $id_vestido";
        $r   = $this->db->query($sql)->row();

        return $r;
    }


    public function getTotalCatalogo($tipo_producto, $param = array())
    {
        $where = "";
        foreach ($param as $key => $value) {
            if ($key == "disenador" || $key == "diseñador") {
                $where .= " disenador='".str_replace("-", " ", $value)."' and ";
            } else {
                $where .= " $key='$value' and ";
            }
        }
        $where .= " tipo_vestido=$tipo_producto ";
        $query = "SELECT count(*) as total FROM  vestido   where $where  ;";
        $r     = $this->db->query($query, array($tipo_producto))->row();
        if ($r) {
            return $r->total;
        }

        return 0;
    }

    public function getDestacadosMin($tipo_producto, $total = 3)
    {
        $limit = "limit $total";
        $sql
               = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)   
                WHERE
                        tipo_vestido = $tipo_producto
                group by disenador
                order by likes desc,comentarios desc,temporada desc
                $limit ;";

        return 0;
        return $this->db->query($sql)->result();
    }

    public function getDestacados($tipo_producto, $pag = 1)
    {
        $limit = ($pag == 1 ? " limit 15" : "limit ".(($pag - 1) * 15)."15");
        $sql
               = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)
                WHERE
                        tipo_vestido = $tipo_producto
                group by id_vestido
                order by likes desc,comentarios desc,temporada desc
                $limit ;";

        return $this->db->query($sql)->result();
    }

    public function getDisenadoresDestacados($tipo_producto, $pag = 1)
    {
        $limit = ($pag == 1 ? " limit 15" : "limit ".(($pag - 1) * 15)."15");
        $sql
               = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)
                WHERE
                        tipo_vestido = $tipo_producto
                group by disenador
                order by likes desc,comentarios desc,temporada desc
                $limit ;";

        return $this->db->query($sql)->result();
    }

    public function getDisenadoresAll($tipo_producto, $pag = 1)
    {
        $limit = ($pag == 1 ? " limit 15" : "limit ".(($pag - 1) * 15)."15");
        $sql
               = "SELECT
                    vestido.*,
                    sum(case when id_vestido_usuario is null then 0 else 1 end ) as likes,
                    sum(case when id_vestido_comentario is null then 0 else 1 end ) as comentarios
                FROM
                        vestido
                        LEFT JOIN vestido_usuario USING (id_vestido)
                        LEFT JOIN vestido_comentario USING (id_vestido)
                WHERE
                        tipo_vestido = $tipo_producto
                group by disenador
                order by likes desc,comentarios desc,temporada desc
                $limit ;";

        return $this->db->query($sql)->result();
    }

    public function getAllDisenador($nombre, $pag = 1)
    {
        $limit = ($pag == 1 ? " limit 15" : "limit ".(($pag - 1) * 15)."15");
        $sql   = "SELECT * From vestido where disenador=?  order by nombre asc $limit";

        return $this->db->query($sql, array($nombre))->result();
    }

    public function getTotalAllDisenador($nombre, $pag = 1)
    {
        $sql = "SELECT count(*) AS total FROM vestido WHERE disenador=? ";
        $r   = $this->db->query($sql, array($nombre))->row();
        if ($r) {
            return $r->total;
        }

        return 0;
    }

    public function getTemporadasDisenador($nombre)
    {
        $sql = "SELECT temporada,count(id_vestido) AS total FROM vestido WHERE disenador=? GROUP BY temporada";

        return $this->db->query($sql, array($nombre))->result();
    }

    public function existsDisenador($nombre_disenador)
    {
        $nombre = urldecode($nombre_disenador);
        $nombre = str_replace("-", " ", $nombre);
        $sql    = "SELECT disenador FROM vestido WHERE disenador =? LIMIT 1 ";

        return $this->db->query($sql, array($nombre))->row() ? true : false;
    }

    public function countDisenador($nombre_disenador)
    {
        $sql = "SELECT count(id_vestido) AS total FROM vestido WHERE disenador =? GROUP BY disenador ";
        $r   = $this->db->query($sql, array($nombre_disenador))->row();

        return $r ? $r->total : 0;
    }

    public function countComentarios($id_vestido)
    {
        $sql
            = "SELECT count(id_vestido) as total FROM vestido inner join vestido_comentario using(id_vestido)
                where id_vestido=$id_vestido
                 group by id_vestido";
        $r  = $this->db->query($sql)->row();
        if ($r) {
            return $r->total;
        }

        return 0;
    }

    public function countLikes($id_vestido)
    {
        $sql
            = "SELECT count(id_vestido) as total FROM vestido inner join vestido_usuario using(id_vestido)
                    where id_vestido=$id_vestido
                     group by id_vestido";
        $r  = $this->db->query($sql)->row();
        if ($r) {
            return $r->total;
        }

        return 0;
    }

    public function isLikeForMe($id_cliente, $id_vestido)
    {
        $sql = "SELECT * FROM  vestido_usuario WHERE id_vestido=? AND id_cliente=?;";
        $l   = $this->db->query($sql, array($id_vestido, $id_cliente))->row();
        if ($l) {
            return true;
        }

        return false;
    }

    public function misVestidoGroup($id_cliente)
    {
        $sql
            = "SELECT
                        tipo_vestido.*, count(id_tipo_vestido) AS total
                FROM
                        vestido
                INNER JOIN vestido_usuario USING (id_vestido)
                INNER JOIN tipo_vestido ON (id_tipo_vestido = tipo_vestido)
                WHERE
                        id_cliente = $id_cliente
                GROUP BY
                        tipo_vestido";

        return $this->db->query($sql)->result();
    }

    public function misVestidos($id_cliente, $id_tipo = null, $l = null)
    {
        $limit = "";
        if ($l) {
            $limit = " limit 0,$l";
        }
        if ($id_tipo) {
            $sql = "SELECT * FROM $this->tabla inner join vestido_usuario using(id_vestido) where id_cliente=$id_cliente and tipo_vestido=$id_tipo $limit";
        } else {
            $sql = "SELECT * FROM $this->tabla inner join vestido_usuario using(id_vestido) where id_cliente=$id_cliente $limit";
        }

        return $this->db->query($sql)->result();
    }
///////////////////////////////////////////////////////////////////////////////////////////////////
    function make_query()  
    {  
        if(!empty($_POST["search"]["value"]))  
        {  
            if(strlen($_POST["search"]["value"]) < 4)
                return $sql = 'SELECT * FROM vestido WHERE (vestido.nombre LIKE \''.$_POST["search"]["value"].'%\' OR vestido.disenador LIKE \''.$_POST["search"]["value"].'%\') ;';
            else
                return $sql = 'SELECT * FROM vestido WHERE (vestido.nombre LIKE \'%'.$_POST["search"]["value"].'%\' OR vestido.disenador LIKE \'%'.$_POST["search"]["value"].'%\') ;';
        }  
        if(!empty($_POST['order']) && (!empty($_POST['order']['0']['column']) || $_POST['order']['0']['column'] >= 0 ) && !empty($_POST['order']['0']['dir']))  
        {  
            $order = 'vestido.nombre';    
            if($_POST['order']['0']['column'] == 0)
                $order = 'vestido.id_vestido';
            if($_POST['order']['0']['column'] == 1)
                $order = 'vestido.nombre';
            if($_POST['order']['0']['column'] == 2)
                $order = 'vestido.disenador';
            if($_POST['order']['0']['column'] == 3)
                $order = 'vestido.estilo';
            if($_POST['order']['0']['column'] == 4)
                $order = 'vestido.temporada';
            return $sql = 'SELECT * FROM vestido ORDER BY '.$order.' '.$_POST['order']['0']['dir'].' LIMIT '.$_POST['start'].','.$_POST['length'].';';
        }  
        else  
        {  
            return $sql = 'SELECT * FROM vestido LIMIT '.$_POST['start'].','.$_POST['length'].';';
        }  
    } 

    function make_datatables(){  
        $sql = $this->make_query();  
        if($_POST["length"] != -1)  
        {  
                $this->db->limit($_POST['length'], $_POST['start']);  
        }  
        $query = $this->db->query($sql); 
        return $query->result();  
    }  

    function get_all_data()  
    {  
        $this->db->select("*");  
        $this->db->from($this->tabla);  
        return $this->db->count_all_results();  
    }  

    function get_filtered_data(){  
        $this->make_query();  
        $sql = 'SELECT id_vestido,nombre FROM vestido ;';
        $query = $this->db->query($sql);
        return $query->num_rows();  
    }  

}
