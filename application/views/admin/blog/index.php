<?php
$data['extraCSS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
    base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
    base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
                <li class="breadcrumb-item active">Artículos</li>
            </ol>
            <!-- end breadcrumb -->
            <a href="<?php echo base_url()."admin/blog/create" ?>" class="btn btn-success m-3"><i class="fa fa-plus"></i> Crear</a>
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Artículos</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered" id="articleTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Estado</th>
                            <th>Titulo</th>
                            <th>Creado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($posts as $post) : ?>
                            <tr>
                                <td><?php echo $post->id_blog_post ?></td>
                                <td><?php echo ($post->publicado == 1) ? "Publicado" : "Borrador" ?></td>
                                <td><?php echo $post->titulo ?></td>
                                <td><?php echo $post->fecha_creacion ?></td>
                                <td class="with-btn" nowrap="">
                                    <a href="<?php echo base_url()."admin/blog/edit/".$post->id_blog_post ?>"
                                       class="btn btn-sm btn-primary width-60 m-r-2">Editar</a>
                                    <a href="#"
                                       onClick='eliminar(<?php echo($post->id_blog_post) ?>, "<?php echo($post->titulo) ?>");'
                                       class="btn btn-sm btn-danger width-70">Eliminar</a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <!-- end panel-body -->
            </div>
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->
    <?php $this->view("admin/components/page-js", $data) ?>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
    <script>
        const server = "<?php echo base_url() ?>";
        $(document).ready(function() {
            $('#articleTable').DataTable({
                responsive: true,
                columnDefs: [
                    {
                        'targets': 4,
                        'orderable': false,
                    }],
                language: {
                    'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json',
                },
            });
            // href="<?php //echo $this->config->base_url() ?>/blog/post/eliminar/<?php //echo $post->id_blog_post ?>"
        });

        function eliminar(idPost, titulo) {
            // console.log(idPost,titulo);
            swal({
                title: 'Estas a punto de eliminar el artículo: ' + titulo,
                text: 'Esta acción borrara toda la información relacionada con la entrada',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    // eliminar(idPost);
                    $.ajax({
                        url: server + 'admin/Blog/destroy',
                        data: {id: idPost},
                        method: 'POST',
                        success: function(resp) {
                            swal('El artículo: ' + titulo + ' se elimino correctamente', {
                                icon: 'success',
                            });
                            window.location.reload(true);
                        },
                        error: function() {
                            swal('Oops!', 'Ocurrio un error intentelo más tarde', 'error');
                        },
                    });
                }
                else {
                    swal('El articulo no será eliminado!');
                    window.location.reload(true);
                }
            });
        }
    </script>
</body>