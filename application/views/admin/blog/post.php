<?php
if ( ! isset($id_post)) {
    $id_post = 0;
}

function editable($id_post)
{
    if ($id_post > 0) {
        if (is_numeric($id_post)) {
            return true;
        }
    }

    return false;
}

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
        <title>Japy</title>
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/switchery/switchery.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>dist/admin/assets/css/custom/blog/style.css" , rel="stylesheet">
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/src/parsley.css" , rel="stylesheet">
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/animate/animate.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>dist/admin/assets/css/default/style.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>dist/admin/assets/css/default/style-responsive.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>dist/admin/assets/css/default/theme/default.css" rel="stylesheet" id="theme"/>
        <!-- ================== END BASE CSS STYLE ================== -->

        <!-- ================== BEGIN PAGE CSS ================== -->
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/summernote/summernote.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>dist/admin/assets/plugins/dropzone/min/dropzone.min.css" rel="stylesheet"/>
        <!-- ================== END PAGE CSS ================== -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/pace/pace.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/switchery/switchery.min.js"></script>
        <!-- ================== END BASE JS ================== -->
    </head>
    <body>
        <!-- begin #page-loader -->
        <div id="page-loader" class="fade show"><span class="spinner"></span></div>
        <!-- end #page-loader -->

        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a> / <a href="<?php echo base_url()."admin/blog" ?>">Artículos</a></li>
                <li class="breadcrumb-item active"><?php echo(($id_post == 0) ? "Nueva Entrada" : "Editar Entrada") ?></li>
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header"><?php echo(($id_post == 0) ? "Nueva Entrada" : "Editar Entrada") ?></h1>
            <!-- end page-header -->

            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <div class="col-md-12">
                    <!-- begin panel -->
                    <div class="panel panel-inverse m-b-0">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title"> <?php echo(($id_post == 0) ? "Nueva Entrada" : "Editar Entrada") ?></h4>
                        </div>
                        <div class="panel-body panel-form p-0">
                            <form class="form-horizontal form-bordered" data-parsley-validate="true" novalidate>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2" style="margin:auto;">Imagen principal</label>
                                    <div class="col-md-10">
                                        <div id="dropzone">
                                            <div><p>Arrastre una imagen o de click para seleccionar una</p></div>
                                            <input type="file" accept="image/png, image/jpeg"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Titulo</label>
                                    <div class="col-md-10">
                                        <h2 class="editable mce-content-body" id="mce_0" contenteditable="true" style="position: relative;" spellcheck="false"> <?php echo ($id_post == 0) ? "Titulo" : $post->titulo ?> </h2>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Tipo de Articulo</label>
                                    <div class="col-md-10 ">
                                        <select class="form-control" name="tipo">
                                            <option value="0"  disabled <?php if(($id_post == 0) || ($post->tipo == null) ) echo ("selected") ?> >Elige una opción</option>
                                        <?php foreach ($category as $cate) : ?>
                                            <option value="<?php echo $cate->id_categoria ?>" <?php if(($id_post != 0) && $post->tipo == $cate->id_categoria) echo ("selected") ?>> <?php echo $cate->name_category ?></option>
                                       
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Estado</label>
                                    <div class="col-md-10">
                                        <input id="status" type="checkbox" class="js-switch"
                                            <?php if (editable($id_post)) {
                                                echo ($post->publicado == 1) ? "checked" : "";
                                            } ?>/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <textarea class="summernote" name="content"><?php echo ($id_post == 0) ? "" : $post->contenido ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-2">Tags</label>
                                    <div class="col-md-10">
                                        <select class="tag-selector" multiple="multiple"></select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-success float-right" value="Guardar">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
        </div>
        <!-- end page container -->
        <?php if (editable($id_post)) { ?>
            <input type="hidden" id="idImagenDestacada" value="<?php echo($post->id_imagen_destacada) ?>">
            <span id="id_post" class="hide"><?php echo $post->id_blog_post ?></span>
        <?php } ?>

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/js-cookie/js.cookie.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/js/theme/default.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/js/apps.min.js"></script>

        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/parsley.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/i18n/es.js"></script>
        <!-- ================== END BASE JS ================== -->

        <!-- ================== BEGIN PAGE LEVEL JS ================== -->
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/dropzone/min/dropzone.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/highlight/highlight.common.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/js/demo/render.highlight.js"></script>
        <!-- ================== END PAGE LEVEL JS ================== -->

        <!-- ================== BEGIN PAGE LEVEL JS ================== -->
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/summernote/summernote.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/js/demo/form-summernote.demo.min.js"></script>
        <!-- ================== END PAGE LEVEL JS ================== -->

        <!-- ================== CKEditor ================== -->
        <!-- <script src="<?php //echo base_url() ?>dist/admin/assets/plugins/ckeditor/ckeditor.js"></script> -->
        <!-- ================== end js CKEditor ================== -->
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/i18n/es.js" type="text/javascript"></script>

        <script>
            const $tagSelector = $('.tag-selector');
            const $drop = $('#dropzone');
            const server = "<?php echo base_url() ?>";
            const id_blog = parseInt("<?php echo($id_post); ?>");
            let statusSwitch = document.querySelector('.js-switch');
            var imgP = null;
            var status = statusSwitch.checked ? 1 : 0;

            $(document).ready(function() {
                const $profileImage = $('.fa.fa-upload.fa-3x');

                if (id_blog != 0) {
                    const imgDestacada = $('#idImagenDestacada').val();
                    $img = $('<img />').attr({'id': 'img-destacada', 'src': server + 'blog/resource/imagen/' + imgDestacada, 'class': 'responsive-img', 'data-id': imgDestacada}).fadeIn();
                    $('#dropzone div').html($img);
                }

                App.init();
                FormSummernote.init();
                Highlight.init();

                $('.note-editable').height(600);

                //Switchery
                new Switchery(statusSwitch, {
                    color: '#64bd63'
                    , secondaryColor: '#df2e3b'
                    , jackColor: '#fff',
                });
                statusSwitch.onchange = function() {
                    status = statusSwitch.checked ? 1 : 0;
                };
                //End Switchery

                $('form').on('submit', (e) => {
                    e.preventDefault();
                    onSend();
                });
                $tagSelector.select2({
                    ajax: {
                        url: server + 'blog/Post',
                    },
                });

                if (id_blog != 0) {
                    $.ajax({
                        type: 'GET',
                        url: server + 'blog/Post/getTagsById',
                        data: {
                            'id': id_blog,
                        },
                    }).then(function(data) {
                        // create the option and append to Select2
                        const tags = JSON.parse(data);

                        tags.data.forEach(function(tag) {
                            const option = new Option(tag.title, tag.id, true, true);
                            $tagSelector.append(option);
                        });
                        // manually trigger the `select2:select` event
                        $tagSelector.trigger({
                            type: 'select2:select',
                            params: {
                                data: data,
                            },
                        });
                    });
                }

                $tagSelector.select2({
                    tags: true,
                    width: '100%',
                    placeholder: 'Seleccione una etiqueta',
                    allowClear: true,
                    language: 'es',
                    minimumInputLength: 2,
                    ajax: {
                        url: server + 'blog/tag/show',
                        dataType: 'json',
                        type: 'GET',
                        quietMillis: 100,
                        data: function(term) {
                            return {
                                term: term,
                            };
                        },
                        processResults: function(data) {
                            return {
                                results: data.map(function(item) {
                                    return {
                                        text: item.title,
                                        id: item.id,
                                    };
                                }),
                            };
                        },
                    },
                });

                $tagSelector.on('change.select2', function(e) {
                    let data = {};

                    data.tags = $tagSelector.select2('data').map(function(item) {
                        return item.text;
                    });

                    // ESTAS LINEAS GUARDAN UNA NUEVA ETIQUETA SI ESTA NO EXISTIERA
                    $.ajax({
                        url: server + 'blog/tag/store',
                        data: data,
                        method: 'POST',
                        success: function(resp) {
                            $('.tag-selector').html('');
                            resp.forEach(function(item) {
                                let newOption = new Option(item.title, item.id, true, true);
                                $tagSelector.append(newOption);
                            });
                        },
                        error: function() {
                            $('.tag-selector').html('');
                        },
                    });

                });

            });

            $(function() {
                $('#dropzone').on('dragover', function() {
                    $(this).addClass('hover');
                });

                $('#dropzone').on('dragleave', function() {
                    $(this).removeClass('hover');
                });

                $('#dropzone input').on('change', function(e) {
                    var file = this.files[0];

                    $('#dropzone').removeClass('hover');

                    if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
                        return swal('Oops!', 'El archivo que intento subir no es compatible', 'error');
                    }

                    $('#dropzone').addClass('dropped');
                    $('#dropzone img').remove();
                    $('#dropzone div').html('<p>Cargando...</p>');

                    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
                        var reader = new FileReader(file);

                        reader.readAsDataURL(file);

                        reader.onload = function(e) {
                            var data = e.target.result;
                            imgP = data;
                            $.ajax({
                                url: server + 'blog/resource/subir/imagen',
                                method: 'POST',
                                data: {
                                    archivo: imgP,
                                },
                                success: function(res, data) {
                                    if (res.success) {
                                        $img = $('<img />').attr({'id': 'img-destacada', 'src': server + 'blog/resource/imagen/' + res.id, 'class': 'responsive-img', 'data-id': res.id}).fadeIn();
                                        $('#dropzone div').html($img);
                                    }
                                },
                                error: function() {
                                    swal('Oops!', 'Ocurrio un error al subir el archivo', 'error');
                                },
                            });
                        };
                    }
                    else {
                        var ext = file.name.split('.').pop();

                        $('#dropzone div').html(ext);
                    }
                });
            });

            onSend = () => {
                let title = $('#mce_0').clone().attr({'class': 'mce-content-body', 'contenteditable': 'false'}).children();
                let content = $('.summernote').summernote('code');
                let tags = $tagSelector.select2('val');
                var tipo = $('select[name=tipo]').val();

                if ($('#img-destacada').attr('src') != '' && $('#img-destacada').attr('src') != undefined) {

                    $.ajax({
                        url: server + 'admin/blog/' + (id_blog != 0 ? ('update/'+id_blog) : 'store'),
                        method: 'POST',
                        data: {
                            contenido: content,
                            titulo: title.prevObject[0].childNodes[0].data,
                            tags: tags,
                            estado: parseInt(status),
                            tipoPost : tipo,
                            // data: (id_blog != 0) ? id_blog: null,
                            id_imagen_destacada: $('#img-destacada').attr('data-id'),
                        },
                        success: function(res) {
                            swal('OK!', 'Articulo guardado', 'success').then(() => {
                                window.location.replace(server + 'admin/blog');
                            });
                        },
                        error: function() {
                            swal('Oops!', 'Ocurrio un error intente más tarde', 'error');
                        },
                    });
                }
                else {
                    swal('Oops!', 'No puedes publicar el articulo sin agregar una imagen principal', 'error');
                }
            };
        </script>
    </body>
</html>