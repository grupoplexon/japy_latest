<?php

$data['extraCSS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
    base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
    base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
                <li class="breadcrumb-item active">BrideWeekend</li>
            </ol>
            <!-- end breadcrumb -->
            <br><br><br>
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">BrideWeekend</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                    <a class="btn" style="background: black; color: white;" href="<?php echo base_url() ?>admin/home/brideweekendNew">Nueva ciudad</a>
                    <br><br>
                    <div>
                        <table class="table table-striped table-bordered" id="tableBrideWeekend">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ciudad</th>
                                <th>Fecha inicial</th>
                                <th>Fecha final</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->


    <?php $this->view("admin/components/page-js", $data) ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            cargarDatos();
        });

        function cargarDatos(){
            $('#tableBrideWeekend').dataTable().fnDestroy();
            var table = $('#tableBrideWeekend').DataTable({
                // "processing":true,  
                "serverSide":true, 
                // responsive: true,
                "order": [[ 0, "desc" ]],
                "ajax":{  
                    url: '<?php echo base_url() ?>'+'admin/home/fetch_trend',  
                    type:"POST"  
                }, 
                scrollX:        true,
                "columnDefs":[  
                    {  
                        "targets": 2,  
                        "render": function (data, type, row, meta) {
                            fecha = new Date(row[2].replace(/-/g, '\/'));
                            var options = { year: 'numeric', month: 'long', day: 'numeric' };
                            var date = fecha.toLocaleDateString("es-ES", options);
                            return '<span>'+date+'</span>';
                        }  
                    },  
                    {  
                        "targets": 3,  
                        "render": function (data, type, row, meta) {
                            fecha = new Date(row[3].replace(/-/g, '\/'));
                            var options = { year: 'numeric', month: 'long', day: 'numeric' };
                            var date = fecha.toLocaleDateString("es-ES", options);
                            return '<span>'+date+'</span>';
                        }  
                    },  
                    {  
                        "targets": 4,  
                        "render": function (data, type, row, meta){
                            return '<a href="<?php echo base_url() ?>admin/home/editBW/'+row[0]+'"><button type="button" class="btn btn-info btn-sm">Editar</button></a>';
                        }
                    },  
                ],
                language: {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                }
            });
            // $('#tableTendencias tbody').on( 'change', 'select', function (e) {
            //     var data = table.row( $(this).parents('tr') ).data();
            //     var options = $(this).find('option:selected').val();
            //     let id = this.id;
            //     if(data)
            //         tipoEstado(data[5], options, id);
            // });
        }

    </script>
    
</body>