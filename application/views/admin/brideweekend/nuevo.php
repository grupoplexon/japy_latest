<?php
$data['extraCSS'] = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.css',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.css',
        base_url().'dist/admin/assets/css/custom/users/edit/style.css',
        base_url().'dist/admin/assets/plugins/parsley/src/parsley.css',
        base_url().'dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.css',
        base_url().'dist/admin/assets/plugins/select2/dist/css/select2.min.css',
        base_url().'dist/admin/assets/css/custom/blog/style.css',
];
$data['extraJS']  = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.js',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.js',
        base_url().'dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/parsley.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/i18n/es.js',
];
?>
<?php $this->view("admin/components/head", $data) ?>

<style>
    .hide {
        display: none;
    }
    .upload:hover {
        border: 3px dashed #f9a897;
    }

    .upload {
        min-height: 150px;
        border: 3px dashed #f5e6df;
        cursor: pointer;
    }

    .dz-success-mark {
        display: none !important;
    }
    .dz-error-mark {
        display: none !important;
    }
</style>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/home/brideweekend">BrideWeekend</a></li>
            <li class="breadcrumb-item active">Nuevo</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Nueva Ciudad</h1>
        <!-- end page-header -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        <h4 class="panel-title">Ingresar datos</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body panel-form">
                        <form action="<?= base_url() ?>admin/home/saveCity" method="POST" enctype="multipart/form-data" class="form-horizontal form-bordered">
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Ciudad del evento:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="city" required>
                                </div>
                                <label class="col-form-label col-md-2">Nombre del recinto:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="recinto" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Fecha final:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="date_i">
                                </div>
                                <label class="col-form-label col-md-2">Dirección del recinto:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="address">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Fecha inicial:</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="date_f" required>
                                </div>
                                <label class="col-form-label col-md-2">Descripción del recinto:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="description">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Horario de la expo (Sábado):</label>
                                <div class="col-md-2">
                                    <input type="time" class="form-control" name="hourSatExpoStart">
                                </div>
                                <div class="col-md-2">
                                    <input type="time" class="form-control" name="hourSatExpoEnd">
                                </div>
                                <label class="col-form-label col-md-2">Mapa del recinto:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="map" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Horario de la expo (Domingo):</label>
                                <div class="col-md-2">
                                    <input type="time" class="form-control" name="hourSunExpoStart">
                                </div>
                                <div class="col-md-2">
                                    <input type="time" class="form-control" name="hourSunExpoEnd">
                                </div>
                                <label class="col-form-label col-md-2">Logo del recinto:</label>
                                <div class="col-md-4">
                                    <input type="file" name="img_l" accept=".jpg,.png">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Horario de pasarelas (Sábado):</label>
                                <div class="col-md-2">
                                    <input type="time" class="form-control" name="hourSatPasStart">
                                </div>
                                <div class="col-md-2">
                                    <input type="time" class="form-control" name="hourSatPasEnd">
                                </div>
                                <label class="col-form-label col-md-2">Imágen del recinto:</label>
                                <div class="col-md-4">
                                    <input type="file" name="img_r" accept=".jpg,.png">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Horario de pasarelas (Domingo):</label>
                                <div class="col-md-2">
                                    <input type="time" class="form-control" name="hourSunPasStart">
                                </div>
                                <div class="col-md-2">
                                    <input type="time" class="form-control" name="hourSunPasEnd">
                                </div>
                                <label class="col-form-label col-md-2">Imágen de cabecera:</label>
                                <div class="col-md-4">
                                    <input type="file" name="img_c" accept=".jpg,.png">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">¿Quién organiza el evento?:</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="organized_by" required>
                                        <option value="plexon">Grupo Plexon</option>
                                        <option value="externo">Externo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button class="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<!-- ================== END BASE JS ================== -->

    <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>dist/js/tendencia/tendencia.js"></script>
    <!-- <script>tinymce.init({ selector:'textarea' });</script> -->

</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js") ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        
    });
</script>
</body>