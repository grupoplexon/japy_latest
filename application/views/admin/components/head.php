<!DOCTYPE html>
<!--[if IE 8]>
<html lang="es" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>Japy</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap/4.0.0/css/bootstrap.min.css"
          rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css"
          rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/animate/animate.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/css/default/style.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/css/default/style-responsive.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/css/default/theme/default.css" rel="stylesheet" id="theme"/>
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-jvectormap/jquery-jvectormap.css"
          rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"
          rel="stylesheet"/>
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet"/>
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/pace/pace.min.js"></script>
    <!-- ================== END BASE JS ================== -->
    <?php if (isset($extraCSS)) : ?>
        <?php foreach ($extraCSS as $css) : ?>
            <link href="<?php echo $css ?>" rel="stylesheet"/>
        <?php endforeach ?>
    <?php endif ?>
    <link rel="stylesheet" href="../dist/css/fancyzoom.css"/>
</head>