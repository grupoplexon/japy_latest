<div id="header" class="header navbar-default" style="background-color: #35879a;">
    <!-- begin navbar-header -->
    <div class="navbar-header">
        <a href="<?php echo base_url() ?>admin" style="color:#fff;"> <!-- class="navbar-brand" -->
            <img style="color:  #fff;padding: 5px 40px 2px;height: 60px;" class="responsive-img" src="<?php echo base_url() ?>dist/img/japy_nobg_white.png" alt="Japy">
            <!-- <span class="navbar-logo"></span> <b>Japy</b> -->
        </a>
        <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- end navbar-header -->

    <!-- begin header-nav -->
    <ul class="navbar-nav navbar-right">
        <li class="dropdown navbar-user">
            <a href="javascript:;" style="color:#fff;" class="dropdown-toggle" data-toggle="dropdown">
                <img src="../assets/img/user/user-13.jpg" alt=""/>
                <span class="d-none d-md-inline">Administrador</span> <b class="caret"></b>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="<?php echo base_url()."cuenta/logout" ?>" class="dropdown-item">Cerrar sesion</a>
            </div>
        </li>
    </ul>
    <!-- end header navigation right -->
</div>