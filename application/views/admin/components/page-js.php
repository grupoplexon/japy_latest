<!-- ================== BEGIN BASE JS ================== -->
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<!--[if lt IE 9]>
<script src="<?php echo base_url() ?>dist/admin/assets/crossbrowserjs/html5shiv.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/crossbrowserjs/respond.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/js-cookie/js.cookie.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/js/theme/default.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/js/apps.min.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/sparkline/jquery.sparkline.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/js/demo/dashboard.min.js"></script>

<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert2.all.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<?php if (isset($extraJS)) : ?>
    <?php foreach ($extraJS as $js) : ?>
        <script src="<?php echo $js ?>"></script>
    <?php endforeach ?>
<?php endif ?>

<script>
    $(document).ready(function () {
        App.init();
        Dashboard.init();
    });
</script>
