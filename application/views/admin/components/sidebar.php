<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <a href="javascript:;" data-toggle="nav-profile">
                    <div class="cover with-shadow"></div>
                    <div class="image">
                        <img src="../assets/img/user/user-13.jpg" alt=""/>
                    </div>
                    <div class="info">
                        <b class="caret pull-right"></b>
                        BrideAdvisor
                    </div>
                </a>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Navigation</li>
            <?php if($this->session->userdata('correo')!='flori@admin.com') { ?>
                <li class="has-sub">
                    <a href="<?php echo base_url()."admin/home" ?>">
                        <!-- <b class="caret"></b> -->
                        <i class="fa fa-th-large"></i>
                        <span>Dashboard</span>
                    </a>
                    <!-- <ul class="sub-menu">
                        <li class="active"><a href="index.html">Dashboard v1</a></li>
                        <li><a href="index_v2.html">Dashboard v2</a></li>
                    </ul> -->
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/users" ?>">
                        <i class="fas fa-users"></i>
                        <span>Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/blog" ?>">
                        <i class="fa fa-list-ol"></i>
                        <span>Articulos</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/tags" ?>">
                        <i class="fas fa-tags"></i>
                        <span>Etiquetas</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/proveedor" ?>">
                        <i class="fa fa-id-badge"></i>
                        <span>Proveedores</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/verif" ?>">
                        <i class="fa fa-bell"></i>
                        <span>Verificaciones</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/indicators" ?>">
                        <i class="fa fa-eye"></i>
                        <span>Indicadores</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/trends" ?>">
                        <i class="fas fa-indent"></i>
                        <span>Tendencias</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/home/destinations" ?>">
                        <i class="fas fa-map"></i>
                        <span>Destinos</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/promotion" ?>">
                        <i class="fas fa-ticket-alt"></i>
                        <span>Promociones</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url()."admin/home/brideweekend" ?>">
                        <i class="fas fa-indent"></i>
                        <span>BrideWeekend</span>
                    </a>
                </li>
            <?php } else { ?>
                <li>
                    <a href="<?php echo base_url()."admin/trends" ?>">
                        <i class="fas fa-indent"></i>
                        <span>Tendencias</span>
                    </a>
                </li>
            <?php } ?>
            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i
                            class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>