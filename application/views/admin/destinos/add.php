<?php
$data['extraCSS'] = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.css',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.css',
        base_url().'dist/admin/assets/css/custom/users/edit/style.css',
        base_url().'dist/admin/assets/plugins/parsley/src/parsley.css',
        base_url().'dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.css',
        base_url().'dist/admin/assets/plugins/select2/dist/css/select2.min.css',
        base_url().'dist/admin/assets/css/custom/blog/style.css',
        base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
];
$data['extraJS']  = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.js',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.js',
        base_url().'dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/parsley.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/i18n/es.js',
        base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
        base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
];
?>
<?php $this->view("admin/components/head", $data) ?>

<style>
    .hide {
        display: none;
    }
    .upload:hover {
        border: 3px dashed #f9a897;
    }

    .upload {
        min-height: 150px;
        border: 3px dashed #f5e6df;
        cursor: pointer;
    }

    .dz-success-mark {
        display: none !important;
    }
    .dz-error-mark {
        display: none !important;
    }
    .alert {
        margin-top: 10px !important;
    }
</style>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/home/destinations">Destino</a></li>
            <li class="breadcrumb-item active">Nuevo</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Nueva locación o atractivo turístico</h1>
        <!-- end page-header -->
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        <h4 class="panel-title">Ingresar datos</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body panel-form">
                        <form id="location" class="form-horizontal form-bordered" action="<?php echo base_url()."admin/home/addLocation" ?>" method="POST"
                         enctype="multipart/form-data">
                            <?php if(!empty($saved)) { ?>
                                <?php if($saved=='success') { ?>
                                    <div class="form-group row">
                                        <div class="alert alert-success" role="alert">
                                            ¡La información se guardo con éxito!
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="alert alert-danger" role="alert">
                                        ¡Ocurrio un error, la información no se guardo!
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <input type="hidden" value="0" id="edit" name="edit">
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Nombre: </label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="name" name="name" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Tipo: </label>
                                <div class="col-md-6">
                                    <select class="form-control" name="tipo" id="type" required>
                                        <option value="" selected disabled>Selecciona una opcion</option>
                                        <option value="Locacion">Locación</option>
                                        <option value="Atractivo">Atractivo turístico</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Imágen principal: </label>
                                <div class="col-md-4">
                                    <input type="file" name="archivo" id="archivo" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Descripción: </label>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="10" id="desc1" name="desc1"></textarea>
                                </div>
                                <input type="hidden" value="<?= $id ?>" id="idDestino" name="idDestino">
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button class="btn btn-success float-right" type="submit" id="save">Guardar</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <h2> Editar locación o atractivo turístico</h2>
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered" id="locations">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Tipo</th>
                                            <!-- <th>Descripción</th> -->
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
        </div>
    </div>
    <!-- end #content -->
    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<!-- ================== END BASE JS ================== -->

    <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>dist/js/tendencia/tendencia.js"></script>
    <!-- <script>tinymce.init({ selector:'textarea' });</script> -->

</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js") ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        cargarDatos();
    });

    function cargarDatos() {
            $('#locations').dataTable().fnDestroy();
            var table = $('#locations').DataTable({
                    // "processing":true,  
                    "serverSide":true, 
                    // responsive: true,
                    "ajax":{  
                        url: '<?php echo base_url() ?>'+'admin/home/extractLocations',  
                        type:"POST",
                        data: {
                            id: $('#idDestino').val()
                        },
                    }, 
                    scrollX:        true,
                    "columnDefs":[  
                        {  
                            "targets": 3,  
                            "render": function (data, type, row, meta){
                                // console.log(row);
                                return '<a class="btn btn-info btn-sm" onClick="editar('+row[0]+','+'`'+row[1]+'`,'+'`'+row[2]+'`,'+'`'+row[3]+'`,'+'`'+row[4]+'`'+');">Editar</a>';
                            }
                        },  
                        // {  
                        //     "targets": 4,  
                        //     "render": function (data, type, row, meta){
                        //         console.log(row[0]);
                        //         return '<a class="btn btn-secondary btn-sm" href="<?= base_url() ?>admin/home/edit/'+row[0]+'">Editar</a>';
                        //     }
                        // },  
                    ],
                    language: {
                        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                    }
                });
                // $('#tableTendencias tbody').on( 'change', 'select', function (e) {
                //     var data = table.row( $(this).parents('tr') ).data();
                //     var options = $(this).find('option:selected').val();
                //     let id = this.id;
                //     if(data)
                //         tipoEstado(data[5], options, id);
                // });
        }

        function editar(id,name,type,image,description) {
            $('#name').val(name);
            $('#type').val(type);
            $('#desc1').val(description);
            // $('name').val(id);
            console.log(id);
            console.log(name);
            console.log(type);
            console.log(image);
            console.log(description);
            $("html, body").animate({ scrollTop: 0 }, 600);
            $("#save").html('Actualizar');
            $("#edit").val(id);
            $('#archivo').removeAttr("required");
        }
</script>
</body>