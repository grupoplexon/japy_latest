<?php
$data['extraCSS'] = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.css',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.css',
        base_url().'dist/admin/assets/css/custom/users/edit/style.css',
        base_url().'dist/admin/assets/plugins/parsley/src/parsley.css',
        base_url().'dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.css',
        base_url().'dist/admin/assets/plugins/select2/dist/css/select2.min.css',
        base_url().'dist/admin/assets/css/custom/blog/style.css',
];
$data['extraJS']  = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.js',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.js',
        base_url().'dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/parsley.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/i18n/es.js',
];
?>
<?php $this->view("admin/components/head", $data) ?>

<style>
    .hide {
        display: none;
    }
    .upload:hover {
        border: 3px dashed #f9a897;
    }

    .upload {
        min-height: 150px;
        border: 3px dashed #f5e6df;
        cursor: pointer;
    }

    .dz-success-mark {
        display: none !important;
    }
    .dz-error-mark {
        display: none !important;
    }
</style>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/home/destinations">Destino</a></li>
            <li class="breadcrumb-item active">Nuevo</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Nuevo Destino</h1>
        <!-- end page-header -->
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        <h4 class="panel-title">Ingresar datos</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body panel-form">
                        <form id="destino" class="form-horizontal form-bordered" action="<?php echo base_url()."admin/home/saveDestination" ?>" method="POST"
                        enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Nombre: </label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Imágen principal: </label>
                                <div class="col-md-4">
                                    <input type="file" name="archivo" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Descripción del destino: </label>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="10" id="desc1" name="desc1" required>
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Descripción de la locación: </label>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="10" id="desc2" name="desc2" required>
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Descripción de atractivos turísticos: </label>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="10" id="desc3" name="desc3" required>
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button class="btn btn-success float-right" type="submit" id="save">Guardar</button>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<!-- ================== END BASE JS ================== -->

    <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>dist/js/tendencia/tendencia.js"></script>
    <!-- <script>tinymce.init({ selector:'textarea' });</script> -->

</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js") ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
$('#desc1').val('');
$('#desc2').val('');
$('#desc3').val('');
const $registerForm = $('#destino').parsley();

$('#btn_enviar').on('click', function(e) {

    e.preventDefault();

    $registerForm.validate();
    
    if ($registerForm.isValid()) {
        if($('#desc1').val()!='' && $('#desc2').val()!='' && $('#desc3').val()!='') {
            $('#destino').submit();
        } else {
            swal('Atención', 'Complete todos los campos', 'warning');
            return false;
        }
    }
});
</script>
</body>