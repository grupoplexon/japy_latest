<?php

$data['extraCSS'] = [
    base_url()."dist/admin/assets/plugins/morris/morris.css",
];

$data['extraJS'] = [
    base_url()."dist/admin/assets/plugins/morris/morris.min.js",
    base_url()."dist/admin/assets/plugins/morris/raphael.min.js",
];

?>

<?php $this->view("admin/components/head", $data) ?>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->

    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <div id="content" class="content">
            <div class="row">
                <div class="col-12">
                    <div class="panel panel-inverse" data-sortable-id="index-1">
                        <div class="panel-heading">
                            <h4 class="panel-title">Novias registradas en los ultimos 7 dias</h4>
                        </div>
                        <div class="panel-body">
                            <div id="users-registered-chart" class="height-sm"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
        <input type="hidden" id="base-url" value="<?php echo base_url() ?>">
    </div>
    <!-- end page container -->
    <?php $this->view("admin/components/page-js", $data) ?>
    <script>
        $(document).ready(function() {
            const baseUrl = $('#base-url').val();
            const usersRegiteredChart = Morris.Line({
                element: 'users-registered-chart',
                data: [],
                xkey: 'day',
                ykeys: ['desconocido', 'cdmx', 'Guanajuato', 'Jalisco', 'Nuevo_leon', 'Puebla', 'Queretaro', 'Sinaloa', 'Veracruz'],
                labels: ['Desconocido', 'Cdmx', 'Guanajuato', 'Jalisco', 'Nuevo leon', 'Puebla', 'Queretaro', 'Sinaloa', 'Veracruz'],
                xLabels: 'hour',
            });

            $.get(baseUrl +'admin/home/usersRegisteredWeek', function(response) {
                usersRegiteredChart.setData(response.data);
            }).fail(function(error) {
            });

        });
    </script>
</body>
