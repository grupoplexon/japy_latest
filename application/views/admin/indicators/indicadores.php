<?php

$data['extraCSS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
    base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
    base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
                <li class="breadcrumb-item active">Indicadores</li>
            </ol>
            <!-- end breadcrumb -->
            <br><br><br>
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Indicadores</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                    <div>
                        <div style="text-align: right !important; " id="fechas">
                            <label>Del:&nbsp;</label><input type="date" id="fechaInicial" value="<?php echo date("Y-m-d");?>">
                            <label>&nbsp;al:&nbsp;</label><input type="date" id="fechaFinal" value="<?php echo date("Y-m-d");?>">
                            &nbsp;&nbsp;<button class="btn btn-sm btn-primary width-60 m-r-2" id="busca">Buscar</button>
                        </div>
                        <br><br>
                        <table class="table table-striped table-bordered" id="tableIndicators">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Inicios de sesion</th>
                                <th>Vistas de Teléfono</th>
                                <th>Visitas al escaparate</th>
                                <th>Mensajes enviados</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                        <h4 id="barra" style="display: none">Cargando, por favor espere...</h4>
                        <h4 id="barra2" style="display: none">No hay ningun registro</h4>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->
    <?php $this->view("admin/components/page-js", $data) ?>
    <script>
        $(document).ready(function () {
            var val1 = $("#fechaInicial").val();
            var val2 =$("#fechaFinal").val();
            // alert("incial: "+ val1+ " final: "+val2);
            // cargarDatos(val1, val2);
            // $('#fechas').on( 'change', 'input', function (e) {
            //             var val1 = $("#fechaInicial").val();
            //             var val2 =$("#fechaFinal").val();
            //             // alert("incial: "+ val1+ " final: "+val2);
            //             cargarDatos(val1, val2);
            //         });
            $('#fechas').on( 'click', 'button', function () {
                var val1 = $("#fechaInicial").val();
                var val2 =$("#fechaFinal").val();
                cargarDatos(val1, val2);
            });
        });

        function cargarDatos_(fechaIni, fechaFin) {
            $('#tableIndicators').dataTable().fnDestroy();
            var table = $('#tableIndicators').DataTable({
                // "processing":true,  
                "serverSide":true, 
                // responsive: true,
                scrollX:        true,
                // columnDefs: [{
                //     "targets": 4,
                //     "orderable": false
                // }],
                "ajax":{  
                    url: '<?php echo base_url() ?>'+'admin/indicators/fetch_user',  
                    type:"POST",
                    data: {
                        fechaI: fechaIni,
                        fechaF: fechaFin,
                    },
                },
                scrollX:    true,
                language: {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                }
            });
        }

        function cargarDatos(fechaIni, fechaFin) {
            fechaInicial.disabled = true;
            fechaFinal.disabled = true;
            barra.style.display = "block";
            barra2.style.display = "none";
            busca.disabled = true;
            $.ajax({
                url: '<?php echo base_url() ?>'+'admin/indicators/muestraIndicadores',
                method: 'post',
                dataType: 'json',
                data: {
                    fechaI: fechaIni,
                    fechaF: fechaFin,
                },
                success: function(response) {
                    $('#tableIndicators').dataTable().fnDestroy(); 
                    var table = $('#tableIndicators').DataTable({
                        // responsive: true,
                        scrollX:    true,
                        // columnDefs: [{
                        //     "targets": 4,
                        //     "orderable": false
                        // }],
                        data: response.indicadores,
                        columns: [
                            {data: "nombres"},
                            {data: "login"},
                            {data: "phone"},
                            {data: "visit"},
                            {data: "mensajes"},
                        ],
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                        }
                    });
                    fechaInicial.disabled = false;
                    fechaFinal.disabled = false;
                    busca.disabled = false;
                    barra.style.display = "none";
                },
            error: function(e) {
                barra.style.display = "none";
                barra2.style.display = "block";
                fechaInicial.disabled = false;
                fechaFinal.disabled = false;
                busca.disabled = false;
            },
            });
        }
    </script>
    
</body>