<?php
$data['extraCSS'] = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.css',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.css',
        base_url().'dist/admin/assets/css/custom/users/edit/style.css',
        base_url().'dist/admin/assets/plugins/parsley/src/parsley.css',
        base_url().'dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.css',
        base_url().'dist/admin/assets/plugins/select2/dist/css/select2.min.css',
        base_url().'dist/admin/assets/css/custom/blog/style.css',

        base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];
$data['extraJS']  = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.js',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.js',
        base_url().'dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/parsley.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/i18n/es.js',

        base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
        base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
        base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];
?>
<?php $this->view("admin/components/head", $data) ?>

<style>
    .hide {
        display: none;
    }
    .upload:hover {
        border: 3px dashed #f9a897;
    }

    .upload {
        min-height: 150px;
        border: 3px dashed #f5e6df;
        cursor: pointer;
    }

    .dz-success-mark {
        display: none !important;
    }
    .dz-error-mark {
        display: none !important;
    }
</style>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/promotion/">Promociones</a></li>
            <li class="breadcrumb-item active">Editar</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Editar Promocion</h1>
        <!-- end page-header -->
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                        <h4 class="panel-title">Ingresar datos</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body panel-form">
                        <form id="promociones" class="form-horizontal form-bordered" action="<?php echo base_url()."admin/promotion/edit" ?>" method="POST"
                            enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Nombre de el Proveedor</label>
                                <div class="col-md-9">
                                    <input id="id_provider" name="id_promotion" value="<?= $promotion->id_promotion ?>" type="text" class="hide">
                                    <input id="provider" name="provider" value="<?= $promotion->name_provider ?>" type="text" class="form-control m-b-5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Descripción </label>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="10" id="descripcion" name="descripcion" required><?= $promotion->description ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Perfil Proveedor </label>
                                <div class="col-md-9">
                                    <input id="perfil" name="perfil" value="<?= $promotion->profile_provider ?>" type="text" class="form-control m-b-5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Imágen principal: </label>
                                <div class="col-md-4" style="padding-bottom: 50px;">
                                    <input type="file" name="principal" id="principal" >
                                    <br> <br>
                                    <img id="imgPrincipal" width="80%" height="80%" src="<?php echo base_url()?>uploads/promociones/images/<?= $promotion->image ?>" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Imágen Cupón: </label>
                                <div class="col-md-4" style="padding-bottom: 50px;">
                                    <input type="file" name="cupon" id="cupon" >
                                    <br> <br>
                                    <img id="imgCupon" width="100%" height="100%" src="<?php echo base_url()?>uploads/promociones/images/<?= $promotion->cupon ?>" />
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button class="btn btn-success float-right" type="submit" id="save">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<!-- ================== END BASE JS ================== -->

    <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>dist/js/tendencia/tendencia.js"></script>
    <!-- <script>tinymce.init({ selector:'textarea' });</script> -->

</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js") ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    var indicator = 0;
    $(document).ready(function() {
        $('#principal').change(function(e) {
            addImage(e); 
        });
        $('#cupon').change(function(e) {
            addImage2(e); 
        });
    });
    function addImage(e){
        var file = e.target.files[0],
        imageType = /image.*/;
        if (!file.type.match(imageType))
        return;

        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
    }
        
    function fileOnload(e) {
        var result=e.target.result;
        $('#imgPrincipal').attr("src",result);
    }
    function addImage2(e){
        var file = e.target.files[0],
        imageType = /image.*/;
        if (!file.type.match(imageType))
        return;

        var reader = new FileReader();
        reader.onload = fileOnload2;
        reader.readAsDataURL(file);
    }
        
    function fileOnload2(e) {
        var result=e.target.result;
        $('#imgCupon').attr("src",result);
    }
</script>
</body>