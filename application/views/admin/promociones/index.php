<?php

$data['extraCSS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
    base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
    base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
                <li class="breadcrumb-item active">Promociones</li>
            </ol>
            <!-- end breadcrumb -->
            <br><br><br>
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Promociones</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                    <a class="btn" style="background: black; color: white;" href="<?php echo base_url() ?>admin/promotion/newPromotion">Nuevo</a>
                    <br><br>
                    <div>
                        <table class="table table-striped table-bordered" id="destinos">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre Proveedor</th>
                                    <th>Descripcion</th>
                                    <th>Descargas</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($promotions as $promo) : ?> 
                                <tr>
                                    <td><?php echo $promo->id_promotion ?></td>
                                    <td><?php echo $promo->name_provider ?></td>
                                    <td><?php echo $promo->description ?></td>
                                    <td><?php echo $promo->downloads ?></td>
                                    <td style="width:20%">
                                    <a href="<?php echo base_url() ?>admin/promotion/editPromotion?id_promotion=<?php echo $promo->id_promotion ?>" id="editar" onclick="" class="btn btn-sm btn-primary width-60 m-r-2">Editar</a>
                                    <a href="#" id="eliminar" onclick="deletePromo(<?php echo $promo->id_promotion ?>);" class="btn btn-sm btn-danger width-70">Eliminar</a>
                                    </td>
                                </tr>
                            <?php endforeach ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->


    <?php $this->view("admin/components/page-js", $data) ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            // cargarDatos();
        });

        function deletePromo(id){
            swal({
                title: '¿Estas seguro que deseas eliminar esta promoción?',
                icon: 'error',
                buttons: ['Cancelar', true],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                    url: "<?php echo base_url() ?>admin/Promotion/deletePromotion",
                    method: "POST",
                    data: {
                    id_promotion: id,
                    },
                    success: function(response) {
                        console.log("SUCCES");
                        location.reload();
                    },
                    error: function(e) {
                        console.log("ERROR");
                    },
                });
                }
            });
        };

    </script>
</body>