<?php

$data['extraCSS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
    base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
    base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
                <li class="breadcrumb-item active">Proveedores</li>
            </ol>
            <!-- end breadcrumb -->
            <br><br><br>
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Proveedores</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                    <div>
                        <table class="table table-striped table-bordered" id="tableProveedores">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Tipo cuenta</th>
                                <th>Tiempo</th>
                                <th>Expiracion</th>
                                <th>Activación</th>
                                <th>Categoria</th>
                                <th>Usuario</th>
                                <th>Teléfono</th>
                                <th>Estado/País</th>
                                <th>Fecha de registro</th>
                                <th>Información</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                        <h4 id="barra" style="display: none">Cargando, por favor espere...</h4>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->



      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Datos de Usuario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="modal">
                
                <p>Datos Actuales: <h5 id="actualLocation">---</h5></p>
                    <p>
                        <label>
                            Pais
                        </label>
                        <select id="selectCountry" name="pais" class="form-control  browser-default countries validate[required]">
                            <!-- <option value="" disabled selected>-- Pa&iacute;s --</option> -->
                            <option value="">-- Pais --</option>
                            <?php foreach ($countries as $country) : ?>
                                <option value= "<?= $country->id_pais; ?>"><?= $country->nombre; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </p>
                    <p>
                        <label>
                            Estado
                        </label>
                        <select id="selectState" name="estado" class="form-control  browser-default states validate[required]">
                            <option value="" disabled selected>-- Estado --</option>
                        </select>
                    </p>
                    <p>
                        <label>
                            Poblacion
                        </label>
                        <select id="selectCity" name="poblacion" class="form-control validate[required]  browser-default cities">
                            <option value="" disabled selected>-- Poblaci&oacute;n --</option>
                        </select>
                    </p>
                    <p style="text-align: center !important; ">
                        <button class="btn btn-info btn-sm save">Guardar&nbsp;</button>
                    </p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            
            </div>
        </div>



    <?php $this->view("admin/components/page-js", $data) ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            start_page();
        });

        function start_page(){
            cargarDatos();
            // location_proveedor();
            $( "#selectCountry" ).change(function() {
                $.ajax({
                    url: '<?= base_url().'/registro/getStates/' ?>'+$('#selectCountry').val(),
                    dataType: 'json',
                    timeout: 8000,
                    success: function(response) {
                        // $('#selectCity').remove();
                        $("#selectState").empty();
                        var myselect = $('<select>');
                        myselect.append( $('<option></option>').val(0).html('Selecciona un estado...') );
                        $.each(response.states, function(index, key) {
                            myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                        });
                        $('#selectState').append(myselect.html());
                    }
                });
            });

            $( "#selectState" ).change(function() { /// obtiene ciudades
                $.ajax({
                    url: '<?= base_url().'/registro/getCities/' ?>'+$('#selectState').val(),
                    dataType: 'json',
                    timeout: 8000,
                    success: function(response) {
                        // $('#selectCity').remove();
                        $("#selectCity").empty();
                        var myselect = $('<select>');
                        myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                        $.each(response.cities, function(index, key) {
                            myselect.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                        });
                        $('#selectCity').append(myselect.html());
                    }
                });
            });

            $('#modal').on( 'click', 'button', function () {
                let id = this.id;
                var pais = $("#selectCountry").find('option:selected').text();
                var estado = $("#selectState").find('option:selected').text();
                var ciudad = $("#selectCity").find('option:selected').val();
                // alert("id= "+id+" pais= "+pais+" estado= "+estado+" ciudad= "+ciudad);
                if(id!=null && pais!="-- Pais --" && estado!="Selecciona un estado..." && estado!="-- Estado --" && ciudad!=0 && ciudad!="-- Población --") {
                    $.ajax({
                        url: '<?php echo base_url() ?>'+'admin/proveedor/modificarLocalizacion',
                        method: 'post',
                        data: {
                            Id: id,
                            Pais: pais,
                            Estado: estado,
                            Ciudad: ciudad
                        },
                        success: function(response) {
                            selectCountry.value="";
                            $("#selectState").empty();
                            var myselect = $('<select>');
                            myselect.append( $('<option></option>').val(0).html('-- Estado --') );
                            $.each(response.states, function(index, key) {
                                myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                            });
                            $('#selectState').append(myselect.html());
                            $("#selectCity").empty();
                            var myselect2 = $('<select>');
                            myselect2.append( $('<option></option>').val(0).html('-- Ciudad --') );
                            $.each(response.cities, function(index, key) {
                                myselect2.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                            });
                            $('#selectCity').append(myselect2.html());

                            $('#myModal').modal('hide');
                            swal('¡Correcto!', 'Los datos fueron guardados', 'success');
                            cargarDatos();
                        },
                        error: function(e) {
                            swal('¡Error!', 'No se guardaron los datos', 'error');
                        },
                    });
                } else {
                    swal('¡Error!', 'Por favor complete todos los campos', 'error');
                }
            });
        }

        function cargarDatos(){
            barra.style.display = "block";
            $('#tableProveedores').dataTable().fnDestroy();
            var table = $('#tableProveedores').DataTable({
                    // "processing":true,  
                    "serverSide":true, 
                    responsive: true,
                    "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "Todos"]],
                    "ajax":{  
                        url: '<?php echo base_url() ?>'+'admin/proveedor/fetch_user',  
                        type:"POST"  
                    }, 
                    scrollX:        true,
                    "columnDefs":[  
                        {  
                            "targets": 1,  
                            "render": function (data, type, row, meta){
                                var $select = $('<select class="form-control" id="tipo"><option id="basico" value="0">Basico</option><option value="1">Plata</option><option value="2">Oro</option><option value="3">Diamante</option></select>');
                                $select.find('option[value="'+data+'"]').attr('selected', 'selected');
                                return $select[0].outerHTML;
                            }  
                        },  
                        {  
                            "targets": 2,  
                            "render": function (data, type, row, meta){
                                var $select = $('<select class="form-control" id="tiempo"><option value="1">Duracion...</option><option value="1">1 Mes</option><option value="2">2 Meses</option><option value="3">3 Meses</option><option value="4">4 Meses</option><option value="5">5 Meses</option><option value="6">6 Meses</option><option value="7">7 Meses</option><option value="8">8 Meses</option><option value="9">9 Meses</option><option value="10">10 Meses</option><option value="11">11 Meses</option><option value="12">12 Meses</option></select>');
                                $select.find('option[value="'+data+'"]').attr('selected', 'selected');
                                return $select[0].outerHTML;
                            } 
                        },  
                        {  
                            "targets": 4,  
                            "render": function (data, type, row, meta){
                                var $select = $('<select class="form-control" id="estado"><option value="0">Desactivado</option><option value="1">Pendiente</option><option value="2">Activo</option></select>');
                                $select.find('option[value="'+data+'"]').attr('selected', 'selected');
                                return $select[0].outerHTML;
                            } 
                        },  
                        {  
                            "targets": 5,  
                            "render": function (data, type, row, meta){
                                var options = '';
                                options+= '<option value="">Seleccione categoria</option>';
                                for (let i = 0; i < data.categorias.length; i++) {
                                    options+= '<option value="'+data.categorias[i].id+'">'+data.categorias[i].name+'</option>';
                                }
                                var $select = $('<select class="form-control" id="categoria">'+options+'</select>');
                                if(data.category.categoria_id!='') {
                                    $select.find('option[value="'+data.category.categoria_id+'"]').attr('selected', 'selected');
                                }
                                return $select[0].outerHTML;
                            } 
                        },  
                        {  
                            "targets": 10,  
                            "render": function (data, type, row, meta){
                                return '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onClick="location_proveedor('+row[10]+','+'\''+row[11]+'\''+','+'\''+row[12]+'\''+','+'\''+row[13]+'\''+')">Abrir</button>';
                            }
                        },  
                    ],
                    language: {
                        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                    }
                });
                $('#tableProveedores tbody').on( 'change', 'select', function (e) {
                    var data = table.row( $(this).parents('tr') ).data();
                    var options = $(this).find('option:selected').val();
                    let id = this.id;
                    if(data)
                        tipoEstado(data[10], options, id);
                });
                barra.style.display = "none";
        }

        function tipoEstado(iduser, dato, tabla) {
            $.ajax({
                url: '<?php echo base_url() ?>'+'admin/proveedor/tipoEstado',
                method: 'post',
                //dataType: 'json',
                data: {
                    id_user: iduser,
                    date: dato,
                    table: tabla,
                },
                success: function(response) {
                    if(response.tiempo == true){
                        start_page();
                    }
                    // $('#articleTable').dataTable().fnDestroy(); 
                    // cargarDatos();
                },
                error: function(e) {
                    console.log("ERROR");
                },
            });
        }

        function location_proveedor(user_id = null,country = null,state = null,city = null,){
            $( "#actualLocation" ).text(' Pais: '+country+', '+'Estado: '+state+', '+'Ciudad: '+city);
            $(".save").attr("id", user_id);
        }
    </script>
    
</body>