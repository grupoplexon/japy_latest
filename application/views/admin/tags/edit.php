<?php

$data['extraCSS'] = [
        base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
        base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
        base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
        base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
<!-- ESTA VISTA NO ESTA EN USO POR AHORA 18-DIC-2018 -->
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="<?= base_url()."admin/home" ?>">Home</a></li>
            <li class="breadcrumb-item active">Etiquetas</li>
        </ol>
        <!-- end breadcrumb -->
        <div class="panel panel-inverse">
            <!-- begin panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title">Etiquetas</h4>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <form id="personal" class="form-horizontal form-bordered" data-parsley-validate="true" novalidate>
                    <?php //echo base_url()."perfil/foto/".$user->id_usuario ?>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3" style="margin:auto;">Imagen principal</label>
                        <div class="col-md-9">
                            <div id="dropzone" >
                                <div> <p>Arrastre una imagen o de click para seleccionar una</p> </div>
                                <input type="file" accept="image/png, image/jpeg" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Estado</label>
                        <div class="col-md-9">
                            <input id="status" type="checkbox" class="js-switch"
                                    <?php echo $user->activo ? "checked" : "" ?>/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">ID</label>
                        <div class="col-md-9">
                            <input id="id" type="text" class="form-control m-b-5"
                                    value="<?php echo $user->id_usuario ?>" disabled>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end panel-body -->
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js", $data) ?>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">           
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Etiqueta</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="modal">
                <div class="form-group row">
                    <label class="col-form-label col-md-3">Nombre</label>
                    <div class="col-md-9">
                        <input id="id" type="text" class="form-control m-b-5"
                            value="" disabled>
                    </div>
                </div>
                <p style="text-align: center !important; ">
                    <button class="btn btn-info btn-sm save">Guardar&nbsp;</button>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#modal').on( 'click', 'button', function () {
                
        });
    });

</script>
</body>
