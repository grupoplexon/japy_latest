<?php

$data['extraCSS'] = [
        base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
        base_url()."dist/css/bootstrap-select.min.css",
];

$data['extraJS'] = [
        base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
        base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
        base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
        base_url()."dist/js/bootstrap-select.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <!-- <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="<?= base_url()."admin/home" ?>">Home</a></li>
            <li class="breadcrumb-item active">Etiquetas</li>
        </ol> -->
        <!-- end breadcrumb -->
        <div class="panel panel-inverse">
            <!-- begin panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title">Etiquetas</h4>
            </div>
            <a href="" class="btn btn-warning width-60 m-r-2" data-toggle="modal" data-target="#myModal_new">NUEVA</a>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <table class="table table-striped table-bordered" id="tagsTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>SubCategoria</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($tags as $tag) : ?>
                        <tr>
                            <td><?= $tag->id ?></td>
                            <td><?= $tag->title ?></td>
                            <td><?= !empty($tag->subcategoria->name)? $tag->subcategoria->name : "-" ?></td>
                            <td class="with-btn" nowrap="">
                                <a href="" onclick="_edit('<?= $tag->id ?>', '<?= $tag->title ?>', '<?= $tag->subcategoria_id ?>',)"
                                   class="btn btn-sm btn-primary width-60 m-r-2" data-toggle="modal" data-target="#myModal">Editar</a>
                                <a href="#" class="btn btn-sm btn-danger width-70" onclick="_delete('<?= $tag->id ?>')">Eliminar</a>
                                <!-- <a href=""><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onClick="">Abrir</button></a> -->
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <!-- end panel-body -->
        </div>
    </div>
    <!-- end #content -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">           
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Etiqueta</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="modal">
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Nombre</label>
                        <div class="col-md-9">
                            <input id="modalName" type="text" class="form-control m-b-5"
                                value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Sub-Categoria</label>
                        <div class="col-md-9">
                            <select id="selectpicker" class="selectpicker form-control" data-live-search="true">
                            <option value="">Selecciona...</option>
                            <?php foreach ($subcategorias as $k => $v) {?>
                                <option value="<?= $v->id?>"><?= $v->name?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <p style="text-align: center !important; margin:auto;">
                            <button id="button_saveEdit" class="btn btn-info btn-sm save">Guardar&nbsp;</button>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal _NEW-->
    <div class="modal fade" id="myModal_new" role="dialog">
        <div class="modal-dialog">           
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nueva Etiqueta</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="modal_new">
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Nombre</label>
                        <div class="col-md-9">
                            <input id="modalName_new" type="text" class="form-control m-b-5"
                                value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Sub-Categoria</label>
                        <div class="col-md-9">
                            <select id="selectpicker_new" class="selectpicker form-control" data-live-search="true">
                            <option value="">Selecciona...</option>
                            <?php foreach ($subcategorias as $k => $v) {?>
                                <option value="<?= $v->id?>"><?= $v->name?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    <p style="text-align: center !important; ">
                        <button id="button_saveNew" class="btn btn-info btn-sm save">Guardar&nbsp;</button>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js", $data) ?>

<script>
var _ID = "";
var _NAME = "";
var _SUBCATEGORIA = "";
    $(document).ready(function () {

        $('#tagsTable').DataTable({
            responsive: true,
            columnDefs: [{
                "targets": 2,
                "orderable": false
            }],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        });

        // Entra al darle click al botton del modal
        $('#modal').on( 'click', '#button_saveEdit', function () {
            _NAME = $('#modalName').val();
            _SUBCATEGORIA = $('#selectpicker').val();
            $.ajax({
                url: '<?php echo base_url() ?>'+'admin/tags/edit',
                method: 'post',
                //dataType: 'json',
                data: {
                    id: _ID,
                    title: _NAME,
                    subcategoria: _SUBCATEGORIA,
                },
                success: function(response) {
                    location.reload(); 
                },
                error: function(e) {
                    console.log("ERROR al editar");
                },
            });
        });

        // Entra al darle click al botton del modal_new
        $('#modal_new').on( 'click', '#button_saveNew', function () {
            _NAME = $('#modalName_new').val();
            _SUBCATEGORIA = $('#selectpicker_new').val();
            $.ajax({
                url: '<?php echo base_url() ?>'+'admin/tags/new',
                method: 'post',
                //dataType: 'json',
                data: {
                    title: _NAME,
                    subcategoria: _SUBCATEGORIA,
                },
                success: function(response) {
                    location.reload(); 
                },
                error: function(e) {
                    console.log("ERROR al editar");
                },
            });
        });
        $('#selectpicker').selectpicker({
            // live search options
            liveSearch: true,
            liveSearchPlaceholder: "null",
            liveSearchNormalize: true,
            liveSearchStyle: 'contains',
        });
        $('#selectpicker_new').selectpicker({
            // live search options
            liveSearch: true,
            liveSearchPlaceholder: "null",
            liveSearchNormalize: true,
            liveSearchStyle: 'contains',
        });


    });
    // entra cuando le dan a editar
    function _edit(id, name, subcategoria_id){
        _ID = id;
        _NAME = name;
        $('#modalName').val(_NAME);
    }
    function _delete(id){
        Swal({
            title: 'Estas Segur@?',
            text: "Al eliminar la etiqueta dejara de existir en usos actuales!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!'
        }).then((result) => {
            // console.log('afuera');
            if (result.value) {
                // console.log('adentro');
                $.ajax({
                    url: '<?php echo base_url() ?>'+'admin/tags/delete',
                    method: 'post',
                    //dataType: 'json',
                    data: {
                        id: id,
                    },
                    success: function(response) {
                        Swal(
                        'Eliminado!',
                        'Etiqueta eliminada.',
                        'success'
                        );
                        location.reload(); 
                        
                    },
                    error: function(e) {
                        console.log("ERROR al editar");
                    },
                });
                
            }
        });
    }
</script>

</body>
