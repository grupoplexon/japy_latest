<?php
$data['extraCSS'] = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.css',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.css',
        base_url().'dist/admin/assets/css/custom/users/edit/style.css',
        base_url().'dist/admin/assets/plugins/parsley/src/parsley.css',
        base_url().'dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.css',
        base_url().'dist/admin/assets/plugins/select2/dist/css/select2.min.css',
        base_url().'dist/admin/assets/css/custom/blog/style.css',
];
$data['extraJS']  = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.js',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.js',
        base_url().'dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/parsley.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/i18n/es.js',
];
?>
<?php $this->view("admin/components/head", $data) ?>

<style>
    .hide {
        display: none;
    }
    .upload:hover {
        border: 3px dashed #f9a897;
    }

    .upload {
        min-height: 150px;
        border: 3px dashed #f5e6df;
        cursor: pointer;
    }
    .dz-success-mark {
        display: none !important;
    }
    .dz-error-mark {
        display: none !important;
    }
</style>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/trends">Tendencia</a></li>
            <li class="breadcrumb-item active">Nuevo</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Nueva Tendencia</h1>
        <!-- end page-header -->
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        <h4 class="panel-title">Ingresar datos</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body panel-form">
                        <form id="contenido" class="form-horizontal form-bordered">
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Tipo de artículo</label>
                                <div class="col-md-4">
                                    <input id="types" class="form-control m-b-5" disabled type="text" value="<?php echo $tendencia->nombre_vestido ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Modelo</label>
                                <div class="col-md-9">
                                    <input id="model" type="text" class="form-control m-b-5" value="<?php echo $tendencia->nombre ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Descripción</label>
                                <div class="col-md-9">
                                <textarea id="detalle" class="detalle" name="detalle"><?php echo $tendencia->detalle ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Imágen</label>
                                <div class="col-md-9" style="text-align: center;">
                                    <div class="card-panel z-depth-0 valign-wrapper upload  " id="upload" style="padding-top: 7vh;">
                                        <text style="width: 100%;text-align: center;color: gray;"><i class="fa fa-upload fa-2x valign "></i><br>
                                            Suelte los archivos o haga clic aqu&iacute; para cargar.
                                        </text>
                                    </div>
                                    <input type="hidden" name="file" class="archivo" value="<?php echo base_url()."uploads/images/tendencia/".$tendencia->image ?>">
                                    <br><button class="eliminar-foto btn grey right" type="button">
                                        Eliminar foto
                                    </button>
                                </div>
                            </div>
                            <?php if($tendencia->nombre_vestido=="Vestidos de Novia") { ?>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Escote</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualOne"><?php echo $tendencia->escote ?></p>
                                    </div>
                                    <?php if(!empty($tipos[0]->sortable[0]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleOne" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[0]->sortable[0]->valores); $i++) { ?>
                                                <option><?php echo $tipos[0]->sortable[0]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Corte</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualTwo"><?php echo $tendencia->corte ?></p>
                                    </div>
                                    <?php if(!empty($tipos[0]->sortable[1]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleTwo" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[0]->sortable[1]->valores); $i++) { ?>
                                                <option><?php echo $tipos[0]->sortable[1]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if($tendencia->nombre_vestido=="Trajes de Novio") { ?>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Estilo</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualOne"><?php echo $tendencia->estilo ?></p>
                                    </div>
                                    <?php if(!empty($tipos[1]->sortable[0]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleOne" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[1]->sortable[0]->valores); $i++) { ?>
                                                <option><?php echo $tipos[1]->sortable[0]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if($tendencia->nombre_vestido=="Vestidos de Fiesta") { ?>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Largo</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualOne"><?php echo $tendencia->largo ?></p>
                                    </div>
                                    <?php if(!empty($tipos[2]->sortable[0]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleOne" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[2]->sortable[0]->valores); $i++) { ?>
                                                <option><?php echo $tipos[2]->sortable[0]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if($tendencia->nombre_vestido=="Joyeria") { ?>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Tipo</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualOne"><?php echo $tendencia->tipo ?></p>
                                    </div>
                                    <?php if(!empty($tipos[3]->sortable[0]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleOne" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[3]->sortable[0]->valores); $i++) { ?>
                                                <option><?php echo $tipos[3]->sortable[0]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if($tendencia->nombre_vestido=="Zapatos") { ?>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Categoria</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualOne"><?php echo $tendencia->categoria ?></p>
                                    </div>
                                    <?php if(!empty($tipos[4]->sortable[0]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleOne" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[4]->sortable[0]->valores); $i++) { ?>
                                                <option><?php echo $tipos[4]->sortable[0]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if($tendencia->nombre_vestido=="Lencer├¡a") { ?>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Tipo</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualOne"><?php echo $tendencia->tipo ?></p>
                                    </div>
                                    <?php if(!empty($tipos[5]->sortable[0]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleOne" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[5]->sortable[0]->valores); $i++) { ?>
                                                <option><?php echo $tipos[5]->sortable[0]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if($tendencia->nombre_vestido=="Complementos") { ?>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Categoria</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualOne"><?php echo $tendencia->categoria ?></p>
                                    </div>
                                    <?php if(!empty($tipos[6]->sortable[0]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleOne" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[6]->sortable[0]->valores); $i++) { ?>
                                                <option><?php echo $tipos[6]->sortable[0]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group row" id="divtitle">
                                    <label class="col-form-label col-md-3" id="title">Tipo</label>
                                    <div class="col-md-4">
                                        <p>Actual</p>
                                        <p id="actualTwo"><?php echo $tendencia->tipo ?></p>
                                    </div>
                                    <?php if(!empty($tipos[6]->sortable[1]->valores[0])) { ?>
                                    <div class="col-md-4">
                                        <p>Nuevo</p>
                                        <select id="titleTwo" class="form-control" id="category">
                                            <option>Selecciona una opcion</option>
                                            <?php for($i=0; $i<sizeOf($tipos[6]->sortable[1]->valores); $i++) { ?>
                                                <option><?php echo $tipos[6]->sortable[1]->valores[$i]->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div class="form-group row hide">
                                <label class="col-form-label col-md-3">Tipo Vestido</label>
                                <div class="col-md-9">
                                    <input id="tipo_vestido" type="text" class="form-control m-b-5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Diseñador</label>
                                <div class="col-md-9">
                                    <input id="design" type="text" class="form-control m-b-5" value="<?php echo $tendencia->disenador ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Temporada</label>
                                <div class="col-md-9">
                                    <input id="temporada" type="text" class="form-control m-b-5" value="<?php echo $tendencia->temporada ?>" maxlength="4">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <p class="btn btn-success float-right" id="save">Guardar</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<!-- ================== END BASE JS ================== -->

    <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>

</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js") ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    var indicator=0;
    $(document).ready(function() {
        init_tinymce_mini('#detalle');

        $('.upload').each(function(i, elem) {
            var dropzone = new Dropzone(elem, {
                url: '<?php echo base_url() ?>admin/trends/editUpload',
                method: 'POST',
                paramName: 'files', // The name that will be used to transfer the file
                maxFilesize: 6, // MB
                uploadMultiple: false,
                createImageThumbnails: true,
                acceptedFiles: 'image/*',
                autoProcessQueue: false,
                dataType: 'json',
                accept: function(file, done) {
                    done();
                    indicator=1;
                },
                success:function (file) {
                    
                },
                error: function(data, xhr) {
                    
                },
                init: function() {
                    this.on('thumbnail', function(file) {
                        if (file.size < (1024 * 1024 * 1)) // not more than 1mb
                        {
                            $(".upload").parent().find('.archivo').val($(file.previewElement).find('img').attr('src'));
                        }
                    });
                    this.on("sending", function(file, xhr, formData) {
                        formData.append("detalle", $('#types').val());
                        formData.append("modelo", $('#model').val());
                        formData.append("descripcion", tinymce.get('detalle').getContent());
                        formData.append("titleOne", $('#titleOne').val());
                        formData.append("titleTwo", $('#titleTwo').val());
                        formData.append("actualOne", $('#actualOne').text());
                        formData.append("actualTwo", $('#actualTwo').text());
                        formData.append("disenador", $('#design').val());
                        formData.append("temporada", $('#temporada').val());
                        formData.append("indicator", indicator);
                        formData.append("id", '<?php echo $tendencia->id_vestido ?>');
                    });
                    this.on('success', function(file, response) {
                        swal('Correcto', 'Los datos fueron modificados', 'success');
                        setTimeout(function() {
                            location.reload();
                        }, 300);
                    });
                },
            });

            var prom = $(elem).parent().find('.archivo');
            if (prom) {// Create the mock file:
                var file = prom.attr('value');
                if (file != null && file != '') {
                    var cadena = file;
                    var inicio = 47;
                    var subCadena = cadena.substring(inicio);
                    var mockFile = {name: subCadena, size: 12345};
                    dropzone.emit('addedfile', mockFile);
                    dropzone.emit('thumbnail', mockFile, file);
                    dropzone.emit('complete', mockFile);
                    $(elem).find('text').hide();
                }
            }

            $("#save").on('click', function() {
                if(indicator==0) {
                    $.ajax({
                        dataType: 'json',
                        url: '<?php echo base_url()."admin/trends/editUpload" ?>',
                        method: 'post',
                        data: {
                            id: "<?php echo $tendencia->id_vestido ?>",
                            indicator: indicator,
                            actualOne: $('#actualOne').text(),
                            actualTwo: $('#actualTwo').text(),
                            titleOne: $('#titleOne').val(),
                            titleTwo: $('#titleTwo').val(),
                            detalle: $('#types').val(),
                            modelo: $('#model').val(),
                            disenador: $('#design').val(),
                            temporada: $('#temporada').val(),
                            descripcion: tinymce.get('detalle').getContent(),
                        },
                        success: function(response) {
                            swal('Correcto', 'Los datos fueron modificados', 'success');
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        },
                        error: function() {
                            console.log('error');
                        },
                    });
                } else {
                    var myDropzone = Dropzone.forElement(".upload");
                    myDropzone.processQueue();
                }
            });
        });

        $('.eliminar-foto').on('click', function(elm) {
            var $btn = $(elm.currentTarget);
            var $prom = $btn.parent();
            $prom.find('.upload div.dz-preview').remove();
            $prom.find('text').show();
            $prom.find('.archivo').val('');
            $prom.parent().find('.label-error').addClass('hide');
        });
    });

    function init_tinymce_mini(elem) {
        if (typeof tinymce != 'undefined') {
            tinymce.init({
                selector: elem,
                relative_urls: false,
                menubar: '',
                plugins: [
                    'advlist autolink lists link  preview anchor',
                    'searchreplace code ',
                    'insertdatetime paste ',
                ],
                statusbar: false,
                height: 350,
                imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58', 'clubnupcial.com'],
                browser_spellcheck: true,
                toolbar: 'insertfile undo redo  | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist ',
            });
        }
    }
</script>
</body>