<?php

$data['extraCSS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
    base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
    base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
                <li class="breadcrumb-item active">Tendencias</li>
            </ol>
            <!-- end breadcrumb -->
            <br><br><br>
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Tendencias</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                    <a class="btn" style="background: black; color: white;" href="<?php echo base_url() ?>admin/trends/nueva">Nueva</a>
                    <br><br>
                    <div>
                        <table class="table table-striped table-bordered" id="tableTendencias">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Diseñador</th>
                                <th>Estilo</th>
                                <th>Temporada</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                        <h4 id="barra" style="display: none">Cargando, por favor espere...</h4>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->


    <?php $this->view("admin/components/page-js", $data) ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            start_page();
        });

        function start_page(){
            cargarDatos();
        }

        function cargarDatos(){
            barra.style.display = "block";
            $('#tableTendencias').dataTable().fnDestroy();
            var table = $('#tableTendencias').DataTable({
                    // "processing":true,  
                    "serverSide":true, 
                    responsive: true,
                    "ajax":{  
                        url: '<?php echo base_url() ?>'+'admin/trends/fetch_trend',  
                        type:"POST"  
                    }, 
                    scrollX:        true,
                    "columnDefs":[  
                        // {  
                        //     "targets": 1,  
                        //     "render": function (data, type, row, meta){
                        //         var $select = $('<select class="form-control" id="tipo"><option id="basico" value="0">Basico</option><option value="1">Plata</option><option value="2">Oro</option><option value="3">Diamante</option></select>');
                        //         $select.find('option[value="'+data+'"]').attr('selected', 'selected');
                        //         return $select[0].outerHTML;
                        //     }  
                        // },  
                        {  
                            "targets": 5,  
                            "render": function (data, type, row, meta){
                                return '<a href="<?php echo base_url() ?>admin/trends/edit/'+row[0]+'"><button type="button" class="btn btn-info btn-sm">Editar</button></a> &nbsp; <span id="delete" class="btn btn-danger btn-sm" onclick="eliminar('+row[0]+')">Eliminar</span>';
                            }
                        },  
                    ],
                    language: {
                        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                    }
                });
                // $('#tableTendencias tbody').on( 'change', 'select', function (e) {
                //     var data = table.row( $(this).parents('tr') ).data();
                //     var options = $(this).find('option:selected').val();
                //     let id = this.id;
                //     if(data)
                //         tipoEstado(data[5], options, id);
                // });
                barra.style.display = "none";
        }

        function eliminar(id) {
            swal({
                title: 'Estas seguro que quieres borrar esta tendencia?',
                icon: 'error',
                buttons: ['Cancelar', true],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '<?php echo base_url() ?>'+'admin/trends/eliminar',
                        method: 'post',
                        //dataType: 'json',
                        data: {
                            id_tendencia: id,
                        },
                        success: function(response) {
                            start_page();
                            swal('Correcto', 'La tendencia fue eliminada', 'success');
                        },
                        error: function(e) {
                            console.log("ERROR");
                        },
                    });
                }
            });
        }

        // function tipoEstado(iduser, dato, tabla) {
        //     $.ajax({
        //         url: '<?php echo base_url() ?>'+'admin/proveedor/tipoEstado',
        //         method: 'post',
        //         //dataType: 'json',
        //         data: {
        //             id_user: iduser,
        //             date: dato,
        //             table: tabla,
        //         },
        //         success: function(response) {
        //             if(response.tiempo == true){
        //                 start_page();
        //             }
        //             // $('#articleTable').dataTable().fnDestroy(); 
        //             // cargarDatos();
        //         },
        //         error: function(e) {
        //             console.log("ERROR");
        //         },
        //     });
        // }

    </script>
    
</body>