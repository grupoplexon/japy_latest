<?php
$data['extraCSS'] = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.css',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.css',
        base_url().'dist/admin/assets/css/custom/users/edit/style.css',
        base_url().'dist/admin/assets/plugins/parsley/src/parsley.css',
        base_url().'dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.css',
        base_url().'dist/admin/assets/plugins/select2/dist/css/select2.min.css',
        base_url().'dist/admin/assets/css/custom/blog/style.css',
];
$data['extraJS']  = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.js',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.js',
        base_url().'dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/parsley.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/i18n/es.js',
];
?>
<?php $this->view("admin/components/head", $data) ?>

<style>
    .hide {
        display: none;
    }
    .upload:hover {
        border: 3px dashed #f9a897;
    }

    .upload {
        min-height: 150px;
        border: 3px dashed #f5e6df;
        cursor: pointer;
    }

    .dz-success-mark {
        display: none !important;
    }
    .dz-error-mark {
        display: none !important;
    }
</style>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/trends">Tendencia</a></li>
            <li class="breadcrumb-item active">Nuevo</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Nueva Tendencia</h1>
        <!-- end page-header -->
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        <h4 class="panel-title">Ingresar datos</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body panel-form">
                        <form id="contenido" class="form-horizontal form-bordered">
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Tipo de artículo</label>
                                <div class="col-md-4">
                                    <select name="" id="types" class="form-control">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Modelo</label>
                                <div class="col-md-9">
                                    <input id="model" type="text" class="form-control m-b-5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Descripción</label>
                                <div class="col-md-9">
                                <textarea id="detalle" class="detalle" name="detalle"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Imágen</label>
                                <div class="col-md-9">
                                    <section>
                                        <div class="card-panel z-depth-0 valign-wrapper upload " id="upload" style="text-align: center; padding-top: 8vh;">
                                            <text style="width: 100%;text-align: center;color: gray;" div="drop"><i class="fa fa-upload fa-2x valign "></i><br>
                                                Haga clic aqu&iacute; para cargar.
                                            </text>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="form-group row hide" id="divtitle">
                                <label class="col-form-label col-md-3" id="title"></label>
                                <div class="col-md-4">
                                    <select class="form-control" id="category">

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row hide" id="divtitle2">
                                <label class="col-form-label col-md-3" id="title2"></label>
                                <div class="col-md-4">
                                    <select class="form-control" id="types_two">

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row hide">
                                <label class="col-form-label col-md-3">Tipo Vestido</label>
                                <div class="col-md-9">
                                    <input id="tipo_vestido" type="text" class="form-control m-b-5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Diseñador</label>
                                <div class="col-md-9">
                                    <input id="design" type="text" class="form-control m-b-5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Temporada</label>
                                <div class="col-md-9">
                                    <input id="temporada" type="text" class="form-control m-b-5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <p class="btn btn-success float-right" id="save">Guardar</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<!-- ================== END BASE JS ================== -->

    <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>dist/js/tendencia/tendencia.js"></script>
    <!-- <script>tinymce.init({ selector:'textarea' });</script> -->

</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js") ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    var indicator = 0;
    $(document).ready(function() {
        init_tinymce_mini('#detalle');
        $.ajax({
            dataType: 'json',
            url: '<?php echo base_url()."admin/trends/extractTypes" ?>',
            method: 'post',
            data: {
                extract: "extraer"
            },
            success: function(response) {
                var crear="";
                var options = '<option>';
                options += 'Selecciona una opcion';
                options += '</option>';
                crear = crear + options;
                for(i=0; i<response.data.length; i++) {
                    options = '<option>';
                    options += response.data[i].nombre;
                    options += '</option>';
                    crear = crear + options;
                }
                document.getElementById("types").innerHTML = crear;
            },
            error: function() {
                console.log('error');
            },
        });
        $("#types").change(function() {
            if($('#types').val()=='Vestidos de Novia') {
                vestidNovia();
            }
            if($('#types').val()=='Complementos') {
                complementos();
            }
            if($('#types').val()=='Trajes de Novio') {
                trajeNovio();
            }
            if($('#types').val()=='Vestidos de Fiesta') {
                vestidoFiesta();
            }
            if($('#types').val()=='Joyeria') {
                joyeria();
            }
            if($('#types').val()=='Zapatos') {
                zapatos();
            }
            if($('#types').val()=='Lencer├¡a') {
                lenceria();
            }
        });
        $("#save").on('click', function() {
            if($('#types').val()!='Selecciona una opcion' && $('#model').val()!='' &&
            tinymce.get('detalle').getContent()!='' && $('#category').val()!='Selecciona una opcion' &&
            $('#design').val()!='' && $('#temporada').val()!='' && indicator==1) {
                var myDropzone = Dropzone.forElement(".upload");
                myDropzone.processQueue();
            } else {
                swal('Error', 'Por favor complete todos los campos', 'warning');
            }
        });

        $('#upload').dropzone({
        url: '<?php echo base_url() ?>admin/trends/upload',
        method: 'POST',
        paramName: 'files', // The name that will be used to transfer the file
        maxFilesize: 6, // MB
        maxFiles: 1,
        uploadMultiple: true,
        createImageThumbnails: true,
        acceptedFiles: 'image/*',
        autoProcessQueue: false,
        dataType: 'json',
        accept: function(file, done) {
            // opc = this;
            // opc.removeFile();
            done();
            indicator = 1;
        },
        success:function (file) {
            
        },
        error: function(data, xhr) {
            
        },
        init: function() {
            // var submitButton = document.querySelector("#save");
            // myDropzone = this;
            // submitButton.addEventListener("click", function() {
            //     myDropzone.processQueue();
            // });
            this.on("sending", function(file, xhr, formData) {
                formData.append("detalle", $('#types').val());
                formData.append("modelo", $('#model').val());
                formData.append("descripcion", tinymce.get('detalle').getContent());
                formData.append("titleOne", $('#category').val());
                formData.append("titleTwo", $('#types_two').val());
                formData.append("diseñador", $('#design').val());
                formData.append("temporada", $('#temporada').val());
                formData.append("idVestido", $('#tipo_vestido').val());
            });
            this.on('success', function(file, response) {
                swal('Correcto', 'Los datos fueron guardados', 'success');
                setTimeout(function() {
                    location.href ="<?php echo base_url() ?>admin/trends";
                }, 300);
            });
        },
    });

    });

    function init_tinymce_mini(elem) {
        if (typeof tinymce != 'undefined') {
            tinymce.init({
                selector: elem,
                relative_urls: false,
                menubar: '',
                plugins: [
                    'advlist autolink lists link  preview anchor',
                    'searchreplace code ',
                    'insertdatetime paste ',
                ],
                statusbar: false,
                height: 350,
                imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58', 'clubnupcial.com'],
                browser_spellcheck: true,
                toolbar: 'insertfile undo redo  | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist ',
            });
        }
    }
</script>
</body>