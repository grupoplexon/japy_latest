<?php
$data['extraCSS'] = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.css',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.css',
        base_url().'dist/admin/assets/css/custom/users/edit/style.css',
        base_url().'dist/admin/assets/plugins/parsley/src/parsley.css',
        base_url().'dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.css',
        base_url().'dist/admin/assets/plugins/select2/dist/css/select2.min.css',
        base_url().'dist/admin/assets/css/custom/blog/style.css',
];
$data['extraJS']  = [
        base_url().'dist/admin/assets/plugins/dropzone/min/dropzone.min.js',
        base_url().'dist/admin/assets/plugins/switchery/switchery.min.js',
        base_url().'dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/parsley.js',
        base_url().'dist/admin/assets/plugins/parsley/dist/i18n/es.js',
];
$isProvider = 0;
if(isset($provider)){
    $isProvider = 1;
}
?>
<?php $this->view("admin/components/head", $data) ?>


<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/users" ?>">Usuarios</a></li>
            <li class="breadcrumb-item active">Editar</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Usuario</h1>
        <!-- end page-header -->
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        <h4 class="panel-title">Informacion personal</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body panel-form">
                        <form id="personal" class="form-horizontal form-bordered">
                            <?php //echo base_url()."perfil/foto/".$user->id_usuario ?>
                            <!-- <div class="form-group row">
                                <label class="col-form-label col-md-3" style="margin:auto;">Imagen principal</label>
                                <div class="col-md-9">
                                    <div id="dropzone" >
                                        <div> <p>Arrastre una imagen o de click para seleccionar una</p> </div>
                                        <input type="file" accept="image/png, image/jpeg" />
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Estado</label>
                                <div class="col-md-9">
                                    <input id="status" type="checkbox" class="js-switch"
                                            <?php echo $user->activo ? "checked" : "" ?>/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">ID</label>
                                <div class="col-md-9">
                                    <input id="id" type="text" class="form-control m-b-5"
                                           value="<?php echo $user->id_usuario ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Fecha de creación</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control m-b-5"
                                           value="<?php echo $user->fecha_creacion ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Rol</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control m-b-5" disabled
                                           value="<?php echo $user->rol ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Nombre</label>
                                <div class="col-md-9">
                                    <input id="name" type="text" class="form-control m-b-5"
                                           value="<?php echo $user->nombre ?>" data-parsley-length="[2,100]"
                                           data-parsley-required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Apellido</label>
                                <div class="col-md-9">
                                    <input id="lastName" type="text" class="form-control m-b-5"
                                           value="<?php echo $user->apellido ?>" data-parsley-length="[2,30]">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Correo electronico</label>
                                <div class="col-md-9">
                                    <input id="email" type="text" class="form-control m-b-5"
                                           value="<?php echo $user->correo ?>" data-parsley-type="email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Usuario</label>
                                <div class="col-md-9">
                                    <input id="username" type="text" class="form-control m-b-5"
                                           value="<?php echo $user->usuario ?>" data-parsley-length="[2,30]"
                                           data-parsley-required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Contraseña</label>
                                <div class="col-md-9">
                                    <input id="password" type="password" class="form-control m-b-5"
                                           value="" data-parsley-length="[2,30]">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Confirmar contraseña</label>
                                <div class="col-md-9">
                                    <input id="password-confirm" type="password" class="form-control m-b-5"
                                           value="" data-parsley-length="[2,30]">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <p class="btn btn-success float-right" id="save">Guardar</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
            <div class="col-lg-6">
                    <?php if(($user->rol == "Proveedor" && ($provider != null)) ){ ?>
                <div class="col-lg-12">
                    <div class="panel panel-inverse">
                        <!-- begin panel-heading -->
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                            <h4 class="panel-title">Informacion de contacto</h4>
                        </div>
                        <!-- end panel-heading -->
                        <!-- begin panel-body -->
                        <div class="panel-body panel-form">
                            <form id="contacto" class="form-horizontal form-bordered" data-parsley-validate="true" novalidate>

                                <div class="form-group row">
                                    <label class="col-form-label col-md-3">Telefono</label>
                                    <div class="col-md-9">
                                        <input id="telefono" type="digits" class="form-control m-b-5"
                                            value="<?php echo $provider->contacto_telefono ?>" data-parsley-length="[2,30]">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-md-3">Celular</label>
                                    <div class="col-md-9">
                                        <input id="celular" type="digits" class="form-control m-b-5"
                                            value="<?php echo $provider->contacto_celular ?>" data-parsley-length="[2,30]"
                                            data-parsley-required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-md-3">Pagina web</label>
                                    <div class="col-md-9">
                                        <input id="pweb" type="text" class="form-control m-b-5"
                                            value="<?php echo $provider->contacto_pag_web ?>" data-parsley-length="[2,251]">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-md-3">Correos electronico de contacto</label>
                                    <div class="col-md-9">
                                        <textarea id="email_contacto" type="text" class="form-control m-b-5">
                                            <?php echo $provider->contacto_correos ?>
                                        </textarea>
                                        <small> ingrese los correos electronicos separados por comas</small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <p class="btn btn-success float-right" id="save-info">Guardar</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- end panel-body -->
                    </div>
                </div>
                <!-- <div class="col-lg-12">
                    <div class="panel panel-inverse">
                         begin panel-heading 
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                            <h4 class="panel-title">Describe tu empresa</h4>
                        </div>
                         end panel-heading 
                         begin panel-body
                        <div class="panel-body panel-form">
                            <form id="descripcion" class="form-horizontal form-bordered" data-parsley-validate="true" novalidate>
                            <div class="form-group row"></div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                    <textarea id="editor1" name="editor1" rows="20"><?php echo $provider->descripcion ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-success float-right" value="Guardar">
                                    </div>
                                </div>
                            </form>
                        </div>
                         end panel-body 
                    </div> 
                </div> -->
            </div>
            <div class="col-lg-12">
            </div>
            <?php }?>
            <div class="col-lg-12">
                <div class="panel panel-inverse">
                    <!-- FAQS BEGIN -->    
                    <!-- begin panel-heading -->
                    <!-- <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Preguntas frecuentes</h4>
                    </div> -->
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <!-- <div class="panel-body panel-form">
                        <form id="faqs" class="form-horizontal form-bordered" data-parsley-validate="true" novalidate>

                        <div class="form-group row">
                            <label class="col-form-label col-md-3">¿Cuál es el precio de tu servicio?</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <label class="col-form-label col-md-3">Precio Minimo</label>
                                    <div class="col-md-9">
                                        <input id="precioMinimo" type="number" class="form-control m-b-5" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-form-label col-md-3">Precio Maximo</label>
                                    <div class="col-md-9">
                                        <input id="precioMaximo" type="number" class="form-control m-b-5" value="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-md-3">¿Qué servicios ofrece?</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4"></div>
                                </div>
                                <input id="servicio" type="text" class="form-control m-b-5" value="Checkboxes">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-md-3">¿Qé incluye el paquete de novia?</label>
                            <div class="col-md-9">
                                <textarea id="paqueteNovia" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">¿Cuál es tu actividad principal?</label>
                                <div class="col-md-9">
                                    <select class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white">
                                        <option value="" selected>Select a Country</option>
                                        <option value="AF">Afghanistan</option>
                                        <option value="AL">Albania</option>
                                        <option value="DZ">Algeria</option>
                                        <option value="AS">American Samoa</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilla</option>
                                        <option value="AQ">Antarctica</option>
                                        <option value="AG">Antigua and Barbuda</option>
                                    </select>
                                </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-success float-right" value="Guardar">
                                </div>
                            </div>
                        </form>
                    </div> -->
                    <!-- end panel-body -->
                    <!-- FAQS END -->    
                </div>
            </div>
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<!-- ================== END BASE JS ================== -->

</div>
<input type="hidden" id="baseURL" value="<?php echo base_url() ?>">
<!-- end page container -->
<?php $this->view("admin/components/page-js") ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        $('#save').on('click', function() {
            if($('#password').val()==$('#password-confirm').val()) {
                $.ajax({
                    dataType: 'json',
                    url: '<?php echo base_url()."admin/users/editUser" ?>',
                    method: 'post',
                    data: {
                        id: $('#id').val(),
                        status: $('#status').is(':checked'),
                        name: $('#name').val(),
                        lastName: $('#lastName').val(),
                        username: $('#username').val(),
                        email: $('#email').val(),
                        password: $('#password').val(),
                    },
                    success: function(response) {
                        swal('Correcto', 'Los datos se guardaron correctamente', 'success');
                    },
                    error: function() {
                        console.log('error');
                    },
                });
            } else {
                swal('Error', 'Las contraseñas no coinciden', 'error');
            }
        });

        $('#save-info').on('click', function() {
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."admin/users/editInfo" ?>',
                method: 'post',
                data: {
                    id: $('#id').val(),
                    telefono: $('#telefono').val(),
                    celular: $('#celular').val(),
                    pWeb: $('#pweb').val(),
                    email: $('#email_contacto').val(),
                },
                success: function(response) {
                    swal('Correcto', 'Los datos se guardaron correctamente', 'success');
                },
                error: function() {
                    console.log('error');
                },
            });
        });
    });
</script>
</body>
