<?php

$data['extraCSS'] = [
        base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
        base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
        base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
        base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
        base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->
<!-- begin #page-container -->
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <?php $this->view("admin/components/header") ?>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <?php $this->view("admin/components/sidebar") ?>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
            <li class="breadcrumb-item active">Usuarios</li>
        </ol>
        <!-- end breadcrumb -->
        <div class="panel panel-inverse">
            <!-- begin panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title">Usuarios</h4>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                <table class="table table-striped table-bordered" id="usersTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>Rol</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <td><?php echo $user->id_usuario ?></td>
                            <td><?php echo $user->nombre.' '.$user->apellido ?></td>
                            <td><?php echo $user->usuario ?></td>
                            <td><?php echo $user->rol ?></td>
                            <td><?php echo $user->activo ? "Activo" : "Inactivo" ?></td>
                            <td class="with-btn" nowrap="">
                                <a href="<?php echo base_url()."admin/users/edit/".$user->id_usuario ?>"
                                   class="btn btn-sm btn-primary width-60 m-r-2">Editar</a>
                                <a href="#" class="btn btn-sm btn-danger width-70">Eliminar</a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <!-- end panel-body -->
        </div>
    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->
<?php $this->view("admin/components/page-js", $data) ?>
<script>
    $(document).ready(function () {
        $('#usersTable').DataTable({
            responsive: true,
            columnDefs: [{
                "targets": 5,
                "orderable": false
            }],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        });
    });
</script>
</body>
