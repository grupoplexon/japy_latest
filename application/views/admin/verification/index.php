<?php
$data['extraCSS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
];

$data['extraJS'] = [
    base_url()."dist/admin/assets/plugins/DataTables/media/js/jquery.dataTables.js",
    base_url()."dist/admin/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
    base_url()."dist/admin/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
    base_url()."dist/admin/assets/js/demo/table-manage-default.demo.min.js",
];

?>
<?php $this->view("admin/components/head", $data) ?>
<style>
    .modal-dialog {
        max-width: 870px !important;
        margin: 1.75rem auto;
    }
</style>
<body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <!-- begin #header -->
        <?php $this->view("admin/components/header") ?>
        <!-- end #header -->

        <!-- begin #sidebar -->
        <?php $this->view("admin/components/sidebar") ?>
        <!-- end #sidebar -->

        <!-- begin #content -->
        <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb pull-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url()."admin/home" ?>">Home</a></li>
                <li class="breadcrumb-item active">Verificaciones</li>
            </ol>
            <!-- end breadcrumb -->
            <br><br><br>
            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Notificaciones</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered" id="verificationTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Mensaje</th>
                            <th>Aprobacion</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php foreach ($verification as $verif) : ?> 
                            <?php if($verif->tipo == 0) { ?>
                            <tr>
                                <td><?php echo $verif->nombre_prov ?></td>
                                <td>DESCRIPCIÓN: <?php echo $verif->descripcion ?></td>

                                <td style="width:20%">
                                    <a href="#" id="aceptar" onclick="updateVer(<?php echo $verif->id ?>);" class="btn btn-sm btn-primary width-60 m-r-2">Aceptar</a>
                                    <a href="#" id="cancelar" onclick="updateVerDenegada(<?php echo $verif->id ?>);" class="btn btn-sm btn-danger width-70">Cancelar</a>
                                    </td>
                            </tr>
                            <?php } ?>
                            <?php if($verif->tipo == 1) { ?>
                            <tr>
                                <td><?php echo $verif->nombre_prov ?></td>
                                <td>CAMBIO DE PRECIOS: <br> <?php echo $verif->precios ?></td>

                                <td style="width:20%">
                                    <a href="#" id="aceptar" onclick="updateVerPrecios(<?php echo $verif->id ?>);" class="btn btn-sm btn-primary width-60 m-r-2">Aceptar</a>
                                    <a href="#" id="cancelar" onclick="updateVerDenegada(<?php echo $verif->id ?>);" class="btn btn-sm btn-danger width-70">Cancelar</a>
                                    </td>
                            </tr>
                            <?php } ?>
                        <?php endforeach ?> 
                        </tbody>
                    </table>
                </div>
                <!-- end panel-body -->
            </div>

            <div class="panel panel-inverse">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Fotos cargadas</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                    <table class="table table-striped table-bordered" id="articleTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Mensaje</th>
                            <th>Revisar</th>
                            <th>Aprovación</th>
                        </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
                <!-- end panel-body -->
            </div>
        </div>
        
        <!-- Large modal -->
        <div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body" id="modalImg">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>
            
        </div>
        <!-- end #content -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i
                    class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->
    <?php $this->view("admin/components/page-js", $data) ?>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
    
</body>
<script src="../dist/js/jquery.fancyzoom.js"></script>
<script>

    function updateVer(id){
            $.ajax({
                            url: "<?php echo base_url() ?>admin/verif/updateVerification",
                            method: "POST",
                            data: {
                            id2: id,
                    },
                    success: function(response) {
                        console.log("SUCCES");
                        location.reload();
                    },
                    error: function(e) {
                        console.log("ERROR");
                    },



                    });
                  
        };
        function updateVerPrecios(id){
            $.ajax({
                            url: "<?php echo base_url() ?>admin/verif/updatePrecios",
                            method: "POST",
                            data: {
                            id2: id,
                    },
                    success: function(response) {
                        console.log("SUCCES");
                        location.reload();
                    },
                    error: function(e) {
                        console.log("ERROR");
                    },



                    });
                  
        };
        function updateVerDenegada(id){
            $.ajax({
                            url: "<?php echo base_url() ?>admin/verif/updateVerificationDenegada",
                            method: "POST",
                            data: {
                            id2: id,
                    },
                    success: function(response) {
                        console.log("SUCCES");
                        location.reload();
                    },
                    error: function(e) {
                        console.log("ERROR");
                    },
                    });
                    
        };

        $(document).ready(function () {
            $("a.zoomImage").fancyZoom();
            cargarDatos();
        $('#verificationTable').DataTable({
            responsive: true,
            columnDefs: [{
                // "targets": 3,
                "orderable": false
            }],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            }
        });
    });

    function cargarDatos() {
        $.ajax({
            url: '<?php echo base_url() ?>'+'admin/verif/extraerDatos',
            method: 'post',
            dataType: 'json',
            data: {
                status: "prueba",
            },
            success: function(response) {
                var table = $('#articleTable').DataTable({
                    // responsive: true,
                    data: response.alerts.galeria,
                    columns: [
                        {data: "id_user"},
                        {data: "name"},
                        {data: "msj"},
                    ],
                    "columnDefs":[ 
                        {  
                            "targets": 3,  
                            "render": function (data, type, row, meta){
                                return '<button class="btn" style="background: #FFFF71; font-weight: normal;" id="'+row.id_user+'">Ver fotos</button>';
                            }
                        },
                        {  
                            "targets": 4,  
                            "render": function (data, type, row, meta){
                                return '<button class="btn btn-sm btn-primary width-60 m-r-2" id="activate">Activar</button>';
                            }
                        },  
                    ],
                    language: {
                        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                    }
                });

                $('#articleTable tbody').on( 'click', 'button', function () {
                    // alert('ENTRE');
                    var x = $(this).attr("id");
                    if(x == 'activate') {
                        var data = table.row( $(this).parents('tr') ).data();
                        activacion(data.id);
                    } else {
                        mostrarFotos(x);
                    }
                });
            },
            error: function(e) {
                console.log("ERROR");
            },
        });
    }
    
    function mostrarFotos(x) {
        var comentario = '';
        $.ajax({
            url: '<?php echo base_url() ?>'+'admin/verif/mostrarFotos',
            method: 'post',
            //dataType: 'json',
            data: {
                id_user: x,
            },
            success: function(response) {
                for (i = 0; i < response.images.length; i++) {
                    // console.log(response.images[i]['url']);
                    var nombre = '<a class="zoomImage" target="_blank" href="'+response.images[i]['url']+'">'+
                    '<img class="materialboxed" style="width: 200px; height: 200px; margin-right: 10px;" src="'+response.images[i]['url']+'"></a>';
                    comentario = comentario + nombre;
                }
                document.getElementById("modalImg").innerHTML = comentario;
                $("#myModal").modal();
            },
            error: function(e) {
                console.log("ERROR");
            },
        });
    }

    function activacion(id) {
        $.ajax({
            url: '<?php echo base_url() ?>'+'admin/verif/cambiarEstados',
            method: 'post',
            //dataType: 'json',
            data: {
                id_user: id,
            },
            success: function(response) {
                $('#articleTable').dataTable().fnDestroy(); 
                cargarDatos();
            },
            error: function(e) {
                console.log("ERROR");
            },
        });
    }

</script>