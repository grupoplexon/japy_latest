<html>
    <head>
        <?php $this->view("admingaleria/header") ?>
        <style>
            .card-title{
                font-size: 14px !important;
            }
            .imagen img{
                width: auto !important;
                height: 150px;
                margin: 0 auto;
            }
            .imagen div.card-image{
                background: #8C8D8F;
            }

            .imagen{
                transition: 1s;
            }

            .imagen.delete{
                opacity: 0;
                transform: scale(0.1,0.1);
            }
            .imagen.dz-error{
                display: none;
            }
			.upload{
    min-height: 150px;
    border: 3px dashed #f5e6df;
    cursor: pointer;
}

.upload:hover{
    border: 3px dashed #f9a897;
}

        </style>
    </head>
    <body>
        <?php $this->view("admingaleria/menu") ?>
        <div class="row">
        <div class="col s1 m1 l">
		&nbsp;
		</div>
        <div class="col s10 m10 l10">
                    <section>
                        <h5 class="center">Agregar &aacute;lbum</h5>
                    </section>
					<section>
						<div class="col s12 m12">
                                <p >
                                    <label>Nombre del &aacute;lbum</label> 
                                    <input class="form-control " id="nombre_album"  required="" maxlength="200" value="">
                                </p>
                                <p >
									<label>Descripci&oacute;n del &aacute;lbum</label> 
                                    <textarea class="descripcion editable" id="descripcion_album"></textarea> 
                                </p>
									<div id="mensaje-error2" class=" hide card-panel red lighten-2 white-text">
										<i class="fa fa-warning"></i> Ocurrio un error favor de volver a intentar
									</div>
									<div id="mensaje-error3" class=" hide card-panel red lighten-2 white-text">
										<i class="fa fa-warning"></i> Los campos no deben estar vacios.
									</div>
                                <p>
                                    <button type="submit" onclick="addAlbum()" id="guardar" class="btn dorado-2 pull-right">
                                        Guardar
                                    </button>
                                </p>
                          </div>
					</section>
                    
                </div>
				
			
            </div>
		<div class="row">
        <div class="col s1 m1 l">
		&nbsp;
		</div>
        <div class="col s10 m10 l10">
                    <section >
                        <div style="display:none" class="card-panel z-depth-0 valign-wrapper upload " id="upload" >
                            <text  style="width: 100%;text-align: center;color: gray;"><i class="fa fa-upload fa-2x valign "></i><br>
                            Suelte los archivos o haga clic aqu&iacute; para cargar.
                            </text>
                        </div>
                        <div class="container-preview">
                        </div>
                    </section>
                    <section >
                        <div style="display:none" id="mensaje-error" class=" hide card-panel red lighten-2 white-text">
                            <i class="fa fa-warning"></i> Esta imagen no es valida, verifica que no sobrepase el tama&ntilde;o m&aacute;ximo y su nombre no sea mayor a 8 caracteres.
                        </div>
                    </section>
                    <section class="container-imagenes">
                       
                            <!--<?php foreach ($foto as $key => $imagen) { ?>
                                <div id="<?php echo $imagen->id_galeria ?>" class="imagen col s12 m4 l3">
                                    <div class="card">
                                        <div class="card-image waves-effect waves-block waves-light">
                                            <img class="activator" src="<?php echo $imagen->url ?>">
                                        </div>
                                        <div class="card-content">
                                            <span class="card-title activator grey-text text-darken-4 truncate">
                                                <?php echo $imagen->nombre ?>
                                                <i class="material-icons right" style="position: absolute;right: 10px;">more_vert</i>
                                            </span>
                                        </div>
                                        <div class="card-reveal" style="overflow: hidden">
                                            <span class="card-title grey-text text-darken-4">Editar<i class="material-icons right">close</i></span>
                                            <p><input data-id="<?php echo $imagen->id_galeria ?>"  class="form-control nombre" name="nombre" value="<?php echo $imagen->nombre ?>"></p>
                                            <p  class="check-logo">
                                                <input  name="logo" <?php echo $imagen->logo == 1 ? "checked" : "" ?> type="radio" id="check-logo-<?php echo $imagen->id_galeria ?>" />
                                                <label for="check-logo-<?php echo $imagen->id_galeria ?>">Logotipo</label>
                                            </p>
                                            <p  class="check-principal" >
                                                <input name="principal" <?php echo $imagen->principal == 1 ? "checked" : "" ?> type="radio" id="check-foto-<?php echo $imagen->id_galeria ?>" />
                                                <label for="check-foto-<?php echo $imagen->id_galeria ?>">Foto Principal</label>
                                            </p>
                                            <p>
                                                <button data-id="<?php echo $imagen->id_galeria ?>" class="btn-flat btn-block delete red white-text" style="width: 104%">
                                                    Eliminar
                                                </button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        -->
                    </section>
                    <p>
                                    <a href="<?php echo base_url()?>index.php/ADGaleria" id="regresar" class="btn hide dorado-2 pull-right">
                                        Regresar
                                    </a>
                                </p>
                </div>
				
			
            </div>

        <div id="template-imagen" class="template hide">
            <div class="imagen col s12 m4 l3 dz-preview dz-file-preview">
                <div class="card">
                    <div class=" dz-details card-image waves-effect waves-block waves-light">
                        <img class="activator"  data-dz-thumbnail>
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4 truncate" >
                            <text data-dz-name></text>
                            <i class="material-icons right"   style="position: absolute;right: 10px;">more_vert</i>
                        </span>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">
                            Editar
                            <i class="material-icons right">close</i>
                        </span>
                        <p><input class="form-control nombre" name="nombre" value="" data-dz-name ></p>
                        <p  class="check-principal" >
                            <input name="principal" type="radio" id="check-foto-" />
                            <label for="check-foto-">Foto Principal</label>
                        </p>
                        <p>
                            <button class="btn-flat btn-block delete red white-text" style="width: 104%">
                                Eliminar
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <?php $this->view("principal/view_footer") ?>
        <?php $this->view("admingaleria/footer") ?>
    </body>
	<script>
			$(document).ready(function () {
                init_tinymce_mini("textarea.editable");
            });
			
			var id_album = 0;	
			
			var addAlbum = function(){
				$("#mensaje-error2").addClass('hide');
				$("#mensaje-error3").addClass('hide');
				if($('#nombre_album').val()!="" && tinyMCE.get('descripcion_album').getContent()!=""){
				$.ajax({
                url:"<?php echo base_url()?>index.php/ADGaleria/Home/agregarAlbum",
                method:"POST",
                data:{
                    nombre: $('#nombre_album').val(),
                    descripcion: tinyMCE.get('descripcion_album').getContent(),
                },
                success: function(res){					
                    if(res.success){						
						$('#upload').show();
						$('#guardar').hide();
						$('#regresar').removeClass('hide');
						id_album = res.data;
						$("#upload").dropzone({
                    url: "<?php echo base_url() ?>index.php/ADGaleria/home/fotos?id_album="+id_album,
                    previewTemplate: document.getElementById('template-imagen').innerHTML,
                    method: "POST",
                    paramName: "files", // The name that will be used to transfer the file
                    maxFilesize: 1, // MB
                    uploadMultiple: false,
                    createImageThumbnails: true,
                    accept: function (file, done) {
                        console.log(file.status);
                        $(file.previewElement).find(".delete").on("click", delete_img);
                        $(file.previewElement).find(".nombre").on("change", update_img);
                        $(file.previewElement).find(".check-principal input").on("click", update_principal);
                        $(file.previewElement).find(".nombre").val(file.name);


                        $("#mensaje-error").addClass("hide");
                        if (file.type.indexOf("image") < 0) {
                            done("Naha, you don't.");
                        } else {
                            done();
                        }
                        setTimeout(function () {
                            try {
                                var resp = JSON.parse(file.xhr.response);
                                $(file.previewElement).attr("id", resp.data.id);
                                $(file.previewElement).find(".delete").attr("data-id", resp.data.id);
                                $(file.previewElement).find(".nombre").attr("data-id", resp.data.id);
                                $(file.previewElement).find(".check-principal input").attr("id", "check-foto-" + resp.data.id);
                                $(file.previewElement).find(".check-principal label").attr("for", "check-foto-" + resp.data.id);
                            } catch (e) {

                            }
                        }, 1000);
                    },
                    error: function (data) {
                        console.log("data error", data);
                        $(data.previewElement).remove();
                        $("#mensaje-error").removeClass("hide");
                        setTimeout(function () {
                            $("#mensaje-error").addClass("hide");
                        }, 40000);

                    },
                    acceptedFiles: "image/*",
                    init: function () {
                        $("#up");
                    },
                    previewsContainer: ".container-preview"
					});
                $("#upload text").on("click", function (e) {
                    $(e.currentTarget.parentNode).trigger("click");
                });
                $(".imagen .delete").on("click", delete_img);
                $(".imagen .nombre").on("change", update_img);
                $(".imagen .check-principal input").on("click", update_principal);

            function update_principal(elem) {
                var e = elem.currentTarget.parentElement.parentElement.parentElement.parentElement;
                var id = $(e).attr("id");
                $.ajax({
                    url: "<?php echo base_url() ?>index.php/ADGaleria/home/fotos?id_album="+id_album,
                    method: "POST",
                    data: {
                        id: id,
                        method: "UPDATE_PRINCIPAL"
                    },
                    success: function (resp) {
                    },
                    error: function () {
                    }
                });
            }

            function update_img(elem) {
                var $e = $(elem.currentTarget);
                var id = $e.attr("data-id");
                $.ajax({
                    url: "<?php echo base_url() ?>index.php/ADGaleria/home/fotos?id_album="+id_album,
                    method: "POST",
                    data: {
                        id: id,
                        method: "UPDATE",
                        nombre: $e.val()
                    },
                    success: function (resp) {
                        $e.parent().addClass("has-success");
                        setTimeout(function () {
                            $e.parent().removeClass("has-success");
                        }, 1500);
                        $("#" + id + " .card-title.activator").html($e.val() + '<i class="material-icons right">more_vert</i>');
                    },
                    error: function () {
                        $e.parent().addClass("has-error");
                        setTimeout(function () {
                            $e.parent().removeClass("has-error");
                        }, 1500);
                    }
                });
            }

            function delete_img(elem) {
                if (confirm("¿Estas seguro de eliminar la imagen?")) {
                    var e = elem.currentTarget;
                    var id = $(e).data("id");
                    $.ajax({
                        url: "<?php echo base_url() ?>index.php/ADGaleria/home/fotos?id_album="+id_album,
                        method: "POST",
                        data: {
                            id: id,
                            method: "DELETE"
                        },
                        success: function (resp) {
                            var img = $("#" + id);
                            img.addClass("delete");
                            setTimeout(function () {
                                $("#" + id).remove();
                            }, 1050);

                        },
                        error: function () {
                            console.log("error");
                        }
                    });
                }
            }
                    }
                },
                error: function(){
                    $("#mensaje-error2").removeClass('hide');
                }
			});}
			else{
				$("#mensaje-error3").removeClass('hide');
			}
				
			}
            
            
        </script>
</html>
