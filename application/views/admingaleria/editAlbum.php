<html>
    <head>
        <?php $this->view("admingaleria/header") ?>
        <style>
            .card-title{
                font-size: 14px !important;
            }
            .imagen img{
                width: auto !important;
                height: 150px;
                margin: 0 auto;
            }
            .imagen div.card-image{
                background: #8C8D8F;
            }

            .imagen{
                transition: 1s;
            }

            .imagen.delete{
                opacity: 0;
                transform: scale(0.1,0.1);
            }
            .imagen.dz-error{
                display: none;
            }
			.upload{
    min-height: 150px;
    border: 3px dashed #f5e6df;
    cursor: pointer;
}

.upload:hover{
    border: 3px dashed #f9a897;
}

        </style>
    </head>
    <body>
        <?php $this->view("admingaleria/menu") ?>
        <div class="row">
        <div class="col s1 m1 l">
		&nbsp;
		</div>
        <div class="col s10 m10 l10">
                    <section>
                        <h5 class="center">Editar &aacute;lbum</h5>
                    </section>
					<section>
						<div class="col s12 m12">
                                <p >
                                    <label>Nombre del &aacute;lbum</label> 
                                    <input class="form-control " id="nombre_album"  required="" maxlength="200" value="<?php echo $album->nombrealbum?>">
                                </p>
                                <p >
									<label>Descripci&oacute;n del &aacute;lbum</label> 
                                    <textarea class="descripcion editable" id="descripcion_album"><?php echo $album->descripcion?></textarea> 
                                </p>
									<div id="mensaje-error2" class=" hide card-panel red lighten-2 white-text">
										<i class="fa fa-warning"></i> Ocurrio un error favor de volver a intentar
									</div>
									<div id="mensaje-error3" class=" hide card-panel red lighten-2 white-text">
										<i class="fa fa-warning"></i> Los campos no deben estar vacios.
									</div>
									<div id="mensaje-success" class=" hide card-panel green lighten-2 white-text">
										<i class="fa fa-thumbs-o-up"></i> Se edito correctamente
									</div>
                                <p>
                                    <button type="submit" onclick="editAlbum()" id="guardar" class="btn dorado-2 pull-right">
                                        Editar
                                    </button>
                                </p>
                          </div>
					</section>
                    
                </div>
				
			
            </div>
		<div class="row">
        <div class="col s1 m1 l">
		&nbsp;
		</div>
        <div class="col s10 m10 l10">
                    <section >
                        <div class="card-panel z-depth-0 valign-wrapper upload " id="upload" >
                            <text  style="width: 100%;text-align: center;color: gray;"><i class="fa fa-upload fa-2x valign "></i><br>
                            Suelte los archivos o haga clic aqu&iacute; para cargar.
                            </text>
                        </div>
                        <div class="container-preview">
                        </div>
                    </section>
                    <section >
                        <div style="display:none" id="mensaje-error" class=" hide card-panel red lighten-2 white-text">
                            <i class="fa fa-warning"></i> Esta imagen no es valida, verifica que no sobrepase el tama&ntilde;o m&aacute;ximo y su nombre no sea mayor a 8 caracteres.
                        </div>
                    </section>
                    <section class="container-imagenes">
							<?php if(isset($fotos)) {?>
                            <?php foreach ($fotos as $key => $imagen) { ?>
                                <div id="<?php echo $imagen->id_photo ?>" class="imagen col s12 m4 l3">
                                    <div class="card">
                                        <div class="card-image waves-effect waves-block waves-light">
                                            <img class="activator" src="<?php echo "data:" . $imagen->mime . ';' . "base64," . base64_encode($imagen->imagen); ?>">
                                        </div>
                                        <div class="card-content">
                                            <span class="card-title activator grey-text text-darken-4 truncate">
                                                <?php echo $imagen->nombre ?>
                                                <i class="material-icons right" style="position: absolute;right: 10px;">more_vert</i>
                                            </span>
                                        </div>
                                        <div class="card-reveal" style="overflow: hidden">
                                            <span class="card-title grey-text text-darken-4">Editar<i class="material-icons right">close</i></span>
                                            <p><input data-id="<?php echo $imagen->id_photo ?>"  class="form-control nombre" name="nombre" value="<?php echo $imagen->nombre ?>"></p>
                                            <p  class="check-principal" >
                                                <input name="principal" <?php echo $imagen->tipo == 1 ? "checked" : "" ?> type="radio" id="check-foto-<?php echo $imagen->id_photo ?>" />
                                                <label for="check-foto-<?php echo $imagen->id_photo ?>">Foto Principal</label>
                                            </p>
                                            <p>
                                                <button data-id="<?php echo $imagen->id_photo ?>" class="btn-flat btn-block delete red white-text" style="width: 104%">
                                                    Eliminar
                                                </button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php } ?>
                        
                    </section>
                    
                </div>
				
			
            </div>
			<div class="row">
        <div class="col s1 m1 l">
		&nbsp;
		</div>
        <div class="col s10 m10 l10">
		<div style="margin: 10px; float: right">
             <button id="responder" type="button" class="btn waves-effect waves-light dorado-2"><i class="fa fa-reply-all" aria-hidden="true"></i> Responder</button>
        </div>
		</div>
		</div>
		
		<div class="row">
        <div class="col s1 m1 l">
		&nbsp;
		</div>
        <div class="col s10 m10 l10">
                    <div id="panel_comentarios" class="col s12 m12 pull-left"> 
                <?php 
                    if(!empty($comentarios)){
                        foreach ($comentarios as $comentario){
                            echo '<div class="row" id="res-'.$comentario->id_comentario.'">
                                    <div class="col s4 m2 pull-left" class="comentario_usuario" style="margin-top: -7px; text-align: center">
                                        <div class="card">
                                            <div class="card-image">';
                            
                            echo                    '<a href="'.base_url().'index.php/novios/comunidad/perfil/usuario/'.$comentario->id_usuario.'"><img class="foto_perfil" src="'.$comentario->foto_usuario.'"></a>
                                                
                                            </div>
                                        </div>';
                            echo        '
                                    </div>';
									echo '<div class="col s8 m10 grey lighten-5 z-depth-1" id="nuevo_comentario" style="float: right;">';
								
                            echo    '
                                        <div class="col s12 m12" style="padding-top:10px">
                                            <p ><a href="'.base_url().'index.php/novios/comunidad/perfil/usuario/'.$comentario->id_usuario.'" class="link_usuario2" style="margin-right:10px;color: #f9a797!important;" >'.$comentario->usuario.'</a> <text class="fecha_creacion">'.$comentario->fecha.'</text> </p>
                                        </div>
                                        <div class="col s12 m12 contenido_comentario">
                                        '.$comentario->comentario.'
                                        </div>
                                        <div>

                                        </div>';
                                        echo '<div class="col s12 m12 grey lighten-5 pull-left" style="border-top: 1px solid #999; margin-top:10px; margin-bottom:10px; padding-top:15px; padding-bottom:10px">
                                            <a class="responder_c clickable" data-id="comentario-'.$comentario->id_comentario.'"><i class="fa fa-angle-left" aria-hidden="true"></i> Responder</a>';
                             if(!empty($comentarios2)){                                          
                                foreach($comentarios2 as $respuesta){
                                    if($respuesta->parent == $comentario->id_comentario){
                                        echo '<a  style="margin-left:10px" onclick="mostrar(this.id)" class="mostrar clickable" id="respuesta-'.$comentario->id_comentario.'">Mostrar Comentarios</a>';
										if(isset($comentarioo->comentado) && $comentarioo->comentado == $respuesta->comentado){
											$id_mostrar = '#respuesta-'.$comentario->id_comentario;
										}
                                        break;
                                    }
									else if($comentarios2[count($comentarios2)-1] == $respuesta){
											echo '<a onclick="mostrar(this.id)" class="mostrar clickable" style="display:none" id="respuesta-'.$comentario->id_comentario.'">Mostrar Comentarios</a>';
											
										}
                                }
                            }
							if($comentario->activo == 1) {
									echo            '<div style="float:right" id="desac-res"><button onclick="act_comen2('.$comentario->id_comentario.',1)" class="btn waves-effect waves-light dorado-2" >';
									echo '<i class="fa fa-minus-circle" class="" aria-hidden="true"></i> <text id="act">Desactivar</text>';
								} else {
									echo            '<div style="float:right" id="desac-res"><button onclick="act_comen2('.$comentario->id_comentario.',0)" class="btn waves-effect waves-light dorado-2" >';
									echo '<i class="fa fa-check-circle" class="" aria-hidden="true"></i> <text id="act">Activar</text>';
								}
                            echo            '</button> </div>
                                        </div>
                                    <div class="col s12 m12 grey lighten-5 pull-left" id="panel-respuesta'.$comentario->id_comentario.'" style="display:none; padding-top: 20px">

                                    </div>
                                    </div>                                
                                    </div>'; 
                        }
                    }else{
                        echo '<div class="row"> <div class="col s12 m9"><h5 class="existencia_c"> Aun no existen comentarios... </h5> </div></div>';
                    }
                ?>
            </div>
			<?php if($paginas>1) {?>
		<div class="row center">
			<ul class="pagination">
                    <?php if ($pagina == 1) { ?>
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="<?php base_url()?>?pagina=<?php echo $pagina-1?>"><i class="material-icons">chevron_left</i></a></li>
                    <?php } ?>
					<?php if($paginas < 8) { ?>
                    <?php for ($p = 1; $p <= $paginas; $p++) { ?>
                        <li class="waves-effect <?php echo ($p == $pagina ? "active" : "") ?>"><a href="<?php base_url()?>?pagina=<?php echo $p?>"><?php echo $p ?></a></li>
                    <?php } ?>					
                    <?php } else { $ini=0;?>
						<?php if ($pagina < 4) { ?>
						<?php for ($p = 1; $p <= 6; $p++) { ?>
                        <li class="waves-effect <?php echo ($p == $pagina ? "active" : "") ?>"><a href="<?php base_url()?>?pagina=<?php echo $p?>"><?php echo $p ?></a></li>
						<?php } ?>
						<?php } else { ?>
                        <li class="waves-effect"><a href="<?php base_url()?>?pagina=1">1</a></li>
						<li><a class="disable">...</a></li>
						<?php if($pagina+5 > $paginas) {$ini=$paginas-5;$pagina2=$paginas;}else{$ini=$pagina;$pagina2=$pagina+5;} ?>
						<?php for ($p = $ini; $p <= $pagina2; $p++) { ?>
                        <li class="waves-effect <?php echo ($p == $pagina ? "active" : "") ?>"><a href="<?php base_url()?>?pagina=<?php echo $p?>"><?php echo $p ?></a></li>
						<?php } ?>
						<?php } ?>
						<?php if($ini+5 != $paginas) {?>
                        <li><a class="disable">...</a></li>
                        <li class="waves-effect"><a href="<?php base_url()?>?pagina=<?php echo $paginas?>"><?php echo $paginas ?></a></li>
						<?php } ?>
                    <?php } ?>					
                    <?php if ($pagina == $paginas) { ?>
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                    <?php } else { ?>
                        <li class="waves-effect">
                            <a href="<?php base_url()?>?pagina=<?php echo $pagina+1?>">
                                <i class="material-icons">chevron_right</i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
        </div>
        <?php } ?>
            <!-- ------------------------FIN AREA DE COMENTARIOS--------------------------- -->
            
            <!-- -----------------------PAGINADOR DE COMENTARIOS--------------------------- -->
            
            <!-- -----------------------FIN PAGINADOR DE COMENTARIOS--------------------------- -->
            
            <!-- --------------------------EDITOR COMENTARIO---------------------------- -->
            <div class="col s12 m12 pull-left">
                <input type="hidden" id="respuesta">
                <div class="row">
                    <div class="col s12 m12" style="margin-bottom: 30px;">
                        <h5><b>Deja aqui tu comentario</b></h5>
                    </div>
                    <div class="col s12 m12 grey lighten-5 z-depth-1">
                        <div class="col s3 m1 pull-left">
                            <div class="row">
                                <div style="text-align: center">
                                    <div class="card" style="margin-top: 20px">
                                        <div class="card-image">
                                            <img class="foto_perfil" src="<?php if (!empty($foto_logeado)) echo $foto_logeado ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form method="post">
                            <div class="col s9 m11" style="margin-top: 20px; margin-bottom: 30px"> 
                                <div id="comentario">
                                    <textarea id="text_area" class="entorno_mensaje" name="mensaje" required> </textarea>
                                </div>
                            </div>
                            <div class="col s9 m11">
                                <div class="row" style="margin-bottom: 60px">
                                    <div class="col s12 m12">
                                        <button id="comentar" type="button" class="btn waves-effect waves-light dorado-2"><i class="fa fa-comment" aria-hidden="true"></i> Comentar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ---------------------------FIN EDITAR COMENTARIO---------------------------- -->
            
            
            <!-- ---------------   TEMPLATE NUEVO COMENTARIO ------------------------- -->
            <div class="hide" id="template-comentario">
        <div class="row">
            <div class="col s4 m2 pull-left" class="comentario_usuario" style="margin-top: -7px; text-align: center">
                <div class="card">
                    <div class="card-image">
                            <a class="link_usuario1"><img class="foto_perfil" src=""></a>
                    </div>
                </div>
            </div>
            <div class="col s8 m10 grey lighten-5 z-depth-1 nuevo_comen" id="nuevo_comentario" style="float: right;">
                <div class="col s12 m12" style="padding-top:10px">
                    <p ><a class="link_usuario2" style="margin-right:10px; color: #f9a797!important;" ></a> <text class="fecha_creacion"></text> </p>
                </div>
                <div class="col s12 m12 contenido_comentario">
                </div>
                <div>

                </div>
				<div class="col s12 m12 grey lighten-5 pull-left div-razon" style="display:none; text-align:right"><strong>Razon de la denuncia: </strong><text id="razon"></text> </div>
                <div class="col s12 m12 grey lighten-5 pull-left" style="border-top: 1px solid #999; margin-top:10px; margin-bottom:10px; padding-top:15px; padding-bottom:10px">
                    <a class="responder_c clickable"><i class="fa fa-angle-left" aria-hidden="true"></i> Responder</a>
                    <a class="mostrar clickable aMostrar">Mostrar Comentarios</a>
					<div style="float:right" id="desac-res"></div>
                </div>
                <div class="col s12 m12 grey lighten-5 pull-left panel-respuestas">

                </div>
            </div>                                
        </div>
    </div>
            <!-- ---------------   TEMPLATE NUEVO COMENTARIO ------------------------- -->
        </div>
    </div>
                    
                </div>
				
			
            </div>

        <div id="template-imagen" class="template hide">
            <div class="imagen col s12 m4 l3 dz-preview dz-file-preview">
                <div class="card">
                    <div class=" dz-details card-image waves-effect waves-block waves-light">
                        <img class="activator"  data-dz-thumbnail>
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4 truncate" >
                            <text data-dz-name></text>
                            <i class="material-icons right"   style="position: absolute;right: 10px;">more_vert</i>
                        </span>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">
                            Editar
                            <i class="material-icons right">close</i>
                        </span>
                        <p><input class="form-control nombre" name="nombre" value="" data-dz-name ></p>
                        <p  class="check-principal" >
                            <input name="principal" type="radio" id="check-foto-" />
                            <label for="check-foto-">Foto Principal</label>
                        </p>
                        <p>
                            <button class="btn-flat btn-block delete red white-text" style="width: 104%">
                                Eliminar
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <?php $this->view("principal/view_footer") ?>
        <?php $this->view("admingaleria/footer") ?>
    </body>
	<script>
			$(document).ready(function () {
                init_tinymce_mini("textarea.descripcion");
                init_tinymce_mini("textarea.entorno_mensaje");
            });
			
			var id_album = <?php echo $album->id_album?>;	
			
			var act_comen2 = function(comentario, activo) {
			console.log(comentario+" - "+ activo);
			var div = document.getElementById("res-"+comentario);
			
			$.ajax({
                url:"<?php echo base_url()?>index.php/ADGaleria/Home/desactivarComentario",
                method:"POST",
                data:{
                    album: id_album,
                    id_comentario: comentario,
					value: activo
                },
                success: function(res){
					
                    if(res.success){
						
							console.log("res-"+comentario);
						if(activo==1){
							$(div).find('#desac-res').empty();
							$(div).find('#desac-res').append('<button onclick="act_comen2('+comentario+',0)" value="0" class="btn waves-effect waves-light dorado-2" ><i class="fa fa-check-circle" class="" aria-hidden="true"></i> <text id="act">Activar</text></button>');
						}
						else{
							$(div).find('#desac-res').empty();
							$(div).find('#desac-res').append('<button onclick="act_comen2('+comentario+',1)" value="0" class="btn waves-effect waves-light dorado-2" ><i class="fa fa-minus-circle" class="" aria-hidden="true"></i> <text id="act">Desactivar</text></button>');
						}
                    }
                },
                error: function(){
                    alert('Lo sentimos ocurrio un error en el servidor');
                }
            });
		
        }
		
			var mostrar = function (id, mos = 0){
                var comentario = id;
                var comentario2 = comentario.split("-");
                var conca = "#panel-respuesta" + comentario2[1];
                if($('#'+id).text() == "Mostrar Comentarios" || mos == 1){
					
                    $.ajax({
                        url: '<?php echo base_url()?>index.php/ADGaleria/Home/getComent',
                        method: 'POST',
                        data:  {
                            comentario: comentario2[1]
                        },
                        success: function(res) {
                            if(res.success){
								console.log(res);
                                for(var aux in res.data){
                                    if(res.data.hasOwnProperty(aux)){
                                        var val = res.data[aux];
                                    }
                                }
                                var i;
                                for(i=0; i < val.length; i++){
                                    var div = document.createElement("div");
									$(div).attr('id','res-'+val[i].id_comentario);
                                    $(div).html($("#template-comentario").html());
									if(val[i].activo!=1){
										$(div).find('#desac-res').empty();
										$(div).find('#desac-res').append('<button onclick="act_comen2('+val[i].id_comentario+',0)" value="0" class="btn waves-effect waves-light dorado-2" ><i class="fa fa-check-circle" class="" aria-hidden="true"></i> <text id="act">Activar</text></button>');
										
										}
										else{
										$(div).find('#desac-res').empty();
										$(div).find('#desac-res').append('<button onclick="act_comen2('+val[i].id_comentario+',1)" value="0" class="btn waves-effect waves-light dorado-2" ><i class="fa fa-minus-circle" class="" aria-hidden="true"></i> <text id="act">Desactivar</text></button>');
										
										}
                                    $(div).find(".foto_perfil").attr("src", val[i].url_foto);
                                    $(div).find(".link_usuario1").attr("href", "<?php echo base_url() ?>index.php/novios/comunidad/perfil/usuario/"+val[i].id_usuario);
                                    $(div).find(".link_usuario2").attr("href", "<?php echo base_url() ?>index.php/novios/comunidad/perfil/usuario/"+val[i].id_usuario);
                                    $(div).find(".link_usuario2").html(val[i].usuario);
                                    $(div).find(".fecha_creacion").text(val[i].fecha_creacion);
                                    
									$(div).find(".aMostrar").hide();
									$(div).find(".contenido_comentario").html(val[i].comentario);
                                    $(div).find(".responder_c").attr("data-id","comentario-"+comentario2[1]);
                                    $(div).find(".denunciar_c").attr("data-id","denunciar-"+val[i].id_comentario);
                                    $(conca).append(div);
                                    $(".existencia_c").hide();
                                }
                            }
                        },
                        error: function(){
                            alert("Lo sentimos ha sucedido un error");
                        }
                    });
                    $(conca).empty();
                    $(conca).show();
                    $(conca).css("border-top","1px solid #999");
                    $('#'+id).text("Ocultar Comentarios");
                }
				else{
                    $('#'+id).text("Mostrar Comentarios");
                    $(conca).hide();
                    $(conca).empty();
                }
            }
			
			var editAlbum = function(){
				$("#mensaje-error2").addClass('hide');
				$("#mensaje-error3").addClass('hide');
				$("#mensaje-success").addClass('hide');
				if($('#nombre_album').val()!="" && tinyMCE.get('descripcion_album').getContent()!=""){
				$.ajax({
                url:"<?php echo base_url()?>index.php/ADGaleria/Home/editarAlbum",
                method:"POST",
                data:{
                    id: id_album,
                    nombre: $('#nombre_album').val(),
                    descripcion: tinyMCE.get('descripcion_album').getContent(),
                },
                success: function(res){					
                    if(res.success){						
							$("#mensaje-success").removeClass('hide');		
                    }
                },
                error: function(){
                    $("#mensaje-error2").removeClass('hide');
                }
			});}
			else{
				$("#mensaje-error3").removeClass('hide');
			}
				
			}
			
			$("#upload").dropzone({
                    url: "<?php echo base_url() ?>index.php/ADGaleria/home/fotos?id_album="+id_album,
                    previewTemplate: document.getElementById('template-imagen').innerHTML,
                    method: "POST",
                    paramName: "files", // The name that will be used to transfer the file
                    maxFilesize: 1, // MB
                    uploadMultiple: false,
                    createImageThumbnails: true,
                    accept: function (file, done) {
                        console.log(file.status);
                        $(file.previewElement).find(".delete").on("click", delete_img);
                        $(file.previewElement).find(".nombre").on("change", update_img);
                        $(file.previewElement).find(".check-principal input").on("click", update_principal);
                        $(file.previewElement).find(".nombre").val(file.name);


                        $("#mensaje-error").addClass("hide");
                        if (file.type.indexOf("image") < 0) {
                            done("Naha, you don't.");
                        } else {
                            done();
                        }
                        setTimeout(function () {
                            try {
                                var resp = JSON.parse(file.xhr.response);
                                $(file.previewElement).attr("id", resp.data.id);
                                $(file.previewElement).find(".delete").attr("data-id", resp.data.id);
                                $(file.previewElement).find(".nombre").attr("data-id", resp.data.id);
                                $(file.previewElement).find(".check-principal input").attr("id", "check-foto-" + resp.data.id);
                                $(file.previewElement).find(".check-principal label").attr("for", "check-foto-" + resp.data.id);
                            } catch (e) {

                            }
                        }, 1000);
                    },
                    error: function (data) {
                        console.log("data error", data);
                        $(data.previewElement).remove();
                        $("#mensaje-error").removeClass("hide");
                        setTimeout(function () {
                            $("#mensaje-error").addClass("hide");
                        }, 40000);

                    },
                    acceptedFiles: "image/*",
                    init: function () {
                        $("#up");
                    },
                    previewsContainer: ".container-preview"
					});
                $("#upload text").on("click", function (e) {
                    $(e.currentTarget.parentNode).trigger("click");
                });
                $(".imagen .delete").on("click", delete_img);
                $(".imagen .nombre").on("change", update_img);
                $(".imagen .check-principal input").on("click", update_principal);

				$("#responder").click(function (evt) {
                evt.preventDefault();
                $('html,body').animate({
                    scrollTop: $("#comentario").offset().top
                }, 2000);
                tinymce.execCommand('mceFocus', false, '#text_area');
            });
            function update_principal(elem) {
                var e = elem.currentTarget.parentElement.parentElement.parentElement.parentElement;
                var id = $(e).attr("id");
                $.ajax({
                    url: "<?php echo base_url() ?>index.php/ADGaleria/home/fotos?id_album="+id_album,
                    method: "POST",
                    data: {
                        id: id,
                        method: "UPDATE_PRINCIPAL"
                    },
                    success: function (resp) {
                    },
                    error: function () {
                    }
                });
            }

			$("#panel_comentarios").on("click", ".responder_c", function (evt) {
                evt.preventDefault();
                $('html,body').animate({
                    scrollTop: $("#comentario").offset().top
                }, 2000);
                tinymce.execCommand('mceFocus', false, '#text_area');
                var comentario = $(this).attr("data-id");
                var comentario2 = comentario.split("-");
                $("#respuesta").val(comentario2[1]);
            });
			
			 $("#comentar").click(function (evt) {
				
                evt.preventDefault();
                var mensaje = tinyMCE.activeEditor.getContent();
                if(mensaje != "" && mensaje != null){
                    var tipo = $("#respuesta").val();
                    if(tipo == "" || tipo == null){
						
                        $.ajax({
                            url: "<?php echo base_url() ?>index.php/ADGaleria/Home/addComenario",
                            method: "POST",
                            data: {
                                album: id_album,
                                mensaje: mensaje,
                            },
                            success: function (res) {
                                if (res.success) {
                                    var div = document.createElement("div");
									
									$(div).attr('id','res-'+res.data.id_comentario);
                                    $(div).html($("#template-comentario").html());
                                    if(res.data.activo!=1){
										$(div).find('#desac-res').empty();
										$(div).find('#desac-res').append('<button onclick="act_comen2('+res.data.id_comentario+',0)" value="0" class="btn waves-effect waves-light dorado-2" ><i class="fa fa-check-circle" class="" aria-hidden="true"></i> <text id="act">Activar</text></button>');
										
										}
										else{
										$(div).find('#desac-res').empty();
										$(div).find('#desac-res').append('<button onclick="act_comen2('+res.data.id_comentario+',1)" value="0" class="btn waves-effect waves-light dorado-2" ><i class="fa fa-minus-circle" class="" aria-hidden="true"></i> <text id="act">Desactivar</text></button>');
										
										}
                                    $(div).find(".foto_perfil").attr("src", res.data.foto_usuario);
                                    $(div).find(".aMostrar").attr('id', 'respuesta-'+res.data.id_comentario);
                                    $(div).find(".panel-respuestas").attr('id', 'panel-respuesta'+res.data.id_comentario);
                                    $(div).find('#respuesta-'+res.data.id_comentario).attr("onclick","mostrar('respuesta-"+res.data.mensaje+"',1)");
                                    $(div).find('#respuesta-'+res.data.id_comentario).hide();
									
                                    $(div).find(".link_usuario2").html(res.data.usuario);
                                    $(div).find(".fecha_creacion").text(res.data.fecha_creacion);
                                    $(div).find(".contenido_comentario").html(res.data.mensaje);
                                    $(div).find(".responder_c").attr("data-id","comentario-"+res.data.id_comentario);
                                    $(div).find(".denunciar_c").attr("data-id","denunciar-"+res.data.id_comentario);
                                    
                                    $("#panel_comentarios").append(div);
                                    tinyMCE.activeEditor.setContent("");
                                    $(".existencia_c").hide();
                                    $("#respuesta").val("");
                                }
                            },
                            error: function () {
                                alert('Lo sentimos ocurrio un error');
                            }
                        });
                    }else{
                        $.ajax({
                            url: "<?php echo base_url() ?>index.php/ADGaleria/Home/respuestaComentario",
                            method: "POST",
                            data: {
                                album: id_album,
                                mensaje: mensaje,
                                respuesta: tipo
                            },
                            success: function (res) {
                                if (res.success) {
									$('#respuesta-'+res.data.mensaje).attr("onclick","mostrar('respuesta-"+res.data.mensaje+"',1)");
                                    $('#respuesta-'+res.data.mensaje).trigger('click');
                                    $('#respuesta-'+res.data.mensaje).show();
									tinyMCE.activeEditor.setContent("");
                                    $(".existencia_c").hide();
                                    $("#respuesta").val("");
									$('#respuesta-'+res.data.mensaje).attr("onclick","mostrar('respuesta-"+res.data.mensaje+"')");
								}
                            },
                            error: function () {
                                alert('Lo sentimos ocurrio un error');
                            }
                        });
                    }
                }
            });
			
            function update_img(elem) {
                var $e = $(elem.currentTarget);
                var id = $e.attr("data-id");
                $.ajax({
                    url: "<?php echo base_url() ?>index.php/ADGaleria/home/fotos?id_album="+id_album,
                    method: "POST",
                    data: {
                        id: id,
                        method: "UPDATE",
                        nombre: $e.val()
                    },
                    success: function (resp) {
                        $e.parent().addClass("has-success");
                        setTimeout(function () {
                            $e.parent().removeClass("has-success");
                        }, 1500);
                        $("#" + id + " .card-title.activator").html($e.val() + '<i class="material-icons right">more_vert</i>');
                    },
                    error: function () {
                        $e.parent().addClass("has-error");
                        setTimeout(function () {
                            $e.parent().removeClass("has-error");
                        }, 1500);
                    }
                });
            }

            function delete_img(elem) {
                if (confirm("¿Estas seguro de eliminar la imagen?")) {
                    var e = elem.currentTarget;
                    var id = $(e).data("id");
                    $.ajax({
                        url: "<?php echo base_url() ?>index.php/ADGaleria/home/fotos?id_album="+id_album,
                        method: "POST",
                        data: {
                            id: id,
                            method: "DELETE"
                        },
                        success: function (resp) {
                            var img = $("#" + id);
                            img.addClass("delete");
                            setTimeout(function () {
                                $("#" + id).remove();
                            }, 1050);

                        },
                        error: function () {
                            console.log("error");
                        }
                    });
                }
            }
            
            
        </script>
</html>
