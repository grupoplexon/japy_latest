<script src="<?php echo $this->config->base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>dist/js/proveedor.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/location.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/materialize.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.Jcrop.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/classie.js" type="text/javascript"></script>

<script src="<?php echo base_url() ?>dist/js/dropzone.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/slider.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/slick/slick.js" type="text/javascript"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="<?php echo base_url() ?>dist/js/inputFormat.jquery.js"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.touchSwipe.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/facebook.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        $('.button-collapse').sideNav({
                menuWidth: 300, // Default is 240
                edge: 'left', // Choose the horizontal origin
                closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
            },
        );
    });
</script>