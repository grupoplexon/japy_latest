<title><?php echo isset($title) ? $title : "Japy" ?></title>
<link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<?php echo base_url() ?>dist/css/iconos.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/img/clubnupcial.css" rel="icon" sizes="16x16">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo base_url() ?>dist/css/datatables.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/ghpages-materialize.css" rel="stylesheet" type="text/css"/>
<!--<link href="http://materializecss.com/dist/css/materialize.min.css" rel="stylesheet" type="text/css"/>-->
<link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/presupuesto.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/slick/slick.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url() ?>dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
      href="<?php echo $this->config->base_url() ?>dist/tinymce/skins/lightgray/skin.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $this->config->base_url() ?>dist/tinymce/skins/lightgray/content.inline.min.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo $this->config->base_url() ?>dist/tinymce/skins/lightgray/content.inline.min.css"/>