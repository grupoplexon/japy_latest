<html>
    <head>
        <?php $this->view("admingaleria/header") ?>
        
    </head>
    <body>
        <?php $this->view("admingaleria/menu") ?>
        
        <div class="row">
			<h5>Albumes</h5>
		</div>
        <div class="row">
			<?php foreach ($albumes as $key => $album) { ?>
            <div class="col s12 m4 ">
				<div class="card">
					<div class="row" style="max-height:210px">
						<div class="col s9 m9">
							<img style="padding-top: 10px; height:100%; width:100%;"  src="<?php if (isset($album->foto_principal) && $album->foto_principal!=[] && $album->foto_principal->imagen != "" && $album->foto_principal->mime != "") { echo "data:" . $album->foto_principal->mime . ';' . "base64," . base64_encode($album->foto_principal->imagen); } else { echo base_url()."dist/images/comunidad/FotografiaError.png";} ?>" />
						</div>
						<?php foreach ($album->fotos as $key => $foto) { ?>
						<div class="col s3 m3" style="max-height:70px">
							<img style="padding-top: 10px; height:100%; width:100%;" src="<?php if ($foto->imagen != "" && $foto->mime != "") echo "data:" . $foto->mime . ';' . "base64," . base64_encode($foto->imagen) ?>" class="responsive-img"/>
						</div>
						<?php } ?>
						
					</div>
					<div class="row center">
						<a class="dorado-2-text" style="font-weight:bold" href="<?php base_url()?>ADGaleria/Home/editAlbum/<?php echo $album->id_album?>"><?php echo $album->nombrealbum ?></a>
					</div>
					<br>
					
				</div>
            </div>
			<?php } ?>
			
        </div>
		<?php if($paginas>1) {?>
		<div class="row center">
			<ul class="pagination">
                    <?php if ($pagina == 1) { ?>
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="<?php base_url()?>ADGaleria?pagina=<?php echo $pagina-1?>"><i class="material-icons">chevron_left</i></a></li>
                    <?php } ?>
					<?php if($paginas < 8) { ?>
                    <?php for ($p = 1; $p <= $paginas; $p++) { ?>
                        <li class="waves-effect <?php echo ($p == $pagina ? "active" : "") ?>"><a href="<?php base_url()?>ADGaleria?pagina=<?php echo $p?>"><?php echo $p ?></a></li>
                    <?php } ?>					
                    <?php } else { $ini=0;?>
						<?php if ($pagina < 4) { ?>
						<?php for ($p = 1; $p <= 6; $p++) { ?>
                        <li class="waves-effect <?php echo ($p == $pagina ? "active" : "") ?>"><a href="<?php base_url()?>ADGaleria?pagina=<?php echo $p?>"><?php echo $p ?></a></li>
						<?php } ?>
						<?php } else { ?>
                        <li class="waves-effect"><a href="<?php base_url()?>ADGaleria?pagina=1">1</a></li>
						<li><a class="disable">...</a></li>
						<?php if($pagina+5 > $paginas) {$ini=$paginas-5;$pagina2=$paginas;}else{$ini=$pagina;$pagina2=$pagina+5;} ?>
						<?php for ($p = $ini; $p <= $pagina2; $p++) { ?>
                        <li class="waves-effect <?php echo ($p == $pagina ? "active" : "") ?>"><a href="<?php base_url()?>ADGaleria?pagina=<?php echo $p?>"><?php echo $p ?></a></li>
						<?php } ?>
						<?php } ?>
						<?php if($ini+5 != $paginas) {?>
                        <li><a class="disable">...</a></li>
                        <li class="waves-effect"><a href="<?php base_url()?>ADGaleria?pagina=<?php echo $paginas?>"><?php echo $paginas ?></a></li>
						<?php } ?>
                    <?php } ?>					
                    <?php if ($pagina == $paginas) { ?>
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                    <?php } else { ?>
                        <li class="waves-effect">
                            <a href="<?php base_url()?>ADGaleria?pagina=<?php echo $pagina+1?>">
                                <i class="material-icons">chevron_right</i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
        </div>
        <?php } ?>
        <?php $this->view("principal/view_footer") ?>
        <?php $this->view("admingaleria/footer") ?>
    </body>
</html>
