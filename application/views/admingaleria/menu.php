<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="<?php echo site_url('cuenta/logout'); ?>">Cerrar sesi&oacute;n</a></li>
</ul>
<nav class="pink lighten-5 z-depth-1">
    <div class="nav-wrapper">
        <a href="#!" class="brand-logo">
            <img src="<?php echo base_url() ?>dist/img/logo.png" style="width: 64%; height: auto;" alt=""/>
        </a>
        <ul class="right hide-on-med-and-down">
            <li><a class="gray-text darken-6" href="<?php echo base_url()?>index.php/ADGaleria">Inicio</a></li>
            <li><a class="gray-text darken-6" href="<?php echo base_url()?>index.php/ADGaleria/Home/addAlbum">Nuevo</a></li>
            <li class="gray-text darken-6"><b>@<?php echo $this->session->userdata('nombre'); ?></b></li>
            <li><a class="dropdown-button" href="#!" data-activates="dropdown1"><i class="material-icons gray-text darken-6">more_vert</i></a></li>
        </ul>
    </div>
</nav>
