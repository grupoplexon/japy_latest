<!--     SECCION INFORMACION       -->
<title> Empresa - BrideAdvisor</title>
<div class="row"
     style="background: url('<?php echo base_url() ?>/dist/img/alta/empresa.png') no-repeat center center #fff;
         background-size: cover;">
    <div class="body-container">
        <div style="color:white" class="col s12 m12 l6">
            <div class="row" style="margin-top:50px">
                <h3>TODO EL MERCADO DE NOVIOS PARA QUE TU NEGOCIO VENDA</h3>
            </div>
            <div class="row" style="background-color: rgba(0,0,0,0.6);  padding: 10px;">
                <p>No solo ofrecemos una excelente plataforma, tambi&eacute;n los Novios le solicitar&aacute;n
                    cotizaciones.</p>
                <!--<p>Las posibilidades de cierre se incrementan con prospectos frecuentemente actualizados.</p>
                <p>Japy cuenta con proveedores de calidad y especializados.</p>
                <p>Productos flexibles que generan tr&aacute;fico a sus estrategias en internet.</p>-->
            </div>
            <div class="row">
                <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate
                    gratis</a>
            </div>
        </div>
        <div class="col s12 m12 l6">
            <div class="col s1 m1 l4">
                &nbsp;
            </div>
            <div class="col s10 m10 l6">
                <div class="card" style="margin-top:50px">
                    <div class="card-content">
                        <span class="card-title">Acceso Empresas</span>
                    </div>
                    <div class="card-action">
                        <form action="<?php echo base_url()."cuenta" ?>" method="POST">
                            <div class="row">
                                <input id="usuario" type="text" placeholder="Usuario" name="primaryEmail"
                                required="" class="col s12 m12 form-control"></div>
                            <div class="row">
                                <input id="password" required="" type="password" placeholder="Contraseña" name="primaryPassword"
                                       class="col s12 m12 form-control"></div>

                            <div class="row">
                                <a style="font-size:12px" href="#!">¿Olvidaste tu contraseña?</a></div>
                            <div class="row">
                                <button class="col s12 m12 btn buscar primary-background secondary-text">Acceder
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m12 l4">
            <div class="row center"><img width="100px" src="<?php echo base_url() ?>/dist/img/alta/computadora.png"
                                         alt=""></div>
            <div class="row center"><h5>Lo ver&aacute;n los Novios</h5>
                <p>Los Novios le solicitar&aacute;n cotizaciones detallando los servicios en los que est&aacute;n
                    interesados y Usted vera sus datos de contacto.</p>
            </div>
        </div>
        <div class="col s12 m12 l4">
            <div class="row center"><img class=" responsive-img" width="100px"
                                         src="<?php echo base_url() ?>/dist/img/alta/solicitudes.png" alt=""></div>
            <div class="row center"><h5>Recibir&aacute; correos</h5>
                <p>Podr&aacute; contestar desde la cuenta de su correo o bien desde el portal de BrideAdvisor de una manera muy
                    sencilla.</p>
            </div>
        </div>
        <div class="col s12 m12 l4">
            <div class="row center"><img class=" responsive-img" width="100px"
                                         src="<?php echo base_url() ?>/dist/img/alta/like-pink.png" alt=""></div>
            <div class="row center"><h5>Incremente Ventas</h5>
                <p>Contacte directamente a los Novios interesados y tenga m&aacute;s eventos.</p>
            </div>
        </div>
    </div>
</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container ">
        <div class="col s12 m12 l6">

            <div class="row">
                <h4>¿Por que con BrideAdvisor?</h4>
            </div>
            <div class="row valign-wrapper">
                <div class="valign col s3 m3 l2">
                    <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/ofrecemos1.png" alt="">
                </div>
                <span class="valign col s9 m9 l10" style="line-height: 30px; font-size:18px">
                    Por que la plataforma y el mercado frecuentemente actualizado que ponemos a su disposici&oacute;n esta concentrado para que miles de Novios lo contacten y Usted a ellos.
                </span>
            </div>
            <div class="row valign-wrapper">
                <div class="valign col s3 m3 l2">
                    <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/ofrecemos2.png" alt="">
                </div>
                <span class="valign col s9 m9 l10" style="line-height: 30px; font-size:18px">
                    La mejor estrategia de mercadotecnia en l&iacute;nea a la cual se pueda afiliar.
                </span>
            </div>
            <div class="row valign-wrapper">
                <div class="valign col s3 m3 l2">
                    <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/ofrecemos3.png" alt="">
                </div>
                <span class="valign col s9 m9 l10" style="line-height: 30px; font-size:18px">
                    Equipo con experiencia comprobable dentro del mercado y dirigido para que las estrategias de venta funcionen.
                </span>
            </div>
        </div>
        <div class="col s12 m12 l6">
            <img style="margin:20px 0" class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img1.png"
                 alt="">
        </div>

    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m12 l6" style="margin-top:20px;">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img2.png" alt="">
        </div>
        <div class="col s12 m12 l6" style="margin-top:30px">

            <div class="row">
                <h4>Venda f&aacute;cil !!!</h4>
            </div>
            <div class="row valign-wrapper">
                <div class="valign col s3 m3 l2">
                    <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/gestion1.png" alt="">
                </div>
                <span class="valign col s9 m9 l10" style="line-height: 30px; font-size:18px">
                    Subir toda la descripci&oacute;n de sus servicios y productos apoyada con fotograf&iacute;as y videos para impactar mejor de una manera f&aacute;cil.
                </span>
            </div>
            <div class="row valign-wrapper">
                <div class="valign col s3 m3 l2">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/alta/gestion2.png" alt="">
                </div>
                <span class="valign col s9 m9 l10" style="line-height: 30px; font-size:18px">
                    Organice las demandas de sus prospectos y env&iacute;e sus cotizaciones de una manera eficiente y directa a su mercado.
                </span>
            </div>
            <div class="row valign-wrapper">
                <div class="valign col s3 m3 l2">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/alta/gestion3.png" alt="">
                </div>
                <span class="valign col s9 m9 l10" style="line-height: 30px; font-size:18px">
                    Las calificaciones de los Proveedores las dan los Novios y con esta informaci&oacute;n podr&aacute;s mejorar puntos finos que tal vez antes no se lograban ver y tambi&eacute;n las estad&iacute;sticas de solicitudes y respuestas.
                </span>
            </div>

        </div>

    </div>
</div>
<div class="row center">
    <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
    <a href="<?php echo base_url() ?>home/visitaGuiada" class="btn white darken-5" style="color:black">Visita
        guiada&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i></a><br><br>
</div>
<script>
    var changeSoy = function() {
        if ($('#soy').val() == 1) {
            $('#motivo').show();
        }
        else {
            $('#motivo').hide();
        }
    };

    var sendMail = function() {
        if ($('#nombre').val().length < 4) {
            alert('El nombre debe tener al menos 4 caracteres.');
            return false;
        }
        if (!validarEmail($('#email').val())) {
            alert('El formato de email es incorrecto.');
            return false;
        }
        if ($('#soy').val() == 0) {
            alert('Selecciona si eres un usuario o empresa.');
            return false;
        }
        if ($('#soy').val() == 1) {
            if ($('#selectMotivo').val() == 0) {
                alert('Selecciona un motivo.');
                return false;
            }
        }
        if ($('#comentario').val().length < 4) {
            alert('El comentario debe tener al menos 4 caracteres.');
            return false;
        }
        return true;
    };

    function validarEmail(email) {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!expr.test(email)) {
            return false;
        }
        else {
            return true;
        }
    }

</script>

