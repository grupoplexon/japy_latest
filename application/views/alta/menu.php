<div class="z-depth-1" style="padding:10px;display: block; position: relative;background-color: #00bcdd;">
    <div class="body-container">
        <div class="row">
            <div class="col s12" style="float: left;position: relative;">
                <a href="<?php echo base_url() ?>">
                    <small style="color: white"><b>Home</b></small>
                </a>
                <small style="color: white"><b>|</b></small>
                <a href="<?php echo base_url() ?>home/contacto">
                    <small style="color: white"><b>Contacto</b></small>
                </a>
                <small style="color: white"><b>|</b></small>
                <a href="<?php echo base_url() ?>">
                    <small style="color: white"><b>Mapa del sitio</b></small>
                </a>
            </div>
        </div>
    </div>
    <hr>
    <div class="body-container" style="margin-top:15px">
        <div class="row">
            <div class="col s12 m12 l6 logo-provider">
                <a href="<?php echo base_url() ?>">
                    <img style="position:initial; width: 30%;"
                         src="<?php echo base_url() ?>/dist/img/japy_nobg_white.png" alt=""/>
                </a>
            </div>
            <div class="col s12 m12 l6">
                <ul class="tabs " style="background-color: #00bcdd">
                    <li class="tab ">
                        <a <?php if ($active == 1) { ?> class="active text-white"
                        <?php } ?> onclick="clickTab2(1)" href="<?php echo base_url() ?>home/altaEmpresas">
                            Acceso Empresa
                        </a>
                    </li>
                    <li class="tab ">
                        <a <?php if ($active == 2) { ?>  <?php } ?>onclick="clickTab2(2)"
                                                        href="<?php echo base_url() ?>home/visitaGuiada" class="active text-white">Visita
                            Guiada</a>
                    </li>
                    <li class="tab ">
                        <a <?php if ($active == 3) { ?>  <?php } ?>onclick="clickTab2(3)"
                                                        href="<?php echo base_url() ?>home/serviciosPremium" class="active text-white">Servicios
                            premium</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


</div>
<script>
    var url2 = [];
    url2[1] = '<?php echo base_url() ?>home/altaEmpresas';
    url2[2] = '<?php echo base_url() ?>home/visitaGuiada';
    url2[3] = '<?php echo base_url() ?>home/serviciosPremium';

    function clickTab2(tab) {
        //console.log(url[tab]);
        location.href = url2[tab];
    }

</script>
