<!--     SECCION INFORMACION       -->
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/alta/fondo.jpg') no-repeat center center #fff;
        background-size: cover;">
    <div class="body-container">
        <div class="col s12 m12 l6">
            <div class="row" style="margin-top:50px">
                <h3>ESCAPARATE</h3>
            </div>
            <div class="row">
                <p style="font-size:18px">Le ofrecemos que tenga su propio sitio dentro de la plataforma, promocione todos los d&iacute;as sus productos y servicios de una manera atractiva para sus prospectos, mantenga el contacto directo.</p>
            </div>
            <div class="row">
                <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
            </div>
        </div>
        <div class="col s12 m12 l6" style="margin-top:45px">
            <div class="col s1 m1 l1">
                &nbsp;
            </div>
            <div class="col s10 m10 l10">
                <img style="margin: 20px 0;" class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img1.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab"><a onclick="clickTab(1)" href="<?php echo base_url() ?>home/visitaGuiada">Visita Guiada</a></li>
                <li class="tab"><a class="active" onclick="clickTab(2)" href="<?php echo base_url() ?>home/mi-escaparate">Mi Escaparate</a></li>
                <li class="tab"><a onclick="clickTab(3)" href="<?php echo base_url() ?>home/mis-recomendaciones">Mis Recomendaciones</a></li>
                <li class="tab"><a onclick="clickTab(4)" href="<?php echo base_url() ?>home/mis-promociones">Mis Promociones</a></li>
                <li class="tab"><a onclick="clickTab(5)" href="<?php echo base_url() ?>home/mis-eventos">Mis Eventos</a></li>
                <li class="tab"><a onclick="clickTab(6)" href="<?php echo base_url() ?>home/mis-solicitudes">Mis Solicitudes</a></li>
                <li class="tab"><a onclick="clickTab(7)" href="<?php echo base_url() ?>home/mis-estadisticas">Mis Estad&iacute;sticas</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img3.png" alt="">
        </div>
        <div class="col s12 m6" style="margin-top:75px">
            <div class="row">
                <h5>Publique todo....</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Publique sus fotos, videos, promociones y eventos !!! Nosotros lo revisaremos y asesoramos para que su sitio impacte y a sus prospectos.
            </span>
        </div>
    </div>
</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:75px">
            <div class="row">
                <h5>Galer&iacute;a de fotos</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Suba las fotos que den a conocer la calidad de sus servicios, los Novios podr&aacute;n darse una clara idea de lo que ofrece e les ser&aacute; m&aacute;s f&aacute;cil tomar una decisi&oacute;n.
            </span>
        </div>
    </div>
    <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
        <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img4.png" alt="">
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px">
            <video class="responsive-video" controls>
                <source src="<?php echo base_url() ?>/uploads/videos/video-3-42.webm">
            </video>
        </div>
        <div class="col s12 m6" style="margin-top:75px">

            <div class="row">
                <h5>Suba sus videos</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Los videos son un herramienta fundamental para impactar mas sentidos de sus prospectos de una manera audio visual, conocer&aacute;n en vivo y a todo color la calidad de sus productos y servicios.
                No deje de aprovechar esta oportunidad !!!
            </span>
        </div>

    </div>
</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:75px">
            <div class="row">
                <h5>Historial de preguntas</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                En cada ocasi&oacute;n que usted contesta preguntas o dudas de los Novios &eacute;stas aparecer&aacute;n en su sitio y los Novios podr&aacute;n leerlas, esto facilita, filtra y reduce el proceso de cierre.
            </span>
        </div>

    </div>
    <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
        <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/informacion.png" alt="">
    </div>
</div>
<div class="row center">
    <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
    <a href="<?php echo base_url() ?>home/mis-promociones" class="btn white darken-5" style="color:black">Mis promociones&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i></a>
</div>
<script>
    var url = [];
    url[1] = '<?php echo base_url() ?>home/visitaGuiada';
    url[2] = '<?php echo base_url() ?>home/mi-escaparate';
    url[3] = '<?php echo base_url() ?>home/mis-recomendaciones';
    url[4] = '<?php echo base_url() ?>home/mis-promociones';
    url[5] = '<?php echo base_url() ?>home/mis-eventos';
    url[6] = '<?php echo base_url() ?>home/mis-solicitudes';
    url[7] = '<?php echo base_url() ?>home/mis-estadisticas';

    function clickTab(tab) {
        location.href = url[tab];
    }

</script>

