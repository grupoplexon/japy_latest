
<!--     SECCION INFORMACION       -->
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/alta/fondo.jpg') no-repeat center center #fff;
     background-size: cover;">
    <div class="body-container">
        <div class="col s12 m12 l6">
            <div class="row" style="margin-top:50px">
                <h3>ESTAD&Iacute;STICAS</h3>
            </div>
            <div class="row" >
                <p style="font-size:18px">
                    Analizar y entender los resultados es importante para ir creciendo, as&iacute; como conocer el n&uacute;mero de solicitudes y su evoluci&oacute;n desde el momento en que se cre&oacute; tu escaparate.
                </p>
            </div>
            <div class="row" >
                <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
            </div>
        </div>
        <div class="col s12 m12 l6" style="margin-top:45px">
            <div class="col s1 m1">
                &nbsp;
            </div>
            <div class="col s10 m10">
                <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/mis_estadisticas.png" alt="" >
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab"><a onclick="clickTab(1)" href="<?php echo base_url() ?>home/visitaGuiada">Visita Guiada</a></li>
                <li class="tab"><a  onclick="clickTab(2)" href="<?php echo base_url() ?>home/mi-escaparate">Mi Escaparate</a></li>
                <li class="tab"><a onclick="clickTab(3)" href="<?php echo base_url() ?>home/mis-recomendaciones">Mis Recomendaciones</a></li>
                <li class="tab"><a onclick="clickTab(4)" href="<?php echo base_url() ?>home/mis-promociones">Mis Promociones</a></li>
                <li class="tab"><a  onclick="clickTab(5)" href="<?php echo base_url() ?>home/mis-eventos">Mis Eventos</a></li>
                <li class="tab"><a onclick="clickTab(6)" href="<?php echo base_url() ?>home/mis-solicitudes">Mis Solicitudes</a></li>
                <li class="tab"><a class="active" onclick="clickTab(7)" href="<?php echo base_url() ?>home/mis-estadisticas">Mis Estad&iacute;sticas</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/solicitudes-4.png" alt="" >
        </div>
        <div class="col s12 m6" style="margin-top:75px">

            <div class="row" >
                <h5>Informaci&oacute;n al momento</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                La informaci&oacute;n de la estad&iacute;stica de su escaparate estar&aacute; siempre actualizada para que pueda tomar las decisiones m&aacute;s adecuadas.
            </span>
        </div>
    </div>
</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container">

        <div class="col s12 m6" style="margin-top:75px">

            <div class="row"  >
                <h5>COMPORTAMIENTO HIST&Oacute;RICO</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Usted podr&aacute; analizar y comparar sus resultados de meses y años anteriores, as&iacute; se dar&aacute; cuenta del avance y poder reaccionar en tiempo para presentar la mejor propuesta a sus clientes y prospectos.
            </span>
        </div>

    </div>
    <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
        <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/solicitudes-4.png" alt="" >
    </div>				
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/estadisticas-4.png" alt="" >
        </div>	
        <div class="col s12 m6" style="margin-top:75px">

            <div class="row" >
                <h5>Decisiones importantes</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Sabemos que para tomar las mejores decisiones se debe de contar con informaci&oacute;n que muestre el comportamiento de su mercado para analizarla, la plataforma le brinda esto y m&aacute;s, con esta informaci&oacute;n guardada en su escaparate tambi&eacute;n podr&aacute; analizar sus decisiones pasadas para que pueda tomar una decisi&oacute;n en el presente y su negocio crezca.
            </span></div>

    </div>
</div>

<div class="row center" >
    <a href="<?php echo base_url() ?>home/mis-solicitudes" class="btn white darken-5" style="color:black"><i class="fa fa-caret-left" aria-hidden="true"></i>&nbsp;Mis solicitudes</a>
    <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
</div>
<script>
    var url = [];
    url[1] = '<?php echo base_url() ?>home/visitaGuiada';
    url[2] = '<?php echo base_url() ?>home/mi-escaparate';
    url[3] = '<?php echo base_url() ?>home/mis-recomendaciones';
    url[4] = '<?php echo base_url() ?>home/mis-promociones';
    url[5] = '<?php echo base_url() ?>home/mis-eventos';
    url[6] = '<?php echo base_url() ?>home/mis-solicitudes';
    url[7] = '<?php echo base_url() ?>home/mis-estadisticas';
    function clickTab(tab) {
        location.href = url[tab];
    }

</script>

