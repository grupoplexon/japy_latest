<!--     SECCION INFORMACION       -->
<div class="row"
     style="background: url('<?php echo base_url() ?>/dist/img/alta/fondo.jpg') no-repeat center center #fff;
             background-size: cover;">
    <div class="body-container">
        <div class="col s12 m12 l6">
            <div class="row" style="margin-top:50px">
                <h3>MIS EVENTOS</h3>
            </div>
            <div class="row">
                <p style="font-size:18px">
                    La mejor manera de darse a conocer es por medio de los eventos, ya que mediante el trato directo
                    usted podr&aacute; exponer a sus posibles clientes la calidad de sus servicios.
                </p>
            </div>
            <div class="row">
                <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate
                    gratis</a>
            </div>
        </div>
        <div class="col s12 m12 l6" style="margin-top:45px">
            <div class="col s1 m1">
                &nbsp;
            </div>
            <div class="col s10 m10">
                <img style="margin: 20px 0;" class="responsive-img"
                     src="<?php echo base_url() ?>/dist/img/alta/img1.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab"><a onclick="clickTab(1)" href="<?php echo base_url() ?>home/visitaGuiada">Visita
                        Guiada</a></li>
                <li class="tab"><a onclick="clickTab(2)" href="<?php echo base_url() ?>home/mi-escaparate">Mi
                        Escaparate</a></li>
                <li class="tab"><a onclick="clickTab(3)" href="<?php echo base_url() ?>home/mis-recomendaciones">Mis
                        Recomendaciones</a></li>
                <li class="tab"><a onclick="clickTab(4)" href="<?php echo base_url() ?>home/mis-promociones">Mis
                        Promociones</a></li>
                <li class="tab"><a class="active" onclick="clickTab(5)" href="<?php echo base_url() ?>home/mis-eventos">Mis Eventos</a></li>
                <li class="tab"><a onclick="clickTab(6)" href="<?php echo base_url() ?>home/mis-solicitudes">Mis
                        Solicitudes</a></li>
                <li class="tab"><a onclick="clickTab(7)" href="<?php echo base_url() ?>home/mis-estadisticas">Mis
                        Estad&iacute;sticas</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/eventos-2.png" alt="">
        </div>
        <div class="col s12 m6" style="margin-top:75px">
            <div class="row">
                <h5>Publicaci&oacute;n de eventos</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Puedes publicar cualquier tipo de eventos como ferias, inauguraciones, exposiciones, cursos con el fin de atraer a las parejas para que conozcan la calidad de tu servicio.
            </span>
        </div>
    </div>


</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:75px">
            <div class="row">
                <h5>Evento espec&iacute;fico</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Publique toda la informaci&oacute;n de sus eventos e invitaciones para que sea m&aacute;s atractivo para los Novios inscribirse, como los beneficios, fotos, mapa de ubicaci&oacute;n, fecha, hora y especificaciones.
            </span>
        </div>
    </div>
    <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
        <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/eventos-3.png" alt="">
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/eventos-4.png" alt="">
        </div>
        <div class="col s12 m6" style="margin-top:75px">

            <div class="row">
                <h5>Alcance</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Las publicaciones de todos sus eventos pueden hacer la diferencia en la toma de decisiones para los Novios, nosotros le acercamos a su mercado para que Usted logre acercarse a ellos, entre mas cerca y comunicado este con ellos tendr&aacute; m&aacute;s oportunidades de recomendaciones, exposici&oacute;n dentro del sitio y cierres de venta, tal vez su competencia no lo haga con frecuencia !!!
            </span>
        </div>
    </div>
</div>

<div class="row center">
    <a href="<?php echo base_url() ?>home/mis-promociones" class="btn white darken-5" style="color:black"><i
                class="fa fa-caret-left" aria-hidden="true"></i>&nbsp;Mis promociones</a>
    <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
    <a href="<?php echo base_url() ?>home/mis-solicitudes" class="btn white darken-5" style="color:black">Mis solicitudes&nbsp;<i
                class="fa fa-caret-right" aria-hidden="true"></i></a>
</div>
<script>
    var url = [];
    url[1] = '<?php echo base_url() ?>home/visitaGuiada';
    url[2] = '<?php echo base_url() ?>home/mi-escaparate';
    url[3] = '<?php echo base_url() ?>home/mis-recomendaciones';
    url[4] = '<?php echo base_url() ?>home/mis-promociones';
    url[5] = '<?php echo base_url() ?>home/mis-eventos';
    url[6] = '<?php echo base_url() ?>home/mis-solicitudes';
    url[7] = '<?php echo base_url() ?>home/mis-estadisticas';

    function clickTab(tab) {
        location.href = url[tab];
    }

</script>
