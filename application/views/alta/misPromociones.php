<!--     SECCION INFORMACION       -->
<div class="row"
     style="background: url('<?php echo base_url() ?>/dist/img/alta/fondo.jpg') no-repeat center center #fff;
             background-size: cover;">
    <div class="body-container">
        <div class="col s12 m12 l6">
            <div class="row" style="margin-top:50px">
                <h3>PROMOCIONES</h3>
            </div>
            <div class="row">
                <p style="font-size:18px">
                    La mejor estrategia que puede utilizar con el mercado de Novios es atraerlos con ofertas y
                    promociones ya que podr&aacute; darse cuenta de la frecuencia y estacionalidad de las demandas del
                    mercado con el uso de la plataforma.
                </p>
            </div>
            <div class="row">
                <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate
                    gratis</a>
            </div>
        </div>
        <div class="col s12 m12 l6" style="margin-top:45px">
            <div class="col s1 m1">
                &nbsp;
            </div>
            <div class="col s10 m10">
                <img style="margin: 20px 0" class="responsive-img"
                     src="<?php echo base_url() ?>/dist/img/alta/promociones-1.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab"><a onclick="clickTab(1)" href="<?php echo base_url() ?>home/visitaGuiada">Visita Guiada</a></li>
                <li class="tab"><a onclick="clickTab(2)" href="<?php echo base_url() ?>home/mi-escaparate">Mi Escaparate</a></li>
                <li class="tab"><a onclick="clickTab(3)" href="<?php echo base_url() ?>home/mis-recomendaciones">Mis Recomendaciones</a></li>
                <li class="tab"><a class="active" onclick="clickTab(4)" href="<?php echo base_url() ?>home/mis-promociones">Mis Promociones</a></li>
                <li class="tab"><a onclick="clickTab(5)" href="<?php echo base_url() ?>home/mis-eventos">Mis Eventos</a></li>
                <li class="tab"><a onclick="clickTab(6)" href="<?php echo base_url() ?>home/mis-solicitudes">Mis Solicitudes</a></li>
                <li class="tab"><a onclick="clickTab(7)" href="<?php echo base_url() ?>home/mis-estadisticas">Mis Estad&iacute;sticas</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/promociones-1.png" alt="">
        </div>
        <div class="col s12 m6" style="margin-top:75px">

            <div class="row">
                <h5>Mida y cambie la estrategia</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                A trav&eacute;s de Japy los Novios pueden descargar o imprimir sus regalos, promociones y descuentos para que Usted mida la eficiencia, tambi&eacute;n podr&aacute; reaccionar de manera inmediata cambiando la estrategia.
                La plataforma se lo permite, aprov&eacute;chela !!!!!!!!
            </span>
        </div>
    </div>
</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container">

        <div class="col s12 m6" style="margin-top:75px">

            <div class="row">
                <h5>Venda m&aacute;s</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Si Usted utiliza todas las herramientas y productos que le ofrecemos lograr&aacute; incrementar sus ventas, por ejemplo si mide sus promociones dentro de su escaparate con la herramienta de “estad&iacute;stica” podr&aacute; desarrollar una promoci&oacute;n a la medida para cada temporada.
            </span>
        </div>

    </div>
    <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
        <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/promociones-1.png" alt="">
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/promociones-4.png" alt="">
        </div>
        <div class="col s12 m6" style="margin-top:35px">

            <div class="row">
                <h5>M&aacute;s estrategias</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Si usted ofrece un descuento especial o alguna promoci&oacute;n para los Novios que contraten dentro de japybodas.com tendr&aacute; la oportunidad de estar dentro de los primeros resultados de b&uacute;squeda por categor&iacute;a y ciudad o estado.
            </span>
        </div>

    </div>
</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container">

        <div class="col s12 m6" style="margin-top:75px">

            <div class="row">
                <h5>Mayor visibilidad</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Cuando publica una promoci&oacute;n su empresa aparecer&aacute; dentro del cat&aacute;logo de promociones y puede ser mencionado dentro de la secci&oacute;n editorial de la plataforma que se encuentra dentro del Blog para los Novios.
            </span>
        </div>

    </div>
    <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
        <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/promociones-5.png" alt="">
    </div>
</div>
<div class="row center">
    <a href="<?php echo base_url() ?>home/mi-escaparate" class="btn white darken-5" style="color:black"><i
                class="fa fa-caret-left" aria-hidden="true"></i>&nbsp;Mi escaparate</a>
    <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
    <a href="<?php echo base_url() ?>home/mis-eventos" class="btn white darken-5" style="color:black">Mis
        eventos&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i></a>
</div>
<script>
    var url = [];
    url[1] = '<?php echo base_url() ?>home/visitaGuiada';
    url[2] = '<?php echo base_url() ?>home/mi-escaparate';
    url[3] = '<?php echo base_url() ?>home/mis-recomendaciones';
    url[4] = '<?php echo base_url() ?>home/mis-promociones';
    url[5] = '<?php echo base_url() ?>home/mis-eventos';
    url[6] = '<?php echo base_url() ?>home/mis-solicitudes';
    url[7] = '<?php echo base_url() ?>home/mis-estadisticas';

    function clickTab(tab) {
        location.href = url[tab];
    }

</script>

