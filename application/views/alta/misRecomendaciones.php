<!--     SECCION INFORMACION       -->
<div class="row"
     style="background: url('<?php echo base_url() ?>/dist/img/alta/fondo.jpg') no-repeat center center #fff;
             background-size: cover;">
    <div class="body-container">
        <div class="col s12 m12 l6">
            <div class="row" style="margin-top:50px">
                <h3 style="font-size: 2.5em">RECOMENDACIONES</h3>
            </div>
            <div class="row">
                <p style="font-size:18px">
                    En la experiencia que tenemos es mucho m&aacute;s probable que los Novios contacten a los
                    proveedores recomendados por otros Novios que ya contrataron sus servicios.
                </p>
            </div>
            <div class="row">
                <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate
                    gratis</a>
            </div>
        </div>
        <div class="col s12 m12 l6" style="margin-top:45px">
            <div class="col s1 m1 l1">
                &nbsp;
            </div>
            <div class="col s10 m10">
                <img style="margin: 20px 0;" class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img5.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab"><a onclick="clickTab(1)" href="<?php echo base_url() ?>home/visitaGuiada">Visita Guiada</a></li>
                <li class="tab"><a onclick="clickTab(2)" href="<?php echo base_url() ?>home/mi-escaparate">Mi Escaparate</a></li>
                <li class="tab"><a class="active" onclick="clickTab(3)" href="<?php echo base_url() ?>home/mis-recomendaciones">Mis Recomendaciones</a></li>
                <li class="tab"><a onclick="clickTab(4)" href="<?php echo base_url() ?>home/mis-promociones">Mis Promociones</a></li>
                <li class="tab"><a onclick="clickTab(5)" href="<?php echo base_url() ?>home/mis-eventos">Mis Eventos</a></li>
                <li class="tab"><a onclick="clickTab(6)" href="<?php echo base_url() ?>home/mis-solicitudes">Mis Solicitudes</a></li>
                <li class="tab"><a onclick="clickTab(7)" href="<?php echo base_url() ?>home/mis-estadisticas">Mis Estad&iacute;sticas</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:55px">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img6.png" alt="">
        </div>
        <div class="col s12 m6" style="margin-top:35px">

            <div class="row">
                <h5>Comunicaci&oacute;n</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Es importante generar tr&aacute;fico en el sitio de su empresa dentro de japybodas.com ya que muchos m&aacute;s Novios confiar&aacute;n en la calidad de sus productos o servicios.
            </span>
        </div>

    </div>


</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:55px">
            <div class="row">
                <h5>Sobresalga entre los dem&aacute;s</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Por cada recomendaci&oacute;n que recibas podr&aacute;s incluir los sellos “Recomendado japy”, los cuales aparecer&aacute;n en su escaparate.
            </span>
        </div>
    </div>
    <div class="col s12 m6 center" style="margin-top:35px;margin-bottom:35px;">
        <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/reco.png" alt="">
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6 center" style="margin-top:35px">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img7.png" alt="">
        </div>
        <div class="col s12 m6" style="margin-top:95px">
            <div class="row">
                <h5>Distintivo en su sitio web</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Usted tambi&eacute;n podr&aacute; colocar estos distintivos donde Japy lo reconoce como empresa o proveedor “Recomendado” en el sitio web de su empresa para que los Novios que lo visiten sepan que tienen la opci&oacute;n de contar con calidad en sus servicios.
            </span>
        </div>
    </div>


</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
    <div class="body-container">

        <div class="col s12 m6" style="margin-top:75px">

            <div class="row">
                <h5>Que todos lo vean</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Si nos pide este emblema de calidad y prestigio con mucho gusto se lo enviamos para que lo coloque en su punto de venta y que todos se enteren de la calidad de sus servicios.
            </span>
        </div>

    </div>
    <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
        <!--<img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img8.png" alt="" >-->
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px;margin-bottom:35px;">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img9.png" alt="">
        </div>
        <div class="col s12 m6" style="margin-top:75px">

            <div class="row">
                <h5>Se vale pedir u omitir</h5>
            </div>
            <span class="valign" style="line-height: 30px; font-size:18px">
                Podr&aacute; solicitar a los novios que emitan una recomendaci&oacute;n del servicio prestado. Si no esta de acuerdo con alguna puede denunciarlo al administrador del portal.
            </span>
        </div>

    </div>
</div>
<div class="row center">
    <a href="<?php echo base_url() ?>home/mi-escaparate" class="btn white darken-5" style="color:black"><i
                class="fa fa-caret-left" aria-hidden="true"></i>&nbsp;Mi escaparate</a>
    <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
    <a href="<?php echo base_url() ?>home/mis-promociones" class="btn white darken-5" style="color:black">Mis
        Promociones&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i></a>
</div>
<script>
    var url = [];
    url[1] = '<?php echo base_url() ?>home/visitaGuiada';
    url[2] = '<?php echo base_url() ?>home/mi-escaparate';
    url[3] = '<?php echo base_url() ?>home/mis-recomendaciones';
    url[4] = '<?php echo base_url() ?>home/mis-promociones';
    url[5] = '<?php echo base_url() ?>home/mis-eventos';
    url[6] = '<?php echo base_url() ?>home/mis-solicitudes';
    url[7] = '<?php echo base_url() ?>home/mis-estadisticas';

    function clickTab(tab) {
        location.href = url[tab];
    }

</script>
