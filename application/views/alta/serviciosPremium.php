<!--     SECCION INFORMACION       -->
<div class="row"
     style="background: url('<?php echo base_url() ?>/dist/img/alta/fondoPremium1.jpg') no-repeat center center #fff;
             background-size: cover;">
    <div class="body-container">
        <div style="color:white" class="col s12 m6">
            <div class="row" style="margin-top:50px">
                <h3>Consiga m&aacute;s clientes, h&aacute;gase Premium</h3>
            </div>
            <div class="row">
                <p style="font-size:18px">En japybodas.com apostamos por el &eacute;xito de su negocio. Hemos creado
                    los Packs Premium que multiplicar&aacute;n sus posibilidades de conseguir nuevos clientes.</p>
            </div>
            <div class="row">
                <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate
                    gratis</a>
            </div>
        </div>
        <div class="col s12 m6 center" style="margin-top:35px">
            <div class="col s2 m2">
                &nbsp;
            </div>
            <img class="col s8 responsive-img" src="<?php echo base_url() ?>/dist/img/alta/premium.png" alt="">

        </div>

    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m4">
            <div class="row center"><img class=" responsive-img" width="100px"
                                         src="<?php echo base_url() ?>/dist/img/alta/mensaje.png" alt=""></div>
            <div class="row center"><h5>M&aacute;s solicitudes</h5>
                <p>Una posici&oacute;n Premium le asegura mayor visibilidad y m&aacute;s solicitudes. Multiplicar&aacute;
                    por 5 sus resultados en muy poco tiempo.</p>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="row center"><img class=" responsive-img" width="100px"
                                         src="<?php echo base_url() ?>/dist/img/alta/celular.png" alt=""></div>
            <div class="row center"><h5>Datos visibles</h5>
                <p>Su tel&eacute;fono ser&aacute; visible en su ficha y ver&aacute; los datos de los novios en las
                    solicitudes. Tendr&aacute; contacto directo.</p>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="row center"><img class=" responsive-img" width="100px"
                                         src="<?php echo base_url() ?>/dist/img/alta/like-pink.png" alt=""></div>
            <div class="row center"><h5>M&aacute;s ventas</h5>
                <p>Conseguir&aacute; m&aacute;s novios y contactos de mejor calidad. Esto le dar&aacute; m&aacute;s
                    clientes, m&aacute;s bodas y un mayor beneficio.</p>
            </div>
        </div>
    </div>
</div>
<div class="row center">
    <a href="<?php echo base_url() ?>home/visitaGuiada" class="btn white darken-5" style="color:black"><i
                class="fa fa-caret-left" aria-hidden="true"></i>&nbsp;Visita guiada</a>
    <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
</div>
<script>
    var changeSoy = function () {
        if ($('#soy').val() == 1) {
            $("#motivo").show();
        } else {
            $("#motivo").hide();
        }
    }

    var sendMail = function () {
        if ($('#nombre').val().length < 4) {
            alert("El nombre debe tener al menos 4 caracteres.");
            return false;
        }
        if (!validarEmail($('#email').val())) {
            alert("El formato de email es incorrecto.");
            return false;
        }
        if ($('#soy').val() == 0) {
            alert("Selecciona si eres un usuario o empresa.");
            return false;
        }
        if ($('#soy').val() == 1) {
            if ($('#selectMotivo').val() == 0) {
                alert("Selecciona un motivo.");
                return false;
            }
        }
        if ($('#comentario').val().length < 4) {
            alert("El comentario debe tener al menos 4 caracteres.");
            return false;
        }

        return true;
    }

    function validarEmail(email) {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!expr.test(email))
            return false;
        else
            return true;
    }

</script>

