<!--     SECCION INFORMACION       -->
<div class="row"
     style="background: url('<?php echo base_url() ?>/dist/img/alta/fondo.jpg') no-repeat center center #fff;background-size: cover;">
    <div class="body-container">
        <div class="col s12 m12 l6">
            <div class="row" style="margin-top:50px">
                <h3>ENCUENTRE FÁCILMENTE A SU MERCADO</h3>
            </div>
            <div class="row">
                <p style="font-size:18px">
                    Estamos capacitados para lograr nuestro principal objetivo que es que
                    Usted alcance sus ventas y haga crecer su negocio a trav&eacute;s
                    de los servicios que le ofrecemos, La plataforma de Japybodas esta
                    pensada para acercar a Proveedores y Novios de una manera sencilla y
                    eficaz para que ambos encuentren r&aacute;pidamente lo que buscan.
                </p>
            </div>
            <div class="row">
                <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate
                    gratis</a>
            </div>
        </div>
        <div class="col s12 m12 l6">
            <div class="col s1 m1 l1">
                &nbsp;
            </div>
            <div class="col s10 m10 l10">
                <img style="margin:20px 0" class="responsive-img" src="<?php echo base_url() ?>/dist/img/alta/img2.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m12">
            <ul class="tabs">
                <li class="tab "><a class="gris-2-text" style="padding: 0 .6rem;" class="active" onclick="clickTab(1)"
                                   href="<?php echo base_url() ?>home/visitaGuiada">Visita Guiada</a></li>
                <li class="tab "><a class="gris-2-text" style="padding: 0 .6rem;" onclick="clickTab(2)" href="<?php echo base_url() ?>home/mi-escaparate">Escaparate</a></li>
                <!--<li class="tab "><a onclick="clickTab(3)"
                                   href="<?php /*echo base_url() */ ?>home/mis-recomendaciones">Mis
                        Recomendaciones</a></li>-->
                <li class="tab "><a class="gris-2-text" style="padding: 0 .6rem;" onclick="clickTab(4)" href="<?php echo base_url() ?>home/mis-promociones">Promociones</a></li>
                <li class="tab "><a class="gris-2-text" style="padding: 0 .6rem;" onclick="clickTab(5)" href="<?php echo base_url() ?>home/mis-eventos">Eventos</a></li>
                <li class="tab "><a class="gris-2-text" style="padding: 0 .6rem;" onclick="clickTab(6)" href="<?php echo base_url() ?>home/mis-solicitudes">Solicitudes</a></li>
                <li class="tab "><a class="gris-2-text" style="padding: 0 .6rem;" onclick="clickTab(7)" href="<?php echo base_url() ?>home/mis-estadisticas">Estad&iacute;sticas</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="body-container">
        <a class="col s12 m6 l4" href="<?php echo base_url() ?>home/mi-escaparate">
            <div class="card-panel hoverable" style="height: 290px;">
                <div class="row center">
                    <img class=" responsive-img" width="100px"
                         src="<?php echo base_url() ?>/dist/img/alta/computadora.png" alt=""/>
                </div>
                <div class="row center">
                    <div class="gris-1-text" style=" font-size: 14.5px;">Mi Escaparate</div>
                    <p class="gris-2-text">Miles de clientes lo encontrar&aacute;n en Google y japybodas.com, una
                        plataforma de comunicaci&oacute;n y marketing integral online.</p>
                </div>
            </div>
        </a>
        <!-- <a class="col s12 m6 l4" href="<?php /*echo base_url() */ ?>home/misRecomendaciones">
            <div class="card-panel hoverable" style="height: 290px;">
                <div class="row center">
                    <img class=" responsive-img" width="100px"
                         src="<?php /*echo base_url() */ ?>/dist/img/alta/recomendaciones.png" alt=""/>
                </div>
                <div class="row center">
                    <div class="gris-1-text" style=" font-size: 14.5px;">Mis recomendaciones</div>
                    <p class="gris-2-text">Los Novios que han contratado servicios de proveedores los calificar&aacute;n
                        y podr&aacute;n recomendarlos, esto genera confianza y m&aacute;s ventas.</p>
                </div>
            </div>
        </a>-->
        <a class="col s12 m6 l4" href="<?php echo base_url() ?>home/misPromociones">
            <div class="card-panel hoverable" style="height: 290px;">
                <div class="row center">
                    <img class=" responsive-img" width="100px"
                         src="<?php echo base_url() ?>/dist/img/alta/promocion.png" alt=""/>
                </div>
                <div class="row center">
                    <div class="gris-1-text" style=" font-size: 14.5px;">Mis Promociones</div>
                    <p class="gris-2-text">Sus ofertas y promociones estar&aacute;n a la vista de todos mediante el
                        sitio y un APP, lo cual influir&aacute; en la decisi&oacute;n de compra de sus clientes.</p>
                </div>
            </div>
        </a>


        <a class="col s12 m6 l4" href="<?php echo base_url() ?>home/misEventos">
            <div class="card-panel hoverable" style="height: 290px;">
                <div class="row center">
                    <img class=" responsive-img" width="100px"
                         src="<?php echo base_url() ?>/dist/img/alta/calendario.png" alt=""/>
                </div>
                <div class="row center">
                    <div class="gris-1-text" style=" font-size: 14.5px;">Mis Eventos</div>
                    <p class="gris-2-text">P&oacute;ngase en contacto con los novios interesados en sus servicios y
                        aumente sus ventas a trav&eacute;s de publicar todos sus eventos, ferias, y dando a conocer la
                        calidad de sus servicios.</p>
                </div>
            </div>
        </a>
        <a class="col s12 m6 l4" href="<?php echo base_url() ?>home/misSolicitudes">
            <div class="card-panel hoverable" style="height: 290px;">
                <div class="row center">
                    <img class=" responsive-img" width="100px"
                         src="<?php echo base_url() ?>/dist/img/alta/solicitudes.png" alt=""/>
                </div>
                <div class="row center">
                    <div class="gris-1-text" style=" font-size: 14.5px;">Mis Solicitudes</div>
                    <p class="gris-2-text">Recibir&aacute; y responder&aacute; a sus mails de manera c&oacute;moda,
                        aunque tambi&eacute;n podr&aacute; contestar desde su cuenta de mail. Organice de forma sencilla
                        y r&aacute;pida todas las solicitudes que reciba.</p>
                </div>
            </div>
        </a>
        <a class="col s12 m6 l4" href="<?php echo base_url() ?>home/misEstadisticas">
            <div class="card-panel hoverable" style="height: 290px;">
                <div class="row center">
                    <img class=" responsive-img" width="100px"
                         src="<?php echo base_url() ?>/dist/img/alta/estadistica.png" alt=""/>
                </div>
                <div class="row center">
                    <div class="gris-1-text" style=" font-size: 14.5px;">Mis Estad&iacute;sticas</div>
                    <p class="gris-2-text">Sistema r&aacute;pido y seguro para buscar, responder y poner en orden las
                        solicitudes de cotizaci&oacute;n de los novios y estudiar estad&iacute;sticas.</p>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row center">
    <a href="<?php echo base_url() ?>home/altaEmpresas" class="btn white darken-5" style="color:black"><i
                class="fa fa-caret-left" aria-hidden="true"></i>&nbsp;Acceso empresas</a>
    <a href="<?php echo base_url() ?>registro/proveedor" class="btn dorado-2">Reg&iacute;strate gratis</a>
    <a href="<?php echo base_url() ?>home/serviciosPremium" class="btn white darken-5" style="color:black">Servicios
        Premium&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i></a>
</div>
<script>
    var url = [];
    url[1] = '<?php echo base_url() ?>home/visitaGuiada';
    url[2] = '<?php echo base_url() ?>home/mi-escaparate';
    url[3] = '<?php echo base_url() ?>home/mis-recomendaciones';
    url[4] = '<?php echo base_url() ?>home/misPromociones';
    url[5] = '<?php echo base_url() ?>home/misEventos';
    url[6] = '<?php echo base_url() ?>home/misSolicitudes';
    url[7] = '<?php echo base_url() ?>home/misEstadisticas';

    function clickTab(tab) {
        location.href = url[tab];
    }

</script>
