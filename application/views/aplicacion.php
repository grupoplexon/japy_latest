<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Descarga tu APP</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    </head>
    <body style="background-color: white;  ">
        <div class="container">
            
            <!-- <div class="row" style="position: relative;height: 30vh;">
                <div class="col s12 m3 l4"></div>
                <div class="col s12 m3 l4" style="color: #72D6E0;"><h1><u><b>JapyApps</b></u></h1></div>
                <div class="col s12 m3 l4"></div>
            </div> -->
            <div class="row" style="position: relative;min-height: 50vh; margin-top: 15%;">
                <div class="col s12 m6 offset-m3 l6">
                    <div class="card">
                        <div class="card-content" style="height: 140px;">
                        <!-- <span class="card-title black-text">Google Store</span> -->
                        <a class="gb_Me gb_vc gb_3e" href="https://play.google.com/store/apps/details?id=com.japybodas.app" title="Google Play Logo"><img style="width: 100%" class="gb_0a" src="https://www.gstatic.com/android/market_images/web/play_prism_hlock_2x.png" style=""></a>
                        </div>
                        <div class="card-action center-align">
                        <a href="https://play.google.com/store/apps/details?id=com.japybodas.app" class="btn">Descargar para Android</a>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 offset-m3 l6">
                    <div class="card">
                        <div class="card-content center-align" style="height: 140px;">
                        <!-- <span class="card-title black-text">Apple Store</span> -->
                        <a class="gb_Me gb_vc gb_3e" href="https://itunes.apple.com/mx/app/japy/id1453432572" title="Google Play Logo"><img class="gb_0a" src="https://www.apple.com/v/ios/app-store/b/images/overview/app_store_icon_large.jpg" style="margin-top: -2vh;"></a>
                        </div>
                        <div class="card-action center-align">
                        <a href="https://itunes.apple.com/mx/app/japy/id1453432572" class="btn">Descargar para IOS</a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </body>
</html>