
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>


                <div class="right-align">
                    <a class="waves-effect waves-light btn dorado-2" href="<?php echo $this->config->base_url() ?>App/ADExpositor/alta/<?php echo $id_expo ?>">
                        <i class="material-icons left">add_circle_outline</i>  Registrar expositor
                    </a>   
                </div>


                <div class="card">
                    <table class="responsive-table bordered highlight">
                        <thead>
                            <tr class="dorado-2">
                                <th data-field="id">#</th>
                                <th data-field="name">Nombre</th>
                                <th data-field="name">Giro</th>
                                <th data-field="name">Direcci&oacute;n</th>
                                <th data-field="name">Tel&eacute;fono</th>
                                <th data-field="name">Acci&oacute;n</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($expositores as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td ><?php echo $value->nombre ?></td>
                                    <td><?php echo $value->giro ?></td>
                                    <td><?php echo $value->direccion ?></td>
                                    <td ><?php echo $value->telefono ?></td>
                                    <td >
                                        <a style="color:#f9a797; cursor: pointer;" href="<?php echo $this->config->base_url() ?>App/ADExpositor/editar/<?php echo $value->id_expositor; ?>"><i class="material-icons">create</i></a>
                                        <span style="color:#f9a797; cursor: pointer;" onclick="eliminar_programa('<?php echo $value->id_expositor; ?>', '<?php echo $value->nombre; ?>')" ><i class="material-icons">delete</i></span>
                                    </td>
                                </tr>  
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">&nbsp;<div class="row">&nbsp;</div></div>
                <script>
                    function eliminar_programa(id, nombre) {
                        var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
                        if (r == true) {
                            location.href = "<?= $this->config->base_url(); ?>App/ADExpositor/eliminar/" + id;
                        }
                    }
                </script>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>