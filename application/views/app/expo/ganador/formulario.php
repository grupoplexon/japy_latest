
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>


                <div class="card">
                    <div class="card-content">
                        <span class="card-title"><b>GANADOR</b></span>
                        <form method="POST">

                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input placeholder="Nombre del ganador"  id="nombre" name="nombre" type="text" value="<?php echo isset($ganador) ? $ganador->nombre : ''; ?>" class="validate" required>
                                    <label for="nombre">Nombre*</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <input placeholder="Premio"  id="premio" name="premio" type="text" value="<?php echo isset($ganador) ? $ganador->premio : ''; ?>" >
                                    <label for="premio">Premio</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col m12 s12">
                                    <select name="id_expo">
                                        <?php foreach ($expos as $key => $value) { ?>
                                            <option value="<?php echo $value->id_expo; ?>" <?php echo ($id_expo == $value->id_expo) ? 'selected' : ''; ?>>
                                                <?php echo $value->nombre; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <label>Expo*</label>
                                </div>
                            </div>

                            
                            <button class="waves-effect waves-light btn" type="submit" style="width: 100%; margin-top: 30px;">FINALIZAR</button>

                        </form>
                    </div>
                </div>
                <div class="row">&nbsp;<div class="row">&nbsp;</div></div>
                <script>
                    function onReady() {
                        $('select').material_select();
                        $.datetimepicker.setLocale('es');
                        $('.datepicker').datetimepicker({
                            format: 'Y-m-d H:i',
                        });
                    }
                    
                </script> 
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>