
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>


                <div class="right-align">
                    <a class="waves-effect waves-light btn dorado-2" href="<?php echo $this->config->base_url() ?>App/ADGanador/alta/<?php echo $id_expo ?>">
                        <i class="material-icons left">add_circle_outline</i>  Registrar ganador
                    </a>   
                </div>


                <div class="card">
                    <table class="responsive-table bordered highlight">
                        <thead>
                            <tr class="dorado-2">
                                <th data-field="id">#</th>
                                <th data-field="name">Nombre</th>
                                <th data-field="name">Premio</th>
                                <th data-field="name">Acci&oacute;n</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($ganadores as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td ><?php echo $value->nombre ?></td>
                                    <td><?php echo $value->premio ?></td>
                                    <td >
                                        <a style="color:#f9a797; cursor: pointer;" href="<?php echo $this->config->base_url() ?>App/ADGanador/editar/<?php echo $value->id_ganador; ?>"><i class="material-icons">create</i></a>
                                        <span style="color:#f9a797; cursor: pointer;" onclick="eliminar_ganador('<?php echo $value->id_ganador; ?>', '<?php echo $value->nombre; ?>')" ><i class="material-icons">delete</i></span>
                                    </td>
                                </tr>  
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">&nbsp;<div class="row">&nbsp;</div></div>
                <script>
                    function eliminar_ganador(id, nombre) {
                        var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
                        if (r == true) {
                            location.href = "<?= $this->config->base_url(); ?>App/ADGanador/eliminar/" + id;
                        }
                    }
                </script>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>