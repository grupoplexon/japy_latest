<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
    <?php $this->view("app/menu") ?>
    <main>
        <div class="container">
            <div style="margin: 10px;"></div>
            <div class="row">
                <div class="col m4 s12 ">
                    <section id="seccion-expositores">
                        <ul class="collection z-depth-1">
                            <li class="collection-item dismissable grey lighten-3">
                                <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                    EXPOSITORES
                                    <div class="secondary-content">
                                        <a class="dorado-2-text" style=" cursor: pointer;"
                                           href="<?php echo $this->config->base_url() ?>App/ADExpositor/alta/<?php echo $expo['id_expo'] ?>">
                                            <i class="material-icons ">add</i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if (empty($expositores)) {
                                echo '<li class="collection-item"><small class="blue-grey-text lighten-4">No hay expositores</small></li>';
                            } else {
                                foreach ($expositores as $key => $value) {
                                    ?>
                                    <li class="collection-item dismissable">
                                        <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            <?php echo $value->nombre; ?>
                                            <div class="secondary-content">
                                                <a class="dorado-2-text" style="cursor: pointer;"
                                                   href="<?php echo $this->config->base_url() ?>App/ADExpositor/editar/<?php echo $value->id_expositor ?>">
                                                    <i class="material-icons">create</i>
                                                </a>
                                                <span class="dorado-2-text" style="cursor: pointer;"
                                                      onclick="eliminar_expositor('<?php echo $value->id_expositor; ?>', '<?php echo $value->nombre; ?>')">
                                                    <i class="material-icons">delete</i>
                                                </span>

                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }

                                echo '<li class="collection-item" style="text-align:right"><a class="dorado-2-text" href="' . $this->config->base_url() . 'App/ADExpositor/index/' . $expo['id_expo'] . '">Ver todos</a></li>';

                            }
                            ?>
                        </ul>
                    </section>
                    <section id="seccion-publicidad">
                        <ul class="collection z-depth-1">
                            <li class="collection-item dismissable grey lighten-3">
                                <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                    PUBLICIDAD
                                    <div class="secondary-content">
                                        <a class="dorado-2-text" style=" cursor: pointer;"
                                           href="<?php echo $this->config->base_url() ?>App/ADPublicidad/alta/<?php echo $expo['id_expo'] ?>">
                                            <i class="material-icons ">add</i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if (empty($publicidad)) {
                                echo '<li class="collection-item"><small class="blue-grey-text lighten-4">No hay publicidad</small></li>';
                            } else {
                                foreach ($publicidad as $key => $value) {
                                    ?>
                                    <li class="collection-item dismissable">
                                        <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            <img style="width: 60%; height: auto;" src="<?php echo $value->imagen; ?>">
                                            <div class="secondary-content">
                                                <a class="dorado-2-text" style="cursor: pointer;"
                                                   href="<?php echo $this->config->base_url() ?>App/ADPublicidad/editar/<?php echo $value->id_publicidad ?>">
                                                    <i class="material-icons">create</i>
                                                </a>
                                                <span class="dorado-2-text" style=" cursor: pointer;"
                                                      onclick="eliminar_publicidad('<?php echo $value->id_publicidad; ?>')">
                                                    <i class="material-icons">delete</i>
                                                </span>

                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }

                                echo '<li class="collection-item" style="text-align:right"><a class="dorado-2-text" href="' . $this->config->base_url() . 'App/ADPublicidad/index/' . $expo['id_expo'] . '">Ver todos</a></li>';

                            }
                            ?>
                        </ul>
                    </section>
                    <section id="seccion-programa">
                        <ul class="collection z-depth-1">
                            <li class="collection-item dismissable grey lighten-3">
                                <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                    PROGRAMA
                                    <div class="secondary-content">
                                        <a class="dorado-2-text" style="cursor: pointer;"
                                           href="<?php echo $this->config->base_url() ?>App/ADPrograma/alta/<?php echo $expo['id_expo'] ?>">
                                            <i class="material-icons ">add</i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if (empty($programa)) {
                                echo '<li class="collection-item"><small class="blue-grey-text lighten-4">No hay programa</small></li>';
                            } else {
                                foreach ($programa as $key => $value) {
                                    ?>
                                    <li class="collection-item dismissable">
                                        <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            <?php echo $value->titulo; ?>
                                            <div class="secondary-content">
                                                <a class="dorado-2-text" style="cursor: pointer;"
                                                   href="<?php echo $this->config->base_url() ?>App/ADPrograma/editar/<?php echo $value->id_programa ?>">
                                                    <i class="material-icons">create</i>
                                                </a>
                                                <span class="dorado-2-text" style="cursor: pointer;"
                                                      onclick="eliminar_programa('<?php echo $value->id_programa; ?>', '<?php echo $value->titulo; ?>')">
                                                    <i class="material-icons">delete</i>
                                                </span>

                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }

                                echo '<li class="collection-item" style="text-align:right"><a class="dorado-2-text" href="' . $this->config->base_url() . 'App/ADPrograma/index/' . $expo['id_expo'] . '">Ver todos</a></li>';

                            }
                            ?>
                        </ul>
                    </section>
                </div>
                <div class="col m4 s12">
                    <section id="seccion-plano">
                        <ul class="collection z-depth-1">
                            <li class="collection-item dismissable grey lighten-3">
                                <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                    PLANO DE LA EXPO
                                    <div class="secondary-content">
                                        <a class="dorado-2-text" style="cursor: pointer;"
                                           href="<?php echo $this->config->base_url() ?>App/ADPlano/index/<?php echo $expo['id_expo'] ?>">
                                            <i class="material-icons ">add</i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if (empty($expo['plano'])) {
                                echo '<li class="collection-item"><small class="blue-grey-text lighten-4">No hay plano del evento</small></li>';
                            } else {
                                echo "<li class='collection-item'><img class='responsive-img' src='" . $expo['plano'] . "' ></li>";
                            }
                            ?>
                        </ul>
                    </section>
                    <section id="seccion-lugares">
                        <ul class="collection z-depth-1">
                            <li class="collection-item dismissable grey lighten-3">
                                <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                    LUGARES PARA VISITAR
                                    <div class="secondary-content">
                                        <a class="dorado-2-text" style="cursor: pointer;"
                                           href="<?php echo $this->config->base_url() ?>App/ADLugar/alta/<?php echo $expo['id_expo'] ?>">
                                            <i class="material-icons ">add</i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if (empty($lugares)) {
                                echo '<li class="collection-item"><small class="blue-grey-text lighten-4">No hay lugares</small></li>';
                            } else {
                                foreach ($lugares as $key => $value) {
                                    ?>
                                    <li class="collection-item dismissable">
                                        <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            <?php echo $value->titulo_es; ?>
                                            <div class="secondary-content">
                                                <a class="dorado-2-text" style="cursor: pointer;"
                                                   href="<?php echo $this->config->base_url() ?>App/ADLugar/editar/<?php echo $value->id_vivegdl ?>">
                                                    <i class="material-icons">create</i>
                                                </a>
                                                <span class="dorado-2-text" style="cursor: pointer;"
                                                      onclick="eliminar_lugar('<?php echo $value->id_vivegdl; ?>', '<?php echo $value->titulo_es; ?>')">
                                                    <i class="material-icons">delete</i>
                                                </span>

                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }

                                echo '<li class="collection-item" style="text-align:right"><a class="dorado-2-text" href="' . $this->config->base_url() . 'App/ADLugar/index/' . $expo['id_expo'] . '">Ver todos</a></li>';

                            }
                            ?>
                        </ul>
                    </section>
                </div>
                <div class="col m4 s12">
                    <!--<section id="seccion-notificaciones">
                            <ul class="collection z-depth-1">
                                <li class="collection-item dismissable grey lighten-3">
                                    <div>NOTIFICACIONES
                                        <a class="secondary-content" href="<?php echo $this->config->base_url() ?>App/ADNotificacion">
                                            <i class="material-icons">dehaze</i>
                                        </a>
                                        <a class="secondary-content" href="<?php echo $this->config->base_url() ?>App/ADNotificacion/alta">
                                            <i class="material-icons">add</i>
                                        </a>
                                    </div>
                                </li>
                                <?php foreach ($notificaciones as $key => $value) { ?>
                                    <li class="collection-item"> 
                                        <?php echo $value->titulo . ' / ' . strftime("%d-%B-%Y %I:%M %p", strtotime($value->fecha_creacion)); ?>
                                    </li>
                                <?php } ?>
                                <?php
                    if (empty($notificaciones)) {
                        echo '<li class="collection-item"><small class="blue-grey-text lighten-4">No hay notificaciones</small></li>';
                    }
                    ?>
                            </ul>
                        </section>-->
                    <section id="seccion-patrocinadores">
                        <ul class="collection z-depth-1">
                            <li class="collection-item dismissable grey lighten-3">
                                <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                    PATROCINADORES
                                    <div class="secondary-content">
                                        <a class="dorado-2-text" style="cursor: pointer;"
                                           href="<?php echo $this->config->base_url() ?>App/ADPatrocinador/alta/<?php echo $expo['id_expo'] ?>">
                                            <i class="material-icons ">add</i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if (empty($patrocinadores)) {
                                echo '<li class="collection-item"><small class="blue-grey-text lighten-4">No hay patrocinadores</small></li>';
                            } else {
                                foreach ($patrocinadores as $key => $value) {
                                    ?>
                                    <li class="collection-item dismissable">
                                        <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            <?php echo $value->nombre; ?>
                                            <div class="secondary-content">
                                                <a class="dorado-2-text" style="cursor: pointer;"
                                                   href="<?php echo $this->config->base_url() ?>App/ADPatrocinador/editar/<?php echo $value->id_patrocinador ?>">
                                                    <i class="material-icons">create</i>
                                                </a>
                                                <span class="dorado-2-text" style="cursor: pointer;"
                                                      onclick="eliminar_patrocinador('<?php echo $value->id_patrocinador; ?>', '<?php echo $value->nombre; ?>')">
                                                    <i class="material-icons">delete</i>
                                                </span>

                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }

                                echo '<li class="collection-item" style="text-align:right"><a class="dorado-2-text" href="' . $this->config->base_url() . 'App/ADPatrocinador/index/' . $expo['id_expo'] . '">Ver todos</a></li>';

                            }
                            ?>
                        </ul>
                    </section>

                    <section id="seccion-ganadores">
                        <ul class="collection z-depth-1">
                            <li class="collection-item dismissable grey lighten-3">
                                <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                    GANADORES
                                    <div class="secondary-content">
                                        <a class="dorado-2-text" style="cursor: pointer;"
                                           href="<?php echo $this->config->base_url() ?>App/ADGanador/alta/<?php echo $expo['id_expo'] ?>">
                                            <i class="material-icons ">add</i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php
                            if (empty($ganadores)) {
                                echo '<li class="collection-item"><small class="blue-grey-text lighten-4">No hay ganadores</small></li>';
                            } else {
                                foreach ($ganadores as $key => $value) {
                                    ?>
                                    <li class="collection-item dismissable">
                                        <div style="width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            <?php echo $value->nombre; ?>
                                            <div class="secondary-content">
                                                <a class="dorado-2-text" style="cursor: pointer;"
                                                   href="<?php echo $this->config->base_url() ?>App/ADGanador/editar/<?php echo $value->id_ganador ?>">
                                                    <i class="material-icons">create</i>
                                                </a>
                                                <span class="dorado-2-text" style="cursor: pointer;"
                                                      onclick="eliminar_ganador('<?php echo $value->id_ganador; ?>', '<?php echo $value->nombre; ?>')">
                                                    <i class="material-icons">delete</i>
                                                </span>

                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                                echo '<li class="collection-item" style="text-align:right"><a class="dorado-2-text" href="' . $this->config->base_url() . 'App/ADGanador/index/' . $expo['id_expo'] . '">Ver todos</a></li>';
                            }
                            ?>
                        </ul>
                    </section>
                </div>

            </div>
        </div>
        <div class="row">&nbsp;</div>
    </main>
    <script>
        function onReady() {
            $('.collapsible').collapsible({
                accordion: false
            });
        }

        function eliminar_expositor(id, nombre) {
            var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
            if (r == true) {
                location.href = "<?= $this->config->base_url(); ?>App/ADExpositor/eliminar/" + id;
            }
        }

        function eliminar_publicidad(id) {
            var r = confirm("Esta seguro que desea eliminar esta publicidad?");
            if (r == true) {
                location.href = "<?= $this->config->base_url(); ?>App/ADPublicidad/eliminar/" + id;
            }
        }

        function eliminar_programa(id, nombre) {
            var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
            if (r == true) {
                location.href = "<?= $this->config->base_url(); ?>App/ADPrograma/eliminar/" + id;
            }
        }

        function eliminar_lugar(id, nombre) {
            var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
            if (r == true) {
                location.href = "<?= $this->config->base_url(); ?>App/ADLugar/eliminar/" + id;
            }
        }

        function eliminar_patrocinador(id, nombre) {
            var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
            if (r == true) {
                location.href = "<?= $this->config->base_url(); ?>App/ADPatrocinador/eliminar/" + id;
            }
        }

        function eliminar_ganador(id, nombre) {
            var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
            if (r == true) {
                location.href = "<?= $this->config->base_url(); ?>App/ADGanador/eliminar/" + id;
            }
        }


    </script>
    <?php $this->view("app/footer") ?>
</body>
</html>