<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
    <?php $this->view("app/menu") ?>
    <main>
        <div class="container">
            <div style="margin: 10px;"></div>

            <div class="container">
                <form method="POST">
                    <div class="card-panel white">
                        <div class="card-content">
                            <div class="section">
                                <div class="row">
                                    <div class="col s6"><h5>Expo</h5></div>
                                    <?php if (isset($datos)) { ?>
                                        <div class="col s6">
                                            <div class="switch right">
                                                <label>
                                                    Inactivo
                                                    <input type="checkbox"
                                                           name="activo" <?php if ($datos['activo'] == 1) {
                                                        echo 'checked="checked"';
                                                    } ?>>
                                                    <span class="lever"></span>
                                                    Activo
                                                </label>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="divider"></div>
                            <div class="section">
                                <div class="row">

                                    <div class="input-field col s12 m8">
                                        <input placeholder="Nombre de la expo" id="nombre" name="nombre" type="text"
                                               length="20" class="validate"
                                               value="<?php echo isset($datos) ? $datos['nombre'] : '' ?>" required>
                                        <label for="nombre">Nombre</label>
                                    </div>
                                    <div class="input-field col s12 m4">
                                        <i class="material-icons prefix dorado-2-text">attach_money</i>
                                        <input id="icon_money" type="number" name="costo"
                                               value="<?php echo isset($datos) ? $datos['costo'] : 0 ?>"
                                               class="validate" required>
                                        <label for="icon_money">Costo del evento</label>
                                    </div>


                                    <form id="google" name="google" action="#" style="margin-bottom: 10px;">
                                        <div class="input-field col s10">
                                            <input placeholder="Direcci&oacute;n del evento"
                                                   value="<?php echo isset($datos) ? $datos['ubicacion'] : '' ?>"
                                                   id="address" name="ubicacion" type="text" length="70"
                                                   class="validate" required>
                                            <label for="address">Ubicaci&oacute;n</label>
                                        </div>
                                        <div class="col s2" style="margin-top: 10px;">
                                            <button class="waves-effect waves-light btn tooltipped" data-position="top"
                                                    data-delay="50" data-tooltip="Buscar ubicacion en el mapa"
                                                    id="search" type="button"><i class="material-icons">search</i>
                                            </button>
                                        </div>

                                        <div class="col s12" id="map_canvas"
                                             style="width:100%;height:300px; margin-top: 20px;"></div>

                                        <input type="hidden" name="latitud" id="latitud" value="<?php echo $datos['latitud'] ?>"/>
                                        <input type="hidden" name="longitud" id="longitud" value="<?php echo $datos['longitud'] ?>"/>
                                    </form>


                                    <div class="input-field col s6">
                                        <input id="icon_prefix2" type="date" class="datepicker" name="inicio"
                                               value="<?php echo isset($datos) ? $datos['Home'] : '' ?>" required>
                                        <label for="icon_prefix2">Fecha inicio</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="icon_prefix" type="date" class="datepicker" name="fin"
                                               value="<?php echo isset($datos) ? $datos['fin'] : '' ?>" required>
                                        <label for="icon_prefix">Fecha t&eacute;rmino</label>
                                    </div>

                                    <div class="col offset-m6 s6 card-panel red darken-1 white-text"
                                         style="<?php echo isset($mensaje) ? '' : 'display:none;' ?>"><?php echo isset($mensaje) ? $mensaje : '' ?></div>
                                </div>

                                <div class="row" style="padding: 10px;">
                                    <div class="col s4">
                                        <img class="responsive-img" id="img"
                                             src="<?php echo (isset($datos) && !empty($datos['imagen'])) ? $datos['imagen'] : 'https://placeholdit.imgix.net/~text?txtsize=17&txt=200x200&w=200&h=200&txttrack=0' ?>"
                                             alt=""/>
                                        <input type="hidden" name="imagen"
                                               value="<?php echo isset($datos) ? $datos['imagen'] : ''; ?>"/>
                                    </div>
                                    <div class="col s8">
                                        <div class="file-field input-field">
                                            <h6 class="center-align"><b>IMAGEN PRINCIPAL</b></h6>
                                            <div class="btn">
                                                <span>Imagen</span>
                                                <input type="file" accept="image/*" onchange='openFile(event)'>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="padding: 10px;">
                                    <div class="col s4">
                                        <img class="responsive-img" id="img-plano"
                                             src="<?php echo (isset($datos) && !empty($datos['plano'])) ? $datos['plano'] : 'https://placeholdit.imgix.net/~text?txtsize=17&txt=500x500&w=500&h=500&txttrack=0' ?>"
                                             alt=""/>
                                        <input type="hidden" name="plano"
                                               value="<?php echo isset($datos) ? $datos['plano'] : ''; ?>"/>
                                    </div>
                                    <div class="col s8">
                                        <div class="file-field input-field">
                                            <h6 class="center-align"><b>PLANO DE LA EXPO</b></h6>
                                            <div class="btn">
                                                <span>Imagen</span>
                                                <input type="file" accept="image/*" onchange='openFilePlano(event)'>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="waves-effect waves-light btn" type="submit"
                                        style="width: 100%; margin-top: 30px;">FINALIZAR
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">&nbsp;<div class="row">&nbsp;</div>
            </div>
            <script type="text/javascript"
                    src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key') ?>&libraries=adsense&amp;sensor=true&amp;language=es"></script>
            <script>
                var map;

                function onReady() {
                    $('.datepicker').pickadate({
                        selectMonths: true,
                        selectYears: 15,
                        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                        today: 'Hoy',
                        clear: 'Limpiar',
                        close: 'Cerrar',
                        format: 'yyyy/mm/dd',
                        formatSubmit: 'yyyy/mm/dd',
                        min: new Date()
                    });
                    load_map();
                    <?php if (isset($datos) && $datos['ubicacion'] != '') { ?>
                    $('#search').click();
                    <?php } ?>
                }

                function load_map() {
                    $('#search').on('click', function () {
                        var address = $('#address').val();
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'address': address}, geocodeResult);
                    });

                    $('#address').on('change', function () {
                        var address = $('#address').val();
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'address': address}, geocodeResult);
                    });

                    var myLatlng = new google.maps.LatLng(20.68009, -101.35403);
                    var myOptions = {
                        zoom: 4,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    map = new google.maps.Map($("#map_canvas").get(0), myOptions);
                }

                function geocodeResult(results, status) {
                    if (status == 'OK') {
                        var mapOptions = {
                            center: results[0].geometry.location,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map($("#map_canvas").get(0), mapOptions);
                        map.fitBounds(results[0].geometry.viewport);
                        var markerOptions = {position: results[0].geometry.location, draggable: true};
                        var marker = new google.maps.Marker(markerOptions);
                        var markerLatLng = marker.getPosition();

                        marker.setMap(map);

                        google.maps.event.addListener(marker, 'drag', function (event) {
                            $('#latitud').val(event.latLng.lat());
                            $('#longitud').val(event.latLng.lng());
                            geocodePosition(marker.getPosition());
                        });

                        google.maps.event.addListener(marker, 'dragend', function (event) {
                            $('#latitud').val(event.latLng.lat());
                            $('#longitud').val(event.latLng.lng());
                            geocodePosition(marker.getPosition());
                        });

                    } else {
                        alert("Geocoding no tuvo &eacute;xito debido a: " + status);
                    }
                }

                function geocodePosition(pos) {
                    var geocoder = new google.maps.Geocoder();

                    geocoder.geocode({
                        latLng: pos
                    }, function (responses) {
                        if (responses && responses.length > 0) {
                            $('#address').val(responses[0].formatted_address);
                        }
                    });
                }

                function openFile(event) {
                    var input = event.target;

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = document.createElement("img");
                        img.src = e.target.result;

                        var canvas = document.createElement("canvas");
                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(img, 0, 0);

                        var MAX_WIDTH = 500;
                        var MAX_HEIGHT = 500;
                        var width = img.width;
                        var height = img.height;

                        if (width > height) {
                            if (width > MAX_WIDTH) {
                                height *= MAX_WIDTH / width;
                                width = MAX_WIDTH;
                            }
                        } else {
                            if (height > MAX_HEIGHT) {
                                width *= MAX_HEIGHT / height;
                                height = MAX_HEIGHT;
                            }
                        }
                        canvas.width = width;
                        canvas.height = height;
                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(img, 0, 0, width, height);

                        var dataurl = canvas.toDataURL("image/jpg");
                        document.getElementById('img').src = img.src;
                        $('[name="imagen"]').val(img.src);
                    };
                    reader.readAsDataURL(input.files[0]);
                }

                function openFilePlano(event) {
                    var input = event.target;

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = document.createElement("img");
                        img.src = e.target.result;

                        var canvas = document.createElement("canvas");
                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(img, 0, 0);

                        var MAX_WIDTH = 500;
                        var MAX_HEIGHT = 500;
                        var width = img.width;
                        var height = img.height;

                        if (width > height) {
                            if (width > MAX_WIDTH) {
                                height *= MAX_WIDTH / width;
                                width = MAX_WIDTH;
                            }
                        } else {
                            if (height > MAX_HEIGHT) {
                                width *= MAX_HEIGHT / height;
                                height = MAX_HEIGHT;
                            }
                        }
                        canvas.width = width;
                        canvas.height = height;
                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(img, 0, 0, width, height);

                        var dataurl = canvas.toDataURL("image/jpg");
                        document.getElementById('img-plano').src = img.src;
                        $('[name="plano"]').val(img.src);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            </script>
        </div>
    </main>
    <?php $this->view("app/footer") ?>
</body>
</html>