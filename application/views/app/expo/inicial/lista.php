<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>

                <div class="right-align">
                    <a class="waves-effect waves-light btn" href="<?php echo $this->config->base_url() ?>App/ADExpo/alta">
                        <i class="material-icons left">add_circle_outline</i>  Registrar expo
                    </a>
                </div>
                <div class="row">
                    <?php
                    if (isset($expos)) {
                        foreach ($expos as $key => $value) {
                            ?>
                            <div class="col s12 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="<?php echo empty($value->imagen) ? 'https://placeholdit.imgix.net/~text?txtsize=17&txt=Imagen&w=300&h=200&txttrack=0' : $value->imagen ?>">
                                        <span class="card-title"><?php echo $value->nombre ?></span>
                                    </div>
                                    <div class="card-content">
                                        <p><b>Direcci&oacute;n:</b> <?php echo $value->ubicacion ?></p>
                                        <p><b>Fecha comienzo:</b> <?php echo strftime("%A %d de %B del %Y", strtotime($value->inicio)); ?></p>
                                        <p><b>Fecha t&eacute;rmino:</b> <?php echo strftime("%A %d de %B del %Y", strtotime($value->fin)); ?></p>
                                    </div>
                                    <div class="card-action">
                                        <a class="dorado-2-text" style="cursor: pointer;" href="<?php echo $this->config->base_url() ?>App/ADExpo/editar/<?php echo $value->id_expo ?>">
                                            Editar
                                        </a>
                                        <a class="dorado-2-text" style="cursor: pointer;" href="<?php echo $this->config->base_url() ?>App/ADExpo/expo/<?php echo $value->id_expo ?>">
                                            Detalle
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="row">&nbsp;<div class="row">&nbsp;</div></div>
                <script>
                    function eliminar(nombre, id) {
                        var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
                        if (r == true) {
                            location.href = "<?= $this->config->base_url(); ?>App/ADExpo/eliminar/" + id;
                        }
                    }
                </script>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>