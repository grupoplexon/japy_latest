<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
    <?php $this->view("app/menu") ?>
    <main>
        <div class="container">
            <div style="margin: 10px;"></div>


            <div class="card">
                <div class="card-content">
                    <span class="card-title"><b>LUGARES PARA VISITAR</b></span>
                    <form method="POST">

                        <div class="row">
                            <div class="input-field col m6 s12">
                                <input placeholder="T&iacute;tulo en español" id="titulo_es" name="titulo_es"
                                       type="text" value="<?php echo isset($lugar) ? $lugar->titulo_es : ''; ?>"
                                       class="validate" required>
                                <label for="titulo_es">Titulo en español*</label>
                            </div>
                            <div class="input-field col m6 s12">
                                <input placeholder="T&iacute;tulo en ingles" id="titulo_en" name="titulo_en" type="text"
                                       value="<?php echo isset($lugar) ? $lugar->titulo_en : ''; ?>" class="validate"
                                       required>
                                <label for="titulo_en">Titulo en ingles*</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m12 s12">
                                <select name="id_expo">
                                    <?php foreach ($expos as $key => $value) { ?>
                                        <option value="<?php echo $value->id_expo; ?>" <?php echo ($id_expo == $value->id_expo) ? 'selected' : ''; ?>>
                                            <?php echo $value->nombre; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <label>Expo*</label>
                            </div>
                        </div>

                        <div class="row" style="padding: 10px;">
                            <div class="col s4">
                                <img class="responsive-img" id="img"
                                     src="<?php echo (isset($lugar) && !empty($lugar->imagen)) ? $lugar->imagen : 'https://placeholdit.imgix.net/~text?txtsize=17&txt=200x200&w=200&h=200&txttrack=0' ?>"
                                     alt=""/>
                                <input type="hidden" name="imagen"
                                       value="<?php echo isset($lugar) ? $lugar->imagen : ''; ?>"/>
                            </div>
                            <div class="col s8">
                                <div class="file-field input-field">
                                    <h6 class="center-align"><b>FOTO</b></h6>
                                    <div class="btn">
                                        <span>Imagen</span>
                                        <input type="file" accept="image/*" onchange='openFile(event)'>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="waves-effect waves-light btn" type="submit"
                                style="width: 100%; margin-top: 30px;">FINALIZAR
                        </button>

                    </form>
                </div>
            </div>
            <div class="row">&nbsp;<div class="row">&nbsp;</div>
            </div>
            <script>
                function onReady() {
                    $('select').material_select();
                    $.datetimepicker.setLocale('es');
                    $('.datepicker').datetimepicker({
                        format: 'Y-m-d H:i',
                    });
                }

                function openFile(event) {
                    var input = event.target;

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = document.createElement("img");
                        img.src = e.target.result;

                        var canvas = document.createElement("canvas");
                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(img, 0, 0);

                        var MAX_WIDTH = 500;
                        var MAX_HEIGHT = 500;
                        var width = img.width;
                        var height = img.height;

                        if (width > height) {
                            if (width > MAX_WIDTH) {
                                height *= MAX_WIDTH / width;
                                width = MAX_WIDTH;
                            }
                        } else {
                            if (height > MAX_HEIGHT) {
                                width *= MAX_HEIGHT / height;
                                height = MAX_HEIGHT;
                            }
                        }
                        canvas.width = width;
                        canvas.height = height;
                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(img, 0, 0, width, height);

                        var dataurl = canvas.toDataURL("image/jpg");
                        document.getElementById('img').src = img.src;
                        $('[name="imagen"]').val(img.src);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            </script>
        </div>
    </main>
    <?php $this->view("app/footer") ?>
</body>
</html>