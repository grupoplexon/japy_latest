
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>
                <div class="row">
                   
                    <a class="center waves-effect waves-light btn" href="<?php echo $this->config->base_url() ?>App/ADLugar/<?php if($back == 0){echo "lugar/";} else {echo "index/";}?><?php if(isset($id_back)){echo $id_back;} ?>">
                        <i class="material-icons left">arrow_back</i>
                    </a>   
                
                    <a class="right waves-effect waves-light btn" href="<?php echo $this->config->base_url() ?>App/ADLugar/altaLugar/<?php if(isset($id_back)){echo $id_parent;} ?>">
                        <i class="material-icons left">add_circle_outline</i>  Registrar nuevo lugar en <?php echo $titulo ?>
                    </a>  
                </div>

                <div class="card">
                    <table class="responsive-table bordered highlight">
                        <thead>
                            <tr class="dorado-2">
                                <th data-field="id">#</th>
                                <th data-field="name">T&iacute;tulo español</th>
                                <?php if(isset($lugares) && count($lugares)>0 && $lugares[0]->parent == null) {?>
                                <th data-field="name">T&iacute;tulo ingles</th>
                                <?php } else { ?>
                                <th data-field="name">Descripci&oacute;n español</th>
                                <?php } ?>
                                <th data-field="name">Numero de lugares</th>
                                <th data-field="name">Ver lugares</th>
                                <th data-field="name">Acci&oacute;n</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($lugares as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $value->titulo_es ?></td>
                                    <?php if(isset($lugares) && count($lugares)>0 && $lugares[0]->parent == null) {?>
                                    <td><?php echo $value->titulo_en ?></td>
                                    <?php } else { ?>
                                    <td><?php echo $value->descripcion_es ?></td>
                                    <?php } ?>
                                    <td><?php echo $value->hijos ?></td>
                                    <td><a style="color:#f9a797; cursor: pointer;" href="<?php echo $this->config->base_url() ?>App/ADLugar/lugar/<?php echo $value->id_vivegdl; ?>"><i class="material-icons">visibility</i></a></td>
                                    <td>
                                        <a style="color:#f9a797; cursor: pointer;" href="<?php echo $this->config->base_url() ?>App/ADLugar/editarLugar/<?php echo $value->id_vivegdl; ?>"><i class="material-icons">create</i></a>
                                        <span style="color:#f9a797; cursor: pointer;" onclick="eliminar_lugar('<?php echo $value->id_vivegdl; ?>', '<?php echo $value->titulo_es; ?>')" ><i class="material-icons">delete</i></span>
                                    </td>
                                </tr>  
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">&nbsp;<div class="row">&nbsp;</div></div>
                <script>
                    function eliminar_lugar(id, nombre) {
                        var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
                        if (r == true) {
                            location.href = "<?= $this->config->base_url(); ?>App/ADLugar/eliminar/" + id;
                        }
                    }
                </script>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>