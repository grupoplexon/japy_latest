<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
    <?php $this->view("app/menu") ?>
    <main>
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <span class="card-title"><b>Plano</b></span>
                    <form method="post"
                          action="<?php echo $this->config->base_url() ?>App/ADPlano/subirPlano">
                        <div class="row" style="padding: 10px;">
                            <input type="hidden" name="id_expo" value="<?php echo $expo['id_expo']; ?>"/>
                            <div class="col s6">
                                <img class="responsive-img" id="img"
                                     src="<?php echo (isset($expo) && !empty($expo['plano'])) ? $expo['plano'] : 'https://placeholdit.imgix.net/~text?txtsize=17&txt=500x500&w=500&h=500&txttrack=0' ?>"
                                     alt=""/>
                                <input type="hidden" name="plano"
                                       value="<?php echo isset($expo) ? $expo['plano'] : ''; ?>"/>
                            </div>
                            <div class="col s6">
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>Imagen</span>
                                        <input type="file" accept="image/*" onchange='openFile(event)'>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="waves-effect waves-light btn" type="submit"
                                style="width: 100%; margin-top: 20px;">FINALIZAR
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">&nbsp;<div class="row">&nbsp;</div>
        </div>
        <script>
            function openFile(event) {
                var input = event.target;

                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = document.createElement("img");
                    img.src = e.target.result;

                    var canvas = document.createElement("canvas");
                    var ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0);

                    var MAX_WIDTH = 500;
                    var MAX_HEIGHT = 500;
                    var width = img.width;
                    var height = img.height;

                    if (width > height) {
                        if (width > MAX_WIDTH) {
                            height *= MAX_WIDTH / width;
                            width = MAX_WIDTH;
                        }
                    } else {
                        if (height > MAX_HEIGHT) {
                            width *= MAX_HEIGHT / height;
                            height = MAX_HEIGHT;
                        }
                    }
                    canvas.width = width;
                    canvas.height = height;
                    var ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0, width, height);

                    var dataurl = canvas.toDataURL("image/jpg");
                    document.getElementById('img').src = img.src;
                    $('[name="plano"]').val(img.src);
                };
                reader.readAsDataURL(input.files[0]);
            }
        </script>
    </main>
    <?php $this->view("app/footer") ?>
</body>
</html>
