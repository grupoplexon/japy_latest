
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>


                <div class="card">
                    <div class="card-content">
                        <span class="card-title"><b>PROGRAMA</b></span>
                        <form method="POST">

                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input placeholder="T&iacute;tulo del evento" id="titulo" name="titulo" type="text" value="<?php echo isset($programa) ? $programa->titulo : ''; ?>" class="validate" required>
                                    <label for="titulo">Titulo*</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <select name="id_expo">
                                        <?php foreach ($expos as $key => $value) { ?>
                                            <option value="<?php echo $value->id_expo; ?>" <?php echo ($id_expo == $value->id_expo) ? 'selected' : ''; ?>>
                                                <?php echo $value->nombre; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <label>Expo</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="descripcion" name="descripcion" class="materialize-textarea" required><?php echo isset($programa) ? $programa->descripcion : ''; ?></textarea>
                                    <label for="descripcion">Descripci&oacute;n*</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input placeholder="Ponente" id="ponente" name="ponente" value="<?php echo isset($programa) ? $programa->ponente : ''; ?>" type="text" class="validate">
                                    <label for="ponente">Ponente</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <input placeholder="Lugar" id="lugar" name="lugar" value="<?php echo isset($programa) ? $programa->lugar : ''; ?>" type="text" class="validate">
                                    <label for="lugar">Lugar</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input type="text" class="datepicker" id="fecha_inicio" name="fecha_i" value="<?php echo isset($programa) ? $programa->fecha_i : ''; ?>" required>
                                    <label for="fecha_inicio">Fecha Inicio*</label>
                                </div>


                                <div class="input-field col m6 s12">
                                    <input type="text" class="datepicker" id="fecha_termino" name="fecha_t" value="<?php echo isset($programa) ? $programa->fecha_t : ''; ?>" required>
                                    <label for="fecha_termino">Fecha T&eacute;rmino*</label>
                                </div>
                            </div>

                            <button class="waves-effect waves-light btn" type="submit" style="width: 100%; margin-top: 30px;">FINALIZAR</button>

                        </form>
                    </div>
                </div>
                <div class="row">&nbsp;<div class="row">&nbsp;</div></div>
                <script>
                    function onReady() {
                        $('select').material_select();
                        $.datetimepicker.setLocale('es');
                        $('.datepicker').datetimepicker({
                            format: 'Y-m-d H:i',
                        });
                    }
                </script> 
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>