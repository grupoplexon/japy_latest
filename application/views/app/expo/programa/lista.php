
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>


                <div class="right-align">
                    <a class="waves-effect waves-light btn" href="<?php echo $this->config->base_url() ?>App/ADPrograma/alta/<?php echo $id_expo ?>">
                        <i class="material-icons left">add_circle_outline</i>  Registrar programa
                    </a>   
                </div>


                <div class="card">
                    <table class="responsive-table bordered highlight">
                        <thead>
                            <tr class="dorado-2">
                                <th data-field="id">#</th>
                                <th data-field="name">T&iacute;tulo</th>
                                <th data-field="name">Descripci&oacute;n</th>
                                <th data-field="name">Fecha Inicio</th>
                                <th data-field="name">Fecha T&eacute;rmino</th>
                                <th data-field="name">Ponente</th>
                                <th data-field="name">Lugar</th>
                                <th data-field="name">Acci&oacute;n</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($programas as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $value->titulo ?></td>
                                    <td><?php echo $value->descripcion ?></td>
                                    <td><?php echo strftime("%A %d de %B de %Y", strtotime($value->fecha_i)); ?></td>
                                    <td><?php echo strftime("%A %d de %B de %Y", strtotime($value->fecha_t)); ?></td>
                                    <td><?php echo $value->ponente ?></td>
                                    <td><?php echo $value->lugar ?></td>
                                    <td>
                                        <a style="color:#f9a797; cursor: pointer;" href="<?php echo $this->config->base_url() ?>App/ADPrograma/editar/<?php echo $value->id_programa; ?>"><i class="material-icons">create</i></a>
                                        <span style="color:#f9a797; cursor: pointer;" onclick="eliminar_programa('<?php echo $value->id_programa; ?>', '<?php echo $value->titulo; ?>')" ><i class="material-icons">delete</i></span>
                                    </td>
                                </tr>  
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">&nbsp;<div class="row">&nbsp;</div></div>
                <script>
                    function eliminar_programa(id, nombre) {
                        var r = confirm("Esta seguro que desea eliminar " + nombre + "?");
                        if (r == true) {
                            location.href = "<?= $this->config->base_url(); ?>App/ADPrograma/eliminar/" + id;
                        }
                    }
                </script>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>