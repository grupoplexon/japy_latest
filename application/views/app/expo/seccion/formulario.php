
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>

                <div class="card">
                    <div class="card-content">
                        <span class="card-title"><b><?php echo $seccion->nombre; ?></b></span>
                        <form method="post">
                            <input type="hidden" name="id_seccion" value="<?php echo $seccion->id_seccion; ?>" />
                            <div class="section">
                                <div class="input-field">
                                    <textarea id="textarea1" class="materialize-textarea" name="descripcion" required><?php echo $seccion->descripcion; ?></textarea>
                                    <label for="textarea1">Descripci&oacute;n</label>
                                </div>
                            </div>
                            <div class="divider"></div>
                            <div class="section row teal lighten-5">
                                <div class="col s4">
                                    <img class="responsive-img" src="<?php echo (empty($seccion->imagen)) ? 'https://placeholdit.imgix.net/~text?txtsize=17&txt=300x200&w=300&h=200&txttrack=0' : $seccion->imagen ?>" alt=""/>
                                    <input type="hidden" name="url_anterior" value="<?php echo $seccion->imagen ?>" />
                                    <input type="hidden" name="url_nuevo" value="" />
                                </div>
                                <div class="col s8">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Imagen</span>
                                            <input type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="waves-effect waves-light btn" type="submit" style="width: 100%;" >FINALIZAR</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>

