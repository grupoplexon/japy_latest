<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title"><b><?php echo $seccion->nombre . '/' . $expo['nombre']; ?></b></span>
                        <form method="POST">
                            <input type="hidden" name="id_seccion" value="<?php echo $seccion->id_seccion; ?>" />
                            <input type="hidden" name="id_expo" value="<?php echo $expo['id_expo']; ?>" />
                            <input type="hidden" name="id_dt_seccion" value="<?php echo isset($dt_seccion) ? $dt_seccion->id_dt_seccion : ''; ?>" />
                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input placeholder="Nombre" id="nombre" name="nombre" type="text" value="<?php echo isset($dt_seccion) ? $dt_seccion->nombre : ''; ?>" class="validate" required>
                                    <label for="nombre">Nombre*</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <input placeholder="Breve descripci&oacute;n" id="descripcion" name="descripcion" value="<?php echo isset($dt_seccion) ? $dt_seccion->descripcion : ''; ?>" type="text" class="validate" required>
                                    <label for="descripcion">Descripci&oacute;n*</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input placeholder="Correo de contacto" id="correo_electronico" name="correo" value="<?php echo isset($dt_seccion) ? $dt_seccion->correo : ''; ?>" type="email" class="validate">
                                    <label for="correo_electronico">Correo electr&oacute;nico</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <input placeholder="Giro de la empresa" id="giro" name="giro" value="<?php echo isset($dt_seccion) ? $dt_seccion->giro : ''; ?>" type="text" class="validate">
                                    <label for="giro">Giro</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input placeholder="Ejemplo: https://mail.google.com/" id="pag_web" name="pag_web" value="<?php echo isset($dt_seccion) ? $dt_seccion->pag_web : ''; ?>" type="url" class="validate">
                                    <label for="pag_web">URL P&aacute;gina Web</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <input placeholder="Ejemplo: https://www.facebook.com/JaliscoEsMexico" name="red_social" value="<?php echo isset($dt_seccion) ? $dt_seccion->red_social : ''; ?>" id="red_social" type="url" class="validate">
                                    <label for="red_social">URL Red Social</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input placeholder="Tel&eacute;fono" id="telefono" name="telefono" value="<?php echo isset($dt_seccion) ? $dt_seccion->telefono : ''; ?>" type="tel" class="validate">
                                    <label for="telefono">Tel&eacute;fono</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <input placeholder="Fax" id="fax" name="fax" value="<?php echo isset($dt_seccion) ? $dt_seccion->fax : ''; ?>" type="text" class="validate">
                                    <label for="fax">Fax</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m5 s10">
                                    <input placeholder="Ejemplo: Av. Mariano Otero 1499,Verde Valle,44550 Guadalajara, Jal." value="<?php echo isset($dt_seccion) ? $dt_seccion->direccion : ''; ?>" id="address" name="direccion" type="text" length="70" class="validate" required>
                                    <label for="address">Direcci&oacute;n*</label>
                                </div>
                                <div class="col m1 s2" style="margin-top: 10px;">
                                    <button class="waves-effect waves-light btn tooltipped" data-position="top" data-delay="50" data-tooltip="Buscar ubicacion en el mapa" id="search" type="button"><i class="material-icons">search</i></button>
                                </div>

                                <div class="input-field col m6 s12">
                                    <input placeholder="Ejemplo: Lunes a Viernes de 10:00 a.m. a 6:00 p.m." id="horario" name="horario" value="<?php echo isset($dt_seccion) ? $dt_seccion->horario : ''; ?>" type="text" class="validate">
                                    <label for="horario">Horario</label>
                                </div>

                                <div class="col s12" id="map_canvas" style="width:100%;height:300px; margin-top: 20px;"></div>

                                <input type="hidden" name="latitud" id="latitud"/>
                                <input type="hidden" name="longitud" id="longitud"/>
                            </div>

                            <div class="row teal lighten-5" style="padding: 10px;">
                                <div class="col s4">
                                    <img class="responsive-img" id="img" src="https://placeholdit.imgix.net/~text?txtsize=17&txt=300x200&w=300&h=200&txttrack=0" alt=""/>
                                    <input type="hidden" name="imagen" value="<?php echo isset($dt_seccion) ? $dt_seccion->imagen : ''; ?>" />
                                </div>
                                <div class="col s8">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Imagen</span>
                                            <input type="file" accept="image/*" onchange='openFile(event)'>
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button class="waves-effect waves-light btn" id="we" type="submit" style="width: 100%; margin-top: 30px;">FINALIZAR</button>

                        </form>
                    </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRxC6Y4f-j6nECyHWigtBATtJyXyha-XU&amp;libraries=adsense&amp;sensor=true&amp;language=es"></script>
                <script>
                                                var map;
                                                function onReady() {
                                                    load_map();
<?php if (isset($dt_seccion) && !empty($dt_seccion->direccion)) { ?>
                                                        $('#search').click();
<?php } ?>
<?php if (isset($dt_seccion) && !empty($dt_seccion->imagen)) { ?>
                                                        document.getElementById('img').src = '<?php echo $dt_seccion->imagen ?>';
<?php } ?>
                                                }

                                                function load_map() {
                                                    $('#search').on('click', function () {
                                                        var address = $('#address').val();
                                                        var geocoder = new google.maps.Geocoder();
                                                        geocoder.geocode({'address': address}, geocodeResult);
                                                    });

                                                    $('#address').on('change', function () {
                                                        var address = $('#address').val();
                                                        var geocoder = new google.maps.Geocoder();
                                                        geocoder.geocode({'address': address}, geocodeResult);
                                                    });

                                                    var myLatlng = new google.maps.LatLng(20.68009, -101.35403);
                                                    var myOptions = {
                                                        zoom: 4,
                                                        center: myLatlng,
                                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                                    };
                                                    map = new google.maps.Map($("#map_canvas").get(0), myOptions);
                                                }



                                                function geocodeResult(results, status) {
                                                    if (status == 'OK') {
                                                        var mapOptions = {
                                                            center: results[0].geometry.location,
                                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                                        };
                                                        map = new google.maps.Map($("#map_canvas").get(0), mapOptions);
                                                        map.fitBounds(results[0].geometry.viewport);
                                                        var markerOptions = {position: results[0].geometry.location, draggable: true};
                                                        var marker = new google.maps.Marker(markerOptions);
                                                        var markerLatLng = marker.getPosition();
                                                        $('#latitud').val(markerLatLng.lat());
                                                        $('#longitud').val(markerLatLng.lng());
                                                        marker.setMap(map);
                                                    } else {
                                                        alert("Geocoding no tuvo &eacute;xito debido a: " + status);
                                                    }
                                                }


                                                function openFile(event) {
                                                    var input = event.target;

                                                    var reader = new FileReader();
                                                    reader.onload = function (e) {
                                                        var img = document.createElement("img");
                                                        img.src = e.target.result;

                                                        var canvas = document.createElement("canvas");
                                                        var ctx = canvas.getContext("2d");
                                                        ctx.drawImage(img, 0, 0);

                                                        var MAX_WIDTH = 300;
                                                        var MAX_HEIGHT = 200;
                                                        var width = img.width;
                                                        var height = img.height;

                                                        if (width > height) {
                                                            if (width > MAX_WIDTH) {
                                                                height *= MAX_WIDTH / width;
                                                                width = MAX_WIDTH;
                                                            }
                                                        } else {
                                                            if (height > MAX_HEIGHT) {
                                                                width *= MAX_HEIGHT / height;
                                                                height = MAX_HEIGHT;
                                                            }
                                                        }
                                                        canvas.width = width;
                                                        canvas.height = height;
                                                        var ctx = canvas.getContext("2d");
                                                        ctx.drawImage(img, 0, 0, width, height);

                                                        var dataurl = canvas.toDataURL("image/jpg");
                                                        document.getElementById('img').src = dataurl;
                                                        $('[name="imagen"]').val(dataurl);
                                                    };
                                                    reader.readAsDataURL(input.files[0]);
                                                }

                </script>    
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>


