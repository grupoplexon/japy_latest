<footer class="page-footer dorado-1">
    <img class="vineta" src="<?php echo $this->config->base_url() ?>/dist/img/vineta.png" alt=""/>
    <div class="footer-copyright dorado-1" style="height: 60px;">
        <div class="row">
            <div class="col m4 offset-m1 gris-1-text s4">© Derechos Reservados 2018.</div>
            <div class="col m3 offset-m3 s8">
                <img class="" src="<?php echo $this->config->base_url() ?>/dist/img/logo.png"
                     style="    padding: 6px;z-index: 9;    position: relative;    height: 50px;    width: auto;"
                     alt="japybodas.com"/>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo $this->config->base_url() ?>dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>dist/js/jquery.datetimepicker.full.min.js"
        type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>dist/js/datatables.min.js"></script>
<script src="<?php echo $this->config->base_url() ?>dist/js/materialize.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.Jcrop.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $("a.button-collapse").sideNav();
        //$(".dropdown-button").dropdown();

        if (typeof onReady != "undefined") {
            onReady();
        }
    });
</script>