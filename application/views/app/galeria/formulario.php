
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container"><div class="card">
                    <div class="card-content">
                        <span class="card-title"><b>GALERIA</b></span>
                        <form method="POST">
                            <input type="hidden" name="id_album" value="<?php echo isset($album) ? $album->id_album : ''; ?>" />
                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <select name="Nombre del album" required>
                                        <option value="1" selected>Aplicaci&oacute;n m&oacute;vil</option>
                                        <option value="2" <?php echo (isset($publicidad)) ? ((2 == $publicidad->tipo) ? 'selected' : '') : ''; ?>>P&aacute;gina web</option>
                                    </select>
                                    <label>Tipo</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <select name="id_usuario">
                                        <option value="0" selected>ALL</option>
                                        <?php foreach ($expos as $key => $value) { ?>
                                            <option value="<?php echo $value->id_expo; ?>" <?php echo (isset($publicidad)) ? (($publicidad->id_expo == $value->id_expo) ? 'selected' : '') : ''; ?>>
                                                <?php echo $value->nombre; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <label>Expo</label>
                                </div>
                            </div>

                            <div class="row teal lighten-5" style="padding: 10px;">
                                <div class="col s4">
                                    <img class="responsive-img" id="img" src="<?php echo (isset($publicidad) && !empty($publicidad->imagen)) ? $publicidad->imagen : 'https://placeholdit.imgix.net/~text?txtsize=17&txt=800x150&w=800&h=150&txttrack=0' ?>" alt=""/>
                                    <input type="hidden" name="imagen" value="<?php echo isset($publicidad) ? $publicidad->imagen : ''; ?>" />
                                </div>
                                <div class="col s8">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Imagen</span>
                                            <input type="file" accept="image/*" onchange='openFile(event)'>
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button class="waves-effect waves-light btn" id="we" type="submit" style="width: 100%; margin-top: 30px;">FINALIZAR</button>

                        </form>
                    </div>
                </div>
                <script>
                    var MAX_WIDTH = 800;
                    var MAX_HEIGHT = 150;

                    function onReady() {
                        $('select').material_select();
                        $('.datepicker').pickadate({
                            selectMonths: true,
                            selectYears: 15,
                            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                            today: 'Hoy',
                            clear: 'Limpiar',
                            close: 'Cerrar',
                            format: 'yyyy/mm/dd',
                            formatSubmit: 'yyyy/mm/dd',
                            min: new Date()
                        });
                        $('[name="tipo"]').on('change', function () {

                            switch ($('[name="tipo"]').val()) {
                                case '1':
                                    document.getElementById('img').src = "https://placeholdit.imgix.net/~text?txtsize=17&txt=800x150&w=800&h=150&txttrack=0";
                                    MAX_WIDTH = 800;
                                    MAX_HEIGHT = 150;
                                    break;
                                case '2':
                                    document.getElementById('img').src = "https://placeholdit.imgix.net/~text?txtsize=17&txt=500x100&w=500&h=100&txttrack=0";
                                    MAX_WIDTH = 500;
                                    MAX_HEIGHT = 100;
                                    break;

                            }
                        });
                    }

                    function openFile(event) {
                        var input = event.target;

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var img = document.createElement("img");
                            img.src = e.target.result;

                            var canvas = document.createElement("canvas");
                            var ctx = canvas.getContext("2d");
                            ctx.drawImage(img, 0, 0);


                            var width = img.width;
                            var height = img.height;

                            if (width > height) {
                                if (width > MAX_WIDTH) {
                                    height *= MAX_WIDTH / width;
                                    width = MAX_WIDTH;
                                }
                            } else {
                                if (height > MAX_HEIGHT) {
                                    width *= MAX_HEIGHT / height;
                                    height = MAX_HEIGHT;
                                }
                            }
                            canvas.width = width;
                            canvas.height = height;
                            var ctx = canvas.getContext("2d");
                            ctx.drawImage(img, 0, 0, width, height);

                            var dataurl = canvas.toDataURL("image/jpg");
                            document.getElementById('img').src = dataurl;
                            $('[name="imagen"]').val(dataurl);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }

                </script> 
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>





