<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>

                    <div class="right-align">
                        <a class="waves-effect waves-light btn" href="<?php echo $this->config->base_url() ?>App/ADGaleria/alta">
                            <i class="material-icons left">add_circle_outline</i>  Crear Album
                        </a>   
                    </div>


                    <div class="card">
                        <table class="responsive-table bordered highlight">
                            <thead>
                                <tr class="teal lighten-5">
                                    <th data-field="id">Portada</th>
                                    <th data-field="name">Nombre</th>                                    
                                    <th data-field="name">Numero Fotos</th>
                                    <th data-field="name">Fecha Creacion</th>   
                                    <th data-field="name">Descripcion</th>
                                    <th data-field="name">Usuario</th>                                    
                                    <th data-field="name">Acci&oacute;n</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach ($albums as $key => $value) { ?>
                                    <tr>
                                        <td><img style="width: 250px; height: auto;" src="<?php echo $value->portada; ?>" ></td>
                                        <td><?php echo empty($value->nombrealbum) ? 'TODOS' : $value->nombrealbum ?></td>
                                        <td><?php echo empty($value->numerofotos) ? 'TODOS' : $value->numerofotos ?></td>
                                        <td><?php echo strftime("%A %d de %B de %Y", strtotime($value->fecha_creacion)); ?></td> 
                                        <td><?php echo empty($value->descripcion) ? 'TODOS' : $value->descripcion ?></td>
                                        <td><?php echo empty($value->usuario) ? 'TODOS' : $value->usuario ?></td>
                                                            
                                        <td>
                                            <a href="<?php echo $this->config->base_url() ?>App/ADGaleria/editar/<?php echo $value->id_album; ?>"><i class="material-icons">create</i></a>
                                            <span onclick="eliminar_album('<?php echo $value->id_album; ?>');" ><i class="material-icons">delete</i></span>
                                        </td>
                                    </tr>  
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <script>
                        function eliminar_album(id) {
                            var r = confirm("Esta seguro que desea eliminar este album?");
                            if (r == true) {
                                location.href = "<?= $this->config->base_url(); ?>App/ADGaleria/eliminar/" + id;
                            }
                        }
                    </script>

            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>




