<title>Japy</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="theme-color" content="#f5e6df">
<meta name="msapplication-navbutton-color" content="#f5e6df">
<meta name="apple-mobile-web-app-status-bar-style" content="#f5e6df">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/ghpages-materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/estilo-app.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url() ?>dist/css/datatables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url() ?>dist/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css">
<?php
setlocale(LC_MONETARY, "en_US");
setlocale(LC_TIME, "es_ES.UTF-8");
?>
<style>
    main, footer {
        padding-left: 240px
    }

    @media only screen and (max-width: 992px) {
        header, main, footer {
            padding-left: 0;
        }
    }
</style>
