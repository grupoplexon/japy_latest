<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>
                <div class="row">
                    <div class="col s6 m3">
                        <div class="card-panel green  ">
                            <div class="row">
                                <div class="col m4 s4 valign-wrapper" style="height: 108px">
                                    <i class="fa fa-building fa-4x white-text valign"></i>
                                </div>
                                <div class="col m8 s8 center white-text">
                                    <text class="fa-3x"><?php echo $proveedores ?></text>
                                    <br>
                                    Proveedores
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m3">
                        <div class="card-panel blue ">
                            <div class="row">
                                <div class="col m4 s4 valign-wrapper" style="height: 108px">
                                    <i class="fa fa-users fa-4x white-text valign"></i>
                                </div>
                                <div class="col m8 s8 center white-text">
                                    <text class="fa-3x"><?php echo $novios ?></text>
                                    <br>
                                    Novios
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m3">
                        <div class="card-panel orange ">
                            <div class="row">
                                <div class="col m4 s4 valign-wrapper" style="height: 108px">
                                    <i class="fa fa-female fa-4x white-text valign"></i>
                                </div>
                                <div class="col m8 s8 center white-text">
                                    <text class="fa-3x"><?php echo $vestidos ?></text>
                                    <br>
                                    Vestidos
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6">

                    </div>
                    <div class="col s12 m6">
                        <section class="ultimos-usuario-registrados">
                            <div class="divider"></div>
                            <div class="card-panel z-depth-0">
                                <b><i class="fa fa-envelope"></i> Clientes registrados
                                    <small class="pull-right grey-text">&Uacute;ltimos 12 meses</small>
                                </b>
                                <div id="estadisticas-clientes" style="width: 100%; min-height: 350px">
                                </div>
                                <div class="col m6 s12 center-align">
                                    <div class="col m6 s12">
                                        <h5 class="total-solicitudes"><?php echo $novios ?></h5>
                                    </div>
                                    <div class="col m6 s12">
                                        Ultimos 12 meses
                                    </div>
                                </div>
                                <div class="col m6 s12">
                                    La gr&aacute;fica muestra el n&uacute;mero de novios registrados en los ultimos 12 meses
                                </div>
                            </div>
                        </section>
                        <section class="ultimos-proveedores-registrados">
                            <div class="divider"></div>
                            <div class="card-panel z-depth-0">
                                <b><i class="fa fa-envelope"></i> Proveedores registrados
                                    <small class="pull-right grey-text">&Uacute;ltimos 12 meses</small>
                                </b>
                                <div id="estadisticas-proveedores" style="width: 100%; min-height: 350px">
                                </div>
                                <div class="col m6 s12 center-align">
                                    <div class="col m6 s12">
                                        <h5 class="total-proveedores"><?php echo $proveedores ?></h5>
                                    </div>
                                    <div class="col m6 s12">
                                        Ultimos 12 meses
                                    </div>
                                </div>
                                <div class="col m6 s12">
                                    La gr&aacute;fica muestra el n&uacute;mero de proveedores registrados en los ultimos 12
                                    meses
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
        <script>
            var server = "<?php echo base_url() ?>";
            var meses = ['', 'ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DEC'];

            $(document).ready(function() {
                cargar_clientes();
                cargar_proveedores();
            });

            function cargar_clientes() {
                $.ajax({
                    'url': server + 'App/home/est_clientes',
                    success: function(resp) {
                        var data = loadData(resp.data.clientes, 'clientes');
                        grafica_estadisticas('#estadisticas-clientes', data.serie, data.labels, 'Clientes');
                    },
                    error: function() {
                        $('#estadisticas-clientes').parent().remove();
                    },
                });
            }

            function cargar_proveedores() {
                $.ajax({
                    'url': server + 'App/home/est_proveedores',
                    success: function(resp) {
                        console.log(resp.data.proveedores);
                        var data = loadData(resp.data.proveedores, 'proveedores');
                        console.log(data);
                        grafica_estadisticas('#estadisticas-proveedores', data.serie, data.labels, 'Proveedores');
                    },
                    error: function() {
                        $('#estadisticas-proveedores').parent().remove();
                    },
                });
            }

            function loadData(resp, campo) {
                var data = {
                    serie: new Array(),
                    labels: new Array(),
                    total: 0,
                };
                for (var i in resp) {
                    var d = resp[i];
                    data.serie.push(d[campo]);
                    data.labels.push(meses[parseInt(d.fecha.split('-')[0])]);
                    data.total += d[campo];
                }
                return data;
            }

            function grafica_estadisticas(elem, serie, labels, tipo) {
                $(elem).highcharts({
                    title: {
                        text: '',
                    },
                    credits: {
                        enabled: false,
                    },
                    subtitle: {
                        text: '',
                    },
                    xAxis: {
                        categories: labels,
                    },
                    yAxis: {
                        title: {
                            text: tipo,
                        },
                        plotLines: [
                            {
                                value: 0,
                                width: 1,
                                color: '#808080',
                            }],
                    },
                    tooltip: {
                        valueSuffix: ' ' + tipo,
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0,
                    },
                    series: [
                        {
                            name: ' ',
                            color: '#f5c564',
                            data: serie,
                            dataLabels: {},
                        }],
                });

            }

        </script>
    </body>
</html>