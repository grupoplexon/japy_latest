<html>
<head>
    <title>Japy</title>
    <link href="<?php echo $this->config->base_url() ?>dist/img/clubnupcial.png" rel="icon" sizes="16x16">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo $this->config->base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->config->base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <style>
        .contenedor {
            background: url(<?php echo $this->config->base_url() ?>/dist/img/textura_footer2.png);
        }

        footer {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
        }

        .red-text.darken-5 {
            color: #92151b !important;
        }

        .red.darken-5 {
            background: #94151c !important;
        }

        input[type=email], input[type=password] {
            width: 100%;
            text-indent: 15px;
            border: none;
            outline: 0;
            color: #3D3F4D;
            background-color: #F2F2F2 !important;
            font-size: .9em;
            -webkit-appearance: none;
        }

        input[type=password]:focus:not([readonly]), input[type=email]:focus:not([readonly]) {
            border-bottom: 1px solid #F2F2F2;
            box-shadow: 0 1px 0 0 #F2F2F2;
        }

        .btn {
            width: 100%;
        }

        footer.page-footer {
            margin-top: 0px;
            padding-top: 0px;
        }

        li {
            margin-bottom: 10px;
        }
    </style>
</head>
<body class="contenedor">
<div class="container">
    <div class="hide-on-small-only">
        <div style="margin:10%;"></div>
    </div>
    <div class="row">
        <div class="col s2 m3">&nbsp;</div>
        <div class="col s8 m6">
            <form method="POST">
                <div class="card">
                    <div class="card-content ">
                        <img class="responsive-img" src="<?php echo $this->config->base_url() ?>/dist/img/logo.png"
                             alt=""/>
                        <h5 class="center-align">Bienvenido! Administrador aplicaci&oacute;n</h5>
                        <ul>
                            <li>
                                <small class="grey-text">CORREO ELECTR&Oacute;NICO</small>
                                <input type="email" onpaste="return false" required="" name="primaryEmail">
                            </li>
                            <li>
                                <small class="grey-text">CONTRASE&Ntilde;A</small>
                                <input type="password" onpaste="return false" required="" name="primaryPassword">
                            </li>
                            <li>
                                <div class="chip red darken-1 white-text right"
                                     style="margin-bottom:20px; <?php echo (isset($mensaje)) ? '' : 'display:none'; ?>">
                                    <?php echo (isset($mensaje)) ? $mensaje : ''; ?>
                                    <i class="material-icons">close</i>
                                </div>
                            </li>
                            <li class="center-align">
                                <button class="btn waves-effect waves-light dorado-2" type="submit" name="action">
                                    Iniciar Sesi&oacute;n
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
        <div class="col s2 m3">&nbsp;</div>
    </div>
</div>
</body>
<footer class="page-footer black">
    <div class="footer-copyright">
        <div class="container">
            © Derechos Reservados 2018.
            <a class="grey-text text-lighten-4 right" href="#!"></a>
        </div>
    </div>
</footer>
<script src="<?php echo $this->config->base_url() ?>dist/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>dist/js/materialize.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        if (typeof onReady != "undefined") {
            onReady();
        }
        $(".button-collapse").sideNav();
        $(".dropdown-button").dropdown();
    });
</script>
</html>