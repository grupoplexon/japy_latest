<?php
$controller = $_SERVER['REQUEST_URI'];
$controller = strtolower($this->uri->segment(2));
?>
<header>
    <nav class="top-nav dorado-2" style="height: 75px; background-color: #f5c564;">
        <div class="container">
            <div class="nav-wrapper" style="text-align: center;">
                <a class="page-title" style="line-height: 76px; font-size: 36px;"><?php echo isset($titulo) ? $titulo : ''; ?></a>
            </div>
        </div>
    </nav>
    <div class="container">
        <a href="#" data-activates="nav-mobile" class="button-collapse top-nav waves-effect waves-light circle hide-on-large-only">
            <i class="material-icons">menu</i>
        </a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed" >
        <li class="logo">

        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url('App'); ?>">Inicio</a>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold">
                    <a class="collapsible-header  waves-effect waves-orange">Panel Principal</a>
                    <div class="collapsible-body" style="">
                        <ul>
                            <li><a href="<?php echo site_url('App/ADHome') ?>">Slider</a></li>
                            <li><a href="<?php echo site_url('App/ADHome/footer') ?>">Footer</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url("blog") ?>">Blog</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url("App/Usuario") ?>">Usuario</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url("App/ADProveedor") ?>">Proveedores</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url("adminvestidos") ?>">Vestidos</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url('App/ADExpo'); ?>">Expo tu boda</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url('App/ADPublicidad'); ?>">Publicidad</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url('App/ADNotificacion'); ?>">Notificaciones</a>
        </li>
        <!-- <li class="bold">
            <a class="waves-effect waves-orange" href="#">Promociones</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="#">Sorteos</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?//php echo site_url('App/ADNosotros'); ?>">Nosotros</a> -->
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url('App/ADPresupuestador'); ?>">Presupuestador</a>
        </li>
        <li class="bold">
            <a class="waves-effect waves-orange" href="<?php echo site_url('cuenta/logout') ?>">Cerrar sesi&oacute;n</a>
        </li>
    </ul>   
</header>
<?php setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish'); ?>

