<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container"><div class="card">
                    <form method="POST">
                        <div class="card-content">
                            <span class="card-title"><b>NOSOTROS</b></span>
                            <form method="POST">
                                <div class="row">
                                    <div class="input-field col m6 s12">
                                        <input placeholder="Nombre" id="nombre" name="nombre" type="text" value="<?php echo isset($nosotros) ? $nosotros->nombre : ''; ?>" class="validate" required>
                                        <label for="nombre">Nombre*</label>
                                    </div>
                                    <div class="input-field col m6 s12">
                                        <input placeholder="Tel&eacute;fono" id="telefono" name="telefono" type="text" value="<?php echo isset($nosotros) ? $nosotros->telefono : ''; ?>" class="validate" required>
                                        <label for="telefono">Tel&eacute;fono*</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="descripcion" name="descripcion" class="materialize-textarea" required><?php echo isset($nosotros) ? $nosotros->descripcion : ''; ?></textarea>
                                        <label for="descripcion">Descripci&oacute;n*</label>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="input-field col m12 s12">
                                        <input placeholder="Web" id="web" name="web" type="text" value="<?php echo isset($nosotros) ? $nosotros->web : ''; ?>" class="validate" required>
                                        <label for="web">Web*</label>
                                    </div>                                
                                </div>


                                <div class="row">
                                    <form id="google" name="google" action="#" style="margin-bottom: 10px;">
                                        <div class="input-field col s10">
                                            <input placeholder="Ubicaci&oacute;n" value ="<?php echo isset($nosotros) ? $nosotros->direccion : '' ?>" id="address" name="ubicacion" type="text" class="validate" required>
                                            <label for="address">Ubicaci&oacute;n</label>
                                        </div>
                                        <div class="col s2" style="margin-top: 10px;">
                                            <button class="waves-effect waves-light btn tooltipped dorado-2" data-position="top" data-delay="50" data-tooltip="Buscar ubicacion en el mapa" id="search" type="button"><i class="material-icons">search</i></button>
                                        </div>

                                        <div class="col s12" id="map_canvas" style="width:100%;height:300px; margin-top: 20px;"></div>


                                    </form>
                                </div>
                                <input type="hidden" name="latitud" id="latitud"/>
                                <input type="hidden" name="longitud" id="longitud"/>
                                <div class="row" style="padding: 10px;">
                                    <div class="col s4">
                                        <img class="responsive-img" id="img" src="<?php echo (isset($nosotros) && !empty($nosotros->imagen)) ? $nosotros->imagen : 'https://placeholdit.imgix.net/~text?txtsize=17&txt=200x200&w=200&h=200&txttrack=0' ?>" alt=""/>
                                        <input type="hidden" name="imagen" value="<?php echo isset($nosotros) ? $nosotros->imagen : ''; ?>" />
                                    </div>
                                    <div class="col s8">
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>Imagen</span>
                                                <input type="file" accept="image/*" onchange='openFile(event)'>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button class="waves-effect waves-light btn dorado-2" id="we" type="submit" style="width: 100%; margin-top: 30px;">FINALIZAR</button>

                            </form>
                        </div>
                </div>
                <div class="row">&nbsp;<div class="row">&nbsp;</div></div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRxC6Y4f-j6nECyHWigtBATtJyXyha-XU&amp;libraries=adsense&amp;sensor=true&amp;language=es"></script>
                <script>
                                                    var MAX_WIDTH = 800;
                                                    var MAX_HEIGHT = 150;

                                                    function onReady() {
                                                        load_map();
<?php if (isset($nosotros) && $nosotros->direccion != '') { ?>
                                                            $('#search').click();
<?php } ?>
                                                    }
                                                    function load_map() {
                                                        $('#search').on('click', function () {
                                                            var address = $('#address').val();
                                                            var geocoder = new google.maps.Geocoder();
                                                            geocoder.geocode({'address': address}, geocodeResult);
                                                        });

                                                        $('#address').on('change', function () {
                                                            var address = $('#address').val();
                                                            var geocoder = new google.maps.Geocoder();
                                                            geocoder.geocode({'address': address}, geocodeResult);
                                                        });

                                                        var myLatlng = new google.maps.LatLng(20.68009, -101.35403);
                                                        var myOptions = {
                                                            zoom: 4,
                                                            center: myLatlng,
                                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                                        };
                                                        map = new google.maps.Map($("#map_canvas").get(0), myOptions);
                                                    }



                                                    function geocodeResult(results, status) {
                                                        if (status == 'OK') {
                                                            var mapOptions = {
                                                                center: results[0].geometry.location,
                                                                mapTypeId: google.maps.MapTypeId.ROADMAP
                                                            };
                                                            map = new google.maps.Map($("#map_canvas").get(0), mapOptions);
                                                            map.fitBounds(results[0].geometry.viewport);
                                                            var markerOptions = {position: results[0].geometry.location, draggable: true};
                                                            var marker = new google.maps.Marker(markerOptions);
                                                            var markerLatLng = marker.getPosition();
                                                            $('#latitud').val(markerLatLng.lat());
                                                            $('#longitud').val(markerLatLng.lng());
                                                            marker.setMap(map);
                                                        } else {
                                                            alert("Geocoding no tuvo &eacute;xito debido a: " + status);
                                                        }
                                                    }
                                                    function openFile(event) {
                                                        var input = event.target;

                                                        var reader = new FileReader();
                                                        reader.onload = function (e) {
                                                            var img = document.createElement("img");
                                                            img.src = e.target.result;

                                                            var canvas = document.createElement("canvas");
                                                            var ctx = canvas.getContext("2d");
                                                            ctx.drawImage(img, 0, 0);


                                                            var MAX_WIDTH = 500;
                                                            var MAX_HEIGHT = 500;
                                                            var width = img.width;
                                                            var height = img.height;

                                                            if (width > height) {
                                                                if (width > MAX_WIDTH) {
                                                                    height *= MAX_WIDTH / width;
                                                                    width = MAX_WIDTH;
                                                                }
                                                            } else {
                                                                if (height > MAX_HEIGHT) {
                                                                    width *= MAX_HEIGHT / height;
                                                                    height = MAX_HEIGHT;
                                                                }
                                                            }
                                                            canvas.width = width;
                                                            canvas.height = height;
                                                            var ctx = canvas.getContext("2d");
                                                            ctx.drawImage(img, 0, 0, width, height);

                                                            var dataurl = canvas.toDataURL("image/jpg");
                                                            document.getElementById('img').src = dataurl;
                                                            $('[name="imagen"]').val(dataurl);
                                                        };
                                                        reader.readAsDataURL(input.files[0]);
                                                    }

                </script> 
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>