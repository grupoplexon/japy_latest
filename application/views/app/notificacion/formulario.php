
<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title"><b>NOTIFICACI&Oacute;N</b></span>
                        <form method="POST">

                            <div class="row">
                                <div class="input-field col m6 s12">
                                    <input placeholder="T&iacute;tulo de la notificaci&oacute;n" id="titulo" name="titulo" type="text" value="<?php echo isset($dt_seccion) ? $dt_seccion->nombre : ''; ?>" class="validate" required>
                                    <label for="titulo">Titulo*</label>
                                </div>
                                <div class="input-field col m6 s12">
                                    <select name="id_expo">
                                        <option value="0" selected>ALL</option>
                                        <?php foreach ($expos as $key => $value) { ?>
                                            <option value="<?php echo $value->id_expo; ?>" <?php echo (isset($publicidad)) ? (($publicidad->id_expo == $value->id_expo) ? 'selected' : '') : ''; ?>>
                                                <?php echo $value->nombre; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <label>Expo</label>
                                </div>
                            </div>


                            <div class="row">

                                <div class="input-field col s12">
                                    <textarea id="descripcion" name="descripcion" class="materialize-textarea" required></textarea>
                                    <label for="descripcion">Descripci&oacute;n*</label>
                                </div>
                            </div>


                            <button class="waves-effect waves-light btn" id="we" type="submit" style="width: 100%; margin-top: 30px;">FINALIZAR</button>

                        </form>
                    </div>
                </div>
                <script>
                    function onReady() {
                        $('select').material_select();
                    }
                </script>  
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>

