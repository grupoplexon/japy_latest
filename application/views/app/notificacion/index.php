<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
<?php $this->view("app/menu") ?>
<main>
    <div class="container">
        <br>
        <br>
        <div class="right-align">
            <a class="waves-effect waves-light btn "
               href="<?php echo $this->config->base_url() ?>App/ADNotificacion/alta">
                <i class="material-icons left">add_circle_outline</i> Registrar notificacion
            </a>
        </div>
        <div class="card">
            <table class="responsive-table bordered highlight">
                <thead>
                <tr class="teal lighten-5">
                    <th data-field="id">#</th>
                    <th data-field="name">Titulo</th>
                    <th data-field="name">Descripcion</th>
                    <th data-field="name">Fecha</th>
                    <th data-field="name">Expo</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($notificaciones as $key => $value) { ?>
                    <tr>
                        <td><?php echo $key + 1; ?></td>
                        <td><?php echo $value->titulo; ?></td>
                        <td><?php echo $value->descripcion ?></td>
                        <td><?php echo strftime("%d-%B-%Y %I:%M %p", strtotime($value->fecha_creacion)); ?></td>
                        <td><?php echo empty($value->nombre) ? 'TODOS' : $value->nombre ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php $this->view("app/footer") ?>
</body>
</html>
