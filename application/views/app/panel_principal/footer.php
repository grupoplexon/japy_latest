<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
<?php $this->view("app/menu") ?>
<main>
    <div class="container">
        <div style="margin: 20px;"></div>

        <div class="card">
            <div class="card-content">
                <div class="card-title dorado-2-text">
                    <b>HOME</b>
                    <a class="btn btn-flat modal-trigger right" href="#modal1" onclick="insert_seccion('HOME')"><i
                                class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <table class="responsive-table striped centered">
                    <thead>
                    <tr>
                        <th data-field="number">#</th>
                        <th data-field="id">T&iacute;tulo</th>
                        <th data-field="name">URL</th>
                        <th data-field="name">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody id="home">
                    <?php $cont = 0; ?>
                    <?php foreach ($seccion as $key => $value) { ?>
                        <?php if ($value->tipo == "HOME") { ?>
                            <tr id="<?php echo $value->id_home_seccion ?>"
                                data-json='<?php echo json_encode($value); ?>'>
                                <td><?php echo ++$cont; ?></td>
                                <td><?php echo $value->titulo ?></td>
                                <td style="font-size: 10px;"><?php echo empty($value->url) ? '#' : $value->url; ?></td>
                                <td>
                                    <a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1"
                                       onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>


        <br/>
        <div class="card">
            <div class="card-content">
                <div class="card-title dorado-2-text">
                    <b>EVENTOS</b>
                    <a class="btn btn-flat modal-trigger right" href="#modal1" onclick="insert_seccion('EVENTOS')"><i
                                class="fa fa-plus" aria-hidden="true"></i></a>
                </div>

                <table class="responsive-table striped centered">
                    <thead>
                    <tr>
                        <th data-field="number">#</th>
                        <th data-field="id">T&iacute;tulo</th>
                        <th data-field="name">URL</th>
                        <th data-field="name">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody id="eventos">
                    <?php $cont = 0; ?>
                    <?php foreach ($seccion as $key => $value) { ?>
                        <?php if ($value->tipo == "EVENTOS") { ?>
                            <tr id="<?php echo $value->id_home_seccion ?>"
                                data-json='<?php echo json_encode($value); ?>'>
                                <td><?php echo ++$cont; ?></td>
                                <td><?php echo $value->titulo ?></td>
                                <td style="font-size: 10px;"><?php echo empty($value->url) ? '#' : $value->url; ?></td>
                                <td>
                                    <a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1"
                                       onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>


        <br/>
        <div class="card">
            <div class="card-content">
                <div class="card-title dorado-2-text">
                    <b>PROVEEDORES</b>
                    <a class="btn btn-flat modal-trigger right" href="#modal1"
                       onclick="insert_seccion('PROVEEDORES')"><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>

                <table class="responsive-table striped centered">
                    <thead>
                    <tr>
                        <th data-field="number">#</th>
                        <th data-field="id">T&iacute;tulo</th>
                        <th data-field="name">URL</th>
                        <th data-field="name">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody id="proveedores">
                    <?php $cont = 0; ?>
                    <?php foreach ($seccion as $key => $value) { ?>
                        <?php if ($value->tipo == "PROVEEDORES") { ?>
                            <tr id="<?php echo $value->id_home_seccion ?>"
                                data-json='<?php echo json_encode($value); ?>'>
                                <td><?php echo ++$cont; ?></td>
                                <td><?php echo $value->titulo ?></td>
                                <td style="font-size: 10px;"><?php echo empty($value->url) ? '#' : $value->url; ?></td>
                                <td>
                                    <a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1"
                                       onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>


        <br/>
        <div class="card">
            <div class="card-content">
                <div class="card-title dorado-2-text">
                    <b>BLOG</b>
                    <a class="btn btn-flat modal-trigger right" href="#modal1" onclick="insert_seccion('BLOG')"><i
                                class="fa fa-plus" aria-hidden="true"></i></a>
                </div>

                <table class="responsive-table striped centered">
                    <thead>
                    <tr>
                        <th data-field="number">#</th>
                        <th data-field="id">T&iacute;tulo</th>
                        <th data-field="name">URL</th>
                        <th data-field="name">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody id="blog">
                    <?php $cont = 0; ?>
                    <?php foreach ($seccion as $key => $value) { ?>
                        <?php if ($value->tipo == "BLOG") { ?>
                            <tr id="<?php echo $value->id_home_seccion ?>"
                                data-json='<?php echo json_encode($value); ?>'>
                                <td><?php echo ++$cont; ?></td>
                                <td><?php echo $value->titulo ?></td>
                                <td style="font-size: 10px;"><?php echo empty($value->url) ? '#' : $value->url; ?></td>
                                <td>
                                    <a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1"
                                       onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>


        <br/>
        <div class="card">
            <div class="card-content">
                <div class="card-title dorado-2-text">
                    <b>GALERIA</b>
                    <a class="btn btn-flat modal-trigger right" href="#modal1" onclick="insert_seccion('GALERIA')"><i
                                class="fa fa-plus" aria-hidden="true"></i></a>
                </div>

                <table class="responsive-table striped centered">
                    <thead>
                    <tr>
                        <th data-field="number">#</th>
                        <th data-field="id">T&iacute;tulo</th>
                        <th data-field="name">URL</th>
                        <th data-field="name">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody id="galeria">
                    <?php $cont = 0; ?>
                    <?php foreach ($seccion as $key => $value) { ?>
                        <?php if ($value->tipo == "GALERIA") { ?>
                            <tr id="<?php echo $value->id_home_seccion ?>"
                                data-json='<?php echo json_encode($value); ?>'>
                                <td><?php echo ++$cont; ?></td>
                                <td><?php echo $value->titulo ?></td>
                                <td style="font-size: 10px;"><?php echo empty($value->url) ? '#' : $value->url; ?></td>
                                <td>
                                    <a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1"
                                       onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>


        <br/>
        <div class="card">
            <div class="card-content">
                <div class="card-title dorado-2-text">
                    <b>MI BODA</b>
                    <a class="btn btn-flat modal-trigger right" href="#modal1" onclick="insert_seccion('MI BODA')"><i
                                class="fa fa-plus" aria-hidden="true"></i></a>
                </div>

                <table class="responsive-table striped centered">
                    <thead>
                    <tr>
                        <th data-field="number">#</th>
                        <th data-field="id">T&iacute;tulo</th>
                        <th data-field="name">URL</th>
                        <th data-field="name">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody id="mi_boda">
                    <?php $cont = 0; ?>
                    <?php foreach ($seccion as $key => $value) { ?>
                        <?php if ($value->tipo == "MI BODA") { ?>
                            <tr id="<?php echo $value->id_home_seccion ?>"
                                data-json='<?php echo json_encode($value); ?>'>
                                <td><?php echo ++$cont; ?></td>
                                <td><?php echo $value->titulo ?></td>
                                <td style="font-size: 10px;"><?php echo empty($value->url) ? '#' : $value->url; ?></td>
                                <td>
                                    <a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1"
                                       onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>


        <br/>
        <div class="card">
            <div class="card-content">
                <div class="card-title dorado-2-text">
                    <b>REDES SOCIALES</b>
                    <a class="btn btn-flat modal-trigger right" href="#modal1"
                       onclick="insert_seccion('REDES SOCIALES')"><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>

                <table class="responsive-table striped centered">
                    <thead>
                    <tr>
                        <th data-field="number">#</th>
                        <th data-field="id">T&iacute;tulo</th>
                        <th data-field="name">URL</th>
                        <th data-field="name">&Iacute;cono</th>
                        <th data-field="name">Acci&oacute;n</th>
                    </tr>
                    </thead>
                    <tbody id="redes">
                    <?php $cont = 0; ?>
                    <?php foreach ($seccion as $key => $value) { ?>
                        <?php if ($value->tipo == "REDES SOCIALES") { ?>
                            <tr id="<?php echo $value->id_home_seccion ?>"
                                data-json='<?php echo json_encode($value); ?>'>
                                <td><?php echo ++$cont; ?></td>
                                <td><?php echo $value->titulo ?></td>
                                <td style="font-size: 10px;"><?php echo empty($value->url) ? '#' : $value->url; ?></td>
                                <td><?php echo $value->icono ?></td>
                                <td>
                                    <a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1"
                                       onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</main>
<div style="margin: 55px;">&nbsp;</div>


<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
        <input type="hidden" name="tipo"/>
        <input type="hidden" name="id"/>

        <h4 id="tipo">TIPO</h4>

        <div class="input-field">
            <input placeholder="Titulo" id="first_name" name="titulo" type="text" class="validate">
            <label for="first_name">T&iacute;tulo</label>
        </div>
        <div class="input-field">
            <input placeholder="URL" id="segund_name" name="url" type="text" class="validate">
            <label for="segund_name">URL</label>
        </div>
        <div class="input-field" id="icono">

            <select id="tres_name" name="icono">
                <option value="" disabled selected>Eligue una opci&oacute;n</option>
                <option value="facebook">Facebook</option>
                <option value="twitter">Twitter</option>
                <option value="instagram">Instagram</option>
                <option value="pinterest-p">Pinterest</option>
                <option value="youtube">Youtube</option>
                <option value="google-plus">Google Plus</option>
                <option value="tumblr">Tumblr</option>
                <option value="snapchat-ghost">Snapchat</option>
            </select>
            <label for="tres_name">&Iacute;cono</label>
        </div>

        <p id="error" style="color:red;"></p>

    </div>
    <div class="modal-footer">
        <button class="btn" id="btn_modal" onclick="">Listo</button>
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
    </div>
</div>


<?php $this->view("app/footer") ?>
<script>
    $(document).ready(function () {
        $('.collapsible').collapsible({
            accordion: false,
        });
        $('select').material_select();
        $('.modal-trigger').modal();
    });

    function insert_seccion(tipo) {
        if (tipo == "REDES SOCIALES") {
            $('#icono').show();
        } else {
            $('#icono').hide();
        }
        $('#btn_modal').attr('onclick', 'send_insert_seccion()');
        $('[name=titulo]').val('');
        $('[name=url]').val('');
        $('[name=icono]').val('');
        $('[name=id]').val('');
        $('[name=tipo]').val(tipo);
        $('#tipo').text(tipo);
        $('select').material_select();


        $('#error').text('');
    }

    function edit_seccion(elem) {
        var json = $(elem.parentElement.parentElement).data('json');
        if (json.tipo == "REDES SOCIALES") {
            $('#icono').show();
        } else {
            $('#icono').hide();
        }
        $('#btn_modal').attr('onclick', 'send_edit_seccion()');
        $('[name=titulo]').val(json.titulo);
        $('[name=url]').val(json.url);
        $('[name=icono]').val(json.icono);
        $('[name=id]').val(json.id_home_seccion);
        $('[name=tipo]').val(json.tipo);
        $('#tipo').text(json.tipo);
        $('select').material_select();


        $('#error').text('');
    }

    function send_insert_seccion() {
        if (!isEmpty($('[name=titulo]').val())) {
            var info = {
                titulo: $('[name=titulo]').val(),
                tipo: $('[name=tipo]').val(),
                url: $('[name=url]').val(),
                icono: $('[name=icono]').val(),
            };

            $.ajax({
                url: "<?php echo site_url('App/ADHome/insert_seccion') ?>",
                type: "POST",
                timeout: 5000,
                data: info,
            }).done(function (data) {
                info.id_home_seccion = data.id;
                var tr = document.createElement('tr');
                tr.setAttribute('id', data.id);
                $(tr).data('json', info);

                switch (info.tipo) {
                    case "HOME":
                        tr.innerHTML = "<td>" + ($('#home').find('tr').length + 1) + "</td>" +
                            "<td>" + info.titulo + "</td>" +
                            "<td>" + (isEmpty(info.url) ? '#' : info.url) + "</td>" +
                            "<td>" +
                            '<a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1" onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                            '<button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
                            "</td>";
                        $('#home').append(tr);
                        break;
                    case "EVENTOS":
                        tr.innerHTML = "<td>" + ($('#eventos').find('tr').length + 1) + "</td>" +
                            "<td>" + info.titulo + "</td>" +
                            "<td>" + (isEmpty(info.url) ? '#' : info.url) + "</td>" +
                            "<td>" +
                            '<a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1" onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                            '<button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
                            "</td>";
                        $('#eventos').append(tr);
                        break;
                    case "PROVEEDORES":
                        tr.innerHTML = "<td>" + ($('#proveedores').find('tr').length + 1) + "</td>" +
                            "<td>" + info.titulo + "</td>" +
                            "<td>" + (isEmpty(info.url) ? '#' : info.url) + "</td>" +
                            "<td>" +
                            '<a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1" onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                            '<button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
                            "</td>";
                        $('#proveedores').append(tr);
                        break;
                    case "BLOG":
                        tr.innerHTML = "<td>" + ($('#blog').find('tr').length + 1) + "</td>" +
                            "<td>" + info.titulo + "</td>" +
                            "<td>" + (isEmpty(info.url) ? '#' : info.url) + "</td>" +
                            "<td>" +
                            '<a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1" onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                            '<button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
                            "</td>";
                        $('#blog').append(tr);
                        break;
                    case "GALERIA":
                        tr.innerHTML = "<td>" + ($('#galeria').find('tr').length + 1) + "</td>" +
                            "<td>" + info.titulo + "</td>" +
                            "<td>" +
                            '<a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1" onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                            '<button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
                            "</td>";
                        $('#galeria').append(tr);
                        break;
                    case "MI BODA":
                        tr.innerHTML = "<td>" + ($('#mi_boda').find('tr').length + 1) + "</td>" +
                            "<td>" + info.titulo + "</td>" +
                            "<td>" + (isEmpty(info.url) ? '#' : info.url) + "</td>" +
                            "<td>" +
                            '<a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1" onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                            '<button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
                            "</td>";
                        $('#mi_boda').append(tr);
                        break;
                    case "REDES SOCIALES":
                        tr.innerHTML = "<td>" + ($('#redes').find('tr').length + 1) + "</td>" +
                            "<td>" + info.titulo + "</td>" +
                            "<td>" + (isEmpty(info.url) ? '#' : info.url) + "</td>" +
                            ((info.tipo == "REDES SOCIALES") ? "<td>" + info.icono + "</td>" : "") +
                            "<td>" +
                            '<a class="waves-effect waves-light btn-flat modal-trigger" href="#modal1" onclick="edit_seccion(this)"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                            '<button class="waves-effect waves-light btn-flat" onclick="delete_seccion(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>' +
                            "</td>";
                        $('#redes').append(tr);
                        break;
                }
                $('#modal1').modal("close");
            }).fail(function () {
                //location.reload();
            });
        } else {
            $('#error').text('Faltan datos');
        }
    }

    function send_edit_seccion() {
        if (!isEmpty($('[name=titulo]').val())) {
            var info = {
                id_home_seccion: $('[name=id]').val(),
                titulo: $('[name=titulo]').val(),
                tipo: $('[name=tipo]').val(),
                url: $('[name=url]').val(),
                icono: $('[name=icono]').val(),
            };

            $.ajax({
                url: "<?php echo site_url('App/ADHome/update_seccion') ?>/" + info.id_home_seccion,
                type: "POST",
                timeout: 5000,
                data: info,
            }).done(function () {
                var tr = $('#' + info.id_home_seccion).find('td');
                $(tr[1]).text(info.titulo);
                $(tr[2]).text(isEmpty(info.url) ? '#' : info.url);
                if (info.tipo == "REDES SOCIALES") {
                    $(tr[3]).text(info.icono);
                }
                $('#' + info.id_home_seccion).data('json', info);
                $('#modal1').modal("close");
            }).fail(function () {
                location.reload();
            });
        } else {
            $('#error').text('Faltan datos');
        }
    }

    function delete_seccion(elem) {
        var json = $(elem.parentElement.parentElement).data('json');
        var c = window.confirm("Estas seguro de eliminar este elemento '" + json.titulo + "'");
        if (c) {
            $.ajax({
                url: "<?php echo site_url('App/ADHome/delete_seccion') ?>/" + json.id_home_seccion,
                type: "GET",
                timeout: 5000,
                data: {hola: 'mundo'},
            }).done(function () {
                $(elem.parentElement.parentElement).remove();
            }).fail(function () {
                location.reload();
            });
        }
    }


    function isEmpty(str) {
        if (str == "" || str == null || str == undefined || str == "null") {
            return true;
        }
        return false;
    }
</script>
</body>
</html>