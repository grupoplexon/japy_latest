<html>
    <head>
        <?php $this->view("app/header") ?>
        <link href="<?php echo base_url() ?>dist/css/dropzone.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 20px;"></div>
                <div class="carousel carousel-slider">
                    <?php foreach ($imagenes as $key => $value) { ?>
                        <a class="carousel-item" href="#<?php echo $value->id_imagen; ?>!" id="<?php echo $value->id_imagen; ?>">
                            <img src="<?php echo ("data:" . $value->mime . ";base64," . base64_encode($value->base)) ?>"/>
                        </a>
                    <?php } ?>
                </div>
                <br/>
                <form class="dropzone" id="dropzone1"></form>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
        <script src="<?php echo base_url() ?>dist/js/dropzone.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('.collapsible').collapsible({
                    accordion: false,
                });
<?php if (count($imagenes) > 0) { ?>
                    $('.carousel.carousel-slider').carousel({full_width: true});
<?php } ?>

                $('#dropzone1').dropzone({
                    url: "<?php echo site_url('App/ADHome/subir_imagen') ?>",
                    maxFilesize: 4,
                    method: "POST",
                    paramName: "uploadfile",
                    maxThumbnailFilesize: 6,
                    acceptedFiles: "image/*",
                    addRemoveLinks: true,
                    dictCancelUploadConfirmation: "Estas seguro?",
                    dictRemoveFile: 'Eliminar imagen',
                    dictCancelUpload: 'Cancelar carga',
                    dictInvalidFileType: "No se puede cargar este tipo de archivos.",
                    dictFileTooBig: "La imagen es muy grande ({{filesize}}MiB). Tama&ntilde;o m&aacute;ximo: {{maxFilesize}}MiB.",
                    dictDefaultMessage: '<center><i class="material-icons" style="font-size: 28px;">file_upload</i><br><b>Suelte la imagen o haga click aqu&iacute; para cargar</b></center>',
                    init: function () {
<?php foreach ($imagenes as $i => $temp) { ?>
                            var mockFile = {
                                name: "hgdgaggg3333-<?php echo $temp->id_imagen ?>",
                                size: 12345,
                                type: 'image/jpeg',
                                status: this.ADDED,
                            };
                            this.emit("addedfile", mockFile);
                            this.emit("thumbnail", mockFile, '<?php echo "data:" . $temp->mime . ";base64," . base64_encode($temp->base) ?>');
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                            this.files.push(mockFile);
<?php } ?>

                        this.on('success', function (file, json) {
                            var a = document.createElement('a');
                            a.setAttribute('class', 'carousel-item');
                            a.setAttribute('href', '#' + json.id);
                            a.setAttribute('id', json.id);
                            a.innerHTML = "<img src='" + $($(file.previewTemplate).find('.dz-image img')[0]).attr('src') + "' />";
                            //$('.carousel.carousel-slider').carousel({full_width: true});
                            $('.carousel.carousel-slider').append(a);
                            $(file.previewElement).data('id', json.id);
                        });
                        this.on('addedfile', function () {
                            console.log('addedfile');
                        });
                        this.on('drop', function () {
                            console.log('drop');
                        });
                        this.on('sending', function () {

                        });
                        this.on('error', function () {
                            console.log('entra error');
                        });
                    }, removedfile: function (file) {
                        var id = file.name;
                        if (id.indexOf('hgdgaggg3333-') > -1) {
                            id = id.split("hgdgaggg3333-")[1];
                        } else {
                            id = $(file.previewElement).data('id');
                        }
                        $.ajax({
                            url: '<?php echo site_url('App/ADHome/delete_imagen') ?>',
                            method: 'POST',
                            timeout: 4000,
                            data: {
                                id: id,
                            }
                        }).done(function () {
                            var _ref;
                            $('#' + id).remove();
                            $('.carousel.carousel-slider').carousel({full_width: true});
                            (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                            return true;
                        }).fail(function () {
                            console.log('fail');
                        });
                    }
                });
            });


        </script>
    </body>
</html>