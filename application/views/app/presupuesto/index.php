<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
<?php $this->view("app/menu") ?>
<main>
    <div class="container">
        <div style="margin: 20px;"></div>
        <div class="row">
            <h4 class="page-title pull-left">Valores por defecto<br>
                <small>Total:
                    <text class="total-porcentaje">100</text>
                    %
                </small>
            </h4>
            <a id="open-modal-agregar" class="btn btn-flat pull-right waves-effect waves-light "
               style="margin-top: 15px;" href="#modal-agregar">
                <i class="material-icons">add</i> Agregar
            </a>
        </div>
        <?php $this->view("app/mensajes") ?>
        <div class="row">
            <?php foreach ($categories as $key => $category) : ?>
                <div class="col m6">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <h6 class="col s6" style="text-transform: uppercase;font-weight: bold">
                                    <?php echo $category->name ?>
                                    <a class="tooltipped clickable editar"
                                       data-id='<?php echo $category->id ?>'
                                       data-name='<?php echo $category->name ?>'
                                       data-description='<?php echo $category->description ?>'
                                       data-position="top" data-delay="50" data-tooltip="Editar"
                                       style="color: gray;">
                                        <i class="material-icons">create</i>
                                    </a>
                                </h6>
                                <div class="col m12">
                                    <?php foreach ($category->subcategories as $key => $subcategory) : ?>
                                        <div class="row porcentaje" style=" margin-bottom: 1px;">
                                            <div class="col m12">
                                                <p style="position: absolute;margin-top: 10px;"><?php echo $subcategory->name ?>
                                                    :</p>
                                                <a class="tooltipped clickable editar"
                                                   data-id='<?php echo $subcategory->id ?>'
                                                   data-name='<?php echo $subcategory->name ?>'
                                                   data-description='<?php echo $subcategory->description ?>'
                                                   data-position="top" data-delay="50" data-tooltip="Editar"
                                                   style="position: absolute;right: 0; margin-right: 48px; margin-top: 10px; color: gray;">
                                                    <i class="material-icons">create</i>
                                                </a>
                                                <a class="tooltipped clickable eliminar"
                                                   data-id='<?php echo $subcategory->id ?>'
                                                   data-position="top" data-delay="50" data-tooltip="Eliminar"
                                                   style=" position: absolute;right: 0; margin-right: 24px; margin-top: 10px; color: red;">
                                                    <i class="material-icons">close</i>
                                                </a>
                                                <input id="<?php echo $subcategory->id ?>"
                                                       name="<?php echo $subcategory->id ?>"
                                                       type="text"
                                                       value="<?php echo $subcategory->percentage ?>"
                                                       class="validate right-align input-porcentaje" required
                                                       style="margin-bottom: 5px;    width: calc( 100% - 60px );">
                                            </div>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <div class="row">&nbsp;<div class="row">&nbsp;</div>
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal-agregar" class="modal">
        <form method="POST" action="<?php echo site_url('App/ADPresupuestador/add') ?>">
            <div class="modal-content">
                <h4>Agregar subcategoria</h4>
                <div class="row">
                    <div class="col m12">
                        <select name="categoria">
                            <option> Elige una opcion</option>
                            <?php foreach ($categories as $key => $category) { ?>
                                <option value="<?php echo $category->id ?>"><?php echo strtoupper(
                                            $category->name
                                    ) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col m6 s12">
                        <input id="nombre" name="nombre" type="text" class="validate" required>
                        <label for="nombre">Nombre subcategoria</label>
                    </div>
                    <div class="input-field col m6 s12">
                        <input id="porcentaje" name="porcentaje" type="text" class="validate" required>
                        <label for="porcentaje">Porcentaje</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button href="#!" class="modal-action modal-close waves-effect btn-flat">Aceptar</button>
                <a href="#!" class="modal-action modal-close waves-effect btn-flat">Cancelar</a>
            </div>
        </form>
    </div>
</main>
<div id="modal-eliminar" class="modal">
    <div class="modal-content">
        <h4>Eliminar concepto</h4>
        <p>Estas seguro de eliminar el concepto?</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect red-text waves-red btn-flat aceptar">Eliminar</a>
        <a href="#!" class="modal-action modal-close waves-effect btn-flat">Cancelar</a>
    </div>
</div>
<div id="modal-editar" class="modal">
    <div class="modal-content">
        <h4>Editar concepto</h4>
        <div class="row">
            <form action="" class="col s12">
                <div class="row">
                    <div class="input-field col s12">
                        <input value="" id="name" type="text" class="validate">
                        <label for="name">Nombre</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea value="" id="description" class="materialize-textarea"></textarea>
                        <label for="description">Descripcion</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect red-text waves-red btn-flat aceptar">Aceptar</a>
        <a href="#!" class="modal-action modal-close waves-effect btn-flat">Cancelar</a>
    </div>
</div>
<?php $this->view("app/footer") ?>
<script>
    $(document).ready(function () {
        $('.modal').modal();
        var prev;

        load_value();

        $('select').material_select();

        $('#open-modal-agregar').modal();

        $(".input-porcentaje").on('focusin', function () {
            $(this).data('val', $(this).val());
            prev = this.value;
        }).keypress(function (key) {
            if (key.charCode == 13) $(this).change();
            if ((key.charCode < 48 || key.charCode > 57) && key.charCode != 46) return false;
        }).on('change', function () {
            var value = this.value;
            var self = $(this);
            var id = self.attr('name');

            if (!value.length) {
                value = 0;
                $(this).val(value);
            } else {
                value = parseFloat(value);
            }

            $.ajax({
                context: this,
                url: '<?php echo site_url('App/ADPresupuestador/update') ?>',
                method: 'POST',
                data: {
                    id: id,
                    value: value,
                },
                success: function (res) {
                    load_value();
                }
                ,
                error: function (response) {
                    alert(response.responseJSON.message);
                    $(this).val(prev);
                }
            })
            ;
        });

        $(".editar").on('click', function () {
            const id = $(this).data('id');
            const self = $(this);
            const $modalEditar = $('#modal-editar');
            const $name = $modalEditar.find('#name');
            const $description = $modalEditar.find('#description');

            $name.val(self.data('name'));
            $description.val(self.data('description'));

            Materialize.updateTextFields();

            $("#modal-editar .aceptar").one('click', function (e) {
                e.preventDefault();
                $.ajax({
                    url: '<?php echo site_url('App/ADPresupuestador/update') ?>',
                    method: 'POST',
                    data: {
                        id: id,
                        name: $name.val(),
                        description: $description.val()
                    },
                    success: function (res) {

                        self.prev().html($name.val());
                        self.data('name', $name.val());
                        self.data('description', $description.val());

                        Materialize.toast('Concepto editado', 4000);
                        load_value();
                    }, error: function () {
                        Materialize.toast('No se edito el concepto', 4000);
                        $("#modal-editar").modal("close");
                    }
                });
            });

            $modalEditar.modal("open");
        });

        $(".eliminar").on('click', function () {
            var id = $(this).data('id');
            var self = $(this);
            $("#modal-eliminar .aceptar").one('click', function () {
                $.ajax({
                    url: '<?php echo site_url('App/ADPresupuestador/delete') ?>',
                    method: 'POST',
                    data: {
                        id: id
                    },
                    success: function (res) {
                        self.parent().parent().remove();
                        $("#modal-eliminar").modal("close");
                        Materialize.toast('Concepto eliminado', 4000);
                        load_value();
                    }, error: function () {
                        Materialize.toast('No se elimino el concepto', 4000);
                        $("#modal-eliminar").modal("close");
                    }
                });
            });
            $("#modal-eliminar").modal("open");
        });
    });

    function load_value() {
        var t = document.querySelectorAll('.porcentaje input');
        var total = 0;
        for (var i = 0; i < t.length; i++) {
            total += parseFloat(t[i].value);
        }
        document.querySelector('.total-porcentaje').innerHTML = total;
    }
</script>
</body>
</html>