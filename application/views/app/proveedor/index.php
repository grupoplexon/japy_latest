<?php

function classTipo($tipo)
{
    switch ($tipo) {
        case 0:
            return "dropdown-button btn z-depth-0 ";
        case 1:
            return "dropdown-button btn z-depth-0 black-text grey lighten-2";
        case 2:
            return "dropdown-button btn z-depth-0 amber accent-4";
        case 3:
            return "dropdown-button btn z-depth-0 blue-grey darken-4";
    }
}

?>

<html>
<head>
    <?php $this->view("app/header") ?>
    <!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>-->
    <style>
        a.btn {
            font-size: 12px;
            width: 100%;
            padding: 0px;
            line-height: 27px;
            height: 27px;
        }
    </style>
</head>
<body>
<?php $this->view("app/menu") ?>
<main>
    <div class="container">
        <div style="margin: 20px;"></div>
        <div class="row">
            <h5>Proveedores</h5>
            <div class="divider"></div>
            <div class="card-panel">
                <?php if (isset($proveedores)) { ?>
                    <table id="data-table-proveedores">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Tipo cuenta</th>
                            <th>Usuario</th>
                            <th>Correo</th>
                            <th>Tel&eacute;fono</th>
                            <th>Estado/Pa&iacute;s</th>
                            <th>Registro desde</th>
                            <th>Activo en sistema</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($proveedores as $key => $proveedor) { ?>
                            <tr id="<?= $proveedor->id_proveedor ?>">
                                <td>
                                    <?php echo $proveedor->nombre ?>
                                </td>
                                <td>
                                    <a class="<?php echo classTipo($proveedor->tipo_cuenta) ?>" href="#"
                                       data-activates="menu-tipo<?= $proveedor->id_proveedor ?>"><?php echo $this->proveedor->labelTipoCuenta(
                                                $proveedor->tipo_cuenta) ?></a>
                                    <!-- Dropdown Structure -->
                                    <ul id="menu-tipo<?= $proveedor->id_proveedor ?>"
                                        class="dropdown-content menu-tipo"
                                        data-proveedor="<?= $proveedor->id_proveedor ?>">
                                        <li><a href="#!" class="black-text" data-value="basico">Basico</a></li>
                                        <li><a href="#!" class="black-text" data-value="plata">Plata</a></li>
                                        <li><a href="#!" class="black-text" data-value="oro">Oro</a></li>
                                        <li><a href="#!" class="black-text" data-value="diamante">Diamante</a></li>
                                    </ul>
                                </td>
                                <td>
                                    <?php echo $proveedor->usuario ?>
                                </td>
                                <td>
                                    <?php echo $proveedor->correo ?>
                                </td>
                                <td>
                                    <?php echo $proveedor->contacto_telefono ?>
                                </td>
                                <td>
                                    <?php echo $proveedor->localizacion_estado ?>
                                    , <?php echo $proveedor->localizacion_pais ?>
                                </td>
                                <td data-order="<?php echo timeFormat($proveedor->fecha_creacion, "%s") ?>">
                                    <?php echo timeFormat($proveedor->fecha_creacion, "%d-%b-%Y") ?>
                                </td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            NO
                                            <input onchange="onChangeEstadoProveedor(this)" class="activar"
                                                   type="checkbox" <?php echo $proveedor->acceso ? "checked" : "" ?>
                                                   data-proveedor="<?= $proveedor->id_proveedor ?>">
                                            <span class="lever"></span>
                                            SI
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <div class="center">
                        <i class="fa fa-info fa-4x"></i><br> No tienes proveedores registrados
                    </div>
                <?php } ?>
            </div>
        </div>
        <div style="margin-bottom: 250px">
            &nbsp;
        </div>
    </div>
</main>
<?php $this->view("app/footer") ?>

<!--    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>-->
<script>
    var server = "<?php echo base_url() ?>";

    $(document).ready(function () {
        $("#data-table-proveedores").DataTable({
            autoWidth: false,
            responsive: true
        });

        $("body").on("click", ".menu-tipo a", function () {
            var $this = $(this);
            var proveedor = $this.parent().parent().data("proveedor");

            $.ajax({
                url: server + "App/ADProveedor/cambiar_cuenta",
                method: "POST",
                data: {
                    tipo: $this.data("value"),
                    proveedor: proveedor
                },
                success: function (res) {
                    if (res.success) {
                        var btn = $("#" + proveedor + " .dropdown-button");
                        btn.html($this.data("value"));
                        switch ($this.data("value")) {
                            case "basico":
                                btn.attr("class", "dropdown-button btn z-depth-0 ");
                                break;
                            case "plata":
                                btn.attr("class", "dropdown-button btn z-depth-0 black-text grey lighten-2");
                                break;
                            case "oro":
                                btn.attr("class", "dropdown-button btn z-depth-0 amber accent-4");
                                break;
                            case "diamante":
                                btn.attr("class", "dropdown-button btn z-depth-0 blue-grey darken-4");
                                break;
                        }
                    }
                }
            });
        });

    });

    function onChangeEstadoProveedor(elem) {
        var $this = $(elem);
        var proveedor = $this.data("proveedor");
        $.ajax({
            url: server + "App/ADProveedor/toggle_activar",
            method: "POST",
            data: {
                proveedor: proveedor
            },
            success: function (res) {
                if (res.success) {
                }
            }
        });
    }
</script>
</body>
</html>


