<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
    <?php $this->view("app/menu") ?>
    <main>
        <div class="container">
            <div style="margin: 10px;"></div>
            <div class="right-align">
                <a class="waves-effect waves-light btn dorado-2"
                   href="<?php echo $this->config->base_url() ?>App/ADPublicidad/alta/<?php echo $id_expo ?>">
                    <i class="material-icons left">add_circle_outline</i> Registrar publicidad
                </a>
            </div>
            <div class="card">
                <table class="responsive-table bordered highlight">
                    <thead>
                    <tr class="dorado-2">
                        <th data-field="id">Imagen</th>
                        <th data-field="name">Tipo</th>
                        <th data-field="name">Fecha Inicio</th>
                        <th data-field="name">Fecha Fin</th>
                        <th data-field="name">Expo</th>
                        <th data-field="name">Estado</th>
                        <th data-field="name">Acci&oacute;n</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($publicidades as $key => $value) { ?>
                        <tr>
                            <td><img style="width: 250px; height: auto;" src="<?php echo $value->imagen; ?>"></td>
                            <td><?php echo ($value->tipo == 1) ? 'Aplicaci&oacute;n m&oacute;vil' : 'P&aacute;gina web'; ?></td>
                            <td><?php echo utf8_encode(strftime("%A %d de %B de %Y", strtotime($value->fecha_inicio))); ?></td>
                            <td><?php echo utf8_encode(strftime("%A %d de %B de %Y", strtotime($value->fecha_termino))); ?></td>
                            <td><?php echo empty($value->nombre) ? 'TODOS' : $value->nombre ?></td>
                            <td>
                                <?php
                                $datetime1 = new DateTime($value->fecha_inicio);
                                $datetime2 = new DateTime($value->fecha_termino);
                                if (new DateTime("now") >= $datetime1 && new DateTime("now") <= $datetime2) {
                                    echo '<div class="chip green white-text">ACTIVO</div>';
                                } else {
                                    echo '<div class="chip">INACTIVO</div>';
                                }
                                ?>
                            </td>
                            <td>
                                <a style="color:#f5c564; cursor: pointer;"
                                   href="<?php echo $this->config->base_url() ?>App/ADPublicidad/editar/<?php echo $value->id_publicidad; ?>"><i
                                            class="material-icons">create</i></a>
                                <span style="color:#f5c564; cursor: pointer;"
                                      onclick="eliminar_publicidad('<?php echo $value->id_publicidad; ?>');"><i
                                            class="material-icons">delete</i></span>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="row">&nbsp;<div class="row">&nbsp;</div>
            </div>
            <script>
                function eliminar_publicidad(id) {
                    var r = confirm("Esta seguro que desea eliminar esta publicidad?");
                    if (r == true) {
                        location.href = "<?= $this->config->base_url(); ?>App/ADPublicidad/eliminar/" + id;
                    }
                }
            </script>

        </div>
    </main>
    <?php $this->view("app/footer") ?>
</body>
</html>




