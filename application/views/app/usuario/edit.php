<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
    <?php $this->view("app/menu") ?>
    <main>
        <div class="container">
            <div style="margin: 10px;"></div>
            <div class="card">
                <div class="card-content">
                    <form method="post"
                          action="<?php echo isset($info) ? site_url('App/Usuario/editar') : site_url('App/Usuario/alta') ?>">
                        <?php if (isset($error)) { ?>
                            <div class="card-panel red lighten-1">Usuario ya existe, intente con otro!</div>
                        <?php } ?>

                        <div class="row">
                            <div class="input-field col s6">
                                <input class="validate" id="usuario" name="usuario" placeholder="Usuario"
                                       value="<?php echo isset($info) ? $info->usuario : (isset($error) ? $error['usuario'] : '') ?>"
                                       type="text" <?php echo isset($info) ? 'readonly' : '' ?> required>
                                <label for="usuario">Usuario</label>
                            </div>
                            <div class="input-field col s6">
                                <label>Rol</label><br/><br/>
                                <select class="browser-default"
                                        name="rol" <?php echo (isset($info) && $info->id_usuario == $this->session->userdata('id_usuario')) ? 'disabled' : '' ?>
                                        required>
                                    <option value="" disabled="" selected="">Eligue un rol</option>
                                    <option value="1" <?php echo (isset($info) && $info->rol == 1) ? 'selected' : ((isset($error) && $error['rol'] == 1) ? 'selected' : '') ?>>
                                        Administrador
                                    </option>
                                    <option value="4" <?php echo (isset($info) && $info->rol == 4) ? 'selected' : ((isset($error) && $error['rol'] == 4) ? 'selected' : '') ?>>
                                        Redactor
                                    </option>
                                    <option value="6" <?php echo (isset($info) && $info->rol == 6) ? 'selected' : ((isset($error) && $error['rol'] == 6) ? 'selected' : '') ?>>
                                        Moderador
                                    </option>
                                    <option value="7" <?php echo (isset($info) && $info->rol == 7) ? 'selected' : ((isset($error) && $error['rol'] == 7) ? 'selected' : '') ?>>
                                        Galeria
                                    </option>
                                    <option value="8" <?php echo (isset($info) && $info->rol == 8) ? 'selected' : ((isset($error) && $error['rol'] == 8) ? 'selected' : '') ?>>
                                        Blog
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <input class="validate" id="password" name="contrasena" placeholder="Contrase&ntilde;a"
                                       type="password" <?php echo isset($info) ? 'disabled' : 'required' ?> >
                                <label for="password">Contrase&ntilde;a</label>
                            </div>
                            <div class="input-field col s6">
                                <?php if (isset($info)) { ?>
                                    <p>
                                        <input type="checkbox" id="cambiar_contrasena">
                                        <label for="cambiar_contrasena">Cambiar contrase&ntilde;a</label>
                                    </p>
                                <?php } ?>
                                &nbsp;
                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s6">
                                <input class="validate" id="first_name" name="nombre" placeholder="Nombre"
                                       value="<?php echo isset($info) ? $info->nombre : (isset($error) ? $error['nombre'] : '') ?>"
                                       type="text" required>
                                <label for="first_name" class="active">Nombre</label>
                            </div>
                            <div class="input-field col s6">
                                <input class="validate" id="last_name" name="apellido" placeholder="Apellido(s)"
                                       value="<?php echo isset($info) ? $info->apellido : (isset($error) ? $error['apellido'] : '') ?>"
                                       type="text" required>
                                <label for="last_name">Apellido(s)</label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s2" style="padding: 20px;">
                                <img class="responsive-img" id="img"
                                     src="<?php echo (isset($info) && !empty($info->mime)) ? ("data:" . $info->mime . ";base64," . base64_encode($info->foto)) : ((isset($error) && !empty($error['mime'])) ? ("data:" . $error['mime'] . ";base64," . base64_encode($error['foto'])) : base_url() . 'dist/img/Interrogacion-simbolo.png') ?>"
                                     alt=""/>
                                <input type="hidden" name="imagen"
                                       value="<?php echo isset($info) ? ("data:" . $info->mime . ";base64," . base64_encode($info->foto)) : (isset($error) ? ("data:" . $error['mime'] . ";base64," . base64_encode($error['foto'])) : ''); ?>"/>

                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>Imagen</span>
                                        <input type="file" accept="image/*" onchange='openFile(event)'>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col s2 offset-s8">
                                <br/><br/>
                                <input type="hidden" name="id_usuario"
                                       value="<?php echo isset($info) ? $info->id_usuario : ''; ?>"/>

                                <button class="btn waves-effect waves-light" type="submit" name="action">Guardar
                                </button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>

        </div>
    </main>

    <?php $this->view("app/footer") ?>
    <script>
        var MAX_WIDTH = 400;
        var MAX_HEIGHT = 400;

        $(document).ready(function () {
            $('#cambiar_contrasena').on('change', function () {
                if ($('#cambiar_contrasena').prop('checked')) {
                    $('[name="contrasena"]').attr('required', 'required');
                    $('[name="contrasena"]').removeAttr('disabled');
                } else {
                    $('[name="contrasena"]').attr('disabled', 'disabled');
                    $('[name="contrasena"]').removeAttr('required');
                }
            });
        });

        function openFile(event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function (e) {
                var img = document.createElement("img");
                img.src = e.target.result;

                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var width = img.width;
                var height = img.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);

                var dataurl = canvas.toDataURL("image/jpg");
                document.getElementById('img').src = img.src;
                $('[name="imagen"]').val(img.src);
            };
            reader.readAsDataURL(input.files[0]);
        }
    </script>

</body>
</html>