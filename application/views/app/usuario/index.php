<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>

                <div class="card">
                    <div class="card-content">
                        <div class="card-title dorado-2-text">
                            <b>USUARIO</b>
                            <a class="btn btn-flat right" href="<?php echo site_url('App/Usuario/alta'); ?>" ><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Usuario</td>
                                    <td>Nombre</td>
                                    <td>Rol</td>
                                    <td>Opciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($usuarios as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $key + 1 ?></td>
                                        <td><?php echo $value->usuario ?></td>
                                        <td><?php echo $value->nombre . ' ' . $value->apellido ?></td>
                                        <td><?php echo $value->rol ?></td>
                                        <td>
                                            <a class="waves-effect waves-light btn-flat" href="<?php echo site_url("App/Usuario/editar") . "/" . $value->id_usuario ?>">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <?php if ($this->session->userdata('id_usuario') != $value->id_usuario) { ?>
                                                <div class="waves-effect waves-light btn-flat" onclick="eliminar(this,<?php echo $value->id_usuario ?>)"><i class="fa fa-trash" aria-hidden="true"></i></div>
                                                <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </main>
        <?php $this->view("app/footer") ?>
        <script>
            function eliminar(esto, id) {
                var c = confirm("¿Seguro de eliminar este usuario?");
                if(c){
                    $.ajax({
                        method:'POST',
                        url:'<?php echo site_url('App/Usuario/delete')?>',
                        data:{'id':id},
                    }).done(function(){
                        $(esto.parentElement.parentElement).remove();
                    }).fail(function(){
                        location.reload();
                    });
                }
            }
        </script>
    </body>
</html>