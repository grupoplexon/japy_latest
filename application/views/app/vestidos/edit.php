<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        <?php $this->view("app/header") ?>
        <style>
            .upload:hover {
                border: 3px dashed #f9a897;
            }

            .upload {
                min-height: 150px;
                border: 3px dashed #f5e6df;
                cursor: pointer;
            }

            .dz-progress, .dz-error-message, .dz-success-mark, .dz-error-mark {
                display: none;
            }

            .dz-preview {
                margin: 0px auto;
            }

            .dz-image {
                transition: 0.5s;
            }

            .dz-image img {
                width: 150px;
                height: auto;
            }

            .dz-details {
                text-align: center;
                position: absolute;
                margin-top: -95px;
                background: rgba(255, 255, 255, 0.7);
                border-radius: 20px;
                font-size: 11px;
                padding: 5px;
                width: 150px;
                opacity: 0;
                transition: 0.3s;
            }
        </style>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>
                <div class="right-align">
                    <a class="waves-effect waves-light btn dorado-2" href="<?php echo site_url("adminvestidos/home/nuevo") ?>">
                        <i class="material-icons left">add_circle_outline</i> Registrar articulo
                    </a>
                </div>
                <h4>Editar
                    <small><?php echo $vestido->nombre ?></small>
                </h4>
                <div class="divider"></div>
                <?php $this->view("proveedor/mensajes") ?>
                <br>
                <div class="row">
                    <div class="card-panel">
                        <form method="POST" action="<?php echo site_url("adminvestidos/home/editar/$vestido->id_vestido") ?>">
                            <p>
                                <label for="input-nombre">nombre</label>
                                <input id="input-nombre" name="nombre" value="<?php echo $vestido->nombre ?>">
                            </p>
                            <p>
                                <label for="detalle">Descripcion</label>
                                <textarea id="detalle" class="detalle" name="detalle"><?php echo $vestido->detalle ?></textarea>
                            </p>
                            <div class="row">
                                <div class="col s12 m6">
                                    <?php foreach ($tipo->sortable as $key => $value) { ?>
                                        <p>
                                            <?php if ($value->valores) { ?>
                                                <label style="top: -25px;"><?php echo ucfirst($value->nombre) ?></label>
                                                <select name="<?php echo $value->nombre ?>" class="browser-default">
                                                    <?php foreach ($value->valores as $key => $v) { ?>
                                                        <option value="<?php echo $v->nombre ?>" <?php echo ($v->nombre == $vestido->{$value->nombre}) ? "selected" : "" ?> > <?php echo ucfirst($v->nombre) ?></option>
                                                    <?php } ?>
                                                </select>
                                            <?php } else { ?>
                                                <label style="top: 0px;"><?php echo ucfirst($value->nombre) ?></label>
                                                <?php if ($value->nombre == "diseñador") { ?>
                                                    <input name="<?php echo($value->nombre) ?>" value="<?php echo $vestido->disenador ?>">
                                                <?php } else { ?>
                                                    <input name="<?php echo($value->nombre) ?>" value="<?php echo $vestido->{$value->nombre} ?>">
                                                <?php } ?>
                                            <?php } ?>
                                        </p>
                                    <?php } ?>
                                </div>
                                <div class="col s12 m6">
                                    <div class="card-panel z-depth-0 valign-wrapper upload  " id="upload">
                                        <text style="width: 100%;text-align: center;color: gray;"><i class="fa fa-upload fa-2x valign "></i><br>
                                            Suelte los archivos o haga clic aqu&iacute; para cargar.
                                        </text>
                                    </div>
                                    <input type="hidden" name="file" class="archivo" value="<?php echo $vestido->imagen ?>">
                                    <button class="eliminar-foto btn grey right" type="button">
                                        Eliminar foto
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <button class="btn dorado-2 right btn-block-on-small">
                                    <i class="material-icons left">save</i>Guardar
                                </button>
                            </div>
                        </form>

                    </div>
                    <div style="margin-bottom: 250px"></div>
                </div>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
        <script src="<?php echo base_url() ?>dist/js/dropzone.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                init_tinymce_mini('#detalle');
                $('.upload').each(function(i, elem) {
                    var dropzone = new Dropzone(elem, {
                        url: "<?php echo base_url() ?>proveedor/escaparate/fotossad3",
                        method: 'POST',
                        paramName: 'files', // The name that will be used to transfer the file
                        maxFilesize: 1, // MB
                        uploadMultiple: false,
                        thumbnailWidth: null,
                        thumbnailHeight: null,
                        createImageThumbnails: true,
                        accept: function(file, done) {
                            $(elem).find('text').hide();

                            var t = $(elem).find('div.dz-preview').length;

                            if (t > 1) {
                                $(elem).find('div.dz-preview').each(function(i, value) {
                                    if (t - 1 != i) {
                                        value.remove();
                                    }
                                });
                            }

                            $(elem).parent().find('.label-error').addClass('hide');

                            $(elem).find('.dz-preview').on('mouseenter', function() {
                                $(elem).find('.dz-details').css({opacity: '1'});
                                $(elem).find('img').css({
                                    'filter': 'blur(5)',
                                    '-webkit-filter': ' blur(5px)',
                                });
                            });

                            $(elem).find('.dz-preview').on('mouseleave', function() {
                                $(elem).find('.dz-details').css({opacity: '0'});
                                $(elem).find('img').css({
                                    'filter': 'blur(0)',
                                    '-webkit-filter': ' blur(0px)',
                                });
                            });

                        },
                        error: function(data) {
                            $('.eliminar-foto').trigger('click');
                            $(elem).parent().find('.archivo').val();
                            $(elem).parent().find('.label-error').removeClass('hide');
                        },
                        acceptedFiles: 'image/*',
                        init: function() {
                            this.on('thumbnail', function(file) {
                                if (file.size < (1024 * 1024 * 1)) // not more than 1mb
                                {
                                    $(elem).parent().find('.archivo').val($(file.previewElement).find('img').attr('src'));
                                }
                            });
                        },
                    });

                    var prom = $(elem).parent().find('.archivo');
                    if (prom) {// Create the mock file:
                        var file = prom.attr('value');
                        if (file != null && file != '') {
                            var mockFile = {name: 'Filename', size: 12345};
                            dropzone.emit('addedfile', mockFile);
                            dropzone.emit('thumbnail', mockFile, file);
                            dropzone.emit('complete', mockFile);
                            $(elem).find('text').hide();
                        }
                    }
                });

                $('.upload text').on('click', function(e) {
                    $(e.currentTarget.parentNode).trigger('click');
                });

                $('.eliminar-foto').on('click', function(elm) {
                    var $btn = $(elm.currentTarget);
                    var $prom = $btn.parent();
                    $prom.find('.upload div.dz-preview').remove();
                    $prom.find('text').show();
                    $prom.find('.archivo').val('');
                    $prom.parent().find('.label-error').addClass('hide');
                });
            });

            function init_tinymce_mini(elem) {
                if (typeof tinymce != 'undefined') {
                    tinymce.init({
                        selector: elem,
                        relative_urls: false,
                        menubar: '',
                        plugins: [
                            'advlist autolink lists link  preview anchor',
                            'searchreplace code ',
                            'insertdatetime paste ',
                        ],
                        statusbar: false,
                        height: 350,
                        imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58', 'clubnupcial.com'],
                        browser_spellcheck: true,
                        toolbar: 'insertfile undo redo  | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist ',
                    });
                }
            }
        </script>
    </body>
</html>




