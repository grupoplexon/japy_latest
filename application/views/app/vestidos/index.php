<html>
    <head>
        <?php $this->view("app/header") ?>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>
                <div class="right-align">
                    <a class="waves-effect waves-light btn dorado-2" href="<?php echo site_url("adminvestidos/home/nuevo") ?>">
                        <i class="material-icons left">add_circle_outline</i>  Registrar articulo
                    </a>   
<!--                    <a class="waves-effect waves-light btn grey" href="<?php echo site_url("adminvestidos/home/nuevo_tipo") ?>">
                        <i class="material-icons left">add_circle_outline</i>  Registrar tipo de art&iacute;culo
                    </a>  -->
                </div>
                <br>
                <div class="row">
                    <?php $colores = array("deep-purple", "light-green", "blue", "teal", "cyan", "indigo", "green", "light-blue"); ?>
                    <?php $c = 0; ?>
                    <?php foreach ($tipos as $key => $tipo) { ?>
                        <div class="col m3 s6">
                            <div class="card-panel <?php echo $colores[$c++] ?> clickable z-depth-2-hover">
                                <a href="<?php echo site_url("adminvestidos/home/articulos/" . str_replace(" ", "-", $tipo->nombre)) ?>" class="row" style="margin-bottom: 0px">
                                    <b style="font-size: 20px" class="col s12 m4 center white-text"><?php echo $tipo->total ?></b>
                                    <text class="right col s12 m8 center truncate white-text"> <?php echo $tipo->nombre ?></text>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php $this->view("proveedor/mensajes") ?>
            </div>
        </main>
        <?php $this->view("app/footer") ?>
    </body>
</html>




