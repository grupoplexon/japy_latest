<html>
<head>
    <?php $this->view("app/header") ?>
</head>
<body>
<?php $this->view("app/menu") ?>
<main>
    <div class="container">
        <div style="margin: 10px;"></div>
        <div class="right-align">
            <a class="waves-effect waves-light btn dorado-2" href="<?php echo site_url("adminvestidos/home/nuevo") ?>">
                <i class="material-icons left">add_circle_outline</i> Registrar articulo
            </a>
        </div>
        <?php $this->view("proveedor/mensajes") ?>
        <br>
        <div class="row">
            <h4><?php echo $tipo->nombre ?></h4>
            <div class="divider"></div>
            <?php if ($articulos) { ?>
                <?php $tipo->sortable = explode(",", $tipo->sortable) ?>
                <div class="card-panel">
                    <table>
                        <thead>
                        <tr>
                            <th>
                                Nombre
                            </th>
<!--                            <th>-->
<!--                                Descripci&oacute;n-->
<!--                            </th>-->
                            <?php foreach ($tipo->sortable as $key => $sort) { ?>
                                <th>
                                    <?php echo ucfirst($sort) ?>
                                </th>
                            <?php } ?>
                            <th>
                                Opciones
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($articulos as $key => $art) { ?>
                            <tr>
                                <td>
                                    <?php echo $art->nombre ?>
                                </td>
<!--                                <td>-->
<!--                                    --><?php //echo substr($art->detalle, 0, 150) ?>
<!--                                </td>-->
                                <?php foreach ($tipo->sortable as $key => $sort) { ?>
                                    <td>
                                        <?php if ($sort == "diseñador") { ?>
                                            <?php echo $art->disenador ?>
                                        <?php } else { ?>
                                            <?php echo $art->{$sort} ?>
                                        <?php } ?>
                                    </td>
                                <?php } ?>
                                <td>
                                    <a href="<?php echo site_url("adminvestidos/home/editar/$art->id_vestido") ?>"
                                       class="tooltipped dorado-2-text" data-position="top" data-delay="50"
                                       data-tooltip="Editar">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="<?php echo site_url("adminvestidos/home/delete/$art->id_vestido") ?>"
                                       class="tooltipped dorado-2-text" data-position="top" data-delay="50"
                                       data-tooltip="Borrar">
                                        <i class="material-icons">delete</i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } else { ?>
                <div class="card-panel center">
                    <div class="row">
                        <i class="fa fa-info fa-4x circle "></i> <br>
                        No hay <?php echo ucfirst($tipo->nombre) ?> no registrados, te recomendamos
                        registrar <?php echo ucfirst($tipo->nombre) ?> para atraer nuevos usuarios a la plataforma
                        <br>
                        <a class="btn-flat dorado-2-text right"
                           href="<?php echo site_url("adminvestidos/home/nuevo") ?>">
                            Registrar nuevo articulo <i class="material-icons right">keyboard_arrow_right</i>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</main>
<?php $this->view("app/footer") ?>
</body>
</html>




