<html>
    <head>
        <?php $this->view("app/header") ?>
        <style>
            .dropdown-content li > a, .dropdown-content li > span {
                color: black;
            }

            .upload:hover {
                border: 3px dashed #f9a897;
            }

            .upload {
                min-height: 150px;
                border: 3px dashed #f5e6df;
                cursor: pointer;
            }

            .dz-progress, .dz-error-message, .dz-success-mark, .dz-error-mark {
                display: none;
            }

            .dz-preview {
                margin: 0px auto;
            }

            .dz-image {
                transition: 0.5s;
            }

            .dz-image img {
                width: 150px;
                height: auto;
            }

            .dz-details {
                text-align: center;
                position: absolute;
                margin-top: -95px;
                background: rgba(255, 255, 255, 0.7);
                border-radius: 20px;
                font-size: 11px;
                padding: 5px;
                width: 150px;
                opacity: 0;
                transition: 0.3s;
            }

            .input-field > select {
                height: 48px !important;
                margin-top: 10px !important;
                position: initial !important;
                pointer-events: initial !important;
                opacity: initial !important;
            }
        </style>
    </head>
    <body>
        <?php $this->view("app/menu") ?>
        <main>
            <div class="container">
                <div style="margin: 10px;"></div>
                <div class="right-align">
                    <a class="waves-effect waves-light btn dorado-2" href="<?php echo site_url("adminvestidos/home/nuevo") ?>">
                        <i class="material-icons left">add_circle_outline</i> Registrar articulo
                    </a>
                </div>
                <h4>Registrar articulo</h4>
                <div class="divider"></div>
                <div class="row">
                    <?php $this->view("proveedor/mensajes") ?>
                    <div class="col m12">
                        <div class="card-panel">
                            <form method="POST" action="<?php echo site_url("adminvestidos/home/nuevo") ?>">
                                <div class="row">
                                    <div class="col s12">
                                        <div class="row">
                                            <div class="input-field col s6">
                                                <i class="material-icons prefix">group_work</i>
                                                <select id="select-tipo-vestido" name="tipo_vestido">
                                                    <option value="">Selecciona una opcion</option>
                                                    <?php $val = value_post("tipo_vestido"); ?>
                                                    <?php foreach ($tipos as $key => $t) { ?>
                                                        <?php if ($val != "" && $val == $t->id_tipo_vestido) { ?>
                                                            <option value="<?php echo $t->id_tipo_vestido ?>" selected><?php echo $t->nombre ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $t->id_tipo_vestido ?>"><?php echo $t->nombre ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <label for="select-tipo-vestido">Tipo de articulo</label>
                                            </div>
                                            <div class=" col s6">
                                                <div class="card-panel blue lighten-5">
                                                    <i class="fa fa-info"></i> Selecciona primero el tipo de articulo a subir
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                            <b>DATOS GENERALES : </b>
                                        </p>
                                        <div class="divider"></div>
                                        <p>
                                            <label for="input-nombre">Modelo</label>
                                            <input id="input-nombre" name="nombre" value="<?php echo value_post("nombre") ?>">
                                        </p>
                                        <label>Descripcion</label>
                                        <textarea id="detalle" class="detalle" name="detalle"><?php echo value_post("detalle") ?></textarea>
                                        <div class=" formulario" style="">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div style="margin-bottom: 200px"></div>
                    </div>
                </div>
            </div>
        </main>
        <div id="template-formulario" class="hide">
            <?php foreach ($tipos as $key => $value) { ?>
                <div id="<?php echo $value->id_tipo_vestido ?>">
                    <div class="row">
                        <div class="col s12 m6">
                            <p>
                                <b>DETALLE : </b>
                            </p>
                            <div class="divider"></div>
                            <br>
                            <?php foreach ($value->sortable as $key => $value) { ?>
                                <div class="input-field col s12 ">
                                    <i class="material-icons prefix"><?php echo icon_descriptor($value->nombre) ?></i>
                                    <?php if ($value->valores) { ?>
                                        <select id="input-<?php echo($value->nombre) ?>" name="<?php echo $value->nombre ?>" class="browser-default" style="margin-left: 50px;width: calc( 100% - 50px );">
                                            <option value=""> -- Selecciona una opcion --</option>
                                            <?php foreach ($value->valores as $key => $v) { ?>
                                                <option value="<?php echo $v->nombre ?>"> <?php echo ucfirst($v->nombre) ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="input-<?php echo($value->nombre) ?>" style="top: -25px;"><?php echo ucfirst($value->nombre) ?></label>
                                    <?php } else { ?>
                                        <br>
                                        <input id="input-<?php echo($value->nombre) ?>" name="<?php echo($value->nombre) ?>">
                                        <label for="input-<?php echo($value->nombre) ?>" style="top: 0px;"><?php echo ucfirst($value->nombre) ?></label>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col s12 m6">
                            <p>
                                <b>IMAGEN:</b>
                            </p>
                            <div class="divider"></div>
                            <div class="imagen">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button class="btn right dorado-2 waves-effect">
                            <i class="material-icons right">send</i> guardar
                        </button>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div id="template-upload" class="hide">
            <div class="card-panel z-depth-0 valign-wrapper upload  " id="upload">
                <text style="width: 100%;text-align: center;color: gray;"><i class="fa fa-upload fa-2x valign "></i><br>
                    Suelte los archivos o haga clic aqu&iacute; para cargar.
                </text>
            </div>
            <input type="hidden" name="file" class="archivo">
            <button class="eliminar-foto btn grey right" type="button">
                Eliminar
            </button>
        </div>
        <?php $this->view("app/footer") ?>
        <script src="<?php echo base_url() ?>dist/js/dropzone.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
        <script>
            var data = JSON.parse('<?php echo json_encode($_POST); ?>');
            $(document).ready(function() {
                $('select').material_select();
                init_tinymce_mini('#detalle');
                $('#select-tipo-vestido').on('change', on_change_select_vestido);
                var v = $('#select-tipo-vestido').val();
                if (v && v != '') {
                    $('#select-tipo-vestido').trigger('change');
                    var $form = $('.formulario');
                    for (var i in data) {
                        $form.find('input[name=\'' + i + '\']').val(data[i]);
                        $form.find('select[name=\'' + i + '\']').val(data[i]).change();
                    }
                    var prom = $form.find('.archivo');
                    if (prom) {// Create the mock file:
                        var file = prom.attr('value');
                        if (file != null && file != '') {
                            var mockFile = {name: 'Filename', size: 12345};
                            var dropzone = Dropzone.forElement('.formulario .upload');
                            dropzone.emit('addedfile', mockFile);
                            dropzone.emit('thumbnail', mockFile, file);
                            dropzone.emit('complete', mockFile);
                            $form.find('.upload').find('text').hide();
                        }
                    }
                }
            });

            function on_change_select_vestido() {

                var $this = $(this);
                var $form = $('.formulario');
                $form.html('');
                $form.html($('#template-formulario #' + $this.val()).html());
                $form.find('.imagen').append($('#template-upload').html());
                $form.find('.upload').each(function(i, elem) {
                    var dropzone = new Dropzone(elem, {
                        url: "<?php echo base_url() ?>proveedor/escaparate/fotossad3",
                        method: 'POST',
                        paramName: 'files', // The name that will be used to transfer the file
                        maxFilesize: 1, // MB
                        uploadMultiple: false,
                        thumbnailWidth: null,
                        thumbnailHeight: null,
                        createImageThumbnails: true,
                        accept: function(file, done) {
                            $(elem).find('text').hide();
                            var t = $(elem).find('div.dz-preview').length;
                            if (t > 1) {
                                $(elem).find('div.dz-preview').each(function(i, value) {
                                    if (t - 1 != i) {
                                        value.remove();
                                    }
                                });
                            }
                            $(elem).parent().find('.label-error').addClass('hide');
                            setTimeout(function() {
                                $(elem).parent().find('.archivo').val($(file.previewElement).find('img').attr('src'));
                            }, 1000);
                            $(elem).find('.dz-preview').on('mouseenter', function() {
                                $(elem).find('.dz-details').css({opacity: '1'});
                                $(elem).find('img').css({
                                    'filter': 'blur(5)',
                                    '-webkit-filter': ' blur(5px)',
                                });
                            });
                            $(elem).find('.dz-preview').on('mouseleave', function() {
                                $(elem).find('.dz-details').css({opacity: '0'});
                                $(elem).find('img').css({
                                    'filter': 'blur(0)',
                                    '-webkit-filter': ' blur(0px)',
                                });
                            });
                        },
                        error: function(data) {
                            $('.eliminar-foto').trigger('click');
                            $(elem).parent().find('.archivo').val();
                            $(elem).parent().find('.label-error').removeClass('hide');
                        },
                        acceptedFiles: 'image/*',
                    });
                });
                $form.find('.upload text').on('click', function(e) {
                    $(e.currentTarget.parentNode).trigger('click');
                });
                $form.find('.eliminar-foto').on('click', function(elm) {
                    var $btn = $(elm.currentTarget);
                    var $prom = $btn.parent();
                    $prom.find('.upload div.dz-preview').remove();
                    $prom.find('text').show();
                    $prom.find('.archivo').val('');
                    $prom.parent().find('.label-error').addClass('hide');
                });
            }

            function init_tinymce_mini(elem) {
                if (typeof tinymce != 'undefined') {
                    tinymce.init({
                        selector: elem,
                        relative_urls: false,
                        menubar: '',
                        plugins: [
                            'advlist autolink lists link  preview anchor',
                            'searchreplace code ',
                            'insertdatetime paste ',
                        ],
                        statusbar: false,
                        height: 350,
                        imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58', 'clubnupcial.com'],
                        browser_spellcheck: true,
                        toolbar: 'insertfile undo redo  | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist ',
                    });
                }
            }
        </script>
    </body>
</html>
<?php

function value_post($nombre)
{
    if ($_POST && array_key_exists($nombre, $_POST)) {
        return $_POST[$nombre];
    }

    return "";
}

function icon_descriptor($nombre)
{
    switch (strtoupper($nombre)) {
        case "TIPO":
            break;
        case "CATEGORIA":
            break;
        case "TEMPORADA":
            break;
        case "ESTILO":
            break;
        case "CORTE":
            break;
        case "LARGO":
            break;
    }

    return "";
}

?>




