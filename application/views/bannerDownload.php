<style>
    .cel-img-download{
        display: block;
        margin: auto;
    }
    .info-app-download{
        margin: 8% 0;
    }
    .info-app-download p{
        font-family: Raleway;
        font-size: 12px;
        text-align: justify ;
    }
    .fondo_gradiente_downloadapp{
        background: rgba(158, 155, 158, 1);
        background: -moz-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(158, 155, 158, 1)), color-stop(70%, rgba(158, 155, 158, 1)), color-stop(100%, rgba(94, 91, 94, 1)));
        background: -webkit-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -o-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -ms-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: linear-gradient(to bottom, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#9e9b9e', endColorstr='#5e5b5e', GradientType=0);
        height: 455px;
        overflow: hidden;
    }

    @media only screen and (max-width: 425px) {

        .fondo_gradiente_downloadapp{
            height: 600px;
            padding-top: 10px;
        }
        .cel-img-download{
          width: 130px;
        }
        .info-app-download p{
            text-align: center;
        }
    }


    @media only screen and (max-width: 768px) and (min-width: 700px) {

        .fondo_gradiente_downloadapp{
            height: 400px;
            padding-top: 3%;
        }
        .cel-img-download{
            width: 195px;
        }


    }

</style>

<div class="row fondo_gradiente_downloadapp" style="margin-bottom: 20px">
    <div class="col s6 m10 l12">
        <div class="row " style="margin-bottom: 0px">
            <div class="col s12 m4 l3 offset-l3">
                <img class="imagen-responsive cel-img-download" src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png" alt="">
            </div>
            <div class="col s12 m8 l4 info-app-download">
                <p class="white-text" style="font-weight: bold;font-size: 22px;line-height: 25px"><b>¡INSPIRACIÓN PARA LLEVAR!</b></p>
                <p class="white-text" style="font-size: 14px;font-weight: 600">En el lugar y momento que quieras</pc>
                <p class="white-text" style="line-height: 15px;">Descarga japy para descubrir tu vestido perfecto de entre la variedad de diseños, puedes marcarlos para volver a verlos y mostrarlo a quien tú gustes.</p>
                <p class="white-text" style="font-weight: bold;font-size: 20px">DESCARGA LA APP</pc>
                <p><a href="https://itunes.apple.com/mx/app/japy/id1453432572"> <img style="width: 100px;" src="<?php echo base_url() ?>dist/img/planeador_boda/apple.png"></a><a href="https://play.google.com/store/apps/details?id=com.japybodas.app"> <img style="width: 100px;" src="<?php echo base_url() ?>dist/img/planeador_boda/google.png"></a></p>
            </div>
        </div>
    </div>
</div>

<div class="row fondo_gradiente_downloadapp" style="display: none;">
    <div class="col s6 m8 offset-s1 offset-m2">
        <div class="row " style="margin-bottom: 0px">
            <div class="col s12 m3 offset-m3">
                <img class="imagen-responsive cel-img-download" src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png" alt="">
            </div>
            <div class="col s12 m4 info-app-download">
                <p class="white-text" style="font-weight: bold;font-size: 22px;line-height: 25px"><b>&iexcl;CREA TU INSTA-&Aacute;LBUM DEL EVENTO!</b></p>
                <p class="white-text" style="font-size: 14px;font-weight: 600">En el lugar y momento que quieras</p>
                <p class="white-text" style="line-height: 15px;">Tus invitados la pueden hacer de fot&oacute;grafos profesionales y tomar fotos a lo largo del
                                                                 evento, posteriormente subirlas a la aplicaci&oacute;n de &quot;wedshoots&quot; y &iexcl;listo!.</p>
                <p class="white-text" style="font-weight: bold;font-size: 20px">DESCARGA LA APP</p>
                <p><a href="https://play.google.com/store/apps/details?id=com.japybodas.app"> <img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png"></a>
            </div>
        </div>
    </div>
</div>
