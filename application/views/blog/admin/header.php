<html lang="es">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Japy</title>
    <link href="<?php echo $this->config->base_url() ?>dist/img/clubnupcial.css" rel="icon" sizes="16x16">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo $this->config->base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->config->base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo $this->config->base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->config->base_url() ?>dist/css/blog.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $this->config->base_url() ?>dist/tinymce/skins/lightgray/skin.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo $this->config->base_url() ?>dist/tinymce/skins/lightgray/content.inline.min.css"
          rel="stylesheet" type="text/css"/>
    <script src="<?php echo $this->config->base_url() ?>dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->config->base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
</head>
<body>
<?php if ($this->checker->isLogin() && $this->checker->isBlogger()) { ?>
    <div class="menu-admin-blog" style="z-index: 99999;">
        <div class="row">
            <ul id="dropdown1" class="dropdown-content">
                <?php if ($this->checker->getIdRol() == Checker::$ADMIN) { ?>
                    <li><a href="<?php echo $this->config->base_url() ?>blog/usuario">Usuarios </a></li>
                <?php } ?>
                <li><a href="<?php echo $this->config->base_url() ?>blog">Ver Blog</a></li>
                <?php if ($this->checker->getIdRol() == Checker::$ADMIN) { ?>
                    <li><a href="<?php echo $this->config->base_url() ?>app">Regresarl al panel principal</a>
                    </li>
                <?php } ?>
                <?php if ($this->checker->getIdRol() == Checker::$REDACTOR) { ?>
                    <li><a href="<?php echo $this->config->base_url() ?>cuenta/logout">Cerrar sesi&oacute;n</a>
                    </li>
                <?php } ?>
            </ul>
            <nav style="background: black; z-index: 99999" class="nav-fixed">
                <div class="nav-wrapper">
                    <div class="brand-logo hide-on-med-and-down" style="font-size: 16px;">
                        Administrador : <?php echo $this->checker->getUserName() ?>
                    </div>
                    <ul class="right">
                        <li><a href="<?php echo $this->config->base_url() ?>blog/post/nuevo"><i
                                        class="material-icons left">add</i><span
                                        class="hide-on-med-and-down">Redactar</span> </a></li>
                        <li><a href="<?php echo $this->config->base_url() ?>blog/post"><i
                                        class="material-icons left">list</i><span
                                        class="hide-on-med-and-down">Entradas </span></a></li>
                        <li><a href="<?php echo $this->config->base_url() ?>blog/comentario"><i
                                        class="material-icons left">mode_comment</i><span class="hide-on-med-and-down">Comentarios</span><span
                                        class="new badge"><?php echo $this->checker->getMensajesNuevo() ?></span</a>
                        </li>
                        <li><a class="dropdown-button" data-activates="dropdown1" data-constrainwidth="true"
                               data-beloworigin="true" data-alignment="bottom" href="#"><i class="material-icons left">more_vert</i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div style="height: 44px">
            &nbsp;
        </div>
    </div>
<?php } ?>
<?php
$mensaje      = isset($mensaje) ? $mensaje : $this->session->userdata('mensaje');
$tipo_mensaje = isset($tipo_mensaje) ? $tipo_mensaje : $this->session->userdata('tipo_mensaje');
$this->session->unset_userdata('mensaje');
$this->session->unset_userdata('tipo_mensaje');
if (isset($mensaje)) {
?>
<div class="row">
    <div class="col s12">
        <div class="card-panel white-text <?php echo $tipo_mensaje == "success" ? "teal" : "orange" ?>">
            <?php
            if (is_array($mensaje)) {
                echo "<ul>";
                foreach ($mensaje as $key => $value) {
                    echo "<li>$value</li>";
                }
                echo "</ul>";
            } else {
                echo $mensaje;
            }
            ?>
        </div>
    </div>
</div>
<?php } ?>