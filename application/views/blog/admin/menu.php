<?php if ($this->checker->isLogin() && $this->checker->isBlogger()) { ?>
    <div class="menu-admin-blog" style="z-index: 99999;">
        <div class="row">
            <nav style="background: black; z-index: 99999" class="nav-fixed">
                <div class="nav-wrapper">
                    <div class="brand-logo hide-on-med-and-down" style="font-size: 16px;">
                        <a onclick="history.back()" class="btn-flat white-text waves-effect waves-light"
                           style="padding: 0px 20px;margin: 0px;height: initial;">
                            <i class="material-icons">arrow_back</i>
                        </a>
                        Administrador : <?php echo $this->checker->getUserName() ?>
                    </div>
                    <ul class="right">
                        <li><a href="<?php echo $this->config->base_url() ?>blog/post/nuevo"> <i
                                        class="material-icons left">add</i> <span
                                        class="hide-on-med-and-down">Redactar</span> </a></li>
                        <li><a href="<?php echo $this->config->base_url() ?>blog/post"> <i
                                        class="material-icons left">list</i> <span
                                        class="hide-on-med-and-down">Entradas </span></a></li>
                        <li><a class="dropdown-button" data-activates="dropdown-menu-blog" data-constrainwidth="false"
                               data-beloworigin="true" data-alignment="bottom" href="#"><i class="material-icons left">more_vert</i></a>
                        </li>
                    </ul>
                </div>
            </nav>
            <ul id="dropdown-menu-blog" class="dropdown-content">
                <li><a class="black-text" href="<?php echo $this->config->base_url() ?>blog">Ver Blog</a></li>
                <?php if ($this->checker->getIdRol() == Checker::$ADMIN) { ?>
                    <li><a class="black-text" href="<?php echo $this->config->base_url() ?>App">Regresar al
                            panel principal</a></li>
                <?php } ?>
                <?php if ($this->checker->getIdRol() == Checker::$REDACTOR) { ?>
                    <li><a class="black-text" href="<?php echo $this->config->base_url() ?>cuenta/logout">Cerrar
                            sesi&oacute;n</a></li>
                <?php } ?>
            </ul>
        </div>
        <div style="height: 44px">
            &nbsp;
        </div>
    </div>
<?php } ?>