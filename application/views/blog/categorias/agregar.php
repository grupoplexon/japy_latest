<!DOCTYPE html>
<html>
    <head>
        <?php $this->view("blog/header") ?>
    </head>    
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <div class="" style="padding: 20px 40px 40px 40px;">
            <div class="row">
                <div class="col m12">
                    <h3>
                        Crear categoria en <?php echo $grupo->nombre ?>
                    </h3>
                    <?php $this->view('blog/mensajes') ?>
                    <div class="card-panel ">
                        <div class="row">
                            <form class="col m12 s12" method="POST" action="<?php echo $this->config->base_url() ?>blog/categorias/agregar/<?php echo $grupo->id_blog_grupo ?>">
                                <div class="row">
                                    <div class="col s12"  data-focus="true">
                                        <div class="input-field col s12">
                                            <input id="nombre" name="nombre" type="text" required="true" class="validate">
                                            <label for="nombre">Nombre del grupo</label>
                                        </div>
                                    </div>
                                    <div class="col s12"  data-focus="true">
                                        <div class="input-field col s12">
                                            <textarea name="descripcion" required="true" class="materialize-textarea" maxlength="255"></textarea>
                                            <label for="nombre">Descripción</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col m12">
                                    <button class="btn pull-right dorado-2" type="submit" >
                                        Guardar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("blog/footer") ?>
    </body>
</html>