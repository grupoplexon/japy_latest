<!DOCTYPE html>
<html>
    <head>
        <title>Categorias <?php echo $grupo->nombre ?> </title>
        <?php $this->view('blog/header') ?>
    </head>
    <body>
        <?php $this->view('blog/admin/menu') ?>
        <div class=" container-body" style="padding: 0px 40px;">
            <div class="row">
                <h4 class="col s8">
                    Categorias: <?php echo $grupo->nombre ?> 
                </h4>
                <div class="col s4">
                    <div class="">
                        <a  href="<?php echo $this->config->base_url() ?>blog/categorias/agregar/<?php echo $grupo->id_blog_grupo ?>" class="btn pull-right" style="margin-top: 15px;"><i class="material-icons left">add</i> Agregar</a>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <?php $this->view('blog/mensajes') ?>
            <div class="row">
                <div class="col s12 m12">
                    <div class="card ">
                        <div class="card-content">
                            <div style="    padding: 10px;">
                                <?php if ($categorias > 0) { ?>
                                    <table class="striped"> 
                                        <thead>
                                            <tr>
                                                <th>
                                                    NOMBRE
                                                </th>
                                                <th>
                                                    DESCRIPCION
                                                </th>
                                                <th>
                                                    No. ENTRADAS
                                                </th>
                                                <th>
                                                    OPCIONES
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($categorias as $categoria) { ?>
                                                <tr>
                                                    <td><b><?php echo $categoria->nombre ?></b></td>
                                                    <td><?php echo $categoria->descripcion ?></td>
                                                    <td><?php echo $categoria->total ?></td>
                                                    <td>
                                                        <a href="<?php echo $this->config->base_url() ?>blog/categorias/editar/<?php echo $categoria->id_blog_categoria ?>" class="dorado-2-text tooltipped"
                                                           data-position="top" data-delay="50" data-tooltip="Editar">
                                                            <i class="material-icons">mode_edit</i> 
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                <?php } else { ?>
                                    <div class="alert  center">
                                        <i class="material-icons fa-5x">info_outline</i>
                                        <p>
                                            No tienes Categorias dentro del este grupo.
                                        </p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("blog/footer") ?>
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function () {

            });
        </script>
    </body>
</html>
