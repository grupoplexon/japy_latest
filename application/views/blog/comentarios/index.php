<html>
    <head>
        <?php $this->view("blog/header") ?>
    </head>
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <div class="" style="padding: 0px 40px;">
            <h4>
                Entradas: 
            </h4>
            <div class="row">
                <?php $this->view("blog/mensajes") ?>
                <div class="col s12 m12">
                    <div class="card ">
                        <div class="card-content">
                            <div style="    padding: 10px;">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>
                                                TITULO
                                            </th>
                                            <th>
                                                CREADO EL
                                            </th>
                                            <th>
                                                AUTOR
                                            </th>
                                            <th>
                                                NUEVOS
                                            </th>
                                            <th>
                                                VER
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($posts as $key => $post) { ?>
                                            <tr>
                                                <td>
                                                    <?php echo $post->titulo ?>
                                                </td>
                                                <td>
                                                    <?php echo dateFormat($post->fecha_creacion) ?>
                                                </td>
                                                <td>
                                                    <?php echo $post->autor->nombre ?> <?php echo $post->autor->apellido ?>
                                                </td>
                                                <td>
                                                    <?php echo $post->comentarios ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo $this->config->base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>">
                                                        <i class="material-icons">chevron_right</i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <ul class="pagination">
                                    <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                    <?php for ($i = 1; $i <= $total_paginas; $i++) { ?>
                                        <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo $this->config->base_url() ?>blog/post?pag=<?php echo $i ?>"><?php echo $i ?></a></li>
                                    <?php } ?>
                                    <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("blog/footer") ?>
    </body>
</html>
