<script src="<?php echo $this->config->base_url() ?>/dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>/dist/js/materialize.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>/dist/js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>/dist/js/classie.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>/dist/js/slider.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->base_url() ?>dist/js/facebook.js" type="text/javascript"></script>
<script>
    var $window = $(window);
    $(document).ready(function () {
        $(".nav-overlay-top").hide();
        var nav = $('.title.pink');
        var logo = $('.logo_header');
        if (nav.position()) {
            var top = nav.position().top;
            $window.scroll(function () {
                if ($window.scrollTop() >= top) {
                    $(".nav-overlay-top").show();
                    nav.addClass("nav-fixed");
                    logo.addClass("nav-fixed");
                } else {
                    $(".nav-overlay-top").hide();
                    nav.removeClass("nav-fixed");
                    logo.removeClass("nav-fixed");
                }
            });
        }
        $('select').not(".tag-selector").material_select();
    });
</script>