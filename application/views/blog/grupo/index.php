<!DOCTYPE html>
<html>
<head>
    <title>Temas</title>
    <?php $this->view('blog/header') ?>
</head>
<body>
<?php $this->view('blog/admin/menu') ?>
<div class=" container-body" style="padding: 0px 40px;">
    <div class="row">
        <h4 class="col s8">
            Temas:
        </h4>
        <div class="col s4">
            <div class="">
                <a href="<?php echo $this->config->base_url() ?>blog/categorias/grupo_agregar" class="btn pull-right"
                   style="margin-top: 15px;"><i class="material-icons left">add</i> Agregar</a>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <?php $this->view('blog/mensajes') ?>
    <div class="card ">
        <div class="card-content">
            <i class="fa fa-info"></i> Crear grupos de categorias, y agrega en ellos categorias para organizar tu blog,
            cada entrada puede ser asignada a más de una categoria.
        </div>
    </div>
    <div class="row">
        <?php foreach ($grupo as $categoria) { ?>
            <div class="col s12 m6 l4">
                <div class="card hoverable ">
                    <div class="card-content">
                        <b><?php echo $categoria->nombre ?></b>
                        <small><?php echo $categoria->total ?></small>
                        <a href="<?php echo $this->config->base_url() ?>blog/categorias/grupo/<?php echo $categoria->id_blog_grupo ?>"
                           class="tooltipped pull-right"
                           data-position="top" data-delay="50" data-tooltip="Categorias">
                            <i class="material-icons">add</i>
                        </a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php $this->view("blog/footer") ?>
<script>
    var server = "<?php echo base_url() ?>";
    $(document).ready(function () {

    });
</script>
</body>
</html>
