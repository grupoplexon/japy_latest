<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="theme-color" content="#f5e6df">
<meta name="msapplication-navbutton-color" content="#f5e6df">
<meta name="apple-mobile-web-app-status-bar-style" content="#f5e6df">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
<?php if (isset($titulo)) { ?>
    <title><?php echo strip_tags($titulo) ?></title>
<?php } else { ?>
    <title>Japy</title>
<?php } ?>
<link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
<link href="<?php echo $this->config->base_url() ?>dist/img/clubnupcial.css" rel="icon" sizes="16x16">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo $this->config->base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->config->base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet"
      type="text/css"/>
<link href="<?php echo $this->config->base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->config->base_url() ?>dist/css/blog.css" rel="stylesheet" type="text/css"/>
<?php
setlocale(LC_MONETARY, "en_US");
setlocale(LC_TIME, "es_ES.UTF-8");
?>
<script type='text/javascript' data-cfasync='false'>window.purechatApi = {
        l: [], t: [], on: function() {
            this.l.push(arguments);
        },
    };
    (function() {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function(e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({c: 'f3f97b28-73ac-4929-a04d-527e409169b7', f: true});
                done = true;
            }
        };
    })();
</script>

<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement('script');
        mf.type = 'text/javascript';
        mf.async = true;
        mf.src = '//cdn.mouseflow.com/projects/4315bf06-815c-4f86-b787-cb87d939a11e.js';
        document.getElementsByTagName('head')[0].appendChild(mf);
    })();
</script>
