<?php
$url         = $this->config->base_url()."blog/home/index";
$url_img     = $this->config->base_url()."blog/resource/imagen/";
$url_default = $this->config->base_url()."/dist/img/blog/default.png";
?>
<html>
    <head>
        <?php $this->view("principal/newHeader");
        $this->view("general/newheader"); ?>

        <style>

            @media only screen and (max-width: 321px) {
                .img-blog {
                    height: 30% !important;
                }

                .p-title {
                    font-size: 24px !important;
                    line-height: 25px !important;
                }

                .p-content {
                    max-height: 210px !important;
                    overflow: hidden !important;
                    text-align: justify;
                }

                .p-autor {
                    line-height: 18px !important;
                }

                .info-blog {
                    height: 99%;
                }

                .card-blog {
                    height: 600px;
                }

                .blog-last {
                    text-align: center;
                }
            }

            @media only screen and (max-width: 425px) and (min-width: 322px) {
                .img-blog {
                    height: 30% !important;
                }

                .card-title {
                    line-height: 30px !important;
                }

                .p-title {
                    font-size: 24px !important;
                }

                .p-autor {
                    line-height: 20px !important;
                }

                .card-blog {
                    height: 580px;
                }

                .info-blog {
                    height: 95%;
                }

                .p-content {
                    max-height: 201px !important;
                    overflow: hidden !important;
                }
            }
        </style>

    </head>
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <div class="max-content">
            <div class="row">
                <div class="col m8 l9">
                    <div id="blogs " class="blogs">

                    <!-- STATIC BLOGS -->
                    <div class="card hoverable card-blog-lg hide-on-med-and-down">
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col m4 s12">
                                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>tendencia-oscars">
                                                <p class="img-blog img-destacada"
                                                style="background-image: url('<?php echo base_url() ?>dist/img/static_blogs/oscar/BrieLarsonVestido.jpg');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;
                                                cursor: pointer">
                                                </p>
                                            </a>
                                    </div>
                                    <div class="col m8 s12" style="height: 350px">
                                        <div class="row info-blog">
                                            <div>
                                                <a class="card-title p-title"
                                                href="<?php echo base_url() ?>tendencia-oscars">
                                                Los Metálicos ¡La tendencia que marcaron los Óscar en 2019! 
                                                </a>
                                            </div>
                                            <div class="tag-slide">
                                                    
                                            </div>
                                            <p class="por" style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador<br>
                                            </p>
                                            <p class="p-content">¿Aún No sabes qué vestido ponerte para la boda de tu amiga? Rescatamos la tendencia de las 
                                                actrices que acudieron a los premios Óscar y los vestidos más vanguardistas para esta temporada...<br></p>

                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social icon-comment comments ">
                                                0 <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social icon-fav comments "> 0 <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div style="float: right;   margin-top: -5px;">
                                                <a class="isBlogUrl" href="<?php echo base_url() ?>tendencia-oscars"
                                                >Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="card hoverable card-blog-lg hide-on-med-and-down">
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col m4 s12">
                                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>bride-w">
                                                <p class="img-blog img-destacada"
                                                style="background-image: url('<?php echo base_url() ?>dist/img/static_blogs/staticblogbw.png');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;
                                                cursor: pointer">
                                                </p>
                                            </a>
                                    </div>
                                    <div class="col m8 s12" style="height: 350px">
                                        <div class="row info-blog">
                                            <div>
                                                <a class="card-title p-title"
                                                href="<?php echo base_url() ?>bride-w">
                                                Planea tu boda en Bride Weekend 
                                                </a>
                                            </div>
                                            <div class="tag-slide">
                                                    
                                            </div>
                                            <p class="por" style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador<br>
                                            </p>
                                            <p class="p-content">Si no sabes por dónde empezar con los preparativos para tu boda, llega para ti Bride Weekend, un concepto vanguardista que más que ser una expo, es un evento interactivo y divertido donde podrás...<br></p>

                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social icon-comment comments ">
                                                0 <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social icon-fav comments "> 0 <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div style="float: right;   margin-top: -5px;">
                                                <a class="isBlogUrl" href="<?php echo base_url() ?>bride-w"
                                                >Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="card hoverable card-blog-lg hide-on-med-and-down">
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col m4 s12">
                                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>pablo_saracho">
                                                <p class="img-blog img-destacada"
                                                style="background-image: url('<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PSboda4.jpg');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;
                                                cursor: pointer">
                                                </p>
                                            </a>
                                    </div>
                                    <div class="col m8 s12" style="height: 350px">
                                        <div class="row info-blog">
                                            <div>
                                                <a class="card-title p-title"
                                                href="<?php echo base_url() ?>pablo_saracho">
                                                Cinco Minutos con Pablo Saracho
                                                </a>
                                            </div>
                                            <div class="tag-slide">
                                                    
                                            </div>
                                            <p class="por" style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador<br>
                                            </p>
                                            <p class="p-content">Descubre cómo ha sido el camino de Pablo Saracho en la fotografía de boda,
                                            desde sus inicios hasta la actualidad, lo que lo motiva a hacer click y lo que le
                                            inspira en sus Storytellings...<br></p>

                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social icon-comment comments ">
                                                0 <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social icon-fav comments "> 0 <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div style="float: right;   margin-top: -5px;">
                                                <a class="isBlogUrl" href="<?php echo base_url() ?>pablo_saracho"
                                                >Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="card hoverable card-blog-lg hide-on-med-and-down">
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col m4 s12">
                                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>manuel_diaz">
                                                <p class="img-blog img-destacada"
                                                style="background-image: url('<?php echo base_url() ?>dist/img/blog/Manuel01.jpg');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;
                                                cursor: pointer">
                                                </p>
                                            </a>
                                </div>
                                <div class="col m8 s12" style="height: 350px">
                                    <div class="row info-blog">
                                        <div>
                                            <a class="card-title p-title"
                                            href="<?php echo base_url() ?>manuel_diaz">
                                            Cinco Minutos con Manuel Díaz
                                            </a>
                                        </div>
                                        <div class="tag-slide">
                                        </div>
                                        <p class="por" style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                            Por Administrador<br>
                                        </p>
                                        <p class="p-content">Manuel Díaz, fundador y director de Grupo Plexon comienza una aventura más en su camino, 
                                        creando un nuevo e innovador concepto llamado Bride Weekend en el que busca reinventar la industria nupcial en México...<br></p>

                                    </div>
                                    <div class="row">
                                        <div class=" container-icons-social "
                                                style=" float:left;   margin-top: -5px;">
                                            <p class="row">
                                        <span class="icon-social icon-comment comments ">
                                            0 <i
                                                    class="fa fa-comment"></i></span>
                                                <span class="icon-social icon-fav comments "> 0 <i
                                                            class="fa fa-heart"></i></span>
                                            </p>
                                        </div>
                                        <div style="float: right;   margin-top: -5px;">
                                            <a class="isBlogUrl" href="<?php echo base_url() ?>manuel_diaz"
                                            >Leer M&aacute;s</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="card hoverable card-blog-lg hide-on-med-and-down ">
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col m4 s12">
                                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>victoria_samano">
                                                <p class="img-blog img-destacada"
                                                style="background-image: url('<?php echo base_url() ?>dist/img/static_blogs/victoria2-responsive.png');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;
                                                cursor: pointer">
                                                </p>
                                            </a>
                                    </div>
                                    <div class="col m8 s12" style="height: 350px">
                                        <div class="row info-blog">
                                            <div>
                                                <a class="card-title p-title"
                                                href="<?php echo base_url() ?>victoria_samano">
                                                Cinco Minutos con Victoria Sámano. Novias de España
                                                </a>
                                            </div>
                                            <div class="tag-slide">
                                                    
                                            </div>
                                            <p class="por" style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador<br>
                                            </p>
                                            <p class="p-content">El vestido de novia, es la prenda más importante que vas a vestir en tu vida, y para que sepas
                                            más sobre el proceso creativo para elegir tu vestido, entrevistamos a Victoria Sámano de
                                            Novias de España....<br></p>

                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social icon-comment comments ">
                                                0 <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social icon-fav comments "> 0 <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div style="float: right;   margin-top: -5px;">
                                                <a class="isBlogUrl" href="<?php echo base_url() ?>victoria-samano"
                                                >Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        <!-- Blogs versión movil -->

                        <div class="card card-blog hoverable hide-on-large-only">
                            <div class="card-image">
                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>tendencia-oscars">
                                <div class="img-blog img-destacada"
                                        style="background-image: url('<?php echo base_url() ?>dist/img/static_blogs/oscar/BrieLarsonVestido.jpg');
                                                background-repeat: no-repeat;
                                                background-position: top;
                                                background-size:cover;
                                                height: 350px;">

                                </div>
                                </a>
                            </div>
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col s12 m12" style="height: 350px">
                                        <div class="row info-blog">
                                            <span class="card-title p-title" style="margin: 0">Los Metálicos ¡La tendencia que marcaron los Óscar en 2019!</span>
                                            <p class="p-autor"
                                                style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador
                                                <br>
                                            </p>
                                            <div class="p-content">¿Aún No sabes qué vestido ponerte para la boda de tu amiga? Rescatamos la tendencia de las 
                                                actrices que acudieron a los premios Óscar y los vestidos más vanguardistas para esta... <br></div>
                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social comments "> 0 
                                                <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social comments "> 0  <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div class="isBlogUrl" style="float: right;   margin-top: -5px;">
                                                <a href="<?php echo base_url() ?>tendencia-oscars">Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card card-blog hoverable hide-on-large-only">
                            <div class="card-image">
                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>bride-w">
                                <div class="img-blog img-destacada"
                                        style="background-image: url('<?php echo base_url() ?>dist/img/static_blogs/staticblogbw.png');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;">

                                </div>
                                </a>
                            </div>
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col s12 m12" style="height: 350px">
                                        <div class="row info-blog">
                                            <span class="card-title p-title" style="margin: 0">Planea tu boda en Bride Weekend</span>
                                            <p class="p-autor"
                                                style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador
                                                <br>
                                            </p>
                                            <div class="p-content">Si no sabes por dónde empezar con los preparativos para tu boda, llega para ti Bride Weekend, un concepto vanguardista que más que ser una expo, es un evento interactivo y divertido donde podrás...<br></div>
                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social comments "> 0 
                                                <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social comments "> 0  <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div class="isBlogUrl" style="float: right;   margin-top: -5px;">
                                                <a href="<?php echo base_url() ?>bride-w">Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card card-blog hoverable hide-on-large-only">
                            <div class="card-image">
                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>pablo_saracho">
                                <div class="img-blog img-destacada"
                                        style="background-image: url('<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PSboda4.jpg');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;">

                                </div>
                                </a>
                            </div>
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col s12 m12" style="height: 350px">
                                        <div class="row info-blog">
                                            <span class="card-title p-title" style="margin: 0">Cinco Minutos con Pablo Saracho</span>
                                            <p class="p-autor"
                                                style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador
                                                <br>
                                            </p>
                                            <div class="p-content">Descubre cómo ha sido el camino de Pablo Saracho en la fotografía de boda,
                                    desde sus inicios hasta la actualidad, lo que lo motiva a hacer click y lo que le
                                    inspira en sus Storytellings...<br></div>
                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social comments "> 0 
                                                <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social comments "> 0  <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div class="isBlogUrl" style="float: right;   margin-top: -5px;">
                                                <a href="<?php echo base_url() ?>pablo_saracho">Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-blog hoverable hide-on-large-only">
                            <div class="card-image">
                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>manuel_diaz">
                                <div class="img-blog img-destacada"
                                        style="background-image: url('<?php echo base_url() ?>dist/img/blog/Manuel01.jpg');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;">

                                </div>
                                </a>
                            </div>
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col s12 m12" style="height: 350px">
                                        <div class="row info-blog">
                                            <span class="card-title p-title" style="margin: 0">Cinco Minutos con Manuel Díaz</span>
                                            <p class="p-autor"
                                                style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador
                                                <br>
                                            </p>
                                            <div class="p-content">Manuel Díaz, fundador y director de Grupo Plexon comienza una aventura más en su camino, 
                                        creando un nuevo e innovador concepto llamado Bride Weekend en el que busca reinventar la industria nupcial en México...<br></div>
                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social comments "> 0 
                                                <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social comments "> 0  <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div class="isBlogUrl" style="float: right;   margin-top: -5px;">
                                                <a href="<?php echo base_url() ?>manuel_diaz">Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-blog hoverable hide-on-large-only">
                            <div class="card-image">
                            <a class="img-nuevo"
                                            href="<?php echo base_url() ?>victoria_samano">
                                <div class="img-blog img-destacada"
                                        style="background-image: url('<?php echo base_url() ?>dist/img/static_blogs/victoria2-responsive.png');
                                                background-repeat: no-repeat;
                                                background-position: center;
                                                background-size:cover;
                                                height: 350px;">

                                </div>
                                </a>
                            </div>
                            <div class="card-content">
                                <div class="row" style="margin-bottom: 2px">
                                    <div class="col s12 m12" style="height: 350px">
                                        <div class="row info-blog">
                                            <span class="card-title p-title" style="margin: 0">Cinco Minutos con Victoria Sámano. Novias de España</span>
                                            <p class="p-autor"
                                                style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                Por Administrador
                                                <br>
                                            </p>
                                            <div class="p-content">El vestido de novia, es la prenda más importante que vas a vestir en tu vida, y para que sepas
                                            más sobre el proceso creativo para elegir tu vestido, entrevistamos a Victoria Sámano de
                                            Novias de España....<br></div>
                                        </div>
                                        <div class="row">
                                            <div class=" container-icons-social "
                                                    style=" float:left;   margin-top: -5px;">
                                                <p class="row">
                                            <span class="icon-social comments "> 0 
                                                <i
                                                        class="fa fa-comment"></i></span>
                                                    <span class="icon-social comments "> 0  <i
                                                                class="fa fa-heart"></i></span>
                                                </p>
                                            </div>
                                            <div class="isBlogUrl" style="float: right;   margin-top: -5px;">
                                                <a href="<?php echo base_url() ?>victoria_samano">Leer M&aacute;s</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- END STATIC BLOGS -->
                        <?php if (isset($posts)) {
                            // dd($posts);
                            foreach ($posts as $post) {
                                ?>
                                <div class="card hoverable card-blog-lg hide-on-med-and-down">
                                    <div class="card-content">
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col m4 s12">
                                                <?php if ($post->id_imagen_destacada) : ?>
                                                    <a class="img-nuevo"
                                                    href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>">
                                                        <p class="img-blog img-destacada"
                                                        style="background-image: url('<?php echo $url_img.$post->id_imagen_destacada ?>');
                                                        background-repeat: no-repeat;
                                                        background-position: center;
                                                        background-size:cover;
                                                        height: 350px;
                                                        cursor: pointer">
                                                        </p>
                                                    </a>
                                                <?php else : ?>
                                                    <div class="img-blog"
                                                         style="background-image: url('<?php echo $url_default ?>');
                                                                 background-repeat: no-repeat;
                                                                 background-position: center;
                                                                 background-size:cover;
                                                                 height: 350px;">
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col m8 s12" style="height: 350px">
                                                <div class="row info-blog">
                                                    <!--<span class="card-title p-title"><?php //echo strip_tags($post->titulo) ?></span>-->
                                                    <div>
                                                        <a class="card-title p-title"
                                                        href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>">
                                                            <?php echo strip_tags($post->titulo) ?>
                                                        </a>
                                                    </div>
                                                    <div class="tag-slide">
                                                        <?php foreach ($post->tags->tags as $t) { ?>
                                                            <div class="chip tagElement">
                                                                <?php echo $t->title ?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <p class="por" style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                        Por <?php echo $post->autor->nombre ?> <?php echo $post->autor->apellido ?> <?php echo relativeTimeFormat($post->fecha_creacion) ?>
                                                        <br>
                                                    </p>
                                                    <p class="p-content"><?php echo substr(strip_tags($post->contenido), 0, 450) ?> ...<br></p>

                                                </div>
                                                <div class="row">
                                                    <div class=" container-icons-social "
                                                         style=" float:left;   margin-top: -5px;">
                                                        <p class="row">
                                                    <span class="icon-social icon-comment comments "><?php echo $post->comentarios ?>
                                                        <i
                                                                class="fa fa-comment"></i></span>
                                                            <span class="icon-social icon-fav comments "><?php echo $post->fav ?> <i
                                                                        class="fa fa-heart"></i></span>
                                                        </p>
                                                    </div>
                                                    <div style="float: right;   margin-top: -5px;">
                                                        <a class="isBlogUrl" href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>"
                                                        >Leer M&aacute;s</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-blog hoverable hide-on-large-only">
                                    <div class="card-image">
                                        <?php if ($post->id_imagen_destacada) : ?>
                                            <div class="img-blog img-destacada"
                                                 style="background-image: url('<?php echo $url_img.$post->id_imagen_destacada ?>');
                                                         background-repeat: no-repeat;
                                                         background-position: center;
                                                         background-size:cover;
                                                         height: 350px;">
                                            </div>
                                        <?php else : ?>
                                            <div class="img-blog" style="background-image: url('<?php echo $url_default ?>');
                                                    background-repeat: no-repeat;
                                                    background-position: center;
                                                    background-size:cover;
                                                    height: 350px;">
                                            </div>
                                        <?php endif; ?>

                                    </div>
                                    <div class="card-content">
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col s12 m12" style="height: 350px">
                                                <div class="row info-blog">
                                                    <span class="card-title p-title" style="margin: 0"><?php echo strip_tags($post->titulo) ?></span>
                                                    <p class="p-autor"
                                                       style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                                                        Por <?php echo $post->autor->nombre ?> <?php echo $post->autor->apellido ?> <?php echo relativeTimeFormat($post->fecha_creacion) ?>
                                                        <br>
                                                    </p>
                                                    <div class="p-content"><?php echo substr(strip_tags($post->contenido), 0, 450) ?> ...<br></div>
                                                </div>
                                                <div class="row">
                                                    <div class=" container-icons-social "
                                                         style=" float:left;   margin-top: -5px;">
                                                        <p class="row">
                                                    <span class="icon-social comments "><?php echo $post->comentarios ?>
                                                        <i
                                                                class="fa fa-comment"></i></span>
                                                            <span class="icon-social comments "><?php echo $post->fav ?> <i
                                                                        class="fa fa-heart"></i></span>
                                                        </p>
                                                    </div>
                                                    <div class="isBlogUrl" style="float: right;   margin-top: -5px;">
                                                        <a href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>"
                                                        >Leer M&aacute;s</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <!-- <ul class="pagination">
                        <li class="<?php echo($page == 1 ? "disabled" : "waves-effect") ?>"><a
                                    href="<?php echo "$url/$cat/".($page - 1) ?>"><i
                                        class="material-icons">chevron_left</i></a>
                        </li>
                        <?php for ($i = 1; $i <= $total_paginas; $i++) { ?>
                            <li class="<?php echo($i == $page ? "active" : "waves-effect") ?>"><a
                                        href="<?php echo "$url/$cat/$i" ?>"><?php echo $i ?></a></li>
                        <?php } ?>
                        <li class="<?php echo($page == $total_paginas ? "disabled" : "waves-effect") ?>">
                            <a href="<?php echo "$url/$cat/".($page + 1) ?>">
                                <i class="material-icons">chevron_right</i>
                            </a>
                        </li>
                    </ul> -->
                            <div class="isContainer" style="text-align: center;">
                                <span class="onLoad">Cargando...</span>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="card-panel teal">
                                    <span class="white-text">A&uacute;n no hay publicaciones en esta categor&iacute;a intenta m&aacute;s tarde
                                    </span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
                <div class="col m4 l3 hide-on-small-only">
                    <?php $this->view("blog/posts/sidebar") ?>
                </div>
            </div>
            <script src="<?php echo $this->config->base_url() ?>/dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
            <!-- <script src="<?php //echo $this->config->base_url() ?>/dist/js/yofinity.min.js" type="text/javascript"></script> -->

        </div>
        <script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>
        <script>

            var page = 1;
            var totPages = parseInt("<?php echo $total_paginas ?>");
            var urlD = "<?php echo "$url/$cat/"?>";
            var blogUrl = "<?php echo base_url() ?>" + 'blog/post/ver/';
            var url_img = "<?php echo $url_img ?>";
            var url_default = "<?php echo $url_default ?>";
            var detected = false;

            $(window).scroll(function() {
                var top_of_element = $('.onLoad').offset().top;
                var bottom_of_element = $('.onLoad').offset().top + $('.onLoad').outerHeight();
                var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) && (!detected)) {
                    detected = true;
                    this.addMore();
                }
                else {
                    // console.log("El elemento no esta visible");
                }
            });

            function addMore() {
                page += 1;

                if (page <= totPages) {
                    $.when($.ajax({
                        url: urlD + page,
                        headers: {'Accept': 'application/json'},
                        success: function(response) {
                            console.log('ok');
                        },
                    })).done(function(data) {
                        detected = false;
                        $.each(data, function(i, dat) {
                            var content = dat.contenido.replace(/(<([^>]+)>)/ig, '');
                            var newLGCard = $('.card-blog-lg:last').clone(true);
                            var newCard = $('.card-blog:last').clone(true);
                            var imgToLoad = url_default;
                            //Estilos aplicados para cambiar la imagen
                            if (dat.id_imagen_destacada) {
                                imgToLoad = url_img + dat.id_imagen_destacada;
                            }
                            var styles = {
                                'background-image': 'url(\'' + imgToLoad + '\')',
                                'background-repeat': 'no-repeat',
                                'background-position': 'center',
                                'background-size': 'cover',
                                'height': '350px',
                            };

                            if (dat.tags.tags) {
                                newLGCard.find('div.tag-slide').html($.each(dat.tags.tags, function(i, tag) {
                                    '<div class="chip tagElement">' + tag.title + '</div>';
                                }));
                                newCard.find('div.tag-slide').html($.each(dat.tags.tags, function(i, tag) {
                                    '<div class="chip tagElement">' + tag.title + '</div>';
                                }));
                            }
                            else {
                                newLGCard.find('div.tag-slide').html('');
                                newCard.find('div.tag-slide').html('');
                            }

                            //Cargado de Tarjetas(Card) pantallas grandes
                            newLGCard.find('a.card-title').html(dat.titulo.replace(/(<([^>]+)>)/ig, ''));
                            newLGCard.find('a.card-title').attr('href', blogUrl + dat.id_blog_post);
                            newLGCard.find('a.img-nuevo').attr('href', blogUrl + dat.id_blog_post);
                            newLGCard.find('p.por').html(dat.autor.nombre + ' ' + dat.autor.apellido + ' ' + dat.fecha_creacion);
                            newLGCard.find('p.p-content').html(content.substring(0, 400) + '...');
                            newLGCard.find('span.icon-comment').html(dat.comentarios + '<i class="fa fa-comment"></i>');
                            newLGCard.find('span.icon-fav').html(dat.fav + '<i class="fa fa-heart"></i>');
                            newLGCard.find('a.isBlogUrl').attr('href', blogUrl + dat.id_blog_post);

                            //Cargado de Tarjetas(Card) pantallas pequeñas
                            newCard.find('span.card-title').html(dat.titulo.replace(/(<([^>]+)>)/ig, ''));
                            newCard.find('p.por').html(dat.autor.nombre + ' ' + dat.autor.apellido + ' ' + dat.fecha_creacion);
                            newCard.find('p.p-content').html(content.substring(0, 400) + '...');
                            newCard.find('span.icon-comment').html(dat.comentarios + '<i class="fa fa-comment"></i>');
                            newCard.find('span.icon-fav').html(dat.fav + '<i class="fa fa-heart"></i>');
                            newLGCard.find('a.isBlogUrl').attr('href', blogUrl + dat.id_blog_post);

                            if (dat.id_imagen_destacada) {
                                newLGCard.find('p.img-destacada').css(styles);
                                newCard.find('p.img-destacada').css(styles);
                            }

                            newCard.insertBefore('.isContainer');
                            newLGCard.insertBefore('.isContainer');
                        });
                    });
                }
                else {
                    $('.onLoad').html('No quedan más elementos por mostrar');
                }
            }

            $(document).ready(function() {
                $('.tag-slide').slick({
                    dots: false,
                    arrows: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 6,
                    variableWidth: true,
                    nextArrow: '<i class="fa fa-arrow-right" style="margin:0 0 0 10px;"></i>',
                    prevArrow: '<i class="fa fa-arrow-left" style="margin:0 25px 0 0;"></i>',
                });
            });

        </script>
        <?php $this->view("principal/footer") ?>
    </body>
</html>
