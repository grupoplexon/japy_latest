
<?php
$mensaje = isset($mensaje) ? $mensaje : $this->session->userdata('mensaje');
$tipo_mensaje = isset($tipo_mensaje) ? $tipo_mensaje : $this->session->userdata('tipo_mensaje');
$this->session->unset_userdata('mensaje');
$this->session->unset_userdata('tipo_mensaje');
if (isset($mensaje)) {
    ?>
    <div class="row">
        <div class="col s12">
            <div class="card-panel white-text <?php echo $tipo_mensaje == "success" ? "teal" : "orange" ?>">
                <?php
                if (is_array($mensaje)) {
                    echo "<ul>";
                    foreach ($mensaje as $key => $value) {
                        echo "<li>$value</li>";
                    }
                    echo "</ul>";
                } else {
                    echo $mensaje;
                }
                ?>
                <i class="fa fa-times right clickable " onclick="$(this).parent().parent().parent().remove()"></i>
            </div>
        </div>
    </div>
<?php } ?>