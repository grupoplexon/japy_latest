<!DOCTYPE html>
<html>
    <head>
        <title>
            Eliminar:   <?php echo $post->titulo ?>
        </title>
        <?php $this->view("blog/header") ?>
    </head>
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <div class=" container-body" style="padding: 0px 40px;">
            <h4>  Eliminar:   <?php echo $post->titulo ?></h4>
            <div class="divider"></div>
            <?php $this->view("blog/mensajes") ?>
            <div class="row">
                <div class="col s12 m12">
                    <div class="card ">
                        <div class="card-content">
                            <p>
                                Estas seguro de eliminar la entrada del blog:  <b><?php echo $post->titulo ?></b>? Esta acción borrara toda la información relacionada con la entrada

                            </p>
                            <br>
                            <form method="POST">
                                <button id="cancel" type="button" class="btn grey ">
                                    Cancel
                                </button>
                                <input type="hidden" value="<?php echo $post->titulo ?>" name="titulo">
                                <input type="hidden" value="<?php echo $post->id_blog_post ?>" name="xid">
                                <button class="btn red" type="submit">
                                    Eliminar
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view('blog/footer') ?>
        <script>
            $(document).ready(function () {
                $("#cancel").on("click", function () {
                    history.back();
                });
            });
        </script>
    </body>
</html>
