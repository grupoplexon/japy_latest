<!DOCTYPE html>
<html>
    <head>
        <?php $this->view("blog/header") ?>
    </head>
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <div class=" container-body" style="padding: 0px 40px;">
            <h4>
                Entradas: 
            </h4>
            <div class="divider"></div>
            <?php $this->view("blog/mensajes") ?>
            <div class="row">
                <div class="col s12 m12">
                    <div class="card ">
                        <div class="card-content">
                            <div style="    padding: 10px;">
                                <table class="striped"> 
                                    <thead>
                                        <tr>
                                            <th>
                                                ESTADO
                                            </th>
                                            <th>
                                                TITULO
                                            </th>
                                            <th>
                                                CREADO EL
                                            </th>
                                            <th>
                                                TAGS
                                            </th>
                                            <th>
                                                OPCIONES
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($posts as $key => $post) { ?>
                                            <tr>
                                                <td>
                                                    <span class="<?php echo ($post->publicado) ? "teal" : "orange" ?> white-text center" style=" display: block; width: 100%;font-size: 11px;padding: 6px;border-radius: 5px;margin-bottom: 10px"><?php echo ($post->publicado) ? "PUBLICADO" : "BORRADOR" ?>
                                                    </span>
                                                    <span>
                                                        &nbsp;<?php echo $post->fav ?> <i class="fa fa-heart"></i>
                                                    </span>
                                                    <span>
                                                        &nbsp;<?php echo $post->comentarios ?> <i class="fa fa-comment"></i>
                                                    </span>
                                                </td>
                                                <td>
                                                    <?php echo $post->titulo ?><br>
                                                    <small class="grey-text"> <i class="material-icons " style="font-size: 12px">person</i><?php echo $post->autor->apellido ?>, <?php echo $post->autor->nombre ?></small>
                                                </td> 
                                                <td>
                                                    <?php echo dateFormat($post->fecha_creacion, '%d/%b/%Y') ?>
                                                </td>
                                                <td>
                                                    <?php if (count($post->tags) > 0) { ?>
                                                        <?php foreach ($post->tags as $key => $tag) { ?>
                                                            <div class="chip truncate" style="max-width:120px" ><?php echo $tag->nombre ?></div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo $this->config->base_url() ?>/blog/post/editar/<?php echo $post->id_blog_post ?>" class="dorado-2-text tooltipped"
                                                       data-position="top" data-delay="50" data-tooltip="Editar">
                                                        <i class="material-icons">mode_edit</i> 
                                                    </a>
                                                    <?php if ($post->publicado) { ?>
                                                        <a data-post='<?php echo $post->id_blog_post ?>' data-estado="<?php echo $post->publicado ?>" class="gris-1-text tooltipped btn-cambiar-estado clickable" data-position="top" data-delay="50" data-tooltip="Dejar de publicar">
                                                            <i class="material-icons">file_download</i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a data-post='<?php echo $post->id_blog_post ?>' data-estado="<?php echo $post->publicado ?>"  class="green-text darken-5 tooltipped btn-cambiar-estado clickable" data-position="top" data-delay="50" data-tooltip="Publicar">
                                                            <i class="material-icons">file_upload</i>
                                                        </a>
                                                    <?php } ?>
                                                    <a href="<?php echo $this->config->base_url() ?>/blog/post/eliminar/<?php echo $post->id_blog_post ?>" class="red-text tooltipped"
                                                       data-position="top" data-delay="50" data-tooltip="Eliminar">
                                                        <i class="material-icons">close</i> 
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <ul class="pagination">
                                    <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                    <?php for ($i = 1; $i <= $total_paginas; $i++) { ?>
                                        <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo $this->config->base_url() ?>blog/post?pag=<?php echo $i ?>"><?php echo $i ?></a></li>
                                    <?php } ?>
                                    <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("blog/footer") ?>
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function () {
                $(".btn-cambiar-estado").on("click", function () {
                    var $this = $(this);
                    var data = {
                        post: $this.data("post"),
                        estado: $this.data("estado"),
                    };
                    $.ajax({
                        url: server + "blog/post/publish",
                        data: data,
                        method: "POST",
                        error: function () {
                        },
                        success: function (resp) {
                            location.reload();
                        }
                    });
                });
            });
        </script>
    </body>
</html>
