<!DOCTYPE html>
<?php
if ( ! isset($post)) {
    $id_post = 0;
} else {
    $id_post = $post->id_blog_post;
}

function editable($id_post)
{
    if ($id_post > 0) {
        if (is_numeric($id_post)) {
            return true;
        }
    }

    return false;
}

?>
<html>
    <head>
        <?php $this->view("blog/header") ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/css/select2.min.css">
        <style>
            .content {
                overflow: visible;
                position: relative;
                width: auto;
                margin-left: 0;
                padding: inherit;
            }

            .btn-block-on-small {
                margin-bottom: 10px;
            }

            strong {
                font-weight: bold;
            }

            .editable ul {
                padding-left: 10px;
            }

            .editable ul > li {
                list-style-type: disc;
            }

            .img-responsive {
                max-width: 100%;
                height: auto;
            }
        </style>
    </head>
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <?php $this->view("blog/mensajes") ?>
        <span id="id_post" class="hide"><?php echo $id_post ?></span>
        <div class="" style="padding: 0px 40px;">
            <div class="pull-right">
                <a class="waves-effect waves-light btn btn-block-on-small  btn-cambiar-estado red"
                   data-post='<?php echo editable($id_post) ? $post->id_blog_post : '' ?>'
                   data-estado="<?php echo editable($id_post) ? $post->publicado : '0' ?>">
                    <i class="material-icons left"><?php echo(editable(
                            $id_post
                        )
                        && $post->publicado ? 'file_download' : 'publish') ?></i>
                    <text><?php echo(editable($id_post) && $post->publicado ? "Volver Borrador" : "Publicar") ?></text>
                </a>
                <a id="btn-borrador" class="waves-effect waves-light btn btn-block-on-small grey"><i
                            class="material-icons left">save</i>Guardar</a>
            </div>
            <h4>
                Entrada:
                <br>
                <small id="estado"> <?php echo(editable($id_post) ? 'Editando' : 'Nueva') ?></small>
            </h4>
            <div class="row">
                <div class="col s12 m8">
                    <div class="card ">
                        <div class="card-content">
                            <h2 class="editable"><?php echo(editable($id_post) ? $post->titulo : 'Titulo') ?></h2>
                            <div class="editable" style="    padding: 10px;">
                                <p>
                                    <?php echo(editable($id_post) ? $post->contenido : 'Aqui el contenido del post') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <ul class="collapsible" data-collapsible="expandable">
                        <li>
                            <div class="collapsible-header active"><i class="material-icons">photo</i>Imagen Destacada</div>
                            <div class="collapsible-body">
                                <?php if (editable($id_post)) { ?>
                                    <img id="img-destacada"
                                         data-id="<?php echo $post->id_imagen_destacada ?>" <?php echo $post->id_imagen_destacada ? "src='".base_url()."blog/resource/imagen/".$post->id_imagen_destacada."'" : "" ?>
                                         class="responsive-img" style="display: block;margin: 0px auto;margin-top: 18px;">
                                <?php } else { ?>
                                    <img id="img-destacada" data-id="0" class="responsive-img"
                                         style="display: block;margin: 0px auto;margin-top: 18px;">
                                <?php } ?>
                                <p>
                                    <a id="seleccionar-imagen" class="btn btn-block  dorado-2">
                                        Seleccionar
                                    </a>
                                </p>

                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header active"><i class="material-icons">local_offer</i>Tags</div>
                            <div class="collapsible-body">
                                <select class="tag-selector" multiple="multiple">

                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div style="padding: 50px"></div>
        <div style="z-index: 999999;">
            <div id="modal-file-browser" class="modal  modal-fixed-footer" style="">
                <div class="modal-content" style="padding-left: 0px;padding-right: 0px;">
                    <h4>Buscar Imagen</h4>
                    <div class="row nowrap">
                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s3"><a class="active" href="#test1">Imagenes</a></li>
                                <li class="tab col s3"><a href="#test2">Subir Imagen</a></li>
                            </ul>
                        </div>
                        <div id="test1" class="col s12">
                            <div class="container">
                                <div id="sin-imagenes" class="card-panel teal">
                                    <span class="white-text">A&uacute;n no tienes imagenes cargadas.
                                    </span>
                                </div>
                                <div class="row contenedor-imagenes">
                                </div>
                            </div>
                        </div>
                        <div id="test2" class="col s12">
                            <div class="container">
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>File</span>
                                        <input id="file-ajax" type="file" accept="image/*">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text"
                                               placeholder="Upload one or more files">
                                    </div>
                                </div>
                                <input type="text" value="" id="file-data" style="display: none">
                                <div class="modal-imagen">
                                    <img id="img-preview" class="responsive-img"/>
                                    <div class="modal-image-cargando">
                                        <i class="fa fa-spinner fa-pulse fa-2x"></i> SUBIENDO ...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
                    <a id="modal-aceptar" href="#!" class=" modal-action waves-effect waves-green btn-flat">Aceptar</a>
                </div>
            </div>
        </div>
        <?php $this->view("blog/footer") ?>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.full.min.js"
                type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/i18n/es.js"
                type="text/javascript"></script>

        <script>
            const $tagSelector = $('.tag-selector');
            const server = "<?php echo base_url() ?>";
            let id_blog = $('#id_post').text();

            $(document).ready(function() {

                tinymce.init({
                    selector: 'h2.editable',
                    inline: true,
                    toolbar: 'undo redo',
                    menubar: false,
                    relative_urls: false,
                });

                tinymce.init({
                    selector: 'div.editable',
                    inline: true,
                    relative_urls: false,
                    menubar: 'edit insert view format table tools',
                    plugins: [
                        'advlist autolink lists link image imagetools charmap preview anchor',
                        'searchreplace visualblocks code ',
                        'insertdatetime media table contextmenu  textcolor colorpicker paste ',
                    ],
                    imagetools_cors_hosts: ['*'],
                    browser_spellcheck: true,
                    file_picker_callback: fileBrowser,
                    toolbar: 'insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    image_dimensions: false,
                    image_class_list: [
                        {title: 'Responsive', value: 'img-responsive'},
                    ],
                });

                $tagSelector.select2({
                    ajax: {
                        url: server + 'blog/Post',
                    },
                });

                if (id_blog != 0) {
                    $.ajax({
                        type: 'GET',
                        url: server + 'blog/Post/getTagsById',
                        data: {
                            'id': id_blog,
                        },
                    }).then(function(data) {
                        // create the option and append to Select2
                        console.log(data);
                        const tags = JSON.parse(data);

                        tags.data.forEach(function(tag) {
                            const option = new Option(tag.title, tag.id, true, true);
                            $tagSelector.append(option);
                        });
                        // manually trigger the `select2:select` event
                        $tagSelector.trigger({
                            type: 'select2:select',
                            params: {
                                data: data,
                            },
                        });
                    });
                }

                $tagSelector.select2({
                    tags: true,
                    width: '100%',
                    placeholder: 'Seleccione una etiqueta',
                    allowClear: true,
                    language: 'es',
                    minimumInputLength: 2,
                    ajax: {
                        url: server + 'blog/tag/show',
                        dataType: 'json',
                        type: 'GET',
                        quietMillis: 100,
                        data: function(term) {
                            return {
                                term: term,
                            };
                        },
                        processResults: function(data) {
                            return {
                                results: data.map(function(item) {
                                    return {
                                        text: item.title,
                                        id: item.id,
                                    };
                                }),
                            };
                        },
                    },
                });

                $tagSelector.on('change.select2', function(e) {
                    let data = {};

                    data.tags = $tagSelector.select2('data').map(function(item) {
                        return item.text;
                    });

                    $.ajax({
                        url: server + 'blog/tag/store',
                        data: data,
                        method: 'POST',
                        success: function(resp) {
                            $('.tag-selector').html('');
                            resp.forEach(function(item) {
                                let newOption = new Option(item.title, item.id, true, true);
                                $tagSelector.append(newOption);
                            });
                        },
                        error: function() {
                            $('.tag-selector').html('');
                        },
                    });

                });

                $('.btn-cambiar-estado').on('click', function() {
                    var $this = $(this);
                    if (!$this.data('post')) {
                        alert('Guarda primero la entrada');
                        return;
                    }
                    $this.addClass('disabled');
                    $this.find('i').html('cached');
                    var data = {
                        post: $this.data('post'),
                        estado: $this.data('estado'),
                    };

                    $.ajax({
                        url: server + 'blog/post/publish',
                        data: data,
                        method: 'POST',
                        error: function() {
                        },
                        success: function(resp) {
                            $this.removeClass('disabled');
                            $this.data('estado', data.estado == 1 ? 0 : 1);
                            if (data.estado == 0) {
                                $this.find('i').html('file_download');
                                $this.find('text').html('Volver a borrador');
                            }
                            else {
                                $this.find('i').html('publish');
                                $this.find('text').html('Publicar');
                            }
                        },
                    });
                });

                $('.modal-image-cargando').hide();

                $('#btn-borrador').on('click', function() {
                    save(false);
                });

                $('#modal-aceptar').on('click', function() {
                    var action = $('.modal .tab .active').html().toUpperCase();
                    if (action.indexOf('SUBIR') !== -1) {
                        $('.modal-image-cargando').show();
                        var data = $('#file-data').val();
                        if (data != null && data != '') {
                            $.ajax({
                                url: "<?php echo $this->config->base_url() ?>blog/resource/subir/imagen",
                                method: 'POST',
                                data: {
                                    archivo: $('#file-data').val(),
                                },
                                success: function(res) {
                                    if (res.success) {
                                        window.callback(res.data, {alt: 'alt'});
                                        $('#modal-file-browser').modal('close');
                                        $('.mce-window').css({'display': 'block'});
                                    }
                                    $('.modal-image-cargando').hide();
                                    $('#file-data').val('');
                                    $('#img-preview').get(0).src = '';
                                    $('.file-path validate').val();
                                },
                                error: function() {
                                    $('.modal-image-cargando').hide();
                                },
                            });
                        }
                    }
                    else if (action.indexOf('IMAGENES') != -1) {
                        var url = $('.contenedor-imagenes .active .card-image').get(0).style['background-image'];
                        var url = url.replace('url("', '').replace('")', '');
                        window.callback(url, {alt: 'alt'});
                        $('#modal-file-browser').modal('close');
                        $('.mce-window').css({'display': 'block'});
                    }
                });

                $('#file-ajax').on('change', function() {
                    var preview = $('#img-preview').get(0);
                    var file = $('#file-ajax').get(0).files[0];
                    var reader = new FileReader();
                    reader.onloadend = function() {
                        preview.src = reader.result;
                        $('#file-data').val(reader.result);
                    };
                    if (file) {
                        reader.readAsDataURL(file);
                    }
                    else {
                        preview.src = '';
                    }

                    this.value = '';

                });

                $('#seleccionar-imagen').on('click', function() {
                    fileBrowser(function(url) {
                        var a = url.split('/');
                        $('#img-destacada').attr('src', url);
                        $('#img-destacada').attr('data-id', a[a.length - 1]);
                    }, 'url', {filetype: 'image'});
                });

            });

            function save(publicar) {
                if ($('#img-destacada').attr('src') != '' && $('#img-destacada').attr('src') != undefined) {
                    $('#btn-borrador').addClass('disabled');
                    $('#btn-borrador i').html('cached');
                    $('#estado').html('Guardando ...');
                    let tags = $tagSelector.select2('val');

                    $.ajax({
                        url: server + 'blog/post/guardar',
                        method: 'POST',
                        data: {
                            contenido: tinymce.get(1).getContent(),
                            titulo: tinymce.get(0).getContent(),
                            tags: tags,
                            id: $('#id_post').text(),
                            imagen_destacada: $('#img-destacada').attr('data-id'),
                        },
                        success: function(res) {
                            $('.btn-cambiar-estado').data('post', res.data);
                            $('#id_post').val(res.data);
                            $('#btn-borrador').removeClass('disabled');
                            $('#btn-borrador i').html('save');
                            $('#estado').html('Guardado');
                        },
                        error: function() {
                            $('#btn-borrador').removeClass('disabled');
                            $('#estado').html('Ocurrio un error intenta mas tarde');
                        },
                    });
                }
                else {
                    $('#estado').html('Ingresa una imagen destacada antes de publicar');
                    $('#estado').addClass('red-text');
                    setTimeout(function() {
                        $('#estado').removeClass('red-text');
                        $('#estado').html('Nueva');
                    }, 5000);
                }
            }

            function fileBrowser(callback, value, meta) {
                if (meta.filetype == 'image') {
                    if ($('.contenedor-imagenes').children().length <= 1) {
                        $.ajax({
                            url: "<?php echo $this->config->base_url() ?>blog/resource/imagenes/",
                            success: function(res) {
                                $('#sin-imagenes').hide();
                                var template = ' <div class="card hoverable " style="background: black;"><div class="card-image image-galeria"></div></div>';
                                for (var i in res.data) {
                                    var imagen = res.data[i];
                                    var div = document.createElement('div');
                                    var $div = $(div);
                                    $div.addClass('col s6 m3');
                                    $div.html(template);
                                    $div.find('.image-galeria').css('background-image', 'url(' + imagen.url + ')');
                                    $div.on('click', function(evt) {
                                        $('.contenedor-imagenes .active').removeClass('active');
                                        var $elm = $(evt.currentTarget).find('.card');
                                        $elm.addClass('active');
                                    });
                                    $('.contenedor-imagenes').append($div);
                                }
                            },
                        });
                    }

                    window.callback = callback;

                    $('#modal-file-browser').modal({
                        dismissible: true,
                        opacity: .5,
                        in_duration: 100,
                        out_duration: 100,
                        ready: function() {
                            $('.mce-window').css({'display': 'none'});
                        },
                        complete: function() {
                            $('.mce-window').css({'display': 'block'});
                        },
                    });
                    $('#modal-file-browser').modal('open');
                    $('#modal-file-browser').css({'z-index': 999999});
                }
                return false;
            }
        </script>
    </body>
</html>