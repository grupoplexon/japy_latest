<?php
$url     = $this->config->base_url()."blog/home/index";
$url_img = $this->config->base_url()."blog/resource/imagen/"
?>
<?php if ($destacados) { ?>
    <div class="recomendaciones hide-on-small-only">
        <div class="card">
            <div class="row">
                <div class="col s12">
                    <ul class="tabs" style="overflow: hidden;">
                        <li class="tab col m6">
                            <a class="active" href="#mas_leido"
                               style="font-size: 12px;color: #00bcdd;">LO M&Aacute;S LE&Iacute;DO</a>
                        </li>
                        <li class="tab col m6">
                            <a href="#mas_comentado" style="font-size: 12px;color: #00bcdd;">LO M&Aacute;S
                                COMENTADO</a>
                        </li>
                    </ul>
                </div>
                <div id="mas_leido" class="col s12" style="padding: 0px">
                    <div class="collection">
                        <?php if (count($destacados)) : ?>
                            <?php foreach ($destacados as $key => $d) { ?>
                                <a class="collection-item avatar"
                                   href="<?php echo base_url() ?>blog/post/ver/<?php echo $d->post->id_blog_post ?>">
                                    <img src="<?php echo $url_img ?><?php echo $d->post->id_imagen_destacada ?>"
                                         class="material-icons circle"/>
                                    <span class="title" style="color: #00BCDD;">
                                    <?php echo strip_tags($d->post->titulo) ?>
                                </span>
                                    <p class="truncate hoverable hide-on-med-and-down" style="color: black">
                                        <?php echo strip_tags(substr($d->post->contenido, 0, 150)) ?>
                                    </p>
                                </a>
                            <?php } ?>
                        <?php else : ?>
                            <p style="color: black;text-align: center;">
                                Oops al parecer no hay publicaciones
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
                <div id="mas_comentado" class="col s12" style="padding: 0px">
                    <div class="collection">
                        <?php if (count($comentados)) : ?>
                            <?php foreach ($comentados as $key => $d) { ?>
                                <a class="collection-item avatar"
                                   href="<?php echo base_url() ?>blog/post/ver/<?php echo $d->id_blog_post ?>">
                                    <span class="badge"><?php echo $d->total ?></span>
                                    <img src="<?php echo $url_img ?><?php echo $d->id_imagen_destacada ?>"
                                         class="material-icons circle"/>
                                    <span class="title" style="color: #00BCDD;">
                                    <?php echo $d->titulo ?>
                                </span>
                                    <p class="truncate hoverable hide-on-med-and-down" style="color: black">
                                        <?php echo strip_tags(substr($d->contenido, 0, 150)) ?>
                                    </p>
                                </a>
                            <?php } ?>
                        <?php else : ?>
                            <p style="color: black;text-align: center;">
                                Oops al parecer no hay publicaciones
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="organizador-proveedores">
</div>
<div class="organizador-novio hide-on-small-only">
    <div class="card">
        <div class="card-content">
            <div class="card-title" style="color: #00BCDD; font-size: 13px;font-weight: 400;">
                Empieza a Organizar tu Boda
            </div>
        </div>
        <div class="collection">
            <a href="<?php echo site_url("novios/tarea") ?>" class="collection-item black-text">Agenda de Tareas
                <span class="secondary-content"> <i
                            class="material-icons dorado-2-text">keyboard_arrow_right</i></span></a>
            <a href="<?php echo site_url("novios/invitados") ?>" class="collection-item black-text">Mis Invitados
                <span class="secondary-content"> <i
                            class="material-icons dorado-2-text">keyboard_arrow_right</i></span></a>
            <a href="<?php echo site_url("novios/mesa") ?>" class="collection-item black-text">Organizador de Mesa
                <span class="secondary-content"> <i
                            class="material-icons dorado-2-text">keyboard_arrow_right</i></span></a>
            <a href="<?php echo site_url("novios/presupuesto") ?>" class="collection-item black-text">Presupuestador
                <span class="secondary-content"> <i
                            class="material-icons dorado-2-text">keyboard_arrow_right</i></span></a>
            <!--                <a href="<?php /*echo site_url("novios/miweb") */ ?>"     class="collection-item black-text">Web de boda         <span class="secondary-content"> <i class="material-icons dorado-2-text">keyboard_arrow_right</i></span></a>-->
        </div>
    </div>
</div>
