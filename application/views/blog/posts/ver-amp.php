<!doctype html>
<html AMP>
    <head>
        <meta charset="utf-8">
        <title><?php echo isset($titulo) ? $titulo : "clubnupcial.com" ?></title>
        <link rel="canonical" href="https://medium.com/p/cb7f223fad86">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <link href='https://fonts.googleapis.com/css?family=Georgia|Open+Sans|Roboto' rel='stylesheet' type='text/css'>
        <style amp-custom>
            body {
                margin: 0;
                font-family: 'Georgia', Serif;
            }

            .brand-logo {
                font-family: 'Open Sans';
            }

            .ad-container {
                display: flex;
                justify-content: center;
            }

            .content-container p {
                line-height: 24px;
            }

            header,
            .article-body {
                padding: 15px;
            }

            /* In the shadow-doc mode, the parent shell already has header and nav. */
            .amp-shadow header {
                display: none;
            }

            .lightbox {
                background: #222;
            }

            .full-bleed {
                margin: 0 -15px;
            }

            .lightbox-content {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                justify-content: center;
                align-items: center;
            }

            .lightbox-content p {
                color: #fff;
                padding: 15px;
            }

            .lightbox amp-img {
                width: 100%;
            }

            figure {
                margin: 0;
            }

            figcaption {
                color: #6f757a;
                padding: 15px 0;
                font-size: .9em;
            }

            .author {
                display: flex;
                align-items: center;
                background: #f4f4f4;
                padding: 0 15px;
                font-size: .8em;
                border: solid #dcdcdc;
                border-width: 1px 0;
            }

            .header-time {
                color: #a8a3ae;
                font-family: 'Roboto';
                font-size: 12px;
            }

            .author p {
                margin: 5px;
            }

            .byline {
                font-family: 'Roboto';
                display: inline-block;
            }

            .byline p {
                line-height: normal;
            }

            .byline .brand {
            }

            .standfirst {
                color: #6f757a;
            }

            .mailto {
                text-decoration: none;
            }

            #author-avatar {
                margin: 10px;
                border: 5px solid #fff;
                width: 50px;
                height: 50px;
                border-radius: 50%;
            }

            h1 {
                margin: 5px 0;
                font-weight: normal;
            }

            footer {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 226px;
                background: #f4f4f4;
            }

            hr {
                margin: 0;
            }

            amp-app-banner {
                background: #f4f4f4;
                padding: 10px;
                box-shadow: 0 2px 5px 0 #000;
            }

            amp-app-banner .content {
                display: flex;
            }

            amp-img {
                background-color: #f4f4f4;
            }

            .slot-fallback {
                padding: 16px;
                background: yellow;
            }

            amp-app-banner amp-img {
                margin-right: 15px
            }

            amp-app-banner h3 {
                margin: 0;
            }

            amp-app-banner p {
                font-size: 14px;
                margin: 3px 0;
            }

            amp-app-banner a {
                padding: 8px;
                margin-right: 10px;
            }

            amp-app-banner .actions {
                text-align: right;
                font-size: 14px;
            }

            amp-sidebar {
                width: 150px;
            }

            nav li {
                list-style: none;
                margin-bottom: 10px;
            }

            nav li a {
                text-decoration: none;
                color: #666666;
            }

            .chip {
                display: inline-block;
                height: 32px;
                font-size: 13px;
                font-weight: 500;
                color: rgba(0, 0, 0, 0.6);
                line-height: 32px;
                padding: 0 12px;
                border-radius: 16px;
                background-color: #e4e4e4;
            }
        </style>
        <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <script async custom-element="amp-app-banner" src="https://cdn.ampproject.org/v0/amp-app-banner-0.1.js" data-amp-report-test="amp-app-banner.js"></script>
        <style amp-boilerplate>body {
                -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
                -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
                -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
                animation: -amp-start 8s steps(1, end) 0s 1 normal both
            }

            @-webkit-keyframes -amp-start {
                from {
                    visibility: hidden
                }
                to {
                    visibility: visible
                }
            }

            @-moz-keyframes -amp-start {
                from {
                    visibility: hidden
                }
                to {
                    visibility: visible
                }
            }

            @-ms-keyframes -amp-start {
                from {
                    visibility: hidden
                }
                to {
                    visibility: visible
                }
            }

            @-o-keyframes -amp-start {
                from {
                    visibility: hidden
                }
                to {
                    visibility: visible
                }
            }

            @keyframes -amp-start {
                from {
                    visibility: hidden
                }
                to {
                    visibility: visible
                }
            }</style>
        <noscript>
            <style amp-boilerplate>body {
                    -webkit-animation: none;
                    -moz-animation: none;
                    -ms-animation: none;
                    animation: none
                }</style>
        </noscript>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <meta name="apple-itunes-app" content="app-id=828256236, app-argument=medium://p/cb7f223fad86">
        <link rel="amp-manifest" href="medium-manifest.json">
    </head>
    <body>
        <amp-sidebar id="sidebar" layout="nodisplay">
            <nav>
                <ul>
                    <li>
                        <a href="#">
                            Home
                            <amp-img src="img/ampicon.png" width="15" height="15"></amp-img>
                        </a>
                    </li>
                    <li><a href="#">Link 1</a></li>
                    <li><a href="#">Link 2</a></li>
                    <li><a href="#">Link 3</a></li>
                    <li><a href="#">Link 4</a></li>
                </ul>
            </nav>
        </amp-sidebar>
        <header>
            <button on="tap:sidebar.toggle" class="menu" title="Toggle Sidebar">
                ☰
            </button>
            <span class="brand-logo">
            <amp-img src="<?php echo base_url() ?>dist/img/logo.png"
                     height="60"
                     layout="responsive"
                     on="tap:headline-img-lightbox" role="img"
            >
            </amp-img>
        </span>
        </header>

        <main role="main">
            <article>
                <figure>
                    <amp-img
                            src="<?php echo base_url() ?>dist/img/blog/default.png"
                            layout="responsive" width="360" placeholder
                            alt="<?php echo $post->titulo ?>"
                            height="216" on="tap:headline-img-lightbox" role="img" tabindex="0">
                    </amp-img>
                </figure>

                <amp-lightbox id="headline-img-lightbox" class="lightbox" layout="nodisplay">

                    <div class="lightbox-content">
                        <amp-img id="floating-headline-img"
                                 src="<?php echo base_url() ?>dist/img/blog/default.png"
                                 placeholder
                                 width="360" height="216" layout="responsive">
                        </amp-img>
                        <p>
                        </p>
                    </div>
                </amp-lightbox>

                <div class="content-container">
                    <header>
                        <h1 itemprop="headline"><?php echo $post->titulo ?></h1>
                        <time class="header-time" itemprop="datePublished"
                              datetime="2015-09-14 13:00"><?php echo dateMiniFormat($post->fecha_creacion) ?></time>

                        <p class="standfirst">
                        </p>
                        <?php foreach ($post->tags as $key => $t) { ?>
                            <div class="chip">
                                <?php echo $t->nombre ?>
                            </div>
                        <?php } ?>
                    </header>
                    <div class="author">
                        <amp-img src="<?php echo site_url("perfil/foto/".$post->id_autor) ?>" id="author-avatar" placeholder
                                 height="50" width="50">
                        </amp-img>
                        <div class="byline">
                            <p>
                                Por <span itemscope itemtype="http://schema.org/Person"
                                          itemprop="author">
                                <b><?php echo $post->autor->nombre ?><?php echo $post->autor->apellido ?></b>
                            </span>
                            </p>
                            <p class="brand">Blogger
                            <p>
                        </div>
                    </div>
                    <div class="article-body" itemprop="articleBody">
                        <?php echo $post->contenido ?>
                    </div>
                </div>
            </article>
        </main>

        <footer>
            <amp-img
                    src="<?php echo base_url() ?>dist/img/logo.png"
                    layout="responsive" width="360" placeholder
                    alt="clubnupcial.com"
                    height="216" role="img" tabindex="0">
            </amp-img>
        </footer>
    </body>
</html>