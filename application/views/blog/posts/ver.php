<!DOCTYPE html>
<?php
$url         = $this->config->base_url()."blog/post/index";
$url_img     = $this->config->base_url()."blog/resource/imagen/";
$url_default = $this->config->base_url()."/dist/img/blog/default.png";
?>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta property="og:title" content="<?php echo strip_tags($post->titulo) ?>"/>
        <meta property="og:image" content="<?php echo base_url()."blog/resource/imagen/".$post->id_imagen_destacada ?>">
        <meta property="og:image:type" content="image/png">
        <meta property="og:url" content="<?php echo base_url()."blog/post/ver/$post->id_blog_post" ?>"/>
        <meta property="og:image:width" content="900">
        <meta property="og:image:height" content="476">
        <meta property="og:site_name" content="Japy" />
        <!-- <meta property="og:description" content="Todo lo que nbición." /> -->
        <meta property="og:type" content="website" />
        <?php
        $this->view("blog/header");
        $this->view("general/newheader");
        ?>
    </head>
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <?php $this->view("general/menu"); ?>
        <style>
            .toast {
                min-height: 68px !important;
            }

            .post strong {
                font-weight: bold;
            }

            .post ul {
                padding-left: 10px;
            }

            .post ul > li {
                list-style-type: disc;
            }

            p {
                font-family: 'Raleway';
                font-size: 14px;
            }

            .img-responsive {
                max-width: 100% !important;
                height: auto;
            }

            @media only screen and (max-width: 425px) {
                .post {
                    width: 293px !important;
                }

                p img {
                    width: 100%;
                    height: auto;
                }

                .blog-title {
                    text-align: center !important;
                }

                .card-content p, .card-content ul {
                    text-align: justify;
                }

            }


                hr{
                    border: 0;
                    height: 1px;
                    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
                }

        </style>
        <div class="row body-container">
            <div class="col s12 m9">
                <div class="card ">
                    <div class="card-content">
                        <p class="card-title blog-title"
                           style="font-size: 38px;line-height: initial"><?php echo $post->titulo ?></p><br>
                        <p style="color: #828282; font-size: 12px;margin-top: 10px;margin-bottom: 5px">
                            Por <?php echo $post->autor->nombre ?> <?php echo $post->autor->apellido ?>
                            el <?php echo dateMiniFormat($post->fecha_creacion) ?><br>
                        </p>
                        <div class="tag-slide hide-on-small-only">
                            <?php foreach ($post->tags->tags as $t) { ?>
                                <div class="chip tagElement">
                                    <?php echo $t->title ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="hide-on-med-and-up">
                            <?php foreach ($post->tags->tags as $t) { ?>
                                <div class="chip tagElement">
                                    <?php echo $t->title ?>
                                </div>
                            <?php } ?>
                        </div>
                        
                        <p class="content-post"><?php echo $post->contenido ?></p>
                        <div class="container-icons-social">
                            <p class="row">

                                <a class="btn blue col m2 s3 l1 btn-share-link"
                                   href="https://www.facebook.com/sharer/sharer.php?app_id=2052619388319572&sdk=joey&u=<?php echo(base_url()."blog/post/ver/$post->id_blog_post") ?>&display=popup&ref=plugin&src=share_button"
                                   onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')"
                                   style="margin-bottom: 10px; padding-right: 5px;">
                                    <i class="fa fa-facebook-official"></i>
                                </a>
                                <a class="btn blue lighten-3 col m2 s3 l1 btn-share-link-tw"
                                   data-href="<?php echo base_url("blog/post/ver/$post->id_blog_post") ?>"
                                   data-quote="<?php echo "#japy"." ".substr(strip_tags($post->contenido), 0, 150) ?>" style="margin-bottom: 10px">
                                    <i class="fa fa-twitter"></i> Tweet
                                </a>
                                <?php if ($this->checker->isLogin()) { ?>
                                    <button class="btn-flat right white waves-effect icon-social comments">
                                        <a id="btn-favorito" data-position="top" data-delay="50"
                                           data-tooltip="<?php echo(! $favorito ? "Me gusta" : "No me gusta") ?>"
                                           class="tooltipped "
                                           style="cursor: pointer;display: inline-block;">
                                            <i class="fa <?php echo(! $favorito ? "fa-heart-o" : "fa-heart") ?>"></i>
                                        </a>
                                        <text class="num-fav "><?php echo $post->fav ?></text>
                                    </button>
                                    <button class="col s3 btn-flat right icon-social comments white waves-effect ">
                                        <i class="fa fa-comment"></i> <?php echo($comentarios ? count(
                                            $comentarios
                                        ) : "0") ?>
                                    </button>
                                <?php } else { ?>
                                    <button id="icon-heart" class="col s3 btn-flat right white waves-effect icon-social comments">
                                        <i class="fa fa-heart "></i>
                                        <?php echo $post->fav ?>
                                    </button>
                                    <button id="comentarios-post"
                                            class="col s3 btn-flat right icon-social comments white waves-effect ">
                                        <i class="fa fa-comment"></i> <?php echo($comentarios ? count(
                                            $comentarios
                                        ) : "0") ?>
                                        
                                    </button>
                                <?php } ?>
                            </p>
                        </div>
                        <br>
                        <hr> 
                        <div class="flex-container">

                            <div>

                                <div class="title-section2" >
                                    <h3>Artículos Relacionados</h3>
                                </div>          
                                <?php foreach ($posts as $post) : ?>
                                    
                                        <div class="content-data2">
                                            <img class="element-img2"
                                                src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada
                                                    : $url_default ?>" alt="" style="">
                                            <a href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>"
                                            class="element-name2 truncate"><?php echo strip_tags($post->titulo) ?></a>
                                            
                                        </div>
                                    
                                <?php endforeach; ?> 
                            </div>   
                            
                            <div >
                            <div class="title-section2" >
                                <h3>Proveedores Sugeridos</h3>
                                        </div>
                            <?php foreach ($providers as $provider) : ?>

                                <?php if ($provider->imagePrincipal->count()) {
                                    $providerImageUrl = base_url()."uploads/images/".$provider->imagePrincipal->first()->nombre;
                                    ?>
                                <?php } else {
                                    $providerImageUrl = base_url()."dist/img/slider1.png";
                                    ?>
                                <?php } ?>
                                    <div class="content-data2">
                                        <img class="element-img2" src="<?php echo($providerImageUrl) ?>" alt="">
                                        <a class="element-name2 truncate" href="<?php echo base_url() ?>boda-<?php echo $provider->slug ?>"><?php echo $provider->nombre ?></a>
                                    </div>
                                
                            <?php endforeach; ?>
                            </div>

                        </div>
                    </div>
                </div>
                <?php if ($comentarios || $this->checker->isLogin()) : ?>
                    <div class="col s12  m11 offset-m1" id="comentarios">
                        <h5>
                            Comentarios
                        </h5>
                        <?php if ($this->checker->isLogin()) { ?>
                            <div class="comentar">
                                <div class="card-panel" style="padding-bottom: 10px;padding-top: 5px">
                                    <div class="row" style="margin-bottom: 0px">
                                        <div class="col">
                                            <img class="responsive-img circule-img border"
                                                 src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $this->session->userdata(
                                                     "usuario"
                                                 ) ?>">
                                        </div>
                                        <div class="input-field col s10 m10">
                                            <textarea id="text-comment" class="materialize-textarea "
                                                      length="250"></textarea>
                                            <label for="icon_prefix2">Comentario</label>
                                        </div>
                                        <div class="col s12 m12">
                                            <a id="btn-comentar" class="waves-effect waves-light btn dorado-2"
                                               style="    float: right;"><i class="material-icons right">send</i>Comentar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="comentarios">
                            <?php
                            if ($comentarios) {
                                foreach ($comentarios as $key => $c) {
                                    ?>
                                    <div id="<?php echo $c->id_blog_comentario ?>"
                                         class="card-panel comentario  <?php echo $c->comentarios ? "has-comments" : "" ?>"
                                         style="padding-bottom: 10px;padding-top: 5px">
                                        <div class="row" style="margin-bottom: 5px">
                                            <div class="col">
                                                <img class="responsive-img circule-img"
                                                     src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $c->id_usuario ?>">
                                            </div>
                                            <div class="input-field col s10 m10">
                                                <h6>
                                                    <b><?php echo $c->usuario->nombre ?><?php echo $c->usuario->apellido ? $c->usuario->apellido : "" ?></b>
                                                    <small>
                                                        <?php echo relativeTimeFormat($c->fecha) ?></small>
                                                </h6>
                                                
                                                <p>
                                                    <?php echo $c->mensaje ?>
                                                </p>
                                            </div>
                                            <div class="col s6">
                                                <a data-href="#comentar-<?php echo $c->id_blog_comentario ?>"
                                                   class="btn-flat dorado-2-text waves-effect comment clickable">
                                                    comentar
                                                </a>
                                            </div>
                                            <div class="col s6">
                                                <div class="container-icons-social">
                                                    <p>
                                                    <span class="icon-social comments total-comment"><text><?php echo $c->comentarios ?></text>
                                                        <i class="fa fa-comment-o left tooltipped"
                                                           data-position="top"
                                                           data-tooltip="Comentarios"></i>
                                                    </span>
                                                        <span class="icon-social comments "><text><?php echo $c->favoritos ?></text>
                                                        <a class="tooltipped clickable like-comment"
                                                           data-comment="<?php echo $c->id_blog_comentario ?>"
                                                           data-position="top"
                                                           data-tooltip="<?php echo $c->favorito ? "Ya no me gusta" : "Me gusta" ?>">
                                                            <i class="fa fa-heart<?php echo $c->favorito ? "" : "-o" ?> left"></i>
                                                        </a>
                                                    </span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="commentarios-2 row">
                                            <div class="divider"></div>
                                            <div class="load-comentarios valign-wrapper <?php echo $c->comentarios ? "" : "hide" ?> "
                                                 style="height: 50px">
                                                <div class="valign col s12 center">
                                                    <i class="fa fa-spin fa-spinner fa-2x"></i>
                                                </div>
                                            </div>
                                            <?php if ($this->checker->isLogin()) { ?>
                                                <div id="comentar-<?php echo $c->id_blog_comentario ?>"
                                                     class="comment ">
                                                    <div class="card-panel z-depth-0"
                                                         style="padding-bottom: 10px;padding-top: 5px">
                                                        <div class="row" style="margin-bottom: 0px">
                                                            <div class="col">
                                                                <img class="responsive-img circule-img border"
                                                                     src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $this->session->userdata(
                                                                         "usuario"
                                                                     ) ?>">
                                                            </div>
                                                            <div class="input-field col s10 m10">
                                                                <textarea class="materialize-textarea "
                                                                          length="250"></textarea>
                                                                <label for="icon_prefix2">Comentario</label>
                                                            </div>
                                                            <div class="col s12 m12">
                                                                <a class="waves-effect waves-light btn dorado-2 btn-comment"
                                                                   data-comment='<?php echo $c->id_blog_comentario ?>'
                                                                   style="    float: right;"><i
                                                                            class="material-icons right">send</i>Comentar</a>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        
                    </div>
                <?php endif ?>
            </div>
            <div class="col s12 m3">
                <?php $this->view("blog/posts/sidebar") ?>
            </div>
        </div>
        
        <script src="<?php echo base_url() ?>/dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>/dist/js/blog/ver.js" type="text/javascript"></script>
        <?php $this->view("principal/footer") ?>
        <script>
            $(document).ready(function() {
                $('ul.left > li, ul.right > li').on('click', function(e) {
                    e.preventDefault();
                    if ($(this).attr('data-href')) {
                        window.location.href = $(this).attr('data-href');
                    }
                    else {
                        const url = $(this).find('a').attr('href');
                        window.location.href = url;
                    }
                });
            });
        </script>
        <script>
            var post = "<?php echo $post->id_blog_post ?>";
            var server = "<?php echo base_url() ?>";
            Comentarios.url = server;
            $(document).ready(function() {

                $('.tag-slide').slick({
                    dots: false,
                    arrows: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 6,
                    variableWidth: true,
                    nextArrow: '<i class="fa fa-arrow-right" style="margin:0 0 0 10px;"></i>',
                    prevArrow: '<i class="fa fa-arrow-left" style="margin:0 25px 0 0;"></i>',
                });

                $('textarea#text-comment').characterCounter();
                $('.tooltipped').tooltip();
                $('a#btn-favorito').parent().on('click', UIComentarios.principal_btn_like);
                $('a#btn-favorito').on('click', UIComentarios.principal_btn_like);

                $('#icon-heart').on('click', function(e) {
                    e.preventDefault();
                    if (!$('#toast-container .toast').get(0)) {
                        Materialize.toast('Debes estar registrado para dar Me Gusta<br> ' + " <a href='<?php echo site_url('Cuenta') ?>?callback=" + location.href + '\' class=\'dorado-2-text pull-right\'>Inicia Sesi&oacute;n <i class=\'material-icons right\'>keyboard_arrow_right</i></a>', 5000);
                        $('.toast').css({display: 'block'});
                    }
                });

                $('#comentarios-post').on('click', function(e) {
                    e.preventDefault();
                    if (!$('#toast-container .toast').get(0)) {
                        Materialize.toast('Debes estar registrado para comentar<br> ' + " <a href='<?php echo site_url('Cuenta') ?>?callback=" + location.href + '\' class=\'dorado-2-text pull-right\'>Inicia Sesi&oacute;n <i class=\'material-icons right\'>keyboard_arrow_right</i></a>', 5000);
                        $('.toast').css({display: 'block'});
                    }

                });

                var comentarios = $('.comentario.has-comments');
                if (comentarios.length > 0) {
                    UIComentarios.load_comments(comentarios, 0);
                }
                $('a#btn-comentar').on('click', function() {
                    if (!$('#btn-comentar').hasClass('disabled')) {
                        $('a#btn-comentar').addClass('disabled');

                        var f = new Date();
                        var data = {
                            'comentario': $('textarea#text-comment').val(),
                        };

                        Comentarios.comentarPost(post, data, function(resp) {
                            if (resp) {
                                console.log(resp);
                                var c = $('textarea#text-comment').val();
                                var div = document.createElement('div');
                                div.setAttribute('class', 'card-panel');
                                div.setAttribute('style', 'padding-bottom: 10px;padding-top: 5px');
                                var $div = $(div);
                                $div.attr('id', resp.data);
                                $div.html($('#template-comentario').html());
                                $div.append($('#template-comentarios').html());
                                $div.find('.nombre').html("<?php echo $this->session->userdata("nombre") ?>");
                                $div.find('.fecha').html(f.getDate() + ' de ' + meses[f.getMonth()] + ' de ' + f.getFullYear());
                                $div.find('.contenido').html(c);
                                $div.find('.foto').attr('src', Comentarios.url + "perfil/foto/<?php echo $this->session->userdata(
                                    "usuario"
                                ) ?>");
                                $div.find('.like-comment').data('comment', resp.data);
                                $div.find('.like-comment').attr('data-comment', resp.data);
                                $div.find('.like-comment').on('click', UIComentarios.like_comment);
                                $div.find('#comentar').attr('id', `comentar-${resp.data}`);
                                $div.find('a.btn-flat.comment.dorado-2-text').attr('data-href', `#comentar-${resp.data}`);
                                $div.find('a.btn.dorado-2.btn-comment').attr('data-comment', `${resp.data}`);
                                $('textarea#text-comment').val('');
                                $('a#btn-comentar').removeClass('disabled');
                                $('.comentarios').prepend($div);
                            }
                            else {
                                $('a#btn-comentar').removeClass('disabled');
                            }
                            var c = parseInt($('#comentarios-post').text());
                            $('#comentarios-post').html('<i class=\'fa fa-comment\'></i> ' + (c + 1) + '');
                        });
                    }
                });

                $(document).on('click', 'a.like-comment', UIComentarios.like_comment);

                $(document).on('click', 'a.comment', UIComentarios.btn_comment);

                $(document).on('click', 'a.btn-comment', send_comment);

                $('.btn-share-link-tw').on('click', function() {
                    var $this = $(this);
                    var url = $this.data('href');
                    if (url) {
                        var titulo = $this.data('quote');
                        console.log('https://twitter.com/share?url=' + url + '&text=' + titulo);
                        window.open('https://twitter.com/share?url=' + url + '&text=' + encodeURIComponent(titulo), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                    }
                });

                var social = new FBclub();

                social.initShareLinks();

                $('').on('', function() {

                });

            });

            function send_comment() {
                var $self = $(this);

                console.log($self.data('comment'), $('#comentar-' + $self.data('comment')).find('textarea').val());

                var data = {
                    comentario: $('#comentar-' + $self.data('comment')).find('textarea').val(),
                    parent: $self.data('comment'),
                };

                var container = $('#' + data.parent).find('.load-comentarios');

                if (container.hasClass('valign-wrapper')) {
                    container.html('');
                    container.removeClass('valign-wrapper');
                    container.removeClass('hide');
                    container.attr('style', 'margin-bottom: -10px;padding-left: 35px;');
                    container.addClass('row');
                }

                Comentarios.comentarComment(post, data, function(r) {
                    if (r) {
                        $('#comentar-' + $self.data('comment')).removeClass('active');
                        $('#comentar-' + $self.data('comment')).find('textarea').val('');
                        var div = document.createElement('div');
                        div.setAttribute('class', 'card-panel z-depth-0');
                        div.setAttribute('style', 'padding-bottom: 10px;padding-top: 5px');
                        var $div = $(div);
                        $div.html($('#template-comentario').html());
                        $div.find('.nombre').html("<?php echo $this->session->userdata("nombre") ?>");
                        $div.find('.fecha').html('Hace un momento');
                        $div.find('.contenido').html(data.comentario);
                        $div.find('.foto').attr('src', Comentarios.url + "perfil/foto/<?php echo $this->session->userdata(
                            "usuario"
                        ) ?>");
                        $div.find('.like-comment').data('comment', r.data);
                        $div.find('.like-comment').attr('data-comment', r.data);
                        $div.find('.like-comment').on('click', UIComentarios.like_comment);
                        $div.find('.col.s8').remove();
                        $div.find('.col.s4').remove();
                        $div.find('div.row').attr('style', 'margin-bottom:0px');
                        container.removeClass('valign-wrapper');
                        container.attr('style', 'margin-bottom: -10px;padding-left: 35px;');
                        container.addClass('row');
                        container.append('<div class=\'divider\'></div>');
                        container.append($div);
                        var total_c = $('#comentar-' + $self.data('comment')).parent().parent().find('.total-comment text');
                        var c = parseInt(total_c.text());
                        total_c.text(c + 1);
                    }
                });
            }
        </script>
    </body>
</html>




