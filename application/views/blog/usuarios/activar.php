<!DOCTYPE html>
<html>
    <head>
        <?php $this->view("blog/header") ?>
    </head>    
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <div class="" style="padding: 20px 40px 40px 40px;">
            <div class="row">
                <div class="col m12">
                    <div class="pull-right">
                        <a  href="<?php echo $this->config->base_url() ?>blog/usuario/nuevo"id="btn-publicar" class="waves-effect waves-light btn btn-block-on-small dorado-2"><i class="material-icons left">add</i>Nuevo Redactor</a>
                    </div>
                    <h3>
                        <?php echo ($usuario->activo ? "Desactivar" : "Activar") ?> Redactor:
                    </h3>
                    <div class="card-panel ">
                        <div class="row">
                            <form class="col m12 s12" method="POST" action="<?php echo $this->config->base_url() ?>blog/usuario/activar/<?php echo $usuario->id_usuario ?>">
                                <p>
                                    Estas Seguro que deseas <?php echo ($usuario->activo ? "desactivar" : "activar") ?> a <?php echo $usuario->nombre ?>  <?php echo $usuario->apellido ?>  ?
                                </p>
                                <input name="usuario" type="hidden" value="usuario-<?php echo $usuario->id_usuario ?>">
                                <button class="btn  red btn-block-on-small" type="submit" style="margin-bottom:  10px" >
                                    <?php echo ($usuario->activo ? "Desactivar" : "Activar") ?>
                                </button>
                                <a class="btn  white black-text btn-block-on-small" style="margin-bottom:  10px"  href="<?php echo base_url() ?>blog/usuario" >
                                    Cancelar
                                </a>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("blog/footer") ?>
        <script>

            $(document).ready(function () {
                $(".descripcion1").children(".card-panel").css({visibility: "hidden"});
                $(".descripcion2").children(".card-panel").css({visibility: "hidden"});
                $("div[data-focus]").each(function (index, value) {
                    var t = value.getAttribute("data-toggle");
                    $(t).children(".card-panel").css({visibility: "hidden"});
                    $(value).find("input").each(function (index, value) {
                        $(value).on("focus", function () {
                            $(t).children(".card-panel").css({visibility: "visible"});
                        });
                        $(value).on("blur", function () {
                            $(t).children(".card-panel").css({visibility: "hidden"});
                        });
                    });
                });
            });

            function seguridad_clave(clave) {
                var seguridad = 0;
                if (clave.length !== 0) {
                    if (tiene_numeros(clave) && tiene_letras(clave)) {
                        seguridad += 30;
                    }
                    if (tiene_minusculas(clave) && tiene_mayusculas(clave)) {
                        seguridad += 30;
                    }
                    if (clave.length >= 4 && clave.length <= 5) {
                        seguridad += 10;
                    } else {
                        if (clave.length >= 6 && clave.length <= 8) {
                            seguridad += 30;
                        } else {
                            if (clave.length > 8) {
                                seguridad += 40;
                            }
                        }
                    }
                }
                return seguridad;
            }


        </script>

    </body>
</html>