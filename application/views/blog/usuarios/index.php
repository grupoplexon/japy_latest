<!DOCTYPE html>
<html>
    <head>
        <?php $this->view("blog/header") ?>
    </head>    
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <div class="" style="padding: 20px 40px 40px 40px;">
            <div class="row">
                <div class="col m12">
                    <div class="pull-right">
                        <a  href="<?php echo $this->config->base_url() ?>blog/usuario/nuevo"id="btn-publicar" class="waves-effect waves-light btn btn-block-on-small dorado-2">
                            <i class="material-icons left">add</i>
                            Nuevo Redactor
                        </a>
                    </div>
                    <h3>
                        Usuarios:
                    </h3>
                    <?php $this->view("blog/mensajes") ?>
                    <div class="card-panel ">
                        <?php if ($usuarios) { ?>
                            <table>
                                <thead>
                                    <tr>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Correo
                                        </th>
                                        <th>
                                            Fecha creacion
                                        </th>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            Opciones
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php foreach ($usuarios as $key => $u) { ?>
                                        <tr>
                                            <td>
                                                <?php echo $u->nombre ?> <?php echo $u->apellido ?>
                                            </td> 
                                            <td>
                                                <?php echo $u->correo ?>
                                            </td> 
                                            <td>
                                                <?php echo dateFormat($u->fecha_creacion) ?> 
                                            </td>
                                            <td>
                                                <?php echo $u->activo ? "Activo" : "Inactivo" ?> 
                                            </td>
                                            <td>
                                                <a href="<?php echo $this->config->base_url() ?>blog/usuario/activar/<?php echo $u->id_usuario ?>" class="tooltipped" data-position="top" data-delay="50" data-tooltip="<?php echo $u->activo ? "Desactivar" : "Activar" ?> ">
                                                    <?php if ($u->activo) { ?>
                                                        <i class="material-icons">check_box</i>
                                                    <?php } else { ?>
                                                        <i class="material-icons">check_box_outline_blank</i>
                                                    <?php } ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tr>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col s12">
                                    <div class=" card-panel teal white-text">
                                        <i class="material-icons">info</i>  No hay redactores registrados
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("blog/footer") ?>
    </body>
</html>
