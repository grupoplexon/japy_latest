<!DOCTYPE html>
<html>
    <head>
        <?php $this->view("blog/header") ?>
    </head>    
    <body>
        <?php $this->view("blog/admin/menu") ?>
        <div class="" style="padding: 20px 40px 40px 40px;">
            <div class="row">
                <div class="col m12">
                    <div class="pull-right">
                        <a  href="<?php echo $this->config->base_url() ?>blog/usuario/nuevo"id="btn-publicar" class="waves-effect waves-light btn dorado-2 btn-block-on-small">
                            <i class="material-icons left">add</i>Nuevo Redactor
                        </a>
                    </div>
                    <h3>
                        Crear Redactor:
                    </h3>
                    <?php $this->view("blog/mensajes") ?>
                    <div class="card-panel ">
                        <div class="row">
                            <form class="col m12 s12" method="POST" action="<?php echo $this->config->base_url() ?>blog/usuario/nuevo">
                                <div class="row">
                                    <div class="col m8" data-focus="true" data-toggle=".descripcion1">
                                        <div class="input-field col s6">
                                            <input id="first_name" name="nombre" type="text" class="validate">
                                            <label for="first_name">Nombre</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="last_name" name="apellido" type="text" class="validate">
                                            <label for="last_name">Apellidos</label>
                                        </div>
                                    </div>
                                    <div class="col m4 descripcion1" >
                                        <div class="card-panel blue lighten-5 text-black ">
                                            <p class="">
                                                Estos datos son importantes ya que se usaran para mostrar el autor en cada entrada del blog 
                                            </p>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s8"  data-focus="true" data-toggle=".descripcion2">
                                        <div class="input-field col s12">
                                            <input id="email" name="correo" type="text" class="validate">
                                            <label for="email">Correo</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="password" name="password" type="password" class="validate">
                                            <label for="password">Contraseña</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="repassword" name="re-password" type="password" class="validate">
                                            <label for="password">Confirmar Contraseña</label>
                                        </div>
                                    </div>
                                    <div class="col m4 descripcion2">
                                        <div class="card-panel blue lighten-5 text-black">
                                            <p class="">
                                                La contraseña y el correo son los datos de acceso a la plataforma, asegurate de que ingresas un correo real para que nosotros podamos enviarle notificaciones
                                            </p>
                                        </div> 
                                    </div>
                                </div>
                                <!-- Switch -->
                                <!--                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="switch">
                                                                    <label>
                                                                        <input type="checkbox" name="send_correo">
                                                                        <span class="lever"></span>
                                                                        Enviar Correo Electronico con datos de acceso
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                <div class="col m8">
                                    <button class="btn pull-right dorado-2" type="submit" >
                                        Guardar
                                    </button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("blog/footer") ?>
        <script>

            $(document).ready(function () {
                $(".descripcion1").children(".card-panel").css({visibility: "hidden"});
                $(".descripcion2").children(".card-panel").css({visibility: "hidden"});
                $("div[data-focus]").each(function (index, value) {
                    var t = value.getAttribute("data-toggle");
                    $(t).children(".card-panel").css({visibility: "hidden"});
                    $(value).find("input").each(function (index, value) {
                        $(value).on("focus", function () {
                            $(t).children(".card-panel").css({visibility: "visible"});
                        });
                        $(value).on("blur", function () {
                            $(t).children(".card-panel").css({visibility: "hidden"});
                        });
                    });
                });
            });

            function seguridad_clave(clave) {
                var seguridad = 0;
                if (clave.length !== 0) {
                    if (tiene_numeros(clave) && tiene_letras(clave)) {
                        seguridad += 30;
                    }
                    if (tiene_minusculas(clave) && tiene_mayusculas(clave)) {
                        seguridad += 30;
                    }
                    if (clave.length >= 4 && clave.length <= 5) {
                        seguridad += 10;
                    } else {
                        if (clave.length >= 6 && clave.length <= 8) {
                            seguridad += 30;
                        } else {
                            if (clave.length > 8) {
                                seguridad += 40;
                            }
                        }
                    }
                }
                return seguridad;
            }


        </script>
    </body>
</html>