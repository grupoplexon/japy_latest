<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proveedores - BrideAdvisor</title>
    <?php $this->view("japy/prueba/header"); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="<?php echo base_url() ?>dist/css/brideAdvisor/style_brideadvisor.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>dist/css/fontawesome-5.8.1/css/all.css" rel="stylesheet" type="text/css"/>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/brideAdvisor/index.css?v=3">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/brideAdvisor/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/slick/slick-theme.css">
</head>
<body>
    <div class="row" style="background-image: url('<?php echo base_url() ?>dist/img/texturaBa.jpg');
                            background-size: cover;">
        <div class="col l12 s12 m12 margin-search">
            <!-- <div id="buscador" class="row  centrado" style="align-items: center;
                justify-content: center;"> -->
                <!-- <img class="hide-on-small-only" src="<?php echo base_url() ?>dist/img/brideadvisor/headProvider.png" style="width: 100%;"> -->
                <?php $this->view("brideadvisor/search") ?>
            <!-- </div> -->
        </div>
        <div class="col l12 s12 margin-content">
            <div class="col l3 s12 padding-mov hide-on-small-only">
                <div class="col l12 s12 white card-shadow center-align">
                    <h5>FILTRAR POR</h5>
                    <!-- <select name="" id="selectState">
                        <option value="Ubicacion"><i class="fas fa-map-marker-alt"></i>Ubicacion</option>
                        <option value="Jalisco">Jalisco</option>
                        <option value="Puebla">Puebla</option>
                        <option value="Queretaro">Queretaro</option>
                        <option value="Todos">Todos</option>
                    </select> -->
                    <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3733.4079591298855!2d-103.39399608563109!3d20.652975286203187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428addaf761b92d%3A0x2f0e04ac2a4f3e36!2sExpo+Guadalajara!5e0!3m2!1ses-419!2smx!4v1559932998554!5m2!1ses-419!2smx" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                    <!-- <p><i class="fas fa-circle"></i>-RANGO DE PRECIO</p> -->
                    <!-- <div class="col l6 margins">
                        <select name="" id="">
                            <option value="Mínimo">Mínimo</option>
                        </select>
                    </div>
                    <div class="col l6 margins">
                        <select name="" id="">
                            <option value="Máximo">Máximo</option>
                        </select>
                    </div> -->
                    <p><!--<i class="fas fa-circle"></i>-->CALIFICACIÓN</p>
                    <div class="col l12 s12 m12 center-align">
                    <fieldset class="rating" style="width: 100%;">
                        <input type="radio" id="star5" name="rating" class="rating" value="5" required /><label class = "full" for="star5" title="5 stars"></label>

                        <input type="radio" id="star4" name="rating" class="rating" value="4" /><label class = "full" for="star4" title="4 stars"></label>

                        <input type="radio" id="star3" name="rating" class="rating" value="3" /><label class = "full" for="star3" title="3 stars"></label>

                        <input type="radio" id="star2" name="rating" class="rating" value="2" /><label class = "full" for="star2" title="2 stars"></label>

                        <input type="radio" id="star1" name="rating" class="rating" value="1" /><label class = "full" for="star1" title="1 star"></label>
                    </fieldset>
                    </div>
                    <br><br>
                    <!-- <p>-CATEGORÍA</p> -->
                    <!-- <div class="autocomplete"> -->
                        <input class="inputs width" id="searchCategory" type="hidden" name="myCountry" placeholder="Nombre de la categoria">
                    <!-- </div> -->
                    <div class="col l12 center-align">
                        <p class="btn blue" id="filter">Aplicar</p>
                    </div>
                </div>
            </div>
            <div class="col l9" id="cards">
                
            </div>
            <div class="col l9 offset-l3 center-align more">
                    <p class="onLoad">Cargando...</p>
            </div>
        </div>
    </div>
</body>
<!-- <script src="<?php echo base_url() ?>dist/js/brideadvisor/jquery-ui.js"></script> -->
<?php $this->view("japy/prueba/footer") ?>
<script src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/brideadvisor/proveedores.js?v=6"></script>
</html>