<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $info->nombre ?> - BrideAdvisor</title>
    <?php $this->view("japy/prueba/header"); ?>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="<?php echo base_url() ?>dist/css/brideAdvisor/style_brideadvisor.css" rel="stylesheet" type="text/css"/>
    <!-- <link href="<?php echo base_url() ?>dist/css/fontawesome-5.8.1/css/all.css" rel="stylesheet" type="text/css"/> -->
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/brideAdvisor/profile.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/brideAdvisor/lightslider.css"/>
    <!-- <link href="<?php echo base_url() ?>dist/css/brideAdvisor/starrr.css" rel=stylesheet/> -->
    <!-- <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/brideAdvisor/jquery-ui.css"> -->
</head>
<body>
    <div class="row" style="background-image: url('<?php echo base_url() ?>dist/img/texturaBa.jpg');
                            background-size: cover;">
        <div class="col s12 hide-on-large-only margin-mov" style="margin-top: 15vh;">
            <div class="col s4">
                <img src="<?= base_url() ?>uploads/images/<?= $galeria[0]->nombre ?>" style="width: 100%;">
            </div>
            <div class="col s8">
                <?php if ( $this->checker->isLogin()) { ?>
                    <img id="like-mv" onclick="M.toast({html: 'Post Guardado (:'})" style="width: 40px;" class="like tooltipped" data-tooltip="♡ Guardar en Mis Proveedores" src="<?php echo base_url() ?>dist/img/brideadvisor/megusta.png">  
                    <img id="like-mv2" onclick="M.toast({html: 'Post Eliminado :('})" style="width: 40px;" class="like like2 " src="<?php echo base_url() ?>dist/img/brideadvisor/megusta02.png">  
                <?php } else { ?>
                    <img onclick="M.toast({html: 'Inicia sesion para guardar'})" class="like tooltipped" data-tooltip="♡ Guardar en Mis Proveedores" src="<?php echo base_url() ?>dist/img/brideadvisor/megusta.png">
                <?php } ?>
                <h6 class="color"><?= $info->nombre ?></h6>
                <h6 class="color"><i class="fas fa-map-marker-alt"></i>&nbsp;<?= $info->localizacion_direccion ?></h6>
                <h6 class="color"><i class="fas fa-phone"></i>&nbsp;<?= $info->contacto_telefono ?></h6>
            </div>
        </div>
        <div class="col l12 margin-body">
            <div class="col l5 s12">
                <div class="item">            
                    <div class="clearfix" style="max-width:474px;">
                        <ul id="image-gallery" style="height: 350px !important;" class="gallery list-unstyled cS-hidden">
                            <?php if(!empty($video)) { ?>
                                <li data-thumb="<?= base_url() ?>uploads/images/<?= $galeria[0]->nombre ?>">
                                    <video src="<?= base_url() . "uploads/videos/" . $video->nombre ?>" autoplay="true" controls style="width: 100%;"></video>
                                </li>
                            <?php } ?>
                            <?php foreach ($galeria as $key => $value) { ?>
                                <?php if($value->principal!=1 && $value->tipo!="VIDEO") {?>
                                <li data-thumb="<?= base_url() ?>uploads/images/<?= $value->nombre ?>"> 
                                    <img src="<?= base_url() ?>uploads/images/<?= $value->nombre ?>" style="width: 100%; object-fit: cover;"/>
                                </li>
                            <?php } } ?>
                        </ul>
                    </div>
                </div>
            </div> 
            <div class="col l7 s12">
                <div class="col l3 hide-on-small-only">
                    <img src="<?= base_url() ?>uploads/images/<?= $galeria[0]->nombre ?>" style="width: 100%;">
                </div>
                <div class="col l9 hide-on-small-only">
                    <?php if ( $this->checker->isLogin()) { ?>
                        <img id="like" onclick="M.toast({html: 'Post Guardado (:'})" class="like tooltipped" data-tooltip="♡ Guardar en Mis Proveedores" src="<?php echo base_url() ?>dist/img/brideadvisor/megusta.png">  
                        <img id="like2" onclick="M.toast({html: 'Post Eliminado :('})"  class="like like2 " src="<?php echo base_url() ?>dist/img/brideadvisor/megusta02.png">  
                    <?php } else { ?>
                        <img onclick="M.toast({html: 'Inicia sesion para guardar'})" class="like tooltipped" data-tooltip="♡ Guardar en Mis Proveedores" src="<?php echo base_url() ?>dist/img/brideadvisor/megusta.png">
                    <?php } ?>
                    <br><br>
                    <h5 class="color"><?= $info->nombre ?></h5>
                    <h6 class="color"><i class="fas fa-map-marker-alt"></i>&nbsp;<?= $info->localizacion_direccion ?></h6>
                    <h6 class="color"><i class="fas fa-phone"></i>&nbsp;<?= $info->contacto_telefono ?></h6>
                </div>
                <div class="col l12 hide-on-small-only">
                    <br><hr>
                </div>
                <div class="col l12">
                    <h5 class="color">DESCRIPCIÓN</h5>
                    <h6 class="color"><?= $info->descripcion ?></h6>
                </div>
            </div>
        </div>
        <div class="col l12 margin-body">
            <div class="col l5 s12">
                <?php if(count($promociones)!=0){ ?>   
                <div class="col l12 s12" style="margin-bottom: 4vh;">
                    <div class="col l12 s12 grey darken-3 center-align">
                        <h6 style="color: white;">PROMOCIONES</h6>
                    </div>
                    <div class="col l12 white card-shadow">
                    <?php foreach ($promociones as $key => $promo) { ?>
                        <div class="col l4">
                            <br>
                            <img class="responsive-img" style="width:100%; height:100%;" 
                                id="imagen"  src="<?php  echo "data:image/$promo->mime_imagen;base64,".base64_encode($promo->imagen) ?>">                
                        </div>
                        <div class="col l8">
                            <h6 class="title-review"><?php echo $promo->nombre ?></h6>
                            <h6 class="" style="text-align:justify;"><?php echo $promo->descripcion ?></h6>
                            <h6 class="grey-light"> <s> Antes: $<?php echo $promo->precio_original ?> </s></h6>
                            <h6 class="title-review">AHORA: $<?php echo $promo->precio_desc ?></h6>
                            <h6 class="" style="font-size: 1rem;">Promoción valida hasta el <b> <?php $newDate = date("d/m/Y", strtotime($promo->fecha_fin)); echo $newDate;  ?> </b></h6>
                        </div>
                    <?php } ?>   
                    </div>
                </div>
                <?php } ?>   
                <ul id="tabs-swipe-demo" class="tabs">
                    <li class="tab col s3"><a class="active pestana" href="#test-swipe-1">Calificar</a></li>
                    <li class="tab col s3"><a class="pestana" href="#test-swipe-2">Reseñas</a></li>
                </ul>
                <div id="test-swipe-1" class="col l12 s12 card-shadow white center-align">

                    <div class="row">
                        <br>
                        <h6 class="title-review">Califica este proveedor</h6>
                    </div>
                <?php if ( $this->checker->isLogin()) { ?>
                <div class="row   center-align" >
                    <form method="POST" id="sendReview" >
                        <div class="col l12 s12 center-align">
                            <fieldset class="rating" style="width: 100%;">
                                <input type="radio" id="star5" name="rating" class="rating" value="5" required /><label class = "full" for="star5" title="5 stars"></label>

                                <input type="radio" id="star4" name="rating" class="rating" value="4" /><label class = "full" for="star4" title="4 stars"></label>

                                <input type="radio" id="star3" name="rating" class="rating" value="3" /><label class = "full" for="star3" title="3 stars"></label>

                                <input type="radio" id="star2" name="rating" class="rating" value="2" /><label class = "full" for="star2" title="2 stars"></label>

                                <input type="radio" id="star1" name="rating" class="rating" value="1" /><label class = "full" for="star1" title="1 star"></label>
                            </fieldset>
                        </div>
                        <div class="input-field col l12 s12 center-align">
                            <input id="mensaje" name="mensaje" type="text" class="validate">
                            <label for="mensaje">Describe tu experiencia</label>
                            <input type="hidden" value="<?php echo $info->id_proveedor ?>" id="proveedor" name="proveedor">
                        </div>
                        <div class="col l12 s12 center-align">
                                <button class="btn grey darken-3" id="" type="submit">PUBLICAR</button>
                        </div>
                    </form>          
                </div>
                <?php } else { ?>
                    <div class="row   center-align">
                        <h6 class="">Inicia sesión para dejar un comentario</h6>
                    </div>
                <?php } ?>
                </div>
                <div id="test-swipe-2" class="col l12 s12 card-shadow white" style="height:300px; overflow: scroll;">

                    <div class="col l12 s12">
                        <br>
                        <h6 class="title-review center-align">Reseñas y calificaciones</h6>
                        <?php if($reviews!="[]") { ?>
                            <div class="row" >
                                <?php foreach ($reviews as $review) { ?>
                                    <h6 class="title-review"><i class="far fa-user-circle"></i> <?= $review->nombre ?></h6>
                                    <?php for($i=1; $i<= 5 ; $i++) {
                                        if($i<=$review->calificacion) { ?>
                                            <span class="fa fa-star checked"></span>
                                        <?php } else{ ?>
                                            <span class="fa fa-star"></span>
                                        <?php }  } ?>
                                    <h6> <?= $review->mensaje ?> </h6>
                                    <hr style="border: 1px solid;">
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="row center-align">
                                <br><br>
                                <h6>Este proveedor aun no tiene reseñas.</h6>
                                <h6>Se el primero en dejar su opinión.</h6>
                            </div>
                        <?php } ?>
                    </div>
                    
                </div>
            </div>
            <?php if ( $this->checker->isLogin()) { ?>
            <div class="col l7 s12 margin-card-mov">
                <div class="col l12">
                    <div class="col l12 s12 grey darken-3 center-align">
                        <h6 style="color: white;">SOLICITA COTIZACIÓN CON <?= strtoupper($info->nombre); ?></h6>
                    </div>
                    <div class="col l12 white card-shadow">
                        <form method="POST" id="sendMessage" >
                            <br>
                            <input type="hidden" value="<?php echo $info->id_proveedor ?>" id="proveedor">
                            <input type="hidden" name="asunto" value="Solicitud de informaci&oacute;n">
                            <div class="col l2 s3">
                                <h6>Mensaje:</h6>
                            </div>
                            <div class="col l10 s9">
                                <input class="inputs" type="text" id="mensaje">
                            </div>
                            <div class="col l12 s12 center-align">
                                <button class="btn grey darken-3" type="submit">ENVIAR CONSULTA</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php } else { ?>
                <div class="col l7 s12 margin-card-mov">
                <div class="col l12">
                    <div class="col l12 grey darken-3 center-align">
                        <h6 style="color: white;">SOLICITA COTIZACIÓN CON <?= strtoupper($info->nombre); ?></h6>
                    </div>
                    <div class="col l12 white card-shadow center-align">
                        <h5>PARA PONERTE EN CONTACTO DEBES INGRESAR A TU CUENTA</h5>
                        <!-- <form id="sendMessage">
                            <br>
                            <div class="col l2 s3">
                                <h6>Nombre:</h6>
                            </div>
                            <div class="col l10 s9">
                                <input class="inputs" type="text" required>
                            </div>
                            <div class="col l2 s3">
                                <h6>Email:</h6>
                            </div>
                            <div class="col l10 s9">
                                <input class="inputs" type="email" required>
                            </div>
                            <div class="col l2 s3">
                                <h6>Mensaje:</h6>
                            </div>
                            <div class="col l10 s9">
                                <input class="inputs" type="text" required>
                            </div>
                            <div class="col l12 s12 center-align">
                                <button class="btn grey darken-3" type="submit">ENVIAR CONSULTA</button>
                            </div>
                        </form> -->
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col s12 pink lighten-5 center-align hide-on-large-only margin-top">
            <h6 class="text">NOTICIAS Y ACTUALIZACIONES</h6>
        </div>
        <div class="col s12 hide-on-large-only center-align margins-register">
            <br>
            <h6 class="text">REGÍSTRATE</h6>
            <input class="inputs" type="text" placeholder="Introduce tu correo electrónico">
            <br><br>
            <button class="btn pink lighten-5" style="color:grey">SUSCRIBIRME</button>
        </div>
    </div>
    <input type="hidden" value="<?php echo $info->id_proveedor ?>" id="proveedor">
    <input type="hidden" value="<?php echo base_url() ?>" id="base-url">
</body>
<!-- <script src="<?php echo base_url() ?>dist/js/brideadvisor/jquery-ui.js"></script> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<?php $this->view("japy/prueba/footer") ?>
<!-- <script src="<?php echo base_url() ?>dist/js/brideadvisor/proveedores.js"></script> -->
<script src="<?php echo base_url() ?>dist/js/brideadvisor/lightslider.js"></script>
<script src="<?php echo base_url() ?>dist/js/brideadvisor/profile_provider.js"></script>
<script>
    const baseUrl = $('#base-url').val();
    $( document ).ready(function() {
    
    $('.tooltipped').tooltip();
    
    like();
});
    function like(){
        $.ajax({
            url: baseUrl + 'proveedores/like_proveedor',
            dataType: 'json',
            method: 'post',
            data: {
                proveedor: $('#proveedor').val(),
            },
            success: function(response) {
                if(response.data=='ok') {
                    $('#like').hide();
                    $('#like2').show();

                    $('#like-mv').hide();
                    $('#like-mv2').show();
                    
                } else {
                    $('#like2').hide();
                    $('#like').show();

                    $('#like-mv2').hide();
                    $('#like-mv').show();
                }
            },
            error: function() {
                console.log('error');
            }
        });
    }
    $( ".like" ).click(function() {
        $.ajax({
            url: baseUrl + 'proveedores/create_like',
            dataType: 'json',
            method: 'post',
            data: {
                proveedor: $('#proveedor').val(),
            },
            success: function(response) {
                if(response.data=='ok') {
                    $('#like').hide();
                    $('#like2').show();

                    $('#like-mv').hide();
                    $('#like-mv2').show();
                }
            },
            error: function() {
                console.log('error');
            }
        });
    });
    $( ".like2" ).click(function() {
        $.ajax({
            url: baseUrl + 'proveedores/delete_like',
            dataType: 'json',
            method: 'post',
            data: {
                proveedor: $('#proveedor').val(),
            },
            success: function(response) {
                if(response.data=='ok') {
                    $('#like2').hide();
                    $('#like').show();

                    $('#like-mv2').hide();
                    $('#like-mv').show();
                }
            },
            error: function() {
                console.log('error');
            }
        });
    });
</script>
</html>