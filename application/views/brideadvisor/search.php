<div class="container space-search">
    <!-- <div class="row center-align hide-on-small-only">
        <h5 class="title space" style="color: #515151;">NOVIA</h5> 
        <div class="vl"></div>
        <h5 class="title space" style="color: #515151;">NOVIO</h5>
        <div class="vl"></div>
        <h5 class="title space" style="color: #515151;">BODA</h5>
        <div class="vl"></div>
        <h5 class="title space" style="color: #515151;">DESTINOS DE BODA</h5>
        <div class="vl"></div>
        <h5 class="title space" style="color: #515151;">HOGAR</h5>
    </div> -->
    <div class="hide-on-large-only center-align top">
        <hr>
        <h5>PROVEEDORES</h5>
        <hr>
        <p>Encuentra todo lo que necesitas para celebrar tu boda. Proveedores como fotógrafos, invitaciones, coches de bodas, restaurantes, hoteles, haciendas, salones y jardines, vestidos, trajes y complementos para los novios.</p>
        <br>
        <div class="col s12 pink lighten-5" style="height: 15px;">

        </div>
        <br>
        <h5 style="color: #515151;">¿Qué estas buscando?</h5>
    </div>
    <!-- <div class=""> -->
        <!-- <div class="col s12 m12 l12 alinear margin-top" style=""> -->
            <div class="col l1 margins right-align hide-on-small-only">
                <p class="btn searchProvider" style="margin: 0 !important;" id="search"> <i class="fas fa-search " style="margin-top: 8px;"></i></p>
            </div>
            <div class="col s7 l8 margins">
                <input id="btn-search" type="text" placeholder="¿Qué estas buscando?"  value="<?php if(!empty($_GET["search"])) {echo $_GET["search"];} ?>">
                <input type="hidden" id="url" value="<?php if(!empty($_GET["search"])) {echo $_GET["search"];} ?>">
                <input type="hidden" value="<?php if(!empty($_GET["estado"])) {echo $_GET["estado"];} ?>" id="prueba">
            </div>
            <div class="col s5 l3 margins">
                <select name="estado"  class="browser-default  col l4" id="stateId" style="width: 100%;">
                    <option id="Todos" value="Todos">Todos</option>
                    <option id="Jalisco" value="Jalisco">Jalisco</option>
                    <option id="Puebla" value="Puebla">Puebla</option>
                    <option id="Querétaro" value="Querétaro">Querétaro</option>
                </select>
            </div>
            <div class="col s12 hide-on-large-only center-align">
                <p class="btn pink lighten-5" style="color:black;" id="searchMov">Buscar</p>
            </div>
        <!-- </div> -->
    <!-- </div> -->
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    var searchProvider = 0, page=0, visible = 0, charge = 0;
    var busqueda = '';
    $(document).ready(function() {
        $("#btn-search").focus();
        if($('#url').val()!='') {
            busqueda = $('#url').val();
            search($('#url').val());
        }
    });

    $( "#btn-search" ).autocomplete({
        source: '<?php echo base_url(); ?>proveedores/providers',
        select: (event, ui) => {
            busqueda = ui.item.value;
            page = 0;
            visible = 0;
            searchProvider = 0;
            search(busqueda);
        }
    });

    $('#stateId').on('change', function() {
        if($('#btn-search').val()!='') {
            page = 0;
            visible = 0;
            searchProvider = 0;
            search($('#btn-search').val());
        } else {
            pageCategories = 0;
            providers($('#stateId').val(), 'select');
        }
    });

    // $('#search').on('click', function() {
    //     // alert($("#btn-search").val()+', '+$('#stateId').val());
    //     search();
    // });

    // $('#searchMov').on('click', function() {
    //     search();
    // });

    function search(busqueda) {
        // $('#searchCategory').val($("#btn-search").val());
        $('.onLoad').text('Cargando...');
        $('.more').removeClass('oculto');
        categories('select', busqueda);
    //    $.ajax({
    //         dataType: 'json',
    //         url: '<?= base_url() ?>proveedores/searchProvider',
    //         method: 'post',
    //         data: {
    //             name: busqueda,
    //             state: $('#stateId').val()
    //         },
    //         success: function(response) {
    //             if(response.info!='not found') {
    //                 searchProvider = 1;
    //                 var options="", crear = '', div=2;
    //                 options = '<div class="col l12">';
    //                     options+='<div class="col l12">';
    //                         options+='<div class="col l12">';
    //                             options+='<div class="col l8">';
    //                                 options+='<h4 class="bold">'+response.category+'</h4>';
    //                             options+='</div>';
    //                         options+='</div>';
    //                         for (let i = 0; i < response.info.length; i++) {
    //                             if(i%3==0 && i!=0) {
    //                                 options+='<div class="col s12" style="height: 30px;"></div>';
    //                             }
    //                             options+='<div class="col l4">';
    //                                 options+='<div class="col l12 white card-shadow center-align height margins">';
    //                                     if(response.image!=null) {
    //                                         options+='<a href="<?= base_url() ?>boda-'+response.info[i].slug+'"><img src="https://brideadvisor.mx/uploads/images/'+response.info[i].namephoto+'" style="width: 100%; object-fit: cover; height: 30vh;"></a>';
    //                                     } else {
    //                                         options+='<a href="<?= base_url() ?>boda-'+response.info[i].slug+'"><img src="https://brideadvisor.mx/dist/img/user.jpg" style="width: 100%; object-fit: cover; height: 30vh;"></a>';
    //                                     }
    //                                     options+='<a class="names" href="<?= base_url() ?>boda-'+response.info[i].slug+'"><h6 class="size bold-name">'+response.info[i].nombre+'</h6></a>';
    //                                     options+='<p>Recomendaciones</p>';
    //                                     options+='<i class="far fa-star"></i>&nbsp;&nbsp;';
    //                                     options+='<i class="far fa-star"></i>&nbsp;&nbsp;';
    //                                     options+='<i class="far fa-star"></i>&nbsp;&nbsp;';
    //                                     options+='<i class="far fa-star"></i>&nbsp;&nbsp;';
    //                                     options+='<i class="far fa-star"></i>';
    //                                     options+='<p><i class="fas fa-map-marker-alt"></i>&nbsp;'+response.info[i].localizacion_direccion+'</p>';
    //                                 options+='</div>';
    //                             options+='</div>';
    //                         }
    //                     options+='</div>';
    //                 options+='</div>';
    //                 document.getElementById("cards").innerHTML = options;
    //                 $('.more').addClass('oculto');
    //             } else {
    //                 $('#searchCategory').val($("#btn-search").val());
    //                 $('.more').removeClass('oculto');
    //                 categories('select');
    //             }
    //         },
    //         error: function() {
    //             console.log('error');
    //         }
    //     });
    }
</script>