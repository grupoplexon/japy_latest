<div class="row">
    <br><br>
    <div class="col s12 l10 offset-l1">
        <div class="row jcarousel-cities center">
            <?php foreach($cities as $val) { ?>
                <div class="col s12 m2 l4-jcaroul post">
                    <a href="<?= base_url() ?>brideweekend/ciudad/<?= $val->city ?>">
                        <div class="col l12 m12 s12" 
                        style="
                            background-image: url('<?= base_url() ?>uploads/images/brideweekend/<?= $val->img_enclosure ?>');
                            background-size: cover;
                            background-repeat: no-repeat;
                            height: 350px;
                            width: 100% !important;
                        ">
                            <h4 class="white-text title-carousel"><?= $val->city ?></h4> 
                            <h6 class="white-text title-carousel size-subtitle"><?= $val->dateFormat ?></h6>
                            <div class="div-logo">
                                    <img class="img img-carousel" src="<?= base_url() ?>uploads/images/brideweekend/<?= $val->logo_enclosure ?>">
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>