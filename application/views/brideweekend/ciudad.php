<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BrideWeekend - <?= $city->city ?></title>
    <?php $this->view("brideweekend/header"); ?>
    <link rel="stylesheet" href="<?= base_url() ?>dist/css/brideweekend/ciudad.css">
</head>
<body>
    <div class="row">
        <br>
        <div class="col l12 m12 s12 width-head"
        style="background-image: url('<?= base_url() ?>uploads/images/brideweekend/<?= $city->img_head ?>');
            height: 350px !important;
            background-size: cover !important;
            background-repeat: no-repeat !important;
            padding: 0px !important;">
            <div class="col l12 m12 s12 center-align background-title">
                <h3 class="white-text"><?= $city->city ?></h3>
                <hr class="hr-city">
                <h5 class="white-text"><?= $city->enclosure.', '.$city->enclosure_description ?></h5>
            </div>
        </div>
        <div class="col l6 offset-l3">
            <div class="col l12 m12 s12 card">
                <div class="col l4 height-logo">
                    <img class="img img-logo" src="<?= base_url() ?>uploads/images/brideweekend/<?= $city->logo_enclosure ?>">
                    <br><br>
                </div>
                <div class="col l8">
                    <br>
                    <h5 class="size-date bold center-align"><?= $date ?></h5>
                    <p class="bold center-align size-schedule"><?= $schedule ?></p>
                    <p class="bold center-align size-schedule"><?= $city->enclosure_address ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row paddings">
        <div class="col l6 s12 m6 paddings">
            <img class="img object large-location" src="<?= base_url() ?>uploads/images/brideweekend/<?= $city->img_enclosure ?>">
        </div>
        <div class="col l6 s12 m6 paddings">
            <iframe class="map" src="<?= $city->map ?>"></iframe>
        </div>
    </div>
    <input type="hidden" id="url" value="<?= base_url() ?>">
    <?php if($city->organized_by=='plexon') { ?>
        <?php $this->view('brideweekend/contactLOC') ?>
    <?php } else { ?>
        <?php $this->view('brideweekend/contactEXT') ?>
    <?php } ?>
</body>
<?php $this->view("brideweekend/footer"); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?= base_url() ?>dist/js/brideweekend/charging.js"></script>
<script src="<?= base_url() ?>dist/js/brideweekend/contact.js"></script>
</html>