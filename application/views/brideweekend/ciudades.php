<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ciudades Brideweekend</title>
    <?php $this->view("brideweekend/header"); ?>
    <?php setlocale(LC_ALL,"es_ES"); ?>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/brideweekend/ciudades.css">
</head>
<body>
    <div class="row center-align">
        <h3>PROXIMOS EVENTOS</h3>
        <br>
        <div class="col l8 offset-l2 s8 offset-s2 calendario"> <h5>CALENDARIO 2010</h5></div>
        <div class="row ciudades">
            <br>
            <?php foreach($ciudades as $c){ ?>
                <a href="<?php echo base_url() ?>brideweekend/ciudad/<?= $c->city; ?>" class="col l4 s12 m6 div-ciudad" style="">
                    <div class="ciudad" style="background: url(<?php echo base_url() ?>uploads/images/brideweekend/<?= $c->img_enclosure; ?>);">
                        <div class="grises">
                            <img class="recinto" src="" alt="">
                                <h5> <?= $c->city; ?></h5>
                                <?php if($c->mes_initial == $c->mes_final){ ?>
                                    <h6> <?php echo $c->mes_initial;?> <?php echo $c->dia_initial;?> & <?php echo $c->dia_final;?> </h6>
                                <?php }else{ ?>
                                    <h6> <?php echo $c->mes_initial;?> <?php echo $c->dia_initial;?> &  <?php echo $c->mes_final;?> <?php echo $c->dia_final;?> </h6>
                                <?php } ?>
                                <div class="div-logo">
                                    <img class="img-logo" src="<?php echo base_url() ?>uploads/images/brideweekend/<?= $c->logo_enclosure; ?>" alt="">
                                </div>
                        </div>
                    </div>
                </a>
            <?php } ?>
        </div>
    </div>
</body>
<?php $this->view("brideweekend/footer"); ?>
</html>