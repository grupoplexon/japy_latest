<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url() ?>dist/fontawesome/css/all.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/brideweekend/footer.css">
</head>

<body>
    <div class="footer row center-align">
        <div class="col l3 s6"> <img src="<?php echo base_url() ?>dist/img/brideweekend2/AMPROFEC.png" alt=""> </div>
        <div class="col l3 s6"> <a href="https://www.nupcialmexicana.com/"><img src="<?php echo base_url() ?>dist/img/brideweekend2/LOGO_C.png" alt=""> </a> </div>
        <div class="col l3 s6" > <a href="https://brideadvisor.mx/"> <img src="<?php echo base_url() ?>dist/img/brideweekend2/LOGO_N.png" alt=""> </a> </div>
        <div class="col l3 s6"> <img src="<?php echo base_url() ?>dist/img/brideweekend2/IWA-Logo.png" alt=""> </div>
    </div>
    <div class="footer-copyright row center-align vertical-align" >
            <div class="col l4 s12 m6">
                <img class="logo-footer" src="<?php echo base_url() ?>dist/img/brideweekend2/logo.png">
            </div>
            <div class="col l4 s12 m6">
                <h6>CONTÁCTANOS:</h6>
                <h6>contacto@brideweekend.com</h6>
                <h6>01 800 7191 421</h6>
            </div>
            <div class="col l4 s12 m12">
                <h5> <a href="https://www.facebook.com/brideweekendmx/"><i class="fab fa-facebook-square"></i>&nbsp;&nbsp; </a>
                <a href="https://www.instagram.com/brideweekendbodas/"><i class="fab fa-instagram"></i>&nbsp;&nbsp; </a>
                <i class="fab fa-whatsapp"></i></h5>
                <h6>© 2019 Bride Weekend</h6>
            </div>
    </div>
</body>
</html>