<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Brideweekend</title>
    <?php $this->view("brideweekend/header"); ?>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/brideweekend/index.css">
</head>
<body>
    <div class="row" id="home" >
        <div class="slider slide">
            <ul class="slides slide2">
                <li>
                    <img src="<?php echo base_url() ?>dist/img/brideweekend2/Slider.png" style="cursor: pointer;">
                </li>
            </ul>
        </div>
    </div>
    <div class="row center-align container">
        <h4>¡Consulta los proximos eventos en tu ciudad!</h4>
        <div class="ciudad">
            <h5>Selecciona tu ciudad</h5>
        </div>
        <div class="select-ciudad">
            <select id="ciudad">
            <option value="" disabled selected> ______________</option>7
            <?php foreach($ciudades as $ciu) { ?>
                <option value="<?php echo $ciu->city?>"> <?php echo $ciu->city?></option>
            <?php } ?>
            </select>
        </div>
    </div>
    <div class="row banner center-align">
        <a href="https://brideadvisor.mx/"><img src="<?php echo base_url() ?>dist/img/brideweekend2/banner01.png" alt=""></a>
    </div>
    <div class="row center-align galeria">
        <br>
        <h4>EXPERIENCIA BRIDE WEEKEND</h4>
        <br>
        <video src="<?php echo base_url() ?>dist/img/brideweekend2/slider.mp4" autoplay controls></video>
    </div>
    <input type="hidden" id="url" class="hidden" value="<?php echo base_url() ?>">
</body>
<?php $this->view("brideweekend/footer"); ?>
<script src="<?= base_url() ?>dist/js/brideweekend/index.js"></script>
<script>

</script>
</html>