<div class="row">
    <h4 > PRÓXIMOS EVENTOS </h4>
    <div class="row jcarousel-cities center">
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/new_york">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/newyork_carousel.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/new_york">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> NEW YORK </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">NYCB LIVE</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Home of the Nassau Veterans Memorial Coliseum</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/new_york"><h4> 04 Y 05 ENERO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/chicago">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/chicago_carousel.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/chicago">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> CHICAGO </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Donald E. Stephens</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Convention Center</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/chicago"><h4> 04 Y 05 ENERO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/monterrey">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/monterrey_carousel.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/monterrey">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> MONTERREY </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Centro Convex</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;"><br></h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/monterrey"><h4> 11 Y 12 ENERO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/expo_gdl">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/guadalajara_carousel.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/expo_gdl">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> GUADALAJARA </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Expo Guadalajara</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;"><br></h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/expo_gdl"><h4> 25 Y 26 ENERO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/expo_leon">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/recinto_leon.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/expo_leon">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> LEON </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">LA CASA DE PIEDRA</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;"><br></h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/expo_leon"><h4> 08 Y 09 FEBRERO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/expo_qro">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/queretaro.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/expo_qro">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> QUERETARO </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Centro de Congresos</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;"><br></h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/expo_qro"><h4> 22 Y 23 FEBRERO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/expo_puebla">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/puebla.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/expo_puebla">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> PUEBLA </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Centro Expositor</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Los Fuertes</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/expo_puebla"><h4> 29 FEBRERO 01 MARZO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/phoenix">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/phoenix.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/phoenix">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> PHOENIX </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Arizona State</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Fairgrounds</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/phoenix"><h4> 07 Y 08 MARZO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/expo_cdmx">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/mexico.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/expo_cdmx">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> CDMX </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">CENTRO <br> CITIBANAMEX</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;"><br></h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/expo_cdmx"><h4> 21 Y 22 MARZO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/san_diego">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/sandiego.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/san_diego">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> SAN DIEGO </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">San Diego Convention Center</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Hall C1</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/san_diego"><h4> 29 MARZO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/houston">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/houston.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/houston">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> Houston </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">George R. Brown</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Convention Center Hall A3</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/houston"><h4> 04 Y 05 ABRIL </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/atlanta">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/atlanta.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/atlanta">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> Atlanta </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Georgia World Congress Center</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Hall C1</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/atlanta"><h4> 18 Y 19 ABRIL </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/san_luis">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/slpotosi.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/san_luis">
                    <h4 class="white-text" style="font-size: 30px; text-shadow: 2px 2px 2px black;"> SAN LUIS POTOSI </h4>
                    <h4 class="white-text" style="font-size: 23px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Centro de Covenciones</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">San Luis Potosi</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/san_luis"><h4> 30 Y 31 MAYO </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/los_angeles">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/losangeles.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/los_angeles">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> LOS ÁNGELES </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">Los Angeles Convention Center</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;">South Hall K</h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/los_angeles"><h4> 03 Y 04 OCTUBRE </h4></a>
            </div>
        </div>
        <div class="col s12-m2-l4-jcaroul post">
            <a href="<?php echo base_url() ?>brideweekend/san_antonio">
                <img class="img-home " style="width:100%; height:359px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/sanantonio.png">
            </a>
            <div class="texto-encima">
                <a href="<?php echo base_url() ?>brideweekend/san_antonio">
                    <h4 class="white-text" style="text-shadow: 2px 2px 2px black;"> SAN ANTONIO </h4>
                    <h4 class="white-text" style="font-size: 26px; margin: 0px !important;text-shadow: 2px 2px 2px black;">FREEMAN COLISEUM</h4>
                    <h4 class="white-text" style="font-size: 17px; margin: 0px !important;text-shadow: 2px 2px 2px black;"><br></h4>
                </a>
            </div>
            <div class="centrado">
                <a class="white-text" href="<?php echo base_url() ?>brideweekend/san_antonio"><h4> 01 NOVIEMBRE </h4></a>
            </div>
        </div>
    </div>
</div>