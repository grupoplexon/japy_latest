<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    
    <title>BW CDMX</title>
    <?php $this->view("brideweekend/header_ciudades"); ?>
</head>
<style>
.body{
    font-family: 'Roboto', sans-serif;
}
.content {
    padding-left: 5vh;
    padding-right: 5vh;
    padding-top: 3vh;
    padding-bottom: 1vh;
}
.contentsecond {
    padding-left: 5vh;
    padding-right: 5vh;
    padding-top: 0vh;
    padding-bottom: 0vh;
}
.alinear {
    padding-left: 0 !important;
    padding-right: 0 !important;
    padding-top: 0 !important;
    padding-bottom: 0 !important;
}
.carousel .carousel-item {
    visibility: unset !important;
}
.imgrecinto {
    height: 500px;
}
.map {
    width: unset;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px) {
    .imgrecinto {
        height: unset !important;
    }
    .map {
        width: 100% !important;
    }
    .content {
    padding-left: 0vh;
    padding-right: 0vh;
    padding-top: 0vh;
    padding-bottom: 0vh;
    }
    .contentsecond {
        padding-left: 0vh;
        padding-right: 0vh;
        padding-top: 0vh;
        padding-bottom: 0vh;
    }
}
.slick-prev, .slick-next {
    color: #515151;
}
.file-field .btn{
    border-radius: 1px;
    width: 40%;
}
.file-field .btn, .btn-large:hover {
    background-color: #848484;
}
input {
    box-sizing: border-box !important;
}
input[type=text]:not(.browser-default){
    margin: 0 0 0px 0;
}
input {
    display: block !important;
    font-size: 14px !important;
    line-height: 1.42857143 !important;
    color: #555 !important;
    background-color: #fff !important;
    background-image: none !important;
    border: 1px solid #ccc !important;
    border-radius: 2px !important;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075) !important;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075) !important;
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s !important;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
    padding-left: 5px !important;
}
.btn{
    background-color: #848484;
    border-radius: 1px;
}
#WindowLoad
{
    position:fixed;
    top:0px;
    left:0px;
    z-index:3200;
    filter:alpha(opacity=65);
   -moz-opacity:65;
    opacity:0.9;
    background:#999;
}
hr{ 
  border: 2px solid;
  color: #8dd7e0;
  align: center;
  width: 5%
}
</style>
<body>
    <img src="<?php echo base_url() ?>dist/img/brideweekend/headcdmx.jpg" style="width: 100%;">
    <div class="row" id="contacto">
        <div class="row center-align"id="form">
            <!-- <form method="POST" class="col  s12  m12 l12 "
                action="<?php echo base_url() ?>index.php/Brideweekend/registro"> -->
            <!-- <div class="col  s12  m12 l12 "> -->
                <div class="row " id="">
                    <h4 style="font-weight: 900;" > REGÍSTRATE Y GANA</h4>
                    <hr>
                    <div class="col s12 m6 l5 offset-l1" >
                        <div class="file-field input-field " style="margin-bottom: 35px; padding-top: 55px;">
                            <div class="btn">
                                <span>Nombre</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" name="nombre" id="nombre" class="form-control" required>
                            </div>
                        </div>
                        <div class="file-field input-field " style="margin-bottom: 35px;">
                            <div class="btn">
                                <span>Email</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input id="correo" name="correo" type="text" class="validate" required>
                            </div>
                        </div>
                        <div class="file-field input-field  " style="margin-bottom: 35px;">
                            <div class="btn">
                                <span>Telefono</span>
                            </div>
                            <div class="file-path-wrapper">
                                <!-- <input type="number" name="telefono" id="telefono" class="form-control" required> -->
                                <input name="telefono" id="telefono" class="form-control" required type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                            </div>
                        </div>
                        <div class="file-field input-field ">
                            <div class="btn alinear">
                                <span>Fecha de Boda</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" class="datepicker" name="fecha" id="fecha" required>
                            </div>
                        </div>
                        <br>
                        <button class="btn btn-block-on-small dorado-2 register">
                           Enviar
                        </button>
                    </div><br>
                    <div class="col l6 s12 grey lighten-4">
                    <div class="col s12 m6 l6 grey lighten-4">
                        <h5>Participa para ganar increíbles premios para este día tan especial.</h5>
                        <!-- <h1 style="font-weight: bold; margin: 0; color: #72D6E0">UN AUTO</h1>
                        <h4 style="font-weight: bold; margin: 0;">ÚLTIMO MODELO</h4>
                        <p style="text-align: center !important; margin: 0;">y otros increíbles premios para este día tan especial:</p> -->
                        <br>
                        <strong style="font-size: 25px;">Bride&nbsp;</strong><strong style="font-weight: italic; font-size: 25px;">Weekend&nbsp;</strong><span>tiene todo lo que necesitas para hacer la boda perfecta en un solo lugar.</span>
                        <br><strong style="font-size: 20px;">Ven, vive la experiencia y gana.</strong>
                        <br><br><strong style="font-size: 20px;">Ciudad de México</strong>
                        <br><strong style="font-size: 20px; font-weight: normal;">Sábado 21 y Domingo 22, Marzo 2020 Horario: 11:00am a 8:00pm</strong>
                        <br><br><strong style="font-size: 18px; font-weight: normal;">Horario de pasarela: 2:30 y 5:30 horas.</strong>
                    </div>
                    </div>
                </div>
                
            <!-- </div> -->
            
        </div>
    </div>

    <!-- <div class="row center-align">
        <p class="btn grey darken-1" id="afiliados">LISTA DE EXPOSITORES</p>
    </div> -->

    <div class="row contentsecond imgrecinto">
        <div class="col l6 s12 alinear" style="height: 100%">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/cdmx.jpg" style="width: 100%; height: 100%;">
        </div>
        <div class="col l6 s12 alinear map" style="height: 100%" id="div-map">
            <iframe id="mapa" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3762.3499767950475!2d-99.223858!3d19.440472!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5d42bb0157a92f85!2sCentro+Citibanamex!5e0!3m2!1ses!2smx!4v1564166929315!5m2!1ses!2smx" style="width: 100%; height: 100%;" frameborder="0" style="border:0"></iframe>
        </div>
    </div>
    <!--<div class="row beneficios " id="div_afiliados" style="">
        <br>
        <h3 style="text-align: center; font-weight:900; font-size: 2.5rem;">EXPOSITORES PARTICIPANTES</h3>
        <hr>
        <div class="col l2 s6 ">
            <p style="text-align: center; color:black; font-weight:bold;"> ANIMACIÓN </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-robot-show" target="_blank">ROBOT SHOW</a> </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-clover" target="_blank">CLOVER</a></p> 
            <p style="text-align: center; color:black; font-weight:bold;"> BANQUETES </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-riquisimos-banquetes" target="_blank">RIQUÍSIMOS BANQUETES</a> </p>
            <p style="text-align: center; color:black; font-weight:bold;"> CABINA FOTOGRÁFICA </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">PHOTO BOX</a> </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">MOMENTOS</a> </p>
        </div>
        <div class="col l2 s6">
            
            <p style="text-align: center; color:black; font-weight:bold;"> COCTELERÍA </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">JALATE LA BARRA</a> </p>
            <p style="text-align: center; color:black; font-weight:bold;"> DECORACIÓN </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-alegris-eventos" target="_blank">ALEGRIS</a> </P>
            <p style="text-align: center; color:black; font-weight:bold;"> HOTEL </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-hotel-lucerna" target="_blank">HOTEL LUCERNA</a></p>   
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-hotel-san-luis-lindavista" target="_blank">HOTEL SAN LUIS</a></p>
        </div>
        <div class="col l2 s6">
            <p style="text-align: center; color:black; font-weight:bold;"> INMOBILIARIA </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">PONTEVEDRA</a></p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">IMPULSA</a></p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">FINCAMEX</a></p>
            <p style="text-align: center; color:black; font-weight:bold;"> INVITACIONES </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-invitaciones-vanely" target="_blank">VANELY</a></p>
            
        </div>
        <div class="col l2 s6">  
            <p style="text-align: center; color:black; font-weight:bold;"> LUNAS DE MIEL </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-the-life-experiences" target="_blank">LIFE EXPERIENCES</a></p>        
            <p style="text-align: center; color:black; font-weight:bold;"> MAQUILLAJE Y PEINADO </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">CLAUDIA LUGGO</a></p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">ALEXIA PALAZUELOS</a></p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">RAQUEL TOLOZA</a></p>
            <p style="text-align: center; color:black; font-weight:bold;"> MESA DE REGALOS </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-sears-culiacan" target="_blank">SEARS</a></p>
            
        </div>
        <div class="col l2 s6">          
            <p style="text-align: center; color:black; font-weight:bold;"> MÚSICA </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-grupo-musical-gomez-romero" target="_blank">GÓMEZ ROMERO</a></p>
            <p style="text-align: center; color:black; font-weight:bold;"> RECUERDOS </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-gi-rosnovski" target="_blank">ROSNOVSKI</a></p>
            <p style="text-align: center; color:black; font-weight:bold;"> REPOSTERIA </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-pasteleria-panama" target="_blank">PASTELERIA PANAMÁ</a></p>
            <p style="text-align: center; color:black; font-weight:bold;"> ROPA DE ETIQUETA </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>boda-dpaul-culiacan" target="_blank">D´PAUL</a></p>
        </div>
        <div class="col l2 s6">          
            
            <p style="text-align: center; color:black; font-weight:bold;"> SALÓN DE EVENTOS </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">CAMPESTRE SAN MIGUEL</a></p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">THE GALLERY BEACH</a></p>
            <p style="text-align: center; color:black; font-weight:bold;"> VESTIDOS DE NOVIA </p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">CASA ALICIA</a></p>
            <p style="text-align: center;"><a href="<?php echo base_url() ?>" target="_blank">ABOLENGO NOVIAS</a></p>
        </div>
    </div>-->

    

    <!-- <div class="row contentsecond">
        <video src="<?php echo base_url() ?>dist/img/brideweekend/Japy_c7_comprimido.mp4" width="100%" autoplay muted loop controls></video>
    </div> -->

    <div class="row center-align" style="background: #E9EBEA; margin-bottom: 0px !important">
        <br>
        <span class="row">
            <strong style="color: black;font-size: 4vh; font-weight: 900;">MIEMBROS DE ASOCIACIONES</strong>
            <hr >
        </span>
        <div class="container">
            <div class="col l4 s12">
                <a href="http://amprofec.org"><img src="<?php echo base_url() ?>dist/img/brideweekend/amprofec.png" style="width: 80%"></a>
            </div>
            <div class="col l4 s12">
                <a href="https://nupcialmexicana.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/anm.png" style="width: 82%"></a>
            </div>
            <div class="col l4 s12">
                <a href="https://japybodas.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/japy.png" style="width: 64%"></a>
            </div>
        </div>
    </div>

    
</body>
<?php $this->view("brideweekend/footer"); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    var STATE="";
    $(document).ready(function(){

        $("#afiliados").click(function () {
            $('html,body').animate({
                scrollTop: $("#div_afiliados").offset().top-65
            }, 1000);
        });

        $('.datepicker').pickadate({disable_picker: true, format: 'yyyy/mm/dd',
            format_submit: 'yyyy-mm-dd',
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            weekdaysShort: ['Lun','Mar','Mier','Jue', 'Vie','Sab', 'Dom'],
            weekdaysAbbrev: ['L','M','Mi','J','V','S','D']
        });

        $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    if(location){
                        if(location.state)
                            STATE = location.state;
                    }
                },
            });

        $('.register').on('click', registrar);

        $('.carousel').carousel(
        {
            dist: 0,
            padding: 0,
            indicators: true,
            duration: 100,
        }
        );
    });

    function jsShowWindowLoad(mensaje) {
        //eliminamos si existe un div ya bloqueando
        jsRemoveWindowLoad();
    
        //si no enviamos mensaje se pondra este por defecto
        if (mensaje === undefined) mensaje = "Creando perfil, por favor espere!!";
    
        //centrar imagen gif
        height = 20;//El div del titulo, para que se vea mas arriba (H)
        var ancho = 0;
        var alto = 0;
    
        //obtenemos el ancho y alto de la ventana de nuestro navegador, compatible con todos los navegadores
        if (window.innerWidth == undefined) ancho = window.screen.width;
        else ancho = window.innerWidth;
        if (window.innerHeight == undefined) alto = window.screen.height;
        else alto = window.innerHeight;
    
        //operaci贸n necesaria para centrar el div que muestra el mensaje
        var heightdivsito = alto/2 - parseInt(height)/2;//Se utiliza en el margen superior, para centrar
    
    //imagen que aparece mientras nuestro div es mostrado y da apariencia de cargando
        imgCentro = "<div style='text-align:center;height:" + alto + "px;'><div  style='color:black;margin-top:" + heightdivsito + "px; font-size:20px;font-weight:bold'>" + mensaje + "</div><img style='width: 70px; height:70px;' src='<?php echo base_url() ?>dist/img/brideweekend/loader.gif'></div>";
    
            //creamos el div que bloquea grande------------------------------------------
            div = document.createElement("div");
            div.id = "WindowLoad"
            div.style.width = ancho + "px";
            div.style.height = alto + "px";
            $("body").append(div);
    
            //creamos un input text para que el foco se plasme en este y el usuario no pueda escribir en nada de atras
            input = document.createElement("input");
            input.id = "focusInput";
            input.type = "text"
    
            //asignamos el div que bloquea
            $("#WindowLoad").append(input);
    
            //asignamos el foco y ocultamos el input text
            $("#focusInput").focus();
            $("#focusInput").hide();
    
            //centramos el div del texto
            $("#WindowLoad").html(imgCentro);
    
    }

    function jsRemoveWindowLoad() {
        // eliminamos el div que bloquea pantalla
        $("#WindowLoad").remove();
    
    }

    function registrar() {
        if($('#nombre').val()!=null && $('#correo').val()!=null && $('#telefono').val()!=null
        && $('#fecha').val()!=null && $('#nombre').val()!="" && $('#correo').val()!="" 
        && $('#telefono').val()!="" && $('#fecha').val()!="") {
            jsShowWindowLoad();
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."brideweekend/register" ?>',
                method: 'post',
                data: {
                    nombre: $('#nombre').val(),
                    correo: $('#correo').val(),
                    come_from: 'japy',
                    genero: '2',
                    telefono: $('#telefono').val(),
                    fecha: $('#fecha').val(),
                    come_from: STATE,
                },
                success: function(response) {
                    if(response.validate==false) {
                        jsRemoveWindowLoad();
                        swal('Error', 'Correo registrado anteriormente.', 'error');
                    } else {
                        jsRemoveWindowLoad();
                        $('#nombre').val('');
                        $('#correo').val('');
                        $('#telefono').val('');
                        $('#fecha').val('');
                        // $('#modal1').openModal();
                        swal('Felicidades', 'Tu registro fue exitoso.', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        } else {
            swal('Error', 'Por favor complete todos los campos.', 'error');
        }
    }
</script>
</html>