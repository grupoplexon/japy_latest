<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url() ?>dist/img/brideweekend/logo.png" rel="image_src">
    <title>Bride Weekend</title>
    <?php $this->view("brideweekend/header_ciudades"); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
</head>
<style>
h5 {
    font-weight: bold;
    margin-bottom: 2rem;
    font-size: 3rem;
}
.info{
    text-align: justify;
    font size: 1rem;
}
.grises {
    filter: url('#grayscale'); /* Versión SVG para IE10, Chrome 17, FF3.5, Safari 5.2 and Opera 11.6 */
    -webkit-filter: grayscale(100%);
    -moz-filter: grayscale(100%);
    -ms-filter: grayscale(100%);
    -o-filter: grayscale(100%);
    filter: grayscale(100%); /* Para cuando es estándar funcione en todos */
    filter: Gray(); /* IE4-8 and 9 */

    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
.grises:hover { 
    -webkit-filter: grayscale(0%);
    -moz-filter: grayscale(0%);
    -ms-filter: grayscale(0%);
    -o-filter: grayscale(0%);
    filter: none;

    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
.cardtitle {
    position:absolute; 
    z-index: 10; 
    color: white; 
    font-weight: bold;
    font-size: 4vh;
}
.cardfooter {
    position:absolute; 
    z-index: 10; 
    color: white; 
    font-weight: bold;
    font-size: 10vh;
}
.cardfooters {
    position:absolute; 
    z-index: 10; 
    color: white; 
    font-weight: bold;
    font-size: 7vh;
}
.alinear {
    padding-left: 0 !important;
    padding-right: 0 !important;
    padding-top: 0 !important;
    padding-bottom: 0 !important;
}
.fondo{
        height: 60px;
    }
.card-title{
    color: white !important;
    font-size: 10vh !important;
    font-weight: bold !important;
    text-align: center !important;
    vertical-align:middle;
}
.fecha{
    color: white !important;
    font-size: 8vh !important;
    font-weight: bold !important;
    text-align: center !important;
    vertical-align:middle;
}
.contenedor{
    position: relative;
    text-align: center;
}
.contenedor img{
    width:100%;
}
.texto-encima{
    position: absolute;
    top: 4.5rem;
    left: 50%;
    width: 100%;
    transform: translate(-50%, -50%);
    /* background: rgba(5, 4, 4, 0.85); */
}
.texto-encima h4{
    color: white;
    font-weight: bold;
    font-size:5vh;
    text-shadow: 0px 0px 18px black;
}
.texto-encima-two{
    position: absolute;
    top: 3.5rem;
    left: 50%;
    width: 100%;
    transform: translate(-50%, -50%);
    /* background: rgba(5, 4, 4, 0.85); */
}
.texto-encima-two h4{
    color: white;
    font-weight: bold;
    font-size:5vh;
    text-shadow: 0px 0px 18px black;
}
.centrado{
    position: absolute;
    width: 100% !important;
    top: 82%;
    left: 50%;
    transform: translate(-50%, -50%);
}
.centrado h4{
    color: white;
    font-weight: bold;
    font-size: 4vh;
    text-shadow: 0px 0px 18px black;
}
.centrado-two{
    position: absolute;
    width: 100% !important;
    top: 79%;
    left: 50%;
    transform: translate(-50%, -50%);
}
.centrado-two h4{
    color: white;
    font-weight: bold;
    font-size: 4vh;
    text-shadow: 2px 2px 2px #000 !important;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px) {
    .texto-encima{
        top: 2.5rem !important;
        top: 2rem !important;
    }
    .texto-encima h4{
        font-size:4vh !important;
    }
    .texto-encima-two{
        top: 2rem !important;
    }
    .texto-encima-two h4{
        font-size:4vh !important;

    }
    h5{
        font-size: 5vh;
        margin-bottom: 5vh;
    }
}

</style>
<body>
    <div class="content center-align">
    <div class="row center-align">
            <br>
            <h5>PRÓXIMOS EVENTOS</h5>
            <!-- <br> -->
            <div class="row">
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/new_york"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/newyork.png" /></a>
                        <div class="texto-encima">
                            <a href="<?php echo base_url() ?>brideweekend/new_york">
                                <h4> NEW YORK </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">NYCB LIVE</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">Home of the Nassau Veterans Memorial Coliseum</h4>
                            </a>
                        </div>
                        <div class="centrado">
                            <a href="<?php echo base_url() ?>brideweekend/new_york"><h4> 04 Y 05 SEPTIEMBRE </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/chicago"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/chicago.png" /></a>
                        <div class="texto-encima">
                            <a href="<?php echo base_url() ?>brideweekend/chicago">
                                <h4> CHICAGO </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Donald E. Stephens</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">Convention Center</h4>
                            </a>
                        </div>
                        <div class="centrado">
                            <a href="<?php echo base_url() ?>brideweekend/chicago"><h4> 04 Y 05 ENERO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/monterrey"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/monterrey.png" /></a>
                        <div class="texto-encima">
                            <a href="<?php echo base_url() ?>brideweekend/monterrey">
                                <h4> MONTERREY </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Centro Convex</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;"><br></h4>
                            </a>
                        </div>
                        <div class="centrado">
                            <a href="<?php echo base_url() ?>brideweekend/monterrey"><h4> 11 Y 12 ENERO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/expo_gdl"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/guadalajara.png" /></a>
                        <div class="texto-encima">
                            <a href="<?php echo base_url() ?>brideweekend/expo_gdl">
                                <h4> GUADALAJARA </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Expo Guadalajara</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;"><br></h4>
                            </a>
                        </div>
                        <div class="centrado">
                            <a href="<?php echo base_url() ?>brideweekend/expo_gdl"><h4> 25 Y 26 ENERO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/leon.png" />
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/expo_leon">
                                <h4> LEON </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">LA CASA DE PIEDRA</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;"><br></h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <h4> 08 Y 09 FEBRERO </h4>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/expo_qro"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/queretaro.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/expo_qro">
                                <h4> QUERÉTARO </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Centro de Congresos</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;"><br></h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/expo_qro"><h4> 22 Y 23 FEBRERO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/expo_puebla"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/puebla.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/expo_puebla">
                                <h4> PUEBLA </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Centro Expositor</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">Los Fuertes</h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/expo_puebla"><h4> 29 FEBRERO 01 MARZO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/phoenix"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/phoenix.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/phoenix">
                                <h4> PHOENIX </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Arizona State</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">Fairgrounds</h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/phoenix"><h4> 07 Y 08 MARZO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/expo_cdmx"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/mexico.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/expo_cdmx">
                                <h4> CDMX </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">CENTRO CITIBANAMEX</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;"><br></h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/expo_cdmx"><h4> 21 Y 22 MARZO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/san_diego"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/sandiego.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/san_diego">
                                <h4> SAN DIEGO </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">San Diego Convention Center</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">Hall C1</h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/san_diego"><h4> 29 MARZO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/houston"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/houston.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/houston">
                                <h4> HOUSTON </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">George R. Brown</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">Convention Center Hall A3</h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/houston"><h4> 04 Y 05 ABRIL </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/atlanta"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/atlanta.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/atlanta">
                                <h4> ATLANTA </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Georgia World Congress Center</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">Hall C1</h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/atlanta"><h4> 18 Y 19 ABRIL </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/san_luis"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/slpotosi.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/san_luis">
                                <h4> SAN LUIS POTOSI </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Centro de Covenciones</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">San Luis Potosi</h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/san_luis"><h4> 30 Y 31 MAYO </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/los_angeles"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/losangeles.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/los_angeles">
                                <h4> LOS ÁNGELES </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">Los Angeles Convention Center</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;">South Hall K</h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/los_angeles"><h4> 03 Y 04 OCTUBRE </h4></a>
                        </div>
                    </div>
                </div>
                <div class="col s12 l3">
                    <div class="contenedor">
                        <a href="<?php echo base_url() ?>brideweekend/san_antonio"><img class="" src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/sanantonio.png" /></a>
                        <div class="texto-encima-two">
                            <a href="<?php echo base_url() ?>brideweekend/san_antonio">
                                <h4> SAN ANTONIO </h4>
                                <h4 style="font-size: 26px; margin: 0px !important;">FREEMAN COLISEUM</h4>
                                <h4 style="font-size: 17px; margin: 0px !important;"><br></h4>
                            </a>
                        </div>
                        <div class="centrado-two">
                            <a href="<?php echo base_url() ?>brideweekend/san_antonio"><h4> 01 NOVIEMBRE </h4></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col s12 l12 center-align" style="background: #E9EBEA;">
                <br>
                <span class="row">
                    <strong style="color: black;font-size: 4vh;">MIEMBROS DE ASOCIACIONES</strong>
                    <hr style="width: 40px;color: #f38083;display: block;background: #515151;height: 1px;border: 0;border: 2px solid #515151;padding: 0; ">
                </span>
                <!-- <br>
                <img src="<?php echo base_url() ?>dist/img/brideweekend/comprador-footer.png" style="width: 100%"> -->
                <div class="container">
                <div class="col l3 s12">
                    <a href="http://amprofec.org"><img src="<?php echo base_url() ?>dist/img/brideweekend/amprofec.png" style="width: 90%"></a>
                </div>
                <div class="col l3 s12">
                    <a href="https://nupcialmexicana.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/anm.png" style="width: 92%"></a>
                </div>
                <div class="col l3 s12">
                    <a href="https://japybodas.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/plecaBrideAdv.png" style="width: 74%"></a>
                </div>
                <div class="col l3 s12">
                    <img src="<?php echo base_url() ?>dist/img/brideweekend/iwa.png" style="width: 54%">
                </div>
                </div>
            </div>

            </div>

        </div>
    </div>

</div>


</body>
<?php $this->view("brideweekend/footer"); ?>
</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

</script>
