<style>
.page-footer {
    background: white !important;
}
</style>
<div id="footer">
<div class="row" style=" background: #e9ebea8c; padding-top: 20px; margin-bottom: 0px;" >
        <div class="row" id="info">
            <div class="container"> 
                <div class="col s12 l12">
                    <img style="width: 200px;" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/bw_logo.png">
                    <!-- <h4 style="text-align: left;">Bride <strong style="font-weight: normal; font-style: italic">Weekend</strong></h4> -->
                </div>
                <div class="col s12 m4 l4" style="font-family: 'Roboto', sans-serif;">
                    <label > 
                        <p>CONTÁCTANOS  <br>
                        Santa Rosa de Lima 4428 Fracc. Camino Real Zapopan, Jal.
                        <br>
                        contacto@brideweekend.com
                        <br>
                        (0133)31229622/9606
                        <br>
                        
                        </p>
                        <hr style="width: 100%;color: #f38083;display: block;background: #9e9e9e;;border: 0;border: .5px solid #9e9e9e;;padding: 0; "><br>
                        <span style="font-size: 3vh; font-style: italic;">Follow Us:<a href="https://www.facebook.com/brideweekendmx" target="_blank">
                            <i class="fa fa-facebook  icon-style" style="font-size: 4vh; color: #72D6E0;"></i>
                            &nbsp;<a href="https://instagram.com/brideweekend?utm_source=ig_profile_share&igshid=15c7fw9ym27rq" target="_blank">
                            <i class="fa fa-instagram  icon-style" style="font-size: 4vh; color: #72D6E0;"></i>&nbsp;
                            <a href="https://wa.me/523329386878" target="_blank"><i class="fa fa-whatsapp" style="font-size: 4vh; color: #72D6E0;" aria-hidden="true"></i></a>
                        </a></span>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <footer style="padding-top: unset; background: white;" class="page-footer">
        <!-- <div class="container">
        <div class="row">
            <div class="col l6 s12">
            <h5 class="white-text">Footer Content</h5>
            <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
            </div>
            <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Links</h5>
            <ul>
                <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
            </ul>
            </div>
        </div>
        </div> -->

        <div class="footer-copyright" style="background: white;">
        <div class="container center align" style="color: black">
        <b>© 2019 Bride <strong style="font-weight: bold; font-style: italic">Weekend</strong></b>
        <!-- <a class="grey-text text-lighten-4 right" href="#!">More Links</a> -->
        </div>
        </div>
    </footer>
</div>