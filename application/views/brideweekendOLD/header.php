<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-40486092-9');
    </script>
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2323895250959097');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=2323895250959097&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->


    
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.1/css/materialize.min.css"  media="screen,projection"/>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">


</head>
<style>
strong{
    font-weight: bold;
}
body{
    font-family: 'Montserrat', sans-serif !important;
}
nav{
    /* background: linear-gradient(110deg, #b5b5b5 60%, #ffed4b 60%, #fdcd3b 60%); */
    background-color: #515151;
}
#sidenav-overlay{
    z-index: unset !important;
}
nav ul a{
    font-size: 14px !important;
    font-weight: bold;
}
nav .nav-wrapper {
    position: relative;
    height: 100%;
    margin-right: 10%;
    margin-left: 10%;
}
.slider .indicators .indicator-item.active {
    background-color: #8dd7e0;
}

</style>
<div id="header">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <div class="navbar-fixed">
        <nav id="menuBW">
            <div class="nav-wrapper">
            <!-- <a href="<?php echo base_url() ?>" class=" brand-logo" style="max-height: 64px;position: absolute;">
                <img  style="max-height: 64px;"
                src="<?php echo base_url() ?>dist/img/japy_nobg_white.png"alt="Japy"/>
            </a> -->
            
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li class=""><a href="<?php echo base_url().'brideweekend' ?>" class="brand-logo" style="max-height: 64px;position: relative !important;font-size: 2.1rem !important;">
                    <img src="<?php echo base_url() ?>dist/img/brideweekend/logo.png" alt="" style="height: 9vh; margin-right: 5vh;">
                </a></li>
                <!-- <li><a id="menu-home">HOME</a></li> -->
                <li><a href="<?php echo base_url() ?>brideweekend/exposiciones">CIUDADES</a></li>
                <li><a id="menu-registro">REGISTRO</a></li>
                <!-- <li><a id="menu-actividades">ACTIVIDADES</a></li> -->
                <li><a id="menu-sede" href="#sedes">UBICACIÓN</a></li>
                
                <li><a id="menu-bride">CONCEPTO</a></li>
                <li><a id="menu-novia">NOVIA</a></li>
                <li><a id="menu-expositor">EXPOSITOR</a></li>
                <!-- <li><a href="#galeria">GALERIA</a></li> -->
                <!-- <li><a href="#blog">BLOG</a></li> -->
            </ul>
            <ul id="mobile-demo" class="side-nav">
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <!-- <li><a href="#home">HOME</a></li> -->
                        <li><a href="<?php echo base_url() ?>brideweekend/exposiciones">CIUDADES</a></li>
                        <li><a href="#contacto">REGISTRO</a></li>
                        <!-- <li><a id="#actividades">ACTIVIDADES</a></li> -->
                        <li><a href="#sedes">UBICACIÓN</a></li>
                        <li><a href="#brideweekend">CONCEPTO</a></li>
                        <li><a href="#comprador">NOVIA</a></li>
                        <li><a href="#expositor">EXPOSITOR</a></li>
                        <!-- <li><a href="#galeria">GALERIA</a></li>
                        <li><a href="#blog">BLOG</a></li> -->
                    </ul>
                </li>
            </ul>
                    
            
            </div>
        </nav>
    </div>


</div>
<script>
$(document).ready(function(){
    $(".button-collapse").sideNav(); 

});
</script>