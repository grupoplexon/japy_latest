<head>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.1/css/materialize.min.css"  media="screen,projection"/>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-40486092-9');
    </script>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>
<style>
@font-face {
  font-family: "Futura_asd_Bold";
  src: url("../../dist/fonts/brideweekend/Futura LT Bold.ttf") format("truetype");
  /* src: url("../fonts/line-awesome.eot?v=1.1."); */
  /* src: url("../fonts/line-awesome.eot??v=1.1.#iefix") format("embedded-opentype"),
       url("../fonts/line-awesome.woff2?v=1.1.") format("woff2"),
       url("../fonts/line-awesome.woff?v=1.1.") format("woff"),
       url("../fonts/line-awesome.ttf?v=1.1.") format("truetype"),
       url("../../dist/fonts/brideweekend/Futura LT Bold.ttf") format("truetype"); */
  font-weight: normal;
  font-style: normal;
}
/* @font-face {
    font-family: "Futura_bold";
    src: url("../../../dist/fonts/brideweekend/Futura-Bold.woff") format("woff"),
        url("../../../dist/fonts/brideweekend/Futura_Bold_font.ttf") format("truetype");
} */
@font-face {
    font-family: "Futura_bold";
    src: url("FuturaBold.woff") format("woff");
}
p{
    font-family: "Roboto", sans-serif;
}
strong{
    font-weight: bold;
}
body{
    font-family: sans-serif;
}
nav{
    /* background: linear-gradient(110deg, #b5b5b5 60%, #ffed4b 60%, #fdcd3b 60%); */
    background-color: #515151;
}
#sidenav-overlay{
    z-index: unset !important;
}
nav ul a{
    font-size: 14px !important;
    font-weight: bold;
}
nav .nav-wrapper {
    position: relative;
    height: 100%;
    margin-right: 20%;
    margin-left: 10%;
}
.slider .indicators .indicator-item.active {
    background-color: #8dd7e0;
}

</style>
<div id="header">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <div class="navbar-fixed">
        <nav id="menuBW">
            <div class="nav-wrapper">
            <!-- <a href="<?php echo base_url() ?>" class=" brand-logo" style="max-height: 64px;position: absolute;">
                <img  style="max-height: 64px;"
                src="<?php echo base_url() ?>dist/img/japy_nobg_white.png"alt="Japy"/>
            </a> -->
            
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li class=""><a href="<?php echo base_url().'brideweekend' ?>" class="brand-logo" style="max-height: 64px;position: relative !important;font-size: 2.1rem !important;">
                    <img src="<?php echo base_url() ?>dist/img/brideweekend/logo.png" alt="" style="height: 9vh; margin-right: 5vh;">
                </a></li>
                <li><a id="menu-home" href="<?= base_url() ?>brideweekend#home">HOME</a></li>
                <li><a id="menu-registro" href="<?= base_url() ?>brideweekend#contacto">REGISTRO</a></li>
                <li><a id="menu-sede" href="<?= base_url() ?>brideweekend#sedes">UBICACIÓN</a></li>
                <li><a id="menu-bride" href="<?= base_url() ?>brideweekend#brideweekend">BRIDE WEEKEND</a></li>
                <li><a id="menu-expositor" href="<?= base_url() ?>brideweekend#expositor">EXPOSITOR</a></li>
                <li><a id="menu-novia" href="<?= base_url() ?>brideweekend#comprador">NOVIA</a></li>
                <!-- <li><a href="#galeria">GALERIA</a></li> -->
                <!-- <li><a href="#blog">BLOG</a></li> -->
            </ul>
            <ul id="mobile-demo" class="side-nav">
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li><a href="#home">HOME</a></li>
                        <li><a href="#contacto">REGISTRO</a></li>
                        <li><a href="#sedes">UBICACIÓN</a></li>
                        <li><a href="#brideweekend">BRIDEWEEKEND</a></li>
                        <li><a href="#expositor">EXPOSITOR</a></li>
                        <li><a href="#comprador">NOVIA</a></li>
                        <!-- <li><a href="#galeria">GALERIA</a></li>
                        <li><a href="#blog">BLOG</a></li> -->
                    </ul>
                </li>
            </ul>
                    
            
            </div>
        </nav>
    </div>


</div>
<script>
$(document).ready(function(){
    $(".button-collapse").sideNav(); 

});
</script>