<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
    </script>
    <meta property="og:title" content="Bride Weekend Puebla" />
    <meta property="og:image" content="https://brideadvisor.mx/dist/img/brideweekend/bw_new.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:url" content="https://www.brideadvisor.mx/brideweekend" />
    <meta property="og:site_name" content="Bride Weekend Puebla" />
    <meta property="og:description" content="Todo lo que necesitas para tu boda este 24 y 25 de Agosto en Puebla, Centro Expositor los Fuertes. Pasarelas con las últimas tendencias y más de 1000 vestidos de novia en exhibición." />
    <meta property="og:type" content="website" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url() ?>dist/img/brideweekend/logo.png" rel="image_src">
    <title>Bride Weekend</title>
    <?php $this->view("brideweekend/header"); ?>
    
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="<?php echo base_url() ?>dist/css/brideweekend.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo base_url() ?>dist/slick/slick.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>dist/slick/slick-theme.css" rel="stylesheet" type="text/css"/>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>
</head>
<style>
</style>
<body>

<div class="content center-align">
    <!-- <a class='flotante' href='brideweekend/boletos' ><img src="<?php echo base_url() ?>dist/img/brideweekend/comprar.png" style="width: 27vh;"></a> -->
    <!-- <a  class='flotante' href='https://play.google.com/store/apps/details?id=com.japybodas.app' ><img src="https://cdn.worldvectorlogo.com/logos/google-play-download-android-app.svg" style="width: 20vh;"></a>
    <a class='flotante2' href='https://itunes.apple.com/mx/app/japy/id1453432572' ><img src="https://madbeerweek.com/wp-content/uploads/2017/04/app-store-logo.png" style="width: 20vh;"></a> -->
    <a class='flotante' href='https://boletos.brideadvisor.mx/evento/Puebla-2019-08' ><img class="tam-flotante" src="<?php echo base_url() ?>dist/img/brideweekend/BOTON_BW2.png"></a>
    <a class='flotante2' href='https://wa.me/523329386878' ><img src="<?php echo base_url() ?>dist/img/brideweekend/whatsapp.png" style="width: 8vh; margin-left: 2vh;"></a>
    <div class="row" id="home" style="background: #4d4d4f">
        <div class="slider slide">
            <ul class="slides slide2">
                <li>
                    <a href="<?= base_url() ?>brideweekend/exposiciones"><img src="<?php echo base_url() ?>dist/img/brideweekend/slider_pue.jpg"></a>
                </li>
                <li>
                    <img src="<?php echo base_url() ?>dist/img/brideweekend/slider_exp.jpg" style="cursor: pointer;">
                </li>
            </ul>
        </div>
    </div>
    <?php if(count($promotions)!=0) { ?>  
    <div class="row promociones ">
        <br>   
        <h4 > PROMOCIONES ACTUALES</h4>
        <hr style="width:20%; color:#555555;">
        <br>
        <div class="row">
            <div class="col l12 s12 articulos jcarousel-articulos center">
                <?php foreach ($promotions as $prom) : ?>            
                    <div class="col m2-4-jcaroul">
                        <div class="row card promos">
                            <div class="col l12 ">
                                <a style="" href="<?php echo base_url() ?>brideweekend/promocion?name=<?php echo $prom->id_promotion ?>"><img style="width: 100%;" class="art-img"
                                    src="<?php echo base_url() ?>uploads/promociones/images/<?php echo $prom->image ?>" ></a>
                                <?php if($prom->profile_provider!=null || $prom->profile_provider!='') { ?>
                                <a href="<?php echo $prom->profile_provider ?>"  class="btn">Ver Perfil</a>
                                <?php } ?>
                                <div class="row info">
                                <a style="" href="<?php echo base_url() ?>brideweekend/promocion?name=<?php echo $prom->id_promotion ?>"><h5 class="descrip"><?php echo strip_tags($prom->name_provider) ?></h5></a>
                                    <h6 class="contenido"><?php echo substr(strip_tags($prom->description), 0, 100) ?>...</h6>
                                </div>
                                <div class="row buttom">
                                    
                                    <label>
                                        <input type="checkbox" name="<?php echo $prom->id_promotion ?>" value="<?php echo $prom->id_promotion ?>" />
                                        <span>Obtener cupón</span>
                                    </label>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <button class="btn btn-block-on-small dorado-2 " id="save_cupon"> Guardar cupones </button>
            <h4> ó </h4>
            <button class="btn btn-block-on-small dorado-2 " id="mail"> Enviar por email </button>
            <div class="correo col l4 offset-l4 hide" id="div-correo">
                <div class="file-path-wrapper ">
                    <h5>Ingresa tu correo electronico</h5>
                    <input type="text" name="correoC" id="correoC" placeholder="Correo electronico" class="form-control" required>
                    <button class="btn btn-block-on-small dorado-2 " id="send_cupon"> Enviar </button>
                </div>
            </div>
        </div>
        
    </div>
    <br><br><br>
    <?php } ?>  
    <div class="row" id="contacto">
        <div class="row "id="form">
                <div class="row " id="">
                    <h4 > REGÍSTRATE Y GANA</h4>
                    <hr>
                    <div class="col s12 m6 l5 offset-l1 form-contact" >
                        <div class="file-field input-field " >
                            <div class="btn">
                                <span>Nombre</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" name="nombre" id="nombre" class="form-control" required>
                            </div>
                        </div>
                        <div class="file-field input-field " >
                            <div class="btn">
                                <span>Email</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input id="correo" name="correo" type="text" class="validate" required>
                            </div>
                        </div>
                        <div class="file-field input-field  " >
                            <div class="btn">
                                <span>Telefono</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input name="telefono" id="telefono" class="form-control" required type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                            </div>
                        </div>
                        <div class="file-field input-field ">
                            <div class="btn alinear">
                                <span>Fecha de Boda</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="date" class="" name="fecha" id="fecha" required>
                            </div>
                        </div>
                        <br>
                        <button class="btn btn-block-on-small dorado-2 register">
                           Enviar
                        </button>
                        <br><br>
                    </div><br>
                    <div class="col l6 s12 grey lighten-4">
                        <div class="col s12 m6 l7 grey lighten-4">
                            <h5 style="font-size:22px;">Participa para ganar increíbles premios para este día tan especial:</h5>
                            <br>  
                            <strong style="font-size: 20px;">Bride&nbsp;</strong><strong style="font-style: italic; font-size: 20px;">Weekend&nbsp;</strong><span style="font-size: 18px;"> tiene todo lo que necesitas para hacer la boda perfecta en un solo lugar.</span>
                            <br><br><strong style="font-size: 20px;">Ven, vive la experiencia y gana.</strong>
                            <br><br><strong style="font-size: 20px;">Querétaro</strong>
                            <br><strong style="font-size: 18px;">Centro de Congresos</strong>
                            <br><br><strong style="font-size: 18px;">Fecha:</strong><br><strong style="font-size: 16px; font-weight: normal;">Sábado 07 y Domingo 08 de Septiembre de 2019</strong>
                            <br><strong style="font-size: 18px;">Horario:</strong><br><span style="font-size:16px;">Sábado </span></span><strong style="font-size: 16px; font-weight: normal;">04:00pm a 08:00pm</strong>
                            <br><span style="font-size:16px;">Domingo </span></span><strong style="font-size: 16px; font-weight: normal;">11:00am a 08:00pm</strong>
                            <br><strong style="font-size: 18px;">Horario de pasarela:</strong><br><span style="font-size:16px;">Sábado </span></span><strong style="font-size: 16px; font-weight: normal;">06:00pm</strong>
                            <br><span style="font-size:16px;">Domingo </span></span><strong style="font-size: 16px; font-weight: normal;">02:30pm y 05:30pm</strong>
                            <br><br>
                        </div>
                    </div>
                </div>
        </div>
    </div>

    <div class="row center-align " style="background-color: #f4f4f4;">
        <div class="col l10 s12 m12 offset-l1">
            <div class="col l12 s12 m12 center-align">
                <h4>REGISTRO ACTIVIDADES</h4><br>
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/CATA-VINO.png" style="width:100%; cursor:pointer;" id="cata">
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/RAMOS.png" style="width:100%; cursor:pointer;" id="ramos">
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/MAQUILLAJE-PEINADO.png" style="width:100%; cursor:pointer;" id="maquillaje">
            </div>
        </div>
        <div class="col l12 s12 m12 oculto alinear cata" style="background-color: #f4f4f4;" id="">
            <h4>CATA DE VINOS</h4><br>
            <h5 class="horario">HORARIOS:</h5>
            <h5 style="font-size: 1.3rem;">SÁBADO 7 DE SEPTIEMBRE 7:00 PM <b class="neg"> | </b> DOMINGO 8 DE SEPTIEMBRE 6:30 PM </h5>
            <h5 style="font-size: 1.3rem;">STAND BRIDE ADVISOR</h5>
            <div class="col l12 s12 center-align" id="dia_asistencia">
                <!-- <h5>Día en que asistirás:</h5> -->
                <br>
                <form name="dia">
                <strong>
                    <label>
                        <input name="group1" type="radio" value="sabado" checked />
                        <span style="color: black; font-size: 17px;">Sábado</span>
                    </label>
                </strong>&nbsp;&nbsp;
                <strong>
                    <label>
                        <input name="group1" type="radio" value="domingo" />
                        <span style="color: black; font-size: 17px;">Domingo</span>
                    </label>
                </strong>
                </form>
                <br>
            </div>
            <div class="col l10 offset-l1">
                <div class="col l6 s12">
                    <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA"><br>
                    <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                    <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                    <br>
                </div>
                
                <div class="col l6 s12">
                    <input type="text" id="nombreNovio" placeholder="NOMBRE DEL NOVIO"><br>
                    <input type="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                    <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                </div>
                <div class="col l12 s12 center-align">
                    <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p><br>
                    <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                </div>
            </div>
            <div class="col l12 s12 center-align">
                <br>
                <button class="btn grey darken-1 registerPreview " id="registerCata" style=" height: 50px; border-radius: 15px;
                font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
            </div>
        </div>
        <div class="col l12 s12 m12 oculto alinear maquillaje">
            <h4>MASTER CLASS DE MAQUILLAJE</h4><br>
            <h5 class="horario">HORARIOS:</h5>
            <h5>SÁBADO 7 DE SEPTIEMBRE <b class="neg"> | </b>  7:00 PM <b class="neg"> | </b> STAND BRIDE ADVISOR</h5>
            <br>
            <div class="col l10 offset-l1 s12">
                <div class="col l6 offset-l3 s12">
                    <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA"><br>
                </div>
                <div class="col l6 s12">
                    <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                    <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                    <br>
                </div>
                <div class="col l6 s12">
                    <input type="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                    <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                </div>
                <div class="col l12 s12">
                    <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p><br>
                    <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                </div>
                <div class="col l12 s12 center-align">
                    <br>
                    <button class="btn grey darken-1 registerPreview " id="registerMaquillaje" style=" height: 50px; border-radius: 15px;
                    font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
                </div>
            </div>                
        </div>
        <div class="col l12 s12 m12 oculto alinear ramos">
            <h4>TALLER DE RAMOS DE NOVIA</h4><br>
            <h5 class="horario">HORARIOS:</h5>
            <h5>SÁBADO 7 DE SEPTIEMBRE <b class="neg"> | </b>  1:00 PM <b class="neg"> | </b> STAND BRIDE ADVISOR</h5>
            <br>
            <div class="col l10 offset-l1 s12">
                    <div class="col l6 offset-l3 s12">
                        <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA">
                        <br>
                    </div>
                    <div class="col l6 s12">
                        
                        <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                        <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                        <br>
                    </div>
                    <div class="col l6 s12">
                        <!-- <input type="text" id="nombreNovio" placeholder="NOMBRE DEL NOVIO">
                        <br> -->
                        <input type="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                        <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                    </div>
                    <div class="col l12 s12">
                        <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p>
                        <br>
                        <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                    </div>
                    
                </div>
                <div class="col l12 s12 center-align">
                    <br>
                    <button class="btn grey darken-1 registerPreview " id="registerRamos" style=" height: 50px; border-radius: 15px;
                    font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
                </div>
                
        </div>
    </div> 
    

    <div class="row">
        <div class="container">
            <h4>Ganadores BrideWeekend Puebla</h4>
            <br>
            <h5>Ganadora del Crucero 7 Noches (Panamá, Colombia, Curazao, Aruba y Bonaire): <br><strong>Norma Alicia Gonzalez Torres</strong></h5>
            <br>
            <h5>Ganadores de premios generales:</h5>
            <table class="centered striped">
                <thead>
                <tr>
                    <th>Proveedor</th>
                    <th>Ganador</th>
                    <th>Premio</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td>D'OPORTOS BRIDE</td>
                    <td>Cyndell Jacqueline Pérez Ríos</td>
                    <td>VESTIDO DE NOVIA POR PRE-COMPRA</td>
                </tr>
                <tr>
                    <td>STUDIO HAIR</td>
                    <td>Diana Isabel </td>
                    <td>MAQUILLAJE Y PEINADO</td>
                </tr>
                <tr>
                    <td>HISTORIAS EN PAPEL</td>
                    <td>Irani Avalos</td>
                    <td>100 INVITACIONES</td>
                </tr>
                <tr>
                    <td>D'OPORTO NOVIAS</td>
                    <td>Gricelia Salas Paredes</td>
                    <td>UN VELO DE NOVIA</td>
                </tr>
                <tr>
                    <td>PHOTOSMILE</td>
                    <td>Raquel guzman</td>
                    <td>PAQUETE MINI/1 HORA DE SERVICIO</td>
                </tr>
                <tr>
                    <td>POLAR</td>
                    <td>Karen Jazmín Ramirez</td>
                    <td>1 SESIÓN DE FOTO PREVIA</td>
                </tr>
                <tr>
                    <td>VESTIDOS SOPHIE</td>
                    <td>Esther Castillo Sanchez</td>
                    <td>1 VESIDO</td>
                </tr>
                <tr>
                    <td>DREAMS HUATULCO</td>
                    <td>Alma susana</td>
                    <td>RESORT & SPA ESTANCIA 4 DÍAS 3 NOCHES</td>
                </tr>
                <tr>
                    <td>SECRETS HUATULCO</td>
                    <td>Gabriela Arroyo</td>
                    <td>RESORT & SPA ESTANCIA 4 DÍAS 3 NOCHES</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    
    <div class="row center-align" id="sedes" style="background: white;">
        <span class="row">
            <br><i class="fa fa-map-marker" aria-hidden="true" style="font-size: 5vh; color: #848484;"></i><br>
            <strong style="color: #848484;font-size: 4vh;">UBICACIÓN</strong>
            <hr>
        </span>
        <div class="col l6 s12 banner">
            <img class="banner-sedes" style="width: 100%; height:100%;" src="<?php echo base_url() ?>dist/img/brideweekend/qro.jpg">
        </div>
        <div class="col l6 s12 mapa">
            <iframe id="mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3735.2362624184007!2d-100.35181168563265!3d20.578406986244076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d35ad251120a2f%3A0x80670acc687aff68!2sQuer%C3%A9taro%20Centro%20de%20Congresos!5e0!3m2!1ses-419!2smx!4v1566834210092!5m2!1ses-419!2smx" style="width: 100%; height:100%;" frameborder="0" style="border:0"></iframe>
        </div>
        <div class="col l12 s12" id="event-counter" style="background: #515151 !important;" >
            <div class="col l2 s12 offset-l1 center-align" id="pleca">
                <img class="pleca" src="<?php echo base_url() ?>dist/img/brideweekend/plecaqro.png">
            </div>
            <div class="class col l2 s3 center-align date-expo ">
                <h5 class="date-text" id="event-days"> </h5>
                <p>Días</p>
            </div>
            <div class="class col l2 s3 center-align date-expo">
                <h5 class="date-text" id="event-hours"> </h5>
                <p>Horas</p>
            </div>
            <div class="class col l2 s3 center-align date-expo">
                <h5 class="date-text" id="event-minutes"> </h5>
                <p>Min</p>
            </div>
            <div class="class col l2 s3 center-align date-expo">
                <h5 class="date-text" id="event-seconds"> </h5>
                <p>Seg</p>
            </div>
        </div>
    </div> 

    <div class="row" id="brideweekend">
        <div class="col l6 s12 hide-on-small-only" id="img-bride">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend4.jpg" style="width: 100%">
        </div>
        <div class="col l6 s12 seccion">
            <div class="col s12 l12" id="title-bride">
                <h5 class="title" >CONCEPTO</h5>
                <hr>
            </div>
            <div class="col l12 s12 alinear text-bride">
                <h5>
                    En BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> reinventamos el concepto tradicional de las expos de boda, en una experiencia única capaz de ofrecer a las parejas en un solo fin de semana y en un único espacio todo lo necesario para crear su boda perfecta.
                </h5>
                <h5>
                    Nos mantenemos a la vanguardia en la industria nupcial, al contar con más de 2,000 modelos de vestidos de diferentes marcas para estar completamente seguros de que aquí encontrarán el ideal.
                </h5>
                <h5>
                    Tendencias, creatividad y emoción se dan cita en BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong>, donde encontrarás una variada y seleccionada oferta de servicios nupciales, conocer
                    novedades, obtener el mejor asesoramiento, disfrutar de desfiles y obtener regalos,
                    para hacer realidad la boda de tus sueños.
                </h5>
            </div>
        </div>
        <div class="col l6 hide-on-large-only" id="img-bride2">
            <br>
            <img src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend4.jpg" style="width: 100%">
        </div>
    </div>

    <div class="row" id="comprador" style="background: #cdcdcd">
        <div class="col l6 s12 seccion">
            <div class="col s12 l12" id="title-bride">
                <h5 class="title" >NOVIA</h5>
                <hr style="color: #515151;">
            </div>
            <div class="col l12 S12 alinear text-bride">
                <h5>    
                    En BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> nos esforzamos cada día para que todo salga impecable, nuestro compromiso es hacer realidad el sueño de la boda perfecta de cada pareja según sus gustos e ideas.
                </h5>
                <h5>
                    Somos especialistas en reducir el estrés y calmar a los novios en esos momentos de nervios durante la organización de su boda, logrando que las vivencias durante todo este proceso resulten divertidas, excitantes, con ilusión y sobre todo con un concepto vanguardista, jóven, moderno e incluyendo las ultimas tendencias de moda. 
                    <br> Por ello tendremos dos pasarelas por día con las últimas tendencias de vestidos de novia en horario de 2:30 y 5:30 para que nadie quede fuera. 
                </h5>
                <h5>
                Te recordamos que nuestro horario de atención en la expo será Sábado de 04:00pm a 08:00pm y Domingo de 12:pm a 08:00pm este 07 y 08 de Septiembre en Centro de Congresos.
                </h5>
                <br><br>
            </div>
        </div>
        <div class="col l6 s12" id="img-novia">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/novia.jpg" style="width: 100%">
        </div>
    </div>

    <div class="row" id="expositor">
        <div class="col l6  hide-on-small-only" id="img-expositor">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/expositor.jpg" >
        </div>
        <div class="col l6">
            <div class="col s12 l12 s12 alinear"  id="title-bride">
                <h5 class="title">EXPOSITOR</h5>
                <hr>
            </div>
            <div class="col l12 alinear s12 text-bride">
                <h5>BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> es la cita ineludible para los profesionales de la industria nupcial y el evento ideal para todas aquellas parejas que están planeando su boda y que puedan vivir la experiencia de encontrar lo necesario para el día más importante de sus vidas.</h5>
                <h5>Un nuevo concepto de exposición en el que las parejas, podrán disfrutar de pasarelas y otras actividades en las que obsequiarán importantes premios como productos, y servicios para su boda, lunas de miel y hasta un auto.</h5>
                <h5>Somos líderes en el mercado de exposiciones, debido a nuestra experiencia con diversos conceptos de éxito,  esperamos a más de 5,000 asistentes en esta edición y contar con la participación de 200 expositores de diferentes giros.</h5>
            </div>
            <div class="col l12 s12 center-align">
                <br>
                <a class="btn contratar"  style="background: #848484">CONTRATA AQUÍ</a>
                <br><br>
            </div>
        </div>
        <div class="col l6 hide-on-large-only"  id="img-expositor2">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/expositor.jpg" style="width: 100%">
        </div>
    </div>

    <div class="row">
        <div class="col s12 l12 center-align" style="background: #E9EBEA; margin-bottom: -20px !important;">
            <div class="col s12 l12"  id="">
                <h5 class="title">MIEMBROS DE ASOCIACIONES</h5>
                <hr style="color: #515151;">
            </div>
            <div class="container">
                <div class="col l4 s12">
                    <a href="http://amprofec.org"><img src="<?php echo base_url() ?>dist/img/brideweekend/amprofec.png" style="width: 80%"></a>
                </div>
                <div class="col l4 s12">
                    <a href="https://nupcialmexicana.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/anm.png" style="width: 82%"></a>
                </div>
                <div class="col l4 s12">
                    <a href="https://brideadvisor.mx"><img src="<?php echo base_url() ?>dist/img/brideweekend/plecaBrideAdv.png" style="width: 64%"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                        style="float: right !important;
                        position: absolute !important;
                        right:0px; !important; color: #8DD7E0 !important;
                        font-size:4vh;">X</button>
        <a href="brideweekend/boletos"><div class="col m12 l12" style="position: absolute; width: 100%; height: 50%; cursor: pointer;"></div></a>
        <a href="<?php echo base_url() ?>novios/perfil"><div class="col m12 l12" style="position: absolute; width: 100%; height: 50%; cursor: pointer;margin-top: 37%;"></div></a>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/registro_exitoso.png" style="width: 100%">

    </div>
    <div id="modal4" class="modal">
        <br>
        <form method="POST" class="col  s12  m12 l12 " onsubmit="return submitUserForm();"
                action="<?php echo base_url()."brideweekend/email_expositores"?>">
            <div class="model-content row center-align">
                <h4>CONTACTAR</h4>
                <div class="input-field col s10 offset-s1">
                    <input id="nombre_" type="text" name="name" class="validate" required>
                    <label for="nombre_">Nombre</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <input id="correo_" type="text" name="email" class="validate" required>
                    <label for="correo_">Correo</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <input id="telefono_" type="text" name="telefono" class="validate" required>
                    <label for="telefono_">Teléfono</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <textarea id="mensaje_" type="text-area" name="msg" class="validate" required></textarea>
                    <label for="mensaje_">Mensaje</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <div class="g-recaptcha" data-sitekey="6Lexi6kUAAAAAGHT7BAXC4rQnAlR3NJHm0sTiQHA" data-callback="onSuccess"></div>
                    <div id="g-recaptcha-error"></div>
                </div>
                
                <br/>
                <button class="btn" type="submit" >Enviar</button>
            	<!--<input type="hidden" id="msj" value="<?php echo $mensaje ?>">-->
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
            </div>
        </form>
    </div>

</div>


</body>
<?php $this->view("brideweekend/footer"); ?>
</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
var STATE = "";

// to top right away
if ( window.location.hash ) scroll(0,0);
// void some browsers issue
setTimeout( function() { scroll(0,0); }, 1);

function submitUserForm() {
    var response = grecaptcha.getResponse();
    if(response.length == 0) {
        document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">Debes marcar la casilla para continuar</span>';
        return false;
    }else{
        document.getElementById('g-recaptcha-error').innerHTML = '';
    }
    return true;
}

var onSuccess = function(response) {
    var response = grecaptcha.getResponse();
    if(response.length != 0) {
        document.getElementById('g-recaptcha-error').innerHTML = '';
        return true;
    }
}
 
function verifyCaptcha() {
    document.getElementById('g-recaptcha-error').innerHTML = '';
}

$(function() {

    // your current click function
    $('.scroll').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top-65 + 'px'
        }, 1000, 'swing');
    });

    // *only* if we have anchor on the url
    if(window.location.hash) {

        // smooth scroll to the anchor id
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top-65 + 'px'
        }, 1000, 'swing');
    }
});
    $('#cata').on('click', function() {
        $('.cata').removeClass('oculto');
        $('.ramos').addClass('oculto');
        $('.maquillaje').addClass('oculto');
    });
    $('#ramos').on('click', function() {
        $('.ramos').removeClass('oculto');
        $('.cata').addClass('oculto');
        $('.maquillaje').addClass('oculto');
    });
    $('#maquillaje').on('click', function() {
        $('.maquillaje').removeClass('oculto');
        $('.ramos').addClass('oculto');
        $('.cata').addClass('oculto');
    });
    $(document).ready(function(){
        $.ajax({
            url: 'https://geoip-db.com/jsonp/',
            jsonpCallback: 'callback',
            dataType: 'jsonp',
            timeout: 8000,
            success: function(location) {
                if(location){
                    if(location.state)
                        STATE = location.state;
                        // alert(STATE.state);
                        // if(location.state=="Queretaro"){
                        //     $('.promociones').removeClass('hide');
                        // }
                }
            }, 

        });
        // alerta();
        $('#send_cupon').on('click', function() {
            alert("entro");
            let valoresCheck = [];
            var mail;
            $("input[type=checkbox]:checked").each(function(){
                valoresCheck.push(this.value);
            });
            valoresCheck = JSON.stringify(valoresCheck);
            // mail = $('#correo').val();
            jsShowWindowLoad("Enviando cupones, espere un momento...");
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."admin/Promotion/sendPDF" ?>',
                method: 'post',
                data: {
                    id_promotion: valoresCheck,
                    mail: $('#correoC').val(),
                },
                success: function(response) { 
                    if(response.validate==false) {
                        jsRemoveWindowLoad();
                        swal('Error', 'Ocurrio un problema, intente de nuevo', 'error');
                    } else {
                        jsRemoveWindowLoad();
                        $('#correoC').val('');
                        swal('Felicidades', 'Tus cupones fueron enviados correctamente', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
            // window.open('<?php echo base_url() ?>admin/Promotion/sendPDF?id_promotion='+valoresCheck+'&mail='+mail);
        });
        $('#mail').on('click', function() {
            $('#div-correo').removeClass('hide');
            $('#mail').addClass('hide');
        });
        $("#save_cupon").click(function () {
            let valoresCheck = [];
            $("input[type=checkbox]:checked").each(function(){
                valoresCheck.push(this.value);
            });
            valoresCheck = JSON.stringify(valoresCheck);
            window.open('<?php echo base_url() ?>admin/Promotion/createPDF?id_promotion='+valoresCheck,'_blank');
        });
        $('.jcarousel-articulos').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        speed: 300,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left flecha" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right flecha" aria-hidden="true"></i></button>',
        autoplay: true,
        lazyLoad: 'ondemand',
        responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ],
        });
        $('#teamForm').submit(function(e) {
            $('saveTeamBride').attr("disabled", true);
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url()."brideweekend/teams" ?>',
                dataType: 'json',
                method: 'post',
                data: {
                    nameTeam: $('#teamName').val(),
                    nameNovia: $('#nameNovia').val(),
                    phoneTeam: $('#phoneTeam').val(),
                    mailTeam: $('#mailTeam').val(),
                    dateTeam: $('#dateTeam').val(),
                    integrante1: $('#integrante1').val(),
                    integrante2: $('#integrante2').val(),
                    integrante3: $('#integrante3').val(),
                    integrante4: $('#integrante4').val(),
                    integrante5: $('#integrante5').val(),
                },
                success: function(response) {
                    if(response.status=='saved') {
                        swal('Correcto', 'Tu equipo fue registrado correctamente.', 'success');
                        setTimeout(function(){
                            location.reload();
                        },2000);
                    } else {
                        swal('Error', 'Lo sentimos, ocurrio un error.', 'error');
                    }
                },
                error: function() {
                    console.log('error');
                }
            });
        });
        $(".contratar").click(function () {
            $('#modal4').openModal();
        });
        window.addEventListener("hashchange", function () {
            window.scrollTo(window.scrollX, window.scrollY - 65);
        });
        // $('.registerPreview').on('click', guardarActividad);
        $("#registerMaquillaje").click(function () {
            var tipo= "maquillaje";
            guardarActividad(tipo);
        });
        $("#registerCata").click(function () {
            var tipo= "cata";
            guardarActividad(tipo);
        });
        $("#registerRamos").click(function () {
            var tipo= "ramos";
            guardarActividad(tipo);
        });
        $("#menu-home").click(function () {
            $('html,body').animate({
                scrollTop: $("#home").offset().top-65
            }, 1000);
        });
        $("#menu-actividades").click(function () {
            $('html,body').animate({
                scrollTop: $("#actividad").offset().top-65
            }, 1000);
        });
        $("#menu-bride").click(function () {
            $('html,body').animate({
                scrollTop: $("#brideweekend").offset().top-65
            }, 1000);
        });
        $("#menu-sede").click(function () {
            $('html,body').animate({
                scrollTop: $("#sedes").offset().top-65
            }, 1000);
        });
        $("#menu-expositor").click(function () {
            $('html,body').animate({
                scrollTop: $("#expositor").offset().top-65
            }, 1000);
        });
        $("#menu-novia").click(function () {
            $('html,body').animate({
                scrollTop: $("#comprador").offset().top-65
            }, 1000);
        });
        $("#menu-registro").click(function () {
            $('html,body').animate({
                scrollTop: $("#contacto").offset().top-65
            }, 1000);
        });
        $("#slider2").click(function () {
            $('html,body').animate({
                scrollTop: $("#divTeam").offset().top-65
            }, 1000);
        });
        $("#slider1").click(function () {
            $('html,body').animate({
                scrollTop: $("#divTeam").offset().top-65
            }, 1000);
        });

        $('.datepicker').pickadate({disable_picker: true, format: 'yyyy/mm/dd',
            format_submit: 'yyyy-mm-dd',
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            weekdaysShort: ['Lun','Mar','Mier','Jue', 'Vie','Sab', 'Dom'],
            weekdaysAbbrev: ['L','M','Mi','J','V','S','D']
        });
        $('.slider').slider({full_width: true, interval: 7777});
        const eventDate = "2019-09-07 12:00:00";
        setCountDownTimer(eventDate);
        $('#-gdl').on('click', updateFetch);
        $('#-qro').on('click', updateFetch);
        $('#-pue').on('click', updateFetch);
        $('#-cul').on('click', updateFetch);
        $('#gdl_movil').on('click', replaceImg);
        $('#qro_movil').on('click', replaceImg);
        $('#pue_movil').on('click', replaceImg);
        $('#cul_movil').on('click', replaceImg);
        $('.register').on('click', registrar);
        $('.ganadores').on('click', gandor);

    });

    function jsRemoveWindowLoad() {
        // eliminamos el div que bloquea pantalla
        $("#WindowLoad").remove();
    
    }

    function jsShowWindowLoad(mensaje) {
        //eliminamos si existe un div ya bloqueando
        jsRemoveWindowLoad();
    
        //si no enviamos mensaje se pondra este por defecto
        if (mensaje === undefined) mensaje = "Creando perfil, por favor espere!!";
    
        //centrar imagen gif
        height = 20;//El div del titulo, para que se vea mas arriba (H)
        var ancho = 0;
        var alto = 0;
    
        //obtenemos el ancho y alto de la ventana de nuestro navegador, compatible con todos los navegadores
        if (window.innerWidth == undefined) ancho = window.screen.width;
        else ancho = window.innerWidth;
        if (window.innerHeight == undefined) alto = window.screen.height;
        else alto = window.innerHeight;
    
        //operación necesaria para centrar el div que muestra el mensaje
        var heightdivsito = alto/2 - parseInt(height)/2;//Se utiliza en el margen superior, para centrar
    
    //imagen que aparece mientras nuestro div es mostrado y da apariencia de cargando
        imgCentro = "<div style='text-align:center;height:" + alto + "px;'><div  style='color:black;margin-top:" + heightdivsito + "px; font-size:20px;font-weight:bold'>" + mensaje + "</div><img style='width: 70px; height:70px;' src='<?php echo base_url() ?>dist/img/brideweekend/loader.gif'></div>";
    
            //creamos el div que bloquea grande------------------------------------------
            div = document.createElement("div");
            div.id = "WindowLoad"
            div.style.width = ancho + "px";
            div.style.height = alto + "px";
            $("body").append(div);
    
            //creamos un input text para que el foco se plasme en este y el usuario no pueda escribir en nada de atras
            input = document.createElement("input");
            input.id = "focusInput";
            input.type = "text"
    
            //asignamos el div que bloquea
            $("#WindowLoad").append(input);
    
            //asignamos el foco y ocultamos el input text
            $("#focusInput").focus();
            $("#focusInput").hide();
    
            //centramos el div del texto
            $("#WindowLoad").html(imgCentro);
    
    }
    var day=null;
    function guardarActividad(tipo) {
        if(tipo=="cata"){
            if(day==null) {
            var memo=document.getElementsByName('group1');
            for(i=0; i<memo.length; i++) {
                if(memo[i].checked) {
                    day=memo[i].value;
                }
            }
        }
        }else{
            day="sabado";
        }
        if($('.'+ tipo +' #nombreNovia').val()!=null &&
           $('.'+ tipo +' #nombreNovia').val()!='' &&
           $('.'+ tipo +' #fechaActividad').val()!=null &&
           $('.'+ tipo +' #fechaActividad').val()!='' &&
           $('.'+ tipo +' #ciudad').val()!=null &&
           $('.'+ tipo +' #ciudad').val()!='' &&
           $('.'+ tipo +' #telActividad').val()!=null &&
           $('.'+ tipo +' #telActividad').val()!='' &&
           $('.'+ tipo +' #correoActividad').val()!=null &&
           $('.'+ tipo +' #correoActividad').val()!='') {
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."brideweekend/registerActivity" ?>',
                method: 'post',
                data: {
                    nomNovia: $('.'+ tipo +' #nombreNovia').val(),
                    nomNovio: $('.'+ tipo +' #nombreNovio').val(),
                    fecha: $('.'+ tipo +' #fechaActividad').val(),
                    correo: $('.'+ tipo +' #correoActividad').val(),
                    ciudad: $('.'+ tipo +' #ciudad').val(),
                    telefono: $('.'+ tipo +' #telActividad').val(),
                    actividad: tipo,
                    dia: day,
                },
                success: function(response) {
                    if(response.validate==false) {
                        day=null;
                        swal('Error', 'Ya esta registrado', 'error');
                    } else if(response.validate=='llena') {
                        day=null;
                        $('.'+ tipo +' #nombreNovia').val('');
                        $('.'+ tipo +' #nombreNovio').val('');
                        $('.'+ tipo +' #correoActividad').val('');
                        $('.'+ tipo +' #ciudad').val('');
                        $('.'+ tipo +' #telActividad').val('');
                        $('.'+ tipo +' #fechaActividad').val('');
                        // $('.'+ tipo +'#modal2').closeModal();
                        swal('Error', 'Lo sentimos, la actividad llegó al máximo de registros', 'error');
                    } else {
                        day=null;
                        $('.'+ tipo +' #nombreNovia').val('');
                        $('.'+ tipo +' #nombreNovio').val('');
                        $('.'+ tipo +' #correoActividad').val('');
                        $('.'+ tipo +' #ciudad').val('');
                        $('.'+ tipo +' #telActividad').val('');
                        $('.'+ tipo +' #fechaActividad').val('');
                        // $('.'+ tipo +'#modal2').closeModal();
                        swal('Felicidades', 'Tu registro fue exitoso.', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        } else {
            day=null;
            swal('Error', 'Por favor complete todos los campos.', 'error');
        }
    }

    function gandor() {
        $('#modal3').openModal();
    }

    function registrar() {
        if($('#nombre').val()!=null && $('#correo').val()!=null && $('#telefono').val()!=null
        && $('#fecha').val()!=null && $('#nombre').val()!="" && $('#correo').val()!="" 
        && $('#telefono').val()!="" && $('#fecha').val()!="") {
            jsShowWindowLoad();
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."brideweekend/register" ?>',
                method: 'post',
                data: {
                    nombre: $('#nombre').val(),
                    correo: $('#correo').val(),
                    come_from: 'japy',
                    genero: '2',
                    telefono: $('#telefono').val(),
                    fecha: $('#fecha').val(),
                    come_from: STATE,
                },
                success: function(response) { 
                    if(response.validate==false) {
                        jsRemoveWindowLoad();
                        swal('Error', 'Correo registrado anteriormente.', 'error');
                    } else {
                        jsRemoveWindowLoad();
                        $('#nombre').val('');
                        $('#correo').val('');
                        $('#telefono').val('');
                        $('#fecha').val('');
                        $('#modal1').openModal();
                        // swal('Felicidades', 'Tu registro fue exitoso.', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        } else {
            swal('Error', 'Por favor complete todos los campos.', 'error');
        }
    }
    function replaceImg() {
        var id = $(this).attr("id");
        $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/'+id+'.png');
    }
    function updateFetch() {
        var id = $(this).attr("id");
        // var fecha = $('#'+id+'-fecha').text();
        var fecha=null;
        if(id=='-cdmx') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/cdmx.png');
            fecha = '2019-03-16 12:00:00';
        } else if(id=='-mon') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/mon.png');
            fecha = '2019-03-17 12:00:00';
        } else if(id=='-gdl') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaculiacan.png');
            fecha = '2019-02-23 12:00:00';
        } else if(id=='-leon') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/leon.png');
            fecha = '2019-03-19 12:00:00';
        } else if(id=='-qro') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/qro.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaqro.png');
            fecha = '2019-09-07 12:00:00';
        } else if(id=='-pue') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/pue.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecapuebla.png');
            fecha = '2019-09-07 12:00:00';
        } else if(id=='-cul') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/cul.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaculiacan.png');
            fecha = '2019-03-23 12:00:00';
        } else if(id=='-slp') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/slp.png');
            fecha = '2019-03-23 12:00:00';
        }
        setCountDownTimer(fecha);
    }
    
    function alerta(){
        alert($('#msj').val());
        if($('#msj').val()=='errorCaptcha'){
            alert('Es necesario rellenar el Captcha');
        }
        
    }

    var timer='';
    function setCountDownTimer(eventDate) {
        clearInterval(timer);
        let countDownDate = new Date(eventDate).getTime();
        const $eventDays = $('#event-days');
        const $eventHours = $('#event-hours');
        const $eventMinutes = $('#event-minutes');
        const $eventSeconds = $('#event-seconds');
        const $eventCounter = $('#event-counter');

        timer = setInterval(function() {
            let now = new Date().getTime();

            let distance = countDownDate - now;

            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            $eventDays.html(days);
            $eventHours.html(hours);
            $eventMinutes.html(minutes);
            $eventSeconds.html(seconds);

            if (distance < 0) {
                $eventCounter.hide();
            }
        }, 1000);
    }
</script>