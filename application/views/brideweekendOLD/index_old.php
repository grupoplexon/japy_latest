<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
    </script>
    <meta property="og:title" content="Bride Weekend Puebla" />
    <meta property="og:image" content="https://brideadvisor.mx/dist/img/brideweekend/bw_new.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:url" content="https://www.brideadvisor.mx/brideweekend" />
    <meta property="og:site_name" content="Bride Weekend Puebla" />
    <meta property="og:description" content="Todo lo que necesitas para tu boda este 24 y 25 de Agosto en Puebla, Centro Expositor los Fuertes. Pasarelas con las últimas tendencias y más de 1000 vestidos de novia en exhibición." />
    <meta property="og:type" content="website" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url() ?>dist/img/brideweekend/logo.png" rel="image_src">
    <title>Bride Weekend</title>
    <?php $this->view("brideweekend/header"); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<style>
body{
    background-color: white;
    font-family: 'Montserrat', sans-serif !important;
}
p{
    text-align: justify !important;
}
.content{
    min-height: calc(100vh - 64px);
    position: relative;
}
.indicator-item{
    width: 70px !important;
    height: 6px !important;
    box-shadow: inset 0 0 7px #8dd7e0;
    border-radius: unset !important;
    /* background-color: #fff !important; */
}
.tabs .tab .active {
    transition: .2s ease-in-out;
}
.nav {
    width:450px;
    margin:0 auto;
    list-style:none;
}
.nav li {
    float:left;
}
.nav a {
    display:block;
    text-align:center;
    width:150px; /* fixed width */
    text-decoration:none; 
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}

.card.medium .card-image {
    max-height: 80%;
    overflow: hidden;
}
hr{ 
  border: 2px solid;
  color: #8dd7e0;
  align: center;
  width: 5%
}
a {
    color: black  ;
}
h1, h3,h2,h4 {
    text-align: center;
}
h4{
    font-weight: 900;
}
.content{
    min-height: calc(100vh - 64px);
    position: relative;
}
#contact, #info{
    margin-left: 5%;
    margin-right: 5%;
}
 #registro{
    margin-left: 10%;
    margin-right: 10%;
}
#info{
    text-align: justify;
    font-family: sans-serif;
    font size: 1rem;
}

#cont{ 
  border: .5px solid;
  color: #b5b5b5;
  width: 100%;
  margin-top: 5%;
}
#registro h4{
    font-weight: bold;
}
#registro h6{
    font-size: 1.5rem !important;
}

.picker__date-display {
    background-color: #848484;
}
.picker__day--selected, .picker__day--selected:hover, .picker--focused .picker__day--selected {
    background-color: #ee6e73;
}
.picker__day.picker__day--today, .picker__close, .picker__today {
    color: #ee6e73;
}
hr{ 
  border: 2px solid;
  color: #8dd7e0;
  align: center;
  width: 5%
}
input {
    box-sizing: border-box !important;
}
.btn{
    background-color: #848484;
    border-radius: 1px;
}
#form{
    background-color: #ffff;
}
#susc{
    float: right;
}
.btn:hover, .btn-large:hover {
    background-color: #ee6e73;
}
form p{
    text-align: center !important;
    font-size: 1.5rem;
    color: #757575;
}
.centerDate {
    margin-left: 42% !important;
}
.slider .slides li img {
    height: 100%;
    width: 100%;
    background-size: cover;
    background-position: center;
}

.slide {
    height: 600px !important;
    touch-action: pan-y;
    -webkit-user-drag: none;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
.slide2 {
    height: 96% !important;
}
input[type=text]:not(.browser-default){
    margin: 0 0 0px 0;
}
input {
    display: block !important;
    font-size: 14px !important;
    line-height: 1.42857143 !important;
    color: #555 !important;
    background-color: #fff !important;
    background-image: none !important;
    border: 1px solid #ccc !important;
    border-radius: 2px !important;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075) !important;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075) !important;
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s !important;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
    padding-left: 5px !important;
}
.file-field .btn:hover, .btn-large:hover {
    background-color: #848484;
}
.btn span{
    font-weight: bold;
}
.file-field .btn{
    border-radius: 1px;
    width: 40%;
}
@media only screen and  (min-width: 500px) and  (max-width: 1000px){
    .file-field .btn{
    border-radius: 1px;
    width: fit-content;
    }
}
.pasarelas{
        display: block;
    }
    .pasarelas-movil{
        display: none;
    }
    .barberia{
        display: block;
    }
    .barberia-movil{
        display: none;
    }
@media only screen and  (min-width: 300px) and  (max-width: 500px){
    #inputs{
        margin-top: -12%;
        margin-bottom: 3%;
    }
    .pasarelas{
        display: none;
    }
    .pasarelas-movil{
        display: block;
    }
    .barberia{
        display: none;
    }
    .barberia-movil{
        display: block;
    }
    .file-field .btn{
    border-radius: 1px;
    width: 50%;
    }
    #registro{
    margin-left: 0%;
    margin-right: 0%;
    }
    .slide {
        height: 146px !important;
        touch-action: pan-y;
        -webkit-user-drag: none;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }
    .slide2 {
        height: 90% !important;
    }
    .tam-flotante {
        width: 18vh !important;
    }
    .videobw{
        height: 180px;
    }
}
.modal {
    max-height: 600px !important;
}

.flotante {
    display:scroll;
    position:fixed;
    bottom:300px;
    left:0px;
    background: unset;
    z-index: 10;
}
.flotante2 {
    display:scroll;
    position:fixed;
    bottom:220px;
    left:0px;
    background: unset;
    z-index: 10;
}
.flotante3 {
    display:scroll;
    position:fixed;
    bottom:140px;
    left:0px;
    background: unset;
    z-index: 10;
}
#WindowLoad
{
    position:fixed;
    top:0px;
    left:0px;
    z-index:3200;
    filter:alpha(opacity=65);
   -moz-opacity:65;
    opacity:0.9;
    background:#999;
}

.curso h4{
    font-size: 2.4rem;
}
.curso h6{
    font-size: 1.3rem;
}
.curso h5{
    font-size: 1.6rem;
}
.curso .row{
    margin-bottom: 0px !important;
    padding-bottom: 10px !important;
}

.makeup{
    background: #f0eae3;
    background:
    linear-gradient(115deg, transparent 65%, rgba(255,255,255,.8) 75%) 0 0,
    linear-gradient(245deg, transparent 65%, rgba(255,255,255,.8) 75%) 0 0,
    linear-gradient(115deg, transparent 65%, rgba(255,255,255,.8) 75%) 7px -15px,
    linear-gradient(245deg, transparent 65%, rgba(255,255,255,.8) 75%) 7px -15px,
    #e6e0d9;;
    background-size: 10px 30px;
}
.vino{
    background: #761C24;
    background:
    linear-gradient(115deg, transparent 65%, rgba(96,16,24,.8) 75%) 0 0,
    linear-gradient(245deg, transparent 65%, rgba(96,16,24,.8) 75%) 0 0,
    linear-gradient(115deg, transparent 65%, rgba(96,16,24,.8) 75%) 7px -15px,
    linear-gradient(245deg, transparent 65%, rgba(96,16,24,.8) 75%) 7px -15px,
    #761C24;
    background-size: 10px 30px;
}
.asesoria{
    background: #321430;
    background:
    linear-gradient(115deg, transparent 65%, rgba(41,13,39,.8) 75%) 0 0,
    linear-gradient(245deg, transparent 65%, rgba(41,13,39,.8) 75%) 0 0,
    linear-gradient(115deg, transparent 65%, rgba(41,13,39,.8) 75%) 7px -15px,
    linear-gradient(245deg, transparent 65%, rgba(41,13,39,.8) 75%) 7px -15px,
    #321430;
    background-size: 10px 30px;
}
.sushi{
    background: #1B163F;
    background:
    linear-gradient(115deg, transparent 65%, rgba(20,15,55,.8) 75%) 0 0,
    linear-gradient(245deg, transparent 65%, rgba(20,15,55,.8) 75%) 0 0,
    linear-gradient(115deg, transparent 65%, rgba(20,15,55,.8) 75%) 7px -15px,
    linear-gradient(245deg, transparent 65%, rgba(20,15,55,.8) 75%) 7px -15px,
    #1B163F;
    background-size: 10px 30px;
}

.actividades {
    text-align: center !important; 
    margin: 0px !important; 
    font-weight: bold !important;
}
.textura {
    background: #FFECED;
    background:
    linear-gradient(115deg, transparent 65%, rgba(255,255,255,.8) 75%) 0 0,
    linear-gradient(245deg, transparent 65%, rgba(255,255,255,.8) 75%) 0 0,
    linear-gradient(115deg, transparent 65%, rgba(255,255,255,.8) 75%) 7px -15px,
    linear-gradient(245deg, transparent 65%, rgba(255,255,255,.8) 75%) 7px -15px,
    #FFECED;;
    background-size: 10px 30px;
}
.textura2 {
    background: #848484;
}
.oculto {
    display: none !important;
}
.tam-flotante {
    width: 30vh;
}
.act{
    padding: 0 0rem !important;
}
.horario{
    font-weight: bold;
    font-size: 1.8rem;
}
.neg{
    padding: 2rem 1rem;
    font-size: 1.2em;
}
::-webkit-input-placeholder {
   text-align: center;
   color: black;
}

:-moz-placeholder { /* Firefox 18- */
   text-align: center;
   color: black;  
}

::-moz-placeholder {  /* Firefox 19+ */
   text-align: center;
   color: black;  
}

:-ms-input-placeholder {  
   text-align: center;
   color: black; 
}
</style>
<body>

<div class="content center-align">
    <!-- <a class='flotante' href='brideweekend/boletos' ><img src="<?php echo base_url() ?>dist/img/brideweekend/comprar.png" style="width: 27vh;"></a> -->
    <!-- <a  class='flotante' href='https://play.google.com/store/apps/details?id=com.japybodas.app' ><img src="https://cdn.worldvectorlogo.com/logos/google-play-download-android-app.svg" style="width: 20vh;"></a>
    <a class='flotante2' href='https://itunes.apple.com/mx/app/japy/id1453432572' ><img src="https://madbeerweek.com/wp-content/uploads/2017/04/app-store-logo.png" style="width: 20vh;"></a> -->
    <a class='flotante' href='https://boletos.brideadvisor.mx/evento/Puebla-2019-08' ><img class="tam-flotante" src="<?php echo base_url() ?>dist/img/brideweekend/BOTON_BW2.png"></a>
    <a class='flotante2' href='https://wa.me/523329386878' ><img src="<?php echo base_url() ?>dist/img/brideweekend/whatsapp.png" style="width: 8vh; margin-left: 2vh;"></a>
    <!-- <a class='ganadores btn grey darken-3 flotante3'>GANADORES</a> -->
    <div class="row" id="home" style="background: #4d4d4f">
        <div class="slider slide">
            <ul class="slides slide2">
                <li>
                    <a href="<?= base_url() ?>brideweekend/exposiciones"><img src="<?php echo base_url() ?>dist/img/brideweekend/slider_pue.jpg"></a>
                </li>
                <li>
                <img src="<?php echo base_url() ?>dist/img/brideweekend/slider_exp.jpg" style="cursor: pointer;">
                <!-- <div class="caption right-align">
                    <h3>Tendencias, creatividad y emoción se dan cita en BRIDE WEEKEND<strong style="font-size: 17px">©</strong></h3>
                    <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5> 
                </div> -->
                </li>
            </ul>
        </div>
    </div>

    <div class="row" id="contacto">
        <div class="row "id="form">
            <!-- <form method="POST" class="col  s12  m12 l12 "
                action="<?php echo base_url() ?>index.php/Brideweekend/registro"> -->
            <!-- <div class="col  s12  m12 l12 "> -->
                <div class="row " id="">
                    <h4 > REGÍSTRATE Y GANA</h4>
                    <hr>
                    <div class="col s12 m6 l5 offset-l1" >
                        <div class="file-field input-field " style="margin-bottom: 35px; padding-top: 150px;">
                            <div class="btn">
                                <span>Nombre</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" name="nombre" id="nombre" class="form-control" required>
                            </div>
                        </div>
                        <div class="file-field input-field " style="margin-bottom: 35px;">
                            <div class="btn">
                                <span>Email</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input id="correo" name="correo" type="text" class="validate" required>
                            </div>
                        </div>
                        <div class="file-field input-field  " style="margin-bottom: 35px;">
                            <div class="btn">
                                <span>Telefono</span>
                            </div>
                            <div class="file-path-wrapper">
                                <!-- <input type="number" name="telefono" id="telefono" class="form-control" required> -->
                                <input name="telefono" id="telefono" class="form-control" required type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                            </div>
                        </div>
                        <div class="file-field input-field ">
                            <div class="btn alinear">
                                <span>Fecha de Boda</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="date" class="" name="fecha" id="fecha" required>
                            </div>
                        </div>
                        <br>
                        <button class="btn btn-block-on-small dorado-2 register">
                           Enviar
                        </button>
                    </div><br>
                    <div class="col l6 s12 grey lighten-4">
                    <div class="col s12 m6 l6 grey lighten-4">
                        <h5>Participa para ganar increíbles premios para este día tan especial:</h5>
                        <br>
                        <strong style="font-size: 25px;">Bride&nbsp;</strong><strong style="font-weight: italic; font-size: 25px;">Weekend&nbsp;</strong><span style="font-size: 20px;">tiene todo lo que necesitas para hacer la boda perfecta en un solo lugar.</span>
                        <br><br><strong style="font-size: 23px;">Ven, vive la experiencia y gana.</strong>
                        <br><br><strong style="font-size: 23px;">Querétaro</strong>
                        <br><strong style="font-size: 20px;">Centro de Congresos</strong>
                        <br><br><strong style="font-size: 23px;">Fecha:</strong><br><strong style="font-size: 20px; font-weight: normal;">Sábado 07 y Domingo 08 de Septiembre de 2019</strong>
                        <br><br><strong style="font-size: 23px;">Horario:</strong><br><span style="font-size:20px;">Sábado </span></span><strong style="font-size: 20px; font-weight: normal;">04:00pm a 08:00pm</strong>
                        <br><span style="font-size:20px;">Domingo </span></span><strong style="font-size: 20px; font-weight: normal;">11:00am a 08:00pm</strong>
                        <br><br><strong style="font-size: 23px;">Horario de pasarela:</strong><br><span style="font-size:20px;">Sábado </span></span><strong style="font-size: 20px; font-weight: normal;">06:00pm</strong>
                        <br><span style="font-size:20px;">Domingo </span></span><strong style="font-size: 20px; font-weight: normal;">02:30pm y 05:30pm</strong>
                    </div>
                    </div>
                    <!-- <div class="col s12 center-align  " >
                        <button class="btn btn-block-on-small dorado-2 register">
                           Enviar
                        </button>
                    </div> -->
                    
                    
                </div>
                
            <!-- </div> -->
            
        </div>
    </div>

    <div class="row">
        <div class="container">
            <h4>Ganadores BrideWeekend Puebla</h4>
            <br>
            <h5>Ganadora del Crucero 7 Noches (Panamá, Colombia, Curazao, Aruba y Bonaire): <br><strong>Norma Alicia Gonzalez Torres</strong></h5>
            <br>
            <h5>Ganadores de premios generales:</h5>
            <table class="centered striped">
                <thead>
                <tr>
                    <th>Proveedor</th>
                    <th>Ganador</th>
                    <th>Premio</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td>D'OPORTOS BRIDE</td>
                    <td>Cyndell Jacqueline Pérez Ríos</td>
                    <td>VESTIDO DE NOVIA POR PRE-COMPRA</td>
                </tr>
                <tr>
                    <td>STUDIO HAIR</td>
                    <td>Diana Isabel </td>
                    <td>MAQUILLAJE Y PEINADO</td>
                </tr>
                <tr>
                    <td>HISTORIAS EN PAPEL</td>
                    <td>Irani Avalos</td>
                    <td>100 INVITACIONES</td>
                </tr>
                <tr>
                    <td>D'OPORTO NOVIAS</td>
                    <td>Gricelia Salas Paredes</td>
                    <td>UN VELO DE NOVIA</td>
                </tr>
                <tr>
                    <td>PHOTOSMILE</td>
                    <td>Raquel guzman</td>
                    <td>PAQUETE MINI/1 HORA DE SERVICIO</td>
                </tr>
                <tr>
                    <td>POLAR</td>
                    <td>Karen Jazmín Ramirez</td>
                    <td>1 SESIÓN DE FOTO PREVIA</td>
                </tr>
                <tr>
                    <td>VESTIDOS SOPHIE</td>
                    <td>Esther Castillo Sanchez</td>
                    <td>1 VESIDO</td>
                </tr>
                <tr>
                    <td>DREAMS HUATULCO</td>
                    <td>Alma susana</td>
                    <td>RESORT & SPA ESTANCIA 4 DÍAS 3 NOCHES</td>
                </tr>
                <tr>
                    <td>SECRETS HUATULCO</td>
                    <td>Gabriela Arroyo</td>
                    <td>RESORT & SPA ESTANCIA 4 DÍAS 3 NOCHES</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

     <div class="row center-align " style="background-color: #f4f4f4;">
        <div class="col l10 s12 m12 offset-l1">
            <div class="col l12 s12 m12 center-align">
                <h4>REGISTRO ACTIVIDADES</h4><br>
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/CATA-VINO.png" style="width:100%; cursor:pointer;" id="cata">
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/RAMOS.png" style="width:100%; cursor:pointer;" id="ramos">
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/MAQUILLAJE-PEINADO.png" style="width:100%; cursor:pointer;" id="maquillaje">
            </div>
        </div>
        <div class="col l12 s12 m12 oculto alinear cata" style="background-color: #f4f4f4;" id="">
            <h4>CATA DE VINOS</h4><br>
            <h5 class="horario">HORARIOS:</h5>
            <h5 style="font-size: 1.3rem;">SÁBADO 7 DE SEPTIEMBRE 7:00 PM <b class="neg"> | </b> DOMINGO 8 DE SEPTIEMBRE 6:30 PM </h5>
            <h5 style="font-size: 1.3rem;">STAND BRIDE ADVISOR</h5>
            <div class="col l12 s12 center-align" id="dia_asistencia">
                <!-- <h5>Día en que asistirás:</h5> -->
                <br>
                <form name="dia">
                <strong>
                    <label>
                        <input name="group1" type="radio" value="sabado" checked />
                        <span style="color: black; font-size: 17px;">Sábado</span>
                    </label>
                </strong>&nbsp;&nbsp;
                <strong>
                    <label>
                        <input name="group1" type="radio" value="domingo" />
                        <span style="color: black; font-size: 17px;">Domingo</span>
                    </label>
                </strong>
                </form>
                <br>
            </div>
            <div class="col l10 offset-l1">
                <div class="col l6 s12">
                    <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA"><br>
                    <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                    <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                    <br>
                </div>
                
                <div class="col l6 s12">
                    <input type="text" id="nombreNovio" placeholder="NOMBRE DEL NOVIO"><br>
                    <input type="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                    <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                </div>
                <div class="col l12 s12 center-align">
                    <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p><br>
                    <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                </div>
            </div>
            <div class="col l12 s12 center-align">
                <br>
                <button class="btn grey darken-1 registerPreview " id="registerCata" style=" height: 50px; border-radius: 15px;
                font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
            </div>
        </div>
        <div class="col l12 s12 m12 oculto alinear maquillaje">
            <h4>MASTER CLASS DE MAQUILLAJE</h4><br>
            <h5 class="horario">HORARIOS:</h5>
            <h5>SÁBADO 7 DE SEPTIEMBRE <b class="neg"> | </b>  7:00 PM <b class="neg"> | </b> STAND BRIDE ADVISOR</h5>
            <br>
            <div class="col l10 offset-l1 s12">
                <div class="col l6 offset-l3 s12">
                    <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA"><br>
                </div>
                <div class="col l6 s12">
                    <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                    <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                    <br>
                </div>
                <div class="col l6 s12">
                    <input type="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                    <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                </div>
                <div class="col l12 s12">
                    <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p><br>
                    <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                </div>
                <div class="col l12 s12 center-align">
                    <br>
                    <button class="btn grey darken-1 registerPreview " id="registerMaquillaje" style=" height: 50px; border-radius: 15px;
                    font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
                </div>
            </div>                
        </div>
        <div class="col l12 s12 m12 oculto alinear ramos">
            <h4>TALLER DE RAMOS DE NOVIA</h4><br>
            <h5 class="horario">HORARIOS:</h5>
            <h5>SÁBADO 7 DE SEPTIEMBRE <b class="neg"> | </b>  1:00 PM <b class="neg"> | </b> STAND BRIDE ADVISOR</h5>
            <br>
            <div class="col l10 offset-l1 s12">
                    <div class="col l6 offset-l3 s12">
                        <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA">
                        <br>
                    </div>
                    <div class="col l6 s12">
                        
                        <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                        <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                        <br>
                    </div>
                    <div class="col l6 s12">
                        <!-- <input type="text" id="nombreNovio" placeholder="NOMBRE DEL NOVIO">
                        <br> -->
                        <input type="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                        <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                    </div>
                    <div class="col l12 s12">
                        <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p>
                        <br>
                        <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                    </div>
                    
                </div>
                <div class="col l12 s12 center-align">
                    <br>
                    <button class="btn grey darken-1 registerPreview " id="registerRamos" style=" height: 50px; border-radius: 15px;
                    font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
                </div>
                
        </div>
    </div> 
    
     <!-- <div class="row center-align curso" id="actividad">

         <div class="row" id="actividad">
            <div class="row "id="form">
                    <div class="row " id="">
                        <h4 > PROGRAMA DE ACTIVIDADES</h4>
                        <hr>
                        <div class="" style="margin-left: 7%; margin-right: 7%;">
                            <img style="width:100%;" src="<?php echo base_url() ?>dist/img/brideweekend/landing-evento.jpg" alt="">
                        </div>
                    </div>
            </div>
        </div>

        <div class="row makeup" style="padding-left:15%; padding-right:15%;  text-align: center;"> 
            <div class="col s12 l12"> 
                <h4 style="">CLASE DE MIXOLOGÍA</h4>
                <br>
                <div class="row " > 
                    <div class="col s12 l4" style="padding-top:4%;">
                        <img src="<?php echo base_url() ?>dist/img/brideweekend/mixologia.png" style="width: 100%">
                    </div>
                    <div class="col s12 l4" style="padding-top:3%;"> 
                        
                        <h6 style="font-weight:bold;">HORARIO: </h6>
                        <h5>SÁBADO Y DOMINGO</h5>
                        <h5>04:30 PM</h5>
                    </div>
                    <div class="col s12 l4 center-align" style="padding-top:7%;">   
                        <button class="btn btn-block-on-small dorado-2 activity  grey darken-2" id="mixologia">
                           REGÍSTRATE GRATIS
                        </button>
                        <br><br>
                        <h6>Limitado a 10 parejas.</h6>
                    </div>
                </div>
            </div>
        </div>

    </div>  -->
    
    <div class="row center-align" id="sedes" style="background: white;">
        <span class="row">
            <br><i class="fa fa-map-marker" aria-hidden="true" style="font-size: 5vh; color: #848484;"></i><br>
            <strong style="color: #848484;font-size: 4vh;">UBICACIÓN</strong>
            <hr>
        </span>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <div id="tab-sedes">
            <div class="col s12 l12" style="height: 500px" id="location">
                <div class="col l6 alinear" style="height: 100%">
                    <img class="banner-sedes" style="width: 100%; height: 100%;" src="<?php echo base_url() ?>'+'dist/img/brideweekend/qro.jpg">
                </div>
                <div class="col l6 alinear" style="height: 100%" id="div-map">
                    <iframe id="mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3735.2362624184007!2d-100.35181168563265!3d20.578406986244076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d35ad251120a2f%3A0x80670acc687aff68!2sQuer%C3%A9taro%20Centro%20de%20Congresos!5e0!3m2!1ses-419!2smx!4v1566834210092!5m2!1ses-419!2smx" style="width: 100%; height: 100%;" frameborder="0" style="border:0"></iframe>
                </div>
            </div>

            <div class="col l12" style="background: #515151 !important;">
                <div class="row" id="event-counter">
                    <div class="faltante col s12" style="padding: 0">
                        <div class="col l1"></div>
                        <div class="col s12 l2 center-align counter-container"  style="border-right: white solid 1px; border-left: white solid 1px;">
                            <img class="plecas" src="">
                        </div>
                        <div id="div-dias" class="col s3 l2 ofset-m4 center-align counter-container" style="border-right: white solid 1px;">
                            <h5 class="gris-1-text no-margin counter-data" style="margin-top: 13vh;">
                                <b class="dias" id="event-days" style="color: white; font-size: 80px;">
                                    
                                </b>
                            </h5>
                            <p class="counter-title " style="color: white; margin-left: 13vh;">D&iacute;as</p>
                        </div>
                        <div class="col s3 l2 center-align counter-container" style="border-right: white solid 1px;">
                            <h5 class="gris-1-text no-margin counter-data" style="margin-top: 13vh;">
                                <b class="horas" id="event-hours" style="color: white; font-size: 80px;">
                                    
                                </b>
                            </h5>
                            <p class="counter-title " style="color: white; margin-left: 13vh;">Horas</p>
                        </div>
                        <div class="col s3 l2 center-align counter-container" style="border-right: white solid 1px;">
                            <h5 class="gris-1-text no-margin counter-data" style="margin-top: 13vh;">
                                <b class="minutos" id="event-minutes" style="color: white; font-size: 80px;">
                                    
                                </b>
                            </h5>
                            <p class="counter-title" style="color: white; margin-left: 13vh;">Min</p>
                        </div>
                        <div class="col s3 l2 center-align counter-container" style="border-right: white solid 1px;">
                            <h5 class="gris-1-text no-margin counter-data" style="margin-top: 13vh;">
                                <b class="segundos" id="event-seconds" style="color: white; font-size: 80px;">
                                    
                                </b>
                            </h5>
                            <p class="counter-title" style="color: white; margin-left: 13vh;">Seg</p>
                        </div>
                        <div class="col l1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="brideweekend">
        <div class="col l6 s12" id="img-bride">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend4.jpg" style="width: 100%">
            <!-- <iframe class="videobw" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fbrideweekendmx%2Fvideos%2F347270985963979%2F&show_text=0&width=560" width="100%" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe> -->
        </div>
        <div class="col l6">
            <div class="col s12 l8 alinear" style="margin-top: 30px; margin-left: 70px" id="title-bride">
                <span class="row">
                    <div class="col s12 l8 alinear" style="text-align: left !important;">
                        <strong style="color: black;font-size: 4vh; ">CONCEPTO</strong>
                    </div>
                    <div class="col s12 l12 alinear">
                    <hr id="line2" style="position: absolute;    background: #8dd7e0; width: 40px;color: #f38083;display: block;height: 1px;border: 0;border: 2px solid #8dd7e0;padding: 0; ">
                    </div>
                </span>
            </div>
            <div class="col l12 alinear">
            <p id="text-bride" style="text-align: justify; margin-left: 70px; margin-right: 70px;">
            En BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> reinventamos el concepto tradicional de las expos de boda, en una experiencia única capaz de ofrecer a las parejas en un solo fin de semana y en un único espacio todo lo necesario para crear su boda perfecta.
            <br> Nos mantenemos a la vanguardia en la industria nupcial, al contar con más de 2,000 modelos de vestidos de diferentes marcas para estar completamente seguros de que aquí encontrarán el ideal.
            <br><br>
            Tendencias, creatividad y emoción se dan cita en BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong>, donde encontrarás una variada y seleccionada oferta de servicios nupciales, conocer
            novedades, obtener el mejor asesoramiento, disfrutar de desfiles y obtener regalos,
            para hacer realidad la boda de tus sueños.
                <br><br>
                <!-- <button class="btn grey darken-1">POLÍTICAS DE ACCESO</button> -->
            </p>
            </div>
        </div>
        <div class="col l6" style="display: none;" id="img-bride2">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend4.jpg" style="width: 100%">
        </div>
    </div>

    <div class="row" id="comprador" style="background: #cdcdcd">
        <div class="col l5 s12 offset-l1">
            <div class="col s12 l8 alinear" style="margin-top: 20px; margin-left: 70px" id="title-novia">
                <span class="row">
                    <div class="col s2 l1 alinear">
                        <strong style="color: black;font-size: 4vh;">NOVIA</strong>
                    </div>
                    <div class="col s12 l12 alinear">
                    <hr id="line2" style="position: absolute; width: 40px;background: #515151;color: #515151;display: block;height: 1px;border: 0;border: 2px solid #515151;padding: 0; ">
                    </div>
                </span>
            </div>
            <div class="col l12 alinear">
                <p id="text-novia" style="text-align: justify; margin-left: 70px; margin-right: 70px;">
                En BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> nos esforzamos cada día para que todo salga impecable, nuestro compromiso es hacer realidad el sueño de la boda perfecta de cada pareja según sus gustos e ideas.
                <br><br>
                Somos especialistas en reducir el estrés y calmar a los novios en esos momentos de nervios durante la organización de su boda, logrando que las vivencias durante todo este proceso resulten divertidas, excitantes, con ilusión y sobre todo con un concepto vanguardista, jóven, moderno e incluyendo las ultimas tendencias de moda. 
                <br> Por ello tendremos dos pasarelas por día con las últimas tendencias de vestidos de novia en horario de 2:30 y 5:30 para que nadie quede fuera. 
                <br><br>
                Te recordamos que nuestro horario de atención en la expo será Sábado de 04:00pm a 08:00pm y Domingo de 12:pm a 08:00pm este 07 y 08 de Septiembre en Centro de Congresos.
                <!-- <br><br>
                Déjanos apoyarte con nuestra experiencia a llevar la batuta frente a todo el equipo de proveedores que forman parte de la orquesta, para que toda la pieza musical salga perfecta. -->
                    
                    <!-- <button class="btn grey darken-1">POLÍTICAS DE ACCESO</button> -->
                </p>
            </div>
        </div>
        <div class="col l6" id="img-novia">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/novia.jpg" style="width: 100%">
        </div>
        <!-- <div class="col l6" style="display: none;" id="img-novia2">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/novia.jpg" style="width: 100%">
        </div> -->
    </div>

    <div class="row" id="expositor">
        <div class="col l6 hide-on-small-only" id="img-expositor">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/expositor.jpg" >
        </div>
        <div class="col l6">
            <div class="col s12 l8 alinear" style="margin-top: 30px; margin-left: 0px" id="title-expositor">
                <span class="row">
                    <div class="col s12 l8 alinear" style="text-align: left !important;">
                        <strong style="color: black;font-size: 4vh; ">EXPOSITOR</strong>
                    </div>
                    <div class="col s12 l12 alinear">
                    <hr id="line2" style="position: absolute;    background: #8dd7e0; width: 40px;color: #f38083;display: block;height: 1px;border: 0;border: 2px solid #8dd7e0;padding: 0; ">
                    </div>
                </span>
            </div>
            <div class="col l12 alinear">
            <p id="text-expositor" style="text-align: justify; margin-left: 0px; margin-right: 70px;">
            BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> es la cita ineludible para los profesionales de la industria nupcial y el evento ideal para todas aquellas parejas que están planeando su boda y que puedan vivir la experiencia de encontrar lo necesario para el día más importante de sus vidas.
                    <br><br>
                    Un nuevo concepto de exposición en el que las parejas, podrán disfrutar de pasarelas y otras actividades en las que obsequiarán importantes premios como productos, y servicios para su boda, lunas de miel y hasta un auto.
                    <br><br>
                    Somos líderes en el mercado de exposiciones, debido a nuestra experiencia con diversos conceptos de éxito,  esperamos a más de 5,000 asistentes en esta edición y contar con la participación de 200 expositores de diferentes giros.
                <!-- <button class="btn grey darken-1">POLÍTICAS DE ACCESO</button> -->
            </p>
            </div>
            <div class="col l12">
                <br>
                <a class="btn contratar"  style="background: #848484">CONTRATA AQUÍ</a>
                <br><br>
            </div>
        </div>
        <div class="col l6" style="display: none;" id="img-expositor2">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/expositor.jpg" style="width: 100%">
        </div>
    </div>


    <!--<div class="row center-align white">-->
    <!--    <div class="col s12 l12 center-align white">-->
    <!--        <a style="background: #848484" class="btn" href="<?php echo base_url() ?>dist/archivo/expositoresgdl.pdf" target="_blank">LISTA DE EXPOSITORES</a>-->
    <!--    </div>-->
    <!--</div>-->

    <div class="row">
        <div class="col s12 l12 center-align" style="background: #E9EBEA; margin-bottom: -20px !important; ">
            <br>
            <span class="row">
                <strong style="color: black;font-size: 4vh;">MIEMBROS DE ASOCIACIONES</strong>
                <hr style="width: 40px;color: #f38083;display: block;background: #515151;height: 1px;border: 0;border: 2px solid #515151;padding: 0; ">
            </span>
            <!-- <br>
            <img src="<?php echo base_url() ?>dist/img/brideweekend/comprador-footer.png" style="width: 100%"> -->
            <div class="container">
            <div class="col l4 s12">
                <a href="http://amprofec.org"><img src="<?php echo base_url() ?>dist/img/brideweekend/amprofec.png" style="width: 80%"></a>
            </div>
            <div class="col l4 s12">
                <a href="https://nupcialmexicana.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/anm.png" style="width: 82%"></a>
            </div>
            <div class="col l4 s12">
                <a href="https://brideadvisor.mx"><img src="<?php echo base_url() ?>dist/img/brideweekend/plecaBrideAdv.png" style="width: 64%"></a>
            </div>
            </div>
        </div>
    </div>


    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                        style="float: right !important;
                        position: absolute !important;
                        right:0px; !important; color: #8DD7E0 !important;
                        font-size:4vh;">X</button>
        <!-- <button class="btn grey darken-2" style="position: absolute; margin-top: 29vh; margin-left: 66vh;">COMPRA TU BOLETO Y GANA</button> -->
        <a href="brideweekend/boletos"><div class="col m12 l12" style="position: absolute; width: 100%; height: 50%; cursor: pointer;"></div></a>
        <a href="<?php echo base_url() ?>novios/perfil"><div class="col m12 l12" style="position: absolute; width: 100%; height: 50%; cursor: pointer;margin-top: 37%;"></div></a>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/registro_exitoso.png" style="width: 100%">
        <!-- <div style="height:50%; cursor:pointer;"> </div> -->
        <!-- <map name="billar">
            <area alt="Si clías aquí irás a la portada" shape="rect" coords="509,180,628,222" href="brideweekend/boletos">
            <area alt="Si clías aquí irás a la portada" shape="rect" coords="500,410,633,460" href="<?php echo base_url() ?>novios/perfil">
        </map> -->
        <!-- <div class="modal-content">
        <h4>Modal Header</h4>
        <p>A bunch of text</p>
        </div>
        <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
        </div> -->
    </div>

    <!-- Modal Structure -->
    <div id="modal2" class="modal">
    <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                        style="position: absolute !important; right: 0px; font-weight: bold;">X</button>
        <div class="modal-content">
            <div class="row textura">
                <!-- <div class="col l6" style="background: #7A7C7F">
                    <img src="<?php echo base_url() ?>dist/img/brideweekend/logo_bw.png" alt="">
                </div>
                <div class="col l6" style="background: #7A7C7F">
                    <img src="<?php echo base_url() ?>dist/img/brideweekend/logo_bw.png" alt="">
                </div> -->
                <div class="col l12 textura2">
                    <p id="titleActivity" class="actividades" style="color: white; font-size: 5vh;"></p>
                </div>
                <div class="textura">
                <div class="col l12 center-align">
                    <br>
                    <strong style="font-size: 3vh;">HORARIOS</strong> <br>
                    <strong id="fechaActivity"></strong>
                    <br><br>
                </div>
                <div class="col l12">
                    <div class="col l6">
                        <p class="actividades">NOMBRE DE LA NOVIA</p>
                        <input type="text" id="nombreNovia">
                        <p class="actividades" style="margin-top: 17px !important;">FECHA DE LA BODA</p>
                        <input type="date" id="fechaActividad">
                        <p class="actividades">CIUDAD DE LA BODA</p>
                        <input type="text" id="ciudad">
                    </div>
                    <div class="col l6">
                        <p class="actividades">NOMBRE DEL NOVIO</p>
                        <input type="text" id="nombreNovio">
                        <p class="actividades" style="margin-top: 17px !important;">CORREO ELECTRÓNICO</p>
                        <input type="email" id="correoActividad">
                        <p class="actividades">TELÉFONO DE CONTACTO</p>
                        <input id="telActividad" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                    </div>
                </div>
                <!-- <div class="col l12 center-align" id="dia_asistencia">
                    <br>
                    <h5>Día en que asistirás:</h5>
                    <form name="dia">
                    <strong>
                        <label>
                            <input name="group1" type="radio" value="sabado" checked />
                            <span style="color: black; font-size: 17px;">Sábado</span>
                        </label>
                    </strong>&nbsp;&nbsp;
                    <strong>
                        <label>
                            <input name="group1" type="radio" value="domingo" />
                            <span style="color: black; font-size: 17px;">Domingo</span>
                        </label>
                    </strong>
                    </form>
                </div> -->
                <!-- <div class="col l12">
                    <br><br>
                    <div class="container">
                        <p class="actividades">3 HORAS ANTES DEL EVENTO RECIBIRÁS UN WHATSAPP A EL NÚMERO DE CONTACTO REGISTRADO PARA CONFIRMAR TU PARTICIPACIÓN.</p>
                    </div>
                </div> -->
                <div class="col l12 center-align">
                    <br><br>
                    <button class="btn grey darken-1 registerPreview">REGISTRAR ACTIVIDADES</button> <br><br>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="modal3" class="modal">
        <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                        style="float: right !important;
                        position: absolute !important;
                        right:0px; !important; color: #8DD7E0 !important;
                        font-size:4vh;">X</button>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/headganadores.png" style="width: 100%">
        <div class="row">
            <table class="centered striped">
                <thead>
                    <tr>
                        <th>FOLIO</th>
                        <th>NOMBRE</th>
                        <th>PREMIO</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1838</td>
                    <td>Marisol Pelayo</td>
                    <td>Estancia  en el hotel Pelicanos 3 días y 2 noches 2 adultos y dos menores</td>
                </tr>
                <tr>
                    <td>2922</td>
                    <td>Angelica</td>
                    <td>Barra de quesos  para tu boda</td>
                </tr>
                <tr>
                    <td>999</td>
                    <td>Carolina Vega</td>
                    <td>Maquillaje de novia</td>
                </tr>
                </tbody>
            </table>
        </div>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/footerganadores.png" style="width: 100%">
    </div>
    
    <div id="modal4" class="modal">
        <br>
        <form method="POST" class="col  s12  m12 l12 " onsubmit="return submitUserForm();"
                action="<?php echo base_url()."brideweekend/email_expositores"?>">
            <div class="model-content row center-align">
                <h4>CONTACTAR</h4>
                <div class="input-field col s10 offset-s1">
                    <input id="nombre_" type="text" name="name" class="validate" required>
                    <label for="nombre_">Nombre</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <input id="correo_" type="text" name="email" class="validate" required>
                    <label for="correo_">Correo</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <input id="telefono_" type="text" name="telefono" class="validate" required>
                    <label for="telefono_">Teléfono</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <textarea id="mensaje_" type="text-area" name="msg" class="validate" required></textarea>
                    <label for="mensaje_">Mensaje</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <div class="g-recaptcha" data-sitekey="6Lexi6kUAAAAAGHT7BAXC4rQnAlR3NJHm0sTiQHA" data-callback="onSuccess"></div>
                    <div id="g-recaptcha-error"></div>
                </div>
                
                <br/>
                <button class="btn" type="submit" >Enviar</button>
            	<!--<input type="hidden" id="msj" value="<?php echo $mensaje ?>">-->
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
            </div>
        </form>
    </div>

</div>


</body>
<?php $this->view("brideweekend/footer"); ?>
</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
var STATE = "";

// to top right away
if ( window.location.hash ) scroll(0,0);
// void some browsers issue
setTimeout( function() { scroll(0,0); }, 1);

function submitUserForm() {
    var response = grecaptcha.getResponse();
    if(response.length == 0) {
        document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">Debes marcar la casilla para continuar</span>';
        return false;
    }else{
        document.getElementById('g-recaptcha-error').innerHTML = '';
    }
    return true;
}

var onSuccess = function(response) {
    var response = grecaptcha.getResponse();
    if(response.length != 0) {
        document.getElementById('g-recaptcha-error').innerHTML = '';
        return true;
    }
}
 
function verifyCaptcha() {
    document.getElementById('g-recaptcha-error').innerHTML = '';
}

$(function() {

    // your current click function
    $('.scroll').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top-65 + 'px'
        }, 1000, 'swing');
    });

    // *only* if we have anchor on the url
    if(window.location.hash) {

        // smooth scroll to the anchor id
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top-65 + 'px'
        }, 1000, 'swing');
    }

});

    

    $('#cata').on('click', function() {
        $('.cata').removeClass('oculto');
        $('.ramos').addClass('oculto');
        $('.maquillaje').addClass('oculto');
    });
    $('#ramos').on('click', function() {
        $('.ramos').removeClass('oculto');
        $('.cata').addClass('oculto');
        $('.maquillaje').addClass('oculto');
    });
    $('#maquillaje').on('click', function() {
        $('.maquillaje').removeClass('oculto');
        $('.ramos').addClass('oculto');
        $('.cata').addClass('oculto');
    });
    $(document).ready(function(){
        
        // alerta();

        $('#teamForm').submit(function(e) {
            $('saveTeamBride').attr("disabled", true);
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url()."brideweekend/teams" ?>',
                dataType: 'json',
                method: 'post',
                data: {
                    nameTeam: $('#teamName').val(),
                    nameNovia: $('#nameNovia').val(),
                    phoneTeam: $('#phoneTeam').val(),
                    mailTeam: $('#mailTeam').val(),
                    dateTeam: $('#dateTeam').val(),
                    integrante1: $('#integrante1').val(),
                    integrante2: $('#integrante2').val(),
                    integrante3: $('#integrante3').val(),
                    integrante4: $('#integrante4').val(),
                    integrante5: $('#integrante5').val(),
                },
                success: function(response) {
                    if(response.status=='saved') {
                        swal('Correcto', 'Tu equipo fue registrado correctamente.', 'success');
                        setTimeout(function(){
                            location.reload();
                        },2000);
                    } else {
                        swal('Error', 'Lo sentimos, ocurrio un error.', 'error');
                    }
                },
                error: function() {
                    console.log('error');
                }
            });
        });
        
        $(".contratar").click(function () {
            $('#modal4').openModal();
        });

        $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    if(location){
                        if(location.state)
                            STATE = location.state;
                    }
                },
            });

        window.addEventListener("hashchange", function () {
            window.scrollTo(window.scrollX, window.scrollY - 65);
        });

        // $('.registerPreview').on('click', guardarActividad);

        $("#registerMaquillaje").click(function () {
            var tipo= "maquillaje";
            guardarActividad(tipo);
        });

        $("#registerCata").click(function () {
            var tipo= "cata";
            guardarActividad(tipo);
        });

        $("#registerRamos").click(function () {
            var tipo= "ramos";
            guardarActividad(tipo);
        });

        $("#menu-home").click(function () {
            $('html,body').animate({
                scrollTop: $("#home").offset().top-65
            }, 1000);
        });
        $("#menu-actividades").click(function () {
            $('html,body').animate({
                scrollTop: $("#actividad").offset().top-65
            }, 1000);
        });
        $("#menu-bride").click(function () {
            $('html,body').animate({
                scrollTop: $("#brideweekend").offset().top-65
            }, 1000);
        });
        $("#menu-sede").click(function () {
            $('html,body').animate({
                scrollTop: $("#sedes").offset().top-65
            }, 1000);
        });
        $("#menu-expositor").click(function () {
            $('html,body').animate({
                scrollTop: $("#expositor").offset().top-65
            }, 1000);
        });
        $("#menu-novia").click(function () {
            $('html,body').animate({
                scrollTop: $("#comprador").offset().top-65
            }, 1000);
        });
        $("#menu-registro").click(function () {
            $('html,body').animate({
                scrollTop: $("#contacto").offset().top-65
            }, 1000);
        });
        $("#slider2").click(function () {
            $('html,body').animate({
                scrollTop: $("#divTeam").offset().top-65
            }, 1000);
        });
        $("#slider1").click(function () {
            $('html,body').animate({
                scrollTop: $("#divTeam").offset().top-65
            }, 1000);
        });

        $('.datepicker').pickadate({disable_picker: true, format: 'yyyy/mm/dd',
            format_submit: 'yyyy-mm-dd',
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            weekdaysShort: ['Lun','Mar','Mier','Jue', 'Vie','Sab', 'Dom'],
            weekdaysAbbrev: ['L','M','Mi','J','V','S','D']
        });
        $('.slider').slider({full_width: true, interval: 7777});
        const eventDate = "2019-09-07 12:00:00";
        setCountDownTimer(eventDate);
        // $('#-cdmx').on('click', updateFetch);
        // $('#-mon').on('click', updateFetch);
        $('#-gdl').on('click', updateFetch);
        // $('#-leon').on('click', updateFetch);
        $('#-qro').on('click', updateFetch);
        $('#-pue').on('click', updateFetch);
        $('#-cul').on('click', updateFetch);
        // $('#-slp').on('click', updateFetch);

        // $('#mon_movil').on('click', replaceImg);
        // $('#cdmx_movil').on('click', replaceImg);
        $('#gdl_movil').on('click', replaceImg);
        // $('#leon_movil').on('click', replaceImg);
        $('#qro_movil').on('click', replaceImg);
        $('#pue_movil').on('click', replaceImg);
        $('#cul_movil').on('click', replaceImg);
        $('.register').on('click', registrar);
        $('.ganadores').on('click', gandor);
        // $('.activity').on('click', registrarActividad);
        // $('#slp_movil').on('click', replaceImg);

        var medida= $( window ).width();
        if (medida< 326) {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/qro.jpg');
            $('#location').css('height', 'unset');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaqro.png');
            $('#mapa').css('height', '230px');
            $('#div-map').css('width', '100%');
            $('#event-days').css('font-size', '28px');
            $('#event-minutes').css('font-size', '28px');
            $('#event-seconds').css('font-size', '28px');
            $('#event-hours').css('font-size', '28px');
            $('.counter-title').css('margin-left', 'unset');
            $('.counter-data').css('margin-top', '2vh');
            $('#title-bride').css('margin-left', 'unset');
            $('#title-expositor').css('margin-left', 'unset');
            $('#text-bride').css('margin-left', 'unset');
            $('#text-bride').css('margin-top', 'unset');
            $('#text-bride').css('margin-right', 'unset');
            $('#text-expositor').css('margin-left', 'unset');
            $('#text-expositor').css('margin-top', 'unset');
            $('#text-expositor').css('margin-right', 'unset');
            // $('#img-bride').css('display', 'none');
            $('#img-bride2').css('display', 'block');
            $('#img-expositor').css('display', 'none');
            $('#img-expositor2').css('display', 'block');
            $('#title-novia').css('margin-left', 'unset');
            $('#text-novia').css('margin-left', 'unset');
            $('#text-novia').css('margin-top', 'unset');
            $('#text-novia').css('margin-right', 'unset');
            // $('#img-novia').css('display', 'none');
            // $('#img-novia2').css('display', 'block');
        }
        else if (medida < 426) {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/qro.jpg');
            $('#location').css('height', 'unset');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaqro.png');
            $('#mapa').css('height', '230px');
            $('#div-map').css('width', '100%');
            $('#event-days').css('font-size', '28px');
            $('#event-minutes').css('font-size', '28px');
            $('#event-seconds').css('font-size', '28px');
            $('#event-hours').css('font-size', '28px');
            $('.counter-title').css('margin-left', 'unset');
            $('.counter-data').css('margin-top', '2vh');
            $('#title-bride').css('margin-left', 'unset');
            $('#title-expositor').css('margin-left', 'unset');
            $('#text-bride').css('margin-left', 'unset');
            $('#text-bride').css('margin-top', 'unset');
            $('#text-bride').css('margin-right', 'unset');
            $('#text-expositor').css('margin-left', 'unset');
            $('#text-expositor').css('margin-top', 'unset');
            $('#text-expositor').css('margin-right', 'unset');
            // $('#img-bride').css('display', 'none');
            $('#img-bride2').css('display', 'block');
            $('#img-expositor').css('display', 'none');
            $('#img-expositor2').css('display', 'block');
            $('#title-novia').css('margin-left', 'unset');
            $('#text-novia').css('margin-left', 'unset');
            $('#text-novia').css('margin-top', 'unset');
            $('#text-novia').css('margin-right', 'unset');
            // $('#img-novia').css('display', 'none');
            // $('#img-novia2').css('display', 'block');
        }
        else if (medida < 769) {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/qro.jpg');
            $('#location').css('height', 'unset');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaqro.png');
            $('#mapa').css('height', '230px');
            $('#div-map').css('width', '100%');
            $('#event-days').css('font-size', '28px');
            $('#event-minutes').css('font-size', '28px');
            $('#event-seconds').css('font-size', '28px');
            $('#event-hours').css('font-size', '28px');
            $('.counter-title').css('margin-left', 'unset');
            $('.counter-data').css('margin-top', '2vh');
            $('#title-bride').css('margin-left', 'unset');
            $('#title-expositor').css('margin-left', 'unset');
            $('#text-bride').css('margin-left', 'unset');
            $('#text-bride').css('margin-top', 'unset');
            $('#text-bride').css('margin-right', 'unset');
            $('#text-expositor').css('margin-left', 'unset');
            $('#text-expositor').css('margin-top', 'unset');
            $('#text-expositor').css('margin-right', 'unset');
            // $('#img-bride').css('display', 'none');
            $('#img-bride2').css('display', 'block');
            $('#img-expositor').css('display', 'none');
            $('#img-expositor2').css('display', 'block');
            $('#title-novia').css('margin-left', 'unset');
            $('#text-novia').css('margin-left', 'unset');
            $('#text-novia').css('margin-top', 'unset');
            $('#text-novia').css('margin-right', 'unset');
            // $('#img-novia').css('display', 'none');
            // $('#img-novia2').css('display', 'block');
        }
        else if (medida < 2100) {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/qro.jpg');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaqro.png');
        }
    });

    function jsRemoveWindowLoad() {
        // eliminamos el div que bloquea pantalla
        $("#WindowLoad").remove();
    
    }

    function jsShowWindowLoad(mensaje) {
        //eliminamos si existe un div ya bloqueando
        jsRemoveWindowLoad();
    
        //si no enviamos mensaje se pondra este por defecto
        if (mensaje === undefined) mensaje = "Creando perfil, por favor espere!!";
    
        //centrar imagen gif
        height = 20;//El div del titulo, para que se vea mas arriba (H)
        var ancho = 0;
        var alto = 0;
    
        //obtenemos el ancho y alto de la ventana de nuestro navegador, compatible con todos los navegadores
        if (window.innerWidth == undefined) ancho = window.screen.width;
        else ancho = window.innerWidth;
        if (window.innerHeight == undefined) alto = window.screen.height;
        else alto = window.innerHeight;
    
        //operación necesaria para centrar el div que muestra el mensaje
        var heightdivsito = alto/2 - parseInt(height)/2;//Se utiliza en el margen superior, para centrar
    
    //imagen que aparece mientras nuestro div es mostrado y da apariencia de cargando
        imgCentro = "<div style='text-align:center;height:" + alto + "px;'><div  style='color:black;margin-top:" + heightdivsito + "px; font-size:20px;font-weight:bold'>" + mensaje + "</div><img style='width: 70px; height:70px;' src='<?php echo base_url() ?>dist/img/brideweekend/loader.gif'></div>";
    
            //creamos el div que bloquea grande------------------------------------------
            div = document.createElement("div");
            div.id = "WindowLoad"
            div.style.width = ancho + "px";
            div.style.height = alto + "px";
            $("body").append(div);
    
            //creamos un input text para que el foco se plasme en este y el usuario no pueda escribir en nada de atras
            input = document.createElement("input");
            input.id = "focusInput";
            input.type = "text"
    
            //asignamos el div que bloquea
            $("#WindowLoad").append(input);
    
            //asignamos el foco y ocultamos el input text
            $("#focusInput").focus();
            $("#focusInput").hide();
    
            //centramos el div del texto
            $("#WindowLoad").html(imgCentro);
    
    }

    $(window).resize(function() {
        var medida= $( window ).width();
        if (medida< 326) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            // $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');
        }
        else if (medida < 426) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            // $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');
        }
        else if (medida < 769) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            // $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');
        }
        else if (medida < 2100) {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl.png');
            // $("#tab-sedes").css('display','block');
            // $("#menu-movil").css('display','none');
            $("#head_mov").css('display','none');
        }
    });
    var day=null;
    function guardarActividad(tipo) {
        if(tipo=="cata"){
            if(day==null) {
            var memo=document.getElementsByName('group1');
            for(i=0; i<memo.length; i++) {
                if(memo[i].checked) {
                    day=memo[i].value;
                }
            }
        }
        }else{
            day="sabado";
        }
        if($('.'+ tipo +' #nombreNovia').val()!=null &&
           $('.'+ tipo +' #nombreNovia').val()!='' &&
           $('.'+ tipo +' #fechaActividad').val()!=null &&
           $('.'+ tipo +' #fechaActividad').val()!='' &&
           $('.'+ tipo +' #ciudad').val()!=null &&
           $('.'+ tipo +' #ciudad').val()!='' &&
           $('.'+ tipo +' #telActividad').val()!=null &&
           $('.'+ tipo +' #telActividad').val()!='' &&
           $('.'+ tipo +' #correoActividad').val()!=null &&
           $('.'+ tipo +' #correoActividad').val()!='') {
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."brideweekend/registerActivity" ?>',
                method: 'post',
                data: {
                    nomNovia: $('.'+ tipo +' #nombreNovia').val(),
                    nomNovio: $('.'+ tipo +' #nombreNovio').val(),
                    fecha: $('.'+ tipo +' #fechaActividad').val(),
                    correo: $('.'+ tipo +' #correoActividad').val(),
                    ciudad: $('.'+ tipo +' #ciudad').val(),
                    telefono: $('.'+ tipo +' #telActividad').val(),
                    actividad: tipo,
                    dia: day,
                },
                success: function(response) {
                    if(response.validate==false) {
                        day=null;
                        swal('Error', 'Ya esta registrado', 'error');
                    } else if(response.validate=='llena') {
                        day=null;
                        $('.'+ tipo +' #nombreNovia').val('');
                        $('.'+ tipo +' #nombreNovio').val('');
                        $('.'+ tipo +' #correoActividad').val('');
                        $('.'+ tipo +' #ciudad').val('');
                        $('.'+ tipo +' #telActividad').val('');
                        $('.'+ tipo +' #fechaActividad').val('');
                        // $('.'+ tipo +'#modal2').closeModal();
                        swal('Error', 'Lo sentimos, la actividad llegó al máximo de registros', 'error');
                    } else {
                        day=null;
                        $('.'+ tipo +' #nombreNovia').val('');
                        $('.'+ tipo +' #nombreNovio').val('');
                        $('.'+ tipo +' #correoActividad').val('');
                        $('.'+ tipo +' #ciudad').val('');
                        $('.'+ tipo +' #telActividad').val('');
                        $('.'+ tipo +' #fechaActividad').val('');
                        // $('.'+ tipo +'#modal2').closeModal();
                        swal('Felicidades', 'Tu registro fue exitoso.', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        } else {
            day=null;
            swal('Error', 'Por favor complete todos los campos.', 'error');
        }
    }
    function registrarActividad() {
        x = $(this).attr("id");
        if(x=='mixologia') {
            document.getElementById("titleActivity").innerHTML = 'CLASE DE MIXOLOGÍA';
            document.getElementById("fechaActivity").innerHTML = 'SÁBADO 06 Y DOMINGO 7 DE ABRIL  |  04:30 PM  A 05:30 PM';
            day=null;
            var maquillaje = document.getElementById('dia_asistencia');
            maquillaje.style.display = 'block';
        } else if(x=='sushi') {
            document.getElementById("titleActivity").innerHTML = 'CLASE DE SUSHI PARA PAREJAS';
            document.getElementById("fechaActivity").innerHTML = 'SÁBADO 06 DE ABRIL  |  03:30 PM A 04:30 PM';
            day='sabado';
            var maquillaje = document.getElementById('dia_asistencia');
            maquillaje.style.display = 'none';
        } else if(x=='cata_vino') {
            document.getElementById("titleActivity").innerHTML = 'CATA DE VINOS';
            document.getElementById("fechaActivity").innerHTML = 'SÁBADO 06 Y DOMINGO 07 DE ABRIL  |  06:30 PM A 07:30 PM';
            day=null;
            var maquillaje = document.getElementById('dia_asistencia');
            maquillaje.style.display = 'block';
        } else if(x=='asesor_imagen') {
            document.getElementById("titleActivity").innerHTML = 'TALLER DE ASESORÍA DE IMAGEN';
            document.getElementById("fechaActivity").innerHTML = 'DOMINGO 24 DE FEBRERO  |  12:30 PM A 01:30 PM  |  ZONA: Japy Spot';
            day='domingo';
            var oculto = document.getElementById('dia_asistencia');
            oculto.style.display = 'none';
        }
        $('#modal2').openModal();
        // alert('id= '+x);
    }

    function gandor() {
        $('#modal3').openModal();
    }

    function registrar() {
        if($('#nombre').val()!=null && $('#correo').val()!=null && $('#telefono').val()!=null
        && $('#fecha').val()!=null && $('#nombre').val()!="" && $('#correo').val()!="" 
        && $('#telefono').val()!="" && $('#fecha').val()!="") {
            jsShowWindowLoad();
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."brideweekend/register" ?>',
                method: 'post',
                data: {
                    nombre: $('#nombre').val(),
                    correo: $('#correo').val(),
                    come_from: 'japy',
                    genero: '2',
                    telefono: $('#telefono').val(),
                    fecha: $('#fecha').val(),
                    come_from: STATE,
                },
                success: function(response) { 
                    if(response.validate==false) {
                        jsRemoveWindowLoad();
                        swal('Error', 'Correo registrado anteriormente.', 'error');
                    } else {
                        jsRemoveWindowLoad();
                        $('#nombre').val('');
                        $('#correo').val('');
                        $('#telefono').val('');
                        $('#fecha').val('');
                        $('#modal1').openModal();
                        // swal('Felicidades', 'Tu registro fue exitoso.', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        } else {
            swal('Error', 'Por favor complete todos los campos.', 'error');
        }
    }
    function replaceImg() {
        var id = $(this).attr("id");
        $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/'+id+'.png');
    }
    function updateFetch() {
        var id = $(this).attr("id");
        // var fecha = $('#'+id+'-fecha').text();
        var fecha=null;
        if(id=='-cdmx') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/cdmx.png');
            fecha = '2019-03-16 12:00:00';
        } else if(id=='-mon') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/mon.png');
            fecha = '2019-03-17 12:00:00';
        } else if(id=='-gdl') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaculiacan.png');
            fecha = '2019-02-23 12:00:00';
        } else if(id=='-leon') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/leon.png');
            fecha = '2019-03-19 12:00:00';
        } else if(id=='-qro') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/qro.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaqro.png');
            fecha = '2019-09-07 12:00:00';
        } else if(id=='-pue') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/pue.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecapuebla.png');
            fecha = '2019-09-07 12:00:00';
        } else if(id=='-cul') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/cul.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaculiacan.png');
            fecha = '2019-03-23 12:00:00';
        } else if(id=='-slp') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/slp.png');
            fecha = '2019-03-23 12:00:00';
        }
        setCountDownTimer(fecha);
    }
    
    function alerta(){
        alert($('#msj').val());
        if($('#msj').val()=='errorCaptcha'){
            alert('Es necesario rellenar el Captcha');
        }
        
    }

    var timer='';
    function setCountDownTimer(eventDate) {
        clearInterval(timer);
        let countDownDate = new Date(eventDate).getTime();
        const $eventDays = $('#event-days');
        const $eventHours = $('#event-hours');
        const $eventMinutes = $('#event-minutes');
        const $eventSeconds = $('#event-seconds');
        const $eventCounter = $('#event-counter');

        timer = setInterval(function() {
            let now = new Date().getTime();

            let distance = countDownDate - now;

            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            $eventDays.html(days);
            $eventHours.html(hours);
            $eventMinutes.html(minutes);
            $eventSeconds.html(seconds);

            if (distance < 0) {
                $eventCounter.hide();
            }
        }, 1000);
    }
</script>
