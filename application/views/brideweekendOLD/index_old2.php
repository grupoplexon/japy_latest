<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bride Weekend</title>
    <?php $this->view("brideweekend/header"); ?>
</head>
<style>
body{
    background-color: white;
}
p{
    text-align: justify !important;
}
.content{
    min-height: calc(100vh - 64px);
    position: relative;
}
.indicator-item{
    width: 70px !important;
    height: 6px !important;
    box-shadow: inset 0 0 7px #747474;
    border-radius: unset !important;
    /* background-color: #fff !important; */
}
.tabs .tab .active {
    transition: .2s ease-in-out;
}
.nav {
    width:450px;
    margin:0 auto;
    list-style:none;
}
.nav li {
    float:left;
}
.nav a {
    display:block;
    text-align:center;
    width:150px; /* fixed width */
    text-decoration:none; 
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}

.card.medium .card-image {
    max-height: 80%;
    overflow: hidden;
}
hr{ 
  border: 1px solid;
  color: #ee6e73;
  align: center;
  width: 5%
}
a {
    color: black  ;
}
h1, h3,h2,h4 {
    text-align: center;
}
.content{
    min-height: calc(100vh - 64px);
    position: relative;
}
#contact, #info{
    margin-left: 5%;
    margin-right: 5%;
}

#info{
    text-align: justify;
    font-family: sans-serif;
    font size: 1rem;
}

#cont{ 
  border: .5px solid;
  color: #b5b5b5;
  width: 100%;
  margin-top: 5%;
}

.picker__date-display {
    background-color: #8e8e8e;
}
.picker__day--selected, .picker__day--selected:hover, .picker--focused .picker__day--selected {
    background-color: #ee6e73;
}
.picker__day.picker__day--today, .picker__close, .picker__today {
    color: #ee6e73;
}
hr{ 
  border: 1px solid;
  color: #ee6e73;
  align: center;
  width: 5%
}
.btn{
    background-color: #8e8e8e;
}
#form{
    background-color: #ffff;
}
#susc{
    float: right;
}
.btn:hover, .btn-large:hover {
    background-color: #ee6e73;
}
form p{
    text-align: center !important;
    font-size: 1.5rem;
    color: #757575;
}
.centerDate {
    margin-left: 42% !important;
}
.slider .slides li img {
    height: 100%;
    width: 100%;
    background-size: cover;
    background-position: initial;
}
</style>
<body>

<div class="content center-align">
    <div class="row" id="home" style="background: #4d4d4f">
        

    <!-- <div class="carousel">
        <a class="carousel-item" href="#one!"><img src="https://lorempixel.com/250/250/nature/1"></a>
        <a class="carousel-item" href="#two!"><img src="https://lorempixel.com/250/250/nature/2"></a>
        <a class="carousel-item" href="#three!"><img src="https://lorempixel.com/250/250/nature/3"></a>
    </div> -->
    <div class="slider">
    <ul class="slides">
        <li>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal.png"> 
        <!-- <div class="caption center-align">
            <h3>Todo lo que necesitas para una boda perfecta </h3>
            <h4 class="light grey-text text-lighten-3">en un solo fin de Semana.</h4>
        </div> -->
        </li>
        <li>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal2.png"> 
        <div class="caption left-align">
            <h3>Disfruta de pasarelas, conferencias, premios y más de 200 expositores</h3>
            <h4 class="light grey-text text-lighten-3">esperando para tu evento.</h4>
        </div>
        </li>
        <li>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal3.png"> 
        <div class="caption right-align">
            <h3>Tendencias, creatividad y emoción se dan cita en BRIDE WEEKEND<strong style="font-size: 17px">©</strong></h3>
            <!-- <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5> -->
        </div>
        </li>
    </ul>
</div>

    
    </div>
    <div class="row center-align" id="brideweekend">
        <span class="row">
            <strong style="color: black;font-size: 4vh;">BRIDE WEEKEND</strong>
            <hr style="width: 40px;color: #f38083;display: block;height: 1px;border: 0;border-top: 2px solid #f38083;padding: 0; ">
        </span>
        <div style="width: 70%;margin: auto;">
            
            <p>BRIDE WEEKEND© reinventa el concepto tradicional de las expos de boda, en una experiencia única capaz de ofrecer a las parejas en un solo fin de semana y en un único espacio todo lo necesario para crear una boda perfecta y de ir más allá del recinto expositivo con la realización de diferentes actividades en algunos de los lugares más destacados de la ciudad sede en la que se celebre. </p>
            <p>Nos mantenemos a la vanguardia en la industria nupcial, al contar con más de 2 mil modelos de vestidos, donde la novia no tendrá que buscar más, ya que estamos seguros, encontrará el ideal.  </p>
            <p>Tendencias, creatividad y emoción se dan cita en BRIDE WEEKEND©, donde es
                posible encontrar una variada y seleccionada oferta de servicios nupciales, conocer
                novedades, obtener el mejor asesoramiento, disfrutar de las mejores pasarelas y ganar obsequios,
                para hacer realidad la boda soñada.
            </p>
        </div>
        <div id="recientes" class="row" style="width: 81%;">
                <div class="col s12 m6 l4" style="cursor: pointer;" >
                    <div class="row" style="margin: 0px;">
                        <div class="card">
                            <div class="card-image">
                            <img class="materialboxed" style="object-fit: cover;" src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4" style="cursor: pointer;" >
                    <div class="row" style="margin: 0px;">
                        <div class="card">
                            <div class="card-image">
                            <img class="materialboxed" style="object-fit: cover;" src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend2.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l4" style="cursor: pointer;" >
                    <div class="row" style="margin: 0px;">
                        <div class="card">
                            <div class="card-image">
                            <img class="materialboxed" style="object-fit: cover;" src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend3.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    
    </div>

    <div class="row center-align" id="sedes" style="background: #595859;">
        <span class="row">
            <strong style="color: white;font-size: 4vh;">SEDES</strong>
            <hr style="width: 40px;color: #f38083;display: block;height: 1px;border: 0;border-top: 2px solid #f38083;padding: 0; ">
        </span>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <div class="col l12">
            <nav>
                <div class="nav-wrapper grey darken-1">
                    <!-- <a href="#" data-activates="mobile-demo2" class="button-collapse"><i class="material-icons">menu</i></a> -->
                    <ul class="hide-on-med-and-down tabs grey darken-1 nav">
                        <!-- <li class="tab"><a style="color: white !important;" id="-cdmx">CDMX</a></li> -->
                        <!-- <li class="tab"><a style="color: white !important;" id="-mon">MONTERREY</a></li> -->
                        <li class="tab"><a style="color: white !important;" id="-gdl">GDL</a></li>
                        <!-- <li class="tab"><a style="color: white !important;" id="-leon">LEÓN</a></li> -->
                        <li class="tab"><a style="color: white !important;" id="-qro">QUERÉTARO</a></li>
                        <li class="tab"><a style="color: white !important;" id="-pue">PUEBLA</a></li>
                        <li class="tab"><a style="color: white !important;" id="-cul">CULIACÁN</a></li>
                        <!-- <li class="tab"><a style="color: white !important;" id="-slp">S.LP.</a></li> -->
                    </ul>
                </div>
            </nav>
        </div>

        <div style="display: none;" id="head_mov">
            <img class="head-cdmx" style="width: 100%">
            <!-- <span class="row">
                <strong style="color: white;font-size: 4vh; cursor: pointer;" id="cdmx_movil">CDMX</strong>
                <hr style="width: 40px;color: white;display: block;height: 1px;border: 0;border-top: 2px solid white;padding: 0; ">
            </span>
            <span class="row">
                <strong style="color: white;font-size: 4vh; cursor: pointer;" id="mon_movil">MONTERREY</strong>
                <hr style="width: 40px;color: white;display: block;height: 1px;border: 0;border-top: 2px solid white;padding: 0; ">
            </span> -->
            <span class="row">
                <strong style="color: white;font-size: 4vh; cursor: pointer;" id="gdl_movil">GDL</strong>
                <hr style="width: 40px;color: white;display: block;height: 1px;border: 0;border-top: 2px solid white;padding: 0; ">
            </span>
            <!-- <span class="row">
                <strong style="color: white;font-size: 4vh; cursor: pointer;" id="leon_movil">LEÓN</strong>
                <hr style="width: 40px;color: white;display: block;height: 1px;border: 0;border-top: 2px solid white;padding: 0; ">
            </span> -->
            <span class="row">
                <strong style="color: white;font-size: 4vh; cursor: pointer;" id="qro_movil">QUERÉTARO</strong>
                <hr style="width: 40px;color: white;display: block;height: 1px;border: 0;border-top: 2px solid white;padding: 0; ">
            </span>
            <span class="row">
                <strong style="color: white;font-size: 4vh; cursor: pointer;" id="pue_movil">PUEBLA</strong>
                <hr style="width: 40px;color: white;display: block;height: 1px;border: 0;border-top: 2px solid white;padding: 0; ">
            </span>
            <span class="row">
                <strong style="color: white;font-size: 4vh; cursor: pointer;" id="cul_movil">CULIACÁN</strong>
                <hr style="width: 40px;color: white;display: block;height: 1px;border: 0;border-top: 2px solid white;padding: 0; ">
            </span><br>
            <!-- <span class="row">
                <strong style="color: white;font-size: 4vh; cursor: pointer;" id="slp_movil">S.L.P.</strong>
                <hr style="width: 40px;color: white;display: block;height: 1px;border: 0;border-top: 2px solid white;padding: 0; ">
            </span><br> -->
        </div>

        <div id="tab-sedes">
            <div class="col s12 l12">
                <img class="banner-sedes" style="width: 100%">
            </div>

            <div class="col l12">
                <div class="row" id="event-counter">
                    <div class="faltante col s12" style="padding: 0">
                        <div class="col l1"></div>
                        <div class="col s12 l2 center-align counter-container"  style="border-right: white solid 1px; border-left: white solid 1px;">
                            <img class="plecas">
                        </div>
                        <div id="div-dias" class="col s12 l2 ofset-m4 center-align counter-container" style="border-right: white solid 1px; border-left: white solid 1px;">
                            <br><br><br>
                            <h5 class="gris-1-text no-margin counter-data">
                                <b class="dias" id="event-days" style="color: white; font-size: 80px;">
                                    
                                </b>
                            </h5>
                            <p class="counter-title " style="color: white; margin-left: 13vh;">D&iacute;as</p>
                        </div>
                        <div class="col s12 l2 center-align counter-container" style="border-right: white solid 1px; border-left: white solid 1px;">
                            <br><br><br>
                            <h5 class="gris-1-text no-margin counter-data">
                                <b class="horas" id="event-hours" style="color: white; font-size: 80px;">
                                    
                                </b>
                            </h5>
                            <p class="counter-title " style="color: white; margin-left: 13vh;">Horas</p>
                        </div>
                        <div class="col s12 l2 center-align counter-container" style="border-right: white solid 1px; border-left: white solid 1px;">
                            <br><br><br>
                            <h5 class="gris-1-text no-margin counter-data">
                                <b class="minutos" id="event-minutes" style="color: white; font-size: 80px;">
                                    
                                </b>
                            </h5>
                            <p class="counter-title" style="color: white; margin-left: 13vh;">Min</p>
                        </div>
                        <div class="col s12 l2 center-align counter-container" style="border-right: white solid 1px; border-left: white solid 1px;">
                            <br><br><br>
                            <h5 class="gris-1-text no-margin counter-data">
                                <b class="segundos" id="event-seconds" style="color: white; font-size: 80px;">
                                    
                                </b>
                            </h5>
                            <p class="counter-title" style="color: white; margin-left: 13vh;">Seg</p>
                        </div>
                        <div class="col l1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="cols12" id="menu-movil" style="display: none">
        
    </div> -->

    <div class="row" style="background: white;" id="expositor">
        <div class="container row">
            <div class="col s12 l12 alinear">
                <br>
                <!-- <span class="row"> -->
                    <div class="col s12 l1 alinear">
                        <strong style="color: black;font-size: 4vh;">EXPOSITOR</strong>
                    </div>
                    <div class="col s12 l12 alinear">
                        <!-- <div class="alinear"> -->
                            <hr id="line" style="position: absolute; width: 40px;color: #f38083;display: block;height: 1px;border: 0;border-top: 2px solid #f38083;padding: 0; ">
                        <!-- </div> -->
                    </div>
                <!-- </span> -->
            </div>
            <div class="col s12 l6 alinear">
                <p style="text-align: justify;">
                BRIDE WEEKEND© es la cita ineludible para los profesionales de la industria nupcial y el evento ideal para todas aquellas parejas que están planeando su boda. 
                <br><br>  Pueden vivir la experiencia de encontrar lo necesario para el día más importante de sus vidas. 
                <br><br>
                Este es un nuevo concepto de exposición en la que las parejas, podrán disfrutar de pasarelas, conferencias y otras actividades en las que obsequiarán importantes premios como vestidos, productos, y servicios para su boda, lunas de miel y hasta un auto. 
                </p>
                <p>Somos líderes en el mercado de exposiciones, debido a nuestra experiencia con diversos conceptos de éxito,  esperamos a más de 5,000 asistentes en esta edición y contar con la participación de 200 expositores de diferentes giros.</p>
                <!-- <div class="col l6">
                <br>
                    <button class="btn grey">DIRECTORIO DE</button>
                </div> -->
                <div class="col l12">
                    <br>
                    <a class="btn grey" target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSdeLSZri2OcymXepFgGwobfceOTS51aR9znRkXk1aPjflbupg/viewform?vc=0&c=0&w=1&fbzx=-615113661037242951">CONTRATA AQUÍ</a>
                    <br><br>
                </div>
            </div>
            <div class="col s12 l1"></div>
            <div class="col s12 l5 alinear" style="margin-top: 20px;">
            <!-- <p>You can customize the behavior of each modal using these options. For example, you can call a custom function to run when a modal is dismissed. To do this, just place your function in the intialization code as shown below.</p> -->
                <img src="<?php echo base_url() ?>dist/img/brideweekend/expositor.png" style="width: 100%;">
            </div>
        </div>
    </div>

    <div class="row" id="comprador" style="background: #cdcdcd">
        <div class="col l6" id="comp-esc">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/comprador.png" style="width: 100%">
        </div>
        <div class="col l6">
            <div class="col s12 l8 alinear" style="margin-top: 80px; margin-left: 70px" id="titulo">
                <span class="row">
                    <div class="col s12 l1 alinear">
                        <strong style="color: black;font-size: 4vh;">NOVIA</strong>
                    </div>
                    <div class="col s12 l12 alinear">
                    <hr id="line2" style="position: absolute; width: 40px;color: #f38083;display: block;height: 1px;border: 0;border-top: 2px solid #f38083;padding: 0; ">
                    </div>
                </span>
            </div>
            <!-- <div class="col s12 l12 alinear">
                <br>
                    <div class="col s12 l1 alinear">
                        <strong style="color: black;font-size: 4vh;">COMPRADOR</strong>
                    </div>
                    <div class="col s12 l12 alinear">
                            <hr id="line2" style="position: absolute; width: 40px;color: #f38083;display: block;height: 1px;border: 0;border-top: 2px solid #f38083;padding: 0; ">
                    </div>
            </div> -->
            <div class="col l12 alinear">
            <div style="text-align: left !important;">
                <h5 id="subtitulo" style="margin-left: 70px; margin-top: 30px;">BrideWeekend© es más que una Expo</h5>
            </div>
            <p id="texto" style="text-align: justify; margin-left: 70px; margin-right: 70px;">
            Es especialista en reducir el estrés y calmar a los novios en esos momentos de nervios durante la organización de su boda, logrando que las vivencias durante todo este proceso resulten divertidas, excitantes, con ilusión y amor, gracias también a su visión de líder y de saber llevar la batuta frente a todo el equipo de proveedores que forman parte de la orquesta, para que toda la pieza de música salga perfecta.
                <br><br>
                Estamos enamorados del mundo de las bodas para dar lo mejor de uno para que todo salga perfecto, para hacer realidad el sueño de cada pareja según sus gustos e ideas, y Bride Weekend expone en esta gran plataforma y fin de semana todo lo necesario y más para que en el gran día todo marche de la mejor manera posible.
                <br><br>
                <!-- <button class="btn grey darken-1">POLÍTICAS DE ACCESO</button> -->
            </p>
            </div>
        </div>
        <div class="col l6" style="display: none;" id="comp-movil">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/comprador.png" style="width: 100%">
        </div>
        <div class="col s12 l12 center-align" style="background: #E9EBEA;">
            <br>
            <span class="row">
                <strong style="color: black;font-size: 4vh;">MIEMBROS DE ASOCIACIONES</strong>
                <hr style="width: 40px;color: #f38083;display: block;height: 1px;border: 0;border-top: 2px solid #f38083;padding: 0; ">
            </span>
            <br>
            <img src="<?php echo base_url() ?>dist/img/brideweekend/comprador-footer.png" style="width: 100%">
        </div>
    </div>

    <!-- GALERIA -->
    <!-- <div class="row" style="background: white;" id="galeria">

        <div class="row center-align">
            <h4> GALERÍA </h4>
            <hr>
            <br>
            <span> I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively. </span>
        </div>
        <div class="row" id="galeria">
            <div class="container"> 
            <div class="col s12 m4">
                <div class="card medium">
                    <div class="card-image">
                    <img class="materialboxed" width="250" data-caption="A picture of a way with a group of trees in a park"   object-fit="cover" src="<?php echo base_url() ?>dist/img/brideweekend/1.png">  
                    </div>
                    <div class="card-content">
                    <span class="card-title"><a href="#">Card Title</a></span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="card medium">
                    <div class="card-image"  >
                    <img class="materialboxed"  data-caption="A picture of a way with a group of trees in a park"   object-fit="cover" src="<?php echo base_url() ?>dist/img/brideweekend/3.png">  
                    </div>
                    <div class="card-content">
                    <span class="card-title small"><a href="#">Card Title</a></span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="card medium">
                    <div class="card-image">
                    <img class="materialboxed" data-caption="A picture of a way with a group of trees in a park"   object-fit="cover" src="<?php echo base_url() ?>dist/img/brideweekend/2.png">  
                    </div>
                    <div class="card-content">
                    <span class="card-title"><a href="#">Card Title</a></span>
                    </div>
                </div>
            </div>
            </div>
        </div>

    </div> -->

    <!-- BLOG -->

    <!-- <div class="row" style="background: white;" id="blog">
      <div class="row " >
          <h4> BLOG BRIDE WEEKEND </h4>
          <hr>
      </div>
      <div class="row" id="blog">
        <div class="container">
          <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                  <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal.png">
                
              </div>
              <div class="card-content">
                <span class="card-title"><a href="#">Card Title</a></span>
                <p>I am a very simple card. I am good at containing small bits of information.
                I am convenient because I require little markup to use effectively.</p>
              </div>
              <div class="card-action">
                
                <i class="material-icons" style="color: #ee6e73;font-size: large;">visibility 0</i>
                <i class="material-icons" style="color: #ee6e73;font-size: large;">chat 0</i> 
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                  <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal.png">
                
              </div>
              <div class="card-content">
                <span class="card-title"><a href="#">Card Title</a></span>
                <p>I am a very simple card. I am good at containing small bits of information.
                I am convenient because I require little markup to use effectively.</p>
              </div>
              <div class="card-action">
                
              <i class="material-icons" style="color: #ee6e73;font-size: large;">visibility 0</i>
                <i class="material-icons" style="color: #ee6e73;font-size: large;">chat 0</i> 
                                      
              </div>
            </div>
          </div>
          <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                  <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal.png">
                
              </div>
              <div class="card-content">
                <span class="card-title"><a href="#">Card Title</a></span>
                <p>I am a very simple card. I am good at containing small bits of information.
                I am convenient because I require little markup to use effectively.</p>
              </div>
              <div class="card-action">
              <i class="material-icons" style="color: #ee6e73;font-size: large;">visibility 0</i>
                <i class="material-icons" style="color: #ee6e73;font-size: large;">chat 0</i> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <!-- CONTACTO -->

    <div class="row" id="contacto">
        <!-- <div class="row" id="map"  style="background: #AAAAAA">
            <h4> OPEN MAP </h4>
            <hr>
        </div> -->
        <div class="row "id="form">
            <form method="POST" class="col  s12  m12 l12 "
                action="<?php echo base_url() ?>index.php/Brideweekend/registro">
                <div class="row " id="contact">
                    <h4> REGISTRO </h4>
                    <hr>
                    <p>¡Registrate aquí para participar!</p>
                    <h6 style="color: #757575">Regístrate en BrideWeekend y Japy</h6>
                    <div class="input-field col s12 m6 l3">
                    <i class="material-icons prefix">email</i>
                    <input id="icon_prefix" id="correo" name="correo" type="email" class="validate" required>
                    <label for="icon_telephone" >Correo</label>
                    </div>
                    <div class="input-field col s12 m6 l3">
                    <i class="material-icons prefix">account_circle</i>
                    <input id="icon_prefix" type="text" name="nombre" id="nombre" class="form-control" required>
                    <label for="icon_prefix">Nombre</label>
                    </div>
                    <div class="input-field col s12 m6 l3">
                    <i class="material-icons prefix">call</i>
                    <input id="icon_prefix" type="number" name="telefono" id="telefono" class="form-control" required>
                    <label for="icon_prefix">Telefono</label>
                    </div>
                    <div class="input-field col s12 m6 l3 ">
                    <i class="material-icons prefix">date_range</i>
                    <input type="text" class="datepicker" name="fecha" id="fecha" required>
                    <label for="icon_telephone">Fecha de Boda</label>
                    </div>
                    <div class="col s12 center-align ">
                        <button type="submit" class="btn btn-block-on-small dorado-2 pull-right">
                           Enviar
                        </button>
                    </div>
                    
                    
                </div>
            </form>
        </div>
    
        <div class="row" id="info">
            <div class="col s12 l12">
                <h4 style="text-align: left;">Bride Weekend</h4>
            </div>
            <div class="col s12 m4 l4">
                <label> 
                <p>Santa Rosa de Lima 4428 Fracc. Camino Real Zapopan, Jal.
                <br>
                info@grupoplexon.com
                <br>
                (0133)31229622/9606
                <br>
                ventas@grupoplexon.com
                </p>

                <span><a class="btn-floating btn-large waves-effect waves-light blue" href="https://www.facebook.com/brideweekendmx" target="_blank">FB</a></span>

                    <!-- CONTÁCTANOS PARA RESOLVER TUS DUDAS.
                    CDMX, Monterrey, León, Veracruz y San Luis Potosí
                    Envíanos un correo electrónico: @Proveedor o @Novia
                    o comunícate con nosotros: Monterrey (81) 1933.3222 CDMX (55)  -->
                </label>
                <br>
                <hr id="cont">
                <!-- <label> Follow Us: </label>
                <form action="#">
                    <div class="file-field input-field" style="padding-left: 0%;">
                        <a class="waves-effect waves-light btn" id="susc">Suscribirse</a>
                    <div class="file-path-wrapper" style="padding-left: 0%;">
                        <input class="file-path validate" type="text" placeholder="Correo">
                    </div>
                    </div>
                </form> -->
            </div>
            <div class="col s12 m4 l4">
                <!-- <label>
                    Guadalajara, Puebla, Querétaro y Culiacán
                    Envíanos un correo electrónico:@Proveedor o @Novia 
                    o comunícate con nosotros: Guadalajara, Puebla, Querétaro 01 (33) 31229622 /06
                </label> -->
            </div>
            <!-- <div class="col s12 m4 l4">
                <h5>INSTAGRAM</h5>
            </div> -->
        </div>
    </div>
</div>


</body>
<?php $this->view("brideweekend/footer"); ?>
</html>
<script>
    $(document).ready(function(){
        $('.datepicker').pickadate({disable_picker: true, format: 'yyyy/mm/dd',
                format_submit: 'yyyy-mm-dd',
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthsShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                weekdaysShort: ['Lun','Mar','Mier','Jue', 'Vie','Sab', 'Dom'],
                weekdaysAbbrev: ['L','M','Mi','J','V','S','D']
            });
        $('.slider').slider({full_width: true, interval: 7777});
        const eventDate = "2019-02-23 12:00:00";
        setCountDownTimer(eventDate);
        // $('#-cdmx').on('click', updateFetch);
        // $('#-mon').on('click', updateFetch);
        $('#-gdl').on('click', updateFetch);
        // $('#-leon').on('click', updateFetch);
        $('#-qro').on('click', updateFetch);
        $('#-pue').on('click', updateFetch);
        $('#-cul').on('click', updateFetch);
        // $('#-slp').on('click', updateFetch);

        // $('#mon_movil').on('click', replaceImg);
        // $('#cdmx_movil').on('click', replaceImg);
        $('#gdl_movil').on('click', replaceImg);
        // $('#leon_movil').on('click', replaceImg);
        $('#qro_movil').on('click', replaceImg);
        $('#pue_movil').on('click', replaceImg);
        $('#cul_movil').on('click', replaceImg);
        // $('#slp_movil').on('click', replaceImg);

        var medida= $( window ).width();
        if (medida< 326) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');

            $("#titulo").css('margin-left','0px');
            $("#titulo").css('margin-top','30px');
            $("#subtitulo").css('margin-left','0px');
            $("#subtitulo").css('margin-top','0px');
            $("#texto").css('margin-left','0px');
            $("#texto").css('margin-right','0px');

            $("#comp-movil").css('display','block');
            $("#comp-esc").css('display','none');
            $("#line").css('position','static');
            $("#line2").css('position','static');
        }
        else if (medida < 426) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');

            $("#titulo").css('margin-left','0px');
            $("#titulo").css('margin-top','30px');
            $("#subtitulo").css('margin-left','0px');
            $("#subtitulo").css('margin-top','0px');
            $("#texto").css('margin-left','0px');
            $("#texto").css('margin-right','0px');

            $("#comp-movil").css('display','block');
            $("#comp-esc").css('display','none');
            $("#line").css('position','static');
            $("#line2").css('position','static');
        }
        else if (medida < 769) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');

            $("#titulo").css('margin-left','0px');
            $("#titulo").css('margin-top','30px');
            $("#subtitulo").css('margin-left','0px');
            $("#subtitulo").css('margin-top','0px');
            $("#texto").css('margin-left','0px');
            $("#texto").css('margin-right','0px');

            $("#comp-movil").css('display','block');
            $("#comp-esc").css('display','none');
            $("#line").css('position','static');
            $("#line2").css('position','static');
        }
        else if (medida < 2100) {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecagdl.png');
        }
        window.addEventListener("hashchange", function () {
            window.scrollTo(window.scrollX, window.scrollY - 65);
        });
    });

    $(window).resize(function() {
        var medida= $( window ).width();
        if (medida< 326) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');
        }
        else if (medida < 426) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');
        }
        else if (medida < 769) {
            $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl_movil.png');
            $("#tab-sedes").css('display','none');
            // $("#menu-movil").css('display','block');
            $("#head_mov").css('display','block');
        }
        else if (medida < 2100) {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl.png');
            $("#tab-sedes").css('display','block');
            // $("#menu-movil").css('display','none');
            $("#head_mov").css('display','none');
        }
    });

    function replaceImg() {
        var id = $(this).attr("id");
        $('img.head-cdmx').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/'+id+'.png');
    }

    function updateFetch() {
        var id = $(this).attr("id");
        // var fecha = $('#'+id+'-fecha').text();
        var fecha=null;
        if(id=='-cdmx') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/cdmx.png');
            fecha = '2019-03-16 12:00:00';
        } else if(id=='-mon') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/mon.png');
            fecha = '2019-03-17 12:00:00';
        } else if(id=='-gdl') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/gdl.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecagdl.png');
            fecha = '2019-02-23 12:00:00';
        } else if(id=='-leon') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/leon.png');
            fecha = '2019-03-19 12:00:00';
        } else if(id=='-qro') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/qro.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaqro.png');
            fecha = '2019-09-07 12:00:00';
        } else if(id=='-pue') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/pue.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecapuebla.png');
            fecha = '2019-09-07 12:00:00';
        } else if(id=='-cul') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/cul.png');
            $('img.plecas').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/plecaculiacan.png');
            fecha = '2019-03-23 12:00:00';
        } else if(id=='-slp') {
            $('img.banner-sedes').attr('src', '<?php echo base_url() ?>'+'dist/img/brideweekend/slp.png');
            fecha = '2019-03-23 12:00:00';
        }
        setCountDownTimer(fecha);
    }

    var timer='';
    function setCountDownTimer(eventDate) {
        clearInterval(timer);
        let countDownDate = new Date(eventDate).getTime();
        const $eventDays = $('#event-days');
        const $eventHours = $('#event-hours');
        const $eventMinutes = $('#event-minutes');
        const $eventSeconds = $('#event-seconds');
        const $eventCounter = $('#event-counter');

        timer = setInterval(function() {
            let now = new Date().getTime();

            let distance = countDownDate - now;

            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            $eventDays.html(days);
            $eventHours.html(hours);
            $eventMinutes.html(minutes);
            $eventSeconds.html(seconds);

            if (distance < 0) {
                $eventCounter.hide();
            }
        }, 1000);
    }
</script>