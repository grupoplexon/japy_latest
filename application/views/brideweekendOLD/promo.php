<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
    </script>
    <meta property="og:title" content="Promocion <?php echo $promotion->name_provider ?>" />
    <meta property="og:image" content="https://brideadvisor.mx/uploads/promociones/images/<?php echo $promotion->image ?>">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:url" content="https://www.brideadvisor.mx/brideweekend/promociones?id_promotion=<?php echo $promotion->id_promotion ?>" />
    <meta property="og:site_name" content="Promocion <?php echo $promotion->name_provider ?>" />
    <meta property="og:description" content="<?php echo $promotion->description ?>" />
    <meta property="og:type" content="website" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url() ?>dist/img/brideweekend/logo.png" rel="image_src">
    <title>Promoción <?php echo $promotion->name_provider ?></title>
    <?php $this->view("brideweekend/header"); ?>
    
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="<?php echo base_url() ?>dist/css/brideweekend.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo base_url() ?>dist/slick/slick.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>dist/slick/slick-theme.css" rel="stylesheet" type="text/css"/>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>
</head>
<style>
    .name{
        padding: 1rem 0 0 2rem;
    }
    .card{
        margin: 0 25% 0 25% !important;
    }
    .info, .boton{
        margin: 0 10% 0 10% !important;
    }
    .info{
        border-left: 10px solid;
    }
    body{
        color: #555555;
    }
    hr{
        color: #555555;
        width:40%;
        border: 1px solid;
    }
    .btn{
        border-radius: 4px;
    }
</style>
<body>
    <div class="row center-align ">
        <div class="row name">
            <h5 class="text-align: left !important;"> > PROMOCIONES ACTUALES EN QUERÉTARO</h5>
        </div>
        <div class="row ">
            <div class="card" style="height: 100%;">
                <div class="row" style="height: 400px;">
                    <img id="imgPrincipal" style="width: 100%; height:100%; object-fit: scale-down;"  src="<?php echo base_url()?>uploads/promociones/images/<?= $promotion->image ?>" />
                </div>
                <div class="row info" style="height: 350px;">
                    <input type="text" id="id" class="hide" value="<?php echo $promotion->id_promotion ?>">
                    <h2> <?php echo $promotion->name_provider ?></h2>
                    <hr> <br>
                    <h5> <?php echo $promotion->description ?></h5> <br>
                </div>
                <div class="row boton" style="height: 180px;">
                    <button class="btn btn-block-on-small dorado-2 " id="save_cupon"> Guardar cupón </button>
                    <h4> ó</h4>
                    <button class="btn btn-block-on-small dorado-2 " id="mail"> Enviar por email </button>
                    <div class="correo col l12 hide" id="div-correo">
                        <div class="file-path-wrapper ">
                            <h5>Ingresa tu correo electronico</h5>
                            <input type="text" name="correoC" id="correoC" placeholder="Correo electronico" class="form-control" required>
                            <button class="btn btn-block-on-small dorado-2 " id="send_cupon"> Enviar </button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</body>
<?php $this->view("brideweekend/footer"); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function(){
        $("#save_cupon").click(function () {
            let valoresCheck = [];
            // $("input[type=checkbox]:checked").each(function(){
            //     valoresCheck.push(this.value);
            // });
            valoresCheck = JSON.stringify($("#id").val());

            window.open('<?php echo base_url() ?>admin/Promotion/createPDF?id_promotion='+valoresCheck,'_blank');
        });

        $('#send_cupon').on('click', function() {

            let valoresCheck = [];
            var mail;
            valoresCheck = JSON.stringify($("#id").val());

            jsShowWindowLoad("Enviando cupones, espere un momento...");
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."admin/Promotion/sendPDF" ?>',
                method: 'post',
                data: {
                    id_promotion: valoresCheck,
                    mail: $('#correoC').val(),
                },
                success: function(response) { 
                    if(response.validate==false) {
                        jsRemoveWindowLoad();
                        swal('Error', 'Ocurrio un problema, intente de nuevo', 'error');
                    } else {
                        jsRemoveWindowLoad();
                        $('#correoC').val('');
                        swal('Felicidades', 'Tu cupon fue enviado correctamente', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });

            // window.open('<?php echo base_url() ?>admin/Promotion/sendPDF?id_promotion='+valoresCheck+'&mail='+mail);

        });

        $('#mail').on('click', function() {
            $('#div-correo').removeClass('hide');
            $('#mail').addClass('hide');
            $(".boton").css("height", "300px");

        });

        QUE HAGOOOZ

    function jsRemoveWindowLoad() {
    // eliminamos el div que bloquea pantalla
        $("#WindowLoad").remove();
    
    }

    function jsShowWindowLoad(mensaje) {
        //eliminamos si existe un div ya bloqueando
        jsRemoveWindowLoad();
    
        //si no enviamos mensaje se pondra este por defecto
        if (mensaje === undefined) mensaje = "Creando perfil, por favor espere!!";
    
        //centrar imagen gif
        height = 20;//El div del titulo, para que se vea mas arriba (H)
        var ancho = 0;
        var alto = 0;

        //obtenemos el ancho y alto de la ventana de nuestro navegador, compatible con todos los navegadores
        if (window.innerWidth == undefined) ancho = window.screen.width;
        else ancho = window.innerWidth;
        if (window.innerHeight == undefined) alto = window.screen.height;
        else alto = window.innerHeight;
    
        //operación necesaria para centrar el div que muestra el mensaje
        var heightdivsito = alto/2 - parseInt(height)/2;//Se utiliza en el margen superior, para centrar
    
    //imagen que aparece mientras nuestro div es mostrado y da apariencia de cargando
        imgCentro = "<div style='text-align:center;height:" + alto + "px;'><div  style='color:black;margin-top:" + heightdivsito + "px; font-size:20px;font-weight:bold'>" + mensaje + "</div><img style='width: 70px; height:70px;' src='<?php echo base_url() ?>dist/img/brideweekend/loader.gif'></div>";
    
            //creamos el div que bloquea grande------------------------------------------
            div = document.createElement("div");
            div.id = "WindowLoad"
            div.style.width = ancho + "px";
            div.style.height = alto + "px";
            $("body").append(div);
    
            //creamos un input text para que el foco se plasme en este y el usuario no pueda escribir en nada de atras
            input = document.createElement("input");
            input.id = "focusInput";
            input.type = "text"
    
            //asignamos el div que bloquea
            $("#WindowLoad").append(input);
    
            //asignamos el foco y ocultamos el input text
            $("#focusInput").focus();
            $("#focusInput").hide();
    
            //centramos el div del texto
            $("#WindowLoad").html(imgCentro);
    
    }

    });
</script>
</html>