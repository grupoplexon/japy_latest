<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
    </script>
    <meta property="og:title" content="Bride Weekend Puebla" />
    <meta property="og:image" content="https://brideadvisor.mx/dist/img/brideweekend/bw_new.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:url" content="https://www.brideadvisor.mx/brideweekend" />
    <meta property="og:site_name" content="Bride Weekend Puebla" />
    <meta property="og:description" content="Todo lo que necesitas para tu boda este 24 y 25 de Agosto en Puebla, Centro Expositor los Fuertes. Pasarelas con las últimas tendencias y más de 1000 vestidos de novia en exhibición." />
    <meta property="og:type" content="website" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url() ?>dist/img/brideweekend/logo.png" rel="image_src">
    <title>Bride Weekend</title>
    <?php $this->view("brideweekend/header"); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link href="https://fonts.googleapis.com/css?family=Muli:400,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="<?php echo base_url() ?>dist/css/brideweekend.css" rel="stylesheet" type="text/css"/>
</head>
<style>

</style>
<body>
<div class="content center-align">
    <div class="row" id="home" style="background: #4d4d4f">
        <div class="slider slide">
            <ul class="slides slide2">
                <li>
                    <a href="<?= base_url() ?>brideweekend/exposiciones"><img src="<?php echo base_url() ?>dist/img/brideweekend/slider_pue.jpg"></a>
                </li>
                <li>
                    <img src="<?php echo base_url() ?>dist/img/brideweekend/slider_exp.jpg" style="cursor: pointer;">
                </li>
            </ul>
        </div>
    </div>
    <div class="row" id="contacto">
        <div class="row "id="form">
                <div class="row " id="">
                    <h4 style="font-weight: 900;"> REGÍSTRATE Y GANA</h4> <hr>
                    <div class="col s12 m6 l5 offset-l1 form-contact">
                        <div class="file-field input-field " >
                            <div class="btn">
                                <span>Nombre</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" name="nombre" id="nombre" class="form-control" required>
                            </div>
                        </div>
                        <div class="file-field input-field " >
                            <div class="btn">
                                <span>Email</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input id="correo" name="correo" type="text" class="validate" required>
                            </div>
                        </div>
                        <div class="file-field input-field  " >
                            <div class="btn">
                                <span>Telefono</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input name="telefono" id="telefono" class="form-control" required type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                            </div>
                        </div>
                        <div class="file-field input-field ">
                            <div class="btn alinear">
                                <span>Fecha de Boda</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="date" class="" name="fecha" id="fecha" required>
                            </div>
                        </div>
                        <br>
                        <button class="btn btn-block-on-small dorado-2 register">
                           Enviar
                        </button>
                        <br><br>
                    </div>
                    <div class="col l6 s12 grey lighten-4">
                    <div class="col s12 m6 l6 grey lighten-4">
                        <h5 style="font-size:22px;">Participa para ganar increíbles premios para este día tan especial:</h5>
                        <br>  
                        <strong style="font-size: 20px;">Bride&nbsp;</strong><strong style="font-style: italic; font-size: 20px;">Weekend&nbsp;</strong><span style="font-size: 18px;">tiene todo lo que necesitas para hacer la boda perfecta en un solo lugar.</span>
                        <br><br><strong style="font-size: 20px;">Ven, vive la experiencia y gana.</strong>
                        <br><br><strong style="font-size: 20px;">Querétaro</strong>
                        <br><strong style="font-size: 18px;">Centro de Congresos</strong>
                        <br><br><strong style="font-size: 18px;">Fecha:</strong><br><strong style="font-size: 16px; font-weight: normal;">Sábado 07 y Domingo 08 de Septiembre de 2019</strong>
                        <br><strong style="font-size: 18px;">Horario:</strong><br><span style="font-size:16px;">Sábado </span></span><strong style="font-size: 16px; font-weight: normal;">04:00pm a 08:00pm</strong>
                        <br><span style="font-size:16px;">Domingo </span></span><strong style="font-size: 16px; font-weight: normal;">11:00am a 08:00pm</strong>
                        <br><strong style="font-size: 18px;">Horario de pasarela:</strong><br><span style="font-size:16px;">Sábado </span></span><strong style="font-size: 16px; font-weight: normal;">06:00pm</strong>
                        <br><span style="font-size:16px;">Domingo </span></span><strong style="font-size: 16px; font-weight: normal;">02:30pm y 05:30pm</strong>
                        <br><br>
                    </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="row center-align " style="background-color: #f4f4f4;">
        <div class="col l10 s12 m12 offset-l1">
            <div class="col l12 s12 m12 center-align">
                <h4 style="font-size: 2rem; font-weight: 900;">REGISTRO ACTIVIDADES</h4><br>
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/CATA-VINO.png" style="width:100%; cursor:pointer;" id="cata">
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/RAMOS.png" style="width:100%; cursor:pointer;" id="ramos">
            </div>
            <div class="col l4 s12 m6 act">
                <img src="<?= base_url() ?>dist/img/brideweekend/MAQUILLAJE-PEINADO.png" style="width:100%; cursor:pointer;" id="maquillaje">
            </div>
        </div>
        <div class="col l12 s12 m12 oculto alinear  cata" style="background-color: #f4f4f4;" id="">
            <h4 style="font-weight: 900;">CATA DE VINOS</h4><br>
            <h5 style="font-weight: 900;" class="horario">HORARIOS:</h5>
            <h5 style="font-size: 1.3rem;">SÁBADO 7 DE SEPTIEMBRE 7:00 PM <b class="neg"> | </b> DOMINGO 8 DE SEPTIEMBRE 6:30 PM </h5>
            <h5 style="font-size: 1.3rem;">STAND BRIDE ADVISOR</h5>
            <div class="col l12 s12 center-align" id="dia_asistencia">
                <!-- <h5>Día en que asistirás:</h5> -->
                <br>
                <form name="dia">
                <strong>
                    <label>
                        <input name="group1" type="radio" value="sabado" checked />
                        <span style="color: black; font-size: 17px;">Sábado</span>
                    </label>
                </strong>&nbsp;&nbsp;
                <strong>
                    <label>
                        <input name="group1" type="radio" value="domingo" />
                        <span style="color: black; font-size: 17px;">Domingo</span>
                    </label>
                </strong>
                </form>
                <br>
            </div>
            <div class="col l10 offset-l1 s12">
                <div class="col l6 s12">
                    <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA"><br>
                    <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                    <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                </div>
                <div class="col l6 s12">
                    <input type="text" id="nombreNovio" placeholder="NOMBRE DEL NOVIO"><br>
                    <input type="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                    <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                </div> 
                <div class="col l12 s12">
                    <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p><br>
                    <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                </div>
            </div>
            <div class="col l12 s12 center-align">
                <br>
                <button class="btn grey darken-1 registerPreview " id="registerCata" style=" height: 50px; border-radius: 15px;
                font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
            </div>
        </div>
        <div class="col l12 s12 m12 oculto alinear maquillaje">
            <h4 style="font-weight: 900;">MASTER CLASS DE MAQUILLAJE</h4><br>
            <h5 style="font-weight: 900;" class="horario">HORARIOS:</h5>
            <h5 style="font-size: 1.3rem;">SÁBADO 7 DE SEPTIEMBRE <b class="neg"> | </b>  7:00 PM <b class="neg"> | </b> STAND BRIDE ADVISOR</h5>
            <br>
            <div class="col l10 offset-l1 s12">
                <div class="col l6 offset-l3 s12">
                    <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA">
                </div>
                <div class="col l6 s12">
                    <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                    <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                </div>
                <div class="col l6 s12">
                    <input type="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                    <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                </div>
                <div class="col l12 s12">
                    <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p><br>
                    <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                </div>
                <div class="col l12 s12 center-align">
                    <br><button class="btn grey darken-1 registerPreview " id="registerMaquillaje" style=" height: 50px; border-radius: 15px;
                    font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
                </div>
            </div>                
        </div>
        <div class="col l12 s12 m12 oculto alinear ramos">
            <h4 style="font-weight: 900;">TALLER DE RAMOS DE NOVIA</h4><br>
            <h5 style="font-weight: 900;" class="horario">HORARIOS:</h5>
            <h5 style="font-size: 1.3rem;">SÁBADO 7 DE SEPTIEMBRE <b class="neg"> | </b>  1:00 PM <b class="neg"> | </b> STAND BRIDE ADVISOR</h5>
            <br>
            <div class="col l10 offset-l1 s12">
                    <div class="col l6 offset-l3 s12">
                        <input type="text" id="nombreNovia" placeholder="NOMBRE DE LA NOVIA">
                    </div>
                    <div class="col l6 s12">
                        <input type="date" id="fechaActividad" placeholder="FECHA DE LA BODA">
                        <input type="text" id="ciudad" placeholder="CIUDAD DE LA BODA">
                    </div>
                    <div class="col l6 s12">
                        <input tyspe="email" id="correoActividad" placeholder="CORREO ELECTRÓNICO">
                        <input id="telActividad" placeholder="TELÉFONO DE CONTACTO" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                    </div>
                    <div class="col l12 s12">
                        <p style="">*TODOS LOS CAMPOS SON REQUERIDOS</p><br>
                        <h5> 3 horas antes del evento recibiras un mail para confirmar tu participación </h5>
                    </div>
                    
                </div>
                <div class="col l12 s12 center-align">
                    <br><button class="btn grey darken-1 registerPreview " id="registerRamos" style=" height: 50px; border-radius: 15px;
                    font-weight: bold; font-size: 1rem;background-color: #545151 !important;">REGISTRAR ACTIVIDADES</button> <br><br>
                </div>
                
        </div>
    </div>
    <div class="row">
        <div class="container">
            <h4>Ganadores BrideWeekend Puebla</h4>
            <br>
            <h5>Ganadora del Crucero 7 Noches (Panamá, Colombia, Curazao, Aruba y Bonaire): <br><strong>Norma Alicia Gonzalez Torres</strong></h5>
            <br>
            <h5>Ganadores de premios generales:</h5>
            <table class="centered striped">
                <thead>
                <tr>
                    <th>Proveedor</th>
                    <th>Ganador</th>
                    <th>Premio</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>D'OPORTOS BRIDE</td>
                    <td>Cyndell Jacqueline Pérez Ríos</td>
                    <td>VESTIDO DE NOVIA POR PRE-COMPRA</td>
                </tr>
                <tr>
                    <td>STUDIO HAIR</td>
                    <td>Diana Isabel </td>
                    <td>MAQUILLAJE Y PEINADO</td>
                </tr>
                <tr>
                    <td>HISTORIAS EN PAPEL</td>
                    <td>Irani Avalos</td>
                    <td>100 INVITACIONES</td>
                </tr>
                <tr>
                    <td>D'OPORTO NOVIAS</td>
                    <td>Gricelia Salas Paredes</td>
                    <td>UN VELO DE NOVIA</td>
                </tr>
                <tr>
                    <td>PHOTOSMILE</td>
                    <td>Raquel guzman</td>
                    <td>PAQUETE MINI/1 HORA DE SERVICIO</td>
                </tr>
                <tr>
                    <td>POLAR</td>
                    <td>Karen Jazmín Ramirez</td>
                    <td>1 SESIÓN DE FOTO PREVIA</td>
                </tr>
                <tr>
                    <td>VESTIDOS SOPHIE</td>
                    <td>Esther Castillo Sanchez</td>
                    <td>1 VESIDO</td>
                </tr>
                <tr>
                    <td>DREAMS HUATULCO</td>
                    <td>Alma susana</td>
                    <td>RESORT & SPA ESTANCIA 4 DÍAS 3 NOCHES</td>
                </tr>
                <tr>
                    <td>SECRETS HUATULCO</td>
                    <td>Gabriela Arroyo</td>
                    <td>RESORT & SPA ESTANCIA 4 DÍAS 3 NOCHES</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row center-align" id="sedes" style="background: white;">
        <span class="row">
            <br><i class="fa fa-map-marker" aria-hidden="true" style="font-size: 5vh; color: #848484;"></i><br>
            <strong style="color: #848484;font-size: 4vh;">UBICACIÓN</strong>
            <hr>
        </span>
        <div class="col l6 s12 banner">
            <img class="banner-sedes" style="width: 100%; height:100%;" src="<?php echo base_url() ?>dist/img/brideweekend/qro.jpg">
        </div>
        <div class="col l6 s12 mapa">
            <iframe id="mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3735.2362624184007!2d-100.35181168563265!3d20.578406986244076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d35ad251120a2f%3A0x80670acc687aff68!2sQuer%C3%A9taro%20Centro%20de%20Congresos!5e0!3m2!1ses-419!2smx!4v1566834210092!5m2!1ses-419!2smx" style="width: 100%; height:100%;" frameborder="0" style="border:0"></iframe>
        </div>
        <div class="col l12 s12" id="event-counter" style="background: #515151 !important;" >
            <div class="col l2 s12 offset-l1 center-align" id="pleca">
                <img class="pleca" src="<?php echo base_url() ?>dist/img/brideweekend/plecaqro.png">
            </div>
            <div class="class col l2 s3 center-align date-expo ">
                <h5 class="date-text" id="event-days"> </h5>
                <p>Días</p>
            </div>
            <div class="class col l2 s3 center-align date-expo">
                <h5 class="date-text" id="event-hours"> </h5>
                <p>Horas</p>
            </div>
            <div class="class col l2 s3 center-align date-expo">
                <h5 class="date-text" id="event-minutes"> </h5>
                <p>Min</p>
            </div>
            <div class="class col l2 s3 center-align date-expo">
                <h5 class="date-text" id="event-seconds"> </h5>
                <p>Seg</p>
            </div>
        </div>
    </div>  
    <div class="row" id="brideweekend">
        <div class="col l6 s12 hide-on-small-only" id="img-bride">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend4.jpg" style="width: 100%">
        </div>
        <div class="col l6 s12 seccion">
            <div class="col s12 l12" id="title-bride">
                <h5 class="title" >CONCEPTO</h5>
                <hr>
            </div>
            <div class="col l12 s12 alinear text-bride">
                <h5>
                    En BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> reinventamos el concepto tradicional de las expos de boda, en una experiencia única capaz de ofrecer a las parejas en un solo fin de semana y en un único espacio todo lo necesario para crear su boda perfecta.
                </h5>
                <h5>
                    Nos mantenemos a la vanguardia en la industria nupcial, al contar con más de 2,000 modelos de vestidos de diferentes marcas para estar completamente seguros de que aquí encontrarán el ideal.
                </h5>
                <h5>
                    Tendencias, creatividad y emoción se dan cita en BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong>, donde encontrarás una variada y seleccionada oferta de servicios nupciales, conocer
                    novedades, obtener el mejor asesoramiento, disfrutar de desfiles y obtener regalos,
                    para hacer realidad la boda de tus sueños.
                </h5>
            </div>
        </div>
        <div class="col l6 hide-on-large-only" id="img-bride2">
            <br>
            <img src="<?php echo base_url() ?>dist/img/brideweekend/brideweekend4.jpg" style="width: 100%">
        </div>
    </div>
    <div class="row" id="comprador" style="background: #cdcdcd">
        <div class="col l6 s12 seccion">
            <div class="col s12 l12" id="title-bride">
                <h5 class="title" >NOVIA</h5>
                <hr>

            </div>
            <div class="col l12 S12 alinear text-bride">
                <h5>    
                    En BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> nos esforzamos cada día para que todo salga impecable, nuestro compromiso es hacer realidad el sueño de la boda perfecta de cada pareja según sus gustos e ideas.
                </h5>
                <h5>
                    Somos especialistas en reducir el estrés y calmar a los novios en esos momentos de nervios durante la organización de su boda, logrando que las vivencias durante todo este proceso resulten divertidas, excitantes, con ilusión y sobre todo con un concepto vanguardista, jóven, moderno e incluyendo las ultimas tendencias de moda. 
                    <br> Por ello tendremos dos pasarelas por día con las últimas tendencias de vestidos de novia en horario de 2:30 y 5:30 para que nadie quede fuera. 
                </h5>
                <h5>
                Te recordamos que nuestro horario de atención en la expo será Sábado de 04:00pm a 08:00pm y Domingo de 12:pm a 08:00pm este 07 y 08 de Septiembre en Centro de Congresos.
                </h5>
                <br><br>
            </div>
        </div>
        <div class="col l6 s12" id="img-novia">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/novia.jpg" style="width: 100%">
        </div>
    </div>
    
    <div class="row" id="expositor">
        <div class="col l6  hide-on-small-only" id="img-expositor">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/expositor.jpg" >
        </div>
        <div class="col l6">
            <div class="col s12 l12 s12 alinear"  id="title-bride">
                <h5 class="title">EXPOSITOR</h5>
                <hr>
            </div>
            <div class="col l12 alinear s12 text-bride">
                <h5>BRIDE <strong style="font-weight: normal; font-style: italic">WEEKEND</strong> es la cita ineludible para los profesionales de la industria nupcial y el evento ideal para todas aquellas parejas que están planeando su boda y que puedan vivir la experiencia de encontrar lo necesario para el día más importante de sus vidas.</h5>
                <h5>Un nuevo concepto de exposición en el que las parejas, podrán disfrutar de pasarelas y otras actividades en las que obsequiarán importantes premios como productos, y servicios para su boda, lunas de miel y hasta un auto.</h5>
                <h5>Somos líderes en el mercado de exposiciones, debido a nuestra experiencia con diversos conceptos de éxito,  esperamos a más de 5,000 asistentes en esta edición y contar con la participación de 200 expositores de diferentes giros.</h5>
            </div>
            <div class="col l12">
                <br>
                <a class="btn contratar"  style="background: #848484">CONTRATA AQUÍ</a>
                <br><br>
            </div>
        </div>
        <div class="col l6 hide-on-large-only"  id="img-expositor2">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/expositor.jpg" style="width: 100%">
        </div>
    </div>
    <div class="row">
        <div class="col s12 l12 center-align" style="background: #E9EBEA; ">
            <div class="col s12 l12"  id="">
                <h5 class="title">MIEMBROS DE ASOCIACIONES</h5>
                <hr>
            </div>
            <div class="container">
                <div class="col l4 s12">
                    <a href="http://amprofec.org"><img src="<?php echo base_url() ?>dist/img/brideweekend/amprofec.png" style="width: 80%"></a>
                </div>
                <div class="col l4 s12">
                    <a href="https://nupcialmexicana.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/anm.png" style="width: 82%"></a>
                </div>
                <div class="col l4 s12">
                    <a href="https://brideadvisor.mx"><img src="<?php echo base_url() ?>dist/img/brideweekend/plecaBrideAdv.png" style="width: 64%"></a>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL -->

    <div id="modal4" class="modal">
        <br>
        <form method="POST" class="col  s12  m12 l12 " onsubmit="return submitUserForm();"
                action="<?php echo base_url()."brideweekend/email_expositores"?>">
            <div class="model-content row center-align">
                <h4>CONTACTAR</h4>
                <div class="input-field col s10 offset-s1">
                    <input id="nombre_" type="text" name="name" class="validate" required>
                    <label for="nombre_">Nombre</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <input id="correo_" type="text" name="email" class="validate" required>
                    <label for="correo_">Correo</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <input id="telefono_" type="text" name="telefono" class="validate" required>
                    <label for="telefono_">Teléfono</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <textarea id="mensaje_" type="text-area" name="msg" class="validate" required></textarea>
                    <label for="mensaje_">Mensaje</label>
                </div>
                <div class="input-field col s10 offset-s1">
                    <div class="g-recaptcha" data-sitekey="6Lexi6kUAAAAAGHT7BAXC4rQnAlR3NJHm0sTiQHA" data-callback="onSuccess"></div>
                    <div id="g-recaptcha-error"></div>
                </div>
                <br/><button class="btn" type="submit" >Enviar</button>
            	<!--<input type="hidden" id="msj" value="<?php echo $mensaje ?>">-->
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
            </div>
        </form>
    </div>

</div>
</body>
<?php $this->view("brideweekend/footer"); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    var STATE = "";
    $(document).ready(function(){
        $('.slider').slider({full_width: true, interval: 7777});

            $('#cata').on('click', function() {
            $('.cata').removeClass('oculto');
            $('.ramos').addClass('oculto');
            $('.maquillaje').addClass('oculto');
        });
        $('#ramos').on('click', function() {
            $('.ramos').removeClass('oculto');
            $('.cata').addClass('oculto');
            $('.maquillaje').addClass('oculto');
        });
        $('#maquillaje').on('click', function() {
            $('.maquillaje').removeClass('oculto');
            $('.ramos').addClass('oculto');
            $('.cata').addClass('oculto');
        });
        $("#registerMaquillaje").click(function () {
            var tipo= "maquillaje";
            guardarActividad(tipo);
        });
        $("#registerCata").click(function () {
            var tipo= "cata";
            guardarActividad(tipo);
        });
        $("#registerRamos").click(function () {
            var tipo= "ramos";
            guardarActividad(tipo);
        });
        const eventDate = "2019-09-07 12:00:00";
        setCountDownTimer(eventDate);
        $(".contratar").click(function () {
            $('#modal4').openModal();
        });
        $('.register').on('click', registrar);

        $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    if(location){
                        if(location.state)
                            STATE = location.state;
                    }
                },
            });

    });

    var day=null;
    function guardarActividad(tipo) {
        if(tipo=="cata"){
            if(day==null) {
            var memo=document.getElementsByName('group1');
            for(i=0; i<memo.length; i++) {
                if(memo[i].checked) {
                    day=memo[i].value;
                }
            }
        }
        }else{
            day="sabado";
        }
        if($('.'+ tipo +' #nombreNovia').val()!=null &&
           $('.'+ tipo +' #nombreNovia').val()!='' &&
           $('.'+ tipo +' #fechaActividad').val()!=null &&
           $('.'+ tipo +' #fechaActividad').val()!='' &&
           $('.'+ tipo +' #ciudad').val()!=null &&
           $('.'+ tipo +' #ciudad').val()!='' &&
           $('.'+ tipo +' #telActividad').val()!=null &&
           $('.'+ tipo +' #telActividad').val()!='' &&
           $('.'+ tipo +' #correoActividad').val()!=null &&
           $('.'+ tipo +' #correoActividad').val()!='') {
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."brideweekend/registerActivity" ?>',
                method: 'post',
                data: {
                    nomNovia: $('.'+ tipo +' #nombreNovia').val(),
                    nomNovio: $('.'+ tipo +' #nombreNovio').val(),
                    fecha: $('.'+ tipo +' #fechaActividad').val(),
                    correo: $('.'+ tipo +' #correoActividad').val(),
                    ciudad: $('.'+ tipo +' #ciudad').val(),
                    telefono: $('.'+ tipo +' #telActividad').val(),
                    actividad: tipo,
                    dia: day,
                },
                success: function(response) {
                    if(response.validate==false) {
                        day=null;
                        swal('Error', 'Ya esta registrado', 'error');
                    } else if(response.validate=='llena') {
                        day=null;
                        $('.'+ tipo +' #nombreNovia').val('');
                        $('.'+ tipo +' #nombreNovio').val('');
                        $('.'+ tipo +' #correoActividad').val('');
                        $('.'+ tipo +' #ciudad').val('');
                        $('.'+ tipo +' #telActividad').val('');
                        $('.'+ tipo +' #fechaActividad').val('');
                        // $('.'+ tipo +'#modal2').closeModal();
                        swal('Error', 'Lo sentimos, la actividad llegó al máximo de registros', 'error');
                    } else {
                        day=null;
                        $('.'+ tipo +' #nombreNovia').val('');
                        $('.'+ tipo +' #nombreNovio').val('');
                        $('.'+ tipo +' #correoActividad').val('');
                        $('.'+ tipo +' #ciudad').val('');
                        $('.'+ tipo +' #telActividad').val('');
                        $('.'+ tipo +' #fechaActividad').val('');
                        // $('.'+ tipo +'#modal2').closeModal();
                        swal('Felicidades', 'Tu registro fue exitoso.', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        } else {
            day=null;
            swal('Error', 'Por favor complete todos los campos.', 'error');
        }
    }

    function jsRemoveWindowLoad() {
        // eliminamos el div que bloquea pantalla
        $("#WindowLoad").remove();
    
    }
    function jsShowWindowLoad(mensaje) {
        //eliminamos si existe un div ya bloqueando
        jsRemoveWindowLoad();
    
        //si no enviamos mensaje se pondra este por defecto
        if (mensaje === undefined) mensaje = "Creando perfil, por favor espere!!";
    
        //centrar imagen gif
        height = 20;//El div del titulo, para que se vea mas arriba (H)
        var ancho = 0;
        var alto = 0;
    
        //obtenemos el ancho y alto de la ventana de nuestro navegador, compatible con todos los navegadores
        if (window.innerWidth == undefined) ancho = window.screen.width;
        else ancho = window.innerWidth;
        if (window.innerHeight == undefined) alto = window.screen.height;
        else alto = window.innerHeight;
    
        //operación necesaria para centrar el div que muestra el mensaje
        var heightdivsito = alto/2 - parseInt(height)/2;//Se utiliza en el margen superior, para centrar
    
    //imagen que aparece mientras nuestro div es mostrado y da apariencia de cargando
        imgCentro = "<div style='text-align:center;height:" + alto + "px;'><div  style='color:black;margin-top:" + heightdivsito + "px; font-size:20px;font-weight:bold'>" + mensaje + "</div><img style='width: 70px; height:70px;' src='<?php echo base_url() ?>dist/img/brideweekend/loader.gif'></div>";
    
            //creamos el div que bloquea grande------------------------------------------
            div = document.createElement("div");
            div.id = "WindowLoad"
            div.style.width = ancho + "px";
            div.style.height = alto + "px";
            $("body").append(div);
    
            //creamos un input text para que el foco se plasme en este y el usuario no pueda escribir en nada de atras
            input = document.createElement("input");
            input.id = "focusInput";
            input.type = "text"
    
            //asignamos el div que bloquea
            $("#WindowLoad").append(input);
    
            //asignamos el foco y ocultamos el input text
            $("#focusInput").focus();
            $("#focusInput").hide();
    
            //centramos el div del texto
            $("#WindowLoad").html(imgCentro);
    
    }
    
    function registrar() {
        if($('#nombre').val()!=null && $('#correo').val()!=null && $('#telefono').val()!=null
        && $('#fecha').val()!=null && $('#nombre').val()!="" && $('#correo').val()!="" 
        && $('#telefono').val()!="" && $('#fecha').val()!="") {
            jsShowWindowLoad();
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."brideweekend/register" ?>',
                method: 'post',
                data: {
                    nombre: $('#nombre').val(),
                    correo: $('#correo').val(),
                    come_from: 'japy',
                    genero: '2',
                    telefono: $('#telefono').val(),
                    fecha: $('#fecha').val(),
                    come_from: STATE,
                },
                success: function(response) { 
                    if(response.validate==false) {
                        jsRemoveWindowLoad();
                        swal('Error', 'Correo registrado anteriormente.', 'error');
                    } else {
                        jsRemoveWindowLoad();
                        $('#nombre').val('');
                        $('#correo').val('');
                        $('#telefono').val('');
                        $('#fecha').val('');
                        $('#modal1').openModal();
                        // swal('Felicidades', 'Tu registro fue exitoso.', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        } else {
            swal('Error', 'Por favor complete todos los campos.', 'error');
        }
    }
    function submitUserForm() {
            var response = grecaptcha.getResponse();
                if(response.length == 0) {
                    document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">Debes marcar la casilla para continuar</span>';
                    return false;
                }else{
                    document.getElementById('g-recaptcha-error').innerHTML = '';
                }
                return true;
    }
    var onSuccess = function(response) {
        var response = grecaptcha.getResponse();
        if(response.length != 0) {
            document.getElementById('g-recaptcha-error').innerHTML = '';
            return true;
        }
    }
    function verifyCaptcha() {
        document.getElementById('g-recaptcha-error').innerHTML = '';
    }
    var timer='';
    function setCountDownTimer(eventDate) {
        clearInterval(timer);
        let countDownDate = new Date(eventDate).getTime();
        const $eventDays = $('#event-days');
        const $eventHours = $('#event-hours');
        const $eventMinutes = $('#event-minutes');
        const $eventSeconds = $('#event-seconds');
        const $eventCounter = $('#event-counter');

        timer = setInterval(function() {
            let now = new Date().getTime();
            let distance = countDownDate - now;
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            $eventDays.html(days);
            $eventHours.html(hours);
            $eventMinutes.html(minutes);
            $eventSeconds.html(seconds);

            if (distance < 0) {
                $eventCounter.hide();
            }
        }, 1000);
    }
</script>
</html>