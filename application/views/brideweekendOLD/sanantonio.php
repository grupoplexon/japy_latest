<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BW San Antonio</title>
    <link href="<?php echo base_url() ?>dist/img/brideweekend/logo.png" rel="image_src">
        <?php $this->view("brideweekend/header_ciudades"); ?>
     <!-- UIkit CSS -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>


    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <link href="<?php echo base_url() ?>dist/slick/slick.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>dist/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
</head>
<style>
body {
    font-family: 'Roboto', sans-serif;
    color: black !important;
}

hr{ 
  border: 2px solid;
  color: #8dd7e0;
  align: center;
  width: 5%;
  margin: auto;
}
.contentsecond {
    padding-left: 5vh;
    padding-right: 5vh;
    padding-top: 0vh;
    padding-bottom: 0vh;
}
.alinear {
    padding-left: 0 !important;
    padding-right: 0 !important;
    padding-top: 0 !important;
    padding-bottom: 0 !important;
}
.carousel .carousel-item {
    visibility: unset !important;
}
.imgrecinto {
    height: 500px;
}
.map {
    width: unset;
}


.file-field .btn:hover, .btn-large:hover {
    background-color: #848484;
}
.btn span{
    font-weight: bold;
}
.file-field .btn{
    border-radius: 1px;
    width: 40%;
}
span{
    font-size: 15px;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px) {
    .imgrecinto {
        height: unset !important;
    }
    .map {
        width: 100% !important;
    }
    .file-field .btn{
    border-radius: 1px !important;
    width: 50% !important;
    }
    #inputs{
        margin-top: -12% !important;
        margin-bottom: 3% !important;
    }

    .contentsecond {
    padding-left: 0vh !important;
    padding-right: 0vh !important;
    padding-top: 0vh !important;
    padding-bottom: 0vh !important;
}
}
.slick-prev, .slick-next {
    color: #515151;
}
.uk-lightbox {
    background: #000000e6;
}
.file-field .btn{
    border-radius: 1px;
    width: 40%;
}
.file-field .btn, .btn-large:hover {
    background-color: #848484;
}
input {
    box-sizing: border-box !important;
}
input[type=text]:not(.browser-default){
    margin: 0 0 0px 0;
}
input {
    display: block !important;
    font-size: 14px !important;
    line-height: 1.42857143 !important;
    color: #555 !important;
    background-color: #fff !important;
    background-image: none !important;
    border: 1px solid #ccc !important;
    border-radius: 2px !important;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075) !important;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075) !important;
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s !important;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
    padding-left: 5px !important;
}
.btn{
    background-color: #848484;
    border-radius: 1px;
}
#WindowLoad
{
    position:fixed;
    top:0px;
    left:0px;
    z-index:3200;
    filter:alpha(opacity=65);
   -moz-opacity:65;
    opacity:0.9;
    background:#999;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px) {

}
</style>
<body>
    <div class="content center-align">
        <div class="row">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/SA.png" style="width: 100%;">
        </div>

        <div class="row" id="contacto">
            <div class="row center-align"id="form">
                <div class="row " id="">
                    <h5 style="color: black !important; font-size: 40px !important; margin:0px !important; font-weight: bold !important;">FREEMAN COLISEUM</h5>
                    <!-- <h5 style="color: black !important; font-size: 20px !important; margin:0px !important;">South Hall K</h5> --><br>
                    <div class="col l6">
                        <div class="file-field input-field " style="margin-bottom: 35px; padding-top: 0px;">
                            <div class="btn">
                                <span>Nombre</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" name="nombre" id="nombre" class="form-control" required>
                            </div>
                        </div>
                        <div class="file-field input-field " style="margin-bottom: 35px;">
                            <div class="btn">
                                <span>Email</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input id="correo" name="correo" type="text" class="validate" required>
                            </div>
                        </div>
                        <div class="file-field input-field  " style="margin-bottom: 35px;">
                            <div class="btn">
                                <span>Telefono</span>
                            </div>
                            <div class="file-path-wrapper">
                                <!-- <input type="number" name="telefono" id="telefono" class="form-control" required> -->
                                <input name="telefono" id="telefono" class="form-control" required type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                            </div>
                        </div>
                        <div class="file-field input-field ">
                            <div class="btn alinear">
                                <span>Comentarios</span>
                            </div>
                            <div class="file-path-wrapper">
                                <input type="text" name="fecha" id="fecha" required>
                            </div>
                        </div>
                        <br>
                        <button class="btn btn-block-on-small dorado-2 register">
                            Enviar
                        </button>
                    </div>
                    <div class="col s12 m6 l6 center-align grey lighten-4">
                        <img src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/SA-logo.png" style="width: 30%; margin-top: 20px !important;">
                        <h5 style="color: black !important; font-size: 40px !important; margin-top:20px !important; margin-bottom: 0px !important; font-weight: bold !important;">NOVIEMBRE 1 | 2020</h5>
                        <h5 style="color: black !important; font-size: 20px !important; margin-top:0px !important;">Sábado y Domingo 11:00 am - 5:00 pm</h5>
                        <h5 style="color: black !important; font-size: 30px !important; margin-top:30px !important; margin-bottom: 0px !important; font-weight: bold !important;">San Antonio, TX</h5>
                        <h5 style="color: black !important; font-size: 20px !important; margin-top:0px !important;">3201 East Houston Street</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row  imgrecinto">
        <div class="col l6 s12 alinear" style="height: 100%">
            <img src="<?php echo base_url() ?>dist/img/brideweekend/ciudades/sanantonio.png" style="width: 100%; height: 100%;">
        </div>
        <div class="col l6 s12 alinear map" style="height: 100%" id="div-map">
            <iframe id="mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3475.062708171398!2d-98.44134628541819!3d29.42696358211103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x865cf6806cbe203d%3A0x303300b41e493a05!2s3201%20E%20Houston%20St%2C%20San%20Antonio%2C%20TX%2078219%2C%20EE.%20UU.!5e0!3m2!1ses-419!2smx!4v1571778560714!5m2!1ses-419!2smx" style="width: 100%; height: 100%;" frameborder="0" style="border:0"></iframe>
        </div>
    </div>

    <div class="row center-align" style="    margin-bottom: 0px !important; background: #E9EBEA; ">
        <br>
        <span class="row">
            <strong style="color: black;font-size: 4vh;">MIEMBROS DE ASOCIACIONES</strong>
            <!-- <hr style="width: 40px;color: #f38083;display: block;background: #515151;height: 1px;border: 0;border: 2px solid #515151;padding: 0; "> -->
        </span>
        <div class="container">
            <div class="col l3 s12">
                <a href="http://amprofec.org"><img src="<?php echo base_url() ?>dist/img/brideweekend/amprofec.png" style="width: 90%"></a>
            </div>
            <div class="col l3 s12">
                <a href="https://nupcialmexicana.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/anm.png" style="width: 92%"></a>
            </div>
            <div class="col l3 s12">
                <a href="https://japybodas.com"><img src="<?php echo base_url() ?>dist/img/brideweekend/plecaBrideAdv.png" style="width: 74%"></a>
            </div>
            <div class="col l3 s12">
                <img src="<?php echo base_url() ?>dist/img/brideweekend/iwa.png" style="width: 54%">
            </div>
        </div>
    </div>

    
    </div>
</body>
<?php $this->view("brideweekend/footer"); ?>
</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
     var STATE="";
    $(document).ready(function(){

        $("#afiliados").click(function () {
            $('html,body').animate({
                scrollTop: $("#div_afiliados").offset().top-65
            }, 1000);
        });

        $('.datepicker').pickadate({disable_picker: true, format: 'yyyy/mm/dd',
            format_submit: 'yyyy-mm-dd',
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            weekdaysShort: ['Lun','Mar','Mier','Jue', 'Vie','Sab', 'Dom'],
            weekdaysAbbrev: ['L','M','Mi','J','V','S','D']
        });

        $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    if(location){
                        if(location.state)
                            STATE = location.state;
                    }
                },
            });

        $('.register').on('click', registrar);

        $('.jcarousel-proveedores').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            speed: 300,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left dorado-1-text" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right dorado-1-text" aria-hidden="true"></i></button>',
            autoplay: true,
            lazyLoad: 'ondemand',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
            ],
        });
    });

    function jsShowWindowLoad(mensaje) {
        //eliminamos si existe un div ya bloqueando
        jsRemoveWindowLoad();
    
        //si no enviamos mensaje se pondra este por defecto
        if (mensaje === undefined) mensaje = "Enviando, por favor espere!!";
    
        //centrar imagen gif
        height = 20;//El div del titulo, para que se vea mas arriba (H)
        var ancho = 0;
        var alto = 0;
    
        //obtenemos el ancho y alto de la ventana de nuestro navegador, compatible con todos los navegadores
        if (window.innerWidth == undefined) ancho = window.screen.width;
        else ancho = window.innerWidth;
        if (window.innerHeight == undefined) alto = window.screen.height;
        else alto = window.innerHeight;
    
        //operación necesaria para centrar el div que muestra el mensaje
        var heightdivsito = alto/2 - parseInt(height)/2;//Se utiliza en el margen superior, para centrar
    
    //imagen que aparece mientras nuestro div es mostrado y da apariencia de cargando
        imgCentro = "<div style='text-align:center;height:" + alto + "px;'><div  style='color:black;margin-top:" + heightdivsito + "px; font-size:20px;font-weight:bold'>" + mensaje + "</div><img style='width: 70px; height:70px;' src='<?php echo base_url() ?>dist/img/brideweekend/loader.gif'></div>";
    
            //creamos el div que bloquea grande------------------------------------------
            div = document.createElement("div");
            div.id = "WindowLoad"
            div.style.width = ancho + "px";
            div.style.height = alto + "px";
            $("body").append(div);
    
            //creamos un input text para que el foco se plasme en este y el usuario no pueda escribir en nada de atras
            input = document.createElement("input");
            input.id = "focusInput";
            input.type = "text"
    
            //asignamos el div que bloquea
            $("#WindowLoad").append(input);
    
            //asignamos el foco y ocultamos el input text
            $("#focusInput").focus();
            $("#focusInput").hide();
    
            //centramos el div del texto
            $("#WindowLoad").html(imgCentro);
    
    }

    function jsRemoveWindowLoad() {
        // eliminamos el div que bloquea pantalla
        $("#WindowLoad").remove();
    
    }

    function registrar() {
        if($('#nombre').val()!=null && $('#correo').val()!=null && $('#telefono').val()!=null
        && $('#fecha').val()!=null && $('#nombre').val()!="" && $('#correo').val()!="" 
        && $('#telefono').val()!="" && $('#fecha').val()!="") {
            jsShowWindowLoad();
            $.ajax({
                dataType: 'json',
                url: '<?php echo base_url()."brideweekend/registerUSA" ?>',
                method: 'post',
                data: {
                    nombre: $('#nombre').val(),
                    correo: $('#correo').val(),
                    come_from: 'japy',
                    genero: '2',
                    telefono: $('#telefono').val(),
                    msg: $('#fecha').val(),
                    come_from: STATE,
                },
                success: function(response) {
                    if(response.validate==false) {
                        jsRemoveWindowLoad();
                        swal('Error', 'Lo sentimos ocurrio un error', 'error');
                    } else {
                        jsRemoveWindowLoad();
                        $('#nombre').val('');
                        $('#correo').val('');
                        $('#telefono').val('');
                        $('#fecha').val('');
                        // $('#modal1').openModal();
                        swal('Correcto', 'Tu mensaje fue enviado.', 'success');
                    }
                },
                error: function() {
                    console.log('error');
                },
            });
        } else {
            swal('Error', 'Por favor complete todos los campos.', 'error');
        }
    }
</script>
