<div class="row center-align titulo">
    <div class="col s2 m4 ">
        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/barra.png" alt="">
    </div>
    <div class="col s8 m4">
        <h5 class="title-menu">AGENDA</h5>
    </div>
    <div class="col s2 m4">
        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/barra.png" alt="">
    </div>
</div>
<div class="row container center-align" >
    <h5>
        Aquí encontrarás una lista de actividades por realizar con sus respectivas fechas.
        <br/>Llena cada espacio y que no te falte nada para el gran día.
    </h5>
</div>
<br>
<div class="row titulo" >
        <div class="col s12 m6 l6 titulo" >
            <img class="responsive-img col s12 m10 right" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-1.png"
                 alt="">
        </div>
        <div class="col s12 m6 l6 center-align valign-wrapper caja" >
            <div class="row container">
                <h4> No olvides agendar todos tus pendientes y actividades</h4>
                <h3>
                    Una de las funciones de BrideAdvisor es ayudarte a planificar tus actividades, as&iacute; que revisa tus
                    pendientes cuantas veces necesites.</h3>
            </div>
        </div>
</div>
<div class="row titulo">
    <div class="col s12 m6 l6  center-align valign-wrapper caja"  >
        <div class="row container">
            <h4> Adem&aacute;s de las tareas que BrideAdvisor tiene para ti, t&uacute; puedes enlistar todas tus
                actividades pendientes.</h4>
        </div>
    </div>
    <div class="col s12 m6 l6">
        <img class="responsive-img col s12 m8" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-2.png" alt="">
    </div>
</div>
<div class="row titulo" >
    <div class="col s12 m12 l6 " >
            <div class="row container ">
            
                <img class="responsive-img " src="<?php echo base_url() ?>/dist/img/agenda_boda/img-4.png" alt="">

                <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-6.png" alt="">
            
        </div>
    </div>
    <div class="col s12 m12 l6 center-align valign-wrapper caja" style="">
        <div class="row container">
            <h4>
                Las herramientas que tenemos para ti est&aacute;n conectadas entre s&iacute;, para que puedas llevar un
                control entre tu presupuesto, agenda y proveedores.
            </h4>
        </div>
    </div>
</div>
