<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Conocenos - BrideAdvisor</title>
    <?php $this->view("japy/prueba/header"); ?>

    <link href="<?php echo base_url() ?>dist/css/brideAdvisor/planeador.css" rel="stylesheet" type="text/css"/>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script> -->
</head>
<style>

</style>
<body>
<div class="content background" style=" background-image: url('<?php echo base_url()?>dist/img/fondo.jpg');" style="position: absolute; ">
    <div class="row search background" style=" ">
        <div class="row portada">
        <img class="img-home " style="width:100%;" src="<?php echo base_url() ?>dist/img/brideadvisor/Conocenos01.png">
        </div>
        <div class="col l10 offset-l1 hide-on-small-only center-align menu-planeador">
            <table>
                <tr>
                    <td><a class="opcion" onclick="cargarVista('planeador')"><h5 class="title space" style="color: #515151;">Planeador de boda</h5></a> </td>
                    <td><div class="vl"></div></td>
                    <td><a class="opcion" onclick="cargarVista('agenda')"><h5 class="title space" style="color: #515151;">Agenda</h5></a></td>
                    <td><div class="vl"></div></td>
                    <td><a class="opcion" onclick="cargarVista('invitados')"><h5 class="title space" style="color: #515151;">Invitados & Control de Mesas</h5></a></td>
                    <td><div class="vl"></div></td>
                    <td><a class="opcion" onclick="cargarVista('proveedores')"><h5 class="title space" style="color: #515151;">Proveedores</h5></a></td>
                    <td><div class="vl"></div></td>
                    <td><a class="opcion" onclick="cargarVista('presupuestador')"><h5 class="title space" style="color: #515151;">Presupuestador inteligente</h5></a></td>
                    <td><div class="vl"></div></td>
                    <td><a class="opcion" onclick="cargarVista('vestidos')"><h5 class="title space" style="color: #515151;">Mis vestidos</h5></a></td>

                </tr>
            </table>
        </div>
        <div class="col s12 hide-on-large-only center-align menu-planeador"> 
            <a class="opcion" onclick="cargarVista('planeador')"><h5 class="title space" style="color: #515151;">Planeador de boda</h5></a> 

            <a class="opcion" onclick="cargarVista('agenda')"><h5 class="title space" style="color: #515151;">Agenda</h5></a>

            <a class="opcion" onclick="cargarVista('invitados')"><h5 class="title space" style="color: #515151;">Invitados & Control de Mesas</h5></a>

            <a class="opcion" onclick="cargarVista('proveedores')"><h5 class="title space" style="color: #515151;">Proveedores</h5></a>

            <a class="opcion" onclick="cargarVista('presupuestador')"><h5 class="title space" style="color: #515151;">Presupuestador inteligente</h5></a>

            <a class="opcion" onclick="cargarVista('vestidos')"><h5 class="title space" style="color: #515151;">Mis vestidos</h5></a>
        </div>
        <br>
        <div class="row vistas">
            <?php $this->view("conocenos/planeador"); ?>
        </div>
        <!-- <div class="row portada2 hide-on-med-and-down">
            <a href="" class=""><img class="img-home " src="<?php echo base_url() ?>dist/img/brideadvisor/Conocenos02.png"></a>
        </div> -->
    </div>
    <br>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        
    });
    function cargarVista(opcion){
        $(".vistas").load('<?php echo base_url()."home/" ?>'+opcion);
    }
</script>
</body>
<?php $this->view("japy/prueba/footer"); ?>
</html>