<div class="row center-align titulo">
    <div class="col s2 m4 ">
        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/barra.png" alt="">
    </div>
    <div class="col s8 m4">
        <h5 class="title-menu">MIS VESTIDOS</h5>
    </div>
    <div class="col s2 m4">
        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/barra.png" alt="">
    </div>
</div>
<div class="row container center-align">
    <h5 style="font-weight: bold;">
    Encontrar&aacute;s los mejores dise&ntilde;os de las marcas m&aacute;s importantes
    </h5>
    <h5>
        Cada proveedor te mostrar&aacute; los dise&ntilde;os en tendencia y dar&aacute; consejos para que puedas
        escoger el mejor vestido y as&iacute; luzcas espectacular en tu gran d&iacute;a. Adem&aacute;s encontrarás hermosos vestidos para
        tus damas de honor.
    </h5>
</div>
<div class="row container center-align">
    <div class="col l6 s12 caja valign-wrapper">
        <div class="row">
            <h4>Ponte en contacto con tu proveedor</h4>
            <h3>Agenda una cita con tu proveedor, una vez que hayas seleccionado tu vestido de novia.</h3>
        </div>
    </div>
    <div class="col l6 s12">
        <img class="responsive-img col s12 m12" src="<?php echo base_url() ?>/dist/img/vestidos/contactar.png"
                 alt="">    
    </div>
</div>