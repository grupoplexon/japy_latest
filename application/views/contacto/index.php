<div class="body-container">
    <!--     SECCION INFORMACION       -->
    <div class="row">
        <div class="col s12 m12">
            <div class="card">
                <div class="card-content">
                    <span class="card-title">Contactar con japybodas.com</span>
                    <?php $this->view("proveedor/mensajes") ?>
                </div>
                <div class="card-action">
                    <form action="<?php echo base_url() ?>home/contacto" method="post">
                        <div class="row">
                            <div class="col s12 m2">
                                <p class="right">Nombre <span class="red-text">*</span></p>
                            </div>
                            <input id="nombre" type="text" placeholder="Nombre" name="nombre" class="col s12 m6 form-control"></div>
                        <div class="row">
                            <div class="col s12 m2">
                                <p class="right">E-mail <span class="red-text">*</span></p>
                            </div>
                            <input id="email" type="text" placeholder="E-mail" name="email" class="col s12 m6 form-control"></div>
                        <div  class="row">
                            <div class="col s12 m2">
                                <p class="right">Soy <span class="red-text">*</span></p>
                            </div>
                            <select id="soy" name="soy" onchange="changeSoy()" class="col s12 m6 browser-default form-control">
                                <option value="0">- Selecciona una opci&oacute;n -</option>
                                <option value="Novi@">Novia / Novio</option>
                                <option value="Empresa">Empresas</option>
                            </select></div>
                        <div id="motivo" hidden class="row">
                            <div class="col s12 m2">
                                <p class="right">Motivo <span class="red-text">*</span></p>
                            </div>
                            <select  id="selectMotivo" name="motivo" class="col s12 m6 browser-default form-control">
                                <option value="1">- Selecciona un motivo -</option>
                                <option value="Herramientas del organizador de boda">Herramientas del organizador de boda</option>
                                <option value="Comunidad">Comunidad</option>
                                <option value="Sorteo Japy">Sorteo japybodas.com</option>
                                <option value="Otros motivos">Otros motivos</option>
                            </select></div>
                        <div class="row">
                            <div class="col s12 m2">
                                <p class="right">Comentario <span class="red-text">*</span></p>
                            </div>
                            <textarea id="comentario" placeholder="Comentario	..." name="comentario" style="height:200px" class="col s12 m6 form-control"></textarea></div>
                        <div class="row center">
                            <div class="col s12 m8">
                                <button onclick="return sendMail()" class="btn dorado-2">Enviar</button></div></div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    var changeSoy = function () {
        if ($('#soy').val() == "Novi@") {
            $("#motivo").show();
        } else {
            $("#motivo").hide();
        }
    };

    var sendMail = function () {
        if ($('#nombre').val().length < 4) {
            alert("El nombre debe tener al menos 4 caracteres.");
            return false;
        }
        if (!validarEmail($('#email').val())) {
            alert("El formato de email es incorrecto.");
            return false;
        }
        if ($('#soy').val() == 0) {
            alert("Selecciona si eres un usuario o empresa.");
            return false;
        }
        if ($('#soy').val() == "Novi@") {
            if ($('#selectMotivo').val() == 1) {
                alert("Selecciona un motivo.");
                return false;
            }
        }
        if ($('#comentario').val().length < 4) {
            alert("El comentario debe tener al menos 4 caracteres.");
            return false;
        }

        return true;
    };
    function validarEmail(email) {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!expr.test(email))
            return false;
        else
            return true;
    }

</script>

