<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:url"           content="<?php echo base_url() ?>destinos/destino?destino=<?php echo $destination->name ?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="<?= $destination->name ?>" />
    <meta property="og:description"   content='<?= $destination->description ?>' />
    <meta property="og:image"         content="<?php echo base_url() ?>uploads/images/destinos/<?= $destination->image ?>" />
    
    <title> <?php echo $destination->name ?> - BrideAdvisor</title>
    <?php $this->view("japy/prueba/header"); ?>
    <link rel="stylesheet" href="<?= base_url() ?>dist/css/destino.css">
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script> -->
</head>
<style>
    body{
        background-image: url('<?php echo base_url()?>dist/img/fondo.jpg'); 
        background-size: cover;
        /* margin-bottom: 20vh; */
    }
    
</style>
<body>
    <div class="cont">
        <div class="row principal">
            <img class="img-principal" src="<?php echo base_url()?>uploads/images/destinos/<?= $destination->image ?>" alt="">
            <div class="center-align title-dest">
                <h5 class="title"> <?= $destination->name ?></h5>
            </div>
        </div>
        <div class="row descrip">	
            <h5 class="text"> <?= $destination->description ?> </h5>
        </div>
        <div class="row center-align">
            <h5 class="title">LOCACIÓN</h5>
            <hr>
            <h5 class="text"><?= $destination->location_description ?></h5>
            <br>
            <?php foreach ($locations as $loc) { ?>
                <div class="row card">
                    <div class="col l5 s12 img-loc">
                        <img class="locacion" src="<?php echo base_url()?>uploads/images/destinos/<?= $loc->image ?>" alt="">
                    </div>
                    <div class="col l7 s12 img-loc">
                        <div class="col l10 s10 center-align title-location  ">
                            <h5> <?= $loc->name ?> </h5>
                        </div>
                        <br>
                        <div class="col l12 s12">
                            <h5 class="desc-locacion"> <?= $loc->description ?> </h5>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row ">
            <div class="title-atrac">
                <h5 class="title">ATRACTIVOS TURÍSTICOS PARA TUS INVITADOS</h5>
            </div>
            <h5 class="text"><?= $destination->attractive_description ?></h5>
            <br>
            <div class="row hide-on-small-only" >
            <?php $i=0; 
                foreach ($attractives as $at) { 
                    if($i==1){  $i=0; ?>
                        <div class="row atractivo">
                            <div class="col l6 img-loc right-align">
                                <div class="col l10 s10 title-atrac2 right">
                                    <h5> <?= $at->name ?> </h5>
                                </div>
                                <div class="col s12 l12">
                                    <h5 class="text"><?= $at->description ?></h5>
                                </div>
                            </div>
                            <div class="col l6 img-loc">
                                <img class="locacion" src="<?php echo base_url()?>uploads/images/destinos/<?= $at->image ?>" alt="">
                            </div>
                        </div>
                        <br>
                    <?php }else if($i==0){  $i=1; ?>
                        <div class="row atractivo">
                            <div class="col l6 img-loc">
                                <img class="locacion" src="<?php echo base_url()?>uploads/images/destinos/<?= $at->image ?>" alt="">
                            </div>
                            <div class="col l6 img-loc">
                                <div class="col l10 s10 title-atrac">
                                    <h5>  <?= $at->name ?> </h5>
                                </div>
                                <br>
                                 <div class="col s12 l12">
                                    <h5 class="text"><?= $at->description ?></h5>
                                </div>
                            </div>
                        </div>
                        <br>
                    <?php }
                    }?> 
                </div>
                <div class="row hide-on-large-only">
                    <?php  foreach ($attractives as $at) { ?>
                        <div class="row atractivo">
                            <div class="col l6 img-loc">
                                <img class="locacion" src="<?php echo base_url()?>uploads/images/destinos/<?= $at->image ?>" alt="">
                            </div>
                            <div class="col l6 img-loc">
                                <div class="col l10 s10 title-atrac">
                                    <h5>  <?= $at->name ?> </h5>
                                </div>
                                <br>
                                 <div class="col s12 l12">
                                    <h5 class="text"><?= $at->description ?></h5>
                                </div>
                            </div>
                        </div>
                        <br>
                    <?php  }?> 
                </div>
        </div>
        
    </div>
    <div class="row div-articulos div-recientes center-align">
        <h5 class="title">DESTINOS QUE PUEDEN INTERESARTE</h5>
    </div>
    <div class="row intereses">
        <?php foreach ($recommendations as $rec) { ?>
        <div class="col l3 s12 center-align">
            <a href="<?php echo base_url() ?>destinos/destino?destino=<?php echo $rec->name ?>">
                <div class="col l12 s12" style="background-image: url('<?php echo base_url()?>uploads/images/destinos/<?= $rec->image ?>'); 
                    height: 74px !important; background-repeat: no-repeat; background-size: cover !important; border-radius: 10px 10px 10px 10px;">
                    <!-- <img class="img" src="<?= base_url() ?>dist/img/destinos/img1.png">  -->
                    <p class="padding-d white-text bold shadow-p"><?= $rec->name ?></p>
                </div>
            </a>  
        </div>
        <?php } ?>
    </div>
</body>
<?php $this->view("japy/prueba/footer"); ?>
<script>
   
</script>
</html>