<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Destinos - BrideAdvisor</title>
    <?php $this->view("japy/prueba/header"); ?>
    <link rel="stylesheet" href="<?= base_url() ?>dist/css/destinos.css">
</head>
<body>
    <div class="row background-head">
       <?php $this->view("destinos/search"); ?>
    </div>
    <div class="row">
        <div class="col l10 s12 offset-l1 center-align">
            <h5 class="grey-text bold">PRINCIPALES DESTINOS DE BODA</h5>
            <br>
            <div class="jcarousel-articulos">
            <?php // $city = ['LOS CABOS','SAN MIGUEL DE ALLENDE', 'GUANAJUATO', 'YUCATAN', 'GUANAJUATO']; 
                 // for ($i=0; $i < 5; $i++) { ?>
                <!--<div class="col m2-l3-jcaroul">-->
                <!--<div class="col l12 s12" style="background-image: url('<?= base_url() ?>dist/img/destinos/img<?= $i ?>.png'); -->
                <!--    height: 74px !important; background-repeat: no-repeat; background-size: cover !important;">-->
                    <!-- <img class="img" src="<?= base_url() ?>dist/img/destinos/img1.png"> -->
                <!--    <p class="padding-d white-text bold shadow-p"><?= $city[$i] ?></p>-->
                <!--</div>-->
                <!--</div>-->
            <?php // } ?>
            <?php foreach ($destinations as $dest) { ?>
                <div class="col m2-l3-jcaroul">
                    <a href="<?php echo base_url() ?>destinos/destino?destino=<?php echo $dest->name ?>">
                        <div class="col l12 s12" style="background-image: url('<?php echo base_url()?>uploads/images/destinos/<?= $dest->image ?>'); 
                            height: 74px !important; background-repeat: no-repeat; background-size: cover !important; border-radius: 15px 15px 15px 15px;">
                            <!-- <img class="img" src="<?= base_url() ?>dist/img/destinos/img1.png"> -->
                            <p class="padding-d white-text bold shadow-p"><?= $dest->name ?></p>
                        </div>
                    </a>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
    <!--<div class="row">-->
    <!--    <div class="col l10 offset-l1 center-align">-->
    <!--        <hr>-->
    <!--        <h5 class="grey-text bold">PAQUETES Y PROMOCIONES</h5>-->
    <!--        <br>-->
    <!--        <?php $cont=1; for ($i=0; $i < 6; $i++) { ?>-->
    <!--            <div class="col l4">-->
    <!--                <img class="img" src="<?= base_url() ?>dist/img/destinos/pic<?= $i ?>.png">-->
    <!--            </div>-->
    <!--            <div class="col s12 hide-on-large-only"><br></div>-->
    <!--            <?php if($cont%3==0) { ?>-->
    <!--                <div class="col l12 hide-on-small-only"><br></div>-->
    <!--            <?php } ?>-->
    <!--        <?php $cont+=1; } ?>-->
    <!--    </div>-->
    <!--</div>-->
    <div class="row margins background-footer center-align">
        <h5 class="grey-text bold">INSPIRACIÓN PARA TU BODA</h5>
    </div>
    <br>
    <div class="row">
        <div class="col l12 s12 m12">
        <?php foreach ($magazine as $key => $value) { ?>
            <div class="col l4 s12 m4 center-align">
                <div class="col l12 s12 m12 white shadow height-card">
                    <p class="bold"><?= $value->name_category ?></p>
                    <a href="<?=base_url() ?>magazine/blog/<?= $value->id_blog_post ?>"><h5 class="color bold title-cards"><?= $value->titulo ?></h5></a>
                    <a href="<?=base_url() ?>magazine/blog/<?= $value->id_blog_post ?>">
                        <img class="img-cards" src="<?= base_url() ?>blog/resource/imagen/<?= $value->id_imagen_destacada ?>">
                    </a>
                    <p class="justify"><?= strip_tags (substr($value->contenido,0,250)) ?>...</p>
                </div>
            </div>
            <div class="col s12 hide-on-large-only"><br></div>
        <?php } ?>
        </div>
        <div class="col l2 s12 m12 offset-l5 center-align">
            <br><br><br>
            <a class="color" href="<?= base_url()?>magazine?tipo=destinos"><h5 class="border btn-padding">Ir a magazine</h5></a>
            <br><br><br>
        </div>
    </div>
</body>
<?php $this->view("japy/prueba/footer") ?>
<script>
    $('.jcarousel-articulos').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left flecha" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right flecha" aria-hidden="true"></i></button>',
        lazyLoad: 'ondemand',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });
</script>
</html>