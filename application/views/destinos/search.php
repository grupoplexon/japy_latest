<head>
    
</head>
<style>
    
</style>
<div>
    <div class="col l12 s12 m12 center-align">
        <h4 class="color bold padding-search">Elige tu destino ideal</h4>
        <div class="col l4 s8 offset-s2 offset-l3 margins">
            <input id="btn-search" class="inputs radius-left shadow" type="text" placeholder="Ciudad">
        </div>
        <div class="col l2 s8 offset-s2 margins">
            <button class="btn pink width radius-left-p grey"><i class="fas fa-search"></i> &nbsp;BUSCAR</button>
        </div>
    </div>
</div>
<input type="hidden" id="url" value="<?php echo base_url() ?>">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    var baseURL = $('#url').val();
    var busqueda = '';
    $(document).ready(function() {
        $("#btn-search").focus();
        $(window).scrollTop(0);

        $('.searchProvider').on('click', search);
        
         $( "#btn-search" ).autocomplete({
            source: baseURL+'destinos/autocomplete',
            select: (event, ui) => {
                busqueda = ui.item.value;
                search(busqueda);
            }
        });
    });
    function search(busqueda) {
        window.location.href = "<?php echo base_url() ?>destinos/destino?destino="+busqueda;
    }
</script>