<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<?php $this->view('principal/header'); ?>
<?php if ($this->checker->isLogin() && $this->checker->isNovio()) { ?>
    <?php $this->view('principal/novia/menu'); ?>
<?php } ?>
<title>404 Page Not Found</title>
<meta name="theme-color" content="#f5e6df">
<meta name="msapplication-navbutton-color" content="#f5e6df">
<meta name="apple-mobile-web-app-status-bar-style" content="#f5e6df">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<style type="text/css">
    p,h3{
        text-align: center;
    }
    h3{
        font-weight: bold;
        font-size: 25px;
    }
    .icono{
        font-size: 50px;
    }
    .aviso{
        font-size: 14px;
        color: #75767a!important;
    }
    .container-buscador{
        position: relative;
        height: auto;
        margin-bottom: 0px;
        bottom: 0px;
    }
    .dropdown-content.sub-menu > div{
        line-height: 30px;
    }
</style>
<body>
<!--    	<div id="container">
                    <h1><?php // echo $heading;     ?></h1>
    <?php // echo $message; ?>
            </div>-->
    <div class="body-container">
        <div class="row">
            <div class="col s12 m12">
                <div class="card-panel z-depth-2">
                    <div class="row">
                        <p class="icono"><i class="fa fa-info-circle" aria-hidden="true"></i></p>
                        <h3>P&aacute;gina no encontrada</h3>
                        <p class="aviso">Verifica que la direccion web solicitada no contenga errores.</p>
                        <p>
                            <a class="btn waves-effect waves-light dorado-2 tooltipped clickable atras" data-tooltip="Regresar"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                            <a href="<?php echo base_url() ?>" class="btn waves-effect waves-light dorado-2 tooltipped" data-tooltip="Inicio"><i class="fa fa-home" aria-hidden="true"></i></a>
                        </p>
                    </div>
                    <div class="row">
                        <div class="container-buscador">
                            <div class="head" >
                                ENCUENTRA TODO PARA TU BODA EN UN SOLO LUGAR
                            </div>
                            <?php $this->view('searchBar'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url() ?>dist/js/searchBarClubnupcial.js" type="text/javascript"></script>
</body>
<?php $this->view('principal/footer'); ?>