<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists("base_url")) {

    function base_url() {
        return "http://" . $_SERVER["SERVER_NAME"] . "/" . (ENVIRONMENT == "development" ? "clubnupcial/" : "");
    }

}
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Error</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="theme-color" content="#f5e6df">
        <meta name="msapplication-navbutton-color" content="#f5e6df">
        <meta name="apple-mobile-web-app-status-bar-style" content="#f5e6df">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css"  rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url() ?>dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
        <style type="text/css">

            p,h3{
                text-align: center;
            }
            h3{
                font-weight: bold;
                font-size: 25px;
            }
            .icono{
                font-size: 50px;
            }
            .aviso{
                font-size: 14px;
                color: #75767a!important;
            }
            .container-buscador{
                height: auto;
                margin-bottom: 0px;
                bottom: 0px;
            }
            .dropdown-content.sub-menu > div{
                line-height: 30px;
            }
        </style>
    </head>
    <body>
        <!--	<div id="container">
                        <h1><?php // echo $heading;   ?></h1>
        <?php // echo $message;  ?>
                </div>-->
        <div class="body-container">
            <div class="row">
                <div class="col s12 m12">
                    <div class="card-panel z-depth-2">
                        <div class="row">
                            <p class="icono"><i class="fa fa-info-circle" aria-hidden="true"></i></p>
                            <h3>P&aacute;gina no encontrada</h3>
                            <p class="aviso">Verifica que la direccion web solicitada no contenga errores.</p>
                            <p>
                                <a class="btn waves-effect waves-light dorado-2 tooltipped clickable atras" data-tooltip="Regresar"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                                <a href="<?php echo base_url() ?>" class="btn waves-effect waves-light dorado-2 tooltipped" data-tooltip="Inicio"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() ?>dist/js/materialize.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('.tooltipped').tooltip({delay: 50});
            });

            $(document).ready(function () {
                $(".atras").on('click', function () {
                    history.back();
                });
            });
        </script>
    </body>
</html>
<?php
exit();
