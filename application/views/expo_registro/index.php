<?php
$this->view("general/head");
?>
    <!--  ESTILOS  DE  REGISTRO  -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/dist/css/registro.css">
    <style>
        .registro-main {
            background-image: url('<?php echo base_url()?>/dist/img/registro_expo/fondo_guadalajara.png');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: top;
            position: relative;
        }

        @media only screen and (max-width: 321px) {
            .registro-main {
                background-image: url('<?php echo base_url()?>/dist/img/registro_expo/fondo-movil.png')!important;
                background-position: center;
            }
            .container-block{
                padding: 1.5rem;
            }
        }

        @media only screen and (min-width: 322px) and (max-width: 425px) {
            .registro-main {
                background-image: url('<?php echo base_url()?>/dist/img/registro_expo/fondo-movil.png') !important;
                background-position: center;
            }
            .container-block{
                padding: 1.5rem;
            }
        }

        @media only screen and (min-width: 426px) and (max-width: 768px) {
            .registro-main {
                background-image: url('<?php echo base_url()?>/dist/img/registro_expo/fondo-movil.png') !important;
                background-position: center;
            }
            .container-block{
                padding: 1.5rem;
            }
        }

    </style>
    <!--  ESTILOS  DE  REGISTRO  -->

    <div class="registro-main max-content row">
        <div class="col l3 offset-l4 hide fecha-image hide-on-small-only">
            <a href="" style="display: none">
                <div class="evento">
                    <p class="ciudad">Querétaro</p>
                    <div class="content-evento">
                        <p class="dia"><span>DOMINGO</span></p>
                        <p class="fecha">9 SEPTIEMBRE</p>
                        <p class="lugar">CENTRO DE CONGRESOS</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col s12 m10 offset-m1 l3 offset-l8 registro-sec">
            <div class=" row">
                <div class="square-row">
                    <div class="square background-2">
                        <img src="<?php echo base_url() ?>/dist/img/registro_expo/logo-expo.png" alt="">
                    </div>
                </div>
                <div class="col s12 hide-on-med-and-up" style="margin: 3rem 0 1.2rem;">
                    <a href="">
                        <div class="evento">
                            <p class="ciudad"></p>
                            <div class="content-evento">
                                <p class="dia"><span></span></p>
                                <p class="fecha"></p>
                                <p class="lugar"></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col s12 center-align word">
                    <h3>Registrate</h3>
                </div>
                <form action="<?php echo base_url()."/registro" ?>" method="POST" id="registroexpo"
                      data-parsley-validate="true">
                    <input type="hidden" name="genero" value="2">
                    <input type="hidden" name="come_from" value="">
                    <div class="col s12 m8 offset-m2 l12 formulario ">
                        <div class="row container-block">
                            <div class="col s12 ">
                                <input class=" basic-style" type="text" name="nombre" data-parsley-length="[2,30]"
                                       placeholder="Nombre(s)*" required="">
                            </div>
                            <div class="col s12">
                                <!-- DIVIDER -->
                                <input class=" basic-style" type="email" id="correo" name="correo"
                                       placeholder="Correo*" data-parsley-type="email" required="">
                            </div>
                            <div class="col s12  bt-flabels__wrapper">
                                <input class="basic-style" id="password" type="password" name="contrasena"
                                       placeholder="Ingresa tú Contrase&ntilde;a*" data-parsley-length="[2,30]" required="">
                                <!-- DIVIDER -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m8 offset-m2 center-align">
                                <button class="btn btn-block primary-background darken-5" id="btn_enviar_expo"
                                        type="submit"
                                        style="margin: 30px 0;padding: 0">
                                    CONTINUAR
                                </button>

                            </div>
                            <div class="col s12 center-align">
                                <button class="btn btn-block bg-fb btn-face" id="facebook-login">
                                    <i class="fa fa-facebook-official"></i> <span class="hide-on-med-and-down">Continuar con </span>Facebook
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col s12 terminos center-align text-white">
                    <p>Al dar clic en continuar estás aceptando los terminos , condiciones y nuestras políticas de
                        privacidad </p>
                    <a href="<?php echo base_url()."terminos-japy.pdf" ?>">
                        <p>Consulta en políticas de privacidad</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <input id="base-url" type="hidden" value="<?php echo base_url() ?>">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- FACEBOOK SDK -->
    <script type="text/javascript" src="<?php echo base_url()."dist/js/facebook/login.js" ?>"></script>
    <script>
        $(document).ready(function() {
            const $registerForm = $('#registroexpo').parsley();
            let url = new URL(window.location.href);
            let currentState = (url.searchParams.get('estado') === null) ? 'Jalisco' : url.searchParams.get('estado');

            currentState = currentState.toLowerCase();

            $('input[name=\'come_from\']').val(currentState);

            const events = [
                {
                    'state': 'queretaro',
                    'city': 'Querétaro',
                    'days': 'DOMINGO',
                    'date': '9 SEPTIEMBRE',
                    'place': 'CENTRO DE CONGRESOS',
                    'back': 'fondo_queretaro',
                },
                {
                    'state': 'jalisco',
                    'city': 'Guadalajara',
                    'days': 'SÁBADO Y DOMINGO',
                    'date': '22 Y 23 SEP',
                    'place': 'EXPO GUADALAJARA',
                    'back': 'fondo_guadalajara',
                },
                {
                    'state': 'guanajuato',
                    'city': 'León',
                    'days': 'SÁBADO Y DOMINGO',
                    'date': '26 y 30 SEP',
                    'place': 'POLIFORUM',
                    'back': 'fondo_leon',
                },
                {
                    'state': 'puebla',
                    'city': 'Puebla',
                    'days': 'SÁBADO Y DOMINGO',
                    'date': '6 y 7 OCT',
                    'place': 'CENTRO EXPOSITORES LOS FUERTES',
                    'back': 'fondo_puebla',
                },
                {
                    'state': 'cdmx',
                    'city': 'Cd. de México',
                    'days': 'SÁBADO Y DOMINGO',
                    'date': '12,13 y 14 OCT',
                    'place': 'WORLD TRADE CENTER',
                    'back': 'fondo_cdmx',
                },
                {
                    'state': 'nuevo_leon',
                    'city': 'Monterrey',
                    'days': 'SÁBADO Y DOMINGO',
                    'date': '27 y 28 OCT',
                    'place': 'CINTERMEX',
                    'back': 'fondo_monterrey',
                },
                {
                    'state': 'sinaloa',
                    'city': 'Culiacán',
                    'days': 'DOMINGO',
                    'date': '4 NOVIEMBRE',
                    'place': 'SALÓN FIGLOSTASE',
                    'back': 'fondo_culiacan',
                },
                {
                    'state': 'veracruz',
                    'city': 'Veracruz',
                    'days': 'SÁBADO Y DOMINGO',
                    'date': '23 Y 24 FEB',
                    'place': 'WORLD TRADE CENTER',
                    'back': 'fondo_veracruz',
                },
            ];

            for (let event of events) {
                if (event.state === currentState) {
                    $('.ciudad').text(event.city);
                    $('.dia>span').text(event.days);
                    $('.fecha').text(event.date);
                    $('.lugar').text(event.place);
                    if (screen.width > 768) {
                        $('.registro-main').css('background-image', 'url(<?php echo base_url() ?>dist/img/registro_expo/' + event.back + '.png)');
                    }
                    break;
                }
                else {
                    $('.registro-main').css('background-image', 'url(<?php echo base_url() ?>dist/img/registro_expo/fondo_guadalajara.png)');
                }
            }

            $('#btn_enviar_expo').on('click', function(e) {

                e.preventDefault();

                $registerForm.validate();

                if ($registerForm.isValid()) {
                    $.ajax({
                        url: '<?php echo base_url()."registro/checkUsuario" ?>',
                        method: 'post',
                        data: {
                            'correo': $('#correo').val(),
                        },
                        success: function(res) {
                            if (res.data == true) {
                                swal('Error', 'Correo registrado anteriormente.', 'error');
                                return false;
                            }

                            $('#registroexpo').submit();
                        },
                        error: function() {
                            swal('Error', 'Lo sentimos ocurrio un error', 'error');
                        },
                    });
                }
            });
        });
    </script>
<?php
$this->load->view("general/footer", $this->_ci_cached_vars);
$this->load->view("general/newfooter");
