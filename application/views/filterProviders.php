<?php
$search = isset($_GET["search"]) ? htmlspecialchars($_GET["search"]) : "";
$state  = isset($_GET["state"]) ? htmlspecialchars($_GET["state"]) : "";
?>
<style>
    input.select-dropdown {
        background-color: white !important;
        border: 1px solid #ccc !important;
        border-radius: 6px !important;
        padding-left: 20px !important;
        color: #ccc;
        margin: 0 !important;
    }

    span.caret {
        color: #00bbcd !important;
        right: 19px !important;
        top: 5px !important;
        font-size: 15px !important;
        z-index: 5;
    }
</style>
<form class="row" method="GET" action="<?php echo base_url().(isset($category) ? "mx-".$category->slug : "proveedores") ?>"
      style="padding: 10px 0;">
    <div class="col s12 m3 offset-m1" style="margin:0;padding:10px 0;">
        <div style="display: flex;align-items: center;">
            <input name="buscar" id="btn-search" class="form-control dropdown-button" style="margin: 0;"
                   value="<?php echo $search ?>" placeholder="¿Qué estas buscando?"/>
        </div>
        <div>
            <?php if ( ! empty($search)) : ?>
                <span>Resultados de busqueda para: <?php echo $search ?></span>
            <?php endif ?>
        </div>
    </div>
    <div class="col s12 m3" style="display: flex;align-items: flex-start;margin: 0;padding:10px 0;">
        <div class="input-field col s12" style="margin: 0;">
            <select name="estado" id="stateId" style="width: 100%; margin-top:10px; margin-bottom: 0px; height: 45px">
            </select>
        </div>
    </div>
    <div class="col s12 m3" style="display: flex;align-items: flex-start;margin: 0;padding:10px 0;">
        <div class="input-field col s12" style="margin: 0;">
            <select name="ciudad" id="cities" style="width: 100%; margin-top:10px; margin-bottom: 0px; height: 45px">
            </select>
        </div>
    </div>
    <div class="col s12 m1" style="display: flex;justify-content: center;padding:10px 0;">
        <button type="submit" class="btn btn-custom" style="height: 3rem;">BUSCAR</button>
    </div>
    <input type="hidden" id="url" value="<?php echo base_url() ?>">
</form>
<script>
    $(document).ready(function() {
        let server = $('#url').val();
        let url = new URL(window.location.href);
        let currentState = (url.searchParams.get('state') === null) ? 'Jalisco' : url.searchParams.get('state');
        let currentCity = (url.searchParams.get('city') === null ) ? 'Todos' : url.searchParams.get('city');
        var search = (url.searchParams.get('search') === null ) ? " " : url.searchParams.get('search');
        var stateSelected = "Todos";
        var citySelected = "Todos";
        const states = [
            {'name': 'Todos'},
            {'name': 'Jalisco'},
            {'name': 'Queretaro'},
            {'name': 'Puebla'},
            {'name': 'Sinaloa'},
        ];

        $('#cities').material_select();
        $('#cities').on('contentChanged', function() {
            $(this).material_select();
        });

        $('#stateId').empty();

        $('select[name="estado"]').append('<option value=\'' + currentState + '\'>' + currentState + '</option>');
        for (let state of states) {
            if (currentState !== state.name) {
                $('select[name="estado"]').append('<option value=\'' + state.name + '\'>' + state.name + '</option>');
            }
        }

        $('#stateId').on('change', function() {
            stateSelected = $(this, '.select-dropdown').val();
            updateUrlFromMenu(server,search,stateSelected,citySelected);

            $.ajax({
                url: server + 'proveedores/getCities',
                method: 'POST',
                data: {
                    state: stateSelected,
                },
                dataType: 'json',
                success: function(data) {
                    let $city;

                    if (stateSelected === 'Todos') {
                        $('select[name="ciudad"]').empty().addClass('hidden');
                        $('#cities').trigger('contentChanged');
                    }
                    else {
                        if(currentCity !== "Todos"){
                            $('select[name="ciudad"]').empty().removeClass('hidden').append('<option value=\'' + currentCity + '\'>' + currentCity + '</option>');
                            $city = $('<option>').attr('value',"Todos").text("Todos");
                            $('select[name="ciudad"]').append($city);
                        }else{
                            $('select[name="ciudad"]').empty().removeClass('hidden').append('<option value="Todos" selected>Todos</option>');
                        }
                        for (let state of data.data) {
                            if(currentCity !== state.localizacion_poblacion){
                                $city = $('<option>').attr('value', state.localizacion_poblacion).text(state.localizacion_poblacion);
                                $('select[name="ciudad"]').append($city);
                            }

                        }

                        $('#cities').trigger('contentChanged');
                    }
                },
                error: function(data) {
                },
            });

        });

        $('#cities').on('change',function () {
            citySelected = $("#cities").siblings(".select-dropdown").val();
            updateUrlFromMenu(server,search,stateSelected,citySelected);
        });

        $('#btn-search').on('change',function () {
            search = $('#btn-search').val();
            updateUrlFromMenu(server,search,stateSelected,citySelected);
        });

        $('#stateId').trigger('change');

    });

    function updateUrlFromMenu(server,search,state,city) {
        $('.sub-menu-item').each(function () {
            let urlBase = $(this).attr("href");
            let categoryDirty = urlBase.substr(urlBase.lastIndexOf('/')+1);
            let clearCategory = categoryDirty.split('?');
            $(this).attr("href",server+clearCategory[0]+"?buscar="+search+"&estado="+state+"&ciudad="+city);
        });
    }
</script>

