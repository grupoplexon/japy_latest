<?php $producto_nombre = str_replace(" ", "-", $producto->nombre); ?>
<?php $dis = str_replace(" ", "-", $disenador); ?>
<html>
    <head>
        <?php $this->view("principal/head") ?>
        <style>
            #solicitar_info{
                display: none;
            }
            a.border{
                border: 1px solid #d0d0d0;
                box-shadow: none !important;
            }
        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("principal/menu");
        }
        ?>
        <div class="body-container">
            <br>
            <section id="label">
            </section>
            <section id="menu">
                <?php $this->view("principal/vestidos/menu") ?>
            </section>
            <section id="breadcrumb">
                <?php
                $this->view("principal/breadcrumbs", array("breadcrumbs" => array(
                        "Home" => base_url(),
                        $producto->nombre => site_url("tendencia/index/$producto_nombre/$dis"),
                        "$producto->nombre de $disenador" => site_url("tendencia/disenador/$producto_nombre/$dis")
            )));
                ?>
            </section>
            <section id="catalogo">
                <h5><?php echo $producto->nombre ?> de <?php echo $disenador ?></h5>
                <div class="divider"></div>
                <div class="row">
                    <?php foreach ($vestidos as $key => $c) { ?>
                        <div class="col m20-porcent s12 ">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <a href="<?php echo site_url("tendencia/articulo/$dis/" . str_replace(" ", "-", $c->nombre)) ?>" >
                                        <img src="data:<?php echo $c->mime_imagen ?>;base64,<?php echo base64_encode($c->imagen) ?>">
                                    </a>
                                    <i class="material-icons  tooltipped favorite vestido dorado-2-text"
                                       data-vestido="<?php echo $c->id_vestido ?>" data-position="top" data-delay="5"
                                       data-tooltip="<?php echo $c->like ? "Ya no me gusta" : "Me gusta" ?>"><?php echo $c->like ? "favorite" : "favorite_border" ?></i>
                                    <div class="social dorado-2-text">
                                        <span>
                                            <i class="material-icons">favorite</i> <?php echo $c->likes ?>
                                        </span>
                                        <span>
                                            <i class="material-icons">chat_bubble</i> <?php echo $c->comments ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="card-action">
                                    <a href="<?php echo site_url("tendencia/articulo/$dis/" . str_replace(" ", "-", $c->nombre)) ?>" class="black-text darken-5">
                                        <?php echo $c->disenador ?> (<?php echo $c->temporada ?>)
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <ul class="pagination">
                    <?php if ($pag == 1) { ?>
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="<?php echo site_url("tendencia/disenador/$producto_nombre/" . str_replace(" ", "-", $disenador)) ?>?pagina=<?php echo ($pag - 1); ?>"><i class="material-icons">chevron_left</i></a></li>
                    <?php } ?>
                    <?php $total_paginas = (int) (($total / 15) + 1); ?>
                    <?php for ($p = 1; $p <= $total_paginas; $p++) { ?>
                        <li class="waves-effect <?php echo ($p == $pag ? "active" : "") ?>"><a href="#!"><?php echo $p ?></a></li>
                    <?php } ?>
                    <?php if ($pag == $total_paginas) { ?>
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                    <?php } else { ?>
                        <li class="waves-effect">
                            <a href="<?php echo site_url("tendencia/disenador/$producto_nombre/" . str_replace(" ", "-", $disenador)) ?>?pagina=<?php echo ($pag + 1); ?>">
                                <i class="material-icons">chevron_right</i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </section>
            <section id="temporadas">
                <h5><?php echo $producto->nombre ?> de <?php echo $disenador ?> por temporada</h5>
                <div class="divider"></div>
                <div class="row">
                    <div class="card-panel">
                        <div class="row">
                            <?php foreach ($temporadas as $key => $t) { ?>
                                <div class="col s12 m3 ">
                                    <div class="row">
                                        <div style="position: relative; margin: 10px 0px 15px 0px;">
                                            <a href="<?php echo site_url("tendencia/catalogo") . "?temporada=$t->temporada" ?>" class="black-text darken-5">
                                                <?php echo $producto->nombre . " " . $t->temporada ?>
                                            </a>
                                            <span class="badge"><?php echo $t->total ?></span>
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <?php $this->view("principal/footer") ?>
        <script src="<?php echo base_url() ?>dist/js/tendencia/vestidos.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                var fbc = new FBclub();
                fbc.initShareLinks();
                vestidos.server = "<?php echo base_url() ?>";
                vestidos.initLike();
            });
        </script>
    </body>
</html>