<html>
    <head>
        <?php $this->view("proveedor/header") ?>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("principal/menu");
        }
        ?>
        <div class="body-container">
            <section id="menu" class="row">
                <?php $this->view("principal/proveedor/menu") ?>
            </section>
            <div class="row">
                <div class="col m8 s12">
                    <section class="carrusel row">

                        <div id="galeria" class="col s12 card-panel">
                            <div data-pos="0" class="principal z-depth-1" style="background-image: url(<?php echo base_url()."uploads/images/".$galeria[0]->nombre ?>)">
                            </div>
                            <div class="hide" style="margin-top: 11px; display: block;overflow: hidden;margin-bottom: 20px;">
                                <div class="imagenes ">
                                    <div style="left: 0px">
                                        <?php
                                        $c = 0;
                                        foreach ($galeria as $key => $g) {
                                            ?>
                                            <div class="imagen-wrapper">
                                                <div data-pos="<?php echo $c++ ?>" class="imagen clickable  waves-effect waves-light active" style="background-image: url(<?php echo base_url()."uploads/images/$g->nombre" ?>)">
                                                </div>
                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="fotos" style="margin-top: 80px">
                        <h5>Galeria de fotos de <?php echo $proveedor->nombre ?></h5>
                        <div class="divider"></div>
                        <div class="row" style="margin-top: 20px">
                            <?php
                            foreach ($galeria as $key => $g) {
                                if ($g->tipo = Galeria_model::$TIPO_IMAGEN) {
                                    ?>
                                    <div class="col s6 m4 l3">
                                        <div class="imagen-wrapper z-depth-1">
                                            <div class="imagen materialboxed" data-caption="<?php echo $g->nombre ?>" style="background-image: url(<?php echo base_url().Galeria_model::$URL_IMAGEN.$g->id_galeria ?>)">
                                            </div>
                                            <label class="center truncate">
                                                <?php echo $g->nombre ?>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </section>
                </div>
                <div class="col m4 s12">
                    <?php $this->view("principal/proveedor/solicitar_info") ?>
                </div>
            </div>
        </div>
        <?php $this->view("proveedor/footer") ?>
        <script src="<?php echo base_url() ?>dist/js/slider.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                setInterval(function() {
                    var pos = $('#galeria .principal').data('pos') + 1;
                    var img = $('#galeria .imagenes .imagen[data-pos=\'' + pos + '\']').get(0);
                    if (img) {
                        img = img.style['background-image'];
                    }
                    else {
                        pos = 0;
                        var img = $('#galeria .imagenes .imagen[data-pos=\'' + pos + '\']').get(0).style['background-image'];
                    }
                    $('#galeria .principal').data('pos', pos);
                    $('#galeria .principal').get(0).style['background-image'] = img;
                }, 8000);

            });
        </script>
        <!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap"></script>-->

    </body>
</html>