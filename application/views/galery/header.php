<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="theme-color" content="#f5e6df">
<meta name="msapplication-navbutton-color" content="#f5e6df">
<meta name="apple-mobile-web-app-status-bar-style" content="#f5e6df">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
<?php if (isset($titulo)) { ?>
    <title><?php echo $titulo ?></title>
<?php } else { ?>
    <title>Japy</title>
<?php } ?>
<link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
<link href="<?php echo $this->config->base_url() ?>/dist/img/clubnupcial.css" rel="icon" sizes="16x16">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo $this->config->base_url() ?>/dist/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->config->base_url() ?>/dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet"
      type="text/css"/>
<link href="<?php echo $this->config->base_url() ?>/dist/css/estilo.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->config->base_url() ?>/dist/css/blog.css" rel="stylesheet" type="text/css"/>
