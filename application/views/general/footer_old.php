<footer class="page-footer">
    <!--FOOTER HIGH-->
    <div class="footer-social ">
        <div class="row center-align max-content" style="padding: 10px 0">
            <div class="col s12 m5 l3 offset-l1">
                <div class="row valign-wrapper no-margin-element">
                    <div class="col s6 l3 center-align" style="padding-top: 11px;">
                        <a href="<?php echo base_url() ?>">
                            <img src="<?php echo base_url() ?>/dist/img/japy_nobg_noslogan.png" style="width: 100px;"
                                 alt="Japy">
                        </a>
                    </div>
                    <div class="col s6 l9 center-align">
                        <span> © DERECHOS RESERVADOS 2018</span>
                    </div>
                </div>
            </div>

            <div class="col s12 m3 l4 center-align" style="margin-left: 0 !important;">
                <div class="row valign-wrapper  no-margin-element" style="height: 83px">
                    <div class="col s3 m3 l1 offset-l4 ">
                        <a href="https://www.facebook.com/Japy-177297236307569/" target="_blank">
                            <i class="fa fa-facebook  icon-style"></i>
                        </a>
                    </div>
                    <div class="col s3 m3 l1 no-margin-element">
                        <a href="https://twitter.com/japymx/" target="_blank">
                            <i class="fa fa-twitter icon-style"></i>
                        </a>
                    </div>
                    <div class="col s3 m3 l1 no-margin-element">
                        <a href="https://www.instagram.com/japymx/" target="_blank">
                            <i class="fa fa-instagram icon-style"></i>
                        </a>
                    </div>
                    <div class="col s3 m3 l1 no-margin-element">
                        <a href="https://co.pinterest.com/japymx/" target="_blank">
                            <i class="fa fa-pinterest-p icon-style"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col s12 m4 " style="margin-left: 0 !important;">
                <div class="row valign-wrapper  no-margin-element" style="height: 83px">
                    <div class="col s6 m6 l4 offset-l3 valign-wrapper offset-s2 no-margin-element logos-pos">
                        <a href="<?php echo base_url() ?>">
                            <img class="imagen-responsive f-img"
                                 src="<?php echo base_url() ?>dist/img/logo-apps/play_store.png" alt="Japy"
                                 style="max-width: 100%;height: auto;">
                        </a>
                    </div>
                    <div class="col s6 m6 l3 valign-wrapper offset-s3 no-margin-element logos-pos">
                        <a href="<?php echo base_url() ?>">
                            <img class="imagen-responsive f-img"
                                 src="<?php echo base_url() ?>dist/img/logo-apps/app_store.png" alt="Japy"
                                 style="max-width: 100%;height: auto;">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--FOOTER HIGH-->
    <!--FOOTER LOW-->
    <div class="footer-info max-content " style="padding: 25px 0;">
        <div class="row center-align no-margin-element">

            <div class="col s12 m4 l3 offset-l1">
                <div class="row no-margin-element">
                    <div class="col s12">
                        <span class="text-footer-style">HOME</span>
                    </div>
                </div>
                <div class="row no-margin-element">
                    <div class="col m12 l12">
                        <ul class="text-footer">
                            <li><a href="<?php echo base_url() ?>login">LOGIN NOVIOS </a>|</li>
                            <li><a href="<?php echo base_url() ?>home/altaEmpresas">LOGIN EMPRESA </a>|</li>
                            <li><a href="<?php echo base_url() ?>proveedores/sector/proveedores">BUSCADOR </a>|</li>
                            <li><a href="<?php echo base_url() ?>home/contacto">PRINCIPAL CONTACTO </a>|</li>
                            <li><a href="<?php echo base_url() ?>home/planeador_bodas">MAPA DEL SITIO</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col s12 m4 l4">
                <div class="row no-margin-element">
                    <div class="col s12">
                        <span class="text-footer-style">MI BODA</span>
                    </div>
                </div>
                <div class="row no-margin-element">
                    <div class="col l12">
                        <?php if ($this->checker->isLogin()) : ?>
                            <ul class="text-footer">
                                <li><a href="<?php echo base_url() ?>Novia">MI PERFIL </a>|</li>
                                <li><a href="<?php echo base_url() ?>novios/presupuesto">PRESUPUESTO </a>|</li>
                                <li><a href="<?php echo base_url() ?>novios/mesa">CONTROL DE MESAS </a>|</li>
                                <li><a href="<?php echo base_url() ?>novios/invitados">TAREAS CONTROL DE INVITADOS </a>|
                                </li>
                                <li><a href="<?php echo base_url() ?>novios/proveedor">MIS PROVEEDORES </a></li>
                            </ul>
                        <?php else : ?>
                            <ul class="text-footer">
                                <li><a href="<?php echo base_url() ?>login">MI PERFIL </a>|</li>
                                <li><a href="<?php echo base_url() ?>login">PRESUPUESTO </a>|</li>
                                <li><a href="<?php echo base_url() ?>login">CONTROL DE MESAS </a>|</li>
                                <li><a href="<?php echo base_url() ?>login">TAREAS CONTROL DE INVITADOS </a>|</li>
                                <li><a href="<?php echo base_url() ?>login">MIS PROVEEDORES </a></li>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="col s12 m4 l3">
                <div class="row no-margin-element">
                    <div class="col s12 ">
                        <span class="text-footer-style link"><a href="<?php echo base_url() ?>blog">BLOG</a> | <a
                                href="<?php echo base_url() ?>"> GALERÍA</a></span>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--FOOTER LOW-->

</footer>