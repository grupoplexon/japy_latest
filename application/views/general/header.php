
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.1/css/materialize.min.css"  media="screen,projection"/>
    <title><?php echo $this->config->item("name")?></title>

    <style>
        .max-content{
            margin: auto;
            max-width: 1903px;
        }
        .header-all{
            height: 210px !important;
            padding-top: 35px;
            margin:auto;
            background-color: white !important;
        }
        .container-menu{
            width: 85%;
            margin:auto;
        }

        .text-style{
            color:#00BCDD !important;
            font-size: 18px !important;
        }

        .nav-content{
           margin-top: 45px;
        }

        .menu-style{
            width: 83%;
        }

        .btn-custom{
            background-color:#00BCDD !important;
        }

        .video-home{
            background: grey;
            width: 100%;
            max-width: 2000px;
            height: 600px;
            margin: auto;
            box-shadow: 0px 44px 40px -14px #bbb5b5;
        }

        .video-home iframe{
            width: 100%;
            height: 600px;
        }

        .category-post{
            height: 425px;
            margin-bottom:30px;
        }

        .row-impar{
            background: #f7f7f7;
        }

        .title-section{
            margin-bottom: 20px;
            padding-top: 15px;
            color:#00BCDD !important;
        }

        .title-section h3{
            text-align: center;
            letter-spacing: 5px;
            font-size: 2.2em;
            margin: 0;
        }
        .ccc {
            border: 1px solid #DDDDDD;
            width: 30%;
            height: 250px;
            position: relative;
            margin:0 1.65%;
            box-shadow: 0px 21px 40px -20px #403e3e;
            background-image: url('https://www.hosteleriasalamanca.es/fotos/1466694398.0561.jpg');
            background-repeat: no-repeat;
            background-size: cover;
        }
        .tag {
            position: relative;
            z-index: 1000;
            width: 90%;
            padding: 10px;
            border:1px solid #f1e6e6;
            background:white;
            color: #FFFFFF;
            font-weight: bold;
            margin: auto;
            height: 150px;
            margin-top: 40%;
        }

        .tag a{
            float: right;
            font-weight: 400;
            color:#00BCDD !important;
        }
        .tag p{
            color: black;
            font-size: 14px;
            font-weight: 400;
            text-align: center;
        }
        .tag h4{
            color: black;
            font-size: 1em;
            text-align: center;
            font-weight: 700;
            margin: .3rem 0 .912rem 0;
        }
    </style>

</head>
<body>
<header>
    <?php $this->load->view('menus/user',$this->_ci_cached_vars); ?>
</header>

