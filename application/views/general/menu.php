<style>
    .redes-sociales li {
        margin-bottom: 10px;
    }

    nav ul a {
        /*font-size: 12px !important;*/
        padding: 0px 7px;
    }

    nav ul li {
        text-align: center;
    }

    div.wrapper-redes-sociales {
        width: 62px;
    }

    div.wrapper-menu {
        width: calc(100% - 62px);
    }

    img.logo_header {
        position: relative !important;
        width: 80% !important;
        height: auto !important;
        margin: 10px auto;
        display: block;
    }

    img.logo_header.hide-on-med-and-up {
        float: left;
        width: auto !important;
        height: 48px !important;
        margin-left: 15px;
    }

    #dropdown-login li:nth-child(n+3):nth-child(-n+8) {
        display: none;
    }

    .submenu-one {
        background-color: rgba(0, 0, 0, 0.05);
    }

    .submenu-one a {
        color: #00bcdd !important;
    }

    .submenu-two {
        background-color: #00bcdd;
    }

    .submenu-two a {
        color: white !important;
    }

    .item-side.active {
        background-color: #1c97b3 !important;
    }

    .side-nav .active {
        color: white !important;
    }

    .center-vertical {
        display: flex;
        align-items: center;
    }

    #sidenav-overlay {
        position: inherit;
    }


    @media only screen and (max-width: 325px) {
        #inicio_registro {
            float: left;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            padding: 0 0.75rem;
            min-height: 1px;
        }
        .nav-content, #nav-mobile {
            float: initial !important;
        }

        .btn-sec, .btn-custom {
            margin-right: 0 !important;
        }

        .logo-japy {
            overflow: hidden;
        }

        .menu-user {
            overflow: visible !important;
        }

        #dropdown-login li:nth-child(n+3):nth-child(-n+8) {
            display: block;
        }
    }

    @media only screen and (max-width: 425px) {

        .nav-style {
            z-index: 100000000;
        }

        #toast-container {
            top: 10% !important;
            bottom: auto !important;
            z-index: 100000001;
        }

        .toast {
            min-height: 75px !important;
        }

        .altura-menu {
            height: 80px !important;
        }

        #burger {
            display: none !important;
        }

        .logo-japy-menu {
            height: 60px !important;
        }

        #dropdown-login li:nth-child(n+3):nth-child(-n+8) {
            display: block;
        }
    }

    @media only screen and (min-width: 768px) {
        #dropdown-login li:nth-child(n+3):nth-child(-n+8) {
            display: block;
        }
    }

    @media only screen and (max-width: 992px) {
        .redes-sociales li {
            display: inline-block;
            margin: 0px;
            margin-right: 20px;
            float: right;
        }

        div.wrapper-redes-sociales {
            width: 200px;
        }

        div.wrapper-menu {
            width: calc(100% - 200px);
        }

        #dropdown-login li:nth-child(n+3):nth-child(-n+8) {
            display: block;
        }
    }
</style>
<script>
    $(document).ready(function() {
        $('ul.left > li, ul.right > li').on('click', function(e) {
            e.preventDefault();
            if ($(this).attr('data-href')) {
                window.location.href = $(this).attr('data-href');
            }
            else {
                const url = $(this).find('a').attr('href');
                window.location.href = url;
            }
        });
    });
</script>
<div class="z-depth-1 nav-style" style=" display: block;position: relative;z-index: 1;border-bottom: 2px solid #0bbcdd;">
    <?php
    $controller = $_SERVER['REQUEST_URI'];
    $controller = strtolower($controller);
    ?>
    <div class="row primary-background no-margin" style="min-height: 1rem">
        <?php if ( ! isset($this->session->userdata()['id_usuario'])) : ?>
            <div class="col s6 m3 offset-m7 l3 offset-l8" style="
            <?php echo strpos($controller, 'registro') || strpos($controller, 'expo') ? "display:none;" : "" ?>">
                <div class="nav-content">
                    <ul id="nav-mobile" style="display: flex;justify-content: space-around;margin: 0;">
                        <li class="hide-on-small-only"><a href="<?php echo base_url("home/planeador_bodas") ?>"
                                                          class="waves-effect waves-light btn btn-sec">CON&Oacute;CENOS</a></li>
                        <li class="hide-on-small-only"><a href="<?php echo base_url("home/altaEmpresas") ?>"
                                                          class="waves-effect waves-light btn btn-sec">EMPRESA</a></li>
                    </ul>
                </div>
            </div>
        <?php endif ?>
    </div>
    <div class="row max-content logo-japy">
        <div style="position: relative;z-index: 100;" class="col m9">
            <div class="row no-margin center-vertical">
                <div class="col s1 hide-on-large-only">
                    <a href="#" data-activates="mobile-demo" class="button-collapse">
                        <i class="material-icons primary-text">menu</i>
                    </a>
                </div>
                <div class="col s4 l2 m4 offset-s3 logo-japy-menu center-align">
                    <a href="<?php echo base_url() ?>">
                        <img src="<?php echo base_url() ?>dist/img/japy_copy.png"
                             alt="Japy"/>
                    </a>
                </div>
                <div class="menu-user col s2 offset-s1  m7 l10">
                    <nav class="title nav-style" style="background: transparent!important">
                        <div class="nav-wrapper">
                            <div style="">
                                <!-- MENU  -->
                                <ul class="center hide-on-med-and-down" style="">
                                    <?php if ( ! $this->checker->isModerador()) { ?>
                                        <li><a class="dropdown-button  text-style lett-space"
                                               href="<?php echo base_url('novia') ?>"
                                               data-activates='dropdown-miboda' data-hover="true"
                                               data-constrainwidth="false" data-beloworigin="true">Mi Boda</a></li>
                                    <?php } ?>
                                    <li><a class="dropdown-button  text-style lett-space"
                                           href="<?php echo base_url()."proveedores" ?>"
                                           data-activates='dropdown-proveedores' data-hover="true"
                                           data-constrainwidth="false" data-beloworigin="true">Proveedores</a></li>
                                    <li style="display:none;"><a class="dropdown-button white-text"
                                                                 href="<?php echo base_url("tendencia/index/") ?>"
                                                                 data-activates='dropdown-vestidos' data-hover="true"
                                                                 data-constrainwidth="false" data-beloworigin="true">VESTIDOS</a>
                                    </li>
                                    <li style="display:none;"><a class="white-text" href="#">BODA DESTINO</a></li>
                                    <li style="display:none;"><a class="white-text" href="#">CONECTA CON EXPERTOS</a>
                                    </li>
                                    <li><a class="text-style lett-space" href="<?php echo base_url('blog') ?>">Para t&iacute;</a>
                                    <li><a class="text-style lett-space"
                                           href="<?php echo base_url() ?>home/expo_eventos">Eventos</a>
                                    </li>
                                    <li><a class="text-style lett-space"
                                           href="<?php echo base_url() ?>brideweekend"><img src="<?php echo base_url() ?>dist/img/logo_bw_japy.png"></a>
                                    </li>
                                    <li style="display:none;"><a class="text-style lett-space"
                                           href="<?php echo base_url() ?>promociones">Promociones</a>
                                    </li>
                                    <li style"width: 10px;

height: 10px;

float: left;

color: white;

cursor: pointer;

margin-left: 7vh;"><a style="width: 10px;

height: 10px;

color: white !important;

cursor: unset;

font-size: xx-small;" class="text-style lett-space"
                                           href="<?php echo base_url() ?>inspiracion">Inspiracion</a>
                                    </li>
                                    <!--<li><a class="dorado-2-text" href="<?php echo base_url(
                                        'galery'
                                    ) ?>">GALERIA</a></li>-->
                                </ul>
                                <!-- menu desplegable m&oacute;vil  -->
                                <ul class="side-nav" id="mobile-demo" style="color:#00bcdd!important;">
                                    <li>
                                        <ul class="collapsible collapsible-accordion">
                                            <li>
                                                
                                                <?php if (!empty($this->checker->isLogin()) && $this->checker->isLogin() == 1) : ?>
                                                    <a class="collapsible-header waves-effect waves-light category item-side">Mi Boda</a>
                                                    <div class="collapsible-body" style="color:#00bcdd!important">
                                                        <ul class="submenu-one">
                                                            <li><a href="<?php echo base_url("novios/buzon") ?>">Mi
                                                                    buzon</a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url("novios/presupuesto") ?>">Presupuesto
                                                                    Inteligente</a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url("Novia") ?>">Mi Perfil</a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url("novios/tarea") ?>">Mi
                                                                    Agenda</a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url("novios/proveedor") ?>">Mis
                                                                    Proveedores</a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url("novios/invitados") ?>">Mis
                                                                    Invitados</a>
                                                            </li>
                                                            <li style="display: none;">
                                                                <a href="<?php echo base_url("novios/miscalificaciones") ?>">
                                                                    Calificaciones</a>
                                                            </li>
                                                            <li style="display:none;">
                                                                <a href="<?php echo base_url("novios/misvestidos") ?>">Mis
                                                                    vestidos</a>
                                                            </li>

                                                            <li style="display:none;">
                                                                <a href="<?php echo base_url("novios/mesa") ?>">Control
                                                                    de Mesas</a>
                                                            </li>
                                                            <li style="display:none;">
                                                                <a href="">Comunidades Novia</a>
                                                            </li>
                                                            <li style="display:none;">
                                                                <a href="<?php echo base_url("novios/miweb") ?>">Mi
                                                                    Portal Web</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                <?php else : ?>
                                                    <a class="" style="color:#00bcdd!important;"
                                                       href="<?php echo base_url("novia") ?>">Mi Boda</a>
                                                <?php endif; ?>
                                            </li>
                                            <li>
                                                <a class="collapsible-header waves-effect waves-light item-side primary-text">Proveedores</a>
                                                <div class="collapsible-body" style="color:#f4d266 !important">
                                                    <ul class="collapsible submenu-one" data-collapsible="accordion">
                                                        <?php foreach (categories() as $category) : ?>
                                                            <li class=" item-side">
                                                                <a href="#<?php //echo base_url("mx-$category->slug") ?>" class=" collapsible-header waves-effect waves-light">
                                                                    <?php echo $category->name ?>
                                                                </a>
                                                                <div class="divider"></div>
                                                                <div class="collapsible-body">
                                                                    <ul class="collapsible submenu-two" data-collapsible="accordion">
                                                                        <?php foreach ($category->subcategories as $subcategory) : ?>
                                                                            <li>
                                                                                <a href="<?php echo base_url("mx-$subcategory->slug") ?>">
                                                                                    <?php echo $subcategory->name; ?>
                                                                                </a>
                                                                                <div class="divider"></div>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li style="display:none;">
                                                <a href="<?php echo base_url("tendencia/index") ?>"
                                                   class="collapsible-header waves-effect waves-light"
                                                   style="color:#00bcdd!important">VESTIDOS</a>
                                                <div class="collapsible-body">
                                                    <ul>
                                                        <?php foreach (productos() as $key => $p) { ?>
                                                            <li>
                                                                <a href="<?php echo base_url(
                                                                    "tendencia/index/".str_replace(" ", "-",
                                                                        $p->nombre)
                                                                ) ?>"
                                                                   class="sub-menu-item">
                                                                    <?php echo $p->nombre ?>
                                                                </a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li style="display:none;"><a class="" style="color:#00bcdd!important"
                                                                         href="#">BODA DESTINO</a></li>
                                            <li style="display:none;">
                                                <a class="" style="color:#f4d266!important" href="#">CONTACTA CON
                                                    EXPERTOS</a>
                                                <!--<a class="collapsible-header waves-effect waves-light" style="color:#f4d266!important">CONTACTA CON EXPERTOS</a>
                                                <div class="collapsible-body">
                                                    <ul>
                                                        <li><a  href="<?php echo base_url(
                                                    'proveedores/categoria/hacienda-para-bodas'
                                                ) ?>"  class="sub-menu-item" >Hacienda para bodas</a><li>
                                                        <li><a  href="<?php echo base_url(
                                                    'proveedores/categoria/restaurantes-para-bodas'
                                                ) ?>"  class="sub-menu-item" >Restaurantes para boda</a><li>
                                                        <li><a href="<?php echo base_url(
                                                    'proveedores/categoria/catering-para-bodas'
                                                ) ?>"  class="sub-menu-item" >Catering para bodas</a><li>
                                                        <li><a href="<?php echo base_url(
                                                    'proveedores/categoria/jardines-para-bodas'
                                                ) ?>"  class="sub-menu-item" >Jardines para bodas</a><li>
                                                        <li><a href="<?php echo base_url(
                                                    'proveedores/categoria/hoteles-para-bodas'
                                                ) ?>"  class="sub-menu-item" >Hoteles para bodas</a><li>
                                                        <li><a href="<?php echo base_url(
                                                    'proveedores/categoria/Salones-para-bodas'
                                                ) ?>"  class="sub-menu-item" >Salones para bodas</a><li>
                                                        <li><a href="<?php echo base_url(
                                                    'proveedores/sector/banquetes?q=playa'
                                                ) ?>"  class="sub-menu-item" >Bodas en la playa</a><li>
                                                        <li><a href="<?php echo base_url(
                                                    'proveedores/sector/banquetes?promociones=si'
                                                ) ?>" class="sub-menu-item promocion" > <i class="fa fa-tag" style="line-height: 40px"></i> Promociones</a><li>
                                                    </ul>
                                                </div>-->
                                            </li>
                                            <li><a href="<?php echo base_url('blog') ?>" class="primary-text">Para t&iacute;</a></li>
                                            <li><a class="primary-text" href="<?php echo base_url() ?>home/expo_eventos">Eventos</a></li>
                                            <li><a class="primary-text" href="<?php echo base_url() ?>promociones">Promociones</a></li>
                                            <li><a href="<?php echo base_url('blog') ?>" class="primary-text">Con&oacute;cenos</a></li>
                                            <li><a href="<?php echo base_url("home/altaEmpresas") ?>" class="primary-text">Empresa</a></li>
                                            <li><a class="primary-text" href="<?php echo base_url() ?>brideweekend">Bride Weekend</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>

        <?php if ($this->checker->isLogin()) { ?>
            <!-- <div class="col s12 m5 altura-menu l4"> -->
            <div id="inicio_registro" class="" style="position: relative;z-index: 90;">
                <div>
                    <div class="row clickable dropdown-button waves-effect user-information center-vertical" data-beloworigin="true"
                         data-activates='dropdown-login' style="margin: 0;height: 11vh;display: flex;">
                        <div class="col m5  s5  l5 " style="text-align: right;width: 80px;">
                            <img class="circule-img"
                                 src="<?php echo base_url('perfil/foto/') ?>/<?php echo $this->session->userdata("id_usuario") ?>"
                                 alt=""/>
                        </div>
                        <div class="col m7 s6 l5">
                            <h6 class="primary-text" style="font-weight: bold;margin: 0;">
                                <?php echo $this->session->userdata("nombre") ?>
                            </h6>
                            <p style="font-size: 12px;margin: 0;" class="primary-text"><?php echo $this->session->userdata("genero") ?></p>
                        </div>
                        <i id="burger" class="material-icons primary-text hide-on-med-and-down"
                           style="position: absolute; top: 40%;display: block;right: 6px;">menu</i>
                        <div class="col m1 s1" style="position: relative">
                        </div>
                    </div>
                    <ul id='dropdown-login' class='dropdown-content'
                        style="z-index:30000000 !important;overflow-x: hidden;min-width: 305px;">
                        <?php if ($this->checker->isNovio()) { ?>
                            <li><a class="grey-text darken-5" href="<?php echo base_url('novia') ?>">
                                    <i class="material-icons left primary-text">dashboard</i> Mi organizador<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                            </li>
                            <li class=""
                                style="background: #514f50!important; color: white;    padding: 14px 16px;">
                                <div class="row" style="margin: 0px">
                                    <div class="col s4 align-center">
                                        <div class="align-center">
                                            <i class="material-icons center primary-text">check_circle</i>&nbsp;&nbsp;<b><?php echo $this->checker->getTareasCompletadas() ?></b>
                                        </div>
                                        <small><b>Tareas completadas</b></small>
                                    </div>
                                    <div class="col s4 align-center">
                                        <div class="align-center">
                                            <i class="material-icons center primary-text">people_outline</i>&nbsp;&nbsp;<b><?php echo $this->checker->getInvitadosConfirmados() ?></b>
                                        </div>
                                        <small><b>Invitados confirmados</b></small>
                                    </div>
                                    <div class="col s4 align-center">
                                        <div class="align-center">
                                            <i class="material-icons center primary-text">favorite_border</i>&nbsp;&nbsp;<b><?php echo $this->checker->getProveedoresReservados() ?></b>
                                        </div>
                                        <small><b>Proveedores reservados</b></small>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/buzon') ?>">
                                    <i class="material-icons left primary-text">mail_outline</i>
                                    Mi buz&oacute;n<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/presupuesto') ?>">
                                    <i class="material-icons left primary-text">exposure</i> Mi
                                    presupuesto<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('Novia') ?>">
                                    <i class="material-icons left primary-text">face</i>
                                    Mi perfil
                                    <i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/tarea') ?>">
                                    <i class="material-icons left primary-text">content_paste</i>
                                    Mi Agenda<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/proveedor') ?>">
                                    <i class="material-icons left primary-text"
                                    >class</i> Mis proveedores<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/invitados') ?>">
                                    <i class="material-icons left primary-text"
                                    >people_outline</i> Mis invitados<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <!--<li>
                                    <a class="grey-text darken-5" href="<?php /*echo base_url("novios/misvestidos") */ ?>">
                                        <i class="material-icons left primary-text" >wc</i> Mis
                                        vestidos<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>-->
                            <!--<li>
                                    <a class="grey-text darken-5" href="<?php /*echo base_url('novios/miweb') */ ?>">
                                        <i class="material-icons left" style="color:#f4d266!important">web</i> Mi web de
                                        boda<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>-->
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/perfil') ?>">
                                    <i class="material-icons left primary-text">settings</i> Mi
                                    cuenta<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li class="divider"></li>
                        <?php } else {
                            if ($this->checker->isAdmin()) { ?>
                                <li><a class="grey-text darken-5" href="<?php echo base_url('App') ?>">
                                        <i class="material-icons left" style="color:#f4d266!important">dashboard</i>
                                        Panel de administraci&oacute;n<i
                                                class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                </li>
                            <?php } else {
                                if ($this->checker->isProveedor()) { ?>
                                    <li><a class="grey-text darken-5" href="<?php echo base_url('proveedor') ?>">
                                            <i class="material-icons left"
                                               style="color:#f4d266!important">dashboard</i>
                                            Panel de Proveedor<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                    </li>
                                <?php } else {
                                    if ($this->checker->isModerador()) { ?>
                                        <li><a class="grey-text darken-5"
                                               href="<?php echo base_url('novios/moderador') ?>">
                                                <i class="material-icons left"
                                                   style="color:#f4d266!important">web</i> Comunidad<i
                                                        class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                        </li>
                                    <?php }
                                }
                            }
                        } ?>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo base_url()."cuenta/logout" ?>">
                                <i class="material-icons left primary-text">close</i>
                                Cerrar Sesi&oacute;n
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        <?php } ?>
        <div id="inicio_registro" style="position: relative;z-index: 90;" class="" style="padding: 0px">
            <?php if ( ! isset($this->session->userdata()['id_usuario'])) : ?>
                <nav class="nav-extended " style="margin:auto;background-color: transparent;box-shadow: none;
                    <?php echo strpos($controller, 'registro') || strpos($controller, 'expo') ? "display:none;" : "" ?>">
                    <div class="nav-wrapper">
                        <ul id="nav-mobile" class=" center-vertical alt-botones" style="justify-content: flex-end;height: 11vh;">
                            <li style=""><a href="<?php echo base_url()."cuenta" ?>" class="waves-effect waves-light btn btn-custom">Iniciar Sesi&oacute;n</a>
                            </li>
                            <li style=""><a href="<?php echo base_url()."registro" ?>" class="waves-effect waves-light btn btn-custom">Registrarse</a></li>
                        </ul>
                    </div>
                </nav>
            <?php endif ?>

            <?php

            function typePosition($controller)
            {
                echo ($controller == "/clubnupcial/" || $controller == "/clubnupcial/" || $controller == "/clubnupcial/" || $controller == "/clubnupcial/home" || $controller == "/clubnupcial/home/") ? "position:absolute;" : "position:relative;";
            }

            setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');

            function categories()
            {
                return Category::with('subcategories')->main()->get();
            }

            ?>
        </div>
    </div>

    <div id='dropdown1' class='dropdown-content sub-menu' style="">
        <div>
            <div class="row">
                <div class="col s12 m6">
                    <a href="<?php echo base_url('proveedores/categoria/hacienda-para-bodas') ?>" class="sub-menu-item">Hacienda
                        para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo base_url('proveedores/categoria/restaurantes-para-bodas') ?>"
                       class="sub-menu-item">Restaurantes para boda</a>
                    <div class="divider"></div>
                    <a href="<?php echo base_url('proveedores/categoria/catering-para-bodas') ?>" class="sub-menu-item">Catering
                        para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo base_url('proveedores/categoria/jardines-para-bodas') ?>" class="sub-menu-item">Jardines
                        para bodas</a>
                </div>
                <div class="col s12 m6">
                    <a href="<?php echo base_url('proveedores/categoria/hoteles-para-bodas') ?>" class="sub-menu-item">Hoteles
                        para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo base_url('proveedores/categoria/Salones-para-bodas') ?>" class="sub-menu-item">Salones
                        para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo base_url('proveedores/sector/banquetes?q=playa') ?>" class="sub-menu-item">Bodas
                        en la playa</a>
                    <div class="divider"></div>
                    <a href="<?php echo base_url('proveedores/sector/banquetes?promociones=si') ?>"
                       class="sub-menu-item promocion" style="background: #f4d266!important; color: #514f50"> <i
                                class="fa fa-tag" style="line-height: 40px"></i> Promociones</a>
                </div>
            </div>
        </div>
    </div>

    <?php if ( ! $this->checker->isModerador()) { ?>
        <div id='dropdown-miboda' class='dropdown-content sub-menu' style="">
            <div>
                <?php if ($this->checker->isLogin()) { ?>
                    <div class="row" style="margin-bottom: -5px">
                        <div class="col s12 m6">
                            <a href="<?php echo base_url('novios/presupuesto') ?>" class="sub-menu-item">Mi
                                Presupuestador</a>
                            <div class="divider"></div>
                            <a href="<?php echo base_url('novios/tarea') ?>" class="sub-menu-item">Mi Agenda</a>
                            <div class="divider"></div>
                            <a class="sub-menu-item " href="<?php echo base_url('novios/proveedor') ?>">Mis
                                Proveedores</a>
                            <div class="divider"></div>
                            <a href="<?php echo base_url('novios/invitados') ?>" class="sub-menu-item">Mis Invitados</a>
                            <div class="divider"></div>
                            <a class="sub-menu-item" href="<?php echo base_url('novios/mesa') ?>">Mi Organizador de
                                Mesas</a>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row" style="margin-bottom: -5px">
                        <div class="col s12 m8">
                            <a href="<?php echo base_url('cuenta') ?>" class="sub-menu-item">Mi Presupuestador</a>
                            <div class="divider"></div>
                            <a href="<?php echo base_url('cuenta') ?>" class="sub-menu-item">Mi Agenda</a>
                            <div class="divider"></div>
                            <a class="sub-menu-item " href="<?php echo base_url('cuenta') ?>">Mis Proveedores</a>
                            <div class="divider"></div>
                            <a href="<?php echo base_url('cuenta') ?>" class="sub-menu-item">Mis Invitados</a>
                            <div class="divider"></div>
                            <a class="sub-menu-item" href="<?php echo base_url('cuenta') ?>">Mi Organizador de Mesas</a>
                        </div>

                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <div id='dropdown-proveedores' class='dropdown-content sub-menu'
         style="overflow-x:visible; overflow-y:-webkit-paged-y !important;">
        <div class="row">
            <?php foreach (Category::with('subcategories')->orderBy('orden')->whereNull('parent_id')->get() as $key => $category): ?>
                <?php if ($key == 0): ?>
                    <div class="col s12 m6">
                <?php endif; ?>
                <a href="<?php echo base_url()."mx-$category->slug"; ?>"
                   class="sub-menu-item" style="z-index: 100000000">
                    <?php echo $category->name; ?>
                    <ul class="left">
                        <?php foreach ($category->subcategories as $subcategory) : ?>
                            <li data-href="<?php echo base_url()."mx-$subcategory->slug" ?>"
                                class="sub-sub-menu-item"><?php echo $subcategory->name ?></li>
                        <?php endforeach; ?>
                    </ul>
                </a>
                <div class="divider"></div>
                <?php if ($key == 12): ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>

    <div id='dropdown-vestidos' class='dropdown-content sub-menu' style="width: 650px;">
        <?php
        $tipos_vestido = $this->tipo_vestido->getAll();
        ?>
        <div>
            <div class="row">
                <div class="col s12 m3">
                    <?php foreach ($tipos_vestido as $key => $p) { ?>
                        <a onmouseover="$('.tipo-vestido').hide();$('.tipo-<?php echo $p->id_tipo_vestido ?>').show();"
                           href="<?php echo base_url("tendencia/index/".str_replace(" ", "-", $p->nombre)) ?>"
                           class="sub-menu-item">
                            <?php echo $p->nombre ?>
                        </a>
                        <div class="divider"></div>
                    <?php } ?>
                </div>
                <?php foreach ($tipos_vestido as $key => $p) { ?>
                    <?php $p->vestidos = $this->vestido->getDestacadosMin($p->id_tipo_vestido, 3); ?>
                    <div class="col s12 m9 tipo-vestido tipo-<?php echo $p->id_tipo_vestido ?>" <?php echo $p->id_tipo_vestido == 1 ? "" : 'style="display:none"' ?> >
                        <h6>Dise���adores Destacados</h6>
                        <div class="divider"></div>
                        <?php foreach ($p->vestidos as $key => $value) { ?>
                            <div class="col s4">
                                <div class="card-panel truncate" style="padding: 5px;">
                                    <a style="width: 100%;padding: 0px;margin: 0px;display: block"
                                       href="<?php echo base_url("tendencia/catalogo/".str_replace(" ", "-",
                                               $p->nombre)."?disenador=".str_replace(" ", "-",
                                               $value->disenador)) ?>">
                                        <div class="responsive-img"
                                             style="background-image: url(<?php echo base_url(
                                                 "tendencia/imagen_vestido/$value->id_vestido"
                                             ) ?>) ">
                                        </div>
                                    </a>
                                    <?php echo $value->disenador ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col s12">
                            <a class="primary-text "
                               href="<?php echo base_url("tendencia/index/".str_replace(" ", "-", $p->nombre)) ?>"
                               style="margin-top: -15px;margin-bottom: 20px;">
                                Todos los dise���os de <?php echo $p->nombre ?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>