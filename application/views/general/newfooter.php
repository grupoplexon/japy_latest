<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!--<script src="<?php //echo base_url() ?>dist/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="<?php //echo base_url() ?>dist/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php //echo base_url() ?>dist/js/datatables.min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url() ?>dist/js/materialize.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.Jcrop.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/classie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/slider.min.js" type="text/javascript"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="<?php echo base_url() ?>dist/js/inputFormat.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.touchSwipe.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/facebook.min.js" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
<link href="<?php echo base_url() ?>dist/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo base_url() ?>dist/enjoyhint/enjoyhint.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/tutorial.min.js"></script>
<script src="<?php echo base_url() ?>dist/slider-pro-master/dist/js/jquery.sliderPro.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/parsley.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/i18n/es.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Leo-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/clndr/1.4.7/clndr.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/datepicker-es.js"></script>
<!-- -->

</script>

</script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
</script>
<script>
    $(document).ready(function() {
        if (typeof onReady != 'undefined') {
            onReady();
        }
        $('select').material_select();
        $('.button-collapse').sideNav();
        $('.dropdown-button').dropdown();
        $('.modal').modal();
        $('.collapsible').collapsible({
            accordion: false,
        });

        $('.multiple-items').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            dots: false,
            autoplaySpeed: 5000,
            arrows: false,
            adaptiveHeight: true,
        });

        $('.center-slide').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            arrow: false,
            dots: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3,
                    },
                },
                {
                    breakpoint: 440,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                    },
                },
            ],
        });

        $('#slider-home').sliderPro({
            width: '100%',
            autoHeight: true,
            buttons: false,
        });
    });

    function generoImage(edad, sexo) {
        switch (parseInt(sexo)) {
            case 1:
                //HOMBRE
                switch (parseInt(edad)) {
                    case 1:
                        //ADULTO
                        return 'invitados invitados-Hombre x2';
                        break;
                    case 2:
                        //NINO
                        return 'invitados invitados-Nino x2';
                        break;
                    case 3:
                        //BEBE
                        return 'invitados invitados-Bebe-nino x2';
                        break;
                }
                break;
            case 2:
                //MUJER
                switch (parseInt(edad)) {
                    case 1:
                        //ADULTO
                        return 'invitados invitados-Mujer x2';
                        break;
                    case 2:
                        //NINO
                        return 'invitados invitados-Nina x2';
                        break;
                    case 3:
                        //BEBE
                        return 'invitados invitados-Bebe-nina x2';
                        break;
                }
                break;
        }
    }
</script>