<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
<noscript id="deferred-styles-2">
    <!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons|Raleway:300,400,500,600,700" rel="stylesheet">-->
    <!--<link href="https://fonts.googleapis.com/css?family=Comfortaa:400,700,900" rel="stylesheet">-->
    <!--<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">-->
    <!--<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">-->
    <!--<link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">-->
    <!--<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:700" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/slick/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/slick/slick-theme.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/src/parsley.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/general_styles.css">
    <link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</noscript>

<!-- WORKAROUND FOR NOW, THIS IS FOR THE HOME PAGE THAT USES OTHER VERSIONS OF MATERIALIZE AND FONTAWESOME -->
<?php if (base_url() == current_url()) : ?>
    <link type="text/css" rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.1/css/materialize.min.css"
          media="screen,projection"/>
<?php endif ?>

<title>BrideAdvisor</title>

<!-- Facebook Pixel Code -->
<script>
    !function(f, b, e, v, n, t, s) {
        if (f.fbq) {
            return;
        }
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments);
        };
        if (!f._fbq) {
            f._fbq = n;
        }
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s);

        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById('deferred-styles-2');
            var replacement = document.createElement('div');
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement);
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        if (raf) {
            raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        }
        else {
            window.addEventListener('load', loadDeferredStyles);
        }

    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2323895250959097');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2323895250959097&ev=PageView&noscript=1
https://www.facebook.com/tr?id=2323895250959097&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->



