<?php
$this->load->view("general/head");
$url_img     = $this->config->base_url()."/blog/resource/imagen/";
$url_default = $this->config->base_url()."/dist/img/blog/default.png";
?>
<head>
    <style>
        .searchHome {
            background: #00bcdd;
            margin: 1em 0 0 0;
            padding: 0 10px;
        }

        .place {
            margin: 0 auto 0 0;
        }

        .placeMobile {
            margin: 0 auto 0 auto;
        }

        .main {
            background-image: url('<?php echo base_url() ?>/dist/img/newbackground.png');
            background-size: cover;
            background-position: center;
        }

        .slide_item {
            max-width: 100%;
            height: auto;
        }

        .textura{
            background: url('<?php echo base_url()?>dist/img/slider_home/textura.png');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/slider-pro-master/dist/css/slider-pro.min.css"/>
</head>

<div class="main max-content" style="padding-bottom: 2rem;">

    <div class="slider-pro" id="slider-home">
        <div class="sp-slides">
            <!-- Slide 1 -->
            <div class="sp-slide">
                <img class="slide_item" src="<?php echo base_url() ?>dist/img/slider_home/1slider2000.png"
                     data-src="<?php echo base_url() ?>dist/img/slider_home/1slider2000.png"
                     data-small="<?php echo base_url() ?>dist/img/slider_home/1slider420.png"
                     data-medium="<?php echo base_url() ?>dist/img/slider_home/1slider768.png"
                     data-large="<?php echo base_url() ?>dist/img/slider_home/1slider2000.png"
                     alt="">
            </div>

            <!-- Slide 2 -->
            <div class="sp-slide">
                <img class="slide_item" src="<?php echo base_url() ?>dist/img/slider_home/2slider2000.png"
                     data-src="<?php echo base_url() ?>dist/img/slider_home/2slider2000.png"
                     data-small="<?php echo base_url() ?>dist/img/slider_home/2slider420.png"
                     data-medium="<?php echo base_url() ?>dist/img/slider_home/2slider768.png"
                     data-large="<?php echo base_url() ?>dist/img/slider_home/2slider2000.png"
                     alt="">
            </div>

            <!-- Slide 3 -->
            <div class="sp-slide">
                <img class="slide_item" src="<?php echo base_url() ?>dist/img/slider_home/3slider2000.png"
                     data-src="<?php echo base_url() ?>dist/img/slider_home/3slider2000.png"
                     data-small="<?php echo base_url() ?>dist/img/slider_home/3slider420.png"
                     data-medium="<?php echo base_url() ?>dist/img/slider_home/3slider768.png"
                     data-large="<?php echo base_url() ?>dist/img/slider_home/3slider2000.png"
                     alt="">
            </div>
        </div>
    </div>

    <div class="searchHome">
        <div class="textura">
            <?php $this->view("searchBar") ?>
        </div>
    </div>

    <section class="category-post row-impar hide-on-small-only" style="margin-top: 40px;">
        <div class="title-section">
            <h3>Articulos Recientes</h3>
        </div>
        <div class="row">
            <div class="col m10 offset-m1 ">
                <div class="row">
                    <?php foreach ($posts as $post) : ?>
                        <div class="col ccc"
                             style="background: url('<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>');background-position: center;
                                 background-repeat: no-repeat;
                                 background-size: cover;">
                            <div class="tag">
                                <h4><?php echo strip_tags($post->titulo) ?></h4>
                                <div class="content-info">
                                    <?php echo strip_tags(substr($post->contenido, 0, 150)) ?> ...
                                </div>
                                <a href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>">Leer
                                    m&aacute;s</a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="category-post hide-on-med-and-up" style="margin: 50px 10px 0px 10px">
        <div class="title-section">
            <h3>Articulos Recientes</h3>
        </div>
        <div class="row">
            <div class="col s12 ">
                <div class="center-slide">

                    <?php foreach ($posts as $post) : ?>
                        <a href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>"
                           style="visibility: visible;display: block;padding: 0 15px">
                            <div class="card">
                                <div class="card-image slide-img"
                                     style="background-position: center;background-repeat: no-repeat;background-size: cover;">
                                    <img src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>">
                                    <span class="card-title slide-title"><?php echo strip_tags($post->titulo) ?></span>
                                </div>
                                <div class="card-content">
                                    <div class="content-info slide-info">
                                        <?php echo strip_tags(substr($post->contenido, 0, 150)) ?> ...
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>

                </div>
            </div>
    </section>

    <section class=" hide-on-small-only" style="margin-bottom: 0">
        <div class="title-section">
            <h3>Proveedores Destacados</h3>
        </div>
        <div class="row ">
            <div class="col m10 offset-m1 ">
                <div class="row multiple-items">
                    <?php foreach ($providers as $provider) : ?>

                        <?php if ($provider->imagePrincipal->count()) {
                            $providerImageUrl = base_url()."uploads/images/".$provider->imagePrincipal->first()->nombre;
                            ?>
                        <?php } else {
                            $providerImageUrl = base_url()."dist/img/slider1.png";
                            ?>
                        <?php } ?>
                        <div class="provider-parent" style="margin: 0 20px;">
                            <div class="provider-post"
                                 style="background: url('<?php echo($providerImageUrl) ?>');
                                     background-position: center;
                                     background-repeat: no-repeat;
                                     background-size: cover;
                                     ">
                            </div>
                            <div class="tag-provider">
                                <h4><?php echo $provider->nombre ?></h4>
                                <div class="content-info">
                                    <?php echo strip_tags(substr($provider->descripcion, 0, 150)) ?> ...
                                </div>
                                <a href="<?php echo base_url() ?>boda-<?php echo $provider->slug ?>">Leer
                                    m&aacute;s</a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="category-post hide-on-med-and-up" style="margin-bottom: 0">
        <div class="title-section">
            <h3>Proveedores Destacados</h3>
        </div>
        <div class="row">
            <div class="col s12 ">

                <div id="slide-provider" class="center-slide">
                    <?php foreach ($providers as $provider) : ?>
                        <?php if ($provider->imagePrincipal->count()) {
                            $providerImageUrl = base_url()."uploads/images/".$provider->imagePrincipal->first()->nombre;
                            ?>
                        <?php } else {
                            $providerImageUrl = base_url()."dist/img/slider1.png";
                            ?>
                        <?php } ?>

                        <a href="<?php echo base_url() ?>boda-<?php echo $provider->slug ?>"
                           style="visibility: visible;display: block;padding: 0 15px;">
                            <div class="card">
                                <div class="card-image slide-img"
                                     style="background-position: center;background-repeat: no-repeat;background-size: cover;">
                                    <img src="<?php echo($providerImageUrl) ?>">
                                    <span class="card-title  slide-title"><?php echo $provider->nombre ?></span>
                                </div>
                                <div class="card-content">
                                    <div class="content-info slide-info">
                                        <?php echo strip_tags(substr($provider->descripcion, 0, 150)) ?> ...
                                    </div>
                                </div>
                            </div>
                        </a>

                    <?php endforeach; ?>
                </div>
            </div>
    </section>

</div>

<?php
$this->load->view("general/footer", $this->_ci_cached_vars);
$this->load->view("general/newfooter");
?>

