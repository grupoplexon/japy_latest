<?php
$this->load->view("general/head");
$url_img     = $this->config->base_url()."/blog/resource/imagen/";
$url_default = $this->config->base_url()."/dist/img/blog/default.png";
?>
<head>
    <style>
        .searchHome {
            background: #00bcdd;
            margin: 1em 0 0 0;
            padding: 0 10px;
        }

        .place {
            margin: 0 auto 0 0;
        }

        .placeMobile {
            margin: 0 auto 0 auto;
        }

        .main {
            background-image: url('<?php echo base_url() ?>/dist/img/fondo_home.sv');
            background-size: cover;
            background-position: center;
        }

        .slide_item {
            max-width: 100%;
            height: auto;
        }

    </style>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/slider-pro-master/dist/css/slider-pro.min.css"/>
</head>

<div class="main max-content" style="padding-bottom: 2rem;">

    <div class="slider-pro" id="slider-home">
        <div class="sp-slides">
            <!-- Slide 1 -->
            <div class="sp-slide">
                <img class="slide_item" src="<?php echo base_url() ?>dist/img/slider_home/slider2000x400 expos_JAPY2.png"
                     data-src="<?php echo base_url() ?>dist/img/slider_home/slider2000x400 expos_JAPY2.png"
                     data-small="<?php echo base_url() ?>dist/img/slider_home/slider420 expos_JAPY2.png"
                     data-medium="<?php echo base_url() ?>dist/img/slider_home/slider768 expos_JAPY2.png"
                     data-large="<?php echo base_url() ?>dist/img/slider_home/slider2000x400 expos_JAPY2.png"
                     alt=""
                     style="object-fit: cover">
            </div>
            <!-- <div class="sp-slide">
                <img class="slide_item" src="<?php echo base_url() ?>dist/img/slider_home/slider-1-2000.jpg"
                     data-src="<?php echo base_url() ?>dist/img/slider_home/slider-1-2000.jpg"
                     data-small="<?php echo base_url() ?>dist/img/slider_home/slider-1-420.jpg"
                     data-medium="<?php echo base_url() ?>dist/img/slider_home/slider-1-768.jpg"
                     data-large="<?php echo base_url() ?>dist/img/slider_home/slider-1-2000.jpg"
                     alt=""
                     style="object-fit: cover">
            </div> -->

            <!-- Slide 4 -->
            <div class="sp-slide">
                <img class="slide_item" src="<?php echo base_url() ?>dist/img/slider_home/slider2000x400 expos_JAPY.png"
                     data-src="<?php echo base_url() ?>dist/img/slider_home/slider2000x400 expos_JAPY.png"
                     data-small="<?php echo base_url() ?>dist/img/slider_home/slider420 expos_JAPY.png"
                     data-medium="<?php echo base_url() ?>dist/img/slider_home/slider768 expos_JAPY.png"
                     data-large="<?php echo base_url() ?>dist/img/slider_home/slider2000x400 expos_JAPY.png"
                     alt=""
                     style="object-fit: cover">
            </div>

            <!-- Slide 2 -->
            <div class="sp-slide">
                <img class="slide_item" src="<?php echo base_url() ?>dist/img/slider_home/slider-2-2000.jpg"
                     data-src="<?php echo base_url() ?>dist/img/slider_home/slider-2-2000.jpg"
                     data-small="<?php echo base_url() ?>dist/img/slider_home/slider-2-420.jpg"
                     data-medium="<?php echo base_url() ?>dist/img/slider_home/slider-2-768.jpg"
                     data-large="<?php echo base_url() ?>dist/img/slider_home/slider-2-2000.jpg"
                     alt=""
                     style="object-fit: cover">
            </div>

            <!-- Slide 3 -->
            <div class="sp-slide">
                <img class="slide_item" src="<?php echo base_url() ?>dist/img/slider_home/slider-3-2000.jpg"
                     data-src="<?php echo base_url() ?>dist/img/slider_home/slider-3-2000.jpg"
                     data-small="<?php echo base_url() ?>dist/img/slider_home/slider-3-420.jpg"
                     data-medium="<?php echo base_url() ?>dist/img/slider_home/slider-3-768.jpg"
                     data-large="<?php echo base_url() ?>dist/img/slider_home/slider-3-2000.jpg"
                     alt=""
                     style="object-fit: cover">
            </div>

        </div>
    </div>

    <div class="searchHome row no-margin">
        <div class="textura col s12 m10 offset-m1">
            <?php $this->view("searchBar") ?>
        </div>
    </div>

    <section style="background:url('<?php echo base_url() ?>dist/img/fondo1.svg');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            padding: 3rem 0 5rem 0;">
        <div class="title-section">
            <h3>Artículos Recientes</h3>
        </div>
        <div class="content-data" style="justify-content: space-around!important;align-items: baseline !important;">
            <?php foreach ($posts as $post) : ?>
                <div class="element-all">
                    <div class="content-data">
                        <img class="element-img"
                             src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada
                                 : $url_default ?>" alt="" style="">
                        <h4 class="element-name truncate"><?php echo strip_tags($post->titulo) ?></h4>
                        <div class="label-img" style="">
                            <img src="<?php echo base_url() ?>dist/img/e1.svg" alt="">
                            <img src="<?php echo base_url() ?>dist/img/e2.svg" alt="">
                        </div>
                    </div>
                    <div class="content-data">
                        <p class="center-align" style="width: 70%;"><?php echo substr(strip_tags($post->contenido), 0,
                                150) ?>... </p>
                    </div>
                    <div class=" center-align" style="margin-bottom: 2rem">
                        <a href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>"
                           class="btn-custom" style="padding: .5rem 1.5rem!important">Leer m&aacute;s</a>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </section>

    <div style="width: 100%;background-color: #F0F0F0;height: 1rem"></div>

    <section style="background:url('<?php echo base_url() ?>dist/img/fondo2.svg');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            padding: 3rem 0 5rem 0;">

        <div class="title-section">
            <h3>Proveedores Recientes</h3>
        </div>
        <div class="content-data multiple-item"
             style="justify-content: space-around!important;align-items: baseline !important;">
            <?php foreach ($providers as $provider) : ?>

                <?php if ($provider->imagePrincipal->count()) {
                    $providerImageUrl = base_url()."uploads/images/".$provider->imagePrincipal->first()->nombre;
                    ?>
                <?php } else {
                    $providerImageUrl = base_url()."dist/img/slider1.png";
                    ?>
                <?php } ?>
                <div class="element-all">
                    <div class="content-data">
                        <img class="element-img" src="<?php echo($providerImageUrl) ?>" alt="">
                        <h4 class="element-name truncate"><?php echo $provider->nombre ?></h4>
                        <div class="label-img" style="">
                            <img src="<?php echo base_url() ?>dist/img/e1.svg" alt="">
                            <img src="<?php echo base_url() ?>dist/img/e2.svg" alt="">
                        </div>
                    </div>
                    <div class="content-data">
                        <p class="center-align" style="width: 70%;"><?php echo strip_tags(substr($provider->descripcion,
                                0, 150)) ?>... </p>
                    </div>
                    <div class=" center-align" style="margin-bottom: 2rem">
                        <a href="<?php echo base_url() ?>boda-<?php echo $provider->slug ?>" class="btn-custom"
                           style="padding: .5rem 1.5rem!important">Leer m&aacute;s</a>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </section>

</div>

<?php
$this->load->view("general/footer", $this->_ci_cached_vars);
$this->load->view("general/newfooter");
?>

