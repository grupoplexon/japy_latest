<html>
    <head>
        <?php $this->view("japy/header"); $this->view("general/head"); ?>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.22/dist/vue.js"></script>
        <!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.5.22"></script> -->
        <!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>  -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- <script src="https://unpkg.com/vue"></script> -->
        

        <!-- Compiled and minified JavaScript -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script> -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        
    </head>
    <style>
        ::-webkit-input-placeholder {
            color: #a3a3a3;
            opacity: 1 !important; /* for older chrome versions. may no longer apply. */
        }
        .model{
            position: absolute !important;
        }

        :-moz-placeholder { /* Firefox 18- */
            color: #a3a3a3;
            opacity: 1 !important;
        }

        ::-moz-placeholder {  /* Firefox 19+ */
            color: #a3a3a3;
            opacity: 1 !important;
        }

        :-ms-input-placeholder {  
        color: #a3a3a3;
        }
        
        .card .card-title {
        font-size: unset !important;
        font-weight: unset !important;
        }

        .ui-autocomplete{
            z-index: 9999 !important;
            background-color: #f0e9e9 !important;
            width: 30% !important;
        }

        .ui-helper-hidden-accessible { display:none; }

        .modal {
    position: fixed !important;
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    max-width: 800px !important;
}
.modalImg {
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
}
.modalHead {
    padding-top: 10px !important;
    padding-right: 10px !important;
    padding-bottom: 10px !important;
    padding-left: 10px !important;
}

.tags{
    margin:0;
    padding:0;
    right:24px;
    bottom:-12px;
    list-style:none;
}
.tags li, 
.tags a{
    float:left;
    height:24px;
    line-height:24px;
    position:relative;
    font-size:11px;
    margin-block-end: 5px;
}
.tags a{
    margin-left:20px;
    padding:0 10px 0 12px;
    background:#0089e0;
    color:#fff;
    text-decoration:none;
    -moz-border-radius-bottomright:4px;
    -webkit-border-bottom-right-radius:4px; 
    border-bottom-right-radius:4px;
    -moz-border-radius-topright:4px;
    -webkit-border-top-right-radius:4px;    
    border-top-right-radius:4px;    
}
 
.tags a:before{
    content:"";
    float:left;
    position:absolute;
    top:0;
    left:-12px;
    width:0;
    height:0;
    border-color:transparent #0089e0 transparent transparent;
    border-style:solid;
    border-width:12px 12px 12px 0;      
}

.tags a:hover{
    background:#555;
}   
.tags a:hover:before{
    border-color:transparent #555 transparent transparent;
}

.title1{
    color: #8dd7e0;
    margin: 0 auto;
    font-weight: bold;
    font-size: 2rem !important;
    font-family: 'Raleway', Sans-Serif;
    text-shadow: rgb(0, 0, 0) -2px 2px 0px, rgb(0, 0, 0) -2px 2px 0px;
}

.card-image img {

max-height: unset !important;
height: 33%;

}
    
    </style>
    <body>     
        <div id="app" class="row" style="background-color: #f6f6f6;">        
            <div class="container">

            <div class="row">
                <div class="nav-wrapper">
                    <div class="center">
                        <div id="topbarsearch" class="col s11 m11 l11">
                            <div class="input-field blue-text" style="color: #8dd7e0 !important;">
                                <h3>¿Necesitas inspiracion para tu boda?</h3>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
            <div class="row">
                <nav class="white transparent" style="border-radius: 3em; background-color: white !important;">
                    <div class="nav-wrapper">
                        <div class="center">
                            <div id="topbarsearch" class="col s11 m11 l11">
                                <div class="input-field blue-text" >
                                    <i class="blue-text material-icons prefix">search</i>
                                    <input id="buscador" v-model="search" type="text" placeholder="Buscar...">
                                </div>
                            </div>
                        </div>          
                    </div>

                    <!-- <div class="input-field">
                        <input class="purple darken-4 autocomplete" id="omschrijving" type="search">
                        <label for="autocomplete-input"><i class="material-icons">search</i></label>
                        <i class="material-icons">close</i>
                    </div> -->

                </nav> 
            </div>
            <div v-if="search.length == 0"> <!-- START-IF -->
            <div id="recientes" class="row">
                <div class="col s12 m12 l12">
                    <span class="" style="font-family: 'Arial';"><i class="small material-icons" style="float: left;color: #8dd7e0;">beenhere</i><strong style="font-weight: bold;color: dimgray;font-size: larger;">Recientes</strong></span>
                </div>
                <div class="col s12 m6 l6" style="cursor: pointer;" v-for="inspiration in inspirations_new" @mouseenter="inspiration.hover = true" @mouseleave="inspiration.hover = false" v-on:click="getInspiration(inspiration.id_inspiracion, 1)">
                    <div class="row" style="margin: 0px;">
                        <div class="card">
                            <div class="card-image">
                            <img style="object-fit: cover;" v-bind:src="base_url + 'uploads/images/' + inspiration.nombre">
                            <span class="card-title" style="width: -moz-available; font-size: 2em; margin: auto;padding-bottom: inherit;" >
                                <p class="title1" style="
                                margin: auto;font-size: large;">{{ inspiration.titulo }}</p>
                                <p style="
                                color: #FFF;
                                margin: 0 auto;
                                font: 11px condensed 120% sans-serif;
                                text-shadow: rgb(0, 0, 0) -.8px .8px 0px, rgb(0, 0, 0) -1.3px 1.3px 0px;
                                font-size: 11px; margin: auto;">{{inspiration.descripcion}}</p>
                                <hr style="margin: auto;"/>
                                <i class="fa fa-eye"  style="color: white;font-size: large;">{{inspiration.vistas != null ? inspiration.vistas : 0}}</i>
                                <i class="fa fa-commenting"  style="color: white;font-size: large;">{{inspiration.comments != null ? inspiration.comments : 0}}</i>
                                <!-- <a class="right" style="
                                    background-color: rgb(254, 0, 0);
                                    cursor: pointer;
                                    border: medium none;
                                    color: white;
                                    padding: 0px 21px;
                                    text-align: center;
                                    text-decoration: none;
                                    display: inline-block;
                                    border-radius: 1em;
                                    font-size: 13px;" v-show="inspiration.hover">
                                    Guardar</a> -->
                                
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div id="recomendados" class="row">
                <div class="col s12 m12 l12">
                <span class="" style="font-family: 'Arial';"><i class="small material-icons" style="float: left;color: #8dd7e0;">beenhere</i><strong style="font-weight: bold;color: dimgray;font-size: larger;">Recomendados</strong></span>
                </div>
                <div class="col s12 m6 l6" style="cursor: pointer;" v-for="inspiration in inspirations_last" @mouseenter="inspiration.hover = true" @mouseleave="inspiration.hover = false" v-on:click="getInspiration(inspiration.id_inspiracion, 1)">
                    <div class="row" style="margin: 0px;">
                        <div class="card">
                            <div class="card-image">
                            <img style="object-fit: cover;" v-bind:src="base_url + 'uploads/images/' + inspiration.nombre">
                            <span class="card-title" style="width: -moz-available; font-size: 2em; margin: auto;padding-bottom: inherit;" >
                            <p class="title1" style="
                                margin: auto;font-size: large;">{{ inspiration.titulo }}</p>
                                <p style="
                                color: #FFF;
                                margin: 0 auto;
                                font: 11px condensed 120% sans-serif;
                                text-shadow: rgb(0, 0, 0) -.8px .8px 0px, rgb(0, 0, 0) -1.3px 1.3px 0px;
                                font-size: 11px; margin: auto;">{{inspiration.descripcion}}</p>
                                <hr style="margin: auto;"/>
                                <i class="fa fa-eye"  style="color: white;font-size: large;">{{inspiration.vistas != null ? inspiration.vistas : 0}}</i>
                                <i class="fa fa-commenting"  style="color: white;font-size: large;">{{inspiration.comments != null ? inspiration.comments : 0}}</i>
                                <!-- <a class="right" style="
                                    background-color: rgb(254, 0, 0);
                                    cursor: pointer;
                                    border: medium none;
                                    color: white;
                                    padding: 0px 21px;
                                    text-align: center;
                                    text-decoration: none;
                                    display: inline-block;
                                    border-radius: 1em;
                                    font-size: 13px;" v-show="inspiration.hover">
                                    Guardar</a> -->
                                
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div> <!-- ENDIF -->
            <div v-else id="busqueda" class="row">
                <div class="col s12 m12 l12">
                <span class="" style="font-family: 'Arial';"><i class="small material-icons" style="float: left;color: #8dd7e0;">beenhere</i><strong style="font-weight: bold;color: dimgray;font-size: larger;">Busqueda</strong></span>
                </div>
                <!-- <div class="row center" style="height: 28em;"><h4><strong>BUSCANDO...</strong></h4></div> -->
                <br>
                <div v-if="loading">
                <br>
                <div id="loading_search" class="progress">
                    <div class="indeterminate"></div>
                </div>
                <div><h4>{{nofound}}</h4></div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                </div>
                <div v-else class="col s12 m6 l6" style="cursor: pointer;" v-for="inspiration in inspirations_search" @mouseenter="inspiration.hover = true" @mouseleave="inspiration.hover = false" v-on:click="getInspiration(inspiration.id_inspiracion, 1)">
                    <div class="row" style="margin: 0px;">
                        <div class="card">
                            <div class="card-image">
                            <img style="object-fit: cover;" v-bind:src="base_url + 'uploads/images/' + inspiration.nombre">
                            <span class="card-title" style="width: -moz-available; font-size: 2em; margin: auto;padding-bottom: inherit;" >
                            <p class="title1" style="
                                margin: auto;font-size: large;">{{ inspiration.titulo }}</p>
                                <p style="
                                color: #FFF;
                                margin: 0 auto;
                                font: 11px condensed 120% sans-serif;
                                text-shadow: rgb(0, 0, 0) -.8px .8px 0px, rgb(0, 0, 0) -1.3px 1.3px 0px;
                                font-size: 11px; margin: auto;">{{inspiration.descripcion}}</p>
                                <hr style="margin: auto;"/>
                                <i class="fa fa-eye"  style="color: white;font-size: large;">{{inspiration.vistas != null ? inspiration.vistas : 0}}</i>
                                <i class="fa fa-commenting"  style="color: white;font-size: large;">{{inspiration.comments != null ? inspiration.comments : 0}}</i>
                                <!-- <a class="right" style="
                                    background-color: rgb(254, 0, 0);
                                    cursor: pointer;
                                    border: medium none;
                                    color: white;
                                    padding: 0px 21px;
                                    text-align: center;
                                    text-decoration: none;
                                    display: inline-block;
                                    border-radius: 1em;
                                    font-size: 13px;" v-show="inspiration.hover">
                                    Guardar</a> -->
                                
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Structure -->
            <!-- height: 287px;
                height: 19em; -->
            <div id="modal1" class="modal" style="overflow-x: hidden;">
            

                <!-- <div class="col m6 s6 l4 modalImg"style="
                display: flex;
                align-items: center;
                max-width: 100%;
                overflow: hidden;">
                    <img id="imgInfo" class="img-fluid" style="">
                </div> -->

                <!-- <div class="col m6 s6 l4 modalImg"style="
                        max-width: 100%;
                        height: auto;
                        width: auto\9;
            ">
                    <img id="imgInfo" class="img-fluid" style="">
                </div> -->
                    

                    <!-- <div class="doc">
                    <div class="box">
                        <img id="imgInfo" >
                    </div>
                    </div> -->

                <div class="col m6 s12 l4 modalImg">
                    <div class="doc" style="    
                        flex-flow: column wrap;
                        width: 100%;
                        height: auto;
                        justify-content: center;
                        align-items: center;
                        background: rgb(51, 57, 68) none repeat scroll 0% 0%;
                        margin: 1em;
                        ">
                        <div class="box" style="
                       width: 100%;
                        height: auto;
                        overflow: hidden;
                        ">
                        <img id="imgInfo" style="
                        object-fit: cover;
                        object-position: center center;
                        height: 290px;
                        width: 250px;
                        " >
                        </div>
                    </div>
                    <!-- <img id="imgInfo" style="margin:5px; width: 100% !important; object-fit: fill !important;"> -->
                </div>



                <div class="col m6 s6 l8">
                
                    <div class="col m12 s12 l12">
                    <div class="modal-header">
                        <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                        style="float: right !important;
                        position: absolute !important;
                        padding-left: 58% !important;">X</button>
                    </div>
                        <div class="col m3 s12 l2 modalHead">
                            <img class="circule-img" style="background: white" id="logoProvider">
                        </div>
                        <div class="col m9 s12 l10 modalImg">
                            <h6 style="font-weight: bold; color: black;" id="nomproInfo"></h6>
                            <h6 id="titleInfo" style="color: black; font-size: 20px;"></h6>
                            <h6 id="fechaInfo" style="color: black;"></h6>
                        </div>
                    </div>
                    <div class="col m12 s12 l12">
                    <!-- <button style="float: right;margin: 1em;">Guardar</button> -->
                    
                    <!-- <button class="" style="float: right;margin: .5em;" type="submit" name="action">Guardar
                        <i class="material-icons right">send</i>
                    </button> -->
        
                        <p align="justify" id="descInfo" style="color: black;"></p>
                        <!-- <div id="divTags">
                        </div> -->
                        <ul class="tags col l12 m12 s12" id="divTags">
                            <li id="divTags">

                            </li>
                        </ul>
                        <div id="divComent">
                        </div>
                        <br>
                        <div class="col m10">
                            <?php if($this->session->userdata("id_usuario")!=null) { ?>
                                <input type="text" placeholder="Escribe un comentario" id="coment">
                            <?php } else { ?>
                                <p style="color: red">Inicia sesión para poder comentar</p>
                            <?php } ?>
                        </div>
                        <?php if($this->session->userdata("id_usuario")!=null) { ?>
                            <button class="btn fa fa-arrow-right" v-on:click="reloadModal()"></button>
                        <?php } ?>
                        <div class="col m12 s12">
                        <hr>
                            <i class="fa fa-eye" aria-hidden="true" style="color: black;"></i><strong style="color: black;" id="vistasInfo"></strong>
                            <i class="fa fa-commenting" aria-hidden="true" style="color: black;"></i><strong style="color: black;" id="contComent"></strong>
                        <hr>
                        </div>
                    </div>
                </div>
                <button class="" style="float: right;margin: .5em;" type="submit" name="action" v-on:click="saveInspiration()">Guardar
                    <i class="material-icons right">send</i>
                </button>
            </div>

            </div>  <!-- endContainer -->
        </div>  <!-- endAppVue -->
                  
    </body>
    <script>
    
    var app = new Vue({
        el: '#app',
        data: {
            id: "",
            id_user: "",
            base_url: "",
            loading: true,
            nofound: "",
            tags: [],
            active: [],
            message: 'Hello Vue!',
            search: "",
            inspirations_new:[
                {title: 'Prueba Title', hover: false},
            ],
            inspirations_last:[
                {title: 'Prueba Title', hover: false},
            ],
            inspirations_search:[
                {title: 'Prueba Title', hover: false},
            ],
        },
        methods: {
            saveInspiration: function(){
                if(this.id_user > 0 && this.id >0){
                    axios.get(this.base_url+'inspiracion/saveInspirations', {
                        params: {
                        id_inspiracion: this.id,
                        id_usuario: this.id_user
                        }
                    })
                    .then( (response) => {
                        swal('Bien!', 'Inspiracion Guardada', 'success');
                    })
                    .catch(function (error) {
                    });
                }else{
                    swal('¡Atención!', 'Tienes que iniciar sesion', 'warning');
                }
            },
            reloadModal: function() {
                var c = $("#coment").val();
                var id_us = '<?= $this->session->userdata("id_usuario")? $this->session->userdata("id_usuario") : ""; ?>';
                var nombre = '<?= $this->session->userdata("nombre")? $this->session->userdata("nombre") : ""; ?>';
                // alert("id= "+this.id+" coment= "+c+" id user= "+id_user+" nombre= "+nombre);
                if(c!=null && c!='') {
                    axios.get(this.base_url+'inspiracion/saveComent', {
                        params: {
                            id: this.id,
                            coment: c,
                            id_user: id_us,
                            name: nombre
                        }
                    })
                        .then( (response) => {
                            // console.log(response);
                            if(response.data.validar!='coment') {
                                $("#coment").val("");
                                this.getInspiration(this.id, 0);
                            } else {
                                $("#coment").val("");
                                swal('¡Atención!', 'Solo puede poner un comentario por inspiración', 'warning');
                            }
                        })
                        .catch(function (error) {
                        });
                } else {
                    swal('¡Error!', 'Escriba su comentario', 'error');
                }
            },
            getStart: function(){
                axios.get(this.base_url+'inspiracion/start')
                    .then( (response) => {
                        this.inspirations_new = response.data.new;
                        this.inspirations_last = response.data.last;
                    })
                    .catch(function (error) {
                    });
            },
            getTags: function(){

                $( "#buscador" ).autocomplete({
                    source: this.base_url+'inspiracion/tags',
                    select: (event, ui) => {
                        this.search = ui.item.value;
                        this.getInspirations();
                    },
                    messages: {
                        noResults: '',
                        results: function() {}
                    }
                });

            },
            getInspirations: function(){
                axios.get(this.base_url+'inspiracion/getInspirations', {
                        params: {
                        search: this.search
                        }
                    })
                    .then( (response) => {
                        this.inspirations_search = response.data;
                        if(this.inspirations_search.length > 0)
                            {this.loading = false;
                            this.nofound = "";}
                        else
                            {this.loading = true;
                            this.nofound = "Ningun Resultado.";}   
                    })
                    .catch(function (error) {
                    });
            },
            getInspiration: function(id, ban){
                this.id=id;
                $('#modal1').openModal();
                $.ajax({
                    url: '<?php echo base_url() ?>'+'inspiracion/modalInfo',
                    method: 'post',
                    data: {
                        id_ins: id,
                        views: ban
                    },
                    success: function(response) {
                        var ruta = '<?php echo base_url() ?>'+'/uploads/images/'+response.inspiracion.nombre;
                        document.getElementById("imgInfo").src = ruta;
                        var ruta2 = '<?php echo base_url() ?>'+'uploads/images/'+response.fotoInfo;
                        document.getElementById("logoProvider").src = ruta2;
                        document.getElementById("nomproInfo").innerHTML = response.nomproInfo;
                        document.getElementById("titleInfo").innerHTML = response.inspiracion.titulo;
                        let date = new Date(response.inspiracion.updated_at.replace(/-+/g, '/'));
                        let options = {
                            year: 'numeric',
                            month: 'long',
                            day: 'numeric'
                        };
                        document.getElementById("fechaInfo").innerHTML = date.toLocaleDateString('es-MX', options);
                        document.getElementById("descInfo").innerHTML = response.inspiracion.descripcion;
                        if(response.inspiracion.vistas!=null) {
                            document.getElementById("vistasInfo").innerHTML = '&nbsp;'+response.inspiracion.vistas+'&nbsp;&nbsp;';
                        } else {
                            document.getElementById("vistasInfo").innerHTML = '&nbsp;'+0+'&nbsp;&nbsp;';
                        }
                        var tamaño = response.comentarios.length;
                        document.getElementById("contComent").innerHTML = '&nbsp;'+response.comentarios.length;
                        var i;
                        var comentario="";
                        var comentario2="";
                        for (i = 0; i < tamaño; i++) {
                            var nombre = '<strong style="font-weight: bold; color: black;">'; 
                            nombre += response.comentarios[i]['names']+': '; 
                            nombre += '</strong>'; 

                            var texto = '<strong style="color: black;">'; 
                            texto += response.comentarios[i]['texto']; 
                            texto += '</strong>'; 
                            
                            var salto = '<br>'
                            comentario = comentario + (nombre+texto)+salto;
                        }
                        var tamaño2 = response.tags.length;
                        var x=1;
                        for (i=0; i < tamaño2; i++) {
                            var tag = '<a style="font-weight: bold; color: white; font-size: 13px;">'; 
                            tag += response.tags[i]['tags']; 
                            tag += '</a>';
                            comentario2 = comentario2 + tag;
                        }
                        document.getElementById("divComent").innerHTML = comentario;
                        document.getElementById("divTags").innerHTML = comentario2;
                    },
                    error: function(e) {
                        console.log("ERROR");
                    },
                });
            }
        },
        mounted: function () {
            $('.modal-trigger').leanModal();
            // $('.modal-trigger').leanModal({
            //     dismissible: true,
            //     show: true,
            //     keyboard: false,
            //     backdrop: 'static'
            // });
            // $('#modal1').closeModal({ dismissible: true});
            this.base_url = '<?= base_url()? base_url() : ""; ?>';
            this.id_user = '<?= $this->session->userdata("id_usuario")? $this->session->userdata("id_usuario") : ""; ?>';
            this.getStart();
            this.getTags();
            
        }
    })
    </script>
    <!--Footer-->
    <?php $this->view("general/footer") ?>    
    
</html>