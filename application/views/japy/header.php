<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
    <link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-40486092-9');
    </script>
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2323895250959097');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=2323895250959097&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    
    <link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!--<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.1/css/materialize.min.css"  media="screen,projection"/>-->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script> -->

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="<?= base_url() ?>dist/js/newLanding_japy/sweetalert2.js" type="text/javascript"></script>

    <link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <title>BrideAdvisor</title>
</head>
<style>
/* p{
    font-family: "Roboto", sans-serif;
} */
strong{
    font-weight: bold;
}
body{
    font-family: 'Raleway', sans-serif !important;
    color: #757575;
}
nav{
    background-color: white;
    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
    -webkit-box-shadow: 0px 0px 0px rgba(0, 0, 0, 0); 
    -moz-box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
}
nav ul a, a{
    font-size: 14px !important;
    color: #848484 !important;
}
nav.nav-center ul {
    text-align: center;
}
nav.nav-center ul li {
    display: inline;
    float: none;
}
nav.nav-center ul li a {
    display: inline-block;
}
#header{
    height: 85px;
    max-height: 85px;
    width: 100%;
    position: relative !important;
    z-index:999;
}
.img-logo{
    height: 85px;
    vertical-align: middle;
}
.nav-wrapper .button-collapse{
    text-align: right;
}
nav .button-collapse i {
    height: 70px;
    line-height: 70px;
}
.img-movil{
        display: none;
}
h5 a{
    font-size: 4vh !important;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px){
    .img-movil{
        display: block;
        height: 70px;
        width: 80%;
        margin-top: -50px;
    }
}
@media only screen and  (min-width: 300px) and  (max-width: 1000px){
    #menu-user{
        display:none;
    }
    .nav-wrapper{
        width: 85% !important;
    }
}
<?php if ($this->checker->isLogin()) : ?>
/* @media only screen and  (min-width: 975px) and  (max-width: 1134px){
    .nav-wrapper{
        width: 100% !important;
    }
    #menu_user{
        right: -30px !important;
    }
    #menu_ajust{
        width: 85% !important;
        float: left !important;
    }
} */
@media only screen and  (min-width: 975px) and  (max-width: 2500px){
    .nav-wrapper{
        /* width: 100% !important; */
    }
    #menu_user{
        right: -30px !important;
    }
    #menu_ajust{
        /* width: 85% !important; */
        /* width: 100% !important; */
        float: left !important;
    }
}
@media only screen and  (min-width: 975px) and  (max-width: 1180px){
    #menu_ajust{
        width: 85% !important;
        /* width: 100% !important; */
        /* float: left !important; */
    }
}
@media only screen and  (min-width: 1190px) and  (max-width: 2180px){
    #menu_ajust{
        /* width: 85% !important; */
        width: 100% !important;
        /* float: left !important; */
    }
}
<?php endif; ?>

a {
    font-weight: bold;
}
.side-nav {
    position: fixed;
    width: 250px;
}
.drag-target{
    display: none !important;
}
</style>
<div id="header">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <div class="naxbar-fixed">
    <nav class="nav-center" role="navigation">
    <!-- width: 79vw; -->
    <?php if ($this->checker->isLogin()) : ?>
        <div id="menu_ajust" class="nav-wrapper" style="margin: auto;width: 65%;">
    <?php else : ?>
        <div class="nav-wrapper" style="margin: auto;width: 90%;">
    <?php endif; ?>
        
        <a href="#" data-activates="mobile-demo" class="button-collapse right" ><i class="material-icons" style="
    font-size: 3rem; float: right !important; color:#6dc9d2">menu</i></a>
        <a href="<?php echo base_url() ?>" class="" style="
         float: left !important;" >
            <img class="img-logo img-movil" style="padding-left: 10px;" src="<?php echo base_url() ?>dist/img/japy_copy.png" alt=""> </a>
        <ul class="hide-on-med-and-down right" style="margin: auto;width: 100%;">
            <!-- <li>
                <a class="dropdown-button" data-activates='dropdown1'>
                Mi Boda</a>
                <ul id='dropdown1' class='dropdown-content'>
                    <li><a href="#!">one</a></li>
                    <li><a href="#!">two</a></li>
                    <li class="divider"></li>
                    <li><a href="#!">three</a></li>
                    <li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
                    <li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
                </ul>
            </li> -->
            <li><a href="<?php echo base_url('novia') ?>">Mi Boda</a></li>
            <li><a href="<?php echo base_url()."proveedores" ?>">Proveedores</a></li>
            <li><a href="<?php echo base_url('magazine') ?>">Magazine</a></li>
            <li><a href="<?php echo base_url() ?>home/expo_eventos">Expos</a></li>
            <li class="first-logo">
                <a href="<?php echo base_url() ?>" class="" >
                    <img class="img-logo" src="<?php echo base_url() ?>dist/img/japy_copy.png" alt="">
                </a>
            </li>
            <li><a href="<?php echo base_url() ?>brideweekend">BrideWeekend</a></li>
            <li><a href="<?php echo base_url("home/planeador_bodas") ?>">Conócenos</a></li>
            <li><a href="<?php echo base_url("home/altaEmpresas") ?>">Empresa</a></li>
            <?php if (!$this->checker->isLogin()) : ?>
            <li><a href="<?php echo base_url()."cuenta" ?>">Inicia Sesión</a></li>
            <?php endif; ?>
            <li>
                
            </li>
        </ul>
        <ul id="mobile-demo" class="side-nav">
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion" style="display: inline-grid;">
                    <li><a href="<?php echo base_url('novia') ?>">Mi Boda</a></li>
                    <li><a href="<?php echo base_url()."proveedores" ?>">Proveedores</a></li>
                    <li><a href="<?php echo base_url('magazine') ?>">Magazine</a></li>
                    <li><a href="<?php echo base_url() ?>home/expo_eventos">Eventos</a></li>
                    <li class="first-logo">
                        <a href="<?php echo base_url() ?>" class="" >
                            <img class="img-logo" src="<?php echo base_url() ?>dist/img/japy_copy.png" alt="">
                        </a>
                    </li>
                    <li><a href="<?php echo base_url() ?>brideweekend">BrideWeekend</a></li>
                    <li><a href="<?php echo base_url("home/planeador_bodas") ?>">Conócenos</a></li>
                    <li><a href="<?php echo base_url("home/altaEmpresas") ?>">Empresa</a></li>
                    <li><a href="<?php echo base_url()."cuenta" ?>">Inicia Sesión</a></li>
                </ul>
            </li>
        </ul>
        </div>
    </nav>
    </div>
    <div id="menu-user" class="col m5" style="position:absolute;top:1.4vh;right:0;margin-right: 3vh;">
        <?php if ($this->checker->isLogin()) : ?>
            <div class="col s12 m10" style="margin-bottom: -35px;float: right">
                <div class="row clickable dropdown-button waves-effect user-information" data-beloworigin="true"
                    data-activates='dropdown-login'>
                    <div class="col m6 offset-m2 s5" style="text-align: right;width: 80px;">
                        <img class="circule-img"
                            src="<?php echo site_url('perfil/foto/') ?>/<?php echo $this->session->userdata("id_usuario") ?>"
                            alt=""/>
                    </div>
                    <div class="col m4 s6" style=" padding-left: 0px;width: calc( 70% - 80px )">
                        <h6 class="primary-text" style="font-weight: bold;margin-top: 9px;margin-bottom: -5px;">
                            <?php echo $this->session->userdata("nombre") ?>
                        </h6>
                        <!-- <p style="font-size: 12px;"
                        class="primary-text"><?php echo $this->session->userdata("genero") ?></p> -->
                    </div>
                    <!-- <i class="material-icons primary-text"
                    style="position: absolute; top: 10px;display: block;right: 6px;line-height: 1.4;">menu</i> -->
                    <div class="col m1 s1" style="position: relative">
                    </div>
                </div>
                <ul id='dropdown-login' class='dropdown-content'
                    style="overflow-x: hidden;min-width: 305px;">
                    <?php if ($this->checker->isNovio()) { ?>
                        <li><a class="grey-text darken-5" href="<?php echo site_url('novia') ?>">
                                <i class="material-icons left primary-text">dashboard</i> Mi organizador<i
                                        class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/buzon') ?>">
                                <i class="material-icons left primary-text">mail_outline</i>
                                Mi buz&oacute;n<i
                                        class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/presupuesto') ?>">
                                <i class="material-icons left primary-text">exposure</i> Mi
                                presupuesto<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('Novia') ?>">
                                <i class="material-icons left primary-text">face</i>
                                Mi perfil
                                <i class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/tarea') ?>">
                                <i class="material-icons left primary-text">content_paste</i>
                                Mi Agenda<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/proveedor') ?>">
                                <i class="material-icons left primary-text"
                                >class</i> Mis proveedores<i
                                        class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/invitados') ?>">
                                <i class="material-icons left primary-text"
                                >people_outline</i> Mis invitados<i
                                        class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/perfil') ?>">
                                <i class="material-icons left primary-text">settings</i> Mi
                                cuenta<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li class="divider"></li>
                    <?php } else {
                        if ($this->checker->isAdmin()) { ?>
                            <li><a class="grey-text darken-5" href="<?php echo site_url('App') ?>">
                                    <i class="material-icons left" style="color:#f4d266!important">dashboard</i>
                                    Panel de administraci&oacute;n<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                            </li>
                        <?php } else {
                            if ($this->checker->isProveedor()) { ?>
                                <li><a class="grey-text darken-5" href="<?php echo site_url('proveedor') ?>">
                                        <i class="material-icons left primary-text">dashboard</i>
                                        Panel de Proveedor<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                </li>
                            <?php } else {
                                if ($this->checker->isModerador()) { ?>
                                    <li><a class="grey-text darken-5"
                                        href="<?php echo site_url('novios/moderador') ?>">
                                            <i class="material-icons left"
                                            style="color:#f4d266!important">web</i> Comunidad<i
                                                    class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                    </li>
                                <?php }
                            }
                        }
                    } ?>
                    <li>
                        <a class="grey-text darken-5" href="<?php echo site_url('cuenta/logout') ?>">
                            <i class="material-icons left primary-text">close</i>
                            Cerrar Sesi&oacute;n
                        </a>
                    </li>
                </ul>
            </div>
        <?php else : ?>
            <div class="col s12 m10" style="margin-bottom: -35px;float: right">
                
            </div>
        <?php endif; ?>
    </div>

</div>

<?php

            function typePosition($controller)
            {
                echo ($controller == "/clubnupcial/" || $controller == "/clubnupcial/" || $controller == "/clubnupcial/" || $controller == "/clubnupcial/home" || $controller == "/clubnupcial/home/") ? "position:absolute;" : "position:relative;";
            }

            setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');

            function categories()
            {
                return Category::with('subcategories')->main()->get();
            }

            ?>

<script>
$(document).ready(function(){
    $(".button-collapse").sideNav(); 
});
</script>