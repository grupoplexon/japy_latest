<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
    <link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
    </script>
    <link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">

    <link href="<?php echo base_url() ?>dist/slick/slick.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>dist/slick/slick-theme.css" rel="stylesheet" type="text/css"/>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.1/css/materialize.min.css"  media="screen,projection"/> -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>


    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="<?= base_url() ?>dist/js/newLanding_japy/sweetalert2.js" type="text/javascript"></script>

    <link href="<?php echo base_url() ?>dist/css/fontawesome-5.8.1/css/all.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>dist/css/brideAdvisor/style_brideadvisor.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <title>BrideAdvisor</title>

</head>
<style>
strong{
    font-weight: bold;
}
body{
    font-family: 'Raleway', sans-serif !important;
    color: #757575;
}

nav ul a, a{
    font-size: 14px !important;
}

#header{
    height: 85px;
    max-height: 85px;
    width: 100%;
    position: absolute !important;
    z-index:999;
}
.img-logo{
    height: 55px;
    vertical-align: middle;
}
.nav-wrapper .button-collapse{
    text-align: right;
}
nav .button-collapse i {
    height: 70px;
    line-height: 70px;
}
.img-movil{
        display: none;
}
h5 a{
    font-size: 4vh !important;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px){
    .img-movil{
        display: block;
    }
}
@media only screen and  (min-width: 300px) and  (max-width: 1000px){
    .nav-wrapper{
        width: 100% !important;
        height: 70px !important
    }
   
}

</style>
<div id="header">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <div class="naxbar-fixed">
        <nav class="nav-wrapper" role="navigation">       
            <div class="nav-wrapper" style="margin: auto;width: 90%;"> 
                <a href="#" data-activates="mobile-demo" class="button-collapse right button-movil" ><i class="material-icons">menu</i></a>
                <a href="<?php echo base_url() ?>" class="" style="" >
                    <img class="img-logo img-movil brand-logo" src="<?php echo base_url() ?>dist/img/BrideAdvisor.png" alt=""> </a>
                <a href="<?php echo base_url()."cuenta" ?>" class=" right button-movil2 hide-on-large-only" ><i class="fas fa-user-circle" ></i></a>
                <ul class="left hide-on-med-and-down menuBA" >
                    <li><a href="<?php echo base_url('novia') ?>">MI BODA</a></li>
                    <li><a href="<?php echo base_url()."proveedores/index" ?>">PROVEEDORES</a></li>
                    <li><a href="<?php echo base_url('magazine') ?>">MAGAZINE</a></li>
                    <li><a href="<?php echo base_url() ?>home/expo_eventos">EXPOS</a></li>
                </ul>
                <ul class="right hide-on-med-and-down menuBA-right" >
                    <li><div class="vl2"></div></li>
                    <li><a href="<?php echo base_url() ?>brideweekend"><img src="<?php echo base_url() ?>dist/img/brideweekend/logo.png" alt="" style="height: 7vh; margin-right: 5vh;"></a></li>
                    <?php if (!$this->checker->isLogin()) : ?>
                    <li ><a  href="<?php echo base_url()."cuenta" ?>"><i class="fas fa-user user" ></i></a></li>
                    <li><a href="<?php echo base_url("home/planeador_bodas") ?>">Conócenos</a></li>
                    <?php endif; ?>
                    <?php if ($this->checker->isLogin()) : ?>
                    <li><a style="margin-right: 50px;" href="<?php echo base_url("home/planeador_bodas") ?>">Conócenos</a></li>
                    <?php endif; ?>
                    
                    <li></li>
                    <!-- <li><a href="<?php echo base_url("home/altaEmpresas") ?>">Empresa</a></li> -->
                </ul>
                <ul id="mobile-demo" class="side-nav">
                    <li class="no-padding">
                        <ul class="collapsible collapsible-accordion" style="display: inline-grid;">
                            <li><a class="a-mobile" href="<?php echo base_url('novia') ?>">MI BODA</a></li>
                            <li><a class="a-mobile" href="<?php echo base_url()."proveedores/index" ?>">PROVEEDORES</a></li>
                            <li><a class="a-mobile" href="<?php echo base_url('magazine') ?>">MAGAZINE</a></li>
                            <li><a class="a-mobile" href="<?php echo base_url() ?>home/expo_eventos">EXPOS</a></li>
                            <li><a class="a-mobile" href="<?php echo base_url() ?>brideweekend">BrideWeekend</a></li>
                            <li><a class="a-mobile" href="<?php echo base_url("home/planeador_bodas") ?>">Conócenos</a></li>
                            <li><a class="a-mobile" href="<?php echo base_url("home/altaEmpresas") ?>">Empresa</a></li>
                            <li><a class="a-mobile" href="<?php echo base_url()."cuenta" ?>">Inicia Sesion</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div id="menu-user" class="col m5" style="position:absolute;top:1.4vh;right:0;margin-right: 3vh;">
        <?php if ($this->checker->isLogin()) : ?>
            <div class="col s12 m10" style="margin-bottom: -35px;float: right">
                <div class="row clickable dropdown-button waves-effect user-information" data-beloworigin="true"
                    data-activates='dropdown-login'>
                    <div class="col m6 offset-m2 s5" style="text-align: right;width: 80px; padding-right: 90px;">
                        <img class="circule-img"
                            src="<?php echo site_url('perfil/foto/') ?>/<?php echo $this->session->userdata("id_usuario") ?>"
                            alt=""/>
                    </div>
                    <!-- <div class="col m4 s6" style=" padding-left: 0px;width: calc( 70% - 80px )"> -->
                        <!-- <h6 class="primary-text" style="font-weight: bold;margin-top: 9px;margin-bottom: -5px;">
                            <?php //echo $this->session->userdata("nombre") ?>
                        </h6> -->
                        <!-- <p style="font-size: 12px;"
                        class="primary-text"><?php echo $this->session->userdata("genero") ?></p> -->
                    <!-- </div> -->
                    <i id="burger" class="material-icons primary-text hide-on-med-and-down"
                           style="position: absolute; top: 0%;display: block;right: 0px;">menu</i>
                    <!-- <i class="material-icons primary-text"
                    style="position: absolute; top: 10px;display: block;right: 6px;line-height: 1.4;">menu</i> -->
                    <div class="col m1 s1" style="position: relative">
                    </div>
                </div>
                <ul id='dropdown-login' class='dropdown-content'
                    style="overflow-x: hidden;min-width: 305px;">
                    <?php if ($this->checker->isNovio()) { ?>
                        <li><a class="grey-text darken-5" href="<?php echo site_url('novia') ?>">
                                <i class="material-icons left primary-text">dashboard</i> Mi organizador<i
                                        class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/buzon') ?>">
                                <i class="material-icons left primary-text">mail_outline</i>
                                Mi buz&oacute;n<i
                                        class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/presupuesto') ?>">
                                <i class="material-icons left primary-text">exposure</i> Mi
                                presupuesto<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('Novia') ?>">
                                <i class="material-icons left primary-text">face</i>
                                Mi perfil
                                <i class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/tarea') ?>">
                                <i class="material-icons left primary-text">content_paste</i>
                                Mi Agenda<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/proveedor') ?>">
                                <i class="material-icons left primary-text"
                                >class</i> Mis proveedores<i
                                        class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/invitados') ?>">
                                <i class="material-icons left primary-text"
                                >people_outline</i> Mis invitados<i
                                        class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo site_url('novios/perfil') ?>">
                                <i class="material-icons left primary-text">settings</i> Mi
                                cuenta<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                            </a>
                        </li>
                        <li class="divider"></li>
                    <?php } else {
                        if ($this->checker->isAdmin()) { ?>
                            <li><a class="grey-text darken-5" href="<?php echo site_url('App') ?>">
                                    <i class="material-icons left" style="color:#f4d266!important">dashboard</i>
                                    Panel de administraci&oacute;n<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                            </li>
                        <?php } else {
                            if ($this->checker->isProveedor()) { ?>
                                <li><a class="grey-text darken-5" href="<?php echo site_url('proveedor') ?>">
                                        <i class="material-icons left primary-text">dashboard</i>
                                        Panel de Proveedor<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                </li>
                            <?php } else {
                                if ($this->checker->isModerador()) { ?>
                                    <li><a class="grey-text darken-5"
                                        href="<?php echo site_url('novios/moderador') ?>">
                                            <i class="material-icons left"
                                            style="color:#f4d266!important">web</i> Comunidad<i
                                                    class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                    </li>
                                <?php }
                            }
                        }
                    } ?>
                    <li>
                        <a class="grey-text darken-5" href="<?php echo site_url('cuenta/logout') ?>">
                            <i class="material-icons left primary-text">close</i>
                            Cerrar Sesi&oacute;n
                        </a>
                    </li>
                </ul>
            </div>
        <?php else : ?>
            <div class="col s12 m10" style="margin-bottom: -35px;float: right">
                
            </div>
        <?php endif; ?>
    </div>

</div>

<?php

            function typePosition($controller)
            {
                echo ($controller == "/clubnupcial/" || $controller == "/clubnupcial/" || $controller == "/clubnupcial/" || $controller == "/clubnupcial/home" || $controller == "/clubnupcial/home/") ? "position:absolute;" : "position:relative;";
            }

            setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');

            function categories()
            {
                return Category::with('subcategories')->main()->get();
            }

            ?>

<script>
$(document).ready(function(){
    $(".button-collapse").sideNav(); 
});
</script>