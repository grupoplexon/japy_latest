<!DOCTYPE html>

<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
    </script>
    <meta property="og:title" content="Bride Weekend Puebla" />
    <meta property="og:image" content="https://brideadvisor.mx/uploads/images/bw_new.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:url" content="https://www.brideadvisor.mx/brideweekend" />
    <meta property="og:site_name" content="Bride Weekend Puebla" />
    <meta property="og:description" content="Todo lo que necesitas para tu boda este 24 y 25 de Agosto en Puebla, Centro Expositor los Fuertes. Pasarelas con las últimas tendencias y más de 1000 vestidos de novia en exhibición." />
    <meta property="og:type" content="website" />
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>BrideAdvisor</title>
    <?php $this->view("japy/header"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    

    
</head>
<style>
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.slides{
    height: calc(100vh - 85px) !important;
}
.slider .indicators {
    margin-bottom: -14vh;
    z-index: 9;
}
.articulo-movil .slider .indicators {
    margin-bottom: -5vh;
    z-index: 9;
}
.articulo-movil .slider .slides, .proveedor-movil .slider .slides {
    background-color: transparent;
}

::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: black;
}
::-moz-placeholder { /* Firefox 19+ */
  color: black;
}
:-ms-input-placeholder { /* IE 10+ */
  color: black;
}
:-moz-placeholder { /* Firefox 18- */
  color: black;
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}
.slider .indicators .indicator-item.active {
    background-color: #515151;
}
.card-title a, .card-content a {
    font-size: 16px !important;
    font-weight: bold;
    color: black !important;
}
.card-content .btn a {
    font-size: 14px !important;
    font-weight: bold;
    color: white !important;
    background: #6dc9d2 !important;
}
.card-content .btn {
    background: #6dc9d2 !important;
    border-radius: 1px;
    display: inline-block;
    height: 38px;
    line-height: 38px;
    padding: 0 1.2rem;
}
.card-content p{
    font-size: 14px !important!;
    color: #515151 !important;
}

.card .articulo1{
    position: relative;
    margin: 0;
    background-color: transparent;
}

.card{
    background-color: transparent;
    
}
.pro{
    box-shadow: 0 0 0 0 rgba(0,0,0,0), 0 0 0 0 rgba(0,0,0,0), 0 0 0 0 rgba(0,0,0,0) !important;
}
.articulo-movil, .proveedor-movil{
        display: none;
}

/* .searchHome {
            background: #8dd7e0;
            margin: 1em 0 0 0;
            padding: 0 10px;
        } */
.slider-movil{
    display: none;
}
.slider-web{
    display: block;
}
.slider .slides li img {
    height: 100%;
    width: 100%;
    background-size: cover;
    background-position: center;
}

.slide {
    height: 600px !important;
    touch-action: pan-y;
    -webkit-user-drag: none;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
.slide2 {
    height: 96% !important;
}
.slider .indicators {
    margin-bottom: -3vh !important;
}


@media only screen and  (min-width: 300px) and  (max-width: 500px){
    .slide {
        height: 146px !important;
        touch-action: pan-y;
        -webkit-user-drag: none;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }
    .slide2 {
        height: 90% !important;
    }
    .articulo-movil, .proveedor-movil{
        display: block;
    }
    .articulo, .proveedor{
        display: none;
    }
    .slider-movil{
        display: block;
    }
    .slider-web{
    display: none;
    }
    .proveedor-movil{
        height: 700px;
    }
    .pro-movil{
    height: calc(200vh - 85px) !important;
    }
    h4{
        font-size: 1.8rem;
    }
    #buscador{
        width: 100% !important;
    }
    .swal2-popup .swal2-image {
        height: 280px !important;
    }
    .slider .indicators {
        margin-bottom: -3vh !important;
    }
    .slider2 .indicators {
        margin-bottom: 16vh !important;
    }
    #movil .pro{
        margin: -4rem 0 0 0;
    }
    .card .card-title{
        padding: 40px;
    }
    .card .card-content{
        margin-top: -60px;
    }
    .card .art-movil{
        margin-top: -20px;
    }
    .card .art-movil .card-title {
    margin-bottom: -35px;
    }
    #movil .indicators {
        margin-bottom: 9vh !important;
    }
    .articulo-movil .slider .indicators {
    margin-bottom: 15vh;
    z-index: 9;
    }
    .position {
        align-items: center;
        justify-content: center;
        padding: 4vh;
        width: unset !important;
    }
    .tam-flotante {
        width: 18vh !important;
    }
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}
input::-webkit-input-placeholder {
  color: grey;
}
.position {
    align-items: center;
    justify-content: center;
    padding: 4vh;
    width: 40%;
}

@font-face{
    font-family: MyFont;
    src:url('Gentleman1000.otf') format('opentype');
}

.flotante {
    display:scroll;
    position:fixed;
    bottom:300px;
    left:0px;
    background: unset;
    z-index: 10;
}
.flotante2 {
    display:scroll;
    position:fixed;
    bottom:150px;
    left:0px;
    background: unset;
    z-index: 10;
}
.tam-flotante {
    width: 30vh;
}
.titlePost{
    font-size: 1.5rem !important;
    margin: 7.46rem 0 1.168rem 0 !important;
    font-weight: 600 !important;
}

</style>
<body>
<?php //$this->view("japy/search"); ?>
<div class="content center-align">
    <div class="col s12 m12 l12" style="position: relative;">
        <!-- <div class="video" style="height: 550px;">
            <video autoplay muted loop controls controlsList="nodownload" style="max-height: calc(100vh - 85px);" src="<?= base_url()."uploads/videos/japy/Japy_c7_comprimido.mp4"?>" width="100%"></video>
        </div>
        <div class="video_movil" style="height: 550px;">
            <video autoplay muted loop controls controlsList="nodownload" style="max-height: calc(100vh - 85px);" src="<?= base_url()."uploads/videos/japy/Japy_c7_movil.mp4"?>" width="100%"></video>
        </div> -->
        <!--<a class='flotante' href='https://boletos.brideadvisor.mx/evento/Guadalajara-2019-06/BW2X1' ><img class="tam-flotante" src="<?php echo base_url() ?>dist/img/brideweekend/BOTON-MAMA.png"></a>-->
        <!--<a class='flotante' href='<?php echo base_url() ?>brideweekend' ><img class="tam-flotante" src="<?php echo base_url() ?>dist/img/brideweekend/BOTON-BRIDE.png"></a>-->
        <div class="slider slide">
            <ul class="slides slide2">
                
                <li>
                    <img src="<?php echo base_url() ?>dist/img/slider_home/Slider01.png">
                </li>
                <li>
                    <a href="https://brideadvisor.mx/magazine"><img src="<?php echo base_url() ?>dist/img/slider_home/Slider02.png"></a>
                </li>
                <li>
                    <a href="https://brideadvisor.mx/brideweekend/exposiciones"><img src="<?php echo base_url() ?>dist/img/slider_home/Slider03.png"></a>
                </li>
            </ul>
        </div>
    </div>  
    <!-- <div class="col s12 m12 l12 slider-web" style="height: calc(100vh - 85px) !important; position: relative;">
        <div class="slider">
            <ul class="slides">
                <li>
                <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal.png"> 
                </li>
                <li>
                <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal2.png"> 
                <div class="caption left-align">
                    <h3>Disfruta de pasarelas, conferencias, premios y más de 200 expositores</h3>
                    <h4 class="light grey-text text-lighten-3">esperando para tu evento.</h4>
                </div>
                </li>
                <li>
                <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal3.png"> 
                <div class="caption right-align">
                    <h3>Tendencias, creatividad y emoción se dan cita en BRIDE WEEKEND<strong style="font-size: 17px">©</strong></h3>
                </div>
                </li>
            </ul>
        </div>
    </div> 
    <div class="col s12 m12 l12 slider-movil" style="height: calc(100vh - 85px) !important; position: relative;">
        <div class="slider">
            <ul class="slides">
                <li>
                <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal-movil.jpg"> 
                </li>
                <li>
                <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal2.png"> 
                <div class="caption left-align">
                    <h3>Disfruta de pasarelas, conferencias, premios y más de 200 expositores</h3>
                    <h4 class="light grey-text text-lighten-3">esperando para tu evento.</h4>
                </div>
                </li>
                <li>
                <img src="<?php echo base_url() ?>dist/img/brideweekend/bannerprincipal3.png"> 
                <div class="caption right-align">
                    <h3>Tendencias, creatividad y emoción se dan cita en BRIDE WEEKEND<strong style="font-size: 17px">©</strong></h3>
                </div>
                </li>
            </ul>
        </div>
    </div>  -->
    <br><br>
    <div class="row center-align">
        <h5 style="color: #515151;">BÚSQUEDA DE PROVEEDORES</h5>
    </div>
    <div id="buscador" class="row container" style="align-items: center;
        justify-content: center; padding: 4vh;">
        <div class="">
            <?php $this->view("japy/search") ?>
        </div>
    </div>
    <!-- <div id="buscador" class="row container" style="align-items: center;
        justify-content: center; padding: 4vh;">
        <div class="">
            <form class="col s12 m12 l12 alinear" style="display: flex;">
                <input class="" type="text" style="height: 43px;
                border: #c1c1c1 solid 1px; border-radius: unset;padding: inherit; margin: auto;"
                placeholder="¿Qué estas buscando?">
                <select class="browser-default hide-on-small-only col l4" style="height: 45px; border: #c1c1c1 solid 1px;max-width: -moz-min-content;color:#9C9D9F;">
                    <option value="" disabled selected>Todas las categorías</option>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    <option value="3">Option 3</option>
                </select>
                <button class="btn" style="font-size: small;height: 45px;background-color: #72d6e0;border: none;font-weight: bold;">BUSCAR</button>
            </form>
        </div>
    </div> -->
    <div class="row articulo" style="background: white;" >
        <div class="row center-align">
            <h5 style="color: #515151;"> ARTÍCULOS RECIENTES</h5>
        </div>
        <div class="row " id="galeria">
            <div class=" "> 
                <?php foreach ($posts as $post) : ?>            
                    <div class="col s12 m4" style="padding: 0;">
                        <div class="card articulo1" style="">
                            <div class="card-image">
                            <a href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><img class="element-img" object-fit="cover" width="250" style=" height: 300px; object-fit: cover;"
                                    src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>"></a>
                            </div>
                            <div class="card-content">
                            <span class="card-title"><a href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><?php echo strip_tags($post->titulo) ?></a></span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <!--        <a href="<?php echo base_url() ?>tendencia-oscars"><img class="element-img"   width="250" style=" height: 300px; object-fit: cover; object-position:top;"-->

                <!--        <a href="<?php echo base_url() ?>bride-w"><img class="element-img" object-fit="cover" width="250" style=" height: 300px; object-fit: cover;object-position:top;"-->

                <!--        <a href="<?php echo base_url() ?>manuel_diaz"><img class="element-img" object-fit="cover" width="250" style=" height: 300px; object-fit: cover;"-->

        </div>
        </div>
    </div>
    <div class="row articulo-movil" style="background: white; margin-bottom: 70px;" >
        <div class="row center-align">
            <h5 style="color: #515151;" > ARTICULOS RECIENTES</h5>
        </div>
        <div class="slider slider2">
            <ul class="slides">
            <?php foreach ($posts as $post) : ?>            
                <li>
                    <!--<div class="card articulo1" style="">-->
                    <!--    <div class="card-image">-->
                    <!--    <a href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><img class="element-img" object-fit="cover" width="250" style=" height: 300px; object-fit: cover;"-->
                    <!--            src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>" alt="" style=""></a>-->
                    <!--    </div>-->
                    <!--    <div class="card-content art-movil">-->
                    <!--    <span class="card-title"><a href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><?php echo strip_tags($post->titulo) ?></a></span>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <a href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><img  src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>" width="250" style=" height: 300px; object-fit: cover;"></a>
                    <div class="caption center-align">
                      <h3 class="titlePost"><?php echo strip_tags($post->titulo) ?></h3>
                    </div>
                </li>
            <?php endforeach; ?>
            
            </ul>
        </div>
    </div>
    <!-- <div class="row proveedor" style="background: #EEEDED;" >
        <div class="row center-align">
            <br>
            <h5 style="color: #515151;"> PROVEEDORES RECIENTES</h5>
        </div>
        <div class="row center" >
            <div class="container"> 
            <?php //foreach ($providers as $provider) : ?>
                <?php //if ($provider->imagePrincipal->count()) {
                   // $providerImageUrl = base_url()."uploads/images/".$provider->imagePrincipal->first()->nombre;
                    ?>
                <?php //} else {
                    //$providerImageUrl = base_url()."dist/img/slider1.png";?>
                <?php// } ?>
                <div class="col s12 m6 l4">
                    <div class="card pro" style="">
                        <div class="card-image">
                        <img class="element-img" object-fit="cover" width="250" style=" height: 400px; object-fit: cover;"
                                src="<?php //echo($providerImageUrl) ?>">
                        </div>
                        <div class="card-content">
                        <span class="card-title"><a href="<?php //echo base_url() ?>boda-<?php //echo $provider->slug ?>"><?php echo $provider->nombre ?></a></span>
                        <p><?php //(substr(strip_tags($provider->descripcion), 0, 100)) ?>...</p>
                        <br>
                        <div class="btn"> <a href="<?php //echo base_url() ?>boda-<?php //echo $provider->slug ?>" >LEER MÁS </a></div>
                        </div>
                    </div>
                </div>
                <?php //endforeach; ?>
            </div>
        </div>
    </div> -->
    <!-- <div class="row proveedor-movil" style="background: #EEEDED;" >
        <div class="row center-align">
            <br>
            <br>
            <h5 style="color: #515151;"> PROVEEDORES RECIENTES</h5>
        </div>
        <div class="slider" id="movil">
            <ul class="slides pro-movil">
            <?php// foreach ($providers as $provider) : ?>
                <?php //if ($provider->imagePrincipal->count()) {
                   // $providerImageUrl = base_url()."uploads/images/".$provider->imagePrincipal->first()->nombre;
                    ?>
                <?php// } else {
                   // $providerImageUrl = base_url()."dist/img/slider1.png";?>
                <?php //} ?>
                <li>
                    <div class="card pro" style="">
                        <div class="card-image">
                        <img class="element-img" object-fit="cover" width="250" style=" height: 400px; object-fit: cover;"
                                src="<?php// echo($providerImageUrl) ?>">
                        </div>
                        <div class="card-title">
                        <span class=""><a href="<?php //echo base_url() ?>boda-<?php //echo $provider->slug ?>"><?php //echo $provider->nombre ?></a></span>
                        </div >
                        <div class="card-content">
                        <p><?php //echo(substr(strip_tags($provider->descripcion), 0, 100)) ?>...</p>
                        <br>
                        </div>
                        <div class="card-content">
                        
                        <div class="btn"> <a href="<?php //echo base_url() ?>boda-<?php //echo $provider->slug ?>" >LEER MÁS </a></div>
                        <br>
                        </div>
                    </div>
                </li>
                <?php //endforeach; ?>
            </ul>
        </div>
    </div> -->
    
    <!-- Modal Structure -->
    <div id="modal3" class="modal">
        <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                        style="float: right !important;
                        position: absolute !important;
                        right:0px; !important; color: #8DD7E0 !important;
                        font-size:4vh;">X</button>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/headganadores.png" style="width: 100%">
        <div class="row">
            <h5 style="font-weight: bold;">CULIACÁN</h5>
            <table class="centered striped">
                <thead>
                    <tr>
                        <th>FOLIO</th><th>NOMBRE</th><th>PREMIO</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>11</td><td>Lytza Bojorquez</td><td>vestido abolengo novias</td>
                </tr>
                <tr>
                    <td>25</td><td>Maribel Mendoza</td><td>maquillaje y peinado claudia luggo</td>
                </tr>
                <tr>
                    <td>9</td><td>Nathaly Mendoza</td><td>maquillaje y peinado claudia luggo</td>
                </tr>
                <tr>
                    <td>14</td><td>Fernanda </td><td>maquillaje y peinado claudia luggo</td>
                </tr>
                <tr>
                    <td>43</td><td>Melissa sanchez</td><td>50% evento en the Gallery Beach Club</td>
                </tr>
                <tr>
                    <td>16</td><td>Edna Bazua</td><td>1 hora cabina fotografica photobox</td>
                </tr>
                <tr>
                    <td>37</td><td>Itzel</td><td>1 hora cabina fotografica photobox</td>
                </tr>
                <tr>
                    <td>42</td><td>Itzel Ayala </td><td>1 hora cabina fotografica photobox</td>
                </tr>
                <tr>
                    <td>6</td><td>Nahomy</td><td>1 hora cabina fotografica photobox</td>
                </tr>
                <tr>
                    <td>17</td><td>Ana Karen Cardenas Cazares</td><td>2 horas fotos ilimitadas momentos</td>
                </tr>
                <tr>
                    <td>38</td><td>Mario romero </td><td>vestido novia Patricia y Ancona</td>
                </tr>
                <tr>
                    <td>48</td><td>Paola Catalán Chavarin</td><td>vestido de noche luxury</td>
                </tr>
                <tr>
                    <td>20</td><td>Carolina Castañeda </td><td>vestido de noche P&A</td>
                </tr>
                <tr>
                    <td>26</td><td>WENDY ANAHY ARMIENTA DUARTE</td><td>Luna de miel</td>
                </tr>
                </tbody>
            </table>
        </div>
        <img src="<?php echo base_url() ?>dist/img/brideweekend/ganadoraauto.png" style="width: 100%">
        <img src="<?php echo base_url() ?>dist/img/brideweekend/footerganadores.png" style="width: 100%">
    </div>
    
    <!-- <div>
        <img class="banner_ganadores ganadores" style="width: 100%; cursor: pointer;">
    </div> -->
    
    <div style="background-repeat: no-repeat;
        background-position: center;
        background-size:cover;
        height: 350px;
        width: 100%;
        max-width: 100%;
        background-image: url('<?php echo base_url() ?>/dist/img/brideweekend/brideweekend2.png')">
        <div id="prueba" style="height:100%; background: rgba(114, 214, 224, 0.86);">
        <div class="container" style="width: 50%;">
            <i class="fa fa-heart-o" aria-hidden="true" style="font-size: 6vh; color: white; margin-top: 10%;"></i>
            <h5 style="color: white; font-weight: bold;">"Laura Gill."</h5>
            <p style="color: white;">La página de BrideAdvisor.mx me parece muy completa ya que me ayuda a planear 
            mejor mi boda, su blog tiene artículos  interesantes. Muy buena plataforma, felicidades.</p>
        </div>
        </div>
    </div>
    <div class="row position">
        <!-- <div class=""> -->
            <p style="text-align: left; font-weight: bold; ">REGÍSTRATE</p>
            <div id="form_inscripcion" class="col s12 m12 l12 alinear" style="display: flex;">
                <input id="email_susc" class="" type="email" style="height: 43px;
                border: #c1c1c1 solid 1px;
                border-radius: unset;
                padding: inherit;
                margin: auto;
                "
                placeholder="Your email" required>
                <button class="btn grey darken-1 sig_in" style="font-size: small;height: 45px;background-color: #9C9D9F;border: none;font-weight: bold; border-radius: 10px 10px 10px 10px !important;">SUSCRIBIRSE</button>
            </div>
        <!-- </div> -->
    </div>
    <br><br>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
$(document).ready(function(){
    
    if (screen.width< 326) {
        $('img.banner_ganadores').attr('src', '<?php echo base_url() ?>/dist/img/brideweekend/winner_movil.png');
    }
    else if (screen.width < 426) {
        $('img.banner_ganadores').attr('src', '<?php echo base_url()?>dist/img/brideweekend/winner_movil.png');
    }
    else if (screen.width < 769) {
        $('img.banner_ganadores').attr('src', '<?php echo base_url()?>dist/img/brideweekend/winner.png');
    }
    else if (screen.width < 2100) {
        $('img.banner_ganadores').attr('src', '<?php echo base_url()?>dist/img/brideweekend/winner.png');
    }
    
    $('.ganadores').on('click', ganador);

    var width = $(window).width();
    if(width < 510){
        $('.video').remove();
        $('.video_movil').css('height', '180px');
    }else{
        $('.video_movil').remove();
    }

    $('.slider').slider({full_width: true, interval: 7777});
    $('.sig_in').on('click', render);

    // // <img class="swal2-image" src="https://localhost/japy/dist/img/slider_home/banner_start.jpg" alt="" style="display: flex;" width="750" height="550"></img>
    // $(".swal2-image").replaceWith('<a target="_blank"  href="<?= base_url() ?>brideweekend#registro"><img class="swal2-image" src="https://localhost/japy/dist/img/slider_home/banner_start.jpg" alt="" style="display: flex;" width="750" height="550"></img></a>');
    

});

function ganador() {
    $('#modal3').modal('open');
}
function render() {
    window.location.href = "<?php echo base_url() ?>registro?mail="+$('#email_susc').val();
}
// $('#form_inscripcion').submit(function(e) { 
//         e.preventDefault();

//         $.ajax({
//             url: '<?php echo base_url()."home/suscripcion" ?>',
//             method: 'post',
//             data: {
//                 'email': $('#email_susc').val(),
//             },
//             success: function(res) {
//                 if (res.data == true) {
//                     swal('Error', 'Correo registrado anteriormente.', 'error');
//                     return false;
//                 }else{
//                     swal('Bien', 'Te has suscrito correctamente.', 'success');
//                     return true;
//                 }

//                 // $('#registro').submit();
//             },
//             error: function() {
//                 swal('Error', 'Lo sentimos ocurrio un error', 'error');
//             },
//         });
// });
function suscripcion(){
  
}
</script>
</body>
<?php $this->view("japy/footer"); ?>
</html>