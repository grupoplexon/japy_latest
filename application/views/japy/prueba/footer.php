<head>
    <link href="<?php echo base_url() ?>dist/css/brideAdvisor/footer.css" rel="stylesheet" type="text/css"/>
</head>
<style>

#footer a {
    font-size: 25px !important;
    color: #fafafa !important;
    margin: 5%;
}
.icon-style {
    color: white !important;
}
</style>
<div id="footer" class="col l12">
    <footer style="padding-top: unset;" class="page-footer">
        <div class="footer-copyright" style="">
            <div class="row center-align footer" style="">
                <div class="col s12 m12 l5 japy" >
                    <div class="row  " style="">
                        <a href="<?php echo base_url() ?>">
                            <img src="<?php echo base_url() ?>/dist/img/ba-rose.png" style="width: 50%;" class="brideAdvisorimg" alt="Japy">
                        </a>
                    </div>
                </div>
                <div class="col s10 offset-s1 m12 l2" >
                    <div class="row  center-align" style="">
                            <h6 class="center-align titleBA" >BrideAdvisor© 2019 DERECHOS RESERVADOS</h6>
                    </div>
                </div>
                <div class="col s12 m12 l5" >
                    <div class="row  " style="">
                        <a href="https://www.nupcialmexicana.com">
                            <img src="<?php echo base_url() ?>/dist/img/anm_rose.png" class="logo-anm"  alt="BrideAdvisor">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!--<script src="<?php //echo base_url() ?>dist/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="<?php //echo base_url() ?>dist/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php //echo base_url() ?>dist/js/datatables.min.js" type="text/javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->

<!-- <script src="<?php echo base_url() ?>dist/js/materealize.min.js" type="text/javascript"></script> -->
<script src="<?php echo base_url() ?>dist/js/jquery.Jcrop.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/classie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/slider.min.js" type="text/javascript"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="<?php echo base_url() ?>dist/js/inputFormat.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.touchSwipe.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/facebook.min.js" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
<link href="<?php echo base_url() ?>dist/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo base_url() ?>dist/enjoyhint/enjoyhint.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/tutorial.min.js"></script>

<script src="<?php echo base_url() ?>dist/js/validation/validate.engine.js"></script>
<script src="<?php echo base_url() ?>dist/js/validation/validate.engine.plugin.js"></script>

<script src="<?php echo base_url() ?>dist/slider-pro-master/dist/js/jquery.sliderPro.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>



<script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/parsley.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/i18n/es.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<!-- Leo-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/clndr/1.4.7/clndr.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/datepicker-es.js"></script>

<!-- -->
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
</script>
<script>
    $(document).ready(function() {
        if (typeof onReady != 'undefined') {
            onReady();
        }
        // $('select').material_select();
        // $('.button-collapse').sideNav();
        $('.dropdown-button').dropdown();
        $('.modal').modal();
        $('.collapsible').collapsible({
            accordion: false,
        });

        $('.multiple-items').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            dots: false,
            autoplaySpeed: 5000,
            arrows: false,
            adaptiveHeight: true,
        });

        $('.center-slide').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            arrow: false,
            dots: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3,
                    },
                },
                {
                    breakpoint: 440,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                    },
                },
            ],
        });

        $('#slider-home').sliderPro({
            width: '100%',
            autoHeight: true,
            buttons: false,
        });
    });

    function generoImage(edad, sexo) {
        switch (parseInt(sexo)) {
            case 1:
                //HOMBRE
                switch (parseInt(edad)) {
                    case 1:
                        //ADULTO
                        return 'invitados invitados-Hombre x2';
                        break;
                    case 2:
                        //NINO
                        return 'invitados invitados-Nino x2';
                        break;
                    case 3:
                        //BEBE
                        return 'invitados invitados-Bebe-nino x2';
                        break;
                }
                break;
            case 2:
                //MUJER
                switch (parseInt(edad)) {
                    case 1:
                        //ADULTO
                        return 'invitados invitados-Mujer x2';
                        break;
                    case 2:
                        //NINO
                        return 'invitados invitados-Nina x2';
                        break;
                    case 3:
                        //BEBE
                        return 'invitados invitados-Bebe-nina x2';
                        break;
                }
                break;
        }
    }
</script>
