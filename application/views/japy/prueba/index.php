<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>BrideAdvisor</title>
    <?php $this->view("japy/prueba/header_n"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script> -->
</head>
<style>
.content{
    min-height: calc(100vh - 15px);
    position: relative;
}

.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}
#header {
    position: relative !important;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px){
    #header {
        position: absolute !important;
    }
}
</style>
<body>
<div class="content background" style=" background-image: url('<?php echo base_url()?>dist/img/fondo.jpg');">
    <div class="row search background" style="position: relative; ">
        
        <!--<a href="" class="hide-on-small-only"><img class="img-home " style="width:100%;" src="<?php echo base_url() ?>dist/img/slider_home/slider01.png"></a>-->
        <div class="slider slide hide-on-small-only">
            <ul class="slides slide2">
                
                <li>
                    <img class="img-home " style="width:100%;" src="<?php echo base_url() ?>dist/img/slider_home/slider01.png">
                </li>
                <li>
                    <a href="https://brideadvisor.mx/magazine"><img src="<?php echo base_url() ?>dist/img/slider_home/002_r.png"></a>
                </li>
                <li>
                    <img class="img-home " style="width:100%;" src="<?php echo base_url() ?>dist/img/slider_home/003_r.png">
                </li>
                <li>
                    <a href="https://brideadvisor.mx/brideweekend/exposiciones"><img src="<?php echo base_url() ?>dist/img/slider_home/004_r.png"></a>
                </li>
            </ul>
        </div>
        <div id="buscador" class="row b-web centrado hide-on-small-only" style="align-items: center;
            justify-content: center; padding: 4vh;">
            <?php $this->view("japy/prueba/search") ?>
        </div>
        <div id="buscador" class="row b-movil  centrado hide-on-large-only" style="align-items: center;
            justify-content: center; padding: 4vh;">
            <?php $this->view("japy/prueba/search-movil") ?>
        </div>
    </div> 
    <br>
    <div class="row hide-on-large-only" style="margin-top: 50px;">
        <a href="" class=""><img class=" " style="width:100%;" src="<?php echo base_url() ?>dist/img/slider_home/qro_09-20.jpg"></a>
    </div>
    <div class="row articulo background" >
        <div class="row center-align container">
            <h4 class="textPrincipal"> LA MANERA MÁS SENCILLA DE PLANEAR TU BODA</h4>
            <p class="descrip">Bienvenidos a BrideAdvisor, plataforma digital en donde te ayudaremos 
                a planear tu boda con las mejores herramientas.</p>
        </div>
        <br>
        <div class="row div-articulos center-align">
            <h5 class="title">ARTICULOS RECIENTES</h5>
        </div>
        <div class="row margin hide-on-large-only">
            <div class="col l6 s12 articulos jcarousel-articulos center">
                <?php foreach ($posts as $post) : ?>            
                    <div class="col m2-4-jcaroul">
                        <div class="row card " >
                            <div class="col l3 center-align">
                                <a style="" href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><img class="art-img"
                                    src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>"></a>
                            </div>
                            <div class="col l9 ">
                                <h5 class="descrip"><?php echo strip_tags($post->titulo) ?></h5>
                                <h6 class="contenido"><?php echo substr(strip_tags($post->contenido), 0, 150) ?></h6>
                                <a class="color-a" href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><h6 class="mas"> Ver más</h6></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="row margin hide-on-small-only">
            <div class="col l6 s12 articulos ">
                <img style="width:100%; height:100%;" class="img" src="<?php echo base_url() ?>dist/img/novia.jpg">
                <div id="buscador" class="row  izquierda" style="align-items: center;
                        justify-content: center; padding: 4vh;">
                </div>
                <br>
            </div> 
            <div class="col l6 s12 div-left articulos">
                
                <?php foreach ($posts as $post) : ?>            
                    <div class="row" style="padding: 0;">
                        <div class="col l3 center-align">
                            <a style="padding-top:5px;" href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><img class="element-img"
                                src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>"></a>
                        </div>
                        <div class="col l9 ">
                            <h5 class="descrip"><?php echo strip_tags($post->titulo) ?></h5>
                            <h6 class="contenido"><?php echo substr(strip_tags($post->contenido), 0, 150) ?></h6>
                            <a class="color-a" href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><h6 class="mas"> Ver más</h6></a>
                        </div>
                    </div>
                <?php endforeach; ?>
                <br>
            </div>
        </div>
        <!-- <div class="row margin hide-on-small-only">
            <div class="col l6 s12 div-right articulos">
                
                <?php foreach ($posts as $post) : ?>            
                    <div class="row" style="padding: 0;">
                        <div class="col l3 s12 center-align">
                            <a style="padding-top:5px;" href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"><img class="element-img img"
                                src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>"></a>
                        </div>
                        <div class="col s12 l9 ">
                            <h5 class="descrip"><?php echo strip_tags($post->titulo) ?></h5>
                            <h6 class="contenido"><?php echo substr(strip_tags($post->contenido), 0, 150) ?></h6>
                            <a class="color-a" href=""><h6 class="mas"> Ver más</h6></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col l6 s12 articulos hide-on-small-only">
                <img style="width:100%; height:100%;" class="img" src="<?php echo base_url() ?>dist/img/belleza.jpg">
                <div id="buscador" class="row  derecha" style="align-items: center;
                        justify-content: center; padding: 4vh;">
                </div>vvvv
            </div>
        </div> -->
        <br>
        <div class="row  center-align">
            <div class="col l4 offset-l4 s10 offset-s1 boton-secciones">
                <a href="<?php echo base_url('magazine') ?>" class="color-a" href=""><h5 class="secciones">VER ARTICULOS</h5></a>
            </div>
        </div>
        <br>
        <hr class="hr  hide-on-small-only">
        <br>
        <div class="row center-align hide-on-small-only">
            <h4>MI PLANEADOR DE BODA</h4>
            <div class="row">
                <div class="col l2 offset-l1 center-align">
                    <a href="<?php echo base_url() ?>home/planeador_bodas"><img class="icon" src="<?php echo base_url() ?>dist/img/iconos/iconos-rosa/agenda.png" alt=""></a>
                    <br>
                    <h5>AGENDA</h5>
                    <h6>No olvides agendar todos tus pendientes y actividades.</h6>
                </div>     
                <div class="col l2  center-align">
                    <a href="<?php echo base_url() ?>home/planeador_bodas"><img class="icon" src="<?php echo base_url() ?>dist/img/iconos/iconos-rosa/invitados.png" alt=""></a>
                    <br>
                    <h5>INVITADOS</h5>
                    <h6>Haz una lista con la selección de tus invitados, sus contactos y mantén tus confirmaciones al día.</h6>
                </div>     
                <div class="col l2 offset-l2 center-align ">
                    <a href="<?php echo base_url() ?>home/planeador_bodas"><img class="icon" src="<?php echo base_url() ?>dist/img/iconos/iconos-rosa/mesas.png" alt="">   </a>
                    <br>
                    <h5>CONTROL DE MESAS</h5>
                    <h6>Esta parte es divertida, ya que podrás acomodar a tus invitados.</h6>
                </div>     
                <div class="col l2 center-align">
                    <a href="<?php echo base_url() ?>home/planeador_bodas"><img class="icon" src="<?php echo base_url() ?>dist/img/iconos/iconos-rosa/proveedores.png" alt=""></a>
                    <br>
                    <h5>PROVEEDORES</h5>
                    <h6>Tu agenda personal con tus proveedores, tus pagos y pendientes.</h6>
                </div>     
            </div>
            <br>
            <div class="row">
                <div class="col l2 offset-l2 center-align">
                    <a href="<?php echo base_url() ?>home/planeador_bodas"><img class="icon" src="<?php echo base_url() ?>dist/img/iconos/iconos-rosa/presupuesto.png" alt=""></a>
                    <br>
                    <h5>PRESUPUESTADOR INTELIGENTE</h5>
                    <h6>Te ayuda a administrar y maximizar tu dinero y a seleccionar el proveedor ideal de acuerdo a tus necesidades.</h6>
                </div>     
                <div class="col l2 offset-l1 center-align">
                    <a href="<?php echo base_url() ?>home/planeador_bodas"><img class="icon" src="<?php echo base_url() ?>dist/img/iconos/iconos-rosa/vestidos.png" alt=""></a>
                    <br>
                    <h5>MIS VESTIDOS</h5>
                    <h6>Encontrarás diseños exclusivos de las mejores marcas, para que elijas de tus favoritos</h6>
                </div>     
                <div class="col l2 offset-l1 center-align ">
                    <a href="<?php echo base_url() ?>home/planeador_bodas"><img class="icon" src="<?php echo base_url() ?>dist/img/iconos/iconos-rosa/portal.png" alt=""></a> 
                    <br>
                    <h5>MI PORTAL WEB</h5>
                    <h6>Podrás compartir información de tu boda con tus invitados a través de redes sociales.</h6>
                </div>  
            </div>
        </div>
        <div class="row div-articulos center-align">
            <h5 class="title">NUEVOS PROVEEDORES</h5>
        </div>
        <div class="row proveedores hide-on-small-only">
            <br>
            <?php foreach ($providers as $provider) : ?>
                <?php if ($provider->imagePrincipal->count()) {
                    $providerImageUrl = base_url()."uploads/images/".$provider->imagePrincipal->first()->nombre;
                    ?>
                <?php } else {
                    $providerImageUrl = base_url()."dist/img/slider1.png";
                    ?>
                <?php } ?>
                <div class="col l4 s12 height">
                    <div class="card-content">
                        <a href="<?php echo base_url()."boda-".$provider->slug ?>" ><img class="element-img2" src="<?php echo($providerImageUrl) ?>" alt=""></a> 
                        <div class="vl3">
                            <a class="color-a" href="<?php echo base_url()."boda-".$provider->slug ?>"><h5 class="element-name truncate"><?php echo $provider->nombre ?></h5></a>
                            <p class="" style=""><?php echo strip_tags(substr($provider->descripcion,
                                    0, 100)) ?>... </p>
                            <p style="">Calificación:
                                 <?php for($i=1; $i<= 5 ; $i++) {
                                    if($i<=$provider->promedio) { ?>
                                        <span class="fa fa-star checked"></span>
                                <?php } else{ ?>
                                    <span class="fa fa-star"></span>
                                <?php }  } ?>
                            </p>
                            <p><i class="fa fa-map-marker-alt"></i> <?php echo $provider->localizacion_direccion ?>
                                (<?php echo $provider->localizacion_estado ?>)</p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row  hide-on-large-only">
            <br>
            <?php foreach ($providers as $provider) : ?>

                <?php if ($provider->imagePrincipal->count()) {
                    $providerImageUrl = base_url()."uploads/images/".$provider->imagePrincipal->first()->nombre;
                    ?>
                <?php } else {
                    $providerImageUrl = base_url()."dist/img/slider1.png";
                    ?>
                <?php } ?>
                <div class="col l4 s12 ">
                    <div class="card">
                        <div class="card-content">
                            <a class="color-a" href="<?php echo base_url()."boda-".$provider->slug ?>"><h5 class="element-name truncate"><?php echo $provider->nombre ?></h5></a>
                            <a href="<?php echo base_url()."boda-".$provider->slug ?>" ><img class="element-img2" src="<?php echo($providerImageUrl) ?>" alt=""></a> 
                            <p style="float: right;">Calificación:
                                <?php for($i=1; $i<= 5 ; $i++) {
                                    if($i<=$provider->promedio) { ?>
                                        <span class="fa fa-star checked"></span>
                                    <?php } else{ ?>
                                        <span class="fa fa-star"></span>
                                    
                                <?php }  } ?>
                            </p>
                            <div class="">
                                <p><i class="fa fa-map-marker-alt"></i> <?php echo $provider->localizacion_direccion ?>
                                    (<?php echo $provider->localizacion_estado ?>)</p>
                               
                            </div>
                        </div>
                    </div>
                </div> 
            <?php endforeach; ?>
        </div>
        <div class="row  center-align  hide-on-large-only">
            <div class="col l4 offset-l4 s10 offset-s1 boton-secciones">
                <a class="color-a" href="<?php echo base_url()."proveedores" ?>"><h5 class="secciones">VER PROVEEDORES</h5></a>
            </div>
        </div>
        <div class="row div-articulos center-align  hide-on-large-only">
            <h5 class="title">NOTICIAS Y ACTUALIZACIONES</h5>
        </div>
        <div class="row  center-align  hide-on-large-only">
            <div class="col l4 offset-l4 s10 offset-s1 ">
                <a class="color-a" href=""><h5 class="secciones">REGISTRATE</h5></a>
                <input name="email_susc" class="registro" style="" placeholder="Introduce tu correo electronico"/>
                <br><br>
                <button type="" class="btn searchProvider sig_in hide-on-large-only" style="border-radius: 10px 10px 10px 10px !important;" > SUSCRIBIRME</button>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function(){
        $('.sig_in').on('click', render);
        
        $('.slider').slider({full_width: true, interval: 7777});

        $('.jcarousel-articulos').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        speed: 300,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left flecha" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right flecha" aria-hidden="true"></i></button>',
        autoplay: true,
        lazyLoad: 'ondemand',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });
    });
    function render() {
        window.location.href = "<?php echo base_url() ?>registro/registroNovia?mail="+$('#email_susc').val();
    }
</script>
</body>
<?php $this->view("japy/prueba/footer"); ?>
</html>