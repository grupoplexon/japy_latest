<?php
// $search = isset($_GET["buscar"]) ? htmlspecialchars($_GET["buscar"]) : "";
// $state  = isset($_GET["state"]) ? htmlspecialchars($_GET["state"]) : "";
?>
<head>  
</head>
<style>
    .autocomplete-items {
        position: absolute !important;
        background: white !important;
        width: 45% !important;
        margin-top: 50px !important;
        margin-left: 6% !important;
        font-size: 20px !important;
        cursor: pointer !important;
    }
    #btn-search::placeholder {
        color: grey !important;
    }
</style>
<div class="container" style="margin-top: 25px;" >
    <!--<div class="row center-align hide-on-small-only">-->
    <!--    <h5 class="title space" style="color: #515151;">NOVIA</h5> -->
    <!--    <div class="vl"></div>-->
    <!--    <h5 class="title space" style="color: #515151;">NOVIO</h5>-->
    <!--    <div class="vl"></div>-->
    <!--    <h5 class="title space" style="color: #515151;">BODA</h5>-->
    <!--    <div class="vl"></div>-->
    <!--    <h5 class="title space" style="color: #515151;">DESTINOS DE BODA</h5>-->
    <!--    <div class="vl"></div>-->
    <!--    <h5 class="title space" style="color: #515151;">HOGAR</h5>-->
    <!--</div>-->
    <div class="row hide-on-large-only center-align">
        <br>
        <h5 class="title" style="color: #515151;">¿Qué estas buscando?</h5> 
    </div>
    <div class="">
        <div class="col s12 m12 l12 alinear" style="display: flex;">
            <p class="btn searchProvider " style="margin: 0 !important;" id="search"> <i class="fas fa-search " style="margin-top: 8px;"></i></p>
            <input id="btn-search" type="text" placeholder="¿Qué estas buscando?">
            
            <select name="estado"  class="browser-default  col l4" id="stateId" style="">
                <option id="Todos" value="Todos">Todos</option>
                <option id="Jalisco" value="Jalisco">Jalisco</option>
                <option id="Puebla" value="Puebla">Puebla</option>
                <option id="Querétaro" value="Querétaro">Querétaro</option>
            </select>
        </div>
    </div>
</div>
<input type="hidden" id="url" value="<?php echo base_url() ?>">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    var baseURL = $('#url').val();
    var busqueda = '';
    $(document).ready(function() {
        $("#btn-search").focus();
        $(window).scrollTop(0);

        $('.searchProvider').on('click', search);
        
         $( "#btn-search" ).autocomplete({
            source: baseURL+'proveedores/providers',
            select: (event, ui) => {
                busqueda = ui.item.value;
                search(busqueda);
            }
        });
        
        
        $.ajax({
            url: "https://geoip-db.com/jsonp",
            jsonpCallback: "callback",
            dataType: "jsonp",
            success: function( location ) {
                // console.log(location.state);
                STATE = location.state;
                // providers(location.state, 'noSelect');
                if(location.state!='Jalisco' && location.state!='Puebla' && location.state!='Queretaro') {
                    $('#Todos').attr('selected', true);
                    //autoProviders('Todos');
                } else {
                    $('#'+location.state).attr('selected', true);
                    //autoProviders(location.state);
                }
            }
        });
    });

    function search(busqueda) {
        window.location.href = "<?php echo base_url() ?>proveedores/index?search="+busqueda+"&estado="+$('#stateId').val();
    }


</script>

