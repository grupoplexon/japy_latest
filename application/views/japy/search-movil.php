<?php
//$search = isset($_GET["buscar"]) ? htmlspecialchars($_GET["buscar"]) : "";
//$state  = isset($_GET["state"]) ? htmlspecialchars($_GET["state"]) : "";
?>
<head>  
</head>
<style>

</style>
<div class="container" >
    <div class="row hide-on-large-only center-align">
        <br>
        <h5 class="title" style="color: #515151;">¿Qué estas buscando?</h5> 
    </div>
    <div class="">
            <div class="row busc">
                <input id="btn-search2" type="text" placeholder="¿Qué estas buscando?">
                <select name="estado"  class="browser-default  col l4" id="stateId" style="">
                    <option id="Todos" value="Todos">Todos</option>
                    <option id="Jalisco" value="Jalisco">Jalisco</option>
                    <option id="Puebla" value="Puebla">Puebla</option>
                    <option id="Querétaro" value="Querétaro">Querétaro</option>
                </select>
            </div>
            <div class="row center-align">
                <button class="btn searchProvider2  hide-on-large-only" id="search" style="" > Buscar</button>
            </div>
    </div>
</div>
<input type="hidden" id="url" value="<?php echo base_url() ?>">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    var baseURL = $('#url').val();
    var busqueda = '';
    $(document).ready(function() {
        $(window).scrollTop(0);

        $('.searchProvider2').on('click', search2);
        
        // $('#btn-search2').on( "keydown", function(event) {
        //     if(event.which == 13) {
        //         search2();
        //         console.log("entro");
        //     }
        // });

        $( "#btn-search2" ).autocomplete({
            source: baseURL+'proveedores/providers',
            select: (event, ui) => {
                // this.search = ui.item.value;
                busqueda = ui.item.value;

                // console.log(ui.item.value);
                search2(busqueda);
                // this.getInspirations();
            }
        });
        
    });

    function search2() {
        window.location.href = "<?php echo base_url() ?>proveedores/index?search="+$('#btn-search2').val()+"&estado="+$('#stateId').val();
    }
</script>

