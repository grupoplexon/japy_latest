<?php
// $search = isset($_GET["buscar"]) ? htmlspecialchars($_GET["buscar"]) : "";
// $state  = isset($_GET["state"]) ? htmlspecialchars($_GET["state"]) : "";
?>
<head>  
</head>
<style>
    .autocomplete-items {
        position: absolute !important;
        background: white !important;
        width: 45% !important;
        margin-top: 50px !important;
        margin-left: 6% !important;
        font-size: 20px !important;
    }
</style>
<div class="container" >
    <div class="row center-align hide-on-small-only">
        <h5 class="title space" style="color: #515151;">NOVIA</h5> 
        <div class="vl"></div>
        <h5 class="title space" style="color: #515151;">NOVIO</h5>
        <div class="vl"></div>
        <h5 class="title space" style="color: #515151;">BODA</h5>
        <div class="vl"></div>
        <h5 class="title space" style="color: #515151;">DESTINOS DE BODA</h5>
        <div class="vl"></div>
        <h5 class="title space" style="color: #515151;">HOGAR</h5>
    </div>
    <div class="row hide-on-large-only center-align">
        <br>
        <h5 class="title" style="color: #515151;">¿Qué estas buscando?</h5> 
    </div>
    <div class="">
        <div class="col s12 m12 l12 alinear" style="display: flex;">
            <p class="btn searchProvider " style="margin: 0 !important;" id="search"> <i class="fas fa-search " style="margin-top: 8px;"></i></p>
            <input id="btn-search" type="text" placeholder="¿Qué estas buscando?">
            
            <select name="estado"  class="browser-default  col l4" id="stateId" style="">
                <option id="Todos" value="Todos">Todos</option>
                <option id="Jalisco" value="Jalisco">Jalisco</option>
                <option id="Puebla" value="Puebla">Puebla</option>
                <option id="Querétaro" value="Querétaro">Querétaro</option>
            </select>
        </div>
    </div>
</div>
<input type="hidden" id="url" value="<?php echo base_url() ?>">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script>
    var baseURL = $('#url').val();
    $(document).ready(function() {
        $("#btn-search").focus();
        $(window).scrollTop(0);

        $('.searchProvider').on('click', search);
        
        $.ajax({
            url: "https://geoip-db.com/jsonp",
            jsonpCallback: "callback",
            dataType: "jsonp",
            success: function( location ) {
                // console.log(location.state);
                STATE = location.state;
                // providers(location.state, 'noSelect');
                if(location.state!='Jalisco' && location.state!='Puebla' && location.state!='Queretaro') {
                    $('#Todos').attr('selected', true);
                    autoProviders('Todos');
                } else {
                    $('#'+location.state).attr('selected', true);
                    autoProviders(location.state);
                }
            }
        });
    });

    function search() {
        window.location.href = "<?php echo base_url() ?>proveedores/index?search="+$('#btn-search').val()+"&estado="+$('#stateId').val();
    }

    function autoProviders(estado) {
        $.ajax({
            dataType: 'json',
            url: baseURL+'proveedores/providers',
            method: 'post',
            data: {
                state: estado
            },
            success: function(response) {
                // console.log(response.providers);
                autocomplete(document.getElementById("btn-search"), response.providers);
            },
            error: function() {
                console.log('error providers');
            }
        });
    }

    function autocomplete(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function(e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false;}
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function(e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
            } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
            } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
            }
        });
        function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
        }
        function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
            x[i].parentNode.removeChild(x[i]);
            }
        }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }

    // function getUserLocation() {
    //     $.ajax({
    //         url: 'https://geoip-db.com/jsonp/',
    //         jsonpCallback: 'callback',
    //         dataType: 'jsonp',
    //         timeout: 8000,
    //         success: function(location) {
    //             if (currentState == '') {
    //                 if (!states.includes(location.state) && location.state != 'Not Found') {
    //                     currentState = location.state;
    //                 }
    //                 else if (location.state == 'Mexico City') {
    //                     currentState = 'Ciudad de Mexico';
    //                 }
    //                 else if (location.state == 'Estado de Mexico') {
    //                     currentState = 'Ciudad de Mexico';
    //                 }
    //                 else {
    //                     currentState = 'Jalisco';
    //                 }
    //                 // updateUrl();
    //             }

    //             updateSelect();
    //             updateMenuUrl(baseURL, search, currentState);
    //         },
    //     });
    //     return '';
    // }
</script>

