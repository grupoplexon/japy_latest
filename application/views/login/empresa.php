<html>
    <head>
        <title>BrideAdvisor</title>
        <?php $this->view("japy/prueba/header"); ?>
        <link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
        <meta name="viewport" content="width=device-width, user-scalable=no,  initial-scale=1">
        <link href="<?php echo base_url() ?>dist/img/clubnupcial.png" rel="icon" sizes="16x16">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <style>
            footer {
                position: fixed;
            }
            .red-text.darken-5 {
                color: #92151b !important;
            }

            .red.darken-5 {
                background: #94151c !important;
            }
            input[type=email], input[type=password], input[type=text] {
                width: 100%;
                text-indent: 15px;
                border: none;
                outline: 0;
                color: #3D3F4D;
                background-color: white !important;
                font-size: .9em;
                -webkit-appearance: none;
                height: 2rem;
                border-bottom: 1px solid #8dd7e0 !important;
                -webkit-box-shadow: 0 1px 0 0 #8dd7e0 !important;
                box-shadow: 0 1px 0 0 #8dd7e0 !important;
            }
            footer.page-footer {
                margin-top: 0px;
                padding-top: 10px;
                z-index: 3;
            }

            li {
                margin-bottom: 10px;
            }
            footer {
                position: static !important;
            }
            .inputs {
                border-radius: 5px !important;
            }
            #header {
                position: initial !important;
            }
            .border-head {
                border-radius: 15px 15px 0px 0px !important;
            }
            .background {
                background: rgba(247, 217, 222, 0.50) !important;
            }
            .title-card {
                padding-top: 20% !important;
                color: white !important;
                font-weight: bold !important;
                text-shadow: -1px -1px 1px rgb(167, 162, 162), 1px -1px 1px rgb(167, 162, 162), -1px 1px 1px rgb(167, 162, 162), 1px 1px 1px rgb(167, 162, 162);
            }
            .paddings {
                padding: 0px !important;
            }
        </style>
    </head>
    <body class="bg-malla-corazones">
        <script type='text/javascript' data-cfasync='false'>window.purechatApi = {
                l: [], t: [], on: function() {
                    this.l.push(arguments);
                },
            };
            (function() {
                var done = false;
                var script = document.createElement('script');
                script.async = true;
                script.type = 'text/javascript';
                script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
                document.getElementsByTagName('HEAD').item(0).appendChild(script);
                script.onreadystatechange = script.onload = function(e) {
                    if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                        var w = new PCWidget({c: 'f3f97b28-73ac-4929-a04d-527e409169b7', f: true});
                        done = true;
                    }
                };
            })();
        </script>
        <script type="text/javascript">
            window._mfq = window._mfq || [];
            (function() {
                var mf = document.createElement('script');
                mf.type = 'text/javascript';
                mf.async = true;
                mf.src = '//cdn.mouseflow.com/projects/4315bf06-815c-4f86-b787-cb87d939a11e.js';
                document.getElementsByTagName('head')[0].appendChild(mf);
            })();
        </script>
        <div class="container" >
            <div class="row">
            <form method="POST" action="<?php echo base_url() ?>cuenta/empresa">
                <div class="card col l6 offset-l3" style="border-radius: 15px !important; padding-left: 0 !important; padding-right: 0 !important; margin-bottom: 65px !important;">
                    <!-- <div class="col l12 s12 m12" style="background: #ffdbe0; border-radius: 15px 15px 0 0px; padding-left: 0 !important; padding-right: 0 !important;">
                        <img src="<?= base_url() ?>dist/img/slider_home/logologin.png" style="width: 90%; margin-left: 5%; margin-right: 5%;">
                    </div> -->
                    <div class="col l12 s12 border-head paddings card-shadow"
                    style="background-image: url('<?php echo base_url() ?>dist/img/brideadvisor/registerProveedor.jpg');
                                    background-size: cover; background-repeat: no-repeat; height: 250px">
                        <!-- <img class="border-head" src="<?= base_url() ?>dist/img/brideadvisor/registerNovia.jpg" style="width: 100%;"> -->
                        <div class="border-head background col l12 s12 center-align" style="height: 250px;">
                            <div class="col l12 s12 m12">
                                <h4 class="title-card">EMPRESA</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col l12 s12 m12 center-align">
                        <br>
                        <p style="color: black !important; font-size: 18px; font-weight: bold; text-align: center;">Accede con tu usuario</p>
                        <br>
                        <div class="col l10 s12 m10 offset-l1">
                            <input class="inputs" id="email" placeholder="Usuario" type="text" required=""
                                    value="<?php echo isset($email) ? $email : "" ?>" name="primaryEmail"
                                    style="background: #E8E8E8 !important; border-bottom: unset !important; text-align:center;" autofocus>
                            <br>
                            <input class="inputs" type="password" required="" name="primaryPassword" placeholder="Contraseña" 
                                            style="background: #E8E8E8 !important; border-bottom: unset !important; text-align:center;">
                            <div class="chip red darken-1 white-text right"
                                    style="margin-bottom:20px ; width: 100%; <?php echo (isset($mensaje)) ? '' : 'display:none'; ?>">
                                <?php echo (isset($mensaje)) ? $mensaje : ''; ?>
                                <i class="material-icons">close</i>
                            </div>
                        </div>
                        <div class="col l12 s12 m12">
                            <p style="text-align: center; color: black;">
                                ¿Has olvidado tu contraseña?
                                <a class="dorado-2-text" href="<?php echo base_url()."soporte/contrasena" ?>">recuperala aqu&iacute;</a>
                            </p><br>
                            <button class="btn btn-block waves-effect waves-light dorado-2" type="submit"
                                name="action" style="width: 40% !important;">Ingresar
                            </button>
                            <br><br>
                            <!-- <button class="btn btn-block bg-fb" id="facebook-login" style="font-size: 0.8rem; width: 60% !important; margin-left: 20%;">
                                <i class="fa fa-facebook-official"></i> <span class="hide-on-small-only">Ingresa con </span>Facebook
                            </button> -->
                            <p style="text-align:center; color:black;">¿No tienes cuenta? <a style="color: #67B5EF !important;" href="<?php echo base_url()."registro" ?>">Reg&iacute;strate aquí</a></p>
                            <br>
                        </div>
                    </div>
                </div>
            </form>
            </div>
            <!-- <h5><a class="dorado-2-text" href="<?php echo base_url() ?>">REGRESAR</a></h5> -->
            <!-- <div class="row" style="display: flex; justify-content: center; align-items: center;">
                <div class="col s8 m6" style="margin: 0;">
                    <form method="POST" action="<?php echo base_url() ?>cuenta/">
                        <div class="card" style="border-radius: 20px !important; margin-top: 30% !important; margin-bottom: 10% !important;">
                            <div class="card-content" style="background-color: rgba(195,200,204,0.5); border-radius: 20px; background: white;">
                                <div style="background: pink; height: 50px;">

                                </div>
                                <ul>
                                    <li>
                                        <img class="responsive-img"
                                            src="<?php echo base_url() ?>/dist/img/japy_nobg.png"
                                            alt="Japy"/>
                                    </li>
                                    <li style="text-align: center;">
                                        <h6 style="font-weight: bold; font-size: 17px; color: black;">Accede con tu correo electr&oacute;nico</h6>
                                    </li>
                                    <br>
                                    <li>
                                        <input class="inputs" id="email" placeholder="Correo electrónico" type="text" required=""
                                               value="<?php echo isset($email) ? $email : "" ?>" name="primaryEmail"
                                               style="background: #E8E8E8 !important; border-bottom: unset !important; text-align:center;">
                                    </li>
                                    <li>
                                        <input class="inputs" type="password" required="" name="primaryPassword" placeholder="Contraseña" 
                                                style="background: #E8E8E8 !important; border-bottom: unset !important; text-align:center;">
                                    </li>
                                    <li>
                                        <div class="chip red darken-1 white-text right"
                                             style="margin-bottom:20px ; width: 100%; <?php echo (isset($mensaje)) ? '' : 'display:none'; ?>">
                                            <?php echo (isset($mensaje)) ? $mensaje : ''; ?>
                                            <i class="material-icons">close</i>
                                        </div>
                                    </li>
                                    <li style="width: 100%">
                                        <p style="text-align: center; color: black;">
                                            ¿Has olvidado tu contraseña?
                                            <a class="dorado-2-text" href="<?php echo base_url()."soporte/contrasena" ?>">recuperala aqu&iacute;</a>
                                        </p>
                                    </li>
                                    <br>
                                    <li class="center-align">
                                        <button class="btn btn-block waves-effect waves-light dorado-2" type="submit"
                                                name="action" style="width: 40% !important;">Ingresar
                                        </button>
                                    </li>
                                    <li class="center-align">
                                        <button class="btn btn-block bg-fb" id="facebook-login" style="font-size: 0.8rem; width: 60% !important;">
                                            <i class="fa fa-facebook-official"></i> <span class="hide-on-small-only">Ingresa con </span>Facebook
                                        </button>
                                    </li>
                                    <li>
                                        <p style="text-align:center;">¿No tienes cuenta? <a style="color: #67B5EF !important;" href="<?php echo base_url()."registro" ?>">Reg&iacute;strate aquí</a></p>
                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div> -->
        </div>
        <input type="hidden" id="base-url" value="<?php echo base_url() ?>">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="<?php echo base_url()."dist/js/facebook/login.js" ?>" type="text/javascript"></script>
        <script src="<?php echo base_url()."dist/js/jquery-2.1.4.min.js" ?>" type="text/javascript"></script>
        <script src="<?php echo base_url()."dist/js/materialize.min.js" ?>" type="text/javascript"></script>
        <?php $this->view("japy/prueba/footer"); ?>
        <script>
            $(document).ready(function() {
                // $( "#email" ).focus();
                $('.button-collapse').sideNav();
                $('.dropdown-button').dropdown();
            });
        </script>
    </body>
</html>