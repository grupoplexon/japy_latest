<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        <title>Inicia sesión - BrideAdvisor </title>
        <link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
        <meta name="viewport" content="width=device-width, user-scalable=no,  initial-scale=1">
        <link href="<?php echo base_url() ?>dist/img/clubnupcial.png" rel="icon" sizes="16x16">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <style>
            footer {
                position: fixed;
            }

            .red-text.darken-5 {
                color: #92151b !important;
            }

            .red.darken-5 {
                background: #94151c !important;
            }

            input[type=email], input[type=password], input[type=text] {
                width: 100%;
                text-indent: 15px;
                border: none;
                outline: 0;
                color: #3D3F4D;
                background-color: white !important;
                font-size: .9em;
                -webkit-appearance: none;
                height: 2rem;
                border-bottom: 1px solid #00BCDD !important;
                -webkit-box-shadow: 0 1px 0 0 #00BCDD !important;
                box-shadow: 0 1px 0 0 #00BCDD !important;
            }

            footer.page-footer {
                margin-top: 0px;
                padding-top: 10px;
                z-index: 3;
            }

            li {
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body class="bg-malla-corazones">
        <script type='text/javascript' data-cfasync='false'>window.purechatApi = {
                l: [], t: [], on: function() {
                    this.l.push(arguments);
                },
            };
            (function() {
                var done = false;
                var script = document.createElement('script');
                script.async = true;
                script.type = 'text/javascript';
                script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
                document.getElementsByTagName('HEAD').item(0).appendChild(script);
                script.onreadystatechange = script.onload = function(e) {
                    if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                        var w = new PCWidget({c: 'f3f97b28-73ac-4929-a04d-527e409169b7', f: true});
                        done = true;
                    }
                };
            })();
        </script>
        <script type="text/javascript">
            window._mfq = window._mfq || [];
            (function() {
                var mf = document.createElement('script');
                mf.type = 'text/javascript';
                mf.async = true;
                mf.src = '//cdn.mouseflow.com/projects/4315bf06-815c-4f86-b787-cb87d939a11e.js';
                document.getElementsByTagName('head')[0].appendChild(mf);
            })();
        </script>
        <div class="container" style="margin-top: 6%;">
            <h5><a class="dorado-2-text" href="<?php echo base_url() ?>">REGRESAR</a></h5>
            <div class="row" style="display: flex; justify-content: center; align-items: center;">
                <div class="col s8 m6" style="margin: 0;">
                    <form method="POST" action="<?php echo base_url() ?>cuenta/">
                        <div class="card">
                            <div class="card-content" style="background-color: rgba(195,200,204,0.5);">
                                <ul>
                                    <li>
                                        <img class="responsive-img"
                                             src="<?php echo base_url() ?>/dist/img/japy_nobg.png"
                                             alt="Japy"/>
                                        <div class="divider"></div>
                                    </li>
                                    <li>
                                        <h6 class="grey-text">Accede con tu correo electr&oacute;nico</h6>
                                    </li>
                                    <li>
                                        <small class="grey-text">CORREO ELECTR&Oacute;NICO</small>
                                        <input id="email" type="text" required=""
                                               value="<?php echo isset($email) ? $email : "" ?>" name="primaryEmail">
                                    </li>
                                    <li>
                                        <small class="grey-text">CONTRASE&Ntilde;A</small>
                                        <input type="password" required="" name="primaryPassword">
                                    </li>
                                    <li>
                                        <div class="chip red darken-1 white-text right"
                                             style="margin-bottom:20px ; width: 100%; <?php echo (isset($mensaje)) ? '' : 'display:none'; ?>">
                                            <?php echo (isset($mensaje)) ? $mensaje : ''; ?>
                                            <i class="material-icons">close</i>
                                        </div>
                                    </li>
                                    <li style="width: 100%">
                                        <p class="grey-text">
                                            ¿Has olvidado tu contraseña?
                                            <a class="dorado-2-text" href="<?php echo base_url()."soporte/contrasena" ?>">recuperala aqu&iacute;</a>
                                        </p>
                                    </li>
                                    <li class="center-align">
                                        <button class="btn btn-block waves-effect waves-light dorado-2" type="submit"
                                                name="action">Iniciar Sesi&oacute;n
                                        </button>
                                    </li>
                                    <li>
                                        <button class="btn btn-block bg-fb" id="facebook-login" style="font-size: 0.8rem;">
                                            <i class="fa fa-facebook-official"></i> <span class="hide-on-small-only">Continuar con </span>Facebook
                                        </button>
                                    </li>
                                    <li>
                                        <div class="center-align grey-text">¿No tienes cuenta? registrate aqu&iacute;</div>
                                        <a class="btn-flat waves-effect btn-block dorado-2-text"
                                           style=" width: 100%;text-align: center;font-weight: bold"
                                           href="<?php echo base_url()."registro" ?>">REG&Iacute;STRATE</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <input type="hidden" id="base-url" value="<?php echo base_url() ?>">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="<?php echo base_url()."dist/js/facebook/login.js" ?>" type="text/javascript"></script>
        <script src="<?php echo base_url()."dist/js/jquery-2.1.4.min.js" ?>" type="text/javascript"></script>
        <script src="<?php echo base_url()."dist/js/materialize.min.js" ?>" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                $( "#email" ).focus();
                $('.button-collapse').sideNav();
                $('.dropdown-button').dropdown();
            });
        </script>
    </body>
</html>