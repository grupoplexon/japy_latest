<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url() ?>dist/css/brideAdvisor/magazine.css" rel="stylesheet" type="text/css"/>
    <title><?php echo $post->titulo ?></title>
    <?php 
        $url         = $this->config->base_url()."blog/home/index";
        $url_img     = $this->config->base_url()."blog/resource/imagen/";
        $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
    <title>Magazine</title>
    <?php $this->view("japy/prueba/header"); ?>

</head>
<style>
    body {
        font-family: 'Raleway', sans-serif !important;
        color: #757575;
    }
    hr {
        width: 150px;
        border: 1.5px solid #F8DADF;;
    }
    .contenedor{
        position: relative;
        text-align: center;
    }
    .contenedor img{
        width:100%;
        height: 400px;
        object-fit: cover;
    }

    .contenedor .centrado{
        position: absolute;
        top: 84%;
        left: 50%;
        width:100%;
        height: 150px;
        padding: 5px;
        transform: translate(-50%, -50%);
        background: rgba(255,255,255,.9) !important;
    }
    /* .title-articulo h4{
        font-size: 4vh;
        font-weight: bold;
    } */
    .centrado h4{
        color: black;
        font-weight: bold;
        font-size: 3vh;
    }
    .parrafo-l{
        width: 34vw;
        float: left;
        text-align: justify;
    }
    .parrafo-r{
        width: 34vw;
        float: right;
        text-align: justify;
    }
    #header{
        position: relative !important;
    }
    .background {
        
    }
    .post {
        height: 470px !important;
    }
    body{
        background-image: url('<?php echo base_url()?>dist/img/fondo.jpg');
        background-size: contain !important;
    }
</style>
<body>
    <div class=" background" style=" ">
        <div class="row center-align" style="margin-bottom: unset; margin-top: 5vh;">
            <div class="col l12">
                <h3 style=""><?php echo $post->titulo ?></h3>
                <hr>
            </div>
        </div>
        <div id="contenido" style="min-height: 200px;width:85%;margin:auto;">
            <p class="content-post" style="color: #757575"><?php echo $post->contenido ?></p>
        </div>
        <div class="row center-align ">
            <div class="row div-articulos center-align">
                <h5 class="title"> PODRÍA INTERESARTE TAMBIÉN</h5>
            </div>
            <div class="col l10 offset-l1 " style="margin-bottom: 50px;">
                <div class="conta" style="margin-bottom:50px;">
                <?php foreach ($destacados as $dest) : ?>
                    <div class="col l4">
                        <div class="row card post" >
                                <div class="col l12 ">
                                    <br>
                                    <?php foreach ($categories as $cat) : ?>
                                        <?php if($dest->tipo!=null && $dest->tipo==$cat->id_categoria){ ?>
                                            <h6 class="contenido"><?php echo strip_tags($cat->name_category) ?></h6>            
                                        <?php }?>
                                    <?php endforeach; ?>
                                    <h5 class="descrip"><?php echo strip_tags($dest->titulo) ?></h5>
                                    <!-- <h6 class="contenido"><?php echo substr(strip_tags($dest->contenido), 0, 150) ?>...</h6> -->
                                </div>
                                <div class="col l12 center-align">
                                    <a style="" href="<?php echo base_url() ?>magazine/blog/<?php echo $dest->id_blog_post ?>"><img class="art-img"
                                        src="<?php echo $dest->id_imagen_destacada ? $url_img.$dest->id_imagen_destacada : $url_default ?>"></a>
                                </div>
                                <div class="col l12 ">
                                    <!-- <h5 class="descrip"><?php echo strip_tags($dest->titulo) ?></h5> -->
                                    <h6 class=""><?php echo substr(strip_tags($dest->contenido), 0, 150) ?>...</h6>
                                </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                
                </div>
                <div style="margin-bottom: 50px;">
                <br>
                <br>
                </div>
            </div>
        </div>
    </div>
</body>
<?php $this->view("japy/prueba/footer"); ?>
<script src="<?php echo base_url() ?>dist/js/brideadvisor/magazine.js"></script>
</html>