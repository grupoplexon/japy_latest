<style>
.page-footer {
    background-color: #72D6E0;
}
#footer a {
    font-size: 25px !important;
    color: #fafafa !important;
    margin: 5%;
}
.footer-copyright{
        height: 100px !important;
    }
.icon-style {
    color: white !important;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px){
    .footer-copyright{
        height: 200px !important;
    }
    .japy{
        margin-bottom: -30px;
    }
}
</style>
<div id="footer">
    <footer style="padding-top: unset;" class="page-footer">
        <!-- <div class="container">
        <div class="row">
            <div class="col l6 s12">
            <h5 class="white-text">Footer Content</h5>
            <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
            </div>
            <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Links</h5>
            <ul>
                <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
            </ul>
            </div>
        </div>
        </div> -->
        <div class="footer-copyright" style="">
            <div class="container" style="color: #05622a;">
            <div class="row center-align" style="margin-top: 40px">
                <div class="col s12 m6 l4 japy" >
                    <div class="row  center-align" style="">
                            <a href="<?php echo base_url() ?>">
                                    <img src="<?php echo base_url() ?>/dist/img/japy_nobg_white.png" style="height:70px; width: 100px;"
                                        alt="Japy">
                            </a>
                    </div>
                </div>
                <div class="col s12 m6 l4" >
                    <div class="row  center-align" style="height: 20px; padding-top: 5%">
                            <h6 class="center-align" style="color:white; font-size: .8rem">JAPY© 2019 DERECHOS RESERVADOS</h6>
                    </div>
                </div>
                <div class="col s12 m6 l4" >
                    <div class="row center-align" style=" padding-top: 5%">
                    <!-- <div class="col s3 m3 l2 offset-l2 "> -->
                        <a href="https://www.facebook.com/Japy-177297236307569/" target="_blank">
                            <i class="fa fa-facebook  icon-style"></i>
                        </a>
                    <!-- </div>
                    <div class="col s3 m3 l2 no-margin-element"> -->
                        <a href="https://twitter.com/japymx/" target="_blank">
                            <i class="fa fa-twitter icon-style"></i>
                        </a>
                    <!-- </div>
                    <div class="col s3 m3 l2 no-margin-element"> -->
                        <a href="https://www.instagram.com/japymx/" target="_blank">
                            <i class="fa fa-instagram icon-style"></i>
                        </a>
                    <!-- </div>
                    <div class="col s3 m3 l2 no-margin-element"> -->
                        <a href="https://co.pinterest.com/japymx/" target="_blank">
                            <i class="fa fa-pinterest-p icon-style"></i>
                        </a>
                    <!-- </div> -->
                    </div>
                </div>
            </div>
            <!-- <a class="grey-text text-lighten-4 right" href="#!">More Links</a> -->
            </div>
        </div>
    </footer>
</div>





<!--<script src="<?php //echo base_url() ?>dist/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="<?php //echo base_url() ?>dist/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php //echo base_url() ?>dist/js/datatables.min.js" type="text/javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<!-- <script src="<?php echo base_url() ?>dist/js/materealize.min.js" type="text/javascript"></script> -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="<?php echo base_url() ?>dist/js/jquery.Jcrop.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/classie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/slider.min.js" type="text/javascript"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="<?php echo base_url() ?>dist/js/inputFormat.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.touchSwipe.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/facebook.min.js" type="text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-40486092-9"></script>
<link href="<?php echo base_url() ?>dist/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo base_url() ?>dist/enjoyhint/enjoyhint.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/tutorial.min.js"></script>
<script src="<?php echo base_url() ?>dist/slider-pro-master/dist/js/jquery.sliderPro.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/parsley.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/i18n/es.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-40486092-9');
</script>
<script>
    $(document).ready(function() {
        if (typeof onReady != 'undefined') {
            onReady();
        }
        $('select').material_select();
        $('.button-collapse').sideNav();
        $('.dropdown-button').dropdown();
        $('.modal').modal();
        $('.collapsible').collapsible({
            accordion: false,
        });

        $('.multiple-items').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            dots: false,
            autoplaySpeed: 5000,
            arrows: false,
            adaptiveHeight: true,
        });

        $('.center-slide').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            arrow: false,
            dots: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3,
                    },
                },
                {
                    breakpoint: 440,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                    },
                },
            ],
        });

        $('#slider-home').sliderPro({
            width: '100%',
            autoHeight: true,
            buttons: false,
        });
    });

    function generoImage(edad, sexo) {
        switch (parseInt(sexo)) {
            case 1:
                //HOMBRE
                switch (parseInt(edad)) {
                    case 1:
                        //ADULTO
                        return 'invitados invitados-Hombre x2';
                        break;
                    case 2:
                        //NINO
                        return 'invitados invitados-Nino x2';
                        break;
                    case 3:
                        //BEBE
                        return 'invitados invitados-Bebe-nino x2';
                        break;
                }
                break;
            case 2:
                //MUJER
                switch (parseInt(edad)) {
                    case 1:
                        //ADULTO
                        return 'invitados invitados-Mujer x2';
                        break;
                    case 2:
                        //NINO
                        return 'invitados invitados-Nina x2';
                        break;
                    case 3:
                        //BEBE
                        return 'invitados invitados-Bebe-nina x2';
                        break;
                }
                break;
        }
    }
</script>
