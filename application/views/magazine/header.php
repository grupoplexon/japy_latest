<style>
    p {
        margin: 0;
        color: #8dd7e0;
    }
    .menu-background {
        background: white;
    }
    .top-hr {
        border: 4px solid;
        border-color: #8dd7e0;
    }
    .bottom-hr {
        border: 1px solid;
        border-color: #8dd7e0;
    }
    .overwritten-hr {
        width: 100% !important;
    }
    .def-color {
        color: #8dd7e0;
    }
    .single-menu-item {
        display: flex;
        height: 100px;
    }
    .multiple-menu-item {
        padding-top: 1% !important;
        height: 100px !important;
    }
    .menu-icon {
        width:100%;
    }
    .menu-icon-text {
        margin-top: -15%;
        font-size: x-small;
        font-weight: bolder;
    }
    .menu-search-bar {
        width: 100%;
        border-radius: 5px;
        height: 35px;
    }
    .menu-divider {
        border-right: 1px solid lightgray;
    }
    .row-no-margin {
        margin: 0 !important;
    }
    .text-x-small {
        font-size: x-small
    }
    .text-section-indicator {
        font-weight: bolder;
        opacity: 0.5;
    }
    .row .col.l3 {
        width: 13% !important;
    }
</style>
<?php
    $this->view("magazine/header_includes.php");
?>
<div class="row">
    <div class="col s12 m12 l12 menu-background">
        <hr class="top-hr overwritten-hr">

        <div class="col s4 m4 hide-on-large-only">
            <a href="#" class="sidenav-trigger" data-activates="navbar-secondary-menu">
                <i class="material-icons def-color">menu</i>
            </a>
        </div>

        <div class="col s4 m4 l2 valign-wrapper single-menu-item">
            <a href="<?php echo base_url() ?>">
                <img src="<?php echo base_url() ?>dist/img/japy_copy.png" style="width: 100%">
            </a>
        </div>

        <div class="col l2 valign-wrapper hide-on-med-and-down single-menu-item">
            <div class="col l3">
                <a href="#" class="sidenav-trigger" data-activates="navbar-main-menu">
                    <i class="material-icons def-color">menu</i>
                </a>
            </div>

            <div class="col l9">
            <a href="<?php echo base_url() ?>magazine"> <h4 class="def-color">Magazine</h3></a>
            </div>
        </div>

        <div class="col l4 hide-on-med-and-down multiple-menu-item">
            <div class="row row-no-margin">
                <div class="offset-l1 col l2">
                    <a href="<?php echo base_url() ?>magazine?tipo=4">
                    <img class="menu-icon" src="<?php echo base_url() ?>dist/img/iconos/magazine/creadores-video.png">
                    <p class="center-align menu-icon-text">Creadores</p>
                    </a>
                </div>

                <div class="col l2 menu-divider">
                    <a href="<?php echo base_url() ?>magazine?tipo=5">
                    <img class="menu-icon" src="<?php echo base_url() ?>dist/img/iconos/magazine/backstage-video.png">
                    <p class="center-align menu-icon-text">Back Stage</p>
                    </a>
                </div>

                <div class="col l2">
                    <a href="<?php echo base_url() ?>magazine?tipo=1">
                    <img class="menu-icon" src="<?php echo base_url() ?>dist/img/iconos/magazine/5minutos-editorial.png">
                    <p class="center-align menu-icon-text">5 minutos</p>
                    </a>
                </div>

                <div class="col l2">
                    <a href="<?php echo base_url() ?>magazine?tipo=2">
                    <img class="menu-icon" src="<?php echo base_url() ?>dist/img/iconos/magazine/bridaltrend-editorial.png">
                    <p class="center-align menu-icon-text">Bridal Trend</p>
                    </a>
                </div>

                <div class="col l2">
                    <a href="<?php echo base_url() ?>magazine?tipo=3">
                    <img class="menu-icon" src="<?php echo base_url() ?>dist/img/iconos/magazine/articulos-editorial.png">
                    <p class="center-align menu-icon-text">Artículos</p>
                    </a>
                </div>
            </div>

            <div class="row text-x-small text-section-indicator">
                <div class="col l6 center-align">
                    Videos
                </div>
                <div class="col l4 center-align">
                    Articulos
                </div>
            </div>
        </div>

        <form class="col l2 ui-widget valign-wrapper hide-on-med-and-down single-menu-item"
            method="GET" action="<?php echo base_url()?>magazine/buscar">
            <input type="text" name="buscar" id="buscador" class="browser-default menu-search-bar">
            <!-- <i class="fas fa-search"></i> -->
            <!-- <button type="submit" class="btn fa fa-twitter icon-style"  > </button> -->
        </form>

        <div class="col l1 valign-wrapper hide-on-med-and-down single-menu-item">
            <a href="<?php echo base_url() ?>brideweekend">
                <img style="width:120px" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/bw_logo.png">
            </a>
        </div>

        <div class="col s4 m4 l1 valign-wrapper single-menu-item">
            <?php if ($this->checker->isLogin()) { ?>
                    <div id="inicio_registro" class="" style="position: relative;z-index: 90;">
                        <div>
                            <div class="row clickable dropdown-button waves-effect user-information center-vertical" data-beloworigin="true"
                                 data-activates='dropdown-login' style="margin: 0;display: flex;">
                                <div class="col m5  s5  l5 " style="text-align: right;width: 80px;">
                                    <img class="circule-img"
                                         src="<?php echo base_url('perfil/foto/') ?>/<?php echo $this->session->userdata("id_usuario") ?>"
                                         alt=""/>
                                </div>
                                <!-- <div class="col l5 hide-on-med-and-down">
                                    <h6 class="primary-text" style="font-weight: bold;margin: 0;">
                                        <?php echo $this->session->userdata("nombre") ?>
                                    </h6>
                                    <p style="font-size: 12px;margin: 0;" class="primary-text"><?php echo $this->session->userdata("genero") ?></p>
                                </div> -->
                                <i id="burger" class="material-icons primary-text hide-on-med-and-down"
                                   style="position: absolute; top: 0%;display: block;right: 6px;">menu</i>
                                <div class="col m1 s1" style="position: relative">
                                </div>
                            </div>
                            <ul id='dropdown-login' class='dropdown-content'
                                style="z-index:30000000 !important;overflow-x: hidden;min-width: 305px;">
                                <?php if ($this->checker->isNovio()) { ?>
                                    <li><a class="grey-text darken-5" href="<?php echo base_url('novia') ?>">
                                            <i class="material-icons left primary-text">dashboard</i> Mi organizador<i
                                                    class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                    </li>
                                    <li class=""
                                        style="background: #514f50!important; color: white;    padding: 14px 16px;">
                                        <div class="row" style="margin: 0px">
                                            <div class="col s4 align-center">
                                                <div class="align-center">
                                                    <i class="material-icons center primary-text">check_circle</i>&nbsp;&nbsp;<b><?php echo $this->checker->getTareasCompletadas() ?></b>
                                                </div>
                                                <small><b>Tareas completadas</b></small>
                                            </div>
                                            <div class="col s4 align-center">
                                                <div class="align-center">
                                                    <i class="material-icons center primary-text">people_outline</i>&nbsp;&nbsp;<b><?php echo $this->checker->getInvitadosConfirmados() ?></b>
                                                </div>
                                                <small><b>Invitados confirmados</b></small>
                                            </div>
                                            <div class="col s4 align-center">
                                                <div class="align-center">
                                                    <i class="material-icons center primary-text">favorite_border</i>&nbsp;&nbsp;<b><?php echo $this->checker->getProveedoresReservados() ?></b>
                                                </div>
                                                <small><b>Proveedores reservados</b></small>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="grey-text darken-5" href="<?php echo base_url('novios/buzon') ?>">
                                            <i class="material-icons left primary-text">mail_outline</i>
                                            Mi buz&oacute;n<i
                                                    class="material-icons grey-text lighten-4 right">chevron_right</i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="grey-text darken-5" href="<?php echo base_url('novios/presupuesto') ?>">
                                            <i class="material-icons left primary-text">exposure</i> Mi
                                            presupuesto<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="grey-text darken-5" href="<?php echo base_url('Novia') ?>">
                                            <i class="material-icons left primary-text">face</i>
                                            Mi perfil
                                            <i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="grey-text darken-5" href="<?php echo base_url('novios/tarea') ?>">
                                            <i class="material-icons left primary-text">content_paste</i>
                                            Mi Agenda<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="grey-text darken-5" href="<?php echo base_url('novios/proveedor') ?>">
                                            <i class="material-icons left primary-text"
                                            >class</i> Mis proveedores<i
                                                    class="material-icons grey-text lighten-4 right">chevron_right</i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="grey-text darken-5" href="<?php echo base_url('novios/invitados') ?>">
                                            <i class="material-icons left primary-text"
                                            >people_outline</i> Mis invitados<i
                                                    class="material-icons grey-text lighten-4 right">chevron_right</i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="grey-text darken-5" href="<?php echo base_url('novios/perfil') ?>">
                                            <i class="material-icons left primary-text">settings</i> Mi
                                            cuenta<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                <?php } else {
                                    if ($this->checker->isAdmin()) { ?>
                                        <li><a class="grey-text darken-5" href="<?php echo base_url('App') ?>">
                                                <i class="material-icons left" style="color:#f4d266!important">dashboard</i>
                                                Panel de administraci&oacute;n<i
                                                        class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                        </li>
                                    <?php } else {
                                        if ($this->checker->isProveedor()) { ?>
                                            <li><a class="grey-text darken-5" href="<?php echo base_url('proveedor') ?>">
                                                    <i class="material-icons left"
                                                       style="color:#f4d266!important">dashboard</i>
                                                    Panel de Proveedor<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                            </li>
                                        <?php } else {
                                            if ($this->checker->isModerador()) { ?>
                                                <li><a class="grey-text darken-5"
                                                       href="<?php echo base_url('novios/moderador') ?>">
                                                        <i class="material-icons left"
                                                           style="color:#f4d266!important">web</i> Comunidad<i
                                                                class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                                </li>
                                            <?php }
                                        }
                                    }
                                } ?>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo base_url()."cuenta/logout" ?>">
                                        <i class="material-icons left primary-text">close</i>
                                        Cerrar Sesi&oacute;n
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
        </div>
        <hr class="col s12 m12 l12 bottom-hr hide-on-med-and-down">
    </div>

    <div class="col s12 m12 hide-on-large-only center-align">
        <h6 class="primary-text" style="font-weight: bold;margin: 0; color: black !important; opacity: 0.5;">
            <?php echo $this->session->userdata("nombre") ?>
        </h6>
        <p style="font-size: 12px;margin: 0; color: black !important; opacity: 0.5;" class="primary-text"><?php echo $this->session->userdata("genero") ?></p>
    </div>
</div>
<ul id='navbar-main-menu' class='side-nav'>
    <li><a href="<?php echo base_url('novia') ?>">Mi Boda</a></li>
    <li><a href="<?php echo base_url()."proveedores" ?>">Proveedores</a></li>
    <li><a href="<?php echo base_url('blog') ?>">Magazine</a></li>
    <li><a href="<?php echo base_url() ?>home/expo_eventos">Eventos</a></li>
    <li><a href="<?php echo base_url() ?>brideweekend">BrideWeekend</a></li>
    <li><a href="<?php echo base_url("home/planeador_bodas") ?>">Conocenos</a></li>
    <li><a href="<?php echo base_url("home/altaEmpresas") ?>">Empresa</a></li>
 </ul>

 <ul id='navbar-secondary-menu' class='side-nav'>
     <li><a href="#">Creadores</a></li>
     <li><a href="#">Back Stage</a></li>
     <li><a href="#">5 Minutos</a></li>
     <li><a href="#">Bridal Trend</a></li>
     <li><a href="#">Articulos</a></li>
 </ul>

 <script type="text/javascript">
    $(document).ready(function(){
        $(".sidenav-trigger").sideNav();
    });
    $( function() {
    var availableTags = [
      "ActionScript: 1",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];

    base_url = '<?= base_url()? base_url() : ""; ?>';


    $( "#buscador" ).autocomplete({
        source: base_url+'magazine/tags',
        select: (event, ui) => {
            // this.search = ui.item.value;
            $("#resultado").val(ui.item.value);
            // this.getInspirations();
        }
    });

    // $('#buscador').keypress(function(event){
    //     var keycode = (event.keyCode ? event.keyCode : event.which);
    //     if(keycode == '13'){
    //        // alert($('#buscador').val()); 
    //         buscar = $('#buscador').val();
    //         $.ajax({
    //                 url: '<?php echo base_url() ?>'+'magazine/buscar',
    //                 method: 'get',
    //                 data: {
    //                     tag: buscar,
    //                 },
    //                 success: function(response) {
    //                     location.reload();
    //                 },
    //                 error: function(e) {
    //                     console.log("ERROR");
    //                 },
    //             });
    //     }
    // });


    // $( "#buscador" ).autocomplete({
    //   source: availableTags
    // });
  } );
 </script>
