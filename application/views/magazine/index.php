<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Magazine - BrideAdvisor</title>
    <?php $this ->view("magazine/header"); ?>
    <?php 
    $url         = $this->config->base_url()."blog/home/index";
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
</head>
<style>
body{
    font-family: 'Questrial', sans-serif !important;
}
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.destacado{
    background: #eee !important;
    width: 100%;
    padding-left: 10%;
    padding-right: 10%;
}
.contenedor{
    position: relative;
    text-align: center;
}
.contenedor img{
    width:100%;
    height: 400px;
    object-fit: cover;
}

.contenedor .centrado{
    position: absolute;
    top: 83%;
    left: 50%;
    width: 100%;
    height: 146px;
    padding-left: 5%;
    padding-right: 5%;
    transform: translate(-50%, -50%);
    background: rgba(255,255,255,.8) !important;
}
.contenedor2{
    position: relative;
    text-align: center;
}
.contenedor2 img{
    width:100%;
    height: 300px;
    object-fit: cover;
}
.contenedor2 .centrado{
    position: absolute;
    top: 15%;
    left: 30%;
    width:70%;
    float: left;
    /* height: 200px; */
    padding: 5px;
    /* transform: translate(-50%, -50%); */
    background: rgba(255,255,255,.8) !important;
}
.centrado h4{
    color: black;
    font-weight: bold;
    font-size: 3vh;
}
.contenedor2 .centrado h4{
    text-align: justify;
}
.centrado h5{
    color: black;
    font-size: 1.5rem;
    font-weight: bold;
    text-align: justify;
}
.recientes{
    padding-left: 10%;
    padding-right: 10%;
}
.title-articulo h5{
    font-size: 1.3rem;
    font-weight: bold;
    color: #757575 !important;
}
h3{
    font-size: 2rem !important;
}
.content hr{
    text-align: center;
    width: 13%;
    margin-top: -15px;
    border: 1.5px solid ;
    color: #6dc9d2;
}
.grises {
    filter: url('#grayscale'); /* Versión SVG para IE10, Chrome 17, FF3.5, Safari 5.2 and Opera 11.6 */
    -webkit-filter: grayscale(100%);
    -moz-filter: grayscale(100%);
    -ms-filter: grayscale(100%);
    -o-filter: grayscale(100%);
    filter: grayscale(100%); /* Para cuando es estándar funcione en todos */
    filter: Gray(); /* IE4-8 and 9 */

    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}

.contenedor2 p{
    text-align: left;
    color: black !important;
}

.ui-menu .ui-menu-item {
    color: #416367;
    /* background: #ceedf1; */
    /* border: 1px solid #000; */
    border-bottom: 1px solid #8dd7e0;
}

.ui-widget.ui-widget-content {
    border: 1px solid #8dd7e0;
    background: #f5fafb;
    border-radius: 6px;
}

@media only screen and  (min-width: 2000px) and  (max-width: 3000px){
    body{
        margin-left: 15%;
        margin-right: 15%;
    }
    .centrado h4 {
        font-size: 1.3rem;
    }
}

@media only screen and  (min-width: 300px) and  (max-width: 500px){
    .recientes{
        padding-left: 1% !important;
        padding-right: 1% !important;
    }
    .contenedor2 img{
        width:100%;
        height: 200px;
        object-fit: cover;
    }
    .contenedor2 .centrado{
        position: absolute;
        top: 10%;
        left: 30%;
        width:70%;
        float: left;
        /* height: 150px; */
        /* transform: translate(-50%, -50%); */
    }
    .contenedor2 .centrado h4{
        font-size: 2vh;
    }
    .contenedor2 p{
        font-size: 1.5vh;
    }
    .centrado h5{
        font-size: 2.5vh;
    }
    .centrado h4{
        font-size: 2.7vh;
    }
    h3{
        font-size: 5vh;
    }
    .contenedor img{
        height: 350px;
    }
    .contenedor .centrado{
        top: 81%;
        height: 150px;
    }
    .title-articulo h5{
        font-size: 1.5rem;
    }
}
</style>

<body>
    <div class="content center-align">
        <?php if (isset($destacados)) { ?>
        <div class="row destacado" style="margin-bottom: 50px;">
            <br>
            <h3> Artículos destacados </h3>
            <hr>
            <br>
            <div class="conta" style="margin-bottom:50px;">
            <?php foreach ($destacados as $dest) : ?>
                <div class="col s12 m6 l4" >
                    <div class="contenedor ">
                        <div class="title-articulo">
                        <!-- <a href="<?php echo base_url() ?>blog/post/ver/<?php echo $dest->id_blog_post ?>"><h5> <?php echo strip_tags($dest->titulo) ?></h5></a> -->
                        <a href="<?php echo base_url() ?>magazine/blog/<?php echo $dest->id_blog_post ?>"><h5> <?php echo strip_tags($dest->titulo) ?></h5></a>
                        </div>
                        <!-- <a href="<?php echo base_url() ?>blog/post/ver/<?php echo $dest->id_blog_post ?>"> <img class="" src="<?php echo $dest->id_imagen_destacada ? $url_img.$dest->id_imagen_destacada : $url_default ?>"/></a> -->
                        <a href="<?php echo base_url() ?>magazine/blog/<?php echo $dest->id_blog_post ?>"> <img class="" src="<?php echo $dest->id_imagen_destacada ? $url_img.$dest->id_imagen_destacada : $url_default ?>"/></a>

                        <div class="centrado">
                            <h4> <?php echo substr(strip_tags($dest->contenido), 0, 150) ?>...</h4>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            
            </div>
        </div>
        <?php } ?>
        <div class="row recientes">
            <h3> Artículos recientes </h3>
            <hr>
            <br>
            <?php if (isset($posts)) { ?>
            <?php  foreach ($posts as $post) { ?>
                <div class="col s12 l6 card-blog">
                    <div class="contenedor2 grises">
                        <!-- <a class="img-nuevo" href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>"> <img class="img-destacada" src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>" /></a> -->
                        <a class="img-nuevo" href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>"> <img class="img-destacada" src="<?php echo $post->id_imagen_destacada ? $url_img.$post->id_imagen_destacada : $url_default ?>" /></a>
                        <div class="centrado">
                            <!-- <a class="card-title p-title" href="<?php echo base_url() ?>blog/post/ver/<?php echo $post->id_blog_post ?>"> -->
                            <a class="card-title p-title" href="<?php echo base_url() ?>magazine/blog/<?php echo $post->id_blog_post ?>">
                            <h5 class="card-title p-title"> <?php echo strip_tags($post->titulo) ?></h5>
                            </a>
                            <p class="por"> Por <?php echo $post->autor->nombre ?></p>
                            <h4 class="p-content"> <?php echo substr(strip_tags($post->contenido), 0, 150) ?> ... </h4>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- <ul class="pagination">
                        <li class="<?php echo($page == 1 ? "disabled" : "waves-effect") ?>"><a
                                    href="<?php echo "$url/$cat/".($page - 1) ?>"><i
                                        class="material-icons">chevron_left</i></a>
                        </li>
                        <?php for ($i = 1; $i <= $total_paginas; $i++) { ?>
                            <li class="<?php echo($i == $page ? "active" : "waves-effect") ?>"><a
                                        href="<?php echo "$url/$cat/$i" ?>"><?php echo $i ?></a></li>
                        <?php } ?>
                        <li class="<?php echo($page == $total_paginas ? "disabled" : "waves-effect") ?>">
                            <a href="<?php echo "$url/$cat/".($page + 1) ?>">
                                <i class="material-icons">chevron_right</i>
                            </a>
                        </li> -->
             <div class="isContainer" style="text-align: center;">
                    <span class="onLoad">Cargando...</span>
            </div>
            <?php } else { ?>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="card-panel teal">
                                    <span class="white-text">A&uacute;n no hay publicaciones en esta categor&iacute;a intenta m&aacute;s tarde
                                    </span>
                                    </div>
                                </div>
                            </div>
            <?php } ?>
            
        
        </div>
        <br>
    
    </div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
            var page = 1;
            var totPages = parseInt("<?php echo $total_paginas ?>");
            var urlD = "<?php echo "$url/$cat/" ?>";
            // var blogUrl = "<?php echo base_url() ?>" + 'blog/post/ver/';
            var blogUrl = "<?php echo base_url() ?>" + 'magazine/blog/';
            var url_img = "<?php echo $url_img ?>";
            var url_default = "<?php echo $url_default ?>";
            var detected = false;

            $(window).scroll(function() {
                var top_of_element = $('.onLoad').offset().top;
                var bottom_of_element = $('.onLoad').offset().top + $('.onLoad').outerHeight();
                var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
                var top_of_screen = $(window).scrollTop();

                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) && (!detected)) {
                    detected = true;
                    this.addMore();
                }
                else {
                    // console.log("El elemento no esta visible");
                }
            });
            function addMore() {
                page += 1;

                if (page <= totPages) {
                    $.when($.ajax({
                        url: urlD + page,
                        headers: {'Accept': 'application/json'},
                        success: function(response) {
                            console.log('ok');
                        },
                    })).done(function(data) {
                        detected = false;
                        $.each(data, function(i, dat) {
                            var content = dat.contenido.replace(/(<([^>]+)>)/ig, '');
                            var newLGCard = $('.card-blog:last').clone(true);
                            var imgToLoad = url_default;
                            //Estilos aplicados para cambiar la imagen
                            if (dat.id_imagen_destacada) {
                                imgToLoad = url_img + dat.id_imagen_destacada;
                            }
                            //Cargado de Tarjetas(Card) pantallas grandes
                            newLGCard.find('h5.card-title').html(dat.titulo.replace(/(<([^>]+)>)/ig, ''));
                            newLGCard.find('a.card-title').attr('href', blogUrl + dat.id_blog_post);
                            newLGCard.find('a.img-nuevo').attr('href', blogUrl + dat.id_blog_post);
                            newLGCard.find('p.por').html('Por ' + dat.autor.nombre);
                            newLGCard.find('h4.p-content').html(content.substring(0, 150) + '...');

                            if (dat.id_imagen_destacada) {
                                newLGCard.find('img.img-destacada').attr('src', imgToLoad);
                            }
                            newLGCard.insertBefore('.isContainer');
                        });
                    });
                }
                else {
                    $('.onLoad').html('No quedan más elementos por mostrar');
                }
            }
</script>
</body>
<?php $this->view("magazine/footer"); ?>
</html>