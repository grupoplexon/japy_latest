<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Magazine - BrideAdvisor</title>

    <?php 
        $url         = $this->config->base_url()."blog/home/index";
        $url_img     = $this->config->base_url()."blog/resource/imagen/";
        $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
    <title>Magazine</title>
    <?php $this->view("japy/prueba/header"); ?>
    <link href="<?php echo base_url() ?>dist/css/brideAdvisor/magazine.css" rel="stylesheet" type="text/css"/>
    <style>
        
    </style>
</head>
<body>
    
</body>
    <div class="content">
        <div class=" background" style=" background-image: url('<?php echo base_url()?>dist/img/fondo.jpg');">
            <div class="row search background" style="position: relative; ">
                <a href="<?php echo base_url('magazine') ?>" class=""><img class="img-home " style="width:100%;" src="<?php echo base_url() ?>dist/img/slider_home/magazine.png"></a>
                <div class="col l10 offset-l1 center-align menu-magazine " >
                     <div style=""> 
                        <a href="<?php echo base_url('magazine') ?>?tipo=moda"> <h5 class="title opcion space" style="color: #515151;">MODA</h5> </a>
                        <div class="vl hide-on-med-and-down"></div>
                        <a href="<?php echo base_url('magazine') ?>?tipo=belleza"> <h5 class="title opcion space" style="color: #515151;">BELLEZA</h5></a>
                        <div class="vl hide-on-med-and-down"></div>
                        <a href="<?php echo base_url('magazine') ?>?tipo=destinos"> <h5 class="title opcion space" style="color: #515151;">DESTINOS DE BODA</h5></a>
                        <div class="vl hide-on-med-and-down"></div>
                        <a href="<?php echo base_url('magazine') ?>?tipo=espectaculos"> <h5 class="title opcion space" style="color: #515151;">ESPECTACULOS</h5></a>
                        <div class="vl hide-on-med-and-down"></div>
                        <a href="<?php echo base_url('magazine') ?>?tipo=hogar"> <h5 class="title opcion space" style="color: #515151;">HOGAR</h5></a>
                        <div class="vl hide-on-med-and-down"></div>
                        <a href="<?php echo base_url('magazine') ?>?tipo=novia"> <h5 class="title opcion space" style="color: #515151;">NOVIA</h5></a>
                     </div> 
                </div>
            </div>
            <br>
            <?php if (isset($destacados)) { ?>
            <div class="row div-articulos div-recientes center-align">
                <h5 class="title">ARTICULOS RECIENTES</h5>
            </div>
            <br>
            <div class="row center-align destacados">

                    <div class="col l10 offset-l1" style="margin-bottom:50px;">
                    <?php foreach ($destacados as $dest) : ?>
                        <div class="col l4 m6 s12">
                            <div class="row card post" >
                                    <div class="col l12 center-align">
                                        <a style="" href="<?php echo base_url() ?>magazine/blog/<?php echo $dest->id_blog_post ?>"><img class="art-img"
                                            src="<?php echo $dest->id_imagen_destacada ? $url_img.$dest->id_imagen_destacada : $url_default ?>"></a>
                                    </div>
                                    <div class="col l12 ">
                                        <br>
                                        <?php foreach ($categories as $cate) : ?>
                                            <?php if($dest->tipo!=null && $dest->tipo==$cate->id_categoria){ ?>
                                                <h6 class="contenido"><?php echo strip_tags($cate->name_category) ?></h6>            
                                            <?php }?>
                                        <?php endforeach; ?>
                                        <h5 class="descrip"><?php echo strip_tags($dest->titulo) ?></h5>
                                        <h6 class=""><?php echo substr(strip_tags($dest->contenido), 0, 150) ?>...</h6>
                                    </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                
                <div class="row  center-align">
                    <div class="col l2 offset-l5 s10 offset-s1 boton-secciones">
                        <button class="color-b " id="articulos" onclick="articulos()" >VER MÁS</button>
                        <!-- <a href="<?php echo base_url('magazine') ?>" class="color-a" href=""><h5 class="secciones">VER MÁS</h5></a> -->
                    </div>
                </div>
            </div>
            <div class="row center-align all-articulos">
                <div class="row div-articulos center-align">
                    <h5 class="title">ARTICULOS</h5>
                </div>
                <br>
                <?php if (isset($posts)) { ?>
                    <div class="col l10 offset-l1" style="margin-bottom:50px;">
                    <?php foreach ($posts as $dest) : ?>
                        <div class="col l4 m6 s12 card-blog">
                            <div class="row card post" >
                                    <div class="col l12 center-align">
                                        <a class="card-title" style="" href="<?php echo base_url() ?>magazine/blog/<?php echo $dest->id_blog_post ?>"><img class="art-img"
                                            src="<?php echo $dest->id_imagen_destacada ? $url_img.$dest->id_imagen_destacada : $url_default ?>"></a>
                                    </div>
                                    <div class="col l12 ">
                                        <br>
                                        <?php //foreach ($categories as $cate) : ?>
                                            <?php //if($dest->tipo!=null && $dest->tipo==$cate->id_categoria){ ?>
                                                <h6 class="categoria"><?php echo strip_tags($dest->categoria) ?></h6>            
                                            <?php //}?>
                                        <?php //endforeach; ?>
                                        <h5 class="descrip"><?php echo strip_tags($dest->titulo) ?></h5>
                                        <h6 class="contenido"><?php echo substr(strip_tags($dest->contenido), 0, 150) ?>...</h6>
                                    </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                        <div class="isContainer" style="text-align: center;">
                                <span class="onLoad">Cargando...</span>
                        </div>
                    </div>

                <?php } else { ?>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="card-panel teal">
                                    <span class="white-text">A&uacute;n no hay publicaciones en esta categor&iacute;a intenta m&aacute;s tarde
                                    </span>
                                    </div>
                                </div>
                            </div>
                <?php } ?>
            </div>
            <?php }else{ ?>
            <div class="row center-align ">
                <div class="row div-articulos center-align">
                    <h5 class="title">ARTICULOS</h5>
                </div>
                <br>
                <?php if (isset($posts)) { ?>
                    <div class="col l10 offset-l1" style="margin-bottom:50px;">
                    <?php foreach ($posts as $dest) : ?>
                        <div class="col l4 m6 s12 card-blog">
                            <div class="row card post" >
                                    <div class="col l12 center-align">
                                        <a class="card-title" style="" href="<?php echo base_url() ?>magazine/blog/<?php echo $dest->id_blog_post ?>"><img class="art-img"
                                            src="<?php echo $dest->id_imagen_destacada ? $url_img.$dest->id_imagen_destacada : $url_default ?>"></a>
                                    </div>
                                    <div class="col l12 ">
                                        <br>
                                        <?php foreach ($categories as $cate) : ?>
                                            <?php if($dest->tipo!=null && $dest->tipo==$cate->id_categoria){ ?>
                                                <h6 class="categoria"><?php echo strip_tags($cate->name_category) ?></h6>            
                                            <?php }?>
                                        <?php endforeach; ?>
                                        <h5 class="descrip"><?php echo strip_tags($dest->titulo) ?></h5>
                                        <h6 class="contenido"><?php echo substr(strip_tags($dest->contenido), 0, 150) ?>...</h6>
                                    </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                        <div class="isContainer" style="text-align: center;">
                                <span class="onLoad">Cargando...</span>
                        </div>
                    </div>

                <?php } else { ?>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="card-panel teal">
                                    <span class="white-text">A&uacute;n no hay publicaciones en esta categor&iacute;a intenta m&aacute;s tarde
                                    </span>
                                    </div>
                                </div>
                            </div>
                <?php } ?>
            </div>
            <?php }?>
            <div class="row div-articulos center-align">
                <h5 class="title">NOTICIAS Y ACTUALIZACIONES </h5>
            </div>
            <div class="row  center-align ">
                <div class="col l12 s12 center-align">
                    <div class="row center-align">
                        <a class="color-a" href=""><h5 class="secciones">REGISTRATE PARA SER EL PRIMERO EN RECIBIR NOTIFICACIONES</h5></a>
                        <br>
                        <input id="email_susc" class="registro col l8 offset-l2" style="" placeholder="Introduce tu correo electronico"/>
                    </div>
                    <button type="" id="buttonRegistro" class="btn sig_in " style="" > SUSCRIBIRME</button>
                </div>
            </div>
            <br>
            <hr class="hr2">
            <br><br>
        </div>
    </div>
</body>
<?php $this->view("japy/prueba/footer"); ?>
<script>
    $(document).ready(function(){
        $('.sig_in').on('click', render);
    });

    function render() {
        window.location.href = "<?php echo base_url() ?>registro/registroNovia?mail="+$('#email_susc').val();
    }

    var page = 1;
    var totPages = parseInt("<?php echo $total_paginas ?>");
    var urlD = "<?php echo "$url/$cat/" ?>";
    // var blogUrl = "<?php echo base_url() ?>" + 'blog/post/ver/';
    var blogUrl = "<?php echo base_url() ?>" + 'magazine/blog/';
    var url_img = "<?php echo $url_img ?>";
    var url_default = "<?php echo $url_default ?>";
    var detected = false;

    $(window).scroll(function() {
        var top_of_element = $('.onLoad').offset().top;
        var bottom_of_element = $('.onLoad').offset().top + $('.onLoad').outerHeight();
        var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
        var top_of_screen = $(window).scrollTop();

        if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) && (!detected)) {
            detected = true;
            this.addMore();
        }
        else {
            // console.log("El elemento no esta visible");
        }
    });
    function addMore() {
        page += 1;

        if (page <= totPages) {
            $.when($.ajax({
                url: urlD + page,
                headers: {'Accept': 'application/json'},
                success: function(response) {
                    console.log('ok');
                },
            })).done(function(data) {
                detected = false;
                $.each(data, function(i, dat) {
                    var content = dat.contenido.replace(/(<([^>]+)>)/ig, '');
                    var newLGCard = $('.card-blog:last').clone(true);
                    var imgToLoad = url_default;
                    //Estilos aplicados para cambiar la imagen
                    if (dat.id_imagen_destacada) {
                        imgToLoad = url_img + dat.id_imagen_destacada;
                    }
                    //Cargado de Tarjetas(Card) pantallas grandes
                    newLGCard.find('h5.descrip').html(dat.titulo.replace(/(<([^>]+)>)/ig, ''));
                    if (dat.categoria) {
                        newLGCard.find('h6.categoria').html( dat.categoria);
                    }else{
                        newLGCard.find('h6.categoria').html(' ');
                    }
                    newLGCard.find('a.card-title').attr('href', blogUrl + dat.id_blog_post);
                    // newLGCard.find('a.img-nuevo').attr('href', blogUrl + dat.id_blog_post);
                    // newLGCard.find('p.por').html('Por ' + dat.autor.nombre);
                    newLGCard.find('h6.contenido').html(content.substring(0, 150) + '...');

                    if (dat.id_imagen_destacada) {
                        newLGCard.find('img.art-img').attr('src', imgToLoad);
                    }
                    newLGCard.insertBefore('.isContainer');
                });
            });
        }
        else {
            $('.onLoad').html('No quedan más elementos por mostrar');
        }
    }
</script>
<script src="<?php echo base_url() ?>dist/js/brideadvisor/magazine.js"></script>
</html>