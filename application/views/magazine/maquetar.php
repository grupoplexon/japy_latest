<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url() ?>dist/css/brideAdvisor/magazine.css" rel="stylesheet" type="text/css"/>

    <?php 
        $url         = $this->config->base_url()."blog/home/index";
        $url_img     = $this->config->base_url()."blog/resource/imagen/";
        $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
    <title>Magazine</title>
    <?php $this->view("japy/prueba/header"); ?>

</head>
<style>
    hr {
        width: 150px;
        border: 1.5px solid #F8DADF;
    }
    .contenedor{
        position: relative;
        text-align: center;
    }
    .contenedor img{
        width:100%;
        height: 400px;
        object-fit: cover;
    }

    .contenedor .centrado{
        position: absolute;
        top: 84%;
        left: 50%;
        width:100%;
        height: 150px;
        padding: 5px;
        transform: translate(-50%, -50%);
        background: rgba(255,255,255,.9) !important;
    }
    /* .title-articulo h4{
        font-size: 4vh;
        font-weight: bold;
    } */
    .centrado h4{
        color: black;
        font-weight: bold;
        font-size: 3vh;
    }
    .parrafo-l{
        width: 34vw;
        float: left;
        text-align: justify;
    }
    .parrafo-r{
        width: 34vw;
        float: right;
        text-align: justify;
    }
    #header{
        position: relative !important;
    }
    .background {
        background-size: contain !important;
    }
    .post {
        height: 470px !important;
    }
</style>
<body>
    <div class=" background" style=" background-image: url('<?php echo base_url()?>dist/img/fondo.jpg');">
        <div class="row center-align" style="margin-bottom: unset;">
            <div class="col l12">
                <h2 style="">Exfoliantes caseros y naturales </h2>
                <hr>
            </div>
        </div>
        <div id="contenido" style="min-height: 200px;width:85%;margin:auto;">
                    <p class="content-post">
                <div class="row">
                    <div class="col s12 l12">
                        <p class="col l12 s12" style="font-size: 18px; text-align: justify;">
                        Cuando se piensa en una piel perfecta, debemos de tener en mente que los exfoliantes son indispensables para eso.
                        </p>
                        <div class="col l12 s12 center-align">
                            <img src="https://brideadvisor.mx/uploads/blogs/belleza/coco1.jpg" style="width: 40%;">
                        </div>
                        <p class="col l12 s12" style="font-size: 18px; text-align: justify;">
                        Existen diferentes tipos de exfoliantes naturales para nuestra piel con diferentes funciones. Los puedes realizar con ingredientes que tienes en tu casa y económicos </p>
                        <p class="col l12 s12" style="font-size: 18px; text-align: justify;"> <b> Toma nota.</b> <br>
                        - Azúcar con aceite de coco en cantidades iguales, este ayuda en prevenir el envejecimiento, repara la piel dañada, remueve la piel muerta y estimulara tu piel.   <br> 
                        - Azúcar, miel y té verde, limpia y repara la piel y ayuda a evitar el acné. <br>
                        - Café y aceite de coco, ayuda a remover la piel y ayuda a la circulación sanguínea   <br> 
                        - Azúcar, aceite de coco y fresas en cantidades iguales, ayuda en remover las células de la piel y las fresas proporcionan un brillo único, blanquea y protege la piel del sol e hidrata<br> 
                        - Azúcar, aceite de oliva, limón y miel, te ayuda a blanquear tu piel y es recomendable utilizarlo por las noches ya que el limón te puede manchar con el sol.   </p>
                        <div class="col l12 s12 center-align">
                            <img src="https://brideadvisor.mx/uploads/blogs/belleza/coco4.jpg" style="width: 40%;">
                        </div>
                        <p class="col l12 s12" style="font-size: 18px; text-align: justify;">
                        Son recomendables usarlos una o dos veces a la semana para obtener mejor resultado ya que si lo utilizas más veces puedes irritar demasiado tu piel. </p>
                    </div>
                </div>
            </p>
        </div>
        <div class="row center-align ">
            <div class="row div-articulos center-align">
                <h5 class="title"> PODRÍA INTERESARTE TAMBIÉN</h5>
            </div>
            <div class="col l10 offset-l1 " style="margin-bottom: 50px;">
                <div class="conta" style="margin-bottom:50px;">
                </div>
                <div style="margin-bottom: 50px;">
                <br>
                <br>
                </div>
            </div>
        </div>
    </div>
</body>
<?php $this->view("japy/prueba/footer"); ?>
<script src="<?php echo base_url() ?>dist/js/brideadvisor/magazine.js"></script>
</html>