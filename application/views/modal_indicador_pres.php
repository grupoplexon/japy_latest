<style>
    #indicador-1 {
        background-image:url(<?php echo base_url()."dist/img/indicador_pres.png" ?>);
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 50% 50%;
        background-color: transparent;
        box-shadow: none;
        width: 400px;
        max-width: 100%;
        height: 50%;
        top: 15% !important;
    }
    #indicador-1 a{
        background-color: transparent !important;
        width: 100%;
        height: 100%;
    }

</style>
<div id="indicador-1" class="modal">
    <a id="indicador-11" href="#modal-presupuesto" class="btn modal-trigger"></a>
</div>
<script>
    $(document).ready(function () {
        $('.modal').modal();

    });
</script>