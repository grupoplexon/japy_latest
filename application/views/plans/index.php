<?php
$this->load->view("general/head");
?>
<style>
    body {
        display: flex;
        flex-direction: column;
    }

    .plans {
        flex: 1;
    }

    .plans li {
        list-style-type: initial !important;
    }

    form input {
        margin-top: 20px !important;
        margin-bottom: 0 !important;
    }

    footer {
        margin: 0 !important;
    }
</style>


<div class="container plans">
    <div class="row">
        <div class="col s12 m3">
            <h3>Básico</h3>
            <div>$9.99 / mes</div>
            <div>
                <select name="" id="">
                    <option value="1">Plan mensual</option>
                    <option value="12">Plan anual</option>
                </select>
            </div>
            <div>
                Incluye:
                <ul>
                    <li>Something</li>
                    <li>Something</li>
                    <li>Something</li>
                    <li>Something</li>
                </ul>
            </div>
        </div>
        <div class="col s12 m3">
            <h3>Básico</h3>
            <div>$9.99 / mes</div>
            <div>
                <select name="" id="">
                    <option value="1">Plan mensual</option>
                    <option value="12">Plan anual</option>
                </select>
            </div>
            <div>
                Incluye:
                <ul>
                    <li>Something</li>
                    <li>Something</li>
                    <li>Something</li>
                    <li>Something</li>
                </ul>
            </div>
            <div>
                <a href="#payment-modal" class="btn btn-buy waves-effect waves-light modal-trigger" data-plan="1">Comprar ahora</a>
            </div>
        </div>
        <div class="col s12 m3">
            <h3>Básico</h3>
            <div>$9.99 / mes</div>
            <div>
                <select name="" id="">
                    <option value="1">Plan mensual</option>
                    <option value="12">Plan anual</option>
                </select>
            </div>
            <div>
                Incluye:
                <ul>
                    <li>Something</li>
                    <li>Something</li>
                    <li>Something</li>
                    <li>Something</li>
                </ul>
            </div>
            <div>
                <a href="#payment-modal" class="btn btn-buy waves-effect waves-light modal-trigger" data-plan="2">Comprar ahora</a>
            </div>
        </div>
        <div class="col s12 m3">
            <h3>Básico</h3>
            <div>$9.99 / mes</div>
            <div>
                <select name="" id="">
                    <option value="1">Plan mensual</option>
                    <option value="12">Plan anual</option>
                </select>
            </div>
            <div>
                Incluye:
                <ul>
                    <li>Something</li>
                    <li>Something</li>
                    <li>Something</li>
                    <li>Something</li>
                </ul>
            </div>
            <div>
                <a href="#payment-modal" class="btn btn-buy waves-effect waves-light modal-trigger" data-plan="3">Comprar ahora</a>
            </div>
        </div>
    </div>
</div>

<div id="payment-modal" class="modal">
    <div class="modal-content">
        <h4>Agregar forma de pago</h4>
        <div class="row">
            <form id="payment">
                <div class="col s12 m8">
                    <input name="card_number" required="required" data-parsley-creditcard="" placeholder="Numero de tarjeta">
                </div>
                <div class="col s12 m2">
                    <input name="card_expiration" required="required" data-parsley-cardexpiry="" placeholder="MM/YYYY">
                </div>
                <div class="col s12 m2">
                    <input name="card_cvv" required="required" data-parsley-cvv="" placeholder="CVV">
                </div>
                <div class="col s12">
                    <input name="card_holder" required="required" placeholder="Nombre">
                </div>
            </form>
            <div class="col s12" id="payment-details">
                <h3>Básico <span></span></h3>
                <div>
                    Incluye:
                    <ul>
                        <li>Something</li>
                        <li>Something</li>
                        <li>Something</li>
                        <li>Something</li>
                    </ul>
                </div>
                <div>Precio: 1200000</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-flat">Cancelar</a>
        <a href="#!" id="submit-payment" class="modal-action waves-effect btn">Aceptar</a>
    </div>
</div>

<input type="hidden" id="base-url" value="<?php echo base_url() ?>">
<input type="hidden" id="user-id" value="<?php echo $this->session->userdata("id_usuario") ?>">

<?php
$this->load->view("general/footer", $this->_ci_cached_vars);
?>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/i18n/es.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery.payment/lib/jquery.payment.js"></script>
<script type="text/javascript" src="https://resources.openpay.mx/lib/openpay.v1.min.js"></script>
<script type="text/javascript" src="https://resources.openpay.mx/lib/openpay-data.v1.min.js"></script>

<script>
    jQuery(function($) {
        const $form = $('#payment');
        const $paymentDetails = $('#payment-details');

        const $cardNumber = $('input[name=\'card_number\']');
        const $cardExpiration = $('input[name=\'card_expiration\']');
        const $cardCVV = $('input[name=\'card_cvv\']');
        const $cardHolder = $('input[name=\'card_holder\']');

        const baseUrl = $('#base-url').val();
        const userId = $('#user-id').val();
        let userCardToken = '';
        let selectedPlan = {};
        let data = {};

        $paymentDetails.hide();

        //Set openpay
        OpenPay.setId('mmlgnvqylu1kdtdgflj7');
        OpenPay.setApiKey('pk_97a51669ca444517ac655acb57e40d56');
        OpenPay.setSandboxMode(true);
        OpenPay.deviceData.setup('payment');

        //Set card validators
        createCardValidators();

        $form.on('submit', function(e) {
            e.preventDefault();

            if (userCardToken) {
                pay(userCardToken, selectedPlan);
            }
            else {
                if (!$form.parsley().isValid()) {
                    $form.parsley().validate();
                    return false;
                }

                data.cardHolder = $cardHolder.val();
                data.cardNumber = $cardNumber.val();
                data.cardCVV = $cardCVV.val();
                data.expirationYear = $cardExpiration.val().split('/')[1];
                data.expirationMonth = $cardExpiration.val().split('/')[0];
                data.deviceId = $('#deviceDataId').val();

                createToken(data);
            }

        });

        $('#submit-payment').on('click', function(e) {
            e.preventDefault();
            $form.submit();
        });

        $('.modal-trigger').on('click', function(e) {
            selectedPlan.id = $(this).data('plan');
            selectedPlan.months = $(this).parent().parent().find('select').val();
        });

        function createCardValidators() {
            window.Parsley.setLocale('es');

            window.Parsley.addValidator('creditcard',
                function(value, requirement) {
                    var valid = $.payment.validateCardNumber(value);

                    // Checks for specific brands
                    if (valid && requirement.length) {
                        var valid_cards = requirement.split(','),
                            valid = false,
                            card = $.payment.cardType(value);

                        if (requirement.indexOf(card) > -1) {
                            valid = true;
                        }
                    }
                    return valid;
                }, 32).addMessage('es', 'creditcard', 'Tarjeta invalida');

            window.Parsley.addValidator('cvv',
                function(value) {
                    var valid = $.payment.validateCardCVC(value);
                    return valid;
                }, 32).addMessage('es', 'cvv', 'CVV invalido');

            window.Parsley.addValidator('cardexpiry',
                function(value) {
                    if (value.indexOf('/') === -1) {
                        return false;
                    }

                    var date = $.payment.cardExpiryVal(value),
                        month = date.month,
                        year = date.year;

                    var valid = $.payment.validateCardExpiry(month, year);
                    return valid;
                }, 32).addMessage('es', 'cardexpiry', 'Fecha invalida');
        }

        function createToken(data) {
            OpenPay.token.create({
                'card_number': data.cardNumber,
                'holder_name': data.cardHolder,
                'cvv2': data.cardCVV,
                'expiration_year': data.expirationYear,
                'expiration_month': data.expirationMonth,
            }, function(response) {
                userCardToken = response.data.id;
                $('#payment-modal h4').html('Confirmar compra');
                $form.hide();
                $paymentDetails.show();
            }, function(error) {
                console.log(error);
                alert('Ocurrio un error intentalo de nuevo mas tarde.');
            });
        }

        function pay(token, plan) {
            data.token = token;
            data.plan = plan;

            $.post('plans/pay', data).done(function(data) {
                $('#payment-modal h4').html('Pago confirmado');
            }).fail(function(data) {
                alert('Ocurrio un error intentalo de nuevo mas tarde.');
            });
        }

    });
</script>
<?php
$this->load->view("general/newfooter");
?>


