<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
    <?php
    if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
        $this->view("portalweb_visitante/estructura/login");
    } else {
        ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="albums">
                            <div id="albums-seccion" style="padding:11px;">
                                <h4 class="titulo"><?php echo $informacion->titulo ?></h4>
                                <div class="descripcion"><?php echo $informacion->descripcion ?></div>
                            </div>
                            <?php foreach ($albums as $key => $obj) { ?>
                                <a href="<?php echo base_url() ?>index.php/Web/album/<?php echo $id_web ?>/<?php echo $obj->id_miportal_album ?>" >
                                    <div class="myFotos" style="padding:11px;">
                                        <div class="row">
                                            <div class="col m7 s12">
                                                <div class="row" style="padding:10px;">
                                                    <div class="col s12 image-principal"><img class="imageprincipal" src="<?php echo empty($obj->imagenes[0]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[0]->mime . ";base64," . base64_encode($obj->imagenes[0]->base)) ?>"></div>
                                                    <div class="col s12"><br></div>
                                                    <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo empty($obj->imagenes[1]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[1]->mime . ";base64," . base64_encode($obj->imagenes[1]->base)) ?>"></div>
                                                    <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo empty($obj->imagenes[2]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[2]->mime . ";base64," . base64_encode($obj->imagenes[2]->base)) ?>"></div>
                                                    <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo empty($obj->imagenes[3]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[3]->mime . ";base64," . base64_encode($obj->imagenes[3]->base)) ?>"></div>
                                                </div>
                                            </div>
                                            <div class="col m5 s12">
                                                <h5 class="title-album"><?php echo $obj->titulo ?></h5>
                                                <h6 class="number-album" data-contador="<?php echo count($obj->imagenes); ?>"><?php echo count($obj->imagenes); ?> fotos</h6>
                                                <h6 class="numberCom-album" data-contador="<?php echo count($obj->comentarios); ?>"><i class="material-icons">chat_bubble_outline</i> <?php echo count($obj->comentarios); ?> Comentario</h6>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>   
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer"); ?>
</body>