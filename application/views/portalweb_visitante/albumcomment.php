<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
    <?php
    if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
        $this->view("portalweb_visitante/estructura/login");
    } else {
        ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <div class="blogcomment">
                        <?php if (!empty($album)) { ?>
                            <h4 class="titulo"><?php echo $album->titulo ?></h4>
                            <div class="descripcion"><?php echo $album->descripcion ?></div>
                            <div class="row">
                                <?php foreach ($album->imagenes as $value) { ?>
                                    <div class="col s4 image-secundarias"><img class="materialboxed responsive-img" src="<?php echo "data:" . $value->mime . ";base64," . base64_encode($value->base) ?>"></div>
                                <?php } ?>
                            </div>

                            <div class="seccion">
                                <h5 class="titulo">Comentario</h5>
                                <hr class="hr-separador">
                                <div class="libro-comentario">
                                    <?php foreach ($album->comentarios as $i => $temp) { ?>
                                        <div class="comentario">
                                            <span class="lc-titulo"><?php echo $temp->nombre ?></span>
                                            <span class="lc-fecha">&#183;<?php echo relativeTimeFormat($temp->fecha) ?></span>
                                            <p><?php echo $temp->comentario ?></p>
                                        </div>            
                                    <?php } ?>
                                </div>
                            </div>
                            <div id="mensaje_envio"></div>
                            <div class="seccion">
                                <h5 class="titulo">Deja tu comentario!!</h5>
                                <hr class="hr-separador">
                                <form id="form">
                                    <div class="row formulario">
                                        <div class="col m6 s12">
                                            <p>Nombre</p>
                                            <input class="form-portal validate[required,minSize[4]]" name="nombre" value="<?php echo $this->session->userdata('nombre_visita') ?>" type="text"  />
                                        </div>
                                        <div class="col m6 s12">
                                            <p>E-Mail</p>
                                            <input class="form-portal validate[required,custom[email]]" name="email" value="<?php echo $this->session->userdata('email_visita') ?>" type="email"  />
                                        </div>
                                        <div class="col s12">
                                            <p>Comentario</p>
                                            <textarea class="form-portal validate[required,minSize[10]]" name="comentario"></textarea>
                                        </div>
                                    </div>
                                    <button class="waves-effect waves-light btn-flat boton" type="button" id="submit">Enviar Comentario</button>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer"); ?>
    <script type="text" id="template-1" >
        <span class="lc-titulo"></span>
        <span class="lc-fecha">&#183;</span>
        <p></p>
    </script>
    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();


            $('#form').validationEngine();
            $('#submit').on('click', function () {
                if ($("#form").validationEngine('validate')) {
                    $.ajax({
                        url: '<?php echo base_url() ?>index.php/Web/album/<?php echo $id_web ?>/<?php echo $album->id_miportal_album ?>',
                                            method: 'POST',
                                            timeout: 3000,
                                            data: {
                                                'nombre': $('[name="nombre"]').val(),
                                                'correo': $('[name="email"]').val(),
                                                'comentario': $('[name="comentario"]').val()
                                            }
                                        }).done(function () {
                                            var div = document.createElement("div");
                                            div.setAttribute('class', 'comentario');
                                            div.innerHTML = $("#template-1").html();
                                            $($(div).find('.lc-titulo')[0]).text($('[name="nombre"]').val());
                                            $($(div).find('.lc-fecha')[0]).html('&#183; Hace un momento');
                                            $($(div).find('p')[0]).text($('[name="comentario"]').val());
                                            $('.libro-comentario').append(div);

                                            $('[name="comentario"]').val('');

                                            $('#mensaje_envio').html('<div class="chip teal darken-2 white-text" style="border-radius: 0px; width: 100%;">Gracias por tu comentario<i class="material-icons">close</i></div>');
                                        }).fail(function () {
                                            $('#mensaje_envio').html('<div class="chip deep-orange darken-2 white-text" style="border-radius: 0px; width: 100%;">Intentelo de nuevo m&aacute;s tarde.<i class="material-icons">close</i></div>');
                                        });
                                    }
                                });
                            });
    </script>

</body>