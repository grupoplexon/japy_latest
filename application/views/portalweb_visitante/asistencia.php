<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
        <?php if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
    $this->view("portalweb_visitante/estructura/login");    
    } else { ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="confirma" style="padding:11px;">
                            <h4 class="titulo"><?php echo $informacion->subtitulo ?></h4>
                            <div class="descripcion"><?php echo $informacion->descripcion ?></div>
                            <hr class="hr-separador">
                            
                            <form method="post">
                                <div class="row descripcion">
                                    <div class="col m6 s12">
                                        <p>Nombre</p>
                                        <input class="form-portal" name="nombre" value="<?php echo $this->session->userdata('nombre_visita') ?>" type="text">
                                    </div>
                                    <div class="col m6 s12">
                                        <p>E-Mail</p>
                                        <input class="form-portal" name="email" value="<?php echo $this->session->userdata('email_visita') ?>" type="email" >
                                    </div>
                                </div>
                                <button class="waves-effect waves-light btn-flat boton" type="submit">Enviar</button>
                            </form>
                            <div id="mensaje_envio"></div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer");?>
</body>