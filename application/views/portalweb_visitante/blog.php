<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
        <?php if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
    $this->view("portalweb_visitante/estructura/login");    
    } else { ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="blog">
                            <div id="blog-seccion">
                                <h4 class="titulo"><?php echo $informacion->titulo ?></h4>
                                <div class="descripcion"><?php echo $informacion->descripcion ?></div>
                            </div>


                            <?php foreach ($blogs as $key => $obj) { ?>
                                <hr class="hr-separador">
                                <div class="myBlog" style="padding:11px;">
                                    <a href='<?php echo base_url() ?>index.php/Web/blog/<?php echo $id_web ?>/<?php echo $obj->id_miportal_blog ?>'>
                                        <h5 class="titulo"><?php echo $obj->titulo ?></h5>
                                    </a>
                                    
                                    <div class="carrusel">
                                        <div class="row">
                                            <div class="col s1"><i class="material-icons button_prev">keyboard_arrow_left</i></div>
                                            <div class="col s10">
                                                <div class="carousel carousel-slider">
                                                    <?php foreach ($obj->imagenes as $nC => $img) { ?>
                                                        <a class="carousel-item" href="#<?php echo $nC . $obj->id_miportal_blog . '!' ?>"><img src='<?php echo "data:" . $img->mime . ";base64," . base64_encode($img->base) ?>'/></a>   
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col s1"><i class="material-icons button_next">keyboard_arrow_right</i></div>
                                        </div>
                                    </div>

                                    <div class="descripcion"><?php echo $obj->descripcion; ?></div>
                                    <a href='<?php echo base_url() ?>index.php/Web/blog/<?php echo $id_web ?>/<?php echo $obj->id_miportal_blog ?>'>
                                        <h6 class="numComentario" data-contador="<?php echo count($obj->comentarios); ?>"><i class="material-icons">chat_bubble_outline</i> <?php echo count($obj->comentarios); ?> Comentario</h6>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer");?>
</body>
<script>
    $(document).ready(function () {
        $('.carousel').carousel();
        
        $('.button_prev').attr('style','-webkit-appearance: button; cursor: pointer;');
        $('.button_prev').on('click',function(){
            $($(this.parentNode.parentNode).find('.carousel')[0]).carousel('prev');
        });
        
        $('.button_next').attr('style','-webkit-appearance: button; cursor: pointer;');
        $('.button_next').on('click',function(){
            $($(this.parentNode.parentNode).find('.carousel')[0]).carousel('next');
        });
    });
</script>