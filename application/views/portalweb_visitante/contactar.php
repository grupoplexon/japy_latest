<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
        <?php if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
    $this->view("portalweb_visitante/estructura/login");    
    } else { ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="contactanos" style="padding:11px;">
                            <h4 class="titulo"><?php echo $informacion->subtitulo ?></h4>
                            <div class="descripcion"><?php echo $informacion->descripcion ?></div>
                            <hr class="hr-separador">
                            <div id="mensaje_envio"></div>
                            <form id="form">
                                <div class="row descripcion">
                                    <div class="col m6 s12">
                                        <p>Nombre</p>
                                        <input class="form-portal" name="nombre" value="<?php echo $this->session->userdata('nombre_visita') ?>" type="text">
                                    </div>
                                    <div class="col m6 s12">
                                        <p>E-Mail</p>
                                        <input class="form-portal" name="email" value="<?php echo $this->session->userdata('email_visita') ?>" type="email" >
                                    </div>
                                    <div class="col s12">
                                        <p>Asunto</p>
                                        <input class="form-portal" name="asunto" type="text" >
                                    </div>
                                    <div class="col s12">
                                        <p>Comentario</p>
                                        <textarea class="form-portal" name="comentario"></textarea>
                                    </div>
                                </div>
                                <button class="waves-effect waves-light btn-flat boton" type="button" id="submit">Enviar Comentario</button>
                            </form>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer");?>
</body>
<script>
    $(document).ready(function () {
        $('.materialboxed').materialbox();


        $('#form').validationEngine();
        $('#submit').on('click', function () {
            if ($("#form").validationEngine('validate')) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Web/contactar',
                    method: 'POST',
                    timeout: 3000,
                    data: {
                        'nombre': $('[name="nombre"]').val(),
                        'email': $('[name="email"]').val(),
                        'asunto': $('[name="asunto"]').val(),
                        'comentario': $('[name="comentario"]').val()
                    }
                }).done(function () {
                    $('[name="comentario"]').val('');
                    $('[name="asunto"]').val('');

                    $('#mensaje_envio').html('<div class="chip teal darken-2 white-text" style="border-radius: 0px; width: 100%;">Gracias por tu comentario<i class="material-icons">close</i></div>');
                }).fail(function () {
                    $('#mensaje_envio').html('<div class="chip deep-orange darken-2 white-text" style="border-radius: 0px; width: 100%;">Intentelo de nuevo m&aacute;s tarde.<i class="material-icons">close</i></div>');
                });
            }
        });
    });
</script>

