<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
        <?php if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
    $this->view("portalweb_visitante/estructura/login");    
    } else { ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="direcciones">
                            <div id="direcciones-seccion" style="padding:11px;">
                                <h4 class="titulo"><?php echo $informacion->titulo ?></h4>
                                <div class="descripcion"><?php echo $informacion->descripcion ?></div>
                            </div>

                            <?php foreach ($direcciones as $key => $obj) { ?>
                                <hr class="hr-separador">

                                <div>
                                    <div class="misdirecciones direcciones-agregados" id="direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>" style="padding:11px;">
                                        <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-<?php echo $obj->id_miportal_direccion ?>" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                                        <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-<?php echo $obj->id_miportal_direccion ?>" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
                                        <h5 class="titulo"><?php echo $obj->titulo; ?></h5>
                                        <p class="telefono"><b><i class="material-icons">phone</i>Tel&eacute;fono: </b> <?php echo $obj->telefono; ?></p>
                                        <div class="descripcion"><?php echo $obj->descripcion; ?></div>
                                        <div class="direccion-mapa" style="overflow: hidden;">
                                            <iframe
                                                width="100%" height="400"
                                                frameborder="0" style="border:0; margin-top: -100px;"
                                                src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=<?php echo $obj->latitud . ',' . $obj->longitud ?>&zoom=18" allowfullscreen></iframe>
                                        </div>
                                        <p class="dir-direccion"><b><i class="material-icons">place</i>Direcci&oacute;n: </b><?php echo $obj->direccion . ' ' . $obj->codigo_postal . ' ' . $obj->estado . ', ' . $obj->poblado; ?></p>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer");?>
</body>