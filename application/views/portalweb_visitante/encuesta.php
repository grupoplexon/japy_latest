<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
    <?php
    if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
        $this->view("portalweb_visitante/estructura/login");
    } else {
        ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="encuesta">
                            <div id="encuestas-seccion" style="padding:11px;">
                                <h4 class="titulo"><?php echo $informacion->titulo ?></h4>
                                <div class="descripcion"><?php echo $informacion->descripcion ?></div>
                            </div>
                            <?php foreach ($encuestas as $key => $obj) { ?>
                                <hr class="hr-separador">
                                <div class="encuesta-informacion">
                                    <h5 class="titulo"><?php echo $obj->titulo ?></h5>
                                    <div class="descripcion"><?php echo $obj->descripcion ?></div>
                                    <?php foreach ($obj->opciones as $count => $opt) { ?>
                                        <p>
                                            <input class="with-gap" name="group<?php echo $obj->id_miportal_encuesta ?>" type="radio" value="<?php echo $opt->id_miportal_encuesta_opcion ?>" id="test<?php echo $count . $obj->id_miportal_encuesta ?>" />
                                            <label for="test<?php echo $count . $obj->id_miportal_encuesta ?>"><?php echo $opt->texto ?></label>
                                        </p>
                                    <?php } ?>
                                    <button class="waves-effect waves-light btn-flat boton send_answer" data-id="<?php echo $obj->id_miportal_encuesta ?>">Enviar</button>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer"); ?>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
</body>
<script>
    $(document).ready(function () {
        $('.send_answer').on('click', function () {
            var _super = this;
            var checked = $(_super.parentNode).find('input:checked');
            if (checked.length > 0) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Web/encuesta/' + $(_super).data('id'),
                    method: 'post',
                    timeout: 2000,
                    data: {
                        respuesta: $(checked[0]).val()
                    }
                }).done(function (request) {
                    $(_super.parentNode).attr('style', 'width:' + ($($('.encuesta .descripcion')[0]).width() - 50) + 'px; height:250px;');
                    $(_super.parentNode).highcharts({
                        credits: {
                            enabled: false
                        },
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'top',
                            floating: true,
                            backgroundColor: '#FFFFFF',
                            lineHeight: 25
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: false
                                },
                                showInLegend: true
                            }
                        },
                        series: [{
                                name: 'Respuesta ',
                                colorByPoint: true,
                                data: request.resultado
                            }]
                    });
                    $(_super.parentNode).attr('style', 'width:90%; height:250px;');

                }).fail(function () {

                });
            } else {
                alert('Seleccione una opci&oacute;n.');
            }
        });
    });
</script>