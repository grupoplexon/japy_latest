<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright
            <a class="grey-text text-lighten-4 right" href="#!"></a>
        </div>
    </div>
</footer>
<script src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/materialize.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/validation/validate.engine.js"    type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/validation/validate.engine.plugin.js"    type="text/javascript"></script>
<script>
    $(document).ready(function () {
        if (typeof onReady != "undefined") {
            onReady();
        }
        
        $('select').material_select();
        $(".dropdown-button").dropdown();
        $('.modal-trigger').modal();
    });
</script>
