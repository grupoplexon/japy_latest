<section class="body">
    <nav class="pink lighten-5">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">
                <img style="width: 350px; height: auto; margin-top: 10px;"
                     src="<?php echo base_url() ?>dist/img/clubnupcial.png" alt=""/>
            </a>
        </div>
    </nav>
    <div class="container-body"
         style="min-height:86.5%; background: url(<?php echo base_url() ?>dist/img/textura_footer2.png)">

        <div class="row">
            <div class="col s2 m4">&nbsp;</div>
            <div class="col s8 m3">
                <h4 class="center"><?php echo $miportal->nosotros ?></h4>
                <div class="divider" style="height: 7px; margin: 10px 50px 20px 50px;"></div>
                <h5 class="center"><?php echo strtoupper(strftime("%d %B %Y ",
                            strtotime($miportal->fecha_boda))) ?></h5>
                <br><br>

                <form id='send-visit'>
                    <div class="card">
                        <?php if (empty($this->session->userdata('nombre_visita'))) { ?>
                            <div class="card-action pink lighten-5">
                                <p style="text-align: center; font-weight: bold;">¿Nos puedes decir qui&eacute;n
                                    eres?Nos gusta saber qui&eacute;n visita nuestra web</p>
                            </div>
                            <div class="card-content">
                                <div id='mensaje-visita'></div>
                                <h6><b>NOMBRE</b></h6>
                                <input class="formulario-invitaciones validate[required,minSize[2]]" type="text"
                                       name="nombre" placeholder="" style="width:100%"/>
                                <h6><b>APELLIDO(S)</b></h6>
                                <input class="formulario-invitaciones validate[required,minSize[2]]" type="text"
                                       name="apellido" placeholder="" style="width:100%"/>
                                <h6><b>E-MAIL</b></h6>
                                <input class="formulario-invitaciones validate[required,custom[email]]" type="email"
                                       name="email" placeholder="" style="width:100%"/>
                            </div>
                        <?php } ?>

                        <?php if ($informacion->visible == 2 && ! empty($miportal->contrasena)) { ?>
                            <div class="card-action pink lighten-5">
                                <p style="text-align: center; font-weight: bold;">Est&aacute;s intentando entrar en una
                                    web privada de boda, los novios te pueden facilitar la contrase&ntilde;a para
                                    acceder.</p>
                            </div>
                            <div class="card-content">
                                <div id='mensaje-email'></div>
                                <h6><b>CONTRASE&Ntilde;A</b></h6>
                                <input class="formulario-invitaciones validate[required,minSize[1]]" type="password"
                                       name="password" style="width:100%"/>
                            </div>
                            <div class="card-action">
                                <a class="modal-trigger" href="#modal1">
                                    <small class="dorado-2-text">Solicitar la contraseña a los novios →</small>
                                </a>
                            </div>
                        <?php } ?>
                        <div class="card-content">
                            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id='button-visit'
                                    style="width:100%; margin-bottom: 5px;" type="button">ACCEDER
                            </button>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col s2 m4">&nbsp;</div>
        </div>
    </div>
</section>
<div id="modal1" class="modal" style="width: 30%;">
    <div class="modal-content">
        <form id='send_email'>
            <h4>Solicitar contrase&ntilde;a</h4>
            <p>Indica tu nombre y email para pedir la contraseña a los novios</p>
            <h6><b>NOMBRE</b></h6>
            <input class="formulario-invitaciones validate[required,minSize[4]]" type="text" name="nombre-send"
                   placeholder="" value="<?php echo $this->session->userdata('nombre_visita') ?>" style="width:100%"/>
            <h6><b>E-MAIL</b></h6>
            <input class="formulario-invitaciones validate[required,custom[email]]" type="email" name="email-send"
                   placeholder="" value="<?php echo $this->session->userdata('email_visita') ?>" style="width:100%"/>
            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id='submit-email'
                    style="width:100%; margin-bottom: 5px;" type="button">ENVIAR
            </button>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.send_email').validationEngine();
        $('#submit-email').on('click', function () {
            if ($("#send_email").validationEngine('validate')) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Web/login',
                    method: 'POST',
                    timeout: 1500,
                    data: {
                        'nombre-send': $('[name="nombre-send"]').val(),
                        'email-send': $('[name="email-send"]').val(),
                        'ip': '<?php echo $id_web ?>'
                    }
                }).done(function () {
                    $('#mensaje-email').html('<div class="chip teal darken-2 white-text" style="border-radius: 0px; width: 100%;">Se lo hemos comunicado a los novios<i class="material-icons">close</i></div>');
                    $('[name="nombre-send"]').val('');
                    $('[name="email-send"]').val('');
                    $('#modal1').modal("close");
                }).fail(function () {
                    $('#mensaje-email').html('<div class="chip deep-orange darken-2 white-text" style="border-radius: 0px; width: 100%;">Intentelo de nuevo m&aacute;s tarde.<i class="material-icons">close</i></div>');
                    $('[name="nombre-send"]').val('');
                    $('[name="email-send"]').val('');
                    $('#modal1').modal("close");
                });
            }
        });

        $('#send-visit').validationEngine();
        $('#button-visit').on('click', function () {
            console.log('entra');
            if ($('#send-visit').validationEngine('validate')) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Web/login',
                    method: 'POST',
                    timeout: 1500,
                    data: {
                        'nombre': ($('[name="nombre"]').val() === undefined) ? '' : $('[name="nombre"]').val(),
                        'apellido': ($('[name="apellido"]').val() === undefined) ? '' : $('[name="apellido"]').val(),
                        'email': ($('[name="email"]').val() === undefined) ? '' : $('[name="email"]').val(),
                        'password': ($('[name="password"]').val() === undefined) ? '' : $('[name="password"]').val()
                    }
                }).done(function () {
                    location.reload();
                    console.log('correcto');
                }).fail(function (result) {
                    console.log('error');
                    console.log(result);
                    console.log(result.responseJSON.success);
                    switch (result.responseJSON.success) {
                        case 3:
                            $('#mensaje-email').html('<div class="chip deep-orange darken-2 white-text" style="border-radius: 0px; width: 100%;">Password erroneo.<i class="material-icons">close</i></div>');
                            break;
                        case 1:
                            $('#mensaje-visita').text('<div class="chip deep-orange darken-2 white-text" style="border-radius: 0px; width: 100%;">Intentelo de nuevo m&aacute;s tarde.<i class="material-icons">close</i></div>');
                            $('#mensaje-email').html('');
                            break;
                    }
                });
            }
        });
    });
</script>