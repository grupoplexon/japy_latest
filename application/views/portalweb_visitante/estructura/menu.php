<div class="cabecera">
    <!-- TAB-BAR FOR  MOBILE -->
    <nav class="hide-on-med-and-up menu-mobile">
        <div class="nav-wrapper">
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="side-nav" id="mobile-demo">
                <?php foreach ($miportal_seccion as $key => $value) {?>
                    <li id="<?php echo $value->tipo.'-navmobile' ?>" style = '<?php echo ($value->activo == 2 || $value->visible == 3)?'display:none':'' ?>'><a class="<?php echo (($seccion == $value->tipo)?'active':''); ?>" href='<?php echo base_url() ?>index.php/web/index/<?php echo $id_web ?>/<?php echo $value->tipo ?>'><?php echo $value->titulo ?></a></li>
                <?php }?>
            </ul>
        </div>
    </nav>
    
    <!-- ENCABEZADO -->
    <div class="encabezado">
        <div class="encabezado-principal">
            <div class="encabezado-top">
                <h5 class="titulo"><?php echo $miportal->titulo ?></h5>
            </div>
            <div class="encabezado-descripcion">
                <h4 class="nosotros"><?php echo $miportal->nosotros ?></h4>
                <h5 class="date"><?php echo strtoupper(strftime("%d %B %Y ", strtotime($miportal->fecha_boda))) ?></h5>
            </div>
        </div>
        <!-- CUENTA REGRESIVA -->
        <div class="cuentaregresiva hide-on-small-only" style="<?php echo ($miportal->cuenta_atras == 2)?'display:none':''; ?>">
            <p>FALTAN</p>
            <?php 
            $datetime1 = date_create($miportal->fecha_boda);
            $datetime2 = date_create(date('Y-m-d'));
            $interval = date_diff($datetime1, $datetime2);
            ?>
            <h6 class="numero"><?php echo $interval->format('%a'); ?></h6>
            <p class="unidadtiempo">D&Iacute;AS</p>
        </div>
    </div>

</div>
<nav class="hide-on-small-only menu">
    <div class="body-container-web nav-wrapper">
        <ul class="center">
            <?php foreach ($miportal_seccion as $key => $value) { ?>
            <li id="<?php echo $value->tipo.'-navseccion' ?>" style='<?php echo ($value->activo == 2 || $value->visible == 3)?'display:none':'' ?>'><a class="<?php echo (($seccion == $value->tipo)?'active':''); ?>" href='<?php echo base_url() ?>index.php/web/index/<?php echo $id_web ?>/<?php echo $value->tipo ?>'><?php echo $value->titulo ?></a></li>
            <?php }?>
        </ul>
    </div>
</nav>