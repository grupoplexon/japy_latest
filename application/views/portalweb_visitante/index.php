<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
        <?php if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
    $this->view("portalweb_visitante/estructura/login");    
    } else { ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="welcome" style="padding:11px;">
                            <h4 class="titulo"><?php echo $informacion->subtitulo; ?></h4>
                            <img class="imagen" style="<?php echo empty($bienvenido) ? 'display:none' : ''; ?>" src="<?php echo!empty($bienvenido) ? ("data:$bienvenido->mime;base64," . base64_encode($bienvenido->base)) : ''; ?>">
                            <div class="descripcion"><?php echo $informacion->descripcion; ?></div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
   <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer");?>
</body>