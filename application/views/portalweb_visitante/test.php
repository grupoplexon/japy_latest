<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
    <?php
    if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
        $this->view("portalweb_visitante/estructura/login");
    } else {
        ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="test">

                            <div id="tests-seccion">
                                <h4 class="titulo"><?php echo $informacion->titulo ?></h4>
                                <div class="descripcion"><?php echo $informacion->descripcion ?></div>
                            </div>


                            <?php foreach ($tests as $key => $obj) { ?>
                                <hr class="hr-separador">
                                <div class="test-informacion">
                                    <h5 class="titulo"><?php echo $obj->titulo ?></h5>
                                    <div class="descripcion"><?php echo $obj->descripcion ?></div>

                                    <p>
                                        <input class="with-gap" name="group-<?php echo $obj->id_miportal_test ?>" type="radio" value="1" id="test1<?php echo $obj->id_miportal_test ?>" <?php echo (isset($obj->respuesta_visita) && $obj->respuesta_visita == '1') ? 'checked' : '' ?> />
                                        <label for="test1<?php echo $obj->id_miportal_test ?>"><?php echo $obj->answer1 . '&nbsp&nbsp' . (isset($obj->respuesta_visita) ? (($obj->respuesta_visita == '1' && $obj->respuesta == '1') ? '<span class="badge success">Correct</span>' : (($obj->respuesta == '1') ? '<span class="badge success">Correct</span>' : (($obj->respuesta_visita == '1') ? '<span class="badge incorrect">Error</span>' : ''))) : '') ?></label> 
                                    </p>
                                    <p>
                                        <input class="with-gap" name="group-<?php echo $obj->id_miportal_test ?>" type="radio" value="2" id="test2<?php echo $obj->id_miportal_test ?>" <?php echo (isset($obj->respuesta_visita) && $obj->respuesta_visita == '2') ? 'checked' : '' ?> />
                                        <label for="test2<?php echo $obj->id_miportal_test ?>"><?php echo $obj->answer2 . '&nbsp&nbsp' . (isset($obj->respuesta_visita) ? (($obj->respuesta_visita == '2' && $obj->respuesta == '2') ? '<span class="badge success">Correct</span>' : (($obj->respuesta == '2') ? '<span class="badge success">Correct</span>' : (($obj->respuesta_visita == '2') ? '<span class="badge incorrect">Error</span>' : ''))) : '') ?></label> 
                                    </p>

                                    <?php if (!empty($obj->answer3)) { ?>
                                        <p>
                                            <input class="with-gap" name="group-<?php echo $obj->id_miportal_test ?>" type="radio" value="3" id="test3<?php echo $obj->id_miportal_test ?>" <?php echo (isset($obj->respuesta_visita) && $obj->respuesta_visita == '3') ? 'checked' : '' ?> />
                                            <label for="test3<?php echo $obj->id_miportal_test ?>"><?php echo $obj->answer3 . '&nbsp&nbsp' . (isset($obj->respuesta_visita) ? (($obj->respuesta_visita == '3' && $obj->respuesta == '3') ? '<span class="badge success">Correct</span>' : (($obj->respuesta == '3') ? '<span class="badge success">Correct</span>' : (($obj->respuesta_visita == '3') ? '<span class="badge incorrect">Error</span>' : ''))) : '') ?></label> 
                                        </p>
                                    <?php } ?>

                                    <?php if (!empty($obj->answer4)) { ?>
                                        <p>
                                            <input class="with-gap" name="group-<?php echo $obj->id_miportal_test ?>" type="radio" value="4" id="test4<?php echo $obj->id_miportal_test ?>" <?php echo (isset($obj->respuesta_visita) && $obj->respuesta_visita == '4') ? 'checked' : '' ?> />
                                            <label for="test4<?php echo $obj->id_miportal_test ?>"><?php echo $obj->answer4 . '&nbsp&nbsp' . (isset($obj->respuesta_visita) ? (($obj->respuesta_visita == '4' && $obj->respuesta == '4') ? '<span class="badge success">Correct</span>' : (($obj->respuesta == '4') ? '<span class="badge success">Correct</span>' : (($obj->respuesta_visita == '4') ? '<span class="badge incorrect">Error</span>' : ''))) : '') ?></label> 
                                        </p>
                                    <?php } ?>
                                    <?php if (!isset($obj->respuesta_visita)) { ?>
                                        <button class="waves-effect waves-light btn-flat boton send_answer" data-id="<?php echo $obj->id_miportal_test ?>" >Enviar</button>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer"); ?>
</body>
<script>
    $(document).ready(function () {
        $('.send_answer').on('click', function () {
            var _super = this;
            var checked = $(_super.parentNode).find('input:checked');
            if (checked.length > 0) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/Web/test/' + $(this).data('id'),
                    method: 'post',
                    timeout: 800,
                    data: {
                        respuesta: $(checked[0]).val()
                    }
                }).done(function (request) {
                    if (request.resultado == $(checked[0]).val()) {
                        var label = $(checked[0].parentNode).find('label')[0];
                        $(label).html($(label).text() + '<span class="badge success">Correct</span>');
                    } else {
                        var label = $(checked[0].parentNode).find('label')[0];
                        $(label).html($(label).text() + '<span class="badge incorrect">Error</span>');
                        var answer = $(_super.parentNode).find('input[value="' + request.resultado + '"]');
                        label = $(answer[0].parentNode).find('label')[0];
                        $(label).html($(label).text() + '<span class="badge success">Correct</span>');
                    }
                    $(_super).remove();

                }).fail(function () {

                });
            } else {
                alert('Seleccione una opci&oacute;n.');
            }
        });
    });

</script>