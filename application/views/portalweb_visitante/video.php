<?php $this->view("portalweb_visitante/estructura/header"); ?>
<body>
    <?php
    if (empty($this->session->userdata('nombre_visita')) || ($informacion->visible == 2 && !empty($miportal->contrasena) && empty($this->session->userdata('contrasenia_visita')))) {
        $this->view("portalweb_visitante/estructura/login");
    } else {
        ?>
        <section class="body">
            <?php $this->view("portalweb_visitante/estructura/menu"); ?>
            <div class="container-body">
                <div class="body-container-web detail">
                    <?php if (!empty($informacion)) { ?>
                        <div class="videos">
                            <div id="videos-seccion" style="padding:11px;">
                                <h4 class="titulo"><?php echo $informacion->titulo ?></h4>
                                <div class="descripcion"><?php echo $informacion->descripcion ?></div>
                            </div>
                            <?php foreach ($videos as $key => $obj) { ?>
                                <hr class="hr-separador">
                                <div id="<?php echo $obj->id_miportal_video ?>" style="position:relative;">
                                    <div class="video-informacion videos-agregados" id="videos-seccion-<?php echo $obj->id_miportal_video ?>" style="padding:11px;">
                                        <h5 class="titulo"><?php echo $obj->titulo; ?></h5>
                                        <div class="myvideo">
                                            <iframe class="videoPreviews" title="<?php echo (strpos($obj->direccion_web, 'youtube')) ? 'youtube' : 'vimeo' ?>" width="100%" height="280" src="<?php
                                            $temp = $obj->direccion_web;
                                            if (strpos($temp, 'youtube')) {
                                                $temp = str_replace('watch?v=', 'embed/', $temp);
                                            } else if (strpos($temp, 'vimeo')) {
                                                $temp = str_replace("//vimeo.com/", "//player.vimeo.com/video/", $temp);
                                            } echo $temp . (strpos($temp, 'youtube') ? '?version=3&enablejsapi=1&playerapiid=ytplayer' : '')
                                            ?>" frameborder="0" allowfullscreen=""></iframe>
                                        </div>
                                        <div class="descripcion"><?php echo $obj->descripcion; ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php $this->view("portalweb_visitante/estructura/footer"); ?>
</body>