<style>
    .wrapper-breadcrumbs {
        width: 100%;
        padding: 4px;
    }

    .wrapper-breadcrumbs .breadcrums {
        color: #00BCDD;
    }

    .wrapper-breadcrumbs .breadcrums:after {
        content: '\E5CC';
        color: #00BCDD;
        vertical-align: top;
        display: inline-block;
        font-family: 'Material Icons';
        font-weight: normal;
        font-style: normal;
        margin: 0 10px 0 8px;
        -webkit-font-smoothing: antialiased;
    }
</style>
<div class="col m10 l11 wrapper-breadcrumbs">
    <?php foreach ($breadcrumbs as $key => $value) : ?>
        <a href="<?php echo $value ?>" class="breadcrums"><?php echo $key ?></a>
    <?php endforeach ?>
</div>