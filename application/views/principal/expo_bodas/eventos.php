<title>Expos - BrideAdvisor</title>
<?php
//$this->load->view("general/head");
$this->view("principal/head");
$this->view("general/newheader");
$this->view("japy/header");
?>
    
    <style>
        .counter {
            background-image: url('<?php echo base_url()?>/dist/img/registro_expo/BG-REGISTRO.png');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: top;
            height: auto;
            position: relative;
        }

        .logo {
            background-image: url('<?php echo base_url()?>/dist/img/expo_bodas/fondo.png');
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            width: 50%;
            float: right;
        }

        .eventos {
            margin: 5rem 0;
        }

        .evento {
            background-color: white;
            margin: 1rem 0;
            box-shadow: 0px 0px 10px 2px rgba(75, 75, 75, 0.8);
            border-radius: 5px;
        }

        .evento:hover {
            box-shadow: 0px 0px 20px 2px #000;
        }

        .evento p {
            width: 100%;
            text-align: center;
            margin: 0;
            font-family: 'Josefin Sans', sans-serif;
        }

        .ciudad {
            font-family: "Dancing Script", cursive !important;
            background-color: #b9b9b9;
            font-size: 3rem;
            color: white;
            font-weight: bold;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .dia, .fecha, .lugar {
            font-weight: bold;
            color: #000;
        }

        .dia {
            border-bottom: 1px solid #000;
            line-height: 0.1em;
            margin: 1rem 0 1.2rem !important;
        }

        .dia span {
            background: #fff;
            padding: 0 10px;
        }

        .dia, .lugar {
            font-size: 1em;
        }

        .fecha {
            font-size: 2.5em;
        }

        .content-evento {
            padding: 1rem 2.5rem;
        }

        @media only screen and (max-width: 321px) {
            .logo {
                width: 40%;
            }

            .counter {
                background-image: url('<?php echo base_url()?>/dist/img/expo_bodas/background-degradado.png');
                background-repeat: no-repeat;
                background-size: cover;
                background-position: left;
                height: 30rem;
            }

            .eventos {
                margin: 0;
            }

            .fecha {
                font-size: 1.5em;
            }
        }

        @media only screen and (min-width: 322px) and (max-width: 425px) {
            .logo {
                width: 40%;
            }

            .counter {
                background-image: url('<?php echo base_url()?>/dist/img/expo_bodas/background-degradado.png');
                background-repeat: no-repeat;
                background-size: cover;
                background-position: left;
                height: 30rem;
            }

            .fecha {
                font-size: 2em;
            }

            .eventos {
                margin: 0;
            }
            .margin {
                margin-bottom: 10vh !important;
            }
        }

        @media only screen and (min-width: 426px) and (max-width: 768px) {
            .counter {
                background-image: url('<?php echo base_url()?>/dist/img/expo_bodas/background-degradado.png');
                background-repeat: no-repeat;
                background-size: cover;
                background-position: left;
                height: auto;
            }

            .fecha {
                font-size: 2rem;
            }
        }

        @media only screen and (min-width: 1023px) and (max-width: 1440px) {
            .counter {
                background-position: left;
            }

            .eventos > div {
                width: 33% !important;
            }

            .content-evento {
                padding: 1rem;
            }

            .evento {
                height: 13rem;
            }

            .fecha {
                font-size: 1.5rem;
            }

            .lugar {
                font-size: .8rem;
            }
        }
        .data {
            margin-left: 63% !important;
        }


    </style>

    <div class="max-content">
        <section class="counter row no-margin">
            <div class="col s12 m10 offset-m1">
                <div class="row">
                    <div class="data col s12 m6 offset-m6 l4 offset-l8 center-align">
                        <div class="hide-on-small-only">
                            <img src="<?php echo base_url() ?>dist/img/registro_expo/logo-expo-new.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="eventos row hide-on-small-only">
                    <!-- <div class="col s12 m6 l3">
                        <a href="<?php echo base_url() ?>brideweekend/expo_culiacan">
                            <div class="evento">
                                <p class="ciudad">Culiacán</p>
                                <div class="content-evento">
                                    <p class="dia"><span>SABADO & DOMINGO</span></p>
                                    <p class="fecha">06 y 06 ABRIL</p>
                                    <p class="lugar">SALÓN FIGLOSTASE</p>
                                </div>
                            </div>
                        </a>
                    </div> -->
                    <div class="col s12 m6 l3">
                        <a href="<?php echo base_url() ?>brideweekend/expo_puebla">
                            <div class="evento">
                                <p class="ciudad">Puebla</p>
                                <div class="content-evento">
                                    <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                    <p class="fecha">24 Y 25 AGOSTO</p>
                                    <p class="lugar">CENTRO EXPOSITOR LOS FUERTES</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col s12 m6 l3">
                        <a href="<?php echo base_url() ?>brideweekend/expo_qro">
                            <div class="evento">
                                <p class="ciudad">Querétaro</p>
                                <div class="content-evento">
                                    <p class="dia"><span>SÁBADO Y DOMINGO</span></p>
                                    <p class="fecha">07 Y 08 DE SEPTIEMBRE</p>
                                    <p class="lugar">CENTRO DE CONGRESOS</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col s12 m6 l3">
                        <a href="<?php echo base_url() ?>brideweekend/expo_gdl">
                            <div class="evento">
                                <p class="ciudad">Guadalajara</p>
                                <div class="content-evento">
                                    <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                    <p class="fecha">25 Y 26 ENERO</p>
                                    <p class="lugar">EXPO GUADALAJARA</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col s12 m6 l3">
                        <a href="<?php echo base_url() ?>home/expo?estado=Nuevo_leon">
                            <div class="evento">
                                <p class="ciudad">Monterrey</p>
                                <div class="content-evento">
                                    <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                    <p class="fecha">26 Y 27 ENERO</p>
                                    <p class="lugar">CINTERMEX</p>
                                </div>
                            </div>
                        </a>
                    </div> -->
                    <!-- <div class="col s12 m6 l3">
                        <a href="<?php echo base_url() ?>home/expo?estado=cdmx">
                            <div class="evento">
                                <p class="ciudad">Cd. de México</p>
                                <div class="content-evento">
                                    <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                    <p class="fecha">8,9 Y 10 FEBRERO</p>
                                    <p class="lugar">WORLD TRADE CENTER</p>
                                </div>
                            </div>
                        </a>
                    </div> -->
                    <!-- <div class="col s12 m6 l3">
                        <a href="<?php echo base_url() ?>home/expo?estado=Guanajuato">
                            <div class="evento">
                                <p class="ciudad">León</p>
                                <div class="content-evento">
                                    <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                    <p class="fecha">16 Y 17 FEBRERO</p>
                                    <p class="lugar">POLIFORUM</p>
                                </div>
                            </div>
                        </a>
                    </div> -->
                    <!-- <div class="col s12 m6 l3">
                        <a href="<?php echo base_url() ?>home/expo?estado=Veracruz">
                            <div class="evento">
                                <p class="ciudad">Veracruz</p>
                                <div class="content-evento">
                                    <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                    <p class="fecha">23 Y 24 FEBRERO</p>
                                    <p class="lugar">WORLD TRADE CENTER</p>
                                </div>
                            </div>
                        </a>
                    </div> -->
                </div>
            </div>
        </section>
        <section class="hide-on-med-and-up">
            <div class="eventos row">
                <!-- <div class="col s12 m6 l3">
                    <a href="<?php echo base_url() ?>brideweekend/expo_culiacan">
                        <div class="evento">
                            <p class="ciudad">Culiacán</p>
                            <div class="content-evento">
                                <p class="dia"><span>SABADO & DOMINGO</span></p>
                                <p class="fecha">06 y 07 ABRIL</p>
                                <p class="lugar">SALÓN FIGLOSTASE</p>
                            </div>
                        </div>
                    </a>
                </div> -->
                <div class="col s12 m6 l3">
                    <a href="<?php echo base_url() ?>brideweekend/expo_puebla">
                        <div class="evento">
                            <p class="ciudad">Puebla</p>
                            <div class="content-evento">
                                <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                <p class="fecha">24 Y 25 AGOSTO</p>
                                <p class="lugar">CENTRO EXPOSITOR LOS FUERTES</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col s12 m6 l3">
                    <a href="<?php echo base_url() ?>brideweekend/expo_qro">
                        <div class="evento">
                            <p class="ciudad">Querétaro</p>
                            <div class="content-evento">
                                <p class="dia"><span>SÁBADO Y DOMINGO</span></p>
                                <p class="fecha">07 Y 08 DE SEPTIEMBRE</p>
                                <p class="lugar">CENTRO DE CONGRESOS</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col s12 m6 l3 margin">
                    <a href="<?php echo base_url() ?>brideweekend/expo_gdl">
                        <div class="evento">
                            <p class="ciudad">Guadalajara</p>
                            <div class="content-evento">
                                <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                <p class="fecha">25 Y 26 DE ENERO</p>
                                <p class="lugar">EXPO GUADALAJARA</p>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- <div class="col s12 m6 l3">
                    <a href="<?php echo base_url() ?>home/expo?estado=Nuevo_leon">
                        <div class="evento">
                            <p class="ciudad">Monterrey</p>
                            <div class="content-evento">
                                <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                <p class="fecha">26 Y 27 ENERO</p>
                                <p class="lugar">CINTERMEX</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col s12 m6 l3">
                    <a href="<?php echo base_url() ?>home/expo?estado=cdmx">
                        <div class="evento">
                            <p class="ciudad">Cd. de México</p>
                            <div class="content-evento">
                                <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                <p class="fecha">8,9 Y 10 FEBRERO</p>
                                <p class="lugar">WORLD TRADE CENTER</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col s12 m6 l3">
                    <a href="<?php echo base_url() ?>home/expo?estado=Guanajuato">
                        <div class="evento">
                            <p class="ciudad">León</p>
                            <div class="content-evento">
                                <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                <p class="fecha">16 Y 17 FEBRERO</p>
                                <p class="lugar">POLIFORUM</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col s12 m6 l3">
                    <a href="<?php echo base_url() ?>home/expo?estado=Veracruz">
                        <div class="evento">
                            <p class="ciudad">Veracruz</p>
                            <div class="content-evento">
                                <p class="dia"><span>SÁBADO & DOMINGO</span></p>
                                <p class="fecha">23 Y 24 FEBRERO</p>
                                <p class="lugar">WORLD TRADE CENTER</p>
                            </div>
                        </div>
                    </a>
                </div> -->
            </div>
        </section>
    </div>

<?php
//$this->load->view("general/footer");
$this->load->view("japy/footer");
?>