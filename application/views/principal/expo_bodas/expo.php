<?php
$this->load->view("general/head");
$interval = $now->diff($fexpo);
?>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/expo_landing.css">
    <style>
        .counter {
            background-image: url('<?php echo base_url()?>/dist/img/registro_expo/BG-REGISTRO.png');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: top;
            height: auto;
            position: relative;
        }

        .word {
            margin: 2rem auto;
        }

        .word h3 {
            font-size: 4.5rem;
            color: #8dd7e0;
            font-family: "Dancing Script", cursive;
            margin: 0;
        }

        .container-block {
            background: #e0e0e0;
            border: none;
            height: 15rem;
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
            padding: 1.5rem;
        }

        .container-block div {
            padding: 0 !important;
        }

        .container-block div input {
            margin: 0 !important;
            padding: 1rem !important;

        }

        .date-text-big {
            font-family: 'Dancing Script', cursive;
            font-size: 4.5rem;
        }

        .display-content h3 {
            font-family: 'Dancing Script', cursive;
            font-size: 1.8rem;
            text-transform: none;
        }

        .display-content:first-child h3 {
            font-family: 'Great vibes', cursive;
            text-transform: none;
            font-size: 5.5rem !important;
            letter-spacing: .6rem !important;
            font-weight: 100;

        }

        .logo {
            background-image: url('<?php echo base_url()?>/dist/img/expo_bodas/fondo.png');
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
        }

        @media only screen and (max-width: 321px) {
            .counter {
                background-image: url('<?php echo base_url()?>/dist/img/expo_bodas/background-degradado.png');
                background-repeat: no-repeat;
                background-size: cover;
                background-position: left;
                height: 100% !important;
                position: relative;
            }

            .display-content:first-child h3 {
                font-size: 4.5rem !important;
            }
        }

        @media only screen and (min-width: 322px) and (max-width: 425px) {
            .counter {
                background-image: url('<?php echo base_url()?>/dist/img/expo_bodas/background-degradado.png');
                background-repeat: no-repeat;
                background-size: cover;
                background-position: left;
                height: 100% !important;
                position: relative;
            }

            .logo {
                width: 40vw;
                margin-top: -90vh;
            }

            .sello {
                width: 44vw;
            }

            .display-content:first-child h3 {
                font-size: 4.5rem !important;
            }
        }
    </style>

    <div class="max-content">
        <div class="col l12 m12 s12">
            <img class="responsive-img banner_expo" style="width: 100% !important; object-fit: fill !important;"/>
        </div>
        <section class="counter row no-margin">
            <div class="col s12 m10 offset-m1" style="z-index: 100; position: relative;">
                <div class="row">
                    <div class="col s12 l4 offset-l3 hide-on-med-and-down" style="margin-top: 10rem">
                        <?php if ($this->checker->isLogin()) { ?>
                            <img class="sello " src="<?php echo base_url() ?>dist/img/expos/sello.png" alt="">
                        <?php } else { ?>
                            <div class="word center-align">
                                <h3>Registrate</h3>
                            </div>
                            <form action="<?php echo base_url()."/registro" ?>" method="POST" id="registro"
                                  data-parsley-validate="true">
                                <input type="hidden" name="genero" value="2">
                                <input type="hidden" name="come_from" value="">
                                <div class="col s12 m8 offset-m2 l12 formulario ">
                                    <div class="row container-block">
                                        <div class="col s12 ">
                                            <input class=" basic-style white" type="text" name="nombre" data-parsley-length="[2,30]"
                                                   placeholder="Nombre(s)*" required="">
                                        </div>
                                        <div class="col s12">
                                            <!-- DIVIDER -->
                                            <input class=" basic-style white" type="email" id="correo" name="correo"
                                                   placeholder="Correo*" data-parsley-type="email" required="">
                                        </div>
                                        <div class="col s12  bt-flabels__wrapper">
                                            <input class="basic-style white" id="password" type="password" name="contrasena"
                                                   placeholder="Ingresa tú Contrase&ntilde;a*" data-parsley-length="[2,30]" required="">
                                            <!-- DIVIDER -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 m8 offset-m2 center-align">
                                            <button class="btn btn-block primary-background darken-5" id="btn_enviar"
                                                    type="submit"
                                                    style="margin: 30px 0;padding: 0">
                                                CONTINUAR
                                            </button>

                                        </div>
                                        <div class="col s12 center-align">
                                            <button class="btn btn-block bg-fb btn-face" id="facebook-login">
                                                <i class="fa fa-facebook-official"></i> <span class="hide-on-med-and-down">Continuar con </span>
                                                <!-- Facebook -->
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>
                    </div>
                    <div class="data col s12 m6 offset-m6 l5 center-align">
                        <div class="logo">
                            <img style="width: 100%" src="<?php echo base_url() ?>dist/img/registro_expo/logo-expo.png" alt="">
                        </div>
                        <?php if ($this->checker->isLogin()): ?>
                            <div class="sello hide-on-large-only">
                                <img class="image-responsive" src="<?php echo base_url() ?>dist/img/expos/sello.png" alt="">
                            </div>
                        <?php else : ?>
                            <div class="row">
                                <div class="word center-align hide-on-med-and-up">
                                    <h3>Registrate</h3>
                                </div>
                                <form class="hide-on-med-and-up" action="<?php echo base_url()."/registro" ?>" method="POST" id="registroexpo"
                                      data-parsley-validate="true" style="box-sizing: border-box">
                                    <input type="hidden" name="genero" value="2">
                                    <input type="hidden" name="come_from" value="">
                                    <div class="col s12 m8 offset-m2 l12 formulario ">
                                        <div class="row container-block">
                                            <div class="col s12 ">
                                                <input class=" basic-style white" type="text" name="nombre" data-parsley-length="[2,30]"
                                                       placeholder="Nombre(s)*" required="">
                                            </div>
                                            <div class="col s12">
                                                <!-- DIVIDER -->
                                                <input class=" basic-style white" type="email" id="correo" name="correo"
                                                       placeholder="Correo*" data-parsley-type="email" required="">
                                            </div>
                                            <div class="col s12  bt-flabels__wrapper">
                                                <input class="basic-style white" id="password" type="password" name="contrasena"
                                                       placeholder="Ingresa tú Contrase&ntilde;a*" data-parsley-length="[2,30]" required="">
                                                <!-- DIVIDER -->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m8 offset-m2 center-align">
                                                <button class="btn btn-block primary-background darken-5" id="btn_enviar_expo"
                                                        type="submit"
                                                        style="margin: 30px 0;padding: 0">
                                                    CONTINUAR
                                                </button>

                                            </div>
                                            <div class="col s12 center-align">
                                                <button class="btn btn-block bg-fb btn-face" id="facebook-login">
                                                    <i class="fa fa-facebook-official"></i> <span class="hide-on-med-and-down">Continuar con </span>Facebook
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        <?php endif; ?>
                        <?php if ($interval->format('%a') > 0): ?>
                            <p>
                            <h3 class="date-text date-text-big">Estamos a:</h3>
                            </p>
                            <div class="row" id="event-counter">
                                <div class="faltante col s12" style="padding: 0">
                                    <div class="col s3 center-align counter-container">
                                        <p class="counter-title ">D&iacute;as</p>
                                        <h5 class="gris-1-text no-margin counter-data">
                                            <b class="dias" id="event-days">
                                                <?php echo $interval->format('%a'); ?>
                                            </b>
                                        </h5>
                                    </div>
                                    <div class="col s3 center-align counter-container">
                                        <p class="counter-title ">Horas</p>
                                        <h5 class="gris-1-text no-margin counter-data">
                                            <b class="horas" id="event-hours">
                                                <?php echo $interval->format('%h'); ?>
                                            </b>
                                        </h5>
                                    </div>
                                    <div class="col s3 center-align counter-container">
                                        <p class="counter-title">Min</p>
                                        <h5 class="gris-1-text no-margin counter-data">
                                            <b class="minutos" id="event-minutes">
                                                <?php echo $interval->format('%i'); ?>
                                            </b>
                                        </h5>
                                    </div>
                                    <div class="col s3 center-align counter-container">
                                        <p class="counter-title">Seg</p>
                                        <h5 class="gris-1-text no-margin counter-data">
                                            <b class="segundos" id="event-seconds">
                                                <?php echo $interval->format('%s'); ?>
                                            </b>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="eventDate" value="<?php echo $eventDate ?>">
                        <?php else: ?>
                            <div class="default-counter"></div>
                        <?php endif; ?>
                        <div class="divider separator"></div>
                        <p class="date-text date"></p><br>
                        <p class="date-text place"></p>
                        <!-- <div class="extra-mensaje">
                            <p style="font-weight: bold; font-size: 1.5rem">¡Visita el evento más important de Latinoamérica!</p>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="logo" style="background-image: unset !important; width: 100%; height: 63px; transform: translate(0, -129%);">
                <img style="width: 100%" src="<?php echo base_url() ?>dist/img/registro_expo/BG-AUTO.png" alt="">
            </div>
        </section>

        <section class=" row blog">
            <div class="col s12 m12 l10 offset-l1">
                <div class="row no-margin">
                    <div class="col s12 m4 display-content">
                        <h3>Bienvenida a</h3>
                        <img src="<?php echo base_url() ?>dist/img/japy_nobg_cut.png" class="image-responsive" alt="">
                    </div>
                    <div class="col s12 m4 display-content">
                        <h3>Tendencias para novias este 2019</h3>
                        <a href="">
                            <img src="<?php echo base_url() ?>dist/img/expo_bodas/foto2.png" class="image-responsive" alt="">
                        </a>
                    </div>
                    <div class="col s12 m4 display-content">
                        <h3>Pasos para realizar tú boda por la iglesia</h3>
                        <a href="<?php echo base_url() ?>blog/post/ver/43">
                            <img src="<?php echo base_url() ?>dist/img/expo_bodas/foto1.png" class="image-responsive" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- <section id="video_gdl" class="video row center-align">
            <div class="col s12 m10 offset-m1 center-align">
                <div class="row center-align">
                        <video 
                            style="background-repeat: no-repeat;
                                        background-position: center;
                                        background-size:cover;
                                        height: 300px;
                                        max-width: 100%" 
                            contextmenu="false" controls="true"
                            src="<?php echo(base_url().'uploads/videos/expo-tu-boda-redes-high.mp4') ?>">
                        </video>
                </div>
            </div>
        </section> -->

        <section class="info_expo row">
            <div class="col s12 m10 offset-m1">
                <div class="row">
                    <div class="col s12 center-align margin-text">
                        <h4>¡No lo dejes pasar!</h4>
                    </div>
                    <div class="col s12 m8 offset-m2 l6">
                        <div id="image_up_video" class="image" style="
                                background-repeat: no-repeat;
                                background-size: cover;
                                background-position: center;
                                height: 24rem">

                        </div>
                        <section id="video_gdl" class="video row center-align">
            <div class="col s12 m10 offset-m1 center-align">
                <div class="row center-align">
                        <video 
                            style="background-repeat: no-repeat;
                                        background-position: center;
                                        background-size:cover;
                                        height: 300px;
                                        max-width: 100%" 
                            contextmenu="false" controls="true"
                            src="<?php echo(base_url().'uploads/videos/expo-tu-boda-redes-high.mp4') ?>">
                        </video>
                </div>
            </div>
        </section>
                    </div>
                    <div class="col s12 m8 offset-m2 l6 center-align">
                        <div>
                            <p class="direction-text">

                                Te esperamos en <b><span class="place"> Centro de Congreso</span> <span class="city"></span></b>: <span class="direction"></span> <br><br>
                                Horario de <b>Expo Tu Boda <span class="city"></span>: 11:00 am - 8:00 pm</b><br><br>
                                Más de mil opciones para elegir entre las <b>EXCLUSIVAS</b> casas de novias; ven y
                                disfruta de las <b>PASARELAS</b> en sus
                                horarios: <span class="date"> </span> 2:30 pm y 5:30 pm
                            </p>
                        </div>
                        <a style="margin: auto" href="#" class="btn waves-effect waves-light"><span
                                    class="hide-on-med-and-down">Programa de</span> Actividades</a>
                    </div>
                </div>
            </div>
        </section>


        <?php if($estado=="queretaro") { ?>
        <section class="info_expo row">
            <div class="col s12 m10 offset-m1">
                <div class="row">
                    <div class="col s12 center-align margin-text">
                        <h4>Ganadores de sorteos:</h4>
                        <h5>
                            A partir del Lunes 21 de Enero
                        </h5>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>




        <section class="dress row">
            <div class="col s12 m10 offset-m1">
                <div class="row">
                    <div class="col s12 center-align margin-text">
                        <h4>¡Más de <span class="dresses">1000</span> Vestidos de Novia en exhibición!</h4>
                    </div>
                    <div class="container photo-cont">
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="" alt="">
                        </a>
                        <a class="foto" href="">
                            <img src="<" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="providers row">
            <div class="col s12 m10 offset-m1">
                <div class="row">
                    <div class="col s12 center-align margin-text">
                        <h4>¡Más de <span class="exhibitors">200</span> Expositores!</h4>
                    </div>
                    <div class="container photo-cont">
                        <a href="<?php echo base_url() ?>" class="foto">
                            <img src="" alt="">
                        </a>
                        <a href="<?php echo base_url() ?>" class="foto">
                            <img src="" alt="">
                        </a>
                        <a href="<?php echo base_url() ?>" class="foto">
                            <img src="" alt="">
                        </a>
                        <a href="<?php echo base_url() ?>" class="foto">
                            <img src="" alt="">
                        </a>
                    </div>
                    <div class="col s12 m4 offset-m4 center-align margin-text">
                        <!-- <a href="" class="btn waves-effect waves-light"><span
                                    class="hide-on-med-and-down">Directorio de </span> Expositores</a> -->
                                    <!-- <button class="btn modal-trigger mostrar" data-target="modal1">Directorio de expositores</button> -->
                            <?php if($estado=="puebla") { ?>
                                <a href="<?php echo base_url() ?>DirectorioPUE.pdf" class="btn waves-effect waves-light" target="_blank">Directorio de Expositores</a>
                            <?php } else if($estado=="queretaro") { ?>
                                <a href="<?php echo base_url() ?>DirectorioQRO.pdf" class="btn waves-effect waves-light" target="_blank">Directorio de Expositores</a>
                            <?php } else { ?>
                                <a href="" class="btn waves-effect waves-light"><span
                                    class="hide-on-med-and-down">Directorio de </span> Expositores</a>
                            <?php } ?>
                            
                    </div>

                </div>
            </div>
        </section>
        <input id="server" type="hidden" value="<?php echo base_url() ?>">

    </div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            const $registerForm = $('#registro').parsley();

            $('#btn_enviar').on('click', function(e) {

                e.preventDefault();

                $registerForm.validate();

                if ($registerForm.isValid()) {
                    $.ajax({
                        url: '<?php echo base_url()."registro/checkUsuario" ?>',
                        method: 'post',
                        data: {
                            'correo': $('#correo').val(),
                        },
                        success: function(res) {
                            if (res.data == true) {
                                swal('Error', 'Correo registrado anteriormente.', 'error');
                                return false;
                            }

                            $('#registro').submit();
                        },
                        error: function() {
                            swal('Error', 'Lo sentimos ocurrio un error', 'error');
                        },
                    });
                }
            });
        });
    </script>

    <script>
    var cabecera320="";
    var cabecera420="";
    var cabecera768="";
    var cabecera1024="";
    var bandera=0;

        $(document).ready(function() {
            
            <?php if($estado=="puebla") { ?>
                cabecera320 = '/uploads/images/slider320 expo_JAPY PUE.png';
                cabecera420 = '/uploads/images/slider420 expo_JAPY PUE.png';
                cabecera768 = '/uploads/images/slider768 expo_JAPY PUE.png';
                cabecera1024 = '/dist/img/registro_expo/cabecerapbl.png';
                bandera=1;
            <?php } else if($estado=="queretaro") { ?>
                cabecera320 = '/uploads/images/slider320 expo_JAPY QRO.png';
                cabecera420 = '/uploads/images/slider420 expo_JAPY QRO.png';
                cabecera768 = '/uploads/images/slider768 expo_JAPY QRO.png';
                cabecera1024 = '/dist/img/registro_expo/cabeceraqro.png';
                bandera=1;
            <?php } else if($estado=="jalisco") { ?>
                cabecera320 = '/uploads/images/slider320 expo_JAPY GDL.png';
                cabecera420 = '/uploads/images/slider420 expo_JAPY GDL.png';
                cabecera768 = '/uploads/images/slider768 expo_JAPY GDL.png';
                cabecera1024 = '/dist/img/registro_expo/cabeceragdl.png';
                bandera=1;
            <?php } else if($estado=="sinaloa") { ?>
                cabecera320 = '/uploads/images/slider320 expo_JAPY GDL.png';
                cabecera420 = '/uploads/images/slider420 expo_JAPY GDL.png';
                cabecera768 = '/uploads/images/slider768 expo_JAPY GDL.png';
                cabecera1024 = '/dist/img/registro_expo/cabeceraCuliacan.png';
                bandera=1;
            <?php } else {?>
                bandera=0;
            <?php } ?>

            // alert(cabecera1024);
            if(bandera==1) {
                var medida= $( window ).width();
                // alert("medida= "+medida);
                if (medida< 326) {
                    $('img.banner_expo').attr('src', '<?php echo base_url() ?>'+cabecera320);
                    $(".msj").css('display','none');
                    // alert("1");
                }
                else if (medida < 426) {
                    $('img.banner_expo').attr('src', '<?php echo base_url() ?>'+cabecera420);
                    // alert("2");
                }
                else if (medida < 769) {
                    $('img.banner_expo').attr('src', '<?php echo base_url() ?>'+cabecera768);
                    $(".msj").css('display','inline');
                    // alert("3");
                }
                else if (medida < 2100) {
                    $('img.banner_expo').attr('src', '<?php echo base_url() ?>'+cabecera1024);
                    $(".msj").css('display','inline');
                    // alert("4");
                }

                $(window).resize(function() {
                    var medida= $( window ).width();
                    // alert("medida= "+medida);
                    if (medida< 326) {
                        $('img.banner_expo').attr('src', '<?php echo base_url() ?>'+cabecera320);
                        $(".msj").css('display','none');
                        // alert("1");
                    }
                    else if (medida < 426) {
                        $('img.banner_expo').attr('src', '<?php echo base_url() ?>'+cabecera420);
                        // alert("2");
                    }
                    else if (medida < 769) {
                        $('img.banner_expo').attr('src', '<?php echo base_url() ?>'+cabecera768);
                        $(".msj").css('display','inline');
                        // alert("3");
                    }
                    else if (medida < 2100) {
                        $('img.banner_expo').attr('src', '<?php echo base_url() ?>'+cabecera1024);
                        $(".msj").css('display','inline');
                        // alert("4");
                    }
                });
            }

            let url = new URL(window.location.href);
            let currentState = (url.searchParams.get('estado') === null) ? 'Jalisco' : url.searchParams.get('estado');
            let image_event;
            let server = $('#server').val();
            const eventDate = $('#eventDate').val();
            setCountDownTimer(eventDate);
            formPosition();
            
            if(url.searchParams.get('estado') != 'jalisco'){
                $('#video_gdl').remove();
               
            }else{
                $('#image_up_video').remove();
            }

            $('input[name=\'come_from\']').val(currentState);

            const eventsInfo = [
                // {
                //     'state': 'queretaro',
                //     'imageFolder': 'expo-queretaro',
                //     'directionText': 'Te esperamos en <b>Centro de Congresos Querétaro</b>: Paseo de las artes No. 1531 B Cd. De las Artes Querétaro, Qro <br><br>' +
                //     'Horario de <b>Expo tu Boda Querétaro: 11:00 am - 8:00 pm</b><br><br>' +
                //     'Más de mil opciones de vestidos para elegir entre las <b>EXCLUSIVAS</b> casas de novias; ven y disfruta de las <b>PASARELAS</b> en sus horarios: Domingo 9 de Septiembre 2:30 pm y 5:30 pm',
                // },
                // {
                //     'state': 'jalisco',
                //     'imageFolder': 'expo-guadalajara',
                //     'directionText': 'Te esperamos en <b>Expo tu Boda Guadalajara</b>: Av. Mariano Otero No. 1400, Col Verde Valle, Guadalajara, Jalisco <br><br>' +
                //     'Horario de <b>Expo tu Boda Guadalajara: 11:00 am - 8:00 pm</b><br><br>' +
                //     'Más de 1,500 opciones de vestidos para elegir entre las <b>EXCLUSIVAS</b> casas de novias; ven y disfruta de las <b>PASARELAS</b> en sus horarios: Sábado 22 y Domingo 23 de Septiembre 2:30 pm y 5:30 pm',
                // },
                // {
                //     'state': 'jalisco',
                //     'imageFolder': 'expo-guadalajara',
                //     'directionText': 'Te esperamos en <b>Expo tu Boda Guadalajara</b>: Av. Mariano Otero No. 1400, Col Verde Valle, Guadalajara, Jalisco <br><br>' +
                //     'Horario de <b>Expo tu Boda Guadalajara: 11:00 am - 8:00 pm</b><br><br>' +
                //     'Más de 1,500 opciones de vestidos para elegir entre las <b>EXCLUSIVAS</b> casas de novias; ven y disfruta de las <b>PASARELAS</b> en sus horarios: Sábado 22 y Domingo 23 de Septiembre 2:30 pm y 5:30 pm',
                // },
                {
                    'state': 'Queretaro',
                    'imageFolder': 'expo-queretaro',
                    'directionText': 'Te esperamos en <b>Centro de Congresos Querétaro</b>: Paseo de las artes No. 1531 B Cd. De las Artes Querétaro, Qro <br><br>' +
                    'Horario de <b>Bride Weekend Querétaro: Sábado 07 y Domingo 08 de Septiembre 11:00AM. A 8:00PM</b><br><br>' +
                    'Más de mil opciones de vestidos para elegir entre las <b>EXCLUSIVAS</b> casas de novias; ven y disfruta de las <b>PASARELAS</b> en su horario: Domingo 14:30 y 17:30 HORAS.',
                },
                {
                    'state': 'Jalisco',
                    'imageFolder': 'expo-guadalajara',
                    'directionText': 'Te esperamos en <b>Bride Weekend Guadalajara</b>: Av. Mariano Otero No. 1400, Col Verde Valle, Guadalajara, Jalisco <br><br>' +
                    'Horario de <b>Bride Weekend Guadalajara: 12:00 pm - 8:00 pm</b><br><br>' +
                    'Más de 1,500 opciones de vestidos para elegir entre las <b>EXCLUSIVAS</b> casas de novias; ven y disfruta de las <b>PASARELAS</b> en sus horarios: Sábado 29 y Domingo 30 de Junio 2:30 pm y 5:30 pm',
                },
                {
                    'state': 'Puebla',
                    'imageFolder': 'expo-puebla',
                    'directionText': 'Te esperamos en <b>Bride Weekend Puebla</b>: Zona de los Fuertes, Cívica 5 de Mayo, 72260 Puebla, Puebla. <br><br>' +
                    'Horario de <b>Bride Weekend Puebla: 12:00 pm - 8:00 pm</b><br><br>' +
                    'Más de 1,000 opciones de vestidos para elegir entre las <b>EXCLUSIVAS</b> casas de novias; ven y disfruta de las <b>PASARELAS</b> en sus horarios: Sábado 24 y Domingo 25 de Agosto 2:30 pm y 5:30 pm',
                },
                {
                    'state': 'Sinaloa',
                    'imageFolder': 'expo-puebla',
                    'directionText': 'Te esperamos en <b>Bride Weekend Culiacàn</b>: Blvd. Rotarismo 3344, Humaya, 80020 Culiacán Rosales, Sinaloa <br><br>' +
                    'Horario de <b>Bride Weekend Culiacàn: 12:00 pm - 8:00 pm</b><br><br>' +
                    'Más de 1,000 opciones de vestidos para elegir entre las <b>EXCLUSIVAS</b> casas de novias; ven y disfruta de las <b>PASARELAS</b> en sus horarios: Sábado 06 y Domingo 07 de Abril 2:30 pm y 5:30 pm',
                },
                
            ];

            for (let event of eventsInfo) {
                image_event = server + 'dist/img/expos/' + event.imageFolder + '/evento.png';
                if (event.state == currentState) {
                    $('.direction-text').html(event.directionText);
                    $('.image').css('background-image', 'url("' + image_event + '")');
                    $('.foto img').each(function(index) {
                        $(this).attr('src', '<?php echo base_url() ?>dist/img/expos/' + event.imageFolder + '/foto' + index + '.png');
                    });
                    break;
                }
                else {
                    $('.direction-text').html(event.directionText);
                    $('.image').css('background-image', 'url("' + image_event + '")');
                    $('.foto img').each(function(index) {
                        $(this).attr('src', '<?php echo base_url() ?>dist/img/expos/' + event.imageFolder + '/foto' + index + '.png');
                    });
                }
            }
        });

        function setCountDownTimer(eventDate) {
            let countDownDate = new Date(eventDate).getTime();
            const $eventDays = $('#event-days');
            const $eventHours = $('#event-hours');
            const $eventMinutes = $('#event-minutes');
            const $eventSeconds = $('#event-seconds');
            const $eventCounter = $('#event-counter');

            setInterval(function() {

                let now = new Date().getTime();

                let distance = countDownDate - now;

                let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                let seconds = Math.floor((distance % (1000 * 60)) / 1000);

                $eventDays.html(days);
                $eventHours.html(hours);
                $eventMinutes.html(minutes);
                $eventSeconds.html(seconds);

                if (distance < 0) {
                    $eventCounter.hide();
                }
            }, 1000);

        }

        function formPosition() {
            if (screen.width < 426 && $('.sello').is(':visible')) {
                $('.counter').css({'margin-bottom': '16rem'});
            }
            else if (screen.width < 426) {
                $('.counter').css({'margin-bottom': '50rem'});
            }
        }

    </script>
<?php
$this->load->view("general/footer");
$this->load->view("general/newfooter");
?>