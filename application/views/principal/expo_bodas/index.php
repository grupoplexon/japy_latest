<html>
<head>
    <?php $this->view("principal/head"); ?>
    <style>
        hr.separador-p {
            background-color: #b9babc;
            height: 1px;
            border: 0;
        }
    </style>
</head>
<body>
    <?php $this->view("principal/menu"); ?>

    <div class="body-container z-depth-1" style="overflow: hidden;">
        <img class="responsive-img" style="width: 100%;"
             src="<?php echo base_url() ?>dist/img/expo_bodas/image_principal.png" alt=""/>
        <div style="background: url(<?php echo base_url() ?>dist/img/textura_footer2.png);">
            <div class="container" style="padding-bottom: 20px; padding-top: 20px;">
                <br/><br/><br/>
                <h5 class="grey-text darken-5 align-center" style="font-weight: bold;">
                    En clubnupcial nos enorgullecemos de ser socios comerciales de la Expo de bodas m&aacute;s
                    importantes en Latinoam&eacute;rica.
                    <br/><br/>
                    Te compartimos los pr&oacute;ximos eventos
                </h5>
                <br/><br/><br/>
            </div>
        </div>

        <div class="container">
            <div style="margin:22px">&nbsp;</div>
            <div class="row">
                <div class="col s4">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/vineta2.png" alt="">
                </div>
                <div class="col s4">
                    <p class="dorado-2-text seccion-title"><b>PR&Oacute;XIMOS EVENTOS</b></p>
                </div>
                <div class="col s4">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/vineta3.png" alt="">
                </div>
            </div>
            <div style="margin:8px">&nbsp;</div>
            <!-- GUADALAJARA -->
            <div class="row">
                <div class="col s5">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/expo_bodas/guadalajara.png"
                         alt=""/>
                </div>
                <div class="col s7">
                    <h6 class="grey-text darken-5" style="font-weight: bold;">EXPO TU BODA GUADALAJARA</h6>
                    <br/><br/>
                    <div class="grey-text darken-5">29 y 30 de Octubre 2016 en Expo Guadalajara</div>
                    <br/><br/>
                    <a class="btn-flat dorado-2 white-text" href="http://www.expotuboda.com.mx/guadalajara/"
                       target="_blank" style="text-transform: none;">M&aacute;s informaci&oacute;n</a>
                    <br/><br/>
                    <h6 class="grey-text darken-5">www.expotuboda.com.mx/guadalajara/</h6>
                </div>
            </div>
            <hr class="separador-p"/>
            <!-- CIUDAD DE MEXICO -->
            <div class="row">
                <div class="col s5">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/expo_bodas/ciudadmexico.png"
                         alt=""/>
                </div>
                <div class="col s7">
                    <h6 class="grey-text darken-5" style="font-weight: bold;">EXPO TU BODA CIUDAD DE M&Eacute;XICO</h6>
                    <br/><br/>
                    <div class="grey-text darken-5">14, 15 y 16 de Octubre 2016 World Trade Center</div>
                    <br/><br/>
                    <a class="btn-flat dorado-2 white-text" href="http://www.expotuboda.com.mx/mexico/" target="_blank"
                       style="text-transform: none;">M&aacute;s informaci&oacute;n</a>
                    <br/><br/>
                    <h6 class="grey-text darken-5">www.expotuboda.com.mx/mexico/</h6>
                </div>
            </div>
            <hr class="separador-p"/>
            <!-- MONTERREY -->
            <div class="row">
                <div class="col s5">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/expo_bodas/monterrey.png" alt=""/>
                </div>
                <div class="col s7">
                    <h6 class="grey-text darken-5" style="font-weight: bold;">EXPO TU BODA MONTERREY</h6>
                    <br/><br/>
                    <div class="grey-text darken-5">29 y 30 de octubre 2016 Cintermex</div>
                    <br/><br/>
                    <a class="btn-flat dorado-2 white-text" href="http://www.expotuboda.com.mx/monterrey/"
                       target="_blank" style="text-transform: none;">M&aacute;s informaci&oacute;n</a>
                    <br/><br/>
                    <h6 class="grey-text darken-5">www.expotuboda.com.mx/monterrey/</h6>
                </div>
            </div>
            <hr class="separador-p"/>
            <!-- LEON -->
            <div class="row">
                <div class="col s5">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/expo_bodas/leon.png" alt=""/>
                </div>
                <div class="col s7">
                    <h6 class="grey-text darken-5" style="font-weight: bold;">EXPO TU BODA LE&Oacute;N</h6>
                    <br/><br/>
                    <div class="grey-text darken-5">24 y 25 de Septiembre 2016 Poliforum Le&oacute;n</div>
                    <br/><br/>
                    <a class="btn-flat dorado-2 white-text" href="http://www.expotuboda.com.mx/leon/" target="_blank"
                       style="text-transform: none;">M&aacute;s informaci&oacute;n</a>
                    <br/><br/>
                    <h6 class="grey-text darken-5">www.expotuboda.com.mx/leon/</h6>
                </div>
            </div>
            <hr class="separador-p"/>
            <!-- QUERETARO -->
            <div class="row">
                <div class="col s5">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/expo_bodas/queretaro.png" alt=""/>
                </div>
                <div class="col s7">
                    <h6 class="grey-text darken-5" style="font-weight: bold;">EXPO TU BODA QUER&Eacute;TARO</h6>
                    <br/><br/>
                    <div class="grey-text darken-5">15 de Enero 2017 Centro de Congresos</div>
                    <br/><br/>
                    <a class="btn-flat dorado-2 white-text" href="http://www.expotuboda.com.mx/queretaro/"
                       target="_blank" style="text-transform: none;">M&aacute;s informaci&oacute;n</a>
                    <br/><br/>
                    <h6 class="grey-text darken-5">www.expotuboda.com.mx/queretaro/</h6>
                </div>
            </div>
            <hr class="separador-p"/>
            <!-- PRUEBLA -->
            <div class="row">
                <div class="col s5">
                    <img class="responsive-img" src="<?php echo base_url() ?>dist/img/expo_bodas/puebla.png" alt=""/>
                </div>
                <div class="col s7">
                    <h6 class="grey-text darken-5" style="font-weight: bold;">EXPO TU BODA PUEBLA</h6>
                    <br/><br/>
                    <div class="grey-text darken-5">21 y 22 de enero 2017 Centro Expositor Los Fuertes</div>
                    <br/><br/>
                    <a class="btn-flat dorado-2 white-text" href="http://www.expotuboda.com.mx/puebla/" target="_blank"
                       style="text-transform: none;">M&aacute;s informaci&oacute;n</a>
                    <br/><br/>
                    <h6 class="grey-text darken-5">www.expotuboda.com.mx/puebla/</h6>
                </div>
            </div>
        </div>
    </div>

    <?php $this->view("principal/view_footer"); ?>
    <?php $this->view("principal/foot"); ?>
</body>
</html>