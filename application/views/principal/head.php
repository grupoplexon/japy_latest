<meta property="og:image" content="https://japybodas.com/uploads/images/bw.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:url" content="http://www.japybodas.com/brideweekend" />
    <meta property="og:site_name" content="Bride Weekend Culiacán" />
    <meta property="og:description" content="Todo lo que necesitas para tu boda este 6 y 7 de Abril en Culiacán, Centro de Convenciones Figlostase. Pasarelas con las últimas tendencias y más de 1000 vestidos de novia en exhibición." />
    <meta property="og:type" content="website" /><title><?php echo isset($title) ? $title : "BrideAdvisor" ?></title>
<meta name="theme-color" content="#f5e6df">
<meta name="msapplication-navbutton-color" content="#f5e6df">
<meta name="apple-mobile-web-app-status-bar-style" content="#f5e6df">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="icon" href="<?php echo base_url() ?>dist/img/baIcon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<?php echo base_url() ?>dist/css/iconos.css" rel="stylesheet" type="text/css"/>
<!--<link href="--><?php //echo base_url() ?><!--dist/img/clubnupcial.css" rel="icon" sizes="16x16">-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons|Raleway:300,400,500,600,700" rel="stylesheet">
<link href="<?php echo base_url() ?>dist/css/datatables.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/presupuesto.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/slick/slick.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url() ?>dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
<link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' data-cfasync='false'>window.purechatApi = {
        l: [], t: [], on: function () {
            this.l.push(arguments);
        }
    };
    (function () {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function (e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({c: 'f3f97b28-73ac-4929-a04d-527e409169b7', f: true});
                done = true;
            }
        };
    })();
</script>

<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function () {
        var mf = document.createElement("script");
        mf.type = "text/javascript";
        mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/4315bf06-815c-4f86-b787-cb87d939a11e.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>
