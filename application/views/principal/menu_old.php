<div class="pink lighten-5 z-depth-1" style="padding:10px;     display: block; position: relative;">
    <div class="container">
        <div class="row">
            <div class="col s12" style="float: left;position: relative;">
                <a href="<?php echo base_url() ?>"><small class="dorado-2-text"><b>inicio</b></small></a>
                <small class="red-tex lighten-5"><b>|</b></small>
                <a href="<?php echo base_url() ?>home/contacto"><small class="red-tex lighten-5"><b>contacto</b></small></a>
                <small class="red-tex lighten-5"><b>|</b></small>
            </div>
            <div class="col s12 m7  offset-m5">
                <?php if ($this->checker->isLogin()) { ?>
                    <div class="col m10 s12" style="margin-bottom: -35px;float: right" >
                        <div class="row clickable dropdown-button waves-effect user-information"  data-beloworigin="true"   data-activates='dropdown-login'>

                            <div class="col m6 offset-m2 s5" style="text-align: right;width: 80px;">
                                <img class="circule-img" src="<?php echo site_url('perfil/foto/') ?>/<?php echo $this->session->userdata("id_usuario") ?>" alt="" />
                            </div>
                            <div class="col m4 s6" style=" padding-left: 0px;width: calc( 70% - 80px )">
                                <h6 class="dorado-2-text" style="font-weight: bold;margin-top: 9px;margin-bottom: -5px;"><?php echo $this->session->userdata("nombre") ?></h6>
                                <p style="font-size: 12px; color: #6C6D6F;"><?php echo $this->session->userdata("genero") ?></p>
                            </div>
                            <i class="material-icons" style="position: absolute; top: 20px;color: #8C8D8F; display: block;right: 6px;" >menu</i>
                            <div class="col m1 s1" style="position: relative">
                            </div>
                        </div>
                        <ul id='dropdown-login' class='dropdown-content' style="overflow-x: hidden;min-width: 305px">
                            <?php if ($this->checker->isNovio()) { ?>
                                <li><a class="grey-text darken-5" href="<?php echo site_url('novia') ?>">
                                        <i class="material-icons left dorado-2-text">dashboard</i> Mi organizador<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                </li>
                                <li class="pink lighten-5" style="    padding: 14px 16px;">
                                    <div class="row" style="margin: 0px">
                                        <div class="col s4 align-center">
                                            <div class="align-center">
                                                <i class="material-icons center dorado-2-text">check_circle</i>&nbsp;&nbsp;<b><?php echo $this->checker->getTareasCompletadas() ?></b>
                                            </div>
                                            <small><b>Tareas completadas</b></small>
                                        </div>
                                        <div class="col s4 align-center" >
                                            <div class="align-center">
                                                <i class="material-icons center dorado-2-text">people_outline</i>&nbsp;&nbsp;<b><?php echo $this->checker->getInvitadosConfirmados() ?></b>
                                            </div>
                                            <small><b>Invitados confirmados</b></small>
                                        </div>
                                        <div class="col s4 align-center">
                                            <div class="align-center">
                                                <i class="material-icons center dorado-2-text">favorite_border</i>&nbsp;&nbsp;<b><?php echo $this->checker->getProveedoresReservados() ?></b>
                                            </div>
                                            <small><b>Proveedores reservados</b></small>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo site_url('novios/buzon') ?>"> 
                                        <i class="material-icons left dorado-2-text">mail_outline</i> Mi buz&oacute;n<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo site_url('novios/tarea') ?>"> 
                                        <i class="material-icons left dorado-2-text">content_paste</i> Mi tareas<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo site_url('novios/invitados') ?>"> 
                                        <i class="material-icons left dorado-2-text">people_outline</i> Mis invitados<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo site_url('novios/presupuesto') ?>"> 
                                        <i class="material-icons left dorado-2-text">exposure</i> Mi presupuesto<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo site_url('novios/proveedor') ?>"> 
                                        <i class="material-icons left dorado-2-text">favorite_border</i> Mis proveedores<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo site_url("novios/misvestidos") ?>"> 
                                        <i class="material-icons left dorado-2-text">wc</i> Mis vestidos<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo site_url('novios/miweb') ?>"> 
                                        <i class="material-icons left dorado-2-text">web</i> Mi web de boda<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>
                                <li>
                                    <a class="grey-text darken-5" href="<?php echo site_url('novios/perfil') ?>"> 
                                        <i class="material-icons left dorado-2-text">settings</i> Mi cuenta<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                    </a>
                                </li>
                                <li class="divider"></li>
                            <?php } else if ($this->checker->isAdmin()) { ?>
                                <li><a class="grey-text darken-5" href="<?php echo site_url('App') ?>">
                                        <i class="material-icons left dorado-2-text">dashboard</i> Panel de administraci&oacute;n<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                </li>
                            <?php } else if ($this->checker->isProveedor()) { ?>
                                <li><a class="grey-text darken-5" href="<?php echo site_url('proveedor') ?>">
                                        <i class="material-icons left dorado-2-text">dashboard</i> Panel de Proveedor<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                </li>
                            <?php } else if ($this->checker->isModerador()) { ?>
                                <li><a class="grey-text darken-5" href="<?php echo site_url('novios/moderador') ?>">
                                        <i class="material-icons left dorado-2-text">web</i> Comunidad<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                </li>
                            <?php } ?>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo site_url('cuenta/logout') ?>">
                                    <i class="material-icons left dorado-2-text">close</i>
                                    Cerrar Sesi&oacute;n
                                </a>
                            </li>
                        </ul>
                    </div>
                <?php } else { ?>
                    <a href="<?php echo site_url("home/altaEmpresas") ?>" class="btn-flat  btn-block-on-small pull-right dorado-2-text">Empresas</a>
                    <a href="<?php echo site_url("registro") ?>" class="btn-flat btn-block-on-small pull-right dorado-2-text">Registrate<i class="material-icons right">keyboard_arrow_right</i></a>
                    <a href="<?php echo site_url("cuenta") ?>" class="btn-flat  btn-block-on-small pull-right gray-text darken-6">Iniciar Sesi&oacute;n<i class="material-icons right">keyboard_arrow_right</i></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container" >
        <img class="logo_header" src="<?php echo base_url() ?>/dist/img/logo.png" alt=""/>
    </div>
    <nav class="title pink lighten-5">
        <div class="nav-wrapper">
            <div class="container" style="    overflow: hidden;height: 75px;">
                <a href="#" data-activates="mobile-demo" class="button-collapse dorado-2-text">
                    <i class="material-icons">menu</i>
                </a>
                <!-- MENU  -->
                <ul class="center hide-on-med-and-down">
                    <?php if (!$this->checker->isModerador()) { ?>
                        <li><a class="dorado-2-text dropdown-button " href="<?php echo site_url('novia') ?>" data-activates='dropdown-miboda' data-hover="true" data-constrainwidth="false"  data-beloworigin="true">MI BODA</a></li>
                    <?php } ?>
                    <li><a class="dorado-2-text dropdown-button " href="<?php echo site_url('proveedores/sector/proveedores') ?>" data-activates='dropdown-proveedores' data-hover="true" data-constrainwidth="false"  data-beloworigin="true">PROVEEDORES</a></li>
                    <li><a class="dorado-2-text " href="<?php echo site_url('Home/expo_bodas') ?>"  >EXPO TU BODA</a></li>
                    <li><a class="dorado-2-text dropdown-button " href="<?php echo site_url("tendencia/index/") ?>" data-activates='dropdown-vestidos' data-hover="true" data-constrainwidth="false"  data-beloworigin="true"v>VESTIDOS</a></li>
                    <li><a class="dorado-2-text dropdown-button " href="<?php echo site_url('proveedores/sector/banquetes') ?>" data-activates='dropdown1' data-hover="true" data-constrainwidth="falseproveedores/sector/banquetes"  data-beloworigin="true" >BANQUETES</a></li>
                    <li><a class="dorado-2-text" href="<?php echo site_url('blog') ?>">BLOG</a></li>
                    <!--<li><a class="dorado-2-text" href="<?php echo site_url('galery') ?>">GALERIA</a></li>-->
                </ul>
                <!-- menu desplegable m&oacute;vil  -->
                <ul class="side-nav" id="mobile-demo">
                    <li>
                        <ul class="collapsible collapsible-accordion">
                            <li>
                                <?php if ($this->checker->isLogin()) { ?>
                                    <a class="collapsible-header waves-effect waves-light dorado-2-text">MI BODA</a>
                                    <div class="collapsible-body">
                                        <ul>
                                            <li><a href="<?php echo site_url("Novia") ?>"           >Mi Perfil</a></li>
                                            <li><a href="<?php echo site_url("novios/tarea") ?>"    >Tareas</a></li>
                                            <li><a href="<?php echo site_url("novios/proveedor") ?>">Proveedores</a></li>
                                            <li><a href="<?php echo site_url("novios/miscalificaciones") ?>" >Mi Calificaciones</a></li>
                                            <li><a href="<?php echo site_url("novios/misvestidos") ?>" >Mis vestidos</a></li>
                                            <li><a href="<?php echo site_url("novios/invitados") ?>" >Lista de invitados</a></li>
                                            <li><a href="<?php echo site_url("novios/mesa") ?>"      >Control de Mesas</a></li>
                                            <li><a href="<?php echo site_url("novios/presupuesto") ?>" >Presupuesto</a></li>
                                            <li><a href="" >Comunidades Novia</a></li>
                                            <li style="display:none;"><a href="<?php echo site_url("novios/miweb") ?>" >Mi Portal Web</a></li>
                                        </ul>
                                    </div>
                                <?php } else { ?>
                                    <a class="dorado-2-text" href="<?php echo site_url() ?>index.php/novia">MI BODA</a>
                                <?php } ?>
                            </li>
                            <li>
                                <a class="collapsible-header waves-effect waves-light dorado-2-text">PROVEEDORES</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a  href="<?php echo site_url('proveedores/categoria/hacienda-para-bodas') ?>"  class="sub-menu-item" >Hacienda para bodas</a></li>
                                        <li><a  href="<?php echo site_url('proveedores/categoria/restaurantes-para-bodas') ?>"  class="sub-menu-item" >Restaurantes para boda</a></li>
                                        <li><a href="<?php echo site_url('proveedores/categoria/catering-para-bodas') ?>"  class="sub-menu-item" >Catering para bodas</a></li>
                                        <li><a href="<?php echo site_url('proveedores/categoria/jardines-para-bodas') ?>"  class="sub-menu-item" >Jardines para bodas</a></li>
                                        <li><a href="<?php echo site_url('proveedores/categoria/hoteles-para-bodas') ?>"  class="sub-menu-item" >Hoteles para bodas</a></li>
                                        <li><a href="<?php echo site_url('proveedores/categoria/Salones-para-bodas') ?>"  class="sub-menu-item" >Salones para bodas</a></li>
                                        <li><a href="<?php echo site_url('proveedores/sector/banquetes?q=playa') ?>"  class="sub-menu-item" >Bodas en la playa</a></li>
                                        <li><a href="<?php echo site_url('proveedores/sector/banquetes?promociones=si') ?>" class="sub-menu-item promocion" > <i class="fa fa-tag" style="line-height: 40px"></i> Promociones</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li><a class="dorado-2-text " href="<?php echo site_url("Home/expo_bodas") ?>">EXPO TU BODA</a></li>
                            <li>
                                <a href="<?php echo site_url("tendencia/index") ?>" class="collapsible-header waves-effect waves-light dorado-2-text">VESTIDOS</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <?php foreach (productos() as $key => $p) { ?>
                                            <li>
                                                <a href="<?php echo site_url("tendencia/index/" . str_replace(" ", "-", $p->nombre)) ?>" class="sub-menu-item" >
                                                    <?php echo $p->nombre ?>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsible-header waves-effect waves-light dorado-2-text">BANQUETES</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a  href="<?php echo site_url('proveedores/categoria/hacienda-para-bodas') ?>"  class="sub-menu-item" >Hacienda para bodas</a><li>
                                        <li><a  href="<?php echo site_url('proveedores/categoria/restaurantes-para-bodas') ?>"  class="sub-menu-item" >Restaurantes para boda</a><li>
                                        <li><a href="<?php echo site_url('proveedores/categoria/catering-para-bodas') ?>"  class="sub-menu-item" >Catering para bodas</a><li>
                                        <li><a href="<?php echo site_url('proveedores/categoria/jardines-para-bodas') ?>"  class="sub-menu-item" >Jardines para bodas</a><li>
                                        <li><a href="<?php echo site_url('proveedores/categoria/hoteles-para-bodas') ?>"  class="sub-menu-item" >Hoteles para bodas</a><li>
                                        <li><a href="<?php echo site_url('proveedores/categoria/Salones-para-bodas') ?>"  class="sub-menu-item" >Salones para bodas</a><li>
                                        <li><a href="<?php echo site_url('proveedores/sector/banquetes?q=playa') ?>"  class="sub-menu-item" >Bodas en la playa</a><li>
                                        <li><a href="<?php echo site_url('proveedores/sector/banquetes?promociones=si') ?>" class="sub-menu-item promocion" > <i class="fa fa-tag" style="line-height: 40px"></i> Promociones</a><li>
                                    </ul>
                                </div>
                            </li>
                            <li><a class="dorado-2-text" href="<?php echo site_url('blog') ?>">BLOG</a></li>
                        </ul>
                    </li>


                </ul>
            </div>
        </div>
    </nav>

    <div id='dropdown1' class='dropdown-content sub-menu'>
        <div>
            <div class="row">
                <div class="col s12 m6" >
                    <a  href="<?php echo site_url('proveedores/categoria/hacienda-para-bodas') ?>"  class="sub-menu-item" >Hacienda para bodas</a>
                    <div class="divider"></div>
                    <a  href="<?php echo site_url('proveedores/categoria/restaurantes-para-bodas') ?>"  class="sub-menu-item" >Restaurantes para boda</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/catering-para-bodas') ?>"  class="sub-menu-item" >Catering para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/jardines-para-bodas') ?>"  class="sub-menu-item" >Jardines para bodas</a>
                </div>
                <div class="col s12 m6">
                    <a href="<?php echo site_url('proveedores/categoria/hoteles-para-bodas') ?>"  class="sub-menu-item" >Hoteles para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/Salones-para-bodas') ?>"  class="sub-menu-item" >Salones para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/sector/banquetes?q=playa') ?>"  class="sub-menu-item" >Bodas en la playa</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/sector/banquetes?promociones=si') ?>" class="sub-menu-item promocion" > <i class="fa fa-tag" style="line-height: 40px"></i> Promociones</a>
                </div>
            </div>
        </div>
    </div>

    <?php if (!$this->checker->isModerador()) { ?>
        <div id='dropdown-miboda' class='dropdown-content sub-menu'>
            <div>
                <div class="row" style="margin-bottom: -5px">
                    <h6 style="padding: 0px 20px;">Mi organizador de boda </h6>
                    <a href="<?php echo site_url("novia") ?>" style="display: block;"><small class="dorado-2-text clickable" style="position: absolute;top: -5px;right: 9px;">Ver todo ></small></a>
                    <div class="divider"></div>
                    <div class="col s12 m6" >
                        <a href="<?php echo site_url('novios/tarea') ?>"  class="sub-menu-item" >
                            <img style="width: 30px;float: left;margin-top: 5px;" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-tareas1.png" >
                            Mi agenda
                        </a>
                        <div class="divider"></div>
                        <a class="sub-menu-item" href="<?php echo site_url('novios/mesa') ?>" >
                            <img style="width: 30px;float: left;margin-top: 5px;" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-controldemesas1.png" >
                            Mi organizador de mesas
                        </a>
                        <div class="divider"></div>
                        <a class="sub-menu-item " href="<?php echo site_url('novios/proveedor') ?>"  >
                            <img style="width: 30px;float: left;margin-top: 5px;" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-proveedores1.png" >
                            Mis proveedores
                        </a>
                        <div class="divider"></div>
                        <a href="<?php echo site_url('novios/miweb') ?>"  class="sub-menu-item" >
                            <img style="width: 30px;float: left;margin-top: 5px;" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-portalweb1.png" >
                            Mi web de boda
                        </a>
                    </div>
                    <div class="col s12 m6">
                        <a href="<?php echo site_url('novios/invitados') ?>" class="sub-menu-item" >
                            <img style="width: 30px;float: left;margin-top: 5px;" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-listadeinvitados1.png" >
                            Mis invitados
                        </a>
                        <div class="divider"></div>
                        <a href="<?php echo site_url('novios/presupuesto') ?>" class="sub-menu-item" >
                            <img style="width: 30px;float: left;margin-top: 5px;" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-presupuesto1.png" >
                            Mi presupuesto
                        </a>
                        <div class="divider"></div>
                        <a href="<?php echo site_url("novios/misvestidos") ?>" class="sub-menu-item" >
                            <img style="width: 30px;float: left;margin-top: 5px;" src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-mivestido1.png" >
                            Mis vestidos
                        </a>
<!--                        <div class="divider"></div>-->
<!--                        <a class="sub-menu-item" href="--><?php //echo site_url("novios/comunidad") ?><!--" >-->
<!--                            <img style="width: 30px;float: left;margin-top: 5px;" src="--><?php //echo base_url() ?><!--dist/img/iconos/iconos-color/icono-comunidad1.png" >-->
<!--                            Mi comunidad-->
<!--                        </a>-->
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div id='dropdown-proveedores' class='dropdown-content sub-menu'>
        <div>
            <div class="row">
                <div class="col s12 m6" >
                    <a href="<?php echo site_url('proveedores/categoria/invitaciones-de-boda') ?>" class="sub-menu-item" >Invitaciones de bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/fotografos-de-bodas') ?>" class="sub-menu-item" >Fot&oacute;grafos de boda</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/musica-para-bodas') ?>" class="sub-menu-item" >M&uacute;sica para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/renta-de-sillas-y-mesas') ?>" class="sub-menu-item" >Renta de sillas y mesas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/animacion-bodas') ?>" class="sub-menu-item" >Animaci&oacute;n bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/mesas-de-regalos') ?>" class="sub-menu-item" >Mesa de Regalos</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/luna-de-miel') ?>"  class="sub-menu-item" >Luna de miel</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/pasteles-para-bodas') ?>" class="sub-menu-item" >Pasteles para boda</a>
                </div>
                <div class="col s12 m6">
                    <a href="<?php echo site_url('proveedores/categoria/recuerdos-para-boda') ?>" class="sub-menu-item" >Recuerdos para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/video-para-bodas') ?>" class="sub-menu-item" >Videos para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/autos-para-bodas') ?>" class="sub-menu-item" >Autos para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/carpas-para-bodas') ?>" class="sub-menu-item" >Carpas para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/arreglos-florales-boda') ?>" class="sub-menu-item" >Arreglos florales</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/organizadores-de-bodas') ?>" class="sub-menu-item" >Organizadores para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/decoracion-para-bodas') ?>" class="sub-menu-item" >Decoracion para bodas</a>
                    <div class="divider"></div>
                    <a href="<?php echo site_url('proveedores/categoria/promocion') ?>" class="sub-menu-item promocion" > <i class="fa fa-tag" style="line-height: 40px"></i> Promociones</a>
                </div>
            </div>
        </div>
    </div>

    <div id='dropdown-vestidos' class='dropdown-content sub-menu' style="width: 650px">
        <?php
        $tipos_vestido = $this->tipo_vestido->getAll();
        ?>
        <div>
            <div class="row">
                <div class="col s12 m3" >
                    <?php foreach ($tipos_vestido as $key => $p) { ?>
                        <a onmouseover="$('.tipo-vestido').hide();$('.tipo-<?php echo $p->id_tipo_vestido ?>').show();" href="<?php echo site_url("tendencia/index/" . str_replace(" ", "-", $p->nombre)) ?>"  class="sub-menu-item" >
                            <?php echo $p->nombre ?>
                        </a>
                        <div class="divider"></div>
                    <?php } ?>
                </div>
                <?php foreach ($tipos_vestido as $key => $p) { ?>
                    <?php $p->vestidos = $this->vestido->getDestacadosMin($p->id_tipo_vestido, 3); ?>
                    <div class="col s12 m9 tipo-vestido tipo-<?php echo $p->id_tipo_vestido ?>" <?php echo $p->id_tipo_vestido == 1 ? "" : 'style="display:none"' ?>  >
                        <h6>Diseñadores Destacados</h6>
                        <div class="divider"></div>
                        <?php foreach ($p->vestidos as $key => $value) { ?>
                            <div class="col s4" >
                                <div class="card-panel truncate"  style="padding: 5px;" >
                                    <a style="width: 100%;padding: 0px;margin: 0px;display: block"  href="<?php echo site_url("tendencia/catalogo/" . str_replace(" ", "-", $p->nombre) . "?disenador=" . str_replace(" ", "-", $value->disenador)) ?>">
                                        <div class="responsive-img" style="background-image: url(<?php echo site_url("tendencia/imagen_vestido/$value->id_vestido") ?>) ">
                                        </div>
                                    </a>
                                    <?php echo $value->disenador ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col s12">
                            <a class="dorado-2-text " href="<?php echo site_url("tendencia/index/" . str_replace(" ", "-", $p->nombre)) ?>" style="margin-top: -15px;margin-bottom: 20px;">
                                Todos los diseños de <?php echo $p->nombre ?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
