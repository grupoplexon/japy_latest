
<?php $this->view("principal/foot") ?>
<script>
  $(document).ready(function() {
    if (typeof onReady != 'undefined') {
      onReady();
    }
    $('select').material_select();
    //$(".button-collapse").sideNav();
    $('.dropdown-button').dropdown();
    $('.modal-trigger').modal();
    $('.collapsible').collapsible({
      accordion: false,
    });

      $('.carousel-slider').carousel();
      $('.carousel-slider').slider({full_width: true});

  });

  function generoImage(edad, sexo) {
    switch (parseInt(sexo)) {
      case 1:
        //HOMBRE
        switch (parseInt(edad)) {
          case 1:
            //ADULTO
            return 'invitados invitados-Hombre x2';
            break;
          case 2:
            //NINO
            return 'invitados invitados-Nino x2';
            break;
          case 3:
            //BEBE
            return 'invitados invitados-Bebe-nino x2';
            break;
        }
        break;
      case 2:
        //MUJER
        switch (parseInt(edad)) {
          case 1:
            //ADULTO
            return 'invitados invitados-Mujer x2';
            break;
          case 2:
            //NINO
            return 'invitados invitados-Nina x2';
            break;
          case 3:
            //BEBE
            return 'invitados invitados-Bebe-nina x2';
            break;
        }
        break;
    }
  }
</script>
</body>
</html>

