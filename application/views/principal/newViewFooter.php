<?php $home_section = getFooter(); ?>
<footer class="">
    <div class="body-container">
        <div class="row">
            <div class="col s12 m6 pull-left">
                <p>© Derechos Reservados 2018.</p>
            </div>
            <div class="col s12 m6 pull-right">
                <img src="<?php echo base_url() ?>/dist/img/icon-app-store.png" alt="" style="float: right"/>
                <img src="<?php echo base_url() ?>/dist/img/icon-google-play.png" alt="" style="float: right; margin-right: 20px"/>
            </div>
        </div>
    </div>
</footer>

