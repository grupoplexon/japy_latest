<!DOCTYPE html>
<html>
<head>
    <?php $this->view("principal/head") ?>
    <style>
        #solicitudes.collection .collection-item.avatar.remove {
            opacity: 0;
            height: 0px;
            display: block;
            overflow: hidden;
            background: #8C8D8F;
            min-height: 0px;
            padding: 0px;
        }

        #solicitudes.collection .collection-item.avatar {
            transition: 0.5s;
        }
    </style>
</head>
<body>
<?php $this->view("principal/menu") ?>
<?php $this->view("principal/novia/menu") ?>
<div class="body-container">
    <div class="row">
        <div class="col s1 m4 l3">
            <?php $this->view("principal/novia/buzon/menu") ?>
        </div>
        <div class=" col s11 m8 l9">
            <h5>Solicitudes enviadas</h5>
            <div class="divider"></div>
            <div class="row" style="font-size: 11px;padding-top: 10px">
                <div class="col  " style="    margin-left: 6px;margin-top: 8px;">
                    <input type="checkbox" id="test5" class="checkAll" data-checks=".check"/>
                    <label for="test5"> Todos</label>
                </div>
                <div class="col">
                    <button id="eliminar" class="btn white black-text waves-effect "><i class="fa fa-trash left"></i>
                        Eliminar
                    </button>
                </div>
            </div>
            <ul id="solicitudes" class="collection z-depth-1 ">
                <?php if (($solicitudes)) { ?>
                    <?php foreach ($solicitudes as $key => $s) { ?>
                        <li id="<?php echo $s->id_correo ?>" class=" solicitud collection-item avatar"
                            style="padding-left: 87px !important;">
                            <div class="circle" style="border-radius: 0px; height: 100%;width: 25px">
                                <input type="checkbox" data-solicitud="<?php echo $s->id_correo ?>"
                                       id="check<?php echo $s->id_correo ?>" class="check" name="solicitud[]"
                                       value="<?php echo $s->id_correo ?>"/>
                                <label for="check<?php echo $s->id_correo ?>"
                                       style="  display: block;float: left;"></label>

                            </div>
                            <a href="<?php echo base_url() ?>index.php/novios/buzon/ver/<?php echo $s->to->id_usuario ?>/<?php echo $s->id_correo ?>"
                               class="title dorado-2-text">
                                <?php echo $s->to->nombre ?>
                            </a>
                            <p style="width: calc( 100% - 115px);    margin-bottom: 10px;" class="grey-text darken-3">
                                <?php echo substr($s->mensaje, 0, 250) ?>...
                            </p>
                            <div class="secondary-content black-text " style="font-size: 12px">
                                <i class="fa fa-calendar-o  dorado-2-text"
                                   style="font-size: 12px"></i> <?php echo str_replace("-", "/",
                                        strtoupper(relativeTimeFormat($s->fecha_creacion, true))) ?> <br>
                            </div>
                        </li>
                    <?php } ?>

                <?php } else { ?>
                    <div class=" valign-wrapper" style="height: 150px">
                        <div class="center valign" style="width: 100%">
                            <i class="fa fa-info fa-3x"></i><br>
                            No se han econtrado solicitudes en esta carpeta
                        </div>
                    </div>
                <?php } ?>
            </ul>

            <?php if ($solicitudes) { ?>
                <ul class="pagination">
                    <li class="waves-effect <?php echo $page == 1 ? "disabled" : "" ?>">
                        <a <?php echo $page == 1 ? "" : "href='".base_url()."index.php/novios/buzon?pagina=".($page - 1) ?> >
                            <i class="material-icons">chevron_left</i>
                        </a>
                    </li>
                    <?php $total_paginas = ($total / 15) + 1 ?>
                    <?php for ($i = 1; $i < $total_paginas; $i++) { ?>
                        <li class="waves-effect <?php echo $page == $i ? "active" : "" ?>">
                            <a href="<?php echo base_url() ?>index.php/novios/buzon?pagina=<?php echo $i ?>"><?php echo $i ?></a>
                        </li>
                    <?php } ?>
                    <li class="waves-effect <?php echo $page == $i - 1 ? "disabled" : "" ?>">
                        <a <?php echo $page == $i - 1 ? "" : "href='".base_url()."index.php/novios/buzon?pagina=".($page + 1) ?> >
                            <i class="material-icons">chevron_right</i>
                        </a>
                    </li>
                </ul>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->view("principal/foot") ?>
<script>
    var server = "<?php echo base_url() ?>";
    $(document).ready(function () {
        $(".checkAll").on("change", function () {
            var $o = $($(this).data("checks"));
            $o.prop("checked", $(this).prop("checked"))
        });
        $("#eliminar").on("click", function (evt) {
            var solicitudes = new Array();
            $(".check:checked").each(function () {
                solicitudes.push($(this).data("solicitud"));
            });
            if (solicitudes.length > 0) {
                $.ajax({
                    url: server + "index.php/novios/buzon/eliminar/",
                    method: "POST",
                    data: {
                        solicitudes: solicitudes
                    },
                    success: function (resp) {
                        $(".check:checked").each(function () {
                            $("#" + $(this).data("solicitud")).addClass("remove");
                            $(this).remove();
                        });
                        Materialize.toast('Solicitudes eliminadas', 4000);
                    },
                    error: function () {

                    }
                });
            } else {
            }
        });
    });

</script>
</body>
</html>




