<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        <?php $this->view("principal/newheadernovia") ?>
        <style>
            #solicitudes.collection .collection-item.avatar.remove {
                opacity: 0;
                height: 0px;
                display: block;
                overflow: hidden;
                background: #8C8D8F;
                min-height: 0px;
                padding: 0px;
            }

            #solicitudes.collection .collection-item.avatar {
                transition: 0.5s;
            }

            .space-left {
                padding-left: 0 !important;
            }

            @media screen and (max-width: 425px) {
                .pagination li a {
                    padding: 0 !important;
                }
            }


        </style>
    </head>
    <body>
        <?php $this->view("principal/novia/menu") ?>
        <div class="body-container">
            <div class="row">
                <div class="col s12 ">
                    <?php if ($mails->count()) : ?>
                        <h5 class="section-title">Mi buzón</h5>
                        <div class="divider"></div>

                        <!-- TOOLBAR -->
                        <div class="row" style="font-size: 11px;padding-top: 10px">
                            <div class="col" style="margin-left: 6px;margin-top: 8px;">
                                <input type="checkbox" id="test5" class="checkAll" data-checks=".check"/>
                                <label for="test5">Todos</label>
                            </div>
                            <div class="col">
                                <button id="eliminar" class="btn white black-text waves-effect "><i
                                            class="fa fa-trash left"></i>
                                    Eliminar
                                </button>
                            </div>
                        </div>

                        <!-- MAILS -->
                        <ul id="solicitudes" class="collection z-1depth-1 buzon">
                            <?php foreach ($mails as $key => $mail) { ?>
                                <?php
                                $mailHasBeenSeen = true;
                                if ($mail->latestReply) {
                                    $mailHasBeenSeen = $mail->latestReply->leida_usuario;
                                }
                                ?>
                                <li id="<?php echo $mail->id_correo ?>"
                                    class="solicitud collection-item avatar <?php echo $mailHasBeenSeen ? "" : "active"; ?>"
                                    style="padding-left: 50px !important;">
                                    <div class="circle" style="border-radius: 0px; height: 100%;width: 25px">
                                        <input type="checkbox" data-solicitud="<?php echo $mail->id_correo ?>" id="check<?php echo $mail->id_correo ?>" class="check" name="solicitud[]"
                                               value="<?php echo $mail->id_correo ?>"/>
                                        <label for="check<?php echo $mail->id_correo ?>" style="  display: block;float: left;"></label>
                                    </div>
                                    <a href="<?php echo base_url() ?>novios/buzon/ver/<?php echo $mail->to ?>/<?php echo $mail->id_correo ?>"
                                       class="title dorado-2-text  space-left col s12">
                                        <?php echo $mail->userTo ? "Conversacion con ".$mail->userTo->provider->nombre : "Has recibido un mensaje"; ?>
                                    </a>
                                    <p style="width: calc( 100% - 90px);    margin-bottom: 10px;overflow:hidden" class="black-text ">
                                        <?php echo(substr(strip_tags($mail->mensaje), 0, 250)) ?>...
                                    </p>
                                    <div class="secondary-content black-text " style="font-size: 12px">
                                        <i class="fa fa-calendar-o  dorado-2-text"
                                           style="font-size: 12px"></i> <?php echo timeFormat($mail->latestReply == null ? $mail->fecha_creacion : $mail->latestReply->fecha_creacion,
                                            "%Y-%m-%d") ?>
                                        <br>
                                        <i class="fa fa-users"
                                           style="font-size: 12px;visibility: hidden;"></i> <?php echo dateFormat($mail->latestReply == null ? $mail->fecha_creacion : $mail->latestReply->fecha_creacion,
                                            "%I:%M %p") ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>

                        <!-- PAGINATION -->
                        <?php if ($mails->lastPage() > 1) { ?>
                            <ul class="pagination">

                                <li class="waves-effect <?php echo $mails->currentPage() == 1 ? "disabled" : "" ?>">
                                    <a href="<?php echo base_url() ?>novios/buzon?pagina=<?php echo $mails->currentPage() - 1 ?>">
                                        <i class="material-icons">chevron_left</i>
                                    </a>
                                </li>

                                <?php for ($i = 1; $i <= $mails->lastPage(); $i++) : ?>
                                    <li class="waves-effect <?php echo $mails->currentPage() == $i ? "active" : "" ?>">
                                        <a href="<?php echo base_url() ?>novios/buzon?pagina=<?php echo $i ?>">
                                            <?php echo $i ?>
                                        </a>
                                    </li>
                                <?php endfor; ?>

                                <li class="waves-effect <?php echo $mails->currentPage() == $mails->lastPage() ? "disabled" : "" ?>">
                                    <a href="<?php echo base_url() ?>novios/buzon?pagina=<?php echo $mails->currentPage() + 1 ?>">
                                        <i class="material-icons">chevron_right</i>
                                    </a>
                                </li>

                            </ul>
                        <?php } ?>
                    <?php else : ?>
                        <h2>No tienes correos</h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <script>
            var server = "<?php echo base_url() ?>";

            $(document).ready(function() {

                $('.checkAll').on('change', function() {
                    var $o = $($(this).data('checks'));
                    $o.prop('checked', $(this).prop('checked'));
                });

                $('#eliminar').on('click', function(evt) {
                    var solicitudes = new Array();

                    $('.check:checked').each(function() {
                        solicitudes.push($(this).data('solicitud'));
                    });

                    if (solicitudes.length > 0) {
                        $.ajax({
                            url: server + 'novios/buzon/eliminar/',
                            method: 'POST',
                            data: {
                                solicitudes: solicitudes,
                            },
                            success: function(resp) {
                                $('.check:checked').each(function() {
                                    $('#' + $(this).data('solicitud')).addClass('remove');
                                    $('#' + $(this).data('solicitud')).remove();
                                });

                                Materialize.toast('Solicitud eliminada', 4000);

                                if ($('#solicitudes > li').length == 0) {
                                    $('#solicitudes').closest('.col.s12').empty().append('<h2>No tienes correos</h2>');
                                }
                            },
                            error: function() {

                            },
                        });
                    }
                });
            });

        </script>
        <?php $this->load->view('principal/footer'); ?>
    </body>
</html>




