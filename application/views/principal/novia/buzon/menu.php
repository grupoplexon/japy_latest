<?php
$controller = $_SERVER['REQUEST_URI'];
$controller = strtolower($controller);
?>
<style>
    .collection .collection-item.active {
        background-color: #f5e6df;
    }
</style>
<div class="collection">
    <a class="collection-item black-text <?php echo ($menu == "buzon") ? 'active' : ''; ?>" href="<?php echo base_url() ?>index.php/novios/buzon/">
        <i class="fa fa-envelope"></i>
        <text class="hide-on-small-and-down">Bandeja de entrada</text>
    </a>
    <a class="collection-item black-text  <?php echo ($menu == "enviados") ? 'active' : ''; ?>" href="<?php echo base_url() ?>index.php/novios/buzon/enviados" >
        <i class="fa fa-reply"></i>
        <text class="hide-on-small-and-down">Mensajes enviados</text>
    </a>
</div>
<br>
<?php if ($amigos) { ?>
    <ul class="collection">
        <?php foreach ($amigos as $key => $value) { ?>
            <li class="collection-item avatar" style="    min-height: 65px;">
                <img src="<?php echo site_url("perfil/foto/$value->id_usuario_confirmacion") ?>" alt="" class="circle">
                <span class="title">
                    <a class="dorado-2-text" href="<?php echo site_url("/novios/comunidad/perfil/usuario/$value->id_usuario_confirmacion") ?>">
                        <?php echo $value->usuario->apellido . ", " . $value->usuario->nombre ?>
                    </a>
                </span>
                <a href="<?php echo site_url("/novios/buzon/nuevo?usuario=$value->id_usuario_confirmacion") ?>" class="secondary-content dorado-2-text"><i class="material-icons">email</i></a>
            </li>
        <?php } ?>
    </ul>
<?php } ?>