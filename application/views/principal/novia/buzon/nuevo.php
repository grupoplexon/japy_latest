<!DOCTYPE html>
<html>
    <head>
        <?php $this->view("principal/head") ?>
        <style>
            #solicitudes.collection .collection-item.avatar.remove {
                opacity: 0;
                height: 0px;
                display: block;
                overflow: hidden;
                background: #8C8D8F;
                min-height: 0px;
                padding: 0px;
            }
            #solicitudes.collection .collection-item.avatar{
                transition: 0.5s;
            }
        </style>
    </head>
    <body>
        <?php $this->view("principal/menu") ?>
        <?php $this->view("principal/novia/menu") ?>
        <div class="body-container">
            <div class="row">
                <div class="col s1 m4 l3">
                    <?php $this->view("principal/novia/buzon/menu") ?>
                </div>
                <div class=" col s11 m8 l9">
                    <h5>Nuevo mensaje</h5>
                    <div class="divider"></div>
                    <?php $this->view("principal/novia/buzon/mensajes") ?>
                    <form method="POST" class="form-validate" action="<?php echo site_url("novios/buzon/nuevo") ?>">
                        <div class="row" style="font-size: 11px;padding-top: 10px">
                            <label>
                                Para:
                            </label>
                            <div>
                                <?php if ($to) { ?>
                                    <div class="chip">
                                        <img src="<?php echo site_url("perfil/foto/$to->id_usuario") ?>" alt="<?php echo $to->nombre ?>">
                                        <?php echo $to->apellido ?>, <?php echo $to->nombre ?>
                                    </div>
                                    <input type="hidden" value="<?php echo $to->id_usuario ?>" name="to">
                                <?php } else { ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row" style="font-size: 11px;padding-top: 10px">
                            <label>Asunto:</label>
                            <input id="asunto" class="form-control validate[required]" name="asunto">
                        </div>
                        <div class="row" style="font-size: 11px;padding-top: 10px">
                            <textarea id="mensaje"  name="mensaje"></textarea>
                        </div>
                        <div class="row">
                            <button type="submit" class="btn waves-effect waves-light right dorado-2">
                                Enviar <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php $this->view("principal/foot") ?>
        <script src="<?php echo base_url() ?>dist/js/validation/validate.engine.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/js/validation/validate.engine.plugin.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/js/proveedor.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
        <!--<script src="<?php echo base_url() ?>dist/tinymce/jquery.tinymce.min.js" type="text/javascript"></script>-->
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function () {
                init_tinymce_mini("#mensaje");
            });
        </script>
    </body>
</html>




