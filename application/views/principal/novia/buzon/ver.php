<html>
    <head>
        <?php $this->view("principal/head");
        $this->view("general/newheader"); ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/proveedor.css">
    </head>
    <body>
        <?php $this->view("general/menu") ?>
        <?php $this->view("principal/novia/menu") ?>
        <div class="body-container">
            <div class="row">
                <!-- PROVIDER SIDE LINK -->
                <?php if ($provider) { ?>
                    <div class="col s10 offset-s1 m4 l3 right provider-side">
                        <div class="card">
                            <div class="card-image">
                                <?php if ($provider->imagePrincipalMiniature->count()) : ?>
                                    <img class="thumb responsive-img" src="<?php echo(base_url()."uploads/images/".$provider->imagePrincipalMiniature->first()->nombre) ?>">
                                <?php else : ?>
                                    <img class="thumb responsive-img" src="<?php echo base_url() ?>/dist/img/slider1.png">
                                <?php endif ?>
                            </div>
                            <div class="card-content" style="padding-top: 35px">
                                <h5><?php echo $provider->nombre ?></h5>
                                <a href="<?php echo base_url() ?>boda-<?php echo $provider->slug ?>"
                                   class="btn dorado-2" style="font-size: 12px;width: 100%">
                                    <i class="fa fa-chevron-right right"></i>Ver escaparate</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- REPLIES  -->
                <div class="col s12 m8 l9 left">
                    <h5><?php echo $titulo ?></h5>
                    <div class="divider"></div>
                    <br>
                    <!-- ACTIONS -->
                    <div class="row" style="display: flex;justify-content: space-between">
                        <div class="col s12 m3">
                            <a href="<?php echo base_url() ?>novios/buzon" class="btn white dorado-2-text">
                                <i class="fa fa-chevron-left hide-on-small-only"></i>
                                Mi buz&oacute;n
                            </a>
                        </div>
                        <div class="col s12 m3 offset-m6" style="display:flex;justify-content: flex-end;">
                            <button class="btn dorado-2 goto waves-effect" data-target="#responder"> Responder</button>
                        </div>
                    </div>

                    <!-- REPLIES -->
                    <div class="row imprimible">
                        <div class="card-panel">
                            <div class="row">
                                <?php
                                if ( ! $novio) {
                                    $novio             = new stdClass();
                                    $novio->id_usuario = 0;
                                    $novio->nombre     = "japybodas.com";
                                    $novio->correo     = "";
                                    $novio->apellido   = "";
                                }
                                ?>
                                <div class="col s12 m2 center-align">
                                    <?php if ($novio->id_usuario) { ?>
                                        <img class="responsive-img"
                                             src="<?php echo base_url() ?>perfil/foto/<?php echo $novio->id_usuario ?>">
                                    <?php } else { ?>
                                        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/favicon.png">
                                    <?php } ?>
                                </div>
                                <?php if ($novio->id_usuario) { ?>
                                    <div class="col s12 m5 ">
                                        <h6 class="dorado-2-text">
                                            <b>De: <?php echo $mail->user->nombre ?></b>
                                        </h6>
                                        <h6 class="dorado-2-text">
                                            <b>Para: <?php echo $mail->userTo->nombre ?></b>
                                        </h6>
                                        <label><i class="fa fa-phone"></i>
                                            Tel&eacute;fono</label> <?php echo $cliente->telefono ?> <br>
                                        <label><i class="fa fa-envelope"></i> Email</label> <?php echo $novio->correo ?><br>
                                    </div>
                                    <div class="col s12 m5">
                                        <label><i class="fa fa-calendar-o"></i> Fecha del
                                            evento</label> <?php echo dateFormat($boda->fecha_boda) ?><br>
                                        <label><i class="fa fa-users"></i> Invitados</label> <?php echo $boda->no_invitado ?>
                                        <br>
                                    </div>
                                <?php } ?>

                            </div>
                            <div class="row text-justify">
                                <label>Mensaje enviado: <?php echo relativeTimeFormat($mail->fecha_creacion) ?></label><br>
                                <?php echo $mail->mensaje ?>
                            </div>
                        </div>

                        <?php if ($mail->replies->count()) { ?>
                            <section id="conversacion">
                                <h5 style="margin-top: 50px;">Mensajes</h5>
                                <div class="divider" style="margin-bottom:  20px;"></div>
                                <?php foreach ($mail->replies as $key => $reply) { ?>
                                    <div class="card-panel">
                                        <?php if ($reply->from == $this->session->id_usuario) : ?>
                                            <i class="fa fa-mail-forward blue-text"></i> <b
                                                    class="dorado-2-text bold"><?php echo $this->session->nombre ?></b>,
                                        <?php else : ?>
                                            <i class="fa fa-mail-reply green-text"></i>
                                            <b class="dorado-2-text bold"><?php echo $provider->nombre ?></b>,
                                        <?php endif ?>

                                        <label>el <?php echo dateFormat($reply->fecha_creacion) ?> a las <?php echo dateFormat($reply->fecha_creacion, "%H:%M") ?></label>

                                        <div class="right">
                                            <a href="#" class="delete" data-id="<?php echo $reply->id_correo ?>">
                                                <i class="fa fa-trash"></i>
                                                <span>Eliminar</span>
                                            </a>
                                        </div>

                                        <div class="divider"></div>

                                        <div>
                                            <h6 style="font-weight: bold"><?php echo $reply->asunto ?></h6>
                                            <?php echo $reply->mensaje ?>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <?php foreach ($reply->attachments as $attachment) { ?>
                                                <a href="<?php echo base_url() ?>proveedores/archivo/<?php echo $attachment->id_adjunto ?>"
                                                   class="btn-flat dorado-2-text right" download> <i
                                                            class="fa fa-download"></i> <?php echo $attachment->nombre ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </section>
                        <?php } ?>
                    </div>

                    <!-- RESPOND -->
                    <div id="responder" class="responder">
                        <form method="POST"
                              action="<?php echo base_url() ?>novios/buzon/ver/<?php echo $provider->user->id_usuario ?>/<?php echo $mail->id_correo ?>">
                            <div class="row">
                                <div class="col s 12 m8">
                                    <h5 style="margin-top: 30px;">Responder</h5>
                                </div>
                                <div class=" col s12 m4 right">
                                </div>
                                <div class="col s12">
                                    <div class="divider" style="margin-bottom:  20px;"></div>
                                </div>
                            </div>
                            <div class="card-panel">
                                <div class="row">
                                    <div class="col s12">
                                        <label>
                                            Asunto:
                                        </label>
                                        <input id="asunto" class="form-control" name="asunto" value="" required>
                                    </div>
                                    <div class="col s12">
                                        <label>
                                            Mensaje:
                                        </label>
                                        <textarea id="mensaje" name="mensaje"></textarea>
                                    </div>
                                    <div class="col s12">
                                        <div id="nuevo">
                                            <div class="card-panel z-depth-0 valign-wrapper upload" id="upload" enctype="multipart/form-data" style="display:flex;justify-content:space-around;overflow-x:auto;">
                                                <text style="width: 100%;text-align: center;color: gray;">
                                                    <i class="fa fa-upload fa-2x valign "></i><br>
                                                    Suelte el archivo o haga clic aqu&iacute; para cargar.
                                                </text>
                                            </div>
                                            <input name="files[]" type="hidden" class="archivo" value="">
                                            <input name="files_names[]" type="hidden" class="archivo-name" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <p>
                                        <button type="submit" class="btn dorado-2 right">Enviar</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("proveedor/footer") ?>
        <script>
            $(document).ready(function() {
                init_tinymce_mini('#mensaje');
                setupDropzone();

                const server = "<?php echo base_url() ?>";

                $('.goto').on('click', function() {
                    var $self = $(this);
                    $('html,body').animate({
                        scrollTop: $($self.data('target')).offset().top,
                    }, 1000);
                });

                $('.card-panel').find('ul').addClass('browser-default').css('padding-left', '40px');

                $('.upload').each(function(i, elem) {
                    var dropzone = new Dropzone(elem, {
                        autoProcessQueue: false,
                        url: server + 'proveedor/escaparate/fotos',
                        paramName: 'files', // The name that will be used to transfer the file
                        maxFilesize: 3, // MB
                        uploadMultiple: true,
                        maxFiles: 5,
                        createImageThumbnails: true,
                        acceptedFiles: 'image/*,application/pdf',
                        init: function() {
                            this.on('addedfile', function(file) {
                                let removeButton = Dropzone.createElement('<button class=\'btn waves-effect\' style=\'margin-top:5px;padding:0 1rem;\'>Eliminar</button>');
                                let _this = this;
                                let thumbnail = $(file.previewElement).find('.dz-image img');
                                let fileReader = new FileReader();
                                $inputName = $('.archivo-name').first();
                                $input = $('.archivo').first();
                                file.id = guid();

                                if (file instanceof File) {
                                    fileReader.onload = function(fileLoadedEvent) {
                                        if (file.status != 'error') {
                                            if ($input.val()) {
                                                $input.clone().attr('file-id', file.id).appendTo('#nuevo').val(fileLoadedEvent.target.result);
                                            }
                                            else {
                                                $input.val(fileLoadedEvent.target.result);
                                                $input.attr('file-id', file.id);
                                            }
                                        }
                                    };

                                    fileReader.readAsDataURL(file);
                                }

                                if ($inputName.val()) {
                                    $inputName.clone().attr('file-id', file.id).appendTo('#nuevo').val(file.name);
                                }
                                else {
                                    $inputName.val(file.name);
                                    $inputName.attr('file-id', file.id);
                                }

                                if (file.type == 'application/pdf') {
                                    thumbnail.attr('src', server + 'dist/img/pdf-icon.png');
                                }

                                $('.dz-filename').addClass('truncate');

                                removeButton.addEventListener('click', function(e) {
                                    e.preventDefault();
                                    e.stopPropagation();

                                    _this.removeFile(file);
                                });

                                file.previewElement.appendChild(removeButton);
                            });
                            this.on('maxfilesexceeded', function(file) {
                                var _this = this;
                                _this.removeFile(file);

                                setTimeout(function() {
                                    $('.upload').parent().find('label.red-text').remove();
                                }, 3000);

                            });
                            this.on('removedfile', function(file) {
                                const $input = $('.archivo');

                                if ($input.length > 1) {
                                    $('input[file-id=' + file.id + ']').removeAttr('file-id').remove();
                                }
                                else {
                                    $('input[file-id=' + file.id + ']').removeAttr('file-id').val('');
                                }

                                let t = $(elem).find('div.dz-preview').length;

                                if (t == 0) {
                                    $(elem).find('text').show();
                                }
                                else {
                                    $(elem).find('text').hide();
                                }
                            });
                        },
                        accept: function(file, done) {
                            $(elem).find('text').hide();
                            done();
                        },
                        error: function(data, errorMessage) {
                            $('.upload').parent().append(`<label class='red-text'>${errorMessage}</label>`);

                            let _this = this;
                            _this.removeFile(data);
                            setTimeout(function() {
                                $('.upload').parent().find('label.red-text').remove();
                            }, 3000);
                        },
                    });
                });

                $('.upload text').on('click', function(e) {
                    $(e.currentTarget.parentNode).trigger('click');
                });

                $('.eliminar-foto').on('click', function(elm) {
                    var $btn = $(elm.currentTarget);
                    var $prom = $('#' + $btn.data('promocion'));
                    $prom.find('.upload div.dz-preview').remove();
                    $prom.find('text').show();
                    $prom.find('.archivo').val('');
                    $('#nuevo .nombre').html('');
                });

                $('.delete').on('click', function(e) {
                    e.preventDefault();
                    const solicitud = $(this).data('id');
                    const $self = $(this);

                    $.ajax({
                        url: server + 'novios/buzon/eliminar/',
                        method: 'POST',
                        data: {
                            solicitudes: solicitud,
                        },
                        success: function(resp) {
                            $self.closest('.card-panel').remove();
                            Materialize.toast('Mensaje eliminado', 4000);
                        },
                        error: function() {

                        },
                    });
                });
            });

            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                }

                return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
            }

            function setupDropzone() {
                Dropzone.prototype.defaultOptions.dictDefaultMessage = 'Tira tus archivos aqui';
                Dropzone.prototype.defaultOptions.dictFallbackMessage = 'Lo sentimos parece ser que tu navegador no es compatible';
                Dropzone.prototype.defaultOptions.dictFileTooBig = 'El archivo es muy grande ({{filesize}}MiB). Tamaño maximo: {{maxFilesize}}MiB.';
                Dropzone.prototype.defaultOptions.dictInvalidFileType = 'No se pueden subir archivos de este tipo';
                Dropzone.prototype.defaultOptions.dictResponseError = 'El servidor respondio con este codigo {{statusCode}}.';
                Dropzone.prototype.defaultOptions.dictCancelUpload = 'Cancelar subida';
                Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = 'Estas seguro que deseas cancelar?';
                Dropzone.prototype.defaultOptions.dictRemoveFile = 'Remover archivo';
                Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = 'Ya no puedes subir mas archivos.';
            }
        </script>
    </body>
</html>