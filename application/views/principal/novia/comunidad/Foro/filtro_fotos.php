<?php $this->view('principal/header');?>
<?php $this->view('principal/novia/menu');?>
<link href="<?php echo base_url()?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<body>
    <!--BARRA DE TITULO CON UN BUSCADOR DE DEBATES-->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
            <p>
            <div class="col s12 m2">
                <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            </div>
            <form id="buscador" method="get" action="<?php echo base_url()."novios/comunidad/forum/buscar"?>" autocomplete="off">
                <div class="input-field col s7 m5" style="margin:0">
                    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
                    <input type="hidden" name="page" value="1"/>
                    <div id="ventana_buscador">
                    </div>
                </div>
                <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
                    <button type="submit" class="btn waves-effect waves-light dorado-2"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                </div>
            </form>
        </p>
        </div>
    </div>
    
    <!--TARJETA CON UN BOTON DICIENDO QUE PUEDES CREAR UN NUEVO DEBATE EN LA COMUNIDAD-->
    <div class="body-container">
        <div class="row">
            
            <!--COMPONENTES LATERALES-->
            <?php 
                if(empty($usuarios_boda)){
                    $usuarios_boda = "";
                }
                if(empty($visitas_perfil)){
                    $visitas_perfil = "";
                }
                if(empty($grupos_miembro)){
                    $grupos_miembro = "";
                }
                if(empty($grupos)){
                    $grupos = "";
                }
                $data = array(
                    'usuarios_boda' => $usuarios_boda,
                    'visitas_perfil' => $visitas_perfil,
                    'grupos_miembro' => $grupos_miembro,
                    'grupos' => $grupos
                ); 
                $this->view('principal/novia/comunidad/lateral.php',$data);
            ?>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m6">
                <?php if(!empty($fotos)){ 
                        if(!empty($ordenamiento) && $ordenamiento == "recientes"){
                ?>
                    <h5>&Uacute;ltimas Fotos Publicadas</h5>
                <?php }else if(!empty($ordenamiento) && $ordenamiento == "visitadas"){ ?>
                    <h5>Fotos Publicadas M&aacute;s Visitadas</h5>
                <?php }else if(!empty($ordenamiento) && $ordenamiento == "comentadas"){ ?>
                    <h5>Fotos Publicadas M&a&aacute;s Comentadas</h5>
                <?php } ?>
                    </div>
                    <div class="col s12 m6 input-field">
                        <select id="selector">
                            <option value="0">Ordenar Por</option>
                            <option value="1" <?php if(!empty($ordenamiento) && $ordenamiento == "recientes") echo "selected" ?>>Fecha</option>
                            <option value="2" <?php if(!empty($ordenamiento) && $ordenamiento == "visitadas") echo "selected" ?>>Visitas</option>
                            <option value="3" <?php if(!empty($ordenamiento) && $ordenamiento == "comentadas") echo "selected" ?>>Comentarios</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <?php
                        foreach ($fotos as $foto){
                            echo '
                            <div class="col s6 m3 foto_publicada">
                                <div class="card small">
                                    <div class="card-image">
                                        <a href="'.base_url().'novios/comunidad/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'"> <img class="foto_zoom" src="'.base_url().'novios/comunidad/picture/foto/'.$foto->id_foto.'"></a>
                                    </div>
                                    <div class="card-content" style="text-align: center">
                                        <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                            <img alt="" src="'.$foto->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                                        </div>
                                        <p class="clickable truncate"><a class="titulo_foto" href="'.base_url().'novios/comunidad/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'">'.$foto->titulo.'</a></p><br/>
                                        <p class="link_usuario">'.$foto->usuario.'</p>
                                    </div>
                                </div>
                            </div>';
                        }
                    ?>
                </div>
                <?php } ?>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
                        <div class="col s12 m9" style="float: right">
                            <ul class="pagination">
                                <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a <?php if($pagina > 1) echo 'href="'.base_url()."novios/comunidad/forum/fotos/$ordenamiento/".($pagina - 1).'"' ?>><i class="material-icons">chevron_left</i></a></li>
                                <?php 
                                $j = 1;
                                $contador = 0;
                                if($total_paginas > 10 && $pagina > 0 && $pagina > 5) { 
                                    $pagina2 = $pagina + 5;
                                    if($pagina2 <= $total_paginas){
                                        $j = $pagina - 4;
                                    }else{
                                        $j = $pagina - 4;
                                        $j = $j - ($pagina2 - $total_paginas);
                                    }
                                }else{
                                    $j = 1;
                                }
                                for($i=$j; $i <= $total_paginas && $contador < 10; $i++){
                                ?>
                                <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo base_url()."novios/comunidad/forum/fotos/$ordenamiento/$i" ?>"><?php echo $i ?></a></li>
                                <?php $contador++; } ?>
                                <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a <?php if($pagina < $total_paginas) echo 'href="'.base_url()."novios/comunidad/forum/fotos/$ordenamiento/".($pagina + 1).'"'?>><i class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- -----------------  TEMPLATE VENTANA BUSCADOR -------------------------- -->
    <ul id="lista_debates">
        <li id="item_debate">
            <a class="enlace_debate" href="">
                <div class="row">
                    <div class="col s4 m3">
                        <div class="card">
                            <div class="card-image">
                                <img class="imagen_debate">
                            </div>
                        </div>
                    </div>
                    <div class="col s9 m9">
                        <p class="titulo_debate"></p>
                        <p class="fecha_debate"></p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <!-- ------------------  FIN TAMPLETE VENTANA BUSCADOR --------------------- -->
    <script>
    $(document).ready(function (){
        $(".fotos_recientes").click(function (){
            window.location.href = "<?php echo base_url()?>novios/comunidad/Forum/fotos/recientes";
        });
        $(".fotos_vistas").click(function (){
            window.location.href = "<?php echo base_url()?>novios/comunidad/Forum/fotos/visitas";
        });
    });
    
    $(document).ready(function (){
        $("#selector").change(function (){
            switch($(this).val()){
                case "1":
                    window.location.href = "<?php echo base_url()?>novios/comunidad/forum/fotos/recientes";
                    break;
                case "2":
                    window.location.href = "<?php echo base_url()?>novios/comunidad/forum/fotos/visitadas";
                    break;
                case "3":
                    window.location.href = "<?php echo base_url()?>novios/comunidad/forum/fotos/comentadas";
                    break;
            }
        });
    });
    $(document).ready(function (){
        $('.desplegar_grupos').click(function (){
            var mostrar = $(this).text();
            var display = $('.oculto').css('display');
            if(display == "none"){
                $('.oculto').css('display','block');
                $(this).text("Ocultar Grupos");
            }else{
                $('.oculto').css('display','none');
                $(this).text("M&aacute;s Grupos...");
            }
        });
    });
    
    $(document).ready(function (){
        var activo = 0;
        var height = 0;
        var bandera = true;
        
        $("input[name=buscar]").keydown(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(key == 40){
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo++;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo > 1){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height > 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                }else{
                    if(bandera){
                        activo++;
                        bandera = false;
                    }
                    activo--;
                }
            }else if(key == 38){
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo--;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo >= 0){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height >= 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                }else{
                    if(!bandera){
                        activo--;
                        bandera = true;
                    }
                    activo++;
                }
            }else if(key == 13){
                $("#buscador").on('keypress',function(e){
                    e.preventDefault();
                    return false;
                });
                window.location.href = $('.item-'+activo+' a').attr("href");
            }else if(key != 39 && key != 37){
                activo = 0;
                grupos();
            }
        });
        
        $("input[name=buscar]").keyup(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(titulo != "" && key != 38 && key != 40 && key != 39 && key != 37){
                activo = 0;
                debates(titulo);
            }else if(key == 13){
                window.location.href = $('.item-'+activo+' a').attr("href");
            }
        });
        
        $(document).on('mouseenter','#ventana_buscador > ul > li',function(){
            $('.item-'+activo).removeClass("hover");
            var clase = $(this).attr("class");
            var token = clase.split("-");
            activo = token[1];
            $(this).addClass("hover");
        });
       
        $("body").on('click',function(){
            $("#ventana_buscador").hide();
            $('.item-'+activo).removeClass("hover");
        });
        
        $("input[name=buscar]").on('click', function (e){
            e.stopPropagation();
            $('.item-'+activo).removeClass("hover");
            activo = 0;
            var titulo = $("input[name=buscar]").val();
            if(titulo == "" && $("#ventana_buscador ul").html() == undefined){
                grupos();
            }
            $("#ventana_buscador").show();
        });
        
        $("#ventana_buscador").on('mouseleave',function(){
            setTimeout(function(){
                $('.item-'+activo).removeClass("hover");
            },1000);
        });
        
        function grupos(){
            $.ajax({
                url: '<?php echo base_url()."novios/comunidad/home/getGrupos"?>',
                success: function(res) {
                    var val = Array();
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_grupo);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].imagen);
                                $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                $(li).find(".fecha_debate").text(res.data[aux].debates+" Debates");
                                $(ul).append(li);
                                if(i == 16){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").data("grupos","true");
                        $("#ventana_buscador").show();
                    }
                }          
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
        
        function debates(titulo){
            $.ajax({
                url: '<?php echo base_url()."novios/comunidad/home/buscar" ?>',
                method: 'post',
                data:{
                    'titulo_debate': titulo
                },
                success: function(res){
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        var val = Array();
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_debate);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].foto_usuario);
                                $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                $(ul).append(li);
                                if(i > (res.data.length - 1)){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").show();
                    }
                }
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
    });
    </script>
</body>
<?php $this->view('principal/footer'); ?>