<?php $this->view('principal/header');?>
<?php $this->view('principal/novia/menu');?>
<link href="<?php echo base_url()?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<body>
    <!--BARRA DE TITULO CON UN BUSCADOR DE DEBATES-->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
            <p>
            <div class="col s12 m2">
                <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            </div>
            <form id="buscador" method="get" action="<?php echo base_url()."novios/comunidad/forum/buscar"?>" autocomplete="off">
                <div class="input-field col s7 m5" style="margin:0">
                    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
                    <input type="hidden" name="page" value="1"/>
                    <div id="ventana_buscador">
                    </div>
                </div>
                <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
                    <button type="submit" class="btn waves-effect waves-light dorado-2"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                </div>
            </form>
        </p>
        </div>
    </div>
    
    <!--TARJETA CON UN BOTON DICIENDO QUE PUEDES CREAR UN NUEVO DEBATE EN LA COMUNIDAD-->
    <div class="body-container">
        <div class="row">
            
            <!--COMPONENTES LATERALES-->
            <?php 
                if(empty($usuarios_boda)){
                    $usuarios_boda = "";
                }
                if(empty($visitas_perfil)){
                    $visitas_perfil = "";
                }
                if(empty($grupos_miembro)){
                    $grupos_miembro = "";
                }
                if(empty($grupos)){
                    $grupos = "";
                }
                $data = array(
                    'usuarios_boda' => $usuarios_boda,
                    'visitas_perfil' => $visitas_perfil,
                    'grupos_miembro' => $grupos_miembro,
                    'grupos' => $grupos
                ); 
                $this->view('principal/novia/comunidad/lateral.php',$data);
            ?>
            <!--<div class="col s12 m9 pull-left">
                <div class="row">
                    <?php if(!empty($grupos)){ ?>
                    <div class="row" style="border-bottom: 1px solid #999">
                    <h5>Grupos Por Estado</h5>
                    </div>
                    <div class="row">
                        <div class="card">
                            <div class="card-image">
                                <img style="height: 150px; width: 100%" src="<?php echo base_url()."dist/images/comunidad/foros/grupos-estado.jpg"?>">
                            </div>
                        </div>
                    </div>
                    <div class="col s12" style="margin: 0px; padding: 0px; padding-top: 0px">
                        <ul class="grey lighten-5">
                            <?php
                            $i = 0;
                            foreach ($grupos as $grupo){
                                if($i > 15){
                            ?>
                            <li>
                                <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."novios/comunidad/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                                    <div class="listaG col s12 m6 grey lighten-5 z-depth-1">
                                        <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen" ?>"> 
                                        <p class="nombre_grupo pull-s1 truncate"> <?php echo !empty($grupo->nombre)? $grupo->nombre : "" ?></p>
                                        <!--<p class="titulo_debate"> <?php // echo !empty($grupo->miembros)? "$grupo->miembros Miembros" : "" ?> </p>-->
                                    </div>
                                </a>
                            </li>
                            <?php
                                }
                                $i++; 
                            } ?>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
            </div>-->
        </div>
    </div>
    <!-- -----------------  TEMPLATE VENTANA BUSCADOR -------------------------- -->
    <ul id="lista_debates">
        <li id="item_debate">
            <a class="enlace_debate" href="">
                <div class="row">
                    <div class="col s4 m3">
                        <div class="card">
                            <div class="card-image">
                                <img class="imagen_debate">
                            </div>
                        </div>
                    </div>
                    <div class="col s9 m9">
                        <p class="titulo_debate"></p>
                        <p class="fecha_debate"></p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <!-- ------------------  FIN TAMPLETE VENTANA BUSCADOR --------------------- -->
    <script>
        $(document).ready(function (){
            $('.desplegar_grupos').click(function (){
                var mostrar = $(this).text();
                var display = $('.oculto').css('display');
                if(display == "none"){
                    $('.oculto').css('display','block');
                    $(this).text("Ocultar Grupos");
                }else{
                    $('.oculto').css('display','none');
                    $(this).text("M&aacute;s Grupos...");
                }
            });
        });
        
    $(document).ready(function (){
        var activo = 0;
        var height = 0;
        var bandera = true;
        
        $("input[name=buscar]").keydown(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(key == 40){
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo++;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo > 1){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height > 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                }else{
                    if(bandera){
                        activo++;
                        bandera = false;
                    }
                    activo--;
                }
            }else if(key == 38){
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo--;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo >= 0){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height >= 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                }else{
                    if(!bandera){
                        activo--;
                        bandera = true;
                    }
                    activo++;
                }
            }else if(key == 13){
                $("#buscador").on('keypress',function(e){
                    e.preventDefault();
                    return false;
                });
                window.location.href = $('.item-'+activo+' a').attr("href");
            }else if(key != 39 && key != 37){
                activo = 0;
                grupos();
            }
        });
        
        $("input[name=buscar]").keyup(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(titulo != "" && key != 38 && key != 40 && key != 39 && key != 37){
                activo = 0;
                debates(titulo);
            }else if(key == 13){
                window.location.href = $('.item-'+activo+' a').attr("href");
            }
        });
        
        $(document).on('mouseenter','#ventana_buscador > ul > li',function(){
            $('.item-'+activo).removeClass("hover");
            var clase = $(this).attr("class");
            var token = clase.split("-");
            activo = token[1];
            $(this).addClass("hover");
        });
       
        $("body").on('click',function(){
            $("#ventana_buscador").hide();
            $('.item-'+activo).removeClass("hover");
        });
        
        $("input[name=buscar]").on('click', function (e){
            e.stopPropagation();
            $('.item-'+activo).removeClass("hover");
            activo = 0;
            var titulo = $("input[name=buscar]").val();
            if(titulo == "" && $("#ventana_buscador ul").html() == undefined){
                grupos();
            }
            $("#ventana_buscador").show();
        });
        
        $("#ventana_buscador").on('mouseleave',function(){
            setTimeout(function(){
                $('.item-'+activo).removeClass("hover");
            },1000);
        });
        
        function grupos(){
            $.ajax({
                url: '<?php echo base_url()."novios/comunidad/home/getGrupos"?>',
                success: function(res) {
                    var val = Array();
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_grupo);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].imagen);
                                $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                $(li).find(".fecha_debate").text(res.data[aux].debates+" Debates");
                                $(ul).append(li);
                                if(i == 16){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").data("grupos","true");
                        $("#ventana_buscador").show();
                    }
                }          
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
        
        function debates(titulo){
            $.ajax({
                url: '<?php echo base_url()."novios/comunidad/home/buscar" ?>',
                method: 'post',
                data:{
                    'titulo_debate': titulo
                },
                success: function(res){
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        var val = Array();
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_debate);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].foto_usuario);
                                $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                $(ul).append(li);
                                if(i > (res.data.length - 1)){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").show();
                    }
                }
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
    });
    </script>
</body>
<?php $this->view('principal/footer'); ?>