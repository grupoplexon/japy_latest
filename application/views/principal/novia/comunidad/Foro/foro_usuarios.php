<?php $this->view('principal/header'); ?>
<?php $this->view('principal/novia/menu'); ?>
    <link href="<?php echo base_url() ?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <body>
    <!--BARRA DE TITULO CON UN BUSCADOR DE DEBATES-->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
            <p>
                <div class="col s12 m2">
            <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
        </div>
        <form id="buscador" method="get" action="<?php echo base_url()."novios/comunidad/forum/buscar" ?>"
              autocomplete="off">
            <div class="input-field col s7 m5" style="margin:0">
                <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
                <input type="hidden" name="page" value="1"/>
                <div id="ventana_buscador">
                </div>
            </div>
            <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
                <button type="submit" class="btn waves-effect waves-light dorado-2"><i class="fa fa-search"
                                                                                       aria-hidden="true"></i> Buscar
                </button>
            </div>
        </form>
        </p>
    </div>
    </div>

    <!--TARJETA CON UN BOTON DICIENDO QUE PUEDES CREAR UN NUEVO DEBATE EN LA COMUNIDAD-->
    <div class="body-container">
        <div class="row">

            <!--COMPONENTES LATERALES-->
            <?php
            if (empty($usuarios_boda)) {
                $usuarios_boda = "";
            }
            if (empty($visitas_perfil)) {
                $visitas_perfil = "";
            }
            if (empty($grupos_miembro)) {
                $grupos_miembro = "";
            }
            if (empty($grupos)) {
                $grupos = "";
            }
            $data = [
                    'usuarios_boda'  => $usuarios_boda,
                    'visitas_perfil' => $visitas_perfil,
                    'grupos_miembro' => $grupos_miembro,
                    'grupos'         => $grupos,
            ];
            $this->view('principal/novia/comunidad/lateral.php', $data);
            ?>
            <?php
            if ( ! empty($ordenamiento)) {
                $token = explode("-", $ordenamiento);
            }
            ?>
            <div class="col s12 m9 pull-left">
                <div class="row" style="border-bottom: 1px solid #999">
                    <h5>Usuarios</h5>
                </div>
                <form id="filtros" method="get"
                      action="<?php echo base_url() ?>novios/comunidad/forum/buscarMiembro">
                    <div class="row">
                        <div class="col s12 m12 z-depth-1 grey lighten-5">
                            <div class="input-field col s4 m4">
                                <input type="text" class="form-control datepicker"
                                       value="<?php if ( ! empty($token[4]) && $token[4] != "fecha_boda") {
                                           echo str_replace("_", "/", $token[4]);
                                       } else {
                                           echo "Fecha Boda";
                                       } ?>">
                                <input type="hidden" id="fecha_boda" name="fecha_boda"
                                       value="<?php if ( ! empty($token[4]) && $token[4] != "fecha_boda") {
                                           echo str_replace("_", "/", $token[4]);
                                       } else {
                                           echo "Fecha Boda";
                                       } ?>">
                            </div>
                            <div class="input-field col s6 m6">
                                <input type="text" class="form-control" name="nombre" value="<?php
                                if ( ! empty($ordenamiento)) {
                                    $token = explode("-", $ordenamiento);
                                    if (count($token) == 1) {
                                        echo str_replace(".", " ", $token[0]);
                                    }
                                }
                                ?>">
                                <input type="hidden" id="pagina" name="pagina"
                                       value="<?php if ( ! empty($pagina)) echo $pagina ?>">
                            </div>
                            <div class="col s2 m2">
                                <button type="submit" class="btn waves-effect waves-light dorado-2"
                                        style="margin: 0; margin-top: 15px"><i class="fa fa-search"
                                                                               aria-hidden="true"></i> Buscar
                                </button>
                            </div>
                        </div>
                </form>
                <div class="row nav-bar">
                    <input type="hidden" id="color" name="color" value="<?php if (count($token) == 5) {
                        echo ! empty($token[0]) ? $token[0] : "color";
                    } else echo "color" ?>">
                    <a class="col s3 m3 dropdown-button clickable" data-beloworigin="true" data-activates='edit-color'>
                        <text class="color">
                            <?php
                            if ( ! empty($token[0])) {
                                switch ($token[0]) {
                                    case 'yellow':
                                        echo 'Amarillo';
                                        break;
                                    case 'orange':
                                        echo 'Anaranjado';
                                        break;
                                    case 'blue':
                                        echo 'Azul';
                                        break;
                                    case 'beige':
                                        echo 'Beige';
                                        break;
                                    case 'white':
                                        echo 'Blanco';
                                        break;
                                    case 'white-black':
                                        echo 'Blanco y Negro';
                                        break;
                                    case 'brown':
                                        echo 'Cafe';
                                        break;
                                    case 'golden':
                                        echo 'Dorado';
                                        break;
                                    case 'fucsia':
                                        echo 'Fucsia';
                                        break;
                                    case 'grey':
                                        echo 'Gris';
                                        break;
                                    case 'purple':
                                        echo 'Morado';
                                        break;
                                    case 'black':
                                        echo 'Negro';
                                        break;
                                    case 'silver':
                                        echo 'Plateado';
                                        break;
                                    case 'red':
                                        echo 'Rojo';
                                        break;
                                    case 'pink':
                                        echo 'Rosa';
                                        break;
                                    case 'green':
                                        echo 'Verde';
                                        break;
                                    case 'wine':
                                        echo 'Vino';
                                        break;
                                    default :
                                        echo 'Color';
                                        break;
                                }
                            } else {
                                echo "Color";
                            }
                            ?></text>
                        <text><i class="fa fa-chevron-down" aria-hidden="true"></i></text>
                    </a>
                    <input type="hidden" id="temporada" name="temporada" value="<?php if (count($token) == 5) {
                        echo ! empty($token[1]) ? $token[1] : "temporada";
                    } else echo "temporada" ?>">
                    <a class="col s3 m3 dropdown-button clickable" data-beloworigin="true"
                       data-activates='edit-temporada'>
                        <text class="temporada">
                            <?php
                            if ( ! empty($token[1])) {
                                switch ($token[1]) {
                                    case 'invierno':
                                        echo 'Invierno';
                                        break;
                                    case 'otono':
                                        echo 'Oto&ntilde;o';
                                        break;
                                    case 'primavera':
                                        echo 'Primavera';
                                        break;
                                    case 'verano':
                                        echo 'Verano';
                                        break;
                                    default :
                                        echo 'Temporada';
                                        break;
                                }
                            } else {
                                echo "Temporada";
                            }
                            ?></text>
                        <text><i class="fa fa-chevron-down" aria-hidden="true"></i></text>
                    </a>
                    <input type="hidden" id="estilo" name="estilo" value="<?php if (count($token) == 5) {
                        echo ! empty($token[2]) ? $token[2] : "";
                    } else echo "estilo" ?>">
                    <a class="col s3 m3 dropdown-button clickable" data-beloworigin="true" data-activates='edit-hora'>
                        <text class="estilo">
                            <?php
                            if ( ! empty($token[2])) {
                                switch ($token[2]) {
                                    case 'aire_libre':
                                        echo 'Al aire libre';
                                        break;
                                    case 'campo':
                                        echo 'En el campo';
                                        break;
                                    case 'de_noche':
                                        echo 'De noche';
                                        break;
                                    case 'elegante':
                                        echo 'Elegante';
                                        break;
                                    case 'playa':
                                        echo 'En la playa';
                                        break;
                                    case 'moderna':
                                        echo 'Modernas';
                                        break;
                                    case 'rustica':
                                        echo 'R&uacute;sticas';
                                        break;
                                    case 'vintage':
                                        echo 'Vintage';
                                        break;
                                    default :
                                        echo 'Estilo';
                                }
                            } else {
                                echo "Estilo";
                            }
                            ?></text>
                        <text><i class="fa fa-chevron-down" aria-hidden="true"></i></text>
                    </a>
                    <input type="hidden" id="estado" name="estado" value="<?php if (count($token) == 5) {
                        echo ! empty($token[3]) ? $token[3] : "";
                    } else echo "estado" ?>">
                    <a class="col s3 m3 dropdown-button clickable" data-beloworigin="true" data-activates='edit-estado'>
                        <text class="estado">
                            <?php
                            if ( ! empty($token[3])) {
                                switch ($token[3]) {
                                    case "Aguascalientes":
                                        echo "Aguascalientes";
                                        break;
                                    case "Baja California":
                                        echo "Baja California";
                                        break;
                                    case "Baja California Sur":
                                        echo "Baja California Sur";
                                        break;
                                    case "Campeche":
                                        echo "Campeche";
                                        break;
                                    case "Chiapas":
                                        echo "Chiapas";
                                        break;
                                    case "Chihuahua":
                                        echo "Chihuahua";
                                        break;
                                    case "Coahuila":
                                        echo "Coahuila";
                                        break;
                                    case "Colima":
                                        echo "Colima";
                                        break;
                                    case "Distrito Federal":
                                        echo "Distrito Federal";
                                        break;
                                    case "Durango":
                                        echo "Durango";
                                        break;
                                    case "Estado Mexico":
                                        echo "Estado Mexico";
                                        break;
                                    case "Guanajuato":
                                        echo "Guanajuato";
                                        break;
                                    case "Guerrero":
                                        echo "Guerrero";
                                        break;
                                    case "Hidalgo":
                                        echo "Hidalgo";
                                        break;
                                    case "Jalisco":
                                        echo "Jalisco";
                                        break;
                                    case "Michoac&aacute;n":
                                        echo "Michoac&aacuoten";
                                        break;
                                    case "Morelos":
                                        echo "Morelos";
                                        break;
                                    case "Nayarit":
                                        echo "Nayarit";
                                        break;
                                    case "Nuevo Leon":
                                        echo "Nuevo Leon";
                                        break;
                                    case "Oaxaca":
                                        echo "Oaxaca";
                                        break;
                                    case "Puebla":
                                        echo "Puebla";
                                        break;
                                    case "Queretaro":
                                        echo "Queretaro";
                                        break;
                                    case "Quintana Roo":
                                        echo "Quintana Roo";
                                        break;
                                    case "San Lu&iacute;s Potos&iacute;":
                                        echo "San Lu&iacuotes Potos&iacuote";
                                        break;
                                    case "Sinaloa":
                                        echo "Sinaloa";
                                        break;
                                    case "Sonora":
                                        echo "Sonora";
                                        break;
                                    case "Tabasco":
                                        echo "Tabasco";
                                        break;
                                    case "Tamaulipas":
                                        echo "Tamaulipas";
                                        break;
                                    case "Tlaxcala":
                                        echo "Tlaxcala";
                                        break;
                                    case "Veracruz":
                                        echo "Veracruz";
                                        break;
                                    case "Yucat&aacute;n":
                                        echo "Yucat&aacuoten";
                                        break;
                                    case "Zacatecas":
                                        echo "Zacatecas";
                                        break;
                                    default :
                                        echo "Estado";
                                        break;
                                }
                            } else {
                                echo "Estado";
                            }
                            ?>
                        </text>
                        <text><i class="fa fa-chevron-down" aria-hidden="true"></i></text>
                    </a>
                </div>
            </div>
        </div>
        <?php if ( ! empty($usuarios)) { ?>
            <div class="col s12 m9 pull-left">
                <?php
                $i = 1;
                if ( ! empty($usuarios)) {
                    foreach ($usuarios as $usuario) {
                        if ($i % 2 != 0) {
                            echo '<div class="row">';
                        }
                        echo '<div class="col s12 m6">
                                    <div class="tarjeta-perfil z-depth-1">
                                        <div class="row">
                                            <div class="col s3 m4">
                                                <div class="card">
                                                    <div class="card-image">
                                                        <img class="foto_perfil" src="'.$usuario->url_foto_usuario.'">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s9 m8">
                                                <p class="truncate"><a class="titulo_tarjeta" href="'.$usuario->url_usuario.'">'.$usuario->usuario.'</a></p>
                                            </div>
                                            <div class="col s9 m8">
                                                <p>'.$usuario->lugar.'</p>
                                                <p>'.$usuario->fecha_creacion.'</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$usuario->url_debates_participacion.'"><p class="truncate"><text class="numeros">'.$usuario->mensajes.'</text> Mensajes</p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$usuario->url_debates.'"><p><text class="numeros">'.$usuario->debates.'</text> Post</p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$usuario->url_fotos.'"><p><text class="numeros">'.$usuario->fotos.'</text> Fotos<p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$usuario->url_amigos.'"><p><text class="numeros">'.$usuario->amigos.'</text> Amigos</p></div>
                                        </div>
                                        <div class="col s6 m6 pull-left" style="margin-bottom: 10px;">';
                        if ( ! empty($usuario->estado_usuario) && $usuario->id_usuario != $this->session->userdata('id_usuario')) {
                            echo '<p style="text-align: center"><a class="btn_agregar clickable" style="color: #999" data-id="'.$usuario->url_agregar_amigo.'"><i class="fa fa-user-plus" aria-hidden="true"></i> <text class="agregar-'.$usuario->id_usuario.'">'.$usuario->estado_usuario.'</text></a><p>';
                        }
                        echo '</div>
                                        <div class="col s6 m6" style="margin-bottom: 10px;">
                                            <p style="text-align: center"><a href="#modal1" class="modal-trigger comentar" style="color: #999" data-name="comentar-'.$usuario->id_usuario.'"><i class="fa fa-comments" aria-hidden="true"></i> Poner Comentario</a></p>
                                        </div>
                                    </div>
                                </div>';
                        if ($i % 2 == 0) {
                            echo "</div>";
                        }
                        $i++;
                    }
                    if (count($usuarios) % 2 != 0) {
                        echo "</div>";
                    }
                }
                ?>
            </div>
        <?php } else { ?>
            <div class="col s12 m9 pull-left grey lighten-5 z-depth-1" style="text-align: center">
                <p style="font-size: 40px; color: red"><i class="fa fa-info-circle" aria-hidden="true"></i></p>
                <p><b>Tu busqueda no ha tenido resultados</b></p>
            </div>
        <?php } ?>
        <div class="col s12 m9 pull-left">
            <?php if ( ! empty($total_paginas) && $total_paginas > 1 && $tipo == "validarMiembros") { ?>
                <div class="row">
                    <div class="col s12 m9 pull-left"
                         style="<?php if ($total_paginas <= 1) echo "display: none;" ?>text-align: center">
                        <div class="col s12 m8" style="float: right">
                            <ul class="pagination">
                                <?php $token = explode("-", $ordenamiento); ?>
                                <li class="<?php echo($pagina == 1 ? "disabled" : "waves-effect") ?>">
                                    <a <?php if ($pagina > 1) echo 'href="'.base_url().'novios/comunidad/forum/usuarios/'.$token[0].'-'.$token[1].'-'.$token[2].'-'.$token[3].'-'.$token[4].'/'.($pagina - 1).'"' ?>><i
                                                class="material-icons">chevron_left</i></a></li>
                                <?php
                                $j        = 1;
                                $contador = 0;
                                if ($total_paginas > 10 && $pagina > 0 && $pagina > 5) {
                                    $pagina2 = $pagina + 5;
                                    if ($pagina2 <= $total_paginas) {
                                        $j = $pagina - 4;
                                    } else {
                                        $j = $pagina - 4;
                                        $j = $j - ($pagina2 - $total_paginas);
                                    }
                                } else {
                                    $j = 1;
                                }
                                for ($i = $j; $i <= $total_paginas && $contador < 10; $i++) {
                                    ?>
                                    <li class="<?php echo($i == $pagina ? "active" : "waves-effect") ?>"><a
                                                href="<?php echo base_url()."novios/comunidad/forum/usuarios/$token[0]-$token[1]-$token[2]-$token[3]-$token[4]/$i" ?>"><?php echo $i ?></a>
                                    </li>
                                    <?php $contador++;
                                } ?>
                                <li class="<?php echo($pagina == $total_paginas ? "disabled" : "waves-effect") ?>">
                                    <a <?php if ($pagina < $total_paginas) echo 'href="'.base_url().'novios/comunidad/forum/usuarios/'.$token[0].'-'.$token[1].'-'.$token[2].'-'.$token[3].'-'.$token[4].'/'.($pagina + 1).'"' ?>><i
                                                class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } elseif ( ! empty($total_paginas) && $total_paginas > 1 && $tipo == "buscarMiembro") { ?>
                <div class="row">
                    <div class="col s12 m9 pull-left"
                         style="<?php if ($total_paginas <= 1) echo "display: none;" ?> text-align: center">
                        <p>
                        <div class="col s12 m8" style="float: right">
                            <ul class="pagination">
                                <?php $token = explode("-", $ordenamiento); ?>
                                <li class="<?php echo($pagina == 1 ? "disabled" : "waves-effect") ?> buscar_miembro"
                                    data-id="<?php echo($pagina - 1) ?>"><a class="clickable"><i class="material-icons">chevron_left</i></a>
                                </li>
                                <?php
                                $j        = 1;
                                $contador = 0;
                                if ($total_paginas > 10 && $pagina > 0 && $pagina > 5) {
                                    $pagina2 = $pagina + 5;
                                    if ($pagina2 <= $total_paginas) {
                                        $j = $pagina - 4;
                                    } else {
                                        $j = $pagina - 4;
                                        $j = $j - ($pagina2 - $total_paginas);
                                    }
                                } else {
                                    $j = 1;
                                }
                                for ($i = $j; $i <= $total_paginas && $contador < 10; $i++) {
                                    ?>
                                    <li class="<?php echo($i == $pagina ? "active" : "waves-effect") ?> buscar_miembro"
                                        data-id="<?php echo $i ?> "><a class="clickable"><?php echo $i ?></a></li>
                                    <?php $contador++;
                                } ?>
                                <li class="<?php echo($pagina == $total_paginas ? "disabled" : "waves-effect") ?> buscar_miembro"
                                    data-id="<?php echo($pagina + 1) ?>"><a class="clickable"><i class="material-icons">chevron_right</i></a>
                                </li>
                            </ul>
                        </div>
                        </p>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!-- ----------------------LIST DROPDOWN FILTRO COLOR------------------------ -->
        <ul id='edit-color' class='dropdown-content edit-boda' data-name="color">
            <li class="edit-item" data-name="yellow">
                <div class="circle-img yellow"></div>
                <small class="truncate">Amarillo</small>
            </li>
            <li class="edit-item" data-name="orange">
                <div class="circle-img orange"/>
    </div>
    <small class="truncate">Anaranjado</small>
    </li>
    <li class="edit-item" data-name="blue">
        <div class="circle-img blue"></div>
        <small class="truncate">Azul</small>
    </li>
    <li class="edit-item" data-name="beige">
        <div class="circle-img beige"></div>
        <small class="truncate">Beige</small>
    </li>
    <li class="edit-item" data-name="white">
        <div class="circle-img white"></div>
        <small class="truncate">Blanco</small>
    </li>
    <li class="edit-item" data-name="white-black">
        <div class="circle-img white-black"></div>
        <small class="truncate">Blanco y Negro</small>
    </li>
    <li class="edit-item" data-name="brown">
        <div class="circle-img brown"></div>
        <small class="truncate">Cafe</small>
    </li>
    <li class="edit-item" data-name="golden">
        <div class="circle-img golden"></div>
        <small class="truncate">Dorado</small>
    </li>
    <li class="edit-item" data-name="fucsia">
        <div class="circle-img fucsia"></div>
        <small class="truncate">Fucsia</small>
    </li>
    <li class="edit-item" data-name="grey">
        <div class="circle-img grey"></div>
        <small class="truncate">Gris</small>
    </li>
    <li class="edit-item" data-name="purple">
        <div class="circle-img purple"></div>
        <small class="truncate">Morado</small>
    </li>
    <li class="edit-item" data-name="black">
        <div class="circle-img black"></div>
        <small class="truncate">Negro</small>
    </li>
    <li class="edit-item" data-name="silver">
        <div class="circle-img silver"></div>
        <small class="truncate">Plateado</small>
    </li>
    <li class="edit-item" data-name="red">
        <div class="circle-img red"></div>
        <small class="truncate">Rojo</small>
    </li>
    <li class="edit-item" data-name="pink">
        <div class="circle-img pink"/>
        </div>
        <small class="truncate">Rosa</small>
    </li>
    <li class="edit-item" data-name="green">
        <div class="circle-img green"></div>
        <small class="truncate">Verde</small>
    </li>
    <li class="edit-item" data-name="wine">
        <div class="circle-img wine"></div>
        <small class="truncate">Vino</small>
    </li>
    <li class="divider"></li>
    <li class="edit-item" data-name="color" style="text-align: left; width: 100%">
        <a class="todas_opciones">Todos Los Colores</a>
    </li>
    </ul>
    <!-- -------------------- FIN LIST DROPDOWN FILTRO COLOR------------------------ -->

    <!-- ----------------------LIST DROPDOWN FILTRO TEMPORADA------------------------ -->
    <ul id='edit-temporada' class='dropdown-content edit-boda' data-name="temporada">
        <li class="edit-item" data-name="invierno">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/invierno.png" alt=""/>
            <br>
            <small class="truncate">Invierno</small>
        </li>
        <li class="edit-item" data-name="otono">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/otono.png" alt=""/>
            <br>
            <small class="truncate">Oto&ntilde;o</small>
        </li>
        <li class="edit-item" data-name="primavera">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/primavera.png" alt=""/>
            <br>
            <small class="truncate">Primavera</small>
        </li>
        <li class="edit-item" data-name="verano">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/verano.png" alt=""/>
            <br>
            <small class="truncate">Verano</small>
        </li>
        <li class="divider"></li>
        <li class="edit-item" data-name="temporada" style="text-align: left; width: 100%">
            <a class="todas_opciones">Todas Las Temporadas</a>
        </li>
    </ul>
    <!-- ----------------------LIST DROPDOWN FIN FILTRO TEMPORADA------------------------ -->

    <!-- ----------------------LIST DROPDOWN FILTRO ESTILO------------------------ -->
    <ul id='edit-hora' class='dropdown-content edit-boda' data-name="hora">
        <li class="edit-item" data-name="aire_libre">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/aire_libre.png" alt=""/>
            <br>
            <small class="truncate">Al aire libre</small>
        </li>
        <li class="edit-item" data-name="campo">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/campo.png" alt=""/>
            <br>
            <small class="truncate">En el campo</small>
        </li>
        <li class="edit-item" data-name="de_noche">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/de_noche.png" alt=""/>
            <br>
            <small class="truncate">De noche</small>
        </li>
        <li class="edit-item" data-name="elegante">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/elegante.png" alt=""/>
            <br>
            <small class="truncate">Elegantes</small>
        </li>
        <li class="edit-item" data-name="playa">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/playa.png" alt=""/>
            <br>
            <small class="truncate">En la playa</small>
        </li>
        <li class="edit-item" data-name="moderna">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/moderna.png" alt=""/>
            <br>
            <small class="truncate">Modernas</small>
        </li>
        <li class="edit-item" data-name="rustica">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/rustica.png" alt=""/>
            <br>
            <small class="truncate">R&uacute;sticas</small>
        </li>
        <li class="edit-item" data-name="vintage">
            <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/vintage.png" alt=""/>
            <br>
            <small class="truncate">Vintage</small>
        </li>
        <li class="divider"></li>
        <li class="edit-item" data-name="estilo" style="text-align: left; width: 100%">
            <a class="todas_opciones">Todos Los Estilos</a>
        </li>
    </ul>
    <!-- ----------------------LIST DROPDOWN FIN FILTRO ESTILO------------------------ -->

    <!-- ----------------------LIST DROPDOWN FILTRO ESTADO------------------------ -->
    <ul id='edit-estado' class='dropdown-content edit-estado text-black' data-name="estado">
        <li class="edit-item" data-name="Aguascalientes">
            <a class="option_estado">
                <text>Aguascalientes</text>
            </a>
        </li>
        <li class="edit-item" data-name="Baja_California">
            <a class="option_estado">
                <text>Baja California</text>
            </a>
        </li>
        <li class="edit-item" data-name="Baja_California_Sur">
            <a class="option_estado">
                <text>Baja California Sur</text>
            </a>
        </li>
        <li class="edit-item" data-name="Campeche">
            <a class="option_estado">
                <text>Campeche</text>
            </a>
        </li>
        <li class="edit-item" data-name="Chiapas">
            <a class="option_estado">
                <text>Chiapas</text>
            </a>
        </li>
        <li class="edit-item" data-name="Chihuahua">
            <a class="option_estado">
                <text>Chihuahua</text>
            </a>
        </li>
        <li class="edit-item" data-name="Coahuila">
            <a class="option_estado">
                <text>Coahuila</text>
            </a>
        </li>
        <li class="edit-item" data-name="Colima">
            <a class="option_estado">
                <text>Colima</text>
            </a>
        </li>
        <li class="edit-item" data-name="Distrito_Federal">
            <a class="option_estado">
                <text>Distrito Federeal</text>
            </a>
        </li>
        <li class="edit-item" data-name="Durango">
            <a class="option_estado">
                <text>Durango</text>
            </a>
        </li>
        <li class="edit-item" data-name="Estado_Mexico">
            <a class="option_estado">
                <text>Estado Mexico</text>
            </a>
        </li>
        <li class="edit-item" data-name="Guanajuato">
            <a class="option_estado">
                <text>Guanajuato</text>
            </a>
        </li>
        <li class="edit-item" data-name="Guerrero">
            <a class="option_estado">
                <text>Guerrero</text>
            </a>
        </li>
        <li class="edit-item" data-name="Hidalgo">
            <a class="option_estado">
                <text>Hidalgo</text>
            </a>
        </li>
        <li class="edit-item" data-name="Jalisco">
            <a class="option_estado">
                <text>Jalisco</text>
            </a>
        </li>
        <li class="edit-item" data-name="Michoacan">
            <a class="option_estado">
                <text>Michoac&&aacute;n</text>
            </a>
        </li>
        <li class="edit-item" data-name="Morelos">
            <a class="option_estado">
                <text>Morelos</text>
            </a>
        </li>
        <li class="edit-item" data-name="Nayarit">
            <a class="option_estado">
                <text>Nayarit</text>
            </a>
        </li>
        <li class="edit-item" data-name="Nuevo_Leon">
            <a class="option_estado">
                <text>Nuevo Leon</text>
            </a>
        </li>
        <li class="edit-item" data-name="Oaxaca">
            <a class="option_estado">
                <text>Oaxaca</text>
            </a>
        </li>
        <li class="edit-item" data-name="Puebla">
            <a class="option_estado">
                <text>Puebla</text>
            </a>
        </li>
        <li class="edit-item" data-name="Queretaro">
            <a class="option_estado">
                <text>Queretaro</text>
            </a>
        </li>
        <li class="edit-item" data-name="Quintana_Roo">
            <a class="option_estado">
                <text>Quintana Roo</text>
            </a>
        </li>
        <li class="edit-item" data-name="San_Luis_Potosi">
            <a class="option_estado">
                <text>San Lu&iacute;s Potos&iacute;</text>
            </a>
        </li>
        <li class="edit-item" data-name="Sinaloa">
            <a class="option_estado">
                <text>Sinaloa</text>
            </a>
        </li>
        <li class="edit-item" data-name="Sonora">
            <a class="option_estado">
                <text>Sonora</text>
            </a>
        </li>
        <li class="edit-item" data-name="Tabasco">
            <a class="option_estado">
                <text>Tabasco</text>
            </a>
        </li>
        <li class="edit-item" data-name="Tamaulipas">
            <a class="option_estado">
                <text>Tamaulipas</text>
            </a>
        </li>
        <li class="edit-item" data-name="Tlaxcala">
            <a class="option_estado">
                <text>Tlaxcala</text>
            </a>
        </li>
        <li class="edit-item" data-name="Veracruz">
            <a class="option_estado">
                <text>Veracruz</text>
            </a>
        </li>
        <li class="edit-item" data-name="Yucatan">
            <a class="option_estado">
                <text>Yucat&aacute;n</text>
            </a>
        </li>
        <li class="edit-item" data-name="Zacatecas">
            <a class="option_estado">
                <text>Zacatecas</text>
            </a>
        </li>
        <li class="divider"></li>
        <li class="edit-item" data-name="estado" style="text-align: left; width: 100%;">
            <a class="todas_opciones">
                <text>Todos Los Estados</text>
            </a>
        </li>
    </ul>
    <!-- ----------------------LIST DROPDOWN FIN FILTRO ESTADO------------------------ -->
    </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Escribe un comentario en el muro</h4>
            <div class="col s12 m12 input-field">
                <textarea id="comentario" class="form-control" rows="20" style="resize: none; height: 50%;"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn dorado-2 comentar-muro"
               data-id="">Publicar</a>
        </div>
    </div>
    <!-- -----------------  TEMPLATE VENTANA BUSCADOR -------------------------- -->
    <ul id="lista_debates">
        <li id="item_debate">
            <a class="enlace_debate" href="">
                <div class="row">
                    <div class="col s4 m3">
                        <div class="card">
                            <div class="card-image">
                                <img class="imagen_debate">
                            </div>
                        </div>
                    </div>
                    <div class="col s9 m9">
                        <p class="titulo_debate"></p>
                        <p class="fecha_debate"></p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <!-- ------------------  FIN TAMPLETE VENTANA BUSCADOR --------------------- -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            showButtonPanel: true,
            yearRange: "2016:2050"
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        $(".datepicker").datepicker({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year
            dateFormat: 'dd/mm/yy',
            onSelect: function (dateText, inst) {
                var fecha = dateText;
                var color = $("#color").val();
                var estacion = $("#temporada").val();
                var estilo = $("#estilo").val();
                var estado = $("#estado").val();
                if (color == "") {
                    color = "color";
                }
                if (estacion == "") {
                    estacion = "temporada";
                }
                if (estilo == "") {
                    estilo = "estilo";
                }
                if (estado == "") {
                    estado = "estado";
                }
                estado = estado.replace(" ", "_");
                fecha = fecha.replace("/", "_");
                fecha = fecha.replace("/", "_");
                window.location.href = "<?php echo base_url()?>novios/comunidad/forum/validarMiembros/" + color + "/" + estacion + "/" + estilo + "/" + estado + "/" + fecha;

            }
        });

        $(document).ready(function () {
            $(".buscar_miembro").on('click', function () {
                if ($(this).hasClass('waves-effect') || $(this).hasClass('active')) {
                    $("#pagina").val($(this).attr("data-id"));
                    $("form").submit();
                }
            });
        });

        $(function () {
            $('.edit-item').on('click', function () {
                switch ($(this.parentElement).data('name')) {
                    case 'color':
                        $("#color").val($(this).data("name"));
                        $('.color').text($(this).text());
                        break;
                    case 'temporada':
                        $("#temporada").val($(this).data("name"));
                        $('.temporada').text($(this).text());
                        break;
                    case 'hora':
                        $("#estilo").val($(this).data("name"));
                        $('.estilo').text($(this).text());
                        break;
                    case 'estado':
                        $("#estado").val($(this).data("name"));
                        $(".estado").text($(this).text());
                        break;
                }
                var color = $("#color").val();
                var estacion = $("#temporada").val();
                var estilo = $("#estilo").val();
                var estado = $("#estado").val();
                var fecha = $("#fecha_boda").val();
                if (color == "") {
                    color = "color";
                }
                if (estacion == "") {
                    estacion = "temporada";
                }
                if (estilo == "") {
                    estilo = "estilo";
                }
                if (estado == "") {
                    estado = "estado";
                }
                if (fecha == "Fecha Boda" || fecha == "") {
                    fecha = "fecha_boda";
                }
                estado = estado.replace(" ", "_");
                fecha = fecha.replace("/", "_");
                fecha = fecha.replace("/", "_");
                window.location.href = "<?php echo base_url()?>novios/comunidad/forum/validarMiembros/" + color + "/" + estacion + "/" + estilo + "/" + estado + "/" + fecha;
            });
        });

        $("#fecha_boda").keyup(function () {
            return false;
        });
        $("#fecha_boda").click(function () {
            $(this).blur();
        });

        $(document).ready(function () {
            $('.botones_tarjeta').on('click', function () {
                var click = $(this).attr('data-id');
                var boton = click.split("-");
                if (boton[0] == "participacion") {
                    window.location.href = "<?php echo base_url()."novios/comunidad/perfil/debates/"?>" + boton[0] + "/" + boton[1];
                } else if (boton[0] == "misdebates") {
                    window.location.href = "<?php echo base_url()."novios/comunidad/perfil/debates/"?>" + boton[0] + "/" + boton[1];
                } else if (boton[0] == "fotos") {
                    window.location.href = "<?php echo base_url()."novios/comunidad/perfil/fotos/"?>" + boton[1];
                } else if (boton[0] == "amigos") {
                    window.location.href = "<?php echo base_url()."novios/comunidad/perfil/amigos/"?>" + boton[1];
                }
            });
        });

        $(document).ready(function () {
            $('.modal-trigger').modal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: .5, // Opacity of modal background
                    in_duration: 300, // Transition in duration
                    out_duration: 200, // Transition out duration
                    starting_top: '4%', // Starting top style attribute
                    ending_top: '10%', // Ending top style attribute
                }
            );
            $(".comentar").on("click", function () {
                var name = $(this).attr("data-name");
                var token = name.split("-");
                $(".modal-action").attr("data-id", token[1]);
            });
        });

        $(document).ready(function () {
            $(".comentar-muro").on("click", function () {
                var comentario = $("#comentario").val();
                var usuario = $(this).attr("data-id");
                $.ajax({
                    url: '<?php echo base_url()."novios/comunidad/perfil/comentario"?>',
                    method: 'POST',
                    data: {
                        'comentario': comentario,
                        'muro_usuario': usuario
                    },
                    success: function (res) {
                        if (res.success) {
                            setTimeout(function () {
                                alert("Se ha publicado con exito");
                                $("#comentario").val("");
                            }, 500);
                        }
                    },
                    error: function () {
                        setTimeout(function () {
                            alert("Lo sentimos ocurrio un error");
                        }, 500);
                    }
                });
            });
        });

        $(document).ready(function () {
            $('.desplegar_grupos').click(function () {
                var mostrar = $(this).text();
                var display = $('.oculto').css('display');
                if (display == "none") {
                    $('.oculto').css('display', 'block');
                    $(this).text("Ocultar Grupos");
                } else {
                    $('.oculto').css('display', 'none');
                    $(this).text("M&aacute;s Grupos...");
                }
            });
        });

        $(document).ready(function () {
            $('.btn_agregar').on("click", function () {
                var usuario = $(this).attr("data-id");
                var token = usuario.split('-');
                var tipo = $("." + usuario).text();
                $.ajax({
                    url: '<?php echo base_url() ?>novios/comunidad/perfil/solicitudAmistad',
                    method: 'POST',
                    data: {
                        'id_usuario_confirmacion': token[1],
                        'tipo': tipo
                    },
                    success: function (res) {
                        if (res.success) {
                            $("." + usuario).text(res.data);
                        }
                    },
                    error: function () {
                        alert("Lo sentimos ocurrio un error");
                    }
                });
            });
        });
        $(document).ready(function () {
            var activo = 0;
            var height = 0;
            var bandera = true;

            $("input[name=buscar]").keydown(function (evt) {
                var titulo = $("input[name=buscar]").val();
                var key = evt.keyCode || evt.which;
                $("#ventana_buscador").show();
                if (key == 40) {
                    if ($('.item-' + activo).hasClass("hover")) {
                        $('.item-' + activo).removeClass("hover");
                    }
                    activo++;
                    if ($('.item-' + activo).html() != undefined) {
                        $('.item-' + activo).addClass("hover");
                        if (activo > 1) {
                            height = (activo - 1) * $('.item-' + activo).height();
                            if (height > 0) {
                                $("#ventana_buscador").scrollTop(height);
                            }
                        }
                    } else {
                        if (bandera) {
                            activo++;
                            bandera = false;
                        }
                        activo--;
                    }
                } else if (key == 38) {
                    if ($('.item-' + activo).hasClass("hover")) {
                        $('.item-' + activo).removeClass("hover");
                    }
                    activo--;
                    if ($('.item-' + activo).html() != undefined) {
                        $('.item-' + activo).addClass("hover");
                        if (activo >= 0) {
                            height = (activo - 1) * $('.item-' + activo).height();
                            if (height >= 0) {
                                $("#ventana_buscador").scrollTop(height);
                            }
                        }
                    } else {
                        if (!bandera) {
                            activo--;
                            bandera = true;
                        }
                        activo++;
                    }
                } else if (key == 13) {
                    $("#buscador").on('keypress', function (e) {
                        e.preventDefault();
                        return false;
                    });
                    window.location.href = $('.item-' + activo + ' a').attr("href");
                } else if (key != 39 && key != 37) {
                    activo = 0;
                    grupos();
                }
            });

            $("input[name=buscar]").keyup(function (evt) {
                var titulo = $("input[name=buscar]").val();
                var key = evt.keyCode || evt.which;
                $("#ventana_buscador").show();
                if (titulo != "" && key != 38 && key != 40 && key != 39 && key != 37) {
                    activo = 0;
                    debates(titulo);
                } else if (key == 13) {
                    window.location.href = $('.item-' + activo + ' a').attr("href");
                }
            });

            $(document).on('mouseenter', '#ventana_buscador > ul > li', function () {
                $('.item-' + activo).removeClass("hover");
                var clase = $(this).attr("class");
                var token = clase.split("-");
                activo = token[1];
                $(this).addClass("hover");
            });

            $("body").on('click', function () {
                $("#ventana_buscador").hide();
                $('.item-' + activo).removeClass("hover");
            });

            $("input[name=buscar]").on('click', function (e) {
                e.stopPropagation();
                $('.item-' + activo).removeClass("hover");
                activo = 0;
                var titulo = $("input[name=buscar]").val();
                if (titulo == "" && $("#ventana_buscador ul").html() == undefined) {
                    grupos();
                }
                $("#ventana_buscador").show();
            });

            $("#ventana_buscador").on('mouseleave', function () {
                setTimeout(function () {
                    $('.item-' + activo).removeClass("hover");
                }, 1000);
            });

            function grupos() {
                $.ajax({
                    url: '<?php echo base_url()."novios/comunidad/home/getGrupos"?>',
                    success: function (res) {
                        var val = Array();
                        if (res.success) {
                            $("#ventana_buscador").html("");
                            var i = 1;
                            var ul = document.createElement("ul");
                            $(ul).html($("#lista_debates").html());
                            for (var aux in res.data) {
                                if (res.data.hasOwnProperty(aux)) {
                                    var li = document.createElement("li");
                                    $(li).html($("#item_debate").html());
                                    $(li).addClass("item-" + i);
                                    $(li).find(".enlace_debate").attr('href', res.data[aux].enlace_grupo);
                                    $(li).find(".imagen_debate").attr('src', res.data[aux].imagen);
                                    $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                    $(li).find(".fecha_debate").text(res.data[aux].debates + " Debates");
                                    $(ul).append(li);
                                    if (i == 16) {
                                        break;
                                    }
                                    i++;
                                }
                            }
                            $("#ventana_buscador").append($(ul));
                            $("#ventana_buscador").data("grupos", "true");
                            $("#ventana_buscador").show();
                        }
                    }
                });
                $("#item_debate").hide();
                $("#ventana_buscador").empty();
            }

            function debates(titulo) {
                $.ajax({
                    url: '<?php echo base_url()."novios/comunidad/home/buscar" ?>',
                    method: 'post',
                    data: {
                        'titulo_debate': titulo
                    },
                    success: function (res) {
                        if (res.success) {
                            $("#ventana_buscador").html("");
                            var i = 1;
                            var ul = document.createElement("ul");
                            $(ul).html($("#lista_debates").html());
                            var val = Array();
                            for (var aux in res.data) {
                                if (res.data.hasOwnProperty(aux)) {
                                    var li = document.createElement("li");
                                    $(li).html($("#item_debate").html());
                                    $(li).addClass("item-" + i);
                                    $(li).find(".enlace_debate").attr('href', res.data[aux].enlace_debate);
                                    $(li).find(".imagen_debate").attr('src', res.data[aux].foto_usuario);
                                    $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                    $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                    $(ul).append(li);
                                    if (i > (res.data.length - 1)) {
                                        break;
                                    }
                                    i++;
                                }
                            }
                            $("#ventana_buscador").append($(ul));
                            $("#ventana_buscador").show();
                        }
                    }
                });
                $("#item_debate").hide();
                $("#ventana_buscador").empty();
            }
        });
    </script>
    </body>
<?php $this->view('principal/footer'); ?>