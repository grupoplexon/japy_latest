<div class="col s12 m9 pull-left">
    <div class="row">
        <div class="col s12 m5">
            <h5><?php if(!empty($titulo)) echo $titulo?></h5>
        </div>
        <div class="input-field col s6 m5" style="margin: 0">
            <select id="selector">
                <option value="1" <?php if(!empty($ordenamiento) && $ordenamiento == "comentarios") echo 'selected'?>>Comentarios</option>
                <option value="2" <?php if(!empty($ordenamiento) && $ordenamiento == "visitas") echo 'selected'?>>Visitas</option>
                <option value="3" <?php if(!empty($ordenamiento) && $ordenamiento == "fecha") echo 'selected'?>>Fecha</option>
            </select>
        </div>
        <div class="col s6 m2">
            <a href="<?php echo base_url() ?>index.php/novios/comunidad/picture/nuevaFoto/<?php echo empty($id_grupo)? "" : $id_grupo ?>" class="btn dorado-2"> <i class="fa fa-camera" aria-hidden="true"></i> Subir una Foto </a>
        </div>
    </div>
    <div class="row">
        <?php
        if(!empty($fotos)){
            foreach ($fotos as $foto){
                echo '
                <div class="col s6 m3 foto_publicada">
                    <div class="card small">
                        <div class="card-image">
                            <a href="'.base_url().'index.php/novios/comunidad/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'"> <img class="foto_zoom" src="'.base_url().'index.php/novios/comunidad/picture/foto/'.$foto->id_foto.'"></a>
                        </div>
                        <div class="card-content" style="text-align: center">
                            <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                <img alt="" src="'.$foto->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">
                            </div>
                            <p class="clickable truncate"><a class="titulo_foto" href="'.base_url().'index.php/novios/comunidad/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'">'.$foto->titulo.'</a></p><br/>
                            <p class=""><a class="link_usuario">'.$foto->usuario.'</a></p>
                        </div>
                    </div>
                </div>';
            }
        }
        ?>
    </div>
    <div class="row">
        <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
            <div class="col s12 m9" style="float: right">
                <ul class="pagination">
                    <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a <?php if($pagina > 1) echo 'href="'.base_url()."index.php/novios/comunidad/Group/grupo/$id_grupo/fotos/$ordenamiento/".($pagina - 1).'"' ?>><i class="material-icons">chevron_left</i></a></li>
                    <?php
                    $j = 1;
                    $contador = 0;
                    if($total_paginas > 10 && $pagina > 0 && $pagina > 5) {
                        $pagina2 = $pagina + 5;
                        if($pagina2 <= $total_paginas){
                            $j = $pagina - 4;
                        }else{
                            $j = $pagina - 4;
                            $j = $j - ($pagina2 - $total_paginas);
                        }
                    }else{
                        $j = 1;
                    }
                    for($i=$j; $i <= $total_paginas && $contador < 10; $i++){
                    ?>
                    <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo base_url()."index.php/novios/comunidad/Group/grupo/$id_grupo/fotos/$ordenamiento/$i" ?>"><?php echo $i ?></a></li>
                    <?php $contador++; } ?>
                    <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a <?php if($pagina < $total_paginas) echo 'href="'.base_url()."index.php/novios/comunidad/Group/grupo/$id_grupo/fotos/$ordenamiento/".($pagina + 1).'"'?>><i class="material-icons">chevron_right</i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (){
        $("#selector").change(function (){
            switch($(this).val()){
                case "1":
                    window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/fotos/comentarios";
                    break;
                case "2":
                    window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/fotos/visitas";
                    break;
                case "3":
                    window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/fotos/fecha";
                    break;
            }
        });
    });
</script>
