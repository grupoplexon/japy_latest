<div class="col s12 m9 pull-left">
    <div class="row">
    <!-----------------------FILTRO FECHA BODA-------------------------------------->
        <div class="col s12 m12 pull-left grey lighten-5 z-depth-1">
        <div class="col s4 m4 input-field" style="margin-top: 15px;">
            <?php
            if(!empty($ordenamiento)){
                $token = explode("-", $ordenamiento);
            }
            ?>
            <input type="text" class="datepicker form-control" id="fecha_boda" name="fecha_boda" value="<?php
                                                                                                        if(!empty($token[4])){
                                                                                                            if(!empty($token[4]) && $token[4] != "fecha_boda"){
                                                                                                                echo str_replace("_","/",$token[4]);
                                                                                                            } else {
                                                                                                                echo "Fecha Boda";
                                                                                                            }
                                                                                                        }else{
                                                                                                            echo "Fecha Boda";
                                                                                                        }
                                                                                                        ?>">
        </div>
    <!---------------------FIN FILTRO FECHA BODA-------------------------------------->

    <!-------------------------BUSCAR USUARIO-------------------------------------->
    <form method="get" action="<?php echo base_url()?>index.php/novios/comunidad/Group/buscarMiembro">

    <div class="col s6 m6 input-field">
        <input type="text" class="form-control" name="usuario" value="<?php
            if(!empty($ordenamiento)){
                $token = explode("-", $ordenamiento);
                if(count($token) == 1 && $token[0] != 1){
                    echo str_replace("."," ",$token[0]);
                }
            }
        ?>">
    </div>
    <div class="col s2 m2" style="margin-top: 15px;">
        <input type="hidden" name="grupo" value="<?php if(!empty($id_grupo)) echo $id_grupo?>">
        <input type="hidden" id="pagina" name="pagina" value="<?php if(!empty($pagina)) echo $pagina?>">
        <button type="submit" class="waves-effect waves-light btn dorado-2" style="width: 100%;"><i class="fa fa-search" aria-hidden="true"></i></button>
    </div>
    </form>
        </div>
    <!-----------------------FIN BUSCAR USUARIO-------------------------------------->
    </div>
</div>
<div class="col s12 m9 pull-right">
    <div class="row nav-bar">
    <!-----------------------FILTRO COLOR-------------------------------------->
    <input type="hidden" name="seleccion-color" id="seleccion-color" value="<?php if(!empty($ordenamiento)){
                                                                                $token = explode("-", $ordenamiento);
                                                                                if(count($token) == 1){
                                                                                    echo "color";
                                                                                }else if(!empty ($token[0])){
                                                                                    echo $token[0];
                                                                                }
                                                                            }?>">

            <a class="dropdown-button col s3 m3 clickable" data-beloworigin="true"  data-activates='edit-color'>
                <text class="color">
                <?php
                if(!empty($ordenamiento)){
                    $token = explode("-", $ordenamiento);
                    $token[0] = str_replace("_","-",$token[0]);
                    if(!empty($token[0])){
                        switch ($token[0]) {
                            case 'yellow':
                                echo 'Amarillo';
                                break;
                            case 'orange':
                                echo 'Anaranjado';
                                break;
                            case 'blue':
                                echo 'Azul';
                                break;
                            case 'beige':
                                echo 'Beige';
                                break;
                            case 'white':
                                echo 'Blanco';
                                break;
                            case 'white-black':
                                echo 'Blanco y Negro';
                                break;
                            case 'brown':
                                echo 'Cafe';
                                break;
                            case 'golden':
                                echo 'Dorado';
                                break;
                            case 'fucsia':
                                echo 'Fucsia';
                                break;
                            case 'grey':
                                echo 'Gris';
                                break;
                            case 'purple':
                                echo 'Morado';
                                break;
                            case 'black':
                                echo 'Negro';
                                break;
                            case 'silver':
                                echo 'Plateado';
                                break;
                            case 'red':
                                echo 'Rojo';
                                break;
                            case 'pink':
                                echo 'Rosa';
                                break;
                            case 'green':
                                echo 'Verde';
                                break;
                            case 'wine':
                                echo 'Vino';
                                break;
                            default :
                                echo 'Color';
                                break;
                        }
                    }else{
                        echo "Color";
                    }
                }
                ?>  </text> <text><i class="fa fa-chevron-down" aria-hidden="true"></i></text>
            </a>

        <ul id='edit-color' class='dropdown-content edit-boda' data-name="color">
            <li class="edit-item" data-name="yellow">
                <div class="circle-img yellow"></div>
                <small class="truncate">Amarillo</small>
            </li>
            <li class="edit-item" data-name="orange">
                <div class="circle-img orange"/></div>
                <small class="truncate">Anaranjado</small>
            </li>
            <li class="edit-item" data-name="blue">
                <div class="circle-img blue"></div>
                <small class="truncate">Azul</small>
            </li>
            <li class="edit-item" data-name="beige">
                <div class="circle-img beige"></div>
                <small class="truncate">Beige</small>
            </li>
            <li class="edit-item" data-name="white">
                <div class="circle-img white"></div>
                <small class="truncate">Blanco</small>
            </li>
            <li class="edit-item" data-name="white-black">
                <div class="circle-img white-black"></div>
                <small class="truncate">Blanco y Negro</small>
            </li>
            <li class="edit-item" data-name="brown">
                <div class="circle-img brown"></div>
                <small class="truncate">Cafe</small>
            </li>
            <li class="edit-item" data-name="golden">
                <div class="circle-img golden"></div>
                <small class="truncate">Dorado</small>
            </li>
            <li class="edit-item" data-name="fucsia">
                <div class="circle-img fucsia"></div>
                <small class="truncate">Fucsia</small>
            </li>
            <li class="edit-item" data-name="grey">
                <div class="circle-img grey"></div>
                <small class="truncate">Gris</small>
            </li>
            <li class="edit-item" data-name="purple">
                <div class="circle-img purple"></div>
                <small class="truncate">Morado</small>
            </li>
            <li class="edit-item" data-name="black">
                <div class="circle-img black"></div>
                <small class="truncate">Negro</small>
            </li>
            <li class="edit-item" data-name="silver">
                <div class="circle-img silver"></div>
                <small class="truncate">Plateado</small>
            </li>
            <li class="edit-item" data-name="red">
                <div class="circle-img red"></div>
                <small class="truncate">Rojo</small>
            </li>
            <li class="edit-item" data-name="pink">
                <div class="circle-img pink"/></div>
                <small class="truncate">Rosa</small>
            </li>
            <li class="edit-item" data-name="green">
                <div class="circle-img green"></div>
                <small class="truncate">Verde</small>
            </li>
            <li class="edit-item" data-name="wine">
                <div class="circle-img wine"></div>
                <small class="truncate">Vino</small>
            </li>
            <li class="divider"></li>
            <li class="edit-item" data-name="color" style="text-align: left; width: 100%">
                <a class="todas_opciones">Todos Los Colores</a>
            </li>
        </ul>
    <!---------------------FIN FILTRO COLOR-------------------------------------->

    <!-----------------------FILTRO ESTACION-------------------------------------->
    <input type="hidden" name="seleccion-estacion" id="seleccion-estacion" value="<?php if(!empty($ordenamiento)){ $token = explode("-", $ordenamiento); echo !empty($token[1])? $token[1]:"temporada"; }?>">

            <a class="dropdown-button col s3 m3 clickable" data-beloworigin="true"  data-activates='edit-temporada'>
                <text class="temporada">
                <?php
                if(!empty($ordenamiento)){
                    $tokenn = explode("-", $ordenamiento);
                    if(!empty($token[1])){
                        switch ($token[1]) {
                            case 'invierno':
                                echo 'Invierno';
                                break;
                            case 'otono':
                                echo 'Oto&ntilde;o';
                                break;
                            case 'primavera':
                                echo 'Primavera';
                                break;
                            case 'verano':
                                echo 'Verano';
                                break;
                            default :
                                echo 'Temporada';
                                break;
                        }
                    }else{
                        echo "Temporada";
                    }
                }
                ?>  </text><text><i class="fa fa-chevron-down" aria-hidden="true"></i></text>
            </a>
        <ul id='edit-temporada' class='dropdown-content edit-boda' data-name="temporada">
            <li class="edit-item" data-name="invierno">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/invierno.png" alt=""/>
                <br><small class="truncate">Invierno</small>
            </li>
            <li class="edit-item" data-name="otono">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/otono.png" alt=""/>
                <br><small class="truncate">Oto&ntilde;o</small>
            </li>
            <li class="edit-item" data-name="primavera">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/primavera.png" alt=""/>
                <br><small class="truncate">Primavera</small>
            </li>
            <li class="edit-item" data-name="verano">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/verano.png" alt=""/>
                <br><small class="truncate">Verano</small>
            </li>
            <li class="divider"></li>
            <li class="edit-item" data-name="temporada" style="text-align: left; width: 100%">
                <a class="todas_opciones">Todas Las Temporadas</a>
            </li>
        </ul>
    <!--------------------FIN FILTRO ESTACION-------------------------------------->

    <!-----------------------FILTRO ESTILO-------------------------------------->
        <input type="hidden" name="seleccion-estilo" id="seleccion-estilo" value="<?php if(!empty($ordenamiento)){
        $token = explode("-", $ordenamiento); echo !empty($token[2])? $token[2]:"estilo";}?>">

            <a class="dropdown-button col s3 m3 clickable" data-beloworigin="true"  data-activates='edit-hora'>
                <text class="estilo">
                <?php
                if(!empty($ordenamiento)){
                    $token = explode("-", $ordenamiento);
                    if(!empty($token[2])){
                        switch ($token[2]) {
                            case 'aire_libre':
                                echo 'Al aire libre';
                                break;
                            case 'campo':
                                echo 'En el campo';
                                break;
                            case 'de_noche':
                                echo 'De noche';
                                break;
                            case 'elegante':
                                echo 'Elegante';
                                break;
                            case 'playa':
                                echo 'En la playa';
                                break;
                            case 'moderna':
                                echo 'Modernas';
                                break;
                            case 'rustica':
                                echo 'R&uacute;sticas';
                                break;
                            case 'vintage':
                                echo 'Vintage';
                                break;
                            default :
                                echo 'Estilo';
                        }
                    }else{
                        echo "Estilo";
                    }
                }
                ?>
                </text><text><i class="fa fa-chevron-down" aria-hidden="true"></i></text>
            </a>
        <ul id='edit-hora' class='dropdown-content edit-boda' data-name="hora">
            <li class="edit-item" data-name="aire_libre">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/aire_libre.png" alt=""/>
                <br><small class="truncate">Al aire libre</small>
            </li>
            <li class="edit-item" data-name="campo">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/campo.png" alt=""/>
                <br><small class="truncate">En el campo</small>
            </li>
            <li class="edit-item" data-name="de_noche">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/de_noche.png" alt=""/>
                <br><small class="truncate">De noche</small>
            </li>
            <li class="edit-item" data-name="elegante">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/elegante.png" alt=""/>
                <br><small class="truncate">Elegantes</small>
            </li>
            <li class="edit-item" data-name="playa">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/playa.png" alt=""/>
                <br><small class="truncate">En la playa</small>
            </li>
            <li class="edit-item" data-name="moderna">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/moderna.png" alt=""/>
                <br><small class="truncate">Modernas</small>
            </li>
            <li class="edit-item" data-name="rustica">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/rustica.png" alt=""/>
                <br><small class="truncate">R&uacute;sticas</small>
            </li>
            <li class="edit-item" data-name="vintage">
                <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/vintage.png" alt=""/>
                <br><small class="truncate">Vintage</small>
            </li>
            <li class="divider" data-name=""></li>
            <li class="edit-item" data-name="estilo" style="text-align: left; width: 100%">
                <a class="todas_opciones">Todas Los Estilos</a>
            </li>
        </ul>
    <!---------------------FIN FILTRO ESTILO-------------------------------------->

    <!-----------------------FILTRO ESTADO-------------------------------------->
        <input type="hidden" name="seleccion-estado" id="seleccion-estado" value="<?php if(!empty($ordenamiento))
            $token = explode("-", $ordenamiento);
            echo !empty($token[3])? str_replace("_", " ", $token[3]):"estado";?>">

            <a class="dropdown-button col s3 m3 clickable" data-beloworigin="true"  data-activates='edit-estado'>
                <text class="estado">
                <?php
                if(!empty($ordenamiento)){
                    $token = explode("-", $ordenamiento);
                    if(!empty($token[3])){
                        $token[3] = str_replace("_", " ", $token[3]);
                        switch ($token[3]){
                            case "Aguascalientes":
                                echo "Aguascalientes";
                                break;
                            case "Baja California":
                                echo "Baja California";
                                break;
                            case "Baja California Sur":
                                echo "Baja California Sur";
                                break;
                            case "Campeche":
                                echo "Campeche";
                                break;
                            case "Chiapas":
                                echo "Chiapas";
                                break;
                            case "Chihuahua":
                                echo "Chihuahua";
                                break;
                            case "Coahuila":
                                echo "Coahuila";
                                break;
                            case "Colima":
                                echo "Colima";
                                break;
                            case "Distrito Federal":
                                echo "Distrito Federal";
                                break;
                            case "Durango":
                                echo "Durango";
                                break;
                            case "Estado Mexico":
                                echo "Estado Mexico";
                                break;
                            case "Guanajuato":
                                echo "Guanajuato";
                                break;
                            case "Guerrero":
                                echo "Guerrero";
                                break;
                            case "Hidalgo":
                                echo "Hidalgo";
                                break;
                            case "Jalisco":
                                echo "Jalisco";
                                break;
                            case "Michoac&aacute;n":
                                echo "Michoac&aacuoten";
                                break;
                            case "Morelos":
                                echo "Morelos";
                                break;
                            case "Nayarit":
                                echo "Nayarit";
                                break;
                            case "Nuevo Leon":
                                echo "Nuevo Leon";
                                break;
                            case "Oaxaca":
                                echo "Oaxaca";
                                break;
                            case "Puebla":
                                echo "Puebla";
                                break;
                            case "Queretaro":
                                echo "Queretaro";
                                break;
                            case "Quintana Roo":
                                echo "Quintana Roo";
                                break;
                            case "San Lu&iacute;s Potos&iacute;":
                                echo "San Lu&iacuotes Potos&iacuote";
                                break;
                            case "Sinaloa":
                                echo "Sinaloa";
                                break;
                            case "Sonora":
                                echo "Sonora";
                                break;
                            case "Tabasco":
                                echo "Tabasco";
                                break;
                            case "Tamaulipas":
                                echo "Tamaulipas";
                                break;
                            case "Tlaxcala":
                                echo "Tlaxcala";
                                break;
                            case "Veracruz":
                                echo "Veracruz";
                                break;
                            case "Yucat&aacute;n":
                                echo "Yucat&aacuoten";
                                break;
                            case "Zacatecas":
                                echo "Zacatecas";
                                break;
                            default :
                                echo "Estado";
                                break;
                        }
                    }else{
                        echo "Estado";
                    }
                }
                ?>
                </text>
                <text><i class="fa fa-chevron-down" aria-hidden="true"></i></text>
            </a>
            <ul id='edit-estado' class='dropdown-content edit-estado text-black' data-name="estado">
                <li class="edit-item" data-name="Aguascalientes">
                    <a class="option_estado"><text >Aguascalientes</text></a>
                </li>
                <li class="edit-item" data-name="Baja_California">
                    <a class="option_estado"><text>Baja California</text></a>
                </li>
                <li class="edit-item" data-name="Baja_California_Sur">
                    <a class="option_estado"><text>Baja California Sur</text></a>
                </li>
                <li class="edit-item" data-name="Campeche">
                    <a class="option_estado"><text>Campeche</text></a>
                </li>
                <li class="edit-item" data-name="Chiapas">
                    <a class="option_estado"><text>Chiapas</text></a>
                </li>
                <li class="edit-item" data-name="Chihuahua">
                    <a class="option_estado"><text>Chihuahua</text></a>
                </li>
                <li class="edit-item" data-name="Coahuila">
                    <a class="option_estado"><text>Coahuila</text></a>
                </li>
                <li class="edit-item" data-name="Colima">
                    <a class="option_estado"><text>Colima</text></a>
                </li>
                <li class="edit-item" data-name="Distrito_Federal">
                    <a class="option_estado"><text>Distrito Federeal</text></a>
                </li>
                <li class="edit-item" data-name="Durango">
                    <a class="option_estado"><text>Durango</text></a>
                </li>
                <li class="edit-item" data-name="Estado_Mexico">
                    <a class="option_estado"><text>Estado Mexico</text></a>
                </li>
                <li class="edit-item" data-name="Guanajuato">
                    <a class="option_estado"><text>Guanajuato</text></a>
                </li>
                <li class="edit-item" data-name="Guerrero">
                    <a class="option_estado"><text>Guerrero</text></a>
                </li>
                <li class="edit-item" data-name="Hidalgo">
                    <a class="option_estado"><text>Hidalgo</text></a>
                </li>
                <li class="edit-item" data-name="Jalisco">
                    <a class="option_estado"><text>Jalisco</text></a>
                </li>
                <li class="edit-item" data-name="Michoacan">
                    <a class="option_estado"><text>Michoac&aacute;n</text></a>
                </li>
                <li class="edit-item" data-name="Morelos">
                    <a class="option_estado"><text>Morelos</text></a>
                </li>
                <li class="edit-item" data-name="Nayarit">
                    <a class="option_estado"><text>Nayarit</text></a>
                </li>
                <li class="edit-item" data-name="Nuevo_Leon">
                    <a class="option_estado"><text>Nuevo Leon</text></a>
                </li>
                <li class="edit-item" data-name="Oaxaca">
                    <a class="option_estado"><text>Oaxaca</text></a>
                </li>
                <li class="edit-item" data-name="Puebla">
                    <a class="option_estado"><text>Puebla</text></a>
                </li>
                <li class="edit-item" data-name="Queretaro">
                    <a class="option_estado"><text>Queretaro</text></a>
                </li>
                <li class="edit-item" data-name="Quintana_Roo">
                    <a class="option_estado"><text>Quintana Roo</text></a>
                </li>
                <li class="edit-item" data-name="San_Luis_Potosi">
                    <a class="option_estado"><text>San Lu&iacute;s Potos&iacute;</text></a>
                </li>
                <li class="edit-item" data-name="Sinaloa">
                    <a class="option_estado"><text>Sinaloa</text></a>
                </li>
                <li class="edit-item" data-name="Sonora">
                    <a class="option_estado"><text>Sonora</text></a>
                </li>
                <li class="edit-item" data-name="Tabasco">
                    <a class="option_estado"><text>Tabasco</text></a>
                </li>
                <li class="edit-item" data-name="Tamaulipas">
                    <a class="option_estado"><text>Tamaulipas</text></a>
                </li>
                <li class="edit-item" data-name="Tlaxcala">
                    <a class="option_estado"><text>Tlaxcala</text></a>
                </li>
                <li class="edit-item" data-name="Veracruz">
                    <a class="option_estado"><text>Veracruz</text></a>
                </li>
                <li class="edit-item" data-name="Yucatan">
                    <a class="option_estado"><text>Yucat&aacute;n</text></a>
                </li>
                <li class="edit-item" data-name="Zacatecas">
                    <a class="option_estado"><text>Zacatecas</text></a>
                </li>
                <li class="divider"></li>
                <li class="edit-item" data-name="estado" style="text-align: left; width: 100%;">
                    <a class="todas_opciones"><text>Todos Los Estados</text></a>
                </li>
            </ul>
        </div>
    <!---------------------FIN FILTRO ESTADO-------------------------------------->
    <div class="col s12 m12 pull-left">
    <?php
        $i = 1;
        if(!empty($miembros)){
            foreach ($miembros as $miembro){
                if($i % 2 != 0){
                    echo '<div class="row">';
                }
                echo '<div class="col s12 m6">
                        <div class="tarjeta-perfil z-depth-1">
                            <div class="row">
                                <div class="col s3 m4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img class="foto_perfil" src="'.$miembro->url_foto_usuario.'">
                                        </div>
                                    </div>
                                </div>
                                <div class="col s6 m5">
                                    <p class="truncate"><a class="titulo_tarjeta" href="'.$miembro->url_usuario.'">'.$miembro->usuario.'</a></p>
                                </div>
                                <div class="col s3 m3">
                                    <p class="dorado-2-text">'.$miembro->puntos.'</p>
                                </div>
                                <div class="col s9 m8">
                                    <p>'.$miembro->lugar_boda.'</p>
                                    <p>'.$miembro->fecha_boda.'</p>
                                    <p>'.$miembro->fecha_creacion.'</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$miembro->url_debates_participacion.'"><p class="truncate"><text class="numeros">'.$miembro->num_mensajes.'</text> Mensajes</p></div>
                                <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$miembro->url_debates.'"><p><text class="numeros">'.$miembro->num_debates.'</text> Post</p></div>
                                <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$miembro->url_fotos.'"><p><text class="numeros">'.$miembro->num_fotos.'</text> Fotos<p></div>
                                <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$miembro->url_amigos.'"><p><text class="numeros">'.$miembro->num_amigos.'</text> Amigos</p></div>
                            </div>
                            <div class="col s6 m6 pull-left" style="margin-bottom: 10px;">';
                if(!empty($miembro->estado_usuario) && $miembro->id_usuario != $this->session->userdata('id_usuario')){
                    echo    '<p style="text-align: center"><a class="btn_agregar clickable" style="color: #999" data-id="'.$miembro->url_agregar_amigo.'"><i class="fa fa-user-plus" aria-hidden="true"></i> <text class="agregar-'.$miembro->id_usuario.'">'.$miembro->estado_usuario.'</text></a><p>';
                }
                echo        '</div>
                            <div class="col s6 m6" style="margin-bottom: 10px;">
                                <p style="text-align: center"><a href="#modal1" class="modal-trigger comentar" style="color: #999" data-name="comentar-'.$miembro->id_usuario.'"><i class="fa fa-comments" aria-hidden="true"></i> Poner Comentario</a></p>
                            </div>
                        </div>
                    </div>';
                if($i % 2 == 0){
                    echo "</div>";
                }
                $i++;
            }
        }else{
            echo    '<div class="row">
                        <div class="col s12 m12 grey lighten-5 z-depth-1">
                            <p class="aviso"> <i class="fa fa-info-circle icon_informacion" aria-hidden="true"></i></p>
                            <p class="aviso"> Lo sentimos tu busqueda no ha tenido resultados</p>
                        </div>
                    </div>';
        }
    ?>
    </div>

    <?php if(!empty($total_paginas) && $total_paginas > 1 && $tipo == "validarMiembros"){ ?>
    <div class="row">
        <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
            <div class="col s12 m6" style="float: right">
                <ul class="pagination">
                    <?php $token = explode("-", $ordenamiento); ?>
                    <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a <?php if($pagina > 1) echo 'href="'.base_url().'index.php/novios/comunidad/Group/validarMiembros/'.$id_grupo.'/'.$token[0].'/'.$token[1].'/'.$token[2].'/'.$token[3].'/'.$token[4].'/'.($pagina - 1).'"'?>><i class="material-icons">chevron_left</i></a></li>
                    <?php
                    $j = 1;
                    $contador = 0;
                    if($total_paginas > 10 && $pagina > 0 && $pagina > 5) {
                        $pagina2 = $pagina + 5;
                        if($pagina2 <= $total_paginas){
                            $j = $pagina - 4;
                        }else{
                            $j = $pagina - 4;
                            $j = $j - ($pagina2 - $total_paginas);
                        }
                    }else{
                        $j = 1;
                    }
                    for($i=$j; $i <= $total_paginas && $contador < 10; $i++){
                    ?>
                    <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo base_url()."index.php/novios/comunidad/Group/validarMiembros/$id_grupo/$token[0]/$token[1]/$token[2]/$token[3]/$token[4]/$i" ?>"><?php echo $i ?></a></li>
                    <?php $contador++; } ?>
                    <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a <?php if($pagina < $total_paginas) echo 'href="'.base_url().'index.php/novios/comunidad/Group/validarMiembros/'.$id_grupo.'/'.$token[0].'/'.$token[1].'/'.$token[2].'/'.$token[3].'/'.$token[4].'/'.($pagina + 1).'"'?>><i class="material-icons">chevron_right</i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php }else if(!empty($total_paginas) && $total_paginas > 1 && $tipo == "buscarMiembro"){ ?>
    <div class="row">
        <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
            <div class="col s12 m6" style="float: right">
                <ul class="pagination">
                    <?php $token = explode("-", $ordenamiento); ?>
                    <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?> buscar_miembro" data-id="<?php echo ($pagina - 1) ?>"><a class="clickable"><i class="material-icons">chevron_left</i></a></li>
                    <?php
                    $j = 1;
                    $contador = 0;
                    if($total_paginas > 10 && $pagina > 0 && $pagina > 5) {
                        $pagina2 = $pagina + 5;
                        if($pagina2 <= $total_paginas){
                            $j = $pagina - 4;
                        }else{
                            $j = $pagina - 4;
                            $j = $j - ($pagina2 - $total_paginas);
                        }
                    }else{
                        $j = 1;
                    }
                    for($i=$j; $i <= $total_paginas && $contador < 10; $i++){
                    ?>
                    <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?> buscar_miembro" data-id="<?php echo $i ?> "><a class="clickable"><?php echo $i ?></a></li>
                    <?php $contador++; } ?>
                    <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?> buscar_miembro" data-id="<?php echo ($pagina + 1) ?>"><a class="clickable"><i class="material-icons">chevron_right</i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<!-- Modal Structure -->
<div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Escribe un comentario en el muro</h4>
        <div class="col s12 m12 input-field">
            <textarea id="comentario" class="form-control" rows="20" style="resize: none; height: 50%;"></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-green btn dorado-2 comentar-muro" data-id="">Publicar</a>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $(".datepicker").datepicker({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateText, inst) {
            var fecha = dateText;
            var color = $("#seleccion-color").val();
            var estacion = $("#seleccion-estacion").val();
            var estilo = $("#seleccion-estilo").val();
            var estado = $("#seleccion-estado").val();
            if(color == ""){
                color = "color";
            }
            if(estacion == ""){
                estacion = "temporada";
            }
            if(estilo == ""){
                estilo = "estilo";
            }
            if(estado == ""){
                estado = "estado";
            }
            estado = estado.replace(" ","_");
            fecha = fecha.replace("/","_");
            fecha = fecha.replace("/","_");
            window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/Group/validarMiembros/<?php if(!empty($id_grupo)) echo $id_grupo?>/"+color+"/"+estacion+"/"+estilo+"/"+estado+"/"+fecha;

        }
    });

    $(document).ready(function(){
        $(".buscar_miembro").on('click',function (){
            if($(this).hasClass('waves-effect') || $(this).hasClass('active')){
                $("#pagina").val($(this).attr("data-id"));
                $("form").submit();
            }
        });
    });

    $(function () {
        $('.edit-item').on('click', function () {
            switch ($(this.parentElement).data('name')) {
                case 'color':
                    $("#seleccion-color").val($(this).data("name"));
                    $('.color').text($(this).text());
                    break;
                case 'temporada':
                    $("#seleccion-estacion").val($(this).data("name"));
                    $('.temporada').text($(this).text());
                    break;
                case 'hora':
                    $("#seleccion-estilo").val($(this).data("name"));
                    $('.estilo').text($(this).text());
                    break;
                case 'estado':
                    $("#seleccion-estado").val($(this).data("name"));
                    $(".estado").text($(this).text());
                    break;
            }
            var color = $("#seleccion-color").val();
            var estacion = $("#seleccion-estacion").val();
            var estilo = $("#seleccion-estilo").val();
            var estado = $("#seleccion-estado").val();
            var fecha = $("#fecha_boda").val();
            if(color == ""){
                color = "color";
            }
            if(estacion == ""){
                estacion = "temporada";
            }
            if(estilo == ""){
                estilo = "estilo";
            }
            if(estado == ""){
                estado = "estado";
            }
            if(fecha == "Fecha Boda" || fecha == ""){
                fecha = "fecha_boda";
            }
            estado = estado.replace(" ","_");
            fecha = fecha.replace("/","_");
            fecha = fecha.replace("/","_");
            window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/Group/validarMiembros/<?php if(!empty($id_grupo)) echo $id_grupo?>/"+color+"/"+estacion+"/"+estilo+"/"+estado+"/"+fecha;
        });
    });

    $("#fecha_boda").keyup(function (){
        return false;
    });
    $("#fecha_boda").click(function(){
        $(this).blur();
    });

    $(document).ready(function(){
            $('.botones_tarjeta').on('click', function(){
                var click = $(this).attr('data-id');
                var boton = click.split("-");
                if(boton[0] == "participacion"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "misdebates"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "fotos"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/fotos/"?>"+boton[1];
                }else if(boton[0] == "amigos"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/amigos/"?>"+boton[1];
                }
            });
        });

        $(document).ready(function(){
            $('.modal-trigger').modal({
                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                in_duration: 300, // Transition in duration
                out_duration: 200, // Transition out duration
                starting_top: '4%', // Starting top style attribute
                ending_top: '10%', // Ending top style attribute
                }
            );
            $(".comentar").on("click", function(){
                var name = $(this).attr("data-name");
                var token = name.split("-");
                $(".modal-action").attr("data-id",token[1]);
            });
        });

        $(document).ready(function (){
            $(".comentar-muro").on("click", function(){
                var comentario = $("#comentario").val();
                var usuario = $(this).attr("data-id");
                $.ajax({
                    url: '<?php echo base_url()."index.php/novios/comunidad/perfil/comentario"?>',
                    method: 'POST',
                    data:{
                        'comentario': comentario,
                        'muro_usuario': usuario
                    },
                    success: function(res){
                        if(res.success){
                            setTimeout(function(){
                                alert("Se ha publicado con exito");
                                $("#comentario").val("");
                            },500);
                        }
                    },
                    error: function(){
                        setTimeout(function(){
                            alert("Lo sentimos ocurrio un error");
                        },500);
                    }
                });
            });
        });
</script>
