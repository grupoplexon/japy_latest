<div class="col s12 m9 pull-left">
    <div class="row">
        <h5>Nuevos Miembros</h5>
    </div>
</div>
<div class="col s12 m9 pull-left">
    <?php 
        $i = 1;
        if(!empty($nuevos_integrantes)){
            foreach ($nuevos_integrantes as $integrante){
                if($i % 2 != 0){
                    echo '<div class="row">';
                }
                echo '<div class="col s12 m6">
                        <div class="tarjeta-perfil z-depth-1">
                            <div class="row">
                                <div class="col s3 m4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img class="foto_perfil" src="'.$integrante->url_foto_usuario.'">
                                        </div>
                                    </div>
                                </div>
                                <div class="col s6 m5">
                                    <p class="truncate"><a class="titulo_tarjeta" href="'.$integrante->url_usuario.'">'.$integrante->usuario.'</a></p>
                                </div>
                                <div class="col s3 m3">
                                    <p class="dorado-2-text">'.$integrante->puntos.'</p>
                                </div>
                                <div class="col s9 m8">
                                    <p>'.$integrante->lugar_boda.'</p>
                                    <p>'.$integrante->fecha_boda.'</p>
                                    <p>'.$integrante->fecha_creacion.'</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$integrante->url_debates_participacion.'"><p class="truncate"><text class="numeros">'.$integrante->num_mensajes.'</text> Mensajes</p></div>
                                <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$integrante->url_debates.'"><p><text class="numeros">'.$integrante->num_debates.'</text> Post</p></div>
                                <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$integrante->url_fotos.'"><p><text class="numeros">'.$integrante->num_fotos.'</text> Fotos<p></div>
                                <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$integrante->url_amigos.'"><p><text class="numeros">'.$integrante->num_amigos.'</text> Amigos</p></div>
                            </div>
                            <div class="col s6 m6 pull-left" style="margin-bottom: 10px;">';
                if(!empty($integrante->estado_usuario) && $integrante->id_usuario != $this->session->userdata('id_usuario')){
                    echo    '<p style="text-align: center"><a class="btn_agregar clickable" style="color: #999" data-id="'.$integrante->url_agregar_amigo.'"><i class="fa fa-user-plus" aria-hidden="true"></i> <text class="agregar-'.$integrante->id_usuario.'">'.$integrante->estado_usuario.'</text></a><p>';
                }            
                echo        '</div>
                            <div class="col s6 m6" style="margin-bottom: 10px;">
                                <p style="text-align: center"><a href="#modal1" class="modal-trigger comentar" style="color: #999" data-name="comentar-'.$integrante->id_usuario.'"><i class="fa fa-comments" aria-hidden="true"></i> Poner Comentario</a></p>
                            </div>
                        </div>
                    </div>';
                if($i % 2 == 0){
                    echo "</div>";
                }
                $i++;
            }
        }
    ?>
</div>
<div class="row">
    <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
        <div class="col s12 m6" style="float: right">
            <ul class="pagination">
                <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a <?php if($pagina > 1) echo 'href="'.base_url()."index.php/novios/comunidad/Group/grupo/$id_grupo/$seccion/1/".($pagina - 1).'"' ?>><i class="material-icons">chevron_left</i></a></li>
                <?php 
                $j = 1;
                $contador = 0;
                if($total_paginas > 10 && $pagina > 0 && $pagina > 5) { 
                    $pagina2 = $pagina + 5;
                    if($pagina2 <= $total_paginas){
                        $j = $pagina - 4;
                    }else{
                        $j = $pagina - 4;
                        $j = $j - ($pagina2 - $total_paginas);
                    }
                }else{
                    $j = 1;
                }
                for($i=$j; $i <= $total_paginas && $contador < 10; $i++){
                ?>
                <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo base_url()."index.php/novios/comunidad/Group/grupo/$id_grupo/$seccion/1/$i" ?>"><?php echo $i ?></a></li>
                <?php $contador++; } ?>
                <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a <?php if($pagina < $total_paginas) echo 'href="'.base_url()."index.php/novios/comunidad/Group/grupo/$id_grupo/$seccion/1/".($pagina + 1).'"' ?>><i class="material-icons">chevron_right</i></a></li>
            </ul>
        </div>
    </div>
</div>

<!-- Modal Structure -->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Escribe un comentario en el muro</h4>
        <div class="col s12 m12 input-field">
            <textarea id="comentario" class="form-control" rows="20" style="resize: none; height: 50%;"></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-green btn dorado-2 comentar-muro" data-id="">Publicar</a>
    </div>
  </div>
<script>
    $(document).ready(function(){
        $('.btn_agregar').on("click", function(){
            var usuario = $(this).attr("data-id");
            var token = usuario.split('-');
            var tipo = $("."+usuario).text();
            $.ajax({
                url: '<?php echo base_url() ?>index.php/novios/comunidad/perfil/solicitudAmistad',
                method: 'POST',
                data:{
                    'id_usuario_confirmacion': token[1],
                    'tipo': tipo
                },
                success: function(res){
                    if(res.success){
                        $("."+usuario).text(res.data);
                    }
                },
                error: function(){
                    alert("Lo sentimos ocurrio un error");
                }
            });
        });
    });
    
    $(document).ready(function(){
            $('.botones_tarjeta').on('click', function(){
                var click = $(this).attr('data-id');
                var boton = click.split("-");
                if(boton[0] == "participacion"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "misdebates"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "fotos"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/fotos/"?>"+boton[1];
                }else if(boton[0] == "amigos"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/amigos/"?>"+boton[1];
                }
            });
        });
        
        $(document).ready(function(){
            $('.modal-trigger').modal({
                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                in_duration: 300, // Transition in duration
                out_duration: 200, // Transition out duration
                starting_top: '4%', // Starting top style attribute
                ending_top: '10%', // Ending top style attribute
                }
            );
            $(".comentar").on("click", function(){
                var name = $(this).attr("data-name");
                var token = name.split("-");
                $(".modal-action").attr("data-id",token[1]);
            });
        });
        
        $(document).ready(function (){
            $(".comentar-muro").on("click", function(){
                var comentario = $("#comentario").val();
                var usuario = $(this).attr("data-id");
                $.ajax({
                    url: '<?php echo base_url()."index.php/novios/comunidad/perfil/comentario"?>',
                    method: 'POST',
                    data:{
                        'comentario': comentario,
                        'muro_usuario': usuario 
                    },
                    success: function(res){
                        if(res.success){
                            setTimeout(function(){
                                alert("Se ha publicado con exito");
                                $("#comentario").val("");
                            },500);
                        }
                    },
                    error: function(){
                        setTimeout(function(){
                            alert("Lo sentimos ocurrio un error");
                        },500);
                    }
                });
            });
        });
</script>
    