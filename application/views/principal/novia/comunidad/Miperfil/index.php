<?php $this->view('principal/header');?>
<?php $this->view('principal/novia/menu');?>
<link href="<?php echo base_url()?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<body>
    <!-- BARRA DE TITULO CON UN BUSCADOR DE DEBATES -->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
            <p>
            <div class="col s12 m2">
                <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            </div>
            <form id="buscador" method="get" action="<?php echo base_url()."index.php/novios/comunidad/forum/buscar"?>" autocomplete="off">
                <div class="input-field col s7 m5" style="margin:0">
                    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
                    <input type="hidden" name="page" value="1"/>
                    <div id="ventana_buscador">
                    </div>
                </div>
                <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
                    <button type="submit" class="btn waves-effect waves-light dorado-2"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                </div>
            </form>
        </p>
        </div>
    </div>
    <!-- FIN BARRA DE TITULO CON UN BUSCADOR DE DEBATES -->
    
    <div class="body-container">
        <div class="row">
            <!--COMPONENTES LATERALES-->
            <?php 
                if(empty($usuarios_boda)){
                    $usuarios_boda = "";
                }
                if(empty($visitas_perfil)){
                    $visitas_perfil = "";
                }
                if(empty($grupos_miembro)){
                    $grupos_miembro = "";
                }
                if(empty($grupos)){
                    $grupos = "";
                }
                $data = array(
                    'usuarios_boda' => $usuarios_boda,
                    'visitas_perfil' => $visitas_perfil,
                    'grupos_miembro' => $grupos_miembro,
                    'grupos' => $grupos
                ); 
                $this->view('principal/novia/comunidad/lateral.php',$data);
            ?>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1" style="margin-top: 10px">
                        <div class="row">
                            <div class="col s4 m2"> 
                                <div class="card">
                                    <div class="card-image">
                                        <?php
                                            if(!empty($datos_usuario->mime)){
                                                echo '<img src="'.base_url().'index.php/novios/comunidad/Home/foto_usuario/'.$datos_usuario->id_usuario.'">';
                                                $foto_usuario = base_url().'index.php/novios/comunidad/Home/foto_usuario/'.$datos_usuario->id_usuario; 
                                            }else{
                                                echo '<img src="'.base_url().'dist/img/blog/perfil.png">';
                                                $foto_usuario = base_url().'dist/img/blog/perfil.png';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col s8 m10" style="margin-bottom: 20px">
                                <div style="margin-bottom: 10px">
                                    <div class="col s9 m9" style="font-weight: bold"><p><?php if(!empty($datos_usuario->usuario)) echo $datos_usuario->usuario ?></p></div>   
                                    <div class="col s3 m3 dorado-2-text"><p style="text-align: center"><?php if(!empty($datos_usuario->puntos)) echo "$datos_usuario->puntos Puntos" ?></p></div>
                                </div>
                                <?php if(!empty($datos_usuario->poblacion) && !empty($datos_usuario->estado)) {?>
                                <div class="col s12 m12"><p><i class='fa fa-map-marker' aria-hidden='true'></i> <?php
                                    if(!empty($datos_usuario->poblacion) && !empty($datos_usuario->estado)) echo "$datos_usuario->poblacion, $datos_usuario->estado";
                                ?></p></div><?php } ?>
                                <div class="col s12 m12"><p class="link_usuario"><?php echo !empty($datos_usuario->fecha_creacion)? $datos_usuario->fecha_creacion : ""?></p></div>
                                <div class="col s12 m12" style="text-align: justify; margin-bottom: 15px">
                                    <p>
                                    <?php
                                        if(!empty($datos_usuario)){
                                            echo "$datos_usuario->sobre_mi";
                                        }
                                    ?>
                                    </p>
                                </div>
                                <div class="col s12 m6" style="margin-bottom: 10px">
                                    <a class="btn waves-effect waves-light dorado-2" href="<?php echo base_url()?>index.php/novios/perfil">
                                        <i class="fa fa-pencil" aria-hidden="true"></i> Editar <!-- Me redirecciona a la parte de modificar mi perfil-->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 white lighten-5 z-depth-1">
                        <ul class="tabs filtro1">
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "perfil") echo 'class="active"'?> href="#">Mi Perfil</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "muro") echo 'class="active"'?> href="#">Mi Muro</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "debates") echo 'class="active"'?> href="#">Debates</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "amigos") echo 'class="active"'?> href="#">Amigos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "fotos") echo 'class="active"'?> href="#">Fotos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "videos") echo 'class="active"'?> href="#">Videos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "visitas") echo 'class="active"'?> href="#">Visitas</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "actividad") echo 'class="active"'?> href="#">Actividad</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "proveedores") echo 'class="active"'?> href="#">Proveedores</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> 
            <div class="col s12 m9 pull-left" style="display: <?php echo empty($datos_usuario->sobre_mi)? "block" : "none" ?>">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1">
                        <p>A&uacute;n no te has presentado con la cumunidad rompe el hielo y agrega una presentaci&oacute;n.</p>
                        <a class="btn waves-effect waves-light dorado-2" href="<?php echo base_url()."index.php/novios/perfil"?>" style="margin-bottom: 20px">Agrega una presentaci&oacute;n</a>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left">
                <h5>Sobre Mi Boda</h5>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1">
                        <div class="col s12 m12" style="border-bottom: 1px solid #999" style="display: <?php echo !empty($datos_usuario->sobre_boda)? "true" : "false"?>">
                            <p><?php if(!empty($datos_usuario->sobre_boda)) echo $datos_usuario->sobre_boda ?></p>
                        </div>
                        <div class="col s4 m4">
                            <div class="col s12 m12" style="text-align: center">
                                <p>
                                    <div id="color-boda" class="principal clickable circle-img <?php echo empty($datos_usuario->color) ? '' : $datos_usuario->color; ?>" style="<?php echo empty($datos_usuario->color) ? 'background-image:url(' . base_url() . 'dist/img/agenda_novia/interrogacion.png); background-size:100%;' : ''; ?>"></div>
                                </p>    
                            </div>
                            <div class="col s12 m12" style="text-align: center">
                                <p><?php echo !empty($datos_usuario->color2)? $datos_usuario->color2 : ""?></p>
                            </div>
                            <div class="col s12 m12" style="text-align: center">
                                <p>Color de mi boda</p>
                            </div>
                        </div>
                        <div class="col s4 m4">
                            <div class="col s12 m12" style="text-align: center">
                                <p>
                                    <img id="temporada-boda" class="circle-img clickable" src="<?php echo empty($datos_usuario->estacion) ? base_url() . 'dist/img/agenda_novia/interrogacion.png' : base_url() . 'dist/img/agenda_novia/temporada/' . $datos_usuario->estacion . '.png' ?>" alt=""/>
                                </p>
                            </div>
                            <div class="col s12 m12" style="text-align: center">
                                <p><?php echo !empty($datos_usuario->estacion2)? $datos_usuario->estacion2 : ""?></p>
                            </div>
                            <div class="col s12 m12" style="text-align: center">
                                <p>Temporada de mi boda</p>
                            </div>
                        </div>   
                        <div class="col s4 m4">
                            <div class="col s12 m12" style="text-align: center">
                                <p>
                                    <img id="hora-boda" class="circle-img clickable" src="<?php echo empty($datos_usuario->estilo) ? base_url() . 'dist/img/agenda_novia/interrogacion.png' : base_url() . 'dist/img/agenda_novia/estilo/' . $datos_usuario->estilo . '.png' ?>" alt=""/>
                                </p>
                            </div>
                            <div class="col s12 m12" style="text-align: center">
                                <p><?php echo !empty($datos_usuario->estilo2)? $datos_usuario->estilo2 : ""?></p>
                            </div>
                            <div class="col s12 m12" style="text-align: center">
                                <p>Estilo de mi boda</p>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($debates)? "block": "none"?>">
                <h5>Debates en los que participo</h5>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1" style="text-align: center">
                        <p style="font-size: 40px"><i class="fa fa-pencil-square" aria-hidden="true"></i></p>
                        <p style="font-weight: bold; font-size: 20px">Participa en la publicacion de un debate</p>
                        <p>La planeaci&oacute;n de una boda no es fac&iacute;l, si quieres ayuda para la realizaci&oacute;n de tu boda, publica un debate y recibe la ayuda de los miembros de la comunidad de clubnupcial.</p>
                        <a href="<?php echo base_url()."index.php/novios/comunidad/home/nuevoDebate"?>" class="btn waves-effect waves-light dorado-2" style="margin-bottom: 20px">Publicar un debate</a>
                    </div>
                </div>
            </div>
            <div class="col s12 m9" style=" <?php if(empty($debates)) echo 'display: none' ?>">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1" style=" <?php if(empty($debates)) echo 'display: none' ?>">
                    <?php 
                    if(!empty($debates)){
                        $j = count($debates) - 1;
                        $i = 0;
                        foreach ($debates as $debate){
                            if($i <= $j){
                                echo '  <div class="col s12 m12" style="border-bottom: 1px solid #999; padding-top: 10px">
                                        <div class="col s3 m2">
                                            <div class="card">
                                                <div class="card-imagear">
                                                    <img class="perfil" src="'.$debate->url_foto.'">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s6 m8 contenido_debates" style="border-right: 1px solid #999; text-align:justify">
                                            <p style="font-size:18px"><a class="titulo_foto" href="'.base_url().'/index.php/novios/comunidad/Home/debatePublicado/'.$debate->id_debate.'">'.$debate->titulo_debate.'</a></p>
                                            <p class="clickable truncate">Creado Por <a class="titulo_foto" href="'.$debate->url_usuario.'">'.$debate->usuario.'</a> '.$debate->fecha_creacion.'</p>
                                            <p>'.$debate->debate.'</p>
                                        </div>
                                        <div class="col s3 m2" style="text-align:center">
                                            <div class="col s12 m12" style="text-align:center"> 
                                                <p><i class="fa fa-comments" aria-hidden="true"></i> '.$debate->num_comentarios.'</p>
                                            </div>
                                            <div class="col s12 m12" style="text-align:center"> 
                                                <p><i class="fa fa-eye" aria-hidden="true"></i> '.$debate->vistas.'</p>
                                            </div>
                                        </div>
                                        </div>';
                            }
                        }
                        echo    '<div class="col s12 m12" style="text-align:right">
                                    <p>
                                        <a href="'.base_url().'/index.php/novios/comunidad/perfil/debates/'.$datos_usuario->id_usuario.'" style="color:#999">Ver Debates <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                    </p>
                                </div>';
                    }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left">
                <h5>&Uacute;ltimas Fotos Publicadas</h5>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s6 m6">
                        <div class="card">
                            <div class="card-content" style="text-align: justify">
                                <span class="card-title">Agrega Imagenes</span>
                                <p>Todo sobre tus preparativos puedes compartirlo por medio de imagenes.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m6">
                        <div class="card">
                            <div id="foto" class="upload card-image z-depth-0 valign-wrapper clickable subir_foto" style="font-size: 17px">
                                <p class="texto" style="width: 100%; text-align: center;color: gray;"><i class="fa fa-upload fa-2x valign "></i><br>
                                Haga clic aqu&iacute; para subir una foto.
                                </p>
                            </div>
                            <div class="card-action">
                                <button type="button" class="btn dorado-2 subir_foto" data-id="datos_perfil" style="width:100%;">Subir Foto</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($fotos)? "block": "none"?>">
                <div class="row">
                    <?php
                    if(!empty($fotos)){
                        foreach ($fotos as $foto){
                            echo '
                            <div class="col s6 m3 foto_publicada">
                                <div class="card small">
                                    <div class="card-image">
                                        <a href="'.base_url().'index.php/novios/comunidad/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'"> <img class="foto_zoom" src="'.base_url().'index.php/novios/comunidad/picture/foto/'.$foto->id_foto.'"></a>
                                    </div>
                                    <div class="card-content" style="text-align: center">
                                        <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                            <img alt="" src="'.$foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                                        </div>
                                        <p class="clickable truncate"><a class="titulo_foto" href="'.base_url().'index.php/novios/comunidad/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'">'.$foto->titulo.'</a></p><br/>
                                        <p><a class="link_usuario">'.$foto->usuario.'</a></p>
                                    </div>
                                </div>
                            </div>';
                        }
                        echo '
                            <div class="col s6 m3 foto_publicada">
                                <div class="card small fotos_recientes fotos clickable">
                                    <div class="card-image" style="text-align: center;">
                                        <img style="height:200px" src="'.base_url().'dist/images/comunidad/Fotografia.png">
                                    </div>
                                    <div class="card-content" style="text-align: center">
                                        <p><button type="button" class="btn dorado-2 clickable fotos_recientes">M&aacute;s fotos</button></p>
                                    </div>
                                </div>
                            </div>';
                    }
                    ?>
                </div>
            </div>
            <div class="col s12 m9 pull-left">
                <h5>&Uacute;ltimos Videos Publicados</h5>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s6 m6">
                        <div class="card">
                            <div class="card-content" style="text-align: justify">
                                <span class="card-title">Agrega Videos</span>
                                <p>Publica tus videos favoritos de bodas y compartelo con la cumunidad.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col s6 m6">
                        <div class="card">
                            <div id="foto" class="upload card-image z-depth-0 valign-wrapper clickable subir_video" style="font-size: 17px">
                                <p class="texto" style="width: 100%; text-align: center;color: gray;"><i class="fa fa-upload fa-2x valign "></i><br>
                                Haga clic aqu&iacute; para subir un video.
                                </p>
                            </div>
                            <div class="card-action">
                                <button type="button" class="btn dorado-2 subir_video" data-id="datos_perfil" style="width:100%;">Subir Video</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($videos)? "block": "none"?>">
                <?php
                    if(!empty($videos)){
                        foreach ($videos as $video){
                            $direccion_img = str_replace("https://www.youtube.com", "https://i.ytimg.com/", $video->direccion_web);
                            $direccion_img = str_replace("embed", "vi", $direccion_img);
                            $direccion_img = $direccion_img."/default.jpg";
                            echo '
                            <div class="col s6 m3 video_publicado">
                                <div class="card small">
                                    <div class="card-image">
                                        <a href="'.base_url().'index.php/novios/comunidad/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'"> <img class="video_zoom" src="'.$direccion_img.'"></a>
                                    </div>
                                    <div class="card-content" style="text-align: center">
                                        <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                            <img alt="" src="'.$foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                                        </div>
                                        <p class="clickable truncate"><a class="titulo_video" href="'.base_url().'index.php/novios/comunidad/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'">'.$video->titulo.'</a></p><br/>
                                        <p><a class="link_usuario">'.$video->usuario.'</a></p>
                                    </div>
                                </div>
                            </div>';
                        }
                        echo '
                        <div class="col s6 m3 video_publicado">
                            <div class="card small videos_recientes fotos clickable">
                                <div class="card-image" style="text-align: center;">
                                    <img style="height:200px" src="'.base_url().'dist/images/comunidad/Video.png">
                                </div>
                                <div class="card-content" style="text-align: center">
                                    <p><button type="button" class="btn dorado-2 clickable videos_recientes">M&aacute;s videos</button></p>
                                </div>
                            </div>
                        </div>';
                    }
                ?>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($ultimas_actividades)? "block": "none"?>">
                <h5>&Uacute;ltima Actividad</h5>
            </div>
            <div class="col s12 m9 pull-left">
                <?php if(!empty($ultimas_actividades)){
                        foreach ($ultimas_actividades as $actividad){
                ?>
                <div class='row' style="display: <?php echo !empty($ultimas_actividades)? "block" : "none"?>">
                    <div class='col s12 m12 grey lighten-5 z-depth-1'>
                        <div class='col s3 m2' style="display:<?php echo !empty($actividad->url_imagen)? "block" : "none"?>">
                            <div class='card'>
                                <div class='card-image'>
                                    <img class='perfil' src='<?php echo !empty($actividad->url_imagen)? $actividad->url_imagen : "" ?>'>
                                </div>
                            </div>
                        </div>
                        <div class='col s9 m10'>
                            <p style="font-weight: bold"> <?php echo !empty($actividad->participacion)? $actividad->participacion : "" ?> 
                                <a href="<?php echo !empty($actividad->url_titulo)? $actividad->url_titulo : ""?>" class="titulo_foto"><?php echo !empty($actividad->titulo)? $actividad->titulo : "" ?></a></p>
                            <p style="display:<?php echo !empty($actividad->contenido)? "block" : "none"?>; text-align: justify"> <?php echo !empty($actividad->contenido)? $actividad->contenido : ""?></p>
                            <p class="link_usuario"><?php echo !empty($actividad->fecha_creacion)? $actividad->fecha_creacion : "" ?></p>
                        </div>
                    </div>
                </div>
                <?php }}?>
                <div class="row" style="display: <?php echo !empty($ultimas_actividades)? "block": "none"?>">
                    <p style="text-align: center">
                        <a href="<?php echo !empty($datos_usuario->id_usuario)? base_url()."index.php/novios/comunidad/perfil/actividad/$datos_usuario->id_usuario" : ""?>" class="btn waves-effect waves-light dorado-2">Ver M&aacute;s Actividades</a>
                    </p>
                </div>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($amigos)? "block": "none"?>">
                <h5>Mis Amigos</h5>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($amigos)? "block": "none"?>">
                <?php 
                    $i = 1;
                    if(!empty($amigos)){
                        foreach ($amigos as $amigo){
                            if($i % 2 != 0){
                                echo '<div class="row">';
                            }
                            echo '<div class="col s12 m6">
                                    <div class="tarjeta-perfil z-depth-1">
                                        <div class="row">
                                            <div class="col s3 m4">
                                                <div class="card">
                                                    <div class="card-image">
                                                        <img class="foto_perfil" src="'.$amigo->url_foto_usuario.'">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s9 m8">
                                                <p class="truncate"><a class="titulo_tarjeta" href="'.$amigo->url_usuario.'">'.$amigo->usuario.'</a></p>
                                            </div>
                                            <div class="col s9 m8">
                                                <p>'.$amigo->lugar.'</p>
                                                <p>'.$amigo->fecha_creacion.'</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$amigo->url_debates_participacion.'"><p class="truncate"><text class="numeros">'.$amigo->mensajes.'</text> Mensajes</p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$amigo->url_debates.'"><p><text class="numeros">'.$amigo->debates.'</text> Post</p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$amigo->url_fotos.'"><p><text class="numeros">'.$amigo->fotos.'</text> Fotos<p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$amigo->url_amigos.'"><p><text class="numeros">'.$amigo->amigos.'</text> Amigos</p></div>
                                        </div>
                                        <div class="col s6 m6 pull-left" style="margin-bottom: 10px;">';
                            if($this->session->userdata('id_usuario') != $amigo->id_usuario){
                                echo    '<p style="text-align: center"><a class="btn_agregar clickable" style="color: #999" data-id="'.$amigo->url_agregar_amigo.'"><i class="fa fa-user-plus" aria-hidden="true"></i> <text class="agregar-'.$amigo->id_usuario.'">'.$amigo->agregar.'</text></a><p>';
                            }            
                            echo        '</div>
                                        <div class="col s6 m6" style="margin-bottom: 10px;">
                                            <p style="text-align: center"><a href="#modal1" class="modal-trigger comentar" style="color: #999" data-name="comentar-'.$amigo->id_usuario.'"><i class="fa fa-comments" aria-hidden="true"></i> Poner Comentario</a></p>
                                        </div>
                                    </div>
                                </div>';
                            if($i % 2 == 0){
                                echo "</div>";
                            }
                            $i++;
                        }
                        if(count($amigos) % 2 != 0){
                            echo "</div>";
                        }
                    }
                ?>
                
            </div>
            <div class="col s12 m9 pull-left">
                <h5>Mi Muro</h5>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1">
                        <div class="col s12 m9" style="margin-top: 15px; margin-bottom: 15px">
                            <textarea id="contenido_comentario" class="form-control comentario_muro" placeholder="Deja un comentario en el muro..." rows="30" style="resize: none"></textarea>
                        </div>
                        <div class="col s12 m3">
                            <a id="publicar" class="clickable comentar_muro btn waves-effect waves-light dorado-2" style="margin: 0; margin-top: 15px; margin-bottom: 15px">Publicar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="panel_comentarios" class="col s12 m9 pull-left" style="display: <?php echo !empty($comentarios)? "block": "none"?>">
                    <?php
                    if(!empty($comentarios)){
                        foreach ($comentarios as $comentario){
                            echo    "<div class='row'>
                                        <div class='col s12 m12'>
                                            <div class='col s3 m2' style='margin-top: -8px'>
                                                <div class='card'>
                                                    <div class='card-image'>
                                                        <img class='perfil' src='$comentario->foto_usuario'>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col s9 m10 grey lighten-5 z-depth-1'>
                                                <p><a class='titulo_foto' href='$comentario->url_usuario'>$comentario->usuario</a></p>
                                                <p>$comentario->comentario</p>
                                                <p class='link_usuario'>$comentario->fecha_creacion</p>
                                            </div>
                                        </div>    
                                    </div>
                                    ";
                        }
                    }
                    ?>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row" style="display: <?php echo !empty($comentarios)? "block": "none"?>">
                    <p style="text-align: center">
                        <a href="<?php echo !empty($datos_usuario->id_usuario)? base_url()."index.php/novios/comunidad/perfil/mi_muro/$datos_usuario->id_usuario" : ""?>" class="btn waves-effect waves-light dorado-2">Todos los comentarios</a>
                    </p>
                </div>
            </div>
            <!-- ---------------   TEMPLATE NUEVO COMENTARIO ------------------------- -->
            <div class="hide" id="template-comentario">
                <div class='row'>
                    <div class='col s12 m12'>
                        <div class='col s3 m2' style='margin-top: -8px'>
                            <div class='card'>
                                <div class='card-image'>
                                    <img class='perfil' src=''>
                                </div>
                            </div>
                        </div>
                        <div class='col s9 m10 grey lighten-5 z-depth-1'>
                            <p><a class='titulo_foto nombre_usuario' href=''></a></p>
                            <p class="comentario"></p>
                            <p class='link_usuario'></p>
                        </div>
                    </div>    
                </div>  
            </div>
            <!-- ---------------   TEMPLATE NUEVO COMENTARIO ------------------------- -->
            
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Escribe un comentario en el muro</h4>
            <div class="col s12 m12 input-field">
                <textarea id="comentario" class="form-control" rows="20" style="resize: none; height: 50%;"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn dorado-2 comentar-muro" data-id="">Publicar</a>
        </div>
    </div>
    <!-- -----------------  TEMPLATE VENTANA BUSCADOR -------------------------- -->
    <ul id="lista_debates">
        <li id="item_debate">
            <a class="enlace_debate" href="">
                <div class="row">
                    <div class="col s4 m3">
                        <div class="card">
                            <div class="card-image">
                                <img class="imagen_debate">
                            </div>
                        </div>
                    </div>
                    <div class="col s9 m9">
                        <p class="titulo_debate"></p>
                        <p class="fecha_debate"></p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <!-- ------------------  FIN TAMPLETE VENTANA BUSCADOR --------------------- -->
    <script>
        $(document).ready(function(){
            $('ul.tabs').tabs();
        });
        
        $(document).ready(function(){
            $('.filtro1 a').on("click", function (){
                var opcion = $(this).text();
                switch(opcion){
                    case "Mi Perfil":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/usuario/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Mi Muro":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/mi_muro/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Debates":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/debates/participacion/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Amigos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/amigos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Fotos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/fotos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Videos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/videos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Actividad":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/actividad/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Visitas":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/visitas/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Proveedores":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/proveedores/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                }
            }); 
        });
        
        $(document).ready(function(){
            $('#color-boda').click(function(){
                var  ordenamiento = "<?php echo !empty($datos_usuario->color)? $datos_usuario->color: "color"?>-temporada-estilo-estado-fecha_boda";
                window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/Group/grupo/1/miembros/"+ordenamiento;
            });
            $('#temporada-boda').click(function(){
                var  ordenamiento = "color-<?php echo !empty($datos_usuario->estacion)? $datos_usuario->estacion: "temporada"?>-estilo-estado-fecha_boda";
                window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/Group/grupo/1/miembros/"+ordenamiento;
            });
            $('#hora-boda').click(function (){
                var  ordenamiento = "color-temporada-<?php echo !empty($datos_usuario->estilo)? $datos_usuario->estilo: "estilo"?>-estado-fecha_boda";
                window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/Group/grupo/1/miembros/"+ordenamiento;
            });
        });
        
        $(document).ready(function (){
            $(".fotos_recientes").click(function (){
                window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/fotos/<?php if(!empty($datos_usuario->id_usuario)) echo $datos_usuario->id_usuario?>";
            });
            $(".videos_recientes").click(function (){
                window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/videos/<?php if(!empty($datos_usuario->id_usuario)) echo $datos_usuario->id_usuario?>";
            });
        });
        
        $(document).ready(function(){
            $('.modal-trigger').modal({
                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                in_duration: 300, // Transition in duration
                out_duration: 200, // Transition out duration
                starting_top: '4%', // Starting top style attribute
                ending_top: '10%', // Ending top style attribute
                }
            );
            $(".comentar").on("click", function(){
                var name = $(this).attr("data-name");
                var token = name.split("-");
                $(".modal-action").attr("data-id",token[1]);
            });
        });
    
        $(document).ready(function (){
            $(".comentar-muro").on("click", function(){
                var comentario = $("#comentario").val();
                var usuario = $(this).attr("data-id");
                $.ajax({
                    url: '<?php echo base_url()."index.php/novios/comunidad/perfil/comentario"?>',
                    method: 'POST',
                    data:{
                        'comentario': comentario,
                        'muro_usuario': usuario 
                    },
                    success: function(res){
                        if(res.success){
                            setTimeout(function(){
                                alert("Se ha publicado con exito");
                                $("#comentario").val("");
                            },500);
                        }
                    },
                    error: function(){
                        setTimeout(function(){
                            alert("Lo sentimos ocurrio un error");
                        },500);
                    }
                });
            });
        });
        
        $(document).ready(function(){
            $('.botones_tarjeta').on('click', function(){
                var click = $(this).attr('data-id');
                var boton = click.split("-");
                if(boton[0] == "participacion"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "misdebates"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "fotos"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/fotos/"?>"+boton[1];
                }else if(boton[0] == "amigos"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/amigos/"?>"+boton[1];
                }
            });
        });
        
        $(document).ready(function(){
            $('#publicar').click(function(){
                var comentario = $('#contenido_comentario').val();
                if(comentario != "" && comentario != null){
                    $.ajax({
                        url: '<?php echo base_url()."index.php/novios/comunidad/perfil/setComentarioMuro"?>',
                        method: 'POST',
                        data:{
                            usuario: '<?php echo $this->session->userdata('id_usuario')?>',
                            muro: '<?php echo !empty($datos_usuario->id_usuario)? $datos_usuario->id_usuario : ""?>',
                            comentario: comentario
                        },
                        success: function(res){
                            if(res.success){
                                console.log(res);
                                var div = document.createElement("div");
                                $(div).html($("#template-comentario").html());
                                $(div).find(".perfil").attr("src", res.data.foto_usuario);
                                $(div).find(".nombre_usuario").attr("href", res.data.url_usaurio);
                                $(div).find(".nombre_usuario").text(res.data.usuario);
                                $(div).find(".comentario").html(res.data.comentario);
                                $(div).find(".link_usuario").text(res.data.fecha_creacion);
                                $("#panel_comentarios").prepend(div);
                                $("#contenido_comentario").val("");
                                $("#panel_comentarios").show();
                            }
                        },
                        error: function(){
                            
                        }
                    });
                }
            });
        });
        
        $(document).ready(function (){
            $(".subir_foto").click(function (){
                window.location.href = "<?php echo base_url()."index.php/novios/comunidad/picture/nuevaFoto"?>";
            });
            
            $(".subir_video").click(function (){
                window.location.href = "<?php echo base_url()."index.php/novios/comunidad/video/nuevoVideo"?>";
            });
        });
        
        $(document).ready(function (){
            $('.desplegar_grupos').click(function (){
                var mostrar = $(this).text();
                var display = $('.oculto').css('display');
                if(display == "none"){
                    $('.oculto').css('display','block');
                    $(this).text("Ocultar Grupos");
                }else{
                    $('.oculto').css('display','none');
                    $(this).text("M&aacute;s Grupos...");
                }
            });
        });
        
    $(document).ready(function (){
        var activo = 0;
        var height = 0;
        var bandera = true;
        
        $("input[name=buscar]").keydown(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(key == 40){
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo++;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo > 1){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height > 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                }else{
                    if(bandera){
                        activo++;
                        bandera = false;
                    }
                    activo--;
                }
            }else if(key == 38){
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo--;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo >= 0){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height >= 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                }else{
                    if(!bandera){
                        activo--;
                        bandera = true;
                    }
                    activo++;
                }
            }else if(key == 13){
                $("#buscador").on('keypress',function(e){
                    e.preventDefault();
                    return false;
                });
                window.location.href = $('.item-'+activo+' a').attr("href");
            }else if(key != 39 && key != 37){
                activo = 0;
                grupos();
            }
        });
        
        $("input[name=buscar]").keyup(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(titulo != "" && key != 38 && key != 40 && key != 39 && key != 37){
                activo = 0;
                debates(titulo);
            }else if(key == 13){
                window.location.href = $('.item-'+activo+' a').attr("href");
            }
        });
        
        $(document).on('mouseenter','#ventana_buscador > ul > li',function(){
            $('.item-'+activo).removeClass("hover");
            var clase = $(this).attr("class");
            var token = clase.split("-");
            activo = token[1];
            $(this).addClass("hover");
        });
       
        $("body").on('click',function(){
            $("#ventana_buscador").hide();
            $('.item-'+activo).removeClass("hover");
        });
        
        $("input[name=buscar]").on('click', function (e){
            e.stopPropagation();
            $('.item-'+activo).removeClass("hover");
            activo = 0;
            var titulo = $("input[name=buscar]").val();
            if(titulo == "" && $("#ventana_buscador ul").html() == undefined){
                grupos();
            }
            $("#ventana_buscador").show();
        });
        
        $("#ventana_buscador").on('mouseleave',function(){
            setTimeout(function(){
                $('.item-'+activo).removeClass("hover");
            },1000);
        });
        
        function grupos(){
            $.ajax({
                url: '<?php echo base_url()."index.php/novios/comunidad/home/getGrupos"?>',
                success: function(res) {
                    var val = Array();
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_grupo);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].imagen);
                                $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                $(li).find(".fecha_debate").text(res.data[aux].debates+" Debates");
                                $(ul).append(li);
                                if(i == 16){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").data("grupos","true");
                        $("#ventana_buscador").show();
                    }
                }          
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
        
        function debates(titulo){
            $.ajax({
                url: '<?php echo base_url()."index.php/novios/comunidad/home/buscar" ?>',
                method: 'post',
                data:{
                    'titulo_debate': titulo
                },
                success: function(res){
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        var val = Array();
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_debate);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].foto_usuario);
                                $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                $(ul).append(li);
                                if(i > (res.data.length - 1)){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").show();
                    }
                }
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
    });
    </script>
</body>
<?php $this->view('principal/footer'); ?>