<?php $this->view('principal/header');?>
<?php $this->view('principal/novia/menu');?>
<link href="<?php echo base_url()?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<body>
    <!-- BARRA DE TITULO CON UN BUSCADOR DE DEBATES -->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
            <p>
            <div class="col s12 m2">
                <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            </div>
            <form id="buscador" method="get" action="<?php echo base_url()."index.php/novios/comunidad/forum/buscar"?>" autocomplete="off">
                <div class="input-field col s7 m5" style="margin:0">
                    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
                    <input type="hidden" name="page" value="1"/>
                    <div id="ventana_buscador">
                    </div>
                </div>
                <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
                    <button type="submit" class="btn waves-effect waves-light dorado-2"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                </div>
            </form>
        </p>
        </div>
    </div>
    <!-- FIN BARRA DE TITULO CON UN BUSCADOR DE DEBATES -->
    
    <div class="body-container">
        <div class="row">
            <!--COMPONENTES LATERALES-->
            <?php 
                if(empty($usuarios_boda)){
                    $usuarios_boda = "";
                }
                if(empty($visitas_perfil)){
                    $visitas_perfil = "";
                }
                if(empty($grupos_miembro)){
                    $grupos_miembro = "";
                }
                if(empty($grupos)){
                    $grupos = "";
                }
                $data = array(
                    'usuarios_boda' => $usuarios_boda,
                    'visitas_perfil' => $visitas_perfil,
                    'grupos_miembro' => $grupos_miembro,
                    'grupos' => $grupos
                ); 
                $this->view('principal/novia/comunidad/lateral.php',$data);
            ?>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1" style="margin-top: 10px">
                        <div class="row">
                            <div class="col s4 m2"> 
                                <div class="card">
                                    <div class="card-image">
                                        <?php
                                            if(!empty($datos_usuario->mime)){
                                                echo '<img src="'.base_url().'index.php/novios/comunidad/Home/foto_usuario/'.$datos_usuario->id_usuario.'">';
                                            }else{
                                                echo '<img src="'.base_url().'dist/img/blog/perfil.png">';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col s8 m10" style="margin-bottom: 20px">
                                <div style="margin-bottom: 10px">
                                    <div class="col s9 m9" style="font-weight: bold"><p><?php if(!empty($datos_usuario->usuario)) echo $datos_usuario->usuario ?></p></div>   
                                    <div class="col s3 m3 dorado-2-text"><p style="text-align: center"><?php if(!empty($datos_usuario->puntos)) echo "$datos_usuario->puntos Puntos" ?></p></div>
                                </div>
                                <div class="col s12 m12"><p><i class='fa fa-map-marker' aria-hidden='true'></i> <?php
                                    if(!empty($datos_usuario->poblacion) && !empty($datos_usuario->estado)) echo "$datos_usuario->poblacion, $datos_usuario->estado";
                                ?></p></div>
                                <div class="col s12 m12"><p class="link_usuario"><?php echo !empty($datos_usuario->fecha_creacion)? $datos_usuario->fecha_creacion : ""?></p></div>
                                <div class="col s12 m12" style="text-align: justify; margin-bottom: 15px">
                                    <p>
                                    <?php
                                        if(!empty($datos_usuario)){
                                            echo "$datos_usuario->sobre_mi";
                                        }
                                    ?>
                                    </p>
                                </div>
                                <div class="col s12 m6" style="margin-bottom: 10px">
                                    <a class="btn waves-effect waves-light dorado-2" href="<?php echo base_url()?>index.php/novios/perfil">
                                        <i class="fa fa-pencil" aria-hidden="true"></i> Editar <!-- Me redirecciona a la parte de modificar mi perfil-->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 white lighten-5 z-depth-1">
                        <ul class="tabs filtro1">
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "perfil") echo 'class="active"'?> href="#">Mi Perfil</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "muro") echo 'class="active"'?> href="#">Mi Muro</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "debates") echo 'class="active"'?> href="#">Debates</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "amigos") echo 'class="active"'?> href="#">Amigos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "fotos") echo 'class="active"'?> href="#">Fotos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "videos") echo 'class="active"'?> href="#">Videos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "visitas") echo 'class="active"'?> href="#">Visitas</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "actividad") echo 'class="active"'?> href="#">Actividad</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "proveedores") echo 'class="active"'?> href="#">Proveedores</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($visitas)? "block": "none"?>">
                <h5>Quienes Visitaron Mi Perfil</h5>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($visitas)? "block": "none"?>">
                <?php 
                    $i = 1;
                    if(!empty($visitas)){
                        foreach ($visitas as $visita){
                            if($i % 2 != 0){
                                echo '<div class="row">';
                            }
                            echo '<div class="col s12 m6">
                                    <div class="tarjeta-perfil z-depth-1">
                                        <div class="row">
                                            <div class="col s3 m4">
                                                <div class="card">
                                                    <div class="card-image">
                                                        <img class="foto_perfil" src="'.$visita->url_foto_usuario.'">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s9 m8">
                                                <p class="truncate"><a class="titulo_tarjeta" href="'.$visita->url_usuario.'">'.$visita->usuario.'</a></p>
                                            </div>
                                            <div class="col s9 m8">
                                                <p>'.$visita->lugar.'</p>
                                                <p>'.$visita->fecha_creacion.'</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$visita->url_debates_participacion.'"><p class="truncate"><text class="numeros">'.$visita->mensajes.'</text> Mensajes</p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$visita->url_debates.'"><p><text class="numeros">'.$visita->debates.'</text> Post</p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$visita->url_fotos.'"><p><text class="numeros">'.$visita->fotos.'</text> Fotos<p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$visita->url_amigos.'"><p><text class="numeros">'.$visita->amigos.'</text> Amigos</p></div>
                                        </div>
                                        <div class="col s6 m6 pull-left" style="margin-bottom: 10px;">';
                            if(!empty($visita->estado_usuario)){
                                echo    '<p style="text-align: center"><a class="btn_agregar clickable" style="color: #999" data-id="'.$visita->url_agregar_amigo.'"><i class="fa fa-user-plus" aria-hidden="true"></i> <text class="agregar-'.$visita->id_usuario.'">'.$visita->estado_usuario.'</text></a><p>';
                            }            
                            echo        '</div>
                                        <div class="col s6 m6" style="margin-bottom: 10px;">
                                            <p style="text-align: center"><a href="#modal1" class="modal-trigger comentar" style="color: #999" data-name="comentar-'.$visita->id_usuario.'"><i class="fa fa-comments" aria-hidden="true"></i> Poner Comentario</a></p>
                                        </div>
                                    </div>
                                </div>';
                            if($i % 2 == 0){
                                echo "</div>";
                            }
                            $i++;
                        }
                        if(count($visitas) % 2 != 0){
                            echo "</div>";
                        }
                    }
                ?>
                
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
                        <div class="col s12 m9" style="float: right">
                            <ul class="pagination">
                                <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a <?php if($pagina > 1) echo 'href="'.base_url()."index.php/novios/comunidad/perfil/debates/$ordenamiento/$datos_usuario->id_usuario/".($pagina - 1).'"' ?>><i class="material-icons">chevron_left</i></a></li>
                                <?php 
                                $j = 1;
                                $contador = 0;
                                if($total_paginas > 10 && $pagina > 0 && $pagina > 5) { 
                                    $pagina2 = $pagina + 5;
                                    if($pagina2 <= $total_paginas){
                                        $j = $pagina - 4;
                                    }else{
                                        $j = $pagina - 4;
                                        $j = $j - ($pagina2 - $total_paginas);
                                    }
                                }else{
                                    $j = 1;
                                }
                                for($i=$j; $i <= $total_paginas && $contador < 10; $i++){
                                ?>
                                <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo base_url()."index.php/novios/comunidad/perfil/debates/$ordenamiento/$datos_usuario->id_usuario/$i" ?>"><?php echo $i ?></a></li>
                                <?php $contador++; } ?>
                                <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a <?php if($pagina < $total_paginas) echo 'href="'.base_url()."index.php/novios/comunidad/perfil/debates/$ordenamiento/$datos_usuario->id_usuario/".($pagina + 1).'"'?>><i class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Escribe un comentario en el muro</h4>
            <div class="col s12 m12 input-field">
                <textarea id="comentario" class="form-control" rows="20" style="resize: none; height: 50%;"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn dorado-2 comentar-muro" data-id="">Publicar</a>
        </div>
    </div>
    <!-- -----------------  TEMPLATE VENTANA BUSCADOR -------------------------- -->
    <ul id="lista_debates">
        <li id="item_debate">
            <a class="enlace_debate" href="">
                <div class="row">
                    <div class="col s4 m3">
                        <div class="card">
                            <div class="card-image">
                                <img class="imagen_debate">
                            </div>
                        </div>
                    </div>
                    <div class="col s9 m9">
                        <p class="titulo_debate"></p>
                        <p class="fecha_debate"></p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <!-- ------------------  FIN TAMPLETE VENTANA BUSCADOR --------------------- -->
    <script>
        $(document).ready(function(){
            $('ul.tabs').tabs();
        });

        $(document).ready(function(){
            $('.filtro1 a').on("click", function (){
                var opcion = $(this).text();
                switch(opcion){
                    case "Mi Perfil":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/usuario/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Mi Muro":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/mi_muro/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Debates":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/debates/participacion/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Amigos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/amigos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Fotos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/fotos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Videos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/videos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Actividad":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/actividad/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Visitas":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/visitas/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Proveedores":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/proveedores/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                }
            }); 
        });
        
        $(document).ready(function(){
            $('.botones_tarjeta').on('click', function(){
                var click = $(this).attr('data-id');
                var boton = click.split("-");
                if(boton[0] == "participacion"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "misdebates"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "fotos"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/fotos/"?>"+boton[1];
                }else if(boton[0] == "amigos"){
                    window.location.href = "<?php echo base_url()."index.php/novios/comunidad/perfil/amigos/"?>"+boton[1];
                }
            });
        });
        
        $(document).ready(function(){
            $('.modal-trigger').modal({
                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                in_duration: 300, // Transition in duration
                out_duration: 200, // Transition out duration
                starting_top: '4%', // Starting top style attribute
                ending_top: '10%', // Ending top style attribute
                }
            );
            $(".comentar").on("click", function(){
                var name = $(this).attr("data-name");
                var token = name.split("-");
                $(".modal-action").attr("data-id",token[1]);
            });
        });
        
        $(document).ready(function (){
            $(".comentar-muro").on("click", function(){
                var comentario = $("#comentario").val();
                var usuario = $(this).attr("data-id");
                $.ajax({
                    url: '<?php echo base_url()."index.php/novios/comunidad/perfil/comentario"?>',
                    method: 'POST',
                    data:{
                        'comentario': comentario,
                        'muro_usuario': usuario 
                    },
                    success: function(res){
                        if(res.success){
                            setTimeout(function(){
                                alert("Se ha publicado con exito");
                                $("#comentario").val("");
                            },500);
                        }
                    },
                    error: function(){
                        setTimeout(function(){
                            alert("Lo sentimos ocurrio un error");
                        },500);
                    }
                });
            });
        });
        
        $(document).ready(function(){
            $('.btn_agregar').on("click", function(){
                var usuario = $(this).attr("data-id");
                var token = usuario.split('-');
                var tipo = $("."+usuario).text();
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/comunidad/perfil/solicitudAmistad',
                    method: 'POST',
                    data:{
                        'id_usuario_confirmacion': token[1],
                        'tipo': tipo
                    },
                    success: function(res){
                        if(res.success){
                            $("."+usuario).text(res.data);
                        }
                    },
                    error: function(){
                        alert("Lo sentimos ocurrio un error");
                    }
                });
            });
        });
        
        $(document).ready(function (){
            $('.desplegar_grupos').click(function (){
                var mostrar = $(this).text();
                var display = $('.oculto').css('display');
                if(display == "none"){
                    $('.oculto').css('display','block');
                    $(this).text("Ocultar Grupos");
                }else{
                    $('.oculto').css('display','none');
                    $(this).text("M&aacute;s Grupos...");
                }
            });
        });
        
    $(document).ready(function (){
        var activo = 0;
        var height = 0;
        var bandera = true;
        
        $("input[name=buscar]").keydown(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(key == 40){
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo++;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo > 1){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height > 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                }else{
                    if(bandera){
                        activo++;
                        bandera = false;
                    }
                    activo--;
                }
            }else if(key == 38){
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo--;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo >= 0){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height >= 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                }else{
                    if(!bandera){
                        activo--;
                        bandera = true;
                    }
                    activo++;
                }
            }else if(key == 13){
                $("#buscador").on('keypress',function(e){
                    e.preventDefault();
                    return false;
                });
                window.location.href = $('.item-'+activo+' a').attr("href");
            }else if(key != 39 && key != 37){
                activo = 0;
                grupos();
            }
        });
        
        $("input[name=buscar]").keyup(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(titulo != "" && key != 38 && key != 40 && key != 39 && key != 37){
                activo = 0;
                debates(titulo);
            }else if(key == 13){
                window.location.href = $('.item-'+activo+' a').attr("href");
            }
        });
        
        $(document).on('mouseenter','#ventana_buscador > ul > li',function(){
            $('.item-'+activo).removeClass("hover");
            var clase = $(this).attr("class");
            var token = clase.split("-");
            activo = token[1];
            $(this).addClass("hover");
        });
       
        $("body").on('click',function(){
            $("#ventana_buscador").hide();
            $('.item-'+activo).removeClass("hover");
        });
        
        $("input[name=buscar]").on('click', function (e){
            e.stopPropagation();
            $('.item-'+activo).removeClass("hover");
            activo = 0;
            var titulo = $("input[name=buscar]").val();
            if(titulo == "" && $("#ventana_buscador ul").html() == undefined){
                grupos();
            }
            $("#ventana_buscador").show();
        });
        
        $("#ventana_buscador").on('mouseleave',function(){
            setTimeout(function(){
                $('.item-'+activo).removeClass("hover");
            },1000);
        });
        
        function grupos(){
            $.ajax({
                url: '<?php echo base_url()."index.php/novios/comunidad/home/getGrupos"?>',
                success: function(res) {
                    var val = Array();
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_grupo);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].imagen);
                                $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                $(li).find(".fecha_debate").text(res.data[aux].debates+" Debates");
                                $(ul).append(li);
                                if(i == 16){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").data("grupos","true");
                        $("#ventana_buscador").show();
                    }
                }          
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
        
        function debates(titulo){
            $.ajax({
                url: '<?php echo base_url()."index.php/novios/comunidad/home/buscar" ?>',
                method: 'post',
                data:{
                    'titulo_debate': titulo
                },
                success: function(res){
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        var val = Array();
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_debate);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].foto_usuario);
                                $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                $(ul).append(li);
                                if(i > (res.data.length - 1)){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").show();
                    }
                }
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
    });
    </script>
</body>
<?php $this->view('principal/footer'); ?>