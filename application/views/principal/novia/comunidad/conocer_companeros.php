<?php $this->view('principal/header'); ?>
<?php $this->view('principal/novia/menu'); ?>
<link href="<?php echo base_url() ?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<style>
    .entorno_mensaje {
        height: 300px;
        width: 100%;
        margin-bottom: 50px;
    }

    .encabezado {
        height: 250px;
        width: 100%;
    }

    .usuario {
        text-decoration: none;
        color: #f9a797 !important;
        z-index: 200;
    }

    .ciudad {
        text-decoration: none;
        color: #75767a !important;
        z-index: 200;
    }

    .contenido_fecha {
        width: 80%;
        margin-left: auto;
        margin-right: auto;
        border-color: #797a7d !important;
        border-top-style: dashed;
        border-bottom-style: dashed;
        margin-bottom: 50px;
        background: white;
        text-align: center;
    }
</style>
<body>
<div class="body-container">
    <div class="row">
        <div class="col s12 m3" style="float: right">
            <ul class="collection with-header z-depth-1">
                <li class="collection-header dorado-2"><h6>
                        <b><?php if ( ! empty($fecha_boda)) echo "Nos casamos el $fecha_boda" ?></h6></b></li>
                <?php
                if ( ! empty($companeros)) {
                    foreach ($companeros as $companero) {
                        ?>
                        <li class="collection-item grey lighten-5">
                            <div class="row">
                                <div class="col s6 m4">
                                    <div class="card">
                                        <div class="card-image">
                                            <a href="<?php echo ! empty($companero->url_usuario) ? $companero->url_usuario : "" ?>">
                                                <img src="<?php echo ! empty($companero->foto_usuario) ? $companero->foto_usuario : "" ?>">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s6 m8">
                                    <a href="<?php echo ! empty($companero->url_usuario) ? $companero->url_usuario : "" ?>">
                                        <p class="usuario"><?php if ( ! empty($companero->usuario)) echo $companero->usuario ?></p>
                                    </a>
                                    <p class="ciudad"><?php if ( ! empty($companero->ciudad_boda)) echo $companero->ciudad_boda ?></p>
                                </div>
                            </div>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
        <div class="col s12 m9 pull-left grey lighten-5 z-depth-1 panel_debate" style="margin-top: 9px">
            <div class="row" style="background: #8c8d8f!important; padding-top: 20px;">
                <p style="font-size: 30px; color: white; font-weight: 8px; text-align: center"><i>Novios que nos casamos
                        el</i>
                <p>
                    <div class="contenido_fecha">
                <p>
                    <text class="dorado-2-text" style="font-size: 32px; "><b><i class="fa fa-heart-o"
                                                                                aria-hidden="true"></i></b> <?php echo empty($dia) ? '' : strtoupper($dia) ?>
                    </text>
                    <text class="grey-text"> DE</text>
                    <text class="dorado-2-text"
                          style="font-size: 32px; "><?php echo empty($mes) ? '' : strtoupper($mes) ?> </text>
                    <text class="grey-text"> DE</text>
                    <text class="dorado-2-text"
                          style="font-size: 32px; "> <?php echo empty($anio) ? '' : strtoupper($anio) ?> <b><i
                                    class="fa fa-heart-o" aria-hidden="true"></i></b></text>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <div class="chip red darken-1 white-text right agrega_contenido"
                     style="margin-bottom:20px; display:none;">Agrega contenido al debate.
                </div>
            </div>
            <div class="card-panel light-blue lighten-5">
                <p>Escribe un mensaje de presentaci&oacuote;n para conocerlos y compartir experiencias. Crearemos un
                    debate nuevo en el grupo
                    de Clubnupcial.com y enviaremos una notificaci&oacuote;n a cada uno de los novi@s que se casen
                    el <?php echo empty($fecha_boda) ? '' : "<b>$fecha_boda</b>" ?>.
                </p>
            </div>
        </div>
        <form id="publicar_debate" action="<?php echo base_url() ?>novios/comunidad/Home/publicarDebate"
              method="post">
            <div class="row">
                <textarea id="text_area" name="contenido" class="entorno_mensaje"></textarea>
                <input type="hidden" name="grupos" value="1">
                <input type="hidden" name="titulo"
                       value="Novios que nos casamos el <?php echo empty($fecha_boda) ? '' : $fecha_boda ?>">
            </div>
            <div class="row">
                <button type="button" class="btn waves-effect waves-light dorado-2 btn_publicar">Publicar</button>
            </div>
        </form>
    </div>
</div>
</div>

<!-- ------------------- MODAL PARA SUBIR IMAGENES ---------------------- -->
<div style="z-index: 999999;">
    <div id="modal-file-browser" class="modal  modal-fixed-footer" style="">
        <div class="modal-content" style="padding-left: 0px;padding-right: 0px;">
            <h4>Buscar Imagen</h4>
            <div class="row nowrap">
                <div class="col s12">
                    <ul class="tabs">
                        <!--                            <li class="tab col s3"><a class="active" href="#test1">Imagenes</a></li>-->
                        <li class="tab col s3"><a href="#test2">Subir Imagen</a></li>
                    </ul>
                </div>
                <!--                    <div id="test1" class="col s12">
                                        <div class="container">
                                            <div id="sin-imagenes" class="card-panel teal">
                                                <span class="white-text">A&uacute;n no tienes imagenes cargadas.
                                                </span>
                                            </div>
                                            <div class="row contenedor-imagenes">
                                            </div>
                                        </div>
                                    </div>-->
                <div id="test2" class="col s12">
                    <div class="container">
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>File</span>
                                <input id="file-ajax" type="file" accept="image/*">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Upload one or more files">
                            </div>
                        </div>
                        <input type="text" value="" id="file-data" style="display: none">
                        <div class="modal-imagen">
                            <img id="img-preview" class="responsive-img"/>
                            <div class="modal-image-cargando">
                                <i class="fa fa-spinner fa-pulse fa-2x"></i> SUBIENDO ...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
            <a id="modal-aceptar" href="#!" class=" modal-action waves-effect waves-green btn-flat">Aceptar</a>
        </div>
    </div>
</div>
<!-- ------------------- FINAL MODAL PARA SABIR IMAGENES ------------------- -->

<script type="text/javascript" src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js"></script>
<script>
    $(document).ready(function () {
        init_tinymce_mini("#text_area");
    });

    $(document).ready(function () {
        $(".btn_publicar").click(function () {
            if (tinyMCE.activeEditor.getContent() == "") {
                $('.agrega_contenido').show();
            } else {
                $("#publicar_debate").submit();
            }
        });
    });

    $(document).ready(function () {
        $('.modal').modal();
        $(".modal-image-cargando").hide();
        $('select').material_select();
        $("#btn-publicar").on("click", function () {
            save(true)
        });
        $("#btn-borrador").on("click", function () {
            save(false)
        });

        //SUBE LA IMAGEN AL SERVIDOR Y LA GUARDA EN LA BASE DE DATOS
        $("#modal-aceptar").on("click", function () {
            var action = $(".modal .tab .active").html().toUpperCase();
            if (action.indexOf("SUBIR") != -1) {
                $(".modal-image-cargando").show();
                var data = $("#file-data").val();
                if (data != null && data != "") {
                    $.ajax({
                        url: "<?php echo $this->config->base_url() ?>novios/comunidad/Home/subir/imagen",
                        method: "POST",
                        data: {
                            archivo: $("#file-data").val()
                        },
                        success: function (res) {
                            if (res.success) {
                                console.log(res);
                                window.callback(res.data, {alt: 'alt'});
                                $('#modal-file-browser').modal("close");
                                $(".mce-window").css({"display": "block"});
                            }
                            $(".modal-image-cargando").hide();
                            $("#file-data").val("");
                            $('#img-preview').get(0).src = "";
                            $(".file-path validate").val();
                        },
                        error: function () {
                            $(".modal-image-cargando").hide();
                        }
                    });
                }
            } else if (action.indexOf("IMAGENES") != -1) {
                var url = $(".contenedor-imagenes .active .card-image").get(0).style["background-image"];
                var url = url.replace("url(\"", "").replace("\")", "");
                window.callback(url, {alt: 'alt'});
                $('#modal-file-browser').modal("close");
                $(".mce-window").css({"display": "block"});
            }
        });
        $("#file-ajax").on("change", function () {
            var preview = $('#img-preview').get(0);
            var file = $("#file-ajax").get(0).files[0];
            var reader = new FileReader();
            reader.onloadend = function () {
                preview.src = reader.result;
                $("#file-data").val(reader.result);
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        });
        $("#tags").on("change", function (evt) {
            var chip = '<div class="chip truncate" style="max-width:120px" data-id="NUD" data-nombre="RENOM"  id="chip-ID"><i class="material-icons">close</i>NOMBRE</div>';
            var id = $("#tags").val();
            var nombre = $("#tags #cat-" + id).html();
            $("#tags #cat-" + id).remove();
            chip = chip.replace("ID", id).replace("NOMBRE", nombre).replace("RENOM", nombre).replace("NUD", id);
            $('select').material_select('destroy');
            $('select').material_select();
            $("#tags-chips").append(chip);
            $("#chip-" + id + " i").on("click", function (evt) {
                var e = $(evt.currentTarget.parentNode);
                $("#tags").append('<option id="cat-' + e.attr("data-id") + '" value="' + e.attr("data-id") + '">' + e.attr("data-nombre") + '</option>');
                $('select').material_select('destroy');
                $('select').material_select();
            })
        })
        $("#seleccionar-imagen").on("click", function () {
            fileBrowser(function (url) {
                var a = url.split("/");
                $("#img-destacada").attr("src", url);
                $("#img-destacada").attr("data-id", a[a.length - 1]);
                console.log(a[a.length - 1], url);
            }, "url", {filetype: "image"})
        })
    });

    //        FUNCION DE CREACION DEL EDITOR
    function init_tinymce_mini(elem) {
        if (typeof tinymce != "undefined") {
            tinymce.init({
                selector: elem,
                theme: 'modern',
                menubar: 'false',
                relative_urls: false,
                plugins: [
                    'autolink link image directionality emoticons'
                ],
                toolbar: 'undo redo | bold | italic | link |  alignleft aligncenter alignright alignjustify | image | emoticons',
                imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58'],
                browser_spellcheck: true,
                file_picker_callback: fileBrowser,
            });
        }
    }

    //BUSCADOR DE IMAGENES
    function fileBrowser(callback, value, meta) {
        if (meta.filetype == 'image') {
            if ($(".contenedor-imagenes").children().length <= 1) {
                $.ajax({
                    url: "<?php echo $this->config->base_url() ?>novios/comunidad/Home/imagen",
                    success: function (res) {
                        $("#sin-imagenes").hide();
                        var template = ' <div class="card hoverable " style="background: black;"><div class="card-image image-galeria"></div></div>';
                        for (var i in res.data) {
                            var imagen = res.data[i];
                            var div = document.createElement("div");
                            var $div = $(div)
                            $div.addClass("col s6 m3")
                            $div.html(template);
                            $div.find(".image-galeria").css("background-image", "url(" + imagen.url + ")");
                            $div.on("click", function (evt) {
                                $(".contenedor-imagenes .active").removeClass("active");
                                var $elm = $(evt.currentTarget).find(".card");
                                $elm.addClass("active");
                            })
                            $(".contenedor-imagenes").append($div);
                        }
                    }
                });
            }
            window.callback = callback;
            $('#modal-file-browser').modal({
                dismissible: true,
                opacity: .5,
                in_duration: 100,
                out_duration: 100,
                ready: function () {
                    $(".mce-window").css({"display": "none"});
                },
                complete: function () {
                    $(".mce-window").css({"display": "block"});
                }
            });
            $("#modal-file-browser").css({"z-index": 999999});
        }
        return false;
    }
</script>
</body>
<?php $this->view('principal/footer'); ?>

