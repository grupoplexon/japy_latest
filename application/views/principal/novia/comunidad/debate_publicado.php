<?php $this->view('principal/header'); ?>
<?php $this->view('principal/novia/menu'); ?>
    <link href="<?php echo base_url() ?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
    <body>
    <!--BARRA DE TITULO CON UN BUSCADOR DE DEBATES-->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
            <p>
                <div class="col s12 m2">
            <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
        </div>
        <form id="buscador" method="get" action="<?php echo base_url()."novios/comunidad/forum/buscar" ?>"
              autocomplete="off">
            <div class="input-field col s7 m5" style="margin:0">
                <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
                <input type="hidden" name="page" value="1"/>
                <div id="ventana_buscador">
                </div>
            </div>
            <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
                <button type="submit" class="btn waves-effect waves-light dorado-2"><i class="fa fa-search"
                                                                                       aria-hidden="true"></i> Buscar
                </button>
            </div>
        </form>
        </p>
    </div>
    </div>

    <div class="body-container">
        <div class="row">
            <!--COMPONENTES LATERALES-->
            <?php
            if (empty($usuarios_boda)) {
                $usuarios_boda = "";
            }
            if (empty($visitas_perfil)) {
                $visitas_perfil = "";
            }
            if (empty($grupos_miembro)) {
                $grupos_miembro = "";
            }
            if (empty($grupos)) {
                $grupos = "";
            }
            $data = [
                    'usuarios_boda'  => $usuarios_boda,
                    'visitas_perfil' => $visitas_perfil,
                    'grupos_miembro' => $grupos_miembro,
                    'grupos'         => $grupos,
            ];
            $this->view('principal/novia/comunidad/lateral.php', $data);
            ?>

            <!--            PUBLICACION DEL DEBATE                      -->
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s4 m2 pull-left">
                        <div class="row">
                            <div style="text-align: center">
                                <div class="card" style="margin-top: 10px">
                                    <div class="card-image">
                                        <a href="<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $id_usuario ?>">
                                            <img class="foto_perfil" src="<?php echo $foto ?>">
                                        </a>
                                    </div>
                                </div>
                                <div style="border-radius: 10px; border: 1px solid wheat; height: 20px;margin-bottom: 20px; background: #FFFFCC">
                                    <p class="truncate" style="color: orange; margin-top: 3px; font-size: 12px"><i
                                                class="fa fa-star"
                                                aria-hidden="true"></i><?php echo empty($tipo_novia) ? "" : $tipo_novia ?>
                                        <i class="fa fa-star" aria-hidden="true"></i></p>
                                </div>
                                <p style="margin: 0 auto; margin-top: 10px"><?php if ($fecha_boda != "" && $fecha_boda != null) echo $fecha_boda ?></p>
                                <p style="margin: 0 auto; margin-top: 10px"><?php if ($estado_boda != "" && $estado_boda != null) echo $estado_boda ?></p>
                            </div>
                        </div>
                        <div class="row" style="text-align: center">

                        </div>
                    </div>
                    <div class="col s8 m10 grey lighten-5 z-depth-1" style="float: right; margin-top: 10px">
                        <div class="row" style="border-bottom: 1px solid #999;">
                            <h5 class="col s12 m12"> <?php echo $titulo_debate; ?> </h5>
                            <div class="col s3 m1">
                                <div class="card grey lighten-5 z-depth-0" style=" margin-top: 15px;">
                                    <div class="card-image">
                                        <img class="imagen_grupos"
                                             src="<?php echo base_url()."/dist/images/comunidad/grupos/$imagen" ?>"
                                             style="margin-left: -0px">
                                    </div>
                                </div>
                            </div>
                            <div class="col s9 m11 pull-right">
                                <p>Por <a class="usuario"
                                          href="<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $id_usuario ?>"> <?php echo $usuario ?></a> <?php if ($grupo != "" && $grupo != null) echo 'en el' ?>
                                    <a class="grupo"
                                       href="<?php echo base_url() ?>novios/comunidad/group/grupo/<?php echo $id_grupo ?>/todo"><?php echo $grupo ?></a>
                                </p>
                                <p>Publicado <?php echo $fecha_creacion ?> <a class="respuestas clickable"
                                                                              style="color: #75767a!important;"><?php if ($contador != "" && $contador != null) echo $contador." Respuestas" ?></a>
                                </p>
                            </div>
                        </div>
                        <div class="col s12 m12 grey lighten-5 contenido_debates">
                            <?php echo $contenido; ?>
                        </div>
                        <div class="col s12 m12 grey lighten-5" style="border-top: 1px solid #999;">
                            <div style="float: right; padding: 20px">
                                <?php if (empty($id_denuncia)) { ?>
                                    <a class="denunciar_debate" href="#modal1" style="color:#999"><i
                                                class="fa fa-minus-circle" class="" aria-hidden="true"></i>Denunciar</a>
                                <?php } ?>
                            </div>
                            <div style="margin: 10px; float: right">
                                <button id="suscribirse" type="button" class="btn waves-effect waves-light dorado-2"
                                        style="margin-left: 10px">
                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                    <text id="suscribir"><?php if ($suscripcion != "" && $suscripcion != null) echo $suscripcion ?></text>
                                </button>
                            </div>
                            <div style="margin: 10px; float: right">
                                <button id="responder" type="button" class="btn waves-effect waves-light dorado-2"><i
                                            class="fa fa-reply-all" aria-hidden="true"></i> Responder
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            FIN DE LA PUBLICACION DEL DEBATE                -->

            <div class="col s12 m9 pull-left num_comentarios">
                <div class="row">
                    <div class="col s12 m6">
                        <h5><?php if ($contador != "" && $contador != null && $contador > 0) echo $contador." Respuestas" ?></h5>
                    </div>
                </div>
            </div>

            <!--                COMENTARIOS                             -->
            <div class="col s12 m9 pull-left" id="panel_comentarios">
                <?php
                if ( ! empty($comentarios)) {
                    foreach ($comentarios as $comentario) {
                        if (empty($comentario->id_boda)) {
                            $comentario->fecha_boda = "";
                        }
                        echo '<div class="row">
                                    <div class="col s4 m2 pull-left" class="comentario_usuario" style="margin-top: -7px; text-align: center">
                                        <div class="card">
                                            <div class="card-image">';
                        echo '<a class="link_usuario1" href="'.base_url().'novios/comunidad/perfil/usuario/'.$comentario->id_usuario.'">';
                        echo '<img class="foto_perfil" src="'.$comentario->foto_usuario.'">
                                                </a>
                                            </div>
                                        </div>';
                        echo '<div style="border-radius: 10px; border: 1px solid wheat; height: 20px;margin-bottom: 20px; background: #FFFFCC">
                                            <p class="truncate" style="color: orange; margin-top: 3px; font-size: 12px"><i class="fa fa-star" aria-hidden="true"></i>'.$comentario->tipo_novia.'<i class="fa fa-star" aria-hidden="true"></i></p>
                                        </div>
                                        <p class="fecha_boda" style="margin: 0 auto; margin-top: 10px">'.$comentario->fecha_boda.'</p>
                                        <p class="estado_boda" style="margin: 0 auto; margin-top: 10px">'.$comentario->estado_boda.'</p>
                                    </div>';
                        echo '<div class="col s8 m10 grey lighten-5 z-depth-1" id="nuevo_comentario" style="float: right;">
                                        <div class="col s12 m12" style="padding-top:10px">
                                            <p ><a class="link_usuario2" style="margin-right:10px;color: #f9a797!important;" href="'.base_url().'novios/comunidad/perfil/usuario/'.$comentario->id_usuario.'">'.$comentario->usuario.'</a> <text class="fecha_creacion" style="color: #75767a!important;">'.$comentario->fecha_creacion.'</text> </p>
                                        </div>
                                        <div class="col s12 m12 contenido_comentario contenido_debates">
                                        '.$comentario->mensaje.'
                                        </div>
                                        <div>

                                        </div>
                                        <div class="col s12 m12 grey lighten-5 pull-left" style="border-top: 1px solid #999; margin-top:10px; margin-bottom:10px; padding-top:15px; padding-bottom:10px">
                                            <a class="responder_c clickable" data-id="comentario-'.$comentario->id_comentario.'"><i class="fa fa-angle-left" aria-hidden="true"></i> Responder</a>';
                        if ( ! empty($comentarios2)) {
                            foreach ($comentarios2 as $respuesta) {
                                if ($respuesta->comentado == $comentario->id_comentario) {
                                    echo '<a class="mostrar clickable" data-id="respuesta-'.$comentario->id_comentario.'">Mostrar Comentarios</a>';
                                    break;
                                }
                            }
                        }
                        if (empty($comentario->id_denuncia)) {
                            echo '<a class="denunciar_c" style="float: right; color:#999" href="#model2" data-id="denunciar-'.$comentario->id_comentario.'"><i class="fa fa-minus-circle" aria-hidden="true"></i>Denunciar</a>';
                        }
                        echo '</div>
                                    <div class="col s12 m12 grey lighten-5 pull-left" id="panel-respuesta'.$comentario->id_comentario.'" style="display:none; padding-top: 20px">

                                    </div>
                                    </div>                                
                                    </div>';
                    }
                } else {
                    echo '<div class="row"> <div class="col s12 m9"><h5 class="existencia_c"> A&uacute;n no existen comentarios... </h5> </div></div>';
                }
                ?>
            </div>
            <!--                FIN COMENTARIO                      -->

            <!-- ------------------------------PAGINADOR----------------------------- -->
            <div class="col s12 m9 pull-left"
                 style="<?php if ($contador == 0 || $contador <= 20) echo "display: none" ?>">
                <div class="row">
                    <div class="col s12 m6" style="float: right">
                        <ul class="pagination">
                            <li class="<?php echo($pagina == 1 ? "disabled" : "waves-effect") ?>">
                                <a <?php if ($pagina > 1) echo 'href="'.base_url()."novios/comunidad/Home/debatePublicado/$id_debate/".($pagina - 1).'"' ?>><i
                                            class="material-icons">chevron_left</i></a></li>
                            <?php
                            $j        = 1;
                            $contador = 0;
                            if ($total_paginas > 10) {
                                $r = $total_paginas - 10;
                                if ($r < $pagina) {
                                    $aux = ($pagina + 1) - $r;
                                    if ($aux <= $r) {
                                        $j = $aux;
                                    } else {
                                        $j = $r + 1;
                                    }
                                }
                            } else {
                                $j = 1;
                            }
                            for ($i = $j; $i <= $total_paginas && $contador < 10; $i++) {
                                ?>
                                <li class="<?php echo($i == $pagina ? "active" : "waves-effect") ?>"><a
                                            href="<?php echo base_url()."novios/comunidad/Home/debatePublicado/$id_debate/$i" ?>"><?php echo $i ?></a>
                                </li>
                                <?php $contador++;
                            } ?>
                            <li class="<?php echo($pagina == $total_paginas ? "disabled" : "waves-effect") ?>">
                                <a <?php if ($pagina < $total_paginas) echo 'href="'.base_url()."novios/comunidad/Home/debatePublicado/$id_debate/".($pagina + 1).'"' ?>><i
                                            class="material-icons">chevron_right</i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <!--    ---------------------------FIN PAGINADOR-------------------------- -->

            <!-- ---------------------------EDITOR DE COMENTARIO----------------------- -->
            <div class="col s12 m9 pull-left">
                <input type="hidden" id="respuesta">
                <div class="row">
                    <div class="col s12 m12">
                        <h5><b>Responder al debate</b></h5>
                    </div>
                    <div class="col s12 m12 grey lighten-5 z-depth-1">
                        <div class="col s3 m1 pull-left">
                            <div class="row">
                                <div style="text-align: center">
                                    <div class="card" style="margin-top: 20px">
                                        <div class="card-image">
                                            <a href="<?php echo base_url()."novios/comunidad/perfil/usuario/".$this->session->userdata('id_usuario') ?>">
                                                <img class="foto_perfil"
                                                     src="<?php echo ! empty($foto_logeado) ? $foto_logeado : "" ?>">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form method="post">
                            <input type="hidden" name="debate" value="<?php echo $id_debate ?>">
                            <div class="col s9 m11" style="margin-bottom: 50px">
                                <div id="comentario">
                                    <textarea id="text_area" class="entorno_mensaje" name="mensaje"
                                              required> </textarea>
                                </div>
                            </div>
                            <div class="col s9 m11">
                                <div class="row" style="margin-bottom: 60px">
                                    <div class="col s12 m12">
                                        <button id="comentar" type="button"
                                                class="btn waves-effect waves-light dorado-2"><i class="fa fa-comment"
                                                                                                 aria-hidden="true"></i>
                                            Comentar
                                        </button>
                                    </div>
                                    <div class="input-field col s12 m12">
                                        <div class="row">
                                            <p style="display: inline-block; margin-bottom: 20px;">
                                                <input type="checkbox" id="permiso_notificacion"
                                                       name="permiso_notificacion">
                                                <label for="permiso_notificacion"
                                                       style="display: inline-block; margin-bottom: 15px;">Deseas
                                                    recibir notificaciones via email, cuando haya nuevos mensajes en
                                                    este debate</label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- -------------------------FIN EDITOR DE COMENTARIOS----------------------- -->

        </div>
    </div>

    <!-- ------------------- MODAL PARA SUBIR IMAGENES ---------------------- -->
    <div style="z-index:999;">
        <div id="modal-file-browser" class="modal  modal-fixed-footer" style="">
            <div class="modal-content" style="padding-left: 0px;padding-right: 0px;">
                <h4>Buscar Imagen</h4>
                <div class="row nowrap">
                    <div class="col s12">
                        <ul class="tabs">
                            <!--                            <li class="tab col s3"><a class="active" href="#test1">Imagenes</a></li>-->
                            <li class="tab col s3"><a href="#test2">Subir Imagen</a></li>
                        </ul>
                    </div>
                    <!--                    <div id="test1" class="col s12">
                                            <div class="container">
                                                <div id="sin-imagenes" class="card-panel teal">
                                                    <span class="white-text">A&uacute;n no tienes imagenes cargadas.
                                                    </span>
                                                </div>
                                                <div class="row contenedor-imagenes">
                                                </div>
                                            </div>
                                        </div>-->
                    <div id="test2" class="col s12">
                        <div class="container">
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>File</span>
                                    <input id="file-ajax" type="file" accept="image/*">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text"
                                           placeholder="Upload one or more files">
                                </div>
                            </div>
                            <input type="text" value="" id="file-data" style="display: none">
                            <div class="modal-imagen">
                                <img id="img-preview" class="responsive-img"/>
                                <div class="modal-image-cargando">
                                    <i class="fa fa-spinner fa-pulse fa-2x"></i> SUBIENDO ...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
                <a id="modal-aceptar" href="#!" class=" modal-action waves-effect waves-green btn-flat">Aceptar</a>
            </div>
        </div>
    </div>
    <!-- ------------------- FINAL MODAL PARA SABIR IMAGENES ------------------- -->

    <!-- ---------------   TEMPLATE NUEVO COMENTARIO ------------------------- -->
    <div class="hide" id="template-comentario">
        <div class="row">
            <div class="col s4 m2 pull-left" class="comentario_usuario" style="margin-top: -7px; text-align: center">
                <div class="card">
                    <div class="card-image">
                        <a class="link_usuario1" href="#!">
                            <img class="foto_perfil" src="">
                        </a>
                    </div>
                </div>
                <div style="border-radius: 10px; border: 1px solid wheat; height: 20px;margin-bottom: 20px; background: #FFFFCC">
                    <p class="truncate" style="color: orange; margin-top: 3px; font-size: 12px"><i class="fa fa-star"
                                                                                                   aria-hidden="true"></i>
                        <text class="tipo_novia"></text>
                        <i class="fa fa-star" aria-hidden="true"></i></p>
                </div>
                <p class="fecha_boda" style="margin: 0 auto; margin-top: 10px"></p>
                <p class="estado_boda" style="margin: 0 auto; margin-top: 10px"></p>
            </div>
            <div class="col s8 m10 grey lighten-5 z-depth-1" id="nuevo_comentario" style="float: right;">
                <div class="col s12 m12" style="padding-top:10px">
                    <p><a class="link_usuario2" style="margin-right:10px;color: #f9a797!important;" href=""></a>
                        <text class="fecha_creacion" style="color: #75767a!important;"></text>
                    </p>
                </div>
                <div class="col s12 m12 contenido_comentario">
                </div>
                <div>

                </div>
                <div class="col s12 m12 grey lighten-5 pull-left"
                     style="border-top: 1px solid #999; margin-top:10px; margin-bottom:10px; padding-top:15px; padding-bottom:10px">
                    <a class="responder_c clickable"><i class="fa fa-angle-left" aria-hidden="true"></i>
                        Responder</button></a>
                    <a class="denunciar_c" style="float: right; color:#999" href="#modal2" data-id=""><i
                                class="fa fa-minus-circle" aria-hidden="true"></i>Denunciar</a>
                </div>
                <div class="col s12 m12 grey lighten-5 pull-left panel-respuestas">

                </div>
            </div>
        </div>
    </div>
    <!-- ---------------   TEMPLATE NUEVO COMENTARIO ------------------------- -->
    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <p>
                <input class="with-gap" name="denuncia" type="radio" id="sexual" value="Contenido sexual"/>
                <label for="sexual">Contenido sexual.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia" type="radio" id="comercial" value="Contenido comercial"/>
                <label for="comercial">Contenido comercial.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia" type="radio" id="inapropiado" value="Contenido inapropiado"/>
                <label for="inapropiado">Contenido inapropiado.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia" type="radio" id="ofensivo" value="Contenido ofensivo"/>
                <label for="ofensivo">Contenido ofensivo.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia" type="radio" id="ilegal" value="Contenido ilegal"/>
                <label for="ilegal">Contenido ilegal.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia" type="radio" id="duplicado" value="Contenido duplicado"/>
                <label for="duplicado">Contenido duplicado.</label>
            </p>
        </div>
        <div class="modal-footer">
            <a class="denunciar_debates modal-action waves-effect waves-green btn waves-effect waves-light dorado-2">Denunciar</a>
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <p>
                <input class="with-gap" name="denuncia_c" type="radio" id="sexual2" value="Contenido sexual"/>
                <label for="sexual2">Contenido sexual.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia_c" type="radio" id="comercial2" value="Contenido comercial"/>
                <label for="comercial2">Contenido comercial.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia_c" type="radio" id="inapropiado2" value="Contenido inapropiado"/>
                <label for="inapropiado2">Contenido inapropiado.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia_c" type="radio" id="ofensivo2" value="Contenido ofensivo"/>
                <label for="ofensivo2">Contenido ofensivo.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia_c" type="radio" id="ilegal2" value="Contenido ilegal"/>
                <label for="ilegal2">Contenido ilegal.</label>
            </p>
            <p>
                <input class="with-gap" name="denuncia_c" type="radio" id="duplicado2" value="Contenido duplicado"/>
                <label for="duplicado2">Contenido duplicado.</label>
            </p>
        </div>
        <div class="modal-footer">
            <a class="denunciar_comentarios modal-action waves-effect waves-green btn waves-effect waves-light dorado-2">Denunciar</a>
        </div>
    </div>
    <!-- -----------------  TEMPLATE VENTANA BUSCADOR -------------------------- -->
    <ul id="lista_debates">
        <li id="item_debate">
            <a class="enlace_debate" href="">
                <div class="row">
                    <div class="col s4 m3">
                        <div class="card">
                            <div class="card-image">
                                <img class="imagen_debate">
                            </div>
                        </div>
                    </div>
                    <div class="col s9 m9">
                        <p class="titulo_debate"></p>
                        <p class="fecha_debate"></p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <!-- ------------------  FIN TAMPLETE VENTANA BUSCADOR --------------------- -->
    <script type="text/javascript" src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            init_tinymce_mini("#text_area");
        });

        $(document).ready(function () {
            $("#responder").click(function (evt) {
                evt.preventDefault();
                $('html,body').animate({
                    scrollTop: $("#comentario").offset().top
                }, 2000);
                tinymce.execCommand('mceFocus', false, '#text_area');
            });
            $(".respuestas").click(function (evt) {
                evt.preventDefault();
                $('html,body').animate({
                    scrollTop: $(".num_comentarios").offset().top
                }, 2000);
            });
        });

        $(document).ready(function () {
            $("#comentar").click(function (evt) {
                evt.preventDefault();
                var mensaje = tinyMCE.activeEditor.getContent();
                if (mensaje != "" && mensaje != null) {
                    var tipo = $("#respuesta").val();
                    if (tipo == "" || tipo == null) {
                        $.ajax({
                            url: "<?php echo base_url() ?>novios/comunidad/Home/comentariosDebate",
                            method: "POST",
                            data: {
                                debate: '<?php echo $id_debate ?>',
                                mensaje: mensaje
                            },
                            success: function (res) {
                                if (res.success) {
                                    if (res.data.id_boda == null || res.data.id_boda == "") {
                                        res.data.fecha_boda = "";
                                    }
                                    var div = document.createElement("div");
                                    $(div).html($("#template-comentario").html());
                                    $(div).find(".foto_perfil").attr("src", res.data.foto_usuario);
                                    $(div).find(".tipo_novia").text(res.data.tipo_novia);
                                    $(div).find(".fecha_boda").text(res.data.fecha_boda);
                                    $(div).find(".estado_boda").text(res.data.estado_boda);
                                    $(div).find(".link_usuario1").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $this->session->userdata('id_usuario') ?>");
                                    $(div).find(".link_usuario2").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $this->session->userdata('id_usuario') ?>");
                                    $(div).find(".link_usuario2").html(res.data.usuario);
                                    $(div).find(".fecha_creacion").text(res.data.fecha_creacion);
                                    $(div).find(".contenido_comentario").html(res.data.mensaje);
                                    $(div).find(".responder_c").attr("data-id", "comentario-" + res.data.id_comentario);
                                    $(div).find(".denunciar_c").attr("data-id", "denunciar-" + res.data.id_comentario);
                                    $(div).find(".panel-respuestas").attr("id", "panel-respuesta" + res.data.id_comentario);
                                    $(div).find("#panel-respuesta" + res.data.id_comentario).removeClass("panel-respuestas");
                                    $("#panel_comentarios").append(div);
                                    tinyMCE.activeEditor.setContent("");
                                    $(".existencia_c").hide();
                                    $("#respuesta").val("");
                                    //console.log(res.data);
                                }
                            },
                            error: function () {
                            }
                        });
                    } else {
                        $.ajax({
                            url: "<?php echo base_url() ?>novios/comunidad/Home/respuestaComentario",
                            method: "POST",
                            data: {
                                debate: '<?php echo $id_debate ?>',
                                mensaje: mensaje,
                                respuesta: tipo
                            },
                            success: function (res) {
                                if (res.success) {
                                    if (res.data.id_boda == null || res.data.id_boda == "") {
                                        res.data.fecha_boda = "";
                                    }
                                    var conca = "#panel-respuesta" + tipo;
                                    var div = document.createElement("div");
                                    $(div).html($("#template-comentario").html());
                                    $(div).find(".foto_perfil").attr("src", res.data.url_foto);
                                    $(div).find(".tipo_novia").text(res.data.tipo_novia);
                                    $(div).find(".fecha_boda").text(res.data.fecha_boda);
                                    $(div).find(".estado_boda").text(res.data.estado_boda);
                                    $(div).find(".link_usuario1").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $this->session->userdata('id_usuario') ?>");
                                    $(div).find(".link_usuario2").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $this->session->userdata('id_usuario') ?>");
                                    $(div).find(".link_usuario2").html(res.data.usuario);
                                    $(div).find(".fecha_creacion").text(res.data.fecha_creacion);
                                    $(div).find(".contenido_comentario").html(res.data.comentario);
                                    $(div).find(".responder_c").attr("data-id", "comentario-" + tipo);
                                    $(div).find(".denunciar_c").attr("data-id", "denunciar-" + res.data.id_comentario);
                                    $(conca).prepend(div);
                                    tinyMCE.activeEditor.setContent("");
                                    $(".existencia_c").hide();
                                    $(conca).show();
                                    $("#respuesta").val("");
                                }
                            },
                            error: function () {
                            }
                        });
                    }
                }
            });
        });


        $(document).ready(function () {
            $("#panel_comentarios").on("click", ".responder_c", function (evt) {
                evt.preventDefault();
                $('html,body').animate({
                    scrollTop: $("#comentario").offset().top
                }, 2000);
                tinymce.execCommand('mceFocus', false, '#text_area');
                var comentario = $(this).attr("data-id");
                var comentario2 = comentario.split("-");
                $("#respuesta").val(comentario2[1]);
            });
        });

        $(document).ready(function () {
            $(".mostrar").click(function (evt) {
                evt.preventDefault();
                var comentario = $(this).attr("data-id");
                var comentario2 = comentario.split("-");
                var conca = "#panel-respuesta" + comentario2[1];
                if ($(this).text() == "Mostrar Comentarios") {
                    $.ajax({
                        url: '<?php echo base_url()?>novios/comunidad/Home/getRespuesta',
                        method: 'POST',
                        data: {
                            comentario: comentario2[1]
                        },
                        success: function (res) {
                            if (res.success) {
                                for (var aux in res.data) {
                                    if (res.data.hasOwnProperty(aux)) {
                                        var val = res.data[aux];
                                    }
                                }
                                var i;
                                for (i = 0; i < val.length; i++) {
                                    if (val[i].id_boda == null || val[i].id_boda == "") {
                                        val[i].fecha_boda = "";
                                    }
                                    var div = document.createElement("div");
                                    $(div).html($("#template-comentario").html());
                                    $(div).find(".foto_perfil").attr("src", val[i].url_foto);
                                    $(div).find(".tipo_novia").text(val[i].tipo_novia);
                                    $(div).find(".fecha_boda").text(val[i].fecha_boda);
                                    $(div).find(".estado_boda").text(val[i].estado_boda);
                                    $(div).find(".link_usuario1").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/" + val[i].id_usuario);
                                    $(div).find(".link_usuario2").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/" + val[i].id_usuario);
                                    $(div).find(".link_usuario2").html(val[i].usuario);
                                    $(div).find(".fecha_creacion").text(val[i].fecha_creacion);
                                    $(div).find(".contenido_comentario").html(val[i].mensaje);
                                    $(div).find(".responder_c").attr("data-id", "comentario-" + comentario2[1]);
                                    if (val[i].id_denuncia != "" && val[i].id_denuncia != null) {
                                        $(div).find(".denunciar_c").hide();
                                    } else {
                                        $(div).find(".denunciar_c").attr("data-id", "denunciar-" + val[i].id_comentario);
                                    }
                                    $(conca).append(div);
                                    tinyMCE.activeEditor.setContent("");
                                    $(".existencia_c").hide();
                                }
                            }
                        },
                        error: function () {
                        }
                    });
                    $(conca).empty();
                    $(conca).show();
                    $(conca).css("border-top", "1px solid #999");
                    $(this).text("Ocultar Comentarios");
                } else {
                    $(this).text("Mostrar Comentarios");
                    $(conca).hide();
                    $(conca).empty();
                }
            });
        });

        $("#suscribirse").click(function () {
            $.ajax({
                url: "<?php echo base_url()?>novios/comunidad/Home/suscripcionDebate",
                method: "POST",
                data: {
                    debate: '<?php echo $id_debate ?>',
                },
                success: function (res) {
                    if (res.success) {
                        $('#suscribir').text(res.data);
                    }
                },
                error: function () {
                    //alert('Lo sentimos ocurrio un error en el servidor');
                }
            });
        });

        $(document).ready(function () {
            $('.modal').modal();
            $(".modal-image-cargando").hide();
            $('select').material_select();
            $("#btn-publicar").on("click", function () {
                save(true)
            });
            $("#btn-borrador").on("click", function () {
                save(false)
            });

            //SUBE LA IMAGEN AL SERVIDOR Y LA GUARDA EN LA BASE DE DATOS 
            $("#modal-aceptar").on("click", function () {
                var action = $(".modal .tab .active").html().toUpperCase();
                if (action.indexOf("SUBIR") != -1) {
                    $(".modal-image-cargando").show();
                    var data = $("#file-data").val();
                    if (data != null && data != "") {
                        $.ajax({
                            url: "<?php echo $this->config->base_url() ?>novios/comunidad/Home/subir/imagen",
                            method: "POST",
                            data: {
                                archivo: $("#file-data").val()
                            },
                            success: function (res) {
                                if (res.success) {
                                    //console.log(res);
                                    window.callback(res.data, {alt: 'alt'});
                                    $('#modal-file-browser').modal("close");
                                    $(".mce-window").css({"display": "block"});
                                }
                                $(".modal-image-cargando").hide();
                                $("#file-data").val("");
                                $('#img-preview').get(0).src = "";
                                $(".file-path validate").val();
                            },
                            error: function () {
                                $(".modal-image-cargando").hide();
                            }
                        });
                    }
                } else if (action.indexOf("IMAGENES") != -1) {
                    var url = $(".contenedor-imagenes .active .card-image").get(0).style["background-image"];
                    var url = url.replace("url(\"", "").replace("\")", "");
                    window.callback(url, {alt: 'alt'});
                    $('#modal-file-browser').modal("close")
                    $(".mce-window").css({"display": "block"});
                }
            });
            $("#file-ajax").on("change", function () {
                var preview = $('#img-preview').get(0);
                var file = $("#file-ajax").get(0).files[0];
                var reader = new FileReader();
                reader.onloadend = function () {
                    preview.src = reader.result;
                    $("#file-data").val(reader.result);
                }
                if (file) {
                    reader.readAsDataURL(file);
                } else {
                    preview.src = "";
                }
            });
            $("#tags").on("change", function (evt) {
                var chip = '<div class="chip truncate" style="max-width:120px" data-id="NUD" data-nombre="RENOM"  id="chip-ID"><i class="material-icons">close</i>NOMBRE</div>';
                var id = $("#tags").val();
                var nombre = $("#tags #cat-" + id).html();
                $("#tags #cat-" + id).remove();
                chip = chip.replace("ID", id).replace("NOMBRE", nombre).replace("RENOM", nombre).replace("NUD", id);
                $('select').material_select('destroy');
                $('select').material_select();
                $("#tags-chips").append(chip);
                $("#chip-" + id + " i").on("click", function (evt) {
                    var e = $(evt.currentTarget.parentNode);
                    $("#tags").append('<option id="cat-' + e.attr("data-id") + '" value="' + e.attr("data-id") + '">' + e.attr("data-nombre") + '</option>');
                    $('select').material_select('destroy');
                    $('select').material_select();
                })
            })
            $("#seleccionar-imagen").on("click", function () {
                fileBrowser(function (url) {
                    var a = url.split("/");
                    $("#img-destacada").attr("src", url);
                    $("#img-destacada").attr("data-id", a[a.length - 1]);
                    console.log(a[a.length - 1], url);
                }, "url", {filetype: "image"})
            })
        });

        //        FUNCION DE CREACION DEL EDITOR
        function init_tinymce_mini(elem) {
            if (typeof tinymce != "undefined") {
                tinymce.init({
                    selector: elem,
                    theme: 'modern',
                    menubar: 'false',
                    relative_urls: false,
                    plugins: [
                        'autolink link image directionality emoticons'
                    ],
                    toolbar: 'undo redo | bold | italic | link |  alignleft aligncenter alignright alignjustify | image | emoticons',
                    imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58'],
                    browser_spellcheck: true,
                    file_picker_callback: fileBrowser,
                });
            }
        }

        //BUSCADOR DE IMAGENES 
        function fileBrowser(callback, value, meta) {
            if (meta.filetype == 'image') {
                if ($(".contenedor-imagenes").children().length <= 1) {
                    $.ajax({
                        url: "<?php echo $this->config->base_url() ?>novios/comunidad/Home/imagen",
                        success: function (res) {
                            $("#sin-imagenes").hide();
                            var template = ' <div class="card hoverable " style="background: black;"><div class="card-image image-galeria"></div></div>';
                            for (var i in res.data) {
                                var imagen = res.data[i];
                                var div = document.createElement("div");
                                var $div = $(div)
                                $div.addClass("col s6 m3")
                                $div.html(template);
                                $div.find(".image-galeria").css("background-image", "url(" + imagen.url + ")");
                                $div.on("click", function (evt) {
                                    $(".contenedor-imagenes .active").removeClass("active");
                                    var $elm = $(evt.currentTarget).find(".card");
                                    $elm.addClass("active");
                                })
                                $(".contenedor-imagenes").append($div);
                            }
                        }
                    });
                }
                window.callback = callback;
                $('#modal-file-browser').modal({
                    dismissible: true,
                    opacity: .5,
                    in_duration: 100,
                    out_duration: 100,
                    ready: function () {
                        $(".mce-window").css({"display": "none"});
                    },
                    complete: function () {
                        $(".mce-window").css({"display": "block"});
                    }
                });
                $("#modal-file-browser").css({"z-index:9999});
            }
            return false;
        }

        $(document).ready(function () {
            $('.desplegar_grupos').click(function () {
                var mostrar = $(this).text();
                var display = $('.oculto').css('display');
                if (display == "none") {
                    $('.oculto').css('display', 'block');
                    $(this).text("Ocultar Grupos");
                } else {
                    $('.oculto').css('display', 'none');
                    $(this).text("M&aacute;s Grupos...");
                }
            });
        });

        $(document).ready(function () {
            $('.denunciar_debate').modal({
                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                in_duration: 300, // Transition in duration
                out_duration: 200, // Transition out duration
                starting_top: '4%', // Starting top style attribute
                ending_top: '10%', // Ending top style attribute
            });
            $('.denunciar_debates').on('click', function () {
                if ($("input:radio[name=denuncia]:checked").val() != undefined) {
                    $.ajax({
                        url: '<?php echo base_url() ?>novios/comunidad/home/denuncia_debate',
                        method: 'POST',
                        data: {
                            'razon': $("input:radio[name=denuncia]:checked").val(),
                            'debate': <?php echo ! empty($id_debate) ? $id_debate : "" ?>
                        }
                    });
                    $("#modal1").modal("close");
                    $("input:radio[name=denuncia]:checked").prop("checked", false);
                    $(".denunciar_debate").hide();
                }
            });
        });

        $(document).ready(function () {
            $('.modal').modal();
            var comentario = "";
            $("#panel_comentarios").on("click", ".denunciar_c", function (evt) {
                evt.preventDefault();
                comentario = $(this).attr("data-id");
                $("#modal2").modal("open");
            });

            $('.denunciar_comentarios').on('click', function () {
                var token = comentario.split('-');
                if ($("input:radio[name=denuncia_c]:checked").val() != undefined) {
                    $.ajax({
                        url: '<?php echo base_url() ?>novios/comunidad/home/denuncia',
                        method: 'POST',
                        data: {
                            'id_comentario': token[1],
                            'razon': $("input:radio[name=denuncia_c]:checked").val(),
                            'tipo': '1'
                        }
                    });
                    $("#modal2").modal("close");
                    $("input:radio[name=denuncia_c]:checked").prop("checked", false);
                    $("[data-id='" + comentario + "']").hide();
                }
            });
        });

        $(document).ready(function () {
            var activo = 0;
            var height = 0;
            var bandera = true;

            $("input[name=buscar]").keydown(function (evt) {
                var titulo = $("input[name=buscar]").val();
                var key = evt.keyCode || evt.which;
                $("#ventana_buscador").show();
                if (key == 40) {
                    if ($('.item-' + activo).hasClass("hover")) {
                        $('.item-' + activo).removeClass("hover");
                    }
                    activo++;
                    if ($('.item-' + activo).html() != undefined) {
                        $('.item-' + activo).addClass("hover");
                        if (activo > 1) {
                            height = (activo - 1) * $('.item-' + activo).height();
                            if (height > 0) {
                                $("#ventana_buscador").scrollTop(height);
                            }
                        }
                    } else {
                        if (bandera) {
                            activo++;
                            bandera = false;
                        }
                        activo--;
                    }
                } else if (key == 38) {
                    if ($('.item-' + activo).hasClass("hover")) {
                        $('.item-' + activo).removeClass("hover");
                    }
                    activo--;
                    if ($('.item-' + activo).html() != undefined) {
                        $('.item-' + activo).addClass("hover");
                        if (activo >= 0) {
                            height = (activo - 1) * $('.item-' + activo).height();
                            if (height >= 0) {
                                $("#ventana_buscador").scrollTop(height);
                            }
                        }
                    } else {
                        if (!bandera) {
                            activo--;
                            bandera = true;
                        }
                        activo++;
                    }
                } else if (key == 13) {
                    $("#buscador").on('keypress', function (e) {
                        e.preventDefault();
                        return false;
                    });
                    window.location.href = $('.item-' + activo + ' a').attr("href");
                } else if (key != 39 && key != 37) {
                    activo = 0;
                    grupos();
                }
            });

            $("input[name=buscar]").keyup(function (evt) {
                var titulo = $("input[name=buscar]").val();
                var key = evt.keyCode || evt.which;
                $("#ventana_buscador").show();
                if (titulo != "" && key != 38 && key != 40 && key != 39 && key != 37) {
                    activo = 0;
                    debates(titulo);
                } else if (key == 13) {
                    window.location.href = $('.item-' + activo + ' a').attr("href");
                }
            });

            $(document).on('mouseenter', '#ventana_buscador > ul > li', function () {
                $('.item-' + activo).removeClass("hover");
                var clase = $(this).attr("class");
                var token = clase.split("-");
                activo = token[1];
                $(this).addClass("hover");
            });

            $("body").on('click', function () {
                $("#ventana_buscador").hide();
                $('.item-' + activo).removeClass("hover");
            });

            $("input[name=buscar]").on('click', function (e) {
                e.stopPropagation();
                $('.item-' + activo).removeClass("hover");
                activo = 0;
                var titulo = $("input[name=buscar]").val();
                if (titulo == "" && $("#ventana_buscador ul").html() == undefined) {
                    grupos();
                }
                $("#ventana_buscador").show();
            });

            $("#ventana_buscador").on('mouseleave', function () {
                setTimeout(function () {
                    $('.item-' + activo).removeClass("hover");
                }, 1000);
            });

            function grupos() {
                $.ajax({
                    url: '<?php echo base_url()."novios/comunidad/home/getGrupos"?>',
                    success: function (res) {
                        var val = Array();
                        if (res.success) {
                            $("#ventana_buscador").html("");
                            var i = 1;
                            var ul = document.createElement("ul");
                            $(ul).html($("#lista_debates").html());
                            for (var aux in res.data) {
                                if (res.data.hasOwnProperty(aux)) {
                                    var li = document.createElement("li");
                                    $(li).html($("#item_debate").html());
                                    $(li).addClass("item-" + i);
                                    $(li).find(".enlace_debate").attr('href', res.data[aux].enlace_grupo);
                                    $(li).find(".imagen_debate").attr('src', res.data[aux].imagen);
                                    $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                    $(li).find(".fecha_debate").text(res.data[aux].debates + " Debates");
                                    $(ul).append(li);
                                    if (i == 16) {
                                        break;
                                    }
                                    i++;
                                }
                            }
                            $("#ventana_buscador").append($(ul));
                            $("#ventana_buscador").data("grupos", "true");
                            $("#ventana_buscador").show();
                        }
                    }
                });
                $("#item_debate").hide();
                $("#ventana_buscador").empty();
            }

            function debates(titulo) {
                $.ajax({
                    url: '<?php echo base_url()."novios/comunidad/home/buscar" ?>',
                    method: 'post',
                    data: {
                        'titulo_debate': titulo
                    },
                    success: function (res) {
                        if (res.success) {
                            $("#ventana_buscador").html("");
                            var i = 1;
                            var ul = document.createElement("ul");
                            $(ul).html($("#lista_debates").html());
                            var val = Array();
                            for (var aux in res.data) {
                                if (res.data.hasOwnProperty(aux)) {
                                    var li = document.createElement("li");
                                    $(li).html($("#item_debate").html());
                                    $(li).addClass("item-" + i);
                                    $(li).find(".enlace_debate").attr('href', res.data[aux].enlace_debate);
                                    $(li).find(".imagen_debate").attr('src', res.data[aux].foto_usuario);
                                    $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                    $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                    $(ul).append(li);
                                    if (i > (res.data.length - 1)) {
                                        break;
                                    }
                                    i++;
                                }
                            }
                            $("#ventana_buscador").append($(ul));
                            $("#ventana_buscador").show();
                        }
                    }
                });
                $("#item_debate").hide();
                $("#ventana_buscador").empty();
            }
        });
    </script>
    </body>
<?php $this->view('principal/footer'); ?>