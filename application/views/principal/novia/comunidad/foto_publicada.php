<?php $this->view('principal/header'); ?>
<?php $this->view('principal/novia/menu'); ?>
<link href="<?php echo base_url() ?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<body>
<!--BARRA DE TITULO CON UN BUSCADOR DE DEBATES-->
<div class="row">
    <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
        <p>
            <div class="col s12 m2">
        <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
    </div>
    <form id="buscador" method="get" action="<?php echo base_url()."novios/comunidad/forum/buscar" ?>"
          autocomplete="off">
        <div class="input-field col s7 m5" style="margin:0">
            <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
            <input type="hidden" name="page" value="1"/>
            <div id="ventana_buscador">
            </div>
        </div>
        <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
            <button type="submit" class="btn waves-effect waves-light dorado-2"><i class="fa fa-search"
                                                                                   aria-hidden="true"></i> Buscar
            </button>
        </div>
    </form>
    </p>
</div>
</div>

<div class="body-container">
    <div class="row">
        <!--COMPONENTES LATERALES-->
        <?php
        if (empty($usuarios_boda)) {
            $usuarios_boda = "";
        }
        if (empty($visitas_perfil)) {
            $visitas_perfil = "";
        }
        if (empty($grupos_miembro)) {
            $grupos_miembro = "";
        }
        if (empty($grupos)) {
            $grupos = "";
        }
        $data = [
                'usuarios_boda'  => $usuarios_boda,
                'visitas_perfil' => $visitas_perfil,
                'grupos_miembro' => $grupos_miembro,
                'grupos'         => $grupos,
        ];
        $this->view('principal/novia/comunidad/lateral.php', $data);
        ?>
        <div class="col s12 m9 pull-left">
            <div class="row">
                <div class="col s12 m6" style="margin-bottom: 20px;">
                    <h6><b>Publicado en <a style="color: #f9a797!important;" class="grupo"
                                           href="<?php if ( ! empty($url_grupo)) echo $url_grupo ?>"><?php if ( ! empty($grupo)) echo $grupo ?></a></b>
                    </h6>
                </div>
                <div class="col s12 m6" style="float: right">
                    <div class="col s6 m6 z-depth-1" style="text-align: center">
                        <p><i class="fa fa-eye" aria-hidden="true"></i> <?php if ( ! empty($vistas)) {
                                echo $vistas;
                            } else {
                                echo 0;
                            } ?></p>
                        <p>Vistas</p>
                    </div>
                    <div class="col s6 m6 z-depth-1" style="text-align: center">
                        <p><i class="fa fa-comments" aria-hidden="true"></i> <?php if ( ! empty($contador)) {
                                echo $contador;
                            } else {
                                echo 0;
                            } ?></p>
                        <p>Comentarios</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m9 pull-left">
            <div class="row">
                <div class="col s12 m12 grey lighten-5 z-depth-1">
                    <div class="row" style="border-bottom: 1px solid #999;">
                        <h5><?php if ( ! empty($titulo_foto)) echo $titulo_foto ?></h5>
                    </div>
                    <div class="row" style="border-bottom: 1px solid #999; padding: 5px; vertical-align: baseline;">
                        <div class="col s12 m12" style="position: relative;">
                            <div style="height: auto;">
                                <a class="bx-left clickable" <?php if ( ! empty($url_siguiente)) echo "href=".$url_anterior ?>>
                                    <div class="flecha" style="position: relative; top: 40%;"><i
                                                class="fa fa-chevron-left" aria-hidden="true"></i></div>
                                </a>
                                <a class="bx-right clickable" <?php if ( ! empty($url_anterior)) echo "href=".$url_siguiente ?>>
                                    <div class="flecha" style="position: relative; top: 40%;"><i
                                                class="fa fa-chevron-right" aria-hidden="true"></i></div>
                                </a>
                                <p style="text-align: center">
                                    <img class="responsive-img foto_publicada"
                                         style="<?php if ($imagen == "" || $imagen == null) echo "display:none" ?>"
                                         src="<?php if ($imagen != "" && $mime != "") echo "data:".$mime.';'."base64,".base64_encode($imagen) ?>">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="border-bottom: 1px solid #999;">
                        <div class="col s4 m2">
                            <div class="card">
                                <div class="card-image">
                                    <a class="clickable" <?php if ( ! empty($url_perfil)) echo "href='$url_perfil'" ?>>
                                        <img class="foto_usuario"
                                             src="<?php if ( ! empty($foto_usuario)) echo $foto_usuario ?>">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col s8 m10">
                            <p>
                                <a class="usuario" <?php if ( ! empty($url_perfil)) echo "href='$url_perfil'" ?>><?php if ( ! empty($usuario)) echo $usuario ?></a> <?php if ( ! empty($fecha_creacion)) echo $fecha_creacion ?>
                            </p>
                            <?php if ( ! empty($descripcion)) echo $descripcion ?>
                        </div>
                    </div>
                    <div class="row">
                        <button type="button" name="like" id="like" class="btn waves-effect waves-light dorado-2">
                            <?php if ( ! empty($like)) {
                                if ($like == "Me Gusta") {
                                    echo "<i id='manita' class='fa fa-thumbs-up' aria-hidden='true'></i> ";
                                } else {
                                    echo "<i id='manita' class='fa fa-thumbs-down' aria-hidden='true'></i> ";
                                }
                            } ?>
                            <text id="liked"><?php if ( ! empty($like)) echo $like ?></text>
                        </button>
                        <?php if (empty($id_denuncia)) { ?>
                            <a href="#modal1" class="clickable denunciar_foto"><i class="fa fa-minus-circle"
                                                                                  aria-hidden="true"></i> Denunciar</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 m9 pull-left" style="margin-top: 30px;">
            <div class="row">
                <div class="col s12 m6">
                    <h5><?php if ( ! empty($contador) && $contador > 0) echo $contador." Comentarios" ?></h5>
                </div>
            </div>
        </div>
        <!-- -------------------------AREA DE COMENTARIOS--------------------------- -->
        <div id="panel_comentarios" class="col s12 m9 pull-left">
            <?php
            if ( ! empty($comentarios)) {
                foreach ($comentarios as $comentario) {
                    if (empty($comentario->id_boda)) {
                        $comentario->fecha_boda = "";
                    }
                    echo '<div class="row">
                                    <div class="col s4 m2 pull-left" class="comentario_usuario" style="margin-top: -7px; text-align: center">
                                        <div class="card">
                                            <div class="card-image">';
                    echo '<a class="link_usuario1" href="'.base_url().'novios/comunidad/perfil/usuario/'.$comentario->id_usuario.'">';
                    echo '<img class="foto_perfil" src="'.$comentario->foto_usuario.'">
                                                </a>
                                            </div>
                                        </div>';
                    echo '<div style="border-radius: 10px; border: 1px solid wheat; height: 20px;margin-bottom: 20px; background: #FFFFCC">
                                            <p class="truncate" style="color: orange; margin-top: 3px; font-size: 12px"><i class="fa fa-star" aria-hidden="true"></i>'.$comentario->tipo_novia.'<i class="fa fa-star" aria-hidden="true"></i></p>
                                        </div>
                                        <p class="fecha_boda" style="margin: 0 auto; margin-top: 10px">'.$comentario->fecha_boda.'</p>
                                        <p class="estado_boda" style="margin: 0 auto; margin-top: 10px">'.$comentario->estado_boda.'</p>
                                    </div>';
                    echo '<div class="col s8 m10 grey lighten-5 z-depth-1" id="nuevo_comentario" style="float: right;">
                                        <div class="col s12 m12" style="padding-top:10px">
                                            <p ><a class="link_usuario2" style="margin-right:10px;" href="'.base_url().'novios/comunidad/perfil/usuario/'.$comentario->id_usuario.'">'.$comentario->usuario.'</a> <text class="fecha_creacion">'.$comentario->fecha_creacion.'</text> </p>
                                        </div>
                                        <div class="col s12 m12 contenido_comentario">
                                        '.$comentario->comentario.'
                                        </div>
                                        <div>

                                        </div>
                                        <div class="col s12 m12 grey lighten-5 pull-left" style="border-top: 1px solid #999; margin-top:10px; margin-bottom:10px; padding-top:15px; padding-bottom:10px">
                                            <a class="responder_c clickable col s16" data-id="comentario-'.$comentario->id_comentario.'"><i class="fa fa-angle-left" aria-hidden="true"></i> Responder</a>';
                    if ( ! empty($comentarios2)) {
                        foreach ($comentarios2 as $respuesta) {
                            if ($respuesta->comentado == $comentario->id_comentario) {
                                echo '<a class="mostrar clickable col s6" data-id="respuesta-'.$comentario->id_comentario.'">Mostrar Comentarios</a>';
                                break;
                            }
                        }
                    }
                    if (empty($comentario->id_denuncia)) {
                        echo '<a href="#modal2" class="denunciar_c" style="float: right; color:#999" href="#!" data-id="denunciar-'.$comentario->id_comentario.'"><i class="fa fa-minus-circle" aria-hidden="true"></i>Denunciar</a>';
                    }
                    echo '</div>
                                    <div class="col s12 m12 grey lighten-5 pull-left" id="panel-respuesta'.$comentario->id_comentario.'" style="display:none; padding-top: 20px">

                                    </div>
                                    </div>                                
                                    </div>';
                }
            } else {
                echo '<div class="row"> <div class="col s12 m9"><h5 class="existencia_c"> A&uacute;n no existen comentarios... </h5> </div></div>';
            }
            ?>
        </div>
        <!-- ------------------------FIN AREA DE COMENTARIOS--------------------------- -->

        <!-- -----------------------PAGINADOR DE COMENTARIOS--------------------------- -->
        <div class="col s12 m9 pull-left" style="<?php if ($total_paginas <= 1) echo "display: none" ?>">
            <div class="col s12 m6" style="float: right">
                <ul class="pagination">
                    <li class="<?php echo($pagina == 1 ? "disabled" : "waves-effect") ?>">
                        <a <?php if ($pagina > 1) echo 'href="'.base_url()."novios/comunidad/picture/fotoPublicada/$url_foto/".($pagina - 1).'"' ?>><i
                                    class="material-icons">chevron_left</i></a></li>
                    <?php
                    $j        = 1;
                    $contador = 0;
                    if ($total_paginas > 10 && $pagina > 0 && $pagina > 5) {
                        $pagina2 = $pagina + 5;
                        if ($pagina2 <= $total_paginas) {
                            $j = $pagina - 4;
                        } else {
                            $j = $pagina - 4;
                            $j = $j - ($pagina2 - $total_paginas);
                        }
                    } else {
                        $j = 1;
                    }
                    for ($i = $j; $i <= $total_paginas && $contador < 10; $i++) {
                        ?>
                        <li class="<?php echo($i == $pagina ? "active" : "waves-effect") ?>"><a
                                    href="<?php echo base_url()."novios/comunidad/picture/fotoPublicada/$url_foto/$i" ?>"><?php echo $i ?></a>
                        </li>
                    <?php } ?>
                    <li class="<?php echo($pagina == $total_paginas ? "disabled" : "waves-effect") ?>">
                        <a <?php if ($pagina < $total_paginas) echo 'href="'.base_url()."novios/comunidad/picture/fotoPublicada/$url_foto/".($pagina + 1).'"' ?>><i
                                    class="material-icons">chevron_right</i></a></li>
                </ul>
            </div>
        </div>
        <!-- -----------------------FIN PAGINADOR DE COMENTARIOS--------------------------- -->

        <!-- --------------------------EDITOR COMENTARIO---------------------------- -->
        <div class="col s12 m9 pull-left">
            <input type="hidden" id="respuesta">
            <div class="row">
                <div class="col s12 m12" style="margin-bottom: 30px;">
                    <h5><b>Deja aqu&iacute; tu comentario</b></h5>
                </div>
                <div class="col s12 m12 grey lighten-5 z-depth-1">
                    <div class="col s3 m1 pull-left">
                        <div class="row">
                            <div style="text-align: center">
                                <div class="card" style="margin-top: 20px">
                                    <div class="card-image">
                                        <img class="foto_perfil"
                                             src="<?php if ( ! empty($foto_logeado)) echo $foto_logeado ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="post">
                        <div class="col s9 m11" style="margin-top: 20px; margin-bottom: 30px">
                            <div id="comentario">
                                <textarea id="text_area" class="entorno_mensaje" name="mensaje" required> </textarea>
                            </div>
                        </div>
                        <div class="col s9 m11">
                            <div class="row" style="margin-bottom: 60px">
                                <div class="col s12 m12">
                                    <button id="comentar" type="button" class="btn waves-effect waves-light dorado-2"><i
                                                class="fa fa-comment" aria-hidden="true"></i> Comentar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ---------------------------FIN EDITAR COMENTARIO---------------------------- -->


        <!-- ---------------   TEMPLATE NUEVO COMENTARIO ------------------------- -->
        <div class="hide" id="template-comentario">
            <div class="row">
                <div class="col s4 m2 pull-left" class="comentario_usuario"
                     style="margin-top: -7px; text-align: center">
                    <div class="card">
                        <div class="card-image">
                            <a class="link_usuario1" href="#!">
                                <img class="foto_perfil" src="">
                            </a>
                        </div>
                    </div>
                    <div style="border-radius: 10px; border: 1px solid wheat; height: 20px;margin-bottom: 20px; background: #FFFFCC">
                        <p class="truncate" style="color: orange; margin-top: 3px; font-size: 12px"><i
                                    class="fa fa-star" aria-hidden="true"></i>
                            <text class="tipo_novia"></text>
                            <i class="fa fa-star" aria-hidden="true"></i></p>
                    </div>
                    <p class="fecha_boda" style="margin: 0 auto; margin-top: 10px"></p>
                    <p class="estado_boda" style="margin: 0 auto; margin-top: 10px"></p>
                </div>
                <div class="col s8 m10 grey lighten-5 z-depth-1" id="nuevo_comentario" style="float: right;">
                    <div class="col s12 m12" style="padding-top:10px">
                        <p><a class="link_usuario2" style="margin-right:10px;color: #f9a797!important;" href=""></a>
                            <text class="fecha_creacion" style="color: #75767a!important;"></text>
                        </p>
                    </div>
                    <div class="col s12 m12 contenido_comentario">
                    </div>
                    <div>

                    </div>
                    <div class="col s12 m12 grey lighten-5 pull-left"
                         style="border-top: 1px solid #999; margin-top:10px; margin-bottom:10px; padding-top:15px; padding-bottom:10px">
                        <a class="responder_c clickable truncate"><i class="fa fa-angle-left" aria-hidden="true"></i>
                            Responder</a>
                        <a class="denunciar_c truncate" style="float: right; color:#999" href="#modal2" data-id=""><i
                                    class="fa fa-minus-circle" aria-hidden="true"></i>Denunciar</a>
                    </div>
                    <div class="col s12 m12 grey lighten-5 pull-left panel-respuestas">

                    </div>
                </div>
            </div>
        </div>
        <!-- ---------------   TEMPLATE NUEVO COMENTARIO ------------------------- -->
    </div>
</div>
<!-- Modal Structure -->
<div id="modal1" class="modal">
    <div class="modal-content">
        <p>
            <input class="with-gap" name="denuncia" type="radio" id="sexual" value="Contenido sexual"/>
            <label for="sexual">Contenido sexual.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia" type="radio" id="comercial" value="Contenido comercial"/>
            <label for="comercial">Contenido comercial.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia" type="radio" id="inapropiado" value="Contenido inapropiado"/>
            <label for="inapropiado">Contenido inapropiado.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia" type="radio" id="ofensivo" value="Contenido ofensivo"/>
            <label for="ofensivo">Contenido ofensivo.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia" type="radio" id="ilegal" value="Contenido ilegal"/>
            <label for="ilegal">Contenido ilegal.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia" type="radio" id="duplicado" value="Contenido duplicado"/>
            <label for="duplicado">Contenido duplicado.</label>
        </p>
    </div>
    <div class="modal-footer">
        <a class="denunciar_fotos modal-action waves-effect waves-green btn waves-effect waves-light dorado-2">Denunciar</a>
    </div>
</div>
<!-- Modal Structure -->
<div id="modal2" class="modal">
    <div class="modal-content">
        <p>
            <input class="with-gap" name="denuncia_c" type="radio" id="sexual2" value="Contenido sexual"/>
            <label for="sexual2">Contenido sexual.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia_c" type="radio" id="comercial2" value="Contenido comercial"/>
            <label for="comercial2">Contenido comercial.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia_c" type="radio" id="inapropiado2" value="Contenido inapropiado"/>
            <label for="inapropiado2">Contenido inapropiado.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia_c" type="radio" id="ofensivo2" value="Contenido ofensivo"/>
            <label for="ofensivo2">Contenido ofensivo.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia_c" type="radio" id="ilegal2" value="Contenido ilegal"/>
            <label for="ilegal2">Contenido ilegal.</label>
        </p>
        <p>
            <input class="with-gap" name="denuncia_c" type="radio" id="duplicado2" value="Contenido duplicado"/>
            <label for="duplicado2">Contenido duplicado.</label>
        </p>
    </div>
    <div class="modal-footer">
        <a class="denunciar_comentarios modal-action waves-effect waves-green btn waves-effect waves-light dorado-2">Denunciar</a>
    </div>
</div>
<!-- -----------------  TEMPLATE VENTANA BUSCADOR -------------------------- -->
<ul id="lista_debates">
    <li id="item_debate">
        <a class="enlace_debate" href="">
            <div class="row">
                <div class="col s4 m3">
                    <div class="card">
                        <div class="card-image">
                            <img class="imagen_debate">
                        </div>
                    </div>
                </div>
                <div class="col s9 m9">
                    <p class="titulo_debate"></p>
                    <p class="fecha_debate"></p>
                </div>
            </div>
        </a>
    </li>
</ul>
<!-- ------------------  FIN TAMPLETE VENTANA BUSCADOR --------------------- -->
<script type="text/javascript" src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js"></script>
<script>
    $(document).ready(function () {
        $('.bx-left').on('mouseenter', function () {
            $('.fa-chevron-left').css('color', '#404040');
        });
        $('.bx-left').on('mouseleave', function () {
            $('.fa-chevron-left').css('color', 'rgba(96,96,96,0.5)');
        });
        $('.bx-right').on('mouseenter', function () {
            $('.fa-chevron-right').css('color', '#404040');
        });
        $('.bx-right').on('mouseleave', function () {
            $('.fa-chevron-right').css('color', 'rgba(96,96,96,0.5)');
        });
    });

    $(document).ready(function () {
        init_tinymce_mini("#text_area");
    });

    function init_tinymce_mini(elem) {
        if (typeof tinymce != "undefined") {
            tinymce.init({
                selector: elem,
                theme: 'modern',
                menubar: 'false',
                relative_urls: false,
                plugins: [
                    'autolink link directionality'
                ],
                toolbar: 'undo redo | bold | italic | link |  alignleft aligncenter alignright alignjustify ',
                imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58'],
            });
        }
    }

    $(document).ready(function () {
        $("#like").click(function () {
            $.ajax({
                url: "<?php echo base_url()?>novios/comunidad/Picture/likeFoto",
                method: "POST",
                data: {
                    foto: '<?php echo $id_foto ?>',
                },
                success: function (res) {
                    if (res.success) {
                        if (res.data == "Me Gusta") {
                            $('#manita').removeClass('fa-thumbs-down');
                            $('#manita').addClass('fa-thumbs-up');
                        } else {
                            $('#manita').removeClass('fa-thumbs-up');
                            $('#manita').addClass('fa-thumbs-down');
                        }
                        $('#liked').text(res.data);
                    }
                },
                error: function () {
                    //alert('Lo sentimos ocurrio un error');
                }
            });
        });
    });

    $(document).ready(function () {
        $("#comentar").click(function (evt) {
            evt.preventDefault();
            var mensaje = tinyMCE.activeEditor.getContent();
            if (mensaje != "" && mensaje != null) {
                var tipo = $("#respuesta").val();
                if (tipo == "" || tipo == null) {
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/comunidad/picture/comentariosFoto",
                        method: "POST",
                        data: {
                            foto: '<?php echo $id_foto ?>',
                            mensaje: mensaje,
                        },
                        success: function (res) {
                            if (res.success) {
                                var div = document.createElement("div");
                                $(div).html($("#template-comentario").html());
                                $(div).find(".foto_perfil").attr("src", res.data.foto_usuario);
                                $(div).find(".tipo_novia").text(res.data.tipo_novia);
                                $(div).find(".fecha_boda").text(res.data.fecha_boda);
                                $(div).find(".estado_boda").text(res.data.estado_boda);
                                $(div).find(".link_usuario1").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $this->session->userdata('id_usuario') ?>");
                                $(div).find(".link_usuario2").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $this->session->userdata('id_usuario') ?>");
                                $(div).find(".link_usuario2").html(res.data.usuario);
                                $(div).find(".fecha_creacion").text(res.data.fecha_creacion);
                                $(div).find(".contenido_comentario").html(res.data.mensaje);
                                $(div).find(".responder_c").attr("data-id", "comentario-" + res.data.id_comentario);
                                $(div).find(".denunciar_c").attr("data-id", "denunciar-" + res.data.id_comentario);
                                $(div).find(".panel-respuestas").attr("id", "panel-respuesta" + res.data.id_comentario);
                                $(div).find("#panel-respuesta" + res.data.id_comentario).removeClass("panel-respuestas");
                                $("#panel_comentarios").append(div);
                                tinyMCE.activeEditor.setContent("");
                                $(".existencia_c").hide();
                                $("#respuesta").val("");
                            }
                        },
                        error: function () {
                            //alert('Lo sentimos ocurrio un error');
                        }
                    });
                } else {
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/comunidad/picture/respuestaComentario",
                        method: "POST",
                        data: {
                            foto: '<?php echo $id_foto ?>',
                            mensaje: mensaje,
                            respuesta: tipo
                        },
                        success: function (res) {
                            if (res.success) {
                                var conca = "#panel-respuesta" + tipo;
                                var div = document.createElement("div");
                                $(div).html($("#template-comentario").html());
                                $(div).find(".foto_perfil").attr("src", res.data.url_foto);
                                $(div).find(".tipo_novia").text(res.data.tipo_novia);
                                $(div).find(".fecha_boda").text(res.data.fecha_boda);
                                $(div).find(".estado_boda").text(res.data.estado_boda);
                                $(div).find(".link_usuario1").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $this->session->userdata('id_usuario') ?>");
                                $(div).find(".link_usuario2").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/<?php echo $this->session->userdata('id_usuario') ?>");
                                $(div).find(".link_usuario2").html(res.data.usuario);
                                $(div).find(".fecha_creacion").text(res.data.fecha_creacion);
                                $(div).find(".contenido_comentario").html(res.data.comentario);
                                $(div).find(".responder_c").attr("data-id", "comentario-" + tipo);
                                $(div).find(".denunciar_c").attr("data-id", "denunciar-" + res.data.id_comentario);
                                $(conca).prepend(div);
                                tinyMCE.activeEditor.setContent("");
                                $(".existencia_c").hide();
                                $(conca).show();
                                $("#respuesta").val("");
                            }
                        },
                        error: function () {
                            //alert('Lo sentimos ocurrio un error');
                        }
                    });
                }
            }
        });
    });


    $(document).ready(function () {
        $("#panel_comentarios").on("click", ".responder_c", function (evt) {
            evt.preventDefault();
            $('html,body').animate({
                scrollTop: $("#comentario").offset().top
            }, 2000);
            tinymce.execCommand('mceFocus', false, '#text_area');
            var comentario = $(this).attr("data-id");
            var comentario2 = comentario.split("-");
            $("#respuesta").val(comentario2[1]);
        });
    });

    $(document).ready(function () {
        $(".mostrar").click(function (evt) {
            evt.preventDefault();
            var comentario = $(this).attr("data-id");
            var comentario2 = comentario.split("-");
            var conca = "#panel-respuesta" + comentario2[1];
            if ($(this).text() == "Mostrar Comentarios") {
                $.ajax({
                    url: '<?php echo base_url()?>novios/comunidad/picture/getRespuesta',
                    method: 'POST',
                    data: {
                        comentario: comentario2[1]
                    },
                    success: function (res) {
                        if (res.success) {
                            for (var aux in res.data) {
                                if (res.data.hasOwnProperty(aux)) {
                                    var val = res.data[aux];
                                }
                            }
                            var i;
                            for (i = 0; i < val.length; i++) {
                                var div = document.createElement("div");
                                $(div).html($("#template-comentario").html());
                                $(div).find(".foto_perfil").attr("src", val[i].url_foto);
                                $(div).find(".tipo_novia").text(val[i].tipo_novia);
                                $(div).find(".fecha_boda").text(val[i].fecha_boda);
                                $(div).find(".estado_boda").text(val[i].estado_boda);
                                $(div).find(".link_usuario1").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/" + val[i].id_usuario);
                                $(div).find(".link_usuario2").attr("href", "<?php echo base_url() ?>novios/comunidad/perfil/usuario/" + val[i].id_usuario);
                                $(div).find(".link_usuario2").html(val[i].usuario);
                                $(div).find(".fecha_creacion").text(val[i].fecha_creacion);
                                $(div).find(".contenido_comentario").html(val[i].comentario);
                                $(div).find(".responder_c").attr("data-id", "comentario-" + comentario2[1]);
                                $(div).find(".denunciar_c").attr("data-id", "denunciar-" + val[i].id_comentario);
                                if (val[i].id_denuncia != "" && val[i].id_denuncia != null) {
                                    $(div).find(".denunciar_c").hide();
                                } else {
                                    $(div).find(".denunciar_c").attr("data-id", "denunciar-" + val[i].id_comentario);
                                }
                                $(conca).append(div);
                                tinyMCE.activeEditor.setContent("");
                                $(".existencia_c").hide();
                            }
                        }
                    },
                    error: function () {
                        //alert("Lo sentimos ha sucedido un error");
                    }
                });
                $(conca).empty();
                $(conca).show();
                $(conca).css("border-top", "1px solid #999");
                $(this).text("Ocultar Comentarios");
            } else {
                $(this).text("Mostrar Comentarios");
                $(conca).hide();
                $(conca).empty();
            }
        });
    });

    $(document).ready(function () {
        $('.desplegar_grupos').click(function () {
            var mostrar = $(this).text();
            var display = $('.oculto').css('display');
            if (display == "none") {
                $('.oculto').css('display', 'block');
                $(this).text("Ocultar Grupos");
            } else {
                $('.oculto').css('display', 'none');
                $(this).text("M&aacute;s Grupos...");
            }
        });
    });

    $(document).ready(function () {
        $('.denunciar_foto').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5, // Opacity of modal background
            in_duration: 300, // Transition in duration
            out_duration: 200, // Transition out duration
            starting_top: '4%', // Starting top style attribute
            ending_top: '10%', // Ending top style attribute
        });
        $('.denunciar_fotos').on('click', function () {
            if ($("input:radio[name=denuncia]:checked").val() != undefined) {
                $.ajax({
                    url: '<?php echo base_url() ?>novios/comunidad/picture/denuncia',
                    method: 'POST',
                    data: {
                        'razon': $("input:radio[name=denuncia]:checked").val(),
                        'foto': <?php echo ! empty($id_foto) ? $id_foto : "" ?>
                    }
                });
                $("#modal1").modal("close");
                $("input:radio[name=denuncia]:checked").prop("checked", false);
                $(".denunciar_foto").hide();
            }
        });
    });

    $(document).ready(function () {
        $('.modal').modal();

        $('.denunciar_c').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5, // Opacity of modal background
            in_duration: 300, // Transition in duration
            out_duration: 200, // Transition out duration
            starting_top: '4%', // Starting top style attribute
            ending_top: '10%', // Ending top style attribute
        });
        var comentario = "";
        $("#panel_comentarios").on("click", ".denunciar_c", function (evt) {
            evt.preventDefault();
            comentario = $(this).attr("data-id");
            $("#modal2").modal("open");
        });

        $('.denunciar_comentarios').on('click', function () {
            var token = comentario.split('-');
            if ($("input:radio[name=denuncia_c]:checked").val() != undefined) {
                $.ajax({
                    url: '<?php echo base_url() ?>novios/comunidad/home/denuncia',
                    method: 'POST',
                    data: {
                        'id_comentario': token[1],
                        'razon': $("input:radio[name=denuncia_c]:checked").val(),
                        'tipo': '2'
                    }
                });
                $("#modal2").modal("close");
                $("input:radio[name=denuncia_c]:checked").prop("checked", false);
                $("[data-id='" + comentario + "']").hide();
            }
        });
    });

    $(document).ready(function () {
        var activo = 0;
        var height = 0;
        var bandera = true;

        $("input[name=buscar]").keydown(function (evt) {
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if (key == 40) {
                if ($('.item-' + activo).hasClass("hover")) {
                    $('.item-' + activo).removeClass("hover");
                }
                activo++;
                if ($('.item-' + activo).html() != undefined) {
                    $('.item-' + activo).addClass("hover");
                    if (activo > 1) {
                        height = (activo - 1) * $('.item-' + activo).height();
                        if (height > 0) {
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                } else {
                    if (bandera) {
                        activo++;
                        bandera = false;
                    }
                    activo--;
                }
            } else if (key == 38) {
                if ($('.item-' + activo).hasClass("hover")) {
                    $('.item-' + activo).removeClass("hover");
                }
                activo--;
                if ($('.item-' + activo).html() != undefined) {
                    $('.item-' + activo).addClass("hover");
                    if (activo >= 0) {
                        height = (activo - 1) * $('.item-' + activo).height();
                        if (height >= 0) {
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
                } else {
                    if (!bandera) {
                        activo--;
                        bandera = true;
                    }
                    activo++;
                }
            } else if (key == 13) {
                $("#buscador").on('keypress', function (e) {
                    e.preventDefault();
                    return false;
                });
                window.location.href = $('.item-' + activo + ' a').attr("href");
            } else if (key != 39 && key != 37) {
                activo = 0;
                grupos();
            }
        });

        $("input[name=buscar]").keyup(function (evt) {
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if (titulo != "" && key != 38 && key != 40 && key != 39 && key != 37) {
                activo = 0;
                debates(titulo);
            } else if (key == 13) {
                window.location.href = $('.item-' + activo + ' a').attr("href");
            }
        });

        $(document).on('mouseenter', '#ventana_buscador > ul > li', function () {
            $('.item-' + activo).removeClass("hover");
            var clase = $(this).attr("class");
            var token = clase.split("-");
            activo = token[1];
            $(this).addClass("hover");
        });

        $("body").on('click', function () {
            $("#ventana_buscador").hide();
            $('.item-' + activo).removeClass("hover");
        });

        $("input[name=buscar]").on('click', function (e) {
            e.stopPropagation();
            $('.item-' + activo).removeClass("hover");
            activo = 0;
            var titulo = $("input[name=buscar]").val();
            if (titulo == "" && $("#ventana_buscador ul").html() == undefined) {
                grupos();
            }
            $("#ventana_buscador").show();
        });

        $("#ventana_buscador").on('mouseleave', function () {
            setTimeout(function () {
                $('.item-' + activo).removeClass("hover");
            }, 1000);
        });

        function grupos() {
            $.ajax({
                url: '<?php echo base_url()."novios/comunidad/home/getGrupos"?>',
                success: function (res) {
                    var val = Array();
                    if (res.success) {
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        for (var aux in res.data) {
                            if (res.data.hasOwnProperty(aux)) {
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-" + i);
                                $(li).find(".enlace_debate").attr('href', res.data[aux].enlace_grupo);
                                $(li).find(".imagen_debate").attr('src', res.data[aux].imagen);
                                $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                $(li).find(".fecha_debate").text(res.data[aux].debates + " Debates");
                                $(ul).append(li);
                                if (i == 16) {
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").data("grupos", "true");
                        $("#ventana_buscador").show();
                    }
                }
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }

        function debates(titulo) {
            $.ajax({
                url: '<?php echo base_url()."novios/comunidad/home/buscar" ?>',
                method: 'post',
                data: {
                    'titulo_debate': titulo
                },
                success: function (res) {
                    if (res.success) {
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        var val = Array();
                        for (var aux in res.data) {
                            if (res.data.hasOwnProperty(aux)) {
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-" + i);
                                $(li).find(".enlace_debate").attr('href', res.data[aux].enlace_debate);
                                $(li).find(".imagen_debate").attr('src', res.data[aux].foto_usuario);
                                $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                $(ul).append(li);
                                if (i > (res.data.length - 1)) {
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").show();
                    }
                }
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
    });
</script>
</body>
<?php $this->view('principal/footer'); ?>

