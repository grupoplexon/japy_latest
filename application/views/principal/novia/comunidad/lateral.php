<div class="col s12 m3 lateral" style="float: right; margin-bottom: 20px">
    <!------------------------ IMAGEN DE PUBLICIDAD --------------------->
    <div class="row">
        <div class="card">
            <div class="card-image">
                <img style="height: auto !important;" src="<?php echo base_url() ?>dist/images/comunidad/organizador-boda2.jpg">
            </div>
            <div class="card-content" style="text-align: center">
                <p style="font-size: 16px; padding: 10px;margin-bottom: 10px; line-height: 20px" class="grey-text">Descubre lo f&aacute;cil y r&aacute;pido que es organizar tu boda en clubnupcial.com</p>
                <p><a href="<?php echo base_url() ?>home/planeador-bodas" class="btn waves-effect waves-light dorado-2">Empieza ya</a></p>
            </div>
        </div>
    </div>

    <?php if(!empty($usuarios_boda->novios)){ ?>
    <!--FECHA DE MI BODA CON LOS USUARIOS QUE COINCIDEN-->
    <div class="row">
        <ul class="collection with-header">
            <li class="collection-header texto_lateral grey lighten-5" style="text-align: center">
                <p class="coincidencia"><?php echo !empty($usuarios_boda->fecha_boda)? $usuarios_boda->fecha_boda : "" ?></p>
                <p class="coincidencia"><?php echo !empty($usuarios_boda->estado_boda)? $usuarios_boda->estado_boda : "" ?></p>
            </li>
            <li  class="collection-item texto_lateral dorado-2" style="text-align: center">
                <p class="coincidencia"><?php echo !empty($usuarios_boda->novios)? $usuarios_boda->novios : ""; 
                if($usuarios_boda->novios > 1) echo ' novios se casan el mismo dia que tu'; else echo ' novio se casa el mismo dia que tu'?> </p>
            </li>
            <li class="collection-item texto_lateral grey lighten-5 col s12 m12" style="text-align: center;">
                <div class="col s4 m4">
                    <div class="card">
                        <div class="card-image">
                            <img style="width: 100%" src="<?php echo !empty($usuarios_boda->foto_usuario)? $usuarios_boda->foto_usuario : "" ?>">
                        </div>
                    </div>
                </div>
                <div class="col s12 m12">
                    <a href="<?php echo base_url() ?>novios/comunidad/Home/conocerCompaneros" class="btn dorado-2 truncate" style="margin-top: 10px; margin-bottom: 10px"> Conocelos </a>
                </div>    
            </li>
        </ul>
    </div>
    <?php } ?>

    <?php if(!empty($visitas_perfil)){ ?>
    <!--QUIEN VISITA MI PERFIL, CON LOS USUARIOS QUE LO HACEN-->
    <div class="row">
        <ul class="collection with-header">
            <li class="collection-header visita_perfil dorado-2">
                <h5> ¿Quien Visita Tu Perfil? </h5>
            </li>
            <li class="collection-item visita_perfil grey lighten-5">
                <div class="row">
                <?php 
                        foreach ($visitas_perfil as $visita){
                ?>
                <a href="<?php echo !empty($visita->url_usuario)? $visita->url_usuario : "" ?>">
                    <div class="col s4 m4">
                        <div class="card">
                            <div class="card-image">
                                <img style="width: 100%" src="<?php echo !empty($visita->foto_usuario)? $visita->foto_usuario : "" ?>">
                            </div>
                        </div>
                    </div>
                </a>
                        <?php } ?>
                </div>
            </li>
            <li class="collection-item visita_perfil grey lighten-5">
                <div class="row">
                    <a href="<?php echo !empty($visita->todos)? $visita->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                </div>
            </li>
        </ul>
    </div>
    <?php } ?>
    
    <?php if(!empty($medalla)){ ?>
    <div class="row">
        <ul class="collection with-header">
            <li class="collection-header dorado-2">
                <h5>Mis Medallas</h5>
            </li>
            <li class="collection-item grey lighten-5">
                <p style="text-align: justify; line-height: 25px;"><b>¿A&uacute;n no conoces tus medallas? <br/>Por haber compartido tus publicaciones, disfruta de tu recompensa.</b></p>
            </li>
            <li class="collection-item grey lighten-5">
                <a class="grey-text darken-4" href="<?php echo base_url()?>novios/comunidad/perfil/medallas/<?php echo $this->session->userdata('id_usuario') ?>">Ver medallas</a>
            </li>
        </ul>
    </div>
    <?php } ?>
    <?php if(!empty($grupos_miembro)){ ?>
    <!--MIS GRUPOS-->
    <div class="row">
        <ul class="collection with-header">
            <li class="collection-header mis_grupos dorado-2">
                <h5> Mis Grupos </h5>
            </li>
            <li class="collection-item mis_grupos grey lighten-5">
                <?php foreach ($grupos_miembro as $grupo2){ ?>
                <a href="<?php echo base_url()."novios/comunidad/group/grupo/$grupo2->id_grupos_comunidad/todo"?>">
                <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo2->imagen"?>"> 
                <p class="nombre_grupo truncate"> <?php echo !empty($grupo2->nombre)? $grupo2->nombre : "" ?> </p>
                </a>
                <?php } ?>
            </li>
        </ul>
    </div>
    <?php 
    } 
    ?>

    <?php if(!empty($grupos)){ ?>
    <!--GRUPOS GENERALES-->
    <div class="row">
        <ul class="collection with-header">
            <li class="collection-header dorado-2"><h5>Grupos Generales</h5></li>
            <?php
            $i = 0;
            foreach ($grupos as $grupo2){
                if($i > 15) break;
                if($i < 5){
            ?>
            <li class="collection-item grey lighten-5">
                <a href="<?php if(!empty($grupo2->id_grupos_comunidad)) echo base_url()."novios/comunidad/Group/grupo/$grupo2->id_grupos_comunidad/todo" ?>">
                    <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo2->imagen"?>"> 
                    <p class="nombre_grupo truncate"> <?php echo !empty($grupo2)? $grupo2->nombre : "" ?> </p>
                </a>
            </li>
            <?php 
                }else{
            ?>
            <li class="collection-item oculto grey lighten-5">
                <a href="<?php if(!empty($grupo2->id_grupos_comunidad)) echo base_url()."novios/comunidad/Group/grupo/$grupo2->id_grupos_comunidad/todo" ?>">
                    <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo2->imagen"?>"> 
                    <p class="nombre_grupo truncate"> <?php echo !empty($grupo2)? $grupo2->nombre : "" ?> </p>
                </a>
            </li>
            <?php
                }
                $i++;
            } ?>
            <li class="collection-item grey lighten-5" style="padding-top: 17px;">
                <a class="clickable desplegar_grupos" style="color: #999; padding-left: 30px"> M&aacute;s Grupos... </a>
            </li>
        </ul>
    </div> 
    <?php 
    } 
    ?>
</div>
