<?php $this->view('principal/header'); ?>
<?php $this->view('principal/novia/menu'); ?>
<style>
    .entorno_mensaje {
        height: 300px;
        width: 100%;
        margin-bottom: 50px;
    }

    .fa-ban {
        color: red;
    }
</style>
<body>
<div class="body-container">
    <div class="row">
        <div class="col s12 m3" style="float: right">
            <ul class="collection with-header">
                <li class="collection-header dorado-2">Normas de la Comunidad</li>
                <li class="collection-item grey lighten-5"><b>Participa Correctamente</b></li>
                <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes
                    contenido ofensivo
                </li>
                <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes
                    contenido comercial
                </li>
                <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes
                    contenido ilegal
                </li>
                <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No insultes a
                    ningun usuario o empresa
                </li>
            </ul>
        </div>
        <div class="col s12 m9 pull-left">
            <div class="row">
                <h5> Nueva Foto </h5>
            </div>
            <div class="row">
                <form id="publicar_foto" method="post"
                      action="<?php echo base_url() ?>novios/comunidad/picture/publicarFoto">
                    <div class="col s12 m12 grey lighten-5 z-depth-1">
                        <div class="card-panel light-blue lighten-5">
                            <p>Agrega t&uacute; foto al grupo que consideres adecuado.</p>
                        </div>
                        <div class="row">
                            <div class="col s3 m2" style="margin-top: 15px">
                                <p> Grupos </p>
                            </div>
                            <div class="input-field col s9 m6 l4">
                                <select id="grupos" name="grupos" class="browser-default form-control" required>
                                    <option></option>
                                    <option value="0">-- Grupos Generales --</option>
                                    <?php
                                    $i = 1;
                                    foreach ($grupos as $grupo) {
                                        if ($i == 17) {
                                            echo '<option value="0">-- Grupos por Estados --</option>';
                                        }
                                        if ( ! empty($id_grupo) && $grupo->id_grupos_comunidad == $id_grupo) {
                                            echo '<option value="'.$grupo->id_grupos_comunidad.'" selected> '.$grupo->nombre.' </option>';
                                        } else {
                                            echo '<option value="'.$grupo->id_grupos_comunidad.'"> '.$grupo->nombre.' </option>';
                                        }
                                        $i++;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s3 m2" style="margin-top: 15px">
                                <p>T&iacute;tulo</p>
                            </div>
                            <div class="col s9 m8">
                                <div class="row">
                                    <div class="chip red darken-1 white-text right titulo_de_video"
                                         style="margin-bottom:20px; display:none;">T&iacute;tulo demasiado largo.
                                    </div>
                                </div>
                                <div class="input-field">
                                    <input id="titulo" name="titulo" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="chip red darken-1 white-text right imagen_danada"
                                     style="margin-bottom:20px; display:<?php if (isset($mensaje)) {
                                         echo "block";
                                     } else echo "none" ?>"><?php if (isset($mensaje)) echo "Archivo dañado o no aceptado" ?></div>
                                <div class="chip red darken-1 white-text right tamanio"
                                     style="margin-bottom:20px; display:none">El archivo es demasiado grande
                                </div>
                            </div>
                            <div class="col s3 m2">
                                <p>Foto:</p>
                            </div>
                            <div class="col s9 m10">
                                <div class="file-field input-field">
                                    <div class="btn dorado-2">
                                        <span>File</span>
                                        <input id="file-ajax" type="file" accept="image/*" required="">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Upload one file"
                                               required="">
                                    </div>
                                </div>
                                <input type="text" value="" id="file-data" name="foto" style="display: none">
                                <div class="imagen">
                                    <img id="img-preview" class="responsive-img"/>
                                    <div class="modal-image-cargando">
                                        <i class="fa fa-spinner fa-pulse fa-2x"></i> SUBIENDO ...
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="chip red darken-1 white-text right descripcion_larga"
                                     style="margin-bottom:20px; display:none;">Descripci&oacute;n demasiado larga.
                                </div>
                            </div>
                            <div class="row">
                                <div class="chip red darken-1 white-text right descripcion"
                                     style="margin-bottom:20px; display:none;">Agrega una descripci&oacute;n.
                                </div>
                            </div>
                            <div class="col s3 m2" style="margin-top: 0px">
                                <p>Descripci&oacute;n</p>
                            </div>
                            <div class="col s9 m10" style="margin-bottom: 50px">
                                <div style="min-height: 300px; max-height: 500px">
                                    <textarea id="text_area" class="entorno_mensaje" name="contenido"
                                              required> </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12">
                                <button type="submit" name="publicar" id="publicar"
                                        class="waves-light waves-effect btn dorado-2" style="float: right"> Publicar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        init_tinymce_mini("#text_area");
    });

    //        FUNCION DE CREACION DEL EDITOR
    function init_tinymce_mini(elem) {
        if (typeof tinymce != "undefined") {
            tinymce.init({
                selector: elem,
                theme: 'modern',
                menubar: 'false',
                relative_urls: false,
                plugins: [
                    'autolink link directionality'
                ],
                toolbar: 'undo redo | bold | italic | link |  alignleft aligncenter alignright alignjustify',
                imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58'],
                browser_spellcheck: true,
                file_picker_callback: fileBrowser,
            });
        }
    }

    $(document).ready(function () {
        $('.modal').modal();
        $("#publicar").on('click', function (e) {
            var imagen = $('#img-preview').attr('src');
            var bandera = true;
            if ($('#grupos').val() == "" || $('#grupos').val() == 0) {
                bandera = false;
                $('#grupos').val("");
            }
            if ($('#titulo').val() == "" && bandera) {
                bandera = false;
                $("#titulo").focus();
            }
            if ($('#titulo').val().length > 254 && bandera) {
                $(".titulo_de_video").show();
                bandera = false;
            } else {
                $(".titulo_de_video").hide();
            }
            if (imagen == undefined || imagen == "" && bandera) {
                bandera = false;
                $("#img-preview").focus();
            }
            if (tinyMCE.activeEditor.getContent() == "" && bandera) {
                bandera = false;
                if ($('.descripcion').css('display') == 'none') {
                    $('.descripcion').show();
                }
                tinymce.execCommand('mceFocus', false, '#text_area');
            } else if (tinyMCE.activeEditor.getContent().length > 65534) {
                $('.descripcion_larga').show();
                bandera = false;
            } else {
                if ($('.descripcion').css('display') == 'block') {
                    $('.descripcion').hide();
                }
                $('.descripcion_larga').hide();
            }
            if (!bandera) {
                $('#publicar_foto').on('submit', function (e) {
                    e.preventDefault();
                });
            } else {
                document.getElementById("publicar_foto").submit();
            }
        });
    });

    $(document).ready(function () {
        $(".modal-image-cargando").hide();
        $('select').material_select();
        $("#btn-publicar").on("click", function () {
            save(true)
        });
        $("#btn-borrador").on("click", function () {
            save(false)
        });

        //SUBE LA IMAGEN AL SERVIDOR Y LA GUARDA EN LA BASE DE DATOS
        $("#modal-aceptar").on("click", function () {
            var action = $(".modal .tab .active").html().toUpperCase();
            if (action.indexOf("SUBIR") != -1) {
                $(".modal-image-cargando").show();
                var data = $("#file-data").val();
                if (data != null && data != "") {
                    $.ajax({
                        url: "<?php echo $this->config->base_url() ?>novios/comunidad/Home/subir/imagen",
                        method: "POST",
                        data: {
                            archivo: $("#file-data").val()
                        },
                        success: function (res) {
                            if (res.success) {
                                window.callback(res.data, {alt: 'alt'});
                                $('#modal-file-browser').modal("close");
                                $(".mce-window").css({"display": "block"});
                            }
                            $(".modal-image-cargando").hide();
                            $("#file-data").val("");
                            $('#img-preview').get(0).src = "";
                            $(".file-path validate").val();
                        },
                        error: function () {
                            $(".modal-image-cargando").hide();
                        }
                    });
                }
            } else if (action.indexOf("IMAGENES") != -1) {
                var url = $(".contenedor-imagenes .active .card-image").get(0).style["background-image"];
                var url = url.replace("url(\"", "").replace("\")", "");
                window.callback(url, {alt: 'alt'});
                $('#modal-file-browser').modal("close");
                $(".mce-window").css({"display": "block"});
            }
        });
        $("#file-ajax").on("change", function () {
            var preview = $('#img-preview').get(0);
            var file = $("#file-ajax").get(0).files[0];
            var reader = new FileReader();
            reader.onloadend = function () {
                preview.src = reader.result;
                $("#file-data").val(reader.result);
            }
            if (file) {
                if (file.size < 1024000) {
                    reader.readAsDataURL(file);
                    $(".tamanio").hide();
                } else {
                    file = "";
                    $("#file-ajax").val("");
                    $(".tamanio").show();
                }
            } else {
                preview.src = "";
            }
        });
        $("#tags").on("change", function (evt) {
            var chip = '<div class="chip truncate" style="max-width:120px" data-id="NUD" data-nombre="RENOM"  id="chip-ID"><i class="material-icons">close</i>NOMBRE</div>';
            var id = $("#tags").val();
            var nombre = $("#tags #cat-" + id).html();
            $("#tags #cat-" + id).remove();
            chip = chip.replace("ID", id).replace("NOMBRE", nombre).replace("RENOM", nombre).replace("NUD", id);
            $('select').material_select('destroy');
            $('select').material_select();
            $("#tags-chips").append(chip);
            $("#chip-" + id + " i").on("click", function (evt) {
                var e = $(evt.currentTarget.parentNode);
                $("#tags").append('<option id="cat-' + e.attr("data-id") + '" value="' + e.attr("data-id") + '">' + e.attr("data-nombre") + '</option>');
                $('select').material_select('destroy');
                $('select').material_select();
            })
        });
        $("#seleccionar-imagen").on("click", function () {
            fileBrowser(function (url) {
                var a = url.split("/");
                $("#img-destacada").attr("src", url);
                $("#img-destacada").attr("data-id", a[a.length - 1]);
                console.log(a[a.length - 1], url);
            }, "url", {filetype: "image"})
        })
    });

    //BUSCADOR DE IMAGENES
    function fileBrowser(callback, value, meta) {
        if (meta.filetype == 'image') {
            if ($(".contenedor-imagenes").children().length <= 1) {
                $.ajax({
                    url: "<?php echo $this->config->base_url() ?>novios/comunidad/Home/imagen",
                    success: function (res) {
                        $("#sin-imagenes").hide();
                        var template = ' <div class="card hoverable " style="background: black;"><div class="card-image image-galeria"></div></div>';
                        for (var i in res.data) {
                            var imagen = res.data[i];
                            var div = document.createElement("div");
                            var $div = $(div)
                            $div.addClass("col s6 m3")
                            $div.html(template);
                            $div.find(".image-galeria").css("background-image", "url(" + imagen.url + ")");
                            $div.on("click", function (evt) {
                                $(".contenedor-imagenes .active").removeClass("active");
                                var $elm = $(evt.currentTarget).find(".card");
                                $elm.addClass("active");
                            })
                            $(".contenedor-imagenes").append($div);
                        }
                    }
                });
            }
            window.callback = callback;
            $('#modal-file-browser').modal({
                dismissible: true,
                opacity: .5,
                in_duration: 100,
                out_duration: 100,
                ready: function () {
                    $(".mce-window").css({"display": "none"});
                },
                complete: function () {
                    $(".mce-window").css({"display": "block"});
                }
            });
            $("#modal-file-browser").css({"z-index": 999999});
        }
        return false;
    }

</script>
</body>
<?php $this->view('principal/footer'); ?>

