<?php $this->view('principal/header'); ?>
<?php $this->view('principal/novia/menu'); ?>
<style>
    .menu_botones {
        position: relative;
        border: 1px solid #C0C0C0;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom: 0px;
        padding: 0px;
        height: auto;
    }

    .barra_botones {
        padding: 0;
        margin: 0;
        border: 0px;
        height: auto;
        width: 40px;
        display: inline-table;
        margin-right: -4px;
    }

    .botones {
        position: relative;
        height: 50px;
        width: 50px;
        border: 0px;
        background: #F2F2F2;
        border: 1px solid #C0C0C0;
        border-color: transparent #C0C0C0 #C0C0C0 transparent;
        white-space: nowrap;
    }

    .botones:hover {
        background: #E0E0E0;
    }

    .entorno_mensaje {
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        margin-top: -1px;
        z-index: 10;
        background: white;
        width: 100%;
        border: 1px solid #C0C0C0;
        min-height: 300px;
        max-height: 500px;
        resize: none;
    }

    .collection-item i {
        color: red;
    }
</style>
<body>
<div class="body-container">
    <div class="row">
        <div class="col s12 m3" style="float: right">
            <ul class="collection with-header">
                <li class="collection-header dorado-2">Normas de la Comunidad</li>
                <li class="collection-item grey lighten-5"><b>Participa Correctamente</b></li>
                <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes
                    contenido ofensivo
                </li>
                <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes
                    contenido comercial
                </li>
                <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes
                    contenido ilegal
                </li>
                <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No insultes a
                    ningun usuario o empresa
                </li>
            </ul>
        </div>
        <div class="col s12 m9 pull-left">
            <div class="row">
                <h5> Nuevo Debate </h5>
            </div>
            <div class="row">
                <form id="publicar_debate" method="post"
                      action="<?php echo base_url() ?>novios/comunidad/Home/publicarDebate">
                    <div class="col s12 m12 grey lighten-5 z-depth-1">
                        <div class="card-panel light-blue lighten-5">
                            <p>T&uacute; pregunta se abrir&aacute; en el grupo que selecciones.</p>
                            <p>De esta manera podr&aacute;n responderte y tambi&eacute;n recibir&aacute;s la ayuda de
                                otras novias del grupo.</p>
                        </div>
                        <div class="row">
                            <div class="col s3 m2" style="margin-top: 15px">
                                <p> Grupos </p>
                            </div>
                            <div class="input-field col s9 m6 l4">
                                <select id="grupos" name="grupos" class="browser-default form-control" required>
                                    <option></option>
                                    <option value="0">-- Grupos Generales --</option>
                                    <?php
                                    $i = 1;
                                    foreach ($grupos as $grupo) {
                                        if ($i == 17) {
                                            echo '<option value="0">-- Grupos por Estados --</option>';
                                        }
                                        if ( ! empty($id_grupo) && $id_grupo == $grupo->id_grupos_comunidad) {
                                            echo '<option value="'.$grupo->id_grupos_comunidad.'" selected> '.$grupo->nombre.' </option>';
                                        } else {
                                            echo '<option value="'.$grupo->id_grupos_comunidad.'"> '.$grupo->nombre.' </option>';
                                        }
                                        $i++;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s3 m2" style="margin-top: 15px">
                                <p>T&iacute;tulo</p>
                            </div>
                            <div class="col s9 m8">
                                <div class="row">
                                    <div class="chip red darken-1 white-text right titulo_de_video"
                                         style="margin-bottom:20px; display:none;">T&iacute;tulo demasiado largo.
                                    </div>
                                </div>
                                <div class="input-field">
                                    <input id="titulo" name="titulo" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="chip red darken-1 white-text right descripcion_larga"
                                     style="margin-bottom:20px; display:none;">Debate demasido grande.
                                </div>
                            </div>
                            <div class="row">
                                <div class="chip red darken-1 white-text right descripcion"
                                     style="margin-bottom:20px; display:none;">Agrega contenido al debate.
                                </div>
                            </div>
                            <div class="col s3 m2" style="margin-top: 0px">
                                <p>Mensaje</p>
                            </div>
                            <div class="col s9 m10" style="margin-bottom: 50px">
                                <div style="min-height: 300px; max-height: 500px">
                                    <textarea id="text_area" class="entorno_mensaje"
                                              name="contenido"><?php echo empty($dudas) ? "" : $dudas ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12">
                                <button type="submit" name="publicar" id="publicar"
                                        class="waves-light waves-effect btn dorado-2" style="float: right"> Publicar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="z-index: 999999;">
    <div id="modal-file-browser" class="modal  modal-fixed-footer" style="">
        <div class="modal-content" style="padding-left: 0px;padding-right: 0px;">
            <h4>Buscar Imagen</h4>
            <div class="row nowrap">
                <div class="col s12">
                    <ul class="tabs">
                        <!--                            <li class="tab col s3"><a class="active" href="#test1">Imagenes</a></li>-->
                        <li class="tab col s3"><a href="#test2">Subir Imagen</a></li>
                    </ul>
                </div>
                <!--                    <div id="test1" class="col s12">
                                        <div class="container">
                                            <div id="sin-imagenes" class="card-panel teal">
                                                <span class="white-text">A&uacute;n no tienes imagenes cargadas.
                                                </span>
                                            </div>
                                            <div class="row contenedor-imagenes">
                                            </div>
                                        </div>
                                    </div>-->
                <div id="test2" class="col s12">
                    <div class="container">
                        <div class="row">
                            <div class="chip red darken-1 white-text right tamanio"
                                 style="margin-bottom:20px; display:none;">Imagen no valdia o demasiado grande.
                            </div>
                        </div>
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>File</span>
                                <input id="file-ajax" type="file" accept="image/*">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Upload one or more files">
                            </div>
                        </div>
                        <input type="text" value="" id="file-data" style="display: none">
                        <div class="modal-imagen">
                            <img id="img-preview" class="responsive-img"/>
                            <div class="modal-image-cargando">
                                <i class="fa fa-spinner fa-pulse fa-2x"></i> SUBIENDO ...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
            <a id="modal-aceptar" href="#!" class=" modal-action waves-effect waves-green btn-flat">Aceptar</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        init_tinymce_mini("#text_area");
    });

    $(document).ready(function () {
        $('.modal').modal();
        $(".modal-image-cargando").hide();
        $('select').material_select();
        $("#btn-publicar").on("click", function () {
            save(true)
        });
        $("#btn-borrador").on("click", function () {
            save(false)
        });

        //SUBE LA IMAGEN AL SERVIDOR Y LA GUARDA EN LA BASE DE DATOS
        $("#modal-aceptar").on("click", function () {
            var action = $(".modal .tab .active").html().toUpperCase();
            if (action.indexOf("SUBIR") != -1) {
                $(".modal-image-cargando").show();
                var data = $("#file-data").val();
                if (data != null && data != "") {
                    $.ajax({
                        url: "<?php echo $this->config->base_url() ?>novios/comunidad/Home/subir/imagen",
                        method: "POST",
                        data: {
                            archivo: $("#file-data").val()
                        },
                        success: function (res) {
                            if (res.success) {
                                console.log(res);
                                window.callback(res.data, {alt: 'alt'});
                                $('#modal-file-browser').modal("close");
                                $(".mce-window").css({"display": "block"});
                            }
                            $(".modal-image-cargando").hide();
                            $("#file-data").val("");
                            $('#img-preview').get(0).src = "";
                            $(".file-path validate").val();
                        },
                        error: function () {
                            $(".modal-image-cargando").hide();
                        }
                    });
                }
            } else if (action.indexOf("IMAGENES") != -1) {
                var url = $(".contenedor-imagenes .active .card-image").get(0).style["background-image"];
                var url = url.replace("url(\"", "").replace("\")", "");
                window.callback(url, {alt: 'alt'});
                $('#modal-file-browser').modal("close");
                $(".mce-window").css({"display": "block"});
            }
        });

        $("#file-ajax").on("change", function () {
            var preview = $('#img-preview').get(0);
            var file = $("#file-ajax").get(0).files[0];
            var reader = new FileReader();
            reader.onloadend = function () {
                preview.src = reader.result;
                $("#file-data").val(reader.result);
            }
            if (file) {
                if (file.size < 1024000) {
                    reader.readAsDataURL(file);
                    $(".tamanio").hide();
                } else {
                    file = "";
                    $("#file-ajax").val("");
                    $(".tamanio").show();
                }
            } else {
                preview.src = "";
            }
        });

        $("#tags").on("change", function (evt) {
            var chip = '<div class="chip truncate" style="max-width:120px" data-id="NUD" data-nombre="RENOM"  id="chip-ID"><i class="material-icons">close</i>NOMBRE</div>';
            var id = $("#tags").val();
            var nombre = $("#tags #cat-" + id).html();
            $("#tags #cat-" + id).remove();
            chip = chip.replace("ID", id).replace("NOMBRE", nombre).replace("RENOM", nombre).replace("NUD", id);
            $('select').material_select('destroy');
            $('select').material_select();
            $("#tags-chips").append(chip);
            $("#chip-" + id + " i").on("click", function (evt) {
                var e = $(evt.currentTarget.parentNode);
                $("#tags").append('<option id="cat-' + e.attr("data-id") + '" value="' + e.attr("data-id") + '">' + e.attr("data-nombre") + '</option>');
                $('select').material_select('destroy');
                $('select').material_select();
            })
        });

        $("#seleccionar-imagen").on("click", function () {
            fileBrowser(function (url) {
                var a = url.split("/");
                $("#img-destacada").attr("src", url);
                $("#img-destacada").attr("data-id", a[a.length - 1]);
                console.log(a[a.length - 1], url);
            }, "url", {filetype: "image"})
        })
    });

    //        FUNCION DE CREACION DEL EDITOR
    function init_tinymce_mini(elem) {
        if (typeof tinymce != "undefined") {
            tinymce.init({
                selector: elem,
                theme: 'modern',
                menubar: 'false',
                relative_urls: false,
                plugins: [
                    'autolink link image directionality emoticons'
                ],
                toolbar: 'undo redo | bold | italic | link |  alignleft aligncenter alignright alignjustify | image | emoticons',
                imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58'],
                browser_spellcheck: true,
                file_picker_callback: fileBrowser,
            });
        }
    }

    //BUSCADOR DE IMAGENES
    function fileBrowser(callback, value, meta) {
        if (meta.filetype == 'image') {
            if ($(".contenedor-imagenes").children().length <= 1) {
                $.ajax({
                    url: "<?php echo $this->config->base_url() ?>novios/comunidad/Home/imagen",
                    success: function (res) {
                        $("#sin-imagenes").hide();
                        var template = ' <div class="card hoverable " style="background: black;"><div class="card-image image-galeria"></div></div>';
                        for (var i in res.data) {
                            var imagen = res.data[i];
                            var div = document.createElement("div");
                            var $div = $(div)
                            $div.addClass("col s6 m3")
                            $div.html(template);
                            $div.find(".image-galeria").css("background-image", "url(" + imagen.url + ")");
                            $div.on("click", function (evt) {
                                $(".contenedor-imagenes .active").removeClass("active");
                                var $elm = $(evt.currentTarget).find(".card");
                                $elm.addClass("active");
                            })
                            $(".contenedor-imagenes").append($div);
                        }
                    }
                });
            }
            window.callback = callback;
            $('#modal-file-browser').modal({
                dismissible: true,
                opacity: .5,
                in_duration: 100,
                out_duration: 100,
                ready: function () {
                    $(".mce-window").css({"display": "none"});
                },
                complete: function () {
                    $(".mce-window").css({"display": "block"});
                }
            });
            $("#modal-file-browser").css({"z-index": 999999});
        }
        return false;
    }

    $(document).ready(function () {
        $("#publicar").on('click', function () {
            var bandera = true;
            if ($("#grupos").val() == 0 || $("#grupos").val() == "") {
                $("#grupos").val("");
                bandera = false;
            }

            if ($("#titulo").val() == "" && bandera) {
                bandera = false;
            }
            if ($('#titulo').val().length > 254 && bandera) {
                $(".titulo_de_video").show();
                bandera = false;
            } else {
                $(".titulo_de_video").hide();
            }
            if (tinyMCE.activeEditor.getContent() == "" && bandera) {
                bandera = false;
                if ($('.descripcion').css('display') == 'none') {
                    $('.descripcion').show();
                }
                tinymce.execCommand('mceFocus', false, '#text_area');
            } else if (tinyMCE.activeEditor.getContent().length > 65534) {
                $('.descripcion_larga').show();
                bandera = false;
            } else {
                if ($('.descripcion').css('display') == 'block') {
                    $('.descripcion').hide();
                }
            }

            if (!bandera) {
                $('#publicar_debate').on('submit', function (e) {
                    e.preventDefault();
                });
            } else {
                document.getElementById("publicar_debate").submit();
            }
        });
    });

</script>
</body>
<?php $this->view('principal/footer'); ?>
