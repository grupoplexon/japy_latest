<?php $this->view('principal/header');?>
<?php $this->view('principal/novia/menu');?>
<style>
    .imagen_grupos{
        width: 40px;
        height: 40px;
        margin-left: 15px;
        margin-right: 5px;
    }
    .nombre_grupo{
        position: relative;
        margin-left: 65px;
        margin-top: -25px;
    }
    .entorno_mensaje{
        height: 300px;
        width: 100%;
        margin-bottom: 50px;
    }
    .fa-ban{
        color: red;
    }
</style>
<body>
    <div class="body-container">
        <div class="row">
            <div class="col s12 m3" style="float: right">
                <ul class="collection with-header">
                    <li class="collection-header dorado-2">Normas de la Comunidad</li>
                    <li class="collection-item grey lighten-5"><b>Participa Correctamente</b></li>
                    <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes contenido ofensivo</li>
                    <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes contenido comercial</li>
                    <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No postes contenido ilegal</li>
                    <li class="collection-item grey lighten-5"><i class="fa fa-ban" aria-hidden="true"></i> No insultes a ningun usuario o empresa</li>
                </ul>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <h5> Nueva Video </h5>
                </div>
                <div class="row">
                    <form method="post" id="publicar_video" action="<?php echo base_url() ?>novios/comunidad/video/publicarVideo">
                        <div class="col s12 m12 grey lighten-5 z-depth-1">
                            <div class="card-panel light-blue lighten-5">
                                <p>Agrega tu video al grupo que consideres adecuado.</p>
                            </div>
                            <div class="row">
                                <div class="col s3 m2" style="margin-top: 15px">
                                    <p> Grupos </p>
                                </div>
                                <div class="input-field col s9 m6 l4">
                                    <select id="grupos" name="grupos" class="browser-default form-control" required>
                                        <option></option>
                                        <option value="0">-- Grupos Generales --</option>
                                        <?php
                                        $i = 1;
                                        foreach ($grupos as $grupo){
                                            if($i == 17){
                                                echo '<option value="0">-- Grupos por Estados --</option>';
                                            }
                                            if(!empty($id_grupo) && $grupo->id_grupos_comunidad == $id_grupo){
                                                echo '<option value="'.$grupo->id_grupos_comunidad.'" selected> '.$grupo->nombre.' </option>';
                                            }else{
                                                echo '<option value="'.$grupo->id_grupos_comunidad.'"> '.$grupo->nombre.' </option>';
                                            }
                                            $i++;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s3 m2" style="margin-top: 15px">
                                    <p>T&iacute;tulo</p> 
                                </div>
                                <div class="col s9 m8">
                                    <div class="row">
                                        <div class="chip red darken-1 white-text right titulo_de_video" style="margin-bottom:20px; display:none;">T&iacute;tulo demasiado largo.</div>
                                    </div>
                                    <div class="input-field">
                                        <input id="titulo" name="titulo" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="chip red darken-1 white-text right url_video" style="margin-bottom:20px; display:none;">Url invalida.</div>
                                </div>
                                <div class="col s3 m2">
                                    <p style="margin-top: 30px">Video</p>
                                </div>
                                <div class="col s9 m10">
                                    <div class="input-field">
                                        <textarea id="direccion_video" name="direccion_video" class="form-control" rows="18" style="resize: none; height: 100px;" placeholder="https://www.youtube.com/watch?v=xYbPWyoNN7c" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="chip red darken-1 white-text right descripcion_larga" style="margin-bottom:20px; display:none;">Descripci&oacute;n demasiado larga.</div>
                                </div>
                                <div class="row">
                                    <div class="chip red darken-1 white-text right descripcion" style="margin-bottom:20px; display:none;">Agrega una descripci&oacute;n.</div>
                                </div>
                                <div class="col s3 m2" style="margin-top: 0px">
                                    <p>Descripci&oacute;n</p>
                                </div>
                                <div class="col s9 m10" style="margin-bottom: 50px"> 
                                    <div style="min-height: 300px; max-height: 500px">
                                        <textarea id="text_area" class="entorno_mensaje" name="descripcion"> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12">
                                    <button type="submit" name="publicar" id="publicar" class="waves-light waves-effect btn dorado-2" style="float: right"> Publicar </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url()?>dist/tinymce/tinymce.min.js"></script>
     <script type="text/javascript">
        $(document).ready(function () {           
            init_tinymce_mini("#text_area");
        });
        
        //        FUNCION DE CREACION DEL EDITOR 
        function init_tinymce_mini(elem) {
            if (typeof tinymce != "undefined") {
                tinymce.init({
                    selector: elem,
                    theme: 'modern',
                    menubar: 'false',
                    relative_urls: false,
                    plugins: [
                        'autolink link directionality'
                    ],
                    toolbar: 'undo redo | bold | italic | link |  alignleft aligncenter alignright alignjustify',
                });
            }
        }
        
        $(document).ready(function(){
            $("#publicar").on('click',function(e){
                var video = $("#direccion_video").val();
                var titulo = $('#titulo').val();
                var bandera = true;
                if($("#grupos").val() == "" || $("#grupos").val() == 0){
                    $("#grupos").val("");
                    bandera = false;
                }
                if($("#titulo").val() == "" && bandera){
                    bandera = false;
                }
                if(titulo.length > 254 && bandera){
                    $('.titulo_de_video').show();
                    bandera = false;
                }else{
                    $('.titulo_de_video').hide();
                }
                if(video == "" && bandera){
                    bandera = false;
                }
                if(!validar_url(video) && bandera){
                    if($('.url_video').css('display') == 'none'){
                        $('.url_video').show();
                    }
                    $('#direccion_video').focus();
                    bandera = false;
                }else if(bandera){
                    if($('.url_video').css('display') == 'block'){
                        $('.url_video').hide();
                    }
                }
                if(tinyMCE.activeEditor.getContent() == "" && bandera){ 
                    bandera = false;
                    if($('.descripcion').css('display') == 'none'){
                        $('.descripcion').show();
                    }
                    tinymce.execCommand('mceFocus', false, '#text_area');
                }else if(tinyMCE.activeEditor.getContent().length > 65534){
                    $('.descripcion_larga').show();
                    bandera = false;
                }else{
                    if($('.descripcion').css('display') == 'block'){
                        $('.descripcion').hide();
                    }
                    $('.descripcion_larga').hide();
                }
                if(!bandera){
                    $('#publicar_video').on('submit',function(e){
                        e.preventDefault();
                    });
                }else{
                    document.getElementById("publicar_video").submit();
                }
            });
            
        });
        
        function validar_url(video){
            var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            return (video.match(p))? true : false;
        }
    </script>
</body>
<?php $this->view('principal/footer'); ?>

