<style>
    /* Start Masonry */
    * { box-sizing: border-box; }
    /* ---- grid ---- */
    .grid { background: white; }
    /* clear fix */
    .grid:after { content: ''; display: block; clear: both; }
    /* ---- .grid-item ---- */
    .grid-sizer, .grid-item { width: 33.333%; }
    .grid-item { float: left; padding: .3rem; }
    .grid-item img {
        border-radius: 8px;
        display: block;
        width: 100%;
    }
    /* End Masonry */
    @media (min-width: 320px){ 
        .grid-item{ width: 100%; } 
        .showMedAndUp{ display: none !important; } 
        .showSmall{ display: inline !important;} 
        .iconSize{ font-size: 20px; }
        .op button{
            height: 3rem;
            width: 3rem!important;
        }
        .middle{ left: 50%; }
        .modal-content{ width: 80%; }
    }
    @media(min-width: 800px){
        .grid-item{ width: 33.3333%; };
        .showMedAndUp{ display: none !important; } 
        .showSmall{ display: inline !important;} 
        .iconSize{ font-size: 20px; }
        .op button{
            height: 2rem;
            width: 2rem!important;
        }
        .middle{ left: 46%; }
        .modal-content{ width: 30%; }
    }
    @media (min-width: 1100px){ 
        .grid-item{ width: 33.33%; } 
        .showSmall{ display: none !important;} 
        .showMedAndUp{ display: inline !important;} 
        .iconSize{ font-size: 30px; }
        .op button{
            height: 3rem;
            width: 3rem!important;
        }
        .middle{ left: 50%; }
    }
    .easy-autocomplete{ width: 100%; }
    .easy-autocomplete.eac-round ul li, .easy-autocomplete.eac-round ul .eac-category { border-color: #ccc !important; }
    .searchBar{
        border-radius: 2rem;
        background-color: #e3e3e3e3;
        margin: 1rem 0;
    }
    .searchInput{
        border-radius: 2rem !important;
        background-color: #e3e3e3e3 !important;
        border-bottom: none !important;
        box-shadow: none !important;
        border: none !important;
    }
    .searchInput:focus{ border-bottom: none !important; }
    .centerOnDiv{
        align-items: center;
        justify-content: center;
        display: flex;
    }
    .image-item{
        background-size: contain;
        background-repeat: no-repeat;
        width: 100%;
        height: 11rem;
        border-radius: 1rem;
    }
    .image-item:hover{ margin 1rem; }
    .infoDiv{ border-left: solid 2px #00bcdd; }
    a.op{
        color: #abd77a;
        font-size: 20px;
        cursor: pointer;
    }
    .activeOptoionDiv{ background-color: #00bcdd; }
    .activeOptoionText{ color: white !important;}
    .generalButton{
        color: white;
        font-size: 12px;
        border: none;
        height: 3rem;
        border-radius: 8px;
        width: auto;
        margin: 0 1px;
        padding: 0 1rem;
    }
    .generalButton:focus { outline: 0; }
    .generalButton:hover { box-shadow: 0px 6px 0px 0px rgba(129, 129, 129, 0.2), 0px 6px 13px 0 rgba(137, 137, 137, 0.2); }

    /* Start Hover Menu */
    .container { position: relative; width: 100%; }
    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        width: 50%;
    }
    .grid-item:hover { filter: grayscale(40%); }
    .grid-item:hover .middle { opacity: 1; }
    .op{ display: flex; flex-direction: row; justify-content: center;}
    .op button{
        background-color: #85d823;
        color: white;
        font-size: 14px;
        border: none;
        border-radius: 3rem;
        margin: .3rem 0;
        padding: 0;
    }
    .op div{ margin: 0 0.2rem; }
    /* End Hover Menu */

    /* Start Collapsable Menu */
    .collapsible {
        cursor: pointer;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        padding: 0.5rem;
        font-size: 15px;
    }
    .content {
        padding: 0 18px;
        display: none;
        overflow: hidden;
        background-color: white;
        color: grey;
    }
    /* End Collapsable Menu */

    /* Start Modal for new Inspiracion */
    .modal {
        display: none; 
        position: fixed;
        z-index: 1;
        padding-top: 100px;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: transparent;
        box-shadow: none;
    }
    .modal-content {
        background-color: #1fbddb;
        margin: auto;
        padding: 20px;
        border-radius: 1rem;
    }
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }
    .close:hover, .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    /* End Modal for new Inspiracion */

    /* Start tooltips */
    .tooltip { position: relative; display: inline-block; }
    .tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        position: absolute;
        z-index: 1;
        bottom: 125%;
        left: 50%;
        margin-left: -60px;
        opacity: 0;
        transition: opacity 0.3s;
    }
    .tooltip .tooltiptext::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }
    .tooltip:hover .tooltiptext { visibility: visible; opacity: 1; }
    /* End Tooltips */

    /* Start TooltipMenu Item */
    /* End TooltipMenu Item */
    ::placeholder { color: #179ca5; opacity: 1; /* Firefox */ }
    .easy-autocomplete-container{ width: auto !important; }
    .easy-autocomplete-container ul li{ cursor: pointer; }

</style>
<?php
    $controller = $_SERVER["REQUEST_URI"];
    $controller = strtolower($controller);
    // echo ($controller);
?>

<link href="<?php echo base_url() ?>dist/EasyAutocomplete/easy-autocomplete.themes.min.css" rel="stylesheet"/>
<link href="<?php echo base_url() ?>dist/EasyAutocomplete/easy-autocomplete.min.css" rel="stylesheet"/>

<div class="col s12" >
    <div class="row" >
        <div class=" searchBar col s10 m8 l7 offset-s1 offset-m2 offset-l2 centerOnDiv" >
            <div class="col s1 centerOnDiv" id="searchButton" >
                <i style="color: #8e8e8e; font-size:30px; cursor: pointer;" class="material-icons">search</i>
            </div>
            <div class="col s11" >
                <input class="searchInput" id="searchInput" type="text" name="search" style="margin: 0;" />
            </div>
        </div>
    </div>

    <div class="row" >
        <div id="tags" class="col s10 m8 l7 offset-s1 offset-m2 offset-l2 tag-slide" style="display: flex; justify-content: center; flex-wrap: wrap;" >
            <button class="generalButton" onclick="tagClicked(event)" style="background-color: #fc9296" >VESTIDOS</button>
            <button class="generalButton" onclick="tagClicked(event)" style="background-color: #fdd0a6" >ZAPATOS</button>
            <button class="generalButton" onclick="tagClicked(event)" style="background-color: #c8ddde" >RAMO</button>
            <button class="generalButton" onclick="tagClicked(event)" style="background-color: #95c4be" >NOVIA</button>
            <button class="generalButton" onclick="tagClicked(event)" style="background-color: #6c8a95" >NOVIO</button>
            <button class="generalButton" onclick="tagClicked(event)" style="background-color: #4ba3bb" >DECORACIONES</button>
        </div>
    </div>

    <div class="row centerOnDiv" >
        <div class="infoDiv showSmall col l2" style="display: flex; padding: 0;" >
            <div class="col s12" style="padding: 0;" >
                <div class="col s12 isActiveMenu" style="margin: 0;padding: 0;" >
                    <a class="op collapsible" style="margin: 0!important;" >Inspiración</a>
                    <div class="content" >
                        <a class="op" style="color: grey;"> - Flores</a>
                        <a class="op" style="color: grey;"> - Vestidos</a>
                        <a class="op" style="color: grey;"> - Sesiones</a>
                        <a class="op" id="btnAddInspiration1" style="color: grey; font-weight: bold;"> - Crear nuevo</a>
                    </div>
                </div>
                <div class="col s12 isActiveMenu" style="margin: 0;padding: 0;" >
                    <a class="op collapsible" style="margin: 0!important;" >Proveedores</a>
                    <div class="content" >
                        <a class="op" style="color: grey;"> - Amelie Joyas</a>
                        <a class="op" style="color: grey;"> - Camelia Garden</a>
                        <a class="op" style="color: grey;"> - Grupo Escalafon</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s10 m8 l7 offset-s1 offset-m2 offset-l2" >
            <div id="grid">
                <div class="grid-sizer"></div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/orange-tree.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip parent" >
                                    <button > <i class="material-icons iconSize">add</i>  </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/submerged.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/look-out.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/one-world-trade.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/drizzle.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/cat-nose.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/contrail.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/golden-hour.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/flight-formation.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/look-out.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-item">
                    <div class="container">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/82/look-out.jpg" />
                        <div class="middle">
                            <div class="op">
                                <div class="tooltip" >
                                    <button > <i class="material-icons iconSize">add</i> </button>
                                    <span class="tooltiptext">Agregar a inspiración</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">file_download</i> </button>
                                    <span class="tooltiptext">Guardar proveedor</span>
                                </div>
                                <div class="tooltip" > 
                                    <button > <i class="material-icons iconSize">more_horiz</i> </button>
                                    <span class="tooltiptext">Opciones</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="infoDiv showMedAndUp col l2" style="display: flex; padding: 0;" >
            <div class="col s12" style="padding: 0;" >
                <div class="col s12 isActiveMenu" style="margin: 0;padding: 0;" >
                    <a class="op collapsible" style="margin: 0!important;" >Inspiración</a>
                    <div class="content" >
                        <a class="op" style="color: grey;"> - Flores</a>
                        <a class="op" style="color: grey;"> - Vestidos</a>
                        <a class="op" style="color: grey;"> - Sesiones</a>
                        <a class="op" id="btnAddInspiration2" style="color: grey; font-weight: bold;"> - Crear nuevo</a>
                    </div>
                </div>
                <div class="col s12 isActiveMenu" style="margin: 0;padding: 0;" >
                    <a class="op collapsible" style="margin: 0!important;" >Proveedores</a>
                    <div class="content" >
                        <a class="op" style="color: grey;"> - Amelie Joyas</a>
                        <a class="op" style="color: grey;"> - Camelia Garden</a>
                        <a class="op" style="color: grey;"> - Grupo Escalafon</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="LoadImages" class="col s12" style="text-align: center;">
            <span id="onLoad">Cargando...</span>
        </div>
        
        <!-- Start Modal For New Inspiracion -->
        <div id="myModal" class="modal">
            <div class="modal-content">
                <span class="close" style="color: white;">&times;</span>
                <p style="color: white; font-size: 1.3rem;" >Nombre de la inspiración</p>
                <form method="post" name="fInspiracion" >
                    <input type="text" name="nInspiracion" placeholder="Nombre de la inspiracion" style="color: white;" />
                    <div class="centerOnDiv" >
                        <input class="generalButton" type="submit" value="Guardar" style="border-radius: 3rem; height: 2rem; background-color: #a3d070;" />
                    </div>
                </form>
            </div>
        </div>
        <!-- End Modal For New Inspiracion -->
    </div>

</div>

<script src="<?php echo base_url() ?>dist/masonry/masonry.pkgd.min.js"></script>
<script src="<?php echo base_url() ?>dist/EasyAutocomplete/jquery.easy-autocomplete.js" ></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>dist/slick/slick.min.js"></script>
<!-- <script src="<?php //echo base_url() ?>dist/admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script> -->

<script>
    function tagClicked(event){
        $('#searchInput').val($('#searchInput').val() + ' ' + event.target.firstChild.data.toLowerCase());
    }
    $.noConflict();
    $(document).ready(function () {
        let server = "<?php echo base_url() ?>"; // name of the root direction
        var detected = false; //boolean to check if the element is on screen
        var imgs = $("#grid > .grid-item > img").not(function() { return this.complete; }); //Get all the images on the grid
        var countImages = imgs.length; //Count the total images of the grid
        var ajax=null; //var to check if an ajax request was made
        
        // Strat Search methods
            $("#searchInput").on('keyup', function (e) {
                console.log($("#searchInput").val());
                if (ajax!=null){
                    ajax.abort();
                }

                ajax = $.get( server+"gallery/autocomplete?search="+ $("#searchInput").val(), function(data, status){
                    console.log(data, status);
                    if(data.code === 200 ){
                        getSearchAutocomplete(data.data);
                        console.log(data.data);
                    }
                });
                if (e.keyCode == 13) {
                    var searchString = $('#searchInput').val().split(" ");
                    getImages(searchString);
                }
            });
            $("div#searchButton").click(function() {
                    var searchString = $('#searchInput').val().split(" ");
                    getImages(searchString);
            });
        // End Search methods

        //Start images has been loaded 
            if (countImages) {
                imgs.load(function() {
                    countImages--;
                    if (!countImages) { loadMasonry(); }
                });
            }
            else{ loadMasonry(); }
        //End images has been loaded 

        //Start Collpsible Menu
            $('div.isActiveMenu').click(function() { $(this).toggleClass('activeOptoionDiv'); });
            var coll = $('.collapsible');
            for (var i = 0; i < coll.length; i++) {
                coll[i].addEventListener("click", function() {
                    this.classList.toggle("activeOptoionText");
                    var content = this.nextElementSibling;
                    if (content.style.display === "block") {
                    content.style.display = "none";
                    } else {
                    content.style.display = "block";
                    }
                });
            }            
        //End Collpsible Menu

        // Start Modal Script
            var modal = document.getElementById('myModal');
            var btn = document.getElementById("btnAddInspiration1");
            var btn2 = document.getElementById("btnAddInspiration2");
            var span = document.getElementsByClassName("close")[0];
            btn.onclick = function() { modal.style.display = "block"; }
            btn2.onclick = function() { modal.style.display = "block"; }
            span.onclick = function() { modal.style.display = "none"; }
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        // End Modal Script

        //Start detect component on screen
            $(window).scroll(function() {
                var top_of_element = $('#LoadImages').offset().top;
                var bottom_of_element = $('#LoadImages').offset().top + $('#LoadImages').outerHeight();
                var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
                var top_of_screen = $(window).scrollTop();
                if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) && (!detected)) {
                    detected = true;
                    // console.log("component detected");
                }
            });
        //End detect component on screen

        // Start Masonry Grid
            function loadMasonry(){
                var container = document.querySelector('#grid');
                var msnry = new Masonry( container, {
                    columnWidth: '.grid-sizer',
                    percentPosition: true,
                    itemSelector: '.grid-item'
                });
            }
        // End Masonry Grid

        //Start Function to load all images searched
            function getImages (tags) {
                console.log(tags);
                let colors = ["#fc9296","#fdd0a6","#c8ddde","#95c4be","#6c8a95","#4ba3bb"];
                $.ajax({
                    url: server+'gallery/search',
                    method: 'POST',
                    data: {tags},
                    success: function(resp) {
                        var divTags = $('div#tags');
                        divTags.html('');
                        resp.data.available_tags.map((element, tag) => {
                            divTags.append(`<button class="generalButton" onclick="tagClicked(event)" style="background-color: ${colors[Math.floor(Math.random() * (5 - 0)) + 0]}" >${element.nombre}</button>`);
                        });
                        // if( $('.slick-initialized') != undefined ){
                            //     console.log($('.slick-initialized').length);
                            //     $('.tag-slide').slick('removeSlide', null, null, true);
                        // }
                        $('.tag-slide').slick({
                            dots: false,
                            arrows: true,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 6,
                            variableWidth: true,
                            nextArrow: '<i class="fa fa-arrow-right" style="margin:0 0 0 10px;"></i>',
                            prevArrow: '<i class="fa fa-arrow-left" style="margin:0 25px 0 0;"></i>',
                        });
                        console.log(resp.data.available_tags);
                    },
                    error: function() {
                        // if (!$('#toast-container .toast').get(0)) {
                            //     Materialize.toast(
                            //         "Debes estar registrado para guardar un proveedor<br> <a href='<?php //echo site_url("cuenta") ?>' class='dorado-1-text pull-right'>Inicia Sesi&oacute;n <i class='material-icons right'>keyboard_arrow_right</i></a>",
                            //         5000);
                            //     $('.toast').css({display: 'block'});
                        // }
                    },
                });
            }
        //End Function to load all images searched

        //Start Function to fill EasyAutocomplete
            function getSearchAutocomplete(wordToSearch){
                // var options = {
                    // url: function(phrase) { return "api/countrySearch.php"; },
                    // getValue: function(element) {
                    //     return element.name;
                    // },
                    // ajaxSettings: {
                    //     dataType: "json",
                    //     method: "POST",
                    //     data: { tags: wordToSearch }
                    // },
                    // preparePostData: function(data) {
                    //     data.phrase = $("#searchInput").val();
                    //     return data;
                    // },
                    // requestDelay: 400
                // };
                var options = {
                    data: wordToSearch,
                    list: {	
                        match: {
                        enabled: true
                        },
                        showAnimation: {
                            type: "fade", //normal|slide|fade
                            time: 400,
                            callback: function() {}
                        },
                        hideAnimation: {
                            type: "slide", //normal|slide|fade
                            time: 400,
                            callback: function() {}
                        }
                    },
                    theme: "round"
                }
                $("#searchInput").easyAutocomplete(options);
            };
        //Start Function to fill EasyAutocomplete

    });
    //EasyAutocomplete demo
        // var available_tags = [
        //     "galleta de la fortuna",
        //     "comida chatarra",
        //     "postre",
        //     "comida china",
        //     "comida asiática",
        //     "wonton",
        //     "bola de masa hervida",
        //     "kreplach",
        //     "receta",
        // ];
        // var options = {
        //     data: available_tags,
        //     list: {	
        //         match: {
        //         enabled: true
        //         },
        //         showAnimation: {
        //             type: "fade", //normal|slide|fade
        //             time: 400,
        //             callback: function() {}
        //         },
        //         hideAnimation: {
        //             type: "slide", //normal|slide|fade
        //             time: 400,
        //             callback: function() {}
        //         }
        //     },
        //     theme: "round"
        // }
        // $("#searchInput").easyAutocomplete(options);
    //EasyAutocomplete demo
</script>
