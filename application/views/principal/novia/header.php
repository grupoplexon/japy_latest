<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
<noscript id="deferred-styles-2">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Raleway:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/slick/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/slick/slick-theme.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/src/parsley.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/general_styles.css">
    <link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</noscript>
<title><?php echo isset($title) ? $title : "BrideAdvisor" ?></title>
<meta name="theme-color" content="#f5e6df">
<meta name="msapplication-navbutton-color" content="#f5e6df">
<meta name="apple-mobile-web-app-status-bar-style" content="#f5e6df">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<?php echo base_url() ?>dist/css/iconos.css" rel="stylesheet" type="text/css"/>
<!--<link href="--><?php //echo base_url() ?><!--dist/img/clubnupcial.css" rel="icon" sizes="16x16">-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons|Raleway:300,400,500,600,700" rel="stylesheet">
<link href="<?php echo base_url() ?>dist/css/datatables.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/presupuesto.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/slick/slick.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url() ?>dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
<link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' data-cfasync='false'>window.purechatApi = {
        l: [], t: [], on: function () {
            this.l.push(arguments);
        }
    };
    (function () {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function (e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({c: 'f3f97b28-73ac-4929-a04d-527e409169b7', f: true});
                done = true;
            }
        };
    })();
</script>

<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function () {
        var mf = document.createElement("script");
        mf.type = "text/javascript";
        mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/4315bf06-815c-4f86-b787-cb87d939a11e.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>

<!-- WORKAROUND FOR NOW, THIS IS FOR THE HOME PAGE THAT USES OTHER VERSIONS OF MATERIALIZE AND FONTAWESOME -->
<?php if (base_url() == current_url()) : ?>
    <link type="text/css" rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.1/css/materialize.min.css"
          media="screen,projection"/>
<?php endif ?>

<title><?php echo $this->config->item("name") ?></title>

<!-- Facebook Pixel Code -->
<script>
    !function(f, b, e, v, n, t, s) {
        if (f.fbq) {
            return;
        }
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments);
        };
        if (!f._fbq) {
            f._fbq = n;
        }
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s);

        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById('deferred-styles-2');
            var replacement = document.createElement('div');
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement);
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        if (raf) {
            raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        }
        else {
            window.addEventListener('load', loadDeferredStyles);
        }

    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2323895250959097');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2323895250959097&ev=PageView&noscript=1
https://www.facebook.com/tr?id=2323895250959097&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->



