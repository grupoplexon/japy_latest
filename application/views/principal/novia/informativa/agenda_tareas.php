<html lang="es" xml:lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Japy</title>
    <?php $this->view("japy/header"); ?>

    <link type="text/css" href="<?php echo base_url() ?>dist/css/novios/page-info.min.css" rel="stylesheet"/>
</head>
<body class="page-informativa">
<div class="row cabecera agenda valign-wrapper" style="background-position: center; background-size: cover;">
    <div class="body-container">
        <div class="col s11 m7 l6 valign">
            <div class="row">
                <div class="row ">
                    <h4 class=" col s7 title">AGENDA</h4>
                </div>
            </div>
            <div class="row col s11 m11 l7">
                <p class="subtitle">
                    No olvides agendar todos tus pendientes
                    y actividades
                </p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('principal/novia/informativa/menu.php') ?>
<div class="row texture-corazones" style="margin:0">
    <div class="body-container">
        <div class="col s12 center" style="margin-top:35px;">
            <!--<div class="col s12"><span class="center" style="font-weight:bold; line-height: 30px; font-size:22px">
                    Sabr&aacute;s cu&aacute;ndo y c&oacute;mo hacer cada cosa, sin sorpresas
                </span>
            </div>-->
            <div class="col s2">&nbsp;</div>
            <p class="col s8" style="line-height: 25px;font-size: 18px;">
                Aqu&iacute; encontrar&aacute;s una lista de actividades por realizar con sus respectivas fechas.
                <br/>Llena cada espacio y que no te falte nada para el gran d&iacute;a.
            </p>
        </div>
        <div class="col s2">&nbsp;</div>
        <div class="col s8 center">
            <!--<img class="responsive-img" src="<?php echo base_url() ?>/dist/img/agenda_boda/macbook_agenda.png" alt="" >-->
        </div>

    </div>
</div>
<div class="row" style="margin:0">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:35px;">
            <img class="responsive-img col s12 m10 right" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-1.png"
                 alt="">
        </div>
        <div class="col s12 m5" style="margin-top:75px">
            <div class="row">
                <div class="row valign-wrapper">
                    <div class="valign col s4 m4" style="padding: 0;">
                        <!--<img class="responsive-img" src="<?php echo base_url() ?>/dist/img/agenda_boda/planeador_tareas.png" alt="" >-->
                    </div>
                    <span class="valign" style="font-weight:bold; line-height: 30px; font-size:22px">
                                No olvides agendar todos tus pendientes y actividades
                            </span>
                </div>
            </div>
            <div class="row">
                <p style="line-height: 25px;font-size: 18px;">
                    Una de las funciones de Japy es ayudarte a planificar tus actividades, as&iacute; que revisa tus
                    pendientes cuantas veces necesites.
                </p>
            </div>
        </div>

    </div>
</div>
<div class="row texture-corazones">
    <div class="body-container">
        <div class="col s12 m6" style="margin-top:75px;">
            <div class="row col s12 m10 right">
                <p style="font-weight:bold; font-size: 21px;">
                    Adem&aacute;s de las tareas que Japy tiene para ti, t&uacute; puedes enlistar todas tus
                    actividades pendientes.
                </p>
            </div>
        </div>
        <div class="col s12 m6" style="margin-top:35px;  margin-bottom:35px">
            <img class="responsive-img col s12 m8" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-2.png" alt="">
        </div>

    </div>
</div>
<div class="row" style="margin:0;">
    <div class="body-container">
        <div class="col s12 m12 center" style="margin-top:35px;">
            <p style="font-size: 16px; margin:0">
                Las herramientas que tenemos para ti est&aacute;n conectadas entre s&iacute;, para que puedas llevar un
                control entre tu presupuesto, agenda y proveedores.
            </p>
            <!--<img class="responsive-img s10" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-3.png" alt="" >-->
        </div>
        <div class="col s12 m12" style="margin-top:15px; margin-bottom:35px; text-align:right">
            <div class="col s12 m4" style="margin-top:15px">
                <img class="responsive-img s10 " src="<?php echo base_url() ?>/dist/img/agenda_boda/img-4.png" alt="">
            </div>
            <div class="col s12 m4 center" style="margin-top:15px">
                <img class="responsive-img s10 " src="<?php echo base_url() ?>/dist/img/agenda_boda/img-5.png" alt="">
            </div>
            <div class="col s12 m4" style="margin-top:15px; text-align:left">
                <img class="responsive-img s10" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-6.png" alt="">
            </div>
        </div>

    </div>
</div>

<?php $this->view('bannerDownload'); ?>
<script>
    $(document).ready(function () {
        $('.parallax').parallax();
    });
</script>
<?php $this->view('japy/footer'); ?>
</body>
