<?php $this->view('principal/header'); ?>
<style>
    .parallax-container{
        width: 100%;
    }
    .seccion-novia > .row > .m1-1{
        color: white;
        text-align: center;
        border-right: 1px solid #6c6d6f;
        padding: 10px 10px 10px 10px;
        min-height: 70px;
        text-decoration: none;
    }
    .seccion-novia > .row{
        border-right: transparent;
        margin: 0px;
    }
    .seccion-novia > .row > .m1-1 .active{
        background-color: #6c6d71;
        border-bottom: 5px solid #f9a897;
    }
    .icono-planeador,.titulo_icono,.descripcion_icono{
        text-align: center;
    }
    .descripcion_icono{
        line-height: 15px;
        font-size: 12px;
    }
    .fondo_gradiente{
        background: rgba(158,155,158,1);
        background: -moz-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(158,155,158,1)), color-stop(70%, rgba(158,155,158,1)), color-stop(100%, rgba(94,91,94,1)));
        background: -webkit-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -o-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -ms-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: linear-gradient(to bottom, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9e9b9e', endColorstr='#5e5b5e', GradientType=0 );
        height: 400px;
        overflow: hidden;
    }
    .cel{
        margin: 0;
        margin-left: 10%;
        padding-top: 50px;
    }
    .descarga{
        margin: 0;
        margin-top: 100px;
    }
    @media only screen and (min-width:1281px) {
        /* hi-res laptops and desktops */
        .fondo_gradiente{

        }
    }
</style>
<body>
    <div class="row" style="margin: 0;margin-top: 50px;width:100%; height:300px; background: url('<?php echo base_url() ?>/dist/img/agenda_boda/fondo2.png') no-repeat left center #fff;
         background-size: cover;">
        <div class="body-container">
            <div style="color:white; margin-left:10%;" class="col s11 m7 l6">
                <div class="row " style="margin-top:50px;margin-bottom:0">
                    <div class="row valign-wrapper" >
                        <h4 class="valign col s7" style="color:white; line-height: 30px; font-size: 35px; margin: 0; font-weight:bold;">AGENDA</h4>
                        <div class="valign col s5 m5"><img class="responsive-img" src="<?php echo base_url() ?>/dist/img/agenda_boda/icono.png" alt="" ></div>
                    </div>
                </div>
                <div class="row col s11 m11 l7" style="margin:0">
                    <p style="font-size: 16px; line-height: 25px; margin: 0;color:white">
                        No olvides agendar todos tus pendientes y actividades
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('principal/novia/informativa/planeador_bodas/menu.php') ?>
    <div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png');margin:0">
        <div class="body-container">
            <div class="col s12 center" style="margin-top:35px;">
                <!--<div class="col s12"><span class="center" style="font-weight:bold; line-height: 30px; font-size:22px">
                        Sabr&aacute;s cu&aacute;ndo y c&oacute;mo hacer cada cosa, sin sorpresas
                    </span>
                </div>-->
                <div class="col s2">&nbsp;</div>
                <p class="col s8" style="line-height: 25px;font-size: 18px;">
                    Aqu&iacute; encontrar&aacute;s una lista de actividades por realizar con sus respectivas fechas.
                    <br/>Llena cada espacio y que no te falte nada para el gran d&iacute;a.
                </p>
            </div>
            <div class="col s2">&nbsp;</div>
            <div class="col s8 center">
                <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/agenda_boda/macbook_agenda.png" alt="" >
            </div>

        </div>
    </div>
    <div class="row" style="margin:0">
        <div class="body-container">
            <div class="col s12 m6" style="margin-top:35px;">
                <img class="responsive-img col s12 m10 right" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-1.png" alt="" >
            </div>
            <div class="col s12 m5" style="margin-top:75px">
                <div class="row">
                    <div class="row valign-wrapper" >
                        <div class="valign col s4 m4" style="padding: 0;"><img class="responsive-img" src="<?php echo base_url() ?>/dist/img/agenda_boda/planeador_tareas.png" alt="" ></div>
                        <span class="valign" style="font-weight:bold; line-height: 30px; font-size:22px">
                             No olvides agendar todos tus pendientes y actividades
                        </span>
                    </div>
                </div>
                <div class="row">
                    <p style="line-height: 25px;font-size: 18px;">
                         Una de las funciones de Japy es ayudarte a planificar tus actividades, as&iacute; que revisa tus pendientes cuantas veces necesites.
                    </p>
                </div>
            </div>

        </div>
    </div>
    <div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); ">
        <div class="body-container">
            <div class="col s12 m6" style="margin-top:75px;">
                <div class="row col s12 m10 right" >
                    <p style="font-weight:bold; font-size: 21px;">
                        Adem&aacute;s de las tareas que Japy tiene para ti, t&uacute; puedes enlistar todas tus actividades pendientes.
                    </p>
                </div>
            </div>
            <div class="col s12 m6" style="margin-top:35px;  margin-bottom:35px">
                <img class="responsive-img col s12 m8" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-2.png" alt="" >
            </div>

        </div>
    </div>
    <div class="row" style="margin:0;">
        <div class="body-container">
            <div class="col s12 m12 center" style="margin-top:35px;">
                <p style="font-size: 16px; margin:0" >
                     Las herramientas que tenemos para ti est&aacute;n conectadas entre s&iacute;, para que puedas llevar un control entre tu presupuesto, agenda y proveedores.
                </p>
                <img class="responsive-img s10" src="<?php echo base_url() ?>/dist/img/agenda_boda/img-3.png" alt="" >
            </div>
            <div class="col s12 m12" style="margin-top:15px; margin-bottom:35px; text-align:right">
                <div class="col s12 m4 center" style="margin-top:15px">
                    <img class="responsive-img " src="<?php echo base_url() ?>/dist/img/agenda_boda/img-4.png" alt="" >
                </div>
                <div class="col s12 m4 center" style="margin-top:15px">
                    <img class="responsive-img " src="<?php echo base_url() ?>/dist/img/agenda_boda/img-5.png" alt="" >
                </div>
                <div class="col s12 m4 center" style="margin-top:15px; text-align:left">
                    <img class="responsive-img " src="<?php echo base_url() ?>/dist/img/agenda_boda/img-6.png" alt="" >
                </div>
            </div>

        </div>
    </div>
    <div class="row" style="margin: 0;width:100%; height:400px; background: url('<?php echo base_url() ?>/dist/img/agenda_boda/descarga-app.png') no-repeat left center #fff;
         background-size: cover; ">
        <div class="body-container">
            <div class="col s12 m12 l10" style="margin-top:40px; color:white">
                <div class="row col s12 right" >
                    <p style="font-weight:bold; font-size: 21px;">¡Olv&iacute;date de la pluma y el papel!</p>
                    <p class="col s12 m10" style="padding:0; font-size: 17px;" >Cuando descargues la aplicaci&oacute;n de japybodas.com podr&aacute;s llevar contigo la planeaci&oacute;n de tu boda a todos lados.</p>
                </div>
                <div class="row col s12 right" style="padding:0" >
                    <p class="col s12 m12" style="font-weight:bold; font-size: 21px; color: #c79838">DESCARGA LA APP
                    </p>
                    <a href="#!"><img class="responsive-img col s3" src="<?php echo base_url() ?>/dist/img/agenda_boda/es_app_store.png" alt="" ></a>
                    <a href="#!"><img class="responsive-img col s3" src="<?php echo base_url() ?>/dist/img/agenda_boda/googlePlay.png" alt="" ></a>
                </div>
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.parallax').parallax();
        });
    </script>
</body>
<?php $this->view('principal/footer'); ?>
