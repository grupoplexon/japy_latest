<html lang="es" xml:lang="es">
    <head>
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Japy</title>
        <?php $this->view("japy/header"); ?>
        <link  type="text/css" href="<?php echo base_url() ?>dist/css/novios/page-info.min.css" rel="stylesheet"   />
    </head>
    <body class="page-informativa">
        <div class="row cabecera agenda controlmesas valign-wrapper" style="background-position: center; background-size: cover;">
            <div class="body-container">
                <div class="col s11 m7 l6 valign">
                    <div class="row"  >
                        <div class="row " >
                            <h4 class=" col s7 title">Control de mesas</h4>
                        </div>
                    </div>
                    <div class="row col s11 m11 l7">
                        <p class="subtitle">
                            Ya no te preocupes por la asignaci&oacute;n de mesas y acomodo de invitados
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('principal/novia/informativa/menu.php') ?>
        <div class="col s12 m12 texture-corazones" style="padding-top: 20px; padding-bottom: 20px">
            <div class="body-container">
                <div class="row">
                    <div class="col s12 m8">
                        <p style="font-size: 16px; line-height: 20px; text-align: justify">
                            Cada ajuste y cambio que hagas en el acomodo de las mesas, podr&aacute;s verlo en tiempo real y hacer todos los movimientos que creas convenientes.
                        </p>
                    </div>
                    <div class="col s8 offset-s2 m4">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?php echo base_url() ?>dist/img/planeador_boda/control_mesas/control_mesas.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="fondo_gradiente">
            <div class="body-container">
                <div class="row">
                    <div class="img_cel col m5 pull-left">
                        <div class="cel" style="float: right">
                            <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
                        </div>
                    </div>
                    <div class="col s12 m7">
                        <div class="descarga" style="float: left">
                            <p class="white-text" style="font-size: 22px; line-height: 25px"><b>Tu organizador de mesas siempre contigo.</b></p>
                            <p class="white-text" style="text-align: justify; font-size: 14px; line-height: 20px;">
                                Descargando la aplicaci&oacute;n, el administrador de invitados ira contigo a todas partes.
                            </p>
                            <p class="dorado-2-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                            <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png"><p>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    </body>
    <?php $this->view('japy/footer'); ?>
