<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Conócenos - BrideAdvisor</title>
    <title>BrideAdvisor</title>
    <?php $this->view("japy/header");   ?>
</head>
<style>
    .parallax-container {
        width: 100%;
    }

    .seccion-novia > .row > .m1-1 {
        color: white;
        text-align: center;
        border-right: 1px solid #6c6d6f;
        padding: 10px 10px 10px 10px;
        min-height: 70px;
        text-decoration: none;
    }

    .seccion-novia > .row {
        border-right: transparent;
        margin: 0px;
    }

    .seccion-novia > .row > .m1-1 .active {
        background-color: #6c6d71;
        border-bottom: 5px solid #f9a897;
    }

    .icono-planeador, .titulo_icono, .descripcion_icono {
        text-align: center;
        padding: 5px;
    }

    .descripcion_icono {
        line-height: 15px;
        font-size: 12px;
    }

    .fondo_gradiente {
        background: rgba(158, 155, 158, 1);
        background: -moz-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(158, 155, 158, 1)), color-stop(70%, rgba(158, 155, 158, 1)), color-stop(100%, rgba(94, 91, 94, 1)));
        background: -webkit-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -o-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -ms-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: linear-gradient(to bottom, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#9e9b9e', endColorstr='#5e5b5e', GradientType=0);
        height: 400px;
        overflow: hidden;
    }

    .cel {
        margin: 0;
        margin-left: 10%;
        padding-top: 50px;
    }

    .descarga {
        margin: 0;
        margin-top: 100px;
    }

    @media only screen and (max-width: 425px) {
        .contrast-letters{
            text-shadow: 2px 1px 1px black;

        }
        .no-float{
            float:initial;
        }
    }

    @media only screen and (max-width: 768px) {
        .contrast-letters{
            text-shadow: 2px 1px 1px black;

        }
    }


    @media only screen and (max-width: 1040px) {
        /* hi-res laptops and desktops */
        .fondo_gradiente {
            height: auto;
        }

        .descarga {
            margin: 0;
            margin-left: 15%;
        }

        .cel {
            display: none;
        }
    }
</style>
<body>
<div class="parallax-container">
    <div class="body-container">
        <div class="col s4 m4 pull-right no-float" style="margin-top: 50px">
            <p class="contrast-letters" style="font-size: 2.0rem; color: white; text-align: right; position: relative; max-width: 350px;">La
                manera m&aacute;s sencilla para planear tu boda</p>
            <p class="contrast-letters" style="font-size: 1.2rem; color: white; text-align: right; position: relative; max-width: 360px; line-height: 20px">
                Bienvenidos a BrideAdvizor, plataforma digital en donde te ayudaremos a planear tu boda con las mejores
                herramientas.
            </p>
        </div>
    </div>
    <div class="parallax">
        <img class="img_parallax" src="<?php echo base_url() ?>dist/img/planeador_boda/fondo.png">
    </div>
</div>
<?php $this->load->view('principal/novia/informativa/menu.php') ?>
<div style="background: url(<?php echo base_url() ?>dist/img/textura_footer2.png)">
    <div class="body-container">
        <div class="row">
            <div style="padding: 22px">&nbsp;</div>
            <div class="col s4 m4">
                <img class="responsive-img" src="<?php echo base_url() ?>dist/img/vineta2.png" alt="">
            </div>
            <div class="col s4 m4">
                <p class="dorado-2-text seccion-title" style="font-size: 13px;"><b>MI PLANEADOR DE BODA</b></p>
            </div>
            <div class="col s4 m4">
                <img class="responsive-img" src="<?php echo base_url() ?>dist/img/vineta3.png" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col m6">
                <div class="col s6 m6">
                    <p class="icono-planeador">
                        <a href="<?php echo base_url() ?>home/agenda_tareas">
                            <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/calendario.png">
                        </a>
                    </p>
                    <p class="titulo_icono gris-2-text"><b>AGENDA</b></p>
                    <p class="descripcion_icono">
                        No olvides agendar todos tus pendientes y actividades.
                    </p>
                </div>
                <div class="col s6 m6">
                    <p class="icono-planeador">
                        <a href="<?php echo base_url() ?>home/lista_invitados">
                            <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/invitados.png">
                        </a>
                    </p>
                    <p class="titulo_icono gris-2-text"><b>INVITADOS</b></p>
                    <p class="descripcion_icono">
                        Haz una lista con la selecci&oacute;n de tus invitados, sus contactos y mant&eacute;n tus
                        confirmaciones al d&iacute;a.
                    </p>
                </div>
            </div>
            <div class="col m6">
                <div class="col s6 m6">
                    <p class="icono-planeador">
                        <a href="<?php echo base_url() ?>home/control_mesas">
                            <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/mesas.png">
                        </a>
                    </p>
                    <p class="titulo_icono gris-2-text"><b>CONTROL DE MESAS</b></p>
                    <p class="descripcion_icono">
                        Esta parte es divertida, ya que podr&aacute;s acomodar a tus invitados.
                    </p>
                </div>
                <div class="col s6 m6">
                    <p class="icono-planeador">
                        <a href="<?php echo base_url() ?>home/proveedores">
                            <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/proveedores.png">
                        </a>
                    </p>
                    <p class="titulo_icono gris-2-text"><b>PROVEEDORES</b></p>
                    <p class="descripcion_icono">
                        Tu agenda personal con tus proveedores, tus pagos y pendientes.
                    </p>
                </div>
            </div>
        </div>
        <div class="row" style="margin: 0">
            <div class="col m6">
                <div class="col s6 m6">
                    <p class="icono-planeador">
                        <a href="<?php echo base_url() ?>home/presupuesto">
                            <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/presupuesto.png">
                        </a>
                    </p>
                    <p class="titulo_icono gris-2-text"><b>PRESUPUESTADOR INTELIGENTE</b></p>
                    <p class="descripcion_icono">
                        Te ayuda a administrar y maximizar tu dinero y a seleccionar el proveedor ideal de acuerdo a tus
                        necesidades.
                    </p>
                </div>
                <div class="col s6 m6">
                    <p class="icono-planeador">
                        <a href="<?php echo base_url() ?>home/vestidos">
                            <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/vestidos.png">
                        </a>
                    </p>
                    <p class="titulo_icono gris-2-text"><b>MIS VESTIDOS</b></p>
                    <p class="descripcion_icono">
                        Encontrar&aacute;s dise&ntilde;os exclusivos de las mejores marcas, para que elijas de tus
                        favoritos.
                    </p>
                </div>
            </div>
            <div class="col m6">
                <div class="col s6 m6">
                    <p class="icono-planeador">
                        <a href="<?php echo base_url() ?>home/portal_web">
                            <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/mi_portal_web.png">
                        </a>
                    </p>
                    <p class="titulo_icono gris-2-text"><b>MI PORTAL WEB</b></p>
                    <p class="descripcion_icono">
                        Podr&aacute;s compartir informaci&oacute;n de tu boda con tus invitados a trav&eacute;s de redes
                        sociales.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row" style="margin: 0;display: none;">
    <div class="fondo_gradiente">
        <div class="img_cel col m6">
            <div class="cel" style="float: left">
                <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
            </div>
            <div class="descarga" style="float: left; width: 40%">
                <p class="white-text" style="font-size: 22px; line-height: 25px"><b>&iexcl;INSPIRACI&Oacute;N PARA
                        LLEVAR!</b></p>
                <p class="white-text" style="font-size: 14px; line-height: 20px"><b>En el lugar y momento que
                        quieras.</b></p>
                <p class="white-text" style="text-align: justify; font-size: 12px; line-height: 15px;">Descarga
                    japy para descubrir tu vestido perfecto de entre la variedad de dise&ntilde;os, puedes
                    marcarlos para volver a verlos y
                    mostrarlo a quien t&uacute; gustes.
                </p>
                <p class="white-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png">
                <p>
            </div>
        </div>

        <div class="img_cel col m6">
            <div class="cel" style="float: left">
                <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
            </div>
            <div class="descarga" style="float: left; width: 40%">
                <p class="white-text" style="font-size: 22px; line-height: 25px"><b>&iexcl;CREA TU INSTA-&Aacute;LBUM
                        DEL EVENTO!</b></p>
                <p class="white-text" style="text-align: justify; font-size: 12px; line-height: 15px;">
                    Tus invitados la pueden hacer de fot&oacute;grafos profesionales y tomar fotos a lo largo del
                    evento, posteriormente subirlas a la aplicaci&oacute;n de &quot;wedshoots&quot; y &iexcl;listo!.
                </p>
                <p class="white-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png">
                <p>
            </div>
        </div>
    </div>
</div>


<?php $this->view('bannerDownload'); ?>

<script>
    $(document).ready(function () {
        $('.parallax').parallax();
    });
</script>
</body>
<?php $this->view('japy/footer'); ?>
