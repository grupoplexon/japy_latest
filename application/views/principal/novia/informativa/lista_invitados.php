<html lang="es" xml:lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Japy</title>
    <?php $this->view("japy/header"); ?>
    <link type="text/css" href="<?php echo base_url() ?>dist/css/novios/page-info.min.css" rel="stylesheet"/>
</head>
<body class="page-informativa">
<div class="row cabecera agenda controlmesas valign-wrapper"
     style="background-position: center; background-size: cover;">
    <div class="body-container">
        <div class="col s12 valign">
            <div class="row">
                <div class="row ">
                    <h4 class=" col s12 title">LA LISTA DE INVITADOS</h4>
                </div>
            </div>
            <div class="row col s11 m11 l7">
                <p class="subtitle">
                    Haz una lista con la selecci&oacute;n de tus invitados
                </p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('principal/novia/informativa/menu.php') ?>
<div class="texture-corazones">
    <div class="body-container">
        <div class="row">
            <div class="col s12 m6" style="text-align: center;">
                <p>
                    <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/invitados.png">
                <p style="font-size: 21px"><b>Agregar invitados.</b></p>
                <p style="font-size: 16px">Adjunta un documento de excel o agrega uno por uno.</p>
                </p>
            </div>
            <div class="col s12 m6" style="text-align: center">
                <p>
                    <img src="<?php echo base_url() ?>dist/img/iconos/iconos-colores/mesas.png">
                <p style="font-size: 21px"><b>Acomodo de mesas</b></p>
                <p style="line-height: 20px; font-size: 16px">Haz las agrupaciones necesarias para que tengas el mejor
                    acomodo.</p>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="body-container">
    <div class="col row" style="text-align: center">
        <p style="font-weight: bold; font-size: 21px">¿Qui&eacute;nes ya confirmaron?</p>
        <p style="font-size: 16px; line-height: 20px">Puedes llevar un control en tiempo real de los invitados
            confirmados.</p>
        <img src="<?php echo base_url() ?>dist/img/planeador_boda/invitados/asistencia.png"
             style="max-width: 100%; height: auto">
    </div>
</div>
<div class="col s12 m12 texture-corazones" style=" padding-top: 20px; padding-bottom: 20px">
    <div class="body-container">
        <div style="width: 80%; margin-left: auto; margin-right: auto;">
            <p style="font-weight: bold; font-size: 21px">Acomodo de mesas e invitados</p>
            <p style="font-size: 16px; line-height: 20px; text-align: justify">Cuando est&eacute;s acomodando tus mesas,
                podr&aacute;s ver el nombre de tus invitados, de esta manera ser&aacute; m&aacute;s f&aacute;cil
                identificarlos y seleccionar las mesas.</p>
        </div>
    </div>
</div>
<!--<div class="fondo_gradiente">
            <div class="body-container">
                <div class="row">
                    <div class="img_cel col m5 pull-left">
                        <div class="cel" style="float: right">
                            <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
                        </div>
                    </div>
                    <div class="col s12 m7">
                        <div class="descarga" style="float: left">
                            <p class="white-text" style="font-size: 22px; line-height: 25px"><b>Tu administrador de invitados siempre contigo.</b></p>
                            <p class="white-text" style="text-align: justify; font-size: 14px; line-height: 20px;">
                                Descargando la aplicaci&oacute;n, el administrador de invitados ir&aacute; contigo a todas partes.
                            </p>
                            <p class="dorado-2-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                            <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png"><p>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
</body>
<?php $this->view('japy/footer');
