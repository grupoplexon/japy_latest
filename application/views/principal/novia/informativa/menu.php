<?php
$controller = $_SERVER['REQUEST_URI'];
$controller = strtolower($controller);
?>
<style>
.seccion-novia b{
    color: white !important;
}
</style>
<section class="primary-background">
    <div class="body-container seccion-novia">
        <div class="row hide-on-med-and-down">
            <a class="valign-wrapper col s1 m1-1 <?php echo (strpos($controller,'home/planeador-bodas'))? 'active' : '' ?>" href="<?php echo base_url() ?>home/planeador-bodas"><b>Planeador de Boda</b></a>
            <a class="valign-wrapper col s1 m1-1 <?php echo (strpos($controller,'home/agenda-tareas'))? 'active' : '' ?>" href="<?php echo base_url() ?>home/agenda-tareas"><b style="margin-left: 22%">Agenda</b></a>
            <a class="valign-wrapper col s1 m1-1 <?php echo (strpos($controller,'home/lista-invitados'))? 'active' : '' ?>" href="<?php echo base_url() ?>home/lista-invitados"><b style="margin-left: 22%">Invitados</b></a>
            <a class="valign-wrapper col s1 m1-1 <?php echo (strpos($controller,'home/control-mesas'))? 'active' : '' ?>" href="<?php echo base_url() ?>home/control-mesas"><b>Control de Mesas</b></a>
            <a class="valign-wrapper col s1 m1-1 <?php echo (strpos($controller,'home/proveedores'))? 'active' : '' ?>" href="<?php echo base_url() ?>home/proveedores"><b style="margin-left: 18%">Proveedores</b></a>
            <a class="valign-wrapper col s1 m1-1 <?php echo (strpos($controller,'home/presupuesto'))? 'active' : '' ?>" href="<?php echo base_url() ?>home/presupuesto"><b>Presupuestador Inteligente</b></a>
            <a class="valign-wrapper col s1 m1-1 <?php echo (strpos($controller,'home/vestidos'))? 'active' : '' ?>" href="<?php echo base_url() ?>home/vestidos"><b style="margin-left: 20%">Mis Vestidos</b></a>
<!--            <a class="valign-wrapper col s1 m1-1 --><?php //echo (strpos($controller,'home/portal_web'))? 'active' : '' ?><!--" href="--><?php //echo base_url() ?><!--home/portal_web"><b>Mi p&aacute;gina Web</b></a>-->
        </div>
    </div>
</section>
<nav class="primary-background hide-on-large-only" style="padding-left: 10%">
    <a href="#" data-activates="mobile-demo2" class="button-collapse"><i class="material-icons">menu</i></a>
    <ul class="side-nav" id="mobile-demo2">
        <li><a class="valign-wrapper col s1 m1-1 active dorado-2-text <?php echo (strpos($controller,'home/planeador-bodas'))? 'hide' : '' ?>" href="<?php echo base_url() ?>home/planeador-bodas"><b style="margin-left: 10%">Planeador de Boda</b></a></li>
        <li><a class="valign-wrapper col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/agenda-tareas"><b style="margin-left: 22%">Agenda</b></a></li>
        <li><a class="valign-wrapper col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/lista-invitados"><b style="margin-left: 22%">Invitados</b></a></li>
        <li><a class="valign-wrapper col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/control-mesas"><b style="margin-left: 12%">Control de Mesas</b></a></li>
        <li><a class="valign-wrapper col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/proveedores"><b style="margin-left: 18%">Proveedores</b></a></li>
        <li><a class="valign-wrapper col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/presupuesto"<b style="margin-left: 4%">Presupuestador Inteligente</b></a></li>
        <li><a class="valign-wrapper col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/vestidos"><b style="margin-left: 20%">Mis Vestidos</b></a></li>
<!--        <li><a class="valign-wrapper col s1 m1-1 dorado-2-text" href="--><?php //echo base_url() ?><!--home/portal_web"><b>Mi p&aacute;gina Web</b></a></li>-->
    </ul>
</nav>
