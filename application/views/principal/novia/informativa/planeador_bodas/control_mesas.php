<?php $this->view('principal/header'); ?>
<style>
    .seccion-novia > .row > .m1-1{
        color: white;
        text-align: center;
        border-right: 1px solid #6c6d6f;
        padding: 10px 10px 10px 10px;
        min-height: 70px;
        text-decoration: none;
    }
    .seccion-novia > .row{
        border-right: transparent;
        margin: 0px;
    }
    .seccion-novia > .row > .m1-1 .active{
        background-color: #6c6d71;
        border-bottom: 5px solid #f9a897;
    }
    .banner_planeador{
        background: url('<?php echo base_url() ?>/dist/img/planeador_boda/control_mesas/banner_mesas.png') no-repeat center;
        width: 100%;
        min-height: 300px;
        background-size: cover;
    }
    .fondo_gradiente{
        background: rgba(158,155,158,1);
        background: -moz-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(158,155,158,1)), color-stop(70%, rgba(158,155,158,1)), color-stop(100%, rgba(94,91,94,1)));
        background: -webkit-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -o-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -ms-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: linear-gradient(to bottom, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9e9b9e', endColorstr='#5e5b5e', GradientType=0 );
        height: 400px;
        overflow: hidden;
    }
    .cel{
        margin: 0;
        margin-left: 10%;
        padding-top: 50px;
    }
    .descarga{
        margin: 0;
        margin-top: 100px;
    }
    @media only screen and (max-width:1040px) {
    /* hi-res laptops and desktops */
        .fondo_gradiente{
            height: auto;
        }
        .descarga{
            margin: 0;
            margin-left: 10%;
            margin-right: 10%;
            position: relative;
        }
        .img_cel{
            display: none;
        }
    }
</style>
<body>
    <div class="col s12 m12 banner_planeador" style="margin-top: 50px">
        <div class="row" style="margin: 0">
            <div class="col s10 m6" style="position: relative; padding-left: 10%; margin-top: 50px">
                <p class="col s7 m7 valign"style="font-size: 35px; color: white; position: relative;line-height: 35px;">
                    <b> Ya no te preocupes por la asignaci&oacute;n de mesas y acomodo de invitados </b>
                </p>
                <p class="col s5 m5 responsive-img valign"><img src="<?php echo base_url()?>dist/img/planeador_boda/control_mesas/icono-controldemesas.png"></p>
                <p class="col s10" style="font-size: 20px; color: white; text-align: justify; position: relative; line-height: 20px">

                </p>
            </div>
        </div>
    </div>
    <?php $this->load->view('principal/novia/informativa/planeador_bodas/menu.php') ?>
    <div class="col s12 m12" style="background: url(<?php echo base_url() ?>dist/img/textura_footer2.png); padding-top: 20px; padding-bottom: 20px">
        <div class="body-container">
            <div class="row">
                <div class="col s12 m8">
                    <p style="font-size: 16px; line-height: 20px; text-align: justify">
                        Cada ajuste y cambio que hagas en el acomodo de las mesas, podr&aacute;s verlo en tiempo real y hacer todos los movimientos que creas convenientes.
                    </p>
                </div>
                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?php echo base_url() ?>dist/img/planeador_boda/control_mesas/control_mesas.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="fondo_gradiente">
        <div class="body-container">
            <div class="row">
                <div class="img_cel col m5 pull-left">
                    <div class="cel" style="float: right">
                        <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
                    </div>
                </div>
                <div class="col s12 m7">
                    <div class="descarga" style="float: left">
                        <p class="white-text" style="font-size: 22px; line-height: 25px"><b>Tu organizador de mesas siempre contigo.</b></p>
                        <p class="white-text" style="text-align: justify; font-size: 14px; line-height: 20px;">
                            Descargando la aplicaci&oacute;n, el administrador de invitados ira contigo a todas partes.
                        </p>
                        <p class="dorado-2-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                        <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png"><p>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
</body>
<?php $this->view('principal/footer'); ?>
