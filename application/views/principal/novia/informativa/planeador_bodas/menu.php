<?php
$controller = $_SERVER['REQUEST_URI'];
$controller = strtolower($controller);
?>
<section class="grey darken-7">
    <div class="body-container seccion-novia">
        <div class="row hide-on-med-and-down">
            <a class="col s1 m1-1 <?php echo (strpos($controller, 'home/planeador-bodas')) ? 'hide' : '' ?>"
               href="<?php echo base_url() ?>home/planeador-bodas">Planeador de Boda</a>
            <a class="col s1 m1-1 <?php echo (strpos($controller, 'home/agenda-tareas')) ? 'active' : '' ?>"
               href="<?php echo base_url() ?>home/agenda-tareas">Agenda</a>
            <a class="col s1 m1-1 <?php echo (strpos($controller, 'home/lista-invitados')) ? 'active' : '' ?>"
               href="<?php echo base_url() ?>home/lista-invitados">Invitados</a>
            <a class="col s1 m1-1 <?php echo (strpos($controller, 'home/control-mesas')) ? 'active' : '' ?>"
               href="<?php echo base_url() ?>home/control-mesas">Control de Mesas</a>
            <a class="col s1 m1-1 <?php echo (strpos($controller, 'home/proveedores')) ? 'active' : '' ?>"
               href="<?php echo base_url() ?>home/proveedores">Proveedores</a>
            <a class="col s1 m1-1 <?php echo (strpos($controller, 'home/presupuesto')) ? 'active' : '' ?>"
               href="<?php echo base_url() ?>home/presupuesto">Presupuestador Inteligente</a>
            <a class="col s1 m1-1 <?php echo (strpos($controller, 'home/vestidos')) ? 'active' : '' ?>"
               href="<?php echo base_url() ?>home/vestidos">Mis Vestidos</a>
            <a class="col s1 m1-1 <?php echo (strpos($controller, 'home/portal-web')) ? 'active' : '' ?>"
               href="<?php echo base_url() ?>home/portal-web">Mi p&aacute;gina Web</a>
        </div>
    </div>
</section>
<nav class="grey darken-7 hide-on-large-only" style="padding-left: 10%">
    <a href="#" data-activates="mobile-demo2" class="button-collapse"><i class="material-icons">menu</i></a>
    <ul>
        <li><a class="col s1 m1-1 active dorado-2-text
        <?php echo (strpos($controller, 'home/planeador-bodas')) ? 'hide' : '' ?>"
               href="<?php echo base_url() ?>home/planeador-bodas">Planeador de Boda</a></li>
        <li>
            <a class="col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/agenda-tareas">Agenda</a>
        </li>
        <li>
            <a class="col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/lista-invitados">
                Invitados
            </a>
        </li>
        <li>
            <a class="col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/control-mesas">
                Control de Mesas
            </a>
        </li>
        <li>
            <a class="col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/proveedores">
                Proveedores
            </a>
        </li>
        <li>
            <a class="col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/presupuesto">
                Presupuestador Inteligente
            </a>
        </li>
        <li>
            <a class="col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/vestidos">
                Mis Vestidos
            </a>
        </li>
        <li>
            <a class="col s1 m1-1 dorado-2-text" href="<?php echo base_url() ?>home/portal-web">
                Mi p&aacute;gina Web
            </a>
        </li>
    </ul>
</nav>
