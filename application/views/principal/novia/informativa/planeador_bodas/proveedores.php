<?php $this->view('principal/header'); ?>
<style>
    .seccion-novia > .row > .m1-1 {
        color: white;
        text-align: center;
        border-right: 1px solid #6c6d6f;
        padding: 10px 10px 10px 10px;
        min-height: 70px;
        text-decoration: none;
    }

    .seccion-novia > .row {
        border-right: transparent;
        margin: 0px;
    }

    .seccion-novia > .row > .m1-1 .active {
        background-color: #6c6d71;
        border-bottom: 5px solid #f9a897;
    }

    .banner_planeador {
        background: url('<?php echo base_url() ?>/dist/img/planeador_boda/proveedores/banner_proveedores.png') no-repeat center;
        width: 100%;
        min-height: 300px;
        background-size: cover;
    }

    .fondo_gradiente {
        background: rgba(158, 155, 158, 1);
        background: -moz-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(158, 155, 158, 1)), color-stop(70%, rgba(158, 155, 158, 1)), color-stop(100%, rgba(94, 91, 94, 1)));
        background: -webkit-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -o-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: -ms-linear-gradient(top, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        background: linear-gradient(to bottom, rgba(158, 155, 158, 1) 0%, rgba(158, 155, 158, 1) 70%, rgba(94, 91, 94, 1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#9e9b9e', endColorstr='#5e5b5e', GradientType=0);
        height: 400px;
        overflow: hidden;
    }

    .cel {
        margin: 0;
        margin-left: 10%;
        padding-top: 50px;
    }

    .descarga {
        margin: 0;
        margin-top: 100px;
    }

    @media only screen and (max-width: 1040px) {
        /* hi-res laptops and desktops */
        .fondo_gradiente {
            height: auto;
        }

        .descarga {
            margin: 0;
            margin-left: 10%;
            margin-right: 10%;
            position: relative;
        }

        .img_cel {
            display: none;
        }
    }
</style>
<body>
<div class="col s12 m12 banner_planeador" style="margin-top: 50px">
    <div class="row" style="margin: 0">
        <div class="col s10 m6" style="position: relative; padding-left: 10%; padding-top: 50px; padding-right: 10%;">
            <p class="col s7 m7 valign" style="font-size: 35px; color: white; position: relative;line-height: 35px;">
                <b>
                    En Japy encontrar&aacute;s a todos los expertos en bodas.
                </b>
            </p>
            <p class="col s5 m5 valign responsive-img" style="text-align: right"><img
                        src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/icono-proveedores.png"></p>
            <p class="col s12 m12"
               style="font-size: 20px; color: white; text-align: justify; position: relative; line-height: 20px">
                Te ofrecemos una gran lista de proveedores para cada detalle de tu boda.
            </p>
        </div>
    </div>
</div>
<?php $this->load->view('principal/novia/informativa/planeador_bodas/menu.php') ?>
<div class="col s12 m12" style="background: url(<?php echo base_url() ?>dist/img/textura_footer2.png)">
    <div class="body-container">
        <div class="row">
            <div class="col s12 m12">
                <p style="font-size: 16px; line-height: 20px; text-align: center;">
                    Encuentra a todos los proveedores que necesites para cubrir cada detalle de tu boda, adem&aacute;s
                    de que podr&aacute;s elegir a tus favoritos y recibir notificaciones sobre informaci&oacute;n de tu
                    presupuesto.
                </p>
            </div>
            <div class="col s12 m12">
                <div style="width: 80%; margin-left: auto; margin-right: auto; text-align: center">
                    <p>
                        <img class="responsive-img"
                             src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/macbook_proveedores.png">
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="body-container">
    <div class="row">
        <div class="col s12 m8">
            <p style="font-weight: bold; font-size: 21px">Tenemos a los mejores proveedores para ti</p>
            <p style="font-size: 16px; line-height: 20px; text-align: justify">
                Busca varias opciones para que puedas escoger la mejor, en precio, calidad y sobre todo en gusto. Ir&aacute;s
                creando tu base de datos y as&iacute; podr&aacute;s tener la informaci&oacute;n de cada uno.
            </p>
        </div>
        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/tarjeta_proveedor.png">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col s12 m12" style="background: url(<?php echo base_url() ?>dist/img/textura_footer2.png)">
    <div class="body-container">
        <div class="row">
            <div class="col s12 m4" style="float: right">
                <p style="font-weight: bold; font-size: 21px">Tu mejor decisi&oacute;n</p>
                <p style="font-size: 16px; line-height: 20px; text-align: justify">
                    Los proveedores que tenemos en japybodas.com compartir&aacute;n contigo im&aacute;genes sobre sus
                    productos y servicios, como promociones para mejorar tu presupuesto.
                </p>
            </div>
            <div class="col s12 m8 pull-left">
                <div class="row">
                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/proveedor4.jpg">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m6">
                            <div class="col s12 m12">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="<?php echo base_url(
                                        ) ?>dist/img/planeador_boda/proveedores/proveedor2.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m12">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="<?php echo base_url(
                                        ) ?>dist/img/planeador_boda/proveedores/proveedor3.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="body-container">
    <div class="row">
        <div class="col s12 m5">
            <p style="font-weight: bold; font-size: 21px">Las mejores recomendaciones</p>
            <p style="font-size: 16px; line-height: 20px; text-align: justify">
                Olv&iacute;date de preocupaciones y conoce lo que las novias anteriores dijeron sobre los proveedores y
                su experiencia con ellos.
            </p>
        </div>
        <div class="col s12 m7">
            <div class="card">
                <div class="card-image">
                    <img src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/recomendacion.png">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fondo_gradiente">
    <div class="body-container">
        <div class="row">
            <div class="img_cel col m5 pull-left">
                <div class="cel" style="float: right">
                    <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
                </div>
            </div>
            <div class="col s12 m7">
                <div class="descarga" style="float: left">
                    <p class="white-text" style="font-size: 22px; line-height: 25px"><b>Tus proveedores en cualquier
                            lugar.</b></p>
                    <p class="white-text" style="text-align: justify; font-size: 14px; line-height: 20px;">
                        Te invitamos a que descargues la aplicaci&oacute;n de japybodas.com y tengas la facilidad de
                        acceso en cualquier lugar en el que est&eacute;s.
                    </p>
                    <p class="dorado-2-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                    <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png">
                    <p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php $this->view('principal/footer'); ?>
