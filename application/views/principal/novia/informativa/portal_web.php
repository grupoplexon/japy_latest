<html lang="es" xml:lang="es">
<head>
    <?php $this->view("principal/head"); ?>
    <link type="text/css" href="<?php echo base_url() ?>dist/css/novios/page-info.min.css" rel="stylesheet"/>
</head>
<body class="page-informativa">
<?php $this->view("principal/menu"); ?>
<div class="row cabecera portal_web valign-wrapper">
    <div class="body-container">
        <div class="col s11 m7 l6 valign">
            <div class="row">
                <div class="row ">
                    <h4 class=" col s12 title">Mi p&aacute;gina web</h4>
                </div>
            </div>
            <div class="row col s11 m11 l7">
                <p class="subtitle">
                    Podr&aacute;s compartir informaci&oacute;n de tu boda con tus invitados a trav&eacute;s de redes
                    sociales
                </p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('principal/novia/informativa/menu.php') ?>
<div class="row texture-corazones">
    <div class="body-container">
        <div class="col s12 center" style="margin-top:35px;">
            <div class="col s2">&nbsp;</div>
            <p class="col s8" style="line-height: 25px;font-size: 18px;">
                En Japy podr&aacute;s crear tu propia p&aacute;gina web, para compartir informaci&oacute;n, fotos,
                detalles e incluso organizar eventos y actividades previas a la boda. Todo esto tambi&eacute;n lo tendr&aacute;s
                en tus redes sociales.
            </p>
        </div>
        <div class="col s2">&nbsp;</div>
        <div class="col s8 center">
            <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/portal_web/macbook_portal.png" alt="">
        </div>
    </div>
</div>
<?php $this->view("principal/view_footer") ?>
<?php $this->view("principal/foot") ?>
</body>
</html>

