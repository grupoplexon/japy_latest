<html lang="es" xml:lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Japy</title>
    <?php $this->view("japy/header");?>
    <link type="text/css" href="<?php echo base_url() ?>dist/css/novios/page-info.min.css" rel="stylesheet"/>
    <style>
        .contrast-letters{
            text-shadow: 2px 1px 1px black;
        }
        .space-top{
            margin-top: 80px;
        }
        .width-img-costo{
            width: 300px !important;
        }
        @media only screen and (max-width: 425px) {
            .space-top{
                margin-top: 0 !important;
            }
            .width-img-costo{
                margin: 0 13% !important;
            }

        }
    </style>
</head>
<body class="page-informativa">
<div class="row cabecera presupuesto valign-wrapper">
    <div class="body-container">
        <div class="col s11 m7 l6 valign">
            <div class="row">
                <div class="row ">
                    <h4 class=" col s12 title contrast-letters">PRESUPUESTADOR INTELIGENTE</h4>
                </div>
            </div>
            <div class="row col s11 m11 l7">
                <p class="subtitle contrast-letters">
                    Te ayuda a administrar y maximizar tu dinero.
                </p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('principal/novia/informativa/menu.php') ?>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png');margin:0">
    <div class="body-container">
        <div class="col s12 center" style="margin-top:35px;">

            <div class="col s2">&nbsp;</div>
            <p class="col s8" style="line-height: 25px;font-size: 18px;">
                Con esta herramienta podr&aacute; maximizar tu presupuesto y estar pendiente delo que tienes que pagar y
                lo que ya pagaste. De esta manera no te faltar&aacute; nada por cubrir. Adem&aacute;s te recomendaremos
                a los mejores proveedores de acuerdo a tu presupuesto.
            </p>
        </div>
        <div class="col s2">&nbsp;</div>
        <div class="col s8 center">
            <!--<img class="responsive-img" src="<?php echo base_url() ?>/dist/img/presupuesto_promo/macbook_presupuesto.png" alt="" >-->
        </div>

    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m12 l6 space-top" >
            <div class="row col s12 m10 right">
                <p style="font-weight:bold; font-size: 21px;">Pagos a proveedores</p>
                <p style="font-size: 16px;">
                    Con el presupuestador inteligente podr&aacute;s ver los pagos realizados y lo
                    pendiente por pagar. Adem&aacute;s te permite agendar los pendientes en las notas para que no
                    olvides nada.
                <p>
            </div>
        </div>
        <div class="col s12 m12 l6 center" style="margin-top:35px;  margin-bottom:35px">
            <img class="responsive-img col s12 m12" src="<?php echo base_url() ?>/dist/img/presupuesto_promo/costo.png"
                 alt="">
        </div>

    </div>
</div>
<div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png'); margin:0">
    <div class="body-container">

        <div class="col s12 m6 offset-m4 l6 " style="margin-top:35px;  margin-bottom:35px">
            <img class="responsive-img col s10 m10 l8 width-img-costo offset-l4 "
                 src="<?php echo base_url() ?>/dist/img/presupuesto_promo/presupuesto.png" alt="">
        </div>
        <div class="col s12 m12 l6 space-top" >
            <div class="row col s12 m10">
                <p style="font-weight:bold; font-size: 21px;">Revisa tu presupuesto</p>
                <p style="font-size: 16px;">
                    Recuerda que una vez que nos compartes tu presupuesto aproximado total, seleccionaremos a los
                    proveedores que est&eacute;n dentro de tu rango, adem&aacute;s de agendar tus pagos y pendientes.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="fondo_gradiente" style="display: none">
    <div class="body-container">
        <div class="row">
            <div class="img_cel col m5 pull-left">
                <div class="cel" style="float: right">
                    <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
                </div>
            </div>
            <div class="col s12 m7">
                <div class="descarga" style="float: left">
                    <p class="white-text" style="font-size: 22px; line-height: 25px"><b>Tu presupuestador para
                            llevar</b></p>
                    <p class="white-text" style="text-align: justify; font-size: 14px; line-height: 20px;">
                        Teniendo la App de japybodas.com en tu celular podrás tener contigo todo el tiempo tu
                        presupuesto así como todos los demás beneficios.
                    </p>
                    <p class="dorado-2-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                    <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png">
                    <p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view('bannerDownload'); ?>
<script>
    $(document).ready(function () {
        $('.parallax').parallax();
    });
</script>
</body>
<?php $this->view('japy/footer'); ?>
