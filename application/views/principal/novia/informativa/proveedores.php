<html lang="es" xml:lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        
        <title>Japy</title>
        <?php $this->view("japy/header");?>
        <link  type="text/css" href="<?php echo base_url() ?>dist/css/novios/page-info.min.css" rel="stylesheet"   />

        <style>
            @media only screen and (max-width: 425px) {
                .card-container{
                    width: 270px;
                    margin: auto;
                }
                .card-container-image{
                    height: 200px;
                }

                .card-container-image img{
                    height: 100% !important;
                }

                .contrast-letters{
                    text-shadow: 2px 1px 1px black;

                }

            }
        </style>
    </head>
    <body class="page-informativa">
        <div class="row cabecera proveedores valign-wrapper">
            <div class="body-container">
                <div class="col s11 m7 l6 valign">
                    <div class="row"  >
                        <div class="row " >
                            <h4 class=" col s12 title contrast-letters">Proveedores</h4>
                        </div>
                    </div>
                    <div class="row col s11 m11 l7">
                        <p class="subtitle contrast-letters">
                            En Japy encontrarás a todos los expertos en bodas
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('principal/novia/informativa/menu.php') ?>
        <div class="col s12 m12 texture-corazones">
            <div class="body-container">
                <div class="row">
                    <div class="col s12 m12">
                        <p style="font-size: 16px; line-height: 20px; text-align: center;">
                            Encuentra a todos los proveedores que necesites para cubrir cada detalle de tu boda, adem&aacute;s de que podr&aacute;s elegir a tus favoritos y recibir notificaciones sobre informaci&oacute;n de tu presupuesto.
                        </p>
                    </div>
                    <div class="col s12 m12">
                        <div style="width: 80%; margin-left: auto; margin-right: auto; text-align: center">
                            <p>
                                <img class="responsive-img" src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/macbook_proveedores.png">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-container">
            <div class="row">
                <div class="col s12 m5 offset-m2">
                    <p style="font-weight: bold; font-size: 21px">Tenemos a los mejores proveedores para ti</p>
                    <p style="font-size: 16px; line-height: 20px; text-align: justify">
                        Busca varias opciones para que puedas escoger la mejor, en precio, calidad y sobre todo en gusto. Ir&aacute;s creando tu base de datos y as&iacute; podr&aacute;s tener la informaci&oacute;n de cada uno.
                    </p>
                </div>
                <div class="col s12 m3 ">
                    <div class="card card-container" >
                        <div class="card-image card-container-image" >
                        <img  src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/tarjeta_proveedor.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m12  texture-corazones">
            <div class="body-container">
                <div class="row">
                    <div class="col s12 m4" style="float: right">
                        <p style="font-weight: bold; font-size: 21px">Tu mejor decisi&oacute;n</p>
                        <p style="font-size: 16px; line-height: 20px; text-align: justify">
                            Los proveedores que tenemos en japybodas.com compartir&aacute;n contigo im&aacute;genes sobre sus productos y servicios, como promociones para mejorar tu presupuesto.
                        </p>
                    </div>
                    <div class="col s12 m8 pull-left">
                        <div class="row">
                            <div class="col s12 m4">
                                <div class="card" style="margin: auto;">
                                    <div class="card-image">
                                        <img src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/proveedor4.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/proveedor2.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/proveedor3.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-container">
            <div class="row">
                <div class="col s12 m4">
                    <p style="font-weight: bold; font-size: 21px">Las mejores recomendaciones</p>
                    <p style="font-size: 16px; line-height: 20px; text-align: justify">
                        Olv&iacute;date de preocupaciones y conoce lo que las novias anteriores dijeron sobre los proveedores y su experiencia con ellos.
                    </p>
                </div>
                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?php echo base_url() ?>dist/img/planeador_boda/proveedores/recomendacion.png">
                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?php echo base_url() ?>dist/img/sugerencias.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fondo_gradiente" style="display: none">
            <div class="body-container">
                <div class="row">
                    <div class="img_cel col s12 m5 pull-left">
                        <div class="cel" style="float: right">
                            <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
                        </div>
                    </div>
                    <div class="col s12 m7">
                        <div class="descarga" style="float: left">
                            <p class="white-text" style="font-size: 22px; line-height: 25px"><b>Tus proveedores en cualquier lugar.</b></p>
                            <p class="white-text" style="text-align: justify; font-size: 14px; line-height: 20px;">
                                Te invitamos a que descargues la aplicaci&oacute;n de japybodas.com y tengas la facilidad de acceso en cualquier lugar en el que est&eacute;s.
                            </p>
                            <p class="dorado-2-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                            <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png"><p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->view('bannerDownload'); ?>
    </body>
    <?php
    $this->view('japy/footer');
    