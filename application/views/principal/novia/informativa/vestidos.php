<html lang="es" xml:lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Japy</title>
    <?php $this->view("japy/header"); ?>
    <link type="text/css" href="<?php echo base_url() ?>dist/css/novios/page-info.min.css" rel="stylesheet"/>
    <style>
        .contrast-letters{
            text-shadow: 2px 1px 1px black;
        }

        .space-top{
            margin-top: 80px;
        }
        @media only screen and (max-width: 425px) {
            .space-top{
                margin-top: 0 !important;
            }

        }
    </style>
</head>
<body class="page-informativa">
<div class="row cabecera vestidos valign-wrapper">
    <div class="body-container">
        <div class="col s11 m7 l6 valign">
            <div class="row">
                <div class="row ">
                    <h4 class=" col s12 title contrast-letters">MIS VESTIDOS</h4>
                </div>
            </div>
            <div class="row col s11 m11 l7">
                <p class="subtitle contrast-letters">
                    En Japy encontrar&aacute;s diseños exclusivos de las mejores marcas
                </p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('principal/novia/informativa/menu.php') ?>
<div class="row texture-corazones">
    <div class="body-container">
        <div class="col s12 center" style="margin-top:35px;">
            <div class="col s12"><span class="center" style="font-weight:bold; line-height: 30px; font-size:22px">
                            Encontrar&aacute;s los mejores dise&ntilde;os de las marcas m&aacute;s importantes
                        </span>
            </div>
            <div class="col s2">&nbsp;</div>
            <p class="col s8" style="line-height: 25px;font-size: 18px;">
                Cada proveedor te mostrar&aacute; los dise&ntilde;os en tendencia y dar&aacute; consejos para que puedas
                escoger el mejor vestido
                y as&iacute; luzcas espectacular en tu gran d&iacute;a. Adem&aacute;s encontrarás hermosos vestidos para
                tus damas de honor.
            </p>
        </div>
        <div class="col s2">&nbsp;</div>
        <div class="col s8 center">
            <!--<img class="responsive-img" src="<?php echo base_url(
            ) ?>/dist/img/vestidos/macbook_vestidos.png" alt="" >-->
        </div>

    </div>
</div>
<div class="row">
    <div class="body-container">
        <div class="col s12 m12 l6 space-top">
            <div class="row col s12 m10 right">
                <p style="font-weight:bold; font-size: 21px;">Ponte en contacto con tu proveedor</p>
                <p style="font-size: 16px;">
                    Agenda una cita con tu proveedor, una vez que hayas seleccionado tu vestido de novia.
                </p>
            </div>
        </div>
        <div class="col s12 m12 l6" style="margin-top:35px;  margin-bottom:35px">
            <img class="responsive-img col s12 m12" src="<?php echo base_url() ?>/dist/img/vestidos/contactar.png"
                 alt="">
        </div>

    </div>
</div>
<div class="fondo_gradiente" style="display: none">
    <div class="body-container">
        <div class="row">
            <div class="img_cel col m5 pull-left">
                <div class="cel" style="float: right">
                    <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
                </div>
            </div>
            <div class="col s12 m7">
                <div class="descarga" style="float: left">
                    <p class="white-text" style="font-size: 22px; line-height: 25px"><b>INSPIRACIÓN PARA LLEVAR!</b>
                    </p>
                    <p class="white-text" style="text-align: justify; font-size: 14px; line-height: 20px;">
                        En el lugar y momento que quieras. Descarga japybodas.com para descubrir tu vestido perfecto
                        de entre la variedad de diseños,
                        puedes marcarlos para volver a verlos y enseñarlo a quien tu gustes.
                    </p>
                    <p class="dorado-2-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                    <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png">
                    <p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view('bannerDownload');?>

<script>
    $(document).ready(function () {
        $('.parallax').parallax();
    });
</script>
</body>
<?php $this->view('japy/footer');?>
