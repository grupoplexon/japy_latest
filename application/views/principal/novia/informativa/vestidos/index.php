<?php $this->view('principal/header'); ?>
<style>
    .parallax-container{
        width: 100%;
    }
    .seccion-novia > .row > .m1-1{
        color: white;
        text-align: center;
        border-right: 1px solid #6c6d6f;
        padding: 10px 10px 10px 10px;
        min-height: 70px;
        text-decoration: none;
    }
    .seccion-novia > .row{
        border-right: transparent;
        margin: 0px;
    }
    .seccion-novia > .row > .m1-1 .active{
        background-color: #6c6d71;
        border-bottom: 5px solid #f9a897;
    }
    .icono-planeador,.titulo_icono,.descripcion_icono{
        text-align: center;
    }
    .descripcion_icono{
        line-height: 15px;
        font-size: 12px;
    }
    .fondo_gradiente{
        background: rgba(158,155,158,1);
        background: -moz-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(158,155,158,1)), color-stop(70%, rgba(158,155,158,1)), color-stop(100%, rgba(94,91,94,1)));
        background: -webkit-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -o-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -ms-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: linear-gradient(to bottom, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9e9b9e', endColorstr='#5e5b5e', GradientType=0 );
        height: 400px;
        overflow: hidden;
    }
    .cel{
        margin: 0;
        margin-left: 10%;
        padding-top: 50px;
    }
    .descarga{
        margin: 0;
        margin-top: 100px;
    }
    @media only screen and (min-width:1281px) {
        /* hi-res laptops and desktops */
        .fondo_gradiente{

        }
    }
    @media only screen and (max-width:1040px) {
        /* hi-res laptops and desktops */
        .fondo_gradiente{
            height: auto;
        }
        .descarga{
            margin: 0;
            margin-left: 10%;
            margin-right: 10%;
            position: relative;
        }
        .img_cel{
            display: none;
        }
    }
</style>
<body>
    <div class="row" style="margin: 0;margin-top: 50px;width:100%; height:300px; background: url('<?php echo base_url() ?>/dist/img/vestidos/fondo2.png') no-repeat left center #fff;
         background-size: cover;">
        <div class="body-container">
            <div style="color:white; margin-left:10%;" class="col s11 m7 l6">
                <div class="row " style="margin-top:50px; margin-bottom:0">
                    <div class="row valign-wrapper" >
                        <h4 class="valign col s7" style="color:white; line-height: 30px; font-size: 35px; margin: 0; font-weight:bold;">MIS VESTIDOS</h4>
                        <div class="valign col s5 m5"><img class="responsive-img" src="<?php echo base_url() ?>/dist/img/vestidos/icono.png" alt="" ></div>
                    </div>
                </div>
                <div class="row col s11 m11 l9" style="margin:0">
                    <p style="font-size: 16px; line-height: 25px; margin: 0;color:white">En japybodas.com encontrar&aacute;s diseños exclusivos de las mejores marcas</p>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('principal/novia/informativa/planeador_bodas/menu.php') ?>
    <div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png');margin:0">
        <div class="body-container">
            <div class="col s12 center" style="margin-top:35px;">
                <div class="col s12"><span class="center" style="font-weight:bold; line-height: 30px; font-size:22px">
                        Encontrar&aacute;s los mejores dise&ntilde;os de las marcas m&aacute;s importantes
                    </span>
                </div>
                <div class="col s2">&nbsp;</div>
                <p class="col s8" style="line-height: 25px;font-size: 18px;">
                    Cada proveedor te mostrar&aacute; los dise&ntilde;os en tendencia y dar&aacute; consejos para que puedas escoger el mejor vestido y as&iacute; luzcas espectacular en tu gran d&iacute;a. Adem&aacute;s encontrarás hermosos vestidos para tus damas de honor.
                </p>
            </div>
            <div class="col s2">&nbsp;</div>
            <div class="col s8 center">
                <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/vestidos/macbook_vestidos.png" alt="" >
            </div>

        </div>
    </div>
    <div class="row">
        <div class="body-container">
            <div class="col s12 m12 l6" style="margin-top:90px;">
                <div class="row col s12 m10 right" >
                    <p style="font-weight:bold; font-size: 21px;">Ponte en contacto con tu proveedor</p>
                    <p style="font-size: 16px;" >
                         Agenda una cita con tu proveedor, una vez que hayas seleccionado tu vestido de novia.
                    </p>
                </div>
            </div>
            <div class="col s12 m12 l6" style="margin-top:35px;  margin-bottom:35px">
                <img class="responsive-img col s12 m12" src="<?php echo base_url() ?>/dist/img/vestidos/contactar.png" alt="" >
            </div>

        </div>
    </div>
    <div class="fondo_gradiente">
        <div class="body-container">
            <div class="row">
                <div class="img_cel col m5 pull-left">
                    <div class="cel" style="float: right">
                        <img src="<?php echo base_url() ?>dist/img/planeador_boda/celular.png">
                    </div>
                </div>
                <div class="col s12 m7">
                    <div class="descarga" style="float: left">
                        <p class="white-text" style="font-size: 22px; line-height: 25px"><b>INSPIRACIÓN PARA LLEVAR!</b></p>
                        <p class="white-text" style="text-align: justify; font-size: 14px; line-height: 20px;">
                            En el lugar y momento que quieras. Descarga japybodas.com para descubrir tu vestido perfecto de entre la variedad de diseños, puedes marcarlos para volver a verlos y enseñarlo a quien tu gustes.
                        </p>
                        <p class="dorado-2-text" style="font-size: 16px"><b>DESCARGA LA APP</b></p>
                        <p><img src="<?php echo base_url() ?>dist/img/planeador_boda/descargar_app.png"><p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.parallax').parallax();
        });
    </script>
</body>
<?php $this->view('principal/footer'); ?>
