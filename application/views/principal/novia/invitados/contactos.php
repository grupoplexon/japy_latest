<?php $this->view("principal/header") ?>
<?php $this->view("principal/novia/menu") ?>
    <div class=" body-container">

        <?php $this->view("principal/novia/invitados/menu_principal") ?>

        <div class="row" style="border-top: 1px solid #eee; margin-top: -21px; padding: 10 0 10 0;">
            <div class="col s12 l4">
                <div class="collection grupo">
                    <a class="collection-item grey lighten-3">
                        <input class="formulario" id="input-buscar" type="text" style="width: 100%"
                               placeholder="Buscar...">
                    </a>
                </div>
                <div class="collection grupo name-collection"
                     style="overflow: visible; height: 600px; overflow-y: scroll; margin-top: -18px;">
                    <?php foreach ($invitados as $key => $value) { ?>
                        <a class="collection-item"
                           onclick="updateInterfacePrincipal('<?php echo $value->id_invitado ?>')"><?php echo $value->nombre . ' ' . $value->apellido; ?></a>
                    <?php } ?>
                </div>
            </div>

            <div class="col s12 l8">
                <div class="border listainvitados">
                    <ul class="collection migrupo">
                        <li class="collection-item">
                            &nbsp;
                            <a class="waves-effect waves-light btn-flat right botn" id="formulario_delete" style="z-index: 0">
                                <i class="material-icons">delete_forever</i>
                            </a>
                            <a class="modal-trigger waves-effect waves-light btn-flat right botn no-zindex" id="formulario_update"
                               href="#update_invitado">
                                <i class="material-icons">mode_edit</i>
                            </a>
                        </li>
                        <li class="collection-item avatar margin">
                            <i class="circle invitado invitados-Hombre" id="icono_person"></i>
                            <span class="title invitado">&nbsp;&nbsp;</span>
                            <div class="row">
                                <div class="col s12 l3 avatar-item" style="border-right:1px solid black;">
                                    <select class="listagrupo" id="formulario_id_confirmacion"
                                            onchange="updateInvitado('', 'confirmacion', this);">
                                        <option value='' disabled selected>Asistencia</option>
                                        <option value='1' class="orange-text darken-3">&#xf017;&nbsp;&nbsp;&nbsp;&nbsp;Pendiente</option>
                                        <option value='2' class="green-text darken-4">&#xf05d;&nbsp;&nbsp;&nbsp;&nbsp;Confirmado</option>
                                        <option value='3' class="deep-orange-text darken-4">&#xf057;&nbsp;&nbsp;&nbsp;&nbsp;Cancelado</option>
                                    </select>
                                </div>
                                <div class="col s12 l3 avatar-item" style="border-right:1px solid black;">
                                    <select class="listagrupo" id="formulario_id_grupo"
                                            onchange="updateInvitado('', 'grupo', this);">
                                        <option value='' disabled selected>Grupo</option>
                                        <?php foreach ($grupos as $k => $gp) { ?>
                                            <option value="<?php echo $gp->id_grupo ?>" <?php echo ($gp->grupo == 'Novios') ? 'disabled' : ''; ?>><?php echo $gp->grupo ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col s12 l3 avatar-item" style="border-right:1px solid black;">
                                    <select class="listagrupo mesas-select" id="formulario_id_mesa"
                                            onchange="updateInvitado('', 'mesa', this);"
                                            data-mesa="<?php echo $value->id_mesa ?>">
                                        <option value='' disabled selected>Mesa</option>
                                        <?php foreach ($mesas as $y => $ms) { ?>
                                            <option value="<?php echo $ms->id_mesa ?>" <?php echo (($ms->sillas - $ms->ocupado) <= 0) ? ' disabled ' : '' ?>
                                                    data-sillas="<?php echo $ms->sillas ?>"
                                                    data-ocupadas="<?php echo $ms->ocupado ?>"
                                                    data-nombre="<?php echo $ms->nombre ?>"
                                                <?php echo ($ms->id_mesa == $value->id_mesa) ? 'selected' : '' ?>><?php echo $ms->nombre ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col s12 l3 avatar-item">
                                    <select class="listagrupo" id="formulario_id_menu"
                                            onchange="updateInvitado('', 'menu', this);">
                                        <option value='' disabled selected>Men&uacute;</option>
                                        <?php foreach ($menus as $x => $mn) { ?>
                                            <option value="<?php echo $mn->id_menu ?>"><?php echo $mn->nombre ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="section">
                        <h6><b>DATOS DE CONTACTO</b></h6>
                        <div class="row">
                            <div class="col s10 l3 offset-l9 offset-s1  right provider-side">
                                <select class="listagrupo boton" id="formulario_id_email">
                                    <option value="1" selected>&#xf003;&nbsp;&nbsp;&nbsp;&nbsp;Invitaci&oacute;n
                                        enviada
                                    </option>
                                    <option value="2">&#xf003;&nbsp;&nbsp;&nbsp;&nbsp;Invitaci&oacute;n no enviada
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s2">Email:</div>
                            <div class="col s10" id="formulario_email"></div>
                        </div>
                        <div class="row">
                            <div class="col s12 l2">Tel&eacute;fono:</div>
                            <div class="col s12 l4" id="formulario_telefono"></div>
                            <div class="col s12 l4" id="formulario_celular"></div>
                        </div>
                    </div>
                    <div class="divider"></div>

                    <div class="section">
                        <h6><b>DIRECCI&Oacute;N POSTAL</b></h6>
                        <div class="row">
                            <div class="col s12 l2">Pa&iacute;s:</div>
                            <div class="col s12 l4" id="formulario_pais"></div>
                            <div class="col s12 l2">Estado:</div>
                            <div class="col s12 l4" id="formulario_estado"></div>
                        </div>
                        <div class="row">
                            <div class="col offset-l6 s12 l2">Poblado:</div>
                            <div class="col s12 l4" id="formulario_poblado"></div>
                        </div>
                        <div class="row">
                            <div class="col s12 l2">Direcci&oacute;n:</div>
                            <div class="col s12 l4" id="formulario_direccion"></div>
                            <div class="col s12 l2">C.P.:</div>
                            <div class="col s12 l4" id="formulario_cp"></div>
                        </div>
                    </div>
                    <div class="divider"></div>

                    <div class="section" id="seccion_acompanante">
                        <h6><b>ACOMPA&Ntilde;ANTE</b></h6>
                        <div id="formulario_acompanante">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="update_invitado" class="modal">
        <form method="post" action="<?php echo base_url() ?>novios/invitados/Home/update_invitado">
            <div class="modal-content">
                <h6 class="dorado-2-text">Invitado</h6>
                <div class="divider"></div>
                <div class="row" style="margin-bottom: 0">

                    <div class="col s12" style="padding: 0">
                        <ul class="tabs menu-modal" style="margin: 15px 0;">
                            <li class="tab col m4" id="d_p">
                                <a id="addInvitado1" class="grey-text" href="#datos_personales">
                                    <i class=" material-icons hide-on-med-and-up">assignment_ind</i>
                                    <span class="hide-on-small-only ">Datos Personales</span>
                                </a>
                            </li>
                            <li class="tab col m4" id="d_c">
                                <a id="addInvitado2" class="grey-text" href="#datos_contacto">
                                    <i class=" material-icons hide-on-med-and-up">contact_phone</i>
                                    <span class="hide-on-small-only ">Datos Contacto</span>
                                </a>
                            </li>
                            <li class="tab col m4" id="d_a">
                                <a id="addInvitado3" class="grey-text" href="#acompanante">
                                    <i class=" material-icons hide-on-med-and-up">group_add</i>
                                    <span class="hide-on-small-only ">Acompa&ntilde;antes</span>
                                </a>
                                <!--                                <span id="contador" class="contador primary-text" style="display: none;">0</span>-->
                            </li>
                        </ul>

                    </div>

                    <select name="tabs" class="formulario col s10 offset-s1 hide" id="option-invited" style="background-color: white; border:1px solid grey;">
                        <option id="personaldata" href="#datos_personales" class="grey-text" value="Datos personales">Datos Personales</option>
                        <option id="contactdata" href="#datos_contacto" class="grey-text" value="Datos de contacto">Datos de Contacto</option>
                        <option id="scort" href="#acompanante" class="grey-text" value="Acompañantes">Acompañantes</option>

                    </select>
                    <div id="datos_personales" class="col s12">
                        <div class="row" style="margin-bottom: 0;color: black">
                            <div class="col s12 l3 center-align hide-on-small-only">
                                <i class="responsive-img invitados invitados-Hombre" id="icon_persona_modal"></i>
                            </div>
                            <div class="col s12 l9">
                                <div class="row">
                                    <input id="id_invitado" name="id_invitado" type="hidden">
                                    <input name="nombre" class="formulario col s12 l6" type="text" placeholder="Nombre(s)*"
                                           required>
                                    <input name="apellido" class="formulario col s12 l6" type="text"
                                           placeholder="Apellido(s)" required>
                                    <!-- DIVIDER -->
                                    <select name="grupo" class="formulario col s12 l6" required>
                                        <option value="" disabled selected>Grupo*</option>
                                        <?php foreach ($grupos as $key => $value) { ?>
                                            <option value="<?php echo $value->id_grupo ?>" <?php echo ($value->grupo == 'Novios') ? 'disabled' : ''; ?> ><?php echo ($value->grupo == 'Novios') ? $value->grupo : str_replace($this->session->userdata('genero'), $this->session->userdata('nombre'), $value->grupo); ?></option>
                                        <?php } ?>
                                    </select>
                                    <select name="menu" class="formulario col s12 l6" required>
                                        <option value="" disabled selected>Men&uacute;*</option>
                                        <?php foreach ($menus as $key => $value) { ?>
                                            <option value="<?php echo $value->id_menu ?>"><?php echo $value->nombre ?></option>
                                        <?php } ?>
                                    </select>
                                    <!-- DIVIDER -->
                                    <select name="sexo" class="formulario col s12 l6" required>
                                        <option value="" disabled selected>Sexo*</option>
                                        <option value="1">Hombre</option>
                                        <option value="2">Mujer</option>
                                    </select>
                                    <select name="edad" class="formulario col s12 l6" required>
                                        <option value="" disabled selected>Edad*</option>
                                        <option value="1">Adulto</option>
                                        <option value="2">Ni&ntilde;o</option>
                                        <option value="3">Beb&eacute;</option>
                                    </select>
                                    <!-- DIVIDER -->
                                    <button class="waves-effect waves-light btn-flat col s12 dorado-2-text left-align hide"
                                            type="button" onclick="$('#addInvitado2').click();">
                                        <small><i class="material-icons left">add</i>A&ntilde;adir datos de contacto
                                        </small>
                                    </button>
                                    <button class="waves-effect waves-light btn-flat col s12 dorado-2-text left-align hide"
                                            type="button" onclick="$('#addInvitado3').click();">
                                        <small><i class="material-icons left">add</i>A&ntilde;adir acompa&ntilde;ante
                                        </small>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="datos_contacto" class="col s12">
                        <div class="row" style="margin-bottom: 0;color: black">
                            <input class="formulario col s12" type="email" name="correo" placeholder="E-mail">
                            <!-- DIVIDER -->
                            <input class="formulario col s12 l6" type="tel" name="telefono" placeholder="Tel&eacute;fono">
                            <input class="formulario col s12 l6" type="tel" name="celular" placeholder="Celular">
                            <!-- DIVIDER -->
                            <h6><b>Direcci&oacute;n postal</b></h6>
                            <select name="country" class="col s12 l6 browser-default countries" id="countryId">
                                <option value="" disabled selected>-- Pa&iacute;s --</option>
                            </select>
                            <select name="state" class="col s12 l6 browser-default states" id="stateId">
                                <option value="" disabled selected>-- Estado --</option>
                            </select>
                            <!-- DIVIDER -->
                            <select name="city" id="cityId" class="col s12 offset-l6 l6 browser-default cities">
                                <option value="" disabled selected>-- Poblaci&oacute;n --</option>
                            </select>
                            <!-- DIVIDER -->
                            <input class="formulario col s12 l6" type="text" name="direccion"
                                   placeholder="Direcci&oacute;n">
                            <input class="formulario col s12 l6" type="text" name="cp" placeholder="C&oacute;digo Postal">
                        </div>
                    </div>

                    <div id="acompanante" class="col s12">
                        <div class="row" style="margin-bottom: 0">
                            <div class="col s12 " style="margin-bottom: 20px;">
                                <ul class="tabs">
                                    <li class="tab col s4">
                                        <a class="active btn-flat" href="#anadir_nuevo">
                                            <i class="material-icons left dorado-2-text">add_circle</i>
                                            A&ntilde;adir nuevo
                                        </a>
                                    </li>
                                    <li class="tab col s4">
                                        <a class="btn-flat" href="#contacto_existente">
                                            <i class="material-icons left dorado-2-text">view_list</i>
                                            Contacto existente
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div id="anadir_nuevo" class="col s12">
                                <div class="row" style="color: black">
                                    <input id="nombre_acompanante" class="formulario col s6" type="text"
                                           placeholder="Nombre(s)*">
                                    <input id="apellido_acompanante" class="formulario col s6" type="text"
                                           placeholder="Apellido(s)*">
                                    <!-- DIVIDER -->
                                    <select id="grupo_acompanante" class="formulario col s6">
                                        <option value="" disabled selected>Grupo*</option>
                                        <?php foreach ($grupos as $key => $value) { ?>
                                            <option value="<?php echo $value->id_grupo ?>" <?php echo ($value->grupo == 'Novios') ? 'disabled' : ''; ?> ><?php echo ($value->grupo == 'Novios') ? $value->grupo : str_replace($this->session->userdata('genero'), $this->session->userdata('nombre'), $value->grupo); ?></option>
                                        <?php } ?>
                                    </select>
                                    <select id="menu_acompanante" class="formulario col s6">
                                        <option value="" disabled>Men&uacute;</option>
                                        <?php foreach ($menus as $key => $value) { ?>
                                            <option value="<?php echo $value->id_menu ?>"
                                                    selected><?php echo $value->nombre ?></option>
                                        <?php } ?>
                                    </select>
                                    <!-- DIVIDER -->
                                    <select id="sexo_acompanante" class="formulario col s6">
                                        <option value="" disabled>Sexo*</option>
                                        <option value="1" selected>Hombre</option>
                                        <option value="2">Mujer</option>
                                    </select>
                                    <select id="edad_acompanante" class="formulario col s6">
                                        <option value="" disabled>Edad*</option>
                                        <option value="1" selected>Adulto</option>
                                        <option value="2">Ni&ntilde;o</option>
                                        <option value="3">Beb&eacute;</option>
                                    </select>
                                    <!-- DIVIDER -->
                                    <button class="btn invitados left-align grey darken-5" id="anadir_invitado_form"
                                            type="button">
                                        A&Ntilde;ADIR
                                    </button>
                                </div>
                            </div>
                            <div id="contacto_existente" class="col s12">
                                <select id="id_acompanante" class="formulario col s6" style="color: black">
                                    <?php
                                    foreach ($invitados as $key => $inv) {
                                        if ($inv->grupo != 'Novios') {
                                            ?>
                                            <option value="<?php echo $inv->id_invitado ?>"
                                                    data-sexo="<?php echo $inv->sexo ?>"
                                                    data-edad="<?php echo $inv->edad ?>"><?php echo $inv->nombre . ' ' . $inv->apellido ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <button class="col s12 l2 btn invitados left-align grey darken-5" id="anadir_invitado"
                                        type="button">
                                    A&Ntilde;ADIR
                                </button>
                            </div>
                        </div>
                        <div id="acompanante_new">

                        </div>
                    </div>

                </div>
            </div>
            <?php $this->view("principal/novia/invitados/modal-footer");?>
        </form>
    </div>

    <script>
        function onReady() {
            $('#update_invitado').modal();
            $('#anadir_invitado_form').on('click', function () {
                if (isEmpty($('#grupo_acompanante').val()) || isEmpty($('#nombre_acompanante').val()) || isEmpty($('#apellido_acompanante').val())) {


                    if (isEmpty($('#nombre_acompanante').val())) {
                        $('#nombre_acompanante').attr('class', $('#nombre_acompanante').attr('class') + ' invalid');
                    } else {
                        $('#nombre_acompanante').attr('class', (($('#nombre_acompanante').attr('class')).split('invalid')[0]));
                    }


                    if (isEmpty($('#apellido_acompanante').val())) {
                        $('#apellido_acompanante').attr('class', $('#apellido_acompanante').attr('class') + ' invalid');
                    } else {
                        $('#apellido_acompanante').attr('class', (($('#apellido_acompanante').attr('class')).split('invalid')[0]));
                    }

                    if (isEmpty($('#grupo_acompanante').val())) {
                        alert('Seleccione un grupo');
                    }

                } else {
                    var temp = '<div class="invitado_modal">' +
                        '<i class="material-icons" onclick="deleteNewAcompanante(this);">cancel</i>' +
                        '<i class="' + generoImage($('#edad_acompanante').val(), $('#sexo_acompanante').val()) + '"></i>' +
                        $('#nombre_acompanante').val() +
                        '<div style="display:none">' +
                        '<input type="hidden" name="nombre_acompanante[]" value="' + $('#nombre_acompanante').val() + '" />' +
                        '<input type="hidden" name="apellido_acompanante[]" value="' + $('#apellido_acompanante').val() + '" />' +
                        '<input type="hidden" name="grupo_acompanante[]" value="' + $('#grupo_acompanante').val() + '" />' +
                        '<input type="hidden" name="menu_acompanante[]" value="' + $('#menu_acompanante').val() + '" />' +
                        '<input type="hidden" name="sexo_acompanante[]" value="' + $('#sexo_acompanante').val() + '" />' +
                        '<input type="hidden" name="edad_acompanante[]" value="' + $('#edad_acompanante').val() + '" />' +
                        '</div>' +
                        '</div>';
                    $('#acompanante_new').append(temp);
                    $('#contador').text(parseInt($('#contador').text()) + 1);
                    $('#contador').show();
                    $('#nombre_acompanante').val('');
                    $('#apellido_acompanante').val('');
                }

            });

            $('#input-buscar').on('keyup', function () {
                var valor = ($('#input-buscar').val()).toLowerCase();
                var list = $('.collection.grupo').find('a');
                var temp = 0;
                for (var i = 1; i < list.length; i++) {
                    temp = ($(list[i]).text()).toLowerCase();
                    if (temp.indexOf(valor) != -1) {
                        $(list[i]).show();
                    }
                    else {
                        $(list[i]).hide();
                    }
                }
            });

            $('#anadir_invitado').on('click', function () {
                if ($('#id_acompanante').val() != null) {
                    var id_acompanante = $('#id_acompanante').val();
                    var sexo = $($('#id_acompanante').find(":selected")[0]).data('sexo');
                    var edad = $($('#id_acompanante').find(":selected")[0]).data('edad');

                    var temp = '<div class="invitado_modal">' +
                        '<i class="material-icons" onclick="deleteNewAcompanante(this);">cancel</i>' +
                        '<i class="' + generoImage(edad, sexo) + '" ></i>' +
                        $('#id_acompanante option[value="' + $('#id_acompanante').val() + '"]').text() +
                        '<div style="display:none">' +
                        '<input type="hidden" name="id_acompanante[]" value="' + id_acompanante + '"/>' +
                        '</div>' +
                        '</div>';
                    $('#acompanante_new').append(temp);
                    $('#contador').text(parseInt($('#contador').text()) + 1);
                    $('#contador').show();
                    $('#id_acompanante option[value="' + id_acompanante + '"]').attr('disabled', '');
                    $('#id_acompanante').material_select();
                }
            });

            $('[name="sexo"]').on('change', function () {
                $('#icon_persona_modal').attr('class', 'img-responsive ' + generoImage($('[name="edad"]').val(), $('[name="sexo"]').val()));
            });

            $('[name="edad"]').on('change', function () {
                $('#icon_persona_modal').attr('class', 'img-responsive ' + generoImage($('[name="edad"]').val(), $('[name="sexo"]').val()));
            });

            <?php if (isset($invitados) && count($invitados) > 0) { ?>
            updateInterfacePrincipal('<?php echo $invitados[0]->id_invitado; ?>');
            <?php } ?>
        }

        function deleteInvitado(nombre, id) {
            var r = confirm("¿Esta seguro que desea eliminarlo a  '" + nombre + "' ?.");
            if (r == true) {
                location.href = "<?= base_url(); ?>novios/invitados/Home/delete_invitado/" + id;
            }
        }

        function updateInterfaceInvitado(id) {
            $.ajax({
                url: '<?php echo base_url() ?>novios/invitados/Home/datos_invitado/' + id,
                method: "GET",
                timeout: 4000,
            }).done(function (datos) {
                $('#addInvitado1').click();
                $('#acompanante_new').text('');
                $('#contador').hide();
                $('#contador').text(0);
                $('#id_acompanante option').removeAttr('disabled');
                rellenarInfo(datos);
                $('#id_acompanante').material_select();
            });

            function rellenarInfo(json) {
                $('#icon_persona_modal').attr('class', 'img-responsive ' + generoImage(json.edad, json.sexo));
                $('[name="nombre"]').val(json.nombre);
                $('[name="apellido"]').val(json.apellido);
                $('[name="id_invitado"]').val(json.id_invitado);
                $('[name="grupo"]').val(json.id_grupo);
                $('[name="grupo"]').material_select();
                $('[name="menu"]').val(json.id_menu);
                $('[name="menu"]').material_select();
                $('[name="sexo"]').val(json.sexo);
                $('[name="sexo"]').material_select();
                $('[name="edad"]').val(json.edad);
                $('[name="edad"]').material_select();
                var grupo = $('[name="grupo"] option[value="' + json.id_grupo + '"]').text();
                if (grupo == 'Novios') {
                    $('[name="grupo"]').attr('disabled', '');
                    $('[name="grupo"]').material_select();
                } else {
                    $('[name="grupo"]').removeAttr('disabled', '');
                    $('[name="grupo"]').material_select();
                }


                $('[name="correo"]').val(json.correo);
                $('[name="telefono"]').val(json.telefono);
                $('[name="celular"]').val(json.celular);
                $('[name="direccion"]').val(json.direccion);
                $('[name="cp"]').val(json.codigo_postal);
                if (json.pais != null) {
                    function pais() {
                        $('[name="country"]').val(json.pais);
                        var countryId = $("option:selected", '[name="country"]').data('countryid');

                        function estado() {
                            if (json.estado != null) {
                                $('[name="state"]').val(json.estado);
                                var stateId = $("option:selected", '[name="state"]').data('stateid');

                                function poblado() {
                                    if (json.poblado != null) {
                                        $('[name="city"]').val(json.poblado);
                                    }
                                }

                                _LOCATION.getCities(stateId, poblado);
                            }
                        }

                        _LOCATION.getStates(countryId, estado);
                    }

                    var _LOCATION = new locationInfo();
                    _LOCATION.getCountries(pais);
                }


                if (json.invitado_por != null || grupo == 'Novios') {
                    $('#d_a').hide();
                } else {
                    $('#d_a').show();
                }

                $('#id_acompanante option[value="' + json.id_invitado + '"]').attr('disabled', '');
                if (json.acompanante != null && (json.acompanante).length > 0) {
                    $.each(json.acompanante, function (i, obj) {
                        $('#id_acompanante option[value="' + obj.id_invitado + '"]').attr('disabled', '');
                        var temp = '<div class="invitado_modal">' +
                            '<i class="material-icons" onclick="deleteAcompanante(this,&#39;' + obj.id_invitado + '&#39;);">cancel</i>' +
                            '<i class="' + generoImage(obj.edad, obj.sexo) + '"></i>' +
                            obj.nombre + ' ' + obj.apellido +
                            '</div>';

                        $('#acompanante_new').append(temp);
                    });
                    $('#contador').text((json.acompanante).length);
                    $('#contador').show();
                }
            }
        }

        function deleteNewAcompanante(origen) {
            origen.parentNode.remove();
            $('#contador').text(parseInt($('#contador').text()) - 1);
            if ($('#contador').text() == '0') {
                $('#contador').hide();
            }
        }

        function deleteAcompanante(origen, id) {
            var temp = "<input type='hidden' name='acompanante_remove[]' value='" + id + "' />";
            $('#acompanante_new').append(temp);
            origen.parentNode.remove();
            $('#contador').text(parseInt($('#contador').text()) - 1);
            if ($('#contador').text() == '0') {
                $('#contador').hide();
            }
        }

        function updateInvitado(id_invitado, campo, selected) {
            var origen = $(selected).val();
            switch (campo) {
                case 'confirmacion':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            confirmado: origen,
                            tipo: 'confirmacion',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'grupo':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_grupo: origen,
                            tipo: 'grupo',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'email_enviado':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            email_enviado: origen,
                            tipo: 'email_enviado',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'mesa':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_mesa: origen,
                            tipo: 'mesa',
                        },
                    }).done(function () {
                        $('.mesas-select option[value="' + $(selected).data('mesa') + '"]').each(function (i, obj) {
                            var ocupadas = $(obj).data('ocupadas');
                            ocupadas = (ocupadas == 0) ? 0 : ocupadas - 1;
                            $(obj).data('ocupadas', ocupadas);

                            if (parseInt($(obj).data('sillas')) - ocupadas > 0) {
                                $(obj).removeAttr('disabled');
                            } else {
                                $(obj).attr('disabled', 'true');
                            }
                        });
                        $(selected).data('mesa', origen);

                        $('.mesas-select option[value="' + origen + '"]').each(function (i, obj) {
                            var ocupadas = $(obj).data('ocupadas');
                            ocupadas = ocupadas + 1;
                            $(obj).data('ocupadas', ocupadas);

                            if (parseInt($(obj).data('sillas')) - ocupadas > 0) {
                                $(obj).removeAttr('disabled');
                            } else {
                                $(obj).attr('disabled', 'true');
                            }
                        });
                        $('select').material_select();
                    }).fail(function () {
                        console.log('error');
                    });
                    break;
                case 'menu':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_menu: origen,
                            tipo: 'menu',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
            }
        }

        function isEmpty(element) {
            if (element == '' || element == null || element.length == 0 || element == 0) {
                return true;
            }
            return false;
        }

        function deleteAcompananteInvitado(id_invitado, id_acompanante, nodo) {
            $.ajax({
                url: '<?php echo base_url() ?>novios/invitados/Home/delete_acompanante',
                method: "POST",
                data: {
                    id_invitado: id_invitado,
                    id_acompanante: id_acompanante,
                },
                timeout: 4000,
            }).done(function () {
                nodo.parentNode.remove();
            });
        }

        function updateInterfacePrincipal(id) {
            $.ajax({
                url: '<?php echo base_url() ?>novios/invitados/Home/datos_invitado/' + id,
                method: "GET",
                timeout: 4000,
            }).done(function (datos) {
                $('#formulario_acompanante').text('');
                rellenarInfo(datos);
                $('#formulario_id_grupo').material_select();
                $('#formulario_id_menu').material_select();
                $('#formulario_id_mesa').material_select();
                $('#formulario_id_email').material_select();
                $('#formulario_id_confirmacion').material_select();
                $('.modal-trigger').modal();
            });

            function rellenarInfo(json) {
                $('#icono_person').attr('class', 'circle ' + generoImage(json.edad, json.sexo));
                $('.title.invitado').text(json.nombre + ' ' + json.apellido);
                $('#formulario_update').attr('onclick', 'updateInterfaceInvitado("' + json.id_invitado + '")');
                $('#formulario_delete').attr('onclick', 'deleteInvitado("' + json.nombre + '","' + json.id_invitado + '")');

                $('#formulario_id_grupo').attr('onchange', "updateInvitado('" + json.id_invitado + "','grupo',this)");
                $('#formulario_id_grupo').val(json.id_grupo);

                $('#formulario_id_menu').attr('onchange', "updateInvitado('" + json.id_invitado + "','menu',this);");
                $('#formulario_id_menu').val(json.id_menu);

                $('#formulario_id_mesa').attr('onchange', "updateInvitado('" + json.id_invitado + "','mesa',this);");
                $('#formulario_id_mesa').val(json.id_mesa);

                $('#formulario_id_email').attr('onchange', "updateInvitado('" + json.id_invitado + "','email_enviado',this);");
                $('#formulario_id_email').val(json.email_enviado);

                $('#formulario_id_confirmacion').attr('onchange', "updateInvitado('" + json.id_invitado + "','confirmacion',this);");
                $('#formulario_id_confirmacion').val(json.confirmado);


                var grupo = $('#formulario_id_grupo option[value="' + json.id_grupo + '"]').text();
                if (grupo == 'Novios') {
                    $('#formulario_delete').attr('disabled', '');
                    $('#formulario_id_grupo').attr('disabled', '');
                } else {
                    $('#formulario_delete').removeAttr('disabled', '');
                    $('#formulario_id_grupo').removeAttr('disabled', '');
                }


                $('#formulario_email').html(isEmpty(json.correo) ? '<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)" >A&ntilde;adir correo</a>' : '' + json.correo + '');
                $('#formulario_telefono').html(isEmpty(json.telefono) ? '<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)">A&ntilde;adir telefono</a>' : '' + json.telefono + '');
                $('#formulario_celular').html(isEmpty(json.celular) ? '<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)">A&ntilde;adir celular</a>' : '' + json.celular + '');
                $('#formulario_direccion').html(isEmpty(json.direccion) ? '<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)">A&ntilde;adir direcci&oacute;n</a>' : '' + json.direccion + '');
                $('#formulario_cp').html(isEmpty(json.codigo_postal) ? '<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)">A&ntilde;adir C&oacute;digo postal</a>' : '' + json.codigo_postal + '');
                $('#formulario_pais').html(isEmpty(json.pais) ? '<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)">A&ntilde;adir pais</a>' : '' + json.pais + '');
                $('#formulario_estado').html(isEmpty(json.estado) ? '<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)">A&ntilde;adir estado</a>' : '' + json.estado + '');
                $('#formulario_poblado').html(isEmpty(json.poblado) ? '<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)">A&ntilde;adir poblado</a>' : '' + json.poblado + '');

                if (json.invitado_por != null || grupo == 'Novios') {
                    $('#seccion_acompanante').hide();
                } else {
                    $('#seccion_acompanante').show();
                }

                if (json.acompanante != null && (json.acompanante).length > 0) {
                    $.each(json.acompanante, function (i, obj) {
                        var temp = '<div class="invitado_modal">' +
                            '<i class="material-icons" onclick="deleteAcompananteInvitado(&#39;' + obj.invitado_por + '&#39;,&#39;' + obj.id_invitado + '&#39;,this);">cancel</i>' +
                            '<i class="' + generoImage(obj.edad, obj.sexo) + '" ></i>' +
                            obj.nombre + ' ' + obj.apellido +
                            '</div>';

                        $('#formulario_acompanante').append(temp);
                    });
                } else if (isEmpty(json.invitado_por)) {
                    $('#formulario_acompanante').html('<a class="modal-trigger dorado-2-text" href="#update_invitado" onclick="updateInterfaceInvitado(&#39;' + json.id_invitado + '&#39;)"><small>A&ntilde;adir acompa&ntilde;antes</small></a>');
                }
            }
        }

    </script>
<?php $this->view("japy/prueba/footer") ?>