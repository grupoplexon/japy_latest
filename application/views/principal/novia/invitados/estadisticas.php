<?php $this->view("principal/header") ?>
<?php $this->view("principal/novia/menu") ?>
    <div class=" body-container">
        <?php $this->view("principal/novia/invitados/menu_principal") ?>
        <div class="row" style="border-top: 1px solid #eee; margin-top: -21px; padding: 10 0 10 0;">
            <div class="col m6 s12">
                <div class="card">
                    <div class="row grey lighten-4" style="margin-bottom: 0">
                        <div class="col s6 offset-s3 m5 " style="padding: 20px">
                            <div class="circulo">
                                <div id="grafica_barra" class="grafica"></div>
                                <span style="display: none"><?php
                                    $total_invitados = $sexo['hombre'] + $sexo['mujer'] + $sexo['nino'] + $sexo['bebe'];
                                    echo $total_invitados;
                                    ?></span>

                            </div>
                        </div>
                        <div class="col m7 s12">
                            <div class="white" style="padding: 20px;">
                                <h6><b>INVITADOS</b></h6>
                                <div class="row">
                                    <div class="col s6">
                                        <div class="estadistica-item">
                                            <i class="material-icons grey-text darken-3">stop</i>
                                            <small>Hombre</small>
                                            <span class="grey-text darken-3"><?php echo $sexo['hombre'] ?></span>
                                        </div>
                                        <div class="estadistica-item">
                                            <i class="material-icons dorado-2-text">stop</i>
                                            <small>Mujeres</small>
                                            <span class="dorado-2-text"><?php echo $sexo['mujer'] ?></span>
                                        </div>
                                    </div>
                                    <div class="col s6">
                                        <div class="estadistica-item">
                                            <i class="material-icons green-text darken-3">stop</i>
                                            <small>Ni&ntilde;os</small>
                                            <span class="green-text darken-3"><?php echo $sexo['nino'] ?></span>
                                        </div>
                                        <div class="estadistica-item">
                                            <i class="material-icons cyan-text darken-3">stop</i>
                                            <small>Beb&eacute;s</small>
                                            <span class="cyan-text darken-3"><?php echo $sexo['bebe'] ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-action center">
                        <a class="dorado-2-text" style="font-size: 12px;"
                           href="<?php echo base_url() ?>novios/invitados/Home">Gestionar Invitados</a>
                    </div>
                </div>

                <div class="card">
                    <div class="row grey lighten-4"  style="margin-bottom: 0">
                        <div class="col m5 s6 offset-s3 " style="padding: 20;">
                            <div class="circulo">
                                <div class="grafica" id="grafica_grupo"></div>
                            </div>
                        </div>
                        <div class="col m7 s12 white stat-size">
                            <h6><b>GRUPOS</b></h6>
                            <?php $color = [
                                    'grey-text darken-3',
                                    'brown-text darken-3',
                                    'lime-text darken-2',
                                    'red-text darken-2',
                                    'pink-text darken-2',
                                    'deep-purple-text darken-1',
                                    'light-blue-text darken-2',
                                    'teal-text darken-2',
                                    'dorado-2-text',
                                    'cyan-text darken-3',
                                    'green-text darken-3',
                                    'orange-text darken-2',
                            ]; ?>
                            <?php $i = 0; ?>
                            <?php foreach ($grupos as $key => $value) { ?>
                                <div class="estadistica-item">
                                    <i class="material-icons <?php echo $color[$i] ?>">stop</i>
                                    <small><?php echo $value->grupo ?></small>
                                    <span class="<?php echo $color[$i] ?>"><?php echo $value->total ?></span>
                                </div>
                                <?php $i = ($i == 11) ? 0 : ($i + 1); ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="card-action center">
                        <a class="dorado-2-text" style="font-size: 12px;"
                           href="<?php echo base_url() ?>novios/invitados/Home/grupos">Gestionar Mis
                            Grupos</a>
                    </div>
                </div>

                <div class="card">
                    <div class="row grey lighten-4"  style="margin-bottom: 0">
                        <div class="col m5 s6 offset-s3  " style="padding: 20px;">
                            <div class="circulo">
                                <div class="grafica" id="grafica_asistencia"></div>
                            </div>
                        </div>
                        <div class="col white m7 s12 stat-size" style="padding: 20px;">
                            <h6><b>ASISTENCIA</b></h6>
                            <div class="estadistica-item">
                                <small>Confirmados</small>
                                <span class="green-text darken-3"><b><?php echo $asistencia['confirmados'] ?></b>&nbsp;&nbsp;<i
                                            class="material-icons">check_circle</i></span>
                            </div>
                            <div class="estadistica-item">
                                <small>Pendientes</small>
                                <span class="orange-text darken-3"><b><?php echo $asistencia['pendientes'] ?></b>&nbsp;&nbsp;<i
                                            class="material-icons">help</i></span>
                            </div>
                            <div class="estadistica-item">
                                <small>Cancelados</small>
                                <span class="grey-text darken-3"><b><?php echo $asistencia['cancelados'] ?></b>&nbsp;&nbsp;<i
                                            class="material-icons">cancel</i></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-action center">
                        <a href="<?php echo site_url('novios/invitados/Home/invitaciones') ?>">
                            <small class="dorado-2-text">Solicitar confirmaci&oacute;n de asistencia</small>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col m6 s12">

                <div class="card stat-card">
                    <div class="row grey lighten-4">
                        <div class="col m5 s6 offset-s3  " style="padding: 20;">
                            <div class="circulo">
                                <img class="icono"
                                     src="<?php echo base_url() ?>dist/img/invitado-estadistica/email.png"/>
                            </div>
                        </div>
                        <div class="col white m7 s12 stat-size" style="padding: 20;">
                            <h6><b>INVITACIONES</b></h6>
                            <div class="estadistica-item">
                                <small>Enviadas</small>
                                <span class="green-text darken-3"><b><?php echo $enviado['si'] ?></b></span>
                            </div>
                            <div class="estadistica-item">
                                <small>Pendientes de enviar</small>
                                <span class="orange-text darken-3"><b><?php echo $enviado['no'] ?></b></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="row grey lighten-4" style="margin-bottom: 0">
                        <div class="col m5 s6 offset-s3  " style="padding: 20;">
                            <div class="circulo">
                                <img class="icono"
                                     src="<?php echo base_url() ?>dist/img/invitado-estadistica/table.png"/>
                            </div>
                        </div>
                        <div class="col white m7 s12 stat-size" style="padding: 20;">
                            <h6><b>MI SAL&Oacute;N DE BODA</b></h6>
                            <div class="estadistica-item">
                                <small>Mesas creadas</small>
                                <span class="dorado-2-text"><b>3</b></span>
                            </div>
                            <div class="estadistica-item">
                                <small>Invitados sentados</small>
                                <span class="dorado-2-text"><b>12</b></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-action center">
                        <a href="<?php echo site_url('novios/mesa'); ?>">
                            <small class="dorado-2-text">Ir al Organizador de Mesas</small>
                        </a>
                    </div>
                </div>

                <div class="card">
                    <div class="row grey lighten-4">
                        <div class="col m5 s6 offset-s3" style="padding: 20px;">
                            <div class="circulo">
                                <img class="icono"
                                     src="<?php echo base_url() ?>dist/img/invitado-estadistica/mesero.png"/>
                            </div>
                        </div>
                        <div class="col m7 s12 white stat-size" style="padding: 20;">
                            <h6><b>MIS MEN&Uacute;S</b></h6>
                            <div class="row">
                                <div class="col s5">
                                    <?php $i = 0; ?>
                                    <?php foreach ($menus as $key => $value) { ?>
                                        <div class="estadistica-item">
                                            <i class="material-icons <?php echo $color[$i] ?>">stop</i>
                                            <small><?php echo $value->nombre; ?></small>
                                            <span class="<?php echo $color[$i] ?>"><b><?php echo $value->total; ?></b></span>
                                        </div>
                                        <?php $i = ($i == 11) ? 0 : ($i + 1); ?>
                                    <?php } ?>
                                </div>
                                <div class="col s7">
                                    <div id="grafica_menu" style="height:140px;"></div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="card-action center">
                        <a href="<?php echo base_url(); ?>novios/invitados/Home/menus">
                            <small class="dorado-2-text">Gestionar mis men&uacute;s</small>
                        </a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <script>
        function onReady() {
            grafica_barra();
            grafica_menu();
            grafica_grupos();
            grafica_asistencia();
        }

        function grafica_barra() {
            Highcharts.setOptions({
                colors: ['#24229E', '#C76BA9', '#4CAF50', '#00bcd4']
            });
            //grafica_barra
            $('#grafica_barra').highcharts({
                credits: {
                    enabled: false,
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false,
                    backgroundColor: 'rgba(255,250,250,0.3)',
                },
                title: {
                    text: '<small style="font-size:16px; font-weight:bold; color:#c79838;"><?php echo $total_invitados ?></small><br><small style="font-size:16px; color:#9e9e9e;">Invitados</small>',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 0,
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    visible: false,
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        startAngle: 0,
                        endAngle: 360,
                        center: ['50%', '50%']
                    }
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:0.0f}%</b><br>'
                },
                series: [{
                    type: 'pie',
                    innerSize: '80%',
                    data: [{
                        name: '',
                        y: <?php echo (int)(($sexo['hombre'] * 100) / $total_invitados) ?>,
                    }, {
                        name: '',
                        y: <?php echo (int)(($sexo['mujer'] * 100) / $total_invitados) ?>,
                    }, {
                        name: '',
                        y: <?php echo (int)(($sexo['nino'] * 100) / $total_invitados) ?>,
                    }, {
                        name: '',
                        y: <?php echo (int)(($sexo['bebe'] * 100) / $total_invitados) ?>,
                    },]
                }]

            });
        }


        function grafica_menu() {
            Highcharts.setOptions({
                colors: ['#424242', '#4e342e', '#afb42b', '#d32f2f', '#c2185b', '#5e35b1', '#0288d1', '#00796b', '#c79838', '#00838f', '#2e7d32', '#f57c00']
            });

            $('#grafica_menu').highcharts({
                credits: {
                    enabled: false,
                },
                chart: {
                    type: 'bar',
                    backgroundColor: '#f5f5f5',
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    visible: false,
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:0.0f}%'
                        }
                    }
                },
                tooltip: {
                    pointFormat: '<b>{point.y:0.0f}%</b><br/>'
                },
                series: [{
                    name: '',
                    colorByPoint: true,
                    data: [<?php foreach ($menus as $key => $value) { ?>{
                        name: '<?php for ($x = 0; $x < $key; $x++) {
                            echo ' ';
                        }; ?>', y: <?php echo (int)(($value->total * 100) / $total_invitados) ?>},<?php } ?>]
                }],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'right',
                    format: '{point.y:0.0f}%',
                    y: 10,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            });
        }


        function grafica_grupos() {
            Highcharts.setOptions({
                colors: ['#424242', '#4e342e', '#afb42b', '#d32f2f', '#c2185b', '#5e35b1', '#0288d1', '#00796b', '#c79838', '#00838f', '#2e7d32', '#f57c00']
            });

            $('#grafica_grupo').highcharts({
                credits: {
                    enabled: false,
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false,
                    backgroundColor: 'rgba(255,250,250,0.3)',
                },
                title: {
                    text: '<small style="font-size:16px; font-weight:bold; color:#c79838;"><?php echo (int)count($grupos) ?></small><br><small style="font-size:16px; color:#9e9e9e;">Grupos</small>',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 0,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:0.0f}%</b><br>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        startAngle: 0,
                        endAngle: 360,
                        center: ['50%', '50%']
                    }
                },
                series: [{
                    type: 'pie',
                    name: ' ',
                    innerSize: '80%',
                    data: [
                        <?php foreach ($grupos as $key => $value) { ?>
                        [' ', <?php echo (int)$value->total ?>],
                        <?php } ?>
                    ]
                }]
            });
        }


        function grafica_asistencia() {
            Highcharts.setOptions({
                colors: ['#4CAF50', '#ff9800', '#9e9e9e']
            });

            $('#grafica_asistencia').highcharts({
                credits: {
                    enabled: false,
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false,
                    backgroundColor: 'rgba(255,250,250,0.3)',
                },
                title: {
                    text: '<small style="font-size:16px; font-weight:bold; color:#c79838;"><?php echo (int)(($asistencia['confirmados'] * 100) / $total_invitados); ?>%</small><br><small style="font-size:14px; color:#9e9e9e;">Confirmados</small>',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 0,
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:0.0f}%</b><br>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -20,
                            style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black'
                            }
                        },
                        startAngle: 0,
                        endAngle: 360,
                        center: ['50%', '50%']
                    }
                },
                series: [{
                    type: 'pie',
                    name: ' ',
                    innerSize: '80%',
                    data: [
                        [' ', <?php echo $asistencia['confirmados'] ?>],
                        [' ', <?php echo $asistencia['pendientes'] ?>],
                        [' ', <?php echo $asistencia['cancelados'] ?>],
                    ]
                }]
            });
        }
    </script>
<?php $this->view("japy/prueba/footer") ?>