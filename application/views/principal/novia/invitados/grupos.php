<?php $this->view("principal/header") ?>
<?php $this->view("principal/novia/menu") ?>
    <div class=" body-container">
        <?php $this->view("principal/novia/invitados/menu_principal") ?>
        <div class="row" style="border-top: 1px solid #eee; margin-top: -21px; padding: 10px 0 10px 0;">
            <div class="col s12 l3">
                <div class="collection grupo">
                    <a class="collection-item grey lighten-3">
                        <b class="dorado-2-text">GRUPOS</b>
                        <span class="modal-trigger badge dorado-2 white-text no-zindex" href="#agregar_grupo" ><i
                                    class="material-icons">add</i></span>
                    </a>
                    <?php foreach ($grupos as $key => $value) { ?>
                        <a class="collection-item">
                            <?php echo ($value->grupo == 'Novios') ? $value->grupo : str_replace($this->session->userdata('genero'),
                                    $this->session->userdata('nombre'),
                                    $value->grupo); ?><!--  style="'. (($value->total<= 0)?'background-color:white!important;':'') .'"  -->
                            <?php echo '<span class="badge redondo dorado-2 white-text" id="collec-'.$value->id_grupo.'" >'.$value->total.'</span>' ?>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="col s12 l9">
                <div class="border listainvitados">
                    <?php $this->view("proveedor/mensajes") ?>
                    <?php foreach ($grupos as $key => $val) { ?>
                        <h5>
                            <b><i class="material-icons">wc</i>&nbsp;&nbsp;<?php echo ($val->grupo == 'Novios') ? $val->grupo : str_replace($this->session->userdata('genero'),
                                        $this->session->userdata('nombre'), $val->grupo); ?></b>
                            <?php if (!in_array($val->grupo, ['Novios', 'Familia', 'Amigos'])) { ?>
                                <a class="waves-effect waves-light btn-flat right"
                                   onclick="deleteGrupo('<?php echo $val->id_grupo ?>', '<?php echo $val->grupo ?>');">
                                    <i class="material-icons">delete_forever</i>
                                </a>
                                <a class="modal-trigger waves-effect waves-light btn-flat right" href="#modificar_grupo"
                                   onclick="updateInterface('<?php echo $val->id_grupo ?>', '<?php echo $val->grupo ?>');">
                                    <i class="material-icons">mode_edit</i>
                                </a>
                            <?php } ?>
                        </h5>
                        <div class="divider"></div>
                        <ul class="collection migrupo" id="<?php echo $val->id_grupo; ?>">
                            <?php
                            foreach ($invitados as $i => $value) {
                                if ($value->id_grupo == $val->id_grupo) {
                                    ?>
                                    <li class="collection-item avatar margin">
                                        <i class="circle <?php echo generoImage($value->edad, $value->sexo); ?>"></i>
                                        <span class="title"><?php echo $value->nombre.' '.$value->apellido; ?></span>
                                        <div class="row">
                                            <div class="col s12 l3 avatar-item" style="border-right:1px solid black;">
                                                <select class="listagrupo"
                                                        onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'confirmacion', this);">
                                                    <option value='' disabled selected>Asistencia</option>
                                                    <option value='1'
                                                            class="orange-text darken-3" <?php echo ($value->confirmado == 1) ? 'selected' : '' ?>>Pendiente
                                                    </option>
                                                    <option value='2'
                                                            class="green-text darken-4" <?php echo ($value->confirmado == 2) ? 'selected' : '' ?>>Confirmado
                                                    </option>
                                                    <option value='3'
                                                            class="deep-orange-text darken-4" <?php echo ($value->confirmado == 3) ? 'selected' : '' ?>>Cancelado
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col s12 l3 avatar-item" style="border-right:1px solid black;">
                                                <select class="listagrupo"
                                                        onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'grupo', this);" <?php echo ($value->grupo == 'Novios') ? 'disabled' : ''; ?> >
                                                    <option value='' disabled selected>Grupo</option>
                                                    <?php foreach ($grupos as $k => $gp) { ?>
                                                        <option value="<?php echo $gp->id_grupo ?>" <?php echo ($gp->id_grupo == $value->id_grupo) ? 'selected' : (($value->grupo == 'Novios') ? 'disabled' : (($gp->grupo == 'Novios') ? 'disabled' : '')) ?>><?php echo $gp->grupo ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col s12 l3 avatar-item" style="border-right:1px solid black;">
                                                <select class="listagrupo mesas-select"
                                                        onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'mesa', this);"
                                                        data-mesa="<?php echo $value->id_mesa ?>">
                                                    <option value='' disabled selected>Mesa</option>
                                                    <?php foreach ($mesas as $y => $ms) { ?>
                                                        <option value="<?php echo $ms->id_mesa ?>" <?php echo (($ms->sillas - $ms->ocupado) <= 0) ? ' disabled ' : '' ?>
                                                                data-sillas="<?php echo $ms->sillas ?>"
                                                                data-ocupadas="<?php echo $ms->ocupado ?>"
                                                                data-nombre="<?php echo $ms->nombre ?>"
                                                                <?php echo ($ms->id_mesa == $value->id_mesa) ? 'selected' : '' ?>><?php echo $ms->nombre ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col s12 l3 avatar-item">
                                                <select class="listagrupo"
                                                        onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'menu', this);">
                                                    <option value='' disabled selected>Men&uacute;</option>
                                                    <?php foreach ($menus as $x => $mn) { ?>
                                                        <option value="<?php echo $mn->id_menu ?>" <?php echo ($mn->id_menu == $value->id_menu) ? 'selected' : '' ?>><?php echo $mn->nombre ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    <?php } ?>

                    <h5>
                        <b><i class="material-icons">wc</i>&nbsp;&nbsp;Sin asignar</b>
                    </h5>
                    <div class="divider"></div>
                    <ul class="collection migrupo" id="sin">
                        <?php
                        foreach ($invitados as $i => $value) {
                            if (empty($value->id_grupo)) {
                                ?>
                                <li class="collection-item avatar margin">
                                    <i class="circle <?php echo generoImage($value->edad, $value->sexo); ?>"></i>
                                    <span class="title"><?php echo $value->nombre.' '.$value->apellido; ?></span>
                                    <div class="row">
                                        <div class="col s12 l3" style="border-right:1px solid black;">
                                            <select class="listagrupo"
                                                    onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'confirmacion', this);">
                                                <option value='' disabled selected>Asistencia</option>
                                                <option value='1'
                                                        class="orange-text darken-3" <?php echo ($value->confirmado == 1) ? 'selected' : '' ?>>
                                                    &#xf017;&nbsp;&nbsp;&nbsp;&nbsp;Pendiente
                                                </option>
                                                <option value='2'
                                                        class="green-text darken-4" <?php echo ($value->confirmado == 2) ? 'selected' : '' ?>>
                                                    &#xf05d;&nbsp;&nbsp;&nbsp;&nbsp;Confirmado
                                                </option>
                                                <option value='3'
                                                        class="deep-orange-text darken-4" <?php echo ($value->confirmado == 3) ? 'selected' : '' ?>>
                                                    &#xf057;&nbsp;&nbsp;&nbsp;&nbsp;Cancelado
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col s12 l3" style="border-right:1px solid black;">
                                            <select class="listagrupo"
                                                    onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'grupo', this);" <?php echo ($value->grupo == 'Novios') ? 'disabled' : ''; ?> >
                                                <option value='' disabled selected>Grupo</option>
                                                <?php foreach ($grupos as $k => $gp) { ?>
                                                    <option value="<?php echo $gp->id_grupo ?>" <?php echo ($gp->id_grupo == $value->id_grupo) ? 'selected' : (($value->grupo == 'Novios') ? 'disabled' : (($gp->grupo == 'Novios') ? 'disabled' : '')) ?>><?php echo $gp->grupo ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col s12 l3" style="border-right:1px solid black;">
                                            <select class="listagrupo mesas-select"
                                                    onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'mesa', this);"
                                                    data-mesa="<?php echo $value->id_mesa ?>">
                                                <option value='' disabled selected>Mesa</option>
                                                <?php foreach ($mesas as $y => $ms) { ?>
                                                    <option value="<?php echo $ms->id_mesa ?>" <?php echo (($ms->sillas - $ms->ocupado) <= 0) ? ' disabled ' : '' ?>
                                                            data-sillas="<?php echo $ms->sillas ?>"
                                                            data-ocupadas="<?php echo $ms->ocupado ?>"
                                                            data-nombre="<?php echo $ms->nombre ?>"
                                                            <?php echo ($ms->id_mesa == $value->id_mesa) ? 'selected' : '' ?>><?php echo $ms->nombre ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col s12 l3">
                                            <select class="listagrupo"
                                                    onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'menu', this);">
                                                <option value='' disabled selected>Men&uacute;</option>
                                                <?php foreach ($menus as $x => $mn) { ?>
                                                    <option value="<?php echo $mn->id_menu ?>" <?php echo ($mn->id_menu == $value->id_menu) ? 'selected' : '' ?>><?php echo $mn->nombre ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>


                </div>
            </div>
        </div>
    </div>


    <div id="agregar_grupo" class="modal modal-japy" style="max-width: 500px">
        <form method="post" action="<?php echo base_url() ?>novios/invitados/Grupo/add_grupo">
            <div class="modal-content">
                <i class="material-icons white-text large" style="margin: 0 0 0 35%">group_add</i>
                <h6 class="white-text center-align">Nuevo grupo</h6>
                <div class="divider"></div>
                <br>
                <div class="row" style="margin-bottom: 0">
                    <div class="input-field col s10 offset-s1">
                        <input id="nombre_grupo" name="nombre_grupo" type="text" class="validate"  placeholder="Nombre*" required>
                    </div>
                </div>
            </div>
            <?php $this->view("principal/novia/invitados/modal-footer");?>
        </form>
    </div>


    <div id="modificar_grupo" class="modal">
        <form method="post" action="<?php echo base_url() ?>novios/invitados/Grupo/update_grupo">
            <div class="modal-content">
                <h6 class="dorado-2-text">Grupo</h6>
                <div class="divider"></div>
                <br>
                <div class="row">
                    <input id="id_grupo_update" name="id_grupo" type="hidden" required>
                    <input id="nombre_grupo_update" name="nombre_grupo_update" class="formulario col s12" type="text"
                           placeholder="Nombre*" required>
                </div>
            </div>
            <?php $this->view("principal/novia/invitados/modal-footer");?>
        </form>
    </div>
<?php $this->view("japy/prueba/footer") ?>
    <script>
        function onReady() {
            $('#agregar_grupo').modal();
            $('#modificar_grupo').modal();
            $('#update_invitado').modal();
            $('#anadir_invitado_form').on('click', function () {
                if (isEmpty($('#grupo_acompanante').val()) || isEmpty($('#nombre_acompanante').val()) || isEmpty($('#apellido_acompanante').val())) {


                    if (isEmpty($('#nombre_acompanante').val())) {
                        $('#nombre_acompanante').attr('class', $('#nombre_acompanante').attr('class') + ' invalid');
                    } else {
                        $('#nombre_acompanante').attr('class', (($('#nombre_acompanante').attr('class')).split('invalid')[0]));
                    }


                    if (isEmpty($('#apellido_acompanante').val())) {
                        $('#apellido_acompanante').attr('class', $('#apellido_acompanante').attr('class') + ' invalid');
                    } else {
                        $('#apellido_acompanante').attr('class', (($('#apellido_acompanante').attr('class')).split('invalid')[0]));
                    }

                    if (isEmpty($('#grupo_acompanante').val())) {
                        alert('Seleccione un grupo');
                    }

                } else {
                    var temp = '<div class="invitado_modal">' +
                        '<i class="material-icons" onclick="deleteNewAcompanante(this);">cancel</i>' +
                        '<i class="' + generoImage($('#edad_acompanante').val(), $('#sexo_acompanante').val()) + '"></i>' +
                        $('#nombre_acompanante').val() +
                        '<div style="display:none">' +
                        '<input type="hidden" name="nombre_acompanante[]" value="' + $('#nombre_acompanante').val() + '" />' +
                        '<input type="hidden" name="apellido_acompanante[]" value="' + $('#apellido_acompanante').val() + '" />' +
                        '<input type="hidden" name="grupo_acompanante[]" value="' + $('#grupo_acompanante').val() + '" />' +
                        '<input type="hidden" name="menu_acompanante[]" value="' + $('#menu_acompanante').val() + '" />' +
                        '<input type="hidden" name="sexo_acompanante[]" value="' + $('#sexo_acompanante').val() + '" />' +
                        '<input type="hidden" name="edad_acompanante[]" value="' + $('#edad_acompanante').val() + '" />' +
                        '</div>' +
                        '</div>';
                    $('#acompanante_new').append(temp);
                    $('#contador').text(parseInt($('#contador').text()) + 1);
                    $('#contador').show();
                    $('#nombre_acompanante').val('');
                    $('#apellido_acompanante').val('');
                }

            });

            $('#anadir_invitado').on('click', function () {
                if ($('#id_acompanante').val() != null) {
                    var id_acompanante = $('#id_acompanante').val();
                    var sexo = $($('#id_acompanante').find(":selected")[0]).data('sexo');
                    var edad = $($('#id_acompanante').find(":selected")[0]).data('edad');

                    var temp = '<div class="invitado_modal">' +
                        '<i class="material-icons" onclick="deleteNewAcompanante(this);">cancel</i>' +
                        '<i class="' + generoImage(edad, sexo) + '" ></i>' +
                        $('#id_acompanante option[value="' + $('#id_acompanante').val() + '"]').text() +
                        '<div style="display:none">' +
                        '<input type="hidden" name="id_acompanante[]" value="' + id_acompanante + '"/>' +
                        '</div>' +
                        '</div>';
                    $('#acompanante_new').append(temp);
                    $('#contador').text(parseInt($('#contador').text()) + 1);
                    $('#contador').show();
                    $('#id_acompanante option[value="' + id_acompanante + '"]').attr('disabled', '');
                    $('#id_acompanante').material_select();
                }
            });

            $('[name="sexo"]').on('change', function () {
                $('#icon_persona_modal').attr('class', 'img-responsive ' + generoImage($('[name="edad"]').val(), $('[name="sexo"]').val()));
            });

            $('[name="edad"]').on('change', function () {
                $('#icon_persona_modal').attr('class', 'img-responsive ' + generoImage($('[name="edad"]').val(), $('[name="sexo"]').val()));
            });
        }

        function deleteInvitado(nombre, id) {
            var r = confirm("¿Esta seguro que desea eliminarlo a  '" + nombre + "' ?.");
            if (r == true) {
                location.href = "<?= base_url(); ?>novios/invitados/Home/delete_invitado/" + id;
            }
        }

        function updateInterfaceInvitado(id) {
            $.ajax({
                url: '<?php echo base_url() ?>novios/invitados/Home/datos_invitado/' + id,
                method: "GET",
                timeout: 4000,
            }).done(function (datos) {
                $('#addInvitado1').click();
                $('#acompanante_new').text('');
                $('#contador').hide();
                $('#contador').text(0);
                $('#id_acompanante option').removeAttr('disabled');
                rellenarInfo(datos);
                $('#id_acompanante').material_select();
            });

            function rellenarInfo(json) {
                $('#icon_persona_modal').attr('class', 'img-responsive ' + generoImage(json.edad, json.sexo));
                $('[name="nombre"]').val(json.nombre);
                $('[name="apellido"]').val(json.apellido);
                $('[name="id_invitado"]').val(json.id_invitado);
                $('[name="grupo"]').val(json.id_grupo);
                $('[name="grupo"]').material_select();
                $('[name="menu"]').val(json.id_menu);
                $('[name="menu"]').material_select();
                $('[name="sexo"]').val(json.sexo);
                $('[name="sexo"]').material_select();
                $('[name="edad"]').val(json.edad);
                $('[name="edad"]').material_select();
                var grupo = $('[name="grupo"] option[value="' + json.id_grupo + '"]').text();
                if (grupo == 'Novios') {
                    $('[name="grupo"]').attr('disabled', '');
                    $('[name="grupo"]').material_select();
                } else {
                    $('[name="grupo"]').removeAttr('disabled', '');
                    $('[name="grupo"]').material_select();
                }


                $('[name="correo"]').val(json.correo);
                $('[name="telefono"]').val(json.telefono);
                $('[name="celular"]').val(json.celular);
                $('[name="direccion"]').val(json.direccion);
                $('[name="cp"]').val(json.codigo_postal);
                if (json.pais != null) {
                    function pais() {
                        $('[name="country"]').val(json.pais);
                        var countryId = $("option:selected", '[name="country"]').attr('countryid');

                        function estado() {
                            if (json.estado != null) {
                                $('[name="state"]').val(json.estado);
                                var stateId = $("option:selected", '[name="state"]').attr('stateid');

                                function poblado() {
                                    if (json.poblado != null) {
                                        $('[name="city"]').val(json.poblado);
                                    }
                                }

                                _LOCATION.getCities(stateId, poblado);
                            }
                        }

                        _LOCATION.getStates(countryId, estado);
                    }

                    var _LOCATION = new locationInfo();
                    _LOCATION.getCountries(pais);
                }


                if (json.invitado_por != null || grupo == 'Novios') {
                    $('#d_a').hide();
                } else {
                    $('#d_a').show();
                }

                $('#id_acompanante option[value="' + json.id_invitado + '"]').attr('disabled', '');
                if (json.acompanante != null && (json.acompanante).length > 0) {
                    $.each(json.acompanante, function (i, obj) {
                        $('#id_acompanante option[value="' + obj.id_invitado + '"]').attr('disabled', '');
                        var temp = '<div class="invitado_modal">' +
                            '<i class="material-icons" onclick="deleteAcompanante(this,&#39;' + obj.id_invitado + '&#39;);">cancel</i>' +
                            '<i class="' + generoImage(obj.edad, obj.sexo) + '"></i>' +
                            obj.nombre + ' ' + obj.apellido +
                            '</div>';

                        $('#acompanante_new').append(temp);
                    });
                    $('#contador').text((json.acompanante).length);
                    $('#contador').show();
                }
            }
        }

        function updateInterface(id, nombre) {
            $('#id_grupo_update').val(id);
            $('#nombre_grupo_update').val(nombre);
        }

        function deleteGrupo(id, nombre) {
            swal({
                title: 'Estas seguro que quieres borrar '+nombre+'?',
                icon: 'error',
                buttons: ['Cancelar', true],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    location.href = "<?= base_url(); ?>novios/invitados/Grupo/delete_grupo/" + id;
                }
            });
        }

        function deleteNewAcompanante(origen) {
            origen.parentNode.remove();
            $('#contador').text(parseInt($('#contador').text()) - 1);
            if ($('#contador').text() == '0') {
                $('#contador').hide();
            }
        }

        function deleteAcompanante(origen, id) {
            or = origen;
            var temp = "<input type='hidden' name='acompanante_remove[]' value='" + id + "' />";
            $('#acompanante_new').append(temp);
            origen.parentNode.remove();
            $('#contador').text(parseInt($('#contador').text()) - 1);
            if ($('#contador').text() == '0') {
                $('#contador').hide();
            }
        }

        function updateInvitado(id_invitado, campo, origen) {
            var nodo = origen;
            origen = $(origen).val();
            switch (campo) {
                case 'confirmacion':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            confirmado: origen,
                            tipo: 'confirmacion',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'grupo':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_grupo: origen,
                            tipo: 'grupo',
                        },
                    }).done(function (result) {
                        console.log('success');
                        var nodo_ant = (nodo.parentNode.parentNode.parentNode.parentNode.parentNode).id
                        var nodo_ant_new_value = parseInt($('#collec-' + nodo_ant).text()) - 1;
                        var nodo_nue_new_value = parseInt($('#collec-' + origen).text()) + 1;
                        $('#collec-' + nodo_ant).text(nodo_ant_new_value);
                        $('#collec-' + origen).text(nodo_nue_new_value);
                        document.getElementById(origen).appendChild(nodo.parentNode.parentNode.parentNode.parentNode);
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'mesa':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_mesa: origen,
                            tipo: 'mesa',
                        },
                    }).done(function () {
                        $('.mesas-select option[value="' + $(nodo).data('mesa') + '"]').each(function (i, obj) {
                            var ocupadas = $(obj).data('ocupadas');
                            ocupadas = (ocupadas == 0) ? 0 : ocupadas - 1;
                            $(obj).data('ocupadas', ocupadas);

                            if (parseInt($(obj).data('sillas')) - ocupadas > 0) {
                                $(obj).removeAttr('disabled');
                            } else {
                                $(obj).attr('disabled', 'true');
                            }
                        });
                        $(nodo).data('mesa', origen);

                        $('.mesas-select option[value="' + origen + '"]').each(function (i, obj) {
                            var ocupadas = $(obj).data('ocupadas');
                            ocupadas = ocupadas + 1;
                            $(obj).data('ocupadas', ocupadas);

                            if (parseInt($(obj).data('sillas')) - ocupadas > 0) {
                                $(obj).removeAttr('disabled');
                            } else {
                                $(obj).attr('disabled', 'true');
                            }
                        });
                        $('select').material_select();
                    }).fail(function () {
                        console.log('error');
                    });
                    break;
                case 'menu':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_menu: origen,
                            tipo: 'menu',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
            }
        }

        function isEmpty(element) {
            if (element == '' || element == null || element.length == 0) {
                return true;
            }
            return false;
        }

    </script>
