<?php $this->view("principal/header") ?>
<?php $this->view("principal/novia/menu") ?>
<div class=" body-container">
    <?php $this->view("principal/novia/invitados/menu_principal") ?>
    <div class="row" style="border-top: 1px solid #eee; margin-top: -21px; padding: 10 0 10 0;">
        <?php if (isset($mensaje)) { ?>
            <div class="col s12">
                <div class="<?php echo ($mensaje == 'error') ? ' red lighten-4' : 'light-green lighten-4' ?>"
                     style="padding: 10; margin: 0 10 0 10; font-size: 11;">
                    <?php echo ($mensaje == 'error') ? 'Intentelo de nuevo mas tarde.' : 'Invitados agregados exitosamente.'; ?>
                </div>
                <br/>
            </div>
        <?php } ?>
        <div class="col 12 l4 " style="position: relative;">
            <div class="circle-number floating dorado-2-text z-depth-1">1</div>
            <div class="card">
                <div class="card-content">
                    <img class="responsive-img" style="padding: 10px 40% 10px 40%"
                         src="<?php echo base_url() ?>dist/img/invitados/file-text.png" alt=""/>
                    <h6 class="align-center">Desc&aacute;rgate la plantilla de excel que hemos preparado para subir tus
                        invitados.</h6>
                </div>
                <div class="card-action">
                    <a class="align-center dorado-2-text"
                       href="<?php echo base_url() ?>index.php/novios/invitados/Home/import_excel/formato.xlsx"
                       download="formato.xlsx"><i class="material-icons left">file_download</i>Descargar plantilla</a>
                </div>
            </div>
        </div>


        <div class="col s12 l4" style="position: relative;">
            <div class="circle-number floating dorado-2-text z-depth-1">2</div>
            <div class="card">
                <div class="card-content">
                    <img class="responsive-img" style="padding: 10px 40% 10px 40%"
                         src="<?php echo base_url() ?>dist/img/invitados/pencil.png" alt=""/>
                    <h6 class="align-center">Rellena la plantilla con los datos de tus invitados. <b>Importante:</b> No
                        cambies la primera fila del excel.</h6>
                </div>
                <div class="card-action"><br></div>
            </div>
        </div>


        <div class="col s12 l4" style="position: relative;">
            <div class="circle-number floating dorado-2-text z-depth-1">3</div>
            <div class="card">
                <div class="card-content">
                    <img class="responsive-img" style="padding: 10px 40% 10px 40%"
                         src="<?php echo base_url() ?>dist/img/invitados/file-text-upload.png" alt=""/>
                    <h6 class="align-center">Devu&eacute;lvenos la plantilla rellenada. Podr&aacute;s ver c&oacute;mo
                        quedar&aacute; antes de guardar ning&uacute;n cambio.</h6>
                </div>
                <div class="card-action">
                    <input type="file" id="fileInput" accept=".xlsx" style="display: none;">
                    <a class="align-center clickable dorado-2-text" id="import-csv"><i class="material-icons left">cloud_upload</i>Importar
                        contactos</a>
                </div>
            </div>
        </div>

        <div class="col s12" id="form_excel" style="display: none;">
            <div class="card">
                <div class="card-content">
                    <div class="blue lighten-5" style="padding: 10; margin: 0 10 0 10; font-size: 11;">
                        Se van a añadir los siguientes contactos a tu lista de invitados. Antes de continuar aseg&uacute;rate
                        que todos los datos son correctos y completa los faltantes.
                    </div>

                    <form method="POST">
                        <table class="bordered responsive-table">
                            <thead>
                            <tr>
                                <td><b>Nombre*</b></td>
                                <td><b>Apellido(s)*</b></td>
                                <td><b>E-mail*</b></td>
                                <td><b>Tel&eacute;fono</b></td>
                                <td><b>Celular</b></td>
                                <td><b>Direcci&oacute;n</b></td>
                                <td><b>C&oacute;digo postal</b></td>
                                <td><b>Grupo*</b></td>
                                <td><b>Men&uacute;*</b></td>
                                <td><b>Sexo*</b></td>
                                <td><b>Edad*</b></td>
                                <td><b>Op</b></td>
                            </tr>
                            </thead>
                            <tbody id="table_new"></tbody>
                            <tfoot>
                            <td>
                                <button class="btn invitados align-left dorado-2" type="submit">A&Ntilde;ADIR</button>
                            </td>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h6 class="align-center">O si lo prefieres, puedes añadir tus invitados desde los contactos de tu
                        email.</h6>
                    <br/>
                    <div class="align-center">
                        <button class="waves-effect waves-light btn-flat dorado-2 white-text" onclick="auth()"
                                style="margin-bottom: 20px"><i class="fa fa-google left" aria-hidden="true"></i>Gmail
                        </button>
                        <button class="waves-effect waves-light btn-flat dorado-2 white-text" onclick="auth_outlook()"
                                style="margin-bottom: 20px"><i class="fa fa-envelope-o left" aria-hidden="true"></i>Hotmail/Outlook
                        </button>
                    </div>
                    <br/>
                </div>
                <div class="card-action">
                    <div class="align-left black-text"><i class="material-icons left">visibility</i>Podr&aacute;s
                        controlar en todo momento a qui&eacute;n invitas a tu boda. Tus invitados no recibir&aacute;n
                        ning&uacute;n tipo de notificaci&oacute;n sin tu permiso.
                    </div>
                    <br/><br/>
                    <div class="align-left black-text"><i class="material-icons left">lock</i>Ni te pedimos ni
                        almacenamos tus contraseñas, conectamos directamente con los servicios. Tu s&oacute;lo nos das
                        permiso para importar tus contactos.
                    </div>
                </div>
            </div>
        </div>


    </div>

</div>
<script type="text" id="template-1">
    <td><input class="formulario" style="width:100%;" type="text" name="nombre[]" value="" required /></td>
    <td><input class="formulario" style="width:100%;" type="text" name="apellidos[]" value="" required /></td>
    <td><input class="formulario" style="width:100%;" type="email" name="email[]" value="" required /></td>
    <td><input class="formulario" style="width:100%;" type="tel" name="telefono[]" value="" /></td>
    <td><input class="formulario" style="width:100%;" type="tel" name="celular[]" value="" /></td>
    <td><input class="formulario" style="width:100%;" type="text" name="direccion[]" value="" /></td>
    <td><input class="formulario" style="width:100%;" type="number" name="cp[]" value="" /></td>
    <td>
    <select class="browser-default formulario" name="grupo[]" style="width:100%;" required>
    <option value="" disabled selected>Grupo*</option>
    <?php foreach ($grupos as $key => $value) { ?>
        <option value="<?php echo $value->id_grupo ?>" <?php echo ($value->grupo == 'Novios') ? 'disabled' : ''; ?> > <?php echo str_replace($this->session->userdata('genero'),
            $this->session->userdata('nombre'), $value->grupo); ?></option>
    <?php } ?>
    </select>
    </td>
    <td>
    <select class="browser-default formulario" name="menu[]" style="width:100%;" required>
    <option value="" disabled selected>Men&uacute;*</option>
    <?php foreach ($menus as $key => $value) { ?>
        <option value="<?php echo $value->id_menu ?>"><?php echo $value->nombre ?></option>
    <?php } ?>
    </select>        
    </td>
    <td><select class="browser-default formulario" name="sexo[]" style="width:100%;" required><option value="" disabled selected>Sexo*</option><option value="1">Hombre</option><option value="2">Mujer</option></select></td>
    <td><select class="browser-default formulario" name="edad[]" style="width:100%;" required><option value="" disabled selected>Edad*</option><option value="1">Adulto</option><option value="2">Ni&ntilde;o</option><option value="3">Beb&eacute;</option></select></td>  
    <td><button class="waves-effect waves-light btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></button></td>



</script>
<div id="modal1" class="modal">
    <div class="modal-content">
        <h4>Modal Header</h4>
        <table style="font-size: 12px;">
            <thead>
            <tr>
                <td><b>Nombre</b></td>
                <td><b>E-mail</b></td>
                <td><b>Tel&eacute;fono</b></td>
                <td><b>Agregar</b></td>
            </tr>
            </thead>
            <tbody id="modal-user"></tbody>
        </table>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
        <button class="waves-effect waves-green btn-flat dorado-2 white-text" id="modal-continuar">Continuar</button>
    </div>
</div>

<?php $this->view("principal/footer") ?>
<script src="https://apis.google.com/js/client.js"></script>
<script src="//js.live.net/v5.0/wl.js"></script>
<script src="<?php echo base_url() ?>dist/sheetjs/shim.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/sheetjs/jszip.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/sheetjs/xlsx.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/sheetjs/ods.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.modal').modal();
        $("#import-csv").on("click", function () {
            $("#fileInput").trigger("click");
        });
        $('#modal-continuar').on('click', function () {
            var selected = $('[name="google[]"]:checked');
            if (selected.length > 0) {
                $("#form_excel").show();
                $("#table_new").text('');
                $('#modal1').modal("close");
                $.each(selected, function (i, obj) {
                    var data = $(obj).data('json');
                    if (data.apellido == undefined) {
                        data.nombre = (data.nombre).split(' ');
                        switch ((data.nombre).length) {
                            case 0:
                                data.apellido = '';
                                data.nombre = '';
                                break;
                            case 1:
                                data.apellido = '';
                                data.nombre = data.nombre[0];
                                break;
                            case 2:
                                data.apellido = data.nombre[1];
                                data.nombre = data.nombre[0];
                                break;
                            case 3:
                                data.apellido = data.nombre[1] + ' ' + data.nombre[2];
                                data.nombre = data.nombre[0];
                                break;
                            default:
                                data.apellido = data.nombre[2] + ' ' + data.nombre[3];
                                data.nombre = data.nombre[0] + ' ' + data.nombre[1];
                                break;
                        }
                    }

                    var tr = document.createElement("tr");
                    tr.innerHTML = $("#template-1").html();
                    $($(tr).find('[name="nombre[]"]')).val(data.nombre);
                    $($(tr).find('[name="apellidos[]"]')).val(data.apellido);
                    $($(tr).find('[name="email[]"]')).val(data.email);
                    $($(tr).find('[name="telefono[]"]')).val(data.telefono);
                    $($(tr).find('button')).on('click', function () {
                        var _super = this.parentNode.parentNode;
                        $(_super).remove();
                    });
                    $("#table_new").append(tr);
                });
                $('#modal-user').text('');
            } else {
                alert('Seleccione al menos uno para continuar....');
            }
        });
    });

    function handleFile(e) {
        var files = e.target.files;
        var i, f;
        $("#form_excel").show();
        $("#table_new").text("");
        for (i = 0, f = files[i]; i != files.length; ++i) {
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function (e) {
                var data = e.target.result;
                var workbook = XLSX.read(data, {type: "binary"});
                output = to_json(workbook);
                $.each(output.Hoja1, function (i, obj) {
                    var tr = document.createElement("tr");
                    tr.innerHTML = $("#template-1").html();
                    $($(tr).find('[name="nombre[]"]')).val(obj.NOMBRE);
                    $($(tr).find('[name="apellidos[]"]')).val(obj.APELLIDOS);
                    $($(tr).find('[name="email[]"]')).val(obj.EMAIL);
                    $($(tr).find('[name="telefono[]"]')).val(obj.TELEFONO);
                    $($(tr).find('[name="celular[]"]')).val(obj.CELULAR);
                    $($(tr).find('[name="direccion[]"]')).val(obj.DIRECCION);
                    $($(tr).find('[name="cp[]"]')).val(obj.CODIGO_POSTAL);
                    $($(tr).find('button')).on('click', function () {
                        var _super = this.parentNode.parentNode;
                        $(_super).remove();
                    });
                    $("#table_new").append(tr);
                });
            };
            reader.readAsBinaryString(f);
        }

        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
    }

    document.getElementById("fileInput").addEventListener("change", handleFile, false);

    function auth_outlook() {
        WL.init({
            client_id: '<?php echo $this->config->item('outlook_app_id'); ?>',
            redirect_uri: '<?php echo base_url() ?>index.php/novios/invitados/Home/importar',
            scope: ["wl.basic", "wl.contacts_emails"],
            response_type: "token"
        });

        WL.login({
            scope: ["wl.basic", "wl.contacts_emails"]
        }).then(function (response) {
            WL.api({
                path: "me/contacts",
                method: "GET"
            }).then(function (response) {
                $('#modal-user').text('');
                $.each(response.data, function (i, obj) {
                    var data = {
                        nombre: (obj.first_name !== null) ? obj.first_name : '',
                        email: (obj.emails.preferred !== null) ? obj.emails.preferred : '',
                        telefono: '',
                    };
                    if (obj.last_name !== null) {
                        data.apellido = obj.last_name;
                    }
                    var tr = document.createElement("tr");
                    tr.innerHTML = "<td>" + data.nombre + ' ' + ((data.apellido !== undefined) ? data.apellido : '') + "</td>" +
                        "<td>" + data.email + "</td>" +
                        "<td>" + data.telefono + "</td>" +
                        "<td><p><input name='google[]' data-json='" + JSON.stringify(data) + "' type='checkbox' id='test" + i + "' /><label for='test" + i + "'></label></p></td>";

                    $('#modal-user').append(tr);
                });
                $('#modal1').modal("open");

            }, function (responseFailed) {
                //console.log(responseFailed);
            });
        }, function (responseFailed) {
            //console.log("Error signing in: " + responseFailed.error_description);
        });
    }

    function auth() {
        var config = {
            'client_id': '<?php echo $this->config->item('google_client_oauth'); ?>',
            'scope': 'https://www.google.com/m8/feeds'
        };

        gapi.auth.authorize(config, function () {
            fetch(gapi.auth.getToken());
        });

        function fetch(token) {
            $.ajax({
                url: 'https://www.google.com/m8/feeds/contacts/default/full?alt=json',
                dataType: 'jsonp',
                data: token
            }).done(function (data) {
                $('#modal-user').text('');
                $.each(data.feed.entry, function (i, obj) {
                    var data = {
                        nombre: (obj.title != undefined) ? obj.title.$t : '',
                        email: (obj.gd$email != undefined) ? obj.gd$email[0].address : '',
                        telefono: (obj.gd$phoneNumber != undefined) ? obj.gd$phoneNumber[0].$t : '',
                    };


                    var tr = document.createElement("tr");
                    tr.innerHTML = "<td>" + data.nombre + "</td>" +
                        "<td>" + data.email + "</td>" +
                        "<td>" + data.telefono + "</td>" +
                        "<td><p><input name='google[]' data-json='" + JSON.stringify(data) + "' type='checkbox' id='test" + i + "' /><label for='test" + i + "'></label></p></td>";

                    $('#modal-user').append(tr);

                });
                $('#modal1').modal("open");
            });
        }
    }
</script>