<?php setlocale(LC_TIME, 'es_ES.UTF-8'); ?>
<?php $this->view("principal/header") ?>
<?php $this->view("principal/novia/menu") ?>
<div class="body-container">
    <?php $this->view("principal/novia/invitados/menu_principal") ?>

    <div style="border-top: 1px solid #eee; margin-top: -21px; padding: 10 0 10 0;">
        <div class="row">
            <div class="col s8 offset-s2 offset-l10 l2 center-align">
                <select class="listagrupo boton" name="invitacion" id="historial_invitacion">
                    <option value="" disabled selected>Tus invitaciones guardadas</option>
                    <?php foreach ($invitaciones as $key => $value) { ?>
                        <option value="<?php echo $value->id_invitacion ?>"><?php echo $value->titulo ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php if (isset($mensaje)) { ?>
            <div class="light-green lighten-4"
                 style="padding: 10; margin: 0 10 0 10; font-size: 11;"><?php echo $mensaje ?></div>
        <?php } ?>
        <div class="row">
            <div class="col s12 l4">
                <form method="POST" id="formulario_nuevo"
                      action="<?php echo base_url() ?>novios/invitados/Invitacion/<?php echo isset($invitacion) ? 'update_invitacion' : 'add_invitacion' ?>">
                    <ul class="collection with-header" style="overflow: visible;">
                        <li class="collection-header grey lighten-3">
                            <h6 class="dorado-2-text"><b>Nueva invitaci&oacute;n</b></h6>
                        </li>
                        <li class="collection-item" style="padding-right: 30px;">
                            <div class="row">
                                <input type="hidden" name="imagen"
                                       value="<?php echo isset($invitacion) ? "data:$invitacion->mime;base64,".base64_encode($invitacion->base) : '' ?>"/>
                                <input class="formulario-invitaciones col s12" type="text" name="novia"
                                       placeholder="Novia" value="<?php if (isset($invitacion)) {
                                    echo $invitacion->novia;
                                } elseif (isset($novios) && count($novios) > 0) {
                                    echo $novios[0]->nombre.' '.$novios[0]->apellido;
                                } ?>" required>
                                <input class="formulario-invitaciones col s12" type="text" name="novio"
                                       placeholder="Novio" value="<?php if (isset($invitacion)) {
                                    echo $invitacion->novio;
                                } elseif (isset($novios) && count($novios) > 1) {
                                    echo $novios[1]->nombre.' '.$novios[1]->apellido;
                                } ?>" required>
                                <input class="datepicker formulario-invitaciones col s12" type="date" name="fecha"
                                       placeholder="Fecha de la boda" value="<?php if (isset($invitacion)) {
                                    echo $invitacion->fecha;
                                } elseif (isset($boda)) {
                                    echo (new DateTime($boda->fecha_boda))->format('Y-m-d');
                                } ?>">
                                <input class="formulario-invitaciones col s12" type="text" name="lugar"
                                       placeholder="Lugar de la boda"
                                       value="<?php echo isset($invitacion) ? $invitacion->lugar : '' ?>" required>
                                <input class="formulario-invitaciones col s12" type="text" name="titulo"
                                       placeholder="T&iacute;tulo de la invitaci&oacute;n"
                                       value="<?php echo isset($invitacion) ? $invitacion->titulo : '' ?>" required>
                                <textarea class="formulario-invitaciones materialize-textarea col s12"
                                          name="descripcion" placeholder="Texto de la invitaci&oacute;n"
                                          required><?php echo isset($invitacion) ? $invitacion->descripcion : '' ?></textarea>
                                <small class="col s12">Solicitar confirmaci&oacute;n de asistencia</small>
                                <p class="col s3">
                                    <input class="with-gap" name="alerta" type="radio" value="1"
                                           id="test1" <?php echo isset($invitacion) ? (($invitacion->confirmacion == 1) ? 'checked' : '') : 'checked' ?> />
                                    <label for="test1">S&iacute;</label>
                                </p>
                                <p class="col s3">
                                    <input class="with-gap" name="alerta" type="radio" value="2"
                                           id="test2" <?php echo isset($invitacion) ? (($invitacion->confirmacion == 2) ? 'checked' : '') : '' ?> />
                                    <label for="test2">No</label>
                                </p>
                                <input type="hidden" name="confirmacion"
                                       value="<?php echo isset($invitacion) ? $invitacion->confirmacion : '1' ?>"/>
                                <input type="hidden" name="boton"/>
                                <input type="hidden" name="id_invitacion"
                                       value="<?php echo isset($invitacion) ? $invitacion->id_invitacion : '' ?>"/>
                                <div class="col s12">&nbsp;</div>
                                <button type="button"
                                        class="col s12 waves-effect waves-green btn-flat grey lighten-3 black-text"
                                        id="boton_g">Guardar
                                </button>
                                <div class="col s12">&nbsp;</div>
                                <button type="button"
                                        class="col s12 waves-effect waves-green btn-flat dorado-2 white-text"
                                        id="boton_g_e">
                                    <i class="material-icons left">mail_outline</i>Guardar y Enviar
                                </button>
                            </div>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="col s12 l8 white">
                <div style='margin: 0.5rem 0 1rem 0;border: 1px solid #e0e0e0;border-radius: 2px;'>
                    <div style="position: relative;">
                        <img class="responsive-img" style="width: 100%;" id="imagen"
                             src="<?php echo isset($invitacion) ? "data:$invitacion->mime;base64,".base64_encode($invitacion->base) : 'https://placeholdit.imgix.net/~text?txtsize=35&txt=&w=600&h=300&txttrack=0' ?>"/>
                        <div class="file-field input-field" style="position: absolute;top: 43%;left: 40%;z-index: 0;">
                            <div class="waves-effect dorado-2 waves-light btn-flat white ">
                                <span class="modal-trigger  white-text no-zindex" href="#mensaje_preventivo">
                                    <i class="material-icons left">camera_enhance</i>&nbsp;&nbsp;Subir imagen</span>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <br>
                        <h5 class="center-align" id='novio_novia'>
                            <b>
                                <?php
                                if (isset($invitacion)) {
                                    echo $invitacion->novia.' & '.$invitacion->novio;
                                } elseif (count($novios) == 2) {
                                    echo $novios[0]->nombre.' '.$novios[0]->apellido.' & '.$novios[1]->nombre.' '.$novios[1]->apellido;
                                } elseif ($this->session->userdata('genero') == 'Novia') {
                                    echo $this->session->userdata('nombre').' & Jane Doe';
                                } else {
                                    echo 'Jane Doe & '.$this->session->userdata('nombre');
                                }
                                ?>
                            </b>
                        </h5>
                        <br>
                        <h6 class="center-align" id='fecha'>
                            <?php if (isset($invitacion)) {
                                echo $invitacion->fecha;
                            } elseif (isset($boda)) {
                                echo strftime("%A %d de %B del %Y ", strtotime($boda->fecha_boda));
                            } ?>
                        </h6>
                        <br>
                        <h6 class="center-align"
                            id='lugar'><?php echo isset($invitacion) ? $invitacion->lugar : 'Lugar' ?></h6>
                    </div>
                    <hr class="style12" style="margin: 50 30 50 30;"/>
                    <div class="container">
                        <h6 class="center-align" id='titulo'>
                            <i class="material-icons left grey-text lighten-3">arrow_forward</i>
                            <?php echo isset($invitacion) ? $invitacion->titulo : 'T&iacute;tulo de la invitaci&oacute;n' ?>
                            <i class="material-icons right grey-text lighten-3">arrow_back</i>
                        </h6>
                        <br>
                        <p id='descripcion'><?php echo isset($invitacion) ? $invitacion->descripcion : 'Texto de la invitaci&oacute;n' ?></p>
                        <br>
                        <br>
                    </div>
                    <div id="confirmacion" <?php echo isset($invitacion) ? (($invitacion->confirmacion == 1) ? '' : 'style="display:none;"') : '' ?>>
                        <hr class="style12" style="margin: 50 30 50 30;"/>
                        <div class="container center-align">
                            <p>Por favor,confirma tu asistencia aqu&iacute;</p>
                            <button type="button" class="waves-effect waves-green btn-flat grey lighten-3 black-text">
                                Confirmar
                            </button>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="modal-imagen" class="modal ">
    <div class="modal-content">
        <div class="section">
            <h6 id="modal-imagen-title">Selecciona lo que se ver&aacute; en la invitaci&oacute;n</h6>
        </div>
        <div class="divider"></div>
        <br>
        <div class="row">
            <div class="col offset-m2 m8" id="modal-content">
                <img id="modal-content-image" width="100%">
                <img id="preview" class="hidden"/>
                <canvas id="canvas"></canvas>
            </div>
        </div>
    </div>
    <div class="row modal-footer">
        <button id="modal-imagen-aceptar" type="button"
                class="modal-action modal-close waves-effect waves-green btn-flat dorado-2 white-text col s12 l2 offset-l8">Guardar
        </button>
        <button id="modal-imagen-cancelar" type="button"
                class="modal-action modal-close waves-effect waves-green btn-flat col s12 l2 offset-l8">Cancelar
        </button>
    </div>
</div>

<div id="mensaje_preventivo" class="modal" style="max-width: 500px">
    <div class="modal-content center-align">
        <h4>Tamaño de Foto</h4>
        <p>Sólo están permitidas imágenes JPG o PNG de una altura máxima de 340px</p>
    </div>
    <div class="modal-footer " style="text-align: center">
        <input class="modal-action modal-close " type="file" accept="image/*" onchange="openFile(event)">
    </div>

</div>


<script>
    function onReady() {
        $('.modal').modal();

        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 15,
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar',
            format: 'yyyy-mm-dd',
            min: new Date()
        });
        $('#historial_invitacion').on('change', function () {
            location.href = '<?php echo base_url() ?>novios/invitados/Home/invitaciones/' + $('#historial_invitacion').val();
        });
        $('[name="novia"]').on('keyup', function () {
            $('#novio_novia b').text($('[name="novia"]').val() + ' & ' + $('[name="novio"]').val());
            $('[name="novia"]').attr('class', (($('[name="novia"]').attr('class')).split('invalid')[0]));
        });

        $('[name="novio"]').on('keyup', function () {
            $('#novio_novia b').text($('[name="novia"]').val() + ' & ' + $('[name="novio"]').val());
            $('[name="novio"]').attr('class', (($('[name="novio"]').attr('class')).split('invalid')[0]));
        });

        $('[name="fecha"]').on('change', function () {
            var f = new formato_fecha($('[name="fecha"]').val() + ' 12:00:00', 'es').dianombre_dia_mesnombre_anio();
            $('#fecha').text(f);
            $('[name="fecha"]').attr('class', (($('[name="fecha"]').attr('class')).split('invalid')[0]));
        });

        $('[name="lugar"]').on('keyup', function () {
            $('#lugar').text($('[name="lugar"]').val());
            $('[name="lugar"]').attr('class', (($('[name="lugar"]').attr('class')).split('invalid')[0]));
        });

        $('[name="titulo"]').on('keyup', function () {
            $('#titulo').html('<i class="material-icons left grey-text lighten-3">arrow_forward</i>' + $('[name="titulo"]').val() + '<i class="material-icons right grey-text lighten-3">arrow_back</i>');
            $('[name="titulo"]').attr('class', (($('[name="titulo"]').attr('class')).split('invalid')[0]));
        });

        $('[name="descripcion"]').on('keyup', function () {
            $('#descripcion').text($('[name="descripcion"]').val());
        });

        $('[name="alerta"]').on('change', function () {
            if ($('[name="alerta"]').prop('checked')) {
                $('[name="confirmacion"]').val(1);
                $('#confirmacion').show();
            } else {
                $('[name="confirmacion"]').val(2);
                $('#confirmacion').hide();
            }
        });

        $('#boton_g').on('click', function () {
            function onSuccess() {
                $('[name="boton"]').val(1);
            }

            comprobar(onSuccess);
        });

        $('#boton_g_e').on('click', function () {
            function onSuccess() {
                $('[name="boton"]').val(2);
            }

            comprobar(onSuccess);
        });
    }

    function comprobar(onSuccess) {
        if (isEmpty($('[name="novia"]').val()) || isEmpty($('[name="novio"]').val())
            || isEmpty($('[name="fecha"]').val()) || isEmpty($('[name="lugar"]').val())
            || isEmpty($('[name="titulo"]').val()) || isEmpty($('[name="descripcion"]').val())
            || isEmpty($('[name="imagen"]').val())) {


            if (isEmpty($('[name="novia"]').val())) {
                $('[name="novia"]').attr('class', $('[name="novia"]').attr('class') + ' invalid');
            } else {
                $('[name="novia"]').attr('class', (($('[name="novia"]').attr('class')).split('invalid')[0]));
            }


            if (isEmpty($('[name="novio"]').val())) {
                $('[name="novio"]').attr('class', $('[name="novio"]').attr('class') + ' invalid');
            } else {
                $('[name="novio"]').attr('class', (($('[name="novio"]').attr('class')).split('invalid')[0]));
            }

            if (isEmpty($('[name="fecha"]').val())) {
                $('[name="fecha"]').attr('class', $('[name="fecha"]').attr('class') + ' invalid');
            } else {
                $('[name="fecha"]').attr('class', (($('[name="fecha"]').attr('class')).split('invalid')[0]));
            }

            if (isEmpty($('[name="lugar"]').val())) {
                $('[name="lugar"]').attr('class', $('[name="lugar"]').attr('class') + ' invalid');
            } else {
                $('[name="lugar"]').attr('class', (($('[name="lugar"]').attr('class')).split('invalid')[0]));
            }

            if (isEmpty($('[name="titulo"]').val())) {
                $('[name="titulo"]').attr('class', $('[name="titulo"]').attr('class') + ' invalid');
            } else {
                $('[name="titulo"]').attr('class', (($('[name="titulo"]').attr('class')).split('invalid')[0]));
            }

            if (isEmpty($('[name="descripcion"]').val())) {
                $('[name="descripcion"]').attr('class', $('[name="descripcion"]').attr('class') + ' invalid');
            } else {
                $('[name="descripcion"]').attr('class', (($('[name="descripcion"]').attr('class')).split('invalid')[0]));
            }

            if (isEmpty($('[name="imagen"]').val())) {
                alert('Debe indicar una imagen de cabecera.');
            }
        } else {
            onSuccess();
            $('#formulario_nuevo').submit();
        }
    }

    function openFile(event) {
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function (e) {
            var img = document.createElement("img");
            img.src = e.target.result;

            img.onload = function () {
                console.log(this.width, this.height);
                if (this.width < 600 || this.height < 300) {
                    console.log("cerbero");
                    //alert('La imagen debe tener un ancho minimo de 600 px, y una altura minima de 300 px.');
                } else {
                    jcropModal({
                        img: (this.src),
                        minWidth: 600,
                        minHeight: 300,
                        title: 'Selecciona lo que se ver&aacute; en la invitaci&oacute;n',
                        success: function (result) {
                            $('#imagen').attr('src', result);
                            $('[name="imagen"]').val(result);
                        },
                        error: function () {
                        }
                    });
                }
            };


        };
        reader.readAsDataURL(input.files[0]);
    }

    function jcropModal(opt) {
        $("#modal-content").html('<img id="modal-content-image" ><img id="preview"  class="hidden"  /><canvas id="canvas"  class="hidden" ></canvas>');
        var img = $("#modal-content-image").get(0);
        img.src = opt.img;
        $("#modal-imagen-title").html(opt.titulo);
        if (img.width >= opt.minWidth && img.height >= opt.minHeight) {
            img.onload = function () {
                $("#modal-imagen").modal("open");
                $("#modal-imagen-aceptar").off("click");
                $("#modal-imagen-cancelar").click(function () {
                    opt.error();
                });
                $("#modal-imagen-aceptar").on("click", function () {
                    var img = $("#preview").get(0);
                    $("#modal-imagen").modal("close");
                    var result = img.src;
                    opt.success(result);
                });
                var dim = [img.width, img.height];
                img.width = $("div#modal-content").width();
                $('#modal-content-image').Jcrop({
                    trueSize: dim,
                    bgColor: 'black',
                    bgOpacity: .4,
                    setSelect: [10, 10, 0, 0],
                    aspectRatio: opt.minWidth / opt.minHeight,
                    minSize: [opt.minWidth, opt.minHeight],
                    onChange: showPreview,
                    onSelect: showPreview
                });
            };
        } else {
            opt.error();
        }

        function showPreview(coords) {
            var canvas = document.getElementById("canvas");
            var ctx = canvas.getContext("2d");
            if (ctx) {
                var img = $('#modal-content-image').get(0);
                ctx.drawImage(img, coords.x, coords.y, coords.w, coords.h, 0, 0, coords.w, coords.h);
                $("#preview").get(0).src = (canvas.toDataURL("image/png"));
                canvas.width = coords.w;
                canvas.height = coords.h;
            }
        }
    }

    function isEmpty(element) {
        if (element == '' || element == null || element.length == 0) {
            return true;
        }
        return false;
    }

    var formato_fecha = function (fecha, idioma) {
        var _fecha;
        if (typeof fecha == 'string') {
            _fecha = new Date(fecha.replace(/-/g, "/"));
        } else {
            _fecha = fecha;
        }
        var _lang = idioma;
        var _days = new Array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");
        var _months = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        var _days_en = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        var _months_en = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        this.dianombre_dia_mesnombre_anio = function () {
            return ((_lang == 'es') ? _days[_fecha.getDay()] : _days_en[_fecha.getDay()]) + ' '
                + _fecha.getDate() + ' de '
                + ((_lang == 'es') ? _months[_fecha.getMonth()] : _months_en[_fecha.getMonth()])
                + ' del ' + _fecha.getFullYear();
        };
    };
</script>
<?php $this->view("japy/prueba/footer") ?>
