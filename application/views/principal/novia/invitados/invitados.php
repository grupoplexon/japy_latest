<?php $this->view("principal/newheadernovia") ?>
<?php $this->view("principal/novia/menu") ?>
    <style>

        .panel-invitados{
            padding: 15px 3% !important;
            margin: 0;
        }

        #agregar_invitado a.active{
            color: #0a6aa1!important;
        }

        #agregar_invitado a{
            color: white!important;
        }
        a , .dorado-2-text{
            color: #515055 !important;
        }

        @media only screen and (min-width: 319px) and (max-width: 425px) {
            .panel-invitados{
                padding: 15px 3%!important;

            }
            td.seccion-invitados .texto{
                max-width: 57% !important;
            }
            tr{
                height: 50px;!important;
            }
            .dtr-data select{
                display: block !important;
            }
            .child ul{
                margin-bottom: 0!important;
            }

            .child ul li[data-dtr-index="4"],.child ul li[data-dtr-index="5"] {
                height: 50px!important;
            }
        }
    </style>
    <div class=" body-container" style="margin-bottom: 50px;">
        <?php $this->view("principal/novia/invitados/menu_principal") ?>
        <div class="row" style="border-top: 1px solid #eee; margin-top: -21px; padding: 10px 0 10px 0;">
            <div class="col m12 s12 l6">
                <div class="z-depth-1">
                    <div class="row  grey lighten-5 panel-invitados">
                        <div class="col s4 offset-s1 m3 center-align">
                            <img class="responsive-img principal-invitados-icon center"
                                 src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-listadeinvitados1.png"/>
                        </div>
                        <div class="col s6 m3 center-align" style="padding: 15px 0 0 15px;">
                            <h6 class="dorado-2-text seccion-title"><b>INVITADOS</b></h6>
                            <p href="<?php echo base_url() ?>novios/invitados/Home"
                               style="font-size: 12px; color: #3a393b;"><?php echo $sin_direccion; ?> sin direcci&oacute;n</p>
                            <p style="font-size: 12px; color: #3a393b;"><?php echo $no_enviados; ?> sin invitaci&oacute;n</p>
                        </div>
                        <div class="col s12 m6">
                            <div class="row">
                                <div class="col s3 m3 center-align grey lighten-5 no-padding" style="border-right: 1px solid #fff;">
                                    <h5 class="dorado-2-text"><b><?php echo $sexo['hombre']; ?></b></h5>
                                    <small>hombres</small>
                                    <div style="margin-top:20px;"></div>
                                </div>
                                <div class="col s3 m3 center-align grey lighten-5 no-padding" style="border-right: 1px solid #fff;">
                                    <h5 class="dorado-2-text"><b><?php echo $sexo['mujer']; ?></b></h5>
                                    <small>mujeres</small>
                                    <div style="margin-top:20px;"></div>
                                </div>
                                <div class="col s3 m3 center-align grey lighten-5 no-padding" style="border-right: 1px solid #fff;">
                                    <h5 class="dorado-2-text"><b><?php echo $sexo['nino']; ?></b></h5>
                                    <small>ni&ntilde;os</small>
                                    <div style="margin-top:20px;"></div>
                                </div>
                                <div class="col s3 m3 center-align grey lighten-5 no-padding">
                                    <h5 class="dorado-2-text"><b><?php echo $sexo['bebe']; ?></b></h5>
                                    <small>bebes</small>
                                    <div style="margin-top:20px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row  gris-2 btn-add" style="padding-bottom: 10px;">
                        <div class="col s12  center-align m5 offset-m1">
                            <a id="add_guest" class="btn modal-trigger waves-effect waves-light  invitados dorado-2 no-zindex" onclick="clear_form()" href="#agregar_invitado" style="font-size: 20px">
                                <i class="material-icons left">person_add</i>
                                <small>A&ntilde;adir invitado</small>
                            </a>
                        </div>
                        <div class="col s12 center-align m5">
                            <a class="waves-effect waves-light btn invitados gris-2 white-text"
                               href="<?php echo base_url() ?>novios/invitados/Home/importar">
                                <i class="material-icons left white-text">insert_drive_file</i>
                                Importar invitados
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col m6 s6 l3 center-align">
                <div class="z-depth-1">
                    <div class="row nowrap grey lighten-5" style="padding: 10px 10px 15px 10px;">
                        <div class="col s12  m4">
                            <img class="responsive-img principal-invitados-icon"
                                 src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-mi-perfil1.png" alt=""/>
                        </div>
                        <div class="col s12 m8" style="padding: 15px 0 0 15px;">
                            <h6><b>ASISTENTES</b></h6>
                            <p class="green-text darken-4"
                               style="font-size: 12px;"><?php echo $asistencia['confirmados'] ?> confirmado</p>
                            <p class="dorado-2-text" style="font-size: 12px;"><?php echo $asistencia['cancelados'] ?>
                                cancelados</p>
                        </div>
                    </div>
                    <div class="row nowrap gris-2 center-align" style="padding-bottom: 10px;">
                        <a class="waves-effect waves-light btn invitados gris-2 white-text truncate"
                           href="<?php echo base_url() ?>novios/invitados/Home/invitaciones">
                            <i class="material-icons left white-text">offline_pin</i>Solicitar <span class="hide-on-small-only">confirmaci&oacute;n</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col m6 s6 l3 center-align">
                <div class="z-depth-1">
                    <div class="row nowrap grey lighten-5" style="padding: 10px 10px 15px 10px;">
                        <div class="col s12 m4">
                            <img class="responsive-img principal-invitados-icon"
                                 src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-controldemesas1.png" alt=""/>
                        </div>
                        <div class="col s12 m8" style="padding: 15px 0 0 15px;">
                            <h6><b>MESAS</b></h6>
                            <p class="dorado-2-text"
                               style="font-size: 12px; font-weight: bold;"><?php echo $sentados.' de '.count($invitados); ?></p>
                            <p class="black-text darken-5" style="font-size: 12px;">invitados sentados</p>
                        </div>
                    </div>
                    <div class="row nowrap gris-2 center-align" style="padding-bottom: 10px;">
                        <a href="<?php echo site_url("novios/mesa") ?>"
                           class="waves-effect waves-light btn invitados gris-2 white-text">
                            <i class="material-icons left white-text">panorama_fish_eye</i>Organizar mesa
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <section style="padding:10px;">
            <table class="data-table invitados cell-border" style="width: 100%;">
                <thead>
                <tr>
                    <td style="width: 25%;"><b>Nombre</b></td>
                    <td style="width: 15%"><b>Asistencia</b></td>
                    <td style="width: 15%"><b>Men&uacute;</b></td>
                    <td style="width: 15%"><b>Mesas</b></td>
                    <td style="width: 15%"><b>Contacto</b></td>
                    <td style="width: 15%"><b>Opciones</b></td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($invitados as $key => $value) { ?>
                    <tr id="<?php echo $value->id_invitado; ?>">
                        <td class="seccion-invitados"
                            style="<?php echo empty($value->invitado_por) ? '' : 'border-left: 5px solid #f5c564;'; ?>">
                            <div class="imagen">
                                <i class="<?php echo generoImage($value->edad, $value->sexo); ?>"></i>
                            </div>
                            <div class="texto">
                                <span class="truncate"><?php echo $value->nombre.' '.$value->apellido; ?></span>
                                <small class="grey-text darken-6"><?php echo $value->grupo; ?></small>
                            </div>
                        </td>
                        <td>
                            <select class="listagrupo"
                                    onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'confirmacion', this);">
                                <option value='' disabled selected>Asistencia</option>
                                <option value="1"
                                        class="orange-text darken-3" <?php echo ($value->confirmado == 1) ? "selected" : "" ?>>Pendiente
                                </option>
                                <option value="2"
                                        class="green-text darken-4" <?php echo ($value->confirmado == 2) ? "selected" : "" ?>>Confirmado
                                </option>
                                <option value="3"
                                        class="deep-orange-text darken-4" <?php echo ($value->confirmado == 3) ? "selected" : "" ?>>Cancelado
                                </option>
                            </select>
                        </td>
                        <td>
                            <select class="listagrupo"
                                    onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'menu', this);">
                                <option value='' disabled selected>Men&uacute;</option>
                                <?php foreach ($menus as $x => $mn) { ?>
                                    <option value=" <?php echo $mn->id_menu ?>
                        "<?php echo ($mn->id_menu == $value->id_menu) ? 'selected' : '' ?>
                                    ><?php echo $mn->nombre ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td>
                            <select class="listagrupo mesas-select"
                                    onchange="updateInvitado('<?php echo $value->id_invitado ?>', 'mesa', this);"
                                    data-mesa="<?php echo $value->id_mesa ?>">
                                <option value='' disabled selected>Mesa</option>
                                <?php foreach ($mesas as $y => $ms) { ?>
                                    <option value="<?php echo $ms->id_mesa ?>" <?php echo (($ms->sillas - $ms->ocupado) <= 0) ? ' disabled ' : '' ?>
                                            data-sillas="<?php echo $ms->sillas ?>"
                                            data-ocupadas="<?php echo $ms->ocupado ?>"
                                            data-nombre="<?php echo $ms->nombre ?>"
                                            <?php echo ($ms->id_mesa == $value->id_mesa) ? 'selected' : '' ?>><?php echo $ms->nombre ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td>
                            <a class="modal-trigger secondary-content btn table white" href="#agregar_invitado"
                               onclick='updateInterfaceInvitado("<?php echo $value->id_invitado ?>", 2);'>
                                <i class="fa fa-at grey-text darken-5"
                                   aria-hidden="true"></i><?php echo empty($value->correo) ? '<i class="material-icons dorado-2-text">add_circle</i>' : '<i class="material-icons green-text darken-5">check_circle</i>'; ?>

                            </a>
                            <a class="modal-trigger secondary-content btn table white" href="#agregar_invitado"
                               onclick='updateInterfaceInvitado("<?php echo $value->id_invitado ?>", 2);'>
                                <i class="material-icons grey-text darken-5">home</i><?php echo empty($value->direccion) ? '<i class="material-icons dorado-2-text">add_circle</i>' : '<i class="material-icons green-text darken-5">check_circle</i>'; ?>
                            </a>
                        </td>
                        <td>
                            <?php if ($value->grupo != 'Novios') { ?>
                                <button class="secondary-content btn table white"
                                        onclick="deleteInvitado('<?php echo $value->nombre ?>', '<?php echo $value->id_invitado ?>')">
                                    <i class="material-icons grey-text darken-5">delete</i>
                                </button>
                            <?php } ?>
                            <a class="modal-trigger secondary-content btn table white edit-guest" href="#agregar_invitado"
                               onclick='updateInterfaceInvitado("<?php echo $value->id_invitado ?>", 1);'>
                                <i class="material-icons grey-text darken-5">edit</i>
                            </a>
                            <?php if ($value->email_enviado == 1) { ?>
                                <a class="secondary-content btn table white" href="#">
                                    <i class="material-icons grey-text darken-5">email</i>
                                </a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </section>

    </div>

    <!--        M   O    D    A    L        -->
    <div id="agregar_invitado" class="modal modal-japy">
        <form id="form-add-update" method="post" data-parsley-validate=""
              action="<?php echo base_url() ?>novios/invitados/Home/add_invitado">
            <div class="modal-content row no-margin-element" >
                <div class="col s12">
                    <ul class="tabs primary-background">
                        <li class="tab col s4">
                            <a id="icon-1" href="#invitado">
                                <i class="material-icons">person_pin</i>
                            </a>
                        </li>
                        <!-- <li class="tab col s4" >
                            <a id="icon-2" href="#contacto">
                                <i class="material-icons">phone_iphone</i>
                            </a>
                        </li> -->
                        <li class="tab col s4">
                            <a id="icon-3" href="#acompanante-cont">
                                <i class="material-icons">person_add</i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="invitado">
                    <i class="material-icons white-text large center-align" style="margin:0 0 0 35%">account_circle</i>
                    <h5 class=" center-align white-text">Invitado</h5>
                    <div class="divider"></div>
                    <div id="datos_personales" >
                        <div class="row" style="margin-bottom: 0;color: black">
                            <input id="id_invitado" name="id_invitado" type="hidden">
                            <div class="input-field col s10 offset-s1">
                                <input type="text" class="validate"  name="nombre" placeholder="Nombre(s)*"
                                       minlength="4" maxlength="20"  required autofocus>
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <input  name="apellido" type="text" class="validate" placeholder="Apellido(s)"
                                        minlength="4" maxlength="20"  required>
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <select name="grupo" required>
                                    <!-- <option value="" disabled selected>Grupo*</option> -->
                                    <?php foreach ($grupos as $key => $value) { ?>
                                        <option value="<?php echo $value->id_grupo ?>" ><?php echo  $value->grupo; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <select name="menu" required>
                                    <option value="" disabled selected>Men&uacute;*</option>
                                    <?php foreach ($menus as $key => $value) { ?>
                                        <option value="<?php echo $value->id_menu ?>"><?php echo $value->nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <select name="edad"  required>
                                    <option value="" disabled selected>Edad*</option>
                                    <option value="1">Adulto</option>
                                    <option value="2">Ni&ntilde;o</option>
                                    <option value="3">Beb&eacute;</option>
                                </select>
                            </div>
                            <div class="input-field col s10 offset-s1" >
                                <select name="sexo"  required>
                                    <option value="" disabled selected>Sexo*</option>
                                    <option value="1">Hombre</option>
                                    <option value="2">Mujer</option>
                                </select>
                            </div>
                            <div class="input-field col s10 offset-s1">
                                  <input class="validate" type="email" name="correo" placeholder="E-mail"
                                         minlength="10" maxlength="80">
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <input class="validate" type="number" name="telefono" placeholder="Tel&eacute;fono"
                                    minlength="8" maxlength="15">
                            </div>
                            <div class="input-field col s10 offset-s1">
                                  <input class="validate" type="text" name="direccion" placeholder="Direcci&oacute;n"
                                         minlength="10" maxlength="70">
                                </div>
                                <div class="input-field col s10 offset-s1" style="margin-bottom: 1rem">
                                  <input class="validate" type="number" name="cp" placeholder="C&oacute;digo Postal"
                                         minlength="5" maxlength="5">
                                </div>
                            
                            
                        </div>
                    </div>
                </div>
                <!-- <div id="contacto">
                    <i class="material-icons white-text large center-align" style="margin:0 0 0 35%">phone_iphone</i>
                    <h5 class=" center-align white-text">Contacto</h5>
                    <div class="divider"></div>
                    <div class="row" style="margin-bottom: 0">
                        <div id="datos_contacto" class="col s12">
                            <div class="row" style="margin-bottom: 0;color: black">
                                <div class="input-field col s10 offset-s1">
                                  <input class="validate" type="email" name="correo" placeholder="E-mail"
                                         minlength="10" maxlength="80">
                                <!-- DIVIDER ------><!-- 
                                </div>
                                <div class="input-field col s10 offset-s1">
                                  <input class="validate" type="number" name="telefono" placeholder="Tel&eacute;fono"
                                         minlength="8" maxlength="15">
                                </div>
                                <div class="input-field col s10 offset-s1">
                                  <input class="validate" type="text" name="direccion" placeholder="Direcci&oacute;n"
                                         minlength="10" maxlength="70">
                                </div>
                                <div class="input-field col s10 offset-s1">
                                  <input class="validate" type="number" name="cp" placeholder="C&oacute;digo Postal"
                                         minlength="5" maxlength="5">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div id="acompanante-cont">
                    <i class="material-icons white-text medium center-align" style="margin:0 0 0 35%">person_add</i>
                    <h5 class=" center-align white-text">Acompañante</h5>
                    <div class="divider"></div>
                    <div id="acompanante">
                        <div class="row" style="margin-bottom: 0;">
                            <input id="id_invitado" name="id_invitado" type="hidden">
                            <div class="input-field col s10 offset-s1">
                                <input id="nombre_acompanante" type="text" class="validate" placeholder="Nombre(s)*"
                                       minlength="5" maxlength="30">
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <input  id="apellido_acompanante" type="text" class="validate" placeholder="Apellido(s)"
                                        minlength="5" maxlength="30">
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <select id="grupo_acompanante">
                                    <option value="" disabled selected>Grupo*</option>
                                    <?php foreach ($grupos as $key => $value) { ?>
                                        <option value="<?php echo $value->id_grupo ?>" <?php echo ($value->grupo == 'Novios') ? 'disabled' : ''; ?> ><?php echo ($value->grupo == 'Novios') ? $value->grupo : str_replace($this->session->userdata('genero'), $this->session->userdata('nombre'), $value->grupo); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <select id="menu_acompanante">
                                    <option value="" disabled selected>Men&uacute;*</option>
                                    <?php foreach ($menus as $key => $value) { ?>
                                        <option value="<?php echo $value->id_menu ?>"><?php echo $value->nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="input-field col s10 offset-s1">
                                <select id="edad_acompanante" >
                                    <option value="" disabled selected>Edad*</option>
                                    <option value="1">Adulto</option>
                                    <option value="2">Ni&ntilde;o</option>
                                    <option value="3">Beb&eacute;</option>
                                </select>
                            </div>
                            <div class="input-field col s10 offset-s1" style="margin-bottom: 1rem">
                                <select id="sexo_acompanante" >
                                    <option value="" disabled selected>Sexo*</option>
                                    <option value="1">Hombre</option>
                                    <option value="2">Mujer</option>
                                </select>
                            </div>
                            <button class="col s10 offset-s1 l4 offset-l4 btn-add btn invitados center-align grey darken-5"
                                    id="anadir_invitado_form"
                                    type="button">
                                A&Ntilde;ADIR
                            </button>
                        </div>
                        <div id="acompanante_new"></div>
                    </div>
                </div>
            </div>
            <?php $this->view("principal/novia/invitados/modal-footer");?>
        </form>
    </div>
<?php $this->view("japy/prueba/footer") ?>
    <script>
        $(document).ready(() => {
            let $nombre   = "" ;
            let $apellido = "" ;
            let $grupo    = "" ;
            let $menu     = "" ;
            let $edad     = "" ;
            let $sexo     = "" ;
            let $form = $('#form-add-update').parsley({
                'excluded': ':disabled',
            });

            $("#save").on("click", function (e) {
                e.preventDefault();

                $('#form-add-update').submit();
            });
            $('#icon-1').on('click',function () {
                $('.modal-footer').show();
            });
            $('#icon-2').on('click',function () {
                $nombre   = $('[name="nombre"]').parsley();
                $apellido = $('[name="apellido"]').parsley();
                $grupo    = $('[name="grupo"]').parsley();
                $menu     = $('[name="menu"]').parsley();
                $edad     = $('[name="edad"]').parsley();
                $sexo     = $('[name="sexo"]').parsley();
                if(!$nombre.isValid() || !$apellido.isValid() || !$grupo.isValid() ||
                    !$menu.isValid() || !$edad.isValid() || !$sexo.isValid()){
                    $('.modal-footer').hide();
                    swal({
                        title: '...Atención...',
                        text: 'Para guardar el contacto es necesario llenar los datos del invitado',
                        icon: 'warning',
                        confirmButtonText: 'Ok',
                        buttons: ['Cancel','Ok'],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) $('#icon-1').trigger('click');
                        else  $('#icon-1').trigger('click');
                    });
                }else{
                    $('.modal-footer').show();
                }
            });
            $('#icon-3').on('click',function () {
                $nombre   = $('[name="nombre"]').parsley();
                $apellido = $('[name="apellido"]').parsley();
                $grupo    = $('[name="grupo"]').parsley();
                $menu     = $('[name="menu"]').parsley();
                $edad     = $('[name="edad"]').parsley();
                $sexo     = $('[name="sexo"]').parsley();
                if(!$nombre.isValid() || !$apellido.isValid() || !$grupo.isValid() ||
                    !$menu.isValid() || !$edad.isValid() || !$sexo.isValid()){
                    $('.modal-footer').hide();
                    swal({
                        title: '...Atención...',
                        text: 'Para guardar los acompañantes es necesario llenar los datos del invitado',
                        icon: 'warning',
                        confirmButtonText: 'Ok',
                        buttons: ['Cancel','Ok'],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) $('#icon-1').trigger('click');
                        else  $('#icon-1').trigger('click');

                    });
                }else{
                    $('.modal-footer').show();
                }
            });

            $('.edit-guest').on('click',function () {
                $('#icon-1').trigger('click');
                $form.reset();
            });
            $('.add_guest').on('click',function () {
                $('#icon-1').trigger('click');
            });

        });

        function onReady() {
            $('#agregar_invitado').modal();
            // $('.data-table').DataTable({
            //     "order": [[0, "asc"]],
            //     responsive:true,
            //     select:true,
            //     paging: false,
            //     language: {
            //         processing: "Procesando...",
            //         search: "Buscar:",
            //         lengthMenu: "Mostrar _MENU_ ",
            //         info: "    _TOTAL_ invitados",
            //         infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 invitados",
            //         infoFiltered: "(filtrado de un total de _MAX_ invitados)",
            //         infoPostFix: "",
            //         loadingRecords: "Cargando...",
            //         zeroRecords: "No se encontraron resultados",
            //         emptyTable: "Ning&uacute;n dato disponible en esta tabla",
            //         paginate: {
            //             first: "<i class='material-icons primary-text'>first_page</i>",
            //             previous: "<i class='material-icons primary-text'>chevron_left</i>",
            //             next: "<i class='material-icons primary-text'>chevron_right</i>",
            //             last: "<i class='material-icons primary-text'>last_page</i>"
            //         },
            //         aria: {
            //             sortAscending: ": Activar para ordenar la columna de manera ascendente",
            //             sortDescending: ": Activar para ordenar la columna de manera descendente"
            //         }
            //     },
            //     "columnDefs": [
            //         {"type": "date-euro", targets: 1},
            //         {"orderable": false, "targets": 5},
            //         {"orderable": false, "targets": 4},
            //     ]
            // });
            // $('#table_id').DataTable();

            $('#anadir_invitado_form').on('click', function () {
                if (isEmpty($('#grupo_acompanante').val()) || isEmpty($('#nombre_acompanante').val()) || isEmpty($('#apellido_acompanante').val())) {

                    if (isEmpty($('#nombre_acompanante').val())) {
                        $('#nombre_acompanante').attr('class', $('#nombre_acompanante').attr('class') + ' invalid');
                    } else {
                        $('#nombre_acompanante').attr('class', (($('#nombre_acompanante').attr('class')).split('invalid')[0]));
                    }

                    if (isEmpty($('#apellido_acompanante').val())) {
                        $('#apellido_acompanante').attr('class', $('#apellido_acompanante').attr('class') + ' invalid');
                    } else {
                        $('#apellido_acompanante').attr('class', (($('#apellido_acompanante').attr('class')).split('invalid')[0]));
                    }

                    if (isEmpty($('#grupo_acompanante').val())) {
                        alert('Seleccione un grupo');
                    }

                } else {
                    var temp = '<div class="invitado_modal">' +
                        '<i class="material-icons" onclick="deleteNewAcompanante(this);" style="cursor:pointer">cancel</i>' +
                        '<i class=" ' + generoImage($('#edad_acompanante').val(), $('#sexo_acompanante').val()) + '" ' +
                        'style="background-color: white"></i>' + $('#nombre_acompanante').val() +
                        '<div>' +
                        '<input type="hidden" name="nombre_acompanante[]" value="' + $('#nombre_acompanante').val() + '" />' +
                        '<input type="hidden" name="apellido_acompanante[]" value="' + $('#apellido_acompanante').val() + '" />' +
                        '<input type="hidden" name="grupo_acompanante[]" value="' + $('#grupo_acompanante').val() + '" />' +
                        '<input type="hidden" name="menu_acompanante[]" value="' + $('#menu_acompanante').val() + '" />' +
                        '<input type="hidden" name="sexo_acompanante[]" value="' + $('#sexo_acompanante').val() + '" />' +
                        '<input type="hidden" name="edad_acompanante[]" value="' + $('#edad_acompanante').val() + '" />' +
                        '</div>' +
                        '</div>';
                    $('#acompanante_new').append(temp);
                    $('#contador').text(parseInt($('#contador').text()) + 1);
                    $('#contador').show();
                    $('#nombre_acompanante').val('');
                    $('#apellido_acompanante').val('');
                }

            });

            $('#anadir_invitado').on('click', function () {
                if ($('#id_acompanante').val() != null) {
                    var id_acompanante = $('#id_acompanante').val();
                    var sexo = $($('#id_acompanante').find(":selected")[0]).data('sexo');
                    var edad = $($('#id_acompanante').find(":selected")[0]).data('edad');

                    var temp = '<div class="invitado_modal">' +
                        '<i class="material-icons" onclick="deleteNewAcompanante(this);">cancel</i>' +
                        '<i class="' + generoImage(edad, sexo) + '" ></i>' +
                        $('#id_acompanante option[value="' + $('#id_acompanante').val() + '"]').text() +
                        '<div >' +
                        '<input type="hidden" name="id_acompanante[]" value="' + id_acompanante + '"/>' +
                        '</div>' +
                        '</div>';
                    $('#acompanante_new').append(temp);
                    $('#contador').text(parseInt($('#contador').text()) + 1);
                    $('#contador').show();
                    $('#id_acompanante option[value="' + id_acompanante + '"]').attr('disabled', '');
                    $('#id_acompanante').material_select();
                }
            });

            $('[name="sexo"]').on('change', function () {
                $('#icon_persona_modal').attr('class', generoImage($('[name="edad"]').val(), $('[name="sexo"]').val()));
            });

            $('[name="edad"]').on('change', function () {
                $('#icon_persona_modal').attr('class', generoImage($('[name="edad"]').val(), $('[name="sexo"]').val()));
            });
        }

        function clear_form() {
            $('#form-add-update').attr('action', '<?php echo base_url() ?>novios/invitados/Home/add_invitado');
            $('#icon_persona_modal').attr('class', generoImage(1, 1));
            $('[name="nombre"]').val('');
            $('[name="apellido"]').val('');
            $('[name="id_invitado"]').val('');

            $('[name="menu"]').val("");
            $('[name="menu"]').material_select();

            $('[name="sexo"]').val("");
            $('[name="sexo"]').material_select();

            $('[name="edad"]').val("");
            $('[name="edad"]').material_select();

            $('[name="grupo"]').val("");
            $('[name="grupo"]').removeAttr('disabled', '');
            $('[name="grupo"]').material_select();

            $('[name="correo"]').val('');
            $('[name="telefono"]').val('');
            $('[name="celular"]').val('');
            $('[name="direccion"]').val('');
            $('[name="cp"]').val('');
            $('#addInvitado1').click();
            $('#acompanante_new').text('');
            $('#contador').hide();
            $('#contador').text(0);
            $('#id_acompanante option').removeAttr('disabled');
            $('#id_acompanante').material_select();
        }

        function updateInterfaceInvitado(id, begin) {
            $('#form-add-update').attr('action', '<?php echo base_url() ?>novios/invitados/Home/update_invitado');

            $.ajax({
                url: '<?php echo base_url() ?>novios/invitados/Home/datos_invitado/' + id,
                method: "GET",
                timeout: 4000,
            }).done(function (datos) {

                switch (begin) {
                    case 1:
                        $('#addInvitado1').click();
                        break;
                    case 2:
                        $('#addInvitado2').click();
                        break;
                    case 3:
                        $('#addInvitado3').click();
                        break;
                    default:
                        $('#addInvitado1').click();
                        break;
                }

                $('#acompanante_new').text('');
                $('#contador').hide();
                $('#contador').text(0);
                $('#id_acompanante option').removeAttr('disabled');
                rellenarInfo(datos);
                $('#id_acompanante').material_select();
            });

            function rellenarInfo(json) {
                $('#icon_persona_modal').attr('class', generoImage(json.edad, json.sexo));
                $('[name="nombre"]').val(json.nombre);
                $('[name="apellido"]').val(json.apellido);
                $('[name="id_invitado"]').val(json.id_invitado);
                $('[name="grupo"]').val(json.id_grupo);
                $('[name="grupo"]').material_select();
                $('[name="menu"]').val(json.id_menu);
                $('[name="menu"]').material_select();
                $('[name="sexo"]').val(json.sexo);
                $('[name="sexo"]').material_select();
                $('[name="edad"]').val(json.edad);
                $('[name="edad"]').material_select();
                var grupo = $('[name="grupo"] option[value="' + json.id_grupo + '"]').text();
                if (grupo == 'Novios') {
                    $('[name="grupo"]').attr('disabled', '');
                    $('[name="grupo"]').material_select();
                } else {
                    $('[name="grupo"]').removeAttr('disabled', '');
                    $('[name="grupo"]').material_select();
                }

                $('[name="correo"]').val(json.correo);
                $('[name="telefono"]').val(json.telefono);
                $('[name="celular"]').val(json.celular);
                $('[name="direccion"]').val(json.direccion);
                $('[name="cp"]').val(json.codigo_postal);

                $('#id_acompanante option[value="' + json.id_invitado + '"]').attr('disabled', '');
                if (json.acompanante != null && (json.acompanante).length > 0) {
                    $.each(json.acompanante, function (i, obj) {
                        $('#id_acompanante option[value="' + obj.id_invitado + '"]').attr('disabled', '');
                        var temp = '<div class="invitado_modal">' +
                            '<i class="material-icons" onclick="deleteAcompanante(this,&#39;' + obj.id_invitado + '&#39;);">cancel</i>' +
                            '<i class="' + generoImage(obj.edad, obj.sexo) + '"></i>' +
                            obj.nombre + ' ' + obj.apellido +
                            '</div>';

                        $('#acompanante_new').append(temp);
                    });
                    $('#contador').text((json.acompanante).length);
                    $('#contador').show();
                }
            }
        }

        function updateInvitado(id_invitado, campo, selected) {
            var origen = $(selected).val();
            switch (campo) {
                case 'confirmacion':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            confirmado: origen,
                            tipo: 'confirmacion',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'grupo':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_grupo: origen,
                            tipo: 'grupo',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'email_enviado':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            email_enviado: origen,
                            tipo: 'email_enviado',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'mesa':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_mesa: origen,
                            tipo: 'mesa',
                        },
                    }).done(function () {
                        $('.mesas-select option[value="' + $(selected).data('mesa') + '"]').each(function (i, obj) {
                            var ocupadas = $(obj).data('ocupadas');
                            ocupadas = (ocupadas == 0) ? 0 : ocupadas - 1;
                            $(obj).data('ocupadas', ocupadas);

                            if (parseInt($(obj).data('sillas')) - ocupadas > 0) {
                                $(obj).removeAttr('disabled');
                            } else {
                                $(obj).attr('disabled', 'true');
                            }
                        });
                        $(selected).data('mesa', origen);

                        $('.mesas-select option[value="' + origen + '"]').each(function (i, obj) {
                            var ocupadas = $(obj).data('ocupadas');
                            ocupadas = ocupadas + 1;
                            $(obj).data('ocupadas', ocupadas);

                            if (parseInt($(obj).data('sillas')) - ocupadas > 0) {
                                $(obj).removeAttr('disabled');
                            } else {
                                $(obj).attr('disabled', 'true');
                            }
                        });
                        $('select').material_select();
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
                case 'menu':
                    $.ajax({
                        url: "<?php echo base_url() ?>novios/invitados/Home/updateInvitado",
                        method: "POST",
                        timeout: 4000,
                        data: {
                            id_invitado: id_invitado,
                            id_menu: origen,
                            tipo: 'menu',
                        },
                    }).done(function (result) {
                        console.log('success');
                    }).fail(function (result) {
                        console.log('error');
                    });
                    break;
            }
        }

        function deleteInvitado(nombre, id) {
            swal({
                title: 'Estas seguro que quieres borrar a '+nombre+' ?',
                icon: 'error',
                buttons: ['Cancelar', true],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    location.href = "<?= base_url(); ?>novios/invitados/Home/delete_invitado/" + id;
                }
            });

        }

        function deleteNewAcompanante(origen) {
            origen.parentNode.remove();
            $('#contador').text(parseInt($('#contador').text()) - 1);
            if ($('#contador').text() == '0') {
                $('#contador').hide();
            }
        }

        function deleteAcompanante(origen, id) {
            var temp = "<input type='hidden' name='acompanante_remove[]' value='" + id + "' />";
            $('#acompanante_new').append(temp);
            origen.parentNode.remove();
            $('#contador').text(parseInt($('#contador').text()) - 1);
            if ($('#contador').text() == '0') {
                $('#contador').hide();
            }
        }

        function isEmpty(element) {
            return !Boolean(element);
        }
    </script>
