<?php
$controller = $_SERVER['REQUEST_URI'];
$controller = strtolower($controller);
?>
<style>

    .btn-add{
        -moz-transform: none !important;
        -webkit-transform: none !important;
    }
    .stat-card{
        min-height: 180px;
    }
    .stat-size{
        height: auto;
        min-height: 180px !important;
    }
    .circulo{
        margin: auto;
    }
    a, dorado-2-text{
        color: #515055 !important;
    }

    @media only screen and (max-width: 321px) {
        .text-apperence{
            text-align: center !important;
            margin: 15px;
            padding: 0!important;
        }
        .menu-provider-item{
            width: 16.6% !important;
            font-size: 24px;
            color: #000;
        }
        .file-field{
            top: 30% !important;
            left: 20% !important;
        }
        .avatar-item{
            border: none!important;
        }
        .name-collection{
            height: auto!important;
        }
        .listainvitados{
            padding: 10px !important;
        }

        .listainvitados .section{
            text-align: center;
        }
        .modal-content{
            padding: 10px!important;
        }
        .btn-flat-acomp{
            padding: 0!important;
            font-size:9px!important; ;
        }
        #agregar_invitado h5,#update_invitado h6{
            text-align: center;
        }

    }
    @media only screen and (min-width: 322px) and (max-width: 426px) {
        .text-apperence{
            text-align: center !important;
            margin: 15px;
            padding: 0!important;
        }
        .menu-provider-item{
            width: 16.6% !important;
            font-size: 24px;
        }
        .avatar-item{
            border: none!important;
        }
        .name-collection{
            height: auto!important;
        }
        .listainvitados{
            padding: 10px !important;
        }

        .listainvitados .section{
            text-align: center;
        }
        .file-field{
            top: 35% !important;
            left: 25% !important;
        }
        .modal-content{
            padding: 15px 15px 0 15px!important;
        }

        .btn-flat-acomp{
            padding: 0!important;
            font-size:9px!important; ;
        }

        .btn-flat-acomp i{
            margin: 0;
        }

        .modal-content h5{
            text-align: center;
        }
        .listainvitados h5{
            margin-top: 0 !important;
            text-align: center!important;
        }
        .menu-modal{
            padding: 0!important;
            overflow: hidden;

        }
        #agregar_invitado h5,#update_invitado h6{
            text-align: center;
        }
    }

    @media only screen and (max-width: 768px) {
        .text-apperence{
            margin: 0px;
            padding: 0 15px!important;
        }
        .nav-bar{
            margin-bottom: 70px;
        }

    }

</style>
<div class="row nav-bar hide-on-small-only">
    <div class="col m5 text-apperence" style="border: 1px solid transparent; text-align: left;max-height: initial">
        Haz una lista previa de tus invitados, sep&aacute;ralos por familia y amigos para que sea m&aacute;s f&aacute;cil despu&eacute;s en el acomodo de mesas. Adem&aacute;s podr&aacute;s ver cuando confirmen su asistencia.
    </div>
    <a class="truncate col offset-m1 m1 s2 <?php echo ((substr_count($controller, 'importar') >= 1 ) || $controller == '/clubnupcial/novios/invitados' || $controller == '/clubnupcial/novios/invitados/home') ? 'active' : ''; ?>" href="<?php echo base_url() ?>novios/invitados/Home">Invitados</a>
    <a class="truncate col m1 s2 <?php echo (substr_count($controller, 'grupos') >= 1) ? 'active' : ''; ?>" href="<?php echo base_url() ?>novios/invitados/Home/grupos">Grupos</a>
    <a class="truncate col m1 s2 <?php echo (substr_count($controller, 'menus') >= 1) ? 'active' : ''; ?>" href="<?php echo base_url() ?>novios/invitados/Home/menus">Men&uacute;s</a>
    <a class="truncate col m1 s2 <?php echo (substr_count($controller, 'contactos') >= 1) ? 'active' : ''; ?>" href="<?php echo base_url() ?>novios/invitados/Home/contactos">Contactos</a>
    <a class="truncate col m1 s2 <?php echo (substr_count($controller, 'invitaciones') >= 1 || substr_count($controller, 'send') >= 1) ? 'active' : ''; ?>" href="<?php echo base_url() ?>novios/invitados/Home/invitaciones">Invitaciones</a>
    <a class="truncate col m1 s2 <?php echo (substr_count($controller, 'estadisticas') >= 1) ? 'active' : ''; ?>" href="<?php echo base_url() ?>novios/invitados/Home/estadisticas">Estad&iacute;sticas</a>
</div>

<div class="row menu-provider-esc hide-on-med-and-up">
    <a class="menu-provider-item <?php echo (substr_count($controller, 'importar') >= 1 ) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>novios/invitados/Home">
        <i class=" material-icons">person_add</i>
    </a>
    <a class="menu-provider-item <?php echo (substr_count($controller, 'grupos') >= 1) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>novios/invitados/Home/grupos">
        <i class=" material-icons">group</i>
    </a>
    <a class="menu-provider-item <?php echo (substr_count($controller, 'menus') >= 1) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>novios/invitados/Home/menus">
        <i class=" material-icons">list</i>
    </a>
    <a class="menu-provider-item <?php echo (substr_count($controller, 'contactos') >= 1) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>novios/invitados/Home/contactos">
        <i class=" material-icons">contacts</i>
    </a>
    <a class="menu-provider-item <?php echo (substr_count($controller, 'invitaciones') >= 1 || substr_count($controller, 'send') >= 1) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>novios/invitados/Home/invitaciones">
        <i class=" material-icons">photo_album</i>
    </a>
    <a class="menu-provider-item <?php echo (substr_count($controller, 'estadisticas') >= 1) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>novios/invitados/Home/estadisticas">
        <i class=" material-icons">insert_chart</i>
    </a>

</div>