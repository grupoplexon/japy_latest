<style>
    .modal-footer{
        background-color: transparent!important;
        text-align: center!important;
        height: auto!important;
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
    }
    .save,a.cancel{
        font-size: 1rem!important;
    }
    .save:hover, a.cancel:hover{
        font-size: 1.2rem!important;
    }
</style>
<div class="row modal-footer no-margin-element no-padding">
    <button id="save" type="submit" class="save col s8 waves-effect waves-green white-text btn-flat center-align btn-custom  ">GUARDAR</button>
    <a href="#!" class="cancel col s8 modal-action modal-close waves-effect waves-pink  primary-background
                     white-text btn-flat center-align no-margin-element">Cancelar</a>
</div>