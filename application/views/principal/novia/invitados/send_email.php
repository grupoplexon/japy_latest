<?php $this->view("principal/header") ?>
<?php $this->view("principal/novia/menu") ?>
<?php setlocale(LC_TIME, 'es_ES.UTF-8'); ?>
<div class="body-container">
    
    
    <?php $this->view("principal/novia/invitados/menu_principal") ?>
    
    <div style="border-top: 1px solid #eee; margin-top: -21px; margin-bottom: 20px; padding: 10 0 10 0;">
        <?php if (!isset($invitacion) || empty($invitacion)) { ?>
            <center style="background-color: #d5ece4!important; margin-top: -10px; margin-bottom: -10px;">
                <img style="width: 70%; height: auto;" src="<?php echo base_url() ?>dist/img/404.jpg" alt=""/>
            </center>
        <?php } else { ?>

            <form method="post" action="<?php echo base_url() ?>novios/invitados/Invitacion/sendEmail/<?php echo $id ?>">

                <div class="row">
                    <div class="col offset-s9 s3">
                        <button type="submit" class="waves-effect waves-green btn-flat dorado-2 white-text right"><small>Enviar invitaciones<i class="material-icons right">trending_flat</i></small></button>
                    </div>
                </div>

                <div class="row">



                    <div class="col s12 m6">
                        <ul class="collection with-header" style="overflow: visible;">
                            <li class="collection-header grey lighten-3">
                                <p style="margin: 0 0 5 0;">
                                    <input type="checkbox" class="filled-in" id="filled-in-box" />
                                    <label for="filled-in-box" style="width: 100%;">
                                        <input class="formulario-invitaciones" style="width: 100%;" type="text" id="search" placeholder="Buscar por nombre" />
                                    </label>
                                </p>
                            </li>
                        </ul>
                        <ul class="collection with-header" style="overflow: visible; height: 600px; overflow-y: scroll; margin-top: -18px;">
                            <?php foreach ($invitados as $key => $value) { ?>
                                <li class="collection-item listInvitaciones" data-id="<?php echo $value->id_invitado ?>">
                                    <p style="<?php echo empty($value->correo) ? 'display:none;' : ''; ?>">
                                        <input type="checkbox" class="filled-in" id="box-<?php echo $value->id_invitado ?>" onchange="checkboxCambia(this);" />
                                        <label for="box-<?php echo $value->id_invitado ?>"></label>
                                    </p>
                                    <i class="circle <?php echo generoImage($value->edad, $value->sexo) ?>"></i>
                                    <span class="title"><?php echo $value->nombre . ' ' . $value->apellido; ?></span>
                                    <?php if (empty($value->correo)) { ?>
                                        <a class="secondary-content listInvitaciones"><small>A&ntilde;ade un e-mail  <i class="material-icons right editButton" style="font-size: 22px;line-height: 2;">create</i></small></a>
                                    <?php } else { ?>
                                        <a class="secondary-content listInvitaciones"><small><?php echo $value->correo; ?><i class="material-icons right editButton" style="font-size: 22px;line-height: 2;">create</i></small></a>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>




                    <div class="col s12 m6 white">
                        <div id="asistente_confirm" style="height: auto;padding: 10px; max-width: 100%; overflow: hidden;"></div>
                        <div style='margin: 0.5rem 0 1rem 0;border: 1px solid #e0e0e0;border-radius: 2px;'>
                            <div style="position: relative;">
                                <img class="responsive-img" style="width: 100%" id="imagen" src="<?php echo isset($invitacion) ? "data:$invitacion->mime;base64," . base64_encode($invitacion->base) : 'https://placeholdit.imgix.net/~text?txtsize=35&txt=&w=600&h=300&txttrack=0' ?>" />
                            </div>
                            <div class="container">
                                <br>
                                <h5 class="center-align" id='novio_novia'><b><?php echo $invitacion->novia . ' & ' . $invitacion->novio; ?></b></h5>
                                <br>
                                <h6 class="center-align" id='fecha'><?php echo strftime("%A %d de %B del %Y ", strtotime($invitacion->fecha)); ?></h6>
                                <br>
                                <h6 class="center-align" id='lugar'><?php echo $invitacion->lugar; ?></h6>
                            </div>
                            <hr class="style12" style="margin: 50 30 50 30;"/>
                            <div class="container">
                                <h6 class="center-align" id='titulo'>
                                    <i class="material-icons left grey-text lighten-3">arrow_forward</i>
                                    <?php echo $invitacion->titulo ?>
                                    <i class="material-icons right grey-text lighten-3">arrow_back</i>
                                </h6>
                                <br>
                                <p id='descripcion'><?php echo $invitacion->descripcion; ?></p>
                                <br>
                                <br>
                            </div>
                            <div id="confirmacion" <?php echo ($invitacion->confirmacion == 1) ? '' : 'style="display:none;"'; ?>>
                                <hr class="style12" style="margin: 50 30 50 30;"/>
                                <div class="container center-align" >
                                    <p>Por favor,confirma tu asistencia aqu&iacute;</p>
                                    <button type="button" class="waves-effect waves-green btn-flat grey lighten-3 black-text">Confirmar</button>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>


            </form>
        <?php } ?>
    </div>
</div>
<script>
    function onReady() {
        $('#filled-in-box').on('change', function () {
            var invitados = $('.collection-item.listInvitaciones');
            var li = 0;
            if ($('#filled-in-box').prop('checked')) {
                for (var i = 0; i < invitados.length; i++) {
                    li = $($('.collection-item.listInvitaciones')[i]);
                    if (li.find('p').is(":visible") && $(li.find('input')[0]).prop('checked') == false) {
                        $(li.find('input')[0]).prop('checked', true);
                        $(li.find('input')[0]).change();
                    }
                }
            } else {
                for (var i = 0; i < invitados.length; i++) {
                    li = $($('.collection-item.listInvitaciones')[i]);
                    $(li.find('input')[0]).prop('checked', false);
                    $(li.find('input')[0]).change();
                }
            }
        });

        $('#search').on('keyup', function () {
            var valor = ($('#search').val()).toLowerCase();
            var invitados = $('.collection-item.listInvitaciones');
            var li = 0;
            for (var i = 0; i < invitados.length; i++) {
                li = $($('.collection-item.listInvitaciones')[i]);
                if ((li.find('.title').text().toLowerCase()).indexOf(valor) != -1) {
                    li.show();
                } else {
                    li.hide();
                }
            }
        });

        $('.editButton').on('click', function () {
            var element = this.parentNode.parentNode;
            var correo = '';
            if ($(element).find('small').text() != "Añade un e-mail  create") {
                correo = $(element).find('small').text();
                correo = correo.split('create')[0];
            }
            
            var template = '<input class="formulario-invitaciones" style="width: 170px;" type="email" placeholder="E-mail" value="' + correo + '"/>' +
                    '<a class="waves-effect waves-light btn-flat botn" onclick="updateCorreo(this)" type="button">' +
                    '<i class="material-icons">send</i>' +
                    '</a>';

            $(element).html(template);
        });
    }

    function checkboxCambia(esto) {
        var id = esto.getAttribute('id').split('box-')[1];
        if ($(esto).prop('checked')) {
            var temp = '<div class="etiqueta" id="chip-' + id + '"><small>' +
                    $(esto.parentNode.parentNode).find('.title').text() +
                    '</small><i class="material-icons" onclick="removerEtiqueta(this)">close</i>' +
                    '<input name="id_invitados[]" type="hidden" value="' + id + '" >' +
                    '</div>';
            $('#asistente_confirm').append(temp);
        } else {
            $('#chip-' + id).remove();
        }
    }

    function removerEtiqueta(esto) {
        esto = esto.parentNode;
        var id = esto.getAttribute('id').split('chip-')[1];
        $('#box-' + id).prop('checked', false);
        esto.remove();
    }

    function updateCorreo(esto) {
        esto = esto.parentElement;
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        var correo = $(esto).find('.formulario-invitaciones').val();
        var id = $(esto.parentElement).data('id');
        
        if (correo != '' && regex.test(correo)) {
            $.ajax({
                url: '<?php echo base_url() ?>novios/invitados/Home/updateInvitado',
                data: {
                    'id_invitado': id,
                    'tipo': 'correo',
                    'correo': correo,
                },
                method: "POST",
                timeout: 4000,
            }).done(function () {
                var template = '<small>' + correo + '<i class="material-icons right editButton" style="font-size: 22px;line-height: 2;">create</i></small>';
                $(esto).html(template);
                $(esto.parentElement).find('p').show();
                onReady();
            });
        }
        else{
            $(esto).find('.formulario-invitaciones').attr('class',$(esto).find('.formulario-invitaciones').attr('class') + ' invalid');
        }
    }
</script>
<?php $this->view("principal/footer") ?>