<?php
$controller = $_SERVER["REQUEST_URI"];
$controller = strtolower($controller);
?>
 <link href="<?php echo base_url() ?>dist/css/brideAdvisor/style_brideadvisor.css" rel="stylesheet" type="text/css"/>
<style>
    p {
        margin: 0;
        color: #F8DADF;
        font-size: .9rem;
    }
    img{
        width: 60px;
    }
    body {
         background: white !important;
    }
    .alinear {
          padding-top: 0 !important;
          padding-left: 0 !important;
          padding-right: 0 !important;
          padding-bottom: 0 !important;
    }
    .menu {
        top: 45px !important;
        left: -194.094px !important;
    }
    .logo-p{
            width:100% !important;
            margin-top:25px;
        }
    @media only screen and (min-width: 322px) and (max-width: 425px) {
        .menu {
            min-width: 267px !important;
        }
        .logo-p{
            width:200px !important;
            margin-top: 10px;
        }
    }
    @media only screen and (max-width: 992px){
        .hide-on-med-and-down {
            display: block !important;
        }
    }
</style>
<div class="row alinear">
<div class="col l12" style="background: #3a393b; width: 100%; padding: 0 !important;">
    <hr style="border: 5px solid; border-color: #F8DADF; margin:0 !important;">
    <div class="col l2 center-align">
        <a href="<?php echo base_url() ?>">
        <img class="logo-p" src="<?php echo base_url() ?>/dist/img/ba-rose.png" style=" ">
        </a>
    </div>
    <div class="hide-on-small-only">
          <div class="col l1 m1 center-align">
               <a href="<?php echo base_url() ?>novios/presupuesto">
               <img src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-presupuesto1.png">
               <p>Presupuesto inteligente</p>
               </a>
          </div>
          <div class="col l1 m1 center-align">
               <a href="<?php echo base_url() ?>Novia">
               <img src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-mi-perfil1.png">
               <p>Perfil</p>
               </a>
          </div>
          <div class="col l1 m1 center-align">
               <a href="<?php echo base_url() ?>novios/tarea">
               <img src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-tareas1.png">
               <p>Agenda</p>
               </a>
          </div>
          <div class="col l1 m1 center-align">
               <a href="<?php echo base_url() ?>novios/proveedor">
               <img src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-misproveedores1.png">
               <p>Mis proveedores</p>
               </a>
          </div>
          <div class="col l1 m1 center-align">
               <a href="<?php echo base_url() ?>novios/invitados">
               <img src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-listadeinvitados1.png">
               <p>Invitados</p>
               </a>
          </div>
          <div class="col l1 m1 center-align">
               <a href="<?php echo base_url() ?>novios/mesa">
               <img src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-controldemesas1.png">
               <p>Organizador de mesas</p>
               </a>
          </div>
          <div class="col l1 m1 center-align">
          <a href="<?php echo base_url() ?>novios/mesa_regalos/index">
              <img src="<?php echo base_url() ?>dist/img/iconos/iconos-color/mesa-regalos.png">
              <p>Mesa de regalos</p>
              </a>
          </div>
          <!--<div class="col l1 m1 center-align" style="border-left: 1px solid lightgray;">-->
          <!--     <a href="<?php echo base_url() ?>proveedores">-->
          <!--     <img src="<?php echo base_url() ?>dist/img/iconos/iconos-color/icono-proveedores1.png">-->
          <!--     <p>Proveedores</p>-->
          <!--     </a>-->
          <!--</div>-->
          <div class="col l1 offset-l1 center-align">
            <a href="<?php echo base_url() ?>brideweekend">
                <img style="width: 90px; padding-top: 25px;" src="<?php echo base_url() ?>dist/img/brideweekend/logo.png">
            </a>
          </div>
     </div>

    <div class="col l1 m2 s7 " style="padding-top: 2%; float:right;width: 110px;">
    <?php if ($this->checker->isLogin()) { ?>
            <div id="inicio_registro" class="" style="position: relative;z-index: 90;">
                <div>
                    <div class="row clickable dropdown-button waves-effect user-information center-vertical" data-beloworigin="true"
                         data-activates='dropdown-login' style="margin: 0;display: flex;">
                        <div class="col m5  s5  l5 " style="text-align: right;    width: 80px; margin-right: 20px;">
                            <img class="circule-img"
                                 src="<?php echo base_url('perfil/foto/') ?>/<?php echo $this->session->userdata("id_usuario") ?>"
                                 alt=""/>
                        </div>
                         <div class="col m7 s6 l5">
                            <h6 class="primary-text" style="font-weight: bold;margin: 0;">
                                <?php echo $this->session->userdata("nombre") ?>
                            </h6>
                            <p style="font-size: 12px;margin: 0;" class="primary-text"><?php echo $this->session->userdata("genero") ?></p>
                        </div> 
                        <i id="burger" class="material-icons primary-text "
                           style="position: absolute; top: 30%;display: block;right: 0px;">menu</i>
                        <!-- <div class="col m1 s1" style="position: relative">
                        </div> -->
                    </div>
                    <ul id='dropdown-login' class='dropdown-content menu'
                        style="z-index:30000000 !important;overflow-x: hidden;min-width: 305px;">
                        <?php if ($this->checker->isNovio()) { ?>
                            <li><a class="grey-text darken-5" href="<?php echo base_url('novia') ?>">
                                    <i class="material-icons left primary-text">dashboard</i> Mi organizador<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                            </li>
                            <li class=""
                                style="background: #514f50!important; color: white;    padding: 14px 16px;">
                                <div class="row" style="margin: 0px">
                                    <div class="col s4 align-center">
                                        <div class="align-center">
                                            <i class="material-icons center primary-text">check_circle</i>&nbsp;&nbsp;<b><?php echo $this->checker->getTareasCompletadas() ?></b>
                                        </div>
                                        <small><b>Tareas completadas</b></small>
                                    </div>
                                    <div class="col s4 align-center">
                                        <div class="align-center">
                                            <i class="material-icons center primary-text">people_outline</i>&nbsp;&nbsp;<b><?php echo $this->checker->getInvitadosConfirmados() ?></b>
                                        </div>
                                        <small><b>Invitados confirmados</b></small>
                                    </div>
                                    <div class="col s4 align-center">
                                        <div class="align-center">
                                            <i class="material-icons center primary-text">favorite_border</i>&nbsp;&nbsp;<b><?php echo $this->checker->getProveedoresReservados() ?></b>
                                        </div>
                                        <small><b>Proveedores reservados</b></small>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/buzon') ?>">
                                    <i class="material-icons left primary-text">mail_outline</i>
                                    Mi buz&oacute;n<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/presupuesto') ?>">
                                    <i class="material-icons left primary-text">exposure</i> Mi
                                    presupuesto<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('Novia') ?>">
                                    <i class="material-icons left primary-text">face</i>
                                    Mi perfil
                                    <i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/tarea') ?>">
                                    <i class="material-icons left primary-text">content_paste</i>
                                    Mi Agenda<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/proveedor') ?>">
                                    <i class="material-icons left primary-text"
                                    >class</i> Mis proveedores<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/invitados') ?>">
                                    <i class="material-icons left primary-text"
                                    >people_outline</i> Mis invitados<i
                                            class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li>
                                <a class="grey-text darken-5" href="<?php echo base_url('novios/perfil') ?>">
                                    <i class="material-icons left primary-text">settings</i> Mi
                                    cuenta<i class="material-icons grey-text lighten-4 right">chevron_right</i>
                                </a>
                            </li>
                            <li class="divider"></li>
                        <?php } else {
                            if ($this->checker->isAdmin()) { ?>
                                <li><a class="grey-text darken-5" href="<?php echo base_url('App') ?>">
                                        <i class="material-icons left" style="color:#f4d266!important">dashboard</i>
                                        Panel de administraci&oacute;n<i
                                                class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                </li>
                            <?php } else {
                                if ($this->checker->isProveedor()) { ?>
                                    <li><a class="grey-text darken-5" href="<?php echo base_url('proveedor') ?>">
                                            <i class="material-icons left"
                                               style="color:#f4d266!important">dashboard</i>
                                            Panel de Proveedor<i class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                    </li>
                                <?php } else {
                                    if ($this->checker->isModerador()) { ?>
                                        <li><a class="grey-text darken-5"
                                               href="<?php echo base_url('novios/moderador') ?>">
                                                <i class="material-icons left"
                                                   style="color:#f4d266!important">web</i> Comunidad<i
                                                        class="material-icons grey-text lighten-4 right">chevron_right</i></a>
                                        </li>
                                    <?php }
                                }
                            }
                        } ?>
                        <li>
                            <a class="grey-text darken-5" href="<?php echo base_url()."cuenta/logout" ?>">
                                <i class="material-icons left primary-text">close</i>
                                Cerrar Sesi&oacute;n
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        <?php } ?>
        <br>
    </div>
    <!-- <hr class="col l12 hide-on-small-only" style="border: 1px solid; border-color: #F8DADF"> -->
</div>
</div>