<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mesa de Regalos</title>
    
    <?php $this->view("principal/newheadernovia") ?>
    <?php $this->view("principal/novia/menu"); ?>
    <link href="<?php echo base_url() ?>dist/css/fontawesome-5.8.1/css/all.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/jRange-master/jquery.range.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/dropzone.css">
</head>
<style>
    .body{
        background-image: url('<?php echo base_url()?>dist/img/fondo.jpg'); 
        background-size: cover;
        font-family: 'Raleway', sans-serif !important;s
        color: #757575 !important;
        margin-top: -20px;
        padding-top: 20px;
    }
    h4, h5{
        color: #757575 !important;
    }
    .novios{
        border-radius: 10px;
        text-align: left;
        padding: 4% !important;
        margin-bottom: 8vh;
        background-image: url('<?php echo base_url()?>uploads/regalos/<?php echo($mesa->imagen_fondo ? $mesa->imagen_fondo : "background.png") ?>'); 
        background-size: cover;
    }
    .novios h4{
        font-size: 2rem;
    }
    .novios h5{
        font-size: 1.1rem;
    }
    hr{
        border: 2px solid #757575 !important;
    }
    .novios hr{
        width: 20%; 
        color: #757575;
        text-align: left;
        margin: 0;
        border: 2px solid #757575;
    }
    
    .tabs{
        border-top: 4px solid #757575;
        border-bottom: 2px solid #757575;
        height: 65px;
    }
    .tabs .tab .pestana{
        color:#757575 !important;
        padding-top: 5px;
        font-size: 1.2rem;
    }
    .tabs .tab a.active{
        font-weight: bold !important;
        padding-top: px;
        border-top: 10px solid #f8dadf;
        background: transparent !important;
    }
    .card-regalo p{
        color: #757575 !important;
        font-size:1.1rem !important;
        text-align: left;
    }
    #upload{
        border: 1px solid #757575 !important;
    }
    input, textarea{
        border: 1px solid #757575 !important;
        color: black !important;
        border-radius: 8px !important;
        padding-left: 10px !important;
    }
    .card-regalo{
        padding-bottom: 10px !important;
    }
    .card-regalo {
        padding-bottom: 4vh;
    }
    #regalo, #articulo{
        margin-left: 5%;
        margin-right: 5%;
    }
    select{
        display: block !important;
        border: 1px solid #757575 !important;
         color: black !important;
        border-radius: 8px !important;
    }
    #regalo .btn{
        color: #757575 !important;
        font-size: 1.4rem;
        width: 160px;
        height: 40px;
        border-radius: 10px;
        background: #f8dadf !important;
    }
    #regalo .btn:hover, #regalo .btn:focus{
        background-color: #f8dadf !important;
    }
    .btn{
        color: #757575 !important;
        background-color: #f8dadf !important;
    }
    #upload{
        height: 440px;
        background-color: white;
        /* padding-top: 30%; */
        color: #757575;
    }
    #articulo h4{
        text-align: left;
        font-size: 1.2rem;
    }
    [type="checkbox"]:checked+span:not(.lever):before {
        border-right: 2px solid #f5aeb9;
        border-bottom: 2px solid #f5aeb9;
    }
    strong{
        font-size: 1.1rem;
        font-weight: 700;
    }
    #articulos h5{
        font-size: 2rem;
        font-weight: 500;
    }
    #articulos hr{
        border: 1px solid #757575 !important;
        width: 70%;
    }
    #articulos h4{
        font-size: 1.2rem;
        text-align: center;
    }
    #articulos img{
        width: 100%;
        height: 250px;
        object-fit: cover;
    }
    #articulos .card{
        height: 400px;
    }
    .etiqueta h3{
        font-size: 1.1rem;
        font-weight: bold;
        margin: 0;
    }
    .etiqueta{
        background: #f8dadf;
        color: white;
        position: absolute;
        margin: 0;
        margin-left:10px;
        border-radius: 0px;
        padding: 0 2px !important;
        height: 40px;
    }
    a form {
        visibility: hidden;
    }
    
    a:hover form {
        visibility: visible;
    }
    .p-image:hover{
        background: rgba(258, 218, 223, 0.7);
    }
    .body a{
        color: #757575 !important;
        
    } 
    .editar{
        background: transparent !important;
    }
    .slider-container{
        width: 100% !important;
    }
    .selected-bar{
        background-image: linear-gradient(to bottom, #757575, #757575) !important;
    }
    .slider-container .back-bar .pointer-label{
        font-size: 9px !important;
    }
    .theme-green .back-bar .pointer{
        border: 1px solid #757575 !important;
        background-image: linear-gradient(to bottom, #757575, #757575) !important;
        width: 11px !important;
        height: 11px !important;
        top: -3px !important;
    }
    .dz-image img{
        height: -webkit-fill-available !important;
        object-fit: cover !important;
    }
</style>
<div class="row body" >
    <a class="p-image">
    <div class="col l10 offset-l1 s10 offset-s1 novios card">
            <div class="col l6">
                <h5>Nuestra mesa de regalos</h5>
                <h4>
                    <b>
                    <?php if ($novios) { ?>
                        <?php echo($novios[0] ? $novios[0]->nombre : "") ?>
                        <?php echo(key_exists(1, $novios) ? " & ".$novios[1]->nombre : "") ?>
                    <?php } ?>
                    </b>
                </h4>
                <hr>
                <h5> <i class="far fa-calendar-alt"></i>
                    <?php if ($boda->fecha_boda !== $boda->fecha_creacion) {
                        echo strftime("%d | %B | %Y", strtotime($boda->fecha_boda)); ?>
                    <?php } ?>
                </h5>
            </div>
            <form action="/file-upload" class="dropzone right" id="portada" style="width: 20%;">
                <div class="fallback">
                <div class="dz-message" data-dz-message><span> Selecciona una nueva imagen de portada</span></div>
                    <input name="file" type="file" multiple /> 
                </div>
            </form>
    </div>
    </a>
    <div class="row center-align">
        <br><br>
        <div class="row center-align pestanas">
            <ul class="tabs center-align" id="tabs">
                <li class="tab col s12 l4"> <a class="pestana" href="#articulo">ARTICULOS SELECCIONADOS</a></li>
                <li class="tab col s12 l4"> <a class="pestana"  href="#viaje">VIAJE</a></li>
                <?php if(isset($update)) {?>
                    <li class="tab tabs-regalo col s12 l4"> <a class="pestana active"  href="#regalo">EDITAR REGALO</a></li>
                <?php } else{ ?> 
                    <li class="tab tabs-regalo col s12 l4"> <a class="pestana" href="#regalo">CREAR REGALO</a></li>
                <?php } ?> 
            </ul>
        </div>
        <div id="articulo" class="row">
            <div class="col l3 m3 card">
                <h5> AGREGAR FILTRO </h5>
                <hr>
                <h4>Buscar regalo</h4>
                <input id="search" type="text"  value="<?php if(!empty($_GET["search"])) {echo $_GET["search"];} ?>">
                <input type="hidden" id="url" value="<?php if(!empty($_GET["search"])) {echo $_GET["search"];} ?>">
                <br>
                <h4>Rango de precios</h4>
                <br>
                <input type="hidden" id="rango" class="slider-input" value="1500,6000" style="width: 100% !important;" />
                <br>
                <h4>Categoria</h4>
                <form action="#">
                    <p> <label>
                        <input type="checkbox" name="cocina" value="cocina" class="checkbox" />    
                        <span>Cocina</span>
                    </label> </p>
                    <p> <label>
                        <input type="checkbox" name="decoracion" value="decoracion" class="checkbox" />
                        <span>Decoración</span>
                    </label> </p>
                    <p> <label>
                        <input type="checkbox" name="muebles" value="muebles" class="checkbox" />
                        <span>Muebles</span>
                    </label> </p>
                    <p> <label>
                        <input type="checkbox" name="electrodomesticos" value="electrodomesticos" class="checkbox" />
                        <span>Electrodomésticos</span>
                    </label> </p>
                    <p> <label>
                        <input type="checkbox" name="habitacion" value="habitacion" class="checkbox" />
                        <span>Habitación</span>
                    </label> </p>
                    <p> <label>
                        <input type="checkbox" name="tecnologia" value="tecnologia" class="checkbox" />
                        <span>Tecnología</span>
                    </label> </p>
                    <p> <label>
                        <input type="checkbox" name="deportes" value="deportes" class="checkbox" />
                        <span>Deportes</span>
                    </label> </p>
                    <p> <label>
                        <input type="checkbox" name="jardin" value="jardin" class="checkbox" />
                        <span>Jardín</span>
                    </label> </p>
                    <p> <label>
                        <input type="checkbox" name="varios" value="varios" class="checkbox" />
                        <span>Varios</span>
                    </label> </p>
                </form>
                <!-- <br> -->
                <h4>Orden</h4>
                <select name="" id="orden">
                    <option value="asc">Ascendente</option>
                    <option value="desc">Descendente</option>
                </select>
                <br>
                <button class="btn" type="submit" id="buscar">Buscar</button>
                <br><br>
            </div>
            <div class="col l9 m9 center-align" id="articulos">
                <?php if(count($regalos)!=0) { ?>
                    <?php foreach($regalos as $r) { ?>
                            <div class="col l4 s12 ">
                                <div class="card">
                                    <div class="etiqueta valign-wrapper"><h3><?= $r->disponible ?>/1</h3></div>
                                    <img src="<?php echo base_url() ?>/uploads/regalos/<?php echo $r->imagen ?>" alt="">
                                    <hr>
                                    <h5>$<?= $r->precio; ?><strong>MXN</strong></h5>
                                    <h4><?= $r->nombre_producto; ?></h4>
                                    <a href="#" class="right" style="padding-right: 10%;" onclick="deleteGift(<?php echo $r->id_regalo ?>);">Eliminar</a>
                                    <a class="left" style="padding-left: 10%; cursor:pointer;"  onclick="updateGift(<?php echo $r->id_regalo ?>);">Editar</a>
                                </div>
                            </div>
                    <?php } ?>
                <?php }else{ ?>
                    <br><br><br>
                    <i class="large material-icons" style="font-size: 8rem;">call_made</i> <br>
                    <!-- <img style="width: 10%;height:10% !important; object-fit:contain;" src="<?php echo base_url() ?>/dist/img/flecha.png" alt=""><br> -->
                    <h3 style="color: #757575;"> ¡Aun no tienes regalos guardados, comienza a agregar a tu lista!</h3>
                    <h5 style="font-size: 1.1rem;">Dale clic a CREAR REGALO para comenzar.</h5>
                <?php } ?>
            </div>
        </div>
        <div id="viaje" class="row">
            <h5> VIAJE</h5>
        </div>
        <div id="regalo" class="row">
            <div class="col l6 s12 card-fotos">
                <div class="upload " id="upload" >
                    <text class="text-upload vertical-align center-align" div="drop">
                        <i class="fa fa-upload fa-2x valign " style="margin-top:30%"></i><br><br>
                        Suelte los archivos o haga click en el recuadro para cargar.
                    </text>
                </div>
                <?php if(isset($regalo)){ ?>
                    <input name="file" type="hidden" class="archivo"
                        value="<?php echo base_url() ?>uploads/regalos/<?php echo $regalo->imagen ?>">
                    <button type="button" class="btn dorado-2 eliminar-foto"style="width: 50%"
                            data-promocion="<?php echo $regalo->id_regalo ?>">
                        Eliminar Foto
                    </button>
                <?php }else{ ?>
                    <input name="file" type="hidden" class="archivo" value="">
                    <button type="button" class="btn dorado-2 eliminar-foto"style="width: 50%"
                            data-promocion="nuevo">
                        Eliminar Foto
                    </button>
                <?php } ?>
            </div>
            <div class="col l6 s12 card card-regalo">
                <br>
                <?php if(isset($update)) { ?>
                    <input type="text" value="update" id="tipo" class="hide">
                    <input type="text" value="<?= $regalo->id_regalo ?>" id="idp" class="hide">
                <?php } else { ?>
                    <input type="text" value="save" id="tipo" class="hide">
                <?php } ?>
                <div class="input-field col s12 l10">
                    <p for="producto">Nombre del Producto</p>
                    <input placeholder="" id="producto" value="<?php if(isset($regalo)){ echo($regalo->nombre_producto ? $regalo->nombre_producto : ""); } ?>" type="text" class="validate" value="" >
                </div>
                <br><br>
                <div class="input-field col s12 l10">
                    <p for="precio">Precio (peso mexicano)</p>
                    <input placeholder="" id="precio" value="<?php if(isset($regalo)){ echo($regalo->precio ? $regalo->precio : ""); } ?>" type="text" class="validate">
                </div>
                <div class="input-field col s12 l10"> 
                    <p>Descripción</p>
                    <textarea class="materialize-textarea validate" id="descripcion" type="text"><?php if(isset($regalo)){ echo($regalo->descripcion ? $regalo->descripcion : ""); } ?></textarea>
                </div>

                <div class="col l6 s12">
                    <p>Categoria:</p>
                    <select name="" id="categoria">
                        <option value="0">Selecciona una categoria</option>
                        <?php if(isset($regalo)){ ?>
                            <option value="<?= $regalo->categoria ?>" selected><?= $regalo->categoria ?></option>
                        <?php  } ?>
                        <option value="cocina">Cocina</option>
                        <option value="decoracion">Decoración</option>
                        <option value="muebles">Muebles</option>
                        <option value="electrodomesticos">Electrodomésticos</option>
                        <option value="habitacion">Habitación</option>
                        <option value="tecnologia">Tecnología</option>
                        <option value="deportes">Deportes</option>
                        <option value="jardin">Jardín</option>
                        <option value="varios">Varios</option>
                    </select>
                </div>
                <br><br>
            </div>
            <div class="col l12">
            <br> <br>
                <button class="btn" id="regresar" onclick="regresar();"> Regresar </button>
                <button class="btn"  type="submit" id="guardar"> Guardar </button>
            </div>
        </div>
    </div>
</div>
<?php $this->view("japy/prueba/footer"); ?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url() ?>dist/js/droxxxpzone.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/jRange-master/jquery.range.js"></script>
<script>
    var indicator = 0;
    var searchProvider = 0, page=0, visible = 0, charge = 0;
    var busqueda = '';
    var tipo = '';
    $(document).ready(function(){
        
        $(".upload").each(function (i, elem) {
            var dropzone = new Dropzone(elem, {
            url: '<?php echo base_url() ?>novios/Mesa_regalos/saveRegalo',
            method: 'POST',
            paramName: 'files', // The name that will be used to transfer the file
            maxFilesize: 6, // MB
            maxFiles: 1,
            thumbnailWidth: 500,
            thumbnailHeight: 500,
            uploadMultiple: false,
            createImageThumbnails: true,
            acceptedFiles: 'image/*',
            autoProcessQueue: false,
            dataType: 'json',
            accept: function(file, done) {
                $(elem).find("text").hide();
                done();
                if(file){
                    $('.dz-success-mark').hide();
                    $('.dz-error-mark').hide();
                }
                indicator = 1;
                console.log("Entro");
            },
            success:function (file) {
                
            }, 
            error: function(data, xhr) {
                swal('Error', 'Sucedio un error, recargue la página', 'warning');
            },
            init: function() {
                this.on("sending", function(file, xhr, formData) {
                    formData.append("precio", $('#precio').val());
                    formData.append("categoria", $('#categoria').val());
                    formData.append("producto", $('#producto').val());
                    formData.append("descripcion", $('#descripcion').val());
                    formData.append("tipo", $('#tipo').val());
                    if($('#tipo').val()=="update"){
                        formData.append("id", $('#idp').val());
                    }
                });
                this.on('success', function(file, response) {
                    swal('Correcto', 'Los datos fueron guardados', 'success');
                    setTimeout(function() {
                        location.href ="<?php echo base_url() ?>novios/mesa_regalos/index";
                    }, 300);
                });
            },
            });
                var file = $(".archivo").val();
                if (file != null && file != "") {
                    var mockFile = {name: "Filename", size: 12345};
                    dropzone.emit("addedfile", mockFile);
                    dropzone.emit("thumbnail", mockFile, file);
                    dropzone.emit("complete", mockFile);
                    $('.dz-success-mark').hide();
                    $('.dz-error-mark').hide();
                    $('.dz-details').hide();
                    $(elem).find("text").hide();
                }
        });

        $(".eliminar-foto").on("click", function (elm) {
            console.log("entro eliminar");
            var $prom = $("#upload");
            $prom.find("div.dz-preview").remove();
            $prom.find("text").show();
            $prom.find(".archivo").val("");
        });

        $('.tabs').tabs();

        $('.slider-input').jRange({
            from: 100,
            to: 10000,
            step: 100,
            scale: [100,10000],
            format: '%s',
            width: 300,
            showLabels: true,
            isRange : true
        });

        // $(".checkbox").click(function () {
        //     // let valoresCheck = [];
        //     // $("input[type=checkbox]:checked").each(function(){
        //     //     valoresCheck.push(this.value);
        //     // });
        //     // valoresCheck = JSON.stringify(valoresCheck);
        //     // alert(this.value);
        //     document.location.href = "<?php echo base_url() ?>novios/mesa_regalos/regalo?categoria="+this.value;
        //     // window.open('<?php echo base_url() ?>admin/Promotion/createPDF?id_promotion='+valoresCheck,'_blank');
        // });


        $("#buscar").on( "click", function() {
            let valoresCheck = [];
            $("input[type=checkbox]:checked").each(function(){
                valoresCheck.push(this.value);
            });
            valoresCheck = JSON.stringify(valoresCheck);

            var precio = $("#rango").val();

            var orden = $("#orden").val();

            alert(precio);

            document.location.href = "<?php echo base_url() ?>novios/mesa_regalos/regalo?categoria="+valoresCheck+"&precio="+precio+"&orden="+orden;
        });
    
        $("#orden").change( function(){
            alert(this.value);
        });

        if($('#url').val()!='') {
            busqueda = $('#url').val();
            search($('#url').val());
        }
        $( "#search" ).autocomplete({
            source: '<?php echo base_url(); ?>novios/mesa_regalos/autocomplete',
            select: (event, ui) => {
                busqueda = ui.item.value;
                page = 0;
                visible = 0;
                searchProvider = 0;
                search(busqueda);
            }
        });
        $( "#guardar" ).on( "click", function() {
            if($('#descripcion').val()!='0' && $('#precio').val()!='' && $('#categoria').val()!='0' &&
                $('#producto').val()!='' ) {

                    if(indicator == 1 ){
                        var myDropzone = Dropzone.forElement(".upload");
                        myDropzone.processQueue();
                    }else{
                        save();
                    }
            } else {
                console.log(indicator);
                swal('Error', 'Por favor complete todos los campos', 'warning');
            }
        });
        $('#portada').dropzone({
            dictDefaultMessage: "Selecciona una nueva imagen de portada",
            url: '<?php echo base_url() ?>novios/Mesa_regalos/imagenPortada',
            method: 'POST',
            paramName: 'files', // The name that will be used to transfer the file
            maxFilesize: 6, // MB
            maxFiles: 1,
            uploadMultiple: false,
            createImageThumbnails: true,
            acceptedFiles: 'image/*',
            dataType: 'json',
            success:function (file) {
                setTimeout(function() {
                    location.href ="<?php echo base_url() ?>novios/mesa_regalos/index";
                }, 300);
            },
        });
        
    });
    function save() 
    {
        if($('#tipo').val()=="update"){
            $.ajax({
                url: '<?php echo base_url() ?>novios/Mesa_regalos/updateRegalo',
                data: {
                    descripcion: $('#descripcion').val(),
                    precio: $('#precio').val(),
                    categoria: $('#categoria').val(),
                    producto: $('#producto').val(),
                    tipo: $('#tipo').val(),
                    id: $('#idp').val(),
                },
                method: "POST",
                success: function (response) {
                    swal('Correcto', 'Los datos fueron guardados', 'success');
                    setTimeout(function() {
                        location.href ="<?php echo base_url() ?>novios/mesa_regalos/index";
                    }, 300);
                },
                error: function (error) {
                    swal('Error', 'Ocurrió un problema, intente de nuevo', 'warning');
                }
            });
        }else{
            swal('Error', 'Por favor complete todos los campos', 'warning');
        }
    };
    function search(busqueda) 
    {
        document.location.href = "<?php echo base_url() ?>novios/mesa_regalos/regalos?nombre="+busqueda;
    };
    function regresar()
    {
        document.location.href = "<?php echo base_url() ?>novios/mesa_regalos/index";
    };

    function deleteGift(id)
    {
        swal({
            title: '¿Estas seguro que deseas eliminar este regalo?',
            icon: 'error',
            buttons: ['Cancelar', true],
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                url: "<?php echo base_url() ?>novios/mesa_regalos/deleteRegalo",
                method: "POST",
                data: {
                    id_regalo: id,
                },
                success: function(response) {
                    console.log("SUCCES");
                    swal('Deleted', 'El regalo fue eliminado de tu lista', 'success');
                    location.reload();
                },
                error: function(e) {
                    console.log("ERROR");
                },
            });
            }
        });
    };
    function updateGift(id)
    {
        document.location.href  ="<?php echo base_url() ?>novios/mesa_regalos/editRegalo?regalo="+id;
    };
    
    
</script>
</html>
