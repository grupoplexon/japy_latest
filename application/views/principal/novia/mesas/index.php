<?php $this->view("principal/newheadernovia") ?>
<?php $this->view("principal/novia/menu"); ?>
<header style="margin-bottom: 20px;">
    <link href="<?php echo base_url() ?>dist/css/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>dist/css/tool-mesas.css" rel="stylesheet" type="text/css"/>
</header>
<style>
    @media only screen and (max-width: 425px) {
        .expand-container{
           width: 100% !important;
        }
        .mesas{
            margin-top: 45px;
        }

    }
    @media only screen and (max-width: 768px) and (min-height: 700px) {
        .mesas{

        }

    }
    .background {
        background-size: unset !important;
    }
</style>
<div class="row gray lighten-8 mesas" style="border-top: 1px solid #d9d9d9; margin-top: 0px; padding: 10px 0 10px 0;">
    <div class="col expand-container" style=" width: 300px;border-right: 1px solid #d9d9d9; margin: -11px 0px 0px 0px; padding: 0px;">
        <ul class="collection" style="margin: 0px; padding: 0px; overflow: inherit;">
            <li class="collection-item gray lighten-6"><b>A&ntilde;adir mesa</b></li>
            <li class="collection-item">
                <div class="row" style="height: 50px;">
                    <div class="col s4 m4">
                        <img src="<?php echo base_url() ?>/dist/img/mesas/mesa-01.png" class="responsive-img btn-mesa"
                             data-tipo="1" data-silla="8">
                    </div>
                    <div class="col s4 m4">
                        <img src="<?php echo base_url() ?>/dist/img/mesas/mesa-02.png" class="responsive-img btn-mesa"
                             data-tipo="2" data-silla="6">
                    </div>
                    <div class="col s4 m4">
                        <img src="<?php echo base_url() ?>/dist/img/mesas/mesa-04.png" class="responsive-img btn-mesa"
                             data-tipo="3" data-silla="3">
                    </div>
                </div>
            </li>
            <li class="collection-item gray lighten-6 ">
                <div>
                    <b>Invitados</b>
                    <a class="secondary-content black-text clickable" id="buscar"><i
                                class="material-icons">search</i></a>
                </div>
            </li>
            <li class="collection-item" id="li_filtrar" style="display:none;">
                <div class="row no-padding no-margin">
                    <div class="col s10 no-padding no-margin">
                        <input class="formulario" id="input-buscar" onkeyup="cambio_filtro()" type="text"
                               style="width: 100%" placeholder="Buscar por nombre...">
                    </div>
                    <div class="col s2" style="padding: 0px 0px 0px 7px;">
                        <button class="waves-effect waves-light btn-flat dorado-2 white-text"
                                style="width: 100%; padding: 0px;" type="button"><i class="material-icons">search</i>
                        </button>
                    </div>
                </div>
                <select class="formulario changeimage" id="formulario_id_grupo" onchange="cambio_filtro()">
                    <option value="null" selected> - - Grupo - -</option>
                    <?php foreach ($grupos as $k => $gp) { ?>
                        <option value="<?php echo $gp->id_grupo ?>"><?php echo $gp->grupo ?></option>
                    <?php } ?>
                </select>
                <p>
                    <input type="checkbox" class="filled-in" id="checkbox_change" onchange="cambio_filtro()"/>
                    <label for="checkbox_change" style="font-size: 13px;">S&oacute;lo invitados confirmados</label>
                </p>
            </li>
            <li class="collection-item">
                <ul class="collection with-header" style="overflow: visible; height: 500px; overflow-y: scroll;">
                    <?php foreach ($invitados as $key => $value) { ?>
                        <?php if(empty($value->id_mesa)) { ?>
                            <li class="collection-item list valign-wrapper" style="display: block" id="<?php echo $value->id_invitado.'Visibles'; ?>">
                                <div class="invitado-wrapper invitado" id="<?php echo $value->id_invitado ?>"
                                    data-invitado="<?php echo $value->id_invitado ?>"
                                    data-confirmado="<?php echo $value->confirmado ?>"
                                    data-grupo="<?php echo $value->id_grupo ?>"
                                    data-nombre="<?php echo strtolower($value->nombre . ' ' . $value->apellido); ?>"
                                    style="cursor: pointer">
                                    <div class=" x2 <?php echo generoImage($value->edad, $value->sexo) ?> "></div>
                                </div>
                                <span class="title valign "><?php echo $value->nombre . ' ' . $value->apellido; ?></span>
                            </li>
                        <?php } else { ?>
                            <li class="collection-item list valign-wrapper" style="display: none" id="<?php echo $value->id_invitado.'Ocultos'; ?>">
                                <div class="invitado-wrapper invitado" id="<?php echo $value->id_invitado ?>"
                                    data-invitado="<?php echo $value->id_invitado ?>"
                                    data-confirmado="<?php echo $value->confirmado ?>"
                                    data-grupo="<?php echo $value->id_grupo ?>"
                                    data-nombre="<?php echo strtolower($value->nombre . ' ' . $value->apellido); ?>"
                                    style="cursor: pointer">
                                    <div class=" x2 <?php echo generoImage($value->edad, $value->sexo) ?> "></div>
                                </div>
                                <span class="title valign "><?php echo $value->nombre . ' ' . $value->apellido; ?></span>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </li>
        </ul>
    </div>
    <div class="col panel-mesas">
        <div class="card" style="width: 100%;height: 45rem">
            <div class="card-content">
                <div class="background">

                </div>
            </div>
        </div>
        <br/>
    </div>
</div>


<div id="agregar_invitado" class="modal">
    <form id="form-add-update" method="post"
          action="<?php echo base_url() ?>index.php/novios/invitados/Home/add_invitado">
        <div class="modal-content">
            <h6 class="dorado-2-text">Invitado</h6>
            <div class="divider"></div>
            <div class="row">


                <div class="col s12" style="margin-bottom: 20px;">
                    <ul class="tabs">
                        <li class="tab col s4" id='d_p'><a id="addInvitado1" class="active" href="#datos_personales">Datos
                                personales</a></li>
                        <li class="tab col s4" id='d_c'><a id="addInvitado2" href="#datos_contacto">Datos de
                                contacto</a></li>
                        <li class="tab col s4" id='d_a'>
                            <a id="addInvitado3" href="#acompanante">Acompa&ntilde;antes</a>
                            <span id="contador" class="contador dorado-2" style="display: none;">0</span>
                        </li>
                    </ul>
                </div>


                <div id="datos_personales" class="col s12">
                    <div class="row">
                        <div class="col s3">
                            <div class="img-responsive">
                                <i class="invitados invitados-Hombre" id="icon_persona_modal"></i>
                            </div>
                        </div>
                        <div class="col s9">
                            <div class="row">
                                <input id="id_invitado" name="id_invitado" type="hidden">
                                <input name="nombre" class="formulario col s6" type="text" placeholder="Nombre(s)*"
                                       required>
                                <input name="apellido" class="formulario col s6" type="text" placeholder="Apellido(s)"
                                       required>
                                <!-- DIVIDER -->
                                <select name="grupo" class="formulario col s6" required>
                                    <option value="" disabled selected>Grupo*</option>
                                    <?php foreach ($grupos as $key => $value) { ?>
                                        <option value="<?php echo $value->id_grupo ?>" <?php echo ($value->grupo == 'Novios') ? 'disabled' : ''; ?> ><?php echo ($value->grupo == 'Novios') ? $value->grupo : str_replace($this->session->userdata('genero'), $this->session->userdata('nombre'), $value->grupo); ?></option>
                                    <?php } ?>
                                </select>
                                <select name="menu" class="formulario col s6" required>
                                    <option value="" disabled selected>Men&uacute;*</option>
                                    <?php foreach ($menus as $key => $value) { ?>
                                        <option value="<?php echo $value->id_menu ?>"><?php echo $value->nombre ?></option>
                                    <?php } ?>
                                </select>
                                <!-- DIVIDER -->
                                <select name="sexo" class="formulario col s6" required>
                                    <option value="" disabled selected>Sexo*</option>
                                    <option value="1">Hombre</option>
                                    <option value="2">Mujer</option>
                                </select>
                                <select name="edad" class="formulario col s6" required>
                                    <option value="" disabled selected>Edad*</option>
                                    <option value="1">Adulto</option>
                                    <option value="2">Ni&ntilde;o</option>
                                    <option value="3">Beb&eacute;</option>
                                </select>
                                <!-- DIVIDER -->
                                <button class="waves-effect waves-light btn-flat col s12 dorado-2-text left-align"
                                        type="button" onclick="$('#addInvitado2').click();">
                                    <small><i class="material-icons left">add</i>A&ntilde;adir datos de contacto</small>
                                </button>
                                <button class="waves-effect waves-light btn-flat col s12 dorado-2-text left-align"
                                        type="button" onclick="$('#addInvitado3').click();">
                                    <small><i class="material-icons left">add</i>A&ntilde;adir acompa&ntilde;ante
                                    </small>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="datos_contacto" class="col s12">
                    <div class="row">
                        <input class="formulario col s12" type="email" name="correo" placeholder="E-mail">
                        <!-- DIVIDER -->
                        <input class="formulario col s6" type="tel" name="telefono" placeholder="Tel&eacute;fono">
                        <input class="formulario col s6" type="tel" name="celular" placeholder="Celular">
                        <!-- DIVIDER -->
                        <h6><b>Direcci&oacute;n postal</b></h6>
                        <select name="country" class="col s6 browser-default countries" id="countryId">
                            <option value="" disabled selected>-- Pa&iacute;s --</option>
                        </select>
                        <select name="state" class="col s6 browser-default states" id="stateId">
                            <option value="" disabled selected>-- Estado --</option>
                        </select>
                        <!-- DIVIDER -->
                        <select name="city" id="cityId" class="col offset-s6 s6 browser-default cities">
                            <option value="" disabled selected>-- Poblaci&oacute;n --</option>
                        </select>
                        <!-- DIVIDER -->
                        <input class="formulario col s6" type="text" name="direccion" placeholder="Direcci&oacute;n">
                        <input class="formulario col s6" type="text" name="cp" placeholder="C&oacute;digo Postal">
                    </div>
                </div>


                <div id="acompanante" class="col s12">
                    <div class="row">
                        <div class="col s12" style="margin-bottom: 20px;">
                            <ul class="tabs">
                                <li class="tab col s4">
                                    <a class="active btn-flat" href="#anadir_nuevo">
                                        <i class="material-icons left dorado-2-text">add_circle</i>
                                        A&ntilde;adir nuevo
                                    </a>
                                </li>
                                <li class="tab col s4">
                                    <a class="btn-flat" href="#contacto_existente">
                                        <i class="material-icons left dorado-2-text">view_list</i>
                                        Contacto existente
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="anadir_nuevo" class="col s12">
                            <div class="row">
                                <input id="nombre_acompanante" class="formulario col s6" type="text"
                                       placeholder="Nombre(s)*">
                                <input id="apellido_acompanante" class="formulario col s6" type="text"
                                       placeholder="Apellido(s)*">
                                <!-- DIVIDER -->
                                <select id="grupo_acompanante" class="formulario col s6">
                                    <option value="" disabled selected>Grupo*</option>
                                    <?php foreach ($grupos as $key => $value) { ?>
                                        <option value="<?php echo $value->id_grupo ?>" <?php echo ($value->grupo == 'Novios') ? 'disabled' : ''; ?> ><?php echo ($value->grupo == 'Novios') ? $value->grupo : str_replace($this->session->userdata('genero'), $this->session->userdata('nombre'), $value->grupo); ?></option>
                                    <?php } ?>
                                </select>
                                <select id="menu_acompanante" class="formulario col s6">
                                    <option value="" disabled>Men&uacute;</option>
                                    <?php foreach ($menus as $key => $value) { ?>
                                        <option value="<?php echo $value->id_menu ?>"
                                                selected><?php echo $value->nombre ?></option>
                                    <?php } ?>
                                </select>
                                <!-- DIVIDER -->
                                <select id="sexo_acompanante" class="formulario col s6">
                                    <option value="" disabled>Sexo*</option>
                                    <option value="1" selected>Hombre</option>
                                    <option value="2">Mujer</option>
                                </select>
                                <select id="edad_acompanante" class="formulario col s6">
                                    <option value="" disabled>Edad*</option>
                                    <option value="1" selected>Adulto</option>
                                    <option value="2">Ni&ntilde;o</option>
                                    <option value="3">Beb&eacute;</option>
                                </select>
                                <!-- DIVIDER -->
                                <button class="btn invitados left-align grey darken-5" id="anadir_invitado_form"
                                        type="button">
                                    A&Ntilde;ADIR
                                </button>
                            </div>
                        </div>
                        <div id="contacto_existente" class="col s12">
                            <select id="id_acompanante" class="formulario col s6">
                                <?php
                                foreach ($invitados as $key => $inv) {
                                    if ($inv->grupo != 'Novios') {
                                        ?>
                                        <option value="<?php echo $inv->id_invitado ?>"
                                                data-sexo="<?php echo $inv->sexo ?>"
                                                data-edad="<?php echo $inv->edad ?>"><?php echo $inv->nombre . ' ' . $inv->apellido ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <button class="btn invitados left-align grey darken-5" id="anadir_invitado" type="button">
                                A&Ntilde;ADIR
                            </button>
                        </div>
                    </div>
                    <div id="acompanante_new">

                    </div>
                </div>


            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="waves-effect waves-green btn-flat dorado-2 white-text">Guardar</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-pink btn-flat ">Cancelar</a>
        </div>
    </form>
</div>

<?php $this->view("japy/prueba/footer"); ?>
<script src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/Mesas.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        mesas = new Mesas({server: '<?php echo base_url() ?>'});
        mesas.initialize();
        <?php
         if($mesas !== null && $mesas !== false):
        foreach ($mesas as $key => $mesa) { ?>
        $(mesas.cfg.dashboard).parent().append(mesas.createMesa(<?php echo $mesa->id_mesa ?>, <?php echo $mesa->tipo ?>, <?php echo $mesa->sillas ?>, '<?php echo $mesa->nombre ?>', <?php echo $mesa->x ?>, <?php echo $mesa->y ?>, <?php echo $mesa->orientacion ?>));
        <?php }
        endif;?>

        <?php
        foreach ($invitados as $key => $i) {
        if ($i->id_mesa != null && $i->silla != null) {
        ?>
        mesas.setInvitadoSilla('<?php echo "#mesa-" . decrypt($i->id_mesa) . " #silla-$i->silla" ?>', '<?php echo "#$i->id_invitado" ?>');
        <?php
        }
        }
        ?>

        $('#modal_create_mesa .aceptar').css({'pointer-events':'none'});

        $('#buscar').on('click', function () {
            $('#li_filtrar').toggle("blind", {}, 300);
        });
        $('#anadir_invitado_form').on('click', function () {
            if (isEmpty($('#grupo_acompanante').val()) || isEmpty($('#nombre_acompanante').val()) || isEmpty($('#apellido_acompanante').val())) {


                if (isEmpty($('#nombre_acompanante').val())) {
                    $('#nombre_acompanante').attr('class', $('#nombre_acompanante').attr('class') + ' invalid');
                } else {
                    $('#nombre_acompanante').attr('class', (($('#nombre_acompanante').attr('class')).split('invalid')[0]));
                }


                if (isEmpty($('#apellido_acompanante').val())) {
                    $('#apellido_acompanante').attr('class', $('#apellido_acompanante').attr('class') + ' invalid');
                } else {
                    $('#apellido_acompanante').attr('class', (($('#apellido_acompanante').attr('class')).split('invalid')[0]));
                }

                if (isEmpty($('#grupo_acompanante').val())) {
                    alert('Seleccione un grupo');
                }

            } else {
                var temp = '<div class="invitado_modal">' +
                    '<i class="material-icons">cancel</i>' +
                    '<img class="' + generoImage($('#edad_acompanante').val(), $('#sexo_acompanante').val()) + '" alt="Contact Person">' +
                    $('#nombre_acompanante').val() +
                    '<div style="display:none">' +
                    '<input type="hidden" name="nombre_acompanante[]" value="' + $('#nombre_acompanante').val() + '" />' +
                    '<input type="hidden" name="apellido_acompanante[]" value="' + $('#apellido_acompanante').val() + '" />' +
                    '<input type="hidden" name="grupo_acompanante[]" value="' + $('#grupo_acompanante').val() + '" />' +
                    '<input type="hidden" name="menu_acompanante[]" value="' + $('#menu_acompanante').val() + '" />' +
                    '<input type="hidden" name="sexo_acompanante[]" value="' + $('#sexo_acompanante').val() + '" />' +
                    '<input type="hidden" name="edad_acompanante[]" value="' + $('#edad_acompanante').val() + '" />' +
                    '</div>' +
                    '</div>';
                $('#acompanante_new').append(temp);
                $('#contador').text(parseInt($('#contador').text()) + 1);
                $('#contador').show();
                $('#nombre_acompanante').val('');
                $('#apellido_acompanante').val('');
            }

        });
        $('#anadir_invitado').on('click', function () {
            if ($('#id_acompanante').val() != null) {
                var id_acompanante = $('#id_acompanante').val();
                var sexo = $($('#id_acompanante').find(":selected")[0]).data('sexo');
                var edad = $($('#id_acompanante').find(":selected")[0]).data('edad');
                var temp = '<div class="invitado_modal">' +
                    '<i class="material-icons" onclick="deleteNewAcompanante(this);">cancel</i>' +
                    '<img class="' + generoImage(edad, sexo) + '" alt="Contact Person">' +
                    $('#id_acompanante option[value="' + $('#id_acompanante').val() + '"]').text() +
                    '<div style="display:none">' +
                    '<input type="hidden" name="id_acompanante[]" value="' + id_acompanante + '"/>' +
                    '</div>' +
                    '</div>';
                $('#acompanante_new').append(temp);
                $('#contador').text(parseInt($('#contador').text()) + 1);
                $('#contador').show();
                $('#id_acompanante option[value="' + id_acompanante + '"]').attr('disabled', '');
                $('#id_acompanante').material_select();
            }
        });
        $('[name="sexo"]').on('change', function () {
            $('#icon_persona_modal').attr('class', generoImage($('[name="edad"]').val(), $('[name="sexo"]').val()));
        });
        $('[name="edad"]').on('change', function () {
            $('#icon_persona_modal').attr('class', generoImage($('[name="edad"]').val(), $('[name="sexo"]').val()));
        });
        $('[name="sillas"]').on('change',function () {
            $('#modal_create_mesa .aceptar').css({'pointer-events':'all'});
        });

    });

    function cambio_filtro() {
        var name = $('#input-buscar').val().toLowerCase();
        var grupo = $('#formulario_id_grupo').val();
        $('.collection-item.list').each(function (i, obj) {
            var div = $(obj).children();
            if ($(div).data('nombre').indexOf(name) != -1 && (!$('#checkbox_change').is(':checked') || ($('#checkbox_change').is(':checked') && $(div).data('confirmado') == 2)) && ((grupo == "null") || ($(div).data('grupo') == grupo))) {
                $(obj).show();
            } else {
                $(obj).hide();
            }
        });
    }

    function clear_form() {
        $('#form-add-update').attr('action', '<?php echo base_url() ?>index.php/novios/invitados/Home/add_invitado');
        $('#icon_persona_modal').attr('class', generoImage(1, 1));
        $('[name="nombre"]').val('');
        $('[name="apellido"]').val('');
        $('[name="id_invitado"]').val('');
        $('[name="menu"]').val("");
        $('[name="menu"]').material_select();
        $('[name="sexo"]').val("");
        $('[name="sexo"]').material_select();
        $('[name="edad"]').val("");
        $('[name="edad"]').material_select();
        $('[name="grupo"]').val("");
        $('[name="grupo"]').removeAttr('disabled', '');
        $('[name="grupo"]').material_select();
        $('[name="correo"]').val('');
        $('[name="telefono"]').val('');
        $('[name="celular"]').val('');
        $('[name="direccion"]').val('');
        $('[name="cp"]').val('');
        $('#addInvitado1').click();
        $('#acompanante_new').text('');
        $('#contador').hide();
        $('#contador').text(0);
        $('#id_acompanante option').removeAttr('disabled');
        $('#id_acompanante').material_select();
    }

    function deleteNewAcompanante(origen) {
        origen.parentNode.remove();
        $('#contador').text(parseInt($('#contador').text()) - 1);
        if ($('#contador').text() == '0') {
            $('#contador').hide();
        }
    }

    function deleteAcompanante(origen, id) {
        var temp = "<input type='hidden' name='acompanante_remove[]' value='" + id + "' />";
        $('#acompanante_new').append(temp);
        origen.parentNode.remove();
        $('#contador').text(parseInt($('#contador').text()) - 1);
        if ($('#contador').text() == '0') {
            $('#contador').hide();
        }
    }

    function isEmpty(element) {
        if (element == '' || element == null || element.length == 0) {
            return true;
        }
        return false;
    }

</script>
