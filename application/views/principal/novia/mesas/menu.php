<?php
$controller = $_SERVER['REQUEST_URI'];
$controller = strtolower($controller);
?>
<style>
    @media only screen and (max-width: 425px) {
        .text-apperence{
            text-align: center !important;
            margin: 15px;
            padding: 0 15px!important;
        }

    }

    @media only screen and (max-width: 768px) {
        .text-apperence{
            text-align: center !important;
            padding: 0 15px!important;
        }

    }

</style>
<div class="row nav-bar body-container">
    <a class="col s12 m6 s6 text-apperence" style="border: 1px solid transparent; text-align: left;">
        Que no se te olvide nadie, tienes tiempo para elegir a tus invitados y organizarlos por mesa. Esta parte de la planeaci&oacute;n es padr&iacute;sima, disfr&uacute;tala.
    </a>
    <a class="col m3 s5 offset-s1<?php echo ( $controller == '/clubnupcial/index.php/novios/mesa' || $controller == '/clubnupcial/index.php/novios/mesa/index') ? 'active' : ''; ?>" href="<?php echo base_url() ?>index.php/novios/mesa/index">
        <i class="material-icons  left">widgets</i> Plano de mesas
    </a>
    <a class="col m3 s5 <?php echo (substr_count($controller, 'table') >= 1) ? 'active' : ''; ?>" href="<?php echo base_url() ?>index.php/novios/mesa/table">
        <i class="material-icons  left">list</i>Lista de invitados
    </a>
</div>