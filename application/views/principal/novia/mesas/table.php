<?php $this->view("principal/header") ?>
<?php $this->view("principal/novia/menu"); ?>
<header style="margin-bottom: 20px;">
    <?php $this->view("principal/novia/mesas/menu"); ?>
</header>
<div class="gray lighten-8" style="border-top: 1px solid #d9d9d9; padding: 10 0 10 0;">
    <div class="body-container">
        <div class="row" style="margin-top: 30px;">

            <?php foreach ($mesas as $key => $mesa) { ?>
                <div class="col m4 s6 ">
                    <div class="tarjeta z-depth-1">
                        <div class="encabezado">
                            <?php echo $mesa->nombre ?> (<?php echo count($mesa->invitados) ?> de <?php echo $mesa->sillas ?>)
                        </div>
                        <hr class="lista-separador" />
                        <ul class="cuerpo">
                            <?php if ($mesa->invitados) { ?>
                                <?php foreach ($mesa->invitados as $key => $invitado) { ?>
                                    <li><?php echo $invitado->nombre ?></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</div>
<?php $this->view("principal/footer"); ?>