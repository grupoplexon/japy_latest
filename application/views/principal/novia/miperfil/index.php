<style>
    .contenido_debates img {
        width: 100%;
        height: auto;
    }
    .p-grey {
        margin: 0;
        color: #3a393b;
        font-weight: bold;
    }
    .block-screen {
        pointer-events: none !important;
        opacity: .4 !important;
    }

    #opener_modal_perfil {
        display: none;
    }

    .cover-screen {
        width: 100%;
        height: 100%;
        display: block !important;
        max-width: 1903px;
        background-color: transparent !important;
        position: absolute;
        box-shadow: none;
    }

    .cover-screen:hover {
        box-shadow: none;
    }

    section.head-section-perfil:hover a.btn.editar {
        display: block !important;
    }

    .fondo {
        background: url('<?php echo base_url()?>dist/img/registro_expo/fondo-expo.png');
        padding: 5px !important;
        text-align: center;
        border-top-left-radius: 10px;
        border-top-right-radius: 15px;
        background-position: center;
        background-size: cover;
    }

    .padding-5 {
        padding-bottom: 5px !important;
    }
    .menu-edit-boda{
        top: 35px !important;
    }
</style>
<?php
$now      = new DateTime("now");
$fboda    = new DateTime($boda->fecha_boda);
$fexpo = new DateTime("2019-06-29 12:00:00");
$interval = $now->diff($fboda);
$interval2 = $now->diff($fexpo);
?>
<div class="content background" style=" background-image: url('<?php echo base_url()?>dist/img/fondo.jpg'); margin-bottom: -20px;position: relative;">
    <button id="opener_modal_perfil" href="#modal-perfil" class="btn modal-trigger "></button>
    <div class=" perfil-screen">
        <section class="head-section-perfil" style="padding-top: 20px;">
            <a href="<?php echo site_url("novios/perfil") ?>" class="btn editar white gris-1-text border"
            style="float: right;margin-right: 10px;font-size: 10px;display: none">
                <i class="material-icons left">edit</i> editar
            </a>
            <br><br>
            <div class="row center-align">
                <h5 class="center-align black-text darken-6">
                    <b>
                        <?php if ($novios) { ?>
                            <?php echo($novios[0] ? $novios[0]->nombre : "") ?>
                            <?php echo(key_exists(1, $novios) ? " y ".$novios[1]->nombre : "") ?>
                        <?php } ?>
                    </b>
                </h5>
                <hr style="border: 1px solid; border-color: #F8DADF; width:40%">

                <div class="col offset-m3 m6  s12">
                    <p class=" p-grey">NOS CASAMOS
                        EL <?php if ($boda->fecha_boda !== $boda->fecha_creacion) {
                            echo strtoupper(dateFormat($boda->fecha_boda, "%d de %B del %Y")); ?>
                            <span class="ndias hide">1</span>
                        <?php } else { ?>
                            ???
                            <span class="ndias hide">0</span>
                        <?php } ?>
                    </p>
                </div>
            </div>
            <div class="row" style="background: white;">
                <?php if ($boda->fecha_boda !== $boda->fecha_creacion && $interval->format('%a') > 0) : ?>
                <?php if ($interval2->format('%a') > 0): ?>
                <div class="col s12 m6 l6 offset-l3">
                    <?php else : ?>
                    <div class="col s12 m6 offset-m3">
                        <?php endif; ?>
                        <h6 class="center-align black-text darken-6">D&iacute;as para la boda: </h6>
                        <div class="row" style="height: 105px;margin:0">
                            <div class="faltante col s12 m10 offset-m1" style="padding: 0">
                                <div class="col s3 center-align grey lighten-3">
                                    <h5 class="gris-1-text">
                                        <b class="dias">
                                            <?php echo $interval->format('%a'); ?>
                                        </b>
                                    </h5>
                                    <small>D&iacute;as</small>
                                    <div style="margin-top:20px;"></div>
                                </div>
                                <div class="col s3 center-align grey lighten-3">
                                    <h5 class="gris-1-text">
                                        <b class="horas">
                                            <?php echo $interval->format('%h'); ?>
                                        </b>
                                    </h5>
                                    <small>Horas</small>
                                    <div style="margin-top:20px;"></div>
                                </div>
                                <div class="col s3 center-align grey lighten-3">
                                    <h5 class="gris-1-text">
                                        <b class="minutos">
                                            <?php echo $interval->format('%i'); ?>
                                        </b>
                                    </h5>
                                    <small>Min.</small>
                                    <div style="margin-top:20px;"></div>
                                </div>
                                <div class="col s3 center-align grey lighten-3">
                                    <h5 class="gris-1-text">
                                        <b class="segundos">
                                            <?php echo $interval->format('%s'); ?>
                                        </b>
                                    </h5>
                                    <small>Seg.</small>
                                    <div style="margin-top:20px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="seccion-header">
                    <div class="row center-align">
                        <div class="col l4 offset-l4 s12">
                            <br>
                            <h5 class="p-grey "><b>Mis proveedores </b></h5>
                        </div>
                        <hr style="border: 1px solid; border-color: #F8DADF; width:40%">
                    </div>
                    <div style="margin:8px">&nbsp;</div>
                </div>
                <div class="seccion-body jcarousel-proveedores" style="margin-right: 5%; margin-left: 5%;">
                    <a href="<?php echo base_url() ?>proveedores/index?search=Lugar%20para%20eventos&estado=Todos" class="col m2-4-jcaroul"
                        style="margin-bottom: 15px; padding: 5px;">
                        <div class="card-img">
                            <img src="<?php echo base_url() ?>dist/img/agenda_novia/salon_eventos.jpg"/>
                            <small class="text">
                                <b class="white-text">Salones de Eventos</b><br>
                                <b class="white-text">BUSCAR PROVEEDORES</b>
                            </small>
                        </div>
                    </a>
                    <a href="<?php echo base_url() ?>proveedores/index?search=Catering/Banquete&estado=Todos" class="col m2-4-jcaroul"
                        style="margin-bottom: 15px; padding: 5px;">
                        <div class="card-img">
                            <img src="<?php echo base_url() ?>dist/img/agenda_novia/catering.jpg"/>
                            <small class="text">
                                <b class="white-text">Banquete</b><br>
                                <b class="white-text">BUSCAR PROVEEDORES</b>
                            </small>
                        </div>
                    </a>
                    <a href="<?php echo base_url() ?>proveedores/index?search=Música/Producción&estado=Todos"
                        class="col m2-4-jcaroul" style="margin-bottom: 15px; padding: 5px;">
                        <div class="card-img">
                            <img src="<?php echo base_url() ?>dist/img/agenda_novia/musica.jpg"/>
                            <small class="text">
                                <b class="white-text">M&uacute;sica</b><br>
                                <b class="white-text">BUSCAR PROVEEDORES</b>
                            </small>
                        </div>
                    </a>
                    <a href="<?php echo base_url() ?>proveedores/index?search=Foto/Video&estado=Todos" class="col m2-4-jcaroul"
                        style="margin-bottom: 15px; padding: 5px;">
                        <div class="card-img">
                            <img src="<?php echo base_url() ?>dist/img/agenda_novia/fotografia.jpg"/>
                            <small class="text">
                                <b class="white-text">Foto y video</b><br>
                                <b class="white-text">BUSCAR PROVEEDORES</b>
                            </small>
                        </div>
                    </a>
                    <a href="<?php echo base_url() ?>proveedores/index?search=Carro%20de%20Novios&estado=Todos" class="col m2-4-jcaroul"
                        style="margin-bottom: 15px; padding: 5px;">
                        <div class="card-img">
                            <img src="<?php echo base_url() ?>dist/img/agenda_novia/auto.jpg"/>
                            <small class="text">
                                <b class="white-text">Autos para bodas</b><br>
                                <b class="white-text">BUSCAR PROVEEDORES</b>
                            </small>
                        </div>
                    </a>
                </div>
                 <div style="padding: 10px;">&nbsp;</div> 
            </div>
        </section>
        <section class="section-estatus">
            <!-- <div class="container"> -->
                <div class="seccion-header">
                    <div class="row center-align">
                        <div class="col l4 offset-l4 s12">
                            <h5 class="p-grey "><b>Mi resumen</b></h5>
                        </div>
                        <hr style="border: 1px solid; border-color: #F8DADF; width:40%">
                    </div>
                    
                    <div style="margin:8px">&nbsp;</div>
                </div>
                <div class="row center-align" style="padding-left: 15%;">
                    <a class="col l2" href="<?php echo site_url('novios/proveedor') ?>">
                         <!--<div class=" padding-10">
                            <div class="div-avatar">
                                <img class=""
                                     src="<?php echo base_url() ?>dist/img/iconos/Iconos-grises/icono-Provedores.png" />
                                <br>
                                <span style="font-size: 75%;" class="gris-1-text truncate"><b><?php echo $proveedores ?>
                                        Proveedor guardado</b></span>
                                <span class="black-text truncate" style="font-size: 75%;"></span>
                            </div>
                        </div> -->
                        <div>
                        <img style="" src="<?php echo base_url() ?>dist/img/iconos/Iconos-grises/icono-Provedores.png" alt="">
                        <span style="font-size: 75%; " class="gris-1-text truncate"><b> 
                            <?php echo $proveedores ?>
                                            Proveedor guardado</b></span>    
                        
                        </div>
                                                       
                    </a>
                    <a class="col l2" href="<?php echo site_url('novios/tarea') ?>">
                        
                        <div>
                        <img style="" src="<?php echo base_url() ?>dist/img/iconos/Iconos-grises/icono-Tareas.png" alt="">
                        <span style="font-size: 75%; " class="gris-1-text truncate"><b> 
                            <?php echo $pendientes ?>
                                            tareas pendientes</b></span>    
                        
                        </div>
                    </a>
                    <a class="col l2" href="<?php echo site_url('novios/invitados') ?>">
                        
                        <div>
                        <img style="" src="<?php echo base_url() ?>dist/img/iconos/Iconos-grises/icono-Invitados.png" alt="">
                        <span style="font-size: 75%; " class="gris-1-text truncate"><b> 
                            <?php echo $confirmados ?>
                                            invitados confirmados</b></span>    
                        
                        </div>
                    </a>
                    <a class="col l2" href="<?php echo site_url('novios/mesa') ?>">
                        
                        <div>
                            <img style="" src="<?php echo base_url() ?>dist/img/iconos/Iconos-grises/icono-mesas.png" alt="">
                            <span style="font-size: 75%; " class="gris-1-text truncate"><b> 
                                <?php echo $mesas ?>
                                            mesas preparadas</b></span>    
                        </div>
                    </a>
                    <a class="col l2" href="<?php echo site_url('novios/presupuesto') ?>">
                        
                        <div>
                            <img style=""src="<?php echo base_url() ?>dist/img/iconos/Iconos-grises/icono-presupuesto.png" alt="">
                            <span style="font-size: 75%; " class="gris-1-text truncate"><b> 
                                <?php echo $presupuesto ?>
                                            de presupuesto</b></span>    
                        </div>
                    </a>
                </div>
                
            <!-- </div> -->
        </section>
        <section>
        <br><br><br>
            <div class="container">
                <div class="row">
                    <div class="col m10 offset-m1 s12">
                        <div class="seccion-header">
                            <div class="row center-align">
                                <div class="col l4 offset-l4 s12">
                                    <h5 class="p-grey "><b>Mis tareas</b></h5>
                                </div>
                                <hr style="border: 1px solid; border-color: #F8DADF; width:40%">
                            </div>
                            
                            <div style="margin:8px">&nbsp;</div>
                        </div>
                        <div class="seccion-body">
                            <div class="collection miperfil">
                                <?php
                                if ($tareas_prox) {
                                    foreach ($tareas_prox as $key => $t) {
                                        ?>
                                        <a class="collection-item avatar margin"
                                           href="<?php echo base_url() ?>novios/tarea?grupo=tiempo&estado=0&cat=0#<?php echo $t->id_tarea ?>">
                                            <i class="material-icons square grey darken-6">check</i>
                                            <span class="title black-text"
                                                  style="width: calc( 100% - 40px ); display: block;"><?php echo $t->nombre ?></span>
                                            <p class="dorado-1-text">
                                                <!--<b><?php echo $this->task->categoriaNombre($t->categoria) ?></b>--></p>
                                            <span class="secondary-content">
                                            <b><i class="material-icons gris-1-text">keyboard_arrow_right</i></b>
                                        </span>
                                        </a>
                                        <?php
                                    }
                                }
                                ?>
                                <a class="collection-item avatar"
                                   href="<?php echo base_url() ?>novios/tarea?grupo=tiempo&estado=0&cat=0">
                                    <p class="right-align gris-1-text" style="margin-right: 50px;">
                                        <b><?php echo $pendientes ?> Tareas pendientes</b></p>
                                    <span href="#" class="secondary-content">
                                    <b><i class="material-icons gris-1-text">keyboard_arrow_right</i></b>
                                </span>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!--<div class="row center-align">-->
            <!--    <img style="width: 80%; height:auto;" class=""-->
            <!--            src="<?php echo base_url() ?>dist/img/agenda_novia/banner_principal.png" alt=""/>-->
            <!--</div>  -->
            <br>                  
        </section>
</div>

</div>
<?php $this->view("principal/novia/modal-perfil");
$this->view('modal_indicador_perfil');
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(function() {
        $('.jcarousel-proveedores').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            speed: 300,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left dorado-1-text" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right dorado-1-text" aria-hidden="true"></i></button>',
            autoplay: true,
            lazyLoad: 'ondemand',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
            ],
        });

        $('.edit-item').on('click', function() {
            switch ($(this.parentElement).data('name')) {
                case 'color':
                    $('#color-boda').removeAttr('style');
                    $('#color-boda').attr('class', 'principal circle-img ' + $(this).data('name'));
                    $('#color-boda-name').text($((this.children)[1]).text());
                    $.ajax({
                        url: '<?php echo base_url() ?>novios/invitados/Home/info_boda',
                        method: 'post',
                        data: {
                            'color': $(this).data('name'),
                        },
                        timeout: 1000,
                    }).done(function() {
                    }).fail(function() {
                        location.reload();
                    });
                    break;
                case 'temporada':
                    $('#temporada-boda').attr('src', '<?php echo base_url() ?>dist/img/agenda_novia/temporada/' + $(this).data('name') + '.png');
                    $('#temporada-boda-name').text($((this.children)[2]).text());
                    $.ajax({
                        url: '<?php echo base_url() ?>novios/invitados/Home/info_boda',
                        method: 'post',
                        data: {
                            'estacion': $(this).data('name'),
                        },
                        timeout: 1000,
                    }).done(function() {
                    }).fail(function() {
                        location.reload();
                    });
                    break;
                case 'hora':
                    $('#hora-boda').attr('src', '<?php echo base_url() ?>dist/img/agenda_novia/estilo/' + $(this).data('name') + '.png');
                    $('#hora-boda-name').text($((this.children)[2]).text());
                    $.ajax({
                        url: '<?php echo base_url() ?>novios/invitados/Home/info_boda',
                        method: 'post',
                        data: {
                            'estilo': $(this).data('name'),
                        },
                        timeout: 1000,
                    }).done(function() {
                    }).fail(function() {
                        location.reload();
                    });
                    break;
            }
        });

        setInterval(function() {
            var s = parseInt($('.faltante .segundos').html());
            if (s > 0) {
                s--;
            }
            else {
                s = 59;
                var m = parseInt($('.faltante .minutos').html());
                if (m > 0) {
                    m--;
                }
                else {
                    m = 59;
                    var h = parseInt($('.faltante .horas').html());
                    if (h > 0) {
                        h--;
                    }
                    else {
                        h = 23;
                        var d = parseInt($('.faltante .dias').html());
                        d--;
                        $('.faltante .dias').html(d);
                    }
                    $('.faltante .horas').html(h);
                }
                $('.faltante .minutos').html(m);
            }
            $('.faltante .segundos').html(s);
        }, 1000);

    });
</script>
