<style>
    .step-presupuesto-maestro .card {
        display: block;
        bottom: 0px;
        position: relative;
        top: 0px;
        /*overflow: hidden;*/
    }
    .step-presupuesto-maestro .card .card-content.grey {
        display: block;
        /*height: calc( 100% - 165px);*/
        /*overflow: auto;*/
    }
    .overlay-presupuesto-maestro {
        /*position: fixed;*/
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        /*        background: rgba(0, 0, 0, 0.5);
                z-index: 99;*/
    }
    span.help-block {
        font-size: 10px;
        margin-top: -10px;
        display: block;
    }

    .subcategoria td:first-child {
        padding-left: 25px;
    }

    .spinner {
        width: 40px;
        height: 40px;

        position: relative;
        margin: 100px auto;
    }

    .double-bounce1, .double-bounce2 {
        width: 100%;
        height: 100%;
        border-radius: 50%;
        background-color: #333;
        opacity: 0.6;
        position: absolute;
        top: 0;
        left: 0;

        -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
        animation: sk-bounce 2.0s infinite ease-in-out;
    }

    .double-bounce2 {
        -webkit-animation-delay: -1.0s;
        animation-delay: -1.0s;
    }

    @-webkit-keyframes sk-bounce {
        0%, 100% {
            -webkit-transform: scale(0.0)
        }
        50% {
            -webkit-transform: scale(1.0)
        }
    }

    @keyframes sk-bounce {
        0%, 100% {
            transform: scale(0.0);
            -webkit-transform: scale(0.0);
        }
        50% {
            transform: scale(1.0);
            -webkit-transform: scale(1.0);
        }
    }

    .proveedor {
        background-image: url(<?php echo base_url() ?>dist/img/iconos/clubnupcial_icons_gold.png) !important;
        background-repeat: no-repeat;
        display: block;
    }

    .proveedor-cat-novia-complementos {
        width: 250px;
        height: 250px;
        background-position: 5px -860px !important;
    }

    .proveedor-cat-novio {
        width: 250px;
        height: 250px;
        background-position: 5px -560px !important;
    }

    .tabs li a {
        color: black !important;
    }

    .tabs .indicator {
        background: #1c97b3 !important;
    }

    .yellow.darken-5 {
        background: #00BCDD !important;
        color: #FFFFFF !important;
    }

    .yellow-text.darken-5 {
        color: #00BCDD !important;
    }

    .grey-text.darken-8 {
        color: #514f50 !important;
    }

    input[type=text].valid {
        border-bottom: 1px solid #00BCDD !important;
        box-shadow: 0 1px 0 0 #00BCDD !important;
    }

    input[type=text]:focus:not([readonly]) {
        border-bottom: 1px solid #00BCDD !important;
        box-shadow: 0 1px 0 0 #00BCDD;
    }

    .proveedor.x2 {
        -moz-transform: scale(0.16);
        margin-top: -100px;
        margin-left: -100px;
        margin-right: -100px;
        margin-bottom: -100px;
        -webkit-transform: scale(0.16);
    }

    input[type=number]:focus:not([readonly]) {
        border-bottom: 1px solid #00BCDD !important;
        box-shadow: 0 1px 0 0 #00BCDD !important;
    }

    @media screen and(max-width: 860px) {
        .content-table-porcentaje {
            overflow: auto;
        }
    }
    #mensaje_change {
        margin: 0;
        color: #3B3A3C !important;
        font-size: .9rem;
    }
    hr{
        color: #F8DADF ;
        border:2px solid;
        background-color: #f8dadf;
    }
    .tab {
        background-color: #F8DADF !important;
    }
    .grey.darken-8, .grey.darken-3, .yellow.darken-5 {
        background: #F8DADF !important;
        color: black !important;
    }

</style>
<!--<div class="overlay-presupuesto-maestro">

</div>-->
<body style="background: url('<?php echo base_url() ?>dist/img/textura_footer2.png') repeat;">
<div class="step-presupuesto-maestro">
    <div class="body-container" style="padding-top: 50px">
        <div class="row">
            <h5 class="center-align black-text darken-6" style="margin: 0 0 0.656rem 0">
                BODA
            </h5>
            <div class="col s3" style="text-align: right!important">
                <p style="float: right;visibility: hidden;"><i
                            class="proveedor x2 proveedor-cat-novia-complementos"></i></p>
            </div>
            <div class="col s12 l6">
                <h4 class="center-align black-text darken-6">
                    <b>
                        <?php if ($novios) { ?>
                            <?php echo($novios[0] ? strtoupper($novios[0]->nombre) : "") ?>
                            <?php echo(key_exists(1, $novios) ? " &amp; ".strtoupper($novios[1]->nombre) : "") ?>
                        <?php } ?>
                    </b>
                </h4>
                <hr>
            </div>
            <!--<div class="col s3" style="text-align: left">-->
            <!--    <p style="float: left;visibility: hidden;"><i class="proveedor x2 proveedor-cat-novio"></i></p>-->
            <!--</div>-->
            <!--<div class="col s12 l12" style="text-align: center">-->
            <!--    <p style="">-->
            <!--        <img class="responsive-img" style="width: 35%"-->
            <!--             src="<?php echo base_url() ?>/dist/img/vineta_dorada.png" alt=""/>-->
                    <!--<img class="responsive-img" style="width: 35%" src="<?php echo base_url() ?>/dist/img/vineta3.png" alt=""/>-->
            <!--    </p>-->
            <!--</div>-->
            
            <h5 class="center-align black-text darken-6 s12 l12">FALTAN
                <?php
                $now      = new DateTime("now");
                $fboda    = new DateTime($boda->fecha_boda);
                $interval = $now->diff($fboda);
                ?>
                <?php echo $interval->format('%a'); ?> D&Iacute;AS
            </h5>
            <div class="col s12 l12" style="text-align: center">
                <p id="mensaje_change"
                   style="width: 80%; text-align: justify; margin-left: auto; margin-right: auto">
                    Nuestra misi&oacute;n es que esta experiencia sea inolvidable. Ahora que ya tienes un
                    presupuesto aproximado para tu boda, te ayudaremos a equilibrar y controlar los gastos para que
                    puedas cubrir cada detalle.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="card"
                 style="box-shadow: 0 15px 10px 0px rgba(0,0,0,.5), 0 1px 4px 0 rgba(0,0,0,.3), 0 0 40px 0 rgba(0,0,0,.1) inset;background-color: #3B3A3C;">
                <div class="card-content">
                    <div class="card-title center-align white-text">
                        PRESUPUESTADOR INTELIGENTE
                    </div>
                </div>
                <div class="card-tabs">
                    <ul class="tabs tabs-fixed-width">
                        <li class="tab "><a class="active" href="#paso1">Paso 1</a></li>
                        <li class="tab disabled"><a href="#paso2">Paso 2</a></li>
                        <li class="tab disabled"><a href="#paso3">Paso 3</a></li>
                    </ul>
                </div>
                <div class="card-content" style="background-color: #3B3A3C; margin-bottom: 50px">
                    <div id="paso1">
                        <p>
                            Una vez completados estos datos, no olvides que puedes hacer cualquier modificaci&oacute;n
                            en tu presupuesto total o parcial. M&aacute;s adelante podr&aacute;s hacer los cambios
                            en caso de ser necesario.
                        </p>
                        <div class="row">
                            <div class="input-field col s12">
                                <div class="col s12">
                                    <p class="col s12 m4 right grey darken-8" style="padding: 15px 5px"><b>Presupuesto
                                            aproximado total:</b></p>
                                </div>
                                <span class="grey-text darken-8"><b>$</b></span>
                                <input id="presupuesto" type="text" value="<?php echo $boda->presupuesto ?>"
                                       class="validate formulario" style="width: calc( 100% - 25px ); color: black !important;">
                                <!--<label for="presupuesto" style="    width: calc( 100% - 25px );left: 30px; color: white">Presupuesto:</label>-->
                                <span class="help-block red-text " style="margin-left: 20px;display: none">El campo es obligatorio</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <div class="col s12">
                                    <p class="col s12 m4 right grey darken-8"
                                       style="padding: 15px 5px; margin-bottom: -4px; margin-right: -5px"><b>N&uacute;mero
                                            de invitados aproximados:</b></p>
                                </div>
                                <span class="grey-text darken-8" style="margin-left: -10px;"><b><i
                                                class="material-icons">people</i></b></span>
                                <input id="invitados" type="text" value="<?php echo $boda->no_invitado ?>"
                                       class="validate formulario"
                                       style="width: calc( 100% - 25px ); text-align: right; color: black !important;">
                                <!--<label for="invitados" style="    width: calc( 100% - 25px );left: 30px; color: black">N&uacute;mero de invitados:</label>-->
                                <span class="help-block red-text " style="margin-left: 20px;display: none">El campo es obligatorio</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button class="btn btn-success pull-right btn-next-step1 yellow darken-5">
                                    <i class="material-icons right">chevron_right</i> Siguiente
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="paso2" style="display: none;">
                        <p>
                            Puedes actualizar o cambiar tu presupuesto en cualquier momento. Si ya compraste algo,
                            te regalaron algo, o simplemente ya lo tienes definido por que te encant&oacute;, puedes
                            actualizar los n&uacute;meros de cada concepto. Recuerda que este es tu presupuesto
                            personalizado.
                        </p>
                        <div class="row">
                            <div class="col s12 m6">
                                <table class="tabla-presupuesto hover">
                                    <thead>
                                    <tr>
                                        <th>Categoria</th>
                                        <th class="hide">Porcentaje</th>
                                        <th>Presupuesto</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($categories as $key => $category) : ?>
                                        <?php if ($key > $categories->count() / 2) : ?>
                                            <?php break; ?>
                                        <?php endif ?>
                                        <tr class="categoria grey darken-3 white-text">
                                            <td class="nombre" style="text-transform: uppercase">
                                                <?php echo $category->name ?>
                                            </td>
                                            <td class="" style="padding: 0px;">
                                                <!--<div class="input-field"-->
                                                <!--     style="margin: 0px; width: calc( 100% - 30px );float: left">-->
                                                <!--    <input id="<?php echo $category->name ?>" type="number"-->
                                                <!--           readonly=""-->
                                                <!--        value="<?php echo $category->percentage ?>"-->
                                                <!--           class=""-->
                                                <!--           style="margin: 0px;text-align: right; height: 2rem; width: auto; border-bottom: 1px solid transparent">-->
                                                <!--</div>-->
                                                <div class="input-field" style="margin: 0px; width: calc( 100% - 30px );float: left">
                                                  <input id="<?php echo $category->name ?>" type="number" value="<?php echo $category->percentage ?>" 
                                                  style="margin: 0px;text-align: right; height: 2rem; width: auto; border-bottom: 1px solid transparent">
                                                </div>
                                                <span style="width: 10px;margin-top: 5px;display: block;float: right;">%</span>
                                            </td>
                                            <td class="cantidad" style="text-align: right">
                                            </td>
                                        </tr>
                                        <?php //foreach ($category->subcategories as $subcategory) : ?>
                                            <!--<tr class="subcategoria" for="<?php echo $category->name ?>">-->
                                            <!--    <td class="nombre"-->
                                            <!--        style=" text-transform: capitalize;">  <?php echo $subcategory->name ?> </td>-->
                                            <!--    <td class="hide" style="padding: 0px;">-->
                                            <!--        <div class="input-field"-->
                                            <!--             style="margin: 0px; width: calc( 100% - 30px );float: left">-->
                                            <!--            <input id="<?php echo $subcategory->id ?>" type="number"-->
                                            <!--                   value="<?php echo $subcategory->percentage ?>"-->
                                            <!--                   class="formulario"-->
                                            <!--                   style="margin: 0px;text-align: right">-->
                                            <!--        </div>-->
                                            <!--        <span style="width: 10px;margin-top: 5px;margin-right: -1%;display: block;float: right;">%</span>-->
                                            <!--    </td>-->
                                            <!--    <td class="cantidad" style="text-align: right">-->
                                            <!--    </td>-->
                                            <!--</tr>-->
                                        <?php// endforeach ?>
                                    <?php endforeach ?>

<!--                                        <?php /*$count = 0 */ ?>
                                            <?php /*$c = count($categorias) / 2 */ ?>
                                            <?php /*foreach ($categorias as $cat => $subs) { */ ?>
                                                <?php
/*                                            $count++;
                                            if ($count > $c) {
                                                break;
                                            }
                                            */ ?>
                                                <tr class="categoria grey darken-3 white-text">
                                                    <td class="nombre" style="text-transform: uppercase">
                                                        <?php /*echo $cat */ ?>
                                                    </td>
                                                    <td class="hide" style="padding: 0px;">
                                                        <div class="input-field"
                                                             style="margin: 0px; width: calc( 100% - 30px );float: left">
                                                            <input id="<?php /*echo $cat */ ?>" type="number" readonly=""
                                                                   class=""
                                                                   style="margin: 0px;text-align: right; height: 2rem; width: auto; border-bottom: 1px solid transparent">
                                                        </div>
                                                        <span style="width: 10px;margin-top: 5px;display: block;float: right;">%</span>
                                                    </td>
                                                    <td class="cantidad" style="text-align: right">
                                                    </td>
                                                </tr>
                                                <?php /*foreach ($subs as $key => $value) { */ ?>
                                                    <tr class="subcategoria" for="<?php /*echo $cat */ ?>">
                                                        <td class="nombre"
                                                            style=" text-transform: capitalize;">  <?php /*echo $key */ ?> </td>
                                                        <td class="hide" style="padding: 0px;">
                                                            <div class="input-field"
                                                                 style="margin: 0px; width: calc( 100% - 30px );float: left">
                                                                <input id="<?php /*echo $key */ ?>" type="number"
                                                                       value="<?php /*echo $value */ ?>" class="formulario"
                                                                       style="margin: 0px;text-align: right">
                                                            </div>
                                                            <span style="width: 10px;margin-top: 5px;margin-right: -1%;display: block;float: right;">%</span>
                                                        </td>
                                                        <td class="cantidad" style="text-align: right">
                                                        </td>
                                                    </tr>
                                                <?php /*} */ ?>
                                            --><?php /*} */ ?>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="col s12 m6">
                                <table class=" tabla-presupuesto hover">
                                    <thead>
                                    <tr>
                                        <th>Categoria</th>
                                        <th class="hide">Porcentaje</th>
                                        <th>Presupuesto</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($categories as $key => $category) : ?>
                                        <?php if ($key < ($categories->count() / 2)+1) : ?>
                                            <?php continue; ?>
                                        <?php endif ?>
                                        <tr class="categoria grey darken-3 white-text">
                                            <td class="nombre" style="text-transform: uppercase">
                                                <?php echo $category->name ?>
                                            </td>
                                            <td class="" style="padding: 0px;">
                                                <!--<div class="input-field"-->
                                                <!--     style="margin: 0px; width: calc( 100% - 30px );float: left">-->
                                                <!--    <input id="<?php echo $category->name ?>" type="number"-->
                                                <!--           value="<?php echo $category->percentage ?>"-->
                                                <!--           readonly=""-->
                                                <!--           class=""-->
                                                <!--           style="margin: 0px;text-align: right; height: 2rem; width: auto; border-bottom: 1px solid transparent">-->
                                                <!--</div>-->
                                                 <div class="input-field" style="margin: 0px; width: calc( 100% - 30px );float: left">
                                                  <input id="<?php echo $category->name ?>" type="number" value="<?php echo $category->percentage ?>" 
                                                  style="margin: 0px;text-align: right; height: 2rem; width: auto; border-bottom: 1px solid transparent">
                                                </div>
                                                <span style="width: 10px;margin-top: 5px;display: block;float: right;">%</span>
                                            </td>
                                            <td class="cantidad" style="text-align: right">
                                            </td>
                                        </tr>
                                        <?php // foreach ($category->subcategories as $subcategory) : ?>
                                            <!--<tr class="subcategoria" for="<?php echo $category->name ?>">-->
                                            <!--    <td class="nombre"-->
                                            <!--        style=" text-transform: capitalize;">  <?php echo $category->name ?> </td>-->
                                            <!--    <td class="hide" style="padding: 0px;">-->
                                            <!--        <div class="input-field"-->
                                            <!--             style="margin: 0px; width: calc( 100% - 30px );float: left">-->
                                            <!--            <input id="<?php echo $category->id ?>" type="number"-->
                                            <!--                   value="<?php echo $category->percentage ?>"-->
                                            <!--                   class="formulario"-->
                                            <!--                   style="margin: 0px;text-align: right">-->
                                            <!--        </div>-->
                                            <!--        <span style="width: 10px;margin-top: 5px;margin-right: -1%;display: block;float: right;">%</span>-->
                                            <!--    </td>-->
                                            <!--    <td class="cantidad" style="text-align: right">-->
                                            <!--    </td>-->
                                            <!--</tr>-->
                                        <?php //endforeach ?>
                                    <?php endforeach ?>

                                    <!-- <?php /*$count = 0 */ ?>
                                        <?php /*foreach ($categorias as $cat => $subs) { */ ?>
                                            <?php
                                    /*                                            $count++;
                                                                                if ($count <= $c) {
                                                                                    continue;
                                                                                }
                                                                                */ ?>
                                            <tr class="categoria grey darken-3 white-text">
                                                <td class="nombre" style="text-transform: uppercase">
                                                    <?php /*echo $cat */ ?>
                                                </td>
                                                <td class="hide" style="padding: 0px;">
                                                    <div class="input-field"
                                                         style="margin: 0px; width: calc( 100% - 30px );float: left">
                                                        <input id="<?php /*echo $cat */ ?>" type="number" readonly=""
                                                               class=""
                                                               style="margin: 0px;text-align: right; height: 2rem; width: auto; border-bottom: 1px solid transparent">
                                                    </div>
                                                    <span style="width: 10px;margin-top: 5px;display: block;float: right;">%</span>
                                                </td>
                                                <td class="cantidad" style="text-align: right">
                                                </td>
                                            </tr>
                                            <?php /*foreach ($subs as $key => $value) { */ ?>
                                                <tr class="subcategoria" for="<?php /*echo $cat */ ?>">
                                                    <td class="nombre"
                                                        style=" text-transform: capitalize;">  <?php /*echo $key */ ?> </td>
                                                    <td class="hide" style="padding: 0px;">
                                                        <div class="input-field"
                                                             style="margin: 0px; width: calc( 100% - 30px );float: left">
                                                            <input id="<?php /*echo $key */ ?>" type="number"
                                                                   value="<?php /*echo $value */ ?>" class="formulario"
                                                                   style="margin: 0px;text-align: right">
                                                        </div>
                                                        <span style="width: 10px;margin-top: 5px;margin-right: -1%;display: block;float: right;">%</span>
                                                    </td>
                                                    <td class="cantidad" style="text-align: right">
                                                    </td>
                                                </tr>
                                            <?php /*} */ ?>
                                        --><?php /*} */ ?>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <p class="red-text error-100" style="display: none;">
                            La suma de los porcentajes debe ser 100.
                        </p>
                        <div class="row">
                            <div class="col s12">
                                <button class="btn btn-success pull-right btn-next-step2 yellow darken-5">
                                    <i class="material-icons right">chevron_right</i> Siguiente
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="paso3" style="display: none;">

                        <div class="spinner">
                            <div class="double-bounce1"></div>
                            <div class="double-bounce2"></div>
                        </div>

                        <div class="continuar" style="display: none">
                            <i class='material-icons' style='    font-size: 100px'>check_circle</i>
                            <br>Datos registrados <br>
                            <?php if ( ! empty($editar) && $editar == "editar") { ?>
                                <a href="<?php echo base_url() ?>novios/presupuesto"
                                   class="btn  yellow darken-5 waves-effects"
                                   style=' margin-top: 20px;font-size: 12px;'>Continuar </a>
                            <?php } else { ?>
                                <button style=' margin-top: 20px;font-size: 12px;'
                                        class='btn  yellow darken-5 waves-effects btn-continuar'>Continuar
                                </button>
                            <?php } ?>
                            <!--<a href="<?php /*echo base_url() */ ?>index.php/novios/proveedor/sugeridos"
                               class="btn  yellow darken-5 waves-effect"
                               style='    margin-top: 20px;font-size: 12px;'>
                                Proveedores sugeridos
                            </a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var presupuesto = 0;
        var invitados = 0;
        var categorias = [];

        $("#presupuesto").inputFormat();

        $(".btn-next-step1").on("click", function () {
            presupuesto = $("#presupuesto").inputFormat("value");
            invitados = $("#invitados").val();
            if (presupuesto == 0 || isNaN(presupuesto)) {
                $("#presupuesto").addClass("invalid");
                $("#presupuesto").parent().find(".help-block").show();
            } else if (invitados == 0 || isNaN(invitados)) {
                $("#invitados").addClass("invalid");
                $("#invitados").parent().find(".help-block").show();
            } else {
                $("#presupuesto").removeClass("invalid");
                $("#presupuesto").parent().find(".help-block").hide();
                $(".step-presupuesto-maestro a[href='#paso1']").removeClass("active").parent().addClass("disabled");
                $(".step-presupuesto-maestro a[href='#paso2']").addClass("active").parent().removeClass("disabled");
                $(".step-presupuesto-maestro a[href='#paso2']").click();
                $(".test-pres").html($("#presupuesto").val());
                calc_procentajes(presupuesto)
            }
        });

        $(".btn-next-step2").on("click", function () {
            calc_procentajes(presupuesto);

            var por = 0;

            $(".tabla-presupuesto .categoria").each(function () {
                var c = $(this).find("input").val();
                por += parseFloat(c);
            });

            if (por == 100) {
                $(".step-presupuesto-maestro a[href='#paso2']").removeClass("active").parent().addClass("disabled");
                $(".step-presupuesto-maestro a[href='#paso3']").addClass("active").parent().removeClass("disabled");
                $(".step-presupuesto-maestro a[href='#paso3']").click();
                $(".error-100").hide();
                $('#mensaje_change').text("Con estos pasos y tus datos te vamos a sugerir a los mejores proveedores de acuerdo a tus posibilidades.");
                $.ajax({
                    url: "<?php echo base_url() ?>novia/updatePorcentajes",
                    method: "POST",
                    data: {
                        categorias: JSON.stringify(categorias),
                        presupuesto: presupuesto,
                        invitados: invitados
                    },
                    success: function () {
                        $(".step-presupuesto-maestro .card .card-content.grey").attr("style", "background-color:#f5e6df !important");

                        $("#paso3").css({
                            "text-align": "center",
                            color: "#4f5055",
                            "font-size": "30px",
                        }).find(".spinner").remove();

                        $("#paso3 .continuar").show();

                        $("#paso3").find("button.btn-continuar").on("click", function () {
                            location.replace("<?php echo base_url()?>/novios/presupuesto");
                        });

                    }, error: function () {
                        $(".step-presupuesto-maestro .card .card-content.grey").attr("style", "background-color: #d35400 !important");
                    }
                });
            } else {
                $(".error-100").show();
            }
        });

        $(".tabla-presupuesto input").on("change", function () {
            calc_procentajes(presupuesto)
        });

        function calc_procentajes(pre) {
            // Sacar los porcentajes de las subcategorias
            $(".tabla-presupuesto .categoria").each(function () {
                const id = $(this).find(".nombre").attr('data-id');
                var nombre = $(this).find(".nombre").text().trim();
                var total = 0;
                var p = $(this).find("input").val();
                // $(".subcategoria[for='" + nombre + "']").each(function () {
                //     percentage = parseFloat($(this).find("input").val());
                //     total += percentage;
                //     var category = {
                //         'id': $(this).find("input").attr('id'),
                //         'percentage': percentage,
                //         'name': $(this).find('td.nombre').html()
                //     };

                //     categorias.push(category);
                // });
                
                var valor = (Number(Number((p / 100) * pre).toFixed(2)).toLocaleString("es-MX", {
                    style: "currency",
                    currency: "MXN"
                }));
                
                categorias[nombre] = parseFloat(p);
                
                $(this).find(".cantidad").text(valor);

                // $(this).find("input").val(total);
            });

            $(".tabla-presupuesto tr").each(function () {
                var nombre = $(this).find(".nombre").text().trim();
                var p = $(this).find("input").val();

                var valor = (Number(Number((p / 100) * pre).toFixed(2)).toLocaleString("es-MX", {
                    style: "currency",
                    currency: "MXN"
                }));

                if ($(this).hasClass("subcategoria")) {
                    categorias[nombre] = parseFloat(p);
                }

                $(this).find(".cantidad").text(valor);
            });
        }
    });

</script>
