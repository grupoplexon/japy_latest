<script src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/tokenfield/js/bootstrap-tokenfield.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/materialize.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/dropzone.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.Jcrop.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/modernizr.custom.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/classie.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/validation/validate.engine.js"    type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/validation/validate.engine.plugin.js"    type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/location_multiple.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/slider.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/slick/slick.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/colorpicker/colorpicker.js" type="text/javascript" ></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/inputFormat.jquery.js"></script>
<script>
    $(document).ready(function () {
        if (typeof onReady != "undefined") {
            onReady();
        }
        $('select').material_select();
        $(".button-collapse").sideNav();
        $(".dropdown-button").dropdown();
        $('.modal-trigger').modal();
        $('.collapsible').collapsible({
            accordion: false,
        });
    });

    function isEmpty(element) {
        if (element == '' || element == null || element.length == 0 || element == 0) {
            return true;
        }
        return false;
    }
</script>