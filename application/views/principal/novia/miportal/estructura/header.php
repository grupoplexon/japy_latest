<title>Japy</title>
<link href="<?php echo base_url() ?>dist/img/clubnupcial.css" rel="icon" sizes="16x16">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo base_url() ?>dist/jquery ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/tokenfield/css/bootstrap-tokenfield.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/tokenfield/css/tokenfield-typeahead.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/dropzone.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/slick/slick.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/slick/slick-theme.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/miportal.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/colorpicker/colorpicker.css" rel="stylesheet" media="screen" type="text/css"/>
<style>
    :root {
        --imgBackground: url(<?php echo ($miportal->img_table == 'plantillaportal_imagen')? base_url().$miportal->base : "data:$miportal->mime;base64," . base64_encode($miportal->base) ?>);
        --titleColor: <?php echo empty($miportal->titleColor) ? $plantilla->titleColor : $miportal->titleColor ?>;
        --titleFontType: <?php echo empty($miportal->titleFont) ? $plantilla->titleFont : $miportal->titleFont ?>;

        --menuBackgrouncolor: <?php echo empty($miportal->menuBackground) ? $plantilla->menuBackground : $miportal->menuBackground ?>;
        --menuColor: <?php echo empty($miportal->menuColor) ? $plantilla->menuColor : $miportal->menuColor ?>;
        --menuFontType: <?php echo empty($miportal->menuFont) ? $plantilla->menuFont : $miportal->menuFont ?>;

        --cuentaBackground: <?php echo empty($miportal->cuentaBackground) ? $plantilla->cuentaBackground : $miportal->cuentaBackground ?>;
        --cuentaColor: <?php echo empty($miportal->cuentaColor) ? $plantilla->cuentaColor : $miportal->cuentaColor ?>;

        --bodyBackground: <?php echo empty($miportal->bodyBackground) ? $plantilla->bodyBackground : $miportal->bodyBackground ?>;

        --titleBody: <?php echo empty($miportal->subtitleColor) ? $plantilla->subtitleColor : $miportal->subtitleColor ?>;
        --titleBodyFontType: <?php echo empty($miportal->subtitleFont) ? $plantilla->subtitleFont : $miportal->subtitleFont ?>;

        --textColor: <?php echo empty($miportal->textColor) ? $plantilla->textColor : $miportal->textColor ?>;
        --linkColor: <?php echo empty($miportal->linkColor) ? $plantilla->linkColor : $miportal->linkColor ?>;
        --textFontType: <?php echo empty($miportal->textFont) ? $plantilla->textFont : $miportal->textFont ?>;
    }

    .fixed-sidebar {
        padding: 0px;
        border-top: 1px solid #6c6d6f;
        position: fixed;
        top: 186px;
        bottom: 0px;
        left: 0px;
        display: block;
        background: #eceff1;
        height: 100%;
    }
</style>
<link href="<?php echo base_url() ?>dist/file-portal/css/fuentes.css" rel="stylesheet" type="text/css"/>
<?php
echo '<link id="css-select" href="'.base_url(
    ).'dist/file-portal/css/'.$plantilla->nombreCSS.'.css" rel="stylesheet" type="text/css"/>';
?>
<script src="<?php echo base_url() ?>dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>