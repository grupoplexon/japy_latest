<div class="gris-1" style="padding:10px;">
    <div class="body-container">
        <nav class="title gris-1">
            <div class="nav-wrapper">

                <a class="brand-logo" href="<?php echo base_url() ?>index.php/novios/invitados">
                    <i class="material-icons dorado-2-text left">arrow_back</i>
                    <img style="width: 244px; height: auto;" src="<?php echo base_url() ?>dist/img/clubnupcial_new.png" alt=""/>
                    <!--<img style="width: 244px; height: auto;" src="<?php echo base_url() ?>/dist/img/logo.png"/> -->
                </a>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a class="dorado-2-text" href="<?php echo base_url() ?>index.php/web/index/<?php echo $this->session->userdata('id_miportal') ?>" target="_blank" >
                            MI WEB: <?php echo site_url('web/index') . '/' . $this->session->userdata('id_miportal') ?>
                        </a>
                    </li>
                    <li>
                        <a class="dorado-2-text" href="javascript:postCurrentPage()" target="_blank" >
                            <i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a class="dorado-2-text" href="javascript:tweetCurrentPage()" target="_blank">
                            <i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a class="dorado-2-text" href="javascript:envio_correo()">
                            <i class="fa fa-envelope-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<script>
    function envio_correo() {
        $($('.menu-item')[3]).trigger('click');
    }

    function postCurrentPage() {
        window.open("https://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo sanear_string(strip_tags($seccion->titulo)) ?>&p[images][0]=<?php echo base_url() . 'dist/img/logo.png' ?>&p[url]=<?php echo base_url() . 'index.php/web/index/' . $this->session->userdata('id_miportal') ?>", '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false;
    }

    function tweetCurrentPage() {
        window.open("https://twitter.com/share?url=<?php echo base_url() . 'index.php/web/index/' . $this->session->userdata('id_miportal') ?>&text=<?php echo $this->session->userdata('nombre') ?>", '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false;
    }
</script>