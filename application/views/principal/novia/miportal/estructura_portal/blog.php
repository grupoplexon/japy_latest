<!-- B  O  D  Y -->
<div class="container-body">
    <div class="body-container-web detail">
        <?php
            if (!empty($seccion)) {
                ?>
                <div class="blog">
                    <div id="blog-seccion">
                        <button class="waves-effect waves-light btn-flat button-edit" id="editar-blogprincipal" style="display: none;" type="button"><i class="material-icons left">mode_edit</i>Editar</button>
                        <h4 class="titulo"><?php echo $seccion->titulo ?></h4>
                        <div class="descripcion"><?php echo $seccion->descripcion ?></div>
                    </div>
                    <div class="edit" id="blog-seccion-edit" style="display: none;">
                        <form id="send-form">
                            <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                            <br>
                            <input type="hidden" name="id-blog" value="<?php echo $seccion->id_miportal_seccion ?>" required />
                            <h6><b>T&iacute;tulo*</b></h6>
                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="titulo-blog" value="<?php echo $seccion->titulo; ?>" placeholder="T&iacute;tulo" />
                            <h6><b>Descripci&oacute;n*</b></h6>
                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-blog" id="blogdescripcion"><?php echo $seccion->descripcion; ?></textarea>
                            <br>
                            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                            <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                        </form>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $("#send-form").validationEngine();
                            
                            $('#close-edit').on('click', function () {
                                $('#blog-seccion-edit').hide();
                                $('#blog-seccion').show();
                                $("#blog-seccion").mouseover(function () {
                                    $('#blog-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-blogprincipal').show();
                                });

                                $("#blog-seccion").mouseout(function () {
                                    $('#blog-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-blogprincipal').hide();
                                });
                            });
                            
                            $('#close-cancel').on('click', function () {
                                $('#blog-seccion-edit').hide();
                                $('#blog-seccion').show();
                                $("#blog-seccion").mouseover(function () {
                                    $('#blog-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-blogprincipal').show();
                                });

                                $("#blog-seccion").mouseout(function () {
                                    $('#blog-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-blogprincipal').hide();
                                });
                            });
                            
                            $('.button-edit').on('click', function () {
                                $("#blog-seccion").unbind("mouseover");
                                $("#blog-seccion").unbind("mouseout");
                                $('#blog-seccion-edit').show();
                                $('#blog-seccion').hide();
                            });
                            
                            $("#blog-seccion").mouseover(function () {
                                $('#blog-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#editar-blogprincipal').show();
                            });
                            
                            $("#blog-seccion").mouseout(function () {
                                $('#blog-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                $('#editar-blogprincipal').hide();
                            });
                            
                            $('#button-send-form').on('click', function () {
                                if ($("#send-form").validationEngine('validate')) {
                                    var descrp = tinymce.get(1).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form').attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_headerevento',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-evento': $('[name="id-blog"]').val(),
                                                'titulo-evento': $('[name="titulo-blog"]').val(),
                                                'descripcion-evento': tinymce.get(1).getContent(),
                                            },
                                        }).done(function () {
                                            $($('#blog-seccion').find('.titulo')[0]).text($('[name="titulo-blog"]').val());
                                            $($('#blog-seccion').find('.descripcion')[0]).html(tinymce.get(1).getContent());
                                            $('#close-cancel').trigger('click');
                                            $('#button-send-form').removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form').removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                        });
                    </script>
                    
                    
                    <?php foreach ($blogs as $key => $obj) { ?>
                        <div id="<?php echo $obj->id_miportal_blog ?>" style="position:relative;">
                            <hr class="hr-separador">
                            <div class="myBlog" id="blogs-seccion-<?php echo $obj->id_miportal_blog ?>" style="padding:11px;">
                                <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-<?php echo $obj->id_miportal_blog ?>" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                                <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-<?php echo $obj->id_miportal_blog ?>" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
                                <h5 class="titulo"><?php echo $obj->titulo ?></h5>
                                <div class="carrusel">
                                    <div class="row">
                                        <div class="col s1"><i class="material-icons">keyboard_arrow_left</i></div>
                                        <div class="col s10"><img class="imageprincipal" src="<?php echo empty($obj->imagenes[0]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[0]->mime . ";base64," . base64_encode($obj->imagenes[0]->base)) ?>"></div>
                                        <div class="col s1"><i class="material-icons">keyboard_arrow_right</i></div>
                                    </div>
                                </div>
                                <div class="descripcion"><?php echo $obj->descripcion; ?></div>
                                <h6 class="numComentario" data-contador="<?php echo count($obj->comentarios); ?>"><i class="material-icons">chat_bubble_outline</i> <?php echo count($obj->comentarios); ?> Comentario</h6>
                            </div>
                            
                            <div class="edit" id="blogs-seccion-edit-<?php echo $obj->id_miportal_blog ?>" style="display:none;">
                                <form id="send-form-<?php echo $obj->id_miportal_blog ?>">
                                    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-<?php echo $obj->id_miportal_blog ?>" type="button" ><i class="material-icons">close</i></button>
                                    <h6><b>T&iacute;tulo*</b></h6>
                                    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="tituloblog-<?php echo $obj->id_miportal_blog ?>" value="<?php echo $obj->titulo; ?>" placeholder="T&iacute;tulo" />
                                    <h6><b>Descripci&oacute;n</b></h6>
                                    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionblog-<?php echo $obj->id_miportal_blog ?>"><?php echo $obj->descripcion; ?></textarea>
                                    <br><br>
                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-<?php echo $obj->id_miportal_blog ?>" type="button" >GUARDAR</button>
                                    <button class="waves-effect waves-light btn-flat" id="close-cancel-<?php echo $obj->id_miportal_blog ?>" type="button" >Cancelar</button>
                                </form>
                                <form class="dropzone" drop-zone="" id="dropzone-<?php echo $obj->id_miportal_blog ?>"></form>
                                <h6><b>Comentarios</b></h6>
                                <hr>
                                <div class="libro-comentario">
                                    <?php foreach ($obj->comentarios as $i => $temp) { ?>
                                        <div class="comentario-<?php echo encrypt($temp->id_miportal_blog_comentario) ?>">
                                            <span class="lc-titulo"><?php echo $temp->nombre ?></span>
                                            <span class="lc-fecha">&#183;<?php echo relativeTimeFormat($temp->fecha) ?></span>
                                            <span class="lc-action">&#183;<button class="deleteComentario-<?php echo $obj->id_miportal_blog ?> btn-flat" data-id='<?php echo encrypt($temp->id_miportal_blog_comentario) ?>'><i class="material-icons">delete</i>Borrar</button></span>
                                            <p><?php echo $temp->comentario ?></p>
                                        </div>            
                                    <?php } ?>
                                </div>
                            </div>
                            <br>
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('#close-edit-<?php echo $obj->id_miportal_blog ?>').on('click', function () {
                                    $('#blogs-seccion-edit-<?php echo $obj->id_miportal_blog ?>').hide();
                                    $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').show();
                                    $("#blogs-seccion-<?php echo $obj->id_miportal_blog ?>").mouseover(function () {
                                        $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_blog ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_blog ?>').show();
                                    });

                                    $("#blogs-seccion-<?php echo $obj->id_miportal_blog ?>").mouseout(function () {
                                        $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').attr('style', 'padding:10px; border: 1px solid #d9d9d9; opacity:1;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_blog ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_blog ?>').hide();
                                    });
                                });

                                $('#dropzone-<?php echo $obj->id_miportal_blog ?>').dropzone({
                                    url: "<?php echo base_url() ?>index.php/novios/miweb/home/upload_image_blog/<?php echo $obj->id_miportal_blog ?>",
                                    maxFilesize: 3,
                                    method: "POST",
                                    paramName: "uploadfile",
                                    maxThumbnailFilesize: 6,
                                    acceptedFiles: "image/*",
                                    addRemoveLinks: true,
                                    dictCancelUploadConfirmation: "Estas seguro?",
                                    dictRemoveFile: 'Eliminar imagen',
                                    dictCancelUpload: 'Cancelar carga',
                                    dictInvalidFileType: "No se puede cargar este tipo de archivos.",
                                    dictFileTooBig: "La imagen es muy grande ({{filesize}}MiB). Tama&ntilde;o m&aacute;ximo: {{maxFilesize}}MiB.",
                                    dictDefaultMessage: '<center><i class="material-icons" style="font-size: 28px;">file_upload</i><br><b>Suelte la imagen o haga click aqu&iacute; para cargar</b></center>',
                                    init: function () {
                                    <?php foreach ($obj->imagenes as $i => $temp) { ?>
                                        var mockFile = {
                                            name: "hgdgaggg3333-<?php echo encrypt($temp->id_miportal_imagen) ?>",
                                            size: 12345,
                                            type: 'image/jpeg',
                                            status: this.ADDED,
                                        };
                                        this.emit("addedfile", mockFile);
                                        this.emit("thumbnail", mockFile, '<?php echo "data:" . $temp->mime . ";base64," . base64_encode($temp->base) ?>');
                                        mockFile.previewElement.classList.add('dz-success');
                                        mockFile.previewElement.classList.add('dz-complete');
                                        this.files.push(mockFile);
                                    <?php } ?>

                                this.on('success', function (file, json) {
                                    $(file.previewElement).data('id', json.id);
                                    restartImage('<?php echo $obj->id_miportal_blog ?>');
                                    var element = $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').find('.number-blog')[0];
                                    $(element).data('contador', $(element).data('contador') + 1);
                                    $(element).text($(element).data('contador') + ' blogs');
                                });
                                this.on('addedfile', function () {
                                    console.log('addedfile');
                                });
                                this.on('drop', function () {
                                    console.log('drop');
                                });
                            },
                            removedfile: function (file) {
                                var id = file.name;
                                if (id.indexOf('hgdgaggg3333-') > -1) {
                                    id = id.split("hgdgaggg3333-")[1];
                                } else {
                                    id = $(file.previewElement).data('id');
                                }
                                $.ajax({
                                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/upload_image_blog/<?php echo $obj->id_miportal_blog ?>',
                                    method: 'POST',
                                    timeout: 4000,
                                    data: {
                                        id: id,
                                    }
                                }).done(function () {
                                    console.log('success delete');
                                    var _ref;
                                    (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

                                    restartImage('<?php echo $obj->id_miportal_blog ?>');
                                    return true;
                                }).fail(function () {
                                    console.log('fail');
                                });
                            }
                        });

                        $('.deleteComentario-<?php echo $obj->id_miportal_blog ?>').on('click', function () {
                            var id = $(this).data('id');
                            var r = confirm("Seguro que deseas eliminar este comentario?");
                            if (r == true) {
                                $.ajax({
                                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_comentario_blog',
                                    method: 'POST',
                                    timeout: 4000,
                                    data: {
                                        id_comment: id,
                                    },
                                }).done(function () {
                                    $('.comentario-' + id).remove();
                                    var element = $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').find('.numComentario')[0];
                                    $(element).data('contador', $(element).data('contador') - 1);
                                    $(element).html('<i class="material-icons">chat_bubble_outline</i> ' + $(element).data('contador') + ' Comentario');
                                }).fail(function () {
                                    console.log('error');
                                });
                            }
                        });

                        $('#close-cancel-<?php echo $obj->id_miportal_blog ?>').on('click', function () {
                            $('#blogs-seccion-edit-<?php echo $obj->id_miportal_blog ?>').hide();
                            $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').show();
                            $("#blogs-seccion-<?php echo $obj->id_miportal_blog ?>").mouseover(function () {
                                $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#button-editseccion-<?php echo $obj->id_miportal_blog ?>').show();
                                $('#button-delete-<?php echo $obj->id_miportal_blog ?>').show();
                            });

                            $("#blogs-seccion-<?php echo $obj->id_miportal_blog ?>").mouseout(function () {
                                $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').attr('style', 'padding:11px; border: 0px solid #d9d9d9; opacity:1;');
                                $('#button-editseccion-<?php echo $obj->id_miportal_blog ?>').hide();
                                $('#button-delete-<?php echo $obj->id_miportal_blog ?>').hide();
                            });
                        });

                        $('#button-editseccion-<?php echo $obj->id_miportal_blog ?>').on('click', function () {
                            $("#blogs-seccion-<?php echo $obj->id_miportal_blog ?>").unbind("mouseover");
                            $("#blogs-seccion-<?php echo $obj->id_miportal_blog ?>").unbind("mouseout");
                            $('#blogs-seccion-edit-<?php echo $obj->id_miportal_blog ?>').show();
                            $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').hide();
                        });

                        $("#blogs-seccion-<?php echo $obj->id_miportal_blog ?>").mouseover(function () {
                            $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                            $('#button-editseccion-<?php echo $obj->id_miportal_blog ?>').show();
                            $('#button-delete-<?php echo $obj->id_miportal_blog ?>').show();
                        });

                        $("#blogs-seccion-<?php echo $obj->id_miportal_blog ?>").mouseout(function () {
                            $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>').attr('style', 'padding:11px; border: 0px solid #d9d9d9; opacity:1;');
                            $('#button-editseccion-<?php echo $obj->id_miportal_blog ?>').hide();
                            $('#button-delete-<?php echo $obj->id_miportal_blog ?>').hide();
                        });

                        $('#button-send-form-<?php echo $obj->id_miportal_blog ?>').on('click', function () {
                            if ($("#send-form-<?php echo $obj->id_miportal_blog ?>").validationEngine('validate')) {
                                var descrp = tinymce.get(<?php echo $key + 2 ?>).getContent();
                                if (descrp != '' && descrp != null && descrp.length > 16) {
                                    $('#button-send-form-<?php echo $obj->id_miportal_blog ?>').attr('disabled', 'true');

                                    $.ajax({
                                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_blog',
                                        method: 'POST',
                                        timeout: 4000,
                                        data: {
                                            'titulo': $('#tituloblog-<?php echo $obj->id_miportal_blog ?>').val(),
                                            'descripcion': descrp,
                                            'i': '<?php echo $obj->id_miportal_blog ?>',
                                        },
                                    }).done(function () {
                                        var sup = $('#blogs-seccion-<?php echo $obj->id_miportal_blog ?>');
                                        $(sup.find('.titulo')[0]).text($('#tituloblog-<?php echo $obj->id_miportal_blog ?>').val());
                                        $(sup.find('.descripcion')[0]).html(descrp);
                                        
                                        $('#close-cancel-<?php echo $obj->id_miportal_blog ?>').trigger('click');
                                        $('#button-send-form-<?php echo $obj->id_miportal_blog ?>').removeAttr('disabled');
                                    }).fail(function () {
                                        $('#button-send-form-<?php echo $obj->id_miportal_blog ?>').removeAttr('disabled');
                                    });
                                } else {
                                    alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                }
                            } else {
                                console.log('error');
                            }
                        });

                        $('#button-delete-<?php echo $obj->id_miportal_blog ?>').on('click', function () {
                            var r = confirm('¿Seguro que deseas eliminar este blog?');
                            if (r == true) {
                                $.ajax({
                                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_blog',
                                    method: 'POST',
                                    timeout: 4000,
                                    data: {
                                        'id-blog': '<?php echo $obj->id_miportal_blog ?>',
                                    },
                                }).done(function () {
                                    $('#<?php echo $obj->id_miportal_blog ?>').remove();
                                }).fail(function () {
                                    console.log('error');
                                });
                            }
                        });
                    });
                        </script>
                    <?php } ?>
                    
                    <div id="add_before_blogs"></div>
                    
                    <div class="box">
                        <img src="<?php echo base_url() ?>dist/file-portal/img/edit.png">
                        <br>
                        <button class="waves-effect waves-light btn-flat boton" id="add_blog">A&ntilde;adir entrada</button>
                    </div>


                </div>
                <?php
        }
        ?>
    </div>
</div>
<script type="text" id="template-1" >
    <hr class="hr-separador">
    <div class="myBlog" id="blogs-seccion-" style="display:none; padding:11px;">
    <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
    <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
        <h5 class="titulo"></h5>
        <div class="carrusel">
            <div class="row">
                <div class="col s1"><i class="material-icons">keyboard_arrow_left</i></div>
                <div class="col s10"><img class="imageprincipal" src=""></div>
                <div class="col s1"><i class="material-icons">keyboard_arrow_right</i></div>
            </div>
        </div>
        <div class="descripcion"></div>
        <h6 class="numComentario" data-contador="0"><i class="material-icons">chat_bubble_outline</i> 0 Comentario</h6>
    </div>

    <div class="edit" id="blogs-seccion-edit-">
    <form id="send-form-">
    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-" type="button" ><i class="material-icons">close</i></button>
    <h6><b>T&iacute;tulo*</b></h6>
    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="tituloblog-" value="" placeholder="T&iacute;tulo" />
    <h6><b>Descripci&oacute;n</b></h6>
    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionblog-"></textarea>
    <br><br>
    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-" type="button" >GUARDAR</button>
    <button class="waves-effect waves-light btn-flat" id="close-cancel-" type="button" >Cancelar</button>
    </form>
    <form class="dropzone" drop-zone="" id="dropzone-"></form>
    </div>

    <br>
</script>

<script>
    var count_template = <?php echo count($blogs) + 1; ?>;

    function restartImage(id) {
        var elemento = $('#' + id).find('.dz-image img');
        if (elemento.length == 0) {
            $($('#blogs-seccion-' + id).find('.carrusel')[0]).hide();
        }
        else {
            $($('#blogs-seccion-' + id).find('.carrusel')[0]).show();
            $($('#blogs-seccion-' + id).find('.carrusel .col.s10 img')[0]).attr('src', $(elemento[0]).attr('src'));
        }
    }

    $(document).ready(function () {
        $('#add_blog').on('click', function () {
            count_template++;
            var div = document.createElement("div");
            div.setAttribute('id', count_template);
            div.setAttribute('data-id', 0);
            div.setAttribute('style', 'position:relative;');
            div.innerHTML = $("#template-1").html();

            $($(div).find('#button-editseccion-')[0]).attr('id', 'button-editseccion-' + count_template);
            $($(div).find('#button-delete-')[0]).attr('id', 'button-delete-' + count_template);

            $($(div).find('#blogs-seccion-')[0]).attr('id', 'blogs-seccion-' + count_template);
            $($(div).find('#blogs-seccion-' + count_template)[0]).mouseover(function () {
                var ident = ($(this).attr('id')).split('blogs-seccion-')[1];
                $('#blogs-seccion-' + ident).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('#button-editseccion-' + ident).show();
                $('#button-delete-' + ident).show();
            });
            $($(div).find('#blogs-seccion-' + count_template)[0]).mouseout(function () {
                var ident = ($(this).attr('id')).split('blogs-seccion-')[1];
                $('#blogs-seccion-' + ident).attr('style', 'padding:10px; border: 1px solid #d9d9d9;');
                $('#button-editseccion-' + ident).hide();
                $('#button-delete-' + ident).hide();
            });
            $($(div).find('#blogs-seccion-edit-')[0]).attr('id', 'blogs-seccion-edit-' + count_template);
            $($(div).find('#tituloblog-')[0]).attr('id', 'tituloblog-' + count_template);
            $($(div).find('#descripcionblog-')[0]).attr('id', 'descripcionblog-' + count_template);

            $($(div).find('#dropzone-')[0]).attr('id', 'dropzone-' + count_template);
            $($(div).find('#dropzone-' + count_template)[0]).dropzone({
                url: "<?php echo base_url() ?>index.php/novios/miweb/home/upload_image_blog/0",
                maxFilesize: 3,
                method: "POST",
                paramName: "uploadfile",
                maxThumbnailFilesize: 6,
                acceptedFiles: "image/*",
                addRemoveLinks: true,
                dictCancelUploadConfirmation: "Estas seguro?",
                dictRemoveFile: 'Eliminar imagen',
                dictCancelUpload: 'Cancelar carga',
                dictInvalidFileType: "No se puede cargar este tipo de archivos.",
                dictFileTooBig: "La imagen es muy grande ({{filesize}}MiB). Tama&ntilde;o m&aacute;ximo: {{maxFilesize}}MiB.",
                dictDefaultMessage: '<center><i class="material-icons" style="font-size: 28px;">file_upload</i><br><b>Suelte la imagen o haga click aqu&iacute; para cargar</b></center>',
                init: function () {
                    this.on('success', function (file, json) {
                        var ident = (this.element.id).split('dropzone-')[1];
                        if ($('#' + ident).data('id') != 0) {
                            $.ajax({
                                url: '<?php echo base_url() ?>index.php/novios/miweb/home/insert_relacionimage_blog',
                                method: 'POST',
                                timeout: 4000,
                                data: {
                                    id_blog: $('#' + ident).data('id'),
                                    id_imagen: json.id,
                                }
                            }).done(function () {}).fail(function () {});
                        }
                        $(file.previewElement).data('id', json.id);
                        restartImage(ident);

                    });
                    this.on('addedfile', function () {
                        console.log('addedfile');
                    });
                    this.on('drop', function () {
                        console.log('drop');
                    });
                },
                removedfile: function (file) {
                    var ident = (this.element.id).split('dropzone-')[1];
                    var id = $(file.previewElement).data('id');
                    $.ajax({
                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/upload_image_blog/' + $('#' + ident).data('id'),
                        method: 'POST',
                        timeout: 4000,
                        data: {
                            id: id,
                        }
                    }).done(function () {
                        var _ref;
                        (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

                        restartImage(ident);
                        return true;
                    }).fail(function () {
                        console.log('fail');
                    });
                }
            });

            $($(div).find('#send-form-')[0]).attr('id', 'send-form-' + count_template);
            $($(div).find('#send-form-' + count_template)[0]).validationEngine();

            $($(div).find('#button-send-form-')[0]).attr('id', 'button-send-form-' + count_template);
            $($(div).find('#button-send-form-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('button-send-form-')[1];
                if ($("#send-form-" + ident).validationEngine('validate')) {
                    var descrp = tinymce.get(ident).getContent();
                    if (descrp != '' && descrp != null && descrp.length > 16) {
                        $('#button-send-form-' + ident).attr('disabled', 'true');
                        var imagenes = [];
                        $($('#dropzone-' + ident).find('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete')).each(function () {
                            imagenes.push($(this).data('id'));
                        });
                        $.ajax({
                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/insert_blog',
                            method: 'POST',
                            timeout: 4000,
                            data: {
                                'titulo': $('#tituloblog-' + ident).val(),
                                'descripcion': descrp,
                                'imagenes': imagenes,
                                's': '<?php echo $seccion->id_miportal_seccion ?>',
                            },
                        }).done(function (result) {
                            var sup = $('#blogs-seccion-' + ident);
                            $(sup.find('.titulo')[0]).text($('#tituloblog-' + ident).val());
                            $(sup.find('.descripcion')[0]).html(descrp);
                            
                            $('#' + ident).data('id', result.id);

                            $('#close-edit-' + ident).unbind("click");
                            $('#close-edit-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('close-edit-')[1];
                                $('#blogs-seccion-edit-' + sup_id).hide();
                                $('#blogs-seccion-' + sup_id).show();
                                $("#blogs-seccion-" + sup_id).mouseover(function () {
                                    $('#blogs-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-' + sup_id).show();
                                    $('#button-delete-' + sup_id).show();
                                });

                                $("#blogs-seccion-" + sup_id).mouseout(function () {
                                    $('#blogs-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#button-editseccion-' + sup_id).hide();
                                    $('#button-delete-' + sup_id).hide();
                                });
                            });

                            $('#close-cancel-' + ident).unbind("click");
                            $('#close-cancel-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('close-cancel-')[1];
                                $('#blogs-seccion-edit-' + sup_id).hide();
                                $('#blogs-seccion-' + sup_id).show();
                                $("#blogs-seccion-" + sup_id).mouseover(function () {
                                    $('#blogs-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-' + sup_id).show();
                                    $('#button-delete-' + sup_id).show();
                                });

                                $("#blogs-seccion-" + sup_id).mouseout(function () {
                                    $('#blogs-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#button-editseccion-' + sup_id).hide();
                                    $('#button-delete-' + sup_id).hide();
                                });
                            });

                            $('#button-editseccion-' + ident).show();
                            $('#button-editseccion-' + ident).unbind("click");
                            $('#button-editseccion-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-editseccion-')[1];
                                $("#blogs-seccion-" + sup_id).unbind("mouseover");
                                $("#blogs-seccion-" + sup_id).unbind("mouseout");
                                $('#blogs-seccion-edit-' + sup_id).show();
                                $('#blogs-seccion-' + sup_id).hide();
                            });

                            $('#button-delete-' + ident).unbind("click");
                            $('#button-delete-' + ident).data('id', result.id);
                            $('#button-delete-' + ident).show();
                            $('#button-delete-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-delete-')[1];
                                var r = confirm('¿Seguro que deseas eliminar este blog?');
                                if (r == true) {
                                    $.ajax({
                                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_blog',
                                        method: 'POST',
                                        timeout: 4000,
                                        data: {
                                            'id-blog': $(this).data('id'),
                                        },
                                    }).done(function () {
                                        $('#' + sup_id).remove();
                                    }).fail(function () {
                                        console.log('error');
                                    });
                                }
                            });

                            $('#button-send-form-' + ident).unbind("click");
                            $('#button-send-form-' + ident).data('id', result.id);
                            $('#button-send-form-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];
                                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                                    var descrp = tinymce.get(sup_id).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form-' + sup_id).attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_blog',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'titulo': $('#tituloblog-' + sup_id).val(),
                                                'descripcion': descrp,
                                                'i': $(this).data('id'),
                                            },
                                        }).done(function () {
                                            var sup = $('#blogs-seccion-' + sup_id);
                                            $(sup.find('.titulo')[0]).text($('#tituloblog-' + sup_id).val());
                                            $(sup.find('.descripcion')[0]).html(descrp);
                                            
                                            $('#close-cancel-' + sup_id).trigger('click');
                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });

                            $('#close-cancel-' + ident).trigger('click');
                            $('#button-send-form-' + ident).removeAttr('disabled');
                        }).fail(function () {
                            $('#button-send-form-' + ident).removeAttr('disabled');
                        });
                    } else {
                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                    }
                } else {
                    console.log('error');
                }
            });

            $($(div).find('#close-cancel-')[0]).attr('id', 'close-cancel-' + count_template);
            $($(div).find('#close-cancel-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('close-cancel-')[1];
                $($('#dropzone-' + ident).find('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete')).each(function () {
                    $.ajax({
                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/upload_image_blog',
                        method: 'POST',
                        timeout: 4000,
                        data: {
                            id: $(this).data('id'),
                        }
                    }).done(function () {
                    }).fail(function () {});
                });
                $('#' + ident).remove();
            });


            $($(div).find('#close-edit-')[0]).attr('id', 'close-edit-' + count_template);
            $($(div).find('#close-edit-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('close-edit-')[1];
                $($('#dropzone-' + ident).find('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete')).each(function () {
                    $.ajax({
                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/upload_image_blog',
                        method: 'POST',
                        timeout: 4000,
                        data: {
                            id: $(this).data('id'),
                        }
                    }).done(function () {
                    }).fail(function () {});
                });
                $('#' + ident).remove();
            });

            $('#add_before_blogs').append(div);


            tinymce.init({
                selector: '.tinymce-editor',
                plugins: "autoresize link",
                menubar: '',
                height: "100",
                toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
            });

        });
        
        Dropzone.autoDiscover = false;
    });

</script>
