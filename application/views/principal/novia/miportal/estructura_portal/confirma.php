<!-- B  O  D  Y -->
<div class="container-body">
    <div class="body-container-web detail">
        <?php
        foreach ($miportal_seccion as $key => $value) {
            if ($value->tipo == 'asistencia') {
                ?>
                <div class="confirma" style="padding:11px;">
                    <button class="waves-effect waves-light btn-flat button-edit" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                    <h4 class="titulo"><?php echo $value->subtitulo; ?></h4>
                    <div class="descripcion"><?php echo $value->descripcion; ?></div>
                    <hr class="hr-separador">
                    <form method="post">
                        <div class="row descripcion">
                            <div class="col m6 s12">
                                <p>Nombre</p>
                                <input class="form-portal" name="nombre" type="text">
                            </div>
                            <div class="col m6 s12">
                                <p>E-Mail</p>
                                <input class="form-portal" name="email" type="email" >
                            </div>
                        </div>
                        <button class="waves-effect waves-light btn-flat boton" type="button">Enviar</button>
                    </form>
                </div>
                <div class="confirma-edit edit" style="display:none;">
                    <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                    <form method="post">
                        <br>
                        <input type="hidden" name="id-confirma" value="<?php echo $value->id_miportal_seccion ?>"/>
                        <h6><b>T&iacute;tulo*</b></h6>
                        <input class="formulario-invitaciones" style="width:100%" type="text" name="subtitle-confirma" value="<?php echo $value->subtitulo; ?>" placeholder="T&iacute;tulo" required />
                        <h6><b>Descripci&oacute;n*</b></h6>
                        <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-confirma" id="confirmadescripcion"><?php echo $value->descripcion; ?></textarea>
                        <br>
                        <button class="waves-effect waves-light btn-flat dorado-2 white-text" type="submit" >GUARDAR</button>
                        <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                    </form>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#close-edit').on('click', function () {
            $('.confirma-edit').hide();
            $('.confirma').show();
            $(".confirma").mouseover(function () {
                $('.confirma').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('.button-edit').show();
            });

            $(".confirma").mouseout(function () {
                $('.confirma').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                $('.button-edit').hide();
            });
        });

        $('#close-cancel').on('click', function () {
            $('.confirma-edit').hide();
            $('.confirma').show();
            $(".confirma").mouseover(function () {
                $('.confirma').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('.button-edit').show();
            });

            $(".confirma").mouseout(function () {
                $('.confirma').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                $('.button-edit').hide();
            });
        });

        $('.button-edit').on('click', function () {
            $(".confirma").unbind("mouseover");
            $(".confirma").unbind("mouseout");
            $('.confirma-edit').show();
            $('.confirma').hide();
        });

        $(".confirma").mouseover(function () {
            $('.confirma').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
            $('.button-edit').show();
        });

        $(".confirma").mouseout(function () {
            $('.confirma').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
            $('.button-edit').hide();
        });

    });
</script>