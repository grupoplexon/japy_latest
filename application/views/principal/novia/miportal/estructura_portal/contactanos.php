<!-- B  O  D  Y -->
<div class="container-body">
    <div class="body-container-web detail">
        <?php
        foreach ($miportal_seccion as $key => $value) {
            if ($value->tipo == 'contactar') {
                ?>
                <div class="contactanos" style="padding:11px;">
                    <button class="waves-effect waves-light btn-flat button-edit" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                    <h4 class="titulo"><?php echo $value->subtitulo ?></h4>
                    <div class="descripcion"><?php echo $value->descripcion ?></div>
                    <hr class="hr-separador">
                    <form method="post">
                        <div class="row descripcion">
                            <div class="col m6 s12">
                                <p>Nombre</p>
                                <input class="form-portal" name="nombre" type="text">
                            </div>
                            <div class="col m6 s12">
                                <p>E-Mail</p>
                                <input class="form-portal" name="email" type="email" >
                            </div>
                            <div class="col s12">
                                <p>Asunto</p>
                                <input class="form-portal" name="asunto" type="text" >
                            </div>
                            <div class="col s12">
                                <p>Comentario</p>
                                <textarea class="form-portal" name="comentario"></textarea>
                            </div>
                        </div>
                        <button class="waves-effect waves-light btn-flat boton">Enviar Comentario</button>
                    </form>
                </div>
                <div class="contactanos-edit edit" style="display:none;">
                    <form method="post" id="send-form">
                        <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                        <br>
                        <input type="hidden" name="id-contactar" value="<?php echo $value->id_miportal_seccion ?>" required />
                        <h6><b>T&iacute;tulo*</b></h6>
                        <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="subtitle-contactar" value="<?php echo $value->subtitulo; ?>" placeholder="T&iacute;tulo" />
                        <h6><b>Descripci&oacute;n*</b></h6>
                        <textarea class="materialize-textarea formulario-invitaciones white" name="descripcion-contactar" id="contactardescripcion"><?php echo $value->descripcion; ?></textarea>
                        <br><br>
                        <h6><b>Configuraci&oacute;n de contacto</b></h6>
                        <hr>

                        <!--        E       M       A       I       L        -->
                        <div class="row">
                            <div class="col m4 s12">
                                <h6><b>E-mail de contacto</b></h6>
                            </div>
                            <div class="col m8">
                                <input class="formulario-invitaciones" style="width:100%" type="text" name="email-contactar" value="<?php echo $value->email; ?>" placeholder="" />
                                <small>A&ntilde;ade las direcciones de e-mail separadas por comas donde quieres recibir las alertas</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m4 s12">
                                <h6><b>Recibir alertas</b></h6>
                            </div>
                            <div class="col m8 s12">
                                <div class="row">
                                    <div class="col m3 s6">
                                        <p>
                                            <input name="alert-contactar" type="radio" id="si-contactar" value="1" <?php echo ($value->alerta == 1)?'checked':'' ?> />
                                            <label for="si-contactar">S&iacute;</label>
                                        </p>
                                    </div>
                                    <div class="col m9 s6">
                                        <p>
                                            <input name="alert-contactar" type="radio" id="no-contactar" value="0" <?php echo ($value->alerta == 0)?'checked':'' ?> />
                                            <label for="no-contactar">No</label>
                                        </p>
                                    </div>
                                    <div class="col s12">
                                        <small>Recibe alertas cuando alguien se ponga en contacto a trav&eacute;s del website.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col m4 s12">
                                <h6><b>Tel&eacute;fono</b></h6>
                            </div>
                            <div class="col m8">
                                <input class="formulario-invitaciones validate[custom[phone]]" style="width:100%" type="text" name="telefono-contactar" value="<?php echo $value->telefono; ?>" placeholder="" />
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col m4 s12">
                                <h6><b>Direcci&oacute;n</b></h6>
                            </div>
                            <div class="col m8">
                                <input class="formulario-invitaciones validate[minSize[4]]" style="width:100%" type="text" name="direccion-contactar" value="<?php echo $value->direccion; ?>" placeholder="" />
                            </div>
                        </div>
                        
                        <br>
                        <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                        <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                    </form>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#send-form").validationEngine();
        
        $('#close-edit').on('click', function () {
            $('.contactanos-edit').hide();
            $('.contactanos').show();
            $(".contactanos").mouseover(function () {
                $('.contactanos').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('.button-edit').show();
            });

            $(".contactanos").mouseout(function () {
                $('.contactanos').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                $('.button-edit').hide();
            });
        });

        $('#close-cancel').on('click', function () {
            $('.contactanos-edit').hide();
            $('.contactanos').show();
            $(".contactanos").mouseover(function () {
                $('.contactanos').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('.button-edit').show();
            });

            $(".contactanos").mouseout(function () {
                $('.contactanos').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                $('.button-edit').hide();
            });
        });

        $('.button-edit').on('click', function () {
            tinymce.init({
                selector: 'textarea#contactardescripcion',
                plugins: "autoresize link",
                height : "250",
                menubar: '',
                toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
            });
            $(".contactanos").unbind("mouseover");
            $(".contactanos").unbind("mouseout");
            $('.contactanos-edit').show();
            $('.contactanos').hide();
        });

        $(".contactanos").mouseover(function () {
            $('.contactanos').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
            $('.button-edit').show();
        });

        $(".contactanos").mouseout(function () {
            $('.contactanos').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
            $('.button-edit').hide();
        });
        
        $('#button-send-form').on('click',function(){
            if($("#send-form").validationEngine('validate')){
                var descrp = tinymce.get(1).getContent();
                if(descrp != '' && descrp != null && descrp.length > 16){
                    $('#button-send-form').attr('disabled','true');
                    $("#send-form").submit();
                }
                else{
                    alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                }
            }
            else{
                console.log('error');
            }
            
        });
        
        $('[name="email-contactar"]').tokenfield({
                autocomplete: {
                    source: [],
                    delay: 100
                },
                showAutocompleteOnFocus: true
            });
    });
</script>