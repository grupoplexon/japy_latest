<!-- B  O  D  Y -->
<div class="container-body">
    <div class="body-container-web detail">
        <?php
        $id_seccion = '';
        foreach ($miportal_seccion as $key => $value) {
            if ($value->tipo == 'direccion') { $id_seccion = $value->id_miportal_seccion;
                ?>
                <div class="direcciones">

                    <!-- E  V   E   N   T   O   S -->
                    <div id="direcciones-seccion" style="padding:11px;">
                        <button class="waves-effect waves-light btn-flat button-edit" id="editar-direccionprincipal" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                        <h4 class="titulo"><?php echo $value->titulo ?></h4>
                        <div class="descripcion"><?php echo $value->descripcion ?></div>
                    </div>
                    <div class="edit" id="direcciones-seccion-edit" style="display: none;">
                        <form id="send-form">
                            <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                            <br>
                            <input type="hidden" name="id-direccion" value="<?php echo $value->id_miportal_seccion ?>" required />
                            <h6><b>T&iacute;tulo*</b></h6>
                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="titulo-direccion" value="<?php echo $value->titulo; ?>" placeholder="T&iacute;tulo" />
                            <h6><b>Descripci&oacute;n*</b></h6>
                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-direccion" id="direcciondescripcion"><?php echo $value->descripcion; ?></textarea>
                            <br>
                            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                            <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                        </form>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $("#send-form").validationEngine();

                            $('#close-edit').on('click', function () {
                                $('#direcciones-seccion-edit').hide();
                                $('#direcciones-seccion').show();
                                $("#direcciones-seccion").mouseover(function () {
                                    $('#direcciones-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-direccionprincipal').show();
                                });

                                $("#direcciones-seccion").mouseout(function () {
                                    $('#direcciones-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-direccionprincipal').hide();
                                });
                            });

                            $('#close-cancel').on('click', function () {
                                $('#direcciones-seccion-edit').hide();
                                $('#direcciones-seccion').show();
                                $("#direcciones-seccion").mouseover(function () {
                                    $('#direcciones-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-direccionprincipal').show();
                                });

                                $("#direcciones-seccion").mouseout(function () {
                                    $('#direcciones-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-direccionprincipal').hide();
                                });
                            });

                            $('.button-edit').on('click', function () {
                                $("#direcciones-seccion").unbind("mouseover");
                                $("#direcciones-seccion").unbind("mouseout");
                                $('#direcciones-seccion-edit').show();
                                $('#direcciones-seccion').hide();
                            });

                            $("#direcciones-seccion").mouseover(function () {
                                $('#direcciones-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#editar-direccionprincipal').show();
                            });

                            $("#direcciones-seccion").mouseout(function () {
                                $('#direcciones-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                $('#editar-direccionprincipal').hide();
                            });



                            $('#button-send-form').on('click', function () {
                                if ($("#send-form").validationEngine('validate')) {
                                    var descrp = tinymce.get(1).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form').attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_headerevento',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-evento': $('[name="id-direccion"]').val(),
                                                'titulo-evento': $('[name="titulo-direccion"]').val(),
                                                'descripcion-evento': tinymce.get(1).getContent(),
                                            },
                                        }).done(function () {
                                            $($('#direcciones-seccion').find('.titulo')[0]).text($('[name="titulo-direccion"]').val());
                                            $($('#direcciones-seccion').find('.descripcion')[0]).html(tinymce.get(1).getContent());
                                            $('#close-cancel').trigger('click');
                                            $('#button-send-form').removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form').removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                        });
                    </script>
                    <!--    F  I   N   --  E  V   E   N   T   O   S     -->

                    <?php foreach ($direcciones as $key => $obj) { ?>
                        <div id="<?php echo $obj->id_miportal_direccion ?>" style="position:relative;">
                            <hr class="hr-separador">
                            <div class="misdirecciones direcciones-agregados" id="direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>" style="padding:11px;">
                                <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-<?php echo $obj->id_miportal_direccion ?>" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                                <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-<?php echo $obj->id_miportal_direccion ?>" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
                                <h5 class="titulo"><?php echo $obj->titulo; ?></h5>
                                <p class="telefono"><b><i class="material-icons">phone</i>Tel&eacute;fono: </b> <?php echo $obj->telefono; ?></p>
                                <div class="descripcion"><?php echo $obj->descripcion; ?></div>
                                <div class="direccion-mapa" style="overflow: hidden;">
                                    <iframe
                                        width="100%" height="400"
                                        frameborder="0" style="border:0; margin-top: -100px;"
                                        src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=<?php echo $obj->latitud . ',' . $obj->longitud ?>&zoom=18" allowfullscreen></iframe>
                                </div>
                                <p class="dir-direccion"><b><i class="material-icons">place</i>Direcci&oacute;n: </b><?php echo $obj->direccion . ' ' . $obj->codigo_postal . ' ' . $obj->estado . ', ' . $obj->poblado; ?></p>
                            </div>



                            <div class="edit" id="direcciones-seccion-edit-<?php echo $obj->id_miportal_direccion ?>" style="display:none;" >
                                <form id="send-form-<?php echo $obj->id_miportal_direccion ?>">
                                    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-<?php echo $obj->id_miportal_direccion ?>" type="button" ><i class="material-icons">close</i></button>
                                    <div class="row">
                                        <div class="col m6 s12">
                                            <h6><b>T&iacute;tulo*</b></h6>
                                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="titulodireccion-<?php echo $obj->id_miportal_direccion ?>" value="<?php echo $obj->titulo; ?>" placeholder="T&iacute;tulo" />
                                            <h6><b>Tel&eacute;fono</b></h6>
                                            <input class="formulario-invitaciones validate[custom[phone]]" style="width:100%" type="text" id="telfonodireccion-<?php echo $obj->id_miportal_direccion ?>" value="<?php echo $obj->telefono; ?>" placeholder="Tel&eacute;fono" />
                                            <h6><b>Descripci&oacute;n</b></h6>
                                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripciondireccion-<?php echo $obj->id_miportal_direccion ?>"><?php echo $obj->descripcion; ?></textarea>
                                        </div>



                                        <div class="col m6 s12">
                                            <h6><b>Pa&iacute;s</b></h6>
                                            <select class="browser-default countries" id="countryId-<?php echo $obj->id_miportal_direccion ?>" data-default="<?php echo $obj->pais; ?>" >
                                                <option value="" disabled selected>-- Pa&iacute;s --</option>
                                            </select>

                                            <h6><b>Poblaci&oacute;n</b></h6>
                                            <div class="row">
                                                <div class="col s6">
                                                    <select class="browser-default states" id="stateId-<?php echo $obj->id_miportal_direccion ?>" data-default="<?php echo $obj->estado; ?>" >
                                                        <option value="" disabled selected>-- Estado --</option>
                                                    </select>
                                                </div>
                                                <div class="col s6">
                                                    <select class="browser-default cities" id="cityId-<?php echo $obj->id_miportal_direccion ?>" data-default="<?php echo $obj->poblado; ?>" >
                                                        <option value="" disabled selected>-- Poblaci&oacute;n --</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <h6><b>C&oacute;digo Postal</b></h6>
                                            <input class="formulario-invitaciones validate[custom[number]]" style="width:100%" type="text" id="codigopostaldireccion-<?php echo $obj->id_miportal_direccion ?>" value="<?php echo $obj->codigo_postal; ?>" placeholder="C&oacute;digo Postal" />

                                            <h6><b>Direcci&oacute;n*</b></h6>
                                            <div class="row">
                                                <div class="col s12">
                                                    <input class="formulario-invitaciones validate[required,min[4]]" style="width:100%" type="text" id="direcciondireccion-<?php echo $obj->id_miportal_direccion ?>" value="<?php echo $obj->direccion; ?>" placeholder="Direcci&oacute;n" />
                                                    <input type="hidden" id="latituddireccion-<?php echo $obj->id_miportal_direccion ?>" value="<?php echo $obj->latitud; ?>" />
                                                    <input type="hidden" id="longituddireccion-<?php echo $obj->id_miportal_direccion ?>" value="<?php echo $obj->longitud; ?>" />
                                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="updateMap-<?php echo $obj->id_miportal_direccion ?>" type="button" >Actualizar Mapa</button>
                                                </div>
                                                <div class="col s12">
                                                    <div class="direccion-mapa" style="width:auto; height: 200px" id="imagemap-<?php echo $obj->id_miportal_direccion ?>"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-<?php echo $obj->id_miportal_direccion ?>" type="button" >GUARDAR</button>
                                    <button class="waves-effect waves-light btn-flat" id="close-cancel-<?php echo $obj->id_miportal_direccion ?>" type="button" >Cancelar</button>
                                </form>
                            </div>


                            <script>
                                $(document).ready(function () {
                                    var _LOC = new locationInfo('cityId-<?php echo $obj->id_miportal_direccion ?>', 'stateId-<?php echo $obj->id_miportal_direccion ?>', 'countryId-<?php echo $obj->id_miportal_direccion ?>');

                                    $("#send-form-<?php echo $obj->id_miportal_direccion ?>").validationEngine();

                                    $('#close-edit-<?php echo $obj->id_miportal_direccion ?>').on('click', function () {
                                        $('#direcciones-seccion-edit-<?php echo $obj->id_miportal_direccion ?>').hide();
                                        $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').show();
                                        $("#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>").mouseover(function () {
                                            $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_direccion ?>').show();
                                            $('#button-delete-<?php echo $obj->id_miportal_direccion ?>').show();
                                        });

                                        $("#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>").mouseout(function () {
                                            $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_direccion ?>').hide();
                                            $('#button-delete-<?php echo $obj->id_miportal_direccion ?>').hide();
                                        });
                                    });

                                    $('#close-cancel-<?php echo $obj->id_miportal_direccion ?>').on('click', function () {
                                        $('#direcciones-seccion-edit-<?php echo $obj->id_miportal_direccion ?>').hide();
                                        $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').show();
                                        $("#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>").mouseover(function () {
                                            $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');

                                            $('#button-editseccion-<?php echo $obj->id_miportal_direccion ?>').show();
                                            $('#button-delete-<?php echo $obj->id_miportal_direccion ?>').show();
                                        });

                                        $("#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>").mouseout(function () {
                                            $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');

                                            $('#button-editseccion-<?php echo $obj->id_miportal_direccion ?>').hide();
                                            $('#button-delete-<?php echo $obj->id_miportal_direccion ?>').hide();
                                        });
                                    });

                                    $('#button-editseccion-<?php echo $obj->id_miportal_direccion ?>').on('click', function () {
                                        $("#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>").unbind("mouseover");
                                        $("#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>").unbind("mouseout");
                                        $('#direcciones-seccion-edit-<?php echo $obj->id_miportal_direccion ?>').show();
                                        $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').hide();

                                        $('#updateMap-<?php echo $obj->id_miportal_direccion ?>').click();
                                    });

                                    $("#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>").mouseover(function () {
                                        $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_direccion ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_direccion ?>').show();
                                    });

                                    $("#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>").mouseout(function () {
                                        $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_direccion ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_direccion ?>').hide();
                                    });
                                    /*   -   -   -   -  M       A       P      A     -   I     N   I   C   I   O       -   -   -   - */

                                    $('#updateMap-<?php echo $obj->id_miportal_direccion ?>').on('click', function () {
                                        var address = $('#direcciondireccion-<?php echo $obj->id_miportal_direccion ?>').val();
                                        var geocoder = new google.maps.Geocoder();
                                        geocoder.geocode({'address': address}, geocodeResult<?php echo $obj->id_miportal_direccion ?>);
                                    });

                                    $('#direcciondireccion-<?php echo $obj->id_miportal_direccion ?>').on('change', function () {
                                        var address = $(this).val();
                                        var geocoder = new google.maps.Geocoder();
                                        geocoder.geocode({'address': address}, geocodeResult<?php echo $obj->id_miportal_direccion ?>);
                                    });

                                    var myLatlng = new google.maps.LatLng(<?php echo $obj->latitud; ?>, <?php echo $obj->longitud; ?>);
                                    var myOptions = {
                                        zoom: 4,
                                        center: myLatlng,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };
                                    var temp = new google.maps.Map($("#imagemap-<?php echo $obj->id_miportal_direccion ?>").get(0), myOptions);

                                    function geocodeResult<?php echo $obj->id_miportal_direccion ?>(results, status) {
                                        if (status == 'OK') {
                                            var mapOptions = {
                                                center: results[0].geometry.location,
                                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                            };
                                            var maps = new google.maps.Map($("#imagemap-<?php echo $obj->id_miportal_direccion ?>").get(0), mapOptions);
                                            maps.fitBounds(results[0].geometry.viewport);
                                            var markerOptions = {position: results[0].geometry.location, draggable: true};
                                            var marker = new google.maps.Marker(markerOptions);
                                            var markerLatLng = marker.getPosition();
                                            $('#latituddireccion-<?php echo $obj->id_miportal_direccion ?>').val(markerLatLng.lat());
                                            $('#longituddireccion-<?php echo $obj->id_miportal_direccion ?>').val(markerLatLng.lng());
                                            marker.setMap(maps);
                                        } else {
                                            alert("Direccion invalida ");
                                        }
                                    }
                                    ;



                                    /*   -   -   -   -      F       I       N       -   -   -   - */

                                    $('#button-send-form-<?php echo $obj->id_miportal_direccion ?>').on('click', function () {
                                        if ($("#send-form-<?php echo $obj->id_miportal_direccion ?>").validationEngine('validate')) {
                                            var descrp = tinymce.get(<?php echo $key + 2 ?>).getContent();
                                            if (descrp != '' && descrp != null && descrp.length > 16) {
                                                $('#button-send-form-<?php echo $obj->id_miportal_direccion ?>').attr('disabled', 'true');
                                                
                                                $.ajax({
                                                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_direccion',
                                                    method: 'POST',
                                                    timeout: 4000,
                                                    data: {
                                                        'titulo': $('#titulodireccion-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'descripcion': descrp,
                                                        'telefono': $('#telfonodireccion-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'pais': $('#countryId-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'estado': $('#stateId-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'poblado': $('#cityId-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'codigo_postal': $('#codigopostaldireccion-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'direccion': $('#direcciondireccion-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'latitud': $('#latituddireccion-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'longitud': $('#longituddireccion-<?php echo $obj->id_miportal_direccion ?>').val(),
                                                        'i': '<?php echo $obj->id_miportal_direccion ?>',
                                                    },
                                                }).done(function () {
                                                    var sup = $('#direcciones-seccion-<?php echo $obj->id_miportal_direccion ?>');
                                                    $(sup.find('.titulo')[0]).text($('#titulodireccion-<?php echo $obj->id_miportal_direccion ?>').val());
                                                    $(sup.find('.descripcion')[0]).html(descrp);
                                                    $(sup.find('.telefono')[0]).html('<b><i class="material-icons">phone</i>Tel&eacute;fono: </b>' + $('#telfonodireccion-<?php echo $obj->id_miportal_direccion ?>').val());
                                                    $(sup.find('.direccion-mapa')[0]).html('<iframe ' +
                                                            'width="100%" height="400" ' +
                                                            'frameborder="0" style="border:0; margin-top: -100px;" ' +
                                                            'src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=' + $('#latituddireccion-<?php echo $obj->id_miportal_direccion ?>').val() + ',' + $('#longituddireccion-<?php echo $obj->id_miportal_direccion ?>').val() + '&zoom=18" allowfullscreen></iframe>');
                                                    $(sup.find('.dir-direccion')[0]).html('<b><i class="material-icons">place</i>Direcci&oacute;n: </b>' + $('#direcciondireccion-<?php echo $obj->id_miportal_direccion ?>').val() + ' ' + $('#codigopostaldireccion-<?php echo $obj->id_miportal_direccion ?>').val() + ' ' + $('#stateId-<?php echo $obj->id_miportal_direccion ?>').val() + ' ' + $('#cityId-<?php echo $obj->id_miportal_direccion ?>').val());

                                                    $('#close-cancel-<?php echo $obj->id_miportal_direccion ?>').trigger('click');
                                                    $('#button-send-form-<?php echo $obj->id_miportal_direccion ?>').removeAttr('disabled');
                                                }).fail(function () {
                                                    $('#button-send-form-<?php echo $obj->id_miportal_direccion ?>').removeAttr('disabled');
                                                });
                                            } else {
                                                alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                            }
                                        } else {
                                            console.log('error');
                                        }
                                    });

                                    $('#button-delete-<?php echo $obj->id_miportal_direccion ?>').on('click', function () {
                                        var r = confirm('¿Seguro que deseas eliminar esta direccion?');
                                        if (r == true) {
                                            $.ajax({
                                                url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_direccion',
                                                method: 'POST',
                                                timeout: 4000,
                                                data: {
                                                    'id-direccion': '<?php echo $obj->id_miportal_direccion ?>',
                                                },
                                            }).done(function () {
                                                $('#<?php echo $obj->id_miportal_direccion ?>').remove();
                                            }).fail(function () {
                                                console.log('error');
                                            });
                                        }
                                    });
                                });
                            </script>
                        </div>

                    <?php } ?>

                    <div id="add_before_direccion"></div>   

                    <div class="box">
                        <img src="<?php echo base_url() ?>dist/file-portal/img/maps.png">
                        <br>
                        <button class="waves-effect waves-light btn-flat boton" id="add_direccion">A&ntilde;adir direcci&oacute;n</button>
                    </div>


                </div>
                <?php
            }
        }
        ?>
    </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key'); ?>&amp;libraries=adsense&amp;sensor=true&amp;language=es"></script>
<script type="text" id="template-1" >
    <hr class="hr-separador">
    <div class="misdirecciones direcciones-agregados" id="direcciones-seccion-" style="display:none; padding:11px;>
    <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
    <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
    <h5 class="titulo"></h5>
    <p class="telefono"><b><i class="material-icons">phone</i>Tel&eacute;fono: </b> </p>
    <div class="descripcion"></div>
    <div class="direccion-mapa" style="overflow: hidden;"></div>
    <p class="dir-direccion"><b><i class="material-icons">place</i>Direcci&oacute;n: </b></p>
    </div>

    <div class="edit" id="direcciones-seccion-edit-" >
    <form id="send-form-">
    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-" type="button" ><i class="material-icons">close</i></button>
    <div class="row">
    <div class="col m6 s12">
    <h6><b>T&iacute;tulo*</b></h6>
    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="titulodireccion-" value="" placeholder="T&iacute;tulo" />
    <h6><b>Tel&eacute;fono</b></h6>
    <input class="formulario-invitaciones validate[custom[phone]]" style="width:100%" type="text" id="telfonodireccion-" value="" placeholder="Tel&eacute;fono" />
    <h6><b>Descripci&oacute;n</b></h6>
    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripciondireccion-"></textarea>
    </div>



    <div class="col m6 s12">
    <h6><b>Pa&iacute;s</b></h6>
    <select class="browser-default countries" id="countryId-" data-default="Mexico" >
    <option value="" disabled selected>-- Pa&iacute;s --</option>
    </select>

    <h6><b>Poblaci&oacute;n</b></h6>
    <div class="row">
    <div class="col s6">
    <select class="browser-default states" id="stateId-" >
    <option value="" disabled selected>-- Estado --</option>
    </select>
    </div>
    <div class="col s6">
    <select class="browser-default cities" id="cityId-" >
    <option value="" disabled selected>-- Poblaci&oacute;n --</option>
    </select>
    </div>
    </div>

    <h6><b>C&oacute;digo Postal</b></h6>
    <input class="formulario-invitaciones validate[custom[number]]" style="width:100%" type="text" id="codigopostaldireccion-" value="" placeholder="C&oacute;digo Postal" />

    <h6><b>Direcci&oacute;n*</b></h6>
    <div class="row">
    <div class="col s12">
    <input class="formulario-invitaciones validate[required,min[4]]" style="width:100%" type="text" id="direcciondireccion-" value="" placeholder="Direcci&oacute;n" />
    <input type="hidden" id="latituddireccion-" value="" />
    <input type="hidden" id="longituddireccion-" value="" />
    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="updateMap-" type="button" >Actualizar Mapa</button>
    </div>
    <div class="col s12">
    <div class="direccion-mapa" style="width:auto; height: 200px" id="imagemap-"></div>
    </div>
    </div>
    </div>
    </div>
    <br>
    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-" type="button" >GUARDAR</button>
    <button class="waves-effect waves-light btn-flat" id="close-cancel-" type="button" >Cancelar</button>
    </form>
    </div>
</script>
<script>
                    var count_template = <?php echo count($direcciones) + 1; ?>;

                    $(document).ready(function () {
                        $('#add_direccion').on('click', function () {
                            count_template++;
                            var div = document.createElement("div");
                            div.setAttribute('id', count_template);
                            div.setAttribute('style','position:relative;');
                            div.innerHTML = $("#template-1").html();

                            $($(div).find('#button-editseccion-')[0]).attr('id', 'button-editseccion-' + count_template);
                            $($(div).find('#button-delete-')[0]).attr('id', 'button-delete-' + count_template);
                            $($(div).find('#direcciones-seccion-')[0]).attr('id', 'direcciones-seccion-' + count_template);
                            $($(div).find('#direcciones-seccion-edit-')[0]).attr('id', 'direcciones-seccion-edit-' + count_template);
                            $($(div).find('#titulodireccion-')[0]).attr('id', 'titulodireccion-' + count_template);
                            $($(div).find('#telfonodireccion-')[0]).attr('id', 'telfonodireccion-' + count_template);
                            $($(div).find('#descripciondireccion-')[0]).attr('id', 'descripciondireccion-' + count_template);
                            $($(div).find('#countryId-')[0]).attr('id', 'countryId-' + count_template);
                            $($(div).find('#stateId-')[0]).attr('id', 'stateId-' + count_template);
                            $($(div).find('#cityId-')[0]).attr('id', 'cityId-' + count_template);
                            $($(div).find('#codigopostaldireccion-')[0]).attr('id', 'codigopostaldireccion-' + count_template);
                            $($(div).find('#latituddireccion-')[0]).attr('id', 'latituddireccion-' + count_template);
                            $($(div).find('#longituddireccion-')[0]).attr('id', 'longituddireccion-' + count_template);
                            $($(div).find('#imagemap-')[0]).attr('id', 'imagemap-' + count_template);


                            $($(div).find('#send-form-')[0]).attr('id', 'send-form-' + count_template);
                            $($(div).find('#send-form-' + count_template)[0]).validationEngine();


                            $($(div).find('#direcciondireccion-')[0]).attr('id', 'direcciondireccion-' + count_template);
                            $($(div).find('#direcciondireccion-' + count_template)[0]).on('change', function () {
                                var ident = ($(this).attr('id')).split('direcciondireccion-')[1];
                                var address = $(this).val();
                                var geocoder = new google.maps.Geocoder();
                                geocoder.geocode({'address': address}, function (results, status) {
                                    if (status == 'OK') {
                                        var mapOptions = {
                                            center: results[0].geometry.location,
                                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        };
                                        var maps = new google.maps.Map($("#imagemap-" + ident).get(0), mapOptions);
                                        maps.fitBounds(results[0].geometry.viewport);
                                        var markerOptions = {position: results[0].geometry.location, draggable: true};
                                        var marker = new google.maps.Marker(markerOptions);
                                        var markerLatLng = marker.getPosition();
                                        $('#latituddireccion-' + ident).val(markerLatLng.lat());
                                        $('#longituddireccion-' + ident).val(markerLatLng.lng());
                                        marker.setMap(maps);
                                    } else {
                                        alert("Direccion invalida ");
                                }
                            });
                                });


                            $($(div).find('#updateMap-')[0]).attr('id', 'updateMap-' + count_template);
                            $($(div).find('#updateMap-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('updateMap-')[1];
                                var address = $('#direcciondireccion-' + ident).val();
                                var geocoder = new google.maps.Geocoder();
                                geocoder.geocode({'address': address}, function (results, status) {
                                    if (status == 'OK') {
                                        var mapOptions = {
                                            center: results[0].geometry.location,
                                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        };
                                        var maps = new google.maps.Map($("#imagemap-" + ident).get(0), mapOptions);
                                        maps.fitBounds(results[0].geometry.viewport);
                                        var markerOptions = {position: results[0].geometry.location, draggable: true};
                                        var marker = new google.maps.Marker(markerOptions);
                                        var markerLatLng = marker.getPosition();
                                        $('#latituddireccion-' + ident).val(markerLatLng.lat());
                                        $('#longituddireccion-' + ident).val(markerLatLng.lng());
                                        marker.setMap(maps);
                                    } else {
                                        alert("Direccion invalida ");
                                    }
                                });
                            });


                            $($(div).find('#button-send-form-')[0]).attr('id', 'button-send-form-' + count_template);
                            $($(div).find('#button-send-form-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('button-send-form-')[1];
                                if ($("#send-form-" + ident).validationEngine('validate')) {
                                    var descrp = tinymce.get(ident).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form-' + ident).attr('disabled', 'true');
                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/insert_direccion',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'titulo': $('#titulodireccion-' + ident).val(),
                                                'descripcion': descrp,
                                                'telefono': $('#telfonodireccion-' + ident).val(),
                                                'pais': $('#countryId-' + ident).val(),
                                                'estado': $('#stateId-' + ident).val(),
                                                'poblado': $('#cityId-' + ident).val(),
                                                'codigo_postal': $('#codigopostaldireccion-' + ident).val(),
                                                'direccion': $('#direcciondireccion-' + ident).val(),
                                                'latitud': $('#latituddireccion-' + ident).val(),
                                                'longitud': $('#longituddireccion-' + ident).val(),
                                                's': '<?php echo $id_seccion ?>',
                                            },
                                        }).done(function (result) {
                                            var sup = $('#direcciones-seccion-' + ident);
                                            $(sup.find('.titulo')[0]).text($('#titulodireccion-' + ident).val());
                                            $(sup.find('.descripcion')[0]).html(descrp);
                                            $(sup.find('.telefono')[0]).html('<b><i class="material-icons">phone</i>Tel&eacute;fono: </b>' + $('#telfonodireccion-' + ident).val());
                                            $(sup.find('.direccion-mapa')[0]).html('<iframe ' +
                                                    'width="100%" height="400" ' +
                                                    'frameborder="0" style="border:0; margin-top: -100px;" ' +
                                                    'src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=' + $('#latituddireccion-' + ident).val() + ',' + $('#longituddireccion-' + ident).val() + '&zoom=18" allowfullscreen></iframe>');
                                            $(sup.find('.dir-direccion')[0]).html('<b><i class="material-icons">place</i>Direcci&oacute;n: </b>' + $('#direcciondireccion-' + ident).val() + ' ' + $('#codigopostaldireccion-' + ident).val() + ' ' + $('#stateId-' + ident).val() + ' ' + $('#cityId-' + ident).val());

                                            $('#close-edit-' + ident).unbind("click");
                                            $('#close-edit-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('close-edit-')[1];
                                                $('#direcciones-seccion-edit-' + sup_id).hide();
                                                $('#direcciones-seccion-' + sup_id).show();
                                                $("#direcciones-seccion-" + sup_id).mouseover(function () {
                                                    $('#direcciones-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                    $('#button-editseccion-' + sup_id).show();
                                                    $('#button-delete-' + sup_id).show();
                                                });

                                                $("#direcciones-seccion-" + sup_id).mouseout(function () {
                                                    $('#direcciones-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                    $('#button-editseccion-' + sup_id).hide();
                                                    $('#button-delete-' + sup_id).hide();
                                                });
                                            });

                                            $('#close-cancel-' + ident).unbind("click");
                                            $('#close-cancel-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('close-cancel-')[1];
                                                $('#direcciones-seccion-edit-' + sup_id).hide();
                                                $('#direcciones-seccion-' + sup_id).show();
                                                $("#direcciones-seccion-" + sup_id).mouseover(function () {
                                                    $('#direcciones-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                    /*$('#editar-direccion-' + sup_id).show();*/
                                                    $('#button-editseccion-' + sup_id).show();
                                                    $('#button-delete-' + sup_id).show();
                                                });

                                                $("#direcciones-seccion-" + sup_id).mouseout(function () {
                                                    $('#direcciones-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                    /*$('#editar-direccion-' + sup_id).hide();*/
                                                    $('#button-editseccion-' + sup_id).hide();
                                                    $('#button-delete-' + sup_id).hide();
                                                });
                                            });
                                            
                                            
                                            $('#button-editseccion-' + ident).show();
                                            $('#button-editseccion-' + ident).unbind("click");
                                            $('#button-editseccion-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('button-editseccion-')[1];
                                                $("#direcciones-seccion-" + sup_id).unbind("mouseover");
                                                $("#direcciones-seccion-" + sup_id).unbind("mouseout");
                                                $('#direcciones-seccion-edit-' + sup_id).show();
                                                $('#direcciones-seccion-' + sup_id).hide();
                                            });
                                            

                                            $("#direcciones-seccion-" + ident).mouseover(function () {
                                                var sup_id = ($(this).attr('id')).split('direcciones-seccion-')[1];
                                                $('#direcciones-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                $('#button-editseccion-' + sup_id).show();
                                                $('#button-delete-' + sup_id).show();
                                            });

                                            $("#direcciones-seccion-" + ident).mouseout(function () {
                                                var sup_id = ($(this).attr('id')).split('direcciones-seccion-')[1];
                                                $('#direcciones-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                $('#button-editseccion-' + sup_id).hide();
                                                $('#button-delete-' + sup_id).hide();
                                            });

                                            $('#button-delete-' + ident).unbind("click");
                                            $('#button-delete-' + ident).data('id', result.id);
                                            $('#button-delete-' + ident).show();
                                            $('#button-delete-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('button-delete-')[1];

                                                var r = confirm('¿Seguro que deseas eliminar este direccion?');
                                                if (r == true) {
                                                    $.ajax({
                                                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_direccion',
                                                        method: 'POST',
                                                        timeout: 4000,
                                                        data: {
                                                            'id-direccion': $(this).data('id'),
                                                        },
                                                    }).done(function () {
                                                        $('#' + sup_id).remove();
                                                    }).fail(function () {
                                                        console.log('error');
                                                    });
                                                }
                                            });

                                            $('#button-send-form-' + ident).unbind("click");
                                            $('#button-send-form-' + ident).data('id', result.id);
                                            $('#button-send-form-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];

                                                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                                                    var descrp = tinymce.get(sup_id).getContent();
                                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                                        $('#button-send-form-' + sup_id).attr('disabled', 'true');

                                                        $.ajax({
                                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_direccion',
                                                            method: 'POST',
                                                            timeout: 4000,
                                                            data: {
                                                                'titulo': $('#titulodireccion-' + sup_id).val(),
                                                                'descripcion': descrp,
                                                                'telefono': $('#telfonodireccion-' + sup_id).val(),
                                                                'pais': $('#countryId-' + sup_id).val(),
                                                                'estado': $('#stateId-' + sup_id).val(),
                                                                'poblado': $('#cityId-' + sup_id).val(),
                                                                'codigo_postal': $('#codigopostaldireccion-' + sup_id).val(),
                                                                'direccion': $('#direcciondireccion-' + sup_id).val(),
                                                                'latitud': $('#latituddireccion-' + sup_id).val(),
                                                                'longitud': $('#longituddireccion-' + sup_id).val(),
                                                                'i': $(this).data('id'),
                                                            },
                                                        }).done(function () {
                                                            var sup = $('#direcciones-seccion-' + sup_id);
                                                            $(sup.find('.titulo')[0]).text($('#titulodireccion-' + sup_id).val());
                                                            $(sup.find('.descripcion')[0]).html(descrp);
                                                            $(sup.find('.telefono')[0]).html('<b><i class="material-icons">phone</i>Tel&eacute;fono: </b>' + $('#telfonodireccion-' + sup_id).val());
                                                            $(sup.find('.direccion-mapa')[0]).html('<iframe ' +
                                                                    'width="100%" height="400" ' +
                                                                    'frameborder="0" style="border:0; margin-top: -100px;" ' +
                                                                    'src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=' + $('#latituddireccion-' + sup_id).val() + ',' + $('#longituddireccion-' + sup_id).val() + '&zoom=18" allowfullscreen></iframe>');
                                                            $(sup.find('.dir-direccion')[0]).html('<b><i class="material-icons">place</i>Direcci&oacute;n: </b>' + $('#direcciondireccion-' + sup_id).val() + ' ' + $('#codigopostaldireccion-' + sup_id).val() + ' ' + $('#stateId-' + sup_id).val() + ' ' + $('#cityId-' + sup_id).val());

                                                            $('#close-cancel-' + sup_id).trigger('click');
                                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                                        }).fail(function () {
                                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                                        });
                                                    } else {
                                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                                    }
                                                } else {
                                                    console.log('error');
                                                }
                                            });

                                            $('#close-cancel-' + ident).trigger('click');
                                            $('#button-send-form-' + ident).removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form-' + ident).removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('validacion no aprobada');
                                }
                            });


                            $($(div).find('#close-cancel-')[0]).attr('id', 'close-cancel-' + count_template);
                            $($(div).find('#close-cancel-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('close-cancel-')[1];
                                $('#' + ident).remove();
                            });


                            $($(div).find('#close-edit-')[0]).attr('id', 'close-edit-' + count_template);
                            $($(div).find('#close-edit-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('close-edit-')[1];
                                $('#' + ident).remove();
                            });

                            $('#add_before_direccion').append(div);

                            var myLatlng = new google.maps.LatLng(20.6508562, -103.4016944);
                            var myOptions = {
                                zoom: 4,
                                center: myLatlng,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

                            var temp = new google.maps.Map($("#imagemap-" + count_template).get(0), myOptions);

                            $('.datepicker').datepicker({
                                minDate: new Date(),
                                altFormat: "yy-mm-dd",
                                dateFormat: "yy-mm-dd",
                                closeText: "Cerrar",
                                changeYear: true,
                                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                                dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            });

                            var _LOC = new locationInfo('cityId-' + count_template, 'stateId-' + count_template, 'countryId-' + count_template);

                            tinymce.init({
                                selector: '.tinymce-editor',
                                plugins: "autoresize link",
                                menubar: '',
                                height : "250",
                                toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
                            });
                        });
                    });
</script>