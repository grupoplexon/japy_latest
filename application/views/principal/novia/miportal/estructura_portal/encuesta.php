<!-- B  O  D  Y -->
<div class="container-body">
    <div class="body-container-web detail">
        <?php
        foreach ($miportal_seccion as $key => $value) {
            if ($value->tipo == 'encuesta') {
                ?>
                <div class="encuesta">


                    <div id="encuestas-seccion" style="padding:11px;">
                        <button class="waves-effect waves-light btn-flat button-edit" id="editar-encuestasprincipal" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                        <h4 class="titulo"><?php echo $value->titulo ?></h4>
                        <div class="descripcion"><?php echo $value->descripcion ?></div>
                    </div>
                    <div class="edit" id="encuestas-seccion-edit" style="display: none;">
                        <form id="send-form">
                            <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                            <br>
                            <input type="hidden" name="id-encuestas" value="<?php echo $value->id_miportal_seccion ?>" required />
                            <h6><b>T&iacute;tulo*</b></h6>
                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="titulo-encuestas" value="<?php echo $value->titulo; ?>" placeholder="T&iacute;tulo" />
                            <h6><b>Descripci&oacute;n*</b></h6>
                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-encuesta" id="encuestasdescripcion"><?php echo $value->descripcion; ?></textarea>
                            <br>
                            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                            <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                        </form>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $("#send-form").validationEngine();

                            $('#close-edit').on('click', function () {
                                $('#encuestas-seccion-edit').hide();
                                $('#encuestas-seccion').show();
                                $("#encuestas-seccion").mouseover(function () {
                                    $('#encuestas-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-encuestasprincipal').show();
                                });

                                $("#encuestas-seccion").mouseout(function () {
                                    $('#encuestas-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-encuestasprincipal').hide();
                                });
                            });

                            $('#close-cancel').on('click', function () {
                                $('#encuestas-seccion-edit').hide();
                                $('#encuestas-seccion').show();
                                $("#encuestas-seccion").mouseover(function () {
                                    $('#encuestas-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-encuestasprincipal').show();
                                });

                                $("#encuestas-seccion").mouseout(function () {
                                    $('#encuestas-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-encuestasprincipal').hide();
                                });
                            });

                            $('.button-edit').on('click', function () {
                                $("#encuestas-seccion").unbind("mouseover");
                                $("#encuestas-seccion").unbind("mouseout");
                                $('#encuestas-seccion-edit').show();
                                $('#encuestas-seccion').hide();
                            });

                            $("#encuestas-seccion").mouseover(function () {
                                $('#encuestas-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#editar-encuestasprincipal').show();
                            });

                            $("#encuestas-seccion").mouseout(function () {
                                $('#encuestas-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                $('#editar-encuestasprincipal').hide();
                            });



                            $('#button-send-form').on('click', function () {
                                if ($("#send-form").validationEngine('validate')) {
                                    var descrp = tinymce.get(1).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form').attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_headerevento',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-evento': $('[name="id-encuestas"]').val(),
                                                'titulo-evento': $('[name="titulo-encuestas"]').val(),
                                                'descripcion-evento': tinymce.get(1).getContent(),
                                            },
                                        }).done(function () {
                                            $($('#encuestas-seccion').find('.titulo')[0]).text($('[name="titulo-encuestas"]').val());
                                            $($('#encuestas-seccion').find('.descripcion')[0]).html(tinymce.get(1).getContent());
                                            $('#close-cancel').trigger('click');
                                            $('#button-send-form').removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form').removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                        });
                    </script>

                    <?php foreach ($encuestas as $key => $obj) { ?>
                        <div id="<?php echo $obj->id_miportal_encuesta ?>" style="position:relative;">
                            <hr class="hr-separador">
                            <div class="encuestas-agregados" id="encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>" style="padding:11px;">
                                <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-<?php echo $obj->id_miportal_encuesta ?>" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                                <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-<?php echo $obj->id_miportal_encuesta ?>" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
                                <h5 class="titulo" style="text-align:left;"><?php echo $obj->titulo; ?></h5>
                                <div class="descripcion"><?php echo $obj->descripcion; ?></div>
                                <div id="grafica-<?php echo $obj->id_miportal_encuesta ?>" style="width:90%; height: 250px;"></div>
                            </div>
                            <div class="edit" id="encuestas-seccion-edit-<?php echo $obj->id_miportal_encuesta ?>" style="display:none;" >
                                <form id="send-form-<?php echo $obj->id_miportal_encuesta ?>">
                                    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-<?php echo $obj->id_miportal_encuesta ?>" type="button" ><i class="material-icons">close</i></button>

                                    <h6><b>T&iacute;tulo*</b></h6>
                                    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="tituloencuesta-<?php echo $obj->id_miportal_encuesta ?>" value="<?php echo $obj->titulo; ?>" placeholder="T&iacute;tulo" />
                                    <h6><b>Descripci&oacute;n</b></h6>
                                    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionencuesta-<?php echo $obj->id_miportal_encuesta ?>"><?php echo $obj->descripcion; ?></textarea>
                                    <br><br>
                                    <?php foreach ($obj->opciones as $i => $opcion) { ?>
                                        <div class="row">
                                            <div class="col s3">Respuesta</div>
                                            <div class="col s7">
                                                <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" name="option-<?php echo $obj->id_miportal_encuesta ?>" type="text" id="option-<?php echo encrypt($opcion->id_miportal_encuesta_opcion) ?>" data-id="<?php echo encrypt($opcion->id_miportal_encuesta_opcion) ?>" data-contador="<?php echo $opcion->contador ?>" value="<?php echo $opcion->texto; ?>" />
                                            </div>
                                            <div class="col s2">
                                                <?php if ($i > 1) { ?>
                                                    <button class="waves-effect waves-light btn-flat deleteopcion" data-id="<?php echo encrypt($opcion->id_miportal_encuesta_opcion) ?>" data-encuesta="<?php echo $obj->id_miportal_encuesta ?>" data-elemento="<?php echo $obj->id_miportal_encuesta ?>" type="button"><i class="material-icons">delete_forever</i></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div id="new_option-<?php echo $obj->id_miportal_encuesta ?>"></div>


                                    <button class="waves-effect waves-light btn-flat orange darken-1 white-text" id="add-option-<?php echo $obj->id_miportal_encuesta ?>" type="button" >Agregar pregunta</button>
                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-<?php echo $obj->id_miportal_encuesta ?>" type="button" >GUARDAR</button>
                                    <button class="waves-effect waves-light btn-flat" id="close-cancel-<?php echo $obj->id_miportal_encuesta ?>" type="button" >Cancelar</button>
                                </form>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                Highcharts.setOptions({
                                    colors: ['#424242', '#4e342e', '#afb42b', '#d32f2f', '#c2185b', '#5e35b1', '#0288d1', '#00796b', '#f9a797', '#00838f', '#2e7d32', '#f57c00']
                                });

                                $('#grafica-<?php echo $obj->id_miportal_encuesta ?>').highcharts({
                                    credits: {
                                        enabled: false,
                                    },
                                    chart: {
                                        plotBackgroundColor: null,
                                        plotBorderWidth: null,
                                        plotShadow: false,
                                        type: 'pie'
                                    },
                                    legend: {
                                        layout: 'vertical',
                                        align: 'right',
                                        verticalAlign: 'top',
                                        floating: true,
                                        backgroundColor: '#FFFFFF',
                                        lineHeight: 25,
                                    },
                                    title: {
                                        text: ''
                                    },
                                    tooltip: {
                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                    },
                                    plotOptions: {
                                        pie: {
                                            allowPointSelect: true,
                                            cursor: 'pointer',
                                            dataLabels: {
                                                enabled: false
                                            },
                                            showInLegend: true
                                        }
                                    },
                                    series: [{
                                            name: 'Respuesta ',
                                            colorByPoint: true,
                                            data: [<?php foreach ($obj->opciones as $count => $opt) { ?>{name: '<?php echo $opt->texto ?>', y: <?php echo (int) $opt->contador ?>},<?php } ?>]
                                        }]
                                });

                                $("#send-form-<?php echo $obj->id_miportal_encuesta ?>").validationEngine();

                                $('#close-edit-<?php echo $obj->id_miportal_encuesta ?>').on('click', function () {
                                    $('#encuestas-seccion-edit-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                    $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').show();
                                    $("#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>").mouseover(function () {
                                        $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_encuesta ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_encuesta ?>').show();
                                    });

                                    $("#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>").mouseout(function () {
                                        $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                    });
                                });

                                $('#close-cancel-<?php echo $obj->id_miportal_encuesta ?>').on('click', function () {
                                    $('#encuestas-seccion-edit-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                    $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').show();
                                    $("#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>").mouseover(function () {
                                        $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_encuesta ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_encuesta ?>').show();
                                    });

                                    $("#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>").mouseout(function () {
                                        $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                        //$('#editar-encuestas-<?php /*echo $obj->id_miportal_encuesta */?>').hide();
                                        $('#button-editseccion-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                    });
                                });

                                $('#button-editseccion-<?php echo $obj->id_miportal_encuesta ?>').on('click', function () {
                                    $("#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>").unbind("mouseover");
                                    $("#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>").unbind("mouseout");
                                    $('#encuestas-seccion-edit-<?php echo $obj->id_miportal_encuesta ?>').show();
                                    $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                });

                                $("#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>").mouseover(function () {
                                    $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-<?php echo $obj->id_miportal_encuesta ?>').show();
                                    $('#button-delete-<?php echo $obj->id_miportal_encuesta ?>').show();
                                });

                                $("#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>").mouseout(function () {
                                    $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#button-editseccion-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                    $('#button-delete-<?php echo $obj->id_miportal_encuesta ?>').hide();
                                });

                                $('#button-send-form-<?php echo $obj->id_miportal_encuesta ?>').on('click', function () {
                                    if ($("#send-form-<?php echo $obj->id_miportal_encuesta ?>").validationEngine('validate')) {
                                        var descrp = tinymce.get(<?php echo $key + 2 ?>).getContent();
                                        if (descrp != '' && descrp != null && descrp.length > 16) {
                                            $('#button-send-form-<?php echo $obj->id_miportal_encuesta ?>').attr('disabled', 'true');
                                            var opt = $('[name="option-<?php echo $obj->id_miportal_encuesta ?>"]');
                                            var ar = [];
                                            for (var i = 0; i < opt.length; i++) {
                                                var obj = {id: $(opt[i]).data('id'), texto: $(opt[i]).val()};
                                                ar.push(obj);
                                            }
                                            $.ajax({
                                                url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_encuesta',
                                                method: 'POST',
                                                timeout: 4000,
                                                data: {
                                                    'titulo': $('#tituloencuesta-<?php echo $obj->id_miportal_encuesta ?>').val(),
                                                    'descripcion': descrp,
                                                    'opciones': ar,
                                                    'i_e': '<?php echo $obj->id_miportal_encuesta ?>',
                                                },
                                            }).done(function (result) {
                                                var sup = $('#encuestas-seccion-<?php echo $obj->id_miportal_encuesta ?>');
                                                $(sup.find('.titulo')[0]).text($('#tituloencuesta-<?php echo $obj->id_miportal_encuesta ?>').val());
                                                $(sup.find('.descripcion')[0]).html(descrp);

                                                var opt = $('[name="option-<?php echo $obj->id_miportal_encuesta ?>"]');
                                                var contador = 0;
                                                for (var i = 0; i < opt.length; i++) {
                                                    if ($(opt[i]).data('id') == null) {
                                                        $($($(opt[i]).parent().parent()).find('.deleteopcion')[0]).data('id', result.id_new[contador]);
                                                        $(opt[i]).data('id', result.id_new[contador]);
                                                        contador++;
                                                    }
                                                }
                                                refreshGraphic('<?php echo $obj->id_miportal_encuesta ?>');
                                                $('#close-cancel-<?php echo $obj->id_miportal_encuesta ?>').trigger('click');
                                                $('#button-send-form-<?php echo $obj->id_miportal_encuesta ?>').removeAttr('disabled');
                                            }).fail(function () {
                                                $('#button-send-form-<?php echo $obj->id_miportal_encuesta ?>').removeAttr('disabled');
                                            });
                                        } else {
                                            alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                        }
                                    } else {
                                        console.log('error');
                                    }
                                });

                                $('#button-delete-<?php echo $obj->id_miportal_encuesta ?>').on('click', function () {
                                    var r = confirm('¿Seguro que deseas eliminar este pregunta?');
                                    if (r == true) {
                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_encuesta',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-encuesta': '<?php echo $obj->id_miportal_encuesta ?>',
                                            },
                                        }).done(function () {
                                            $('#<?php echo $obj->id_miportal_encuesta ?>').remove();
                                        }).fail(function () {
                                            console.log('error');
                                        });
                                    }
                                });

                                $('#add-option-<?php echo $obj->id_miportal_encuesta ?>').on('click', function () {
                                    var temp = '<div class="row">' +
                                            '<div class="col s3">Respuesta</div>' +
                                            '<div class="col s7">' +
                                            '<input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" name="option-<?php echo $obj->id_miportal_encuesta ?>" type="text" data-id="null" data-contador="0" value="" />' +
                                            '</div>' +
                                            '<div class="col s2">' +
                                            '<button class="waves-effect waves-light btn-flat deleteopcion" data-id="null" data-encuesta="<?php echo $obj->id_miportal_encuesta ?>" data-elemento="<?php echo $obj->id_miportal_encuesta ?>" type="button"><i class="material-icons">delete_forever</i></button>' +
                                            '</div>' +
                                            '</div>';
                                    $('#new_option-<?php echo $obj->id_miportal_encuesta ?>').append(temp);

                                    $('.deleteopcion').unbind('click');
                                    $('.deleteopcion').on('click', function () {
                                        var esto = this;
                                        var id = $(esto).data('id');
                                        if (id != null) {
                                            var r = confirm('¿Seguro que deseas eliminar este respuesta?');
                                            if (r == true) {
                                                $.ajax({
                                                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_encuesta_opcion',
                                                    method: 'POST',
                                                    timeout: 4000,
                                                    data: {
                                                        'id-enc': $(esto).data('encuesta'),
                                                        'id-opcion': id,
                                                    },
                                                }).done(function () {
                                                    $(esto.parentNode.parentNode).remove();
                                                    refreshGraphic($(esto).data('elemento'));
                                                }).fail(function () {
                                                    console.log('error');
                                                });
                                            }
                                        } else {
                                            $(esto.parentNode.parentNode).remove();
                                        }
                                    });
                                });
                                
                                $('.deleteopcion').unbind('click');
                                $('.deleteopcion').on('click', function () {
                                    var esto = this;
                                    var id = $(esto).data('id');
                                    if (id != null) {
                                        var r = confirm('¿Seguro que deseas eliminar este respuesta?');
                                        if (r == true) {
                                            $.ajax({
                                                url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_encuesta_opcion',
                                                method: 'POST',
                                                timeout: 4000,
                                                data: {
                                                    'id-enc': $(esto).data('encuesta'),
                                                    'id-opcion': id,
                                                },
                                            }).done(function () {
                                                $(esto.parentNode.parentNode).remove();
                                                refreshGraphic($(esto).data('elemento'));
                                            }).fail(function () {
                                                console.log('error');
                                            });
                                        }
                                    } else {
                                        $(esto.parentNode.parentNode).remove();
                                    }
                                });


                            });
                        </script>
                    <?php } ?>

                    <div id="add_before_encuesta"></div> 

                    <div class="box">
                        <img src="<?php echo base_url() ?>dist/file-portal/img/calendar.png">
                        <br>
                        <button class="waves-effect waves-light btn-flat boton" id="add-new-encuesta">A&ntilde;adir encuesta</button>
                    </div>



                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<script type="text" id="template-1" >
    <hr class="hr-separador">
    <div class="encuestas-agregados" id="encuestas-seccion-" style="display:none; padding:11px;">
    <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
    <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-" type="button" ><i class="material-icons">delete_forever</i></button>
    <h5 class="titulo" style="text-align:left;"></h5>
    <div class="descripcion"></div>
    <div id="grafica-" style="width:90%; height: 250px;"></div>
    </div>
    <div class="edit" id="encuestas-seccion-edit-" >
    <form id="send-form-">
    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-" type="button" ><i class="material-icons">close</i></button>
    <h6><b>T&iacute;tulo*</b></h6>
    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="tituloencuesta-" value="" placeholder="T&iacute;tulo" />
    <h6><b>Descripci&oacute;n</b></h6>
    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionencuesta-"></textarea>
    <br><br>
    <div class="row">
    <div class="col s3">Respuesta</div>
    <div class="col s7">
    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" name="option-" type="text" data-id="null" data-contador="0" value="" />
    </div>
    <div class="col s2"></div>
    </div>
    <div class="row">
    <div class="col s3">Respuesta</div>
    <div class="col s7">
    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" name="option-" type="text" data-id="null" data-contador="0" value="" />
    </div>
    <div class="col s2"></div>
    </div>
    <div id="new_option-"></div>


    <button class="waves-effect waves-light btn-flat orange darken-1 white-text" id="add-option-" type="button" >Agregar pregunta</button>
    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-" type="button" >GUARDAR</button>
    <button class="waves-effect waves-light btn-flat" id="close-cancel-" type="button" >Cancelar</button>
    </form>
    </div>
</script>    
<script>
    var count_template = <?php echo count($encuestas) + 1; ?>;
    function refreshGraphic(id) {
        var opt = $('[name="option-' + id + '"]');
        var new_grafic = [];
        for (var i = 0; i < opt.length; i++) {
            var obj = {name: $(opt[i]).val(), y: parseInt($(opt[i]).data('contador'))};
            new_grafic.push(obj);
        }
        var chart = $('#grafica-' + id).highcharts();
        chart.series[0].setData(new_grafic);
    }

    $(document).ready(function () {
        $('#add-new-encuesta').on('click', function () {
            count_template++;
            var div = document.createElement("div");
            div.setAttribute('id', count_template);
            div.setAttribute('style', 'position:relative;');
            div.innerHTML = $("#template-1").html();

            $($(div).find('#encuestas-seccion-edit-')[0]).attr('id', 'encuestas-seccion-edit-' + count_template);
            $($(div).find('#grafica-')[0]).attr('id', 'grafica-' + count_template);
            $($(div).find('#new_option-')[0]).attr('id', 'new_option-' + count_template);
            $($(div).find('#tituloencuesta-')[0]).attr('id', 'tituloencuesta-' + count_template);
            $($(div).find('#descripcionencuesta-')[0]).attr('id', 'descripcionencuesta-' + count_template);
            $($(div).find('[name="option-"]')[0]).attr('name', 'option-' + count_template);
            $($(div).find('[name="option-"]')[0]).attr('name', 'option-' + count_template);
            $($(div).find('[name="option-' + count_template + '"]')[0]).attr('data-elemento', count_template);
            $($(div).find('[name="option-' + count_template + '"]')[1]).attr('data-elemento', count_template);
            $($(div).find('#button-editseccion-')[0]).attr('id', 'button-editseccion-' + count_template);
            $($(div).find('#button-delete-')[0]).attr('id', 'button-delete-' + count_template);
            $($(div).find('#encuestas-seccion-')[0]).attr('id', 'encuestas-seccion-' + count_template);
            
            $($(div).find('#add-option-')[0]).attr('id', 'add-option-' + count_template);
            $($(div).find('#add-option-' + count_template)[0]).on('click', function () {
                var sup_id = ($(this).attr('id')).split('add-option-')[1];
                var temp = '<div class="row">' +
                        '<div class="col s3">Respuesta</div>' +
                        '<div class="col s7">' +
                        '<input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" name="option-' + sup_id + '" type="text" data-id="null" data-contador="0" value="" />' +
                        '</div>' +
                        '<div class="col s2">' +
                        '<button class="waves-effect waves-light btn-flat deleteopcion-new" data-id="null" data-elemento="' + sup_id + '" type="button"><i class="material-icons">delete_forever</i></button>' +
                        '</div>' +
                        '</div>';
                $('#new_option-' + count_template).append(temp);

                $('.deleteopcion-new').unbind('click');
                $('.deleteopcion-new').on('click', function () {
                    $(this.parentNode.parentNode).remove();
                });
            });
            
            $($(div).find('#send-form-')[0]).attr('id', 'send-form-' + count_template);
            $($(div).find('#send-form-' + count_template)[0]).validationEngine();
            
            
            $($(div).find('#close-cancel-')[0]).attr('id', 'close-cancel-' + count_template);
            $($(div).find('#close-cancel-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('close-cancel-')[1];
                $('#' + ident).remove();
            });
            
            $($(div).find('#close-edit-')[0]).attr('id', 'close-edit-' + count_template);
            $($(div).find('#close-edit-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('close-edit-')[1];
                $('#' + ident).remove();
            });
            
            $($(div).find('#button-send-form-')[0]).attr('id', 'button-send-form-' + count_template);
            $($(div).find('#button-send-form-' + count_template)[0]).on('click', function () {
                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];

                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                    var descrp = tinymce.get(sup_id).getContent();
                    if (descrp != '' && descrp != null && descrp.length > 16) {
                        $('#button-send-form-' + sup_id).attr('disabled', 'true');
                        var opt = $('[name="option-' + sup_id + '"]');
                        var ar = [];
                        var graphic = [];
                        for (var i = 0; i < opt.length; i++) {
                            var obj = {texto: $(opt[i]).val()};
                            ar.push(obj);
                            var ob ={name: $(opt[i]).val(),y:0};
                            graphic.push(ob);
                        }
                        $.ajax({
                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/insert_encuesta',
                            method: 'POST',
                            timeout: 4000,
                            data: {
                                'titulo': $('#tituloencuesta-' + sup_id).val(),
                                'descripcion': descrp,
                                's': '<?php echo $obj->id_miportal_seccion ?>',
                                'opciones': ar,
                            },
                        }).done(function (result) {
                            var sup = $('#encuestas-seccion-' + sup_id);
                            $(sup.find('.titulo')[0]).text($('#tituloencuesta-' + sup_id).val());
                            $(sup.find('.descripcion')[0]).html(descrp);
                            
                            
                            var opt = $('[name="option-' + sup_id + '"]');
                            for (var i = 0; i < opt.length; i++) {
                                $(opt[i]).data('id', result.id_new[i]);
                                var buton = $($(opt[i]).parent().parent()).find('.deleteopcion-new');
                                if(buton.length > 0){
                                    $(buton[0]).data('id', result.id_new[i]);
                                    $(buton[0]).data('encuesta', result.id_encuesta);
                                    $(buton[0]).attr('class', 'waves-effect waves-light btn-flat deleteopcion');
                                }
                            }
                            
                            $('#close-edit-' + sup_id).unbind("click");
                            $('#close-edit-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('close-edit-')[1];
                                $('#encuestas-seccion-edit-' + sup_id).hide();
                                $('#encuestas-seccion-' + sup_id).show();
                                $("#encuestas-seccion-" + sup_id).mouseover(function () {
                                    $('#encuestas-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-' + sup_id).show();
                                    $('#button-delete-' + sup_id).show();
                                });

                                $("#encuestas-seccion-" + sup_id).mouseout(function () {
                                    $('#encuestas-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#button-editseccion-' + sup_id).hide();
                                    $('#button-delete-' + sup_id).hide();
                                });
                            });
                            
                            
                            $("#encuestas-seccion-" + sup_id).mouseover(function () {
                                var sup_id = ($(this).attr('id')).split('encuestas-seccion-')[1];
                                $('#encuestas-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#button-editseccion-' + sup_id).show();
                                $('#button-delete-' + sup_id).show();
                            });
                            
                            $("#encuestas-seccion-" + sup_id).mouseout(function () {
                                var sup_id = ($(this).attr('id')).split('encuestas-seccion-')[1];
                                $('#encuestas-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                $('#button-editseccion-' + sup_id).hide();
                                $('#button-delete-' + sup_id).hide();
                            });
                            
                            $('#close-cancel-' + sup_id).unbind("click");
                            $('#close-cancel-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('close-cancel-')[1];
                                $('#encuestas-seccion-edit-' + sup_id).hide();
                                $('#encuestas-seccion-' + sup_id).show();
                                $("#encuestas-seccion-" + sup_id).mouseover(function () {
                                    $('#encuestas-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-' + sup_id).show();
                                    $('#button-delete-' + sup_id).show();
                                });

                                $("#encuestas-seccion-" + sup_id).mouseout(function () {
                                    $('#encuestas-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#button-editseccion-' + sup_id).hide();
                                    $('#button-delete-' + sup_id).hide();
                                });
                            });
                            
                            $('#button-send-form-' + sup_id).unbind("click");
                            $('#button-send-form-' + sup_id).data('id', result.id_encuesta);
                            $('#button-send-form-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];

                                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                                    var descrp = tinymce.get(sup_id).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form-' + sup_id).attr('disabled', 'true');
                                        var opt = $('[name="option-' + sup_id + '"]');
                                        var ar = [];
                                        for (var i = 0; i < opt.length; i++) {
                                            var obj = {id: $(opt[i]).data('id'),texto: $(opt[i]).val()};
                                            ar.push(obj);
                                        }
                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_encuesta',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'titulo': $('#tituloencuesta-' + sup_id).val(),
                                                'descripcion': descrp,
                                                'i_e': $(this).data('id'),
                                                'opciones': ar,
                                            },
                                        }).done(function (result) {
                                            var sup = $('#encuestas-seccion-' + sup_id);
                                            $(sup.find('.titulo')[0]).text($('#tituloencuesta-' + sup_id).val());
                                            $(sup.find('.descripcion')[0]).html(descrp);
                                            var opt = $('[name="option-'+ sup_id +'"]');
                                            var contador = 0;
                                            for (var i = 0; i < opt.length; i++) {
                                                if ($(opt[i]).data('id') == null) {
                                                    $($($(opt[i]).parent().parent()).find('.deleteopcion')[0]).data('id', result.id_new[contador]);
                                                    $(opt[i]).data('id', result.id_new[contador]);
                                                    contador++;
                                                }
                                            }
                                            refreshGraphic(sup_id);
                                            $('#close-cancel-' + sup_id).trigger('click');
                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                            
                            
                            $('#button-delete-' + sup_id).show();
                            $('#button-delete-' + sup_id).data('id',result.id_encuesta);
                            $('#button-delete-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-delete-')[1];
                                var r = confirm('¿Seguro que deseas eliminar este pregunta?');
                                if (r == true) {
                                    $.ajax({
                                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_encuesta',
                                        method: 'POST',
                                        timeout: 4000,
                                        data: {
                                            'id-encuesta': $(this).data('id'),
                                        },
                                    }).done(function () {
                                        $('#' + sup_id).remove();
                                    }).fail(function () {
                                        console.log('error');
                                    });
                                }
                            });
                            
                            
                            $('#button-editseccion-' + sup_id).show();
                            $('#button-editseccion-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-editseccion-')[1];
                                $("#encuestas-seccion-" + sup_id).unbind("mouseover");
                                $("#encuestas-seccion-" + sup_id).unbind("mouseout");
                                $('#encuestas-seccion-edit-' + sup_id).show();
                                $('#encuestas-seccion-' + sup_id).hide();
                            });
                            
                            $('.deleteopcion').unbind('click');
                            $('.deleteopcion').on('click', function () {
                                var esto = this;
                                var id = $(esto).data('id');
                                if (id != null) {
                                    var r = confirm('¿Seguro que deseas eliminar este respuesta?');
                                    if (r == true) {
                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_encuesta_opcion',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-enc': $(esto).data('encuesta'),
                                                'id-opcion': id,
                                            },
                                        }).done(function () {
                                            $(esto.parentNode.parentNode).remove();
                                            refreshGraphic($(esto).data('elemento'));
                                        }).fail(function () {
                                            console.log('error');
                                        });
                                    }
                                } else {
                                    $(esto.parentNode.parentNode).remove();
                                }
                            });

                            
                            $('#grafica-' + sup_id).attr('style','width:'+ ($($('.encuesta .descripcion')[0]).width() - 50) +'px; height:250px;');
                            $('#grafica-' + sup_id).highcharts({
                                credits: {
                                    enabled: false,
                                },
                                chart: {
                                    plotBackgroundColor: null,
                                    plotBorderWidth: null,
                                    plotShadow: false,
                                    type: 'pie'
                                },
                                legend: {
                                    layout: 'vertical',
                                    align: 'right',
                                    verticalAlign: 'top',
                                    floating: true,
                                    backgroundColor: '#FFFFFF',
                                    lineHeight: 25,
                                },
                                title: {
                                    text: ''
                                },
                                tooltip: {
                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                },
                                plotOptions: {
                                    pie: {
                                        allowPointSelect: true,
                                        cursor: 'pointer',
                                        dataLabels: {
                                            enabled: false
                                        },
                                        showInLegend: true
                                    }
                                },
                                series: [{
                                        name: 'Respuesta ',
                                        colorByPoint: true,
                                        data: graphic,
                                    }]
                            });
                            $('#grafica-' + sup_id).attr('style','width:90%; height:250px;');
                            
                            $('#close-cancel-' + sup_id).trigger('click');
                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                        }).fail(function () {
                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                        });

                    } else {
                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                    }
                } else {
                    console.log('no cumple validacion');
                }

            });



            $('#add_before_encuesta').append(div);

            tinymce.init({
                selector: '.tinymce-editor',
                plugins: "autoresize link",
                menubar: '',
                height : "250",
                toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
            });

        });
    });

</script>