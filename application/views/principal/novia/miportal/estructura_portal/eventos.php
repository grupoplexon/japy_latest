<!-- B  O  D  Y -->
<div class="container-body">
    <div class="body-container-web detail">
        <?php
        foreach ($miportal_seccion as $key => $value) {
            if ($value->tipo == 'evento') { $id_seccion = $value->id_miportal_seccion;
                ?>
                <div class="eventos">

                    <!-- E  V   E   N   T   O   S -->
                    <div id="eventos-seccion" style="padding:11px;">
                        <button class="waves-effect waves-light btn-flat button-edit" id="editar-eventoprincipal" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                        <h4 class="titulo"><?php echo $value->titulo ?></h4>
                        <div class="descripcion"><?php echo $value->descripcion ?></div>
                    </div>
                    <div class="edit" id="eventos-seccion-edit" style="display: none;">
                        <form id="send-form">
                            <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                            <br>
                            <input type="hidden" name="id-evento" value="<?php echo $value->id_miportal_seccion ?>" required />
                            <h6><b>T&iacute;tulo*</b></h6>
                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="titulo-evento" value="<?php echo $value->titulo; ?>" placeholder="T&iacute;tulo" />
                            <h6><b>Descripci&oacute;n*</b></h6>
                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-evento" id="eventodescripcion"><?php echo $value->descripcion; ?></textarea>
                            <br>
                            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                            <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                        </form>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $("#send-form").validationEngine();

                            $('#close-edit').on('click', function () {
                                $('#eventos-seccion-edit').hide();
                                $('#eventos-seccion').show();
                                $("#eventos-seccion").mouseover(function () {
                                    $('#eventos-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-eventoprincipal').show();
                                });

                                $("#eventos-seccion").mouseout(function () {
                                    $('#eventos-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-eventoprincipal').hide();
                                });
                            });

                            $('#close-cancel').on('click', function () {
                                $('#eventos-seccion-edit').hide();
                                $('#eventos-seccion').show();
                                $("#eventos-seccion").mouseover(function () {
                                    $('#eventos-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-eventoprincipal').show();
                                });

                                $("#eventos-seccion").mouseout(function () {
                                    $('#eventos-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-eventoprincipal').hide();
                                });
                            });

                            $('.button-edit').on('click', function () {
                                $("#eventos-seccion").unbind("mouseover");
                                $("#eventos-seccion").unbind("mouseout");
                                $('#eventos-seccion-edit').show();
                                $('#eventos-seccion').hide();
                            });

                            $("#eventos-seccion").mouseover(function () {
                                $('#eventos-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#editar-eventoprincipal').show();
                            });

                            $("#eventos-seccion").mouseout(function () {
                                $('#eventos-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                $('#editar-eventoprincipal').hide();
                            });



                            $('#button-send-form').on('click', function () {
                                if ($("#send-form").validationEngine('validate')) {
                                    var descrp = tinymce.get(1).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form').attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_headerevento',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-evento': $('[name="id-evento"]').val(),
                                                'titulo-evento': $('[name="titulo-evento"]').val(),
                                                'descripcion-evento': tinymce.get(1).getContent(),
                                            },
                                        }).done(function () {
                                            $($('#eventos-seccion').find('.titulo')[0]).text($('[name="titulo-evento"]').val());
                                            $($('#eventos-seccion').find('.descripcion')[0]).html(tinymce.get(1).getContent());
                                            $('#close-cancel').trigger('click');
                                            $('#button-send-form').removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form').removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                        });
                    </script>
                    <!--    F  I   N   --  E  V   E   N   T   O   S     -->

                    <?php foreach ($eventos as $key => $obj) { ?>
                        <div id="<?php echo $obj->id_miportal_evento ?>" style="position:relative;">
                            <hr class="hr-separador">
                            <div class="eventos-agregados" id="eventos-seccion-<?php echo $obj->id_miportal_evento ?>" style="padding:11px;">
                                <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-<?php echo $obj->id_miportal_evento ?>" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                                <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-<?php echo $obj->id_miportal_evento ?>" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
                                <h5 class="evento-titulo"><?php echo $obj->titulo; ?></h5>
                                <p class="evento-fecha"><i class="material-icons">event</i><?php echo ucfirst(dateMiniFormat($obj->fecha, '%A %d de')) . ' ' . ucfirst(dateMiniFormat($obj->fecha, '%B de %Y')) ?></p>
                                <p class="evento-hora"><i class="material-icons">access_time</i><?php echo dateMiniFormat($obj->fecha, '%I:%m %p') ?></p>
                                <p class="evento-telefono"><b><i class="material-icons">phone</i>Tel&eacute;fono: </b> <?php echo $obj->telefono; ?></p>
                                <div class="evento-descripcion"><?php echo $obj->descripcion; ?></div>
                                <div class="evento-mapa" style="overflow: hidden;">
                                    <iframe
                                        width="100%" height="400"
                                        frameborder="0" style="border:0; margin-top: -100px;"
                                        src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=<?php echo $obj->latitud . ',' . $obj->longitud ?>&zoom=18" allowfullscreen></iframe>
                                </div>
                                <p class="evento-direccion"><b><i class="material-icons">place</i>Direcci&oacute;n: </b><?php echo $obj->direccion . ' ' . $obj->codigo_postal . ' ' . $obj->estado . ', ' . $obj->poblado; ?></p>
                            </div>



                            <div class="edit" id="eventos-seccion-edit-<?php echo $obj->id_miportal_evento ?>" style="display:none;" >
                                <form id="send-form-<?php echo $obj->id_miportal_evento ?>">
                                    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-<?php echo $obj->id_miportal_evento ?>" type="button" ><i class="material-icons">close</i></button>
                                    <div class="row">
                                        <div class="col m6 s12">
                                            <h6><b>T&iacute;tulo*</b></h6>
                                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="tituloevento-<?php echo $obj->id_miportal_evento ?>" value="<?php echo $obj->titulo; ?>" placeholder="T&iacute;tulo" />
                                            <div class="row">
                                                <div class="row">
                                                    <div class="col s6">
                                                        <h6><b>Fecha*</b></h6>
                                                        <input class="datepicker formulario-invitaciones validate[required]" readonly="readonly" style="width:100%" type="text" id="fechaevento-<?php echo $obj->id_miportal_evento ?>" value="<?php echo dateMiniFormat($obj->fecha, '%Y-%m-%d'); ?>" placeholder="Fecha" />
                                                    </div>
                                                    <div class="col s6">
                                                        <h6><b>Hora*</b></h6>
                                                        <input class="formulario-invitaciones validate[required,custom[timeFormat]]" style="width:100%" type="text" id="horaevento-<?php echo $obj->id_miportal_evento ?>" value="<?php echo dateMiniFormat($obj->fecha, '%H:%M') ?>" placeholder="Hora" />
                                                    </div>
                                                </div>
                                            </div>
                                            <h6><b>Tel&eacute;fono</b></h6>
                                            <input class="formulario-invitaciones validate[custom[phone]]" style="width:100%" type="text" id="telfonoevento-<?php echo $obj->id_miportal_evento ?>" value="<?php echo $obj->telefono; ?>" placeholder="Tel&eacute;fono" />
                                            <h6><b>Descripci&oacute;n</b></h6>
                                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionevento-<?php echo $obj->id_miportal_evento ?>"><?php echo $obj->descripcion; ?></textarea>
                                        </div>



                                        <div class="col m6 s12">
                                            <h6><b>Pa&iacute;s</b></h6>
                                            <select class="browser-default countries" id="countryId-<?php echo $obj->id_miportal_evento ?>" data-default="<?php echo $obj->pais; ?>" >
                                                <option value="" disabled selected>-- Pa&iacute;s --</option>
                                            </select>

                                            <h6><b>Poblaci&oacute;n</b></h6>
                                            <div class="row">
                                                <div class="col s6">
                                                    <select class="browser-default states" id="stateId-<?php echo $obj->id_miportal_evento ?>" data-default="<?php echo $obj->estado; ?>" >
                                                        <option value="" disabled selected>-- Estado --</option>
                                                    </select>
                                                </div>
                                                <div class="col s6">
                                                    <select class="browser-default cities" id="cityId-<?php echo $obj->id_miportal_evento ?>" data-default="<?php echo $obj->poblado; ?>" >
                                                        <option value="" disabled selected>-- Poblaci&oacute;n --</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <h6><b>C&oacute;digo Postal</b></h6>
                                            <input class="formulario-invitaciones validate[custom[number]]" style="width:100%" type="text" id="codigopostalevento-<?php echo $obj->id_miportal_evento ?>" value="<?php echo $obj->codigo_postal; ?>" placeholder="C&oacute;digo Postal" />

                                            <h6><b>Direcci&oacute;n*</b></h6>
                                            <div class="row">
                                                <div class="col s12">
                                                    <input class="formulario-invitaciones validate[required,min[4]]" style="width:100%" type="text" id="direccionevento-<?php echo $obj->id_miportal_evento ?>" value="<?php echo $obj->direccion; ?>" placeholder="Direcci&oacute;n" />
                                                    <input type="hidden" id="latitudevento-<?php echo $obj->id_miportal_evento ?>" value="<?php echo $obj->latitud; ?>" />
                                                    <input type="hidden" id="longitudevento-<?php echo $obj->id_miportal_evento ?>" value="<?php echo $obj->longitud; ?>" />
                                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="updateMap-<?php echo $obj->id_miportal_evento ?>" type="button" >Actualizar Mapa</button>
                                                </div>
                                                <div class="col s12">
                                                    <div class="evento-mapa" style="width:auto; height: 200px" id="imagemap-<?php echo $obj->id_miportal_evento ?>"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-<?php echo $obj->id_miportal_evento ?>" type="button" >GUARDAR</button>
                                    <button class="waves-effect waves-light btn-flat" id="close-cancel-<?php echo $obj->id_miportal_evento ?>" type="button" >Cancelar</button>
                                </form>
                            </div>


                            <script>
                                $(document).ready(function () {
                                    var _LOC = new locationInfo('cityId-<?php echo $obj->id_miportal_evento ?>', 'stateId-<?php echo $obj->id_miportal_evento ?>', 'countryId-<?php echo $obj->id_miportal_evento ?>');

                                    $("#send-form-<?php echo $obj->id_miportal_evento ?>").validationEngine();

                                    $('#close-edit-<?php echo $obj->id_miportal_evento ?>').on('click', function () {
                                        $('#eventos-seccion-edit-<?php echo $obj->id_miportal_evento ?>').hide();
                                        $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').show();
                                        $("#eventos-seccion-<?php echo $obj->id_miportal_evento ?>").mouseover(function () {
                                            $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_evento ?>').show();
                                            $('#button-delete-<?php echo $obj->id_miportal_evento ?>').show();
                                        });

                                        $("#eventos-seccion-<?php echo $obj->id_miportal_evento ?>").mouseout(function () {
                                            $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_evento ?>').hide();
                                            $('#button-delete-<?php echo $obj->id_miportal_evento ?>').hide();
                                        });
                                    });

                                    $('#close-cancel-<?php echo $obj->id_miportal_evento ?>').on('click', function () {
                                        $('#eventos-seccion-edit-<?php echo $obj->id_miportal_evento ?>').hide();
                                        $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').show();
                                        $("#eventos-seccion-<?php echo $obj->id_miportal_evento ?>").mouseover(function () {
                                            $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_evento ?>').show();
                                            $('#button-delete-<?php echo $obj->id_miportal_evento ?>').show();
                                        });

                                        $("#eventos-seccion-<?php echo $obj->id_miportal_evento ?>").mouseout(function () {
                                            $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_evento ?>').hide();
                                            $('#button-delete-<?php echo $obj->id_miportal_evento ?>').hide();
                                        });
                                    });

                                    $('#button-editseccion-<?php echo $obj->id_miportal_evento ?>').on('click', function () {
                                        $("#eventos-seccion-<?php echo $obj->id_miportal_evento ?>").unbind("mouseover");
                                        $("#eventos-seccion-<?php echo $obj->id_miportal_evento ?>").unbind("mouseout");
                                        $('#eventos-seccion-edit-<?php echo $obj->id_miportal_evento ?>').show();
                                        $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').hide();

                                        $('#updateMap-<?php echo $obj->id_miportal_evento ?>').click();
                                    });

                                    $("#eventos-seccion-<?php echo $obj->id_miportal_evento ?>").mouseover(function () {
                                        $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_evento ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_evento ?>').show();
                                    });

                                    $("#eventos-seccion-<?php echo $obj->id_miportal_evento ?>").mouseout(function () {
                                        $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_evento ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_evento ?>').hide();
                                    });
                                    /*   -   -   -   -  M       A       P      A     -   I     N   I   C   I   O       -   -   -   - */

                                    $('#updateMap-<?php echo $obj->id_miportal_evento ?>').on('click', function () {
                                        var address = $('#direccionevento-<?php echo $obj->id_miportal_evento ?>').val();
                                        var geocoder = new google.maps.Geocoder();
                                        geocoder.geocode({'address': address}, geocodeResult<?php echo $obj->id_miportal_evento ?>);
                                    });

                                    $('#direccionevento-<?php echo $obj->id_miportal_evento ?>').on('change', function () {
                                        var address = $(this).val();
                                        var geocoder = new google.maps.Geocoder();
                                        geocoder.geocode({'address': address}, geocodeResult<?php echo $obj->id_miportal_evento ?>);
                                    });

                                    var myLatlng = new google.maps.LatLng(<?php echo $obj->latitud; ?>, <?php echo $obj->longitud; ?>);
                                    var myOptions = {
                                        zoom: 4,
                                        center: myLatlng,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };
                                    var temp = new google.maps.Map($("#imagemap-<?php echo $obj->id_miportal_evento ?>").get(0), myOptions);

                                    function geocodeResult<?php echo $obj->id_miportal_evento ?>(results, status) {
                                        if (status == 'OK') {
                                            var mapOptions = {
                                                center: results[0].geometry.location,
                                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                            };
                                            var maps = new google.maps.Map($("#imagemap-<?php echo $obj->id_miportal_evento ?>").get(0), mapOptions);
                                            maps.fitBounds(results[0].geometry.viewport);
                                            var markerOptions = {position: results[0].geometry.location, draggable: true};
                                            var marker = new google.maps.Marker(markerOptions);
                                            var markerLatLng = marker.getPosition();
                                            $('#latitudevento-<?php echo $obj->id_miportal_evento ?>').val(markerLatLng.lat());
                                            $('#longitudevento-<?php echo $obj->id_miportal_evento ?>').val(markerLatLng.lng());
                                            marker.setMap(maps);
                                        } else {
                                            alert("Direccion invalida ");
                                        }
                                    }
                                    ;



                                    /*   -   -   -   -      F       I       N       -   -   -   - */

                                    $('#button-send-form-<?php echo $obj->id_miportal_evento ?>').on('click', function () {
                                        if ($("#send-form-<?php echo $obj->id_miportal_evento ?>").validationEngine('validate')) {
                                            var descrp = tinymce.get(<?php echo $key + 2 ?>).getContent();
                                            if (descrp != '' && descrp != null && descrp.length > 16) {
                                                $('#button-send-form-<?php echo $obj->id_miportal_evento ?>').attr('disabled', 'true');
                                                
                                                $.ajax({
                                                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_evento',
                                                    method: 'POST',
                                                    timeout: 4000,
                                                    data: {
                                                        'titulo': $('#tituloevento-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'descripcion': descrp,
                                                        'telefono': $('#telfonoevento-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'fecha': $('#fechaevento-<?php echo $obj->id_miportal_evento ?>').val() + ' ' + $('#horaevento-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'pais': $('#countryId-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'estado': $('#stateId-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'poblado': $('#cityId-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'codigo_postal': $('#codigopostalevento-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'direccion': $('#direccionevento-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'latitud': $('#latitudevento-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'longitud': $('#longitudevento-<?php echo $obj->id_miportal_evento ?>').val(),
                                                        'i': '<?php echo $obj->id_miportal_evento ?>',
                                                    },
                                                }).done(function () {
                                                    var sup = $('#eventos-seccion-<?php echo $obj->id_miportal_evento ?>');
                                                    $(sup.find('.evento-titulo')[0]).text($('#tituloevento-<?php echo $obj->id_miportal_evento ?>').val());
                                                    $(sup.find('.evento-descripcion')[0]).html(descrp);
                                                    $(sup.find('.evento-fecha')[0]).html('<i class="material-icons">event</i>' + new FormatDate($('#fechaevento-<?php echo $obj->id_miportal_evento ?>').val() + ' 12:00:00').DD_dd_MM_YYYY());
                                                    $(sup.find('.evento-hora')[0]).html('<i class="material-icons">access_time</i>' + $('#horaevento-<?php echo $obj->id_miportal_evento ?>').val());
                                                    $(sup.find('.evento-telefono')[0]).html('<b><i class="material-icons">phone</i>Tel&eacute;fono: </b>' + $('#telfonoevento-<?php echo $obj->id_miportal_evento ?>').val());
                                                    $(sup.find('.evento-mapa')[0]).html('<iframe ' +
                                                            'width="100%" height="400" ' +
                                                            'frameborder="0" style="border:0; margin-top: -100px;" ' +
                                                            'src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=' + $('#latitudevento-<?php echo $obj->id_miportal_evento ?>').val() + ',' + $('#longitudevento-<?php echo $obj->id_miportal_evento ?>').val() + '&zoom=18" allowfullscreen></iframe>');
                                                    $(sup.find('.evento-direccion')[0]).html('<b><i class="material-icons">place</i>Direcci&oacute;n: </b>' + $('#direccionevento-<?php echo $obj->id_miportal_evento ?>').val() + ' ' + $('#codigopostalevento-<?php echo $obj->id_miportal_evento ?>').val() + ' ' + $('#stateId-<?php echo $obj->id_miportal_evento ?>').val() + ' ' + $('#cityId-<?php echo $obj->id_miportal_evento ?>').val());

                                                    $('#close-cancel-<?php echo $obj->id_miportal_evento ?>').trigger('click');
                                                    $('#button-send-form-<?php echo $obj->id_miportal_evento ?>').removeAttr('disabled');
                                                }).fail(function () {
                                                    $('#button-send-form-<?php echo $obj->id_miportal_evento ?>').removeAttr('disabled');
                                                });
                                            } else {
                                                alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                            }
                                        } else {
                                            console.log('error');
                                        }
                                    });

                                    $('#button-delete-<?php echo $obj->id_miportal_evento ?>').on('click', function () {
                                        var r = confirm('¿Seguro que deseas eliminar este evento?');
                                        if (r == true) {
                                            $.ajax({
                                                url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_evento',
                                                method: 'POST',
                                                timeout: 4000,
                                                data: {
                                                    'id-evento': '<?php echo $obj->id_miportal_evento ?>',
                                                },
                                            }).done(function () {
                                                $('#<?php echo $obj->id_miportal_evento ?>').remove();
                                            }).fail(function () {
                                                console.log('error');
                                            });
                                        }
                                    });
                                });
                            </script>
                        </div>

                    <?php } ?>

                    <div id="add_before_evento"></div>   

                    <div class="box">
                        <img src="<?php echo base_url() ?>dist/file-portal/img/calendar.png">
                        <br>
                        <button class="waves-effect waves-light btn-flat boton" id="add_evento">A&ntilde;adir evento</button>
                    </div>


                </div>
                <?php
            }
        }
        ?>
    </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key'); ?>&amp;libraries=adsense&amp;sensor=true&amp;language=es"></script>
<script type="text" id="template-1" >
    <hr class="hr-separador">
    <div class="eventos-agregados" id="eventos-seccion-" style="display:none; padding:11px;">
    <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
    <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
    <h5 class="evento-titulo"></h5>
    <p class="evento-fecha"><i class="material-icons">event</i></p>
    <p class="evento-hora"><i class="material-icons">access_time</i></p>
    <p class="evento-telefono"><b><i class="material-icons">phone</i>Tel&eacute;fono: </b> </p>
    <div class="evento-descripcion"></div>
    <div class="evento-mapa" style="overflow: hidden;"></div>
    <p class="evento-direccion"><b><i class="material-icons">place</i>Direcci&oacute;n: </b></p>
    </div>

    <div class="edit" id="eventos-seccion-edit-" >
    <form id="send-form-">
    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-" type="button" ><i class="material-icons">close</i></button>
    <div class="row">
    <div class="col m6 s12">
    <h6><b>T&iacute;tulo*</b></h6>
    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="tituloevento-" value="" placeholder="T&iacute;tulo" />
    <div class="row">
    <div class="col s6">
    <h6><b>Fecha*</b></h6>
    <input class="datepicker formulario-invitaciones validate[required]" readonly="readonly" style="width:100%" type="text" id="fechaevento-" value="" placeholder="Fecha" />
    </div>
    <div class="col s6">
    <h6><b>Hora*</b></h6>
    <input class="formulario-invitaciones validate[required,custom[timeFormat]]" style="width:100%" type="text" id="horaevento-" value="" placeholder="Hora" />
    </div>
    </div>
    <h6><b>Tel&eacute;fono</b></h6>
    <input class="formulario-invitaciones validate[custom[phone]]" style="width:100%" type="text" id="telfonoevento-" value="" placeholder="Tel&eacute;fono" />
    <h6><b>Descripci&oacute;n</b></h6>
    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionevento-"></textarea>
    </div>



    <div class="col m6 s12">
    <h6><b>Pa&iacute;s</b></h6>
    <select class="browser-default countries" id="countryId-" data-default="Mexico" >
    <option value="" disabled selected>-- Pa&iacute;s --</option>
    </select>

    <h6><b>Poblaci&oacute;n</b></h6>
    <div class="row">
    <div class="col s6">
    <select class="browser-default states" id="stateId-" >
    <option value="" disabled selected>-- Estado --</option>
    </select>
    </div>
    <div class="col s6">
    <select class="browser-default cities" id="cityId-" >
    <option value="" disabled selected>-- Poblaci&oacute;n --</option>
    </select>
    </div>
    </div>

    <h6><b>C&oacute;digo Postal</b></h6>
    <input class="formulario-invitaciones validate[custom[number]]" style="width:100%" type="text" id="codigopostalevento-" value="" placeholder="C&oacute;digo Postal" />

    <h6><b>Direcci&oacute;n*</b></h6>
    <div class="row">
    <div class="col s12">
    <input class="formulario-invitaciones validate[required,min[4]]" style="width:100%" type="text" id="direccionevento-" value="" placeholder="Direcci&oacute;n" />
    <input type="hidden" id="latitudevento-" value="" />
    <input type="hidden" id="longitudevento-" value="" />
    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="updateMap-" type="button" >Actualizar Mapa</button>
    </div>
    <div class="col s12">
    <div class="evento-mapa" style="width:auto; height: 200px" id="imagemap-"></div>
    </div>
    </div>
    </div>
    </div>
    <br>
    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-" type="button" >GUARDAR</button>
    <button class="waves-effect waves-light btn-flat" id="close-cancel-" type="button" >Cancelar</button>
    </form>
    </div>
</script>
<script>
                    var count_template = <?php echo count($eventos) + 1; ?>;

                    $(document).ready(function () {
                        $('#add_evento').on('click', function () {
                            count_template++;
                            var div = document.createElement("div");
                            div.setAttribute('id', count_template);
                            div.setAttribute('style','position:relative;');
                            div.innerHTML = $("#template-1").html();

                            $($(div).find('#button-editseccion-')[0]).attr('id', 'button-editseccion-' + count_template);
                            $($(div).find('#button-delete-')[0]).attr('id', 'button-delete-' + count_template);
                            $($(div).find('#eventos-seccion-')[0]).attr('id', 'eventos-seccion-' + count_template);
                            $($(div).find('#eventos-seccion-edit-')[0]).attr('id', 'eventos-seccion-edit-' + count_template);
                            $($(div).find('#tituloevento-')[0]).attr('id', 'tituloevento-' + count_template);
                            $($(div).find('#fechaevento-')[0]).attr('id', 'fechaevento-' + count_template);
                            $($(div).find('#horaevento-')[0]).attr('id', 'horaevento-' + count_template);
                            $($(div).find('#telfonoevento-')[0]).attr('id', 'telfonoevento-' + count_template);
                            $($(div).find('#descripcionevento-')[0]).attr('id', 'descripcionevento-' + count_template);
                            $($(div).find('#countryId-')[0]).attr('id', 'countryId-' + count_template);
                            $($(div).find('#stateId-')[0]).attr('id', 'stateId-' + count_template);
                            $($(div).find('#cityId-')[0]).attr('id', 'cityId-' + count_template);
                            $($(div).find('#codigopostalevento-')[0]).attr('id', 'codigopostalevento-' + count_template);
                            $($(div).find('#latitudevento-')[0]).attr('id', 'latitudevento-' + count_template);
                            $($(div).find('#longitudevento-')[0]).attr('id', 'longitudevento-' + count_template);
                            $($(div).find('#imagemap-')[0]).attr('id', 'imagemap-' + count_template);


                            $($(div).find('#send-form-')[0]).attr('id', 'send-form-' + count_template);
                            $($(div).find('#send-form-' + count_template)[0]).validationEngine();


                            $($(div).find('#direccionevento-')[0]).attr('id', 'direccionevento-' + count_template);
                            $($(div).find('#direccionevento-' + count_template)[0]).on('change', function () {
                                var ident = ($(this).attr('id')).split('direccionevento-')[1];
                                var address = $(this).val();
                                var geocoder = new google.maps.Geocoder();
                                geocoder.geocode({'address': address}, function (results, status) {
                                    if (status == 'OK') {
                                        var mapOptions = {
                                            center: results[0].geometry.location,
                                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        };
                                        var maps = new google.maps.Map($("#imagemap-" + ident).get(0), mapOptions);
                                        maps.fitBounds(results[0].geometry.viewport);
                                        var markerOptions = {position: results[0].geometry.location, draggable: true};
                                        var marker = new google.maps.Marker(markerOptions);
                                        var markerLatLng = marker.getPosition();
                                        $('#latitudevento-' + ident).val(markerLatLng.lat());
                                        $('#longitudevento-' + ident).val(markerLatLng.lng());
                                        marker.setMap(maps);
                                    } else {
                                        alert("Direccion invalida ");
                                    }
                                });
                            });


                            $($(div).find('#updateMap-')[0]).attr('id', 'updateMap-' + count_template);
                            $($(div).find('#updateMap-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('updateMap-')[1];
                                var address = $('#direccionevento-' + ident).val();
                                var geocoder = new google.maps.Geocoder();
                                geocoder.geocode({'address': address}, function (results, status) {
                                    if (status == 'OK') {
                                        var mapOptions = {
                                            center: results[0].geometry.location,
                                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        };
                                        var maps = new google.maps.Map($("#imagemap-" + ident).get(0), mapOptions);
                                        maps.fitBounds(results[0].geometry.viewport);
                                        var markerOptions = {position: results[0].geometry.location, draggable: true};
                                        var marker = new google.maps.Marker(markerOptions);
                                        var markerLatLng = marker.getPosition();
                                        $('#latitudevento-' + ident).val(markerLatLng.lat());
                                        $('#longitudevento-' + ident).val(markerLatLng.lng());
                                        marker.setMap(maps);
                                    } else {
                                        alert("Direccion invalida ");
                                    }
                                });
                            });


                            $($(div).find('#button-send-form-')[0]).attr('id', 'button-send-form-' + count_template);
                            $($(div).find('#button-send-form-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('button-send-form-')[1];
                                if ($("#send-form-" + ident).validationEngine('validate')) {
                                    var descrp = tinymce.get(ident).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form-' + ident).attr('disabled', 'true');
                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/insert_evento',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'titulo': $('#tituloevento-' + ident).val(),
                                                'descripcion': descrp,
                                                'telefono': $('#telfonoevento-' + ident).val(),
                                                'fecha': $('#fechaevento-' + ident).val() + ' ' + $('#horaevento-' + ident).val(),
                                                'pais': $('#countryId-' + ident).val(),
                                                'estado': $('#stateId-' + ident).val(),
                                                'poblado': $('#cityId-' + ident).val(),
                                                'codigo_postal': $('#codigopostalevento-' + ident).val(),
                                                'direccion': $('#direccionevento-' + ident).val(),
                                                'latitud': $('#latitudevento-' + ident).val(),
                                                'longitud': $('#longitudevento-' + ident).val(),
                                                's': '<?php echo $id_seccion ?>',
                                            },
                                        }).done(function (result) {
                                            var sup = $('#eventos-seccion-' + ident);
                                            $(sup.find('.evento-titulo')[0]).text($('#tituloevento-' + ident).val());
                                            $(sup.find('.evento-descripcion')[0]).html(descrp);
                                            $(sup.find('.evento-fecha')[0]).html('<i class="material-icons">event</i>' + new FormatDate($('#fechaevento-' + ident).val() + ' 12:00:00').DD_dd_MM_YYYY());
                                            $(sup.find('.evento-hora')[0]).html('<i class="material-icons">access_time</i>' + $('#horaevento-' + ident).val());
                                            $(sup.find('.evento-telefono')[0]).html('<b><i class="material-icons">phone</i>Tel&eacute;fono: </b>' + $('#telfonoevento-' + ident).val());
                                            $(sup.find('.evento-mapa')[0]).html('<iframe ' +
                                                    'width="100%" height="400" ' +
                                                    'frameborder="0" style="border:0; margin-top: -100px;" ' +
                                                    'src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=' + $('#latitudevento-' + ident).val() + ',' + $('#longitudevento-' + ident).val() + '&zoom=18" allowfullscreen></iframe>');
                                            $(sup.find('.evento-direccion')[0]).html('<b><i class="material-icons">place</i>Direcci&oacute;n: </b>' + $('#direccionevento-' + ident).val() + ' ' + $('#codigopostalevento-' + ident).val() + ' ' + $('#stateId-' + ident).val() + ' ' + $('#cityId-' + ident).val());

                                            $('#close-edit-' + ident).unbind("click");
                                            $('#close-edit-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('close-edit-')[1];
                                                $('#eventos-seccion-edit-' + sup_id).hide();
                                                $('#eventos-seccion-' + sup_id).show();
                                                $("#eventos-seccion-" + sup_id).mouseover(function () {
                                                    $('#eventos-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                    $('#button-editseccion-' + sup_id).show();
                                                    $('#button-delete-' + sup_id).show();
                                                });

                                                $("#eventos-seccion-" + sup_id).mouseout(function () {
                                                    $('#eventos-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                    $('#button-editseccion-' + sup_id).hide();
                                                    $('#button-delete-' + sup_id).hide();
                                                });
                                            });

                                            $('#close-cancel-' + ident).unbind("click");
                                            $('#close-cancel-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('close-cancel-')[1];
                                                $('#eventos-seccion-edit-' + sup_id).hide();
                                                $('#eventos-seccion-' + sup_id).show();
                                                $("#eventos-seccion-" + sup_id).mouseover(function () {
                                                    $('#eventos-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                    $('#button-editseccion-' + sup_id).show();
                                                    $('#button-delete-' + sup_id).show();
                                                });

                                                $("#eventos-seccion-" + sup_id).mouseout(function () {
                                                    $('#eventos-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                    $('#button-editseccion-' + sup_id).hide();
                                                    $('#button-delete-' + sup_id).hide();
                                                });
                                            });
                                            
                                            
                                            $('#button-editseccion-' + ident).show();
                                            $('#button-editseccion-' + ident).unbind("click");
                                            $('#button-editseccion-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('button-editseccion-')[1];
                                                $("#eventos-seccion-" + sup_id).unbind("mouseover");
                                                $("#eventos-seccion-" + sup_id).unbind("mouseout");
                                                $('#eventos-seccion-edit-' + sup_id).show();
                                                $('#eventos-seccion-' + sup_id).hide();
                                            });
                                            

                                            $("#eventos-seccion-" + ident).mouseover(function () {
                                                var sup_id = ($(this).attr('id')).split('eventos-seccion-')[1];
                                                $('#eventos-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                $('#button-editseccion-' + sup_id).show();
                                                $('#button-delete-' + sup_id).show();
                                            });

                                            $("#eventos-seccion-" + ident).mouseout(function () {
                                                var sup_id = ($(this).attr('id')).split('eventos-seccion-')[1];
                                                $('#eventos-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                $('#button-editseccion-' + sup_id).hide();
                                                $('#button-delete-' + sup_id).hide();
                                            });

                                            $('#button-delete-' + ident).unbind("click");
                                            $('#button-delete-' + ident).data('id', result.id);
                                            $('#button-delete-' + ident).show();
                                            $('#button-delete-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('button-delete-')[1];

                                                var r = confirm('¿Seguro que deseas eliminar este evento?');
                                                if (r == true) {
                                                    $.ajax({
                                                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_evento',
                                                        method: 'POST',
                                                        timeout: 4000,
                                                        data: {
                                                            'id-evento': $(this).data('id'),
                                                        },
                                                    }).done(function () {
                                                        $('#' + sup_id).remove();
                                                    }).fail(function () {
                                                        console.log('error');
                                                    });
                                                }
                                            });

                                            $('#button-send-form-' + ident).unbind("click");
                                            $('#button-send-form-' + ident).data('id', result.id);
                                            $('#button-send-form-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];

                                                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                                                    var descrp = tinymce.get(sup_id).getContent();
                                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                                        $('#button-send-form-' + sup_id).attr('disabled', 'true');

                                                        $.ajax({
                                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_evento',
                                                            method: 'POST',
                                                            timeout: 4000,
                                                            data: {
                                                                'titulo': $('#tituloevento-' + sup_id).val(),
                                                                'descripcion': descrp,
                                                                'telefono': $('#telfonoevento-' + sup_id).val(),
                                                                'fecha': $('#fechaevento-' + sup_id).val() + ' ' + $('#horaevento-' + sup_id).val(),
                                                                'pais': $('#countryId-' + sup_id).val(),
                                                                'estado': $('#stateId-' + sup_id).val(),
                                                                'poblado': $('#cityId-' + sup_id).val(),
                                                                'codigo_postal': $('#codigopostalevento-' + sup_id).val(),
                                                                'direccion': $('#direccionevento-' + sup_id).val(),
                                                                'latitud': $('#latitudevento-' + sup_id).val(),
                                                                'longitud': $('#longitudevento-' + sup_id).val(),
                                                                'i': $(this).data('id'),
                                                            },
                                                        }).done(function () {
                                                            var sup = $('#eventos-seccion-' + sup_id);
                                                            $(sup.find('.evento-titulo')[0]).text($('#tituloevento-' + sup_id).val());
                                                            $(sup.find('.evento-descripcion')[0]).html(descrp);
                                                            $(sup.find('.evento-fecha')[0]).html('<i class="material-icons">event</i>' + new FormatDate($('#fechaevento-' + sup_id).val() + ' 12:00:00').DD_dd_MM_YYYY());
                                                            $(sup.find('.evento-hora')[0]).html('<i class="material-icons">access_time</i>' + $('#horaevento-' + sup_id).val());
                                                            $(sup.find('.evento-telefono')[0]).html('<b><i class="material-icons">phone</i>Tel&eacute;fono: </b>' + $('#telfonoevento-' + sup_id).val());
                                                            $(sup.find('.evento-mapa')[0]).html('<iframe ' +
                                                                    'width="100%" height="400" ' +
                                                                    'frameborder="0" style="border:0; margin-top: -100px;" ' +
                                                                    'src="https://www.google.com/maps/embed/v1/place?key=<?php echo $this->config->item('google_api_key'); ?>&amp;&q=' + $('#latitudevento-' + sup_id).val() + ',' + $('#longitudevento-' + sup_id).val() + '&zoom=18" allowfullscreen></iframe>');
                                                            $(sup.find('.evento-direccion')[0]).html('<b><i class="material-icons">place</i>Direcci&oacute;n: </b>' + $('#direccionevento-' + sup_id).val() + ' ' + $('#codigopostalevento-' + sup_id).val() + ' ' + $('#stateId-' + sup_id).val() + ' ' + $('#cityId-' + sup_id).val());

                                                            $('#close-cancel-' + sup_id).trigger('click');
                                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                                        }).fail(function () {
                                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                                        });
                                                    } else {
                                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                                    }
                                                } else {
                                                    console.log('error');
                                                }
                                            });

                                            $('#close-cancel-' + ident).trigger('click');
                                            $('#button-send-form-' + ident).removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form-' + ident).removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('validacion no aprobada');
                                }
                            });


                            $($(div).find('#close-cancel-')[0]).attr('id', 'close-cancel-' + count_template);
                            $($(div).find('#close-cancel-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('close-cancel-')[1];
                                $('#' + ident).remove();
                            });


                            $($(div).find('#close-edit-')[0]).attr('id', 'close-edit-' + count_template);
                            $($(div).find('#close-edit-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('close-edit-')[1];
                                $('#' + ident).remove();
                            });

                            $('#add_before_evento').append(div);

                            var myLatlng = new google.maps.LatLng(20.6508562, -103.4016944);
                            var myOptions = {
                                zoom: 4,
                                center: myLatlng,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

                            var temp = new google.maps.Map($("#imagemap-" + count_template).get(0), myOptions);

                            $('.datepicker').datepicker({
                                minDate: new Date(),
                                altFormat: "yy-mm-dd",
                                dateFormat: "yy-mm-dd",
                                closeText: "Cerrar",
                                changeYear: true,
                                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                                dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            });

                            var _LOC = new locationInfo('cityId-' + count_template, 'stateId-' + count_template, 'countryId-' + count_template);

                            tinymce.init({
                                selector: '.tinymce-editor',
                                plugins: "autoresize link",
                                height : "250",
                                menubar: '',
                                toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
                            });
                        });
                    });
</script>