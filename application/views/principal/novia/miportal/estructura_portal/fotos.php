<div class="container-body">
    <div class="body-container-web detail">
        <?php
        $id_seccion = '';
        foreach ($miportal_seccion as $contador => $value) {
            if ($value->tipo == 'album') {
                $id_seccion = $value->id_miportal_seccion;
                ?>
                <div class="albums">
                    <!-- SECCION PRINCIPAL -->
                    <div id="albums-seccion" style="padding:11px;">
                        <button class="waves-effect waves-light btn-flat button-edit" id="editar-albumprincipal" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                        <h4 class="titulo"><?php echo $value->titulo ?></h4>
                        <div class="descripcion"><?php echo $value->descripcion ?></div>
                    </div>
                    <div class="edit" id="albums-seccion-edit" style="display: none;">
                        <form id="send-form">
                            <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                            <br>
                            <input type="hidden" name="id-album" value="<?php echo $value->id_miportal_seccion ?>" required />
                            <h6><b>T&iacute;tulo*</b></h6>
                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="titulo-album" value="<?php echo $value->titulo; ?>" placeholder="T&iacute;tulo" />
                            <h6><b>Descripci&oacute;n*</b></h6>
                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-album" id="albumdescripcion"><?php echo $value->descripcion; ?></textarea>
                            <br>
                            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                            <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                        </form>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $("#send-form").validationEngine();

                            $('#close-edit').on('click', function () {
                                $('#albums-seccion-edit').hide();
                                $('#albums-seccion').show();
                                $("#albums-seccion").mouseover(function () {
                                    $('#albums-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-albumprincipal').show();
                                });

                                $("#albums-seccion").mouseout(function () {
                                    $('#albums-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-albumprincipal').hide();
                                });
                            });

                            $('#close-cancel').on('click', function () {
                                $('#albums-seccion-edit').hide();
                                $('#albums-seccion').show();
                                $("#albums-seccion").mouseover(function () {
                                    $('#albums-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-albumprincipal').show();
                                });

                                $("#albums-seccion").mouseout(function () {
                                    $('#albums-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-albumprincipal').hide();
                                });
                            });

                            $('.button-edit').on('click', function () {
                                $("#albums-seccion").unbind("mouseover");
                                $("#albums-seccion").unbind("mouseout");
                                $('#albums-seccion-edit').show();
                                $('#albums-seccion').hide();
                            });

                            $("#albums-seccion").mouseover(function () {
                                $('#albums-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#editar-albumprincipal').show();
                            });

                            $("#albums-seccion").mouseout(function () {
                                $('#albums-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                $('#editar-albumprincipal').hide();
                            });



                            $('#button-send-form').on('click', function () {
                                if ($("#send-form").validationEngine('validate')) {
                                    var descrp = tinymce.get(1).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form').attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>novios/miweb/home/update_headerevento',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-evento': $('[name="id-album"]').val(),
                                                'titulo-evento': $('[name="titulo-album"]').val(),
                                                'descripcion-evento': tinymce.get(1).getContent(),
                                            },
                                        }).done(function () {
                                            $($('#albums-seccion').find('.titulo')[0]).text($('[name="titulo-album"]').val());
                                            $($('#albums-seccion').find('.descripcion')[0]).html(tinymce.get(1).getContent());
                                            $('#close-cancel').trigger('click');
                                            $('#button-send-form').removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form').removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                        });
                    </script>


                    <?php foreach ($albums as $key => $obj) { ?>
                        <div id="<?php echo $obj->id_miportal_album ?>" style="position:relative;">
                            <br>
                            <div class="myFotos" id="albums-seccion-<?php echo $obj->id_miportal_album ?>" style="padding:11px;">
                                <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-<?php echo $obj->id_miportal_album ?>" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                                <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-<?php echo $obj->id_miportal_album ?>" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
                                <div class="row">
                                    <div class="col m7 s12">
                                        <div class="row" style="padding:10px;">
                                            <div class="col s12 image-principal"><img class="imageprincipal" src="<?php echo empty($obj->imagenes[0]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[0]->mime . ";base64," . base64_encode($obj->imagenes[0]->base)) ?>"></div>
                                            <div class="col s12"><br></div>
                                            <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo empty($obj->imagenes[1]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[1]->mime . ";base64," . base64_encode($obj->imagenes[1]->base)) ?>"></div>
                                            <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo empty($obj->imagenes[2]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[2]->mime . ";base64," . base64_encode($obj->imagenes[2]->base)) ?>"></div>
                                            <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo empty($obj->imagenes[3]) ? base_url() . 'dist/file-portal/img/camara.png' : ("data:" . $obj->imagenes[3]->mime . ";base64," . base64_encode($obj->imagenes[3]->base)) ?>"></div>
                                        </div>
                                    </div>
                                    <div class="col m5 s12">
                                        <h5 class="title-album"><?php echo $obj->titulo ?></h5>
                                        <h6 class="number-album" data-contador="<?php echo count($obj->imagenes); ?>"><?php echo count($obj->imagenes); ?> fotos</h6>
                                        <h6 class="numberCom-album" data-contador="<?php echo count($obj->comentarios); ?>"><i class="material-icons">chat_bubble_outline</i> <?php echo count($obj->comentarios); ?> Comentario</h6>
                                    </div>
                                </div>
                            </div>

                            <div class="edit" id="albums-seccion-edit-<?php echo $obj->id_miportal_album ?>" style="display:none;">
                                <form id="send-form-<?php echo $obj->id_miportal_album ?>">
                                    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-<?php echo $obj->id_miportal_album ?>" type="button" ><i class="material-icons">close</i></button>
                                    <h6><b>T&iacute;tulo*</b></h6>
                                    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="tituloalbum-<?php echo $obj->id_miportal_album ?>" value="<?php echo $obj->titulo; ?>" placeholder="T&iacute;tulo" />
                                    <h6><b>Descripci&oacute;n</b></h6>
                                    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionalbum-<?php echo $obj->id_miportal_album ?>"><?php echo $obj->descripcion; ?></textarea>
                                    <br><br>
                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-<?php echo $obj->id_miportal_album ?>" type="button" >GUARDAR</button>
                                    <button class="waves-effect waves-light btn-flat" id="close-cancel-<?php echo $obj->id_miportal_album ?>" type="button" >Cancelar</button>
                                </form>
                                <form class="dropzone" drop-zone="" id="dropzone-<?php echo $obj->id_miportal_album ?>"></form>
                                <h6><b>Comentarios</b></h6>
                                <hr>
                                <div class="libro-comentario">
                                    <?php foreach ($obj->comentarios as $i => $temp) { ?>
                                        <div class="comentario-<?php echo encrypt($temp->id_miportal_album_comentario) ?>">
                                            <span class="lc-titulo"><?php echo $temp->nombre ?></span>
                                            <span class="lc-fecha">&#183;<?php echo relativeTimeFormat($temp->fecha) ?></span>
                                            <span class="lc-action">&#183;<button class="deleteComentario-<?php echo $obj->id_miportal_album ?> btn-flat" data-id='<?php echo encrypt($temp->id_miportal_album_comentario) ?>'><i class="material-icons">delete</i>Borrar</button></span>
                                            <p><?php echo $temp->comentario ?></p>
                                        </div>            
                                    <?php } ?>
                                </div>
                            </div>
                            <br>
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('#close-edit-<?php echo $obj->id_miportal_album ?>').on('click', function () {
                                    $('#albums-seccion-edit-<?php echo $obj->id_miportal_album ?>').hide();
                                    $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').show();
                                    $("#albums-seccion-<?php echo $obj->id_miportal_album ?>").mouseover(function () {
                                        $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_album ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_album ?>').show();
                                    });

                                    $("#albums-seccion-<?php echo $obj->id_miportal_album ?>").mouseout(function () {
                                        $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').attr('style', 'padding:10px; border: 1px solid #d9d9d9; opacity:1;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_album ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_album ?>').hide();
                                    });
                                });

                                $('#dropzone-<?php echo $obj->id_miportal_album ?>').dropzone({
                                    url: "<?php echo base_url() ?>novios/miweb/home/upload_image/<?php echo $obj->id_miportal_album ?>",
                                                maxFilesize: 3,
                                                method: "POST",
                                                paramName: "uploadfile",
                                                maxThumbnailFilesize: 6,
                                                acceptedFiles: "image/*",
                                                addRemoveLinks: true,
                                                dictCancelUploadConfirmation: "Estas seguro?",
                                                dictRemoveFile: 'Eliminar imagen',
                                                dictCancelUpload: 'Cancelar carga',
                                                dictInvalidFileType: "No se puede cargar este tipo de archivos.",
                                                dictFileTooBig: "La imagen es muy grande ({{filesize}}MiB). Tama&ntilde;o m&aacute;ximo: {{maxFilesize}}MiB.",
                                                dictDefaultMessage: '<center><i class="material-icons" style="font-size: 28px;">file_upload</i><br><b>Suelte la imagen o haga click aqu&iacute; para cargar</b></center>',
                                                init: function () {
            <?php foreach ($obj->imagenes as $i => $temp) { ?>
                                                        var mockFile = {
                                                            name: "hgdgaggg3333-<?php echo encrypt($temp->id_miportal_imagen) ?>",
                                                            size: 12345,
                                                            type: 'image/jpeg',
                                                            status: this.ADDED,
                                                        };
                                                        this.emit("addedfile", mockFile);
                                                        this.emit("thumbnail", mockFile, '<?php echo "data:" . $temp->mime . ";base64," . base64_encode($temp->base) ?>');
                                                        mockFile.previewElement.classList.add('dz-success');
                                                        mockFile.previewElement.classList.add('dz-complete');
                                                        this.files.push(mockFile);
            <?php } ?>

                                                    this.on('success', function (file, json) {
                                                        $(file.previewElement).data('id', json.id);
                                                        restartImage('<?php echo $obj->id_miportal_album ?>');
                                                        var element = $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').find('.number-album')[0];
                                                        $(element).data('contador', $(element).data('contador') + 1);
                                                        $(element).text($(element).data('contador') + ' fotos');
                                                    });
                                                    this.on('addedfile', function () {
                                                        console.log('addedfile');
                                                    });
                                                    this.on('drop', function () {
                                                        console.log('drop');
                                                    });
                                                },
                                                removedfile: function (file) {
                                                    var id = file.name;
                                                    if (id.indexOf('hgdgaggg3333-') > -1) {
                                                        id = id.split("hgdgaggg3333-")[1];
                                                    } else {
                                                        id = $(file.previewElement).data('id')
                                                    }
                                                    $.ajax({
                                                        url: '<?php echo base_url() ?>novios/miweb/home/upload_image/<?php echo $obj->id_miportal_album ?>',
                                                                            method: 'POST',
                                                                            timeout: 4000,
                                                                            data: {
                                                                                id: id,
                                                                            }
                                                                        }).done(function () {
                                                                            console.log('success delete');
                                                                            var element = $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').find('.number-album')[0];
                                                                            $(element).data('contador', $(element).data('contador') - 1);
                                                                            $(element).text($(element).data('contador') + ' fotos');
                                                                            var _ref;
                                                                            (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

                                                                            restartImage('<?php echo $obj->id_miportal_album ?>');
                                                                            return true;
                                                                        }).fail(function () {
                                                                            console.log('fail');
                                                                        });
                                                                    }
                                                                });

                                                                $('.deleteComentario-<?php echo $obj->id_miportal_album ?>').on('click', function () {
                                                                    var id = $(this).data('id');
                                                                    var r = confirm("Seguro que deseas eliminar este comentario?");
                                                                    if (r == true) {
                                                                        $.ajax({
                                                                            url: '<?php echo base_url() ?>novios/miweb/home/delete_comentario',
                                                                            method: 'POST',
                                                                            timeout: 4000,
                                                                            data: {
                                                                                id_comment: id,
                                                                            },
                                                                        }).done(function () {
                                                                            $('.comentario-' + id).remove();
                                                                            var element = $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').find('.numberCom-album')[0];
                                                                            $(element).data('contador', $(element).data('contador') - 1);
                                                                            $(element).html('<i class="material-icons">chat_bubble_outline</i> ' + $(element).data('contador') + ' Comentario');
                                                                        }).fail(function () {
                                                                            console.log('error');
                                                                        });
                                                                    }
                                                                });

                                                                $('#close-cancel-<?php echo $obj->id_miportal_album ?>').on('click', function () {
                                                                    $('#albums-seccion-edit-<?php echo $obj->id_miportal_album ?>').hide();
                                                                    $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').show();
                                                                    $("#albums-seccion-<?php echo $obj->id_miportal_album ?>").mouseover(function () {
                                                                        $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                                        $('#button-editseccion-<?php echo $obj->id_miportal_album ?>').show();
                                                                        $('#button-delete-<?php echo $obj->id_miportal_album ?>').show();
                                                                    });

                                                                    $("#albums-seccion-<?php echo $obj->id_miportal_album ?>").mouseout(function () {
                                                                        $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').attr('style', 'padding:10px; border: 1px solid #d9d9d9; opacity:1;');
                                                                        $('#button-editseccion-<?php echo $obj->id_miportal_album ?>').hide();
                                                                        $('#button-delete-<?php echo $obj->id_miportal_album ?>').hide();
                                                                    });
                                                                });

                                                                $('#button-editseccion-<?php echo $obj->id_miportal_album ?>').on('click', function () {
                                                                    $("#albums-seccion-<?php echo $obj->id_miportal_album ?>").unbind("mouseover");
                                                                    $("#albums-seccion-<?php echo $obj->id_miportal_album ?>").unbind("mouseout");
                                                                    $('#albums-seccion-edit-<?php echo $obj->id_miportal_album ?>').show();
                                                                    $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').hide();
                                                                });

                                                                $("#albums-seccion-<?php echo $obj->id_miportal_album ?>").mouseover(function () {
                                                                    $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                                    $('#button-editseccion-<?php echo $obj->id_miportal_album ?>').show();
                                                                    $('#button-delete-<?php echo $obj->id_miportal_album ?>').show();
                                                                });

                                                                $("#albums-seccion-<?php echo $obj->id_miportal_album ?>").mouseout(function () {
                                                                    $('#albums-seccion-<?php echo $obj->id_miportal_album ?>').attr('style', 'padding:10px; border: 1px solid #d9d9d9; opacity:1;');
                                                                    $('#button-editseccion-<?php echo $obj->id_miportal_album ?>').hide();
                                                                    $('#button-delete-<?php echo $obj->id_miportal_album ?>').hide();
                                                                });

                                                                $('#button-send-form-<?php echo $obj->id_miportal_album ?>').on('click', function () {
                                                                    if ($("#send-form-<?php echo $obj->id_miportal_album ?>").validationEngine('validate')) {
                                                                        var descrp = tinymce.get(<?php echo $key + 2 ?>).getContent();
                                                                        if (descrp != '' && descrp != null && descrp.length > 16) {
                                                                            $('#button-send-form-<?php echo $obj->id_miportal_album ?>').attr('disabled', 'true');

                                                                            $.ajax({
                                                                                url: '<?php echo base_url() ?>novios/miweb/home/update_album',
                                                                                method: 'POST',
                                                                                timeout: 4000,
                                                                                data: {
                                                                                    'titulo': $('#tituloalbum-<?php echo $obj->id_miportal_album ?>').val(),
                                                                                    'descripcion': descrp,
                                                                                    'i': '<?php echo $obj->id_miportal_album ?>',
                                                                                },
                                                                            }).done(function () {
                                                                                var sup = $('#albums-seccion-<?php echo $obj->id_miportal_album ?>');
                                                                                $(sup.find('.title-album')[0]).text($('#tituloalbum-<?php echo $obj->id_miportal_album ?>').val());

                                                                                $('#close-cancel-<?php echo $obj->id_miportal_album ?>').trigger('click');
                                                                                $('#button-send-form-<?php echo $obj->id_miportal_album ?>').removeAttr('disabled');
                                                                            }).fail(function () {
                                                                                $('#button-send-form-<?php echo $obj->id_miportal_album ?>').removeAttr('disabled');
                                                                            });
                                                                        } else {
                                                                            alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                                                        }
                                                                    } else {
                                                                        console.log('error');
                                                                    }
                                                                });

                                                                $('#button-delete-<?php echo $obj->id_miportal_album ?>').on('click', function () {
                                                                    var r = confirm('¿Seguro que deseas eliminar este album?');
                                                                    if (r == true) {
                                                                        $.ajax({
                                                                            url: '<?php echo base_url() ?>novios/miweb/home/delete_album',
                                                                            method: 'POST',
                                                                            timeout: 4000,
                                                                            data: {
                                                                                'id-album': '<?php echo $obj->id_miportal_album ?>',
                                                                            },
                                                                        }).done(function () {
                                                                            $('#<?php echo $obj->id_miportal_album ?>').remove();
                                                                        }).fail(function () {
                                                                            console.log('error');
                                                                        });
                                                                    }
                                                                });
                                                            });
                        </script>
                    <?php } ?>

                    <div id="add_before_fotos"></div>

                    <div class="box">
                        <img src="<?php echo base_url() ?>dist/file-portal/img/album.png">
                        <br>
                        <button class="waves-effect waves-light btn-flat boton" id="add_foto">A&ntilde;adir &aacute;lbum</button>
                    </div>


                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<script type="text" id="template-1" >
    <br>
    <div class="myFotos" id="albums-seccion-" style="display:none; padding:11px;">
    <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
    <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
    <div class="row">
    <div class="col m6 s12">
    <div class="row">
    <div class="col s12 image-principal"><img class="imageprincipal" src="<?php echo base_url() . 'dist/file-portal/img/camara.png' ?>"></div>
    <div class="col s12"><br></div>
    <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo base_url() . 'dist/file-portal/img/camara.png' ?>"></div>
    <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo base_url() . 'dist/file-portal/img/camara.png' ?>"></div>
    <div class="col s4 image-secundarias"><img class="imagesecundary" src="<?php echo base_url() . 'dist/file-portal/img/camara.png' ?>"></div>
    </div>
    </div>
    <div class="col m6 s12">
    <h5 class="title-album"></h5>
    <h6 class="number-album" data-contador="0">0 fotos</h6>
    <h6 class="numberCom-album" data-contador="0"><i class="material-icons">chat_bubble_outline</i> 0 Comentario</h6>
    </div>
    </div>
    </div>

    <div class="edit" id="albums-seccion-edit-">
    <form id="send-form-">
    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-" type="button" ><i class="material-icons">close</i></button>
    <h6><b>T&iacute;tulo*</b></h6>
    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="tituloalbum-" value="" placeholder="T&iacute;tulo" />
    <h6><b>Descripci&oacute;n</b></h6>
    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionalbum-"></textarea>
    <br><br>
    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-" type="button" >GUARDAR</button>
    <button class="waves-effect waves-light btn-flat" id="close-cancel-" type="button" >Cancelar</button>
    </form>
    <form class="dropzone" drop-zone="" id="dropzone-"></form>
    </div>
</script>

<script>
    var count_template = <?php echo count($albums) + 1; ?>;

    function restartImage(id) {
        var elemento = $('#' + id).find('.dz-image img');
        if (elemento.length == 0) {
            $($('#albums-seccion-' + id).find('.imageprincipal')[0]).attr('src', '<?php echo base_url() ?>dist/file-portal/img/camara.png');
            $($('#albums-seccion-' + id).find('.imagesecundary')).each(function () {
                $(this).attr('src', '<?php echo base_url() ?>dist/file-portal/img/camara.png');
            });
        } else if (elemento.length == 1) {
            $($('#albums-seccion-' + id).find('.imageprincipal')[0]).attr('src', $(elemento[0]).attr('src'));
            $($('#albums-seccion-' + id).find('.imagesecundary')).each(function () {
                $(this).attr('src', '<?php echo base_url() ?>dist/file-portal/img/camara.png');
            });
        } else {
            $($('#albums-seccion-' + id).find('.imagesecundary')).each(function () {
                $(this).attr('src', '<?php echo base_url() ?>dist/file-portal/img/camara.png');
            });
            for (var i = 0; i < elemento.length; i++) {
                if (i == 0) {
                    $($('#albums-seccion-' + id).find('.imageprincipal')[0]).attr('src', $(elemento[i]).attr('src'));
                } else {
                    $($('#albums-seccion-' + id).find('.imagesecundary')[i - 1]).attr('src', $(elemento[i]).attr('src'));
                }
            }
        }
    }

    $(document).ready(function () {
        $('#add_foto').on('click', function () {
            count_template++;
            var div = document.createElement("div");
            div.setAttribute('id', count_template);
            div.setAttribute('data-id', 0);
            div.setAttribute('style', 'position:relative;');
            div.innerHTML = $("#template-1").html();

            $($(div).find('#button-editseccion-')[0]).attr('id', 'button-editseccion-' + count_template);
            $($(div).find('#button-delete-')[0]).attr('id', 'button-delete-' + count_template);

            $($(div).find('#albums-seccion-')[0]).attr('id', 'albums-seccion-' + count_template);
            $($(div).find('#albums-seccion-' + count_template)[0]).mouseover(function () {
                var ident = ($(this).attr('id')).split('albums-seccion-')[1];
                $('#albums-seccion-' + ident).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('#button-editseccion-' + ident).show();
                $('#button-delete-' + ident).show();
            });
            $($(div).find('#albums-seccion-' + count_template)[0]).mouseout(function () {
                var ident = ($(this).attr('id')).split('albums-seccion-')[1];
                $('#albums-seccion-' + ident).attr('style', 'padding:10px; border: 1px solid #d9d9d9;');
                $('#button-editseccion-' + ident).hide();
                $('#button-delete-' + ident).hide();
            });
            $($(div).find('#albums-seccion-edit-')[0]).attr('id', 'albums-seccion-edit-' + count_template);
            $($(div).find('#tituloalbum-')[0]).attr('id', 'tituloalbum-' + count_template);
            $($(div).find('#descripcionalbum-')[0]).attr('id', 'descripcionalbum-' + count_template);

            $($(div).find('#dropzone-')[0]).attr('id', 'dropzone-' + count_template);
            $($(div).find('#dropzone-' + count_template)[0]).dropzone({
                url: "<?php echo base_url() ?>novios/miweb/home/upload_image/0",
                maxFilesize: 3,
                method: "POST",
                paramName: "uploadfile",
                maxThumbnailFilesize: 6,
                acceptedFiles: "image/*",
                addRemoveLinks: true,
                dictCancelUploadConfirmation: "Estas seguro?",
                dictRemoveFile: 'Eliminar imagen',
                dictCancelUpload: 'Cancelar carga',
                dictInvalidFileType: "No se puede cargar este tipo de archivos.",
                dictFileTooBig: "La imagen es muy grande ({{filesize}}MiB). Tama&ntilde;o m&aacute;ximo: {{maxFilesize}}MiB.",
                dictDefaultMessage: '<center><i class="material-icons" style="font-size: 28px;">file_upload</i><br><b>Suelte la imagen o haga click aqu&iacute; para cargar</b></center>',
                init: function () {
                    this.on('success', function (file, json) {
                        var ident = (this.element.id).split('dropzone-')[1];
                        if ($('#' + ident).data('id') != 0) {
                            $.ajax({
                                url: '<?php echo base_url() ?>novios/miweb/home/insert_relacionimage',
                                method: 'POST',
                                timeout: 4000,
                                data: {
                                    id_album: $('#' + ident).data('id'),
                                    id_imagen: json.id,
                                }
                            }).done(function () {}).fail(function () {});
                        }
                        $(file.previewElement).data('id', json.id);
                        restartImage(ident);
                        var element = $('#albums-seccion-' + ident).find('.number-album')[0];
                        $(element).data('contador', $(element).data('contador') + 1);
                        $(element).text($(element).data('contador') + ' fotos');

                    });
                    this.on('addedfile', function () {
                        console.log('addedfile');
                    });
                    this.on('drop', function () {
                        console.log('drop');
                    });
                },
                removedfile: function (file) {
                    var ident = (this.element.id).split('dropzone-')[1];
                    var id = $(file.previewElement).data('id');
                    $.ajax({
                        url: '<?php echo base_url() ?>novios/miweb/home/upload_image/' + $('#' + ident).data('id'),
                        method: 'POST',
                        timeout: 4000,
                        data: {
                            id: id,
                        }
                    }).done(function () {
                        console.log('success delete');
                        var element = $('#albums-seccion-' + ident).find('.number-album')[0];
                        $(element).data('contador', $(element).data('contador') - 1);
                        $(element).text($(element).data('contador') + ' fotos');
                        var _ref;
                        (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

                        restartImage(ident);
                        return true;
                    }).fail(function () {
                        console.log('fail');
                    });
                }
            });

            $($(div).find('#send-form-')[0]).attr('id', 'send-form-' + count_template);
            $($(div).find('#send-form-' + count_template)[0]).validationEngine();

            $($(div).find('#button-send-form-')[0]).attr('id', 'button-send-form-' + count_template);
            $($(div).find('#button-send-form-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('button-send-form-')[1];
                if ($("#send-form-" + ident).validationEngine('validate')) {
                    var descrp = tinymce.get(ident).getContent();
                    if (descrp != '' && descrp != null && descrp.length > 16) {
                        $('#button-send-form-' + ident).attr('disabled', 'true');
                        var imagenes = [];
                        $($('#dropzone-' + ident).find('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete')).each(function () {
                            imagenes.push($(this).data('id'));
                        });
                        $.ajax({
                            url: '<?php echo base_url() ?>novios/miweb/home/insert_album',
                            method: 'POST',
                            timeout: 4000,
                            data: {
                                'titulo': $('#tituloalbum-' + ident).val(),
                                'descripcion': descrp,
                                'imagenes': imagenes,
                                's': '<?php echo $id_seccion ?>',
                            },
                        }).done(function (result) {
                            var sup = $('#albums-seccion-' + ident);
                            $(sup.find('.title-album')[0]).text($('#tituloalbum-' + ident).val());
                            $('#' + ident).data('id', result.id);

                            $('#close-edit-' + ident).unbind("click");
                            $('#close-edit-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('close-edit-')[1];
                                $('#albums-seccion-edit-' + sup_id).hide();
                                $('#albums-seccion-' + sup_id).show();
                                $("#albums-seccion-" + sup_id).mouseover(function () {
                                    $('#albums-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-' + sup_id).show();
                                    $('#button-delete-' + sup_id).show();
                                });

                                $("#albums-seccion-" + sup_id).mouseout(function () {
                                    $('#albums-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#button-editseccion-' + sup_id).hide();
                                    $('#button-delete-' + sup_id).hide();
                                });
                            });

                            $('#close-cancel-' + ident).unbind("click");
                            $('#close-cancel-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('close-cancel-')[1];
                                $('#albums-seccion-edit-' + sup_id).hide();
                                $('#albums-seccion-' + sup_id).show();
                                $("#albums-seccion-" + sup_id).mouseover(function () {
                                    $('#albums-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-' + sup_id).show();
                                    $('#button-delete-' + sup_id).show();
                                });

                                $("#albums-seccion-" + sup_id).mouseout(function () {
                                    $('#albums-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#button-editseccion-' + sup_id).hide();
                                    $('#button-delete-' + sup_id).hide();
                                });
                            });

                            $('#button-editseccion-' + ident).show();
                            $('#button-editseccion-' + ident).unbind("click");
                            $('#button-editseccion-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-editseccion-')[1];
                                $("#albums-seccion-" + sup_id).unbind("mouseover");
                                $("#albums-seccion-" + sup_id).unbind("mouseout");
                                $('#albums-seccion-edit-' + sup_id).show();
                                $('#albums-seccion-' + sup_id).hide();
                            });

                            $('#button-delete-' + ident).unbind("click");
                            $('#button-delete-' + ident).data('id', result.id);
                            $('#button-delete-' + ident).show();
                            $('#button-delete-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-delete-')[1];
                                var r = confirm('¿Seguro que deseas eliminar este album?');
                                if (r == true) {
                                    $.ajax({
                                        url: '<?php echo base_url() ?>novios/miweb/home/delete_album',
                                        method: 'POST',
                                        timeout: 4000,
                                        data: {
                                            'id-album': $(this).data('id'),
                                        },
                                    }).done(function () {
                                        $('#' + sup_id).remove();
                                    }).fail(function () {
                                        console.log('error');
                                    });
                                }
                            });

                            $('#button-send-form-' + ident).unbind("click");
                            $('#button-send-form-' + ident).data('id', result.id);
                            $('#button-send-form-' + ident).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];
                                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                                    var descrp = tinymce.get(sup_id).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form-' + sup_id).attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>novios/miweb/home/update_album',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'titulo': $('#tituloalbum-' + sup_id).val(),
                                                'descripcion': descrp,
                                                'i': $(this).data('id'),
                                            },
                                        }).done(function () {
                                            var sup = $('#albums-seccion-' + sup_id);
                                            $(sup.find('.title-album')[0]).text($('#tituloalbum-' + sup_id).val());

                                            $('#close-cancel-' + sup_id).trigger('click');
                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });

                            $('#close-cancel-' + ident).trigger('click');
                            $('#button-send-form-' + ident).removeAttr('disabled');
                        }).fail(function () {
                            $('#button-send-form-' + ident).removeAttr('disabled');
                        });
                    } else {
                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                    }
                } else {
                    console.log('error');
                }
            });

            $($(div).find('#close-cancel-')[0]).attr('id', 'close-cancel-' + count_template);
            $($(div).find('#close-cancel-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('close-cancel-')[1];
                $($('#dropzone-' + ident).find('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete')).each(function () {
                    $.ajax({
                        url: '<?php echo base_url() ?>novios/miweb/home/upload_image',
                        method: 'POST',
                        timeout: 4000,
                        data: {
                            id: $(this).data('id'),
                        }
                    }).done(function () {
                    }).fail(function () {});
                });
                $('#' + ident).remove();
            });


            $($(div).find('#close-edit-')[0]).attr('id', 'close-edit-' + count_template);
            $($(div).find('#close-edit-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('close-edit-')[1];
                $($('#dropzone-' + ident).find('.dz-preview.dz-processing.dz-image-preview.dz-success.dz-complete')).each(function () {
                    $.ajax({
                        url: '<?php echo base_url() ?>novios/miweb/home/upload_image',
                        method: 'POST',
                        timeout: 4000,
                        data: {
                            id: $(this).data('id'),
                        }
                    }).done(function () {
                    }).fail(function () {});
                });
                $('#' + ident).remove();
            });

            $('#add_before_fotos').append(div);


            tinymce.init({
                selector: '.tinymce-editor',
                plugins: "autoresize link",
                menubar: '',
                height: "250",
                toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
            });

        });
    });

</script>