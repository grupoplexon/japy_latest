<!-- B  O  D  Y -->
<div class="container-body">
    <div class="body-container-web detail">
        <?php
        foreach ($miportal_seccion as $key => $value) {
            if ($value->tipo == 'bienvenido') {
                ?>
                <div class="welcome" style="padding:11px;">
                    <button class="waves-effect waves-light btn-flat button-edit" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                    <h4 class="titulo"><?php echo $value->subtitulo; ?></h4>
                    <img class="imagen" style="<?php echo empty($bienvenido) ? 'display:none' : ''; ?>" src="<?php echo!empty($bienvenido) ? ("data:$bienvenido->mime;base64," . base64_encode($bienvenido->base)) : ''; ?>">
                    <div class="descripcion"><?php echo $value->descripcion; ?></div>
                </div>
                <div class="welcome-edit edit" style="display:none;">
                    <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                    <form method="post">
                        <br>
                        <h6><b>T&iacute;tulo*</b></h6>
                        <input class="formulario-invitaciones" style="width:100%" type="text" name="subtitle-bienvenido" value="<?php echo $value->subtitulo; ?>" placeholder="!Bienvenido a nuestra boda!" required />
                        <div class="space-image">
                            <div class="file-field input-field uploadimage">
                                <div class="waves-effect waves-light btn-flat white botn">
                                    <span><i class="material-icons left">camera_enhance</i>&nbsp;&nbsp;<?php echo empty($bienvenido)?'Subir':'Cambiar' ?> imagen</span>
                                    <input type="file" accept="image/*" onchange="openFile(event, 'welcome')">
                                </div>
                            </div>
                            <button class="waves-effect waves-light btn-flat white botn" type="button" id="image-deletebienvenido" style="<?php echo empty($bienvenido)?'display:none;':'' ?>" ><i class="material-icons">delete_forever</i></button>
                            <input type="hidden" name="id-bienvenido" value="<?php echo $value->id_miportal_seccion ?>"/>
                            <img class="responsive-img image-bienvenido" style="width:100%" src="<?php echo empty($bienvenido)? base_url().'dist/file-portal/img/album.png':"data:$bienvenido->mime;base64," . base64_encode($bienvenido->base) ?>" alt=""/>
                            <input type="hidden" name="image-bienvenido-id" value="<?php echo empty($bienvenido)?'':encrypt($bienvenido->id_miportal_imagen) ?>" />
                            <input type="hidden" name="image-bienvenido" value="<?php echo empty($bienvenido)?'':"data:$bienvenido->mime;base64," . base64_encode($bienvenido->base) ?>" />
                        </div>
                        <h6><b>Descripci&oacute;n*</b></h6>
                        <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-bienvenida" id="welcomedescripcion"><?php echo $value->descripcion; ?></textarea>
                        <br>
                        <button class="waves-effect waves-light btn-flat dorado-2 white-text" type="submit" >GUARDAR</button>
                        <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                    </form>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#close-edit').on('click', function () {
            $('.welcome-edit').hide();
            $('.welcome').show();
            $(".welcome").mouseover(function () {
                $('.welcome').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('.button-edit').show();
            });

            $(".welcome").mouseout(function () {
                $('.welcome').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                $('.button-edit').hide();
            });
        });
        
        $('#close-cancel').on('click', function () {
            $('.welcome-edit').hide();
            $('.welcome').show();
            $(".welcome").mouseover(function () {
                $('.welcome').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('.button-edit').show();
            });

            $(".welcome").mouseout(function () {
                $('.welcome').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                $('.button-edit').hide();
            });
        });
        
        $('.button-edit').on('click',function(){
            $(".welcome").unbind("mouseover");
            $(".welcome").unbind("mouseout");
            $('.welcome-edit').show();
            $('.welcome').hide();
        });
        
        $(".welcome").mouseover(function () {
            $('.welcome').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
            $('.button-edit').show();
        });
        
        $(".welcome").mouseout(function () {
            $('.welcome').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
            $('.button-edit').hide();
        });
        
        $('#image-deletebienvenido').on('click',function(){
            $('[name="image-bienvenido"]').val('');
            $('#image-deletebienvenido').hide();
            $('.image-bienvenido').attr('src','<?php echo base_url().'dist/file-portal/img/album_bienvenida.png' ?>');
        });
        
    });
</script>