<div class="container-body">
    <div class="body-container-web detail">
        <?php
        foreach ($miportal_seccion as $key => $value) {
            if ($value->tipo == 'libro') {
                ?>
                <div class="libro" style="padding:11px;">
                    <button class="waves-effect waves-light btn-flat button-edit" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                    <h4 class="titulo"><?php echo $value->subtitulo ?></h4>
                    <div class="descripcion"><?php echo $value->descripcion ?></div>



                    <div class="seccion">

                        <h5 class="titulo">Comentario</h5>
                        <hr class="hr-separador">
                        <div class="libro-comentario">
                            <?php foreach ($comentarios as $key => $obj) { ?>
                                <div class="comentario-<?php echo $obj->id_miportal_libro ?>">
                                    <span class="lc-titulo"><?php echo $obj->nombre ?></span>
                                    <span class="lc-fecha">&#183;<?php echo relativeTimeFormat($obj->fecha) ?></span>
                                    <span class="lc-action">&#183;<button class="btn-flat" onclick="deleteComent('<?php echo $obj->id_miportal_libro ?>');"><i class="material-icons">delete</i>Borrar</button></span>
                                    <p><?php echo $obj->comentario ?></p>
                                </div>            
                            <?php } ?>
                        </div>
                    </div>




                    <div class="seccion">
                        <h5 class="titulo">Deja tu comentario!!</h5>
                        <hr class="hr-separador">
                        <form method="post">
                            <div class="row formulario">
                                <div class="col m6 s12">
                                    <p>Nombre</p>
                                    <input class="form-portal" name="nombre" type="text">
                                </div>
                                <div class="col m6 s12">
                                    <p>E-Mail</p>
                                    <input class="form-portal" name="email" type="email" >
                                </div>
                                <div class="col s12">
                                    <p>Comentario</p>
                                    <textarea class="form-portal" name="comentario"></textarea>
                                </div>
                            </div>
                            <button class="waves-effect waves-light btn-flat boton" type="submit">Enviar Comentario</button>
                        </form>
                    </div>

                </div>


                <div class="libro-edit edit" style="display:none;">
                    <form method="POST" id="send-form">
                        <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                        <br>
                        <input type="hidden" name="id-libro" value="<?php echo $value->id_miportal_seccion ?>" required />
                        <h6><b>T&iacute;tulo*</b></h6>
                        <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="subtitle-libro" value="<?php echo $value->subtitulo; ?>" placeholder="T&iacute;tulo" />
                        <h6><b>Descripci&oacute;n*</b></h6>
                        <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-libro" id="librodescripcion"><?php echo $value->descripcion; ?></textarea>
                        <br>
                        <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                        <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                        <br><br><br>
                        <h6><b>Comentarios</b></h6>
                        <hr>
                        <div class="libro-comentario">
                            <?php foreach ($comentarios as $key => $obj) { ?>
                                <div class="comentario-<?php echo $obj->id_miportal_libro ?>">
                                    <span class="lc-titulo"><?php echo $obj->nombre ?></span>
                                    <span class="lc-fecha">&#183;<?php echo relativeTimeFormat($obj->fecha) ?></span>
                                    <span class="lc-action">&#183;<button class="btn-flat" onclick="deleteComent('<?php echo $obj->id_miportal_libro ?>');"><i class="material-icons">delete</i>Borrar</button></span>
                                    <p><?php echo $obj->comentario ?></p>
                                </div>            
                            <?php } ?>
                        </div>
                    </form>
                </div>


                <?php
            }
        }
        ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#send-form").validationEngine();

        $('#close-edit').on('click', function () {
            $('.libro-edit').hide();
            $('.libro').show();
            $(".libro").mouseover(function () {
                $('.libro').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('.button-edit').show();
            });

            $(".libro").mouseout(function () {
                $('.libro').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                $('.button-edit').hide();
            });
        });

        $('#close-cancel').on('click', function () {
            $('.libro-edit').hide();
            $('.libro').show();
            $(".libro").mouseover(function () {
                $('.libro').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                $('.button-edit').show();
            });

            $(".libro").mouseout(function () {
                $('.libro').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                $('.button-edit').hide();
            });
        });

        $('.button-edit').on('click', function () {
            $(".libro").unbind("mouseover");
            $(".libro").unbind("mouseout");
            $('.libro-edit').show();
            $('.libro').hide();
        });

        $(".libro").mouseover(function () {
            $('.libro').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
            $('.button-edit').show();
        });

        $(".libro").mouseout(function () {
            $('.libro').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
            $('.button-edit').hide();
        });



        $('#button-send-form').on('click', function () {
            if ($("#send-form").validationEngine('validate')) {
                var descrp = tinymce.get(1).getContent();
                if (descrp != '' && descrp != null && descrp.length > 16) {
                    $('#button-send-form').attr('disabled', 'true');
                    $("#send-form").submit();
                } else {
                    alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                }
            } else {
                console.log('error');
            }

        });


    });

    function deleteComent(id) {
        var r = confirm("Seguro que deseas eliminar este comentario?");
        if (r == true) {
            $.ajax({
                url: '<?php echo base_url() ?>index.php/novios/miweb/home/deleteComent',
                method: 'POST',
                timeout: 4000,
                data: {
                    id_comment: id,
                },
            }).done(function () {
                $('.comentario-' + id).remove();
            }).fail(function () {
                console.log('error');
            });
        }
    }
</script>    
