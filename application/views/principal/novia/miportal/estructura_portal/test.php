<div class="container-body">
    <div class="body-container-web detail">
        <?php
        $id_seccion = '';
        foreach ($miportal_seccion as $contador => $value) {
            if ($value->tipo == 'test') {
                $id_seccion = $value->id_miportal_seccion;
                ?>
                <div class="test">

                    <!-- SECCION PRINCIPAL -->
                    <div id="tests-seccion">
                        <button class="waves-effect waves-light btn-flat button-edit" id="editar-testprincipal" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                        <h4 class="titulo"><?php echo $value->titulo ?></h4>
                        <div class="descripcion"><?php echo $value->descripcion ?></div>
                    </div>
                    <div class="edit" id="tests-seccion-edit" style="display: none;">
                        <form id="send-form">
                            <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                            <br>
                            <input type="hidden" name="id-test" value="<?php echo $value->id_miportal_seccion ?>" required />
                            <h6><b>T&iacute;tulo*</b></h6>
                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="titulo-test" value="<?php echo $value->titulo; ?>" placeholder="T&iacute;tulo" />
                            <h6><b>Descripci&oacute;n*</b></h6>
                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-test" id="testdescripcion"><?php echo $value->descripcion; ?></textarea>
                            <br>
                            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                            <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                        </form>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $("#send-form").validationEngine();

                            $('#close-edit').on('click', function () {
                                $('#tests-seccion-edit').hide();
                                $('#tests-seccion').show();
                                $("#tests-seccion").mouseover(function () {
                                    $('#tests-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-testprincipal').show();
                                });

                                $("#tests-seccion").mouseout(function () {
                                    $('#tests-seccion').attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                    $('#editar-testprincipal').hide();
                                });
                            });

                            $('#close-cancel').on('click', function () {
                                $('#tests-seccion-edit').hide();
                                $('#tests-seccion').show();
                                $("#tests-seccion").mouseover(function () {
                                    $('#tests-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-testprincipal').show();
                                });

                                $("#tests-seccion").mouseout(function () {
                                    $('#tests-seccion').attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                    $('#editar-testprincipal').hide();
                                });
                            });

                            $('.button-edit').on('click', function () {
                                $("#tests-seccion").unbind("mouseover");
                                $("#tests-seccion").unbind("mouseout");
                                $('#tests-seccion-edit').show();
                                $('#tests-seccion').hide();
                            });

                            $("#tests-seccion").mouseover(function () {
                                $('#tests-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#editar-testprincipal').show();
                            });

                            $("#tests-seccion").mouseout(function () {
                                $('#tests-seccion').attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                $('#editar-testprincipal').hide();
                            });



                            $('#button-send-form').on('click', function () {
                                if ($("#send-form").validationEngine('validate')) {
                                    var descrp = tinymce.get(1).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form').attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_headerevento',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-evento': $('[name="id-test"]').val(),
                                                'titulo-evento': $('[name="titulo-test"]').val(),
                                                'descripcion-evento': tinymce.get(1).getContent(),
                                            },
                                        }).done(function () {
                                            $($('#tests-seccion').find('.titulo')[0]).text($('[name="titulo-test"]').val());
                                            $($('#tests-seccion').find('.descripcion')[0]).html(tinymce.get(1).getContent());
                                            $('#close-cancel').trigger('click');
                                            $('#button-send-form').removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form').removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                            
                            Highcharts.setOptions({
                                colors: ['#d32f2f', '#4e342e','#c2185b', '#00838f', '#0288d1','#424242', '#00796b', '#f9a797', '#5e35b1', '#2e7d32', '#f57c00']
                            });
                        });
                    </script>


                    <?php foreach ($tests as $key => $obj) { ?>
                        <div id="<?php echo $obj->id_miportal_test ?>" style="position:relative;">
                            <hr class="hr-separador">
                            <div class="tests-agregados" id="tests-seccion-<?php echo $obj->id_miportal_test ?>">
                                <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-<?php echo $obj->id_miportal_test ?>" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                                <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-<?php echo $obj->id_miportal_test ?>" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
                                <h5 class="titulo" style="text-align:left;"><?php echo $obj->titulo; ?></h5>
                                <div class="descripcion"><?php echo $obj->descripcion; ?></div>
                                <div id="grafica-<?php echo $obj->id_miportal_test ?>" style="width:90%; height: 250px;"></div>
                            </div>
                            
                            
                            <div class="edit" id="tests-seccion-edit-<?php echo $obj->id_miportal_test ?>" style="display:none;" >
                                <form id="send-form-<?php echo $obj->id_miportal_test ?>">
                                    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-<?php echo $obj->id_miportal_test ?>" type="button" ><i class="material-icons">close</i></button>
                                    <h6><b>T&iacute;tulo*</b></h6>
                                    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="titulotest-<?php echo $obj->id_miportal_test ?>" value="<?php echo $obj->titulo; ?>" placeholder="T&iacute;tulo" />
                                    <h6><b>Descripci&oacute;n</b></h6>
                                    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripciontest-<?php echo $obj->id_miportal_test ?>"><?php echo $obj->descripcion; ?></textarea>
                                    <br><br>
                                    <div class="row">
                                        <div class="col s3">Respuesta A*</div>
                                        <div class="col s6">
                                            <input class="formulario-invitaciones validate[required,minSize[4]]" data-contador="<?php $res = 0; foreach ($obj->respuestas as $i => $value) {if($value->respuesta == 1){ $res = $value->total;}} echo $res; ?>" value="<?php echo $obj->answer1 ?>" style="width:100%" name="answer1-<?php echo $obj->id_miportal_test ?>" type="text" />
                                        </div>
                                        <div class="col s3">
                                            <p>
                                                <input class="respuestasTests" name="respuesta-<?php echo $obj->id_miportal_test ?>" value="1" type="radio" id="test1<?php echo $obj->id_miportal_test ?>" <?php echo ($obj->respuesta == 1)?'checked':'' ?> />
                                                <label for="test1<?php echo $obj->id_miportal_test ?>">Respuesta correcta</label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s3">Respuesta B*</div>
                                        <div class="col s6">
                                            <input class="formulario-invitaciones validate[required,minSize[4]]" data-contador="<?php $res = 0; foreach ($obj->respuestas as $i => $value) {if($value->respuesta == 2){ $res = $value->total;}} echo $res; ?>" value="<?php echo $obj->answer2 ?>" style="width:100%" name="answer2-<?php echo $obj->id_miportal_test ?>" type="text" />
                                        </div>
                                        <div class="col s3">
                                            <p>
                                                <input class="respuestasTests" name="respuesta-<?php echo $obj->id_miportal_test ?>" value="2" type="radio" id="test2<?php echo $obj->id_miportal_test ?>" <?php echo ($obj->respuesta == 2)?'checked':'' ?> />
                                                <label for="test2<?php echo $obj->id_miportal_test ?>">Respuesta correcta</label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s3">Respuesta C</div>
                                        <div class="col s6">
                                            <input class="formulario-invitaciones validate[minSize[4]]" data-contador="<?php $res = 0; foreach ($obj->respuestas as $i => $value) {if($value->respuesta == 3){ $res = $value->total;}} echo $res; ?>" value="<?php echo $obj->answer3 ?>" style="width:100%" name="answer3-<?php echo $obj->id_miportal_test ?>" type="text" />
                                        </div>
                                        <div class="col s3">
                                            <p>
                                                <input class="respuestasTests" name="respuesta-<?php echo $obj->id_miportal_test ?>" value="3" type="radio" id="test3<?php echo $obj->id_miportal_test ?>" <?php echo ($obj->respuesta == 3)?'checked':'' ?> />
                                                <label for="test3<?php echo $obj->id_miportal_test ?>">Respuesta correcta</label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s3">Respuesta D</div>
                                        <div class="col s6">
                                            <input class="formulario-invitaciones validate[minSize[4]]" data-contador="<?php $res = 0; foreach ($obj->respuestas as $i => $value) {if($value->respuesta == 4){ $res = $value->total;}} echo $res; ?>" value="<?php echo $obj->answer4 ?>" style="width:100%" name="answer4-<?php echo $obj->id_miportal_test ?>" type="text" />
                                        </div>
                                        <div class="col s3">
                                            <p>
                                                <input class="respuestasTests" name="respuesta-<?php echo $obj->id_miportal_test ?>" value="4" type="radio" id="test4<?php echo $obj->id_miportal_test ?>" <?php echo ($obj->respuesta == 4)?'checked':'' ?> />
                                                <label for="test4<?php echo $obj->id_miportal_test?>">Respuesta correcta</label>
                                            </p>
                                        </div>
                                    </div>

                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-<?php echo $obj->id_miportal_test ?>" type="button" >GUARDAR</button>
                                    <button class="waves-effect waves-light btn-flat" id="close-cancel-<?php echo $obj->id_miportal_test ?>" type="button" >Cancelar</button>
                                </form>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('.respuestasTests').on('change',function(){
                                    var element = $(this.parentNode.parentNode.parentNode.parentNode).find('.row .col.s6 input');
                                    $(element[2]).attr('class','formulario-invitaciones validate[minSize[4]]');
                                    $(element[3]).attr('class','formulario-invitaciones validate[minSize[4]]');
                                    $($(this.parentNode.parentNode.parentNode).find('.formulario-invitaciones')[0]).attr('class','formulario-invitaciones validate[required,minSize[4]]');
                                });
                                
                                $('#grafica-<?php echo $obj->id_miportal_test ?>').highcharts({
                                    credits: {
                                        enabled: false,
                                    },
                                    chart: {
                                        plotBackgroundColor: null,
                                        plotBorderWidth: null,
                                        plotShadow: false,
                                        type: 'pie',
                                    },
                                    legend: {
                                        layout: 'vertical',
                                        align: 'right',
                                        verticalAlign: 'top',
                                        floating: true,
                                        backgroundColor: '#FFFFFF',
                                        lineHeight: 25,
                                    },
                                    title: {
                                        text: ''
                                    },
                                    tooltip: {
                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                    },
                                    plotOptions: {
                                        pie: {
                                            allowPointSelect: true,
                                            cursor: 'pointer',
                                            dataLabels: {
                                                enabled: false,
                                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                style: {
                                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                },
                                            },
                                            showInLegend: true,
                                        }
                                    },
                                    series: [{
                                            name: 'Respuesta ',
                                            colorByPoint: true,
                                            data: [<?php if(!empty($obj->answer1)){ echo '{name:"'.$obj->answer1.'",y:'; $res = 0; foreach ($obj->respuestas as $i => $value) {if($value->respuesta == 1){ $res =$value->total;}} echo $res.'},'; } if(!empty($obj->answer2)){ echo '{name:"'.$obj->answer2.'",y:'; $res = 0; foreach ($obj->respuestas as $i => $value) {if($value->respuesta == 2){ $res =$value->total;}} echo $res.'},'; } if(!empty($obj->answer3)){ echo '{name:"'.$obj->answer3.'",y:'; $res = 0; foreach ($obj->respuestas as $i => $value) {if($value->respuesta == 3){ $res =$value->total;}} echo $res.'},'; } if(!empty($obj->answer4)){ echo '{name:"'.$obj->answer4.'",y:'; $res = 0; foreach ($obj->respuestas as $i => $value) {if($value->respuesta == 4){ $res =$value->total;}} echo $res.'},'; } ?>
                                            ]
                                        }]
                                });

                                $("#send-form-<?php echo $obj->id_miportal_test ?>").validationEngine();

                                $('#close-edit-<?php echo $obj->id_miportal_test ?>').on('click', function () {
                                    $('#tests-seccion-edit-<?php echo $obj->id_miportal_test ?>').hide();
                                    $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').show();
                                    $("#tests-seccion-<?php echo $obj->id_miportal_test ?>").mouseover(function () {
                                        $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_test ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_test ?>').show();
                                    });

                                    $("#tests-seccion-<?php echo $obj->id_miportal_test ?>").mouseout(function () {
                                        $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_test ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_test ?>').hide();
                                    });
                                });

                                $('#close-cancel-<?php echo $obj->id_miportal_test ?>').on('click', function () {
                                    $('#tests-seccion-edit-<?php echo $obj->id_miportal_test ?>').hide();
                                    $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').show();
                                    $("#tests-seccion-<?php echo $obj->id_miportal_test ?>").mouseover(function () {
                                        $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_test ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_test ?>').show();
                                    });

                                    $("#tests-seccion-<?php echo $obj->id_miportal_test ?>").mouseout(function () {
                                        $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_test ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_test ?>').hide();
                                    });
                                });

                                $('#button-editseccion-<?php echo $obj->id_miportal_test ?>').on('click', function () {
                                    $("#tests-seccion-<?php echo $obj->id_miportal_test ?>").unbind("mouseover");
                                    $("#tests-seccion-<?php echo $obj->id_miportal_test ?>").unbind("mouseout");
                                    $('#tests-seccion-edit-<?php echo $obj->id_miportal_test ?>').show();
                                    $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').hide();
                                });

                                $("#tests-seccion-<?php echo $obj->id_miportal_test ?>").mouseover(function () {
                                    $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-<?php echo $obj->id_miportal_test ?>').show();
                                    $('#button-delete-<?php echo $obj->id_miportal_test ?>').show();
                                });

                                $("#tests-seccion-<?php echo $obj->id_miportal_test ?>").mouseout(function () {
                                    $('#tests-seccion-<?php echo $obj->id_miportal_test ?>').attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                    $('#button-editseccion-<?php echo $obj->id_miportal_test ?>').hide();
                                    $('#button-delete-<?php echo $obj->id_miportal_test ?>').hide();
                                });

                                $('#button-send-form-<?php echo $obj->id_miportal_test ?>').on('click', function () {
                                    if ($("#send-form-<?php echo $obj->id_miportal_test ?>").validationEngine('validate')) {
                                        var descrp = tinymce.get(<?php echo $key + 2 ?>).getContent();
                                        if (descrp != '' && descrp != null && descrp.length > 16) {
                                            $('#button-send-form-<?php echo $obj->id_miportal_test ?>').attr('disabled', 'true');
                                            
                                            $.ajax({
                                                url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_test',
                                                method: 'POST',
                                                timeout: 4000,
                                                data: {
                                                    'titulo': $('#titulotest-<?php echo $obj->id_miportal_test ?>').val(),
                                                    'descripcion': descrp,
                                                    'answer1': $('[name=answer1-<?php echo $obj->id_miportal_test ?>]').val(),
                                                    'answer2': $('[name=answer2-<?php echo $obj->id_miportal_test ?>]').val(),
                                                    'answer3': $('[name=answer3-<?php echo $obj->id_miportal_test ?>]').val(),
                                                    'answer4': $('[name=answer4-<?php echo $obj->id_miportal_test ?>]').val(),
                                                    'respuesta':$('[name=respuesta-<?php echo $obj->id_miportal_test ?>]:checked').val(),
                                                    'i_e': '<?php echo $obj->id_miportal_test ?>',
                                                },
                                            }).done(function () {
                                                var sup = $('#tests-seccion-<?php echo $obj->id_miportal_test ?>');
                                                $(sup.find('.titulo')[0]).text($('#titulotest-<?php echo $obj->id_miportal_test ?>').val());
                                                $(sup.find('.descripcion')[0]).html(descrp);
                                                refreshGraphic('<?php echo $obj->id_miportal_test ?>');
                                                $('#close-cancel-<?php echo $obj->id_miportal_test ?>').trigger('click');
                                                $('#button-send-form-<?php echo $obj->id_miportal_test ?>').removeAttr('disabled');
                                            }).fail(function () {
                                                $('#button-send-form-<?php echo $obj->id_miportal_test ?>').removeAttr('disabled');
                                            });
                                        } else {
                                            alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                        }
                                    } else {
                                        console.log('error');
                                    }
                                });

                                $('#button-delete-<?php echo $obj->id_miportal_test ?>').on('click', function () {
                                    var r = confirm('¿Seguro que deseas eliminar este pregunta?');
                                    if (r == true) {
                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_test',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-test': '<?php echo $obj->id_miportal_test ?>',
                                            },
                                        }).done(function () {
                                            $('#<?php echo $obj->id_miportal_test ?>').remove();
                                        }).fail(function () {
                                            console.log('error');
                                        });
                                    }
                                });
                            });
                        </script>
                    <?php } ?>

                    <div id="add_before_test"></div>
                    
                    
                    
                    <div class="box">
                        <img src="<?php echo base_url() ?>dist/file-portal/img/grafica.png">
                        <br>
                        <button class="waves-effect waves-light btn-flat boton" id="add-new-test">A&ntilde;adir test</button>
                    </div>
                </div>
            <?php }
        }
        ?>
    </div>
</div>
<script type="text" id="template-1" >
    <hr class="hr-separador">
    <div class="test-agregados" id="tests-seccion-" style="display:none;">
        <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
        <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-" type="button" ><i class="material-icons">delete_forever</i></button>
        <h5 class="titulo" style="text-align:left;"></h5>
        <div class="descripcion"></div>
        <div id="grafica-" style="width:90%; height: 250px;"></div>
    </div>
    <div class="edit" id="tests-seccion-edit-" >
        <form id="send-form-">
            <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-" type="button" ><i class="material-icons">close</i></button>
            <h6><b>T&iacute;tulo*</b></h6>
            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="titulotest-" value="" placeholder="T&iacute;tulo" />
            <h6><b>Descripci&oacute;n</b></h6>
            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripciontest-"></textarea>
            <br><br>
            <div class="row">
                <div class="col s3">Respuesta A*</div>
                <div class="col s6">
                    <input class="formulario-invitaciones validate[required,minSize[4]]" data-contador="0" value="" style="width:100%" name="answer1-" type="text"  />
                </div>
                <div class="col s3">
                    <p>
                        <input class="respuestasTests" name="respuesta-" value="1" type="radio" id="test1-" checked />
                        <label for="test1-">Respuesta correcta</label>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col s3">Respuesta B*</div>
                <div class="col s6">
                    <input class="formulario-invitaciones validate[required,minSize[4]]" data-contador="0" value="" style="width:100%" name="answer2-" type="text"  />
                </div>
                <div class="col s3">
                    <p>
                        <input class="respuestasTests" name="respuesta-" value="2" type="radio" id="test2-"  />
                        <label for="test2-">Respuesta correcta</label>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col s3">Respuesta C</div>
                <div class="col s6">
                    <input class="formulario-invitaciones validate[minSize[4]]" data-contador="" value="" style="width:100%" name="answer3-" type="text"  />
                </div>
                <div class="col s3">
                    <p>
                        <input class="respuestasTests" name="respuesta-" value="3" type="radio" id="test3-"  />
                        <label for="test3-">Respuesta correcta</label>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col s3">Respuesta D</div>
                <div class="col s6">
                    <input class="formulario-invitaciones validate[minSize[4]]" data-contador="0" value="" style="width:100%" name="answer4-" type="text"  />
                </div>
                <div class="col s3">
                    <p>
                        <input class="respuestasTests" name="respuesta-" value="4" type="radio" id="test4-"  />
                        <label for="test4-">Respuesta correcta</label>
                    </p>
                </div>
            </div>

            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-" type="button" >GUARDAR</button>
            <button class="waves-effect waves-light btn-flat" id="close-cancel-" type="button" >Cancelar</button>
        </form>
    </div>
</script>
<script>
    var count_template = <?php echo count($tests) + 1; ?>;
    function refreshGraphic(id){
        var new_grafic = [];
        
        if($('[name=answer1-' + id + ']').val() != ''){
            var obj = {name: $('[name=answer1-' + id + ']').val(), y: parseInt($('[name=answer1-' + id + ']').data('contador'))};
            new_grafic.push(obj);
        }else{
            $('[name=answer1-' + id + ']').data('contador',0);
        }
        if($('[name=answer2-' + id + ']').val() != ''){
            var obj = {name: $('[name=answer2-' + id + ']').val(), y: parseInt($('[name=answer2-' + id + ']').data('contador'))};
            new_grafic.push(obj);
        }else{
            $('[name=answer2-' + id + ']').data('contador',0);
        }
        
        if($('[name=answer3-' + id + ']').val() != ''){
            var obj = {name: $('[name=answer3-' + id + ']').val(), y: parseInt($('[name=answer3-' + id + ']').data('contador'))};
            new_grafic.push(obj);
        }else{
            $('[name=answer3-' + id + ']').data('contador',0);
        }
        
        if($('[name=answer4-' + id + ']').val() != ''){
            var obj = {name: $('[name=answer4-' + id + ']').val(), y: parseInt($('[name=answer4-' + id + ']').data('contador'))};
            new_grafic.push(obj);
        }else{
            $('[name=answer4-' + id + ']').data('contador',0);
        }
        
        var chart = $('#grafica-' + id).highcharts();
        chart.series[0].setData(new_grafic);
    }
    
    $(document).ready(function(){
        $('#add-new-test').on('click',function(){
            count_template++;
            
            var div = document.createElement("div");
            div.setAttribute('id', count_template);
            div.setAttribute('style', 'position:relative;');
            div.innerHTML = $("#template-1").html();
            
            $($(div).find('#grafica-')[0]).attr('id', 'grafica-' + count_template);
            $($(div).find('#grafica-'+ count_template)[0]).attr('style','width:'+ ($($('.tests .descripcion')[0]).width() - 50) +'px; height:250px;');
            $($(div).find('#grafica-'+ count_template)[0]).highcharts({
                credits: {
                    enabled: false,
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    floating: true,
                    backgroundColor: '#FFFFFF',
                    lineHeight: 25,
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        name: 'Respuesta ',
                        colorByPoint: true,
                        data: [],
                    }]
            });
            $($(div).find('#grafica-'+ count_template)[0]).attr('style','width:90%; height:250px;');
            $($(div).find('#button-editseccion-')[0]).attr('id', 'button-editseccion-' + count_template);
            $($(div).find('#button-delete-')[0]).attr('id', 'button-delete-' + count_template);
            $($(div).find('#tests-seccion-')[0]).attr('id', 'tests-seccion-' + count_template);
            $($(div).find('#titulotest-')[0]).attr('id', 'titulotest-' + count_template);
            $($(div).find('#descripciontest-')[0]).attr('id', 'descripciontest-' + count_template);
            $($(div).find('#tests-seccion-edit-')[0]).attr('id', 'tests-seccion-edit-' + count_template);
            $($(div).find('#send-form-')[0]).attr('id', 'send-form-' + count_template);
            $($(div).find('#send-form-' + count_template)[0]).validationEngine();
            $($(div).find('[name="respuesta-"]')).each(function() {
                $(this).attr('name','respuesta-'+count_template);
            });
            $($(div).find('[name="answer1-"]')[0]).attr('name','answer1-' + count_template);
            $($(div).find('[name="answer2-"]')[0]).attr('name','answer2-' + count_template);
            $($(div).find('[name="answer3-"]')[0]).attr('name','answer3-' + count_template);
            $($(div).find('[name="answer4-"]')[0]).attr('name','answer4-' + count_template);
            
            $($(div).find('#test1-')[0]).attr('id','test1-' + count_template);
            $($(div).find('#test2-')[0]).attr('id','test2-' + count_template);
            $($(div).find('#test3-')[0]).attr('id','test3-' + count_template);
            $($(div).find('#test4-')[0]).attr('id','test4-' + count_template);
            $($(div).find('[for="test1-"]')[0]).attr('for','test1-' + count_template);
            $($(div).find('[for="test2-"]')[0]).attr('for','test2-' + count_template);
            $($(div).find('[for="test3-"]')[0]).attr('for','test3-' + count_template);
            $($(div).find('[for="test4-"]')[0]).attr('for','test4-' + count_template);
            
            $($(div).find('#close-cancel-')[0]).attr('id', 'close-cancel-' + count_template);
            $($(div).find('#close-cancel-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('close-cancel-')[1];
                $('#' + ident).remove();
            });
            $($(div).find('#close-edit-')[0]).attr('id', 'close-edit-' + count_template);
            $($(div).find('#close-edit-' + count_template)[0]).on('click', function () {
                var ident = ($(this).attr('id')).split('close-edit-')[1];
                $('#' + ident).remove();
            });
            $($(div).find('#button-send-form-')[0]).attr('id', 'button-send-form-' + count_template);
            $($(div).find('#button-send-form-' + count_template)[0]).on('click', function () {
                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];

                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                    var descrp = tinymce.get(sup_id).getContent();
                    if (descrp != '' && descrp != null && descrp.length > 16) {
                        $('#button-send-form-' + sup_id).attr('disabled', 'true');
                        
                        $.ajax({
                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/insert_test',
                            method: 'POST',
                            timeout: 4000,
                            data: {
                                'titulo': $('#titulotest-' + sup_id).val(),
                                'descripcion': descrp,
                                'answer1': $('[name=answer1-'+ sup_id +']').val(),
                                'answer2': $('[name=answer2-'+ sup_id +']').val(),
                                'answer3': $('[name=answer3-'+ sup_id +']').val(),
                                'answer4': $('[name=answer4-'+ sup_id +']').val(),
                                'respuesta':$('[name=respuesta-'+ sup_id +']:checked').val(),
                                's': '<?php echo $id_seccion?>',
                            },
                        }).done(function (result) {
                            var sup = $('#tests-seccion-' + sup_id);
                            $(sup.find('.titulo')[0]).text($('#titulotest-' + sup_id).val());
                            $(sup.find('.descripcion')[0]).html(descrp);
                            refreshGraphic(sup_id);
                            
                            
                            $('#close-edit-' + sup_id).unbind("click");
                            $('#close-edit-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('close-edit-')[1];
                                $('#tests-seccion-edit-' + sup_id).hide();
                                $('#tests-seccion-' + sup_id).show();
                                $("#tests-seccion-" + sup_id).mouseover(function () {
                                    $('#tests-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-' + sup_id).show();
                                    $('#button-delete-' + sup_id).show();
                                });

                                $("#tests-seccion-" + sup_id).mouseout(function () {
                                    $('#tests-seccion-' + sup_id).attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                    $('#button-editseccion-' + sup_id).hide();
                                    $('#button-delete-' + sup_id).hide();
                                });
                            });
                            
                            
                            $("#tests-seccion-" + sup_id).mouseover(function () {
                                var sup_id = ($(this).attr('id')).split('tests-seccion-')[1];
                                $('#tests-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#button-editseccion-' + sup_id).show();
                                $('#button-delete-' + sup_id).show();
                            });
                            
                            $("#tests-seccion-" + sup_id).mouseout(function () {
                                var sup_id = ($(this).attr('id')).split('tests-seccion-')[1];
                                $('#tests-seccion-' + sup_id).attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                $('#button-editseccion-' + sup_id).hide();
                                $('#button-delete-' + sup_id).hide();
                            });
                            
                            $('#close-cancel-' + sup_id).unbind("click");
                            $('#close-cancel-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('close-cancel-')[1];
                                $('#tests-seccion-edit-' + sup_id).hide();
                                $('#tests-seccion-' + sup_id).show();
                                $("#tests-seccion-" + sup_id).mouseover(function () {
                                    $('#tests-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#button-editseccion-' + sup_id).show();
                                    $('#button-delete-' + sup_id).show();
                                });

                                $("#tests-seccion-" + sup_id).mouseout(function () {
                                    $('#tests-seccion-' + sup_id).attr('style', 'padding:10px; border: 0px dashed #f2f2f2;');
                                    $('#button-editseccion-' + sup_id).hide();
                                    $('#button-delete-' + sup_id).hide();
                                });
                            });
                            
                            $('#button-send-form-' + sup_id).unbind("click");
                            $('#button-send-form-' + sup_id).data('id', result.id);
                            $('#button-send-form-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];

                                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                                    var descrp = tinymce.get(sup_id).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form-' + sup_id).attr('disabled', 'true');
                                        
                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_test',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'titulo': $('#titulotest-' + sup_id).val(),
                                                'descripcion': descrp,
                                                'answer1': $('[name=answer1-'+ sup_id +']').val(),
                                                'answer2': $('[name=answer2-'+ sup_id +']').val(),
                                                'answer3': $('[name=answer3-'+ sup_id +']').val(),
                                                'answer4': $('[name=answer4-'+ sup_id +']').val(),
                                                'respuesta':$('[name=respuesta-'+ sup_id +']:checked').val(),
                                                'i_e': $(this).data('id'),
                                            },
                                        }).done(function () {
                                            var sup = $('#tests-seccion-' + sup_id);
                                            $(sup.find('.titulo')[0]).text($('#titulotest-' + sup_id).val());
                                            $(sup.find('.descripcion')[0]).html(descrp);
                                            refreshGraphic(sup_id);
                                            $('#close-cancel-' + sup_id).trigger('click');
                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                            
                            
                            $('#button-delete-' + sup_id).show();
                            $('#button-delete-' + sup_id).data('id',result.id);
                            $('#button-delete-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-delete-')[1];
                                var r = confirm('¿Seguro que deseas eliminar este pregunta?');
                                if (r == true) {
                                    $.ajax({
                                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_test',
                                        method: 'POST',
                                        timeout: 4000,
                                        data: {
                                            'id-test': $(this).data('id'),
                                        },
                                    }).done(function () {
                                        $('#' + sup_id).remove();
                                    }).fail(function () {
                                        console.log('error');
                                    });
                                }
                            });
                            
                            
                            $('#button-editseccion-' + sup_id).show();
                            $('#button-editseccion-' + sup_id).on('click', function () {
                                var sup_id = ($(this).attr('id')).split('button-editseccion-')[1];
                                $("#tests-seccion-" + sup_id).unbind("mouseover");
                                $("#tests-seccion-" + sup_id).unbind("mouseout");
                                $('#tests-seccion-edit-' + sup_id).show();
                                $('#tests-seccion-' + sup_id).hide();
                            });
                            
                            $('#close-cancel-' + sup_id).trigger('click');
                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                        }).fail(function () {
                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                        });

                    } else {
                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                    }
                } else {
                    console.log('no cumple validacion');
                }

            });



            $('#add_before_test').append(div);

            tinymce.init({
                selector: '.tinymce-editor',
                plugins: "autoresize link",
                menubar: '',
                height : "250",
                toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
            });
        });
    })
    
</script>