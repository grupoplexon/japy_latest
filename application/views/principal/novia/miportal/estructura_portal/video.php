<div class="container-body">
    <div class="body-container-web detail">
        <?php
        $id_seccion = '';
        foreach ($miportal_seccion as $key => $value) {
            if ($value->tipo == 'video') { $id_seccion = $value->id_miportal_seccion;
                ?>
                <div class="videos">

                    <!-- E  V   E   N   T   O   S -->
                    <div id="videos-seccion" style="padding:11px;">
                        <button class="waves-effect waves-light btn-flat button-edit" id="editar-videoprincipal" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                        <h4 class="titulo"><?php echo $value->titulo ?></h4>
                        <div class="descripcion"><?php echo $value->descripcion ?></div>
                    </div>
                    <div class="edit" id="videos-seccion-edit" style="display: none;">
                        <form id="send-form">
                            <button class="waves-effect waves-light btn-flat" id="close-edit" type="button" ><i class="material-icons">close</i></button>
                            <br>
                            <input type="hidden" name="id-video" value="<?php echo $value->id_miportal_seccion ?>" required />
                            <h6><b>T&iacute;tulo*</b></h6>
                            <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" name="titulo-video" value="<?php echo $value->titulo; ?>" placeholder="T&iacute;tulo" />
                            <h6><b>Descripci&oacute;n*</b></h6>
                            <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" name="descripcion-video" id="videodescripcion"><?php echo $value->descripcion; ?></textarea>
                            <br>
                            <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form" type="button" >GUARDAR</button>
                            <button class="waves-effect waves-light btn-flat" id="close-cancel" type="button" >Cancelar</button>
                        </form>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $("#send-form").validationEngine();

                            $('#close-edit').on('click', function () {
                                $('#videos-seccion-edit').hide();
                                $('#videos-seccion').show();
                                $("#videos-seccion").mouseover(function () {
                                    $('#videos-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-videoprincipal').show();
                                });

                                $("#videos-seccion").mouseout(function () {
                                    $('#videos-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-videoprincipal').hide();
                                });
                            });

                            $('#close-cancel').on('click', function () {
                                $('#videos-seccion-edit').hide();
                                $('#videos-seccion').show();
                                $("#videos-seccion").mouseover(function () {
                                    $('#videos-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                    $('#editar-videoprincipal').show();
                                });

                                $("#videos-seccion").mouseout(function () {
                                    $('#videos-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                    $('#editar-videoprincipal').hide();
                                });
                            });

                            $('.button-edit').on('click', function () {
                                $("#videos-seccion").unbind("mouseover");
                                $("#videos-seccion").unbind("mouseout");
                                $('#videos-seccion-edit').show();
                                $('#videos-seccion').hide();
                            });

                            $("#videos-seccion").mouseover(function () {
                                $('#videos-seccion').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                $('#editar-videoprincipal').show();
                            });

                            $("#videos-seccion").mouseout(function () {
                                $('#videos-seccion').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                $('#editar-videoprincipal').hide();
                            });



                            $('#button-send-form').on('click', function () {
                                if ($("#send-form").validationEngine('validate')) {
                                    var descrp = tinymce.get(1).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form').attr('disabled', 'true');

                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_headerevento',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'id-evento': $('[name="id-video"]').val(),
                                                'titulo-evento': $('[name="titulo-video"]').val(),
                                                'descripcion-evento': tinymce.get(1).getContent(),
                                            },
                                        }).done(function () {
                                            $($('#videos-seccion').find('.titulo')[0]).text($('[name="titulo-video"]').val());
                                            $($('#videos-seccion').find('.descripcion')[0]).html(tinymce.get(1).getContent());
                                            $('#close-cancel').trigger('click');
                                            $('#button-send-form').removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form').removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('error');
                                }
                            });
                        });
                    </script>
                    <!--    F  I   N   --  E  V   E   N   T   O   S     -->

                    <?php foreach ($videos as $key => $obj) { ?>
                        <div id="<?php echo $obj->id_miportal_video ?>" style="position:relative;">
                            <hr class="hr-separador">
                            <div class="video-informacion videos-agregados" id="videos-seccion-<?php echo $obj->id_miportal_video ?>" style="padding:11px;">
                                <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-<?php echo $obj->id_miportal_video ?>" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
                                <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-<?php echo $obj->id_miportal_video ?>" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
                                <h5 class="titulo"><?php echo $obj->titulo; ?></h5>
                                <div class="myvideo">
                                    <iframe class="videoPreviews" title="<?php echo (strpos($obj->direccion_web,'youtube'))?'youtube':'vimeo' ?>" width="100%" height="280" src="<?php $temp = $obj->direccion_web; if(strpos($temp,'youtube')){ $temp = str_replace('watch?v=','embed/',$temp);}else if(strpos($temp,'vimeo')){ $temp = str_replace("//vimeo.com/","//player.vimeo.com/video/",$temp);} echo $temp.(strpos($temp,'youtube')?'?version=3&enablejsapi=1&playerapiid=ytplayer':'') ?>" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                                <div class="descripcion"><?php echo $obj->descripcion; ?></div>
                            </div>
                            
                            
                            <div class="edit" id="videos-seccion-edit-<?php echo $obj->id_miportal_video ?>" style="display:none;" >
                                <form id="send-form-<?php echo $obj->id_miportal_video ?>">
                                    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-<?php echo $obj->id_miportal_video ?>" type="button" ><i class="material-icons">close</i></button>
                                    <h6><b>T&iacute;tulo*</b></h6>
                                    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="titulovideo-<?php echo $obj->id_miportal_video ?>" value="<?php echo $obj->titulo; ?>" placeholder="T&iacute;tulo" />
                                    <h6><b>Descripci&oacute;n</b></h6>
                                    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionvideo-<?php echo $obj->id_miportal_video ?>"><?php echo $obj->descripcion; ?></textarea>
                                    <h6><b>Direcci&oacute;n web del video*</b></h6>
                                    <input class="formulario-invitaciones validate[required,custom[urlYoutubeVimeo]]" style="width:100%" type="text" id="direccionweb-<?php echo $obj->id_miportal_video ?>" value="<?php echo $obj->direccion_web; ?>" placeholder="http://www.youtube.com/watch?v=S5JQXojU4D8&list=TLWFuvDkTex7s http://vimeo.com/33091687" />
                                    <small>Copia la direcci&oacute;n web de la p&aacute;gina de Youtube o Vimeo en la que hayas encontrado el video y p&eacute;gala aqu&iacute; (ej: www.youtube.com/video/...)</small>
                                    <br><br>
                                    <div id="videopreview-<?php echo $obj->id_miportal_video ?>" width="100%" height="280">
                                        <iframe class="videoPreviews" title="<?php echo (strpos($obj->direccion_web,'youtube'))?'youtube':'vimeo' ?>" width="100%" height="280" src="<?php echo $temp.(strpos($temp,'youtube')?'?version=3&enablejsapi=1&playerapiid=ytplayer':'')  ?>" frameborder="0" allowfullscreen=""></iframe>
                                    </div>
                                    <br>
                                    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-<?php echo $obj->id_miportal_video ?>" type="button" >GUARDAR</button>
                                    <button class="waves-effect waves-light btn-flat" id="close-cancel-<?php echo $obj->id_miportal_video ?>" type="button" >Cancelar</button>
                                </form>
                            </div>
                            
                            
                            <script>
                                $(document).ready(function () {
                                    $("#send-form-<?php echo $obj->id_miportal_video ?>").validationEngine();

                                    $('#close-edit-<?php echo $obj->id_miportal_video ?>').on('click', function () {
                                        stopAllVideo();
                                        $('#videos-seccion-edit-<?php echo $obj->id_miportal_video ?>').hide();
                                        $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').show();
                                        $("#videos-seccion-<?php echo $obj->id_miportal_video ?>").mouseover(function () {
                                            $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_video ?>').show();
                                            $('#button-delete-<?php echo $obj->id_miportal_video ?>').show();
                                        });

                                        $("#videos-seccion-<?php echo $obj->id_miportal_video ?>").mouseout(function () {
                                            $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_video ?>').hide();
                                            $('#button-delete-<?php echo $obj->id_miportal_video ?>').hide();
                                        });
                                    });

                                    $('#close-cancel-<?php echo $obj->id_miportal_video ?>').on('click', function () {
                                        stopAllVideo();
                                        $('#videos-seccion-edit-<?php echo $obj->id_miportal_video ?>').hide();
                                        $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').show();
                                        $("#videos-seccion-<?php echo $obj->id_miportal_video ?>").mouseover(function () {
                                            $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_video ?>').show();
                                            $('#button-delete-<?php echo $obj->id_miportal_video ?>').show();
                                        });

                                        $("#videos-seccion-<?php echo $obj->id_miportal_video ?>").mouseout(function () {
                                            $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                            $('#button-editseccion-<?php echo $obj->id_miportal_video ?>').hide();
                                            $('#button-delete-<?php echo $obj->id_miportal_video ?>').hide();
                                        });
                                    });

                                    $('#button-editseccion-<?php echo $obj->id_miportal_video ?>').on('click', function () {
                                        stopAllVideo();
                                        $("#videos-seccion-<?php echo $obj->id_miportal_video ?>").unbind("mouseover");
                                        $("#videos-seccion-<?php echo $obj->id_miportal_video ?>").unbind("mouseout");
                                        $('#videos-seccion-edit-<?php echo $obj->id_miportal_video ?>').show();
                                        $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').hide();
                                    });
                                    
                                    $('#direccionweb-<?php echo $obj->id_miportal_video ?>').on('change',function(){
                                        var ident = ($(this).attr('id')).split('direccionweb-')[1];
                                        var result = $(this).validationEngine('validate');
                                        if(result == false){
                                            var url = $(this).val();
                                            if(url.indexOf('youtube') > -1){
                                                url = url.replace("watch?v=", "embed/");
                                                $('#videopreview-'+ident).html('<iframe class="videoPreviews" title="youtube" width="100%" height="280" src="' + url + '?version=3&enablejsapi=1&playerapiid=ytplayer" frameborder="0" allowfullscreen=""></iframe>'); 
                                                $(this).data('url',url+'?version=3&enablejsapi=1&playerapiid=ytplayer');
                                            }
                                            else if(url.indexOf('vimeo') > -1){
                                                url = url.replace("//vimeo.com/","//player.vimeo.com/video/");
                                                $('#videopreview-'+ident).html('<iframe class="videoPreviews" title="vimeo" width="100%" height="280" src="' + url + '" frameborder="0" allowfullscreen=""></iframe>'); 
                                                $(this).data('url',url);
                                            }
                                            /*else if(url.indexOf('//player.vimeo.com/video/') > -1 || url.indexOf('youtube.com/embed/') > -1){
                                                $('#videopreview-'+ident).html('<iframe title="YouTube video player" width="100%" height="280" src="' + url + '" frameborder="0" allowfullscreen=""></iframe>'); 
                                            }*/
                                        }
                                    });
                                    
                                    $("#videos-seccion-<?php echo $obj->id_miportal_video ?>").mouseover(function () {
                                        $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_video ?>').show();
                                        $('#button-delete-<?php echo $obj->id_miportal_video ?>').show();
                                    });

                                    $("#videos-seccion-<?php echo $obj->id_miportal_video ?>").mouseout(function () {
                                        $('#videos-seccion-<?php echo $obj->id_miportal_video ?>').attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                        $('#button-editseccion-<?php echo $obj->id_miportal_video ?>').hide();
                                        $('#button-delete-<?php echo $obj->id_miportal_video ?>').hide();
                                    });
                                    
                                    /*   -   -   -   -      F       I       N       -   -   -   - */

                                    $('#button-send-form-<?php echo $obj->id_miportal_video ?>').on('click', function () {
                                        stopAllVideo();
                                        if ($("#send-form-<?php echo $obj->id_miportal_video ?>").validationEngine('validate')) {
                                            var descrp = tinymce.get(<?php echo $key + 2 ?>).getContent();
                                            if (descrp != '' && descrp != null && descrp.length > 16) {
                                                $('#button-send-form-<?php echo $obj->id_miportal_video ?>').attr('disabled', 'true');
                                                
                                                $.ajax({
                                                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_video',
                                                    method: 'POST',
                                                    timeout: 4000,
                                                    data: {
                                                        'titulo': $('#titulovideo-<?php echo $obj->id_miportal_video ?>').val(),
                                                        'descripcion': descrp,
                                                        'direccion_web': $('#direccionweb-<?php echo $obj->id_miportal_video ?>').val(),
                                                        'i': '<?php echo $obj->id_miportal_video ?>',
                                                    },
                                                }).done(function () {
                                                    var sup = $('#videos-seccion-<?php echo $obj->id_miportal_video ?>');
                                                    $(sup.find('.titulo')[0]).text($('#titulovideo-<?php echo $obj->id_miportal_video ?>').val());
                                                    $(sup.find('.descripcion')[0]).html(descrp);
                                                    $(sup.find('.myvideo')[0]).html('<iframe class="videoPreviews" title="YouTube video player" width="100%" height="280" src="' + $('#direccionweb-<?php echo $obj->id_miportal_video ?>').data('url') + '" frameborder="0" allowfullscreen=""></iframe>'); 
                                                    $('#close-cancel-<?php echo $obj->id_miportal_video ?>').trigger('click');
                                                    $('#button-send-form-<?php echo $obj->id_miportal_video ?>').removeAttr('disabled');
                                                }).fail(function () {
                                                    $('#button-send-form-<?php echo $obj->id_miportal_video ?>').removeAttr('disabled');
                                                });
                                            } else {
                                                alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                            }
                                        } else {
                                            console.log('error');
                                        }
                                    });

                                    $('#button-delete-<?php echo $obj->id_miportal_video ?>').on('click', function () {
                                        var r = confirm('¿Seguro que deseas eliminar esta video?');
                                        if (r == true) {
                                            $.ajax({
                                                url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_video',
                                                method: 'POST',
                                                timeout: 4000,
                                                data: {
                                                    'id-video': '<?php echo $obj->id_miportal_video ?>',
                                                },
                                            }).done(function () {
                                                $('#<?php echo $obj->id_miportal_video ?>').remove();
                                            }).fail(function () {
                                                console.log('error');
                                            });
                                        }
                                    });
                                });
                            </script>
                        </div>

                    <?php } ?>

                    <div id="add_before_video"></div>   

                    <div class="box">
                        <img src="<?php echo base_url() ?>dist/file-portal/img/video.ico">
                        <br>
                        <button class="waves-effect waves-light btn-flat boton" id="add_video">A&ntilde;adir videos</button>
                    </div>


                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<script src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script>
<script type="text" id="template-1" >
    <hr class="hr-separador">
    <div class="video-informacion videos-agregados" id="videos-seccion-" style="display:none; padding:11px;">
    <button class="waves-effect waves-light btn-flat button-edit-seccion" id="button-editseccion-" style="display:none" type="button" ><i class="material-icons left">mode_edit</i>Editar</button>
    <button class="waves-effect waves-light btn-flat button-delete" id="button-delete-" style="display:none" type="button" ><i class="material-icons">delete_forever</i></button>
    <h5 class="titulo"></h5>
    <div class="myvideo"></div>
    <div class="descripcion"></div>
    </div>

    <div class="edit" id="videos-seccion-edit-" >
    <form id="send-form-">
    <button class="waves-effect waves-light btn-flat close-edit" id="close-edit-" type="button" ><i class="material-icons">close</i></button>
    
    <h6><b>T&iacute;tulo*</b></h6>
    <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%" type="text" id="titulovideo-" value="" placeholder="T&iacute;tulo" />
    <h6><b>Descripci&oacute;n</b></h6>
    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor" id="descripcionvideo-"></textarea>
    <h6><b>Direcci&oacute;n web del video*</b></h6>
    <input class="formulario-invitaciones validate[required,custom[urlYoutubeVimeo]]" style="width:100%" type="text" id="direccionweb-" value="" placeholder="http://www.youtube.com/watch?v=S5JQXojU4D8&list=TLWFuvDkTex7s http://vimeo.com/33091687" />
    <small>Copia la direcci&oacute;n web de la p&aacute;gina de Youtube o Vimeo en la que hayas encontrado el video y p&eacute;gala aqu&iacute; (ej: www.youtube.com/video/...)</small>
    <br><br>
    <div id="videopreview-"></div>
    <br>
    <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="button-send-form-" type="button" >GUARDAR</button>
    <button class="waves-effect waves-light btn-flat" id="close-cancel-" type="button" >Cancelar</button>
    </form>
    </div>
</script>
<script>
                    var count_template = <?php echo count($videos) + 1; ?>;
                    
                    function stopAllVideo(){
                        $('.videoPreviews').each(function() {
                            var tipo = $(this).attr('src');
                            if(tipo.indexOf('vimeo') > -1){
                                var player=$f(this);
                                player.api("pause");
                            }
                            else{
                                $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                            }
                        });
                    }
                    
                    $(document).ready(function () {
                        $('#add_video').on('click', function () {
                            count_template++;
                            var div = document.createElement("div");
                            div.setAttribute('id', count_template);
                            div.setAttribute('style','position:relative;');
                            div.innerHTML = $("#template-1").html();

                            $($(div).find('#button-editseccion-')[0]).attr('id', 'button-editseccion-' + count_template);
                            $($(div).find('#button-delete-')[0]).attr('id', 'button-delete-' + count_template);
                            $($(div).find('#videos-seccion-')[0]).attr('id', 'videos-seccion-' + count_template);
                            $($(div).find('#videos-seccion-edit-')[0]).attr('id', 'videos-seccion-edit-' + count_template);
                            $($(div).find('#titulovideo-')[0]).attr('id', 'titulovideo-' + count_template);
                            $($(div).find('#descripcionvideo-')[0]).attr('id', 'descripcionvideo-' + count_template);
                            $($(div).find('#videopreview-')[0]).attr('id', 'videopreview-' + count_template);
                            
                            $($(div).find('#send-form-')[0]).attr('id', 'send-form-' + count_template);
                            $($(div).find('#send-form-' + count_template)[0]).validationEngine();
                            
                            $($(div).find('#direccionweb-')[0]).attr('id', 'direccionweb-' + count_template);
                            $($(div).find('#direccionweb-' + count_template)[0]).on('change',function(){
                                var ident = ($(this).attr('id')).split('direccionweb-')[1];
                                var result = $(this).validationEngine('validate');
                                if(result == false){
                                    var url = $(this).val();
                                    if(url.indexOf('youtube') > -1){
                                        url = url.replace("watch?v=", "embed/");
                                        $('#videopreview-'+ident).html('<iframe class="videoPreviews" title="Preview" width="100%" height="280" src="' + url + '?version=3&enablejsapi=1&playerapiid=ytplayer" frameborder="0" allowfullscreen=""></iframe>'); 
                                        $(this).data('url',url+'?version=3&enablejsapi=1&playerapiid=ytplayer');
                                    }
                                    else if(url.indexOf('vimeo') > -1){
                                        url = url.replace("//vimeo.com/","//player.vimeo.com/video/");
                                        $('#videopreview-'+ident).html('<iframe class="videoPreviews" title="Preview" width="100%" height="280" src="' + url + '" frameborder="0" allowfullscreen=""></iframe>'); 
                                        $(this).data('url',url);
                                    }
                                    /*else if(url.indexOf('//player.vimeo.com/video/') > -1 || url.indexOf('youtube.com/embed/') > -1){
                                        $('#videopreview-'+ident).html('<iframe class="videoPreviews" title="YouTube video player" width="100%" height="280" src="' + url + '" frameborder="0" allowfullscreen=""></iframe>'); 
                                    }*/
                                }
                            });

                            $($(div).find('#button-send-form-')[0]).attr('id', 'button-send-form-' + count_template);
                            $($(div).find('#button-send-form-' + count_template)[0]).on('click', function () {
                                stopAllVideo();
                                var ident = ($(this).attr('id')).split('button-send-form-')[1];
                                if ($("#send-form-" + ident).validationEngine('validate')) {
                                    var descrp = tinymce.get(ident).getContent();
                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                        $('#button-send-form-' + ident).attr('disabled', 'true');
                                        $.ajax({
                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/insert_video',
                                            method: 'POST',
                                            timeout: 4000,
                                            data: {
                                                'titulo': $('#titulovideo-' + ident).val(),
                                                'descripcion': descrp,
                                                'direccion_web': $('#direccionweb-' + ident).val(),
                                                's': '<?php echo $id_seccion ?>',
                                            },
                                        }).done(function (result) {
                                            var sup = $('#videos-seccion-' + ident);
                                            $(sup.find('.titulo')[0]).text($('#titulovideo-' + ident).val());
                                            $(sup.find('.descripcion')[0]).html(descrp);
                                            $(sup.find('.myvideo')[0]).html('<iframe class="videoPreviews" title="Preview" width="100%" height="280" src="' + $('#direccionweb-' + ident).data('url') + '" frameborder="0" allowfullscreen=""></iframe>'); 
                                            
                                            $('#close-edit-' + ident).unbind("click");
                                            $('#close-edit-' + ident).on('click', function () {
                                                stopAllVideo();
                                                var sup_id = ($(this).attr('id')).split('close-edit-')[1];
                                                $('#videos-seccion-edit-' + sup_id).hide();
                                                $('#videos-seccion-' + sup_id).show();
                                                $("#videos-seccion-" + sup_id).mouseover(function () {
                                                    $('#videos-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                    $('#button-editseccion-' + sup_id).show();
                                                    $('#button-delete-' + sup_id).show();
                                                });

                                                $("#videos-seccion-" + sup_id).mouseout(function () {
                                                    $('#videos-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                    $('#button-editseccion-' + sup_id).hide();
                                                    $('#button-delete-' + sup_id).hide();
                                                });
                                            });

                                            $('#close-cancel-' + ident).unbind("click");
                                            $('#close-cancel-' + ident).on('click', function () {
                                                stopAllVideo();
                                                var sup_id = ($(this).attr('id')).split('close-cancel-')[1];
                                                $('#videos-seccion-edit-' + sup_id).hide();
                                                $('#videos-seccion-' + sup_id).show();
                                                $("#videos-seccion-" + sup_id).mouseover(function () {
                                                    $('#videos-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                    $('#button-editseccion-' + sup_id).show();
                                                    $('#button-delete-' + sup_id).show();
                                                });

                                                $("#videos-seccion-" + sup_id).mouseout(function () {
                                                    $('#videos-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                    $('#button-editseccion-' + sup_id).hide();
                                                    $('#button-delete-' + sup_id).hide();
                                                });
                                            });
                                            
                                            
                                            $('#button-editseccion-' + ident).show();
                                            $('#button-editseccion-' + ident).unbind("click");
                                            $('#button-editseccion-' + ident).on('click', function () {
                                                stopAllVideo();
                                                var sup_id = ($(this).attr('id')).split('button-editseccion-')[1];
                                                $("#videos-seccion-" + sup_id).unbind("mouseover");
                                                $("#videos-seccion-" + sup_id).unbind("mouseout");
                                                $('#videos-seccion-edit-' + sup_id).show();
                                                $('#videos-seccion-' + sup_id).hide();
                                            });
                                            

                                            $("#videos-seccion-" + ident).mouseover(function () {
                                                var sup_id = ($(this).attr('id')).split('videos-seccion-')[1];
                                                $('#videos-seccion-' + sup_id).attr('style', 'padding:10px; border: 1px dashed #9c9c9c; opacity:0.5;');
                                                $('#button-editseccion-' + sup_id).show();
                                                $('#button-delete-' + sup_id).show();
                                            });

                                            $("#videos-seccion-" + ident).mouseout(function () {
                                                var sup_id = ($(this).attr('id')).split('videos-seccion-')[1];
                                                $('#videos-seccion-' + sup_id).attr('style', 'padding:11px; border: 0px dashed #f2f2f2; opacity:1;');
                                                $('#button-editseccion-' + sup_id).hide();
                                                $('#button-delete-' + sup_id).hide();
                                            });

                                            $('#button-delete-' + ident).unbind("click");
                                            $('#button-delete-' + ident).data('id', result.id);
                                            $('#button-delete-' + ident).show();
                                            $('#button-delete-' + ident).on('click', function () {
                                                var sup_id = ($(this).attr('id')).split('button-delete-')[1];

                                                var r = confirm('¿Seguro que deseas eliminar este video?');
                                                if (r == true) {
                                                    $.ajax({
                                                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/delete_video',
                                                        method: 'POST',
                                                        timeout: 4000,
                                                        data: {
                                                            'id-video': $(this).data('id'),
                                                        },
                                                    }).done(function () {
                                                        $('#' + sup_id).remove();
                                                    }).fail(function () {
                                                        console.log('error');
                                                    });
                                                }
                                            });

                                            $('#button-send-form-' + ident).unbind("click");
                                            $('#button-send-form-' + ident).data('id', result.id);
                                            $('#button-send-form-' + ident).on('click', function () {
                                                stopAllVideo();
                                                var sup_id = ($(this).attr('id')).split('button-send-form-')[1];

                                                if ($("#send-form-" + sup_id).validationEngine('validate')) {
                                                    var descrp = tinymce.get(sup_id).getContent();
                                                    if (descrp != '' && descrp != null && descrp.length > 16) {
                                                        $('#button-send-form-' + sup_id).attr('disabled', 'true');

                                                        $.ajax({
                                                            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_video',
                                                            method: 'POST',
                                                            timeout: 4000,
                                                            data: {
                                                                'titulo': $('#titulovideo-' + sup_id).val(),
                                                                'descripcion': descrp,
                                                                'direccion_web': $('#direccionweb-' + sup_id).val(),
                                                                'i': $(this).data('id'),
                                                            },
                                                        }).done(function () {
                                                            var sup = $('#videos-seccion-' + sup_id);
                                                            $(sup.find('.titulo')[0]).text($('#titulovideo-' + sup_id).val());
                                                            $(sup.find('.descripcion')[0]).html(descrp);
                                                            $(sup.find('.myvideo')[0]).html('<iframe class="videoPreviews" title="Preview" width="100%" height="280" src="' + $('#direccionweb-' + sup_id).data('url') + '" frameborder="0" allowfullscreen=""></iframe>'); 
                                                            
                                                            $('#close-cancel-' + sup_id).trigger('click');
                                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                                        }).fail(function () {
                                                            $('#button-send-form-' + sup_id).removeAttr('disabled');
                                                        });
                                                    } else {
                                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                                    }
                                                } else {
                                                    console.log('error');
                                                }
                                            });

                                            $('#close-cancel-' + ident).trigger('click');
                                            $('#button-send-form-' + ident).removeAttr('disabled');
                                        }).fail(function () {
                                            $('#button-send-form-' + ident).removeAttr('disabled');
                                        });
                                    } else {
                                        alert('El mensaje debe tener 10 caracteres como m&iacute;nimo.');
                                    }
                                } else {
                                    console.log('validacion no aprobada');
                                }
                            });


                            $($(div).find('#close-cancel-')[0]).attr('id', 'close-cancel-' + count_template);
                            $($(div).find('#close-cancel-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('close-cancel-')[1];
                                $('#' + ident).remove();
                            });


                            $($(div).find('#close-edit-')[0]).attr('id', 'close-edit-' + count_template);
                            $($(div).find('#close-edit-' + count_template)[0]).on('click', function () {
                                var ident = ($(this).attr('id')).split('close-edit-')[1];
                                $('#' + ident).remove();
                            });

                            $('#add_before_video').append(div);

                            
                            tinymce.init({
                                selector: '.tinymce-editor',
                                plugins: "autoresize link",
                                menubar: '',
                                height : "250",
                                toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
                            });
                        });
                    });
</script>