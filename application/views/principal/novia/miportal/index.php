<html>
<head>
    <?php $this->view("principal/novia/miportal/estructura/header") ?>
</head>
<body>
<header>
    <?php $this->view("principal/novia/miportal/estructura/menu") ?>
    <div class="grey darken-7">
        <div class="body-container">
            <div class="row menu-portal">
                <div class="menu-item active" onclick="goTo(1, this);">
                    <i class="icon material-icons">dvr</i><br>
                    <small class="text white-text hide-on-small-only"><b>MI P&Aacute;GINA</b></small>
                </div>
                <div class="menu-item" onclick="goTo(2, this);">
                    <i class="icon material-icons">brush</i><br>
                    <small class="text white-text hide-on-small-only"><b>DISE&Ntilde;O</b></small>
                </div>
                <div class="menu-item" onclick="goTo(3, this);">
                    <i class="icon material-icons">settings</i>
                    <small class="text white-text hide-on-small-only"><b>CONFIGURACI&Oacute;N</b></small>
                </div>
                <div class="menu-item" onclick="goTo(4, this);">
                    <i class="icon material-icons">share</i><br>
                    <small class="text white-text hide-on-small-only"><b>COMPARTIR</b></small>
                </div>
            </div>
        </div>
    </div>
</header>

<main>

    <div class="nav">
        <div class="dorado-2" style="padding: 4px;">
            <button class="btn-flat left white-text" id="titulo_boton">
                <i class="material-icons">home</i>
            </button>
            <h6 class="white-text" id="titulo_cabecera" style="font-weight:bold;">
                &nbsp;&nbsp;&nbsp;&nbsp;P&Aacute;GINAS</h6>
        </div>


        <div id="mi_pagina">


            <!--        M   A   I   N       -->
            <div class="body-container" id="main">
                <div class="section">
                    <button class="waves-effect waves-light btn dorado-2" onclick="back(2, null);" style="width:100%;">
                        <span class="white-text"><i
                                    class="material-icons left">add</i>&nbsp;&nbsp;A&ntilde;adir nueva</span>
                    </button>
                </div>
                <div class="divider"></div>
                <div class="section">
                    <h6><b>EDITAR</b></h6>
                    <ul class="collection" id="seccion_activa">
                        <?php
                        foreach ($miportal_seccion as $key => $value) {
                            if ($value->activo == 0 || $value->activo == 1) {
                                ?>
                                <li class="collection-item dismissable"
                                    onclick="back(4, '<?php echo $value->id_miportal_seccion ?>')"
                                    id="main-<?php echo $value->id_miportal_seccion ?>">
                                    <div>
                                        <span><?php echo $value->titulo; ?></span>
                                        <a class="secondary-content">
                                            <?php echo ($value->visible == 1) ? '<i class="material-icons green-text darken-2">fiber_manual_record</i>' : (($value->visible == 2) ? '<i class="material-icons orange-text darken-3">lock</i>' : '<i class="material-icons blue-grey-text lighten-2">visibility_off</i>'); ?>
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </a>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>


            <!--       A    D   D       S   E   C   C   I   O   N       -->
            <div class="body-container" id="add_seccion" style="display:none;">
                <div class="section">
                    <h6>Seleccione las p&aacute;ginas a agregar.</h6>
                </div>
                <div class="divider"></div>
                <div class="section">
                    <?php foreach ($miportal_seccion as $key => $value) { ?>
                        <div class="card paginas <?php echo ($value->activo == 0 || $value->activo == 1) ? 'select' : ''; ?>"
                             id="add-<?php echo $value->id_miportal_seccion; ?>" <?php echo ($value->activo == 2) ? "onclick='back(3, &#39;".json_encode($value)."&#39;);'" : ''; ?> >
                            <div class="card-content">
                                <i class="material-icons <?php echo $value->color; ?>"><?php echo $value->icono; ?></i>
                                <div class="texto">
                                    <h6 class="<?php echo $value->color; ?>"><b><?php echo $value->titulo; ?></b></h6>
                                    <p class="grey-text lighten-3"><?php echo $value->detalle; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>


            <!--      A     D   D   -   S   E   L   E   C   T   I   O   N  -   S   E   C   C   I   O   N       -->
            <div class="body-container" id="modify_seccion" style="display:none;">
                <div class="section">
                    <h5 class="center" id="icono"></h5>
                    <h6 class="center" id="titulo_seccion"></h6>
                    <h6 class="center" id="detalle"></h6>
                </div>
                <div class="divider"></div>
                <div class="section">
                    <input type="hidden" id="id_miportal_seccion">
                    <input type="hidden" id="tipo_miportal_seccion"/>
                    <h6><b>T&Iacute;TULO</b></h6>
                    <input class="formulario-invitaciones" type="text" id="titulo" value="" style='width: 100%'/>
                    <h6><b>DESCRIPCI&Oacute;N</b></h6>
                    <textarea class="materialize-textarea formulario-invitaciones white tinymce-editor"
                              id="descripcion"></textarea>
                    <br><br>
                    <button id="guardar_seccion" class="waves-effect waves-light btn-flat dorado-2 white-text"
                            style="width:100%; margin-bottom: 5px;">Agregar
                    </button>
                    <button onclick="back(2, null);" class="waves-effect waves-light btn-flat"
                            style="width:100%; margin-bottom: 5px;">Cancelar
                    </button>
                </div>
            </div>


            <!--       M    O   D   I   F   Y   -   -       S   E   C   C   I   O   N       -->
            <div class="body-container" id="modify_seccionactivas" style="display:none;">
                <div class="section">
                    <h6 class="center" id="nombre_modify"></h6>
                    <small class="center" id="detalle_modify"></small>
                </div>
                <div class="divider"></div>
                <div class="section">
                    <input type="hidden" id="id_miportal_seccion_modify">
                    <input type="hidden" id="tipo_modify">
                    <h6><b>T&Iacute;TULO</b></h6>
                    <input class="formulario-invitaciones" type="text" id="titulo_modify" value="" style='width: 100%'/>
                    <h6><b>VISIBILIDAD</b></h6>
                    <div class="card">
                        <div class="card-content">
                            <p class="right">
                                <input class="with-gap" name="visible_radio" type="radio" id="radio_publico"/>
                                <label for="radio_publico"></label>
                            </p>
                            <i class="material-icons green-text darken-2">fiber_manual_record</i>&nbsp;&nbsp;&nbsp;&nbsp;<b>P&Uacute;BLICO</b><br>
                            Tus invitados podr&aacute;n acceder libremente sin contraseña.
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <p class="right">
                                <input class="with-gap" name="visible_radio" type="radio" id="radio_privado"/>
                                <label for="radio_privado"></label>
                            </p>
                            <i class="material-icons orange-text darken-3">lock</i>&nbsp;&nbsp;&nbsp;&nbsp;<b>PRIVADO</b><br>
                            Ser&aacute; necesaria una contraseña para acceder a esta secci&oacute;n.
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <p class="right">
                                <input class="with-gap" name="visible_radio" type="radio" id="radio_novisible"/>
                                <label for="radio_novisible"></label>
                            </p>
                            <i class="material-icons blue-grey-text lighten-2">visibility_off</i>&nbsp;&nbsp;&nbsp;&nbsp;<b>NO
                                VISIBLE</b><br>
                            Nadie podr&aacute; verla, es muy &uacute;til por si dejas una secci&oacute;n a medias.
                        </div>
                    </div>
                    <br>
                    <button id="guardar_modify" class="waves-effect waves-light btn-flat green darken-3 white-text"
                            style="width:100%; margin-bottom: 5px;">Guardar
                    </button>
                    <button id="eliminar_modify" class="waves-effect waves-light btn-flat red darken-3 white-text"
                            style="width:100%; margin-bottom: 5px;">Eliminar p&aacute;gina
                    </button>
                    <button onclick="back(1, null);" class="waves-effect waves-light btn-flat"
                            style="width:100%; margin-bottom: 5px;">Cancelar
                    </button>
                </div>
            </div>


        </div>


        <div id="configuracion" style="display:none">


            <!--        M   A   I   N       -->
            <div class="body-container" id="main-configuracion">
                <div class="section">
                    <input type="hidden" id="id_miportal">
                    <h6><b>T&Iacute;TULO</b></h6>
                    <input class="formulario-invitaciones" type="text" id="titulo_miportal" style="width:100%"
                           value="<?php echo $miportal->titulo ?>"/>
                    <h6><b>NOSOTROS</b></h6>
                    <input class="formulario-invitaciones" type="text" id="nosotros_miportal" style="width:100%"
                           value="<?php echo $miportal->nosotros ?>"/>
                    <h6><b>FECHA DE LA BODA</b></h6>
                    <input class="datepicker formulario-invitaciones" type="text" id="fecha_boda_miportal"
                           style="width:100%" value="<?php echo $miportal->fecha_boda ?>" readonly="readonly"/>
                    <button id="guardar_configuracion" class="waves-effect waves-light btn-flat dorado-2 white-text"
                            style="width:100%; margin-bottom: 5px;">Guardar
                    </button>
                </div>
                <div class="divider"></div>
                <div class="section">
                    <div>
                        <div class="switch right">
                            <label>
                                <input type="checkbox"
                                       id="cuentaatras_configuracion" <?php echo ($miportal->cuenta_atras == 1) ? 'checked' : '' ?>>
                                <span class="lever"></span>
                            </label>
                        </div>
                        <h6><b>CUENTA ATR&Aacute;S</b></h6>
                        <small>Tic, tac... Ya queda menos... Muestra la cuenta atr&aacute;s para saber cu&aacute;nto
                            falta para el gran d&iacute;a.
                        </small>
                    </div>
                    <br>
                    <div>
                        <div class="switch right">
                            <label>
                                <input type="checkbox"
                                       id="contrasena_configuracion" <?php echo empty($miportal->contrasena) ? '' : 'checked' ?>>
                                <span class="lever"></span>
                            </label>
                        </div>
                        <h6><b>CONTRASE&Ntilde;A</b></h6>
                        <small>Crea y activa tu contraseña. S&oacute;lo acceder&aacute; a tu web quien t&uacute;
                            decidas.
                        </small>
                        <div class="center-align">
                            <button id="cambiar_contrasena"
                                    class="waves-effect waves-light btn-flat pink lighten-5 black-text"
                                    onclick="goToConfiguracion(2);"
                                    style="<?php echo empty($miportal->contrasena) ? 'display:none' : '' ?>">
                                Cambiar contrase&ntilde;a
                            </button>
                        </div>
                    </div>
                </div>
            </div>


            <!--        C   O   N   T   R   A   S   E   N   A       -->
            <div class="body-container" id="updateContrasena-configuracion" style="display:none;">
                <div class="section">
                    <h6><b>Acceso con contrase&ntilde;a</b></h6>
                </div>
                <div class="divider"></div>
                <div class="section">
                    <input type="hidden" id="estado_anterior"/>
                    <h6><b>CONTRASE&Ntilde;A</b></h6>
                    <input class="formulario-invitaciones" type="password" style="width:100%" id="contrasena_miportal"
                           value="" placeholder="***********"/>
                    <h6><b>REPETIR CONTRASE&Ntilde;A</b></h6>
                    <input class="formulario-invitaciones" type="password" style="width:100%"
                           id="repetircontrasena_miportal" value="" placeholder="***********"/>
                    <p>
                        <input type="checkbox" class="filled-in" id="aplicar_all"/>
                        <label for="aplicar_all">Aplicar a todas las secciones de la web de novios.</label>
                    </p>
                    <br>
                    <small id="mensaje_error_configurar" style="margin-bottom: 10px;"
                           class="red-text darken-5a"></small>
                    <h6>&nbsp;</h6>
                    <button id="contrasena_modify_configuracion"
                            class="waves-effect waves-light btn-flat green darken-3 white-text"
                            style="width:100%; margin-bottom: 5px;">Guardar
                    </button>
                    <button onclick="goToConfiguracion(1);" class="waves-effect waves-light btn-flat"
                            style="width:100%; margin-bottom: 5px;">Cancelar
                    </button>
                </div>
            </div>

        </div>

        <div id="diseno" style="display:none;">
            <div class="body-container" id="main-diseno">

                <h6><b>PERSONALIZAR</b></h6>
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">format_paint</i>Cambiar temas</div>
                        <div class="collapsible-body">
                            <div class="body-container">
                                <div class="row">
                                    <?php foreach ($otherplantilla as $key => $value) { ?>
                                        <div class="col m6 s12">
                                            <div class="card">
                                                <div class="card-content">
                                                    <img class="responsive-img"
                                                         src="<?php echo base_url().$value->img_demo ?>" alt=""/>
                                                    <p class="center" style="padding:0px;">
                                                        <input class="with-gap"
                                                               name="other_plantilla" <?php echo ($miportal->id_plantillaportal == decrypt($value->id_plantillaportal)) ? 'checked' : '' ?>
                                                               type="radio"
                                                               id="plantilla-<?php echo $value->id_plantillaportal ?>"
                                                               data-id="<?php echo $value->id_plantillaportal ?>"
                                                               data-nombre="<?php echo $value->nombre ?>"/>
                                                        <label for="plantilla-<?php echo $value->id_plantillaportal ?>"></label>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <button class="waves-effect waves-light btn dorado-2" id="restart-tema">Restablecer
                                    tema
                                </button>
                                <div style="margin:20px;"></div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">title</i>T&iacute;tulo de la p&aacute;gina
                        </div>
                        <div class="collapsible-body">
                            <div class="body-container">
                                <h6><b>COLOR TEXTO</b></h6>
                                <div class="row">
                                    <div class="col s4"><input class="formulario-invitaciones" id="inputColorTitulo"
                                                               type="text" maxlength="7" size="7"
                                                               value="<?php echo empty($miportal->titleColor) ? $plantilla->titleColor : $miportal->titleColor ?>"
                                                               style="width:100%"/></div>
                                    <div class="col s8">
                                        <div class="selector" id="colorTitulo">
                                            <div style="background-color: <?php echo empty($miportal->titleColor) ? $plantilla->titleColor : $miportal->titleColor ?>"></div>
                                        </div>
                                    </div>
                                </div>

                                <h6><b>TIPO DE LETRA</b></h6>
                                <div>
                                    <?php $title = empty($miportal->titleFont) ? $plantilla->titleFont : $miportal->titleFont ?>
                                    <select id="selectFontTitulo" class="formulario-invitaciones">
                                        <option value="aquawax" <?php echo ('aquawax' == $title) ? 'selected' : '' ?>>
                                            Aquawax
                                        </option>
                                        <option value="birdsofparadise" <?php echo ('birdsofparadise' == $title) ? 'selected' : '' ?>>
                                            Birds of paradise
                                        </option>
                                        <option value="bodoni" <?php echo ('bodoni' == $title) ? 'selected' : '' ?>>
                                            Bodoni XT
                                        </option>
                                        <option value="brannboll" <?php echo ('brannboll' == $title) ? 'selected' : '' ?>>
                                            Brannboll
                                        </option>
                                        <option value="champagne" <?php echo ('champagne' == $title) ? 'selected' : '' ?>>
                                            Champagne
                                        </option>
                                        <option value="champagne_limousines" <?php echo ('champagne_limousines' == $title) ? 'selected' : '' ?>>
                                            Champagne Limousines
                                        </option>
                                        <option value="comfortaa" <?php echo ('comfortaa' == $title) ? 'selected' : '' ?>>
                                            Comfortaa
                                        </option>
                                        <option value="coolvetica" <?php echo ('coolvetica' == $title) ? 'selected' : '' ?>>
                                            Coolvetica
                                        </option>
                                        <option value="gravity" <?php echo ('gravity' == $title) ? 'selected' : '' ?>>
                                            Gravity
                                        </option>
                                        <option value="jonquilles" <?php echo ('jonquilles' == $title) ? 'selected' : '' ?>>
                                            Jonquilles
                                        </option>
                                        <option value="keep_calm" <?php echo ('keep_calm' == $title) ? 'selected' : '' ?>>
                                            Keep Calm
                                        </option>
                                        <option value="littlelo" <?php echo ('littlelo' == $title) ? 'selected' : '' ?>>
                                            Littlelo
                                        </option>
                                        <option value="lobster" <?php echo ('lobster' == $title) ? 'selected' : '' ?>>
                                            Lobster
                                        </option>
                                        <option value="lora" <?php echo ('lora' == $title) ? 'selected' : '' ?>>Lora
                                        </option>
                                        <option value="mademoiselle_catherine" <?php echo ('mademoiselle_catherine' == $title) ? 'selected' : '' ?>>
                                            Mademoiselle Catherine
                                        </option>
                                        <option value="modikasti" <?php echo ('modikasti' == $title) ? 'selected' : '' ?>>
                                            Modikasti
                                        </option>
                                        <option value="montserrat" <?php echo ('montserrat' == $title) ? 'selected' : '' ?>>
                                            Montserrat
                                        </option>
                                        <option value="pacifico" <?php echo ('pacifico' == $title) ? 'selected' : '' ?>>
                                            Pacifico
                                        </option>
                                        <option value="prata" <?php echo ('prata' == $title) ? 'selected' : '' ?>>
                                            Prata
                                        </option>
                                        <option value="prettygirlsscript" <?php echo ('prettygirlsscript' == $title) ? 'selected' : '' ?>>
                                            Pretty Girls
                                        </option>
                                        <option value="roboto" <?php echo ('roboto' == $title) ? 'selected' : '' ?>>
                                            Roboto
                                        </option>
                                        <option value="sophia" <?php echo ('sophia' == $title) ? 'selected' : '' ?>>
                                            Sophia
                                        </option>
                                        <option value="soputan" <?php echo ('soputan' == $title) ? 'selected' : '' ?>>
                                            Soputan
                                        </option>
                                        <option value="starstruck" <?php echo ('starstruck' == $title) ? 'selected' : '' ?>>
                                            Starstruck
                                        </option>
                                        <option value="yanone_kaffeesatz" <?php echo ('yanone_kaffeesatz' == $title) ? 'selected' : '' ?>>
                                            Yanone Kaffeesatz
                                        </option>
                                        <option value="whitelarch" <?php echo ('whitelarch' == $title) ? 'selected' : '' ?>>
                                            WhiteLarch
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">menu</i>Men&uacute;</div>
                        <div class="collapsible-body">
                            <div class="body-container">
                                <h6><b>COLOR FONDO</b></h6>
                                <div class="row">
                                    <div class="col s4"><input class="formulario-invitaciones" id="inputColorFondoMenu"
                                                               type="text" maxlength="7" size="7"
                                                               value="<?php echo empty($miportal->menuBackground) ? $plantilla->menuBackground : $miportal->menuBackground ?>"
                                                               style="width:100%"/></div>
                                    <div class="col s8">
                                        <div class="selector" id="colorFondoMenu">
                                            <div style="background-color: <?php echo empty($miportal->menuBackground) ? $plantilla->menuBackground : $miportal->menuBackground ?>"></div>
                                        </div>
                                    </div>
                                </div>
                                <h6><b>COLOR TEXTO</b></h6>
                                <div class="row">
                                    <div class="col s4"><input class="formulario-invitaciones" id="inputColorTextoMenu"
                                                               type="text" maxlength="7" size="7"
                                                               value="<?php echo empty($miportal->menuColor) ? $plantilla->menuColor : $miportal->menuColor ?>"
                                                               style="width:100%"/></div>
                                    <div class="col s8">
                                        <div class="selector" id="colorTextoMenu">
                                            <div style="background-color: <?php echo empty($miportal->menuColor) ? $plantilla->menuColor : $miportal->menuColor ?>"></div>
                                        </div>
                                    </div>
                                </div>

                                <h6><b>TIPO DE LETRA</b></h6>
                                <div>
                                    <?php $menu = empty($miportal->menuFont) ? $plantilla->menuFont : $miportal->menuFont ?>
                                    <select id="selectFontTitulo" class="formulario-invitaciones">
                                        <option value="aquawax" <?php echo ('aquawax' == $menu) ? 'selected' : '' ?>>
                                            Aquawax
                                        </option>
                                        <option value="birdsofparadise" <?php echo ('birdsofparadise' == $menu) ? 'selected' : '' ?>>
                                            Birds of paradise
                                        </option>
                                        <option value="bodoni" <?php echo ('bodoni' == $menu) ? 'selected' : '' ?>>
                                            Bodoni XT
                                        </option>
                                        <option value="brannboll" <?php echo ('brannboll' == $menu) ? 'selected' : '' ?>>
                                            Brannboll
                                        </option>
                                        <option value="champagne" <?php echo ('champagne' == $menu) ? 'selected' : '' ?>>
                                            Champagne
                                        </option>
                                        <option value="champagne_limousines" <?php echo ('champagne_limousines' == $menu) ? 'selected' : '' ?>>
                                            Champagne Limousines
                                        </option>
                                        <option value="comfortaa" <?php echo ('comfortaa' == $menu) ? 'selected' : '' ?>>
                                            Comfortaa
                                        </option>
                                        <option value="coolvetica" <?php echo ('coolvetica' == $menu) ? 'selected' : '' ?>>
                                            Coolvetica
                                        </option>
                                        <option value="gravity" <?php echo ('gravity' == $menu) ? 'selected' : '' ?>>
                                            Gravity
                                        </option>
                                        <option value="jonquilles" <?php echo ('jonquilles' == $menu) ? 'selected' : '' ?>>
                                            Jonquilles
                                        </option>
                                        <option value="keep_calm" <?php echo ('keep_calm' == $menu) ? 'selected' : '' ?>>
                                            Keep Calm
                                        </option>
                                        <option value="littlelo" <?php echo ('littlelo' == $menu) ? 'selected' : '' ?>>
                                            Littlelo
                                        </option>
                                        <option value="lobster" <?php echo ('lobster' == $menu) ? 'selected' : '' ?>>
                                            Lobster
                                        </option>
                                        <option value="lora" <?php echo ('lora' == $menu) ? 'selected' : '' ?>>Lora
                                        </option>
                                        <option value="mademoiselle_catherine" <?php echo ('mademoiselle_catherine' == $menu) ? 'selected' : '' ?>>
                                            Mademoiselle Catherine
                                        </option>
                                        <option value="modikasti" <?php echo ('modikasti' == $menu) ? 'selected' : '' ?>>
                                            Modikasti
                                        </option>
                                        <option value="montserrat" <?php echo ('montserrat' == $menu) ? 'selected' : '' ?>>
                                            Montserrat
                                        </option>
                                        <option value="pacifico" <?php echo ('pacifico' == $menu) ? 'selected' : '' ?>>
                                            Pacifico
                                        </option>
                                        <option value="prata" <?php echo ('prata' == $menu) ? 'selected' : '' ?>>Prata
                                        </option>
                                        <option value="prettygirlsscript" <?php echo ('prettygirlsscript' == $menu) ? 'selected' : '' ?>>
                                            Pretty Girls
                                        </option>
                                        <option value="roboto" <?php echo ('roboto' == $menu) ? 'selected' : '' ?>>
                                            Roboto
                                        </option>
                                        <option value="sophia" <?php echo ('sophia' == $menu) ? 'selected' : '' ?>>
                                            Sophia
                                        </option>
                                        <option value="soputan" <?php echo ('soputan' == $menu) ? 'selected' : '' ?>>
                                            Soputan
                                        </option>
                                        <option value="starstruck" <?php echo ('starstruck' == $menu) ? 'selected' : '' ?>>
                                            Starstruck
                                        </option>
                                        <option value="yanone_kaffeesatz" <?php echo ('yanone_kaffeesatz' == $menu) ? 'selected' : '' ?>>
                                            Yanone Kaffeesatz
                                        </option>
                                        <option value="whitelarch" <?php echo ('whitelarch' == $menu) ? 'selected' : '' ?>>
                                            WhiteLarch
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">text_fields</i>T&iacute;tulos</div>
                        <div class="collapsible-body">
                            <div class="body-container">
                                <h6><b>COLOR TEXTO</b></h6>
                                <div class="row">
                                    <div class="col s4"><input class="formulario-invitaciones" id="inputSubtitulo"
                                                               type="text" maxlength="7" size="7"
                                                               value="<?php echo empty($miportal->subtitleColor) ? $plantilla->subtitleColor : $miportal->subtitleColor ?>"
                                                               style="width:100%"/></div>
                                    <div class="col s8">
                                        <div class="selector" id="colorSubtitulo">
                                            <div style="background-color: <?php echo empty($miportal->subtitleColor) ? $plantilla->subtitleColor : $miportal->subtitleColor ?>"></div>
                                        </div>
                                    </div>
                                </div>

                                <h6><b>TIPO DE LETRA</b></h6>
                                <div>
                                    <?php $subtitle = empty($miportal->subtitleFont) ? $plantilla->subtitleFont : $miportal->subtitleFont ?>
                                    <select id="selectFontTitulo" class="formulario-invitaciones">
                                        <option value="aquawax" <?php echo ('aquawax' == $subtitle) ? 'selected' : '' ?>>
                                            Aquawax
                                        </option>
                                        <option value="birdsofparadise" <?php echo ('birdsofparadise' == $subtitle) ? 'selected' : '' ?>>
                                            Birds of paradise
                                        </option>
                                        <option value="bodoni" <?php echo ('bodoni' == $subtitle) ? 'selected' : '' ?>>
                                            Bodoni XT
                                        </option>
                                        <option value="brannboll" <?php echo ('brannboll' == $subtitle) ? 'selected' : '' ?>>
                                            Brannboll
                                        </option>
                                        <option value="champagne" <?php echo ('champagne' == $subtitle) ? 'selected' : '' ?>>
                                            Champagne
                                        </option>
                                        <option value="champagne_limousines" <?php echo ('champagne_limousines' == $subtitle) ? 'selected' : '' ?>>
                                            Champagne Limousines
                                        </option>
                                        <option value="comfortaa" <?php echo ('comfortaa' == $subtitle) ? 'selected' : '' ?>>
                                            Comfortaa
                                        </option>
                                        <option value="coolvetica" <?php echo ('coolvetica' == $subtitle) ? 'selected' : '' ?>>
                                            Coolvetica
                                        </option>
                                        <option value="gravity" <?php echo ('gravity' == $subtitle) ? 'selected' : '' ?>>
                                            Gravity
                                        </option>
                                        <option value="jonquilles" <?php echo ('jonquilles' == $subtitle) ? 'selected' : '' ?>>
                                            Jonquilles
                                        </option>
                                        <option value="keep_calm" <?php echo ('keep_calm' == $subtitle) ? 'selected' : '' ?>>
                                            Keep Calm
                                        </option>
                                        <option value="littlelo" <?php echo ('littlelo' == $subtitle) ? 'selected' : '' ?>>
                                            Littlelo
                                        </option>
                                        <option value="lobster" <?php echo ('lobster' == $subtitle) ? 'selected' : '' ?>>
                                            Lobster
                                        </option>
                                        <option value="lora" <?php echo ('lora' == $subtitle) ? 'selected' : '' ?>>
                                            Lora
                                        </option>
                                        <option value="mademoiselle_catherine" <?php echo ('mademoiselle_catherine' == $subtitle) ? 'selected' : '' ?>>
                                            Mademoiselle Catherine
                                        </option>
                                        <option value="modikasti" <?php echo ('modikasti' == $subtitle) ? 'selected' : '' ?>>
                                            Modikasti
                                        </option>
                                        <option value="montserrat" <?php echo ('montserrat' == $subtitle) ? 'selected' : '' ?>>
                                            Montserrat
                                        </option>
                                        <option value="pacifico" <?php echo ('pacifico' == $subtitle) ? 'selected' : '' ?>>
                                            Pacifico
                                        </option>
                                        <option value="prata" <?php echo ('prata' == $subtitle) ? 'selected' : '' ?>>
                                            Prata
                                        </option>
                                        <option value="prettygirlsscript" <?php echo ('prettygirlsscript' == $subtitle) ? 'selected' : '' ?>>
                                            Pretty Girls
                                        </option>
                                        <option value="roboto" <?php echo ('roboto' == $subtitle) ? 'selected' : '' ?>>
                                            Roboto
                                        </option>
                                        <option value="sophia" <?php echo ('sophia' == $subtitle) ? 'selected' : '' ?>>
                                            Sophia
                                        </option>
                                        <option value="soputan" <?php echo ('soputan' == $subtitle) ? 'selected' : '' ?>>
                                            Soputan
                                        </option>
                                        <option value="starstruck" <?php echo ('starstruck' == $subtitle) ? 'selected' : '' ?>>
                                            Starstruck
                                        </option>
                                        <option value="yanone_kaffeesatz" <?php echo ('yanone_kaffeesatz' == $subtitle) ? 'selected' : '' ?>>
                                            Yanone Kaffeesatz
                                        </option>
                                        <option value="whitelarch" <?php echo ('whitelarch' == $subtitle) ? 'selected' : '' ?>>
                                            WhiteLarch
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">format_textdirection_l_to_r</i>P&aacute;rrafos
                        </div>
                        <div class="collapsible-body">
                            <div class="body-container">
                                <h6><b>COLOR TEXTO</b></h6>
                                <div class="row">
                                    <div class="col s4"><input class="formulario-invitaciones" id="inputColorParrafo"
                                                               type="text" maxlength="7" size="7"
                                                               value="<?php echo empty($miportal->textColor) ? $plantilla->textColor : $miportal->textColor ?>"
                                                               style="width:100%"/></div>
                                    <div class="col s8">
                                        <div class="selector" id="colorParrafo">
                                            <div style="background-color: <?php echo empty($miportal->textColor) ? $plantilla->textColor : $miportal->textColor ?>"></div>
                                        </div>
                                    </div>
                                </div>
                                <h6><b>COLOR LINKS</b></h6>
                                <div class="row">
                                    <div class="col s4"><input class="formulario-invitaciones" id="inputColorLink"
                                                               type="text" maxlength="7" size="7"
                                                               value="<?php echo empty($miportal->linkColor) ? $plantilla->linkColor : $miportal->linkColor ?>"
                                                               style="width:100%"/></div>
                                    <div class="col s8">
                                        <div class="selector" id="colorLink">
                                            <div style="background-color: <?php echo empty($miportal->linkColor) ? $plantilla->linkColor : $miportal->linkColor ?>"></div>
                                        </div>
                                    </div>
                                </div>

                                <h6><b>TIPO DE LETRA</b></h6>
                                <div>
                                    <?php $parrafo = empty($miportal->textFont) ? $plantilla->textFont : $miportal->textFont ?>
                                    <select id="selectFontTitulo" class="formulario-invitaciones">
                                        <option value="aquawax" <?php echo ('aquawax' == $parrafo) ? 'selected' : '' ?>>
                                            Aquawax
                                        </option>
                                        <option value="birdsofparadise" <?php echo ('birdsofparadise' == $parrafo) ? 'selected' : '' ?>>
                                            Birds of paradise
                                        </option>
                                        <option value="bodoni" <?php echo ('bodoni' == $parrafo) ? 'selected' : '' ?>>
                                            Bodoni XT
                                        </option>
                                        <option value="brannboll" <?php echo ('brannboll' == $parrafo) ? 'selected' : '' ?>>
                                            Brannboll
                                        </option>
                                        <option value="champagne" <?php echo ('champagne' == $parrafo) ? 'selected' : '' ?>>
                                            Champagne
                                        </option>
                                        <option value="champagne_limousines" <?php echo ('champagne_limousines' == $parrafo) ? 'selected' : '' ?>>
                                            Champagne Limousines
                                        </option>
                                        <option value="comfortaa" <?php echo ('comfortaa' == $parrafo) ? 'selected' : '' ?>>
                                            Comfortaa
                                        </option>
                                        <option value="coolvetica" <?php echo ('coolvetica' == $parrafo) ? 'selected' : '' ?>>
                                            Coolvetica
                                        </option>
                                        <option value="gravity" <?php echo ('gravity' == $parrafo) ? 'selected' : '' ?>>
                                            Gravity
                                        </option>
                                        <option value="jonquilles" <?php echo ('jonquilles' == $parrafo) ? 'selected' : '' ?>>
                                            Jonquilles
                                        </option>
                                        <option value="keep_calm" <?php echo ('keep_calm' == $parrafo) ? 'selected' : '' ?>>
                                            Keep Calm
                                        </option>
                                        <option value="littlelo" <?php echo ('littlelo' == $parrafo) ? 'selected' : '' ?>>
                                            Littlelo
                                        </option>
                                        <option value="lobster" <?php echo ('lobster' == $parrafo) ? 'selected' : '' ?>>
                                            Lobster
                                        </option>
                                        <option value="lora" <?php echo ('lora' == $parrafo) ? 'selected' : '' ?>>Lora
                                        </option>
                                        <option value="mademoiselle_catherine" <?php echo ('mademoiselle_catherine' == $parrafo) ? 'selected' : '' ?>>
                                            Mademoiselle Catherine
                                        </option>
                                        <option value="modikasti" <?php echo ('modikasti' == $parrafo) ? 'selected' : '' ?>>
                                            Modikasti
                                        </option>
                                        <option value="montserrat" <?php echo ('montserrat' == $parrafo) ? 'selected' : '' ?>>
                                            Montserrat
                                        </option>
                                        <option value="pacifico" <?php echo ('pacifico' == $parrafo) ? 'selected' : '' ?>>
                                            Pacifico
                                        </option>
                                        <option value="prata" <?php echo ('prata' == $parrafo) ? 'selected' : '' ?>>
                                            Prata
                                        </option>
                                        <option value="prettygirlsscript" <?php echo ('prettygirlsscript' == $parrafo) ? 'selected' : '' ?>>
                                            Pretty Girls
                                        </option>
                                        <option value="roboto" <?php echo ('roboto' == $parrafo) ? 'selected' : '' ?>>
                                            Roboto
                                        </option>
                                        <option value="sophia" <?php echo ('sophia' == $parrafo) ? 'selected' : '' ?>>
                                            Sophia
                                        </option>
                                        <option value="soputan" <?php echo ('soputan' == $parrafo) ? 'selected' : '' ?>>
                                            Soputan
                                        </option>
                                        <option value="starstruck" <?php echo ('starstruck' == $parrafo) ? 'selected' : '' ?>>
                                            Starstruck
                                        </option>
                                        <option value="yanone_kaffeesatz" <?php echo ('yanone_kaffeesatz' == $parrafo) ? 'selected' : '' ?>>
                                            Yanone Kaffeesatz
                                        </option>
                                        <option value="whitelarch" <?php echo ('whitelarch' == $parrafo) ? 'selected' : '' ?>>
                                            WhiteLarch
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">brush</i>Fondo y detalles</div>
                        <div class="collapsible-body">
                            <h6><b>FONDO CUENTA REGRESIVA</b></h6>
                            <div class="row">
                                <div class="col s4"><input class="formulario-invitaciones" id="inputFondoCuenta"
                                                           type="text" maxlength="7" size="7"
                                                           value="<?php echo empty($miportal->cuentaBackground) ? $plantilla->cuentaBackground : $miportal->cuentaBackground ?>"
                                                           style="width:100%"/></div>
                                <div class="col s8">
                                    <div class="selector" id="colorFondoCuenta">
                                        <div style="background-color: <?php echo empty($miportal->cuentaBackground) ? $plantilla->cuentaBackground : $miportal->cuentaBackground ?>"></div>
                                    </div>
                                </div>
                            </div>
                            <h6><b>COLOR TEXTO CUENTA REGRESIVA</b></h6>
                            <div class="row">
                                <div class="col s4"><input class="formulario-invitaciones" id="inputTextoCuenta"
                                                           type="text" maxlength="7" size="7"
                                                           value="<?php echo empty($miportal->cuentaColor) ? $plantilla->cuentaColor : $miportal->cuentaColor ?>"
                                                           style="width:100%"/></div>
                                <div class="col s8">
                                    <div class="selector" id="colorTextoCuenta">
                                        <div style="background-color: <?php echo empty($miportal->cuentaColor) ? $plantilla->cuentaColor : $miportal->cuentaColor ?>"></div>
                                    </div>
                                </div>
                            </div>
                            <h6><b>COLOR DE FONDO</b></h6>
                            <div class="row">
                                <div class="col s4"><input class="formulario-invitaciones" id="inputBackground"
                                                           type="text" maxlength="7" size="7"
                                                           value="<?php echo empty($miportal->bodyBackground) ? $plantilla->bodyBackground : $miportal->bodyBackground ?>"
                                                           style="width:100%"/></div>
                                <div class="col s8">
                                    <div class="selector" id="colorBackground">
                                        <div style="background-color: <?php echo empty($miportal->bodyBackground) ? $plantilla->bodyBackground : $miportal->bodyBackground ?>"></div>
                                    </div>
                                </div>
                            </div>
                            <h6><b>IMAGEN DE FONDO</b></h6>
                            <div class="row" id="imagen_fondo_tema">
                                <?php foreach ($image_plantilla as $key => $value) { ?>
                                    <div class="col s12 m6">
                                        <div class="card">
                                            <div class="card-content">
                                                <!-- "data:$value->mime;base64," . base64_encode($value->base64) -->
                                                <img class="responsive-img"
                                                     src="<?php echo "data:$value->mime;base64,".base64_encode($value->base64) ?>"/>
                                                <p class="center" style="padding:0px;">
                                                    <input class="with-gap"
                                                           name="image_fondo" <?php echo ($miportal->img_table == 'plantillaportal_imagen' && $miportal->img_id == $value->id_plantillaportal_imagen) ? 'checked' : '' ?>
                                                           type="radio"
                                                           id="img-<?php echo $value->id_plantillaportal_imagen ?>"
                                                           data-id="<?php echo $value->id_plantillaportal_imagen ?>"
                                                           data-url="<?php echo base_url().$value->url_img ?>"/>
                                                    <label for="img-<?php echo $value->id_plantillaportal_imagen ?>"></label>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div id="compartir" style="display:none">
            <div class="body-container">
                <div class="section">
                    <h6><b>COPIA Y COMPARTE EL ENLACE</b></h6>
                    <input class="formulario-invitaciones" style="width:100%"
                           value="<?php echo base_url().'index.php/web/index/'.decrypt($miportal->id_miportal) ?>"
                           onclick="copyToClipboard('<?php echo base_url().'index.php/web/index/'.decrypt($miportal->id_miportal) ?>');"
                           type="text" readonly=""/>
                </div>
                <div class="divider"></div>
                <div class="section">
                    <form id="send-email-invitad">
                        <h6><b>ENV&Iacute;A INVITACIONES</b></h6>
                        <div id="mensaje-send-email"></div>
                        <h6><i>PARA</i></h6>
                        <small>Comp&aacute;rtela con todos separando los mails por comas (ej: luis@gmail.com,
                            laura@gmail.com).
                        </small>
                        <br>
                        <br>
                        <input class="formulario-invitaciones validate[required]" style="width:100%" name="correos"
                               id="tokenfield" type="text" placeholder="E-mail"/>
                        <h6><i>ASUNTO</i></h6>
                        <input class="formulario-invitaciones validate[required,minSize[4]]" style="width:100%"
                               name="asunto" type="text" value="Te invito a visitar mi web de boda."/>
                        <h6><i>MENSAJE</i></h6>
                        <textarea
                                class="materialize-textarea formulario-invitaciones white validate[required,minSize[4]]"
                                name="mensaje" required>Hola! Hemos creado una p&aacute;gina web en la que explicamos todo sobre nuestro gran d&iacute;a. Adem&aacute;s ver&aacute;s un mont&oacute;n de fotos nuestras y alg&uacute;n juego que hemos preparado :) Y m&iacute;rate WedShoots, es una app que puedes usar para nuestra boda.</textarea>
                        <br>
                        <button class="waves-effect waves-light btn-flat dorado-2 white-text" id="send-invit"
                                type="button" style="width:100%; margin-bottom: 5px;">
                            Enviar Invitaciones
                        </button>
                    </form>
                </div>
            </div>
        </div>

    </div>


    <section class="body">
        <div class="file-field input-field" style="position: absolute; top: 0px; right: 25px;">
            <div class="waves-effect waves-light btn-flat white botn">
                <span><i class="material-icons left">camera_enhance</i>&nbsp;&nbsp;Subir imagen</span>
                <input type="file" accept="image/*" onchange="openFile(event, 'header')">
            </div>
        </div>
        <?php
        $this->view("principal/novia/miportal/estructura_portal/menu");
        $controller = $_SERVER['REQUEST_URI'];
        $controller = strtolower($controller);
        $controller = explode('/clubnupcial/index.php/novios/miweb', $controller);
        $controller = $controller[1];

        switch ($controller) {
            case '/home/blog':
                $this->view("principal/novia/miportal/estructura_portal/blog");
                break;
            case '/home/direccion':
                $this->view("principal/novia/miportal/estructura_portal/direccion");
                break;
            case '/home/album':
                $this->view("principal/novia/miportal/estructura_portal/fotos");
                break;
            case '/home/encuesta':
                $this->view("principal/novia/miportal/estructura_portal/encuesta");
                break;
            case '/home/evento':
                $this->view("principal/novia/miportal/estructura_portal/eventos");
                break;
            case '/home/video':
                $this->view("principal/novia/miportal/estructura_portal/video");
                break;
            case '/home/test':
                $this->view("principal/novia/miportal/estructura_portal/test");
                break;
            case '/home/asistencia':
                $this->view("principal/novia/miportal/estructura_portal/confirma");
                break;
            case '/home/libro':
                $this->view("principal/novia/miportal/estructura_portal/libro");
                break;
            case '/home/contactar':
                $this->view("principal/novia/miportal/estructura_portal/contactanos");
                break;
            case '':
            case'/home':
                $this->view("principal/novia/miportal/estructura_portal/index");
                break;
        }

        $this->view("principal/novia/miportal/estructura_portal/piepagina")
        ?>
    </section>
</main>
<!--</div>-->


<div id="modal-imagen" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="section">
            <h6 id="modal-imagen-title">Selecciona lo que se ver&aacute; en la invitaci&oacute;n</h6>
        </div>
        <div class="divider"></div>
        <br>
        <div class="row">
            <div class="col offset-m2 m8" id="modal-content">
                <img id="modal-content-image" width="100%">
                <img id="preview" class="hidden"/>
                <canvas id="canvas"></canvas>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button id="modal-imagen-cancelar" type="button"
                class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar
        </button>
        <button id="modal-imagen-aceptar" type="button"
                class="modal-action modal-close waves-effect waves-green btn-flat dorado-2 white-text">Guardar
        </button>
    </div>
</div>


<div id="cargando-loader" class="modal">
    <div class="modal-content">
        <div class="preloader-wrapper big active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
<?php $this->view("principal/novia/miportal/estructura/footer") ?>
<script>
    document.addEventListener("DOMContentLoaded", init);

    function init() {
        var navElement = document.querySelector('main > .nav'),
            windowHeight = getWindowHeight();

        function buffer(callBack) {
            var timeoutId, lastCall = 0, bufferSpan = 100;
            var bufferedFunction = function () {
                if (timeoutId) {
                    return;
                }
                if (Date.now() - lastCall > bufferSpan) {
                    callBack();
                    lastCall = Date.now();
                } else {
                    timeoutId = setTimeout(function () {
                        timeoutId = null;
                        callBack();
                        lastCall = Date.now();
                    }, bufferSpan);
                }
            }
            return bufferedFunction;
        }

        function getWindowHeight() {
            return document.getElementsByTagName('header')[0].offsetHeight;
        }

        window.addEventListener('resize', buffer(handleResize));

        function handleResize() {
            windowHeight = getWindowHeight();
            handleScroll();
        }

        var fixed = false;
        window.addEventListener('scroll', buffer(handleScroll));
        window.addEventListener('scroll', function () {
            if (disableScrollEvents)
                return true;
            if (window.pageYOffset >= windowHeight) {
                if (!fixed) {
                    fixed = true;
                    navElement.classList.add('fixed');
                }
            } else {
                if (fixed) {
                    fixed = false;
                    navElement.classList.remove('fixed');
                }
            }
        });

        var disableScrollEvents = false;

        function handleScroll() {
            if (disableScrollEvents)
                return true;
        }
    }

    $(document).ready(function () {
        $('.modal').modal();

        /* ---------    MI PAGINA      ----------- */
        tinymce.init({
            selector: '.tinymce-editor',
            plugins: "autoresize link",
            menubar: '',
            height: "250",
            toolbar: 'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | link',
        });

        $('[name="other_plantilla"]').on('change', function () {
            $.ajax({
                url: '<?php echo base_url() ?>index.php/novios/miweb/home/change_theme',
                method: 'POST',
                timeout: 4000,
                data: {
                    id_plantillaportal: $(this).data('id'),
                },
            }).done(function (datos) {
                console.log('---------');
                changeValue('titleColor', datos.info_plantilla.titleColor);
                changeValue('titleFontType', datos.info_plantilla.titleFont);
                changeValue('menuBackgrouncolor', datos.info_plantilla.menuBackground);
                changeValue('menuColor', datos.info_plantilla.menuColor);
                changeValue('menuFontType', datos.info_plantilla.menuFont);
                changeValue('cuentaBackground', datos.info_plantilla.cuentaBackground);
                changeValue('cuentaColor', datos.info_plantilla.cuentaColor);
                changeValue('bodyBackground', datos.info_plantilla.bodyBackground);
                changeValue('titleBody', datos.info_plantilla.subtitleColor);
                changeValue('titleBodyFontType', datos.info_plantilla.subtitleFont);
                changeValue('textColor', datos.info_plantilla.textColor);
                changeValue('linkColor', datos.info_plantilla.linkColor);
                changeValue('textFontType', datos.info_plantilla.textFont);

                $('#imagen_fondo_tema').text('');
                $.each(datos.image_plantilla, function (i, obj) {
                    if (obj.default == 1) {
                        changeValue('imgBackground', 'url(<?php echo base_url() ?>' + obj.url_img + ')');
                    }
                    var template = '<div class="col s12 m6">' +
                        '<div class="card">' +
                        '<div class="card-content">' +
                        '<img class="responsive-img" src="data:' + obj.mime + ';base64,' + obj.base64 + '" />' +
                        '<p class="center" style="padding:0px;">' +
                        '<input class="with-gap" name="image_fondo" ' + ((obj.default == 1) ? 'checked' : '') + ' type="radio" id="img-' + obj.id_plantillaportal_imagen + '" data-id="' + obj.id_plantillaportal_imagen + '" data-url="<?php echo base_url() ?>' + obj.url_img + '" />' +
                        '<label for="img-' + obj.id_plantillaportal_imagen + '"></label>' +
                        '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                    $('#imagen_fondo_tema').append(template);
                });

                $("#css-select").remove();
                $('head').append('<link id="css-select" rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/file-portal/css/' + datos.info_plantilla.nombreCSS + '.css">');

                $('[name="image_fondo"]').on('change', function () {
                    var aux = this;
                    $.ajax({
                        url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_miportal',
                        method: 'POST',
                        timeout: 4000,
                        data: {
                            tipo: 'img_fondo',
                            img_table: 'plantillaportal_imagen',
                            img_id: $(this).data('id'),
                        },
                    }).done(function () {
                        changeValue('imgBackground', 'url(' + $(aux).data('url') + ')');
                    }).fail(function () {
                        location.reload();
                    });
                });

                /* RESET VALUES DASHBOARD*/
                $('#inputColorTitulo').val(datos.info_plantilla.titleColor);
                $('#colorTitulo div').css('backgroundColor', datos.info_plantilla.titleColor);
                $('#colorTitulo').ColorPickerSetColor(datos.info_plantilla.titleColor);

                $('#inputColorTextoMenu').val(datos.info_plantilla.menuColor);
                $('#colorTextoMenu div').css('backgroundColor', datos.info_plantilla.menuColor);
                $('#colorTextoMenu').ColorPickerSetColor(datos.info_plantilla.menuColor);

                $('#inputColorFondoMenu').val(datos.info_plantilla.menuBackground);
                $('#colorFondoMenu div').css('backgroundColor', datos.info_plantilla.menuBackground);
                $('#colorFondoMenu').ColorPickerSetColor(datos.info_plantilla.menuBackground);

                $('#inputSubtitulo').val(datos.info_plantilla.subtitleColor);
                $('#colorSubtitulo div').css('backgroundColor', datos.info_plantilla.subtitleColor);
                $('#colorSubtitulo').ColorPickerSetColor(datos.info_plantilla.subtitleColor);

                $('#inputColorParrafo').val(datos.info_plantilla.textColor);
                $('#colorParrafo div').css('backgroundColor', datos.info_plantilla.textColor);
                $('#colorParrafo').ColorPickerSetColor(datos.info_plantilla.textColor);

                $('#inputColorLink').val(datos.info_plantilla.linkColor);
                $('#colorLink div').css('backgroundColor', datos.info_plantilla.linkColor);
                $('#colorLink').ColorPickerSetColor(datos.info_plantilla.linkColor);

                $('#inputBackground').val(datos.info_plantilla.bodyBackground);
                $('#colorBackground div').css('backgroundColor', datos.info_plantilla.bodyBackground);
                $('#colorBackground').ColorPickerSetColor(datos.info_plantilla.bodyBackground);

                $('#inputTextoCuenta').val(datos.info_plantilla.cuentaColor);
                $('#colorTextoCuenta div').css('backgroundColor', datos.info_plantilla.cuentaColor);
                $('#colorTextoCuenta').ColorPickerSetColor(datos.info_plantilla.cuentaColor);

                $('#inputFondoCuenta').val(datos.info_plantilla.cuentaBackground);
                $('#colorFondoCuenta div').css('backgroundColor', datos.info_plantilla.cuentaBackground);
                $('#colorFondoCuenta').ColorPickerSetColor(datos.info_plantilla.cuentaBackground);

                $('#selectFontTitulo').val(datos.info_plantilla.titleFont);
                $('#selectFontTitulo').material_select();

                $('#selectFontMenu').val(datos.info_plantilla.menuFont);
                $('#selectFontMenu').material_select();

                $('#selectFontSubtitulo').val(datos.info_plantilla.subtitleFont);
                $('#selectFontSubtitulo').material_select();

                $('#selectFontParrafo').val(datos.info_plantilla.textFont);
                $('#selectFontParrafo').material_select();
            }).fail(function () {
                location.reload();
            });
        });

        $('#titulo_miportal').on('keyup', function () {
            $('.encabezado .encabezado-principal .encabezado-top .titulo').text($(this).val());
        });

        $('[name="image_fondo"]').on('change', function () {
            var aux = this;
            $.ajax({
                url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_miportal',
                method: 'POST',
                timeout: 4000,
                data: {
                    tipo: 'img_fondo',
                    img_table: 'plantillaportal_imagen',
                    img_id: $(this).data('id'),
                },
            }).done(function () {
                console.log('success');
                changeValue('imgBackground', 'url(' + $(aux).data('url') + ')');
            }).fail(function () {
                location.reload();
            });
        });

        $('#nosotros_miportal').on('keyup', function () {
            $('.encabezado .encabezado-principal .encabezado-descripcion .nosotros').text($(this).val());
        });

        $('#fecha_boda_miportal').on('change', function () {
            var f = new Date($(this).val() + ' 23:00:00');
            var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            $('.encabezado .encabezado-principal .encabezado-descripcion .date').text(f.getDate() + " " + meses[f.getMonth()] + " " + f.getFullYear());

            var di_rest = DateDiff.inDays(new Date(), f);
            $('.encabezado .cuentaregresiva .numero').text(di_rest);
        });

        $('#eliminar_modify').on('click', function () {
            $.ajax({
                url: '<?php echo base_url() ?>index.php/novios/miweb/home/restaurar_seccion',
                method: 'POST',
                timeout: 4000,
                data: {
                    tipo: $('#tipo_modify').val(),
                    id: $('#id_miportal_seccion_modify').val(),
                },
            }).done(function () {
                console.log('success');
                if ('<?php echo $controller ?>' == '/home/' + $('#tipo_modify').val()) {
                    location.href = '<?php echo base_url() ?>index.php/novios/miweb';
                }
                $('#add-' + $('#id_miportal_seccion_modify').val()).attr('class', 'card paginas');
                $('#add-' + $('#id_miportal_seccion_modify').val()).on('click', function () {
                    back(3, null, $('#id_miportal_seccion_modify').val());
                });
                $('#main-' + $('#id_miportal_seccion_modify').val()).remove();

                $('#' + $('#tipo_modify').val() + '-navmobile').hide();
                $('#' + $('#tipo_modify').val() + '-navseccion').hide();

                back(1, null);
            }).fail(function () {
                location.reload();
            });
        });

        $('#guardar_modify').on('click', function () {
            if (!isEmpty($('#titulo_modify').val())) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_seccion',
                    method: 'POST',
                    timeout: 4000,
                    data: {
                        titulo: $('#titulo_modify').val(),
                        visible: $('#radio_publico').prop('checked') ? '1' : ($('#radio_privado').prop('checked') ? '2' : '3'),
                        id: $('#id_miportal_seccion_modify').val(),
                        tipo: $('tipo_modify').val(),
                    },
                }).done(function () {
                    console.log('success');
                    var li = $('#main-' + $('#id_miportal_seccion_modify').val());
                    var i = li.find('i')[0];
                    $(li.find('span')[0]).text($('#titulo_modify').val());
                    if ($('#radio_publico').prop('checked')) {
                        $(i).attr('class', 'material-icons green-text darken-2');
                        $(i).text('fiber_manual_record');
                    } else if ($('#radio_privado').prop('checked')) {
                        $(i).attr('class', 'material-icons orange-text darken-3');
                        $(i).text('lock');
                    } else {
                        $(i).attr('class', 'material-icons blue-grey-text lighten-2');
                        $(i).text('visibility_off');
                    }
                    if ($('#radio_novisible').prop('checked')) {
                        $('#' + $('#tipo_modify').val() + '-navmobile').hide();
                        $('#' + $('#tipo_modify').val() + '-navseccion').hide();
                    } else {
                        $('#' + $('#tipo_modify').val() + '-navmobile').show();
                        $('#' + $('#tipo_modify').val() + '-navseccion').show();
                    }
                    $($('#' + $('#tipo_modify').val() + '-navmobile').find('a')[0]).text($('#titulo_modify').val());
                    $($('#' + $('#tipo_modify').val() + '-navseccion').find('a')[0]).text($('#titulo_modify').val());

                    back(1, null);
                }).fail(function () {
                    location.reload();
                });
            } else {
                if (isEmpty($('#titulo_modify').val())) {
                    $('#titulo_modify').attr('class', $('#titulo_modify').attr('class') + ' invalid');
                } else {
                    $('#titulo_modify').attr('class', (($('#titulo_modify').attr('class')).split('invalid')[0]));
                }
            }
        });

        $('#restart-tema').on('click', function () {
            $.ajax({
                url: '<?php echo base_url() ?>index.php/novios/miweb/home/restart_miportal',
                method: 'GET',
                timeout: 4000,
            }).done(function () {
                location.reload();
                console.log('success');
            }).fail(function () {
                location.reload();
            });
        });

        $('#guardar_seccion').on('click', function () {
            if (!isEmpty($('#titulo').val()) && !isEmpty(tinymce.get(0).getContent())) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/activar_seccion',
                    method: "POST",
                    timeout: 4000,
                    data: {
                        titulo: $('#titulo').val(),
                        descripcion: tinymce.get(0).getContent(),
                        id: $('#id_miportal_seccion').val(),
                    },
                }).done(function () {
                    console.log('success');
                    var template = '<li class="collection-item dismissable" onclick="back(4,&#39;' + $('#id_miportal_seccion').val() + '&#39;)" id="main-' + $('#id_miportal_seccion').val() + '">'
                        + '<div>' + $('#titulo').val()
                        + '<a class="secondary-content"><i class="material-icons green-text darken-2">fiber_manual_record</i><i class="material-icons">keyboard_arrow_right</i></a>'
                        + '</div>'
                        + '</li>';
                    $('#seccion_activa').append(template);
                    $('#add-' + $('#id_miportal_seccion').val()).attr('class', 'card paginas select');
                    $('#add-' + $('#id_miportal_seccion').val()).unbind('click');

                    $($('#' + $('#tipo_miportal_seccion').val() + '-navmobile').find('a')[0]).text($('#titulo').val());
                    $($('#' + $('#tipo_miportal_seccion').val() + '-navseccion').find('a')[0]).text($('#titulo').val());

                    $('#' + $('#tipo_miportal_seccion').val() + '-navmobile').show();
                    $('#' + $('#tipo_miportal_seccion').val() + '-navseccion').show();

                    back(1, null);
                }).fail(function () {
                    location.reload();
                });
            } else {
                if (isEmpty($('#titulo').val())) {
                    $('#titulo').attr('class', $('#titulo').attr('class') + ' invalid');
                } else {
                    $('#titulo').attr('class', (($('#titulo').attr('class')).split('invalid')[0]));
                }

                if (isEmpty(tinymce.get(0).getContent())) {
                    alert('Descripcion no puede ser vacio');
                }
            }
        });

        /* ---------    CONFIGURACION      ----------- */
        $('.datepicker').datepicker({
            minDate: new Date(),
            altFormat: "yy-mm-dd",
            dateFormat: "yy-mm-dd",
            closeText: "Cerrar",
            changeYear: true,
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        });

        $('#cuentaatras_configuracion').on('change', function () {
            $.ajax({
                url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_miportal',
                method: 'POST',
                timeout: 4000,
                data: {
                    tipo: 'cuenta_atras',
                    cuenta_atras: ($('#cuentaatras_configuracion').prop('checked')) ? '1' : '2',
                },
            }).done(function () {
                console.log('success');
                if ($('#cuentaatras_configuracion').prop('checked')) {
                    $('.cuentaregresiva').show();
                } else {
                    $('.cuentaregresiva').hide();
                }

            }).fail(function () {
                location.reload();
            });
        });

        $('#contrasena_modify_configuracion').on('click', function () {
            if (isEmpty($('#contrasena_miportal').val()) || isEmpty($('#repetircontrasena_miportal').val())) {
                $('#mensaje_error_configurar').text('Los campos no pueden estar vacios.');
            } else if ($('#contrasena_miportal').val() == $('#repetircontrasena_miportal').val()) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_miportal',
                    method: 'POST',
                    timeout: 4000,
                    asyn: false,
                    data: {
                        tipo: 'contrasena',
                        contrasena: $('#contrasena_miportal').val(),
                        aplicarseccion: '' + $('#aplicar_all').prop('checked'),
                    },
                }).done(function () {
                    if ($('#aplicar_all').prop('checked')) {
                        var a = $('#seccion_activa').find('.secondary-content');
                        for (var x = 0; x < a.length; x++) {
                            var i = $(a[x]).find('i')[0];
                            $(i).attr('class', 'material-icons orange-text darken-3');
                            $(i).text('lock');
                        }
                    }
                    $('#estado_anterior').val('');
                    $('#cambiar_contrasena').show();
                    goToConfiguracion(1);
                    console.log('success');
                }).fail(function () {
                    location.reload();
                });
            } else {
                $('#mensaje_error_configurar').text('Los campos no coinciden.');
            }
        });

        $('#guardar_configuracion').on('click', function () {
            if (isEmpty($('#titulo_miportal').val()) || isEmpty($('#nosotros_miportal').val()) || isEmpty($('#fecha_boda_miportal').val())) {
                if (isEmpty($('#titulo_miportal').val())) {
                    $('#titulo_miportal').attr('class', $('#titulo_miportal').attr('class') + ' invalid');
                } else {
                    $('#titulo_miportal').attr('class', (($('#titulo_miportal').attr('class')).split('invalid')[0]));
                }

                if (isEmpty($('#nosotros_miportal').val())) {
                    $('#nosotros_miportal').attr('class', $('#nosotros_miportal').attr('class') + ' invalid');
                } else {
                    $('#nosotros_miportal').attr('class', (($('#nosotros_miportal').attr('class')).split('invalid')[0]));
                }

                if (isEmpty($('#fecha_boda_miportal').val())) {
                    $('#fecha_boda_miportal').attr('class', $('#fecha_boda_miportal').attr('class') + ' invalid');
                } else {
                    $('#fecha_boda_miportal').attr('class', (($('#fecha_boda_miportal').attr('class')).split('invalid')[0]));
                }
            } else {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_miportal',
                    method: 'POST',
                    timeout: 4000,
                    data: {
                        tipo: 'informacion',
                        titulo: $('#titulo_miportal').val(),
                        nosotros: $('#nosotros_miportal').val(),
                        fecha_boda: $('#fecha_boda_miportal').val(),
                    },
                }).done(function () {
                    $('#cambiar_contrasena').hide();
                    console.log('success');
                }).fail(function () {
                    location.reload();
                });
            }
        });

        $('#contrasena_configuracion').on('change', function () {
            if ($('#contrasena_configuracion').prop('checked')) {
                goToConfiguracion(2);
                $('#estado_anterior').val('inactivo');
            } else {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_miportal',
                    method: 'POST',
                    timeout: 4000,
                    data: {
                        tipo: 'contrasena',
                        contrasena: '',
                        aplicarseccion: 'false',
                    },
                }).done(function () {
                    $('#cambiar_contrasena').hide();
                    console.log('success');
                }).fail(function () {
                    location.reload();
                });
            }
        });

        /* ---------    COMPARTIR      ----------- */
        $('#send-email-invitad').validationEngine();
        $('#send-invit').on('click', function () {
            if ($('#send-email-invitad').validationEngine('validate')) {
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/send_email',
                    method: 'post',
                    timeout: 1000,
                    data: {
                        'correos': $('[name="correos"]').val(),
                        'asunto': $('[name="asunto"]').val(),
                        'mensaje': $('[name="mensaje"]').val(),
                    },
                }).done(function () {
                    $('#mensaje-send-email').html('<div class="chip teal darken-2 white-text" style="border-radius: 0px; width: 100%;">¡Tu mensaje ha sido enviado con &eacute;xito!<i class="material-icons">close</i></div>');
                }).fail(function () {
                    location.reload();
                });
            }
        });

        /* ---------    SELECT EVENT ONCHANGE      -----------*/

        $('#selectFontTitulo').on('change', function () {
            sendChangeColorFont('titleFont', $(this).val());
            changeValue('titleFontType', $(this).val());
        });

        $('#selectFontMenu').on('change', function () {
            sendChangeColorFont('menuFont', $(this).val());
            changeValue('menuFontType', $(this).val());
        });

        $('#selectFontSubtitulo').on('change', function () {
            sendChangeColorFont('subtitleFont', $(this).val());
            changeValue('titleBodyFontType', $(this).val());
        });

        $('#selectFontParrafo').on('change', function () {
            sendChangeColorFont('textFont', $(this).val());
            changeValue('textFontType', $(this).val());
        });

        /* ---------    FONT-FAMILY      ----------- */
        $('div.select-wrapper ul li span:contains("Aquawax")').css('font-family', 'aquawax');
        $('div.select-wrapper ul li span:contains("Birds of paradise")').css('font-family', 'pacifico');
        $('div.select-wrapper ul li span:contains("Bodoni XT")').css('font-family', 'bodonixt');
        $('div.select-wrapper ul li span:contains("Brannboll")').css('font-family', 'brannboll');
        $('div.select-wrapper ul li span:contains("Champagne")').css('font-family', 'champagne');
        $('div.select-wrapper ul li span:contains("Champagne Limousines")').css('font-family', 'champagnelimousines');
        $('div.select-wrapper ul li span:contains("Comfortaa")').css('font-family', 'comfortaa');
        $('div.select-wrapper ul li span:contains("Coolvetica")').css('font-family', 'coolvetica');
        $('div.select-wrapper ul li span:contains("Gravity")').css('font-family', 'gravity');
        $('div.select-wrapper ul li span:contains("Jonquilles")').css('font-family', 'jonquilles');
        $('div.select-wrapper ul li span:contains("Keep Calm")').css('font-family', 'keepcalm');
        $('div.select-wrapper ul li span:contains("Littlelo")').css('font-family', 'littlelo');
        $('div.select-wrapper ul li span:contains("Lora")').css('font-family', 'lora');
        $('div.select-wrapper ul li span:contains("Lobster")').css('font-family', 'lobster');
        $('div.select-wrapper ul li span:contains("Mademoiselle Catherine")').css('font-family', 'mademoiselle_catherine');
        $('div.select-wrapper ul li span:contains("Modikasti")').css('font-family', 'modikasti');
        $('div.select-wrapper ul li span:contains("Montserrat")').css('font-family', 'montserrat');
        $('div.select-wrapper ul li span:contains("Pacifico")').css('font-family', 'pacifico');
        $('div.select-wrapper ul li span:contains("Prata")').css('font-family', 'prata');
        $('div.select-wrapper ul li span:contains("Pretty Girls")').css('font-family', 'prettygirls');
        $('div.select-wrapper ul li span:contains("Roboto")').css('font-family', 'roboto');
        $('div.select-wrapper ul li span:contains("Sophia")').css('font-family', 'sophia');
        $('div.select-wrapper ul li span:contains("Soputan")').css('font-family', 'soputan');
        $('div.select-wrapper ul li span:contains("Starstruck")').css('font-family', 'starstruck');
        $('div.select-wrapper ul li span:contains("Yanone Kaffeesatz")').css('font-family', 'yanone_kaffeesatz');
        $('div.select-wrapper ul li span:contains("WhiteLarch")').css('font-family', 'whiteLarch');

        /* ---------    DISENIO - TITULO      ----------- */
        $('#colorTitulo').ColorPicker({
            color: '<?php echo empty($miportal->titleColor) ? $plantilla->titleColor : $miportal->titleColor ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('titleColor', $('#inputColorTitulo').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorTitulo div').css('backgroundColor', '#' + hex);
                $('#inputColorTitulo').val('#' + hex);
                changeValue('titleColor', '#' + hex);
            }
        });

        $('#inputColorTitulo').on('keyup', function () {
            $('#colorTitulo div').css('backgroundColor', $('#inputColorTitulo').val());
            $('#colorTitulo').ColorPickerSetColor($('#inputColorTitulo').val());
            changeValue('titleColor', $('#inputColorTitulo').val());
        });

        $('#inputColorTitulo').on('change', function () {
            sendChangeColorFont('titleColor', $(this).val());
        });


        /* ---------    DISENIO - MENU      ----------- */
        $('#colorTextoMenu').ColorPicker({
            color: '<?php echo empty($miportal->menuColor) ? $plantilla->menuColor : $miportal->menuColor ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('menuColor', $('#inputColorTextoMenu').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorTextoMenu div').css('backgroundColor', '#' + hex);
                $('#inputColorTextoMenu').val('#' + hex);
                changeValue('menuColor', '#' + hex);
            }
        });

        $('#inputColorTextoMenu').on('keyup', function () {
            $('#colorTextoMenu div').css('backgroundColor', $('#inputColorTextoMenu').val());
            $('#colorTextoMenu').ColorPickerSetColor($('#inputColorTextoMenu').val());
            changeValue('menuColor', $('#inputColorTextoMenu').val());
        });

        $('#inputColorTextoMenu').on('change', function () {
            sendChangeColorFont('menuColor', $(this).val());
        });


        /* COLOR FONDO*/
        $('#colorFondoMenu').ColorPicker({
            color: '<?php echo empty($miportal->menuBackground) ? $plantilla->menuBackground : $miportal->menuBackground ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('menuBackground', $('#inputColorFondoMenu').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorFondoMenu div').css('backgroundColor', '#' + hex);
                $('#inputColorFondoMenu').val('#' + hex);
                changeValue('menuBackgrouncolor', '#' + hex);
            }
        });

        $('#inputColorFondoMenu').on('keyup', function () {
            $('#colorFondoMenu div').css('backgroundColor', $('#inputColorFondoMenu').val());
            $('#colorFondoMenu').ColorPickerSetColor($('#inputColorFondoMenu').val());
            changeValue('menuBackgrouncolor', $('#inputColorFondoMenu').val());
        });

        $('#inputColorFondoMenu').on('change', function () {
            sendChangeColorFont('menuBackground', $(this).val());
        });


        /* ---------    DISENIO - SUBTITULO      ----------- */
        $('#colorSubtitulo').ColorPicker({
            color: '<?php echo empty($miportal->subtitleColor) ? $plantilla->subtitleColor : $miportal->subtitleColor ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('subtitleColor', $('#inputSubtitulo').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorSubtitulo div').css('backgroundColor', '#' + hex);
                $('#inputSubtitulo').val('#' + hex);
                changeValue('titleBody', '#' + hex);
            }
        });

        $('#inputSubtitulo').on('keyup', function () {
            $('#colorSubtitulo div').css('backgroundColor', $('#inputSubtitulo').val());
            $('#colorSubtitulo').ColorPickerSetColor($('#inputSubtitulo').val());
            changeValue('titleBody', $('#inputSubtitulo').val());
        });

        $('#inputSubtitulo').on('change', function () {
            sendChangeColorFont('subtitleColor', $(this).val());
        });


        /* ---------    DISENIO - PARRAFO      ----------- */
        $('#colorParrafo').ColorPicker({
            color: '<?php echo empty($miportal->textColor) ? $plantilla->textColor : $miportal->textColor ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('textColor', $('#inputColorParrafo').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorParrafo div').css('backgroundColor', '#' + hex);
                $('#inputColorParrafo').val('#' + hex);
                changeValue('textColor', '#' + hex);
            }
        });

        $('#inputColorParrafo').on('keyup', function () {
            $('#colorParrafo div').css('backgroundColor', $('#inputColorParrafo').val());
            $('#colorParrafo').ColorPickerSetColor($('#inputColorParrafo').val());
            changeValue('textColor', $('#inputColorParrafo').val());
        });

        $('#inputColorParrafo').on('change', function () {
            sendChangeColorFont('textColor', $(this).val());
        });


        /* COLOR FONDO*/
        $('#colorLink').ColorPicker({
            color: '<?php echo empty($miportal->linkColor) ? $plantilla->linkColor : $miportal->linkColor ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('linkColor', $('#inputColorLink').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorLink div').css('backgroundColor', '#' + hex);
                $('#inputColorLink').val('#' + hex);
                changeValue('linkColor', '#' + hex);
            }
        });

        $('#inputColorLink').on('keyup', function () {
            $('#colorLink div').css('backgroundColor', $('#inputColorLink').val());
            $('#colorLink').ColorPickerSetColor($('#inputColorLink').val());
            changeValue('linkColor', $('#inputColorLink').val());
        });

        $('#inputColorLink').on('change', function () {
            sendChangeColorFont('linkColor', $(this).val());
        });


        /* ---------    DISENIO - FONDO Y DISEÑO      ----------- */
        $('#colorBackground').ColorPicker({
            color: '<?php echo empty($miportal->bodyBackground) ? $plantilla->bodyBackground : $miportal->bodyBackground ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('bodyBackground', $('#inputBackground').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorBackground div').css('backgroundColor', '#' + hex);
                $('#inputBackground').val('#' + hex);
                changeValue('bodyBackground', '#' + hex);
            }
        });

        $('#inputBackground').on('keyup', function () {
            $('#colorBackground div').css('backgroundColor', $('#inputBackground').val());
            $('#colorBackground').ColorPickerSetColor($('#inputBackground').val());
            changeValue('bodyBackground', $('#inputBackground').val());
        });

        $('#inputBackground').on('change', function () {
            sendChangeColorFont('bodyBackground', $(this).val());
        });


        /* COLOR FONDO CUENTA REGRESIVA*/
        $('#colorFondoCuenta').ColorPicker({
            color: '<?php echo empty($miportal->cuentaBackground) ? $plantilla->cuentaBackground : $miportal->cuentaBackground ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('cuentaBackground', $('#inputFondoCuenta').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorFondoCuenta div').css('backgroundColor', '#' + hex);
                $('#inputFondoCuenta').val('#' + hex);
                changeValue('cuentaBackground', '#' + hex);
            }
        });

        $('#inputFondoCuenta').on('keyup', function () {
            $('#colorFondoCuenta div').css('backgroundColor', $('#inputFondoCuenta').val());
            $('#colorFondoCuenta').ColorPickerSetColor($('#inputFondoCuenta').val());
            changeValue('cuentaBackground', $('#inputFondoCuenta').val());
        });

        $('#inputFondoCuenta').on('change', function () {
            sendChangeColorFont('cuentaBackground', $(this).val());
        });


        /* COLOR TEXTO CUENTA REGRESIVA*/
        $('#colorTextoCuenta').ColorPicker({
            color: '<?php echo empty($miportal->cuentaColor) ? $plantilla->cuentaColor : $miportal->cuentaColor ?>',
            livePreview: false,
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                sendChangeColorFont('cuentaColor', $('#inputTextoCuenta').val());
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorTextoCuenta div').css('backgroundColor', '#' + hex);
                $('#inputTextoCuenta').val('#' + hex);
                changeValue('cuentaColor', '#' + hex);
            }
        });

        $('#inputTextoCuenta').on('keyup', function () {
            $('#colorTextoCuenta div').css('backgroundColor', $('#inputTextoCuenta').val());
            $('#colorTextoCuenta').ColorPickerSetColor($('#inputTextoCuenta').val());
            changeValue('cuentaColor', $('#inputTextoCuenta').val());
        });

        $('#inputTextoCuenta').on('change', function () {
            sendChangeColorFont('cuentaColor', $(this).val());
        });


        /* ---------    CONFIGURACION      ----------- */
        $('#tokenfield').tokenfield({
            autocomplete: {
                source: [
                    <?php foreach ($invitados as $key => $value) { ?>
                    '<?php echo $value->correo ?>',
                    <?php } ?>
                ],
                delay: 100
            },
            showAutocompleteOnFocus: true
        });
    });


    function addCSSRule(sheet, selector, rules, index) {
        if ("insertRule" in sheet) {
            sheet.insertRule(selector + "{" + rules + "}", index);
        } else if ("addRule" in sheet) {
            sheet.addRule(selector, rules, index);
        }
    }


    function sendChangeColorFont(campo, valor) {
        var temp_campo = campo.toLowerCase();
        if (temp_campo.indexOf('color') > -1) {
            var isOk = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(valor);
            if (!isOk) {
                console.log('formato incorrecto');
                return false;
            }
        }
        $.ajax({
            url: '<?php echo base_url() ?>index.php/novios/miweb/home/update_miportal',
            method: 'POST',
            timeout: 4000,
            data: {
                tipo: campo,
                valor: valor,
            },
        }).done(function () {
            console.log('success');
        }).fail(function () {
            location.reload();
        });
    }

    function changeValue(campo, valor) {
        $('#' + campo).remove();

        var style = document.createElement('style');
        style.appendChild(document.createTextNode(''));
        style.setAttribute('id', campo);
        document.head.appendChild(style);
        addCSSRule(style.sheet, ':root', "--" + campo + ":" + valor + ";", 0);
    }

    function goToConfiguracion(accion) {
        switch (accion) {
            case 1:
                $('#main-configuracion').show();
                $('#updateContrasena-configuracion').hide();
                $('#titulo_boton').unbind('click');
                $('#titulo_boton').html('<i class="material-icons">home</i>');
                $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;CONFIGURACI&Oacute;N');
                if ($('#estado_anterior').val() == 'inactivo') {
                    $('#contrasena_configuracion').prop('checked', false);
                }
                break;
            case 2:
                /*RESTAURA A VALORES VACIOS*/
                $('#estado_anterior').val('');
                $('#contrasena_miportal').val('');
                $('#repetircontrasena_miportal').val('');
                $('#mensaje_error_configurar').text('');
                $('#aplicar_all').prop('checked', false);
                /*PREPARA VISTA*/
                $('#main-configuracion').hide();
                $('#updateContrasena-configuracion').show();
                $('#titulo_boton').on('click', function () {
                    goToConfiguracion(1);
                });
                $('#titulo_boton').html('<i class="material-icons">keyboard_arrow_left</i>');
                $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;CONFIGURACI&Oacute;N');
                break;
        }
    }

    function back(valor, json, identificador) {
        switch (valor) {
            case 1:
                $('#main').show();
                $('#add_seccion').hide();
                $('#modify_seccion').hide();
                $('#modify_seccionactivas').hide();
                $('#titulo_boton').html('<i class="material-icons">home</i>');
                $('#titulo_boton').unbind('click');
                $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;P&Aacute;GINAS');
                break;
            case 2:
                $('#main').hide();
                $('#modify_seccion').hide();
                $('#modify_seccionactivas').hide();
                $('#add_seccion').show();
                $('#titulo_boton').html('<i class="material-icons">keyboard_arrow_left</i>');
                $('#titulo_boton').on('click', function () {
                    back(1, null);
                });
                $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;NUEVA P&Aacute;GINA');
                break;
            case 3:
                if (json == null) {
                    $.ajax({
                        url: '<?php echo base_url() ?>index.php/novios/miweb/Home/getMiPortalSeccion/' + identificador,
                        method: "GET",
                        async: false,
                        timeout: 4000,
                    }).done(function (datos) {
                        json = datos;
                    });
                } else {
                    json = JSON.parse(json);
                }
                $('#main').hide();
                $('#add_seccion').hide();
                $('#modify_seccionactivas').hide();
                $('#modify_seccion').show();
                $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;NUEVA P&Aacute;GINA');
                $('#titulo_boton').html('<i class="material-icons">keyboard_arrow_left</i>');
                $('#titulo_boton').on('click', function () {
                    back(2, null);
                });
                $('#id_miportal_seccion').val(json.id_miportal_seccion);
                $('#tipo_miportal_seccion').val(json.tipo);
                $('#icono').html('<i class="material-icons ' + json.color + '">' + json.icono + '</i>');
                $('#titulo_seccion').html('<b>' + json.titulo + '</b>');
                switch (json.tipo) {
                    case 'bienvenido':
                        $('#titulo').val(json.subtitulo);
                        break;
                    case 'asistencia':
                        $('#titulo').val(json.subtitulo);
                        break;
                    case 'contactar':
                        $('#titulo').val(json.subtitulo);
                        break;
                    case 'libro':
                        $('#titulo').val(json.subtitulo);
                        break;
                    default:
                        $('#titulo').val(json.titulo);
                        break;
                }
                $('#descripcion').text(json.descripcion);
                $(tinymce.get(0).getBody()).html(json.detalle);
                break;
            case 4:
                var id = json;
                $('#main').hide();
                $('#add_seccion').hide();
                $('#modify_seccion').hide();
                $('#modify_seccionactivas').show();
                $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;EDICI&Oacute;N DE P&Aacute;GINA');
                $('#titulo_boton').html('<i class="material-icons">keyboard_arrow_left</i>');
                $('#titulo_boton').on('click', function () {
                    back(1, null);
                });
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/miweb/Home/getMiPortalSeccion/' + id,
                    method: "GET",
                    timeout: 4000,
                }).done(function (datos) {
                    $('#id_miportal_seccion_modify').val(datos.id_miportal_seccion);
                    $('#titulo_modify').val(datos.titulo);
                    $('#detalle_modify').html(datos.detalle);
                    $('#tipo_modify').val(datos.tipo);
                    if (datos.activo == 0) {
                        $('#eliminar_modify').hide();
                    } else {
                        $('#eliminar_modify').show();
                    }

                    switch (parseInt(datos.visible)) {
                        case 1:
                            $('#radio_publico').prop('checked', true);
                            break;
                        case 2:
                            $('#radio_privado').prop('checked', true);
                            break;
                        case 3:
                            $('#radio_novisible').prop('checked', true);
                            break;
                    }
                    $('#radio_novisible').parent().parent().show();
                    switch (datos.tipo) {
                        case 'bienvenido':
                            $('#nombre_modify').html('Bienvenido');
                            $('#radio_novisible').parent().parent().hide();
                            break;
                        case 'asistencia':
                            $('#nombre_modify').html('Asistencia');
                            break;
                        case 'contactar':
                            $('#nombre_modify').html('Contactar');
                            break;
                        case 'libro':
                            $('#nombre_modify').html('Libro de firmas');
                            break;
                        case 'blog':
                            $('#nombre_modify').html('Blog de boda');
                            break;
                        case 'direccion':
                            $('#nombre_modify').html('Direcciones');
                            break;
                        case 'album':
                            $('#nombre_modify').html('&Aacute;lbumes');
                            break;
                        case 'encuesta':
                            $('#nombre_modify').html('Encuestas');
                            break;
                        case 'test':
                            $('#nombre_modify').html('Tests');
                            break;
                        case 'evento':
                            $('#nombre_modify').html('Eventos');
                            break;
                        case 'video':
                            $('#nombre_modify').html('Videos');
                            break;
                    }
                }).fail(function (error) {
                    location.reload();
                });
                break;
        }
    }

    function goTo(identificador, origen) {
        if ($(origen).attr('class') != 'menu-item active') {
            var divs = $(origen.parentElement).find('.menu-item.active');
            for (var i = 0; i < divs.length; i++) {
                $(divs[i]).attr('class', 'menu-item');
            }
            switch (identificador) {
                case 1:
                    back(1, null);
                    $('#mi_pagina').show();
                    $('#diseno').hide();
                    $('#configuracion').hide();
                    $('#compartir').hide();
                    $('#titulo_boton').html('<i class="material-icons">home</i>');
                    $('#titulo_boton').unbind('click');
                    $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;P&Aacute;GINAS');
                    break;
                case 2:
                    $('#mi_pagina').hide();
                    $('#diseno').show();
                    $('#configuracion').hide();
                    $('#compartir').hide();
                    $('#titulo_boton').html('<i class="material-icons">home</i>');
                    $('#titulo_boton').unbind('click');
                    $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;DISE&Ntilde;O');
                    break;
                case 3:
                    goToConfiguracion(1);
                    $('#mi_pagina').hide();
                    $('#diseno').hide();
                    $('#configuracion').show();
                    $('#compartir').hide();
                    $('#titulo_boton').html('<i class="material-icons">home</i>');
                    $('#titulo_boton').unbind('click');
                    $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;CONFIGURACI&Oacute;N');
                    break;
                case 4:
                    $('#mi_pagina').hide();
                    $('#diseno').hide();
                    $('#configuracion').hide();
                    $('#compartir').show();
                    $('#titulo_boton').html('<i class="material-icons">home</i>');
                    $('#titulo_boton').unbind('click');
                    $('#titulo_cabecera').html('&nbsp;&nbsp;&nbsp;&nbsp;COMPARTIR');
                    break;
            }
            $(origen).attr('class', 'menu-item active');
        }
    }

    function jcropModal(opt) {
        $("#modal-content").html('<img id="modal-content-image" ><img id="preview"  class="hidden"  /><canvas id="canvas"  class="hidden" ></canvas>');
        var img = $("#modal-content-image").get(0);
        img.src = opt.img;
        $("#modal-imagen-title").html(opt.titulo);
        if (img.width >= opt.minWidth && img.height >= opt.minHeight) {
            img.onload = function () {
                $("#modal-imagen").modal("open");
                $("#modal-imagen-aceptar").off("click");
                $("#modal-imagen-cancelar").click(function () {
                    opt.error();
                });
                $("#modal-imagen-aceptar").on("click", function () {
                    var img = $("#preview").get(0);
                    $("#modal-imagen").modal("close");
                    var result = img.src;
                    opt.success(result);
                });
                var dim = [img.width, img.height];
                img.width = $("div#modal-content").width();
                $('#modal-content-image').Jcrop({
                    trueSize: dim,
                    bgColor: 'black',
                    bgOpacity: .4,
                    setSelect: [10, 10, 0, 0],
                    aspectRatio: opt.minWidth / opt.minHeight,
                    minSize: [opt.minWidth, opt.minHeight],
                    onChange: showPreview,
                    onSelect: showPreview
                });
            };
        } else {
            opt.error();
        }

        function showPreview(coords) {
            var canvas = document.getElementById("canvas");
            var ctx = canvas.getContext("2d");
            if (ctx) {
                var img = $('#modal-content-image').get(0);
                ctx.drawImage(img, coords.x, coords.y, coords.w, coords.h, 0, 0, coords.w, coords.h);
                $("#preview").get(0).src = (canvas.toDataURL("image/png"));
                canvas.width = coords.w;
                canvas.height = coords.h;
            }
        }
    }

    function openFile(event, origen) {
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function (e) {
            var width = 0;
            var height = 0;
            switch (origen) {
                case 'header':
                    width = 475;
                    height = 125;
                    break;
                case 'welcome':
                    width = 300;
                    height = 150;
                    break;
            }

            var img = document.createElement("img");
            img.src = e.target.result;
            if (img.width < width || img.height < height) {
                alert('La imagen debe tener un ancho m&iacute;nimo de 475 px, y una altura m&iacute;nima de 125 px.');
            } else {
                jcropModal({
                    img: (img.src),
                    minWidth: width,
                    minHeight: height,
                    title: 'Selecciona lo que se ver&aacute;',
                    success: function (result) {
                        switch (origen) {
                            case 'header':
                                console.log('idmiporal  <?php echo $miportal->id_miportal ?>');
                                $.ajax({
                                    url: '<?php echo base_url() ?>index.php/novios/miweb/home/upload_img',
                                    method: 'POST',
                                    timeout: 4000,
                                    data: {
                                        base64: result,
                                        miportal: '<?php echo $miportal->id_miportal ?>',
                                    },
                                }).done(function () {
                                    console.log('success');
                                    changeValue('imgBackground', 'url(' + result + ')');
                                }).fail(function () {
                                    location.reload();
                                });
                                break;
                            case 'welcome':
                                $('[name="image-bienvenido"]').val(result);
                                $('.responsive-img.image-bienvenido').attr('src', result);
                                $('#image-deletebienvenido').show();
                                break;
                        }
                    },
                    error: function () {
                    }
                });
            }
        };
        reader.readAsDataURL(input.files[0]);
    }

    function copyToClipboard(valor) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(valor).select();
        document.execCommand("copy");
        $temp.remove();
        Materialize.toast('Texto copiado', 4000);
    }

    var DateDiff = {
        inDays: function (d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();

            return parseInt((t2 - t1) / (24 * 3600 * 1000));
        },
        inWeeks: function (d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();

            return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7));
        },
        inMonths: function (d1, d2) {
            var d1Y = d1.getFullYear();
            var d2Y = d2.getFullYear();
            var d1M = d1.getMonth();
            var d2M = d2.getMonth();

            return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
        },
        inYears: function (d1, d2) {
            return d2.getFullYear() - d1.getFullYear();
        }
    }

    function FormatDate(date) {
        var _date = new Date(date);
        var _meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        var _days = new Array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");
        this.DD_dd_MM_YYYY = function () {
            return _days[_date.getDay()] + " " + _date.getDate() + " de " + _meses[_date.getMonth()] + " de " + _date.getFullYear();
        };
        this.dd_MM_YYYY = function () {
            return _date.getDate() + " " + _meses[_date.getMonth()] + " " + _date.getFullYear();
        };
    }

</script>
</html>