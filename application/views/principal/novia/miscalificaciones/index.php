<!DOCTYPE html>
<html>
    <head>
        <title>
            Mis vestidos
        </title>
        <?php $this->view("principal/head");
        $this->view('general/newheader');?>
        <style>
            #total-proveedores i{
                font-size: 2em;
                padding: 10px;

                border-radius: 50%;
                margin-bottom: 6px;
            }
            #total-proveedores i.dorado-2-text{
                border: 1px solid #e91e63;
            }
            #total-proveedores i.green-text{
                border: 1px solid #4caf50;
            }
            #total-proveedores i.orange-text{
                border: 1px solid #ff9800;
            }
            .card-image .row, .card-image .col.s6{
                margin: 0px;padding: 2px
            }
            .card-image .img{
                width: 100%; height: 255px;background-color: lightgray;
                background-size: cover;
                border: 1px solid #e2e2e2;

            }
            .card-image .row > .img{
                height: 125px;
            }
        </style>
        <link href="<?php echo base_url() ?>dist/css/iconos-proveedor.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php $this->view("general/menu") ?>
        <?php $this->view("principal/novia/menu") ?>
        <div class="body-container">
            <div class="row">
                <h5 style="margin-left:5%; ">Mis calificaciones</h5>
                <div class="divider"></div>
                <br>
                <div class=" col s12 m8 offset-m2 l9">
                    <div class="row">
                        <div class="row">
                            <?php if ($calificaciones) { ?>
                                <?php foreach ($calificaciones as $key => $r) { ?>
                                    <div class="card-panel z-depth-1" id="<?php echo $r->id_boda ?>">
                                        <div class="row">
                                            <div class="col" style="width: 130px">
                                                <img src="<?php echo base_url() ?>perfil/foto/<?php echo $r->id_usuario ?>" style="width: 130px;height: 130px" class=" circle">
                                            </div>
                                            <div class="col" style="width:  calc( 100% - 130px )">
                                                <div class="row ">
                                                    <b><?php echo $r->nombre ?></b>
                                                    <small>
                                                        <?php echo relativeTimeFormat($r->fecha_recomendacion) ?>
                                                    </small>
                                                    <br>
                                                </div>
                                                <div class="row">
                                                    <div class="col s6" >
                                                        <div class="row"> 
                                                            <div class="col s12   m6 text-recomendacion ">
                                                                Calidad del servicio:
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress dorado-2">
                                                                    <div class="determinate dorado-1" style="width: <?php echo ($r->calidad_servicio * 20) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col s12   m6 text-recomendacion ">
                                                                Respuesta:
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress dorado-2">
                                                                    <div class="determinate dorado-1" style="width: <?php echo ($r->respuesta * 20) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col s12   m6 text-recomendacion ">
                                                                Relaci&oacute;n calidad/precio:
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress dorado-2">
                                                                    <div class="determinate dorado-1" style="width: <?php echo ($r->relacion_calidad * 20) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col s6">
                                                        <div class="row">
                                                            <div class="col s12 m6 text-recomendacion ">
                                                                Flexibilidad:
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress dorado-2">
                                                                    <div class="determinate dorado-1" style="width: <?php echo ($r->flexibilidad * 20) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col s12 m6 text-recomendacion ">
                                                                Profesionalismo:
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress dorado-2">
                                                                    <div class="determinate dorado-1" style="width: <?php echo ($r->profesionalismo * 20) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="divider"></div>
                                            <div class="col s12 text-justify">
                                                <?php echo $r->resena ?>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="col s12">
                                                <p>
                                                    <a class=" btn-flat waves-effect waves-blue pull-right btn-share-link" data-href="<?php echo site_url("escaparate/recomendaciones/$r->id_proveedor/$r->id_boda") ?>" data-quote="<?php echo substr($r->resena, 0, 150) ?>...">
                                                        <i class="fa fa-facebook-official"> </i> Compartir 
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="card-panel">
                                    <p class="center">
                                        <i class="fa fa-info fa-4x "></i>
                                        <br>
                                        <b>¡A&uacute;n no haz calificado a un proveedor!</b> .
                                        <br>
                                        <br>
                                        <a href="<?php echo site_url("novios/proveedor") ?>" class="btn dorado-2">
                                            ir a proveedores
                                        </a>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div  class="row">
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view('principal/footer'); ?>
        <?php $this->view("principal/foot") ?>
        <script type="text/javascript" src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js"></script>
        <script src="<?php echo base_url() ?>dist/js/tendencia/vestidos.js" type="text/javascript"></script>
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function () {
                var fb=new FBclub();
                fb.initShareLinks();
            });

        </script>
    </body>
</html>



