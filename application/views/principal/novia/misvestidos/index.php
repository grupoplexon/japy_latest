<!DOCTYPE html>
<html>
    <head>
        <title>
            Mis vestidos
        </title>
        <?php $this->view("principal/head") ?>
        <style>
            #total-proveedores i{
                font-size: 2em;
                padding: 10px;

                border-radius: 50%;
                margin-bottom: 6px;
            }
            #total-proveedores i.dorado-2-text{
                border: 1px solid #e91e63;
            }
            #total-proveedores i.green-text{
                border: 1px solid #4caf50;
            }
            #total-proveedores i.orange-text{
                border: 1px solid #ff9800;
            }
            .card-image .row, .card-image .col.s6{
                margin: 0px;padding: 2px
            }
            .card-image .img{
                width: 100%; height: 255px;background-color: lightgray;
                background-size: cover;
                border: 1px solid #e2e2e2;

            }
            .card-image .row > .img{
                height: 125px;
            }
        </style>
        <link href="<?php echo base_url() ?>dist/css/iconos-proveedor.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/iconos-vestidos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php $this->view("principal/menu") ?>
        <?php $this->view("principal/novia/menu") ?>
        <div class="body-container">
            <div class="row">
                <h5>Mis vestidos favoritos</h5>
                <div class="divider"></div>
                <br>
                <div class="col s12 m4 l3 right">
                    <h6>Mis favoritos</h6>
                    <div class="collection z-depth-0">
                        <?php foreach ($grupo as $key => $g) { ?>
                            <a  href="<?php echo site_url("novios/misvestidos/tipo/" . str_replace(" ", "-", $g->nombre)) ?>" class="collection-item avatar">
                                <div class="circle " style="overflow: initial;">
                                    <i class=" vestidos x2 vestidos-<?php echo strtolower(str_replace(" ", "-", $g->nombre)) ?>"></i>
                                </div>
                                <span class="title black-text "><?php echo $g->total ?></span>
                                <p class="gris-2-text"><?php echo $g->nombre ?><br>
                                </p>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="card">
                        <div class="card-image">

                        </div>
                        <div class="card-content center">
                            <p>
                                <b>¿Necesitas inspiraci&oacute;n?</b><br>
                                Encuentra tu diseño ideal entre nuestro catalogo de vestidos.<br>
                            </p><br>
                            <a href="<?php echo site_url("tendencia") ?>" class="btn dorado-2">
                                ir al cat&aacute;logo
                            </a>
                        </div>
                    </div>

                </div>
                <div class=" col s12 m8 l9">
                    <div class="row">
                        <div class="row">
                            <?php if ($grupo) { ?>
                                <?php foreach ($grupo as $key => $value) { ?>
                                    <div class="col s12 m6">
                                        <a href="<?php echo site_url("novios/misvestidos/tipo/" . str_replace(" ", "-", $value->nombre)) ?>">
                                            <div class="card">
                                                <div class="card-image">
                                                    <div class="row">
                                                        <div class="col s6" >
                                                            <div class="img" style="
                                                                 background-image:  url(<?php echo "data:" . $value->vestidos[0]->mime_imagen . ";base64," . base64_encode($value->vestidos[0]->imagen) ?>)
                                                                 ">
                                                            </div>
                                                        </div>
                                                        <div class="col s6">
                                                            <div class="row">
                                                                <?php if (count($value->vestidos) >= 2) { ?>
                                                                    <div class="img" style="
                                                                         background-image: url(<?php echo "data:" . $value->vestidos[1]->mime_imagen . ";base64," . base64_encode($value->vestidos[1]->imagen) ?>)
                                                                         " >
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <div class="img" >
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="row" >
                                                                <?php if (count($value->vestidos) >= 3) { ?>
                                                                    <div class="img" style="
                                                                         background-image: url(<?php echo "data:" . $value->vestidos[2]->mime_imagen . ";base64," . base64_encode($value->vestidos[2]->imagen) ?>)
                                                                         " >
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <div class="img" >
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class = "card-content center">
                                                    <h6>
                                                        <b class="black-text"><?php echo $value->nombre ?></b><br>
                                                        <small class="gris-1-text"><?php echo $value->total ?> guardados</small>
                                                    </h6>
                                                </div>
                                            </div>

                                        </a>


                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="card-panel">
                                    <p class="center">
                                        <i class="fa fa-info fa-4x "></i>
                                        <br>
                                        <b>¿Necesitas inspiraci&oacute;n?</b> Encuentra tu diseño ideal entre nuestro catalogo de vestidos.
                                        <br>
                                        <br>
                                        <a href="<?php echo site_url("tendencia") ?>" class="btn dorado-2">
                                            ir al cat&aacute;logo
                                        </a>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div  class="row">
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("principal/view_footer") ?>
        <?php $this->view("principal/foot") ?>
        <script type="text/javascript" src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js"></script>
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function () {

            });

        </script>
    </body>
</html>



