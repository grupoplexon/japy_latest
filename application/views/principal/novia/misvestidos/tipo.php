<!DOCTYPE html>
<html>
    <head>
        <title>
            Mis vestidos
        </title>
        <?php $this->view("principal/head") ?>
        <style>
            #total-proveedores i{
                font-size: 2em;
                padding: 10px;

                border-radius: 50%;
                margin-bottom: 6px;
            }
            #total-proveedores i.dorado-2-text{
                border: 1px solid #e91e63;
            }
            #total-proveedores i.green-text{
                border: 1px solid #4caf50;
            }
            #total-proveedores i.orange-text{
                border: 1px solid #ff9800;
            }
            .card-image .row, .card-image .col.s6{
                margin: 0px;padding: 2px
            }
            .card-image .img{
                width: 100%; height: 255px;background-color: lightgray;
                background-size: cover;
                border: 1px solid #e2e2e2;

            }
            .card-image .row > .img{
                height: 125px;
            }
        </style>
        <link href="<?php echo base_url() ?>dist/css/iconos-proveedor.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/iconos-vestidos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php $this->view("principal/menu") ?>
        <?php $this->view("principal/novia/menu") ?>
        <div class="body-container">
            <div class="row">
                <div class="row">
                    <a href="<?php echo site_url("novios/misvestidos") ?>" class="dorado-2-text btn-flat white left waves-effect " style="margin-right: 20px; font-size: 11px;">
                        Ver todos <i class="material-icons left">keyboard_arrow_left</i>
                    </a>
                    <h5 class="left">Mis vestidos favoritos</h5>
                </div>
                <div class="divider"></div>
                <br>
                <div class="col s12 m4 l3 right">
                    <h6>Mis favoritos</h6>
                    <div class="collection z-depth-0">
                        <?php foreach ($grupo as $key => $g) { ?>
                            <a  href="<?php echo site_url("novios/misvestidos/tipo/" . str_replace(" ", "-", $g->nombre)) ?>" class="collection-item avatar">
                                <div class="circle " style="overflow: initial;">
                                    <i class=" vestidos x2 vestidos-<?php echo strtolower(str_replace(" ", "-", $g->nombre)) ?>"></i>
                                </div>
                                <span class="title black-text "><?php echo $g->total ?></span>
                                <p class="gris-2-text"><?php echo $g->nombre ?><br>
                                </p>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="card">
                        <div class="card-image">

                        </div>
                        <div class="card-content center">
                            <p>
                                <b>¿Necesitas inspiraci&oacute;n?</b><br>
                                Encuentra tu diseño ideal entre nuestro catalogo de vestidos.<br>
                            </p><br>
                            <a href="<?php echo site_url("tendencia") ?>" class="btn dorado-2">
                                ir al cat&aacute;logo
                            </a>
                        </div>
                    </div>

                </div>
                <div class=" col s12 m8 l9">
                    <div class="row">
                        <div class="row">
                            <?php foreach ($vestidos as $key => $c) { ?>
                                <div class="col m20-porcent s12 ">
                                    <div class="card" id="<?php echo $c->id_vestido ?>">
                                        <div class="card-image waves-effect waves-block waves-light">
                                            <a href="<?php echo site_url("tendencia/articulo/" . str_replace(" ", "-", $c->disenador) . "/" . str_replace(" ", "-", $c->nombre)) ?>">
                                                <img src="data:<?php echo $c->mime_imagen ?>;base64,<?php echo base64_encode($c->imagen) ?>">
                                                <div class="social dorado-1-text">
                                                    <span>
                                                        <i class="material-icons">favorite</i> <?php echo $c->likes ?>
                                                    </span>
                                                    <span>
                                                        <i class="material-icons">chat_bubble</i> <?php echo $c->comments ?>
                                                    </span>
                                                </div>
                                            </a>
                                            <i class="material-icons  tooltipped favorite vestido red-text darken-5"
                                               data-vestido="<?php echo $c->id_vestido ?>" data-position="top" data-delay="5"
                                               data-tooltip="<?php echo $c->like ? "Ya no me gusta" : "Me gusta" ?>"><?php echo $c->like ? "favorite" : "favorite_border" ?></i>
                                        </div>
                                        <div class="card-action">
                                            <a href="<?php echo site_url("tendencia/articulo/" . str_replace(" ", "-", $c->disenador) . "/" . str_replace(" ", "-", $c->nombre)) ?>" class="black-text darken-5">
                                                <?php echo $c->disenador ?> (<?php echo $c->temporada ?>)
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("principal/view_footer") ?>
        <?php $this->view("principal/foot") ?>
        <script type="text/javascript" src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js"></script>
        <script src="<?php echo base_url() ?>dist/js/tendencia/vestidos.js" type="text/javascript"></script>
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function () {

                $(".favorite.vestido").on("click", function () {
                    var id = $(this).data("vestido");
                    vestidos.server = "<?php echo base_url() ?>";
                    vestidos.like(id, function () {
                        $("#" + id).parent().css({
                            transition: "1s",
                            opacity: 0,
                            width: "0px",
                        });
                        setTimeout(function () {
                            $("#" + id).parent().remove();
                        }, 1000);
                    });
                });


            });

        </script>
    </body>
</html>



