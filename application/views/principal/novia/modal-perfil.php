<style>
    #modal-perfil {
        width: 400px !important;
        background-color: #00bcdd;
        border-radius: 10px !important;
        height: 68%;
    }

    #modal-perfil input[type="text"] {
        background: white !important;
        border-radius: 5px !important;
        border: 1px solid #3f4ca7 !important;
        padding: 5px 10px !important;
    }

    #modal-perfil .btn {
        width: 100% !important;
        background: white !important;
        color: #00bbcd !important;
    }

    .modal-content {
        max-height: 670px !important;
    }

    .style-form {
        display: flex;
        justify-content: center;
        margin: 0 !important;
    }

    .style-form i {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #modal-perfil .row p, #modal-perfil .row i {
        color: white;
    }

    @media only screen and (max-width: 320px) {
        #modal-perfil {
            width: 80% !important;
            height: 90% !important;
            max-height: 90% !important;
            top: 5% !important;
        }
    }

    @media only screen and (min-width: 322px) and (max-width: 425px) {
        #modal-perfil {
            width: 80% !important;
            height: 100% !important;
            top: 8% !important;
            max-height: 80% !important;
        }
    }

    @media only screen and (min-width: 768px) {

    }
</style>
<div id="modal-perfil" class="modal">
    <div class="modal-content">
        <div class="row">
            <div class="col s8 m8 offset-m2 offset-s2">
                <img style="margin-top: 15px;" class="responsive-img"
                     src="<?php echo base_url() ?>/dist/img/japy_nobg_white.png"
                     alt="Japy"/>
            </div>
        </div>
        <form id="form-perfil" action="<?php echo base_url() ?>Novia/firstDate" method="POST"
              data-parsley-validate="true" style="margin:0 ">

            <div class="row style-form">
                <p class="col s10 ">Nombre de tú pareja</p>
                <i class="col s2  material-icons">account_circle</i>
            </div>
            <div class="row">
                <input class="col s12 " name="nombre" placeholder="Nombre(s)" minlength="4" maxlength="30" type="text"
                       style="margin-top:10px; margin-bottom: 0px;" required="">
                <input class="col s12 " name="apellido" placeholder="Apellido(s)" minlength="4" maxlength="30"
                       type="text" style="margin-top:10px; margin-bottom: 0px;" required="">
            </div>
            <div class="row style-form">
                <p class="col s10 ">Fecha de la boda</p>
                <i class="col s2  material-icons">date_range</i>
            </div>
            <div class="row">
                <input type="date" class="datepicker col s12  " name="fecha"
                       placeholder="Fecha del evento:" required=""
                       style="background-color: white; text-indent:15px;font-size: 12px; border: 1px solid #f2f2f2;margin-top:10px;margin-bottom: 0">
                <div class="col s12 m10 offset-m1  center-align" style="margin-top:10px;">
                    <button id="btn-modal" class="btn waves-effect waves-dark" type="submit" name="action">Añadir
                        <i class="material-icons ">add_box</i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 15,
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Aceptar',
            format: 'yyyy-mm-dd',
            formatSubmit: 'yyyy-mm-dd',
            min: new Date()
        });

        $('#form-perfil').parsley();

        let days = $(".ndias").text();
        let screen_profile = $(".perfil-screen");
        let modal_profile = $("#opener_modal_perfil");

        if (days <= 0) {

            setTimeout(function () {
                $('#indicador-2').modal('open');
            },300);
            $('#indicador-22').on('click',function () {
                $('#indicador-2').modal('close');
            });

            screen_profile.addClass("block-screen");
            modal_profile.addClass("cover-screen");

            $('#form-perfil').on('submit', function (e) {
                e.preventDefault();
                $('#btn-modal').css('display', 'none');
                var formData = $('#form-perfil').serialize();


                $.ajax({
                    url: $('#form-perfil').attr('action'),
                    method: 'post',
                    data: formData,
                    success: function (res) {
                        swal({
                            title: "Éxito",
                            text: "Fueron guardados con éxito los datos!!",
                            icon: "success",
                        });

                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    },
                    error: function () {
                        swal({
                            title: "Error",
                            text: "No pudimos establecer tú fecha y pareja",
                            icon: "error",
                        });
                        $('#btn-modal').css('display', 'block');
                    }
                });
            });

        } else {
            screen_profile.removeClass("block-screen");
            modal_profile.removeClass("cover-screen");
        }


    });
</script>