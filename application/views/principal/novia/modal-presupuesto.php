<style>
    #modal-presupuesto {
        width: 400px !important;
        background-color: #00bcdd;
        border-radius: 10px !important;
    }

    #modal-presupuesto input[type="text"] {
        background: white !important;
        border-radius: 5px !important;
        border: 1px solid #3f4ca7 !important;
        padding: 5px 10px !important;
    }

    .modal .btn {
        width: 100% !important;
        background: white !important;
        color: #00bbcd !important;
    }

    .style-form {
        display: flex;
        justify-content: center;
        margin: 0 !important;
    }

    .style-form i {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #modal-presupuesto .row p, #modal-presupuesto .row i {
        color: white;
    }

    @media only screen and (max-width: 320px) {
        #modal-presupuesto {
            width: 80% !important;
            max-height: 80% !important;
        }
    }

    @media only screen and (max-width: 425px) {
        #modal-presupuesto {
            width: 80% !important;
            max-height: 80% !important;
        }
    }

    @media only screen and (min-width: 768px) {

    }
</style>

<div id="modal-presupuesto" class="modal">
    <div class="modal-content">
        <div class="row">
            <div class="col s8 m8 offset-m2 offset-s2">
                <img style="margin-top: 15px;" class="responsive-img"
                     src="<?php echo base_url() ?>/dist/img/japy_nobg_white.png"
                     alt="Japy"/>
            </div>
        </div>
        <form id="form-presupuesto" action="<?php echo base_url() ?>novios/presupuesto/Home/firstBudget" method="POST"
              data-parsley-validate="true">
            <div class="row style-form">
                <p class="col s10 ">Presupuesto</p>
                <i class="col s2 material-icons">monetization_on</i>
            </div>
            <div class="row" style="margin: 0">
                <input class="col s12" id="first-budget" style="margin: 0;" name="presupuesto" type="text">
            </div>
            <div class="row style-form">
                <p class="col s10 ">Invitados</p>
                <i class="col s2 material-icons">group</i>
            </div>
            <div class="row">
                <input class="col s12" name="invitados" maxlength="4" data-parsley-type="number" min="1" type="text"
                       style="margin-bottom: 0;text-align: right;" id="ninvited" required="">
            </div>
            <div class="row">
                <div class="col s10 offset-s1">
                    <button id="btn-modal" class="btn waves-effect waves-dark" type="submit" name="action">Añadir
                        <i class="material-icons right">add_box</i>
                    </button>

                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(() => {

        $('#form-presupuesto').parsley();

        let budget = $('.budget-money').text();
        let screen_budget = $('.presupuesto-screen');
        let modal_budget = $('#opener_modal_pres');

        if (budget <= 0) {
            setTimeout(function () {
                $('#indicador-1').modal('open');
            },300);
            $('#indicador-11').on('click',function () {
                $('#indicador-1').modal('close');
            });
            screen_budget.addClass('block-screen');
            modal_budget.addClass('cover-screen');

            $('#form-presupuesto').on('submit', function (e) {
                e.preventDefault();
                $('#btn-modal').css('display', 'none');
                let formData = $('#form-presupuesto').serialize();
                let data;
                data = {
                    budget: $firstBudget.inputmask("unmaskedvalue"),
                    invited: $("#form-presupuesto").find("#ninvited").val(),
                };

                $.ajax({
                    url: $('#form-presupuesto').attr('action'),
                    method: 'post',
                    data,
                    success: function (res) {
                        swal({
                            title: "Actualizado",
                            text: "Se asigno tu presupuesto e invitados",
                            icon: "success",
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    },
                    error: function () {
                        swal({
                            title: "Error",
                            text: "No fue posible asignar tu presupuesto e invitados",
                            icon: "error",
                        });
                        $('#btn-modal').css('display', 'blocl');
                    }
                });
            });
        } else {
            screen_budget.removeClass('block-screen');
            modal_budget.removeClass('cover-screen');
        }


    });

</script>