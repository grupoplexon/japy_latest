<style>
    #modal-ubicacion {
        width: 400px !important;
        background-color: #00bcdd;
        border-radius: 10px !important;
    }

    #modal-ubicacion select {
        background: white !important;
        border-radius: 5px !important;
        border: 1px solid #3f4ca7 !important;
        padding: 5px 10px !important;
    }

    #modal-ubicacion .btn {
        width: 100% !important;
        background: white !important;
        color: #00bbcd !important;
    }

    .style-form {
        display: flex;
        justify-content: center;
        margin: 0 !important;
    }

    .style-form h3 {
        font-size: 22px;
        color: white;
    }

    .style-form i {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #modal-ubicacion .row p, #modal-ubicacion .row i {
        color: white;
    }

    @media only screen and (max-width: 320px) {
        #modal-ubicacion {
            width: 80% !important;
            max-height: 80% !important;
        }
    }

    @media only screen and (max-width: 425px) {
        #modal-ubicacion {
            width: 80% !important;
            max-height: 80% !important;
        }
    }

    @media only screen and (min-width: 768px) {

    }


</style>
<div id="modal-ubicacion" class="modal">
    <div class="modal-content">
        <div class="row">
            <div class="col s10 m8 offset-m2 offset-s1">
                <img style="margin-top: 15px;" class="responsive-img"
                     src="<?php echo base_url() ?>/dist/img/japy_nobg_white.png"
                     alt="Japy"/>
            </div>
        </div>
        <form id="form-ubicacion" action="<?php echo base_url() ?>proveedores/firstLocation" method="POST" data-parsley-validate="true">
            <div class="row style-form">
                <h3 class="col s10">Ubicación</h3>
                <i class="col s2 material-icons">location_city</i>
            </div>
            <div class="row ">
                <select name="country" class="col s12 browser-default countries" id="countryId"
                        style="width: 100%; margin-top:10px; margin-bottom: 0px;"  required="">
                    <option value="" disabled selected>-- Pa&iacute;s --</option>
                </select>
                <select name="state" class="col s12 browser-default states" id="stateId"
                        style="width: 100%; margin-top:10px; margin-bottom: 0px;"  required="">
                    <option value="" disabled selected>-- Estado --</option>
                </select>
                <select name="city" id="cityId" class="col s12 browser-default cities"
                        style="width: 100%; margin-top:10px; margin-bottom: 0px;" >
                    <option value="" disabled selected>-- Poblaci&oacute;n --</option>
                </select>
            </div>

            <div class="row">
                <div class="col s12 m10 offset-m1">
                    <button class="btn waves-effect waves-dark" type="submit" name="action">Añadir
                        <i class="material-icons right">add_box</i>
                    </button>

                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#form-ubicacion').parsley();

        $('#form-ubicacion').on('submit', function (e) {
            e.preventDefault();
            $('#btn-modal').css('display', 'none');
            var formData = $('#form-ubicacion').serialize();

            $.ajax({
                url: $('#form-ubicacion').attr('action'),
                method: 'post',
                data: formData,
                success: function (res) {
                    swal("Éxito", "Tú dirección se guardo !!", "success");

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                },
                error: function () {
                    swal("Error", "No pudimos guardar tu dirección", "error");

                    $('#btn-modal').css('display', 'block');
                }
            });
        });


    });


</script>