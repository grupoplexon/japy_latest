<?php $this->view('principal/header');?>
<style>
    .imagen_grupos{
        width: 40px;
        height: 40px;
        margin-left: 15px;
        margin-right: 5px;
    }
    .listaG{
/*        width: 355px;*/
        height: 50px;
        border: 1px solid #999;
        margin: -1px;
        display: inline-block;
        float: left;
    }
    .nombre_grupo{
        position: relative;
        margin-left: 65px;
        margin-top: -25px;
        text-decoration: none;
        color: #f9a797!important;
    }
    .listaG2{
        border: 1px solid #999;
        margin: -1px;
        height: 50px;
    }
    #mas_grupos{ 
        padding-left: 30px;
        /*width: 708px;*/
    }
    .titulo_foto, .titulo_video, .titulo_tarjeta{
        text-decoration: none; 
        color: #f9a797!important;
        z-index: 200;
    }
    .link_usuario{
        text-decoration: none;
        color: #75767a!important;
        z-index: 200;
    }
    .video_zoom:hover, .foto_zoom:hover{
        transform : scale(1.3);
        -moz-transform : scale(1.3); /* Firefox */
        -webkit-transform : scale(1.3); /* Chrome - Safari */
        -o-transform : scale(1.3); /* Opera */
        -ms-transform : scale(1.3); /* IE9 */
    }
    .coincidencia{
        font-weight: bold;
    }
    .oculto{
        display: none;
    }
    .foto_zoom, .video_zoom{
        height: 200px;
        width: 100%;
    }
    #ventana_buscador{
        margin: 0;
        background: white;
        display: none;
        position: absolute;
        z-index: 100;
        width: 100%;
        max-height: 320px;
        overflow-y: scroll;
    }
    .titulo_debate{
        text-decoration: none; 
        color: #f9a797!important;
        padding-left: 10px;
    }
    .fecha_debate{
        text-decoration: none; 
        color: #75767a!important;
        padding-left: 10px;
    }
	#ventana_buscador{
        top: 45px;
        background: white;
        display: none;
        position: absolute;
        z-index: 100;
        width: 100%;
        max-height: 254px;
        overflow-y: scroll;
        border: 1px solid #ccc;
    }
    #ventana_buscador ul li{
        border-bottom: 1px solid #999;
    }
    #ventana_buscador ul li:hover{
        background: #EBEBEB;
        cursor: hand;
    }
    #ventana_buscador ul li:focus{
        background: #EBEBEB;
    }
    .hover{
        background: #EBEBEB;
    }
	.titulo_debate{
        text-decoration: none; 
        color: #f9a797!important;
        padding-left: 10px;
    }
	.fecha_debate{
        text-decoration: none; 
        color: #75767a!important;
        padding-left: 10px;
    }
	p img{
		max-width:100%;
		height:auto;
	}
</style>
<body>
    <!--BARRA DE TITULO CON UN BUSCADOR DE DEBATES-->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
            <p>
            <div class="col s12 m2">
                <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            </div>
            <form id="buscador" method="get" action="<?php echo base_url()."index.php/novios/moderador/forum/buscar"?>" autocomplete="off">
                <div class="input-field col s7 m5" style="margin:0">
                    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
                    <input type="hidden" name="page" value="1"/>
                    <div id="ventana_buscador">
                    </div>
                </div>
                <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
                    <button type="submit" class="btn waves-effect waves-light dorado-2"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                </div>
            </form>
        </p>
        </div>
    </div>
    
    <!--TARJETA CON UN BOTON DICIENDO QUE PUEDES CREAR UN NUEVO DEBATE EN LA COMUNIDAD-->
    <div class="body-container">
        <div class="row">
            
            <!--COMPONENTES LATERALES-->
            <div class="col s12 m3 lateral" style="float: right; margin-bottom: 20px">
                <!------------------------ IMAGEN DE PUBLICIDAD --------------------->
                <div class="row">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?php echo base_url() ?>dist/images/comunidad/organizador-boda2.jpg">
                        </div>
                        <div class="card-content" style="text-align: center">
                            <p style="font-size: 16px; padding: 10px;margin-bottom: 10px; line-height: 20px" class="grey-text">Descubre lo f&aacute;cil y r&aacute;pido que es organizar tu boda en clubnupcail.com</p>
                            
                        </div>
                    </div>
                </div>
                
                <?php if(!empty($usuarios_boda->novios)){ ?>
                <!--FECHA DE MI BODA CON LOS USUARIOS QUE COINCIDEN-->
                <div class="row">
                    <div class="texto_lateral grey lighten-5" style="height: auto; border: 1px solid #999; text-align: center">
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->fecha_boda)? $usuarios_boda->fecha_boda : "" ?></p>
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->estado_boda)? $usuarios_boda->estado_boda : "" ?></p>
                    </div>
                    <div  class="texto_lateral dorado-2" style="height: 70px; border: 1px solid #999; margin-top: -1px; text-align: center">
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->novios)? $usuarios_boda->novios : ""; 
                        if($usuarios_boda->novios > 1) echo ' novios se casan el mismo dia que tu'; else echo ' novio se casa el mismo dia que tu'?> </p>
                    </div>
                    <div class="texto_lateral grey lighten-5 col s12 m12" style="height: auto; border: 1px solid #999;margin-top: -1px; text-align: center;">
                        <div class="col s4 m4">
                            <div class="card">
                                <div class="card-image">
                                    <img style="width: 100%" src="<?php echo !empty($usuarios_boda->foto_usuario)? $usuarios_boda->foto_usuario : "" ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m12">
                            <a href="<?php echo base_url() ?>index.php/novios/moderador/Home/conocerCompaneros" class="btn dorado-2 truncate" style="margin-top: 10px; margin-bottom: 10px"> Conocelos </a>
                        </div>    
                    </div>
                </div>
                <?php } ?>
                
                <?php if(!empty($visitas_perfil)){ ?>
                <!--QUIEN VISITA MI PERFIL, CON LOS USUARIOS QUE LO HACEN-->
                <div class="row">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="visita_perfil dorado-2">
                        <h5> ¿Quien Visita Tu Perfil? </h5>
                    </div>
                    <div style="height: 120px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($visitas_perfil as $visita){
                        ?>
                        <a href="<?php echo !empty($visita->url_usuario)? $visita->url_usuario : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($visita->foto_usuario)? $visita->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($visita->todos)? $visita->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
                
                <?php if(!empty($grupos_miembro)){ ?>
                <!--MIS GRUPOS-->
                <div class="row">
                    <div class="mis_grupos dorado-2" style="height: auto; padding: 5px; border: 1px solid #999">
                        <h5> Mis Grupos </h5>
                    </div>
                    <div class="mis_grupos grey lighten-5" style="height: auto; border: 1px solid #999; border-top: none">
                        <?php foreach ($grupos_miembro as $grupo){ ?>
                        <a href="<?php echo base_url()."index.php/novios/moderador/group/grupo/$grupo->id_grupos_comunidad/todo"?>">
                        <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                        <p class="nombre_grupo truncate"> <?php echo !empty($grupo->nombre)? $grupo->nombre : "" ?> </p>
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <?php 
                } 
                ?>
                
                <?php if(!empty($grupos)){ ?>
                <!--GRUPOS GENERALES-->
                <div class="row">
                    <div class="listaG2 dorado-2" style="height: auto; padding: 5px; padding-left: 10px"><h5>Grupos Generales</h5></div>
                    <?php
                    $i = 0;
                    foreach ($grupos as $grupo){
                        if($i > 15) break;
                        if($i < 5){
                    ?>
                    <div class="listaG2 grey lighten-5">
                        <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."index.php/novios/moderador/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                            <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                            <p class="nombre_grupo truncate"> <?php echo !empty($grupo)? $grupo->nombre : "" ?> </p>
                        </a>
                    </div>
                    <?php 
                        }else{
                    ?>
                    <div class="listaG2 oculto grey lighten-5">
                        <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."index.php/novios/moderador/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                            <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                            <p class="nombre_grupo truncate"> <?php echo !empty($grupo)? $grupo->nombre : "" ?> </p>
                        </a>
                    </div>
                    <?php
                        }
                        $i++;
                    } ?>
                    <div class="listaG2 grey lighten-5" style="padding-top: 17px;">
                        <a class="clickable desplegar_grupos" style="color: #999; padding-left: 30px"> Mas Grupos... </a>
                    </div>
                    <?php 
                    } 
                    ?>
                </div> 
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row" style="border-bottom: 1px solid #999">
                    <h5>Videos De La Comunidad japybodas.com</h5>
                </div>
                <div class="row">
                    <div class="card">
                        <div class="card-image">
                            <img style="height: 150px; width: 100%" src="<?php echo base_url()."dist/images/comunidad/foros/comparte-videos.jpeg"?>">
                        </div>
                    </div>
                </div>
                <?php if(!empty($videos_recientes)){ ?>
                <div class="row">
                    <h5>Ultimos Videos Publicados</h5>
                </div>
				<div class="row">
                <p>Un total de <?php echo $total ?> videos desactivados.</p>
				</div>	
                <div class="row">
                <?php
                if(!empty($videos_recientes)){
                    foreach ($videos_recientes as $video){
                        $direccion_img = str_replace("https://www.youtube.com", "https://i.ytimg.com/", $video->direccion_web);
                        $direccion_img = str_replace("embed", "vi", $direccion_img);
                        if($direccion_img != $video->direccion_web){
                        $direccion_img = $direccion_img."/default.jpg";}
						else{
							$direccion_img = base_url()."dist/images/comunidad/VideoError.png";
						}
                        echo '
                        <div class="col s6 m3 video_publicado">
                            <div class="card small">
                                <div class="card-image">
                                    <a href="'.base_url().'index.php/novios/moderador/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'"> <img class="video_zoom" src="'.$direccion_img.'"></a>
                                </div>
                                <div class="card-content" style="text-align: center">
                                    <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                        <img alt="" src="'.$video->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                                    </div>
                                    <p class="clickable truncate"><a class="titulo_video" href="'.base_url().'index.php/novios/moderador/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'">'.$video->titulo.'</a></p><br/>
                                    <p><a class="link_usuario">'.$video->usuario.'</a></p>
                                </div>
                            </div>
                        </div>';
                    }
                    echo '
                    <div class="col s6 m3 video_publicado">
                        <div class="card small videos_recientes fotos clickable">
                            <div class="card-image" style="text-align: center; font-size: 90px">
                                <img style="height:200px" src="'.base_url().'dist/images/comunidad/Video.png">
                            </div>
                            <div class="card-content" style="text-align: center">
                                <p><button type="button" class="btn dorado-2 clickable videos_recientes">Mas videos</button></p>
                            </div>
                        </div>
                    </div>';
                }
                ?>
                </div>
                <?php } ?>
                
                <?php if(!empty($videos_vistos)){ ?>
                <div class="row">
                    <h5>Videos Publicados Mas Vistos</h5>
                </div>
                <div class="row">
                <?php
                if(!empty($videos_vistos)){
                    foreach ($videos_vistos as $video){
                        $direccion_img = str_replace("https://www.youtube.com", "https://i.ytimg.com/", $video->direccion_web);
                        $direccion_img = str_replace("embed", "vi", $direccion_img);
                        if($direccion_img != $video->direccion_web){
                        $direccion_img = $direccion_img."/default.jpg";}
						else{
							$direccion_img = base_url()."dist/images/comunidad/VideoError.png";
						}
                        echo '
                        <div class="col s6 m3 video_publicado">
                            <div class="card small">
                                <div class="card-image">
                                    <a href="'.base_url().'index.php/novios/moderador/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'"> <img class="video_zoom" src="'.$direccion_img.'"></a>
                                </div>
                                <div class="card-content" style="text-align: center">
                                    <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                        <img alt="" src="'.$video->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                                    </div>
                                    <p class="clickable truncate"><a class="titulo_video" href="'.base_url().'index.php/novios/moderador/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'">'.$video->titulo.'</a></p><br/>
                                    <p><a class="link_usuario">'.$video->usuario.'</a></p>
                                </div>
                            </div>
                        </div>';
                    }
                    echo '
                    <div class="col s6 m3 video_publicado">
                        <div class="card small videos_vistas fotos clickable">
                            <div class="card-image" style="text-align: center; font-size: 90px">
                                <img style="height:200px" src="'.base_url().'dist/images/comunidad/Video.png">
                            </div>
                            <div class="card-content" style="text-align: center">
                                <p><button type="button" class="btn dorado-2 clickable videos_recientes">Mas videos</button></p>
                            </div>
                        </div>
                    </div>';
                }
                ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- -----------------  TEMPLATE VENTANA BUSCADOR -------------------------- -->
    <ul id="lista_debates">
        <li id="item_debate">
            <a class="enlace_debate" href="">
                <div class="row">
                    <div class="col s4 m3">
                        <div class="card">
                            <div class="card-image">
                                <img class="imagen_debate">
                            </div>
                        </div>
                    </div>
                    <div class="col s8 m9">
                        <p class="titulo_debate truncate"></p>
                        <p class="fecha_debate"></p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <!-- ------------------  FIN TAMPLETE VENTANA BUSCADOR --------------------- -->
    <script>
    $(document).ready(function (){
        $(".videos_recientes").click(function (){
            window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Forum/videos/recientes";
        });
        $(".videos_vistas").click(function (){
            window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Forum/videos/visitados";
        });
    });
	
	$(document).ready(function (){
        var activo = 0;
        var activo2 = 0;
        var height = 0;
        var bandera = true;
        
        $("input[name=buscar]").keydown(function(evt){
			
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
			
            if(key == 40){
				
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo++;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo > 1){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height > 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
					activo2 = 1;
                }else{
                    if(bandera){
                        activo++;
                        bandera = false;
                    }
                    activo--;
					activo2 = 0;
                }
				
            }else if(key == 38){
				
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo--;
				
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo >= 0){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height >= 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
					activo2 = 1
                }else{
					
                   activo = 0;
				   activo2 = 0;
                }
            }else if(key == 13){
				if(activo2!=0){
                $("#buscador").on('keypress',function(e){
                    e.preventDefault();
                    return false;
                });
                window.location.href = $('.item-'+activo+' a').attr("href");
				}
				}else if(key != 39 && key != 37){
                activo = 0;
                grupos();
            }
        });
        
        $("input[name=buscar]").keyup(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(titulo != "" && key != 38 && key != 40 && key != 39 && key != 37){
                activo = 0;
                debates(titulo);
            }else if(key == 13){
                window.location.href = $('.item-'+activo+' a').attr("href");
            }
        });
        
        $(document).on('mouseenter','#ventana_buscador > ul > li',function(){
            $('.item-'+activo).removeClass("hover");
            var clase = $(this).attr("class");
            var token = clase.split("-");
            activo = token[1];
            $(this).addClass("hover");
        });
       
        $("body").on('click',function(){
            $("#ventana_buscador").hide();
            $('.item-'+activo).removeClass("hover");
        });
        
        $("input[name=buscar]").on('click', function (e){
            e.stopPropagation();
            $('.item-'+activo).removeClass("hover");
            activo = 0;
            var titulo = $("input[name=buscar]").val();
            if(titulo == "" && $("#ventana_buscador ul").html() == undefined){
                grupos();
            }
            $("#ventana_buscador").show();
        });
        
        $("#ventana_buscador").on('mouseleave',function(){
            setTimeout(function(){
                $('.item-'+activo).removeClass("hover");
            },1000);
        });
        
        function grupos(){
            $.ajax({
                url: '<?php echo base_url()."index.php/novios/moderador/home/getGrupos"?>',
                success: function(res) {
                    var val = Array();
					
                    if(res.success){
                        $("#ventana_buscador").html("");
						
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        for(var aux in res.data){
							
                            if(res.data.hasOwnProperty(aux)){
								
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_grupo);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].imagen);
                                $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                $(li).find(".fecha_debate").text(res.data[aux].debates+" Debates");
                                $(ul).append(li);
                                if(i == 16){
									console.log(ul);
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").data("grupos","true");
                        $("#ventana_buscador").show();
                    }
                }          
            });
			
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
        
        function debates(titulo){
            $.ajax({
                url: '<?php echo base_url()."index.php/novios/moderador/home/buscar" ?>',
                method: 'post',
                data:{
                    'titulo_debate': titulo
                },
                success: function(res){
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        var val = Array();
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_debate);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].foto_usuario);
                                $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                $(ul).append(li);
                                if(i > (res.data.length - 1)){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").show();
                    }
                }
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
    });
    </script>
</body>
<?php $this->view('principal/footer'); ?>