<div class="col s12 m9 pull-left">
    <div class="row">
        <div class="col s12 m6">
            <h5>Debates</h5>
        </div>
        <div class="col s12 m6 input-field" style="margin: 0; float: left">
            <select id="selector">
                <option value="0">Ordenar Por</option>
                <option value="1" <?php if(!empty($ordenamiento) && $ordenamiento == "mensajes") echo "selected"?>>Mensajes</option>
                <option value="2" <?php if(!empty($ordenamiento) && $ordenamiento == "visitas") echo "selected"?>>Visitas</option>
                <option value="3" <?php if(!empty($ordenamiento) && $ordenamiento == "ultimo_mensaje") echo "selected"?>>Ultimo Mensaje</option>
                <option value="4" <?php if(!empty($ordenamiento) && $ordenamiento == "fecha") echo "selected"?>>Fecha</option>
                <option value="5" <?php if(!empty($ordenamiento) && $ordenamiento == "sin_respuesta") echo "selected"?>>Sin Respuesta</option>
            </select>
        </div>
    </div>
</div>
<div class="col s12 m9 pull-left">
    <div class="row">
        <div class="col s12 m12 grey lighten-5 z-depth-1" style="<?php if(!empty($debates_recientes)) echo 'display: block' ?>">
        <?php 
            if(!empty($debates)){
                $j = count($debates) - 1;
                $i = 0;
                foreach ($debates as $debate){
                        echo '  <div class="col s12 m12" style="border-bottom: 1px solid #999; padding-top: 10px">
                                <div class="col s3 m2">
                                    <div class="card">
                                        <div class="card-imagear">
                                            <img class="perfil" src="'.$debate->foto_usuario.'">
                                        </div>
                                    </div>
                                </div>
                                <div class="col s6 m8" style="border-right: 1px solid #999; text-align:justify">
                                    <p style="font-size:18px"><a class="titulo_foto" href="'.base_url().'index.php/novios/moderador/Home/debatePublicado/'.$debate->id_debate.'">'.$debate->titulo_debate.'</a></p>
                                    <p class="">Creado Por <span class="titulo_foto" >'.$debate->usuario.'</span> '.$debate->fecha_creacion.'</p>
                                    <p>'.$debate->debate.'</p>
                                </div>
                                <div class="col s3 m2" style="text-align:center">
                                    <p><i class="fa fa-comments" aria-hidden="true"></i> '.$debate->num_comentarios.'</p>';
                        if($debate->num_comentarios > 0){
                            echo '<p>Ultimo Mensaje</p>';
                        }
                        echo    '<p>'.$debate->fecha_comentario.'</p>
                                </div></div>';
                    
                }
            }
        ?>
        </div>
    </div>
</div>
<div class="col s12 m9 pull-left">
    <div class="row">
        <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
            <div class="col s12 m6" style="float: right">
                <ul class="pagination">
                    <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a <?php if($pagina > 1) echo 'href="'.base_url()."index.php/novios/moderador/Group/grupo/$id_grupo/debates/$ordenamiento/".($pagina - 1).'"' ?>><i class="material-icons">chevron_left</i></a></li>
                    <?php for($i=1; $i <= $total_paginas; $i++){?>
                    <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo base_url()."index.php/novios/moderador/Group/grupo/$id_grupo/debates/$ordenamiento/$i" ?>"><?php echo $i ?></a></li>
                    <?php } ?>
                    <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a <?php if($pagina < $total_paginas) echo 'href="'.base_url()."index.php/novios/moderador/Group/grupo/$id_grupo/debates/$ordenamiento/".($pagina + 1).'"' ?>><i class="material-icons">chevron_right</i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (){
        $("#selector").change(function (){
            switch($(this).val()){
                case "1":
                    window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/debates/mensajes";
                    break;
                case "2":    
                    window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/debates/visitas";
                    break;
                case "3":
                    window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/debates/ultimo_mensaje";
                    break;
                case "4":
                    window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/debates/fecha";
                    break;
                case "5":    
                    window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/debates/sin_respuesta";
                    break;
            }
        });
    });
</script>
