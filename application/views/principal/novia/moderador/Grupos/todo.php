<div class="col s12 m9 pull-left">
    <?php if(!empty($debates_recientes)){ ?>
    <div class="row">
        <h5>Debates</h5>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col s12 m12 grey lighten-5 z-depth-1" style="<?php if(!empty($debates_recientes)) {echo 'display: block';} ?>">
        <?php 
            if(!empty($debates_recientes)){
                $j = count($debates_recientes) - 1;
                $i = 0;
                foreach ($debates_recientes as $debate){
                        echo '  <div class="col s12 m12" style="border-bottom: 1px solid #999; padding-top: 10px">
                                <div class="col s3 m2">
                                    <div class="card">
                                        <div class="card-imagear">
                                            <img class="perfil" src="'.$debate->foto_usuario.'">
                                        </div>
                                    </div>
                                </div>
                                <div class="col s6 m8" style="border-right: 1px solid #999; text-align:justify">
                                    <p style="font-size:18px"><a class="titulo_foto" href="'.base_url().'/index.php/novios/moderador/Home/debatePublicado/'.$debate->id_debate.'">'.$debate->titulo_debate.'</a></p>
                                    <p class="">Creado Por <span class="titulo_foto" >'.$debate->usuario.'</span> '.$debate->fecha_creacion.'</p>
                                    <p>'.$debate->debate.'</p>
                                </div>
                                <div class="col s3 m2" style="text-align:center">
                                    <p><i class="fa fa-comments" aria-hidden="true"></i> '.$debate->num_comentarios.'</p>
                                    <p>Ultimo Mensaje</p>
                                    <p>'.$debate->fecha_comentario.'</p>
                                </div></div>';
                    
                }
                echo    '<div class="col s12 m12" style="text-align:right">
                            <p>
                                <a href="'.base_url().'index.php/novios/moderador/Group/grupo/'.$id_grupo.'/debates" style="color:#999">Ver Debates <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </p>
                        </div>';
            }
        ?>
        </div>
    </div>
</div>
<div class="col s12 m9 pull-left">
    <?php if(!empty($fotos_recientes)){ ?>
    <div class="row">
        <h5>Fotos Mas Recientes</h5>
    </div>
    <?php } ?>
    <div class="row">
    <?php
    if(!empty($fotos_recientes)){
        foreach ($fotos_recientes as $foto){
            echo '
            <div class="col s6 m3 foto_publicada">
                <div class="card small">
                    <div class="card-image">
                        <a href="'.base_url().'index.php/novios/moderador/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'"> <img class="foto_zoom" src="'.base_url().'index.php/novios/moderador/picture/foto/'.$foto->id_foto.'"></a>
                    </div>
                    <div class="card-content" style="text-align: center">
                        <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                            <img alt="" src="'.$foto->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                        </div>
                        <p class="clickable truncate"><a class="titulo_foto" href="'.base_url().'index.php/novios/moderador/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'">'.$foto->titulo.'</a></p><br/>
                        <p class="clickable"><a class="link_usuario">'.$foto->usuario.'</a></p>
                    </div>
                </div>
            </div>';
        }
        echo '
        <div class="col s6 m3 foto_publicada">
            <div class="card small fotos_recientes fotos clickable">
                <div class="card-image" style="text-align: center; font-size: 90px">
                    <p style="width: 100%; height: 100%"> <i class="fa fa-file-image-o" aria-hidden="true"></i> </p>
                </div>
                <div class="card-content" style="text-align: center">
                    <p><button type="button" class="btn dorado-2 clickable fotos_recientes">Mas fotos</button></p>
                </div>
            </div>
        </div>';
    }
    ?>
    </div>
</div>
<div class="col s12 m9 pull-left">
    <?php if(!empty($videos_recientes)){ ?>
    <div class="row">
        <h5>Videos Mas Recientes</h5>
    </div>
    <?php } ?>
    <div class="row">
    <?php
    if(!empty($videos_recientes)){
        foreach ($videos_recientes as $video){
            $direccion_img = str_replace("https://www.youtube.com", "https://i.ytimg.com/", $video->direccion_web);
                        $direccion_img = str_replace("embed", "vi", $direccion_img);
						if($direccion_img != $video->direccion_web){
                        $direccion_img = $direccion_img."/default.jpg";}
						else{
							$direccion_img = base_url()."dist/images/comunidad/VideoError.png";
						}
            echo '
            <div class="col s6 m3 video_publicado">
                <div class="card small">
                    <div class="card-image">
                        <a href="'.base_url().'index.php/novios/moderador/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'"> <img class="video_zoom" src="'.$direccion_img.'"></a>
                    </div>
                    <div class="card-content" style="text-align: center">
                        <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                            <img alt="" src="'.$video->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                        </div>
                        <p class="clickable truncate"><a class="titulo_video" href="'.base_url().'index.php/novios/moderador/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'">'.$video->titulo.'</a></p><br/>
                        <p class="clickable"><a class="link_usuario">'.$video->usuario.'</a></p>
                    </div>
                </div>
            </div>';
        }
        echo '
        <div class="col s6 m3 video_publicado">
            <div class="card small videos_recientes fotos clickable">
                <div class="card-image" style="text-align: center; font-size: 90px">
                    <p style="width: 100%; height: 100%"> <i class="fa fa-file-image-o" aria-hidden="true"></i> </p>
                </div>
                <div class="card-content" style="text-align: center">
                    <p><button type="button" class="btn dorado-2 clickable videos_recientes">Mas videos</button></p>
                </div>
            </div>
        </div>';
    }
    ?>
    </div>
</div>
<script>
    $(document).ready(function (){
        $(".fotos_recientes").click(function (){
            window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/fotos";
        });
        $(".videos_recientes").click(function (){
            window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Group/grupo/<?php if(!empty($id_grupo)) echo $id_grupo?>/videos";
        });
    });
</script>