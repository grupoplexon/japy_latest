<?php $this->view('principal/header');?>
<?php $this->view('principal/novia/menu');?>
<link href="<?php echo base_url()?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<body>
    <!-- BARRA DE TITULO CON UN BUSCADOR DE DEBATES -->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px;padding: 10px;">
            <p class="col s4 m2" style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            <div id="texto_buscar" class="input-field col s9 m5" style="margin:0">
                <input type="text" name="buscar" class="form-control" placeholder="Buscar..."/>
            </div>
            <div id="btn_buscar" class="col s4 m2">
                <button type="submit" class="btn dorado-2 truncate"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
            </div>
        </div>
    </div>
    <!-- FIN BARRA DE TITULO CON UN BUSCADOR DE DEBATES -->
    
    <div class="body-container">
        <div class="row">
            <!--COMPONENTES LATERALES-->
            <div class="col s12 m3 lateral" style="float: right; margin-bottom: 20px">
                <!------------------------ IMAGEN DE PUBLICIDAD --------------------->
                <div class="row">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?php echo base_url() ?>dist/images/comunidad/plantilla/plantilla.jpeg">
                        </div>
                    </div>
                </div>
                
                <?php if(!empty($usuarios_boda->novios)){ ?>
                <!--FECHA DE MI BODA CON LOS USUARIOS QUE COINCIDEN-->
                <div class="row">
                    <div class="texto_lateral grey lighten-5" style="height: auto; border: 1px solid #999; text-align: center">
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->fecha_boda)? $usuarios_boda->fecha_boda : "" ?></p>
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->estado_boda)? $usuarios_boda->estado_boda : "" ?></p>
                    </div>
                    <div  class="texto_lateral dorado-2" style="height: 70px; border: 1px solid #999; margin-top: -1px; text-align: center">
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->novios)? $usuarios_boda->novios : ""; 
                        if($usuarios_boda->novios > 1) echo ' novios se casan el mismo dia que tu'; else echo ' novio se casa el mismo dia que tu'?> </p>
                    </div>
                    <div class="texto_lateral grey lighten-5 col s12 m12" style="height: auto; border: 1px solid #999;margin-top: -1px; text-align: center;">
                        <div class="col s4 m4">
                            <div class="card">
                                <div class="card-image">
                                    <img style="width: 100%" src="<?php echo !empty($usuarios_boda->foto_usuario)? $usuarios_boda->foto_usuario : "" ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m12">
                            <a href="<?php echo base_url() ?>novios/comunidad/Home/conocerCompaneros" class="btn dorado-2 truncate" style="margin-top: 10px; margin-bottom: 10px"> Conocelos </a>
                        </div>    
                    </div>
                </div>
                <?php } ?>
                
                <?php if(!empty($visitas_perfil)){ ?>
                <!--QUIEN VISITA MI PERFIL, CON LOS USUARIOS QUE LO HACEN-->
                <div class="row">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="visita_perfil dorado-2">
                        <h5> ¿Quien Visita Tu Perfil? </h5>
                    </div>
                    <div style="height: 120px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($visitas_perfil as $visita){
                        ?>
                        <a href="<?php echo !empty($visita->url_usuario)? $visita->url_usuario : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($visita->foto_usuario)? $visita->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($visita->todos)? $visita->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="dorado-2" style="height: auto; border: 1px solid #999; padding: 5px">
                        <h5>Mis Medallas</h5>
                    </div>
                    <div class="grey lighten-5" style="height: auto; border: 1px solid #999; padding: 5px; text-align: justify; border-top: none">
                        <p><b>¿Ya conoces las medallas obtenidas por participar en la comunidad de clubnupcial?. Entra ahora y disfruta de ellas.</b></p>
                    </div>
                    <div class="grey lighten-5" style="height: 50px; border: 1px solid #999; border-top: none">
                        <a href="<?php echo base_url()."novios/comunidad/perfil/medallas/$id_usuario"?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey; marging-bottom: 5px">Ver Medallas <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                
                <?php if(!empty($grupos_miembro)){ ?>
                <!--MIS GRUPOS-->
                <div class="row">
                    <div class="mis_grupos dorado-2" style="height: auto; padding: 5px; border: 1px solid #999">
                        <h5> Mis Grupos </h5>
                    </div>
                    <div class="mis_grupos grey lighten-5" style="height: auto; border: 1px solid #999; border-top: none">
                        <?php foreach ($grupos_miembro as $grupo){ ?>
                        <a href="<?php echo base_url()."novios/comunidad/group/grupo/$grupo->id_grupos_comunidad/todo"?>">
                        <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                        <p class="nombre_grupo truncate"> <?php echo !empty($grupo->nombre)? $grupo->nombre : "" ?> </p>
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <?php 
                } 
                ?>
                
                <?php if(!empty($grupos)){ ?>
                <!--GRUPOS GENERALES-->
                <div class="row">
                    <div class="listaG2 dorado-2" style="height: auto; padding: 5px; padding-left: 10px"><h5>Grupos Generales</h5></div>
                    <?php
                    $i = 0;
                    foreach ($grupos as $grupo){
                        if($i > 15) break;
                        if($i < 5){
                    ?>
                    <div class="listaG2 grey lighten-5">
                        <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."novios/comunidad/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                            <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                            <p class="nombre_grupo truncate"> <?php echo !empty($grupo)? $grupo->nombre : "" ?> </p>
                        </a>
                    </div>
                    <?php 
                        }else{
                    ?>
                    <div class="listaG2 oculto grey lighten-5">
                        <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."novios/comunidad/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                            <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                            <p class="nombre_grupo truncate"> <?php echo !empty($grupo)? $grupo->nombre : "" ?> </p>
                        </a>
                    </div>
                    <?php
                        }
                        $i++;
                    } ?>
                    <div class="listaG2 grey lighten-5" style="padding-top: 17px;">
                        <a class="clickable desplegar_grupos" style="color: #999; padding-left: 30px"> Mas Grupos... </a>
                    </div>
                    <?php 
                    } 
                    ?>
                </div> 
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1" style="margin-top: 10px">
                        <div class="row">
                            <div class="col s4 m2"> 
                                <div class="card">
                                    <div class="card-image">
                                        <?php
                                            if(!empty($datos_usuario->mime)){
                                                echo '<img src="'.base_url().'novios/comunidad/Home/foto_usuario/'.$datos_usuario->id_usuario.'">';
                                            }else{
                                                echo '<img src="'.base_url().'dist/img/blog/perfil.png">';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col s8 m10" style="margin-bottom: 20px">
                                <div style="margin-bottom: 10px">
                                    <div class="col s9 m9" style="font-weight: bold"><p><?php if(!empty($datos_usuario->usuario)) echo $datos_usuario->usuario ?></p></div>   
                                    <div class="col s3 m3 dorado-2-text"><p style="text-align: center"><?php if(!empty($datos_usuario->puntos)) echo "$datos_usuario->puntos Puntos" ?></p></div>
                                </div>
                                <div class="col s12 m12"><p><i class='fa fa-map-marker' aria-hidden='true'></i> <?php
                                    if(!empty($datos_usuario->poblacion) && !empty($datos_usuario->estado)) echo "$datos_usuario->poblacion, $datos_usuario->estado";
                                ?></p></div>
                                <div class="col s12 m12"><p class="link_usuario"><?php echo !empty($datos_usuario->fecha_creacion)? $datos_usuario->fecha_creacion : ""?></p></div>
                                <div class="col s12 m12" style="text-align: justify; margin-bottom: 15px">
                                    <p>
                                    <?php
                                        if(!empty($datos_usuario)){
                                            echo "$datos_usuario->sobre_mi";
                                        }
                                    ?>
                                    </p>
                                </div>
                                <div class="col s12 m6" style="margin-bottom: 10px">
                                    <a class="btn waves-effect waves-light dorado-2" href="<?php echo base_url()?>novios/perfil">
                                        <i class="fa fa-pencil" aria-hidden="true"></i> Editar <!-- Me redirecciona a la parte de modificar mi perfil-->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 white lighten-5 z-depth-1">
                        <ul class="tabs filtro1">
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "perfil") echo 'class="active"'?> href="#">Mi Perfil</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "muro") echo 'class="active"'?> href="#">Mi Muro</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "debates") echo 'class="active"'?> href="#">Debates</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "amigos") echo 'class="active"'?> href="#">Amigos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "fotos") echo 'class="active"'?> href="#">Fotos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "videos") echo 'class="active"'?> href="#">Videos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "visitas") echo 'class="active"'?> href="#">Visitas</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "actividad") echo 'class="active"'?> href="#">Actividad</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "proveedores") echo 'class="active"'?> href="#">Proveedores</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($amigos)? "block": "none"?>">
                <h5>Mis Amigos</h5>
            </div>
            <div class="col s12 m9 pull-left" style="display: <?php echo !empty($amigos)? "block": "none"?>">
                <?php 
                    $i = 1;
                    if(!empty($amigos)){
                        foreach ($amigos as $amigo){
                            if($i % 2 != 0){
                                echo '<div class="row">';
                            }
                            echo '<div class="col s12 m6">
                                    <div class="tarjeta-perfil z-depth-1">
                                        <div class="row">
                                            <div class="col s3 m4">
                                                <div class="card">
                                                    <div class="card-image">
                                                        <img class="foto_perfil" src="'.$amigo->url_foto_usuario.'">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s9 m8">
                                                <p class="truncate"><a class="titulo_tarjeta" href="'.$amigo->url_usuario.'">'.$amigo->usuario.'</a></p>
                                            </div>
                                            <div class="col s9 m8">
                                                <p>'.$amigo->lugar.'</p>
                                                <p>'.$amigo->fecha_creacion.'</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$amigo->url_debates_participacion.'"><p class="truncate"><text class="numeros">'.$amigo->mensajes.'</text> Mensajes</p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$amigo->url_debates.'"><p><text class="numeros">'.$amigo->debates.'</text> Post</p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$amigo->url_fotos.'"><p><text class="numeros">'.$amigo->fotos.'</text> Fotos<p></div>
                                            <div class="col s6 m6 botones_tarjeta dorado-2 clickable" data-id="'.$amigo->url_amigos.'"><p><text class="numeros">'.$amigo->amigos.'</text> Amigos</p></div>
                                        </div>
                                        <div class="col s6 m6 pull-left" style="margin-bottom: 10px;">';
                            if(!empty($amigo->agregar)){
                                echo    '<p style="text-align: center"><a class="btn_agregar clickable" style="color: #999" data-id="'.$amigo->url_agregar_amigo.'"><i class="fa fa-user-plus" aria-hidden="true"></i> <text class="agregar-'.$amigo->id_usuario.'">'.$amigo->agregar.'</text></a><p>';
                            }            
                            echo        '</div>
                                        <div class="col s6 m6" style="margin-bottom: 10px;">
                                            <p style="text-align: center"><a href="#modal1" class="modal-trigger comentar" style="color: #999" data-name="comentar-'.$amigo->id_usuario.'"><i class="fa fa-comments" aria-hidden="true"></i> Poner Comentario</a></p>
                                        </div>
                                    </div>
                                </div>';
                            if($i % 2 == 0){
                                echo "</div>";
                            }
                            $i++;
                        }
                        if(count($amigos) % 2 != 0){
                            echo "</div>";
                        }
                    }
                ?>
                
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
                        <div class="col s12 m9" style="float: right">
                            <ul class="pagination">
                                <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a <?php if($pagina > 1) echo 'href="'.base_url()."novios/comunidad/perfil/debates/$ordenamiento/$datos_usuario->id_usuario/".($pagina - 1).'"' ?>><i class="material-icons">chevron_left</i></a></li>
                                <?php 
                                $j = 1;
                                $contador = 0;
                                if($total_paginas > 10 && $pagina > 0 && $pagina > 5) { 
                                    $pagina2 = $pagina + 5;
                                    if($pagina2 <= $total_paginas){
                                        $j = $pagina - 4;
                                    }else{
                                        $j = $pagina - 4;
                                        $j = $j - ($pagina2 - $total_paginas);
                                    }
                                }else{
                                    $j = 1;
                                }
                                for($i=$j; $i <= $total_paginas && $contador < 10; $i++){
                                ?>
                                <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo base_url()."novios/comunidad/perfil/debates/$ordenamiento/$datos_usuario->id_usuario/$i" ?>"><?php echo $i ?></a></li>
                                <?php $contador++; } ?>
                                <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a <?php if($pagina < $total_paginas) echo 'href="'.base_url()."novios/comunidad/perfil/debates/$ordenamiento/$datos_usuario->id_usuario/".($pagina + 1).'"'?>><i class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Escribe un comentario en el muro</h4>
            <div class="col s12 m12 input-field">
                <textarea id="comentario" class="form-control" rows="20" style="resize: none; height: 50%;"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn dorado-2 comentar-muro" data-id="">Publicar</a>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('ul.tabs').tabs();
        });

        $(document).ready(function(){
            $('.filtro1 a').on("click", function (){
                var opcion = $(this).text();
                switch(opcion){
                    case "Mi Perfil":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/usuario/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Mi Muro":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/mi_muro/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Debates":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/debates/participacion/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Amigos":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/amigos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Fotos":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/fotos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Videos":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/videos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Actividad":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/actividad/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Visitas":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/visitas/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Proveedores":
                        window.location.href = "<?php echo base_url()?>novios/comunidad/perfil/proveedores/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                }
            }); 
        });
        
        $(document).ready(function(){
            $('.botones_tarjeta').on('click', function(){
                var click = $(this).attr('data-id');
                var boton = click.split("-");
                if(boton[0] == "participacion"){
                    window.location.href = "<?php echo base_url()."novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "misdebates"){
                    window.location.href = "<?php echo base_url()."novios/comunidad/perfil/debates/"?>"+boton[0]+"/"+boton[1];
                }else if(boton[0] == "fotos"){
                    window.location.href = "<?php echo base_url()."novios/comunidad/perfil/fotos/"?>"+boton[1];
                }else if(boton[0] == "amigos"){
                    window.location.href = "<?php echo base_url()."novios/comunidad/perfil/amigos/"?>"+boton[1];
                }
            });
        });
        
        $(document).ready(function(){
            $('.modal-trigger').modal({
                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                in_duration: 300, // Transition in duration
                out_duration: 200, // Transition out duration
                starting_top: '4%', // Starting top style attribute
                ending_top: '10%', // Ending top style attribute
                }
            );
            $(".comentar").on("click", function(){
                var name = $(this).attr("data-name");
                var token = name.split("-");
                $(".modal-action").attr("data-id",token[1]);
            });
        });
        
        $(document).ready(function (){
            $(".comentar-muro").on("click", function(){
                var comentario = $("#comentario").val();
                var usuario = $(this).attr("data-id");
                $.ajax({
                    url: '<?php echo base_url()."novios/comunidad/perfil/comentario"?>',
                    method: 'POST',
                    data:{
                        'comentario': comentario,
                        'muro_usuario': usuario 
                    },
                    success: function(res){
                        if(res.success){
                            setTimeout(function(){
                                alert("Se ha publicado con exito");
                                $("#comentario").val("");
                            },500);
                        }
                    },
                    error: function(){
                        setTimeout(function(){
                            alert("Lo sentimos ocurrio un error");
                        },500);
                    }
                });
            });
        });
        
        $(document).ready(function(){
            $('.btn_agregar').on("click", function(){
                var usuario = $(this).attr("data-id");
                var token = usuario.split('-');
                var tipo = $("."+usuario).text();
                $.ajax({
                    url: '<?php echo base_url() ?>novios/comunidad/perfil/solicitudAmistad',
                    method: 'POST',
                    data:{
                        'id_usuario_confirmacion': token[1],
                        'tipo': tipo
                    },
                    success: function(res){
                        if(res.success){
                            $("."+usuario).text(res.data);
                        }
                    },
                    error: function(){
                        alert("Lo sentimos ocurrio un error");
                    }
                });
            });
        });
        
        $(document).ready(function (){
            $('.desplegar_grupos').click(function (){
                var mostrar = $(this).text();
                var display = $('.oculto').css('display');
                if(display == "none"){
                    $('.oculto').css('display','block');
                    $(this).text("Ocoltar Grupos");
                }else{
                    $('.oculto').css('display','none');
                    $(this).text("Mas Grupos...");
                }
            });
        });
    </script>
</body>
<?php $this->view('principal/footer'); ?>

