<?php $this->view('principal/header');?>
<?php $this->view('principal/novia/menu');?>
<link href="<?php echo base_url()?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<body>
    <!-- BARRA DE TITULO CON UN BUSCADOR DE DEBATES -->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px;padding: 10px;">
            <p class="col s4 m2" style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            <div id="texto_buscar" class="input-field col s9 m5" style="margin:0">
                <input type="text" name="buscar" class="form-control" placeholder="Buscar..."/>
            </div>
            <div id="btn_buscar" class="col s4 m2">
                <button type="submit" class="btn dorado-2 truncate"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
            </div>
        </div>
    </div>
    <!-- FIN BARRA DE TITULO CON UN BUSCADOR DE DEBATES -->
    
    <div class="body-container">
        <div class="row">
            <!--COMPONENTES LATERALES-->
            <div class="col s12 m3 lateral" style="float: right; margin-bottom: 20px">
                <!------------------------ IMAGEN DE PUBLICIDAD --------------------->
                <div class="row">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?php echo base_url() ?>dist/images/comunidad/plantilla/plantilla.jpeg">
                        </div>
                    </div>
                </div>
                
                <?php if(!empty($usuarios_boda->novios)){ ?>
                <!--FECHA DE MI BODA CON LOS USUARIOS QUE COINCIDEN-->
                <div class="row">
                    <div class="texto_lateral grey lighten-5" style="height: auto; border: 1px solid #999; text-align: center">
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->fecha_boda)? $usuarios_boda->fecha_boda : "" ?></p>
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->estado_boda)? $usuarios_boda->estado_boda : "" ?></p>
                    </div>
                    <div  class="texto_lateral dorado-2" style="height: 70px; border: 1px solid #999; margin-top: -1px; text-align: center">
                        <p class="coincidencia"><?php echo !empty($usuarios_boda->novios)? $usuarios_boda->novios : ""; 
                        if($usuarios_boda->novios > 1) echo ' novios se casan el mismo dia que tu'; else echo ' novio se casa el mismo dia que tu'?> </p>
                    </div>
                    <div class="texto_lateral grey lighten-5 col s12 m12" style="height: auto; border: 1px solid #999;margin-top: -1px; text-align: center;">
                        <div class="col s4 m4">
                            <div class="card">
                                <div class="card-image">
                                    <img style="width: 100%" src="<?php echo !empty($usuarios_boda->foto_usuario)? $usuarios_boda->foto_usuario : "" ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m12">
                            <a href="<?php echo base_url() ?>index.php/novios/comunidad/Home/conocerCompaneros" class="btn dorado-2 truncate" style="margin-top: 10px; margin-bottom: 10px"> Conocelos </a>
                        </div>    
                    </div>
                </div>
                <?php } ?>
                
                <?php if(!empty($visitas_perfil)){ ?>
                <!--QUIEN VISITA MI PERFIL, CON LOS USUARIOS QUE LO HACEN-->
                <div class="row">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="visita_perfil dorado-2">
                        <h5> ¿Quien Visita Tu Perfil? </h5>
                    </div>
                    <div style="height: 120px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($visitas_perfil as $visita){
                        ?>
                        <a href="<?php echo !empty($visita->url_usuario)? $visita->url_usuario : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($visita->foto_usuario)? $visita->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($visita->todos)? $visita->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
                
                <?php if(!empty($grupos_miembro)){ ?>
                <!--MIS GRUPOS-->
                <div class="row">
                    <div class="mis_grupos dorado-2" style="height: auto; padding: 5px; border: 1px solid #999">
                        <h5> Mis Grupos </h5>
                    </div>
                    <div class="mis_grupos grey lighten-5" style="height: auto; border: 1px solid #999; border-top: none">
                        <?php foreach ($grupos_miembro as $grupo){ ?>
                        <a href="<?php echo base_url()."index.php/novios/comunidad/group/grupo/$grupo->id_grupos_comunidad/todo"?>">
                        <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                        <p class="nombre_grupo truncate"> <?php echo !empty($grupo->nombre)? $grupo->nombre : "" ?> </p>
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <?php 
                } 
                ?>
                
                <?php if(!empty($grupos)){ ?>
                <!--GRUPOS GENERALES-->
                <div class="row">
                    <div class="listaG2 dorado-2" style="height: auto; padding: 5px; padding-left: 10px"><h5>Grupos Generales</h5></div>
                    <?php
                    $i = 0;
                    foreach ($grupos as $grupo){
                        if($i > 15) break;
                        if($i < 5){
                    ?>
                    <div class="listaG2 grey lighten-5">
                        <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."index.php/novios/comunidad/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                            <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                            <p class="nombre_grupo truncate"> <?php echo !empty($grupo)? $grupo->nombre : "" ?> </p>
                        </a>
                    </div>
                    <?php 
                        }else{
                    ?>
                    <div class="listaG2 oculto grey lighten-5">
                        <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."index.php/novios/comunidad/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                            <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen"?>"> 
                            <p class="nombre_grupo truncate"> <?php echo !empty($grupo)? $grupo->nombre : "" ?> </p>
                        </a>
                    </div>
                    <?php
                        }
                        $i++;
                    } ?>
                    <div class="listaG2 grey lighten-5" style="padding-top: 17px;">
                        <a class="clickable desplegar_grupos" style="color: #999; padding-left: 30px"> Mas Grupos... </a>
                    </div>
                    <?php 
                    } 
                    ?>
                </div> 
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m12 grey lighten-5 z-depth-1" style="margin-top: 10px">
                        <div class="row">
                            <div class="col s4 m2"> 
                                <div class="card">
                                    <div class="card-image">
                                        <?php
                                            if(!empty($datos_usuario->mime)){
                                                echo '<img src="'.base_url().'index.php/novios/comunidad/Home/foto_usuario/'.$datos_usuario->id_usuario.'">';
                                            }else{
                                                echo '<img src="'.base_url().'dist/img/blog/perfil.png">';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col s8 m10" style="margin-bottom: 20px">
                                <div style="margin-bottom: 10px">
                                    <div class="col s9 m9" style="font-weight: bold"><p><?php if(!empty($datos_usuario->usuario)) echo $datos_usuario->usuario ?></p></div>   
                                    <div class="col s3 m3 dorado-2-text"><p style="text-align: center"><?php if(!empty($datos_usuario->puntos)) echo "$datos_usuario->puntos Puntos" ?></p></div>
                                </div>
                                <div class="col s12 m12"><p><i class='fa fa-map-marker' aria-hidden='true'></i> <?php
                                    if(!empty($datos_usuario->poblacion) && !empty($datos_usuario->estado)) echo "$datos_usuario->poblacion, $datos_usuario->estado";
                                ?></p></div>
                                <div class="col s12 m12"><p class="link_usuario"><?php echo !empty($datos_usuario->fecha_creacion)? $datos_usuario->fecha_creacion : ""?></p></div>
                                <div class="col s12 m12" style="text-align: justify; margin-bottom: 15px">
                                    <p>
                                    <?php
                                        if(!empty($datos_usuario)){
                                            echo "$datos_usuario->sobre_mi";
                                        }
                                    ?>
                                    </p>
                                </div>
                                <div class="col s12 m6" style="margin-bottom: 10px">
                                    <a class="btn waves-effect waves-light dorado-2 btn_agregar clickable" data-id="agregar-<?php echo !empty($id_usuario)? $id_usuario : ""?>">
                                        <i class="fa fa-user-plus" aria-hidden="true"></i> <text class="agregar-<?php echo !empty($id_usuario)? $id_usuario : "" ?>"><?php echo !empty($amistad)? $amistad : "Enviar Solicitud"?></text>
                                    </a>
                                </div>
                                <div class="col s12 m6">
                                    <button class="btn waves-effect waves-light dorado-2 clickable enviar_mensaje">
                                        <i class="fa fa-envelope" aria-hidden="true"></i> Enviar Un Mensaje
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 white lighten-5 z-depth-1">
                        <ul class="tabs filtro1">
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "perfil") echo 'class="active"'?> href="#">Mi Perfil</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "muro") echo 'class="active"'?> href="#">Mi Muro</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "debates") echo 'class="active"'?> href="#">Debates</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "amigos") echo 'class="active"'?> href="#">Amigos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "fotos") echo 'class="active"'?> href="#">Fotos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "videos") echo 'class="active"'?> href="#">Videos</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "actividad") echo 'class="active"'?> href="#">Actividad</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "medallas") echo 'class="active"'?> href="#">Medallas</a>
                            </li>
                            <li class="tab col s4 m2">
                                <a target="_self" <?php if(!empty($seccion)) if($seccion == "proveedores") echo 'class="active"'?> href="#">Proveedores</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <?php
                    if(!empty($videos)){
                        foreach ($videos as $video){
                            echo '
                            <div class="col s6 m3 foto_publicada">
                                <div class="card small">
                                    <div class="card-image">
                                        <a href="'.base_url().'index.php/novios/comunidad/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'"> <img class="foto_zoom" src="'.$video->direccion_img.'"></a>
                                    </div>
                                    <div class="card-content" style="text-align: center">
                                        <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                            <img alt="" src="'.$video->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                                        </div>
                                        <p class="clickable truncate"><a class="titulo_foto" href="'.base_url().'index.php/novios/comunidad/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'">'.$video->titulo.'</a></p><br/>
                                        <p class="link_usuario">'.$video->usuario.'</p>
                                    </div>
                                </div>
                            </div>';
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m9 pull-left" style="<?php if($total_paginas <= 1) echo "display: none" ?>">
                        <div class="col s12 m9" style="float: right">
                            <ul class="pagination">
                                <li class="<?php echo ($pagina == 1 ? "disabled" : "waves-effect") ?>"><a <?php if($pagina > 1) echo 'href="'.base_url()."index.php/novios/comunidad/perfil/videos/$datos_usuario->id_usuario/".($pagina - 1).'"' ?>><i class="material-icons">chevron_left</i></a></li>
                                <?php 
                                $j = 1;
                                $contador = 0;
                                if($total_paginas > 10 && $pagina > 0 && $pagina > 5) { 
                                    $pagina2 = $pagina + 5;
                                    if($pagina2 <= $total_paginas){
                                        $j = $pagina - 4;
                                    }else{
                                        $j = $pagina - 4;
                                        $j = $j - ($pagina2 - $total_paginas);
                                    }
                                }else{
                                    $j = 1;
                                }
                                for($i=$j; $i <= $total_paginas && $contador < 10; $i++){
                                ?>
                                <li class="<?php echo ($i == $pagina ? "active" : "waves-effect") ?>"><a href="<?php echo base_url()."index.php/novios/comunidad/perfil/videos/$datos_usuario->id_usuario/$i" ?>"><?php echo $i ?></a></li>
                                <?php $contador++; } ?>
                                <li class="<?php echo ($pagina == $total_paginas ? "disabled" : "waves-effect") ?>"><a <?php if($pagina < $total_paginas) echo 'href="'.base_url()."index.php/novios/comunidad/perfil/fotos/$datos_usuario->id_usuario/".($pagina + 1).'"'?>><i class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('ul.tabs').tabs();
        });

        $(document).ready(function(){
            $('.filtro1 a').on("click", function (){
                var opcion = $(this).text();
                switch(opcion){
                    case "Mi Perfil":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/usuario/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Mi Muro":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/mi_muro/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Debates":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/debates/participacion/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Amigos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/amigos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Fotos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/fotos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Videos":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/videos/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Actividad":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/actividad/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Medallas":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/medallas/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                    case "Proveedores":
                        window.location.href = "<?php echo base_url()?>index.php/novios/comunidad/perfil/proveedores/<?php if(!empty($id_usuario)) echo $id_usuario ?>";
                        break;
                }
            }); 
        });
        
        $(document).ready(function (){
            $(".enviar_mensaje").click(function (){
                window.location.href = "<?php echo base_url()."index.php/novios/buzon/nuevo?usuario=$id_usuario"?>";
            });
        });
        
        $(document).ready(function (){
            $('.desplegar_grupos').click(function (){
                var mostrar = $(this).text();
                var display = $('.oculto').css('display');
                if(display == "none"){
                    $('.oculto').css('display','block');
                    $(this).text("Ocoltar Grupos");
                }else{
                    $('.oculto').css('display','none');
                    $(this).text("Mas Grupos...");
                }
            });
        });
        
        $(document).ready(function(){
            $('.btn_agregar').on("click", function(){
                var usuario = $(this).attr("data-id");
                var token = usuario.split('-');
                var tipo = $("."+usuario).text();
                $.ajax({
                    url: '<?php echo base_url() ?>index.php/novios/comunidad/perfil/solicitudAmistad',
                    method: 'POST',
                    data:{
                        'id_usuario_confirmacion': token[1],
                        'tipo': tipo
                    },
                    success: function(res){
                        if(res.success){
                            $("."+usuario).text(res.data);
                        }
                    },
                    error: function(){
                        alert("Lo sentimos ocurrio un error");
                    }
                });
            });
        });
    </script>
</body>
<?php $this->view('principal/footer'); ?>

