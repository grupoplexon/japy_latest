<?php $this->view('principal/newHeader');?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="<?php echo base_url()?>dist/css/comunidad.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<body>
    <!--BARRA DE TITULO CON UN BUSCADOR DE DEBATES-->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px; padding: 10px;">
            <p>
            <div class="col s12 m2">
                <p style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            </div>
            <form id="buscador" method="get" action="<?php echo base_url()."index.php/novios/moderador/forum/buscar"?>" autocomplete="off">
                <div class="input-field col s7 m5" style="margin:0">
                    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Buscar..."/>
                    <input type="hidden" name="page" value="1"/>
                    <div id="ventana_buscador">
                    </div>
                </div>
                <div id="btn_buscar" class="col s5 m2" style="margin-bottom: 10px">
                    <button type="submit" class="btn waves-effect waves-light dorado-2"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                </div>
            </form>
        </p>
        </div>
    </div>
    
    <!--TARJETA CON UN BOTON DICIENDO QUE PUEDES CREAR UN NUEVO DEBATE EN LA COMUNIDAD-->
    <div class="body-container">
        <div class="row">
            <div class="col s12">
                <div class="card grey lighten-5">
                    <div class="card-content">
                        <p class="card-title"> Comunidad</p>
                        Comparte tu experiencia en la red de novios y novias japybodas.com
                    </div>
                    <div class="card-action">
                        <a href="<?php echo base_url() ?>index.php/novios/moderador/Home/nuevoDebate" id="btn_debate" class="btn dorado-2 truncate"> Debate en la comunidad</a>
                    </div> 
                </div>
            </div>
            
            <!--COMPONENTES LATERALES-->
            <div class="col s12 m3 lateral" style="float: right; margin-bottom: 20px">
                
                
                <?php if(!empty($denuncias_debate)){ ?>
                <!--ULTIMAS DENUNCIAS DEBATES, ULTIMAS-->
                <div class="row">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="center visita_perfil dorado-2">
                        <span style="font-weight: 600; font-size: 20px;"> Denuncias debates </span>
                    </div>
                    <div style="overflow:auto; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($denuncias_debate as $debate){
                        ?>
                        <a href="<?php echo !empty($debate->url_denuncia)? $debate->url_denuncia : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($debate->foto_usuario)? $debate->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($debate->todos)? $debate->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
				
				<?php if(!empty($denuncias_comentarios)){ ?>
                <!--ULTIMAS DENUNCIAS DEBATES, ULTIMAS-->
                <div class="row">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="center visita_perfil dorado-2">
                        <span style="font-weight: 600; font-size: 20px;"> Denuncias comentarios debate</span>
                    </div>
                    <div style="overflow:auto; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($denuncias_comentarios as $comentarios){
                        ?>
                        <a href="<?php echo !empty($comentarios->url_denuncia)? $comentarios->url_denuncia : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($comentarios->foto_usuario)? $comentarios->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($comentarios->todos)? $comentarios->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
                
                <?php if(!empty($grupos_miembro)){ ?>
                <!--MIS GRUPOS-->
                <div class="row">
                    <div class="mis_grupos dorado-2 center" style="height: auto; padding: 5px; border: 1px solid #999">
                        <span style="font-weight: 600; font-size: 20px;"> Mis Grupos </span>
                    </div>
                    <div class="mis_grupos grey lighten-5" style="height: auto; border: 1px solid #999; border-top: none">
                        <?php foreach ($grupos_miembro as $grupo){ ?>
                        <a href="<?php echo base_url()."index.php/novios/moderador/group/grupo/$grupo->id_grupos_comunidad/todo"?>">
                        <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen" ?>"> 
                        <p class="nombre_grupo truncate"> <?php echo !empty($grupo->nombre)? $grupo->nombre : "" ?> </p>
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <?php 
                } 
                ?>
                
                <?php if(!empty($grupos)){ ?>
                <!--GRUPOS GENERALES-->
                <div class="row">
                    <div class="listaG2 dorado-2 center" style="height: auto; padding: 5px; padding-left: 10px"><span style="font-weight: 600; font-size: 20px;">Grupos Generales</span></div>
                    <?php
                    $i = 0;
                    foreach ($grupos as $grupo){
                        if($i > 15) break;
                        if($i < 5){
                    ?>
                    <div class="listaG2 grey lighten-5">
                        <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."index.php/novios/moderador/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                            <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen" ?>"> 
                            <p class="nombre_grupo truncate"> <?php echo !empty($grupo)? $grupo->nombre : "" ?> </p>
                        </a>
                    </div>
                    <?php 
                        }else{
                    ?>
                    <div class="listaG2 oculto grey lighten-5">
                        <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."index.php/novios/moderador/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>">
                            <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen" ?>"> 
                            <p class="nombre_grupo truncate"> <?php echo !empty($grupo)? $grupo->nombre : "" ?> </p>
                        </a>
                    </div>
                    <?php
                        }
                        $i++;
                    } ?>
                    <div class="listaG2 grey lighten-5" style="padding-top: 17px;">
                        <a class="clickable desplegar_grupos" style="color: #999; padding-left: 30px"> Mas Grupos... </a>
                    </div>
                    <?php 
                    } 
                    ?>
                </div> 
            </div>
    
            <!----FOTOS Y DEBATES DE LOS USUARIOS ---->
            <div class="col s12 m9 pull-left">
                <?php 
                if(!empty($ultimos_debates)){
                    $i = 1;
                    foreach ($ultimos_debates as $debate){
                        if($i >= 1 && $i <= 3){
                ?>
                <a href="<?php echo !empty($debate->url_debate)? $debate->url_debate : "" ?>">
                    <div id="debate_usuario1" class="debates clickable z-depth-1">
                        <img class="pull-s1 fotos_debate" style="" src="<?php echo !empty($debate->foto_usuario)? $debate->foto_usuario : "" ?>">
                        <div id="datos_debate1">
                            <p class="truncate usuario"><?php echo !empty($debate->usuario)? $debate->usuario : "" ?></p>
                            <p class="titulo_debate"><?php echo !empty($debate->titulo_debate)? $debate->titulo_debate : "" ?></p>
                        </div>
                    </div>
                </a>
                        <?php }else if($i >= 3 && $i <= 6){ ?>
                <a href="<?php echo !empty($debate->url_debate)? $debate->url_debate : "" ?>">
                    <div id="debate_usuario4" class="debates clickable z-depth-1">
                        <img class="fotos_debate" style="float: right" src="<?php echo !empty($debate->foto_usuario)? $debate->foto_usuario : "" ?>">
                        <div class="datos_debate4">
                            <p class="truncate usuario"><?php echo !empty($debate->usuario)? $debate->usuario : "" ?></p>
                            <p class="titulo_debate"><?php echo !empty($debate->titulo_debate)? $debate->titulo_debate : "" ?></p>
                        </div>
                    </div>
                </a>
                <?php 
                        }
                        $i++;
                    }
                }
                ?>
            </div>    
    <!--LISTA DE GRUPOS EN FORMA DE TABLA-->
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <?php if(!empty($grupos)){ ?>
                    <div class="col s9">
                        <h5> Grupos </h5>
                    </div>
                    <div class="col s12" style="margin: 0px; padding: 0px; padding-top: 0px">
                        <ul class="grey lighten-5">
                            <?php
                            $i = 0;
                            foreach ($grupos as $grupo){
                                if($i == 16) break;
                            ?>
                            <li>
                                <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."index.php/novios/moderador/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>"> 
                                    <div class="listaG col s12 m6 grey lighten-5 z-depth-1">
                                        <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen" ?>"> 
                                        <p class="nombre_grupo pull-s1 truncate"> <?php echo !empty($grupo->nombre)? $grupo->nombre : "" ?></p>
<!--                                        <p class="titulo_debate"> <?php // echo !empty($grupo->miembros)? "$grupo->miembros Miembros" : "0 Miembros" ?> </p>-->
                                    </div>
                                </a>
                            </li>
                            <?php
                                $i++; 
                            } ?>
                            <li>
                                <a href="<?php echo base_url() ?>index.php/novios/moderador/Forum/grupos" style="color: #999;">
                                    <div id="mas_grupos" class="listaG col s12 m6 grey lighten-5 z-depth-1"> 
                                        <p style="color: #999;"> Mas Grupos... </p> 
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
            </div>
        
        <!--DEBATES QUE SE HABLA EN LA COMUNIDAD-->
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <?php if(!empty($debates_comentados)){ ?>
                    <div class="col s9">
                        <h5> Que se habla en la comunidad... </h5>
                    </div>
                    <div class="col s9" style="margin-bottom: 20px;">
                        <a href="<?php echo base_url() ?>index.php/novios/moderador/Home/nuevoDebate" class="btn dorado-2"> Nuevo Debate </a>
                    </div>
                    <div id="habla_comunidad1" style="//border: 1px solid #999" class="col s12 m12 grey lighten-5 z-depth-1">
                        <?php
                        $j = count($debates_comentados) - 1;
                        $i = 0;
                        foreach ($debates_comentados as $debate){
                            if($i < $j){
                                echo '  <div class="col s12 m12" style="border-bottom: 1px solid #999; padding-top: 10px">
                                        <div class="col s3 m2">
                                            <div class="card">
                                                <div class="card-imagear">
                                                    <img class="perfil" src="'.$debate->foto_usuario.'">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s6 m8" style="border-right: 1px solid #999; text-align:justify">
                                            <p style="font-size:18px"><a class="titulo_foto" href="'.base_url().'/index.php/novios/moderador/Home/debatePublicado/'.$debate->id_debate.'">'.$debate->titulo_debate.'</a></p>
                                            <p class="">Creado Por <span class="titulo_foto" style="font-weight:600">'.$debate->usuario.'</span> '.$debate->fecha_creacion.'</p>
                                            <p>'.$debate->debate.'</p>
                                        </div>
                                        <div class="col s3 m2" style="text-align:center">
                                            <p><i class="fa fa-comments" aria-hidden="true"></i> '.$debate->num_comentarios.'</p>
                                            <p>Ultimo Mensaje</p>
                                            <p>'.$debate->fecha.'</p>
                                        </div></div>';
                            }
                        }
                        echo    '<div class="col s12 m12" style="text-align:right">
                                    <p>
                                        <a href="'.base_url().'index.php/novios/moderador/Forum/debates" style="color:#999">Ver Debates <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                    </p>
                                </div>';
                        ?>
                        </div>
                        <?php
                        }
                        ?>
                </div>
            </div>
        <!--FIN DEBATES QUE SE HABLA EN LA COMUNIDAD-->
        
        <!--ULTIMAS FOTOS PUBLICADAS-->
            <div class="col s12 m9">
                <?php 
                if(!empty($fotos)){
                ?>
                <div class="row">
                    <div class="col s6 m2" style="margin-top: 25px; float: right">
                        <a href="<?php echo base_url() ?>index.php/novios/moderador/picture/nuevaFoto" class="btn dorado-2"> <i class="fa fa-camera" aria-hidden="true"></i> Subir una Foto </a>
                    </div>
                    <div class="col s6 m7" style="margin-top: 20px">
                        <h5>Ultimas Imagenes Publicadas</h5>
                    </div>
                </div>
                <div class="row">
                    <?php
                    foreach ($fotos as $foto){
                        echo '
                        <div class="col s6 m3 foto_publicada">
                            <div class="card small">
                                <div class="card-image">
                                    <a href="'.base_url().'index.php/novios/moderador/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'"> <img class="foto_zoom" src="'.base_url().'index.php/novios/comunidad/picture/foto/'.$foto->id_foto.'"></a>
                                </div>
                                <div class="card-content" style="text-align: center">
                                    <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                        <img alt="" src="'.$foto->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">     
                                    </div>
                                    <p class="clickable truncate"><a class="titulo_foto" href="'.base_url().'index.php/novios/moderador/picture/fotoPublicada/foto'.$foto->id_grupo.'-g'.$foto->id_foto.'">'.$foto->titulo.'</a></p><br/>
                                    <p class="link_usuario">'.$foto->usuario.'</p>
                                </div>
                            </div>
                        </div>';
                    }
                    echo '
                    <div class="col s6 m3 foto_publicada">
                        <div class="card small fotos_recientes fotos clickable">
                            <div class="card-image" style="text-align: center; font-size: 90px">
                                <img src="'.base_url().'dist/images/comunidad/Fotografia.png">
                            </div>
                            <div class="card-content" style="text-align: center">
                                <p><button type="button" class="btn dorado-2 clickable fotos_recientes">Mas fotos</button></p>
                            </div>
                        </div>
                    </div>';
                    ?>
                </div>
                <?php } ?>
            </div>
			<?php if(!empty($denuncias_img)){ ?>
                <!--ULTIMAS DENUNCIAS DEBATES, ULTIMAS-->
                <div class="col s12 m3">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="center visita_perfil dorado-2">
                        <span style="font-weight: 600; font-size: 20px;"> Denuncias imagenes </span>
                    </div>
                    <div style="overflow:auto; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($denuncias_img as $img){
                        ?>
                        <a href="<?php echo !empty($img->url_usuario)? $img->url_usuario : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($img->foto_usuario)? $img->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($img->todos)? $img->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
				
				<?php if(!empty($denuncias_comentarios_img)){ ?>
                <!--ULTIMAS DENUNCIAS DEBATES, ULTIMAS-->
                <div class="col s12 m3" style="margin-top:20px">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="center visita_perfil dorado-2">
                        <span style="font-weight: 600; font-size: 20px;"> Denuncias comentarios imagenes</span>
                    </div>
                    <div style="overflow:auto; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($denuncias_comentarios_img as $comentarios){
                        ?>
                        <a href="<?php echo !empty($comentarios->url_denuncia)? $comentarios->url_denuncia : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($comentarios->foto_usuario)? $comentarios->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($comentarios->todos)? $comentarios->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
				
				
        <!--FIN ULTIMAS FOTO PUBLICADAS-->    
        
        <!--ULTIMOS VIDESO PUBLICADOS-->
            <div class="col s12 m9">
                <div class="row">
                <?php if(!empty($videos)){ ?>
                    <div class="col s6 m2" style="margin-top: 25px; float: right">
                        <a href="<?php echo base_url() ?>index.php/novios/moderador/video/nuevoVideo" class="waves-effect btn dorado-2"> <i class="fa fa-video-camera" aria-hidden="true"></i> Subir un Video </a>
                    </div>
                    <div class="col s6 m5" style="margin-top: 20px">
                        <h5>Ultimos Videos Publicados</h5>
                    </div>
                </div>
                <div class="row">
                    <?php
                    foreach ($videos as $video){
                        $direccion_img = str_replace("https://www.youtube.com", "https://i.ytimg.com/", $video->direccion_web);
                        $direccion_img = str_replace("embed", "vi", $direccion_img);
						if($direccion_img != $video->direccion_web){
                        $direccion_img = $direccion_img."/default.jpg";}
						else{
							$direccion_img = base_url()."dist/images/comunidad/VideoError.png";
						}
                        echo '
                        <div class="col s6 m3 video_publicado">
                            <div class="card small">
                                <div class="card-image">
                                    <a href="'.base_url().'index.php/novios/moderador/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'"> <img class="video_zoom" src="'.$direccion_img.'"></a>
                                </div>
                                <div class="card-content" style="text-align: center">
                                    <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                         <img alt="" src="'.$video->foto_usuario.'" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">     
                                    </div>
                                    <p class="clickable truncate"><a class="titulo_video" href="'.base_url().'index.php/novios/moderador/video/videoPublicado/video'.$video->id_grupo.'-g'.$video->id_video.'">'.$video->titulo.'</a></p><br/>
                                    <p class=""><a class="link_usuario">'.$video->usuario.'</a></p>
                                </div>
                            </div>
                        </div>';
                    }
                    echo '
                    <div class="col s6 m3 video_publicado">
                        <div class="card small videos_recientes fotos clickable">
                            <div class="card-image" style="text-align: center; font-size: 90px">
                                <img src="'.base_url().'dist/images/comunidad/Video.png">
                            </div>
                            <div class="card-content" style="text-align: center">
                                <p><button type="button" class="btn dorado-2 clickable videos_recientes">Mas videos</button></p>
                            </div>
                        </div>
                    </div>';
                    }
                ?>
                </div>
            </div>
			<?php if(!empty($denuncias_videos)){ ?>
                <!--ULTIMAS DENUNCIAS DEBATES, ULTIMAS-->
                <div class="col s12 m3">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="center visita_perfil dorado-2">
                        <span style="font-weight: 600; font-size: 20px;"> Denuncias videos </span>
                    </div>
                    <div style="overflow:auto; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($denuncias_videos as $video){
                        ?>
                        <a href="<?php echo !empty($video->url_usuario)? $video->url_usuario : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($video->foto_usuario)? $video->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($video->todos)? $video->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
				
				<?php if(!empty($denuncias_comentarios_videos)){ ?>
                <!--ULTIMAS DENUNCIAS DEBATES, ULTIMAS-->
                <div class="col s12 m3" style="margin-top:20px">
                    <div style="height: auto; padding: 5px; padding-left: 10px; border: 1px solid #999" class="center visita_perfil dorado-2">
                        <span style="font-weight: 600; font-size: 20px;"> Denuncias comentarios videos</span>
                    </div>
                    <div style="overflow:auto; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <?php 
                                foreach ($denuncias_comentarios_videos as $comentarios){
                        ?>
                        <a href="<?php echo !empty($comentarios->url_denuncia)? $comentarios->url_denuncia : "" ?>">
                            <div class="col s4 m4">
                                <div class="card">
                                    <div class="card-image">
                                        <img style="width: 100%" src="<?php echo !empty($comentarios->foto_usuario)? $comentarios->foto_usuario : "" ?>">
                                    </div>
                                </div>
                            </div>
                        </a>
                                <?php } ?>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="<?php echo !empty($comentarios->todos)? $comentarios->todos : "" ?>" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>
                </div>
                <?php } ?>
        <!--FIN ULTIMOS VIDEOS PUBLICADOS-->
        
       
        
            <!--<div class="col s12 m9 pull-left">
                <div class="row">
                    <?php if(!empty($grupos)){ ?>
                    <div class="col s9">
                        <h5> Grupos Por Estado </h5>
                    </div>
                    <div class="col s12" style="margin: 0px; padding: 0px; padding-top: 0px">
                        <ul class="grey lighten-5">
                            <?php
                            $i = 0;
                            foreach ($grupos as $grupo){
                                if($i > 32) break;
                                if($i > 16 && $i < 32){
                            ?>
                            <li>
                                <a href="<?php if(!empty($grupo->id_grupos_comunidad)) echo base_url()."index.php/novios/moderador/Group/grupo/$grupo->id_grupos_comunidad/todo" ?>"> 
                                    <div class="listaG col s12 m6 grey lighten-5 z-depth-1">
                                        <img class="imagen_grupos" src="<?php echo base_url()."/dist/images/comunidad/grupos/$grupo->imagen" ?>"> 
                                        <p class="nombre_grupo pull-s1 truncate"> <?php echo !empty($grupo->nombre)? $grupo->nombre : "" ?></p>
                                        <!--<p class="titulo_debate"> <?php // echo !empty($grupo->miembros)? "$grupo->miembros Miembros" : "" ?> </p>-->
                                    </div>
                                </a>
                            </li>
                            <?php
                                }
                                $i++; 
                            } ?>
                            <li>
                                <a href="<?php echo base_url() ?>index.php/novios/moderador/Forum/grupos" style="color: #999;">
                                    <div id="mas_grupos" class="listaG col s12 m6 grey lighten-5 z-depth-1"> 
                                        <p style="color: #999;"> Mas Grupos... </p> 
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
            </div>-->
        </div>
		<ul id="lista_debates">
        <li id="item_debate">
            <a class="enlace_debate" href="">
                <div class="row">
                    <div class="col s4 m3">
                        <div class="card">
                            <div class="card-image">
                                <img class="imagen_debate">
                            </div>
                        </div>
                    </div>
                    <div class="col s8 m9">
                        <p class="titulo_debate truncate"></p>
                        <p class="fecha_debate"></p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    </div>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function (){
        $(".fotos_recientes").click(function (){
            window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Forum/fotos";
        });
        $(".videos_recientes").click(function (){
            window.location.href = "<?php echo base_url()?>index.php/novios/moderador/Forum/videos";
        });
    });
    
	$(document).ready(function (){
        var activo = 0;
        var activo2 = 0;
        var height = 0;
        var bandera = true;
        
        $("input[name=buscar]").keydown(function(evt){
			
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
			
            if(key == 40){
				
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo++;
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo > 1){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height > 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
					activo2 = 1;
                }else{
                    if(bandera){
                        activo++;
                        bandera = false;
                    }
                    activo--;
					activo2 = 0;
                }
				
            }else if(key == 38){
				
                if($('.item-'+activo).hasClass("hover")){
                    $('.item-'+activo).removeClass("hover");
                }
                activo--;
				
                if($('.item-'+activo).html() != undefined){
                    $('.item-'+activo).addClass("hover");
                    if(activo >= 0){
                        height = (activo-1) * $('.item-'+activo).height();
                        if(height >= 0){
                            $("#ventana_buscador").scrollTop(height);
                        }
                    }
					activo2 = 1
                }else{
					
                   activo = 0;
				   activo2 = 0;
                }
            }else if(key == 13){
				if(activo2!=0){
                $("#buscador").on('keypress',function(e){
                    e.preventDefault();
                    return false;
                });
                window.location.href = $('.item-'+activo+' a').attr("href");
				}
				}else if(key != 39 && key != 37){
                activo = 0;
                grupos();
            }
        });
        
        $("input[name=buscar]").keyup(function(evt){
            var titulo = $("input[name=buscar]").val();
            var key = evt.keyCode || evt.which;
            $("#ventana_buscador").show();
            if(titulo != "" && key != 38 && key != 40 && key != 39 && key != 37){
                activo = 0;
                debates(titulo);
            }else if(key == 13){
                window.location.href = $('.item-'+activo+' a').attr("href");
            }
        });
        
        $(document).on('mouseenter','#ventana_buscador > ul > li',function(){
            $('.item-'+activo).removeClass("hover");
            var clase = $(this).attr("class");
            var token = clase.split("-");
            activo = token[1];
            $(this).addClass("hover");
        });
       
        $("body").on('click',function(){
            $("#ventana_buscador").hide();
            $('.item-'+activo).removeClass("hover");
        });
        
        $("input[name=buscar]").on('click', function (e){
            e.stopPropagation();
            $('.item-'+activo).removeClass("hover");
            activo = 0;
            var titulo = $("input[name=buscar]").val();
            if(titulo == "" && $("#ventana_buscador ul").html() == undefined){
                grupos();
            }
            $("#ventana_buscador").show();
        });
        
        $("#ventana_buscador").on('mouseleave',function(){
            setTimeout(function(){
                $('.item-'+activo).removeClass("hover");
            },1000);
        });
        
        function grupos(){
            $.ajax({
                url: '<?php echo base_url()."index.php/novios/moderador/home/getGrupos"?>',
                success: function(res) {
                    var val = Array();
					
                    if(res.success){
                        $("#ventana_buscador").html("");
						
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        for(var aux in res.data){
							
                            if(res.data.hasOwnProperty(aux)){
								
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_grupo);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].imagen);
                                $(li).find(".titulo_debate").text(res.data[aux].nombre);
                                $(li).find(".fecha_debate").text(res.data[aux].debates+" Debates");
                                $(ul).append(li);
                                if(i == 16){
									console.log(ul);
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").data("grupos","true");
                        $("#ventana_buscador").show();
                    }
                }          
            });
			
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
        
        function debates(titulo){
            $.ajax({
                url: '<?php echo base_url()."index.php/novios/moderador/home/buscar" ?>',
                method: 'post',
                data:{
                    'titulo_debate': titulo
                },
                success: function(res){
                    if(res.success){
                        $("#ventana_buscador").html("");
                        var i = 1;
                        var ul = document.createElement("ul");
                        $(ul).html($("#lista_debates").html());
                        var val = Array();
                        for(var aux in res.data){
                            if(res.data.hasOwnProperty(aux)){
                                var li = document.createElement("li");
                                $(li).html($("#item_debate").html());
                                $(li).addClass("item-"+i);
                                $(li).find(".enlace_debate").attr('href',res.data[aux].enlace_debate);
                                $(li).find(".imagen_debate").attr('src',res.data[aux].foto_usuario);
                                $(li).find(".titulo_debate").text(res.data[aux].titulo_debate);
                                $(li).find(".fecha_debate").text(res.data[aux].fecha_creacion);
                                $(ul).append(li);
                                if(i > (res.data.length - 1)){
                                    break;
                                }
                                i++;
                            }
                        }
                        $("#ventana_buscador").append($(ul));
                        $("#ventana_buscador").show();
                    }
                }
            });
            $("#item_debate").hide();
            $("#ventana_buscador").empty();
        }
    });
	
    $(document).ready(function (){
        $('.desplegar_grupos').click(function (){
            var mostrar = $(this).text();
            var display = $('.oculto').css('display');
            if(display == "none"){
                $('.oculto').css('display','block');
                $(this).text("Ocoltar Grupos");
            }else{
                $('.oculto').css('display','none');
                $(this).text("Mas Grupos...");
            }
        });
    });
    $("input[name=buscar]").keyup(function(){
        var titulo = $("input[name=buscar]").val();
        /*if()*/
    });
</script>
    <?php $this->view('principal/footer'); ?>
</body>
