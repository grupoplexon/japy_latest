<?php $this->view('principal/newHeader'); ?>
<?php $this->view('principal/novia/menu'); ?>
<body>
    <style>
        .cabecera{
            width: 100%;
            height: 20%;
        }
        .imagen_grupos{
            width: 40px;
            height: 40px;
            margin-left: 15px;
            margin-right: 5px;
        }
        .nombre_grupo{
            position: relative;
            margin-left: 65px;
            margin-top: -25px;
        }
        .video_zoom{
            width: 200px;
            height: 200px;
        }
        .video_zoom:hover{
            transform : scale(1.3);
            -moz-transform : scale(1.3); /* Firefox */
            -webkit-transform : scale(1.3); /* Chrome - Safari */
            -o-transform : scale(1.3); /* Opera */
            -ms-transform : scale(1.3); /* IE9 */
        }
        .titulo_video{
            text-decoration: none; 
            color: #f9a797!important;
            z-index: 200;
        }
        .link_usuario{
            text-decoration: none;
            color: #75767a!important;
            z-index: 200;
        }
    </style>
<body>
    <!--BARRA DE TITULO CON UN BUSCADOR DE DEBATES-->
    <div class="row">
        <div id="titulo_debate" class="body-container" style="margin-top: -20px;padding: 10px;">
            <p class="col s4 m2" style="font-size: 18px;font-weight: 500;"> Buscar Debate: </p>
            <div id="texto_buscar" class="input-field col s9 m5" style="margin:0">
                <input type="text" name="buscar" class="form-control" placeholder="Buscar..."/>
            </div>
            <div id="btn_buscar" class="col s4 m2">
                <button type="submit" class="btn dorado-2 truncate"> <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
            </div>
        </div>
    </div>

    <div class="body-container">
        <div class="row">
            <div class="col s12 m3" style="float: right">
                <!--IMAGEN DE PLANTILLAS PARA CLUBNUPCIAL-->
                <div class="row">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?php echo base_url() ?>dist/images/comunidad/organizador-boda2.jpg">
                        </div>
                        <div class="card-content" style="text-align: center">
                            <p style="font-size: 16px; padding: 10px;margin-bottom: 10px; line-height: 20px" class="grey-text">Descubre lo f&aacute;cil y r&aacute;pido que es organizar tu boda en clubnupcail.com</p>

                        </div>
                    </div>
                </div>
                <div class="col s12 m12">
                    <!--FECHA DE MI BODA CON LOS USUARIOS QUE COINCIDEN-->
                    <div class="texto_lateral grey lighten-5" style="height: auto; border: 1px solid #999; text-align: center; font-weight: bold;">
                        <p><?php if (!empty($fecha_boda2)) echo $fecha_boda2 ?></p>
                        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/agenda_novia/vineta.png" alt="">
                        <p><?php if (!empty($estado_boda)) echo $estado_boda ?></p>
                        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/agenda_novia/vineta.png" alt="" style="margin-bottom: 10px">
                    </div>
                    <div  class="texto_lateral dorado-2" style="height: 70px; border: 1px solid #999; margin-top: -1px; text-align: center; font-weight: bold">
                        <p><?php //echo $relacion  ?> novios se casan el mismo dia que tu</p>
                    </div>
                    <div class="texto_lateral grey lighten-5" style="height: auto; border: 1px solid #999;margin-top: -1px; text-align: center">
                        <img  style="margin: 0px; margin-top:10px; margin-left: 0%;  width: 70px" class="" src="<?php echo base_url() ?>/dist/images/comunidad/pareja5.jpeg">
                        <img  style="margin: 0px; margin-top:10px; width: 70px" class="" src="<?php echo base_url() ?>/dist/images/comunidad/pareja5.jpeg">
                        <a href="<?php echo base_url() ?>index.php/novios/comunidad/Home/conocerCompaneros" class="btn dorado-2 truncate" 
                           style="margin-top: 0px;margin-bottom: 28px; <?php //if($relacion == 1)echo "display:none" ?>"> Conocerlos </a>
                    </div>

                    <!--QUIEN VISITA MI PERFIL, CON LOS USUARIOS QUE LO HACEN-->
                    <div style="margin-top: 20px; height: auto; padding: 5px;padding-left: 10px; border: 1px solid #999" class="visita_perfil dorado-2">
                        <h5> ¿Quien Visita Tu Perfil? </h5>
                    </div>
                    <div style="height: 120px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="#!"> <img  style="margin-top:10px; margin-left: 15px; width: 70px;" class="" src="<?php echo base_url() ?>/dist/images/comunidad/pareja5.jpeg"> </a>
                    </div>
                    <div style="height: 50px; border: 1px solid #999; border-top: none" class="visita_perfil grey lighten-5">
                        <a href="#!" style="float: left; margin-top: 15px; margin-left: 25px; color:grey"> Ver Todos <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>
                    </div>

                    <!--MIS GRUPOS A LOS QUE ESTOY SUSCRITO-->
                    <div class="mis_grupos dorado-2" style="padding: 5px; margin-top: 20px; border: 1px solid #999">
                        <h5> Mis Grupos </h5>
                    </div>
                    <div class="mis_grupos grey lighten-5" style="height: 50px; border: 1px solid #999; border-top: none">
                        <img class="imagen_grupos" src="<?php echo base_url() ?>/dist/images/comunidad/grupos/jalisco.png"> 
                        <p class="nombre_grupo"> Jalisco </p>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row">
                    <div class="col s12 m12">
                        <h5 style="border-bottom: 1px solid #999">Fotos de la Comunidad </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m12">
                        <img class="cabecera z-depth-1" src="<?php echo base_url() ?>/dist/images/comunidad/pareja9.jpg">
                    </div>
                </div>
                <div class="row">
                    <div class="col s6 m7" style="<?php if (!empty($videos_recientes)) echo "display:block";
else echo "display:none" ?>">
                        <h5>Ultimos Videos Publicados </h5>
                    </div>
                    <div class="col s6 m2" style="float: right">
                        <a href="<?php echo base_url() ?>index.php/novios/comunidad/video/nuevoVideo" class="btn dorado-2"> <i class="fa fa-video-camera" aria-hidden="true"></i> Subir un Video </a>
                    </div>
                </div>
                <div class="row">
                    <?php
                    if (!empty($videos_recientes)) {
                        foreach ($videos_recientes as $video) {
                            $direccion_img = str_replace("https://www", "//img", $video->direccion_web);
                            $direccion_img = str_replace("embed", "vi", $direccion_img);
                            $direccion_img = $direccion_img . "/default.jpg";
                            echo '
                            <div class="col s6 m3 video_publicado">
                                <div class="card small">
                                    <div class="card-image">
                                        <a href="' . base_url() . 'index.php/novios/comunidad/video/videoPublicado/video' . $video->id_grupo . '-g' . $video->id_video . '"> <img class="video_zoom" src="' . $direccion_img . '"></a>
                                    </div>
                                    <div class="card-content" style="text-align: center">
                                        <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                            <img alt="" src="' . base_url() . 'index.php/novios/comunidad/Home/foto_usuario/' . $video->id_usuario . '" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                                        </div>
                                        <p class="clickable truncate"><a class="titulo_video" href="' . base_url() . 'index.php/novios/comunidad/picture/fotoPublicada/foto' . $video->id_grupo . '-g' . $video->id_video . '">' . $video->titulo . '</a></p><br/>
                                        <p class="clickable"><a class="link_usuario">' . $video->usuario . '</a></p>
                                    </div>
                                </div>
                            </div>';
                        }
                        echo '
                        <div class="col s6 m3 video_publicado">
                            <div class="card small videos_recientes fotos clickable">
                                <div class="card-image" style="text-align: center; font-size: 90px">
                                    <img src="' . base_url() . 'dist/images/comunidad/Video.png">
                                </div>
                                <div class="card-content" style="text-align: center">
                                    <p><button type="button" class="btn dorado-2 clickable videos_recientes">Mas Videos</button></p>
                                </div>
                            </div>
                        </div>';
                    }
                    ?>
                </div>
                <div class="row">
                    <div class="col s12 m12" style="<?php if (!empty($videos_vistos)) echo "display:block";
                    else echo "display:none" ?>">
                        <h5>Videos Mas Visitados</h5>
                    </div>
                </div>
                <div class="row">
                    <?php
                    if (!empty($videos_vistos)) {
                        foreach ($videos_vistos as $video) {
                            $direccion_img = str_replace("https://www", "//img", $video->direccion_web);
                            $direccion_img = str_replace("embed", "vi", $direccion_img);
                            $direccion_img = $direccion_img . "/default.jpg";
                            echo '
                            <div class="col s6 m3 video_publicado">
                                <div class="card small">
                                    <div class="card-image">
                                        <a href="' . base_url() . 'index.php/novios/comunidad/video/videoPublicado/video' . $video->id_grupo . '-g' . $video->id_video . '"> <img class="video_zoom" src="' . $direccion_img . '"></a>
                                    </div>
                                    <div class="card-content" style="text-align: center">
                                        <div class="col s4" style="position: absolute; width: 100%; height: 5%; margin: 0; margin-left: -20px; margin-top: -40px">
                                            <img alt="" src="' . base_url() . 'index.php/novios/comunidad/Home/foto_usuario/' . $video->id_usuario . '" style="border: 2px solid #999; border-radius: 100px; height: 40px; width: 40px">    
                                        </div>
                                        <p class="clickable truncate"><a class="titulo_video" href="' . base_url() . 'index.php/novios/comunidad/video/videoPublicado/video' . $video->id_grupo . '-g' . $video->id_video . '">' . $video->titulo . '</a></p><br/>
                                        <p class="clickable"><a class="link_usuario">' . $video->usuario . '</a></p>
                                    </div>
                                </div>
                            </div>';
                        }
                        echo '
                        <div class="col s6 m3 video_publicado">
                            <div class="card small videos_vistos fotos clickable">
                                <div class="card-image" style="text-align: center; font-size: 90px">
                                    <img src="' . base_url() . 'dist/images/comunidad/Video.png">
                                </div>
                                <div class="card-content" style="text-align: center">
                                    <p><button type="button" class="btn dorado-2 clickable videos_vistos">Mas Videos</button></p>
                                </div>
                            </div>
                        </div>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(".videos_recientes").click(function () {
                window.location.href = "<?php echo base_url() ?>index.php/novios/comunidad/video/recientes";
            });
            $(".videos_vistos").click(function () {
                window.location.href = "<?php echo base_url() ?>index.php/novios/comunidad/video/vistas";
            });
        });
    </script>
</div>
</div>
</div>
</body>
<?php $this->view('principal/footer'); ?>
