<?php $this->view('principal/header'); ?>
<?php $this->view('principal/novia/menu'); ?>
<style>
    #facebook {
        margin: 0;
        margin-top: -10px;
    }

    #agregar_amigos small, #agregar_foto small, #publicar_permiso small {
        font-size: 16px;
    }

    #agregar_amigos, #agregar_foto, #publicar_permiso {
        margin-bottom: 20px;
    }
    .swal-button:focus{
     background-color:#efefef !important;
    }
</style>
<body>
    <div class="body-container">
        <h5>Ajustes</h5>
        <div class="divider"></div>

        <div class="row">
            <div class="col s12 m3" style="float: right">
                <div id="conf_perfil">
                    <ul class="collection with-header">
                        <li class="collection-header dorado-2 text-white">Configuraci&oacute;n</li>
                        <li class="collection-item grey lighten-5">
                            <a class="black-text" href="<?php echo base_url() ?>novios/perfil"><i
                                        class="fa fa-pencil" aria-hidden="true"></i> Mi Perfil </a>
                        </li>
                        <li class="collection-item grey lighten-5">
                            <a class="black-text" href="<?php echo base_url() ?>novios/perfil/Home/configuracion">
                                <i class="fa fa-cog" aria-hidden="true"></i> Ajustes </a>
                        </li>
                        <!--                    <li class="collection-item grey lighten-5">-->
                        <!--                        <a class="black-text"-->
                        <!--                           href="--><?php //echo base_url() ?><!--novios/perfil/Home/notificaciones"> <i-->
                        <!--                                    class="fa fa-bell" aria-hidden="true"></i> Notificaciones </a>-->
                        <!--                    </li>-->
                    </ul>
                </div>
            </div>
            <div class="col s12 m9 pull-left">
                <!--LOGIN FACEBOOK-->
                <div class="row grey lighten-5 z-depth-1" id="facebook" style="display: none;">
                    <h5 class="dorado-2" style="padding: 10px"><i class="fa fa-facebook" aria-hidden="true"></i> <b>Tu
                            Facebook</b></h5>
                    <div id="cuenta_facebook" class="col s6" style="text-align: center; padding: 10px">
                        <p><b> Enlaza tu cuenta japy a tu facebook </b></p> <br/>
                        <div style="width: 80%; margin-left: auto; margin-right: auto">
                            <a id="link" style="margin: 0; width: 70%;<?php if ( ! empty($facebook)) {
                                echo "display: none";
                            } ?>" class="btn btn-block  bg-fb truncate"
                               href="<?php echo $this->facebook->link_facebook(); ?>">
                                <i class="fa fa-facebook-official"></i> Link facebook
                            </a>
                        </div>
                        <div style="padding-left: 22%">
                            <a id="unlink" style="margin: 0; width: 70%;<?php if (empty($facebook)) {
                                echo "display: none";
                            } ?>" class="btn btn-block  bg-fb truncate" href="#">
                                <i class="fa fa-facebook-official"></i> Unlink facebook
                            </a>
                        </div>
                        <p>Solo publicaremos con tu permiso</p>
                        <div id="registrar_contraseña" class="input-field col s6" style="display: none">
                            <input type="password" class="form-control" placeholder="Escribe Contraseña" required/>
                        </div>
                        <div id="confirmar_Rcontraseña" class="input-field col s6" style="display: none">
                            <input type="password" class="form-control" placeholder="Confirmar Contraseña" required/>
                        </div>
                        <div id="botones" class="col s6" style="display: none">
                            <button type="submit" class="dorado-2"> Guardar</button>
                            <a class="clickable"> Cancelar </a>
                        </div>
                    </div>
                    <div id="avisos" class="col s6" style="border-left: 1px solid #999">
                        <ul style="padding: 15px">
                            <li>
                                <div id="agregar_amigos">
                                    <i class="fa fa-user-plus" aria-hidden="true" style="font-size: 20px"> </i>
                                    <small id="invitados"> Podras a&ntilde;adir a tus amigos a la lista de invitados</small>
                                </div>
                            </li>
                            <li>
                                <div id="agregar_foto"><i class="fa fa-file-image-o" aria-hidden="true"
                                                          style="font-size: 20px"> </i>
                                    <small id="click_foto"> Podras agregar tu foto de facebook con un click</small>
                                </div>
                            </li>
                            <li>
                                <div id="publicar_permiso"><i class="fa fa-shield" aria-hidden="true"
                                                              style="font-size: 20px"> </i>
                                    <small id="permiso"> Solo publicaremos con tu permiso</small>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--FINAL LOGIN FACEBOOK-->

                <!--CAMBIAR CORREO Y CONTRASEÑA-->
                <div class="row grey lighten-5 z-depth-1" id="email" style="margin-top: 20px">
                    <div class="col s12 dorado-2">
                        <div class="col s6 text-white" id="titulo1"><h5><i class="fa fa-envelope-o" aria-hidden="true"></i> Tu Correo</h5></div>
                        <div class="col s6 text-white" id="titulo2"><h5><i class="fa fa-key" aria-hidden="true"></i> Tu Contrase&ntilde;a
                            </h5></div>
                    </div>
                    <div id="correo" class="col s6" style="border-right: 1px solid #999">
                        <h6 class="card-panel  light-blue lighten-5">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            Te enviaremos todos los correos a: <b> <?php if ( ! empty($correo)) echo $correo ?> </b>
                        </h6>
                        <form id="formulario1" method="post"
                              action="<?php echo $this->config->base_url() ?>novios/perfil/Home/updateEmail">
                            <div id="modificar_correo" class="input-field col s10" style="display: none">
                                <input type="text" id="nuevo_correo" name="nuevo_correo"
                                       class="form-control validate[required]" placeholder="Escribe Nuevo Correo" required/>
                            </div>
                            <button type="submit" id="modificar_email" class="waves-effect waves-light btn dorado-2"
                                    style="display: none;"/>
                            Modificar </button>
                            <a id="cancelar_modificarC" class="clickable" style="display: none"> Cancelar </a>
                            <a id="mostrar_correo" class="waves-effect waves-light btn dorado-2"> Cambiar Email </a>
                        </form>
                    </div>
                    <div id="contrasena" class="col s6">
                        <form id="formulario2" method="post" data-parsley-validate="true"
                              action="<?php echo base_url() ?>novios/perfil/Home/updatePassword">
                            <div id="nueva_contraseña" class="input-field col s10" style="display: none;margin-top: 0">
                                <input type="password" id="nueva_contrasena" name="nueva_contrasena" minlength="6"
                                       class="validate[required] form-control" placeholder="Escribe Nueva Contraseña"
                                       required=""/>
                            </div>
                            <div id="confirmar_contraseña" class="input-field col s10" style="display: none; margin-top: 0">
                                <input type="password" id="confirmar_contrasena" name="confirmar_contrasena" minlength="6"
                                       data-parsley-equalto="#nueva_contrasena" class="validate[required] form-control" placeholder="Confirma Nueva Contraseña"
                                       required=""/>
                            </div>
                            <button type="submit" id="modificar_password" class="waves-effect waves-light btn dorado-2"
                                    style="display: none;margin-top: 15px"> Guardar
                            </button>
                            <a id="cancelar_modificarP" class="clickable" style="display: none"> Cancelar </a>
                            <div class="div_password" style="margin-top: 80px;">
                                <a id="mostrar_password" class="waves-effect waves-light btn dorado-2"> Modificar contrase&ntilde;a </a>
                            </div>
                        </form>
                    </div>
                </div>
                <!--FINAL CAMBIAR CORREO Y CONTRASEÑA-->

                <!--        <div id="cuentas_vinculadas">
                            <h4> <i class="fa fa-link" aria-hidden="true"></i> Tus Cuentas Vinculadas </h4>
                            <h6> S&oacute;lo se comparten las herramientas de organizaci&oacute;n de boda. Ni tu actividad en la comunidad ni tus vestidos son visibles
                                para la otra persona. Ten en cuenta que al hacer t&uacute; la petici&oacute;n de vincular, tu informaci&oacute;n ser&aacute; la que se mantendr&aacute;,
                                ocultando la de tu pareja.
                            </h6>
                            <h6> <b> Correo De La Cuenta Que Deseas Vincular </b> </h6>
                            <div class="input-field">
                                <input type="text" class="form-control"/>
                            </div>
                            <a href="" class="waves-effect waves-light btn green"> Vincular </a>
                        </div>-->

                <!--PERMISO DE VISIBILIDAD PARA TODOS-->
                <div class="row grey lighten-5 z-depth-1" id="visibilidad" style="display: none">
                    <h5 class="dorado-2" style="padding: 10px"><i class="fa fa-eye-slash" aria-hidden="true"></i> <b>Permiso
                            de visibilidad</b></h5>
                    <form method="post" action="<?php echo base_url() ?>novios/perfil/Home/permisoVisibilidad">
                        <div class="row">
                            <div id="permiso_visibilidad" style="display: block; position: relative; margin-bottom: 10px"
                                 class="col s12">
                                <p>
                                    <input type="checkbox" name="visibilidad_todos" value="aceptar"
                                           id="check" <?php if ( ! empty($visibilidad_todos) && $visibilidad_todos == 1) echo "checked" ?>>
                                    <label style="display: block; margin-bottom: 10px;" for="check"> <b> Cualquiera podra
                                            enviarme mensajes dentro de la comunidad
                                            (si no solo mis contactos podran enviarme mensajes).</b>
                                    </label><br/>
                                    <input type="hidden" value="" name="culto"></input>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div id="btn_visibilidad" style="margin-bottom: 10px" class="col s4">
                                <button type="submit" class="waves-effect waves-light btn dorado-2"> Modificar Visibilidad
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!--FINAL PERMISO DE VISIBILIDAD PARA TODOS-->

                <!--DAR LA CUENTA DE BAJA-->
                <div class="row grey lighten-5 z-depth-1" id="baja_cuenta">
                    <h5 class="dorado-2 text-white" style="padding: 10px"><i class="fa fa-trash" aria-hidden="true"></i> Baja De La
                        Cuenta</h5>
                    <form action="<?php echo $this->config->base_url() ?>novios/perfil/Home/deleteCount"
                          method="post" id="formulario3">
                        <h6 class="col s12 "><b> Esta opci&oacute;n eliminara todos tus datos de japybodas.com </b></h6>
                        <div class="col s12" style="margin-top: 10px; margin-bottom: 10px">
                            <button type="submit" id="eliminar" class="btn dorado-2"> Darme De Baja</button>
                        </div>
                    </form>
                </div>
                <!--FINAL DAR DE BAJA LA CUENTA-->
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url() ?>dist/js/configuracion_perfil.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            const $emailForm = $('#formulario1').parsley();
            $('#formulario2').parsley();

            $('#modificar_email').on('click', function(e) {

                e.preventDefault();

                $emailForm.validate();

                if ($emailForm.isValid()) {
                    $.ajax({
                        url: '<?php echo base_url()."registro/checkUsuario" ?>',
                        method: 'post',
                        data: {
                            'correo': $('#nuevo_correo').val(),
                        },
                        success: function(res) {
                            if (res.data == true) {
                                swal('Error', 'Correo registrado anteriormente.', 'error');
                                return false;
                            }

                            $('#formulario1').submit();
                        },
                        error: function() {
                            swal('Error', 'Lo sentimos ocurrio un error', 'error');
                        },
                    });
                }
            });

            $('#eliminar').on('click', function(e) {
                e.preventDefault();
                swal({
                    title: 'Estás segura de proceder?',
                    text: 'Sí aceptas,no podrás volver acceder a tú cuenta de Japy!!',
                    icon: 'warning',
                    buttons: ['Cancelar!', 'Si, adelante!'],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: $('#formulario3').attr('action'),
                            method: 'post',
                            success: function(res) {
                                if (res.data == true) {
                                    $('#formulario3').submit();
                                    window.location.href = '/';
                                }
                            },
                            error: function() {
                                swal({
                                    title: 'Error',
                                    text: 'No fue posible elimianr tu cuenta',
                                    icon: 'error',
                                });
                            },
                        });
                    }
                });
            });
        });
    </script>
    <?php $this->view('japy/prueba/footer'); ?>
</body>
