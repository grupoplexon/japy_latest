<head>
    <title>
        Modificar Perfil
    </title>
    <?php $this->view('principal/header');
    $this->view('general/newheader'); ?>
    <?php $this->view('principal/novia/menu'); ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <style>

        #progreso {
            margin: 0 auto;
            width: 150px;
            height: 150px;
        }

        #datos_perfil {
            margin: 0;
            margin-top: 0px;
        }

        #datos_perfil2 {
            margin-top: -10px;
        }

        .eliminar-foto {
            text-overflow: ellipsis;
            overflow: hidden;
        }

        #foto_perfil {
            padding-left: 10px;
        }

        #color_boda, #temporada_boda, #estilo_boda {
            border-radius: 100px;
            box-shadow: 2px 2px 5px #424242;
            width: 100px;
            height: 100px;
        }

        .inspiraciones {
            margin-top: 20px;
            /*text-align: center;*/
            line-height: 2;
        }

        .formulario select {
            margin-bottom: 15px;
            margin-top: 0;
            height: 50px;
            background-color: #FFF !important;
            color: black !important;
            box-shadow: 3px 3px 9px -3px grey !important;
            padding: 15px !important;
            border: none !important;
        }

        .menu-edit-boda2 {
            position: absolute;
            margin: 0;
            font-size: 17px;
            border: 1px solid black;
            border-radius: 16px;
            padding: 3px;
            top: 76px;
            /*right: 110px;*/
            cursor: pointer;
            background: red;
            color: white;
            border: none;
        }

        .dz-success-mark, .dz-error-mark, .dz-size {
            display: none;
        }

        .error_validate {
            color: red;
            font-size: 11px;
        }

        .error_field {
            border-color: red !important;
        }

        .ui-datepicker-header, .ui-state-default {
            background: #f5c564 !important;
        }

        .ui-datepicker {
            background: #f2f2f2 !important;
        }

        .ui-state-active {
            background: #fff !important;
        }

        #ui-datepicker-div {
            display: none !important;
        }

        .select-dropdown {
            background: white !important;
            border-radius: 4px !important;
            border: 1px solid #ccc !important;
            padding: 0 5px !important;
        }

        .caret {
            color: #00bcdd !important;
            font-size: 14px !important;
            z-index: 5 !important;
            margin-right: 15px !important;
        }

        .location {
            display: flex;
            justify-content: space-between;
        }

        select.countries, select.states, select.cities {
            margin-top: 10px !important;
            margin-bottom: 0px !important;
            height: 43px !important;
            border: 1px solid #ccc !important;
            border-radius: 3px !important;
            width: 30% !important;
        }

        @media only screen and (max-width: 425px) {
            select.countries, select.states, select.cities {
                width: 100%;
            }
        }

        .input-field {
            margin-top: 0 !important;
        }

        #foto {
            height: 100%;
            max-height: 250px;
        }
        p{
            color: #515055;
        }
        a {
            color: #515055;
        }

        .dz-preview, .dz-image img, .upload img {
            width: 100% !important;
            height: 100% !important;
        }

    </style>
</head>
<body>
    <div class="body-container">
        <?php
        $mensaje      = isset($mensaje) ? $mensaje : $this->session->userdata('mensaje');
        $tipo_mensaje = isset($tipo_mensaje) ? $tipo_mensaje : $this->session->userdata('tipo_mensaje');
        $this->session->unset_userdata('mensaje');
        $this->session->unset_userdata('tipo_mensaje');
        if (strcasecmp($tipo_mensaje, "success") === 0) {
            $tipo_mensaje = "teal";
        } elseif (strcasecmp($tipo_mensaje, "info") === 0) {
            $tipo_mensaje = "yellow lighten-5";
        } else {
            $tipo_mensaje = "orange";
        }

        if (isset($mensaje)) {
            ?>
            <div class="row">
                <div class="col s12 ">
                    <div class="card-panel white-text <?php echo $tipo_mensaje ?> ">
                        <i class="material-icons right clickable" onclick="$(this).parent().parent().parent().remove()">close</i>
                        <?php
                        if (is_array($mensaje)) {
                            echo "<ul>";
                            foreach ($mensaje as $key => $value) {
                                if (strpos($value, "no_invitado")) {
                                    $value = str_replace("no_invitado", "Numero de invitados", $value);
                                }
                                echo "<li>$value</li>";
                            }
                            echo "</ul>";
                        } else {
                            echo $mensaje;
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <h5 class="section-title">Modificar Perfil</h5>
        <div class="divider"></div>

        <div class="row">
            <div class="col s12 m3" style="float: right">
                <div id="conf_perfil">
                    <ul class="collection with-header">
                        <li class="collection-header dorado-2 white-text">Configuraci&oacute;n</li>
                        <li class="collection-item grey lighten-5">
                            <a class="black-text" href="<?php echo base_url() ?>novios/perfil"><i
                                        class="fa fa-pencil" aria-hidden="true"></i> Mi Perfil </a>
                        </li>
                        <li class="collection-item grey lighten-5">
                            <a class="black-text" href="<?php echo base_url() ?>novios/perfil/Home/configuracion">
                                <i class="fa fa-cog" aria-hidden="true"></i> Ajustes </a>
                        </li>
                        <!--                    <li class="collection-item grey lighten-5">-->
                        <!--                        <a class="black-text"-->
                        <!--                           href="--><?php //echo base_url() ?><!--novios/perfil/Home/notificaciones"> <i-->
                        <!--                                    class="fa fa-bell" aria-hidden="true"></i> Notificaciones </a>-->
                        <!--                    </li>-->
                    </ul>
                </div>
                <div id="porcentaje" class="card-panel grey lighten-5" style="display: none;">
                    <b>
                        <h10>Completar Perfil</h10>
                    </b><br/><br/>
                    Si deseas llegar al 100% llena todos los campos.
                    <div id="progreso" class="grafica" data-highcharts-chart="2">
                        <div id="highcharts-4" class="highcharts-container"
                             style="position: relative; overflow: hidden; width: 70px; height: 70px; text-align: left; line-height: normal; left: 0.75px; top: 0.43335px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m9 pull-left">
                <div class="row grey lighten-5 z-depth-1" id="datos_perfil">
                    <h5 id="division-datos" class="dorado-2 white-text" style="width: 100%; padding: 10px;">Datos sobre m&iacute;</h5>
                    <form method="post" id="formulario" data-parsley-validate="true"
                          action="<?php echo base_url() ?>novios/perfil/Home/update">
                        <h6 style="padding-left: 20px; " class="col m11 offset-m1 "> Foto de perfil </h6>
                        <div id="foto_perfil" class="col s10 offset-s1 m6 offset-m3  l3 offset-l1 clickable pull-left">
                            <div class="card">
                                <div id="foto" class="upload  z-depth-0 valign-wrapper">
                                    <text class="texto"
                                          style="width: 100%;padding: 28px 20px;text-align: center;color: gray; <?php if ($foto != "" && $foto != null) echo "display:none" ?>">
                                        <i class="fa fa-upload fa-2x valign "></i><br>
                                        Suelte la imagen o haga clic aqu&iacute; para cargar.
                                    </text>
                                    <img id="foto_registrada" class="img"
                                         style="<?php if ($foto == "" || $foto == null) echo "display:none" ?>"
                                         src="<?php if ($foto != "" && $mime != "") echo "data:".$mime.';'."base64,".base64_encode($foto) ?>"/>
                                </div>
                                <input id="file" type="hidden" name="file" class="archivo"
                                       value="<?php if ($foto != "" && $mime != "") echo "data:".$mime.';'."base64,".base64_encode($foto) ?>">
                                <div class="card-action">
                                    <button id="eliminarFoto" type="button" class="btn dorado-2 eliminar-foto"
                                            data-id="datos_perfil"
                                            style="width:100%;"> Eliminar Foto
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="datos_perfil2" class="col l7">
                            <div class="input-field col s12 l12 ">
                                Usuario
                                <input type="text" class="validate form-control" id="user" name="usuario" required=""
                                       value="<?php echo ! empty($usuario) ? $usuario : "" ?>"/>
                            </div>
                            <div class="input-field col s12 m6 l6 ">
                                Nombre
                                <input type="text" class="validate form-control" id="nombre" name="nombre" required=""
                                       value="<?php echo ! empty($nombre) ? $nombre : "" ?>"/>
                            </div>
                            <div class="input-field col s12 m6 l6 ">
                                Apellidos
                                <input type="text" class="validate form-control" id="apellido" name="apellido" required=""
                                       value="<?php echo ! empty($apellido) ? $apellido : "" ?>"/>
                            </div>
                            <div class="input-field col s12 m6 l6">
                                Tel&eacute;fono
                                <input type="text" class="validate[maxSize[20]] form-control" id="telefono" maxlength="20"
                                       name="telefono" value="<?php echo ! empty($telefono) ? $telefono : "" ?>"/>
                            </div>
                            <div class="input-field col s12 m6 l6 ">
                                ¿Eres?
                                <select name="genero">
                                    <?php if ($tipo == 1) { ?>
                                        <option value="1">Novio</option>
                                        <option value="2">Novia</option>
                                    <?php } else { ?>
                                        <option value="2">Novia</option>
                                        <option value="1">Novio</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="input-field col s12 l12">
                                Sobre m&iacute;
                                <textarea id="sobre_mi" class="materialize-textarea form-control" name="sobre_mi"
                                          maxlength="250"
                                          length="250"><?php echo ! empty($sobre_mi) ? $sobre_mi : "" ?></textarea>
                            </div>
                        </div>
                </div>
            </div>
            <div id="datos_perfil4" class="col s12 m9 pull-left">
                <div class="row grey lighten-5 z-depth-1">
                    <h5 id="division-datos" class="dorado-2 white-text" style="padding: 10px">Datos boda</h5>
                    <div class="input-field col s12 m6 l5 offset-l1">
                        Fecha de la Boda
                        <input type="date" class="datepicker col s12 form-control" name="fecha_boda" id="fecha_boda" required="" value="<?php
                        if ($fecha_boda !== '0000-00-00 00:00:00') {
                            $date = date_create($fecha_boda);
                            echo date_format($date, "Y-m-j");
                        } else {
                            echo " ";
                        }
                        ?>"
                               placeholder="" required="" style="background-color: white; text-indent:15px;font-size: 12px; border: 1px solid #f2f2f2;margin-top:0px;margin-bottom: 0">
                    </div>
                    <div class="input-field col s12 m6 l5">
                        Numero de Invitados
                        <input type="text" class="validate form-control" id="no_invitado" maxlength="11" name="no_invitado"
                               value="<?php echo ! empty($no_invitado) ? $no_invitado : "" ?>"/>
                    </div>
                    <div class="col s12 m12 l10 offset-l1" style="margin-bottom:10px ">
                        Ubicación
                        <div class=" location">
                            <select name="country" class="form-control  browser-default countries" id="countryId"
                                    data-default="">
                                <!-- <option value="" disabled selected>-- Pa&iacute;s --</option> -->
                                <option value="142">Mexico</option>
                                <?php foreach ($countries as $country) : ?>
                                    <option value= "<?= $country->id_pais; ?>"><?= $country->nombre; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <select name="state" class="form-control browser-default states" id="stateId"
                                    data-default="">
                                <option value="" disabled selected>-- Estado --</option>
                            </select>
                            
                            <select name="city" id="cityId" class="form-control browser-default cities"
                                    data-default="">
                                <option value="" disabled selected>-- Ciudad --</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-field col s12 m12 l10 offset-l1">
                        Sobre Mi Boda
                        <textarea id="sobre_boda" class="materialize-textarea form-control" name="sobre_boda"
                                  maxlength="250" length="250"
                                  onclick="javascript:cursor()"><?php echo ! empty($sobre_boda) ? $sobre_boda : "" ?></textarea>
                    </div>

                </div>
            </div>
            <div id="datos_perfil5" class="col s12 m9 pull-left">
                <div class="row grey lighten-5 z-depth-1">
                    <h5 id="division-datos" class="dorado-2 white-text" style="padding: 10px;margin-bottom:0 ">Mi inspiraci&oacute;n</h5>
                    <div class="col s4 center-align" style="border-right: 1px solid #fff; position: relative;">
                        <!--  COLOR -->
                        <input type="hidden" name="seleccion-color" id="seleccion-color"
                               value="<?php if ( ! empty($color)) echo $color ?>">
                        <p>COLOR</p>
                        <div id="color-boda" class="principal circle-img <?php echo empty($color) ? '' : $color; ?>"
                             style="<?php echo empty($color) ? 'background-image:url('.base_url().'dist/img/agenda_novia/interrogacion.png); background-size:100%;' : ''; ?>">
                            <i class="material-icons dropdown-button menu-edit-boda2" data-beloworigin="true"
                               data-activates='edit-color'>mode_edit</i></div>
                        <p id="color-boda-name"><?php
                            if ( ! empty($color)) {
                                switch ($color) {
                                    case 'yellow':
                                        echo 'Amarillo';
                                        break;
                                    case 'orange':
                                        echo 'Anaranjado';
                                        break;
                                    case 'blue':
                                        echo 'Azul';
                                        break;
                                    case 'beige':
                                        echo 'Beige';
                                        break;
                                    case 'white':
                                        echo 'Blanco';
                                        break;
                                    case 'white-black':
                                        echo 'Blanco y Negro';
                                        break;
                                    case 'brown':
                                        echo 'Cafe';
                                        break;
                                    case 'golden':
                                        echo 'Dorado';
                                        break;
                                    case 'fucsia':
                                        echo 'Fucsia';
                                        break;
                                    case 'grey':
                                        echo 'Gris';
                                        break;
                                    case 'purple':
                                        echo 'Morado';
                                        break;
                                    case 'black':
                                        echo 'Negro';
                                        break;
                                    case 'silver':
                                        echo 'Plateado';
                                        break;
                                    case 'red':
                                        echo 'Rojo';
                                        break;
                                    case 'pink':
                                        echo 'Rosa';
                                        break;
                                    case 'green':
                                        echo 'Verde';
                                        break;
                                    case 'wine':
                                        echo 'Vino';
                                        break;
                                }
                            }
                            ?></p>
                        <ul id='edit-color' class='dropdown-content edit-boda' data-name="color">
                            <li class="edit-item" data-name="yellow">
                                <div class="circle-img yellow"></div>
                                <small class="truncate">Amarillo</small>
                            </li>
                            <li class="edit-item" data-name="orange">
                                <div class="circle-img orange"/>
                    </div>
                    <small class="truncate">Anaranjado</small>
                    </li>
                    <li class="edit-item" data-name="blue">
                        <div class="circle-img blue"></div>
                        <small class="truncate">Azul</small>
                    </li>
                    <li class="edit-item" data-name="beige">
                        <div class="circle-img beige"></div>
                        <small class="truncate">Beige</small>
                    </li>
                    <li class="edit-item" data-name="white">
                        <div class="circle-img white"></div>
                        <small class="truncate">Blanco</small>
                    </li>
                    <li class="edit-item" data-name="white-black">
                        <div class="circle-img white-black"></div>
                        <small class="truncate">Blanco y Negro</small>
                    </li>
                    <li class="edit-item" data-name="brown">
                        <div class="circle-img brown"></div>
                        <small class="truncate">Cafe</small>
                    </li>
                    <li class="edit-item" data-name="golden">
                        <div class="circle-img golden"></div>
                        <small class="truncate">Dorado</small>
                    </li>
                    <li class="edit-item" data-name="fucsia">
                        <div class="circle-img fucsia"></div>
                        <small class="truncate">Fucsia</small>
                    </li>
                    <li class="edit-item" data-name="grey">
                        <div class="circle-img grey"></div>
                        <small class="truncate">Gris</small>
                    </li>
                    <li class="edit-item" data-name="purple">
                        <div class="circle-img purple"></div>
                        <small class="truncate">Morado</small>
                    </li>
                    <li class="edit-item" data-name="black">
                        <div class="circle-img black"></div>
                        <small class="truncate">Negro</small>
                    </li>
                    <li class="edit-item" data-name="silver">
                        <div class="circle-img silver"></div>
                        <small class="truncate">Plateado</small>
                    </li>
                    <li class="edit-item" data-name="red">
                        <div class="circle-img red"></div>
                        <small class="truncate">Rojo</small>
                    </li>
                    <li class="edit-item" data-name="pink">
                        <div class="circle-img pink"/>
                </div>
                <small class="truncate">Rosa</small>
                </li>
                <li class="edit-item" data-name="green">
                    <div class="circle-img green"></div>
                    <small class="truncate">Verde</small>
                </li>
                <li class="edit-item" data-name="wine">
                    <div class="circle-img wine"></div>
                    <small class="truncate">Vino</small>
                </li>
                </ul>
                <!--  FIN COLOR -->
                <p id="parrafo" class="inspiraciones"> ¿Cu&aacute;l sera el color de tu boda? </p>
            </div>
            <div class="col s4 center-align" style="border-right: 1px solid #fff; position: relative;">
                <!--  TEMPORADA -->
                <input type="hidden" name="seleccion-estacion" id="seleccion-estacion"
                       value="<?php if ( ! empty($estacion)) echo $estacion ?>">
                <p>TEMPORADA</p>
                <div><img id="temporada-boda" class="circle-img"
                          src="<?php echo empty($estacion) ? base_url().'dist/img/agenda_novia/interrogacion.png' : base_url().'dist/img/agenda_novia/temporada/'.$estacion.'.png' ?>"
                          alt=""/>
                    <i class="material-icons dropdown-button menu-edit-boda2" data-beloworigin="true"
                       data-activates='edit-temporada' style="margin-left: -20px">mode_edit</i></div>
                <p id="temporada-boda-name"><?php
                    if ( ! empty($estacion)) {
                        switch ($estacion) {
                            case 'invierno':
                                echo 'Invierno';
                                break;
                            case 'otono':
                                echo 'Oto&ntilde;o';
                                break;
                            case 'primavera':
                                echo 'Primavera';
                                break;
                            case 'verano':
                                echo 'Verano';
                                break;
                        }
                    }
                    ?></p>
                <ul id='edit-temporada' class='dropdown-content edit-boda' data-name="temporada">
                    <li class="edit-item" data-name="invierno">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/invierno.png"
                             alt=""/>
                        <br>
                        <small class="truncate">Invierno</small>
                    </li>
                    <li class="edit-item" data-name="otono">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/otono.png"
                             alt=""/>
                        <br>
                        <small class="truncate">Oto&ntilde;o</small>
                    </li>
                    <li class="edit-item" data-name="primavera">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/primavera.png"
                             alt=""/>
                        <br>
                        <small class="truncate">Primavera</small>
                    </li>
                    <li class="edit-item" data-name="verano">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/temporada/verano.png"
                             alt=""/>
                        <br>
                        <small class="truncate">Verano</small>
                    </li>
                </ul>
                <p id="parrafo2" class="inspiraciones"> ¿Cu&aacute;l sera la estaci&oacute;n del año de tu boda? </p>
                <!-- FIN  TEMPORADA -->
            </div>
            <div class="col s4 center-align" style="position: relative;">
                <!--  HORA -->
                <input type="hidden" name="seleccion-estilo" id="seleccion-estilo"
                       value="<?php if ( ! empty($estilo)) echo $estilo ?>">
                <p>HORA</p>
                <div><img id="hora-boda" class="circle-img"
                          src="<?php echo empty($estilo) ? base_url().'dist/img/agenda_novia/interrogacion.png' : base_url().'dist/img/agenda_novia/estilo/'.$estilo.'.png' ?>"
                          alt=""/>
                    <i class="material-icons dropdown-button menu-edit-boda2" data-beloworigin="true"
                       data-activates='edit-hora' style="margin-left: -20px">mode_edit</i></div>
                <p id="hora-boda-name"><?php
                    if ( ! empty($estilo)) {
                        switch ($estilo) {
                            case 'aire_libre':
                                echo 'Al aire libre';
                                break;
                            case 'campo':
                                echo 'En el campo';
                                break;
                            case 'de_noche':
                                echo 'De noche';
                                break;
                            case 'elegante':
                                echo 'Elegante';
                                break;
                            case 'playa':
                                echo 'En la playa';
                                break;
                            case 'moderna':
                                echo 'Modernas';
                                break;
                            case 'rustica':
                                echo 'R&uacute;sticas';
                                break;
                            case 'vintage':
                                echo 'Vintage';
                                break;
                        }
                    }
                    ?></p>
                <ul id='edit-hora' class='dropdown-content edit-boda' data-name="hora">
                    <li class="edit-item" data-name="aire_libre">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/aire_libre.png"
                             alt=""/>
                        <br>
                        <small class="truncate">Al aire libre</small>
                    </li>
                    <li class="edit-item" data-name="campo">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/campo.png"
                             alt=""/>
                        <br>
                        <small class="truncate">En el campo</small>
                    </li>
                    <li class="edit-item" data-name="de_noche">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/de_noche.png"
                             alt=""/>
                        <br>
                        <small class="truncate">De noche</small>
                    </li>
                    <li class="edit-item" data-name="elegante">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/elegante.png"
                             alt=""/>
                        <br>
                        <small class="truncate">Elegantes</small>
                    </li>
                    <li class="edit-item" data-name="playa">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/playa.png"
                             alt=""/>
                        <br>
                        <small class="truncate">En la playa</small>
                    </li>
                    <li class="edit-item" data-name="moderna">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/moderna.png"
                             alt=""/>
                        <br>
                        <small class="truncate">Modernas</small>
                    </li>
                    <li class="edit-item" data-name="rustica">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/rustica.png"
                             alt=""/>
                        <br>
                        <small class="truncate">R&uacute;sticas</small>
                    </li>
                    <li class="edit-item" data-name="vintage">
                        <img class="circle-img" src="<?php echo base_url() ?>dist/img/agenda_novia/estilo/vintage.png"
                             alt=""/>
                        <br>
                        <small class="truncate">Vintage</small>
                    </li>
                </ul>
                <!--  FIN TEMPORADA -->
                <p class="inspiraciones"> ¿Cu&aacute;l sera el estilo de tu boda? </p>
            </div>
        </div>
    </div>
    </div>
    <div id="botones" class="col s6 m6">
        <div class="row">
            <div class="col m6" style="float: right">
                <button type="submit" id="guardar" class="btn waves-effect waves-light dorado-2 guardar_perfil"> Guardar
                </button>
                <a href="javascript:cancelar()" class="clickable black-text" style="padding: 30px"> Cancelar </a>
            </div>
        </div>
    </div>
    </form>
    </div>
    </div>
    </div>

    <input id="userId" type="hidden" value="<?php echo($this->session->userdata('id_usuario')) ?>">
    <input id="url" type="hidden" value="<?php echo base_url() ?>">

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>dist/js/dropzone.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>dist/js/modificar_perfil.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>dist/js/location.js" type="text/javascript"></script>
    <script>
        $(function() {
            $('.edit-item').on('click', function() {
                switch ($(this.parentElement).data('name')) {
                    case 'color':
                        $('#color-boda').removeAttr('style');
                        $('#color-boda').attr('class', 'principal circle-img ' + $(this).data('name'));
                        $('#color-boda-name').text($((this.children)[2]).text());
                        $('#seleccion-color').val($(this).data('name'));
                        break;
                    case 'temporada':
                        $('#temporada-boda').attr('src', '<?php echo base_url() ?>dist/img/agenda_novia/temporada/' + $(this).data('name') + '.png');
                        $('#temporada-boda-name').text($((this.children)[2]).text());
                        $('#seleccion-estacion').val($(this).data('name'));
                        break;
                    case 'hora':
                        $('#hora-boda').attr('src', '<?php echo base_url() ?>dist/img/agenda_novia/estilo/' + $(this).data('name') + '.png');
                        $('#hora-boda-name').text($((this.children)[2]).text());
                        $('#seleccion-estilo').val($(this).data('name'));
                        break;
                }
            });
        });

        $(document).ready(function() {

            const $form = $('#formulario').parsley();

            $('.datepicker').pickadate({
                selectMonths: true,
                selectYears: 15,
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                today: 'Hoy',
                clear: 'Limpiar',
                close: 'Aceptar',
                format: 'yyyy-mm-dd',
                formatSubmit: 'yyyy-mm-dd',
                min: new Date(),
            });

            $('.guardar_perfil').on('click', function(e) {
                e.preventDefault();

                $form.validate();

                if ($form.isValid()) {
                    $.ajax({
                        url: '<?php echo base_url()."novios/perfil/Home/checkUser" ?>',
                        method: 'post',
                        data: {
                            'usuario': $('#user').val(),
                        },
                        success: function(res) {

                            if (res.data == true) {
                                swal('Error', 'Ya existe un usuario con esos datos.', 'error');
                                location.reload();
                                return false;
                            }

                            $('#formulario').submit();
                        },
                        error: function() {
                            swal('Error', 'Lo sentimos ocurrio un error', 'error');
                        },
                    });

                }
            });


            var location = {};
            $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    // location
                    if($('select[name=country]').children("option:selected").val()){
                        console.log('entrando...');
                        $.ajax({
                            url: '<?= base_url().'/registro/getStates/' ?>'+$('select[name=country]').children("option:selected").val()+'/'+location.state,
                            dataType: 'json',
                            timeout: 8000,
                            success: function(response) {
                                /////////////////////////
                                var myselect = $('<select>');
                                $.each(response.states, function(index, key) {
                                    myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                                });
                                $('#stateId').append(myselect.html());
                                /////////////////////////
                                var myselect = $('<select>');
                                $.each(response.cities, function(index, key) {
                                    myselect.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                                });
                                $('#cityId').append(myselect.html());
                                /////////////////////////
                                // $('#selectCountry').html(location.country_name);
                                $('#stateId').val(response.state.id_estado);
                                $('#cityId').val(location.city);

                                $( "#selectCountry" ).change(function() {
                                    $.ajax({
                                        url: '<?= base_url().'/registro/getStates/' ?>'+$('#selectCountry').val(),
                                        dataType: 'json',
                                        timeout: 8000,
                                        success: function(response) {
                                            // $('#cityId').remove();
                                            $("#stateId").empty();
                                            var myselect = $('<select>');
                                            myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                                            $.each(response.states, function(index, key) {
                                                myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                                            });
                                            $('#stateId').append(myselect.html());
                                        }
                                    });
                                });

                                $( "#stateId" ).change(function() { /// obtiene ciudades
                                    $.ajax({
                                        url: '<?= base_url().'/registro/getCities/' ?>'+$('#stateId').val(),
                                        dataType: 'json',
                                        timeout: 8000,
                                        success: function(response) {
                                            // $('#cityId').remove();
                                            $("#cityId").empty();
                                            var myselect = $('<select>');
                                            myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                                            $.each(response.cities, function(index, key) {
                                                myselect.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                                            });
                                            $('#cityId').append(myselect.html());
                                        }
                                    });
                                });

                                // $( "#cityId" ).change(function() {
                                    
                                // });
                            },
                        });
                    }
                },
            });



        });

        function creat_message(clase1, clase2, mensaje) {
            var text = document.createElement('text');
            $(text).addClass(clase1);
            $(text).addClass(clase2);
            $(text).text(mensaje);
            return text;
        }
    </script>
    <?php $this->view('japy/prueba/footer'); ?>
</body>