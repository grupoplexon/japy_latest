<?php $this->view('principal/header');?>
<?php $this->view('principal/novia/menu');?>
<style>
    [type="checkbox"]+label {
        position: relative;
        padding-left: 35px;
        cursor: pointer;
        display: inline-block;
        height: 25px;
        line-height: 25px;
        font-size: 1rem;
        -webkit-user-select: none;
    }
    .checked{
        position: relative;
        height: auto;
        padding-bottom: 15px;
    }
</style>
<body>
    <div class="body-container">
        <h5>Notificaciones</h5>
        <div class="divider"></div>
        
        
        <div class="row">
            <div class="col s12 m3" style="float: right">
                <div id="conf_perfil">
                    <ul class="collection with-header">
                        <li class="collection-header dorado-2"><b>Configuraci&oacute;n</b></li>
                        <li class="collection-item grey lighten-5"> 
                            <a class="black-text" href="<?php echo base_url() ?>index.php/novios/perfil" ><i class="fa fa-pencil" aria-hidden="true"></i> Mi Perfil </a>
                        </li>
                        <li class="collection-item grey lighten-5"> 
                            <a class="black-text" href="<?php echo base_url() ?>index.php/novios/perfil/Home/configuracion" > <i class="fa fa-cog" aria-hidden="true"></i> Ajustes </a> 
                        </li>
                        <li class="collection-item grey lighten-5"> 
                            <a class="black-text" href="<?php echo base_url() ?>index.php/novios/perfil/Home/notificaciones" > <i class="fa fa-bell" aria-hidden="true"></i> Notificaciones </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col s12 m9 pull-left" style="margin-top: -5px">
                <form method="post" action="<?php echo base_url() ?>index.php/novios/perfil/Home/permisosNotificaciones">
                <!--PERMISOS EMAIL COMUNIDAD-->
                    <div class="row grey lighten-5 z-depth-1" id="comunidad">
                        <h5 class="dorado-2" style="padding: 10px"> <i class="fa fa-comments" aria-hidden="true"></i> <b>Comunidad</b> </h5>
                        <div class="col s12">
                            <h6 class="card-panel light-blue lighten-5 actividad_cuenta"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                <b> Cuando exista actividad en tu cuenta de japy podras recibir estos emails.</b>
                            </h6>
                            <div class="col s12 m6" id="enviar_email" style="padding-bottom: 20px;">
                                <h6> Env&iacute;enme un email cuando... </h6>
                            </div>
                            <div class="col s12 m6" id="respuesta" style="padding-bottom: 30px; border-left: 1px solid #999">
                                <!--- DIVISION --->
                                <p>
                                    <input type="checkbox" name="enviar_mensaje" value="aceptar" id="enviar_mensaje" <?php if(!empty($enviar_mensaje) && $enviar_mensaje == 1) echo "checked" ?>> 
                                    <label for="enviar_mensaje"> Me env&iacute;en un mensaje. </label><br/>
                                </p>
                                <!-- DIVISION -->
                                <p>
                                    <input type="checkbox" name="participacion_debates" value="aceptar" id="participacion_debates" <?php if(!empty($participacion_debates) && $participacion_debates == 1) echo "checked"?>> 
                                    <label for="participacion_debates"> Cuando participen en uno de mis debates. </label><br/>
                                </p>
                                <!-- DIVISION -->
                                <p>
                                    <input type="checkbox" name="valorar_publicaciones" value="aceptar" id="valorar_publicaciones"  <?php if(!empty($valorar_publicaciones) && $valorar_publicaciones == 1) echo "checked"?>> 
                                    <label for="valorar_publicaciones"> Valoren una de mis publicaciones o debates. </label><br/>
                                </p>
                                <!-- DIVISION -->
                                <p>
                                    <input type="checkbox" name="anadir_amigo" value="aceptar" id="anadir_amigo"  <?php if(!empty($anadir_amigo) && $anadir_amigo == 1) echo "checked"?>> 
                                    <label for="anadir_amigo"> Quieran a&ntilde;adirme como amigo. </label><br/>
                                </p>
                                <!-- DIVISION -->
                                <p>
                                    <input type="checkbox" name="aceptar_solicitud" value="aceptar" id="aceptar_solicitud"  <?php if(!empty($aceptar_solicitud) && $aceptar_solicitud == 1) echo "checked"?>> 
                                    <label for="aceptar_solicitud"> Me acepten como amigo. </label><br/>
                                </p>
                                <!-- DIVISION -->
                                <p>
                                    <input type="checkbox" name="mension_posts" value="aceptar" id="mension_posts"  <?php if(!empty($mension_posts) && $mension_posts == 1) echo "checked"?>> 
                                    <label for="mension_posts"> Mensione uno de los posts. </label><br/>
                                </p>
                                <!-- DIVISION -->
                            </div>
                        </div>
                    </div>
                    <!--PERMISOS DE EMAIL-->
                    <div class="row grey lighten-5 z-depth-1">
                        <div class="col s12 dorado-2" id="emails">
                            <div class="col s6" id="titulo1"> <h5> <i class="fa fa-info-circle" aria-hidden="true"></i> <b>Emails Informativos</b> </h5> </div>
                            <div class="col s6" id="titulo2"> <h5> <i class="fa fa-envelope-o" aria-hidden="true"></i> <b>Boletines</b> </h5> </div>
                        </div>
                        <!--PERMISOS EMAIL DIARIO-->
                        <div class="col s12 m6" id="emails_informativos">
                            <h6 class="card-panel light-blue lighten-5 emails_informacion"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                <b> Los emails que se envian diario s&oacute;lo son durante la primera semana en que te unes a japy. </b>
                            </h6>
                            <!-- DIVISION -->
                            <input type="checkbox" name="email_diario" value="aceptar" id="email_diario" <?php if(!empty($email_diario)) echo "checked"?>> 
                            <label for="email_diario"> Recibir los emails diario. </label><br/>
                        </div>
                        <!--PERMISOS NOVEDADES, INFORMACION Y EVENTOS-->
                        <div class="col s12 m6" id="boletines" style="border-left: 1px solid #999">
                            <h6 class="card-panel light-blue lighten-5 emails_informacion"> 
                                <i class="fa fa-info-circle" aria-hidden="true"></i> 
                                <b> Se enviaran emails con las &uacute;ltimas novedades, ofertas, eventos y promociones exclusivas sobre bodas. </b>
                            </h6>
                            <!-- DIVISION -->
                            <div class="row">
                                <p class="checked">
                                    <input type="checkbox" name="email_semanal" value="aceptar" id="email_semanal"  <?php if(!empty($email_semanal) && $email_semanal == 1) echo "checked"?>> 
                                    <label for="email_semanal"> Recibir el email semanal con novedades. </label><br/>
                                </p>
                            </div>
                            <!-- DIVISION -->
                            <div class="row">
                                <p class="checked">
                                    <input type="checkbox" name="invitaciones" value="aceptar" id="invitaciones"  <?php if(!empty($invitaciones) && $invitaciones == 1) echo "checked"?>> 
                                    <label for="invitaciones"> 
                                        Recibir invitaciones para eventos, desfiles y ofertas especiales de japy y empresas colaboradoras.
                                    </label><br/>
                                </p>
                            </div>
                            <!-- DIVISION -->
                            <div class="row">
                                <p class="checked">
                                    <input type="checkbox" name="concursos" value="aceptar" id="concursos"  <?php if(!empty($concursos) && $concursos == 1) echo "checked"?>> 
                                    <label for="concursos"> Recibir informaci&oacute;n sobre concursos de japy. </label><br/>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <button type="submit" name="guardar" id="guardar" class="waves-effect waves-light btn dorado-2"> Guardar </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php $this->view('principal/footer'); ?>
</body>

