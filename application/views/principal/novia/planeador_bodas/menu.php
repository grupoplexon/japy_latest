<?php $controller = $_SERVER['REQUEST_URI'];
$controller       = strtolower($controller); ?>
<section class="grey darken-7">
    <div class="body-container seccion-novia">
        <div class="row hide-on-med-and-down"><a class="col s1 m1-1 <?php echo (strpos($controller, 'home/planeador-bodas')) ? 'hide' : '' ?>" href="<?php echo base_url() ?>home/planeador-bodas">Planeador de Boda</a><a
                    class="col s1 m1-1 <?php echo (strpos($controller, 'home/agenda_tareas')) ? 'active' : '' ?>" href="<?php echo base_url() ?>home/agenda_tareas">Agenda de Tareas</a><a class="col s1 m1-1 <?php echo (strpos($controller, 'home/lista_invitados')) ? 'active' : '' ?>"
                                                                                                                                                                                           href="<?php echo base_url() ?>home/lista_invitados">Lista de Invitados</a><a
                    class="col s1 m1-1 <?php echo (strpos($controller, 'home/control_mesas')) ? 'active' : '' ?>" href="<?php echo base_url() ?>home/control_mesas">Control de Mesas</a><a class="col s1 m1-1 <?php echo (strpos($controller, 'home/proveedores')) ? 'active' : '' ?>"
                                                                                                                                                                                                     href="<?php echo base_url() ?>home/proveedores">Proveedores</a><a
                    class="col s1 m1-1 <?php echo (strpos($controller, 'home/presupuesto')) ? 'active' : '' ?>" href="<?php echo base_url() ?>home/presupuesto">Presupuesto</a><a class="col s1 m1-1 <?php echo (strpos($controller, 'home/vestidos')) ? 'active' : '' ?>"
                                                                                                                                                                                            href="<?php echo base_url() ?>home/vestidos">Mis Vestidos</a><a
                    class="col s1 m1-1 <?php echo (strpos($controller, 'home/portal_web')) ? 'active' : '' ?>" href="<?php echo base_url() ?>home/portal_web">Mi Portal Web</a></div>
    </div>
</section>
<nav class="grey darken-7 hide-on-large-only" style="padding-left: 10%"><a href="#" data-activates="mobile-demo2" class="button-collapse"><i class="material-icons">menu</i></a>
    <ul class="side-nav" id="mobile-demo2">
        <li><a class="col s1 m1-1 active pink-text darken-5 <?php echo (strpos($controller, 'home/planeador-bodas')) ? 'hide' : '' ?>" href="<?php echo base_url() ?>home/planeador-bodas">Planeador de Boda</a></li>
        <li><a class="col s1 m1-1 pink-text darken-5" href="<?php echo base_url() ?>home/agenda_tareas">Agenda de Tareas</a></li>
        <li><a class="col s1 m1-1 pink-text darken-5" href="<?php echo base_url() ?>home/lista_invitados">Lista de Invitados</a></li>
        <li><a class="col s1 m1-1 pink-text darken-5" href="<?php echo base_url() ?>home/control_mesas">Control de Mesas</a></li>
        <li><a class="col s1 m1-1 pink-text darken-5" href="<?php echo base_url() ?>home/proveedores">Proveedores</a></li>
        <li><a class="col s1 m1-1 pink-text darken-5" href="<?php echo base_url() ?>home/presupuesto">Presupuesto</a></li>
        <li><a class="col s1 m1-1 pink-text darken-5" href="<?php echo base_url() ?>home/vestidos">Mis Vestidos</a></li>
        <li><a class="col s1 m1-1 pink-text darken-5" href="<?php echo base_url() ?>home/portal_web">Mi Portal Web</a></li>
    </ul>
</nav>
