<?php $this->view('principal/header'); ?>
<style>
    .parallax-container{
        width: 100%;
    }
    .seccion-novia > .row > .m1-1{
        color: white;
        text-align: center;
        border-right: 1px solid #6c6d6f;
        padding: 10px 10px 10px 10px;
        min-height: 70px;
        text-decoration: none;
    }
    .seccion-novia > .row{
        border-right: transparent;
        margin: 0px;
    }
    .seccion-novia > .row > .m1-1 .active{
        background-color: #6c6d71;
        border-bottom: 5px solid #f9a897;
    }
    .icono-planeador,.titulo_icono,.descripcion_icono{
        text-align: center;
    }
    .descripcion_icono{
        line-height: 15px;
        font-size: 12px;
    }
    .fondo_gradiente{
        background: rgba(158,155,158,1);
        background: -moz-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(158,155,158,1)), color-stop(70%, rgba(158,155,158,1)), color-stop(100%, rgba(94,91,94,1)));
        background: -webkit-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -o-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: -ms-linear-gradient(top, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        background: linear-gradient(to bottom, rgba(158,155,158,1) 0%, rgba(158,155,158,1) 70%, rgba(94,91,94,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9e9b9e', endColorstr='#5e5b5e', GradientType=0 );
        height: 400px;
        overflow: hidden;
    }
    .cel{
        margin: 0;
        margin-left: 10%;
        padding-top: 50px;
    }
    .descarga{
        margin: 0;
        margin-top: 100px;
    }
    @media only screen and (min-width:1281px) { 
        /* hi-res laptops and desktops */ 
        .fondo_gradiente{

        }
    }   
</style>
<body>
    <div class="row" style="margin: 0;width:100%; height:300px; background: url('<?php echo base_url() ?>/dist/img/portal_web/fondo2.png') no-repeat left center #fff;
         background-size: cover;">
        <div class="body-container">
            <div style="color:white; margin-left:10%;" class="col s11 m7 l6">
                <div class="row " style="margin-top:50px; margin-bottom:0">
                    <div class="row valign-wrapper" >
                        <h4 class="valign col s7" style="color:white; line-height: 30px; font-size: 35px; margin: 0; font-weight:bold;">Mi portal web</h4>
                        <div class="valign col s5 m5"><img class="responsive-img" src="<?php echo base_url() ?>/dist/img/portal_web/icono.png" alt="" ></div>
                    </div>
                </div>
                <div class="row col s11 m11 l9" style="margin:0">
                    <p style="font-size: 16px; line-height: 25px; margin: 0;color:white">Un espacio único para ti y tus invitados.</p>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('principal/novia/planeador_bodas/menu.php') ?>
    <div class="row" style="background: url('<?php echo base_url() ?>/dist/img/textura_footer2.png');margin:0">
        <div class="body-container">
            <div class="col s12 center" style="margin-top:35px;">

                <div class="col s2">&nbsp;</div>
                <p class="col s8" style="line-height: 25px;font-size: 18px;">
                    En tu sitio web podr&aacute;s subir fotos previas de ti y tu novio, compartir su historia, estar en contacto con tus invitados, organizar diferentes juegos, informar a tu gente de eventos relacionados con tu boda. Todo esto y más con tu sitio web de japybodas.com.
                </p>
            </div>
            <div class="col s2">&nbsp;</div>
            <div class="col s8 center">
                <img class="responsive-img" src="<?php echo base_url() ?>/dist/img/portal_web/macbook_portal.png" alt="" >
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.parallax').parallax();
        });
    </script>
</body>
<?php $this->view('principal/footer'); ?>

