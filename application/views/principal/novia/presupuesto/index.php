<link href="<?php echo base_url() ?>dist/css/novios/Presupuesto.css" rel="stylesheet" type="text/css"/>
<style>
    .input-group {
        display: flex;
        align-items: center;
        height: 20px;
        margin-top: 10px;
    }

    .input-group input {
        margin: 0;
    }

    .hover-shadow:hover {
        background: #cfcfcf;
    }

    .title-presupuesto {
        border-top-right-radius: 10px;
        padding: 5px;
        width: auto;
        margin-top: -35px;
        cursor: pointer !important;
    }

    .tabla-presupuesto tbody td {
        border-right: 1px solid black;
    }

    input[type=text].formularios {
        margin-top: 17px;
    }

    input:not([type]) {
        border: 0px !important;
    }

    input:not([type]):focus {
        border-bottom: 1px solid #757575 !important;
        box-shadow: 0 1px 0 0 #757575;
    }
    
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
        color: black !important;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
        color: #454545 !important;
    }

    input:not([type]):focus:not([readonly]), input[type=search].formulario:focus:not([readonly]) {
        border-bottom: 1px solid #757575 !important;
        box-shadow: 0 1px 0 0 #757575;
    }

    textarea.nombre {
        background-color: transparent;
        border: none;
        border-radius: 0;
        outline: none;
        height: 20px;
        width: 100%;
        font-size: 1rem;
        margin: 0 0 20px 0;
        padding: 0;
        box-shadow: none;
        box-sizing: content-box;
        transition: all 0.3s;
        resize: unset;
    }

    a.formularios, p.formularios {
        padding-bottom: 12px;
        padding-top: 12px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .pagos {
        padding: 10px;
        background-color: #FFFFFF;
        border-radius: 10px;
    }

    .budget-container {
            background-color: #ffffff !important;
            padding: 5px !important;
            box-shadow: 1px 2px 5px 1px #D8D8D8 !important;
        }
     .date-indicator{
            margin-top: 10px
        } 

    @media only screen and  (min-width: 300px) and  (max-width: 600px){
        .barra ul{
            margin-left: 5% !important; 
            margin-right:5% !important;
        }
         .date-indicator{
            margin-top: 80px
        } 
    }

    @media screen and(max-width: 599px) {
        .barra-lateral {
            top: 0px;
        }
        
    }

    .yellow.darken-5 {
        background: #757575 !important;
        color: #514f50 !important;
    }

    .yellow-text.darken-5 {
        color: #757575 !important;
    }

    .yellow.lighten-5 {
        background: #f4e39b !important;
    }

    .tabs .indicator {
        background: #1c97b3 !important;
    }

    .tab a {
        color: #757575 !important;
        font-weight: 600;
    }

    .pestania {
        background: white !important;
    }

    .seccion-novia > .row > .m1-1.active {
        border-bottom: 5px solid #757575 !important;
    }

    .paginate_button.current {
        background: #514f50 !important;
        color: white !important;
        background-color: #514f50 !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        background: #757575 !important;
        color: white !important;
        background-color: #514f50 !important;
        box-shadow: inset 0 0 3px #514f50 !important;
    }

    .sorting_asc {
        background-image: url('<?php echo base_url() ?>dist/img/data_table/sort_asc-dd.png') !important;
    }

    .sorting_desc {
        background-image: url('<?php echo base_url() ?>dist/img/data_table/sort_desc--.png') !important;
    }

    .proveedor {
        background-image: url('<?php echo base_url() ?>dist/img/iconos/clubnupcial_icons_gold.png') !important;
    }

    .proveedor-cat-salon {
        background-position: -850px 0px !important;
    }

    .proveedor-cat-catering {
        background-position: -850px -850px !important;
    }

    .proveedor-cat-fotografia {
        background-position: 5px 5px !important;
    }

    .proveedor-cat-musica {
        background-position: -300px -560px !important;
    }

    .proveedor-cat-autos {
        background-position: -280px -850px !important;
    }

    .proveedor-cat-invitaciones {
        background-position: -1150px 0px !important;
    }

    .proveedor-cat-recuerdos {
        background-position: -850px -280px !important;
    }

    .proveedor-cat-arreglos {
        background-position: -1150px -280px !important;
    }

    .proveedor-cat-inolvidable {
        width: 250px;
        height: 250px;
        background-position: -1150px -550px !important;
    }

    .proveedor-cat-animacion {
        background-position: -280px -250px !important;
    }

    .proveedor-cat-planner {
        background-position: -550px -850px !important;
    }

    .proveedor-cat-pasteles {
        background-position: -600px -550px !important;
    }

    .proveedor-cat-novia-complementos {
        background-position: 5px -860px !important;
    }

    .proveedor-cat-novio {
        background-position: 5px -555px !important;
    }

    .proveedor-cat-salud-belleza {
        background-position: -850px -550px !important;
    }

    .proveedor-cat-joyeria {
        background-position: -550px 5px !important;
    }

    .proveedor-cat-recepcion {
        width: 250px;
        height: 250px;
        background-position: -550px -280px !important;
    }

    .proveedor-cat-ceremonia {
        width: 250px;
        height: 250px;
        background-position: 10px -270px !important;
    }

    .presupuesto2 {
        background-image: url('<?php echo base_url() ?>dist/img/iconos/iconos-presupuesto.png') !important;
    }

    .presupuesto-pendiente {
        background-position: -245px -265px !important;
    }

    .presupuesto-costo-final {
        background-position: -265px -5px !important;
    }

    .presupuesto-pagado {
        background-position: -0px -235px !important;
    }

    .presupuesto-costo-aproximado {
        width: 250px;
        height: 250px;
        background-position: -5px -5px !important;
    }

    .presupuesto2.x2 {
        -moz-transform: scale(0.16);
        margin-top: -100px;
        margin-left: -100px;
        margin-right: -100px;
        margin-bottom: -100px;
        -webkit-transform: scale(0.16);
    }

    td, th {
        padding: 5px 5px;
        padding-left: 10px;
        font-size: .9rem;
    }

    .btn-favoritos {
        font-size: 12px;
        padding: 0px 16px;
    }

    .tabs .tab .active {
        background: white !important;
        border-bottom: 2px solid #F8DADF !important;
        transition: .2s ease-in-out;
    }

    .block-screen {
        pointer-events: none !important;
        opacity: .4 !important;
    }

    .cover-screen {
        width: 100%;
        height: 100%;
        max-width: 1903px;
        background-color: transparent !important;
        position: absolute;
        box-shadow: none;
    }

    .cover-screen:hover {
        box-shadow: none;
    }

    .titulo {
        padding-left: 20px !important;
    }
    .card{
        color: #818181;
        font-size: 1.1rem;
    }

    td .material-icons {
        font-size: 21px;
    }

    @media screen and (max-width: 375px) and (min-width: 320px){
        .material-icons {
        font-size: 20px;
    }
    }


    @media only screen and (max-width: 321px) {
        .btn-edit {
            justify-content: center !important;
            padding: 0 10px;
        }
    }

    

    @media only screen and (min-width: 322px) and (max-width: 425px) {
        .icon-l {
            padding-left: 17%;
        }

        .btn-edit {
            justify-content: center !important;
            padding: 0 10px;
        }

        .text-presupuestador {
            font-size: 1.9em !important;
        }
        
    }

    @media only screen and (min-width: 426px) and (max-width: 768px) {
        .cost-div {
            padding-left: 17%;

        }
    }

    @media only screen and (min-width: 768px) {
        input[type=text]:focus:not([readonly]), textarea.nombre:focus {
            border-bottom: 1px solid #757575 !important;
            box-shadow: 0 1px 0 0 #757575;
        }

        input[type=text] {
            border: 0px !important;
            border-bottom: 0px !important;
            margin-top: 17px;
        }

        .text-presupuestador {
            font-size: 1.8rem !important;
        }
    }

</style>
<link href="<?php echo base_url() ?>dist/css/iconos-proveedor.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/css/budgetary.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/src/parsley.css">

<body style="background: url('<?php echo base_url() ?>dist/img/textura_footer2.png') repeat;">
    <div class="body-container " style="position: relative">
        <button id="opener_modal_pres" href="#modal-presupuesto" class="btn modal-trigger" style="padding: 0;"></button>
        <input type="hidden" class="tutorial-flag" value="<?php echo $usuario->has_seen_tutorial ?>">
        <div class="row principal-content presupuesto-screen">
            <div class="col s12 m12 l3 right barra">
                <ul class="collection with-header"
                    style="border: 2px solid #818181;border-radius: 15px; ">
                    <li class="collection-item " style=" border-bottom: 0px solid #e0e0e0;">
                        <!-- <div class="row"> -->
                            <div class="titulo">
                                <h5 style="font-size:1.8rem; color:#5a5a5a; ">Presupuestador maestro<br>
                                    <!--                                <small>-->
                                    <!--                                    &nbsp; &nbsp; &nbsp; MI PRESUPUESTO: <b><br/>$-->
                                    <!--                                        <text id="mi_presupuesto">-->
                                    <?php //echo moneyFormat($presupuesto, true) ?><!--</text>-->
                                    <!--                                    </b>-->
                                    <!--                                </small>-->
                                </h5>
                            </div>
                        <!-- </div> -->

                    </li>
                    <li class="collection-item">
                        <div class="row">
                            <div class="col s12 l12 m6">
                                <div class="card hover-shadow clickable z-depth-0 icon-l"
                                     onclick="$('a#btn-tab-presu').click();$('html, body').stop().animate({scrollTop: $('#presupuesto-content').offset().top}, '500', 'swing')">
                                    <!-- <div class="card-content"> -->
                                        <div class="row " style="margin-bottom: 2px">
                                            <div class="col s1">
                                                <i class="presupuesto presupuesto2 presupuesto-costo-aproximado x2"
                                                   style="    margin-left: -130px;"></i>
                                            </div>
                                            <div class="col s10">
                                                Costo aproximado
                                                <span class="hide budget-money"><?php echo $presupuesto ?></span>
                                                <br><b id=""
                                                       class="total-aproximado"><?php echo moneyFormat($presupuesto,
                                                        true) ?></b>
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                            <div class="col s12 l12 m6">
                                <div class="card hover-shadow clickable z-depth-0 icon-l"
                                     onclick="$('a#btn-tab-presu').click();$('html, body').stop().animate({scrollTop: $('#presupuesto-content').offset().top}, '500', 'swing')">
                                    <!-- <div class="card-content"> -->
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col s1">
                                                <i class="presupuesto presupuesto2 presupuesto-costo-final x2"
                                                   style="    margin-left: -130px;"></i>
                                            </div>
                                            <div class="col s10">
                                                Costo final
                                                <br><b class="total-final">$ 0.00</b>
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                            <div class="col s12 l12 m6">
                                <div class="card hover-shadow clickable z-depth-0 icon-l"
                                     onclick="$('a#btn-tab-pagos').click();$('html, body').stop().animate({scrollTop: $('#presupuesto-content').offset().top}, '500', 'swing')">
                                    <!-- <div class="card-content"> -->
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col s1">
                                                <i class="presupuesto presupuesto2 presupuesto-pagado x2"
                                                   style="    margin-left: -130px;"></i>
                                            </div>
                                            <div class="col s10">
                                                Pagado
                                                <br><b class="total-pagado">$ 0.00</b>
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                            <div class="col s12 l12 m6">
                                <div class="card hover-shadow clickable z-depth-0  icon-l"
                                     onclick="$('a#btn-tab-presu').click();$('html, body').stop().animate({scrollTop: $('#presupuesto-content').offset().top}, '500', 'swing')">
                                    <!-- <div class="card-content"> -->
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col s1">
                                                <i class="presupuesto presupuesto2 presupuesto-pendiente x2"
                                                   style="    margin-left: -130px;"></i>
                                            </div>
                                            <div class="col s10">
                                                Pendiente
                                                <br><b class="total-pendiente">$ 0.00</b>
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                            <!-- <div class="col s12 l12 m6 center-align">
                                <!-- <div class="card hover-shadow clickable z-depth-0  icon-l"> -->
                                    <!-- <div class="card-content"> -->
                                        <!-- <div class="row" style="margin-bottom: 2px"> -->
                                        <?php // if(!empty($rutaQR)): ?>
                                        <!-- <img src="<?php //echo $rutaQR ?>" style="width:auto;height:auto;"> -->
                                        <?php //endif; ?>
                                        <!-- </div> -->
                                    <!-- </div> -->
                                <!-- </div> -->
                            <!-- </div> --> 
                        </div>
                    </li>
                    
                </ul>
                
                
            </div>
            
            <div class="col s12 m12 l9 pull-left" id="presupuesto-content">
                <div class="row" style="margin-top: 8px">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s6 secondary-text">
                                <a href="#test1" id="btn-tab-presu">Presupuesto de Boda</a>
                            </li>
                            <li class="tab col s6 secondary-text" style="border-left: 1px solid !important;">
                                <a href="#tab-pagos" id="btn-tab-pagos">Panel de pagos</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12" style="margin-bottom: 30px">
                        <h4 class="center-align primary-text text-presupuestador" style="font-weight: 900;">
                            Presupuestador Inteligente
                        </h4>
                        <div class="d-flex justify-end btn-edit">
                            <a href="<?php echo base_url() ?>novios/presupuesto/Home/editar"
                               class=" waves-effect btn-reset" style="z-index: 0; background: white;font-size: 19px; color: #757575 !important;
                                border-bottom: 2px solid; border-color: #F8DADF;">
                                <i class="fa fa-pencil" aria-hidden="true"></i>Restablecer Presupuesto</a>
                        </div>
                    </div>
                    <div id="test1" class="col s12 hide-on-small-only">
                        <div class="row">
                            <?php
                            $total_aprox = 0;
                            $total_final = 0;
                            foreach ($categories as $category) : ?>
                                <div class="col s12">
                                    <div class="title-presupuesto pestania col s12 m6 l7" style="border-bottom: 2px solid; borde-color: #757575;"
                                         id="<?php echo $category->id ?>">
                                        <p style="color: #757575 !important;">
                                            <img class="pull-left" src="<?php echo base_url() ?>dist/img/iconos/proveedores/<?php echo $category->slug ?>.png"
                                                 alt="<?php echo $category->name ?>"
                                                 style="max-width:35px">
                                            <i class="pull-left"><?php echo ucfirst(strtolower(htmlentities($category->name))) ?></i><i
                                                    class="fa fa-caret-down pull-right" aria-hidden="true"></i>
                                            <span class="aproximado pull-right"></span>
                                        </p>
                                    </div>
                                    <div class="card"
                                         id="cat-<?php echo $category->id/*echo $categoria->id_default_porcentaje*/ ?>"
                                         style="margin-bottom: 70px; width: 100%; overflow-x: auto; ">
                                        <div class="card-content" style="padding: 0px!important;display: none">
                                            <!-- <div class="card-title">
                                                <div class="actions-categoria pull-right" style="font-size: 14px">
                                                    <a data-categoria="<?php echo $category->id/*echo $categoria->id_default_porcentaje*/ ?>"
                                                       data-nombre="<?php echo $category->name/*echo strtolower(str_replace(" ", "-", $categoria->nombre))*/ ?>"
                                                       class="waves-effect btn-anadir  btn-flat" style="">
                                                        <i class="material-icons left">add</i>
                                                        Añadir
                                                    </a>
                                                </div>
                                            </div> -->
                                            <table class="table-presupuesto">
                                                <thead style="overflow-x: auto; box-shadow: 0 3px 3px 0px rgba(0,0,0,.1), 0 1px 4px 0 rgba(0,0,0,.1), 0 0 40px 0 rgba(0,0,0,.1) inset;">
                                                <tr style="bacground: #e2e2e2;">
                                                    <th class="col1 align-center">
                                                        Concepto
                                                    </th>
                                                    <!--<th class="align-center" style="">
                                                            PORCENTAJE
                                                        </th>-->
                                                    <th class="align-center col2">
                                                        Presupuesto
                                                    </th>
                                                    <th class="align-center col3">
                                                        Costo Final
                                                    </th>
                                                    <th class="align-center  col4">
                                                        Pagado
                                                    </th>
                                                    <th class="align-center col5">
                                                        Pendiente
                                                    </th>
                                                    <th class="align-center col6">
                                                        Fecha
                                                    </th>
                                                    <th class="align-center col7">
                                                    <a data-categoria="<?php echo $category->id/*echo $categoria->id_default_porcentaje*/ ?>"
                                                       data-nombre="<?php echo $category->name/*echo strtolower(str_replace(" ", "-", $categoria->nombre))*/ ?>"
                                                       class="waves-effect btn-anadir " style="color: black !important;">
                                                        <i class="material-icons right">add</i>
                                                        Añadir
                                                    </a>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $total_categoria_aprox  = 0;
                                                $total_categoria_final  = 0;
                                                $total_categoria_pagado = 0;

                                                

                                                foreach ($category->budgets as $key => $budget) {

                                                    $p              = $budget;
                                                    $p->pagado      = $p->payments()->where("pagado", 1)->count()
                                                        ?
                                                        $p->payments()->where("pagado", 1)->get()->sum("monto")
                                                        :
                                                        "0.0";
                                                    $p->costo_final = is_null($p->costo_final) ? 0.0 : $p->costo_final;
                                                    
                                                    $total_categoria_aprox  += $p->costo_aproximado;
                                                    $total_categoria_final  += $p->costo_final;
                                                    $total_categoria_pagado += $p->pagado;
                                                    ?>

                                                    <tr class="presupuesto dates"
                                                        id="<?php echo $p->id_presupuesto ?>"
                                                        data-id="<?php echo $p->id_presupuesto ?>"
                                                        data-categoria="<?php echo $category->id ?>"
                                                        data-nota="<?php echo $p->nota ?>">
                                                        <td style="border-right: 1px solid #C0C0C0;">
                                                        <textarea class="nombre"
                                                                  style="margin: 0 0 0px 0;"><?php echo $p->nombre ?></textarea>
                                                        </td>

                                                        <td class="right-align"
                                                            style="border-right: 1px solid #C0C0C0;">
                                                            <div class="input-group">
                                                                <input type="text"
                                                                       class="aprox <?php echo (empty($p->costo_aproximado) || $p->costo_aproximado == "0.00")
                                                                           ?
                                                                           "formularios"
                                                                           :
                                                                           ""; ?>"
                                                                       value="<?php echo "$ ".moneyFormat($p->costo_aproximado
                                                                               ?
                                                                               $p->costo_aproximado
                                                                               :
                                                                               ($presupuesto * ($p->porcentaje / 100)),
                                                                               true) ?>"/>
                                                            </div>
                                                        </td>

                                                        <td class="right-align"
                                                            style="border-right: 1px solid #C0C0C0;">
                                                            <div class="input-group">
                                                                <input type="text"
                                                                       class="final <?php echo empty($p->costo_final) || $p->costo_final == "0.00" ? "formularios" : ""; ?>"
                                                                       value="<?php echo "$ ".moneyFormat($p->costo_final,
                                                                               true) ?>"/>
                                                            </div>
                                                        </td>

                                                        <td class="right-align"
                                                            style="border-right: 1px solid #C0C0C0;">
                                                            <a data-categoria="<?php echo $p->categoria ?>"
                                                               class="<?php echo empty($p->pagado) || $p->pagado == "0.00" ? "formularios" : ""; ?> pagar red-text text-darken-3 pagado clickable tooltipped"
                                                               data-position="top" data-delay="50"
                                                               data-tooltip="Registrar pagos">
                                                                <?php echo "$ ".moneyFormat($p->pagado) ?>
                                                            </a>
                                                        </td>

                                                        <td class="right-align pendiente"
                                                            style="border-right: 1px solid #C0C0C0;">
                                                            <p><?php echo "$ ".moneyFormat($p->costo_final - $p->pagado) ?></p>
                                                        </td>

                                                        <td class="right-align pendiente"
                                                            style="border-right: 1px solid #C0C0C0; width:100px;">
                                                            <div class="input-group ">
                                                                <input
                                                                    type="text"
                                                                    value="<?php echo(isset($p->fecha_asignada) ? date("d/m/Y", strtotime($p->fecha_asignada))  : "-") ?>"
                                                                    class="center-align datepicker"
                                                                    data-task-id="<?php echo $p->id_presupuesto ?>" >
                                                            </div>
                                                        </td>
                                                        <td class="col6" style="width: 150px;">
                                                            <a class="<?php echo($p->nota == "" ? "gray-text" : "amber-text") ?>  darken-5 nota  clickable tooltipped "
                                                               data-position="top" data-delay="50"
                                                               data-tooltip="Agregar Nota">
                                                                <i class="material-icons">note</i>
                                                            </a>
                                                            <a class="gray-text darken-5 date  clickable tooltipped "
                                                               data-position="top" data-delay="50"
                                                               data-tooltip="Asignar fecha"
                                                               data-task-id="<?php echo $p->id_presupuesto ?>">
                                                                <i class="material-icons">date_range</i>
                                                            </a>
                                                            <a class="gray-text darken-5 delete  clickable tooltipped "
                                                               data-position="top" data-delay="50"
                                                               data-tooltip="Eliminar">
                                                                <i class="material-icons">delete</i>
                                                            </a>
                                                            <a class="gray-text darken-5 suggested-provider clickable tooltipped "
                                                               data-position="top" data-delay="50"
                                                               data-tooltip="Proveedores sugeridos">
                                                                <i class="material-icons">book</i>
                                                            </a>
                                                            <?php if(!empty($p->status) && $p->status == 1): ?>
                                                            <a id="<?= !empty($p->id_presupuesto)? $p->id_presupuesto.'_a' : '' ?>" 
                                                                class="gray-text darken-5 clickable tooltipped "
                                                                data-position="top" data-delay="50"
                                                                data-tooltip="Cancelar?">
                                                                <i id="<?= !empty($p->id_presupuesto)? $p->id_presupuesto.'_status' : '' ?>" 
                                                                    class="material-icons" 
                                                                    onclick="change_status(<?= !empty($p->id_presupuesto)? $p->id_presupuesto : '' ?>, 'web')">check_circle</i>
                                                            </a>
                                                            <?php else: ?>
                                                            <a id="<?= !empty($p->id_presupuesto)? $p->id_presupuesto.'_a' : '' ?>" 
                                                                class="gray-text darken-5 clickable tooltipped "
                                                                data-position="top" data-delay="50"
                                                                data-tooltip="Listo?">
                                                                <i id="<?= !empty($p->id_presupuesto)? $p->id_presupuesto.'_status' : '' ?>" 
                                                                    class="material-icons" 
                                                                    onclick="change_status(<?= !empty($p->id_presupuesto)? $p->id_presupuesto : '' ?>, 'web')">donut_large</i>
                                                            </a>
                                                            <?php endif; ?>
                                                            
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }

                                                $total_aprox += $total_categoria_aprox;
                                                $total_final += $total_categoria_final;
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                <tr class="total">
                                                    <td>
                                                        TOTAL:
                                                    </td>
                                                    <td class="right-align aprox">
                                                        <?php echo "$  ".moneyFormat($total_categoria_aprox) ?>
                                                    </td>
                                                    <td class="right-align final">
                                                        <?php echo "$  ".moneyFormat($total_categoria_final) ?>
                                                    </td>
                                                    <td class="right-align pagado">
                                                        <?php echo "$  ".moneyFormat($total_categoria_pagado) ?>
                                                    </td>
                                                    <td class="right-align pendiente">
                                                        <p>
                                                            <?php echo "$  ".moneyFormat($total_categoria_final - $total_categoria_pagado) ?>
                                                        </p>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div id="responsiveBudgetary" class="col s12 hide-on-med-and-up" style="margin-top: 30px;">

                        <?php
                        foreach ($categories as $category) : ?>
                            <div class="col s12">
                                <div class="title-presupuesto pestania  col s12 m6 l7 cat-mobile-<?php echo $category->id ?>"
                                     id="<?php echo $category->id ?>">
                                    <p style="/*color: white!important;*/">
                                        <img src="<?php echo base_url() ?>dist/img/iconos/proveedores/<?php echo $category->slug ?>.png"
                                             alt="<?php echo $category->name ?>"
                                             style="max-width:35px">
                                        <?php echo ucfirst(strtolower(htmlentities($category->name))) ?><i
                                                class="fa fa-caret-down pull-right" aria-hidden="true"></i>
                                        <!-- <span class="pull-right">$500,000</span> -->
                                    </p>
                                </div>
                                <div class="card"
                                     id="cat-<?php echo $category->id ?>"
                                     style="margin-bottom: 70px; width: 100%; border-bottom: 2px solid #757575;">
                                    <div class="card-content" style="padding: 0px!important;display: none;border-bottom: 1px solid #757575;">
                                        <div class="row" style="margin: 0;">


                                            <?php foreach ($category->budgets as $budget) : ?>
                                                <div class="col s12 budget-container"
                                                     data-budget-id="<?php echo $budget->id_presupuesto ?>"
                                                     data-id="<?php echo $budget->id_presupuesto ?>">
                                                    <div class="d-flex justify-between align-items-end"
                                                         style="border-bottom: 1px solid #757575;">
                                                    <span class="budget-title">
                                                        <?php echo $budget->nombre ?>
                                                    </span>
                                                        <i class="material-icons" style="color: #878589;">delete</i>
                                                        <?php if(!empty($budget->status) && $budget->status == 1): ?>
                                                            <a id="<?= !empty($budget->id_presupuesto)? $budget->id_presupuesto.'_a' : '' ?>" 
                                                                class="gray-text darken-5 clickable tooltipped "
                                                                data-position="top" data-delay="50"
                                                                data-tooltip="Cancelar?">
                                                                <i id="<?= !empty($budget->id_presupuesto)? $budget->id_presupuesto.'_status' : '' ?>" 
                                                                    class="material-icons" 
                                                                    onclick="change_status(<?= !empty($budget->id_presupuesto)? $budget->id_presupuesto : '' ?>)">check_circle</i>
                                                            </a>
                                                            <?php else: ?>
                                                            <a id="<?= !empty($budget->id_presupuesto)? $budget->id_presupuesto.'_a' : '' ?>" 
                                                                class="gray-text darken-5 clickable tooltipped "
                                                                data-position="top" data-delay="50"
                                                                data-tooltip="Listo?">
                                                                <i id="<?= !empty($budget->id_presupuesto)? $budget->id_presupuesto.'_status' : '' ?>" 
                                                                    class="material-icons" 
                                                                    onclick="change_status(<?= !empty($budget->id_presupuesto)? $budget->id_presupuesto : '' ?>)">donut_large</i>
                                                            </a>
                                                            <?php endif; ?>
                                                    </div>
                                                    
                                                    <div class="budget-content d-flex justify-between">
                                                    <span data-budget="<?php echo $budget->costo_aproximado ? $budget->costo_aproximado : 0 ?>"
                                                          data-cost="<?php echo $budget->costo_final ? $budget->costo_final : 0 ?>">Costo: $<?php echo money_format("%i",
                                                            $budget->cost) ?></span>
                                                        <span data-paid="<?php echo $budget->paid ? $budget->paid : 0 ?>">Pagado: $<?php echo money_format("%i",
                                                                $budget->paid) ?></span>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>

                                            <div class="col s12 budget-container">
                                                <button class="btn-add-concept">Agregar concepto</button>
                                                <div style="border-bottom: 1px solid #757575;">
                                                    <span class="budget-title"
                                                          style="">TOTAL</span>
                                                </div>
                                                <div class="budget-content d-flex justify-between">
                                                <span>Pendiente: $<?php echo money_format("%i",
                                                        $category->total_final - $category->total_paid) ?></span>
                                                    <span>Pagado: $<?php echo money_format("%i",
                                                            $category->total_paid) ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>

                    </div>
                    <div id="tab-pagos" class="tab2 col s12" style="display: none;">
                        <div class="center-align cargando">
                            <i class="fa fa-pulse fa-spinner" style="font-size: 90px"></i><br>
                            <b>Cargando pagos ...</b>
                        </div>
                        <div class="pagos">
                            <table class="striped data-table">
                                <thead>
                                <tr class="center-align">
                                    <th>
                                        Estado
                                    </th>
                                    <th>
                                        Presupuesto
                                    </th>
                                    <th>
                                        Importe
                                    </th>
                                    <th>
                                        Vencimiento
                                    </th>
                                    <th>
                                        Fecha de pago
                                    </th>
                                    <th>
                                        Pagado Por
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="tabla_pagos_registrados">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l3 center-align m-clndr" style="margin-bottom: 50px;">
                    <div class="row">
                        <div class="col s12 mini-clndr-col">
                            <div id="mini-clndr"></div>
                        </div>
                        <div class="col s12 mini-clndr-col-detail">
                            <div class="date-indicator" style="">
                            </div>
                        </div>
                    </div>
            </div>
            <!-- PRESUPUESTADOR MAESTRO -->
            <div class="col s12 m3  " style="display: none;"> 
                <ul class="collection with-header barra-lateral"
                    style="display: none;box-shadow: 0 15px 10px 0px rgba(0,0,0,.5), 0 1px 4px 0 rgba(0,0,0,.3), 0 0 40px 0 rgba(0,0,0,.1) inset;">
                    <li class="collection-header">
                        <div class="row">
                            <div class="col s12 center-align">
                                <h5 style="font-size:21px">PRESUPUESTADOR MAESTRO <br>
                                    <small>
                                        <!--                                    &nbsp; &nbsp; &nbsp; MI PRESUPUESTO: <b>$-->
                                        <!--                                        <text id="mi_presupuesto">-->
                                        <?php //echo moneyFormat($presupuesto, true) ?><!--</text>-->
                                        </b>
                                    </small>
                                </h5>
                            </div>
                            <div class="col s12 valign-wrapper" style="display:none; height: 70px">
                                <a href="<?php echo base_url() ?>novios/proveedor/sugeridos"
                                   class="btn  yellow darken-5 right waves-effect  valign"
                                   style="margin-left: auto; margin-right: auto;min-height: 36px!important; height: auto">
                                    Proveedores sugeridos
                                </a>
                            </div>
                        </div>
                    </li>
                    <li class="collection-item">
                        <div class="row">
                            <div class="col s12">
                                <div class="card hover-shadow clickable z-depth-0"
                                     onclick="$('a#btn-tab-presu').click();$('html, body').stop().animate({scrollTop: $('#presupuesto-content').offset().top}, '500', 'swing')">
                                    <div class="card-content">
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col s1">
                                                <i class="presupuesto presupuesto2 presupuesto-costo-aproximado x2"
                                                   style="    margin-left: -130px;"></i>
                                            </div>
                                            <div class="col s10">
                                                COSTO APROXIMADO
                                                <span class=""></span>
                                                <br><b class="total-aproximado"><?php echo moneyFormat($presupuesto,
                                                        true) ?></b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12">
                                <div class="card hover-shadow clickable z-depth-0"
                                     onclick="$('a#btn-tab-presu').click();$('html, body').stop().animate({scrollTop: $('#presupuesto-content').offset().top}, '500', 'swing')">
                                    <div class="card-content">
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col s1">
                                                <i class="presupuesto presupuesto2 presupuesto-costo-final x2"
                                                   style="    margin-left: -130px;"></i>
                                            </div>
                                            <div class="col s10">
                                                COSTO FINAL
                                                <br><b class="total-final">$ 0.00</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12">
                                <div class="card hover-shadow clickable z-depth-0"
                                     onclick="$('a#btn-tab-pagos').click();$('html, body').stop().animate({scrollTop: $('#presupuesto-content').offset().top}, '500', 'swing')">
                                    <div class="card-content">
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col s1">
                                                <i class="presupuesto presupuesto2 presupuesto-pagado x2"
                                                   style="    margin-left: -130px;"></i>
                                            </div>
                                            <div class="col s10">
                                                PAGADO
                                                <br><b class="total-pagado">$ 0.00</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12">
                                <div class="card hover-shadow clickable z-depth-0"
                                     onclick="$('a#btn-tab-presu').click();$('html, body').stop().animate({scrollTop: $('#presupuesto-content').offset().top}, '500', 'swing')">
                                    <div class="card-content">
                                        <div class="row" style="margin-bottom: 2px">
                                            <div class="col s1">
                                                <i class="presupuesto presupuesto2 presupuesto-pendiente x2"
                                                   style="    margin-left: -130px;"></i>
                                            </div>
                                            <div class="col s10">
                                                PENDIENTE
                                                <br><b class="total-pendiente">$ 0.00</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div id="budget-edit" class="row" style="display: none;">
        <div class="col s12" style="background-color: #757575;">
            <i class="material-icons" style="color: white;" id="budget-edit-back">arrow_back</i>
        </div>

        <div class="col s12" style="margin-top: 50px;">
            <!-- GASTO -->
            <div class="title-presupuesto pestania z-depth-1">
                <span style="color: white;">Gasto</span>
            </div>
            <div class="card" style="margin: 0; height:450px;">
                <div class="card-content">
                    <div class="row">
                        <div class="col s6 text-center">
                            <span class="budget-edit-title">Categoria</span>
                            <input id="budget-edit-category" type="text" class="budget-edit-input" disabled>
                        </div>
                        <div class="col s6 text-center">
                            <span class="budget-edit-title">Concepto</span>
                            <input id="budget-edit-concept" type="text" class="budget-edit-input">
                        </div>
                        <div class="col s6 text-center">
                            <span class="budget-edit-title">Presupuesto</span>
                            <input id="budget-edit-approximate-cost" type="text" class="budget-edit-input">
                        </div>
                        <div class="col s6 text-center">
                            <span class="budget-edit-title">Costo final</span>
                            <input id="budget-edit-final-cost" type="text" class="budget-edit-input">
                        </div>
                        <div class="col s6 text-center">
                            <span class="budget-edit-title">Fecha</span>
                            <input id="budget-edit-date" type="text" class="budget-edit-input datepicker">
                        </div>
                    </div>
                        <div class="col s12">
                            <span class="budget-edit-title">Notas</span>
                            <textarea id="budget-edit-notes" class="budget-edit-input"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <!-- PAGOS -->
            <div class="title-presupuesto pestania z-depth-1" id="payments" style="margin-top: 10px;">
                <span style="color: white;">Pagos</span>
            </div>
            <div class="card" style="margin: 0;">
                <div class="card-content">
                    <div class="row">
                        <div class="col s6 text-center">
                            <div>
                                <span class="budget-edit-title">Costo final</span>
                            </div>
                            <div>
                                <span id="payments-final-cost" class="budget-edit-title"></span>
                            </div>
                        </div>
                        <div class="col s6 text-center">
                            <div>
                                <span class="budget-edit-title">Pagado</span>
                            </div>
                            <div>
                                <span id="payments-total" class="budget-edit-title"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 d-flex align-items-center">
                            <div class="col s6">
                                <button id="btn-add-payment" type="submit" class="btn waves-effect waves-light"
                                        style="flex: 1 1 0;padding: 0 3px;">
                                    Agregar pago
                                </button>
                            </div>
                            <div class="col s6">
                                <input id="input-payment" type="text" class="budget-edit-input"
                                       style="flex: 1 1 0;margin: 0;height: auto;" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PROVEEDORES SUGERIDOS -->
            <div class="suggested-providers-container" style="display: none;">
                <h5 class="text-center">Proveedores sugeridos</h5>
                <div class="row d-flex justify-center align-items-center">
                    <i class="material-icons p-0" id="previousPageSuggestedProviders" style="color: black !important;">arrow_back_ios</i>
                    <div class="col s10 p-0 m-0">
                        <div class="row m-0" id="suggestedProviders">
                            <div class="col s4 p-0 d-flex justify-center align-items-center"
                                 style="position: relative;">
                                <a href="/">
                                    <img src=""
                                         class="suggested-provider-image">
                                </a>
                                <span class="suggested-provider-title textfit" data-textfit-min="8"
                                      data-textfit-max="9">Prueba</span>
                            </div>
                            <div class="col s4 p-0 d-flex justify-center align-items-center"
                                 style="position: relative;">
                                <a href="/" style="color: black !important;">
                                    <img src=""
                                         class="suggested-provider-image">
                                </a>
                                <span class="suggested-provider-title textfit" data-textfit-min="8"
                                      data-textfit-max="9">Prueba</span>
                            </div>
                            <div class="col s4 p-0 d-flex justify-center align-items-center"
                                 style="position: relative;">
                                <a href="/" style="color: black !important;">
                                    <img src=""
                                         class="suggested-provider-image">
                                </a>
                                <span class="suggested-provider-title textfit" data-textfit-min="8"
                                      data-textfit-max="9">Prueba</span>
                            </div>
                        </div>
                    </div>
                    <i class="material-icons p-0" id="nextPageSuggestedProviders" style="color: black !important;">arrow_forward_ios</i>
                </div>
            </div>

            <button id="budget-save" class="btn waves-effect waves-light" style="width: 100%;margin-top: 15px;">
                GUARDAR
            </button>

        </div>
    </div>

    <!-- Calendar's template -->
<script id="calendar-template" type="text/template">
    <div class="controls valign-wrapper">
        <div class="clndr-previous-button">
            <span class="material-icons" style="font-size: 50px; color: #757575;">chevron_left</span>
        </div>
        <div class="month">
            <h4 style="color: black; font-weight: bolder;">
                <%= month %>
            </h4>
        </div>
        <div class="clndr-next-button">
            <span class="material-icons" style="font-size: 50px; color: #757575;">chevron_right</span>
        </div>
    </div>

    <div class="days-container">
        <div class="days">
            <div class="headers">
                <% _.each(daysOfTheWeek, function(day) { %><div class="day-header"><%= day %></div><% }); %>
            </div>
            <% _.each(days, function(day) { %><div class="<%= day.classes %>" style="font-weight: bolder; border-color: #eae6e6;" id="<%= day.id %>"><%= day.day %></div><% }); %>
        </div>
    </div>
</script>
<!-- -->

    <!-- TEMPLATES -->
    <div class="col s12 budget-container" id="templateBudgetContainer" data-budget-id="-1" style="display: none;">
        <div class="d-flex justify-between align-items-end" style="border-bottom: 1px solid #757575;">
            <span class="budget-title"></span>
            <i class="material-icons" style="color: #878589;">delete</i>
        </div>
        <div class="budget-content d-flex justify-between">
            <span data-budget= "0" data-cost="0">Costo: $0 </span>
            <span data-paid="0">Pagado: $0</span>
        </div>
    </div>

    <!-- MODALES -->
    <div id="modal-nota" data-current-presupuesto="0" class="modal ">
        <div class="modal-content">
            <h4 class="concepto">Modal Header</h4>
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">mode_edit</i>
                        <textarea id="icon_prefix2" class="materialize-textarea  contenido-nota"
                                  length="255"></textarea>
                        <label for="icon_prefix2">Nota</label>
                    </div>
                </div>
            </form>

        </div>
        <div class="modal-footer">
            <a class="modal-action waves-effect waves-green btn-flat aceptar " style="color: black !important;">Aceptar</a>
            <a class="modal-action waves-effect waves-green btn-flat cancelar" style="color: black !important;">Cancelar</a>
        </div>
    </div>

    <div id="modal-pagos"
         data-current-presupuesto="0"
         data-categoria="0"
         class="modal modal-fixed-footer" style="width: 95% ;max-height: 210000000px;">
        <div class="modal-content">
            <h4 class="concepto">Registrar pagos</h4>
            <div class="info-modal-pagos">
                <div class="card z-depth-0 transparent">
                    <div class="card-image waves-effect waves-block waves-light" style="text-align: center">
                        <i class="fa fa-money "
                           style="padding: 15px;border-radius: 50%;color: #cfcfcf;border: 4px solid #cfcfcf;font-size: 90px;margin-top: 10px;"></i>
                    </div>
                    <div class="card-content" style="text-align: center">
                        No se han registrado ningun pago para <b class="nombre"></b>
                    </div>
                </div>
            </div>
            <table class="hide">
                <thead>
                <tr>
                    <th>
                        ESTADO
                    </th>
                    <th>
                        IMPORTE
                    </th>
                    <th>
                        VENCIMIENTO
                    </th>
                    <th>
                        PAGADO POR
                    </th>
                    <th>
                        FECHA PAGO
                    </th>
                    <th>
                        MODO PAGO
                    </th>
                    <th>
                    </th>
                </tr>
                </thead>
                <tbody class="tabla-pagos">
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a class="modal-action waves-effect btn anadir-pago yellow darken-5 " style="float: left;margin-left: 15px">Agregar
                Pago</a>
            <a class="modal-action waves-effect waves-green btn-flat aceptar modal-close" style="color: black !important;">Aceptar</a>
        </div>

        <!-- Calendar's template -->
            <script id="calendar-template" type="text/template">
                <div class="controls valign-wrapper">
                    <div class="clndr-previous-button">
                        <span class="material-icons" style="font-size: 50px; color: #757575;">chevron_left</span>
                    </div>
                    <div class="month">
                        <h4 style="color: black; font-weight: bolder;">
                            <%= month %>
                        </h4>
                    </div>
                    <div class="clndr-next-button">
                        <span class="material-icons" style="font-size: 50px; color: #757575;">chevron_right</span>
                    </div>
                </div>

                <div class="days-container">
                    <div class="days">
                        <div class="headers">
                            <% _.each(daysOfTheWeek, function(day) { %><div class="day-header"><%= day %></div><% }); %>
                        </div>
                        <% _.each(days, function(day) { %><div class="<%= day.classes %>" style="font-weight: bolder; border-color: #eae6e6;" id="<%= day.id %>"><%= day.day %></div><% }); %>
                    </div>
                </div>
            </script>
        <!-- -->

    </div>

    <style>
        #modal-proveedor {
            width: 47% !important;
        }

        #modal-proveedor .modal-content {
            padding-bottom: 0 !important;
        }

        #modal-proveedor .card-image img {
            height: 200px !important;
        }

        #modal-proveedor .card-image {
            text-align: center;
        }

        #modal-proveedor .card-image span {
            width: 100% !important;
        }

        #modal-proveedor .card-content {
            margin: 5px 0;
            overflow: hidden;
            padding: 0;
            text-align: center;
        }

        #modal-proveedor .card-content p p:first-child {
            height: auto !important;
            text-overflow: initial !important;
            white-space: normal;
        }

        #modal-proveedor .card-action {
            padding: 0 !important;
        }

        #modal-proveedor .card-action a {
            width: 100% !important;
        }

        #modal-proveedor ul.pagination {
            margin: 0px !important;
        }

        #modal-proveedor .modal-footer {
            height: initial !important;
        }
    </style>

    <!-- PROVEEDOR SUGERIDO -->
    <?php
    $this->view("proveedor/modal_proveedores");
    $this->view("principal/novia/modal-presupuesto");
    $this->view("modal_indicador_pres");
    ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/parsley.js"></script>
    <script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/i18n/es.js"></script>
    <link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
    <script src="<?php echo base_url() ?>dist/js/textfit.js"></script>
    <script src="<?php echo base_url() ?>dist/js/modal_proveedores.js"></script>
    <script src="<?php echo base_url() ?>dist/js/Presupuestador.js"></script>
    <script src="<?php echo base_url() ?>dist/js/budgetary.js"></script>
    <script src="<?php echo base_url() ?>dist/js/jquery.inputmask.bundle.js"></script>
    <!-- <script src="<?php echo base_url() ?>dist/js/novios/Tareas.js" type="text/javascript"></script> -->

    <script>

        var server = "<?php echo base_url() ?>";

        $(document).ready(function() {

            getEvents();

            var es_datepicker = {
                selectMonths: true,
                selectYears: 15,
                monthsFull: [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre'],
                monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                today: 'Hoy',
                hiddenName: true,
                formatSubmit: 'yyyy/mm/dd',
                clear: 'Limpiar',
                close: 'Aceptar',
            };

            var template_pago = '<td> <a data-pagado="1" style="cursor:pointer" class="cambiar-estado"> <i class="fa fa-check fa-2x green-text darken-2"></i> </a>' +
                '</td><td><div class="input-group" style="margin-top: -15px"><input class="form-control importe" value="0.0" /></div></td><td><input class="form-control datepicker datepicker vencimiento"></td><td><input class="form-control pagado_por"></td><td><input class="form-control fecha_pago datepicker"  /></td><td><input class="form-control modo_pago"></td><td><a style="cursor:pointer" class="eliminar" ><i class="fa fa-times fa-2x red-text darken-2"></i></a></td>';

            var server = "<?php echo base_url() ?>";
            var inputMask = new Inputmask({alias: 'currency', prefix: '$', min: 0});
            inputMask.mask($('input.aprox'));
            inputMask.mask($('input.final'));



            // -------------- Calendario

            

            $('.datepicker').datepicker({
                
                onSelect: function (date) {
                    var reqUrl = server + 'index.php/novios/presupuesto/home/updateDate';
                    var _this = this;
                    var taskId = $(this).data('task-id');
                    var date2 = moment(date, 'DD-MM-YYYY').format('YYYY-MM-DD');
                    console.log(date2);
                    console.log(taskId);

                    var tasks = $(".presupuesto.dates").map(function () {
                        return $(this).data('id');
                    }).toArray();

                    console.log(tasks);

                    $.ajax({
                        url: reqUrl,
                        data: {
                            id: taskId,
                            date: date2,
                            id_presupuesto: tasks,
                        },
                        method: "POST",
                        success: function (response) {
                            
                            // console.log(response.data);

                            events = response.data;

                            $('#mini-clndr').clndr().setEvents(events);

                        },
                        error: function (error) {
                            console.log(error);
                            //Mostrar mensaje de error
                        }
                    });
                }
            });

            $('.date').click(function() {
                var taskId = $(this).data('task-id');
                $(".datepicker[data-task-id='" + taskId + "']").focus();
            });

            // -------------- end calendario

            $('#btn-tab-presu').on('click', function() {
                $('#responsiveBudgetary').show();
                setTimeout(function() {
                    size_div(window);
                }, 1200);
            });

            $('.title-presupuesto.pestania').on('click', function() {
                setTimeout(function() {
                    size_div(window);
                }, 600);
            });

            function size_div(width) {
                if ($(width).width() > 585) {
                    var size = ($('#presupuesto-content').height() - $('.barra-lateral').height() * 2) - 140;
                    $('.barra-lateral').css({'top': size + 'px'});
                    $('.barra-lateral').css({'display': 'block'});
                }
                else {
                    $('.barra-lateral').css({'display': 'none'});
                    $('.barra-lateral').css({'top': '0px'});
                }
                if ($(document).height() < 2500) {
                    $('.barra-lateral').css({'display': 'none'});
                }
                else {

                    $('.barra-lateral').css({'display': 'block'});
                }
            }

            $('#btn-tab-pagos').on('click', function() {
                var $tab = $('#tab-pagos');
                if ($tab.get(0).style.display == 'block') {
                }
                else {
                    $('#responsiveBudgetary').hide();
                    $('#tab-pagos .pagos tbody').html('');
                    $.ajax({
                        url: server + 'novios/pago/getAll',
                        method: 'POST',
                        data: {
                            'data': 1,
                        },
                        success: function(res) {
                            $('.cargando').hide();
                            $('.barra-lateral').hide();

                            var template = '<td class=\'estado center-align\'></td><td class=\'presupuesto presupuesto-not-icon center-align\'></td>' +
                                '<td class=\'importe center-align\' ></td><td class=\'vencimiento center-align\'></td>' +
                                '<td class=\'fecha_pago center-align\'></td><td class=\'pagado_por center-align\' ></td>';

                            for (var i in res.data) {
                                var p = res.data[i];
                                var tr = document.createElement('tr');
                                tr.setAttribute('id', p.id_pago);
                                tr.innerHTML = template;
                                var $tr = $(tr);
                                $tr.find('.estado').html(p.pagado == 1
                                    ? '<i class=\'fa fa-check green-text\'></i> PAGADO'
                                    : '<i class=\'fa fa-clock-o red-text\'></i> PENDIENTE');
                                $tr.find('.presupuesto').html((p.presupuesto));
                                $tr.find('.importe').html(toMoney(p.monto));
                                $tr.find('.vencimiento').html((p.fecha_vencimiento));
                                $tr.find('.fecha_pago').html((p.fecha_pago));
                                $tr.find('.pagado_por').html((p.pagado_por));
                                $('#tab-pagos .pagos tbody').append(tr);
                            }

                            setTimeout(function() {
                                $('.barra-lateral').hide();
                            }, 1200);

                            if (window.tabla_pagos != undefined) {
                                window.tabla_pagos.destroy();
                            }

                            window.tabla_pagos = $('.data-table').DataTable({
                                autoWidth: false,
                                responsive: true,
                                language: {
                                    processing: 'Procesando...',
                                    search: 'Buscar:',
                                    lengthMenu: 'Mostrar _MENU_ ',
                                    info: '_START_ de _END_ / _TOTAL_',
                                    infoEmpty: 'Mostrando pagos',
                                    infoFiltered: '(filtrado de un total de _MAX_ invitados)',
                                    infoPostFix: '',
                                    loadingRecords: 'Cargando...',
                                    zeroRecords: 'No se encontraron resultados',
                                    emptyTable: 'Ning&uacute;n dato disponible en esta tabla',
                                    paginate: {
                                        first: 'Primero',
                                        previous: '<i class=\'material-icons\'>chevron_left</i>',
                                        next: '<i class=\'material-icons\'>chevron_right</i>',
                                        last: 'Ultimo',
                                    },
                                    aria: {
                                        sortAscending: ': Activar para ordenar la columna de manera ascendente',
                                        sortDescending: ': Activar para ordenar la columna de manera descendente',
                                    },
                                },
                            });

                            $('#tab-pagos .pagos').show();

                            $('.barra-lateral').hide();
                        }, error: function() {
                            $('.cargando').hide();
                            setTimeout(function() {
                                $('.barra-lateral').hide();
                            }, 1200);
                        },
                    });
                }
            });

            $('a.btn-anadir').on('click', function(e) {
                window.e = e;
                var name = $(this).attr('data-nombre');
                var template = `<td style=""><input class="nombre  form-control" value="" style="margin: 0 0 0px 0;"></td>
            <td class="right-align" style=""><div class="input-group"><input type="text" class="aprox form-control" value="0.00" /></div></td><td class="right-align" style=""><div class="input-group"><input type="text" class="final form-control" value="0.00" /></div></td><td class="right-align" style=""><a class=" pagar red-text text-darken-3 pagado"  style="cursor: pointer">$ 0.00</a></td><td class="right-align pendiente" style=""><p>$ 0.00</p></td>
            <td><a class="gray-text darken-5 nota" style="cursor: pointer" data-position="top" data-delay="50" data-tooltip="Agregar Nota"><i class="material-icons">note</i></a><a class="gray-text darken-5 delete" data-position="top" data-delay="50" data-tooltip="Eliminar" style="cursor: pointer"><i class="material-icons">delete</i></a></td>`;

                $.ajax({
                    url: server + 'novios/presupuesto/home/crear',
                    method: 'POST',
                    data: {
                        categoria: e.currentTarget.getAttribute('data-categoria'),
                    },
                    success: function(data, textStatus, jqXHR) {
                        var tr = document.createElement('tr');
                        tr.setAttribute('class', 'presupuesto');
                        tr.setAttribute('id', data.data.id_presupuesto);
                        tr.setAttribute('data-categoria', data.data.categoria);
                        tr.setAttribute('data.nota', '');

                        var $tr = $(tr);
                        $tr.html(template);
                        $tr.find('.nota').on('click', notaPresupuesto);
                        $tr.find('.delete').on('click', deletePresupuesto);
                        $tr.find('.nombre').on('change', changeNombre);
                        $tr.find('.porcentaje').on('change', changePorcentaje);
                        $tr.find('.aprox').on('change', changeAproximado);
                        $tr.find('.final').on('change', changePresupuesto);
                        $tr.find('a.pagar').on('click', pagar_presupuesto);
                        inputMask.mask($tr.find('.aprox'));
                        inputMask.mask($tr.find('.final'));

                        $tr.find('input').on('keypress', function(e) {
                            if (e.keyCode == 13) {
                                $(this).trigger('change');
                            }
                        });
                        $('#cat-' + data.data.categoria + ' tbody').append($tr);
                        $tr.find('.nombre').focus();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        var $toastContent = $('<span>Ocurrio un problema al agregar un Gasto</span>');
                        Materialize.toast($toastContent, 5000);
                    },
                });
            });

            $('#modal-nota .aceptar').on('click', function() {
                var nota = {
                    'presupuesto': $('#modal-nota').attr('data-current-presupuesto'),
                    'nota': $('#modal-nota .contenido-nota').val(),
                };
                $.ajax({
                    url: server + 'novios/presupuesto/home/nota',
                    method: 'POST',
                    data: nota,
                    success: function(res) {
                        var $toastContent = $('<span>Nota agregada correctamente</span>');
                        Materialize.toast($toastContent, 5000);
                        $('#' + nota.presupuesto).attr('data-nota', nota.nota);
                        $('#modal-nota .contenido-nota').val('');
                        $('#modal-nota .cancelar').trigger('click');
                        $('#' + nota.presupuesto + ' .nota').removeClass('gray-text');
                        $('#' + nota.presupuesto + ' .nota').addClass('amber-text');
                    },
                    error: function() {
                        $('#modal-nota .cancelar').trigger('click');
                        var $toastContent = $('<span>Ocurrio un error al agregar la nota</span>');
                        Materialize.toast($toastContent, 5000);
                    },
                });
            });

            $('#modal-nota .cancelar').on('click', function() {
                $('#modal-nota .contenido-nota').val('');
                $('#modal-nota').attr('data-current-presupuesto', '');
                $('#modal-nota').modal('close');
            });

            $('.presupuesto .nota').on('click', notaPresupuesto);
            $('.presupuesto .delete').on('click', deletePresupuesto);
            $('.presupuesto .nombre').on('change', changeNombre);
            $('.presupuesto .porcentaje').on('change', changePorcentaje);
            $('.presupuesto .aprox').on('change', changeAproximado);
            $('.presupuesto .final').on('change', changePresupuesto);
            $('.presupuesto a.pagar').on('click', pagar_presupuesto);

            $('input').on('keypress', function(e) {
                if (e.keyCode == 13) {
                    $(this).trigger('change');
                }
            });

            $('#modal-pagos .anadir-pago').on('click', function(evt) {
                $.ajax({
                    url: server + 'novios/pago/nuevo',
                    method: 'POST',
                    data: {
                        'presupuesto': $('#modal-pagos').attr('data-current-presupuesto'),
                        'categoria': $('#modal-pagos').attr('data-categoria'),
                    },
                    success: function(res) {
                        var tr = document.createElement('tr');
                        tr.setAttribute('id', res.data.id_pago);
                        tr.setAttribute('data-pagado', 1);
                        var $tr = $(tr);
                        $tr.html(template_pago);
                        $tr.find('.vencimiento').on('change', changeVencimiento);
                        $tr.find('.fecha_pago').on('change', changeFechaPago);
                        $tr.find('.pagado_por').on('change', changePagadoPor);
                        $tr.find('.modo_pago').on('change', changeModoPago);
                        $tr.find('input.importe').on('blur', changeImporte);
                        inputMask.mask($tr.find('input.importe'));

                        $tr.find('.eliminar').on('click', deletePago);
                        $tr.find('.cambiar-estado').on('click', updateEstado);
                        $('#modal-pagos table:first').append($tr);
                        $tr.find('.datepicker').pickadate(es_datepicker);
                    }, error: function() {
                        $('#modal-nota .cancelar').trigger('click');
                        var $toastContent = $('<span>Ocurrio un error al agregar el pago</span>');
                        Materialize.toast($toastContent, 5000);
                    },
                });
                if (!$('#modal-pagos .info-modal-pagos').hasClass('hide')) {
                    $('#modal-pagos .info-modal-pagos').addClass('hide');
                    $('#modal-pagos table').removeClass('hide');
                }
            });

            function notaPresupuesto(e) {
                var tr = e.currentTarget.parentNode.parentNode;
                $('#modal-nota .contenido-nota').val(tr.getAttribute('data-nota'));
                $('#modal-nota').attr('data-current-presupuesto', tr.getAttribute('id'));
                $('#modal-nota .concepto').html($(tr).find('.nombre').val());
                $('#modal-nota').modal('open');
            }

            function deletePresupuesto(e) {
                swal({
                    title: '¿Estas seguro?',
                    text: 'Una vez borrado no lo podras recuperar',
                    icon: 'warning',
                    confirmButtonText: 'Ok',
                    buttons: ['Cancelar', 'Ok'],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var tr = e.currentTarget.parentNode.parentNode;
                        const categoryId = $(tr).attr('data-categoria');

                        $.ajax({
                            url: server + 'novios/presupuesto/home/eliminar',
                            method: 'POST',
                            data: {
                                'presupuesto': tr.getAttribute('id'),
                            },
                            success: function(res) {
                                $(tr).remove();
                                var $toastContent = $('<span>Gasto eliminado correctamente</span>');
                                Materialize.toast($toastContent, 5000);
                                calcula_totales(categoryId);
                                $('#mini-clndr').clndr().destroy();
                                getEvents();
                            },
                            error: function() {
                                var $toastContent = $('<span>Ocurrio un error al eliminar</span>');
                                Materialize.toast($toastContent, 5000);
                                calcula_totales(categoryId);
                            },
                        });
                    }
                });
            }

            function changePorcentaje(e) {
                var elem = e.currentTarget;
                update('porcentaje', elem, elem.value);
                $(elem).parent().parent().find('.aprox').val((elem.value / 100) * parseFloat(($('#mi_presupuesto').text()).replace(new RegExp(',', 'g'), '')));
            }

            function changeNombre(e) {
                var elem = e.currentTarget;
                update('nombre', elem, elem.value);
            }

            function changeAproximado(e) {
                var elem = e.currentTarget;
                update('costo_aproximado', elem, elem.inputmask.unmaskedvalue());
            }

            function changePresupuesto(e) {
                var elem = e.currentTarget;
                var tr = $(elem.parentElement.parentElement.parentElement);
                update('costo_final', elem, elem.inputmask.unmaskedvalue(), function() {
                    var t = tr.find('.pagado').html().replace('$', '');
                    var pagado = (parseFloat(t.replace(new RegExp(',', '', 'g'), '')));
                    tr.find('.pendiente > p').html(toMoney(elem.inputmask.unmaskedvalue() - pagado + '', true));
                    calcula_totales(tr.attr('data-categoria'));
                });
            }

            function changeImporte(evt) {

                if (evt) {
                    update_pago('monto', evt.currentTarget, evt.currentTarget.inputmask.unmaskedvalue());
                }

                var totalPaid = 0;
                var p = $('#modal-pagos').attr('data-current-presupuesto');
                var finalCost = parseFloat($('.presupuesto#' + p).find('input.final')[0].inputmask.unmaskedvalue());

                $('.tabla-pagos tr input.importe').each(function(index, value) {
                    totalPaid += parseFloat(value.inputmask.unmaskedvalue());
                });

                if (totalPaid == 0) {
                    $('.presupuesto#' + p).find('.pagado').addClass('formularios');
                }
                else {
                    $('.presupuesto#' + p).find('.pagado').removeClass('formularios');
                }

                $('.presupuesto#' + p).find('.pagado').html(toMoney(totalPaid + '', true));

                $('.presupuesto#' + p).find('.right-align.pendiente > p').html(toMoney((finalCost - totalPaid) + '', true));

                calcula_totales($('#modal-pagos').attr('data-categoria'));
            }

            function changeVencimiento(e) {
                update_pago('fecha_vencimiento', e.currentTarget,
                    $(e.currentTarget).pickadate('picker').get('select', 'yyyy/mm/dd'));
            }

            function changeFechaPago(e) {
                update_pago('fecha_pago', e.currentTarget, $(e.currentTarget).pickadate('picker').get('select', 'yyyy/mm/dd'));
            }

            function changeModoPago(evt) {
                update_pago('modo_pago', evt.currentTarget, evt.currentTarget.value);
            }

            function changePagadoPor(evt) {
                update_pago('pagado_por', evt.currentTarget, evt.currentTarget.value);
            }

            function updateEstado(evt) {
                var $tr = $(evt.currentTarget.parentElement.parentElement);
                var pagado = $tr.attr('data-pagado');
                update_pago('pagado', evt.currentTarget, pagado == 1 ? 0 : 1);
                if (pagado == 1) {
                    $(evt.currentTarget).find('i').removeClass('fa-check');
                    $(evt.currentTarget).find('i').removeClass('green-text');
                    $(evt.currentTarget).find('i').addClass('red-text');
                    $(evt.currentTarget).find('i').addClass('fa-clock-o');
                }
                else {
                    $(evt.currentTarget).find('i').removeClass('red-text');
                    $(evt.currentTarget).find('i').removeClass('fa-clock-o');
                    $(evt.currentTarget).find('i').addClass('fa-check');
                    $(evt.currentTarget).find('i').addClass('green-text');
                }
                $tr.attr('data-pagado', pagado == 1 ? 0 : 1);
                var total = 0;
                $('.tabla-pagos tr input.importe').each(function(index, value) {
                    var tr = value.parentElement.parentElement.parentElement;
                    if (tr.getAttribute('data-pagado') == '1') {
                        total += ($(value).inputFormat('value'));
                    }
                });
                var p = $('#modal-pagos').attr('data-current-presupuesto');
                $('.presupuesto#' + p).find('.pagado').html(toMoney(total));
                calcula_totales($('#modal-pagos').attr('data-categoria'));
            }

            function deletePago(e) {
                swal({
                    title: '¿Estas seguro?',
                    text: 'Una vez borrado no lo podras recuperar',
                    icon: 'warning',
                    confirmButtonText: 'Ok',
                    buttons: ['Cancelar', 'Ok'],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var tr = e.currentTarget.parentNode.parentNode;
                        $.ajax({
                            url: server + 'novios/pago/eliminar',
                            method: 'POST',
                            data: {
                                'pago': tr.getAttribute('id'),
                            },
                            success: function(res) {
                                $(tr).remove();
                                var $toastContent = $('<span>Pago eliminado correctamente</span>');
                                Materialize.toast($toastContent, 5000);
                                changeImporte();
                            },
                            error: function() {
                                var $toastContent = $('<span>Ocurrio un error al eliminar</span>');
                                Materialize.toast($toastContent, 5000);
                            },
                        });
                    }
                });
            }

            function update_pago(type, elem, value, fn) {
                var tr = elem.parentNode.parentNode;
                if (tr.tagName != 'TR') {
                    tr = tr.parentNode;
                }
                $.ajax({
                    url: server + 'novios/pago/update',
                    method: 'POST',
                    data: {
                        'pago': tr.getAttribute('id'),
                        'data': value,
                        'type': type,
                    },
                    success: function(res) {
                        $(elem).attr('style',
                            'border:1px solid green !important;box-shadow;inset 0 1px 1px rgba(0, 255, 0, .075) !important');
                        setTimeout(function() {
                            $(elem).attr('style', '');
                        }, 1000);
                        if (typeof fn != 'undefined') {
                            fn(res);
                        }
                    },
                    error: function() {
                        if (typeof fn != 'undefined') {
                            fn(null);
                        }
                        $(elem).attr('style',
                            'border:1px solid red !important;box-shadow;inset 0 1px 1px rgba(255, 0, 0, .075) !important');
                        setTimeout(function() {
                            $(elem).attr('style', '');
                        }, 5000);
                    },
                });
            }

            function update(type, elem, value, fn) {
                var tr = elem.parentNode.parentNode;

                if (typeof value != 'string') {
                    value = isNaN(value) ? 0 : value;
                }

                if (tr.tagName != 'TR') {
                    tr = tr.parentNode;
                }

                $.ajax({
                    url: server + 'novios/presupuesto/home/update',
                    method: 'POST',
                    data: {
                        'presupuesto': tr.getAttribute('id'),
                        'data': value,
                        'type': type,
                    },
                    success: function(res) {
                        elem.inputmask.unmaskedvalue() == 0 ? $(elem).css('background-color', '#F2F2F2') : $(elem).css('background-color', '#FFFFFF');

                        var style = $(elem).attr('style');

                        $(elem).attr(
                            'style',
                            'border:1px solid green !important;box-shadow;inset 0 1px 1px rgba(0, 255, 0, .075) !important;' +
                            style);

                        setTimeout(function() {
                            $(elem).attr('style', style);
                        }, 1000);

                        calcula_totales(tr.getAttribute('data-categoria'));

                        if (typeof fn != 'undefined') {
                            fn();
                        }
                    },
                    error: function() {
                        $(elem).attr('style',
                            'border:1px solid red !important;box-shadow;inset 0 1px 1px rgba(255, 0, 0, .075) !important');
                        setTimeout(function() {
                            $(elem).attr('style', '');
                        }, 5000);
                    },
                    default: function() {
                        calcula_totales(tr.getAttribute('data-categoria'));
                    },
                });
            }

            function pagar_presupuesto(e) {
                var tr = $(this.parentElement.parentElement);
                var $tabla = $('#modal-pagos .tabla-pagos');
                var c = $('#modal-pagos').attr('data-categoria');

                $tabla.html('');
                $.ajax({
                    url: server + 'novios/pago/presupuesto/' + tr.attr('id'),
                    success: function(res) {
                        if (res.success) {
                            $('#modal-pagos .info-modal-pagos').addClass('hide');
                            $('#modal-pagos table').removeClass('hide');
                            for (var i in res.data) {
                                var p = res.data[i];

                                var tr = document.createElement('tr');
                                tr.setAttribute('id', p.id_pago);
                                tr.setAttribute('data-pagado', p.pagado);

                                var $tr = $(tr);
                                $tr.html(template_pago);
                                $tr.find('.datepicker').pickadate(es_datepicker);
                                $tr.find('.vencimiento').pickadate('picker').set('select', p.fecha_vencimiento, {format: 'yyyy-mm-dd'});
                                $tr.find('.vencimiento').on('change', changeVencimiento);
                                $tr.find('.fecha_pago').pickadate('picker').set('select', p.fecha_pago, {format: 'yyyy-mm-dd'});
                                $tr.find('.fecha_pago').on('change', changeFechaPago);
                                $tr.find('.pagado_por').val(p.pagado_por);
                                $tr.find('.pagado_por').on('change', changePagadoPor);
                                $tr.find('.modo_pago').val(p.modo_pago);
                                $tr.find('.modo_pago').on('change', changeModoPago);
                                $tr.find('.eliminar').on('click', deletePago);
                                $tr.find('.importe').val(toMoney(p.monto, false));
                                $tr.find('.importe').on('blur', changeImporte);

                                if (p.pagado == 0) {
                                    $tr.find('.cambiar-estado i').removeClass('fa-check');
                                    $tr.find('.cambiar-estado i').removeClass('green-text');
                                    $tr.find('.cambiar-estado i').addClass('red-text');
                                    $tr.find('.cambiar-estado i').addClass('fa-clock-o');
                                }
                                else {
                                    $tr.find('.cambiar-estado i').removeClass('red-text');
                                    $tr.find('.cambiar-estado i').removeClass('fa-clock-o');
                                    $tr.find('.cambiar-estado i').addClass('fa-check');
                                    $tr.find('.cambiar-estado i').addClass('green-text');
                                }

                                $tr.find('.cambiar-estado').on('click', updateEstado);
                                $tabla.append($tr);
                            }
                            inputMask.mask($tabla.find('input.importe'));
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $('#modal-pagos .info-modal-pagos').removeClass('hide');
                        $('#modal-pagos table').addClass('hide');
                    },
                });

                $('#modal-pagos').modal('open');
                $('#modal-pagos').attr('data-categoria', tr.attr('data-categoria'));
                $('#modal-pagos').attr('data-current-presupuesto', tr.attr('id'));
                $('#modal-pagos').attr('data-categoria', tr.attr('data-categoria'));
            }

        });

        function calcula_totales(categoria) {
            //GETTING TOTALS (ALL THE TABLES)
            var total = 0;
            $('input.aprox').each(function(index, val) {
                total += parseFloat(val.inputmask.unmaskedvalue());
            });
            $('.total-aproximado').html(toMoney(total + '', true));

            total = 0;
            $('input.final').each(function(index, val) {
                total += parseFloat(val.inputmask.unmaskedvalue());
            });
            $('.total-final').html(toMoney(total + '', true));

            total = 0;
            $('tbody tr td.pendiente p').each(function() {
                var t = this.innerHTML.replace('$', '');
                total += (parseFloat(t.replace(new RegExp(',', 'g'), '')));
            });
            $('.total-pendiente').html(toMoney(total + '', true));

            total = 0;
            $('tbody tr a.pagado').each(function() {
                var t = this.innerHTML.replace('$', '');
                total += (parseFloat(t.replace(new RegExp(',', 'g'), '')));
            });
            $('.total-pagado').html(toMoney(total + '', true));

            //GETTING CATEGORY TOTALS
            if (categoria) {
                var cat = '#cat-' + categoria;
                let totalCategoryPaid = 0;
                total = 0;

                $(`${cat} tbody input.aprox`).each(function(index, val) {
                    total += parseFloat(val.inputmask.unmaskedvalue());
                });
                $(`${cat} .total .aprox`).html(toMoney(total + '', true));
                $('#' + categoria).find('span').html(toMoney(total + '', true));

                total = 0;
                $(`${cat} tbody input.final`).each(function(index, val) {
                    total += parseFloat(val.inputmask.unmaskedvalue());
                });
                $(`${cat} .total .final`).html(toMoney(total + '', true));

                total = 0;
                $(`${cat} tbody tr td.pendiente`).each(function() {
                    var t;
                    if ($(this).has('p').length) {
                        t = $(this).find('p').html().replace('$', '');
                    }
                    else {
                        t = this.innerHTML.replace('$', '');
                    }
                    total += (parseFloat(t.replace(new RegExp(',', '', 'g'), '')));
                });
                $(`${cat} .total .pendiente`).html(toMoney(total + '', true));

                total = 0;
                $(`${cat} tbody tr a.pagado`).each(function() {
                    var t = this.innerHTML.replace('$', '');
                    total += (parseFloat(t.replace(new RegExp(',', '', 'g'), '')));
                });
                $(`${cat} .total .pagado`).html(toMoney(total + '', true));
            }
        }

        $(document).ready(function() {
            size_div(window);
            costo_aproximado();
            $('.tabs').tabs();

            $(window).on('resize', function() {
                size_div(window);
            });

            function size_div(width) {
                if ($(width).width() > 585) {
                    var size = ($('#presupuesto-content').height() - $('.barra-lateral').height() * 2) - 140;
                    $('.barra-lateral').css({'top': size + 'px'});
                }
                else {
                    $('.barra-lateral').css({'top': '0px'});
                }
            }

            function costo_aproximado() {
                $('.title-presupuesto').each(function() {
                    //                var content = $(this).siblings().eq("0").attr("id");
                    //                var table = $("#" + content + " .card-content table tfoot").children("tr").find("td").eq("2").text();
                    //                var content2 = $(this).children("p").children("span").text(table);
                    var content = $(this).siblings().eq('0').find('.card-content table tfoot tr td').eq('1').text();
                    $(this).find('p').children('span').text(content);
                });
            }

            $('.title-presupuesto').on('click', function() {
                var card = $(this).next();
                if (card.find('.card-content').css('display') == 'none') {
                    card.find('.card-content').slideDown();
                }
                else {
                    card.find('.card-content').slideUp();
                }
                setTimeout(function() {
                    size_div(window);
                }, 400);
            });
        });

        function change_status(id = null, tipo = null){
            // console.log(id);
            // console.log(document.getElementById(id+"_status").innerHTML == "donut_large");
            if(id != null){
                if(tipo=='web'){
                    if(document.getElementById(id+"_status").innerHTML == "donut_large"){
                        console.log("entro aqui");
                        status(id, "1");

                        document.getElementById(id+"_status").innerHTML = "check_circle";
                        document.getElementById(id+"_a").setAttribute('data-tooltip', 'Listo?');

                        console.log(id);

                    }else{
                        status(id, "0");

                        document.getElementById(id+"_status").innerHTML = "donut_large";
                        document.getElementById(id+"_a").setAttribute('data-tooltip', 'Cancelar?');
                    }

                }else if(tipo=='responsive'){

                    if(document.getElementById(id+"_statusR").innerHTML == "donut_large"){
                        console.log("entro aqui");
                        status(id, "1");

                        document.getElementById(id+"_statusR").innerHTML = "check_circle";
                        document.getElementById(id+"_aR").setAttribute('data-tooltip', 'Listo?');

                        console.log(id);

                    }else{
                        status(id, "0");

                        document.getElementById(id+"_statusR").innerHTML = "donut_large";
                        document.getElementById(id+"_aR").setAttribute('data-tooltip', 'Cancelar?');
                    }
                }
                
            }
            // if(document.getElementById(id+"_status").innerHTML == "check_circle"){
            //     document.getElementById(id+"_status").innerHTML = "donut_large";
            // }
        }
        function status(id = null, status = null){
            var reqUrl = server + 'index.php/novios/presupuesto/home/update';
            $.ajax({
                url: reqUrl,
                data: {
                    presupuesto: id,
                    type: "status",
                    data: status,
                },
                method: "POST",
                success: function (response) {
                    console.log(response.data);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    </script>
