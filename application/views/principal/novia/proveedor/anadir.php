<style>

    .ui-widget-content {
        background: #ffffff;
        color: #222222;
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    }
    .ui-widget {
        font-family: Verdana, Arial, sans-serif;
        font-size: 1.1em;
    }
    .ui-menu {
        list-style: none;
        padding: 0;
        margin: 0;
        display: block;
        outline: none;
    }
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        cursor: default;
    }
    .ui-front {
        z-index: 999999999;
    }
    .ui-menu .ui-menu-item {
        position: relative;
        margin: 0;
        padding: 5px 1em 5px .4em;
        cursor: pointer;
        min-height: 0;
    }
    .ui-menu .ui-menu-item:hover {
        background: #eeeeee;
    }
</style>
<div id="modal-proveedor" class="modal">
    <div class="modal-content">
        <h4>Guardar Proveedor</h4>
        <div class="divider"></div>
        <p>Escribe el nombre del proveedor</p>
        <input id="searchProvider">
        <div class="row">
        </div>
    </div>
    <div class="modal-footer">
        <ul class="pagination">
            <li class="waves-effect" id="previousPage">
                <a href="#!"><i class="material-icons">chevron_left</i></a>
            </li>
            <li>
                <span>
                    <span id="currentPage"></span>
                    /
                    <span id="lastPage"></span>
                </span>
            </li>
            <li class="waves-effect" id="nextPage">
                <a href="#!"><i class="material-icons">chevron_right</i></a>
            </li>
        </ul>
    </div>
</div>
<div class="col s12 m4" id="card-proveedor" style="display: none;">
    <div class="card">
        <a href="" target="_blank">
            <div class="card-image">
                <img src="http://materializecss.com/images/sample-1.jpg" style="height: 120px;">
                <span class="card-title">Card Title</span>
            </div>
        </a>
        <div class="card-content">
            <p style="text-overflow: ellipsis;overflow: hidden; height: 1em;  white-space: nowrap;">I am
                a very simple card. I am good at containing small bits of information. I am convenient
                because I require little markup to use effectively.</p>
        </div>
        <div class="card-action" style="display:flex;justify-content:center;">
            <a class="waves-effect waves-light btn save-provider">Guardar</a>
        </div>
    </div>
</div>
<input id="base-url" type="hidden" value="<?php echo base_url() ?>"/>
<script src="<?php echo base_url() ?>/dist/js/modal_anadir_proveedores.js"></script>
