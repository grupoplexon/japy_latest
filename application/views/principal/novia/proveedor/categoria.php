<!DOCTYPE html>
<html>
<head>
    <title>
        <?php echo "Mis proveedores: $categoria" ?>
    </title>
    <?php $this->view("principal/head") ?>
    <link href="<?php echo base_url() ?>dist/css/iconos-proveedor.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url() ?>dist/css/novios_proveedores.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php $this->view("principal/menu") ?>
<?php $this->view("principal/novia/menu") ?>
<div class="body-container">
    <div class="row">
        <a href="<?php echo base_url() ?>index.php/novios/proveedor/" class="btn  white dorado-2 left btn-block-small"
           style="font-size: 11px;margin-top: 10px"><i class="material-icons left">keyboard_arrow_left</i> Ver todos</a>
        <h5 class="col">Mis proveedores</h5>
    </div>
    <div class="divider"></div>
    <br>
    <div class="row">
        <div class=" col s12">
            <div class="row categorias ">
                <div class="wrapper-categorias" style="    width: 1800px;left: 0px">
                    <?php foreach ($totales->categorias as $key => $value) { ?>
                        <div class="col  tooltipped " style="width: 100px" data-position="top" data-delay="500"
                             data-tooltip="<?php echo $value->nombre ?>">
                            <div class="card z-depth-0 clickable">
                                <?php if ($value->total) { ?>
                                    <span class="badge grey darken-7 white-text "
                                          style="right: 10px;min-width: initial;z-index: 9;padding: 2px 4px;border-radius: 50%;font-size: 11px;top: 10px;">
                                                      <?php echo $value->total ?>
                                            </span>
                                <?php } ?>
                                <a href="<?php echo base_url() ?>index.php/novios/proveedor/categoria/<?php echo str_replace(" ",
                                        "-", strtolower($value->nombre)) ?>" class="clickable  waves-effect">
                                    <div class="card-img">
                                        <div class="proveedor x3 <?php echo icon_categoria($value->nombre) ?>">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <a class="btn-cetegorias prev valign-wrapper clickable">
                    <i class="fa fa-chevron-left valign dorado-2-text"></i>
                </a>
                <a class="btn-cetegorias next valign-wrapper clickable">
                    <i class="fa fa-chevron-right valign dorado-2-text"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <h5 class="col">
            <?php echo ucfirst($categoria_acentos) ?><br>
            <?php $t = count($proveedores); ?>
            <small class="grey-text"><?php echo $t ?> proveedor<?php echo $t > 1 ? "es" : "" ?>
                guardado<?php echo $t > 1 ? "s" : "" ?></small>
        </h5>
        <div class="col s12 m6 right">
            <button class="btn dorado-2 right  btn-block-on-small  waves-effect modal-trigger" href="#modal-proveedor">
                <i class="material-icons left">add</i> Guardar proveedor
            </button>
        </div>
    </div>
    <div class="divider"></div>
    <?php if ($proveedores) { ?>
        <div class="row">
            <?php foreach ($proveedores as $key => $p) { ?>
                <div class="col s12 m4">
                    <div class="card proveedor"
                         id="<?php echo $p->id_proveedor ?>"
                         data-logo="<?php echo $p->logo ? $p->logo->id_galeria : "" ?>"
                    >
                        <div class="card-image waves-effect waves-light">
                            <a href="<?php echo base_url()."boda-".$p->slug ?>">
                                <?php if ($p->principal) { ?>
                                    <img src="<?php echo base_url() ?>uploads/images/<?php echo $p->principal->nombre ?>">
                                <?php } else { ?>
                                    <img src="<?php echo base_url() ?>dist/img/slider1.png">
                                <?php } ?>
                                <span class="card-title"
                                      style="background: -webkit-linear-gradient(top, rgba(0,0,0,0) 3%,rgba(0, 0, 0, 1) 100%);width: 100%;text-shadow: 1px 1px 2px black;">
                                            <text class="nombre"><?php echo $p->nombre ?></text><br>
                                            <small>
                                                <?php echo $p->nombre_tipo_proveedor ?>
                                                (<?php echo $p->localizacion_estado ?>)
                                            </small>
                                        </span>
                            </a>
                        </div>
                        <div class="card-content">
                            <div class="row">
                                <a class='dropdown-button waves-effect btn z-depth-0 border <?php echo($p->estado < 2 ? "dorado-2 " : $p->estado < 5 ? " white black-text" : "green white-text") ?>'
                                   style="float: left; width: calc( 100% - 56px );border: 1px solid #c1c1c1;"
                                   data-beloworigin="true"
                                   data-proveedor="<?php echo $p->id_proveedor ?>"
                                   data-estado="<?php echo $p->estado ?>"
                                   data-activates='mark-<?php echo $p->id_proveedor ?>'> <?php echo $this->categoria->label_select_estado($p->estado); ?>
                                    <i class="material-icons right">keyboard_arrow_down</i></a>
                                <ul id='mark-<?php echo $p->id_proveedor ?>' class='dropdown-content'>
                                    <li><a class="estado gris-2-text" data-value="0"><i
                                                    class="fa fa-times-circle red-text darken-5"></i> No disponible</a>
                                    </li>
                                    <li><a class="estado gris-2-text" data-value="1"><i
                                                    class="fa fa-times-circle red-text darken-5"></i> Descartado</a>
                                    </li>
                                    <li><a class="estado gris-2-text" data-value="2"><i
                                                    class="fa fa-clock-o orange-text darken-5"></i> Revisando</a></li>
                                    <li><a class="estado gris-2-text" data-value="3"><i
                                                    class="fa fa-clock-o orange-text darken-5"></i> Preseleccionado</a>
                                    </li>
                                    <li><a class="estado gris-2-text" data-value="4"><i
                                                    class="fa fa-clock-o orange-text darken-5"></i> Negociando</a></li>
                                    <li><a class="estado gris-2-text" data-value="5"><i
                                                    class="fa fa-check green-text darken-5"></i> Reservando</a></li>
                                </ul>
                                <a class=" btn-flat eliminar gris-1-text waves-effect"
                                   data-proveedor="<?php echo $p->id_proveedor ?>" style="    padding: 0px 17px;">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                            <div class="row">
                                <div class="col s8 cal">
                                    <b class="label" data-label="¿Que te ha parecido?">¿Que te ha parecido?</b>
                                    <br>
                                    <br>
                                    <i class="fa fa-heart-o icon-cal fa-2x clickable <?php echo $p->calificacion >= 0 ? "dorado-1-text" : "grey-text" ?>"
                                       data-value="0" data-proveedor="<?php echo $p->id_proveedor ?>"
                                       data-label="Insuficiente"></i>
                                    <i class="fa fa-heart-o icon-cal fa-2x clickable <?php echo $p->calificacion >= 1 ? "dorado-1-text" : "grey-text" ?>"
                                       data-value="1" data-proveedor="<?php echo $p->id_proveedor ?>"
                                       data-label="Medio"></i>
                                    <i class="fa fa-heart-o icon-cal fa-2x clickable <?php echo $p->calificacion >= 2 ? "dorado-1-text" : "grey-text" ?>"
                                       data-value="2" data-proveedor="<?php echo $p->id_proveedor ?>"
                                       data-label="Bueno"></i>
                                    <i class="fa fa-heart-o icon-cal fa-2x clickable <?php echo $p->calificacion >= 3 ? "dorado-1-text" : "grey-text" ?>"
                                       data-value="3" data-proveedor="<?php echo $p->id_proveedor ?>"
                                       data-label="Muy bueno"></i>
                                    <i class="fa fa-heart-o icon-cal fa-2x clickable <?php echo $p->calificacion >= 4 ? "dorado-1-text" : "grey-text" ?>"
                                       data-value="4" data-proveedor="<?php echo $p->id_proveedor ?>"
                                       data-label="Excelente"></i>
                                </div>
                                <div class="col s4">
                                    <b>Precio</b>
                                    <br>
                                    <br>
                                    <div class="precio">
                                        $<input class="form-control money "
                                                value="<?php echo $p->precio ?>"
                                                data-proveedor="<?php echo $p->id_proveedor ?>"
                                        >
                                    </div>
                                </div>
                            </div>
                            <a class="btn-flat dorado-2 white-text btn-block-on-small center  waves-effect modal-trigger <?php echo $p->calidad_servicio ? "hide" : "btn-recomendar" ?>"
                               data-proveedor="<?php echo $p->id_proveedor ?>" href="#modal-recomendar"
                               style="width: 100%">
                                RECOMENDAR
                            </a>
                        </div>
                        <div class="card-action">
                            <a class="dropdown-button gris-1-text ver_telefono"
                               data-proveedor="<?php echo $p->id_proveedor ?>" data-activates='dropdown-phone'
                               data-beloworigin="true" data-constrainwidth="false"><i class="fa fa-phone"></i> Tel&eacute;fono</a>
                            <ul id='dropdown-phone' class='dropdown-content  valign-wrapper' style="width: 300px;">
                                <div class="row">
                                    <div class="col s12 center" style="    margin: 0px;padding: 0px;">
                                        <h5 class="valign dorado-2-text"
                                            style="width: 100%; background: #4d4e53; margin: 0px 0px 10px 0px; padding: 10px 0px;">
                                            <b>
                                                <i class="fa fa-phone"></i>
                                            </b>
                                            <?php echo $p->contacto_telefono ?>
                                        </h5>
                                        <label>No olvides decir que vienes de japybodas.com</label>
                                    </div>
                                </div>
                            </ul>
                            <a id="contactar" data-proveedor="<?php echo $p->id_proveedor ?>"
                               class="gris-1-text modal-trigger" href="#modal_mas_informacion"><i
                                        class="fa fa-envelope"></i> Contactar</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="card-panel">
                <div class="row">
                    <div class="col s12 m4">
                        <div class="col s12 center">
                            <div class=" circle grey lighten-3 center "
                                 style="width: 290px;height: 290px;padding: 20px;">
                                <div class="proveedor <?php echo icon_categoria($categoria) ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m8">
                        <div class="card-panel blue lighten-5 z-depth-0 ">
                            No tienes proveedores guardados de <b><?php echo ucfirst($categoria) ?></b>
                        </div>
                        <div class="col s6">
                            <i class="fa fa-map-marker fa-2x grey-text darken-3"></i>
                            <b>Ya los tienes?</b>
                            <p>
                                Genial, agregarlos a tu lista de proveedores
                            </p>
                            <button class="btn dorado-2 btn-block-small  waves-effect modal-trigger"
                                    href="#modal-proveedor">
                                <i class="fa fa-plus left "></i> Añadir proveedor
                            </button>
                        </div>
                        <div class="col s6">
                            <i class="fa fa-search fa-2x grey-text darken-3"></i>
                            <b>¿Todavia no te has decidido?</b>
                            <p>
                                No te preocupes te ayudamos a encontrar a tu proveedor
                            </p>
                            <?php $geolocation = json_decode($_COOKIE["geolocation"]) ?>
                            <?php $estado = $geolocation->regionName != "-" ? $geolocation->regionName : "Jalisco" ?>
                            <select id="select-estado" class="form-control browser-default states"
                                    data-default="<?php echo $estado ?>">
                                <option class=""></option>
                            </select>
                            <a id="btn-buscar" class="btn dorado-2 btn-block-small waves-effect right"
                               href="<?php echo base_url() ?>index.php/proveedores/categoria/<?php echo str_replace(" ",
                                       "-", $categoria) ?>/<?php echo $estado ?>">
                                Buscar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<!--        <div id="modal-proveedor" class="modal">
            <div class="modal-content">
                <h4>Añadir Proveedor</h4>
                <div  class="divider"></div>
                <p>Escribe el nombre del proveedor</p>
                <input class="form-control autocomplate-proveedor" >
            </div>
            <div class="modal-footer">
                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat" id="btn-anadir">Añadir</a>
                <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancelar</a>
            </div>
        </div>-->


<div id="modal_mas_informacion" class="modal">
    <?php
    //CLRSF
    function fRand($len)
    {
        $str = '';
        $a   = "abcdefghijklmnopqrstuvwxyz0123456789";
        $b   = str_split($a);
        for ($i = 1; $i <= $len; $i++) {
            $str .= $b[rand(0, strlen($a) - 1)];
        }

        return $str;
    }

    $this->session->unset_userdata("clrsf");
    $this->session->unset_userdata("clrsf_value");
    $this->session->set_userdata("clrsf", fRand(5));
    $this->session->set_userdata("clrsf_value", fRand(5));
    ?>
    <?php $cliente = $this->checker->getCliente() ?>
    <div class="modal-content">
        <i class="fa fa-times fa-2x modal-close modal-action dorado-2-text"
           style="position: absolute; right: 5px;top: 5px;"></i>
        <div class="row" style="background: #8C8D8F;margin: -24px -24px;color: white;text-shadow: 1px 1px 2px black;">
            <img style="width: 110px;background: white;margin: 10px 5px 0px 12px;border: 2px solid #C9C9C9;box-shadow: 1px 1px 10px #595959;"
                 class="col s2 logo-proveedor"
                 src="">
            <div class="col s10" style="width: calc( 100% - 130px )">
                <h5>Solicitar informaci&oacute;n a
                    <text class="nombre-proveedor"></text>
                </h5>
                <p>Rellena este formulario y
                    <text class="nombre-proveedor"></text>
                    se pondr&aacute; en contacto contigo en breve. Todos los datos que env&iacute;es ser&aacute;n
                    tratados de forma confidencial.
                </p>
            </div>
        </div>
        <form method="POST" class="form-validate form-to-ajax"
              data-success="success_solicitud"
              data-error="error_solicitud">
            <!--action="<?php echo base_url() ?>index.php/escaparate/enviar_solicitud_informacion/<?php echo $proveedor->id_proveedor ?>"-->
            <input type="hidden" name="<?php echo $this->session->userdata("clrsf") ?>"
                   value="<?php echo $this->session->userdata("clrsf_value") ?>">
            <input type="hidden" name="proveedor" class="id-proveedor" value="">
            <div class="row">
                <div class="col s12 m6">
                    <p>
                        <label>Nombre</label>
                        <input name="nombre" class="form-control validate[required]"
                               value="<?php echo $this->session->userdata("nombre") ?>">
                    </p>
                    <p>
                        <label>Correo</label>
                        <input name="correo"
                               class="form-control validate[required,custom[email]]" <?php echo $this->checker->isLogin() ? "readonly" : "" ?>
                               value="<?php echo $this->session->userdata("correo") ?>">
                    </p>
                    <p>
                        <label>Tel&eacute;fono</label>
                        <input name="telefono" class="form-control validate[required,custom[phone]]"
                               name="telefono" <?php echo $this->checker->isLogin() && $cliente && $cliente->telefono ? "readonly" : "" ?>
                               value="<?php echo $cliente ? $cliente->telefono : "" ?>">
                    </p>
                    <p>
                        <label>Fecha estimada</label>
                        <input name="fecha" type="date" class="form-control validate[required]" name="fecha"
                               value="<?php echo $boda ? date_format(date_create($boda->fecha_boda), 'Y-m-d') : "" ?>">
                    </p>
                    <?php if ($categoria == "salon" || $categoria == "catering") { ?>
                        <p>
                            <label>Invitados</label>
                            <input name="invitados" type="number" step="any"
                                   class="form-control validate[required,min[0]]" name="invitados" min="0"
                                   value="<?php echo $boda ? $boda->no_invitado : "250" ?>">
                        </p>
                    <?php } ?>
                    <?php if ( ! $this->checker->isLogin()) { ?>
                        <p>
                            <select class="form-control browser-default" name="genero" required>
                                <option value="" disabled selected>Yo soy*</option>
                                <option value="1">Novio</option>
                                <option value="2">Novia</option>
                            </select>
                        </p>
                    <?php } ?>
                </div>
                <div class="col s12 m6">
                    <p>
                        <label>Comentario</label>
                        <textarea name="comentario" class="form-control validate[required,maxSize[600]]" maxlength="600"
                                  style="height: 250px"></textarea>
                    </p>
                </div>
            </div>
            <div class="row">
                <p class="">
                    Al presionar "Enviar" te est&aacute;s dando de alta y aceptando las condiciones legales de
                    japybodas.com .
                </p>
                <p class="pull-right">
                    <button type="submit" class="btn dorado-2 modal-action modal-close">
                        Enviar
                    </button>
                </p>
            </div>
        </form>
    </div>
</div>


<div id="modal_enviado" class="modal modal-fixed-footer">
    <i class="fa fa-times fa-2x modal-close modal-action dorado-2-text"
       style="position: absolute; right: 5px;top: 5px;"></i>
    <div class="row">
        <div class="col s2"
             style="bottom: 0px; left: 0px;position: initial;height: 92%;background: #8C8D8F;color: white;">
            <div style="height: 110px" class="valign-wrapper">
                <div class="valign center" style="width: 100%">
                    <i class="fa fa-money fa-3x"></i>
                </div>
            </div>
            <p style="text-transform: uppercase">
                consigue el mejor presupuesto para tu boda
            </p>
            <p>
                Solicita y compara diferentes presupuestos para conseguir el m&aacute;s adecuado para tu boda.
            </p>
        </div>
        <div class="col s10" style="padding: 0px">
            <div class="modal-content col s12" style="position: initial">
                <h5>Consigue el mejor presupuesto para tu boda</h5>
                <div class="">
                    <div class="relacionados">
                        <h6>Los novios interesados en <b class="nombre-proveedor"></b> pidieron presupuesto a las
                            siguientes empresas </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <a onclick="location.reload();"
           class=" modal-action modal-close waves-effect waves-green btn-flat dorado-2-text">Continuar</a>
    </div>
</div>


<div id="modal-recomendar" class="modal modal-fixed-footer" style="overflow: hidden;padding: 0px;">
    <div class="modal-content" style="padding: 0px">
        <i class="fa fa-times fa-2x modal-close modal-action dorado-2-text"
           style="position: absolute; right: 5px;top: 5px;"></i>
        <div class="row" style="    background: #8C8D8F;color: white;text-shadow: 1px 1px 2px black; margin: 0px;">
            <img style="width: 110px;background: white;margin: 10px 5px 0px 12px;border: 2px solid #C9C9C9;box-shadow: 1px 1px 10px #595959;  position: absolute;"
                 class="col s2 logo-proveedor"
                 src="">
            <div class="col s10" style="width: calc( 100% - 130px );    margin-left: 130px;">
                <h5>Recomienda a
                    <text class="nombre-proveedor"></text>
                </h5>
                <p>
                    Gracias por recomendar a
                    <text class="nombre-proveedor"></text>
                    ,
                    tu opinion es muy importante.<br>
                    <small>Tu recomendaci&oacute;n puede aparecer en el escaparate del proveedor.</small>
                </p>
            </div>
        </div>
        <div style="overflow-y: auto;height: 435px;padding: 24px 14px;">
            <form>
                <div class="row cal-form" data-value="calidad">
                    <p class="col m4 s12">Calidad del servicio</p>
                    <p class="col m4 s8 grey-text  clickable">
                        <i class="material-icons star ">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                    </p>
                    <p class="label col m4 s4" style="font-size: 18px;">
                    </p>
                </div>
                <div class="divider"></div>
                <div class="row cal-form" data-value="respuesta">
                    <p class="col m4 s12">Respuesta</p>
                    <p class="col m4 s8 grey-text  clickable">
                        <i class="material-icons star ">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                    </p>
                    <p class="label col m4 s4" style="font-size: 18px;">
                    </p>
                </div>
                <div class="divider"></div>
                <div class="row cal-form" data-value="relacion_calidad">
                    <p class="col m4 s12">Relaci&oacute;n calidad/precio</p>
                    <p class="col m4 s8 grey-text  clickable">
                        <i class="material-icons star ">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                    </p>
                    <p class="label col m4 s4" style="font-size: 18px;">
                    </p>
                </div>
                <div class="divider"></div>
                <div class="row cal-form" data-value="flexibilidad">
                    <p class="col m4 s12">Flexibilidad</p>
                    <p class="col m4 s8 grey-text  clickable">
                        <i class="material-icons star ">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                    </p>
                    <p class="label col m4 s4" style="font-size: 18px;">
                    </p>
                </div>
                <div class="divider"></div>
                <div class="row cal-form" data-value="profesionalismo">
                    <p class="col m4 s12">Profesionalidad</p>
                    <p class="col m4 s8 grey-text  clickable">
                        <i class="material-icons star ">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                        <i class="material-icons star">star_border</i>
                    </p>
                    <p class="label col m4 s4" style="font-size: 18px;">
                    </p>
                </div>
                <div class="divider"></div>
                <div class="row">
                    <p class="col m4 s12">Reseña</p>
                    <textarea class="form-control" style="height: 200px"></textarea>
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <p class=" error red-text pull-left hide">Rellena todos los datos para poder recomendar</p>
        <a href="#!" class="modal-action waves-effect waves-green btn-flat btn-modal-recomendar ">Recomendar</a>
        <a href="#!" class="modal-action modal-close waves-effect btn-flat  ">Cancelar</a>
    </div>
</div>
<div class="hide" id="template-relacionado">
    <div class="col s12 m6">
        <div class="card">
            <div class="card-image">
                <img src="{{url_logo}}">
                <span class="card-title"
                      style="font-size: 12px;background: -webkit-linear-gradient(top, rgba(0,0,0,0) 3%,rgba(0, 0, 0, 1) 100%);width: 100%;text-shadow: 1px 1px 2px black;">
                            {{nombre}}<br><small>{{poblacion}}</small>
                        </span>
            </div>
            <a class="btn dorado-2 mas-informacion" href="{{url_mas}}" style="font-size: 12px;width: 100%;">
                <i class="fa fa-envelope"></i> M&aacute;s informaci&oacute;n
            </a>
        </div>
    </div>
</div>
<?php $this->view("principal/view_footer") ?>
<?php $this->view("principal/foot") ?>
<script type="text/javascript" src="<?php echo base_url() ?>dist/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js"></script>
<?php $this->view("principal/novia/proveedor/anadir") ?>
<script>
    var server = "<?php echo base_url() ?>";
    $(document).ready(function () {
        $('.modal').modal();
        var left = parseInt($(".categorias .wrapper-categorias").get(0).style.left.replace("px", ""));
        $(".money").inputFormat();
        $(".money").inputFormat("format");
        $(".proveedor .estado").on("click", function () {
            var $this = $(this);
            var estado = $this.data("value");
            var select = $("a[data-activates='" + $this.parent().parent().attr("id") + "']");
            if (estado < 2) {
                select.attr("class", "dropdown-button btn z-depth-0 border dorado-2 white-text");
            } else if (estado < 5) {
                select.attr("class", "dropdown-button btn z-depth-0 border white black-text ");
            } else {
                select.attr("class", "dropdown-button btn z-depth-0 border green white-text ");
            }
            select.html($this.html() + '<i class="material-icons right">keyboard_arrow_down</i>');
            $.ajax({
                url: server + "index.php/novios/proveedor/change_estado/" + select.data("proveedor"),
                method: "POST",
                data: {
                    'estado': estado
                }
            });
        });

        $(".ver_telefono").on("click", function () {
            $.getJSON(server + "index.php/escaparate/telefono/" + $(this).data("proveedor"));
        });

        $("#contactar").on("click", function () {
            var $modal = $("#modal_mas_informacion");
            var $proveedor = $("#" + $(this).data("proveedor"));
            window.nombre_proveedor = $proveedor.find(".nombre").html();
            $modal.find(".nombre-proveedor").html($proveedor.find(".nombre").html());
            var logo = $proveedor.data("logo");
            if (logo && logo != "") {
                $modal.find(".logo-proveedor").attr("src", server + "uploads/images/" + $proveedor.data("logo"));
            } else {
                $modal.find(".logo-proveedor").attr("src", server + "dist/img/blog/perfil.png");
            }
            $modal.find("form").attr("action", server + "index.php/escaparate/enviar_solicitud_informacion/" + $proveedor.attr("id"));
        });

        $(".btn-recomendar").on("click", function () {
            $("#modal-recomendar .error").addClass("hide");
            var $modal = $("#modal-recomendar");
            var $proveedor = $("#" + $(this).data("proveedor"));
            var logo = $proveedor.data("logo");
            $modal.find(".nombre-proveedor").html($proveedor.find(".nombre").html());
            if (logo && logo != "") {
                $modal.find(".logo-proveedor").attr("src", server + "uploads/images/" + $proveedor.data("logo"));
            } else {
                $modal.find(".logo-proveedor").attr("src", server + "dist/img/blog/perfil.png");
            }
            $modal.data("proveedor", $(this).data("proveedor"));
            $modal.modal("open");
            $(".cal-form .star ").removeClass("dorado-2-text");
            $(".cal-form .label ").html("");
            $("#modal-recomendar textarea").val("");

        });

        $(".cal-form").each(function () {
            this.getCal = function () {
                return $(this).find(".star.dorado-2-text").length;
            };
            this.getKey = function () {
                return $(this).data("value");
            };
        });

        $(".cal-form .star").on("click", function () {
            var values = ["Insuficiente", "Medio", "Bueno", "Muy bueno", "Excelente"];
            var $this = $(this);
            var container = $this.parent();
            container.find(".dorado-2-text").removeClass("dorado-2-text");
            do {
                $this.addClass("dorado-2-text");
                //$this.addClass("darken-5");
                $this = $this.prev();
            } while ($this.length !== 0);
            container.parent().find(".label").html(values[container.find(".dorado-2-text").length - 1]);
            container.parent().removeClass("red-text");
        });

        $(".btn-modal-recomendar").on("click", function () {
            $("#modal-recomendar .error").addClass("hide");
            var cal = true;
            var data = {};
            $(".cal-form").each(function () {
                if (this.getCal() === 0) {
                    cal = false;
                    $(this).addClass("red-text");
                } else {
                    data[this.getKey()] = this.getCal();
                }
            });
            if (cal && $("#modal-recomendar textarea").val() != "") {
                var $modal = $("#modal-recomendar");
                data.proveedor = $modal.data("proveedor");
                data.resena = $("#modal-recomendar textarea").val();
                $.ajax({
                    url: server + "index.php/novios/proveedor/recomendar",
                    method: "POST",
                    data: data,
                    success: function () {
                        $("#" + data.proveedor + " .btn-recomendar").off("click");
                        $("#" + data.proveedor + " .btn-recomendar").addClass("hide");
                    },
                    complete: function () {
                        $(".cal-form .star ").removeClass("dorado-2-text");
                        $(".cal-form .label ").html("");
                        $("#modal-recomendar textarea").val("");
                        $modal.modal("close");
                    }
                });
            } else {
                $("#modal-recomendar .error").removeClass("hide");
                return;
            }
        });

        $(".money").on("click", function () {
            $(this).select();
        });
        $(".money").change(update_precio);
        $(".money").keypress(function (e) {
            if (parseInt(e.which) === 13) {
                update_precio.apply(this, []);
            }
        });

        $("#select-estado").on("change", function () {
            var href = '<?php echo base_url() ?>index.php/proveedores/categoria/<?php echo str_replace(" ", "-",
                    $categoria) ?>/' + $(this).val().replaceAll(" ", "-");
            $("#btn-buscar").attr("href", href);
        });

        String.prototype.replaceAll = function (search, replacement) {
            var target = this;
            return target.replace(new RegExp(search, 'g'), replacement);
        };

        $(".eliminar").on("click", function () {
            if (confirm("¿Estas seguro de eliminar el proveedor de tu boda?")) {
                var $self = $(this);
                $.ajax({
                    url: server + "index.php/novios/proveedor/eliminar/" + $self.data("proveedor"),
                    method: "POST",
                    data: {
                        proveedor: $self.data("proveedor")
                    }
                }).done(function () {
                    setTimeout(function () {
                        $("#" + $self.data("proveedor")).parent().remove();
                        if ($("#" + $self.data("proveedor")).parent().parent().children().length == 0) {
                            location.reload();
                        }
                    }, 1000);
                    $("#" + $self.data("proveedor")).addClass("eliminado");
                });
            }
        });

        $(".categorias .next").on("click", function () {
            var left = $(".categorias .wrapper-categorias").get(0).style.left;
            left = left.replace("px", "");
            var top = $(".categorias").width() - 80;
            if (left > (1800 - top) * -1) {
                left -= 200;
                $(".categorias .wrapper-categorias").get(0).style.left = left + "px";
            }
        });

        $(".categorias .prev").on("click", function () {
            var left = $(".categorias .wrapper-categorias").get(0).style.left;
            left = parseInt(left.replace("px", ""));
            left += 200;
            if (left < 0) {
                $(".categorias .wrapper-categorias").get(0).style.left = left + "px";
            } else {
                $(".categorias .wrapper-categorias").get(0).style.left = "0px";
            }
        });

        $(".icon-cal").hover(function () {
            $(".icon-cal").removeClass("darken-5");
            $(".cal .label").html($(".cal .label").data("label"));
            $(this).addClass("darken-5");
            $(this).parent().parent().find(".cal .label").html($(this).data("label"));
            var icon = $(this);
            while (true) {
                icon = icon.prev();
                if (icon.hasClass("icon-cal")) {
                    icon.addClass("darken-5");
                } else {
                    break;
                }
            }
        });

        $(".icon-cal").on("click", function () {
            var $this = $(this);
            $(this).parent().parent().find(".icon-cal").addClass("grey-text")
            $(this).parent().parent().find(".icon-cal").removeClass("dorado-1-text");
            $this.addClass("darken-5");
            $.ajax({
                url: server + "index.php/novios/proveedor/calificar/" + $this.data("proveedor"),
                method: "POST",
                data: {
                    cal: $this.data("value"),
                }
            });
            $(this).parent().parent().find(".cal .label").html($this.data("label"));
            var icon = $(this);
            while (true) {
                if (icon.hasClass("icon-cal")) {
                    icon.removeClass("grey-text");
                    icon.addClass("dorado-1-text");
                } else {
                    break;
                }
                icon = icon.prev();
            }
        });

        $(".row.categorias").swipe({
            triggerOnTouchEnd: true,
            swipeStatus: swipeStatus,
            allowPageScroll: "vertical",
            threshold: 75,
            triggerOnTouchLeave: false
        });
        $(".row.categorias").on("mouseleave", function () {
            $(".row.categorias").swipe("disable");
            $(".row.categorias").swipe("enable");
        });

        function swipeStatus(event, phase, direction, distance) {
            //If we are moving before swipe, and we are going L or R in X mode, or U or D in Y mode then drag.
            if (phase == "start") {
                left = parseInt($(".categorias .wrapper-categorias").get(0).style.left.replace("px", ""));
            } else if (phase == "move" && (direction == "left" || direction == "right")) {
                if (direction == "left") {
                    $(".categorias .wrapper-categorias").get(0).style.left = (left - distance) + "px";
                } else if (direction == "right") {
                    $(".categorias .wrapper-categorias").get(0).style.left = (left + distance) + "px";
                }
            } else if (phase == "cancel") {
                // scrollImages(IMG_WIDTH * currentImg, speed);
            } else if (phase == "end") {
                if (direction == "right") {
                    $(".categorias .wrapper-categorias").get(0).style.left = (left + distance) + "px";
                    left = parseInt($(".categorias .wrapper-categorias").get(0).style.left.replace("px", ""));
                    if (left > 0) {
                        setTimeout(function () {
                            $(".categorias .wrapper-categorias").get(0).style.left = "0px";
                        }, 500);
                    }
                } else if (direction == "left") {
                    $(".categorias .wrapper-categorias").get(0).style.left = (left - distance) + "px";
                    left = parseInt($(".categorias .wrapper-categorias").get(0).style.left.replace("px", ""));
                    var top = $(".categorias").width() - 80;
                    if (left < (1800 - top) * -1) {
                        setTimeout(function () {
                            $(".categorias .wrapper-categorias").get(0).style.left = ((1800 - top) * -1) + "px";
                        }, 500);
                    }
                }
            }
        }

        function update_precio() {
            var $self = $(this);
            $.ajax({
                url: server + "index.php/novios/proveedor/change_precio/" + $self.data("proveedor"),
                method: "POST",
                data: {
                    precio: this.value.replace(new RegExp(",", 'g'), "")
                }
            }).done(function () {
                $self.addClass("active");
                setTimeout(function () {
                    $self.removeClass("active");
                }, 2000);
            }).fail(function () {
                $self.addClass("error");
                setTimeout(function () {
                    $self.removeClass("error");
                }, 2000);
            });
        }
    });


    function success_solicitud(resp) {
        var template = null;
        $("#modal_enviado").find(".nombre-proveedor").html(window.nombre_proveedor);
        for (var i in resp.data) {
            var p = resp.data[i];
            template = $("#template-relacionado").html();
            var template = template.replace("{{nombre}}", p.nombre)
                .replace("{{poblacion}}", p.localizacion_poblacion)
                .replace("{{url_mas}}", server + "boda-" + p.slug);
            if (p.logo != null && p.logo != "") {
                template = template.replace("{{url_logo}}", server + "uploads/images/" + p.logo);
            } else {
                template = template.replace("{{url_logo}}", server + "/dist/img/blog/perfil.png");
            }
            $(".relacionados").append(template);
        }
        $("#modal_enviado").modal("open");
    }
</script>

</body>
</html>




