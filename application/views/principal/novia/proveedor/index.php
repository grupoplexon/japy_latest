<!DOCTYPE html>
<html>
    <head>
        <title>
            Mis proveedores
        </title>
        <?php $this->view("principal/newheadernovia"); ?>
        <style>
            #total-proveedores i {
                font-size: 2em;
                padding: 10px;

                border-radius: 50%;
                margin-bottom: 6px;
            }

            #total-proveedores .fa.fa-heart {
                border: 1px solid #92151b;
            }

            #total-proveedores i.green-text {
                border: 1px solid #4caf50;
            }

            #total-proveedores i.orange-text {
                border: 1px solid #ff9800;
            }

            #total-proveedores {
                margin-top: 15px;
            }

            @media screen and (max-width: 425px) {
                .d-flex {
                    display: initial !important;
                }
            }

            @media (max-width: 990px) {
                .img-responsive {
                    height: 350px;
                    width: auto;
                }
            }

            @media (min-width: 1000px) {
                .img-responsive {
                    width: 100%;
                    height: auto;
                }
            }
            .tags{
                margin:0;
                padding:0;
                /* position:absolute; */
                right:24px;
                bottom:-12px;
                list-style:none;
            }
            .tags li, 
            .tags a{
                float:left;
                height:24px;
                line-height:24px;
                position:relative;
                font-size:11px;
                margin-block-end: 5px;
            }
            .tags a{
                margin-left:20px;
                padding:0 10px 0 12px;
                background:#0089e0;
                color:#fff;
                text-decoration:none;
                -moz-border-radius-bottomright:4px;
                -webkit-border-bottom-right-radius:4px; 
                border-bottom-right-radius:4px;
                -moz-border-radius-topright:4px;
                -webkit-border-top-right-radius:4px;    
                border-top-right-radius:4px;    
            }
            /* parte izquierda de cada etiqueta en forma de triangulo */
            .tags a:before{
                content:"";
                float:left;
                position:absolute;
                top:0;
                left:-12px;
                width:0;
                height:0;
                border-color:transparent #0089e0 transparent transparent;
                border-style:solid;
                border-width:12px 12px 12px 0;      
            }
            /* y por ultimo un :hover para darle algo mas de "estilo" */
            .tags a:hover{
                background:#555;
            }   
            .tags a:hover:before{
                border-color:transparent #555 transparent transparent;
            }
        </style>
        <link href="<?php echo base_url() ?>dist/css/iconos-proveedor.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php $this->view("principal/novia/menu") ?>
        <?php $provider = Provider::where("id_usuario", 1591)->first(); ?>
        <div class="body-container">
            <div class="row">
                <div class="col s12">
                    <div class="row">
                        <div class="col s12 m4">
                            <h4>
                                Mis proveedores
                            </h4>
                        </div>
                        <div class="col s12 m8">
                            <div class="card-panel" style="background: #FFFFCC!important">
                                Aqu&iacute; encontrar&aacute;s a tus proveedores seleccionados favoritos, as&iacute; podr&aacute;s
                                conocerlos y posteriormente seleccionar a tu proveedor final para cada categor&iacute;a.
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <br>
                        <div class="col s12 m6 right">
                            <button class="btn dorado-2 right  btn-block-on-small  waves-effect modal-trigger"
                                    href="#modal-proveedor" style="z-index: 0; position: static;">
                                <i class="fa fa-plus left"></i> Guardar proveedor
                            </button>
                        </div>
                    </div>
                    <!-- Starts here -->
                    <div class="row">
                        <div class="row">
                            <?php if ($categories->count()) : ?>
                                <?php foreach ($categories as $category) : ?>
                                    <div class="col s12">
                                        <h4><?php echo $category->name; ?></h4>
                                            <div class="d-flex">
                                                <?php foreach ($proveedores as $provider) : 
                                                    if($provider->id_category==$category->id) {?>
                                                        <div class="col s12 m3 d-flex ml-0">
                                                        <a href="<?php echo(base_url()."boda-".$provider->slug) ?>"
                                                           class="d-flex" style=" color: inherit; text-decoration: none;">
                                                            <div class="card">
                                                                <div class="card-image">
                                                                    <?php if ($provider->image): ?>
                                                                        <img src="<?php echo(base_url().'uploads/images/'.$provider->image) ?>"
                                                                             class="img-responsive" style="object-fit: cover; height: 15rem">
                                                                    <?php else : ?>
                                                                        <img src="<?php echo(base_url().'/dist/img/slider1.png') ?>"
                                                                             class="img-responsive" style="object-fit: cover;height: 15rem">
                                                                    <?php endif ?>
                                                                </div>
                                                                <div class="card-content">
                                                                    <p style="color: black; font-size: 1.5rem;">
                                                                        <?php echo $provider->nombre ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <?php } endforeach ?> 
                                            </div>
                                    </div>
                                <?php endforeach ?>
                            <?php else: ?>
                                <span>¡Aun no tienes ningun proveedor!</span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m4">
                            <h4>
                                Mis inspiraciones
                            </h4>
                        </div>
                        <div class="col s12 m8">
                            <div class="card-panel" style="background: #FFFFCC!important">
                                Aqu&iacute; encontrar&aacute;s tus inspiraciones seleccionadas favoritas, as&iacute; podr&aacute;s
                                recordarlas y posteriormente seleccionar tus gustos al final para cada categor&iacute;a.
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col l10">
                        <br>
                        <section class="row container-imagenes">
                            <?php if (!empty($inspiracion)) { ?>
                                <?php for ($i=0; $i<sizeOf($inspiracion); $i++) { //print_r($inspiracion[$i]['id_inspiracion']); ?>
                                    <div id="<?php echo $inspiracion[$i]['id_inspiracion'] ?>"
                                        class="imagen col s8 m5 l3 offset-s2">
                                        <div class="card">
                                            <div class="card-image waves-effect waves-block waves-light">
                                                <img class="activator image-card" src="<?php echo (base_url().'uploads/images/'.$inspiracion[$i]['nombre']) ?>" style="object-fit: fill;height: 17.7rem;max-height: 17.7rem!important">
                                            </div>
                                            <div class="card-content center-align" style="display: none">
                                        <span class="card-title activator grey-text text-darken-4 truncate">
                                            <!-- <?php //echo $imagen->nombre ?> -->
                                        </span>
                                            </div>
                                            <div class="card-reveal">
                                        <span class="card-title grey-text text-darken-4">Editar
                                            <i class="material-icons right">close</i></span>
                                            <br>
                                                    <button class="btn-flat btn-block blue white-text modal-trigger mostrar" data-target="modal1" id="<?php echo $inspiracion[$i]['id_inspiracion'] ?>">Información</button>
                                                <p>
                                                    <button data-id="<?php echo $inspiracion[$i]['id_inspiracion'] ?>"
                                                            class="btn-flat btn-block delete red white-text" style="width: 104%" id="deleteInsp">
                                                        Eliminar
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            
                            <!-- Modal Structure -->
                            <!-- height: 287px;
                                height: 19em; -->
                            <div id="modal1" class="modal" style="overflow-x: hidden;">
                            

                            <!-- <div class="col m6 s6 l4 modalImg"style="
                            display: flex;
                            align-items: center;
                            max-width: 100%;
                            overflow: hidden;">
                                <img id="imgInfo" class="img-fluid" style="">
                            </div> -->

                            <!-- <div class="col m6 s6 l4 modalImg"style="
                                    max-width: 100%;
                                    height: auto;
                                    width: auto\9;
                        ">
                                <img id="imgInfo" class="img-fluid" style="">
                            </div> -->
                                

                                <!-- <div class="doc">
                                <div class="box">
                                    <img id="imgInfo" >
                                </div>
                                </div> -->

                            <div class="col m6 s12 l4 modalImg">
                                <div class="doc" style="    
                                    flex-flow: column wrap;
                                    width: 100%;
                                    height: auto;
                                    justify-content: center;
                                    align-items: center;
                                    background: rgb(51, 57, 68) none repeat scroll 0% 0%;
                                    margin: 1em;
                                    ">
                                    <div class="box" style="
                                width: 100%;
                                    height: auto;
                                    overflow: hidden;
                                    ">
                                    <img id="imgInfo" style="
                                    object-fit: cover;
                                    object-position: center center;
                                    height: 290px;
                                    width: 250px;
                                    " >
                                    </div>
                                </div>
                                <!-- <img id="imgInfo" style="margin:5px; width: 100% !important; object-fit: fill !important;"> -->
                            </div>
                            <div class="col m6 s6 l8">
                                <div class="col m12 s12 l12">
                                <div class="modal-header">
                                    <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                                    style="float: right !important;
                                    position: absolute !important;
                                    padding-left: 58% !important;">X</button>
                                </div>
                                    <div class="col m3 s12 l2 modalHead">
                                        <img class="circule-img" style="background: white" id="logoProvider">
                                    </div>
                                    <div class="col m9 s12 l10 modalImg">
                                        <h6 style="font-weight: bold; color: black;" id="nomproInfo"></h6>
                                        <h6 id="titleInfo" style="color: black; font-size: 20px;"></h6>
                                        <h6 id="fechaInfo" style="color: black;"></h6>
                                    </div>
                                </div>
                                <div class="col m12 s12 l12">
                                <!-- <button style="float: right;margin: 1em;">Guardar</button> -->
                                
                                <!-- <button class="" style="float: right;margin: .5em;" type="submit" name="action">Guardar
                                    <i class="material-icons right">send</i>
                                </button> -->
                    
                                    <p align="justify" id="descInfo" style="color: black;"></p>
                                    <!-- <div id="divTags">
                                    </div> -->
                                    <ul class="tags col l12 m12 s12" id="divTags">
                                        <li id="divTags">

                                        </li>
                                    </ul>
                                    <div id="divComent">
                                    </div>
                                    <div class="col m12 s12">
                                    <hr>
                                        <i class="fa fa-eye" aria-hidden="true" style="color: black;"></i><strong style="color: black;" id="vistasInfo"></strong>
                                        <i class="fa fa-commenting" aria-hidden="true" style="color: black;"></i><strong style="color: black;" id="contComent"></strong>
                                    <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("japy/prueba/footer") ?>
        <script type="text/javascript" src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js"></script>
        <?php $this->view("principal/novia/proveedor/anadir") ?>
        <script>
            $('.imagen .delete').on('click', delete_img);
            function delete_img(elem) {
                let iddelete = this.id;
                if(iddelete=='deleteInsp') {
                    swal({
                        title: 'Estas seguro que quieres borrar esta imagen?',
                        icon: 'error',
                        buttons: ['Cancelar', true],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            var e = elem.currentTarget;
                            var id = $(e).data('id');
                            const $imagesContainer = $('.container-imagenes');
                            $.ajax({
                                url: "<?php echo base_url() ?>novios/proveedor/deleteInspiration",
                                method: 'POST',
                                data: {
                                    id: id
                                },
                                success: function(resp) {
                                    console.log("SUCCESS");
                                    // cuentaPublicacion();
                                    var img = $('#' + id);
                                    img.addClass('delete');
                                    setTimeout(function() {
                                        $('#' + id).remove();
                                        if ($imagesContainer.find('.imagen.hidden').length) {
                                            $imagesContainer.find('.imagen.hidden').first().removeClass('hidden');
                                        }
                                    }, 300);
                                },
                                error: function() {
                                    swal('¡Error!', 'Oops ocurrio un error, intentalo de nuevo mas tarde', 'error');
                                },
                            });
                        }
                    });
                } else {
                    swal({
                        title: 'Estas seguro que quieres borrar esta imagen?',
                        icon: 'error',
                        buttons: ['Cancelar', true],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            // document.getElementById("vistaPrevia").innerHTML='';
                            location.reload();
                        }
                    });
                }
            }
            var server = "<?php echo base_url() ?>";
            $(document).ready(function() {
                $('#modal-proveedor').modal();
                $('.mostrar').click(function() {
                    // document.getElementById('divComent').innerHTML='';
                    let id = this.id;
                    $.ajax({
                        url: '<?php echo base_url() ?>'+'novios/proveedor/modalInfo',
                        method: 'post',
                        //dataType: 'json',
                        data: {
                            id_ins: id,
                        },
                        success: function(response) {
                            var ruta = '<?php echo base_url() ?>'+'/uploads/images/'+response.inspiracion.nombre;
                            document.getElementById("imgInfo").src = ruta;
                            var ruta2 = '<?php echo base_url() ?>'+'uploads/images/'+response.fotoInfo;
                            document.getElementById("logoProvider").src = ruta2;
                            document.getElementById("nomproInfo").innerHTML = response.nomproInfo;
                            document.getElementById("titleInfo").innerHTML = response.inspiracion.titulo;
                            let date = new Date(response.inspiracion.created_at.replace(/-+/g, '/'));
                            let options = {
                                year: 'numeric',
                                month: 'long',
                                day: 'numeric'
                            };
                            document.getElementById("fechaInfo").innerHTML = date.toLocaleDateString('es-MX', options);
                            document.getElementById("descInfo").innerHTML = response.inspiracion.descripcion;
                            if(response.inspiracion.vistas!=null) {
                                document.getElementById("vistasInfo").innerHTML = '&nbsp;'+response.inspiracion.vistas+'&nbsp;&nbsp;';
                            } else {
                                document.getElementById("vistasInfo").innerHTML = '&nbsp;'+0+'&nbsp;&nbsp;';
                            }
                            var tamaño = response.comentarios.length;
                            document.getElementById("contComent").innerHTML = '&nbsp;'+response.comentarios.length;
                            var i;
                            var comentario="";
                            var comentario2="";
                            for (i = 0; i < tamaño; i++) {
                                var nombre = '<strong style="font-weight: bold; color: black;">'; 
                                nombre += response.comentarios[i]['names']+': '; 
                                nombre += '</strong>'; 

                                var texto = '<strong style="color: black;">'; 
                                texto += response.comentarios[i]['texto']; 
                                texto += '</strong>'; 
                                
                                var salto = '<br>'
                                comentario = comentario + (nombre+texto)+salto;
                            }
                            var tamaño2 = response.tags.length;
                            var x=1;
                            for (i=0; i < tamaño2; i++) {
                                var tag = '<a style="font-weight: bold; color: white; font-size: 13px;">'; 
                                tag += response.tags[i]['tags']; 
                                tag += '</a>';
                                comentario2 = comentario2 + tag;
                            }
                            document.getElementById("divComent").innerHTML = comentario;
                            document.getElementById("divTags").innerHTML = comentario2;
                        },
                        error: function(e) {
                            console.log("ERROR");
                        },
                    });
                });
            });
        </script>
    </body>
</html>
