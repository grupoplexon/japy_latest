<!DOCTYPE html>
<html>
<head>
    <?php $this->view("principal/head") ?>
    <style>
        .hover-shadow:hover {
            background: #cfcfcf;
        }

        .title-presupuesto {
            border-top-right-radius: 10px;
            padding: 5px;
            width: auto;
            margin-top: -35px;
            cursor: pointer !important;
        }

        .tabla-presupuesto tbody td {
            border-right: 1px solid black;
        }

        input[type=text].formularios {
            background: #f2f2f2;
            margin-top: 17px;
        }

        input[type=text] {
            border: 0px !important;
            border-bottom: 0px !important;
            margin-top: 17px;
        }

        input[type=text]:focus:not([readonly]), textarea.nombre:focus {
            border-bottom: 1px solid #f4d266 !important;
            box-shadow: 0 1px 0 0 #f4d266;
        }

        input:not([type]) {
            border: 0px !important;
        }

        input:not([type]):focus {
            border-bottom: 1px solid #f4d266 !important;
            box-shadow: 0 1px 0 0 #f4d266;
        }

        input:not([type]):focus:not([readonly]), input[type=search].formulario:focus:not([readonly]) {
            border-bottom: 1px solid #f4d266 !important;
            box-shadow: 0 1px 0 0 #f4d266;
        }

        textarea.nombre {
            background-color: transparent;
            border: none;
            border-radius: 0;
            outline: none;
            height: 60px;
            width: 100%;
            font-size: 1rem;
            margin: 0 0 20px 0;
            padding: 0;
            box-shadow: none;
            box-sizing: content-box;
            transition: all 0.3s;
            resize: unset;
        }

        a.formularios, p.formularios {
            padding-bottom: 12px;
            padding-top: 12px;
            padding-left: 5px;
            padding-right: 5px;
            background: #f2f2f2;
        }

        @media screen and(max-width: 599px) {
            .barra-lateral {
                top: 0px;
            }
        }

        .yellow.darken-5 {
            background: #f4d266 !important;
            color: #514f50 !important;
        }

        .yellow-text.darken-5 {
            color: #f4d266 !important;
        }

        .yellow.lighten-5 {
            background: #f4e39b !important;
        }

        .tabs .indicator {
            background: #f4d266 !important;
        }

        .tab a {
            color: #f4d266 !important;
        }

        .pestania {
            background: #575b5f !important;
        }

        .seccion-novia > .row > .m1-1.active {
            border-bottom: 5px solid #1c97b3 !important;
        }

        .paginate_button.current {
            background: #514f50 !important;
            color: white !important;
            background-color: #514f50 !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            background: #514f50 !important;
            color: white !important;
            background-color: #514f50 !important;
            box-shadow: inset 0 0 3px #514f50 !important;
        }

        .sorting_asc {
            background-image: url('<?php echo base_url() ?>dist/img/data_table/sort_asc-dd.png') !important;
        }

        .sorting_desc {
            background-image: url('<?php echo base_url() ?>dist/img/data_table/sort_desc--.png') !important;
        }

        .proveedor {
            background-image: url('<?php echo base_url() ?>dist/img/iconos/clubnupcial_icons_gold.png') !important;
        }

        .proveedor-cat-salon {
            background-position: -850px 0px !important;
        }

        .proveedor-cat-catering {
            background-position: -850px -850px !important;
        }

        .proveedor-cat-fotografia {
            background-position: 5px 5px !important;
        }

        .proveedor-cat-musica {
            background-position: -300px -560px !important;
        }

        .proveedor-cat-autos {
            background-position: -280px -850px !important;
        }

        .proveedor-cat-invitaciones {
            background-position: -1150px 0px !important;
        }

        .proveedor-cat-recuerdos {
            background-position: -850px -280px !important;
        }

        .proveedor-cat-arreglos {
            background-position: -1150px -280px !important;
        }

        .proveedor-cat-inolvidable {
            width: 250px;
            height: 250px;
            background-position: -1150px -550px !important;
        }

        .proveedor-cat-animacion {
            background-position: -280px -250px !important;
        }

        .proveedor-cat-planner {
            background-position: -550px -850px !important;
        }

        .proveedor-cat-pasteles {
            background-position: -600px -550px !important;
        }

        .proveedor-cat-novia-complementos {
            background-position: 5px -860px !important;
        }

        .proveedor-cat-novio {
            background-position: 5px -555px !important;
        }

        .proveedor-cat-salud-belleza {
            background-position: -850px -550px !important;
        }

        .proveedor-cat-joyeria {
            background-position: -550px 5px !important;
        }

        .proveedor-cat-recepcion {
            width: 250px;
            height: 250px;
            background-position: -550px -280px !important;
        }

        .proveedor-cat-ceremonia {
            width: 250px;
            height: 250px;
            background-position: 10px -270px !important;
        }

        .presupuesto2 {
            background-image: url('<?php echo base_url() ?>dist/img/iconos/iconos-presupuesto.png') !important;
        }

        .presupuesto-pendiente {
            background-position: -235px -265px !important;
        }

        .presupuesto-costo-final {
            background-position: -265px -5px !important;
        }

        .presupuesto-pagado {
            background-position: -0px -235px !important;
        }

        .presupuesto-costo-aproximado {
            width: 250px;
            height: 250px;
            background-position: -5px -5px !important;
        }

        .presupuesto2.x2 {
            -moz-transform: scale(0.16);
            margin-top: -100px;
            margin-left: -100px;
            margin-right: -100px;
            margin-bottom: -100px;
            -webkit-transform: scale(0.16);
        }

        td, th {
            padding: 5px 5px;
        }

        .btn-favoritos {
            font-size: 12px;
            padding: 0px 16px;
        }
    </style>
    <link href="<?php echo base_url() ?>dist/css/iconos-proveedor.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php $this->view("principal/menu") ?>
<?php $this->view("principal/novia/menu") ?>
<div class="body-container">
    <div class="row">
        <h5 class="col"><?php echo $title ?></h5>
    </div>
    <div class="row">
        <div class="col s12">
            <div class="card-panel grey lighten-3">
                        <span class="black-text">
                            <i class="material-icons">info</i> Te sugerimos algunos proveedores que se ajustan a tu presupuesto.
                            Tambi&eacute;n puedes usar nuestras herramientas de busquedas para encontrar al mejor proveedor para ti.
                        </span>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <br>


    <?php if ($categorias) { ?>
        <div class="row">


            <?php foreach ($categorias as $key => $cat) { ?>
                <?php if ($cat->id_default_porcentaje == "43") {
                    continue;
                }//omitir ceremonia civil ?>
                <div class="categoria <?php $cat->nombre ?>">
                    <h5>
                        <i class="proveedor x1 left <?php echo icon_categoria_presupuesto($cat->nombre) ?>"></i>
                        <?php echo ucfirst(strtolower($cat->nombre)) ?> <?php echo moneyFormat($cat->presupuesto) ?>
                    </h5>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="row">

                            <?php if ($cat->proveedores) { ?>
                                <?php foreach ($cat->proveedores as $p) { ?>
                                    <div class="col s3" style="width: 200px;height: 175px; overflow: hidden;">
                                        <div class="card hover-shadow">

                                            <div class="card-image waves-effect waves-light valign-wrapper"
                                                 style="    height: 100px;display: flex !important;">
                                                <a class="valign"
                                                   href="<?php echo site_url("boda-$p->slug") ?>">
                                                    <?php if (isset($p->principal)) { ?>
                                                        <img class="valign"
                                                             src="<?php echo base_url() ?>uploads/images/<?php echo $p->principal->nombre ?>">
                                                    <?php } else { ?>
                                                        <img class="valign"
                                                             src="<?php echo base_url() ?>dist/img/slider1.png">
                                                    <?php } ?>
                                                </a>
                                                <i data-proveedor="<?php echo $p->id_proveedor ?>"
                                                   class="fa fa-heart-o proveedor-favorite   clickable  tooltipped <?php echo $p->favorito ? 'active' : '' ?> "
                                                   data-position="bottom" data-delay="5" data-tooltip="Guardar"></i>
                                            </div>
                                            <div class="card-content">
                                                <?php echo $p->nombre ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>

                        </div>
                        <?php if (count($cat->proveedores) >= 1) { ?>
                            <div class="row">
                                <a href="<?php echo base_url() ?>novios/proveedor/sugeridos/<?php echo $cat->nombre ?>"
                                   class="btn-flat white waves-effect dorado-2-text right">Ver m&aacute;s
                                    <i class="material-icons right">keyboard_arrow_right</i>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col s6">

                    </div>

                </div>
                <div class="divider"></div>

            <?php } ?>


        </div>
    <?php } ?>
</div>


<?php $this->view("principal/view_footer") ?>
<?php $this->view("principal/foot") ?>
<script type="text/javascript" src="<?php echo base_url() ?>dist/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js"></script>
<?php $this->view("principal/novia/proveedor/anadir") ?>
<script>
    var server = "<?php echo base_url() ?>";
    $(document).ready(function () {
        $('.tooltipped').tooltip();
        var server = "<?php echo base_url() ?>";
        $(".proveedor-favorite").on("click", function () {
            var $self = $(this);
            $.ajax({
                url: server + "escaparate/guardarlo/" + $self.data("proveedor"),
                method: "POST",
                data: {
                    proveedor: $self.data("proveedor")
                },
                success: function (resp) {
                    if (resp.data.favorito) {
                        $self.addClass("active");
                    } else {
                        $self.removeClass("active");
                    }
                },
                error: function () {
                    if (!$("#toast-container .toast").get(0)) {
                        Materialize.toast("Debes estar registrado para guardar un proveedor<br> "
                            + " <a href='<?php echo site_url("cuenta") ?>' class='dorado-2-text pull-right'>Inicia Sesi&oacute;n <i class='material-icons right'>keyboard_arrow_right</i></a>", 5000);
                        $(".toast").css({display: "block"});
                    }
                }
            });
        });
    });
</script>

</body>
</html>