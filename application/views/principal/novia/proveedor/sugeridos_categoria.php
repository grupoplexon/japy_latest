<!DOCTYPE html>
<html>
    <head>
        <?php $this->view("principal/head") ?>
        <link href="<?php echo base_url() ?>dist/css/iconos-proveedor.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/novios_proveedores.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php $this->view("principal/menu") ?>
        <?php $this->view("principal/novia/menu") ?>
        <div class="body-container">
            <div class="row">
                <h5 class="col"><?php echo $title ?></h5>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="card-panel grey lighten-3">
                        <span class="black-text">
                            <i class="material-icons">info</i> Te sugerimos algunos proveedores que se ajustan a tu presupuesto.
                            Tambi&eacute;n puedes usar nuestras herramientas de busquedas para encontrar al mejor proveedor para ti.
                        </span>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <br>

            <?php if ($categoria) { ?>
                <div class="row">
                    <div class="categoria <?php $categoria->nombre ?>">
                        <h5>
                            <i class="proveedor x1 left <?php echo icon_categoria($categoria->nombre) ?>" ></i> 
                            <?php echo $categoria->nombre ?> presupuesto de :<?php echo moneyFormat($categoria->presupuesto) ?>
                        </h5>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <div class="row">
                                <?php if ($categoria->proveedores) { ?>
                                    <?php foreach ($categoria->proveedores as $p) { ?>
                                        <div class="card">
                                            <div class="card-content" style="padding: 10px;">
                                                <div class="row" style="margin: 0px;">
                                                    <div class="col s12 m4">
                                                        <?php if (isset($p->principal)) { ?>
                                                            <img class=" thumb responsive-img" src="<?php echo base_url() ?>uploads/images/<?php echo $p->principal->nombre ?>">
                                                        <?php } else { ?>
                                                            <img class=" thumb responsive-img" src="<?php echo base_url() ?>/dist/img/slider1.png">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col s12 m8">
                                                        <i data-proveedor="<?php echo $p->id_proveedor ?>" class="fa fa-heart-o proveedor-favorite <?php echo $p->favorito ? "active" : "" ?>  clickable  tooltipped"  data-position="left" data-delay="5" data-tooltip="Guardar"></i>
                                                        <?php if ($p->descuento) { ?>
                                                            <div class="desc" style="">
                                                                - <?php echo $p->descuento ?>%
                                                            </div>
                                                        <?php } ?>
                                                        <h5 >
                                                            <a class="gris-2-text" href="<?php echo base_url() ?>boda-<?php echo $p->slug ?>">
                                                                <?php echo $p->nombre ?>
                                                            </a>
                                                        </h5>
                                                        <small class="grey-text lighten-3 "><?php echo $p->nombre_tipo_proveedor ?>&nbsp;<i class="fa fa-map-marker"></i>&nbsp;<?php echo $p->localizacion_estado ?></small>
                                                        <div class="divider"></div>
                                                        <p>
                                                            <?php echo strip_tags(substr($p->descripcion, 0, 350)) ?>...
                                                        </p>
                                                        <div class="row ">
                                                            <?php if (isset($p->promociones)) { ?>
                                                                <div class="right promo" >
                                                                    <a href="<?php echo base_url() ?>escaparate/promociones/<?php echo $p->id_proveedor ?>" class="white-text"><i  class="fa fa-tag "></i> <?php echo $p->promociones ?>  promocion<?php echo $p->promociones > 1 ? "es" : "" ?></a>
                                                                </div>
                                                            <?php } ?>
                                                            <div class="grey-text darken-5 right" style="display: inline;">
                                                                <?php if (isset($p->imagenes)) { ?>
                                                                    <i class="fa fa-camera-retro "></i> <?php echo $p->imagenes ?> 
                                                                <?php } ?>
                                                                <?php if (isset($p->videos)) { ?>
                                                                    <i class="fa fa-video-camera "></i> <?php echo $p->videos ?> 
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="divider"></div>
                                                        <div class="col m3">
                                                            <?php if (isset($p->precio)) { ?>
                                                                Precio desde: <br>
                                                                <i class="fa fa-money"></i> $ <?php echo $p->precio[0] ?>  <?php echo count($p->precio) > 1 ? "hasta $" . $p->precio[1] : "" ?>
                                                            <?php } else { ?>
                                                                &nbsp;<br> &nbsp;
                                                            <?php } ?>
                                                        </div>
                                                        <div class="col m3">
                                                            <?php if (isset($p->capacidad)) { ?>
                                                                Capacidad: <br>
                                                                <i class="fa fa-users"></i>  <?php echo $p->capacidad[0] ?>  <?php echo count($p->capacidad) > 1 ? " a " . $p->capacidad[1] : "" ?>
                                                            <?php } else { ?>
                                                                &nbsp;<br> &nbsp;
                                                            <?php } ?>
                                                        </div>
                                                        <div class="col m6">
                                                            <p class="pull-right">
                                                                <a href="<?php echo base_url() ?>boda-<?php echo $p->slug ?>" class="btn-flat dorado-2-text " style="font-size: 0.8em">
                                                                    Quiero m&aacute;s informacion
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <?php if (count($categoria->proveedores) >= 8) { ?>
                                <ul class="pagination">
                                    <li class="waves-effect <?php echo $page == 1 ? "disabled" : "" ?>">
                                        <a <?php echo $page == 1 ? "" : "href='" . $_SERVER["PHP_SELF"] . "?pag=" . ($page - 1) . "'" ?> >
                                            <i class="material-icons">chevron_left</i>
                                        </a>
                                    </li>
                                    <?php $total_paginas = ($total / 8) + 1 ?>
                                    <?php for ($i = 1; $i < $total_paginas; $i++) { ?>
                                        <li class="waves-effect <?php echo $page == $i ? "active" : "" ?>">
                                            <a href="<?php echo $_SERVER["PHP_SELF"] . "?pag=$i" ?>"><?php echo $i ?></a>
                                        </li>
                                    <?php } ?>
                                    <li class="waves-effect <?php echo ($page == $i - 1 ? "disabled" : "") ?>">
                                        <a  <?php echo ($page == $i - 1 ? "" : "href='" . $_SERVER["PHP_SELF"] . "?pag=" . ($page + 1)) . "'" ?> >
                                            <i class="material-icons">chevron_right</i>
                                        </a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                        <div class="col s6">

                        </div>

                    </div>
                    <div class="divider"></div>
                </div>
            </div>
        <?php } ?>
        <?php $this->view("principal/view_footer") ?>
        <?php $this->view("principal/foot") ?>
        <script type="text/javascript" src="<?php echo base_url() ?>dist/js/jquery.touchSwipe.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>dist/jquery ui/jquery-ui.js"></script>
        <?php $this->view("principal/novia/proveedor/anadir") ?>
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function () {
                $('.tooltipped').tooltip();
                var server = "<?php echo base_url() ?>";
                $(".proveedor-favorite").on("click", function () {
                    var $self = $(this);
                    $.ajax({
                        url: server + "escaparate/guardarlo/" + $self.data("proveedor"),
                        method: "POST",
                        data: {
                            proveedor: $self.data("proveedor")
                        },
                        success: function (resp) {
                            setTimeout(function () {
                                if (resp.data.favorito) {
                                    $self.addClass("active");
                                } else {
                                    $self.removeClass("active");
                                }
                            }, 100);
                        },
                        error: function () {
                            if (!$("#toast-container .toast").get(0)) {
                                Materialize.toast("Debes estar registrado para guardar un proveedor<br> "
                                        + " <a href='<?php echo site_url("cuenta") ?>' class='dorado-2-text pull-right'>Inicia Sesi&oacute;n <i class='material-icons right'>keyboard_arrow_right</i></a>", 5000);
                                $(".toast").css({display: "block"});
                            }
                        }
                    });
                });
            });
        </script>

    </body>
</html>
