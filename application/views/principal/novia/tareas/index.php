<link href="<?php echo base_url() ?>dist/css/novios/Tareas.css" rel="stylesheet" type="text/css"/>
<style>
    a {
        color: black !important;
    }
    .date-indicator{
        margin-top: 10px
    } 
    @media only screen and  (min-width: 300px) and  (max-width: 600px){
         .date-indicator{
            margin-top: 80px
        } 
    }
</style>
<div class="body-container">
    <div class="row">
        <div class="col s12 m12 l3 " style="float: right">
            <div class="card-panel z-depth-0">
                <?php if (count($tareas)) : ?>
                    <div>
                        <div class="progress general primary-background">
                            <div class="determinate"
                                 style="width: <?php echo(($total_completadas * 100) / count($tareas)) ?>%;background: #1c97b3;">
                            </div>
                        </div>
                    </div>
                    <h6 class="center-align">Has completado <?php echo $total_completadas ?> de <?php echo count($tareas) ?> tareas</h6>
                <?php endif ?>
                <div class="divider">
                </div>
                <?php if ($fecha_boda !== $fecha_creacion): ?>
                    <div class="faltante row center-align">
                        <?php
                        $now      = new DateTime("now");
                        $fboda    = new DateTime($fecha_boda);
                        $interval = $now->diff($fboda);
                        ?>
                        <div class="col l12 m3 s12 center-align grey lighten-3">
                            <h5 class="">
                                <b class="dias">
                                    <?php echo $interval->format('%a'); ?>
                                </b>
                            </h5>
                            <small>D&iacute;as</small>
                            <div style="margin-top:20px;"></div>
                        </div>
                        <div class="col m3 l4 s4 center-align grey lighten-3">
                            <h5 class="">
                                <b class="horas">
                                    <?php echo $interval->format('%h'); ?>
                                </b>
                            </h5>
                            <small>Horas</small>
                            <div style="margin-top:20px;"></div>
                        </div>
                        <div class="col  m3 l4 s4 center-align grey lighten-3">
                            <h5 class="">
                                <b class="minutos">
                                    <?php echo $interval->format('%i'); ?>
                                </b>
                            </h5>
                            <small>Min.</small>
                            <div style="margin-top:20px;"></div>
                        </div>
                        <div class="col m3 l4 s4 center-align grey lighten-3">
                            <h5 class="">
                                <b class="segundos">
                                    <?php echo $interval->format('%s'); ?>
                                </b>
                            </h5>
                            <small>Seg.</small>
                            <div style="margin-top:20px;"></div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col s12 m12 l9 pull-left">
            <div class="row">
                <div class="col s12 m4">
                    <h4>
                        Agenda de Tareas
                    </h4>
                </div>
                <div class="col s12 m8">
                    <div class="card-panel" style="background: #FFFFCC!important">
                        Te vamos a guiar para que no se te escape ning&uacute;n detalle, as&iacute; que agenda cada
                        fecha, los pagos, los viajes y todas las actividades importantes. Nosotros tenemos un check list
                        para ti, adem&aacute;s puedes agregar todos tus pendientes, para que no olvides nada.
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div class="row">
                <div id="test1" class="col s12">
                    <div class="row">
                        <div class="input-field col s12 m4">
                            <a class="btn dorado-1 btn-block-small " id="btn-nueva-tarea">
                                Agregar Tarea
                            </a>
                        </div>
                        <div class="input-field col m4 s6">
                            <select id="select-mostrar-estado" class="select-ordenar">
                                <option value="-1"
                                    <?php echo $estado !== 0 && $estado != 1 ? "selected" : "" ?> >Todas las tareas
                                </option>
                                <option value="1" <?php echo $estado == 1 ? "selected" : "" ?> >Tareas completadas
                                </option>
                                <option value="0" <?php echo $estado === 0 ? "selected" : "" ?> >Tareas pendientes
                                </option>
                            </select>
                        </div>
                        <div class="input-field col m4 s6">
                            <select id="select-mostrar-categoria" class="select-ordenar">
                                <option value="0" selected>Todas las Categor&iacute;as</option>
                                <?php foreach (Categoria::getCategorias() as $key => $c) { ?>
                                    <option value="<?php echo $c->id ?>" <?php echo($categoria == $c->id ? "selected" : "") ?> ><?php echo $c->nombre ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: -20px">
        <div class="col s12 m6 l7">
            <?php if ($tareas) { ?>
                <ul class="collapsible tareas z-depth-1"
                    data-collapsible="accordion"
                    data-order="<?php echo $grupo ?>"
                >
                    <?php
                    $tiempo = 0;
                    foreach ($tareas as $t) {
                        if ($tiempo != $t->tiempo) {
                            $tiempo = $t->tiempo;
                    ?>
                            <li>
                                <div class="collapsible-header collection-header" id="tiempo-<?php echo $tiempo ?>">
                                    <div class="row" style="width:100%; padding-top: 9px;">
                                        <div class="col m7 s12">
                                            <?php if ($grupo == "tiempo") { ?>
                                                <b class="black-text"><?php echo $this->task->timepoRelativo($t->tiempo) ?></b>
                                                <small class="black-text small-text"><?php echo $this->task->timepoToString($t->tiempo) ?></small>
                                            <?php } else { ?>
                                                <b class="black-text"><?php echo(isset($t->categoria) ? $t->categoria_titulo : "Por definir") ?></b>
                                            <?php } ?>
                                        </div>
                                        <div class="col m4 s12">
                                            <div class="progress primary-background" style="margin-bottom: 0px;">
                                                <div class="determinate" style="width: 0%;background: #1c97b3;"></div>
                                            </div>
                                            <div class="text-pregress center-align black-text" style="font-size: 12px">
                                                0 de 0 completadas
                                            </div>
                                        </div>
                                        <div class="col m1 s12">
                                            <i class="fa fa-caret-down"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="collapsible-body">
                                    <ul class="collapsible">
                                        <?php
                                        foreach ($tareas as $t) {
                                            if ($tiempo == $t->tiempo) {
                                        ?>
                                                <li class="collection-item  tarea" id="<?php echo $t->id_tarea ?>"
                                                    data-id="<?php echo $t->id_tarea ?>"
                                                    data-estado="<?php echo $t->completada ?>"
                                                    data-categoria="<?php echo $t->categoria ?>"
                                                    data-tiempo="<?php echo $t->tiempo ?>">
                                                    <div class="collapsible-header waves-effect waves-club" style="display: block">
                                                        <div class="row valign-wrapper">
                                                            <div class="col s2 l1 center-align valign-wrapper">
                                                                <i class="estado fa <?php echo($t->completada == 0 ? "fa-square-o" : "fa-check-square-o green-text") ?> fa-2x"
                                                                   style=""></i>
                                                            </div>
                                                            <div class="col s8 l8 valign-wrapper">
                                                                <b class="tarea-nombre black-text"><?php echo $t->nombre ?></b>
                                                            </div>
                                                            <div class="col s1 l1 right-align valign-wrapper">
                                                                <i class="fa fa-pencil clickable tooltipped"
                                                                   style="font-size: 1.3em" data-position="top" data-delay="50"
                                                                   data-tooltip="Editar esta tarea"></i>
                                                            </div>
                                                            <div class="col s1 l1 right-align valign-wrapper">
                                                                <i class="fa fa-calendar showDatePicker clickable tooltipped"
                                                                   style="font-size: 1.3em" data-position="top" data-delay="50"
                                                                   data-tooltip="Asignar fecha"></i>
                                                            </div>
                                                            <div class="col s1 l1 right-align valign-wrapper">
                                                                <i class="fa fa-trash clickable eliminar tooltipped"
                                                                   style="font-size: 1.3em" data-position="top" data-delay="50"
                                                                   data-tooltip="Eliminar esta tarea"></i>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col l3 offset-l9">
                                                                <input
                                                                    disabled
                                                                    type="text"
                                                                    value="<?php echo $t->fecha_asignada != '' ? Carbon\Carbon::createFromFormat('Y-m-d', $t->fecha_asignada)->format('d/m/Y') : '' ?>"
                                                                    class="center-align datepicker"
                                                                    data-task-id="<?php echo $t->id_tarea ?>"
                                                                    data-task-title="<?php echo $t->nombre ?>"
                                                                >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="collapsible-body">
                                                        <div>
                                                            <div style="padding-top: 15px; font-size: small; font-weight: 500; color: black">
                                                                <?php echo $t->descripcion ?>
                                                            </div>
                                                            <div class="section">
                                                                <div class="row valign-wrapper">
                                                                    <div class="col s12 m8">
                                                                        <div class="additional-note-header">Agregar una nota</div>
                                                                        <div class="card-panel clickable nota">
                                                                            <pre
                                                                                class="black-text"
                                                                                style="overflow: hidden"
                                                                                data-nuevo="<?php echo $t->nota ? 1 : 0 ?>"><?php echo $t->nota ? $t->nota : "Agregar nota" ?>
                                                                            </pre>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col s12 m4 suggested-providers-col">
                                                                        <ul class="collection with-header">
                                                                            <li class="collection-header gray lighten-3 row" style="padding: 0px 0px;">
                                                                                <div class="col s12 center-align suggested-provider" style="background-color:white; border-style: groove;">
                                                                                    <a class="waves-effect btn-cambiar"
                                                                                       style="width: 100%; padding: 0 5px;font-size: 1rem;background-color: white; color: #161717 !important"
                                                                                       data-category="<?php echo $t->categoria ?>">
                                                                                        Proveedores sugeridos
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                            <li class="collection-item" style="padding: 0px;">
                                                                                <div class="card proveedor z-depth-0"
                                                                                     style="margin: 0px;">
                                                                                    <?php $proveedor = isset($t->proveedor) ? $t->proveedor : "" ?>
                                                                                    <div class="card-image <?php echo ($proveedor == null) ? "hide" : "" ?>">
                                                                                        <?php if ($proveedor) : ?>
                                                                                            <a href="<?php echo base_url()."boda-".$proveedor->slug ?>" data-provider="<?php echo $proveedor->id_proveedor ?>">
                                                                                                <?php if ($proveedor->principal) : ?>
                                                                                                    <img src="<?php echo base_url() ?>/uploads/images/<?php echo ($proveedor != null && $proveedor->principal) ? $proveedor->principal : "" ?>"
                                                                                                         style="height: 120px;object-fit:cover;">
                                                                                                <?php else : ?>
                                                                                                    <img src="<?php echo base_url() ?>dist/img/slider1.png" style="height: 120px;object-fit:cover;">
                                                                                                <?php endif; ?>
                                                                                                <span class="card-title"
                                                                                                      style="top:0;bottom:0;left:0;right:0;height:100px;margin:auto;padding: 0;
                                                                          display: flex;justify-content:center;align-items: center;text-align: center;">
                                                                            <?php echo $proveedor->nombre ?>
                                                                        </span>
                                                                                            </a>
                                                                                        <?php endif; ?>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>
                    <?php
                        }
                    }
                    ?>
                </ul>
            <?php } else { ?>
                <div class=" card-panel center-align fa-3x">
                    <div class="col s12 ">
                        <i class="fa fa-info fa-4x"></i>
                    </div>
                    No se encontraron tareas registradas
                </div>
            <?php } ?>
        </div>
        <div class="col s12 m6 l5 center-align">
            <div class="row">
            <div class="col s12 mini-clndr-col">
                <div id="mini-clndr"></div>
            </div>
            <div class="col s12 mini-clndr-col-detail">
                <div class="date-indicator">
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<div id="modal-cambiar" class="modal">
    <div class="modal-content">
        <h4>Proveedor para esta tarea</h4>
        <p>
            Elige un proveedor:
        </p>
        <p>
            <select class="form-control browser-default">
                <option value="" autocomplete="off" selected disabled hidden>Elige una opcion</option>
                <?php foreach ($weddingProviders as $key => $provider) : ?>
                    <option data-nombre="<?php echo $provider->nombre ?>" value="<?php echo $provider->id_proveedor ?>">
                        <?php echo $provider->nombre ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </p>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat aceptar ">Aceptar</a>
        <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Cancelar</a>
    </div>
</div>
<div id="modal-nota" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5>Nota </h5>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">mode_edit</i>
                <textarea class="text materialize-textarea" style="min-height: 230px" length="320"></textarea>
                <label for="icon_prefix2">Nota</label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action cerrar waves-effect waves-red btn-flat">Cerrar</a>
        <a href="#!" class=" modal-action aceptar waves-effect waves-green btn-flat">Aceptar</a>
    </div>
</div>
<div id="modal-presupuesto" class="modal">
    <div class="modal-content">
        <h5>Seleccionar presupuesto </h5>
        <div class="row select">
            <p>
                Selecciona un concepto de gasto para esta tarea.
            </p>
            <div class="col m6">
                <select class="browser-default select-presupuesto">
                    <option value="" disabled selected>Elige una opcion</option>
                </select>
            </div>
            <div class="col m6">
                <a class="btn agregar  btn-block">Agregar nuevo concepto</a>
            </div>
        </div>
        <div class="row nuevo hide">
            <p>
                <label for="category">Categoria:</label>
                <select class="browser-default select-categoria form-control">
                    <option value="" disabled selected>Elige una opcion</option>
                    <?php foreach (Categoria::getCategorias() as $key => $category) { ?>
                        <option value="<?php echo $category->id ?>"><?php echo $category->nombre ?></option>
                    <?php } ?>
                </select>
            </p>
            <p>
                <label for="last_name">Nombre:</label>
                <input class="form-control nombre">
            </p>
            <p>
                <label for="last_name">Costo Aproximado:</label>
            <div class="input-group">
                <span>
                    $
                </span>
                <input class="form-control aprox">
            </div>
            </p>
            <p>
                <label for="last_name">Costo final:</label>
            <div class="input-group">
                <span>
                    $
                </span>
                <input class="form-control final">
            </div>
            </p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action  cerrar modal-close waves-effect waves-red btn-flat">Cerrar</a>
        <a href="#!" class=" modal-action  aceptar waves-effect waves-green btn-flat">Aceptar</a>
    </div>
</div>

<div id="modal-nueva-tarea" class="modal">
    <div class="modal-content">
        <h5>Nueva Tarea </h5>
        <div class="row">
            <div class="row">
                <div class=" col s12 l12">
                    <label for="email">Tarea</label>
                    <input type="text" class=" form-control validate nombre">
                </div>
                <div class=" col s12 l12">
                    <label>Fecha</label>
                    <select class="tiempo" style="display: unset !important;">
                        <?php
                        for ($i = 1; $i <= 9; $i++) {
                            ?>
                            <option value="<?php echo $i ?>"><?php echo $this->task->timepoToString($i) ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col l12 12 m12"><br></div>
                <div class=" col s12 l12">
                    <label>Categoria</label>
                    <select class="categoria" style="display: unset !important;">
                        <?php foreach (Categoria::getCategorias() as $key => $c) { ?>
                            <option value="<?php echo $c->id ?>"><?php echo $c->nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col l12 12 m12"><br></div>
                <div class=" col s12">
                    <label for="textarea1">Descripci&oacute;n</label>
                    <textarea id="textarea1" class="materialize-textarea descripcion form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="" class=" modal-action cerrar  modal-close waves-effect waves-red btn-flat">Cerrar</a>
            <a href="" class=" modal-action aceptar waves-effect waves-green btn-flat">Aceptar</a>
        </div>
    </div>
</div>

<input type="hidden" id="isInTasks">
<style>
    #modal-proveedor .modal-content {
        padding-bottom: 0 !important;
    }

    #modal-proveedor .card-image img {
        height: 200px !important;
    }

    #modal-proveedor .card-image {
        text-align: center;
    }

    #modal-proveedor .card-image span {
        width: 100% !important;
    }

    #modal-proveedor .card-content {
        margin: 5px 0;
        overflow: hidden;
        padding: 0;
        text-align: center;
    }

    #modal-proveedor .card-content p {
        height: 120px !important;
    }

    #modal-proveedor .card-content p p:first-child {
        height: auto !important;
        text-overflow: initial !important;
        white-space: normal;
    }

    #modal-proveedor .card-action {
        padding: 0 !important;
    }

    #modal-proveedor .card-action a {
        width: 100% !important;
    }

    #modal-proveedor ul.pagination {
        margin: 0px !important;
    }

    #modal-proveedor .modal-footer {
        height: initial !important;
    }

    #modal-proveedor .btn {
        background: white !important;
        color: #00bbcd !important;
    }
</style>
<?php
$this->view("proveedor/modal_proveedores");

function getProveedor($categoria, $categorias)
{
    foreach ($categorias as $c) {
        if ($c->id == $categoria) {
            if ($c->total) {
                $c->proveedor->total = $c->total;
            }

            return $c->proveedor;
        }
    }

    return null;
}

?>

<!-- Calendar's template -->
<script id="calendar-template" type="text/template">
    <div class="controls valign-wrapper">
        <div class="clndr-previous-button">
            <span class="material-icons" style="font-size: 50px; color: #F8DADF;">chevron_left</span>
        </div>
        <div class="month">
            <h4 style="color: black; font-weight: bolder;">
                <%= month %>
            </h4>
        </div>
        <div class="clndr-next-button">
            <span class="material-icons" style="font-size: 50px; color: #F8DADF;">chevron_right</span>
        </div>
    </div>

    <div class="days-container">
        <div class="days">
            <div class="headers">
                <% _.each(daysOfTheWeek, function(day) { %><div class="day-header"><%= day %></div><% }); %>
            </div>
            <% _.each(days, function(day) { %><div class="<%= day.classes %>" style="font-weight: bolder; border-color: #eae6e6;" id="<%= day.id %>"><%= day.day %></div><% }); %>
        </div>
    </div>
</script>
<!-- -->

<!-- Modal Pending Tasks -->
<!-- <div id="modal_pending_tasks" class="modal">
    <div class="modal-content">
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Aceptar</a>
    </div>
</div> -->
<!-- -->

<script src="<?php echo base_url() ?>dist/js/modal_proveedores.js"></script>
<script src="<?php echo base_url() ?>dist/js/novios/Tareas.js" type="text/javascript"></script>
<script>
    var server = "<?php echo base_url() ?>";
    $(document).ready(function() {
        initTareas();
        getCalendarEvents();
    });
</script>
