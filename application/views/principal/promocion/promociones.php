<?php
$this->load->view("general/head");
$categoria = isset($_GET["categoria"]) ? htmlspecialchars($_GET["categoria"]) : "";
$state  = isset($_GET["state"]) ? htmlspecialchars($_GET["state"]) : "";
?>
<html>
<head>
        <?php 
        $this->view("japy/prueba/header"); ?>
        <link href="<?php echo base_url() ?>dist/css/lightbox.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/unite-gallery.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/ug-theme-default.css" type="text/css"/>
        
    <style>
        .titulo {
            font-size: 4.2rem;
            text-align: center;
            line-height: 110%;
            font-weight: 500;
            color: #00bcdd !important;
            margin: 2.1rem 0 1.68rem 0;
        }

        #gallery, #gallery2 {
            width: 100% !important;
        }
        
        #promo, #promo4, #promo2 {
            height: 300px;
            position: relative;
        }
        
        #promo3 {
            height: 150px;
            position: relative;
        }

        .desc2{
            color: white;
            font-weight: 500;
            font-size: 1.1rem;
        }

        #imagen {
            max-width: 100% !important;
            max-height: 100% !important;
            height: 100%;
            width: 100%;
            object-fit: cover;
            margin-bottom: -100%;
        }

        #promo2 #imagen{
            margin-bottom: -35%;
        }

        #div-imagen{
            
            position: absolute;
            z-index: 2;
        }

        #promo2 #element-name {
            width: 100%;
            height: 55%;
            background-color: #3f4a4f;
            padding: .9rem;
            font-weight: 400;
            font-size: 1.1rem;
            position: absolute;
            z-index: 1;
            background-color: rgba(47, 55, 59, 0.7);
        }

        #promo3 #element-name {
            width: 70%;
            height: 100%;
            background-color: #3f4a4f;
            padding: .5rem;
            font-weight: 300;
            font-size: 1.1rem;
            position: absolute;
            z-index: 1;
            background-color: rgba(47, 55, 59, 0.7);
        }

        #promo3 #element-name p {
            font-size:15px;
        }

        #promo3 #element-name .nom {
            color: white;
            font-weight: 500;
            font-size: 1.2rem;
        }

        #promo #element-name, #promo4 #element-name {
            width: 50%;
            height: 100%;
            background-color: #3f4a4f;
            padding: .9rem;
            font-weight: 500;
            font-size: 1.1rem;
            padding-top: 12%;
            position: absolute;
            z-index: 1;
            background-color: rgba(47, 55, 59, 0.7);
        }

        #promo4 #element-name{
            left: 50%;
            text-align: right;
        }

        #element-name p {
            color: white;
        }

        #element-name .nom {
            /* color: #00bcdd !important; */
            color: white;
            font-weight: 500;
            font-size: 1.5rem;
        }

        .row .col.prov {
            width: 80%;
            margin-left: auto;
            left: auto;
            right: auto;
        }

        .row .col.menu {
            width: 20%;
            margin-left: auto;
            left: auto;
            right: auto;
        }

        .vertical-menu a {
            background-color: #FFFFFF;
            color: black;
            display: block;
            padding: 20px;
            text-decoration: none;
        }
        .vertical-menu  {
        }

        .menu-movil, #buscar{
                display: none;
            }

        @media only screen and (max-width: 360px) {
            .row .col.prov {
                width: 100%;
                margin-left: auto;
                left: auto;
                right: auto;
            }

            .row .col.menu {
                width: 100%;
                margin-left: auto;
                left: auto;
                right: auto;
            }

            .titulo {
                font-size: 3rem;
                font-weight: 700;

                
            }
            .vertical-menu  {
                margin-top: 1rem;
            }

            #promo, #promo4 {
                height: 400px;
                position: relative;
            
            }

            .txtDesc{
                display: none;
            }

            #promo #element-name, #promo4 #element-name {
                width: 100%;
                height: 45%;
                left: 0%;
                text-align: left;
                padding-top: 5%;
                background-color: rgba(47, 55, 59, 0.9);
            }
            
            #promo #imagen, #promo4 #imagen{
                margin-bottom: -53%;
            }

            #promo2 #element-name {
                height: 50%;
                text-align: right;
            }
            #promo2 #imagen{
                margin-bottom: -47%;
            }

            #promo3 #element-name a{
                font-size: 13px;
            }

            #promo2 p{
                margin-bottom:5px;
            }

            .vertical-menu{
                display:none;
            }

            .menu-movil, #buscar{
                display: block;
            }

        }
        
    input.select-dropdown {
        background-color: white !important;
        border: 1px solid #ccc !important;
        border-radius: 6px !important;
        padding-left: 20px !important;
        color: #ccc;
        margin-bottom: 5rem;
    }

    span.caret {
        color: #00bbcd !important;
        right: 19px !important;
        top: 5px !important;
        font-size: 15px !important;
        z-index: 5;
    }
    </style>

    <script>
    $(document).ready(function() {
        $('ul.left > li, ul.right > li').on('click', function(e) {
            e.preventDefault();
            if ($(this).attr('data-href')) {
                window.location.href = $(this).attr('data-href');
            }
            else {
                const url = $(this).find('a').attr('href');
                window.location.href = url;
            }
        });
    });
</script>
</head>
<body>
<div class="body-container">
    
    
    <div class="row" >
    <h1 class="titulo"> PROMOCIONES </h1>
    
        <div class="col menu s12">
            <form class="row" method="GET"
                action="<?php echo base_url().(isset($category) ? "promociones-".$category->slug : "promociones") ?>"
                style="padding: 10px 0;margin: 0">
                <div class="" style="margin-top: 1rem;">
                    <select name="estado" id="stateId" style="width: 100%; margin-top:10px; margin-bottom: 0px;">
                    </select>
                </div>
                
                <div class="menu-movil" style="margin-top: 1rem;">
                    <select name="categoria" id="categoriaID" style="width: 100%; margin-top:10px; margin-bottom: 0px;">
                        <?php if($categoria == "promociones") { ?>
                            <?php echo $categoria ?>
                            <option value="promociones" selected>Todas las categorias </option>
                        <?php }else{ ?>
                            <option value="promociones" >Todas las categorias </option>
                        <?php } ?>
                        
                    <?php foreach (Category::with("subcategories")->whereNull("parent_id")->get() as $key => $category): 
                        if($categoria == $category->slug) { ?>
                            <option value="<?php echo $category->slug; ?>" selected><?php echo $category->name; ?>  </option>
                        <?php }else{ ?>
                            <option value="<?php echo $category->slug; ?>" ><?php echo $category->name; ?>  </option>
                    <?php } endforeach; ?>
                    </select>
                    
                </div>
                <div class="" style="display: flex;justify-content: center;padding:10px 0;">
                    <button type="submit" id="buscar" class="btn btn-custom" style="height: 3rem;">BUSCAR</button>
                </div>
                <div id="dropdown-proveedores" class="vertical-menu" style="">
                
                <br>
                    <div class="divider"></div>
                        <a href="<?php echo base_url() ?>promociones" class="sub-menu-item">Todas las categorias </a>
                    <div class="divider"></div>
                    <?php foreach (Category::with("subcategories")->whereNull("parent_id")->get() as $key => $category): ?>
                        <a href="<?php echo base_url()."promociones-$category->slug"; ?>" class="sub-menu-item"><?php echo $category->name; ?>  </a>
                    <div class="divider"></div>
                    <?php endforeach; ?>
                </div>
                <input type="hidden" id="url" value="<?php echo base_url() ?>">
            </form>
        </div>

        <div class="col prov s12">
        
            <?php $i = 0; foreach ($promociones as $key => $promocion) { ?>
                <?php 
                    if( $promocion->tipo_cuenta==3){?>  
                      <?php  if ($i==0) { ?>
                        <div id="promo" class="card-panel" style="padding: 0px;margin-top: 10px;">
                                    <img class="responsive-img" id="imagen"
                                            src="<?php  echo $promocion->mime_imagen != null ? "data:$promocion->mime_imagen;base64,".base64_encode($promocion->imagen) : "" ?>">
                                
                                <div id="element-name"> 
                                                
                                    <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="nom"> <?php echo $promocion->nombre_prov ?></a>
                                   
                                    <br>
                                    <p style="">
                                        <?php  echo $promocion->nombre ?>
                                    </p>
                                    <button class="btn dorado-2" style="width: 90%;margin-bottom: 10px">
                                    
                                    <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="desc2"> <text class="txtDesc">DESCUENTO: </text><strike> $ <?php  echo $promocion->precio_original ?> </strike> &nbsp; &rarr; $ <?php  echo $promocion->precio_desc ?> </a>
                                    </button>
                                </div>
                        </div>
                        <?php $i=1; } elseif($i==1){ ?>
                            <div id="promo4" class="card-panel" style="padding: 0px;margin-top: 10px;">
                                    <img class="responsive-img" id="imagen"
                                            src="<?php  echo $promocion->mime_imagen != null ? "data:$promocion->mime_imagen;base64,".base64_encode($promocion->imagen) : "" ?>">
                                
                                <div id="element-name"> 
                                          
                                    <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="nom"> <?php echo $promocion->nombre_prov ?></a>
                                
                                    <br>
                                    <p style="">
                                        <?php  echo $promocion->nombre ?>
                                    </p>
                                    <button class="btn dorado-2" style="width: 90%;">
                                    
                                    <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="desc2"> <text class="txtDesc">DESCUENTO: </text><strike> $ <?php  echo $promocion->precio_original ?> </strike> &nbsp; &rarr; $ <?php  echo $promocion->precio_desc ?> </a>
                                    </button>
                                </div>
                        </div>
                <?php $i=0; } ?>
                <?php } ?>
            <?php } ?>
            <div class="row" >
                <?php foreach ($promociones as $key => $promocion) { ?>
                    <?php 
                        if( $promocion->tipo_cuenta==2){?>
                            <div class="col m6 s12">
                                <div id="promo2" class="card-panel" style="padding: 0px;margin-top: 10px;">
                                            <img class="responsive-img" id="imagen"
                                                    src="<?php  echo $promocion->mime_imagen != null ? "data:$promocion->mime_imagen;base64,".base64_encode($promocion->imagen) : "" ?>">
                                        
                                        <div id="element-name">               
                                            <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="nom"> <?php echo $promocion->nombre_prov ?></a>
                                            
                                            <p style="">
                                                <?php  echo $promocion->nombre ?>
                                            </p>
                                            <button class="btn dorado-2" style="width: 90%;margin-bottom: 10px">
                                            
                                            <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="desc2"> <text class="txtDesc">DESCUENTO: </text><strike> $ <?php  echo $promocion->precio_original ?> </strike> &nbsp; &rarr; $ <?php  echo $promocion->precio_desc ?> </a>
                                            </button>
                                        </div>
                                </div>
                        </div>
                    <?php } ?>
               
                   <?php if( $promocion->tipo_cuenta==1){ ?>  

                    <div class="col m6 s12">
                        <div id="promo3" class="card-panel" style="padding: 0px;margin-top: 10px;">
                                    <img class="responsive-img" id="imagen"
                                            src="<?php  echo $promocion->mime_imagen != null ? "data:$promocion->mime_imagen;base64,".base64_encode($promocion->imagen) : "" ?>">
                                
                                <div id="element-name">                   
                                    <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="nom"> <?php echo $promocion->nombre_prov ?></a>
                                    
                                    <p style="">
                                        <?php  echo $promocion->nombre ?>
                                    </p>
                                    <button class="btn dorado-2" style="width: 100%;margin-bottom: 10px">
                                    
                                    <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="desc2"> <text > <strike> $ <?php  echo $promocion->precio_original ?> </strike> &nbsp; &rarr; $ <?php  echo $promocion->precio_desc ?></text> </a>
                                    </button>
                                </div>
                        </div>
                    </div>
                   <?php } ?>
                <?php } ?>
            </div>
        </div>
        </div>
    </div>
</body>
<script>

$(document).ready(function() {
        let currentState = '';
        let search = '';
        let url = '';
        const states = [
            {'name': 'Todos'},
            {'name': 'Jalisco'},
            {'name': 'Queretaro'},
            {'name': 'Puebla'},
            {'name': 'Sinaloa'},
            {'name': 'Estado de Mexico'},
            {'name': 'Ciudad de Mexico'},
        ];
        const baseURL = $('#url').val();
        url = new URL(window.location.href);
        currentState = (url.searchParams.get('estado') === null) ? '' : url.searchParams.get('estado');
        search = (url.searchParams.get('buscar') === null) ? '' : url.searchParams.get('buscar');
        let stateSelected = 'Todos';

        $('#stateId').on('change', function() {
            stateSelected = $('#stateId').val();
            
            if (stateSelected != null) {
                // location.href = baseUrl+"/promociones";
               updateMenuUrl(baseURL, search, stateSelected);
            }
        });

        $('#btn-search').on('change', function() {
            search = $('#btn-search').val();
            updateMenuUrl(baseURL, search, stateSelected);
        });

        getUserLocation();

        //$('#stateId').trigger('change');

        function updateMenuUrl(baseURL, search, state) {
            search = search == null ? '' : search;
            state = state == null ? '' : state;+
            $('#dropdown-proveedores .sub-menu-item').each(function() {
                let categoryURL = $(this).attr('href');
                let categoryDirty = categoryURL.substr(categoryURL.lastIndexOf('/') + 1);
                let clearCategory = categoryDirty.split('?');
                $(this).attr('href', `${baseURL}${clearCategory[0]}?buscar=${search}&estado=${state}`);
            });

            $('#dropdown-proveedores .sub-sub-menu-item').each(function() {
                let categoryURL = $(this).attr('data-href');
                let categoryDirty = categoryURL.substr(categoryURL.lastIndexOf('/') + 1);
                let clearCategory = categoryDirty.split('?');
                $(this).attr('href', `${baseURL}${clearCategory[0]}?buscar=${search}&estado=${state}`);
            });
        }

        function updateSelect() {
            $('select[name="estado"]').empty();

            if (currentState != '') {
                $('select[name="estado"]').append($('<option>', {
                    value: currentState,
                    text: currentState,
                }));
            }

            for (let state of states) {
                if (currentState !== state.name) {
                    $('select[name="estado"]').append('<option value=\'' + state.name + '\'>' + state.name + '</option>');
                }
            }

            $('select[name="estado"]').material_select();
        }

        function updateUrl() {
            if (url.pathname != '/') {
                window.location.href = window.location.href + '?buscar=' + search + '&estado=' + currentState;
            }
        }

        function getUserLocation() {
            $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    if (currentState == '') {
                        if (!states.includes(location.state) && location.state != 'Not Found') {
                            currentState = location.state;
                        }
                        else if (location.state == 'Mexico City') {
                            currentState = 'Ciudad de Mexico';
                        }
                        else if (location.state == 'Estado de Mexico') {
                            currentState = 'Ciudad de Mexico';
                        }
                        else {
                            currentState = 'Jalisco';
                        }
                        updateUrl();
                    }

                    updateSelect();
                    updateMenuUrl(baseURL, search, currentState);
                },
            });
            return '';
        }
    });

</script>

</html>
<?php
$this->load->view("japy/prueba/footer");
$this->load->view("general/newfooter");
?>