<script>
    $(document).ready(function () {
        var server = "<?php echo base_url() ?>";
        let stars = 0;
        let providerId = $('#providerId').val();

        stars = $('.valoraciones > .fa-star[style*="color:#f9a825"]').length;

        $('.valoraciones > .fa-star').hover((e) => {
            $(e.target).siblings().andSelf().css('color', 'gray');
            $(e.target).prevAll().andSelf().css('color', '#f9a825');
        }, (e) => {
            $(e.target).siblings().andSelf().css('color', 'gray');
            if (stars > 0) {
                $(e.target).parent().find(`.fa-star:nth-child(${stars})`).prevAll().andSelf().css('color', '#f9a825');
            }
        }).click((e) => {
            stars = $(e.target).prevAll().andSelf().length;
            Materialize.toast("Calificaión guardada...",4000);
            $.ajax({
                url: server + "escaparate/calificar/",
                method: "POST",
                data: {
                    providerId: providerId,
                    stars: stars
                },
                success: function (resp) {
                    $(e.target).prevAll().andSelf().css('color', '#f9a825');
                },
                error: function () {
                    $(e.target).prevAll().andSelf().css('color', 'gray');
                }
            });
        });

        $("#galeria .next").on("click", function () {
            var left = parseInt($("#galeria .imagenes > div").get(0).style.left.replace("px", ""));
            left = ((isNaN(left) ? 0 : left) - 200);

            if (left < $("#galeria .imagenes > div").children().length * -75) {
                left = left + 200;
                ;
            }
            $("#galeria .imagenes > div").get(0).style.left = left + "px";
        });

        $("#galeria .prev").on("click", function () {
            var left = parseInt($("#galeria .imagenes > div").get(0).style.left.replace("px", ""));
            left = ((isNaN(left) ? 0 : left) + 200);
            if (left > 0) {
                left = 0;
            }
            $("#galeria .imagenes > div").get(0).style.left = left + "px";
        });

        $("#galeria .imagen").on("click", function () {
            var img = $(this).get(0).style["background-image"];
            $("#galeria .principal").get(0).style["background-image"] = (img);
        });

        $(".card .close-card").on("click", function () {
            var $self = $(this);
            $self.parent().addClass("hide-mini");
            setTimeout(function () {
                $self.parent().remove();
            }, 900);
        });
    });

    /* Encode string to slug */
    function convertToSlug(str) {
        //replace all special characters | symbols with a space
        str = str.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ');

        // trim spaces at start and end of string
        str = str.replace(/^\s+|\s+$/gm, '');

        // replace space with dash/hyphen
        str = str.replace(/\s+/g, '-');
        return str;
    }
</script>