<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <title><?php echo $proveedor->nombre ?> - BrideAdvisor</title>
        <?php //$this->view("proveedor/header");
        //$this->view("general/newheader");
        $this->view("japy/header") ?>
        <link href="<?php echo base_url() ?>dist/css/lightbox.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/unite-gallery.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/ug-theme-default.css" type="text/css"/>
        
        <style>
            #solicitar_info {
                display: none;
            }

            #gallery {
                width: 100% !important;
            }

            #gallery2 {
                width: 100% !important;
            }
            
            #promo {
                height: 200px;
                position: relative
                
            }

            .desc2{
                color: white;
                font-weight: 500;
                font-size: 1.1rem;
            }

            #imagen {
                max-width: 100% !important;
                max-height: 100% !important;
                
                width: 100%;
                object-fit: cover;
                
                margin-bottom: -100%;
            }

            #div-imagen{
                
                position: absolute;
                z-index: 2;
            }

            #element-name {
                width: 70%;
                height: 100%;
                /* max-height: 100% !important; */
                background-color: #3f4a4f;
                /* background-color: white; */
                /* border: 2px solid white; */
                padding: .9rem;
                font-weight: 500;
                font-size: 1.1rem;
                /* text-align: center; */
                position: absolute;
                z-index: 1;
                background-color: rgba(47, 55, 59, 0.7);
                /* float: left; */
            }

            #element-name p {
                color: white;
            }

            #element-name .nom {
                /* color: #00bcdd !important; */
                color: white;
                font-weight: 500;
                font-size: 1.4rem;
            }

            .ug-textpanel-bg {
                background-color: rgba(0, 0, 0, 0.6) !important;
            }

            .contact {
                padding: 5px 1rem;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
            }

            .contact h5 {
                text-align: center;
                font-weight: bolder;
            }

            .contact input, .contact textarea {
                background-color: white !important;
                color: #000000;
                border: none !important;
            }

            .contact span {
                font-size: 1rem;
            }

            .contact button {
                background-color: #8c8d8f;
                border: none;
            }

            .contact a {
                color: white;
            }

            form > input, form > textarea {
                margin: 0 !important;
                margin-top: 10px !important;
            }
            @media only screen and (max-width: 360px) {
                .txtDesc {
                    display: none;
                }
                .descrip{
                    display: none;
                }
                .desc2{
                    font-size: 13px;
                }
            }
        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            // $this->view("proveedor/menu");
        } else {
            // $this->view("general/menu");
            $this->load->view("general/head");
        }
        ?>
        <div class="body-container">
            <?php if ($this->checker->isProveedor()) : ?>
                <?php $this->view("proveedor/escaparate/menu") ?>
            <?php endif; ?>

            <section id="menu" class="row">
                <?php $this->view("principal/proveedor/menu"); ?>
            </section>

            <section id="label">
            </section>

            <div class="row">

            <div  class="col s12 m6" <?php if (count($videos)<1){ echo 'style="display:block;"'; } else { echo 'style="display:none;"'; }?> >
            <?php if ($galeria->count()) { ?>
                    <div id="gallery" class="col s12 card-panel" style="">
                        <?php
                        foreach ($galeria as $key => $g) { ?>
                            <img alt=""
                                src="<?php echo base_url()."uploads/images/$g->nombre" ?>"
                                data-image="<?php echo base_url()."uploads/images/$g->nombre" ?>">
                        <?php } ?>
                    </div>
            <?php } ?>
            </div>

            <div class="col s6 hide-on-small-only" <?php if (count($videos)<1){ echo 'style="display:block;"'; } else { echo 'style="display:none;"'; }?>>
            <section class="contact card-panel primary-background secondary-text">
                <div class="row">
                    <h5>Contacta con <?php echo $proveedor->nombre ?></h5>
                    <form action="" id="contact-form" data-parsley-validate="true">
                        <input type="hidden" value="<?php echo $proveedor->id_proveedor ?>" name="providerId">
                        <?php if ( ! isset($this->session->userdata()["id_usuario"])) : ?>
                            <input type="text" name="name" placeholder="Nombre" data-parsley-minlength="6" data-parsley-required>
                            <input type="email" name="email" placeholder="Email" data-parsley-minlength="6" data-parsley-required>
                        <?php endif; ?>
                        <textarea name="message" id="" cols="30" rows="10" placeholder="Mensaje" data-parsley-minlength="6" data-parsley-maxlength="250" data-parsley-required></textarea>
                        <div class="row" style="margin-top: 1rem;">
                            <div class="col s10" style="padding-left: 0 !important;">
                            <span>Al dar clic en continuar estas aceptando los terminos,condiciones y nuestras politicas de privacidad.
                                <a href="/terminos-japy.pdf">Consulta.</a>
                            </span>
                            </div>
                            <div class="col s2">
                                <button type="submit">ENVIAR</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
            </div>
            </div>
            <div class="row">
            <div class="col s12 m6" <?php if (count($videos)>0){ echo count($videos);  echo 'style="display:block;"'; } else { echo 'style="display:none;"'; }?> >
            <?php if ($galeria->count()) { ?>
                <div id="gallery2" class="col s12 card-panel">
                <img alt=""
                data-type="html5video"
                autoplay="true"
                data-videomp4="<?php echo($this->config->base_url() . "uploads/videos/" . $videos->first()->nombre) ?>"
                
                    <?php foreach ($galeria as $key => $g) { ?>
                        data-image="<?php echo base_url()."uploads/images/$g->nombre" ?>">
                        <img alt=""
                                src="<?php echo base_url()."uploads/images/$g->nombre" ?>"
                                data-image="<?php echo base_url()."uploads/images/$g->nombre" ?>">
                    <?php } ?>
                </div>
            <?php } ?>
            </div>
            
            <div class="col s6 hide-on-small-only" <?php if (count($videos)>0){ echo count($videos);  echo 'style="display:block;"'; } else { echo 'style="display:none;"'; }?>>
            <section class="contact card-panel primary-background secondary-text">
                <div class="row">
                    <h5>Contacta con <?php echo $proveedor->nombre ?></h5>
                    <form action="" id="contact-form" data-parsley-validate="true">
                        <input type="hidden" value="<?php echo $proveedor->id_proveedor ?>" name="providerId">
                        <?php if ( ! isset($this->session->userdata()["id_usuario"])) : ?>
                            <input type="text" name="name" placeholder="Nombre" data-parsley-minlength="6" data-parsley-required>
                            <input type="email" name="email" placeholder="Email" data-parsley-minlength="6" data-parsley-required>
                        <?php endif; ?>
                        <textarea name="message" id="" cols="30" rows="10" placeholder="Mensaje" data-parsley-minlength="6" data-parsley-maxlength="250" data-parsley-required></textarea>
                        <div class="row" style="margin-top: 1rem;">
                            <div class="col s10" style="padding-left: 0 !important;">
                            <span>Al dar clic en continuar estas aceptando los terminos,condiciones y nuestras politicas de privacidad.
                                <a href="/terminos-japy.pdf">Consulta.</a>
                            </span>
                            </div>
                            <div class="col s2">
                                <button type="submit">ENVIAR</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
            </div>
            </div>


            <div class="row" >
                <div class="col m6 s12">
                    <?php if (count($promociones)) { ?>
                        <section id="promociones">

                            <h5>Promociones de <?php echo $proveedor->nombre ?></h5>
                            <div class="divider"></div>

                            <?php foreach ($promociones as $key => $promocion) { ?>
                                <div id="promo" class="card-panel" style="padding: 0px;margin-top: 10px;">
                                    <!-- <div class="row" style="display:flex;align-items: center;"> -->
                                        <!-- <div id="div-imagen"> -->
                                            <img class="responsive-img" id="imagen"
                                                 src="<?php  echo $promocion->mime_imagen != null ? "data:$promocion->mime_imagen;base64,".base64_encode($promocion->imagen) : "" ?>">
                                        <!-- </div> -->
                                        <div id="element-name">
                                            <?php if ($proveedor->localizacion_latitud) { ?>                    
                                            <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="nom"> <?php echo $promocion->nombre ?></a>
                                            <?php } ?>
                                            <br>
                                            <p style="margin: 0;" class="descrip">
                                                <?php  echo substr(strip_tags($promocion->descripcion), 0, 70) ?> ...
                                            </p>
                                            <br>
                                            <button class="btn dorado-2" style="width: 90%;margin-bottom: 10px">
                                            
                                            <a href="<?php echo site_url("escaparate/promociones/$promocion->id_promocion") ?>" class="desc2"> <text class="txtDesc">DESCUENTO: </text><strike> $ <?php  echo $promocion->precio_original ?> </strike> &nbsp; &rarr; $ <?php  echo $promocion->precio_desc ?> </a>
                                            </button>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            <?php } ?>

                        </section>
                    <?php } ?>
                    <?php if ($proveedor->localizacion_latitud) {
                        $lat = $proveedor->localizacion_latitud;
                        $lng = $proveedor->localizacion_longitud;
                        ?>
                        <section id="mapa" data-latitud="<?php echo $proveedor->localizacion_latitud ?>"
                                 data-longitud="<?php echo $proveedor->localizacion_longitud ?>">
                            <h5>Mapa</h5>
                            <div class="divider"></div>
                            <div id="map" class="z-depth-1" style="margin: 0.5rem 0;">
                                <?php if ($proveedor->principal && $proveedor->logo) { ?>
                                    <div class="col s6 offset-s3" style="margin-bottom: -300%">
                                        <div class="card">
                                            <i class="close-card fa fa-times white-text"></i>
                                            <div class="card-image">
                                                <img src="<?php echo $proveedor->principal ?>" style="object-fit: cover">
                                                <span class="card-title"><?php echo $proveedor->nombre ?></span>
                                            </div>
                                            <div class="card-content hide" style="font-size: 12px">
                                                <p><?php echo $proveedor->localizacion_direccion ?></p>
                                            </div>
                                            <div class="card-action" style="font-size: 12px;padding: 0px">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <img class="responsive-img" style="width: 100%"
                                     src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $lat ?>,<?php echo $lng ?>&zoom=13&scale=false&size=600x300&maptype=roadmap&format=png&key=<?php echo $this->config->item('google_api_key') ?>&visual_refresh=true&markers=size:mid%7C<?php echo $lat ?>,<?php echo $lng ?>">
                            </div>
                        </section>
                    <?php } ?>
                    <?php if (count($colaboradores)) : ?>
                        <section id="colaboradores">
                            <h5>Empresas colaboradoras</h5>
                            <div class="divider"></div>
                            <div class="row">
                                <?php foreach ($colaboradores as $colaborador) : ?>
                                    <div class="col s12 l4">
                                        <div class="card">
                                            <div class="card-image">
                                                <img src="<?php echo $colaborador->imageLogo ? "uploads/images/".$colaborador->imageLogo->nombre : "dist/img/blog/perfil.png" ?>">
                                            </div>
                                            <div class="card-content">
                                                <a href="<?php echo base_url()."boda-$colaborador->slug" ?>"><?php echo $colaborador->nombre ?></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </section>
                    <?php endif; ?>
                </div>
                <div class="col m6 s12">
                    <section>
                        <?php if ($faqs->count()) : ?>
                            <h5> Información</h5>
                            <div class="divider"></div>
                            <?php $this->view("principal/proveedor/informacion_escaparate",
                                ["tipo" => $tipo, "preguntas" => $faqs]) ?>
                        <?php endif; ?>
                    </section>
                    <section class="descripcion">
                        <h5>Descripci&oacute;n</h5>
                        <div class="divider"></div>
                        <div class="card-panel">
                            <?php echo $proveedor->descripcion ?>
                        </div>
                    </section>
                    <?php
                    $extraFaqs = $faqs->filter(function ($faq, $key) {
                        return $faq->tipo != "RANGE" && ! $faq->titulo;
                    });
                    ?>
                    <?php if ($extraFaqs->count()) : ?>
                        <section>
                            <h5>
                                M&aacute;s informaci&oacute;n
                            </h5>
                            <div class="divider"></div>
                            <ul class="collection z-depth-1">
                                <?php foreach ($extraFaqs as $key => $faq) : ?>
                                    <li class="collection-item">
                                        <b><?php echo $faq->pregunta ?></b> <?php echo $faq->respuesta ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </section>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div>
            <?php $this->view("principal/proveedor/solicitar_info") ?>
        </div>
        <?php $this->view("japy/footer") ?>
        <?php $this->view("/principal/proveedor/actions_footer") ?>
        <input type="hidden" value="<?php echo base_url() ?>" id="base-url">
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/parsley.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/i18n/es.js"></script>
        <script src="<?php echo base_url() ?>dist/js/lightbox.js"></script>
        <script type="text/javascript" src='<?php echo base_url() ?>dist/js/unitegallery.min.js'></script>
        <script type="text/javascript" src='<?php echo base_url() ?>dist/js/ug-theme-default.js'></script>
        <script>
            $(document).ready(function() {
                const $contactForm = $('form:visible');
                const baseUrl = $('#base-url').val();
                $contactForm.parsley();

                $contactForm.on('submit', function(e) {
                    e.preventDefault();

                    $contactForm.parsley('validate');
                    if ($contactForm.parsley('isvalid')) {
                        swal({
                            title: 'Cargando...',
                            closeOnClickOutside: false,
                            buttons: false,
                        });

                        $.ajax({
                            url: baseUrl + 'escaparate/contact',
                            method: 'post',
                            data: $(this).serialize(),
                            success: function(response) {
                                $contactForm.prev().html('Tu mensaje ha sido enviado!');
                                $contactForm.empty();
                                swal.close();
                            },
                            error: function(error) {
                                swal('Error', error.responseJSON.message, 'error');
                            },
                        });
                    }
                });

                $('a.button-collapse').sideNav();
            });

            var options = [
                {selector: '#fotos', offset: 200, callback: animation},
            ];

            Materialize.scrollFire(options);

            function animation() {
                var speed = 900;
                var container = $('.display-animation');
                container.each(function() {
                    var elements = $(this).children();
                    elements.each(function() {
                        var elementOffset = $(this).offset();
                        var offset = (elementOffset.left * 0.8 + elementOffset.top * 0.8);
                        var delay = parseFloat(offset / speed).toFixed(2) - 1.5;
                        $(this).css('-webkit-transition-delay', delay + 's').css('-o-transition-delay', delay + 's').css('transition-delay', delay + 's').addClass('animated');
                    });
                });

                setTimeout(function() {
                    $('.display-animation').removeClass('display-animation');
                }, 3000);
            }

            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,
                'fitImagesInViewport': true,
            });
            jQuery('#gallery').unitegallery({
                slider_scale_mode: 'fit',
            });

            jQuery('#gallery2').unitegallery({
                slider_scale_mode: 'fit',
            });

        </script>
    </body>
</html>