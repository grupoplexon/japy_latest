
<html>
    <head>
        <?php $this->view("proveedor/header") ?>
        <style>

            .z-depth-2-hover:hover{
                box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            }

            #map{
                height: 350px;
            }
        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("principal/menu");
        }
        ?>
        <div class="body-container">
            <section id="menu" class="row">
                <?php $this->view("principal/proveedor/menu") ?>
            </section>
            <div class="row">
                <div class="col m8 s12">
                    <h5>Evento<?php echo (count($eventos) > 1 ? "s" : "") ?> (<?php echo $proveedor->nombre ?>)</h5>
                    <div class="divider"></div>
                    <div class="col m12 s12">
                        <?php if ($eventos) { ?>
                            <?php if (count($eventos) > 1) { ?>
                                <?php foreach ($eventos as $key => $e) { ?>
                                    <div class="card-panel z-depth-2-hover clickable evento-click"
                                         data-href="<?php echo site_url("/escaparate/eventos/$proveedor->id_proveedor/$e->id_evento") ?>"
                                         data-id="<?php echo $e->id_evento ?>">
                                        <div class="row">
                                            <img src="data:<?php echo $e->mime_imagen ?>;base64,<?php echo base64_encode($e->imagen) ?>" alt="" class="circle left" width="150">
                                            <div style="margin-left: 160px">
                                                <a href="<?php echo site_url("/escaparate/eventos/$proveedor->id_proveedor/$e->id_evento") ?>"   class="title dorado-2-text">
                                                    <?php echo $e->nombre ?>
                                                </a>
                                                <p> <small><b>Del <?php echo dateMiniFormat($e->fecha_inicio) ?> a <?php echo dateMiniFormat($e->fecha_termino) ?></b></small><br>
                                                    <?php echo $e->direccion ?>  <?php echo $e->poblacion ?>, <?php echo $e->estado ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <?php $e = $eventos[0] ?>
                                <div class="card">
                                    <div class="card-content">
                                        <div class="row">
                                            <div style="width: 150px; float: left; border: 1px solid #e2e2e2; border-radius: 5px;    overflow: hidden;">
                                                <div class="mes dorado-1-text white-text center fa-3x" ><?php echo dateMiniFormat($e->fecha_inicio, "%b") ?></div>
                                                <div class="dia center fa-3x">
                                                    <?php echo dateMiniFormat($e->fecha_inicio, "%d") ?>
                                                </div>
                                            </div>
                                            <div style="width: calc( 100% - 160px ); float: left;margin-left: 10px;" > 
                                                <text style="font-size: 28px"  id="titulo"><?php echo $e->nombre ?></text>
                                                <div class="row">
                                                    <div class="col s4">Fecha <br><?php echo dateMiniFormat($e->fecha_inicio) ?></div>
                                                    <div class="col s4">Horario <br><?php echo timeFormat($e->fecha_inicio, "%I:%M") ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col s12 m6">
                                                <?php echo $e->descripcion ?>
                                            </div>
                                            <div class="col s12 m6">
                                                <img style="    border: 9px solid white; box-shadow: 0px 0px 3px #b5b5b5;" class="responsive-img" src="data:<?php echo $e->mime_imagen ?>;base64,<?php echo base64_encode($e->imagen) ?>">
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <b>Mapa de <?php echo $e->direccion ?> <?php echo $e->poblacion ?>, <?php echo $e->estado ?></b>
                                            <div id="map" class="mapa" data-longitud="<?php echo $e->longitud ?>" data-latitud="<?php echo $e->latitud ?>">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        <?php } else { ?>
                            <div class="valign-wrapper card-panel no-hay " style="height: 250px">
                                <div class="valign align-center" style="width: 100%">
                                    <i class="fa fa-info fa-3x "></i><br>No hay eventos
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col m4 s12">
                    <?php $this->view("principal/proveedor/solicitar_info") ?>
                </div>
            </div>
        </div>
        <?php $this->view("proveedor/footer") ?>
        <?php $this->view("/principal/proveedor/actions_footer") ?>
        <script>

            $(document).ready(function () {
                $(".evento-click").on("click", function () {
                    location.href = $(this).data("href");
                });
            });
            var map;
            var marker;
            function initMap() {
                var elem = document.getElementById("map");
                var titulo = document.getElementById("titulo");
                center = {lat: parseFloat(elem.getAttribute("data-latitud")), lng: parseFloat(elem.getAttribute("data-longitud"))};
                map = new google.maps.Map(elem, {
                    center: center,
                    zoom: 14
                });
                marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    title: titulo.innerHTML
                });
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap"></script>
    </body>
</html>