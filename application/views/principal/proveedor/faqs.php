<html>
<head>
    <?php
    $this->view("proveedor/header");
    $this->view("general/newheader");
    ?>
</head>
<body>
<?php
function printRange($values, $type = null)
{
    $v = explode("|", $values);
    if (strtoupper($type) == null) {
        if (count($v) == 1) {
            return "Desde $ ".$v[0];
        } else {
            return "Desde $ ".$v[0]." hasta $ ".$v[1];
        }
    } elseif (strtoupper($type) == "PERSONAS") {
        if (count($v) == 1) {
            return "Desde  ".$v[0];
        } else {
            return "Desde  ".$v[0]." hasta  ".$v[1];
        }
    }
}

if ($this->checker->isProveedor()) {
    // $this->view("proveedor/menu");
} else {
    // $this->view("general/menu");
    $this->load->view("general/head");
}
?>
<div class="body-container">
    <section id="menu" class="row">
        <?php $this->view("principal/proveedor/menu") ?>
    </section>
    <div class="row">
        <div class="col m8 s12">
            <div class="">
                <div class="">
                    <ul class="collection z-depth-1">
                        <?php foreach ($faqs as $key => $faq) { ?>
                            <li class="collection-item">
                                <div class="row">
                                    <div class="col m6">
                                        <b>
                                            <i class="fa fa-chevron-right dorado-2-text"></i> <?php echo $faq->pregunta ?>
                                        </b>
                                    </div>
                                    <div class="col m6">
                                        <?php
                                        if ($faq->tipo == "RANGE") {
                                            echo printRange($faq->respuesta, $faq->valores);
                                        } else {
                                            echo $faq->respuesta;
                                        }
                                        ?>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->view("proveedor/footer") ?>
<?php $this->view("/principal/proveedor/actions_footer") ?>
</body>
</html>