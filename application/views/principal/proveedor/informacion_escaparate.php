<ul class="collection z-depth-1">
    <?php foreach ($preguntas as $key => $p) { ?>
        <?php if ($p->titulo && $p->respuesta) { ?>
            <li class="collection-item row center-align"
                style="display:flex; align-items:center;padding: 0 !important;">
                <div class="col s12 m4">
                    <?php echo $p->titulo ?>
                </div>
                <div class="col s12 m8" style="border-left: 1px solid #e0e0e0">
                    <?php if ($p->tipo == "RANGE") { ?>
                        <p style="margin: 0;"><?php echo printRange($p->respuesta, $p->valores) ?></p>
                    <?php } else { ?>
                        <p style="margin: 0;"><?php echo $p->respuesta ?></p>
                    <?php } ?>
                </div>
            </li>
        <?php } ?>
    <?php } ?>
    <?php if ($tipo && $tipo->producto) { ?>
        <li class="collection-item row center-align">
            <div class="col s12 m4">
                <i class=""></i> <?php echo $tipo->producto ?>
            </div>
            <div class="col s12 m8" style="border-left: 1px solid #e0e0e0">
                <a href="<?php echo site_url("escaparate/productos/".$proveedor->id_proveedor); ?>"
                   class="btn-flat dorado-2-text">
                    Ver m&aacute;s
                </a>
            </div>
        </li>
    <?php } ?>
</ul>


<?php

function printRange($values, $type = null)
{
    $v = explode("|", $values);
    if (strtoupper($type) == null) {
        if (count($v) == 1) {
            return "Desde $ ".$v[0];
        } else {
            return "Desde $ ".$v[0]." hasta $ ".$v[1];
        }
    } elseif (strtoupper($type) == "PERSONAS") {
        if (count($v) == 1) {
            return "Desde  ".$v[0];
        } else {
            return "Desde  ".$v[0]." hasta  ".$v[1];
        }
    }
}
