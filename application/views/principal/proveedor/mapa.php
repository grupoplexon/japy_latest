<html>
    <head>
        <?php $this->view("proveedor/header");
        $this->view("general/newheader"); ?>
        <style>

            .gm-style-iw {
                width: 350px !important;
                top: 15px !important;
                left: 0px !important;
                background-color: #fff;
                box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                border-radius: 2px 2px 10px 10px;
            }

            #iw-container {
                margin-bottom: 10px;
                width: 100%;
            }

            #iw-container .iw-title {
                height: 125px;
                overflow: hidden;
            }

            #iw-container .iw-content {
                font-size: 13px;
                line-height: 18px;
                font-weight: 400;
                margin-right: 0px;
                padding: 5px 5px 0px 15px;
                max-height: 140px;
                overflow-y: auto;
                overflow-x: hidden;
                margin-bottom: 13px;
                overflow: hidden;
            }

            .iw-subTitle {
                font-size: 16px;
                font-weight: 700;
                padding: 5px 0;
            }
        </style>
    </head>
    <body>

        <?php
        if ($this->checker->isProveedor()) {
            // $this->view("proveedor/menu");
        } else {
            // $this->view("general/menu");
            $this->load->view("general/head");
        }
        ?>
        <div class="body-container">

            <?php if ($this->checker->isProveedor()) : ?>
                <?php $this->view("proveedor/escaparate/menu") ?>
            <?php endif; ?>

            <section id="menu" class="row">
                <?php $this->view("principal/proveedor/menu") ?>
            </section>
            <div class="row">
                <div class="col s12">
                    <h5>Mapa (<?php echo $proveedor->nombre ?>)</h5>
                    <div class="divider"></div>
                    <div class="card-panel">
                        <div id="map" style="height: 450px">

                        </div>
                    </div>
                    <input name="latitud" id="latitud" type="hidden" value="<?php echo $proveedor->localizacion_latitud ?>">
                    <input name="longitud" id="longitud" type="hidden"
                           value="<?php echo $proveedor->localizacion_longitud ?>">

                </div>
            </div>
        </div>
        <div id="template-infowidow" class="hide" style="display: none">
            <div id="iw-container">
                <div class="iw-title">
                    <?php if ($proveedor->principal) : ?>
                        <img style="width:100%"
                             src="<?php echo $proveedor->principal ?>"
                             class="responsive-img">
                    <?php else : ?>
                        <img style="width:100%"
                             src="<?php echo base_url() ?>/dist/img/slider1.png"
                             class="responsive-img">
                    <?php endif; ?>
                </div>
                <div class="iw-content">
                    <div class="iw-subTitle dorado-2-text"><?php echo $proveedor->nombre ?></div>
                    <label><?php echo $proveedor->localizacion_direccion ?></label><br>
                    <label><?php echo $proveedor->localizacion_poblacion ?>
                        , <?php echo $proveedor->localizacion_estado ?></label>
                    <div style="width:330px">
                        <div class="row" style="margin:0px">
                            <a class="col s4 dorado-2-text" title="Fotos"
                               href="<?php echo base_url() ?>boda-<?php echo $proveedor->slug ?>">
                                <i class="fa fa-camera "></i> <?php echo $galeria->count() ?>
                            </a>
                            <a class="col s4 dorado-2-text" title="Videos"
                               href="<?php echo($videos->count() ? base_url().'boda-'.$proveedor->slug.'/videos' : "#") ?>">
                                <i class="fa fa-video-camera"></i> <?php echo $videos->count() ?>
                            </a>
                            <a class="col s4 dorado-2-text" title="Promociones"
                               href="<?php echo base_url() ?>boda-<?php echo $proveedor->slug ?>">
                                <i class="fa fa-tag"></i> <?php echo count($promociones) + ($proveedor->descuento ? 1 : 0) ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->view("proveedor/footer") ?>
        <?php $this->view("/principal/proveedor/actions_footer") ?>
        <script>

            var map;
            var marker;

            function initMap() {
                var center = getPosicion();
                var elem = document.getElementById('map');
                drawMap(elem, center);
            }

            function drawMap(elem, center) {
                map = new google.maps.Map(elem, {
                    center: center,
                    zoom: 14,
                });
                marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    animation: google.maps.Animation.DROP,
                });
                var infowindow = new google.maps.InfoWindow({
                    content: $('#template-infowidow').html(),
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker); // map e marker são as vari&aacute;veis definidas anteriormente
                });
                google.maps.event.addListener(map, 'click', function() {
                    infowindow.close();
                });
                google.maps.event.addListener(infowindow, 'domready', function() {
                    var iwOuter = $('.gm-style-iw');
                    var iwBackground = iwOuter.prev();
                    iwBackground.children(':nth-child(2)').css({'display': 'none'});
                    iwBackground.children(':nth-child(4)').css({'display': 'none'});
                    iwOuter.parent().parent().css({left: '0px'});
                    iwBackground.children(':nth-child(1)').attr('style', function(i, s) {
                        return s + 'left: 191px !important;';
                    });
                    iwBackground.children(':nth-child(3)').attr('style', function(i, s) {
                        return s + 'left: 191px !important;';
                    });
                    iwBackground.children(':nth-child(3)').find('div').children().css({
                        'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px',
                        'z-index': '1',
                    });
                    var iwCloseBtn = iwOuter.next();
                    iwCloseBtn.css({
                        opacity: '1',
                        right: '38px',
                        width: '27px',
                        height: '27px',
                        top: '3px',
                        border: '7px solid #FFFFFF',
                        background: 'white',
                        'border-radius': '13px',
                        'box-shadow': 'rgb(127, 127, 127) 0px 0px 5px',
                    });
                    if ($('.iw-content').height() < 140) {
                        $('.iw-bottom-gradient').css({display: 'none'});
                    }
                    iwCloseBtn.mouseout(function() {
                        $(this).css({opacity: '1'});
                    });
                    $('#moreInfo2').on('click', function(e) {
                        e.preventDefault();
                        $('#moreInfo').trigger('click');
                    });
                });

                infowindow.open(map, marker);
            }

            function getPosicion() {
                if ($('#latitud').val() !== '') {
                    return {lat: parseFloat($('#latitud').val()), lng: parseFloat($('#longitud').val())};
                }
                return null;
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap"></script>
    </body>
</html>