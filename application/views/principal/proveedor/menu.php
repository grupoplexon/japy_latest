<?php

function active($nom)
{
    return (strpos(strtolower($_SERVER["REQUEST_URI"]), $nom)) ? "active" : "";
}

?>
<style>
    div.logo {
        width: 130px;
        height: 130px;
        background-color: white;
        border-radius: 100px;
        border: 2px solid white;
        position: absolute;
        margin-left: -65px;
        margin-top: -135px;
        z-index: 99;
        left: 50%;
        background-size: contain;
        background-repeat: no-repeat;
    }

    .card .card-action {
        background-color: rgb(140, 141, 143) !important;
    }

    .card .card-action a:not(.btn):not(.btn-large):not(.btn-floating) {
        color: #efefef !important;
    }

    .card .card-action a:not(.btn):not(.btn-large):not(.btn-floating).active {
        color: #00BCDD !important;
    }

    .card .card-action a:not(.btn):not(.btn-large):not(.btn-floating):hover {
        color: #00BCDD;
    }

    .card .close-card {
        display: block;
        position: absolute;
        float: right;
        top: 5px;
        right: 7px;
        z-index: 30;

    }

    .card.hide-mini {
        transform: scale(0, 0);
        opacity: 0.2;
        transition: 1s;
    }

    .materialize-textarea {
        min-height: 5rem;
    }

    .range-input {
        width: 20%;
        margin: .5rem;
    }

    .rangos {
        display: flex;
    }

    .review h5, div:has(> #btn-review) {
        text-align: center;
    }

    @media only screen and (min-width: 322px) and (max-width: 425px) {
        .icon-provider-op {
            font-size: 21px !important;
        }

        .row-icons {
            text-align: center;
        }

        .logo-provider {
            margin: auto;
        }

        .rangos {
            display: inherit;
        }

        .range-input {
            width: 100%;
        }

        #input-text {
            width: 100%;
        }

        #btn-review {
            margin-top: 2rem;
        }
    }

    @media only screen and (max-width: 321px) {
        .logo-provider {
            margin: auto;
        }
        .row-icons {
            text-align: center;
        }
        .icon-provider-op {
            font-size: 21px !important;

        }
        .rangos {
            display: inherit;
        }
        .range-input {
            width: 100%;
        }
        #input-text {
            width: 100%;
        }
        #btn-review {
            margin-top: 2rem;
        }
    }

    @media only screen and (min-width: 426px) and (max-width: 768px) {
        .rangos {
            justify-content: space-between;
            flex-wrap: wrap;
        }
        .range-input {
            width: 43%;
        }
    }
</style>
<div class="col s12 m12">
    <div class="card z-depth-2">
        <div class="card-content " style="padding-bottom: 0px">
            <div class="row" style="margin-bottom: 0px">
                <div class="col m2 l2 s12">
                    <?php if ($proveedor->logo) : ?>
                        <div class="logo-provider" style="background-image: url('<?php echo($proveedor->logo) ?>');
                                background-repeat: no-repeat;
                                background-position: center;
                                background-size:cover;
                                height: 150px;
                                width: 150px;">
                            <?php if ($provider->tipo_cuenta == 3) { ?>
                                <img src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png" alt="" class="cintillo">
                            <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                <img src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png" alt="" class="cintillo">
                            <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                <img src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png" alt="" class="cintillo">
                            <?php } ?>
                        </div>
                    <?php else : ?>
                        <div class="logo-provider"
                             style="background-image: url('<?php echo(base_url()."dist/img/blog/perfil.png") ?>');
                                     background-repeat: no-repeat;
                                     background-position: center;
                                     background-size:cover;
                                     height: 150px;
                                     width: 150px;">
                            <?php if ($provider->tipo_cuenta == 3) { ?>
                                <img src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png" alt="" class="cintillo">
                            <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                <img src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png" alt="" class="cintillo">
                            <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                <img src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png" alt="" class="cintillo">
                            <?php } ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col m9 l10 s12" style="float: right">
                    <div class=" col m8 s12">
                        <h5 id="provider-name"><?php echo $proveedor->nombre ?></h5>
                        <div class=" yellow-text text-darken-3">
                            <?php $average  = 0;
                            $currentReviews = 0 ?>
                            <?php  $weddings = $provider->weddings->where("visible",0);
                            foreach ($weddings as $wedding) :
                                if ($wedding->pivot->calificacion > 0 ) {
                                    $average = $average + $wedding->pivot->calificacion;
                                    $currentReviews++;
                                }
                            endforeach;
                            if ($currentReviews == 0):
                                $average = $average / 1;
                            else:
                                $average = $average / $currentReviews;
                            endif;
                            ?>
                            <?php if ($average > 0) : ?>
                                <?php for ($i = 0; $i < 5; $i++) : ?>
                                    <i class="fa fa-star" <?php if ($i < round($average)) {
                                        echo("style='color:#f9a825;'");
                                    } else {
                                        echo("style='color:gray;'");
                                    } ?>></i>
                                <?php endfor; ?>
                            <?php else : ?>
                                <?php for ($i = 0; $i < 5; $i++) : ?>
                                    <i class="fa fa-star" style="color:gray;"></i>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <?php if ($currentReviews == 1): ?>
                                <small class="primary-text"><?php echo $currentReviews ?> recomendación</small>
                            <?php else: ?>
                                <small class="primary-text"><?php echo $currentReviews ?> recomendaciones</small>
                            <?php endif; ?>
                        </div>
                        <div class="localizacion">
                            <p>
                                <i class="fa fa-map-marker"></i> <?php echo $proveedor->localizacion_direccion ?>
                                (<?php echo $proveedor->localizacion_estado ?>)
                                <?php if ($proveedor->localizacion_latitud) { ?>
                                <a href="<?php echo base_url()."boda-$proveedor->slug/mapa" ?>">
                                    <small>Ver mapa</small>
                                    <?php } ?>
                                </a>
                            </p>
                        </div>
                        <p>
                            <a class="clickable col s12 l1"
                               href="">
                                <?php if ($proveedor->descuento) { ?>
                                    <label class="desc clickable"
                                           style="background-position: 17px;width: 100px;float: left;font-size: 17px !important;height: 35px !important;padding-top: 4px;padding-left: 32px !important;">
                                        <?php echo $proveedor->descuento ?>%
                                    </label>
                                    <label class="clickable" style="display: table-caption;">
                                        Descuento en brideadvisor.mx
                                    </label>
                                <?php } ?>
                            </a>
                        </p>
                    </div>

                    <div class="col s12 hide-on-med-and-up">
                        <section class="contact card-panel primary-background secondary-text">
                            <div class="row">
                                <h5>Contacta con <?php echo $proveedor->nombre ?></h5>
                                <form action="" id="contact-form">
                                    <input id="providerId" type="hidden" value="<?php echo $proveedor->id_proveedor; ?>" name="providerId">
                                    <?php if ( ! isset($this->session->userdata()["id_usuario"])) : ?>
                                        <input type="text" name="name" placeholder="Nombre">
                                        <input type="email" name="email" placeholder="Email">
                                    <?php endif; ?>
                                    <textarea name="message" id="" cols="30" rows="10" placeholder="Mensaje"></textarea>
                                    <div class="row" style="margin-top: 1rem;">
                                        <div class="col s2" style="padding: 0;">
                                            <button type="submit">ENVIAR</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <div class="col m4 s12">
                        <p>
                            <button data-proveedor="<?php echo $proveedor->id_proveedor ?>"
                                    class="proveedor-favorite btn dorado-2" style="width: 100%;margin-bottom: 10px;color:white;position: initial">
                                <i class="fa fa-heart" style="color: white !important;"></i>
                                <text class="hide-on-small-only"><?php echo $favorito ? "GUARDADO" : "GUARDAR" ?></text>
                            </button>
                            <button class="ver_telefono btn dorado-2" style="width: 100%;margin-bottom: 10px">
                                <i class="fa fa-phone"></i>
                                <text class="hide-on-small-only">Ver tel&eacute;fono</text>
                            </button>
                        </p>
                        <p>
                            <label>Comp&aacute;rtelo en:</label> <br>
                            <a class="btn blue col m4 s12 btn-share-link"
                               href="https://www.facebook.com/sharer/sharer.php?app_id=2052619388319572&sdk=joey&u=<?php echo(base_url()."boda-$proveedor->slug") ?>&display=popup&ref=plugin&src=share_button"
                               onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')"
                               style="margin-bottom: 10px; padding-right: 5px;">
                                <i class="fa fa-facebook-official" style="color: white !important;"></i>
                            </a>
                            <a class="btn blue lighten-3 col m4 s12 btn-share-link-tw"
                               data-href="<?php echo site_url("boda-$proveedor->slug") ?>"
                               data-quote="<?php echo "#japy $proveedor->nombre" ?>" style="margin-bottom: 10px">
                                <i class="fa fa-twitter" style="color: white !important;"></i> <span style="color: white !important;">Tweet<span>
                            </a>
                        </p>
                    </div>
                </div>
                <p>&nbsp;</p>
            </div>
        </div>
        <div class="card-action row-icons" style="font-size: 12px">
            <a href="<?php echo base_url()."boda-".$proveedor->slug ?>"
               class="icon-provider-op <?php echo preg_match("/(boda-)([\\p{L}0-9]+(?:-[\\p{L}0-9]+)*)$/",
                   $_SERVER["REQUEST_URI"]) ? "active" : "" ?>">
                <i class="fa fa-th-large"></i>
                <text class="hide-on-small-only">Perfil</text>
            </a>
            <?php if (count($faqs)) : ?>
                <a href="<?php echo base_url()."boda-".$proveedor->slug."/faqs" ?>"
                   class="icon-provider-op <?php echo active("faqs"); ?>">
                    <i class="fa fa-info"></i>
                    <text class="hide-on-small-only">FAQs</text>
                </a>
            <?php endif; ?>
            <?php if (count($galeria)) { ?>
                <a href="<?php echo base_url()."boda-".$proveedor->slug."/fotos" ?>"
                   class="icon-provider-op <?php echo active("fotos"); ?>" style="display: none">
                    <i class="fa fa-camera-retro"></i>
                    <text class="hide-on-small-only">Fotos</text>
                </a>
            <?php } ?>
            <?php if (count($videos) && $proveedor->tipo_cuenta == 3) { ?>
                <a href="<?php echo base_url()."boda-".$proveedor->slug."/videos" ?>"
                   class="icon-provider-op <?php echo active("videos"); ?>">
                    <i class="fa fa-video-camera"></i>
                    <text class="hide-on-small-only">Videos</text>
                </a>
            <?php } ?>

            <a href="<?php echo site_url("escaparate/recomendaciones/$proveedor->slug") ?>"
               class="icon-provider-op <?php echo active("recomendaciones"); ?>">
                <i class="fa fa-star"></i>
                <text class="hide-on-small-only">Recomendaciones</text>
            </a>
            <!--<a href="#"><i class="fa fa-genderless">    </i> <text class="hide-on-small-only">Bodas Reales</text>   </a>-->

            <!--            --><?php //if (count($promociones)) { ?>
            <!--                <a href="-->
            <?php //echo site_url("escaparate/promociones/$proveedor->id_proveedor") ?><!--"-->
            <!--                   class="icon-provider-op --><?php //echo active("promociones"); ?><!--">-->
            <!--                    <i class="fa fa-tags"></i>-->
            <!--                    <text class="hide-on-small-only">Promociones</text>-->
            <!--                </a>-->
            <!--            --><?php //} ?>
            <!--            --><?php //if ($tipo && $tipo->producto) { ?>
            <!--                <a href="-->
            <?php //echo site_url("escaparate/productos/$proveedor->id_proveedor") ?><!--"-->
            <!--                   class="icon-provider-op --><?php //echo active("productos"); ?><!--">-->
            <!--                    <i class="fa fa-shopping-bag"></i>-->
            <!--                    <text class="hide-on-small-only">--><?php //echo $tipo->producto ?><!--</text>-->
            <!--                </a>-->
            <!--            --><?php //} ?>
            <?php if ($proveedor->localizacion_latitud) { ?>
                <a href="<?php echo base_url()."boda-".$proveedor->slug."/mapa" ?>"
                   class="icon-provider-op <?php echo active("mapa"); ?>">
                    <i class="fa fa-map-marker"></i>
                    <text class="hide-on-small-only">Mapa</text>
                </a>
            <?php } ?>
            <?php //if ($proveedor->localizacion_latitud) { ?>
                <!-- <a href="<?php //echo site_url("escaparate/promociones/$proveedor->slug") ?>"
                   class="icon-provider-op <?php //echo active("mapa"); ?>">
                    <i class="fa fa-map-marker"></i>
                    <text class="hide-on-small-only">Promociones</text>
                </a> -->
            <?php //} ?>
            <!--            --><?php //if ($eventos) { ?>
            <!--                <a href="-->
            <?php //echo site_url("escaparate/eventos/$proveedor->id_proveedor") ?><!--"-->
            <!--                   class="icon-provider-op --><?php //echo active("evento"); ?><!--">-->
            <!--                    <i class="fa fa-calendar-o"></i>-->
            <!--                    <text class="hide-on-small-only">Eventos</text>-->
            <!--                </a>-->
            <!--            --><?php //} ?>
        </div>
    </div>
</div>
<input id="isProvider" type="hidden" value="<?php echo $this->checker->isProveedor() ?>">

<script>
    $(document).ready(function() {
        const $moreInformationModal = $('#modal_mas_informacion');
        const isProvider = $('#isProvider').val();
        let canReview = $('#canReview').val();
        let favorite = $('#favorite').val();

        if (canReview && favorite) {
            $('.review').show();
        }
        else {
            $('.review').hide();
        }

        if (isProvider) {
            $('.proveedor-favorite').on('click', function(e) {
                e.preventDefault();
                swal('Error', 'Esta funcion es solo para usuarios', 'error');
            });

            $('.ver_telefono').on('click', function(e) {
                e.preventDefault();
                swal('Error', 'Esta funcion es solo para usuarios', 'error');
            });

        }
        else {
            $('.proveedor-favorite').on('click', function(e) {
                e.preventDefault();
                var $self = $(this);
                $.ajax({
                    url: server + 'escaparate/guardarlo/' + $self.data('proveedor'),
                    method: 'POST',
                    data: {
                        proveedor: $self.data('proveedor'),
                    },
                    success: function(resp) {
                        if (resp.data.favorito) {
                            $self.find('text').html('GUARDADO');
                        }
                        else {
                            $self.find('text').html('GUARDAR');
                            $('.valoraciones > .fa-star').siblings().andSelf().css('color', 'gray');
                        }
                    }, error: function() {
                        if (isProvider) {
                            $errorModal.modal('open');
                        }
                        if (!isProvider) {
                            if (!$('#toast-container .toast').get(0)) {
                                Materialize.toast('Debes estar registrado para guardar un proveedor<br> ' + " <a href='<?php echo site_url('Cuenta') ?>?callback=" + location.href +
                                    '\' class=\'dorado-2-text pull-right\'>Inicia Sesi&oacute;n <i class=\'material-icons right\'>keyboard_arrow_right</i></a>', 5000);
                                $('.toast').css({display: 'block'});
                            }
                        }
                    },
                });
                if(canReview && $('.proveedor-favorite').find('text').html() === 'GUARDAR'){
                    $('.review').show();
                }else{
                    $('.review').hide();
                }
            });

            $('.ver_telefono').on('click', function(e) {
                e.preventDefault();
                var $self = $(this);
                $.ajax({
                    url: server + "escaparate/telefono/<?php echo $proveedor->id_proveedor ?>",
                    success: function(resp) {
                        var padre = $self.parent();
                        $self.remove();
                        padre.append('<h5 style=\'font-weight: bold;\'>Tel: ' + resp.data.telefono + '<br><small>Y no olvides decir que vienes de brideadvisor.mx</small></h5>');
                    }, error: function() {
                        if (isProvider) {
                            $errorModal.modal('open');
                        }
                        if (!isProvider) {
                            if (!$('#toast-container .toast').get(0)) {
                                Materialize.toast('Debes estar registrado para ver el teléfono del proveedor<br> ' + " <a href='<?php echo site_url('Cuenta') ?>?callback=" + location.href + '\' class=\'dorado-2-text pull-right\'>Inicia Sesi&oacute;n <i class=\'material-icons right\'>keyboard_arrow_right</i></a>', 5000);
                                $('.toast').css({display: 'block'});
                            }
                        }
                    },
                });
            });

        }

        $('.btn-share-link-tw').on('click', function() {
            var $this = $(this);
            var url = $this.data('href');
            if (url) {
                var titulo = $this.data('quote');
                console.log('https://twitter.com/share?url=' + url + '&text=' + titulo);
                window.open('https://twitter.com/share?url=' + url + '&text=' + encodeURIComponent(titulo), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
            }
        });
    });
</script>
