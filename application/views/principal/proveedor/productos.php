<html>
    <head>
        <?php $this->view("proveedor/header") ?>
        <style>
            .producto.ver-mas{
                position: relative;
                margin-top: -21px;
                width: 100%;
                display: block;
                background: white;
                background: -webkit-linear-gradient(rgba(254,254,254,0.2), white);
                background: -o-linear-gradient(rgba(254,254,254,0.2), white);
                background: -moz-linear-gradient(rgba(254,254,254,0.2), white);
                background: linear-gradient(rgba(254,254,254,0.2), white);
                cursor: pointer;
            }
            .content-producto{
                height: 150px;margin-bottom: 0px; overflow: hidden;
                transition: 0.3s;
            }
            .content-producto.active{
                height: inherit;margin-bottom: 10px; overflow:auto;
            }
        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("principal/menu");
        }
        ?>
        <div class="body-container">
            <section id="menu" class="row">
                <?php $this->view("principal/proveedor/menu") ?>
            </section>
            <div class="row">
                <div class="col m8 s12">
                    <h5>Producto<?php echo (count($productos) > 1 ? "s" : "") ?> (<?php echo $proveedor->nombre ?>)</h5>
                    <div class="divider"></div>
                    <div class="col m12 s12">
                        <?php if (($productos)) { ?>
                            <?php foreach ($productos as $key => $r) { ?>
                                <div class="card-panel z-depth-1" id="<?php echo $r->id_producto ?>">
                                    <div class="row">
                                        <h5>
                                            <?php echo $r->nombre ?>
                                        </h5>
                                        <div class="divider"></div>
                                        <div class="row text-justify content-producto ">
                                            <?php echo $r->detalle ?>
                                        </div>
                                        <div class="ver-mas producto center">
                                            <text>ver m&aacute;s</text>
                                            <i class="fa fa-chevron-down dorado-2-text"></i>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="col s12">
                                            <p>
                                                <a class=" btn-flat waves-effect waves-pink pull-right btn-share-link"  data-href="<?php echo site_url("escaparate/producto/$r->id_proveedor/$r->id_producto") ?>" data-quote="<?php echo substr(strip_tags($r->detalle), 0, 150) ?>...">
                                                    <i class="fa fa-facebook-official"> </i> Compartir 
                                                </a>
                                                <a class=" btn-flat waves-effect waves-pink pull-right btn-share-link-tw"  data-href="<?php echo site_url("escaparate/producto/$r->id_proveedor/$r->id_producto") ?>" data-quote="<?php echo substr(strip_tags($r->detalle), 0, 150) ?>...">
                                                    <i class="fa fa-twitter"> </i> Compartir 
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="card-panel z-depth-1">
                                <p class="center">
                                    <i class="material-icons fa-4x">info_outline</i><br>
                                    No tiene <?php echo $tipo->label_producto ?> registrados
                                </p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col m4 s12">
                    <?php $this->view("principal/proveedor/solicitar_info"); ?>
                </div>
            </div>
        </div>
        <?php $this->view("proveedor/footer"); ?>
        <?php $this->view("/principal/proveedor/actions_footer") ?>
        <script>
            $(document).ready(function () {
                $(".ver-mas").on("click", function () {
                    $(this).parent().find(".content-producto").toggleClass("active");
                    $(this).find("i").toggleClass("fa-chevron-down");
                    $(this).find("i").toggleClass("fa-chevron-up");
                });
            });
        </script>
    </body>
</html>