<html>
<head>
    <?php $this->view("proveedor/header");
          $this->view("general/newheader");?>
</head>
<body>
<?php
if ($this->checker->isProveedor()) {
    // $this->view("proveedor/menu");
} else {
    // $this->view("general/menu");
    $this->load->view("general/head");
}
?>
<div class="body-container">
    <section id="menu" class="row">
        <?php $this->view("principal/proveedor/menu") ?>
    </section>
    <div class="row">
        <div class="col m8 s12">
            <section id="promocion" class="carrusel row" data-id="<?php echo $promocion->id_promocion ?>"
                     data-proveedor="<?php echo $proveedor->id_proveedor ?>">
                <h5>Promocion de <?php echo $proveedor->nombre ?></h5>
                <div class="divider"></div>
                <div class="card-panel">
                    <div class="row">
                        <div class="col s12 m6">
                            <div class="card-panel" style="position: relative;overflow: hidden">
                                <img class="responsive-img"
                                     style="width: 100%;max-width: inherit;"
                                     src="<?php echo $promocion->imagen == null ? $proveedor->logo : "data:$promocion->mime_imagen;base64,".base64_encode($promocion->imagen) ?>">
                                <div class="label-promocion <?php echo color_tipo_promocion($promocion->tipo) ?> z-depth-1">
                                    <?php echo icon_tipo_promocion($promocion->tipo) ?><?php echo strtoupper($promocion->tipo) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6">
                            <h5><?php echo $promocion->nombre ?></h5>
                            <div class="divider"></div>
                            <label>
                                <i class="fa fa-calendar-o"></i> <?php echo $promocion->fecha_fin ? "Finaliza el".dateMiniFormat($promocion->fecha_fin) : "Promoci&oacute;n permanente" ?>
                            </label>
                            <div class="descripcion">
                                <?php echo $promocion->descripcion ?>
                            </div>
                            <div class="botones">
                                <?php if ($descargas) { ?>
                                    <div class="card-panel z-depth-0 green-text darken-5 green lighten-5"
                                         style="background-color: #8C8D8F;">
                                        <i class="fa fa-check"></i> <?php echo $descargas > 1 ? "$descargas personas ya descargaron este cup&oacute;n" : "$descargas persona ya descargo este cup&oacute;n" ?>
                                    </div>
                                <?php } else { ?>
                                    <div class="card-panel z-depth-0 green-text darken-5 green lighten-5">
                                        Se el primero en descargar el cupon
                                    </div>
                                <?php } ?>
                                <button class="btn dorado-2 waves-effect waves-light btn modal-trigger"
                                        data-target="modal-descargar">
                                    <i class="fa fa-ticket"></i> Descargar cup&oacute;n
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col m4 s12">
            <?php $this->view("principal/proveedor/solicitar_info") ?>
        </div>
    </div>
</div>

<div id="modal-descargar" class="modal modal-fixed-footer" style="width: 85%">
    <div class="modal-content">
        <div>
            <i class="fa fa-times modal-close"
               style="position: absolute;right: 10px;top: 10px;color: #F9A797;cursor: pointer;font-size: 22px;"></i>
            <h5>Descargar cup&oacute;n de <?php echo $proveedor->nombre ?></h5>
        </div>
        <div class="row green lighten-3"
             style="padding-bottom: 0px;padding: 20px;margin-bottom: 0px;margin-left: -24px;margin-right: -24px;font-size: 12px;">
            <i class="fa fa-tag circle"></i>
            Para descargar el cup&oacute;n debes rellenar el siguiente formulario.
        </div>
        <div class="row" style="padding-bottom: 0px;padding: 20px;margin-bottom: 0px;">
            <?php $cliente = $this->checker->getCliente() ?>
            <?php $boda = $this->checker->getBoda() ?>
            <div class="row">
                <div class="col s12 m6">
                    <p>
                        <label>Nombre</label>
                        <input id="nombre" class="form-control" name="nombre"
                               value="<?php echo $this->session->userdata("nombre") ? $this->session->userdata("nombre") : "" ?>">
                    </p>
                    <p>
                        <label>Tel&eacute;fono</label>
                        <input id="telefono" class="form-control" name="telefono"
                               value="<?php echo $cliente ? $cliente->telefono : "" ?>">
                    </p>
                    <p>
                        <label>Fecha estimada</label>
                        <input id="fecha" class="form-control" name="fecha"
                               value="<?php echo $boda ? date_format(date_create($boda->fecha_boda), 'Y-m-d') : "" ?>">
                    </p>
                </div>
                <div class="col s12 m6">
                    <p>
                        <label>E-mail</label>
                        <input id="email" class="form-control" name="email"
                               value="<?php echo $this->session->userdata("correo") ? $this->session->userdata("correo") : "" ?>">
                    </p>
                    <p>
                        <label>Invitados</label>
                        <input id="invitados" class="form-control" name="invitados" value="250">
                    </p>
                    <p>
                        <label>Comentario</label>
                        <textarea id="comentario" class="form-control" style="height: 150px">He descargado el cup&oacute;n de <?php echo strtolower($promocion->tipo) ?> <?php echo $promocion->nombre ?>
                            y me gustar&iacute;a recibir m&aacute;s informaci&oacute;n sobre esta promoci&oacute;n.
                                </textarea>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div style="width: calc( 100% - 250px );float: left;">
            Al presionar "Descargar cup&oacute;n" te est&aacute;s dando de alta y aceptando las condiciones legales de
            clubnupcial.com.
        </div>
        <a id="btn-descargar" href="#!" class=" modal-action modal-close waves-effect waves-green btn dorado-2"><i
                    class="fa fa-download"></i> Descargar cup&oacute;n</a>
    </div>
</div>

<div id="modal-cupon" class="modal " style="width: 85%">
    <div class="modal-content">
        <i class="fa fa-times modal-close"
           style="position: absolute;right: 10px;top: 10px;color: #F9A797;cursor: pointer;font-size: 22px;"></i>
        <div class="row">
            <div class="col green lighten-3 black-text"
                 style="    width: calc(100% - 250px); padding: 15px;margin-right: 21px;">
                <i class="fa fa-envelope"></i> Se ha enviado a tu buzon
            </div>
            <button id="btn-imprimir" class=" btn dorado-2">
                <i class="fa fa-print"></i> Imprimir cupon
            </button>
        </div>
        <div id="section-print">

            <div class="divider"></div>
            <br>
            <img class="" src="<?php echo base_url() ?>dist/img/clubnupcial.png" style="width: 250px">
            <div class="card-panel">
                <div class="row">
                    <div class="col m4 s4">
                        <img class="responsive-img"
                             style="width: 100%;max-width: inherit;"
                             src="<?php echo $promocion->imagen == null ? $proveedor->logo : "data:$promocion->mime_imagen;base64,".base64_encode($promocion->imagen) ?>">
                    </div>
                    <div class="col m8 s8">
                        <h5 class="gris-2-text"><?php echo $promocion->nombre ?></h5>
                        <b><?php echo $proveedor->nombre ?></b><br>
                        <label>Direccion:</label><br>
                        <?php echo $proveedor->localizacion_direccion ?>
                        , <?php echo $proveedor->localizacion_poblacion ?>
                        (<?php echo $proveedor->localizacion_estado ?>)
                        <br>
                        <div class="divider"></div>
                        <br>
                        <div class="row">
                            <div class="col m6 s6">
                                <i class="fa fa-calendar-o"></i> <?php echo $promocion->fecha_fin ? "Finaliza el ".dateMiniFormat($promocion->fecha_fin) : "Promoci&oacute;n permanente" ?>
                            </div>
                            <div class="col m6 s6">
                                <img class="" src="<?php echo base_url() ?>dist/img/clubnupcial.png"
                                     style="width: 150px;float: right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <h6>Detalle de la oferta</h6>
                    <div class="divider"></div>
                    <h6><?php echo $proveedor->nombre ?></h6>
                    <p>
                        <?php echo $promocion->descripcion ?>
                    </p>
                </div>
                <div class="col s6">
                    <div class="card-panel">
                        <?php $lat = $proveedor->localizacion_latitud ?>
                        <?php $lng = $proveedor->localizacion_longitud ?>
                        <?php if ($lat) { ?>
                            <img class="responsive-img mapa" style="width: 100%"
                                 src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $lat ?>,<?php echo $lng ?>&zoom=13&scale=false&size=600x300&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7C<?php echo $lat ?>,<?php echo $lng ?>&key=<?php echo $this->config->item("google_api_key") ?>">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input id="providerId" type="hidden" value="<?php echo $provider->id_proveedor ?>">

<?php $this->view("proveedor/footer") ?>
<?php $this->view("/principal/proveedor/actions_footer") ?>
<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap"></script>-->
<script>
    var server = "<?php echo base_url() ?>";
    $(document).ready(function () {
        $('.modal').modal();
        $('.modal-trigger').modal();
        const providerId = $('#providerId').val();

        var solicitud = new Solicitudes({server: server});

        $("#btn-descargar").on("click", function () {
            var s = {
                "nombre": $("#nombre").val(),
                "email": $("#email").val(),
                "telefono": $("#telefono").val(),
                "fecha": $("#fecha").val(),
                "invitados": $("#invitados").val(),
                "comentario": $("#comentario").val(),
                "proveedor": providerId,
                "isCoupon": true
            };
            solicitud.send(s, function (r) {
                $('#modal-cupon').modal("open");
            });
            solicitud.registrarDescargaCupon({
                promocion: $("#promocion").data("id"), proveedor: $("#promocion").data("proveedor")
            });
        });

        $("#btn-imprimir").on("click", function () {
            var $elem = $("#section-print");
            ImageUtils.getBase64FromUrl($elem.find("img.mapa").attr("src"), function (base64) {
                $elem.find("img.mapa").attr("src", base64);
                solicitud.print("#section-print");
            });
        });
    });
</script>
</body>
</html>