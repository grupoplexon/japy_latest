<html>
    <head>
        <?php $this->view("proveedor/header");
        $this->view("general/newheader"); ?>
        <style>

            .gm-style-iw {
                width: 350px !important;
                top: 15px !important;
                left: 0px !important;
                background-color: #fff;
                box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                border-radius: 2px 2px 10px 10px;
            }

            #iw-container {
                margin-bottom: 10px;
                width: 100%;
            }

            #iw-container .iw-title {
                height: 125px;
                overflow: hidden;
            }

            #iw-container .iw-content {
                font-size: 13px;
                line-height: 18px;
                font-weight: 400;
                margin-right: 0px;
                padding: 5px 5px 0px 15px;
                max-height: 140px;
                overflow-y: auto;
                overflow-x: hidden;
                margin-bottom: 13px;
                overflow: hidden;
            }

            .name {
                /* color: #00bcdd !important; */
                /* color: white; */
                font-weight: 600;
                font-size: 1.9rem;
            }

            .promo {
                /* color: #00bcdd !important; 
                 color: white; */
                text-align: justify;
                font-size: 1.5rem;
            }

            .des {
                color: #00bcdd !important;
                /* color: white; */
                font-weight: 500;
                font-size: 1.3rem;
            }

            .desc2, p    {
                /* color: #00bcdd !important; */
                color: black;
                font-weight: 500;
                font-size: 1.3rem;
                text-align: justify;
            }

            .terminos{
                color: #00bcdd !important;
            }

            .iw-subTitle {
                font-size: 16px;
                font-weight: 700;
                padding: 5px 0;
            }
            .icon-style2 {
                color: #00BCDD !important;
                font-size: 20px !important;
            }
            #imagen{
                width: 100%; 
                margin-top:20px; 
                height: 70%; 
                object-fit: fill;
            }
            @media only screen and (max-width: 321px){
                section {
                    padding: 1rem 0 0 0 !important;
                }
                .desc2, p    {
                    text-align: justify;
                }
                #imagen{
                height: 100%; 
                }
            }
            @media only screen and (max-width: 425px) and (min-width: 322px){
                .desc2, p    {
                    text-align: justify;

                }
                #imagen{
                height: auto; 
                }
            }
        </style>
    </head>
    <body>

        <?php
        if ($this->checker->isProveedor()) {
            // $this->view("proveedor/menu");
        } else {
            // $this->view("general/menu");
            $this->load->view("general/head");
        }
        ?>
        <div class="body-container">
        
            <?php if ($this->checker->isProveedor()) : ?>
                <?php $this->view("proveedor/escaparate/menu") ?>
            <?php endif; ?>

            <div class="row">
                <div class="col m8 s12 ">
                    <section class="carrusel row">
                        <div >
                            <img class="responsive-img" style="" 
                            id="imagen"  src="<?php  echo "data:image/jpg;base64,".base64_encode($promociones->imagen) ?>">
                        </div>
                    </section>
                </div>
                <div class="col s12 m4">
                    <h3 class="name"><?php echo $proveedor->nombre ?></h3>
                    <h4 class="promo"> PROMOCIÓN:  <?php echo $promociones->nombre?></h4>
                    
                    <h5 class="des"> DESCUENTO: $ <strike> <?php  echo $promociones->precio_original ?> </strike> &nbsp; &rarr; $ <?php  echo $promociones->precio_desc ?></h5>
                    <h6> COMPARTE ESTA OFERTA: 
                    <a href="" class="btn-share-link-tw"  data-href="<?php echo base_url("index.php/escaparate/promociones/$promociones->id_promocion") ?>"
                                   data-quote="<?php echo "#japy"." ".strip_tags($promociones->nombre) ?>" >
                            <i class="fa fa-twitter icon-style2"></i>
                    </a>
                    <a class="btn-share-link"
                                   href="https://www.facebook.com/sharer/sharer.php?app_id=2052619388319572&sdk=joey&u=<?php echo(base_url()."index.php/escaparate/promociones/$promociones->id_promocion") ?>&display=popup&ref=plugin&src=share_button"
                                   onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')"
                                   style="margin-bottom: 10px; padding-right: 5px;">
                            <i class="fa fa-facebook  icon-style2"></i>
                    </a>
                    <a href="https://wa.me/?text=<?php echo(base_url()."index.php/escaparate/promociones/$promociones->id_promocion") ?>" data-action="share/whatsapp/share">
                        <i class="fa fa-whatsapp icon-style2"></i>
                        
                    </a>
                    </h6>
                    <h4 class="desc2"> Caracteristicas: </h4>
                    <h4 class="desc2"> <?php echo $promociones->descripcion?></h4>
                    <h6> Ver terminos y condiciones <a href="<?php echo base_url()."terminos-japy.pdf" ?>" class="terminos"> aquí </a> <h6>
                    
                </div>
            </div>

        </div>
        <?php $this->view("proveedor/footer") ?>
        <?php $this->view("/principal/proveedor/actions_footer") ?>
        <script>

            $('.btn-share-link-tw').on('click', function() {
                    var $this = $(this);
                    var url = $this.data('href');
                    if (url) {
                        var titulo = $this.data('quote');
                        console.log('https://twitter.com/share?url=' + url + '&text=' + titulo);
                        window.open('https://twitter.com/share?url=' + url + '&text=' + encodeURIComponent(titulo), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                    }
                });


        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap"></script>
    </body>
</html>







<!-- <html>
<head>
    <?php // $this->view("proveedor/header") ?>
</head>
<body>
<?php
// if ($this->checker->isProveedor()) {
//     $this->view("proveedor/menu");
// } else {
//     $this->view("principal/menu");
// }
?>
<div class="body-container">
    <section id="menu" class="row">
        <?php //$this->view("principal/proveedor/menu") ?>
    </section>
    <div class="row">
        <div class="col m8 s12">
            <section class="carrusel row">
                <h5>Promociones de <?php //echo $proveedor->nombre ?></h5>
                <div class="divider"></div>
                <?php
                // $c = 1;
                // if ($proveedor->descuento) {
                //     $temp               = new stdClass();
                //     $temp->nombre       = "$proveedor->descuento% de descuento para novi@s de clubnupcial.com ";
                //     $temp->descripcion  = "Si vienes de parte de clubnupcial.com te haremos un $proveedor->descuento% de descuento en los servicios contratados. No olvides presentar tu cup&oacute;n cuando vengas a vernos.";
                //     $temp->fecha_fin    = null;
                //     $temp->imagen       = null;
                //     $temp->mime_imagen  = null;
                //     $temp->id_proveedor = $proveedor->id_proveedor;
                //     $temp->id_promocion = 0;
                //     $temp->tipo         = "DESCUENTO";
                //     $promociones->prepend($temp);
                // }
                ?>
                <?php //foreach ($promociones as $key => $p) { ?>
                    <?php //if ($c % 2 == 0) { ?>
                        <div class="row">
                    <?php //} ?>
                    <div class="col m6 s12">
                        <div class="card">
                            <div class="card-image " style="    height: 250px;overflow: hidden;">
                                <div class="label-promocion <?php //echo color_tipo_promocion($p->tipo) ?> z-depth-1">
                                    <?php // echo icon_tipo_promocion($p->tipo) ?><?php // echo strtoupper($p->tipo) ?>
                                </div>
                                <img src="<?php// echo $p->imagen == null ? $proveedor->logo : "data:$p->mime_imagen;base64,".base64_encode($p->imagen) ?>">
                                <span class="card-title"
                                      style=" width: 100%; background: -webkit-linear-gradient(top, rgba(0,0,0,0) 3%,rgba(0, 0, 0, 0.5) 100%);"><?php echo $p->nombre ?></span>
                            </div>
                            <div class="card-content" style="min-height: 190px;">
                                <?php // echo strip_tags(substr($p->descripcion, 0, 235)) ?> ...
                                <br>
                                <label>
                                    <?php //echo $p->fecha_fin == null ? "promoci&oacute;n permanente" : dateMiniFormat($p->fecha_fin) ?>
                                </label>
                            </div>
                            <div class="card-action">
                                <a href="<?php //echo base_url() ?>escaparate/promociones/<?php //echo $p->id_proveedor ?>/<?php echo $p->id_promocion ?>">
                                    ver promocion <i class="fa fa-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php // if ($c % 2 == 0) { ?>
                        </div>
                    <?php //} ?>
                    <?php// $c++; ?>
                <?php //} ?>
            </section>
        </div>
        <div class="col m4 s12">
            <?php // $this->view("principal/proveedor/solicitar_info") ?>
        </div>
    </div>
</div>
<input id="providerId" type="hidden" value="<?php //echo $provider->id_proveedor ?>">
<?php // $this->view("proveedor/footer") ?>
<?php //$this->view("/principal/proveedor/actions_footer") ?>
</body>
</html> -->