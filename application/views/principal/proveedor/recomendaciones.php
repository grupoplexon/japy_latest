<html>
<head>
    <?php $this->view("proveedor/header");
    $this->view("general/newheader");?>
</head>
<body>
<?php
if ($this->checker->isProveedor()) {
    // $this->view("proveedor/menu");
} else {
    // $this->view("general/menu");
    $this->load->view("general/head");
}
?>
<div class="body-container">
    <section id="menu" class="row">
        <?php $this->view("principal/proveedor/menu") ?>
    </section>
    <div class="row">
        <div class="col m8 s12 offset-m2">
            <h5>
                Recomendacion<?php echo(count($recomendaciones) > 1 ? "es" : "") ?> (<?php echo $proveedor->nombre ?>)
            </h5>
            <div class="divider"></div>
            <div class="col m12 s12">
                <input type="hidden" value="<?php echo $allowToReview;?>" id="canReview">
                <input type="hidden" value="<?php echo $favorito;?>" id="favorite">
                <?php if ($this->checker->isLogin()  ) { ?>
                    <div class="review">
                        <div class="card-panel" style="padding-bottom: 10px;padding-top: 5px">
                            <div class="row no-margin" >
                                <div class="col s12 m2" style="text-align: center;margin-top: 2rem;">
                                    <img class="responsive-img review-photo border"
                                         src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $this->session->userdata(
                                             "usuario"
                                         ) ?>">
                                </div>
                                <div class="col s12 m10">
                                    <div class="row no-margin-element" >
                                        <form class="col s12" id="review-form" data-parsley-validate="true">
                                            <div class="row no-margin-element">
                                                <div class="input-field col s12 m6">
                                                    <input id="input_text" type="text" maxlength="50" minlength="5" required="">
                                                    <label for="input_text">Titúlo</label>
                                                </div>
                                                <div class="col s12 m6 center-align">
                                                    <p class="center-align">Calificación del proveedor</p>
                                                    <div class="valoraciones yellow-text text-darken-3">
                                                        <?php $average = 0;?>
                                                        <?php foreach ($provider->weddings as $wedding) :
                                                            if ($wedding->pivot->id_boda == $this->session->userdata("id_boda")) {
                                                            $average = $wedding->pivot->calificacion;
                                                            break;
                                                            }
                                                            endforeach; ?>
                                                        <?php for ($i = 0; $i < 5; $i++) : ?>
                                                            <i class="fa fa-star" <?php if ($i < round($average)) {
                                                                echo("style='color:#f9a825;'");
                                                            } else {
                                                                echo("style='color:gray;'");
                                                            } ?>></i>
                                                        <?php endfor; ?>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row ">
                                                <div class="input-field col s12">
                                                    <textarea id="textarea" class="materialize-textarea no-margin" data-length="200" maxlength="200"  minlength="5" required=""></textarea>
                                                    <label for="textarea1">Recomendaci&oacute;n</label>
                                                </div>
                                            </div>
                                            <div class="row rangos" >
                                                <p class="range-field range-input center-align">
                                                    <input id="range_service" type="range" class="range"/>
                                                    <b>Servicio</b>
                                                </p>
                                                <p class="range-field range-input center-align">
                                                    <input id="range_answer" type="range" class="range"/>
                                                    <b>Respuesta</b>
                                                </p>
                                                <p class="range-field range-input center-align">
                                                    <input id="range_price" type="range" class="range"/>
                                                    <b>Calidad / Precio</b>
                                                </p>
                                                <p class="range-field range-input center-align">
                                                    <input id="range_flex" type="range" class="range"/>
                                                    <b>Flexibilidad</b>
                                                </p>
                                                <p class="range-field range-input center-align">
                                                    <input id="range_professional" type="range" class="range"/>
                                                    <b>Profesionalismo</b>
                                                </p>
                                            </div>
                                            <div class="row no-margin">
                                                <div class="col s12 m12">
                                                    <a id="btn-review" class="waves-effect waves-light btn dorado-2"
                                                       style="    float: right;"><i class="material-icons right hide-on-small-only">send</i>Recomendar</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if (count($recomendaciones) > 0) { ?>
                    <?php foreach ($recomendaciones as $key => $r) {?>
                        <div class="card-panel z-depth-1 hide" id="<?php echo $r->boda_id ?>">
                            <div class="row">
                                <div class="col s2">
                                    <img src="<?php echo base_url() ?>index.php/perfil/foto/<?php echo $r->usuario_id?>"
                                         class="responsive-img circle">
                                </div>
                                <div class="col s10">
                                    <div class="col s12 ">
                                        <?php echo User::find($r->usuario_id)->nombre?>,
                                        <small>
                                            <?php echo relativeTimeFormat($r->fecha_creacion) ?>
                                        </small>
                                        <small class="pull-right hide" >
                                            <?php
                                            $now        = new DateTime("now");
                                            $fecha_boda = new DateTime($r->fecha_boda);
                                            ?>
                                            <?php if ($fecha_boda < $now) { ?>
                                                Se cas&oacute; el <?php echo $fecha_boda->format("d-m-Y") ?>
                                            <?php } else { ?>
                                                Se casa el <?php echo $fecha_boda->format("d-m-Y") ?>
                                            <?php } ?>
                                        </small>
                                    </div>
                                    <div class="col s6">
                                        <div class="row">
                                            <div class="col s12   m6 text-recomendacion ">
                                                Calidad del servicio:
                                            </div>
                                            <div class="col s12  m6  ">
                                                <div class="progress white" style="border:1px solid #00bcdd">
                                                    <div class="determinate dorado-1"
                                                         style="width: <?php echo($r->servicio) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12   m6 text-recomendacion ">
                                                Respuesta:
                                            </div>
                                            <div class="col s12  m6  ">
                                                <div class="progress white" style="border:1px solid #00bcdd">
                                                    <div class="determinate dorado-1"
                                                         style="width: <?php echo($r->respuesta) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12   m6 text-recomendacion ">
                                                Relaci&oacute;n calidad/precio:
                                            </div>
                                            <div class="col s12  m6  ">
                                                <div class="progress white" style="border:1px solid #00bcdd">
                                                    <div class="determinate dorado-1"
                                                         style="width: <?php echo($r->calidad_precio) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s6">
                                        <div class="row">
                                            <div class="col s12 m6 text-recomendacion ">
                                                Flexibilidad:
                                            </div>
                                            <div class="col s12  m6  ">
                                                <div class="progress white" style="border:1px solid #00bcdd">
                                                    <div class="determinate dorado-1"
                                                         style="width: <?php echo($r->flexibilidad) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m6 text-recomendacion ">
                                                Profesionalismo:
                                            </div>
                                            <div class="col s12  m6  ">
                                                <div class="progress white" style="border:1px solid #00bcdd">
                                                    <div class="determinate dorado-1"
                                                         style="width: <?php echo($r->profesionalidad) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="col s12 text-justify">
                                    <?php echo $r->mensaje ?>
                                </div>
                                <div class="divider"></div>
                                <div class="col s12">
                                    <p>
                                        <a class=" btn-flat waves-effect waves-pink pull-right btn-share-link"
                                           data-href="<?php echo site_url("escaparate/recomendaciones/$r->id_proveedor/$r->id_boda") ?>"
                                           data-quote="<?php echo substr($r->mensaje, 0, 150) ?>...">
                                            <i class="fa fa-facebook-official"> </i> Compartir
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card-panel">
                            <div class="row" style="margin: 0">
                                <div class="col s6 m2 offset-s3 center-align" style="padding-top: 1rem">
                                    <img src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $r->usuario_id?>" alt="" class="review-photo">
                                </div>
                                <div class="col s12 m10">
                                    <h6>
                                        <?php $novio = User::find($r->usuario_id);
                                        echo $novio->nombre." ".$novio->apellido?>,
                                        <small> <?php echo relativeTimeFormat($r->fecha_creacion) ?></small>
                                    </h6>
                                    <small class="pull-right hide" >
                                        <?php
                                        $now        = new DateTime("now");
                                        $fecha_boda = new DateTime($r->fecha_boda);
                                        ?>
                                        <?php if ($fecha_boda < $now) { ?>
                                            Se cas&oacute; el <?php echo $fecha_boda->format("d-m-Y") ?>
                                        <?php } else { ?>
                                            Se casa el <?php echo $fecha_boda->format("d-m-Y") ?>
                                        <?php } ?>
                                    </small>
                                    <div class=" yellow-text text-darken-3">
                                        <?php
                                        $stars = 0;
                                        foreach ($starsReviews as $s):
                                            $id_boda = (int)$s->id_boda;
                                            if($id_boda === $r->boda_id):
                                                $stars = (int)$s->calificacion;
                                            endif;
                                        endforeach;
                                        for ($i = 0; $i < 5; $i++) : ?>
                                            <i class="fa fa-star" <?php if ($i < round($stars)) {
                                                echo("style='color:#f9a825;'");
                                            } else {
                                                echo("style='color:gray;'");
                                            } ?>></i>
                                        <?php endfor; ?>
                                        <a href="#" class="moreInfo">
                                            <small >ver m&aacute;s</small>
                                        </a>
                                    </div>
                                    <div  class="row extraInfo hide">
                                        <div class="col s6">
                                            <div class="row no-margin">
                                                <div class="col s12   m6 no-padding 6 text-recomendacion ">
                                                    Servicio:
                                                </div>
                                                <div class="col s12  m6  ">
                                                    <div class="progress white" style="border:1px solid #00bcdd">
                                                        <div class="determinate dorado-1"
                                                             style="width: <?php echo($r->servicio) ?>%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin">
                                                <div class="col s12   m6 no-padding 6 text-recomendacion ">
                                                    Respuesta:
                                                </div>
                                                <div class="col s12  m6  ">
                                                    <div class="progress white" style="border:1px solid #00bcdd">
                                                        <div class="determinate dorado-1"
                                                             style="width: <?php echo($r->respuesta) ?>%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin">
                                                <div class="col s12   m6 no-padding 6 text-recomendacion ">
                                                     Calidad/Precio:
                                                </div>
                                                <div class="col s12  m6  ">
                                                    <div class="progress white" style="border:1px solid #00bcdd">
                                                        <div class="determinate dorado-1"
                                                             style="width: <?php echo($r->calidad_precio) ?>%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s6">
                                            <div class="row no-margin">
                                                <div class="col s12 m6 no-padding text-recomendacion ">
                                                    Flexibilidad:
                                                </div>
                                                <div class="col s12  m6  ">
                                                    <div class="progress white" style="border:1px solid #00bcdd">
                                                        <div class="determinate dorado-1"
                                                             style="width: <?php echo($r->flexibilidad) ?>%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row no-margin">
                                                <div class="col s12 m6 no-padding text-recomendacion ">
                                                    Profesionalismo:
                                                </div>
                                                <div class="col s12  m6  ">
                                                    <div class="progress white" style="border:1px solid #00bcdd">
                                                        <div class="determinate dorado-1"
                                                             style="width: <?php echo($r->profesionalidad) ?>%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h5><b><?php echo $r->asunto?></b></h5>
                                    <p class="text-justify" style="">
                                        <?php echo $r->mensaje ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="card-panel z-depth-1">
                        <p class="center">
                            <i class="material-icons fa-4x">info_outline</i><br>
                            A&uacute;n no tiene recomendaciones. Ingresa tú recomendación.
                        </p>
                    </div>
                <?php } ?>
            </div>
            <?php if (count($recomendaciones) > 1) { ?>
                <div class="col m8 s12">
                    <ul class="pagination">
                        <li class="<?php echo($page == 1 ? "disabled" : "waves-effect") ?>"><a
                                    href="<?php echo($page == 1 ? "#" : site_url("escaparate/recomendaciones/$id")."?pagina=".($page - 1)) ?>"><i
                                        class="material-icons">chevron_left</i></a></li>
                        <?php for ($i = 1; $i <= $total; $i++) { ?>
                            <li class="<?php echo($i == $page ? "active" : "waves-effect") ?>"><a
                                        href="<?php echo site_url("escaparate/recomendaciones/$id")."?pagina=".$i ?>"><?php echo $i ?></a>
                            </li>
                        <?php } ?>
                        <li class="<?php echo($page >= ($total - 1) ? "disabled" : "waves-effect") ?>"><a
                                    href="<?php echo($page >= ($total - 1) ? "#" : site_url("escaparate/recomendaciones/$id")."?pagina=".($page + 1)) ?>"><i
                                        class="material-icons">chevron_right</i></a></li>
                    </ul>

                </div>
            <?php } ?>
        </div>
        <div class="col m4 s12 hide">
            <?php $this->view("principal/proveedor/solicitar_info") ?>
        </div>
    </div>
</div>
<?php $this->view("proveedor/footer") ?>
<?php $this->view("/principal/proveedor/actions_footer") ?>
</body>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).on('ready',function () {
        let sliders = document.getElementsByClassName('range');
        let $btnReview = $('#btn-review');
        let $moreInfo = $('.moreInfo');
        let formData = new FormData();
        let $reviewForm = $('#review-form').parsley();
        let seeLessData = true;

        [].slice.call(sliders).forEach(function( slider, index ){
            noUiSlider.create(slider, {
                start: [0],
                connect: true,
                step: 1,
                orientation: 'horizontal', // 'horizontal' or 'vertical'
                range: {
                    'min': 0,
                    'max': 100
                },
                format: wNumb({
                    decimals: 0
                })
            });
        });



        $moreInfo.on('click',function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
           $(this).parent().siblings('.extraInfo').toggleClass('hide',500);
           if(seeLessData){
               $(this).find('small').text("Ver menos");
               seeLessData = false;
           }else{
               $(this).find('small').text("Ver más");
               seeLessData = true;
           }
        });

        $btnReview.on('click',function (e) {
            e.preventDefault();

            $reviewForm.validate();

            if($reviewForm.isValid()){
                formData.append("title",$('#input_text').val());
                formData.append("message",$('#textarea').val());
                formData.append("service",$('#range_service').val());
                formData.append("answer",$('#range_answer').val());
                formData.append("price",$('#range_price').val());
                formData.append("flex",$('#range_flex').val());
                formData.append("professional",$('#range_professional').val());
                formData.append("provider",$('.proveedor-favorite').data('proveedor'));

                $.ajax({
                    url:'<?php echo base_url()?>escaparate/saveProviderReview',
                    method: 'POST',
                    data:formData,
                    processData: false,
                    contentType: false,
                    success:function (reponse) {
                        location.reload();
                    },
                    error:function (response) {
                        if(response.success){
                            swal('Error','No fue posible registrar tú opinón, intentalo de nuevo','error');
                        }
                    }
                });
            }

        });


    });
</script>
</html>
