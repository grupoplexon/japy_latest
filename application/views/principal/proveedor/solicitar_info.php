<div id="solicitar_info" class="card">
    <div class="card-content">
        <h6>Solicitar informaci&oacute;n</h6>
    </div>
    <div class="row gris-1 white-text card-content">
        <div class="col m4">
            <img class="responsive-img"
                 src="<?php echo $proveedor->logo ? $proveedor->logo : base_url()."/dist/img/blog/perfil.png" ?>">
        </div>
        <div class="col m8 truncate">
            <h5><?php echo $proveedor->nombre ?></h5>
            <small><?php echo $tipo->nombre_tipo_proveedor ?></small>
        </div>
    </div>
    <div class="card-content">
        <form method="POST" class="form-validate form-to-ajax"
              data-success="success_solicitud"
              data-error="error_solicitud"
              action="<?php echo base_url() ?>escaparate/enviar_solicitud_informacion/<?php echo $proveedor->id_proveedor ?>">
            <?php
            //CLRSF
            function fRand($len)
            {
                $str = '';
                $a   = "abcdefghijklmnopqrstuvwxyz0123456789";
                $b   = str_split($a);
                for ($i = 1; $i <= $len; $i++) {
                    $str .= $b[rand(0, strlen($a) - 1)];
                }

                return $str;
            }

            $this->session->unset_userdata("clrsf");
            $this->session->unset_userdata("clrsf_value");
            $this->session->set_userdata("clrsf", fRand(5));
            $this->session->set_userdata("clrsf_value", fRand(5));
            ?>
            <?php $cliente = $this->checker->getCliente() ?>
            <?php $boda = $this->checker->getBoda(); ?>
            <input type="hidden" name="<?php echo $this->session->userdata("clrsf") ?>"
                   value="<?php echo $this->session->userdata("clrsf_value") ?>">
            <input type="hidden" name="proveedor" value="<?php echo $proveedor->id_proveedor ?>">
            <input type="hidden" name="asunto" value="Solicitud de informaci&oacute;n">
            <!--<p>
                <label>Nombre</label>
                <input name="nombre" class="form-control validate[required]"
                       value="<?php /*echo $this->session->userdata("nombre") */ ?>">
            </p>
            <p>
                <label>Correo</label>
                <input name="correo"
                       class="form-control validate[required,custom[email]]" <?php /*echo $this->checker->isLogin() ? "readonly" : "" */ ?>
                       value="<?php /*echo $this->session->userdata("correo") */ ?>">
            </p>
            <p>
                <label>Tel&eacute;fono</label>
                <input name="telefono" class="form-control validate[required,custom[phone]]"
                       name="telefono" <?php /*echo $this->checker->isLogin() && $cliente && $cliente->telefono ? "readonly" : "" */ ?>
                       value="<?php /*echo $cliente ? $cliente->telefono : "" */ ?>">
            </p>
            <p>
                <label>Fecha estimada</label>
                <input name="fecha" type="date" class="form-control validate[required]" name="fecha"
                       value="<?php /*echo $boda ? date_format(date_create($boda->fecha_boda), 'Y-m-d') : "" */ ?>"
                       min="<?php /*echo $boda ? date_format(date_create($boda->fecha_boda), 'Y-m-d') : "" */ ?>">
            </p>
            <?php /*if ($tipo->grupo == "BANQUETES") { */ ?>
                <p>
                    <label>Invitados</label>
                    <input name="invitados" type="number" step="any" class="form-control validate[required,min[0]]"
                           name="invitados" min="0" value="<?php /*echo $boda ? $boda->no_invitado : "250" */ ?>">
                </p>
            <?php /*} */ ?>
            <?php /*if ( ! $this->checker->isLogin()) { */ ?>
                <p>
                    <select class="form-control browser-default" name="genero" required>
                        <option value="" disabled selected>Yo soy*</option>
                        <option value="1">Novio</option>
                        <option value="2">Novia</option>
                    </select>
                </p>
            --><?php /*} */ ?>
            <p>
                <label>Mensaje</label>
                <textarea name="comentario" class="form-control validate[required,maxSize[600]]" maxlength="600"
                          style="height: 250px"></textarea>
            </p>
            <p>
                Al presionar "Enviar" est&aacute;s aceptando las condiciones legales de Japy.
            </p>
            <p>
                <button type="submit" class="btn dorado-2" style="width: 100%">
                    Enviar
                </button>
            </p>
        </form>
    </div>
</div>

<div id="modal_mas_informacion" class="modal">
    <div class="modal-content">
        <i class="fa fa-times fa-2x modal-close modal-action dorado-2-text"
           style="position: absolute; right: 5px;top: 5px;"></i>
        <div class="row"
             style="background: #8C8D8F;margin: -24px -24px;color: white;text-shadow: 1px 1px 2px black;margin-bottom: 5px;">
            <div
                    style=" background-image: url(<?php echo $proveedor->logo ? $proveedor->logo : base_url()."/dist/img/blog/perfil.png" ?>);
                            width: 110px;background-color: white;background-size: cover;height: 100px;margin: 10px 5px 0px 12px;border: 2px solid #C9C9C9;box-shadow: 1px 1px 10px #595959;"
                    class="col s2"
            ></div>
            <div class="col s10" style="width: calc( 100% - 130px )">
                <h5>Solicitar informaci&oacute;n a <?php echo $proveedor->nombre ?></h5>
                <p>Rellena este formulario y <?php echo $proveedor->nombre ?> se pondr&aacute; en contacto contigo en
                    breve. Todos los datos que env&iacute;es ser&aacute;n tratados de forma confidencial.</p>
            </div>
        </div>
        <form method="POST" class="form-validate form-to-ajax"
              data-success="success_solicitud"
              data-error="error_solicitud"
              action="<?php echo base_url() ?>escaparate/enviar_solicitud_informacion/<?php echo $proveedor->id_proveedor ?>">
            <input type="hidden" name="<?php echo $this->session->userdata("clrsf") ?>"
                   value="<?php echo $this->session->userdata("clrsf_value") ?>">
            <input type="hidden" name="proveedor" value="<?php echo $proveedor->id_proveedor ?>">
            <input type="hidden" name="asunto" value="Solicitud de informaci&oacute;n">
            <div class="row">
                <!--<div class="col s12 m6">
                    <p>
                        <label>Nombre</label>
                        <input name="nombre" class="form-control validate[required]"
                               value="<?php echo $this->session->userdata("nombre") ?>">
                    </p>
                    <p>
                        <label>Correo</label>
                        <input name="correo"
                               class="form-control validate[required,custom[email]]" <?php echo $this->checker->isLogin() ? "readonly" : "" ?>
                               value="<?php echo $this->session->userdata("correo") ?>">
                    </p>
                    <p>
                        <label>Tel&eacute;fono</label>
                        <input name="telefono" class="form-control validate[required,custom[phone]]"
                               name="telefono" <?php echo $this->checker->isLogin() && $cliente && $cliente->telefono ? "readonly" : "" ?>
                               value="<?php echo $cliente ? $cliente->telefono : "" ?>">
                    </p>
                    <p>
                        <label>Fecha estimada</label>
                        <input name="fecha" type="date" class="form-control validate[required]" name="fecha"
                               value="<?php echo $boda ? date_format(date_create($boda->fecha_boda), 'Y-m-d') : "" ?>"
                               min="<?php echo $boda ? date_format(date_create($boda->fecha_boda), 'Y-m-d') : "" ?>">
                    </p>
                    <?php if ($tipo->grupo == "BANQUETES") { ?>
                        <p>
                            <label>Invitados</label>
                            <input name="invitados" type="number" step="any"
                                   class="form-control validate[required,min[0]]" name="invitados" min="0"
                                   value="<?php echo $boda ? $boda->no_invitado : "250" ?>">
                        </p>
                    <?php } ?>
                    <?php if ( ! $this->checker->isLogin()) { ?>
                        <p>
                            <select class="form-control browser-default" name="genero" required>
                                <option value="" disabled selected>Yo soy*</option>
                                <option value="1">Novio</option>
                                <option value="2">Novia</option>
                            </select>
                        </p>
                    <?php } ?>
                </div>-->
                <div class="col s12">
                    <p>
                        <label>Mensaje</label>
                        <textarea name="comentario" class="form-control validate[required,maxSize[600]]" maxlength="600"
                                  style="height: 250px"></textarea>
                    </p>
                </div>
            </div>
            <div class="row">
                <p class="">
                    Al presionar "Enviar" est&aacute;s aceptando las condiciones legales de Japy.
                </p>
                <p class="pull-right">
                    <button type="submit" class="btn dorado-2">
                        Enviar
                    </button>
                </p>
            </div>
        </form>
    </div>
</div>

<div id="modal_enviado" class="modal modal-fixed-footer">
    <i class="fa fa-times fa-2x modal-close modal-action dorado-2-text"
       style="position: absolute; right: 5px;top: 5px;"></i>
    <div class="row">
        <div class="col s2"
             style="bottom: 0px; left: 0px;position: initial;height: 92%;background: #8C8D8F;color: white;">
            <div style="height: 110px" class="valign-wrapper">
                <div class="valign center" style="width: 100%">
                    <i class="fa fa-money fa-3x"></i>
                </div>
            </div>
            <p style="text-transform: uppercase">
                consigue el mejor presupuesto para tu boda
            </p>
            <p>
                Solicita y compara diferentes presupuestos para conseguir el m&aacute;s adecuado para tu boda.
            </p>
        </div>
        <div class="col s10" style="padding: 0px">
            <div class="modal-content col s12" style="position: initial">
                <h5>Consigue el mejor presupuesto para tu boda</h5>
                <div class="">
                    <div class="relacionados">
                        <h6>Los novios interesados en <b><?php echo $proveedor->nombre ?></b> pidieron presupuesto a las
                            siguientes empresas </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat dorado-2-text">Continuar</a>
    </div>
</div>

<div class="hide" id="template-relacionado">
    <div class="col s12 m6">
        <div class="card">
            <div class="card-image">
                <img src="{{url_logo}}">
                <span class="card-title"
                      style="font-size: 12px;background: -webkit-linear-gradient(top, rgba(0,0,0,0) 3%,rgba(0, 0, 0, 1) 100%);width: 100%;text-shadow: 1px 1px 2px black;">
                    {{nombre}}<br><small>{{poblacion}}</small>
                </span>
            </div>
            <a class="btn dorado-2 mas-informacion" href="{{url_mas}}" style="font-size: 12px;width: 100%;">
                <i class="fa fa-envelope"></i> M&aacute;s informaci&oacute;n
            </a>
        </div>
    </div>
</div>

<div id="modal_no_login" class="modal">
    <div class="modal-content">
        <h4>Ingresa en japybodas.com</h4>
        <div>
            <p>
                Esta funcion solo esta habilitada para miembros de nuestra comunidad.
            </p>
            <a href="<?php echo site_url("cuenta") ?>?callback=<?php echo $this->uri->uri_string() ?>"
               class="btn dorado-2 pull-right">
                Ingresar
                <i class="material-icons right">keyboard_arrow_right</i>
            </a>
        </div>
    </div>
    <div class="modal-footer">

    </div>
</div>

<script>
    $(document).ready(function () {
        $("button[type='submit']").on("click", function (e) {
            e.preventDefault();
            const $form = $(this).closest("form");
            $form.validationEngine("attach", {scroll: false});
            if ($form.validationEngine("validate")) {
                $form.siblings().first().trigger("click");
                $form.submit();
            }
        })
    });

    var server = "<?php echo base_url() ?>";

    function success_solicitud(resp) {
        let template = null;
        $('.relacionados').empty();
        for (let i in resp.data) {
            const provider = resp.data[i];
            template = $("#template-relacionado").html();
            template = template.replace("{{nombre}}", provider.nombre)
                .replace("{{poblacion}}", provider.localizacion_poblacion)
                .replace("{{url_mas}}", server + "boda-" + provider.slug);
            if (provider.image_principal_miniature.length) {
                template = template.replace("{{url_logo}}", server + "uploads/images/" + provider.image_principal_miniature[0].nombre);
            } else {
                template = template.replace("{{url_logo}}", server + "/dist/img/blog/perfil.png");
            }
            $(".relacionados").append(template);
        }
        $("#modal_enviado").modal("open");
    }

    function error_solicitud(error) {
        console.log(error);
        $("#modal_no_login").modal("open");
    }
</script>
