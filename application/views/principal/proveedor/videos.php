<html>
<head>
    <?php $this->view("proveedor/header");
    $this->view("general/newheader"); ?>
</head>
<body>
<?php
if ($this->checker->isProveedor()) {
    // $this->view("proveedor/menu");
} else {
    // $this->view("general/menu");
    $this->load->view("general/head");
}
?>
<div class="body-container">
    <?php if ($this->checker->isProveedor()) : ?>
        <?php $this->view("proveedor/escaparate/menu") ?>
    <?php endif; ?>
    <section id="menu" class="row">
        <?php $this->view("principal/proveedor/menu") ?>
    </section>
    <div class="row">
        <div class="col m8 s12 ">
            <section class="carrusel row">
                <div class="divider"></div>
                <div class="video">
                    <video style="width: 100%" class="z-depth-1" contextmenu="false" autoplay="true" controls="true"
                           src="<?php echo($this->config->base_url() . "uploads/videos/" . $videos->first()->nombre) ?>">
                    </video>
                </div>
            </section>
        </div>
        <div class="col s12 m4">
            <h4>Descripci&oacute;n</h4>
            <p class="text-justify"><?php
                foreach ($videos as $video) {
                    echo $video->descripcion;
                }
                ?>
            </p>
        </div>
    </div>
</div>
<?php $this->view("proveedor/footer") ?>
<?php $this->view("/principal/proveedor/actions_footer") ?>
<script src="<?php echo base_url() ?>dist/js/slider.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var pos = $('#galeria .principal').data('pos') + 1;
            var img = $("#galeria .imagenes .imagen[data-pos='" + pos + "']").get(0);
            if (img) {
                img = img.style["background-image"];
            } else {
                pos = 0;
                var img = $("#galeria .imagenes .imagen[data-pos='" + pos + "']").get(0).css("background-image");
            }
            $("#galeria .principal").data("pos", pos);
            $("#galeria .principal").get(0).style["background-image"] = img;
        }, 8000);

    });
</script>
<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap"></script>-->

</body>
</html>