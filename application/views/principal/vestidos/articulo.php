<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        
        <meta property="og:title" content="<?php echo $vestido->nombre ?>"/>
        <meta property="og:image" content="<?php echo "data:".$vestido->mime_imagen."base64,".base64_encode($vestido->imagen) ?>">
        <meta property="og:image:type" content="image/png">
        <meta property="og:url" content="<?php echo base_url("tendencia/articulo/$disenador/$modelo") ?>"/>
        <?php $this->view("principal/head");
        $this->view("general/newheader"); ?>
        <style>
            #solicitar_info {
                display: none;
            }

            a.border {
                border: 1px solid #d0d0d0;
                box-shadow: none !important;
            }

            .commentarios-2 .comment {
                height: 0px;
                transition: 1s;
                overflow: hidden;
            }

            .comentarios .btn-flat {
                font-size: 12px;
            }

            .commentarios-2 .comment.active {
                display: block;
                height: initial;
            }

            textarea.materialize-textarea:focus:not([readonly]) {
                border-bottom: 1px solid #f9a797;
                box-shadow: 0 1px 0 0 #f9a797;
            }

            .info-dress label {
                font-size: 1rem;
            }

            .info-dress p {
                font-size: 1.5rem;
                letter-spacing: 2px;
            }

            .arrows-dress {
                margin: 20px auto !important;
            }

            .preview-dress .card-action {
                padding: 16px 0px !important;
            }

            .card-image img {
                max-height: inherit !important;
            }

            textarea.materialize-textarea:focus:not([readonly]) + label {
                color: #f9a797;
            }

            @media only screen and (max-width: 425px) {
                .preview-dress {
                    margin-top: 104px !important;
                }

                .info-dress p {
                    font-size: 1rem;
                }

                .img img {
                    height: inherit !important;
                }

                .card-stacked {
                    padding: 3% 10px;
                }

                .arrows-dress {
                    margin: 30px auto !important;
                }

                .preview-dress-action {
                    padding: 16px 0px !important;
                }

                .preview-dress-action a {
                    float: none !important;
                    text-align: center !important;
                }
            }

            @media only screen and (max-width: 321px) {
                .card-image img {
                    height: inherit !important;
                }
            }

        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("general/menu");
        }
        ?>
        <div class="body-container">
            <br>
            <?php
            $this->view("principal/breadcrumbs", array(
                "breadcrumbs" => array(
                    "Home"                                       => base_url(),
                    $producto->nombre                            => site_url("tendencia/index/".str_replace(" ", "-", $producto->nombre)),
                    $producto->nombre." de ".$vestido->disenador => site_url("tendencia/disenador/".str_replace(" ", "-", $producto->nombre)."/$disenador"),
                    $vestido->nombre                             => site_url("tendencia/articulo/$disenador/$modelo"),
                ),
            ))
            ?>
            <div class="row">
                <div class="col m3 s12 right">
                    <div class="shares" style="margin-top: 11px;">
                        <a class="btn blue col m4 s12 btn-share-link btn-flat white-text"
                           href="https://www.facebook.com/sharer/sharer.php?app_id=2052619388319572&sdk=joey&u=<?php echo site_url("tendencia/articulo/$disenador/$modelo") ?>&display=popup&ref=plugin&src=share_button"
                           onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')"
                           style="margin-bottom: 10px; padding-right: 5px;">
                            <i class="fa fa-facebook-official"></i>
                        </a>
                        <a class="btn blue lighten-3 col m4 s12 btn-share-link-tw btn-flat white-text"
                           data-href="<?php echo site_url("tendencia/articulo/$disenador/$modelo") ?>"
                           data-quote="<?php echo "#japy $vestido->nombre" ?>" style="margin-bottom: 10px">
                            <i class="fa fa-twitter"></i> Tweet
                        </a>
                        <button style="display: none" class="btn-flat bg-fb white-text btn-share-link"
                                data-quote="#clubnupcial"
                                data-href="<?php echo site_url("tendencia/articulo/$disenador/$modelo") ?>">
                            <i class="fa fa-facebook-official"></i>
                        </button>
                        <button style="display: none" class="btn-flat bg-tw  white-text btn-share-link-tw"
                                data-quote="#clubnupcial"
                                data-href="<?php echo site_url("tendencia/articulo/$disenador/$modelo") ?>">
                            <i class="fa fa-twitter"></i>
                        </button>
                    </div>
                    <div class="card preview-dress" style="margin-top: 54px">
                        <div class="card-content">
                            <h6>Tu busqueda</h6>
                            <div class="divider"></div>
                            <div class="center row arrows-dress">
                                <?php if ($posicion - 2 >= 0 && $vestidos[$posicion - 2]) { ?>
                                    <a href="<?php echo site_url("tendencia/articulo/$disenador/".str_replace(" ", "-", $vestidos[$posicion - 2]->nombre)) ?>">
                                        <i class="fa fa-chevron-left left dorado-2-text"></i>
                                    </a>
                                <?php } else { ?>
                                    <a class="disabled">
                                        <i class="fa fa-chevron-left left grey-text darken-7"></i>
                                    </a>
                                <?php } ?>
                                <?php if (count($vestidos) > $posicion && $vestidos[$posicion]) { ?>
                                    <a href="<?php echo site_url("tendencia/articulo/$disenador/".str_replace(" ", "-", $vestidos[$posicion]->nombre)) ?>">
                                        <i class="fa fa-chevron-right right dorado-2-text"></i>
                                    </a>
                                <?php } else { ?>
                                    <a class=" disabled">
                                        <i class="fa fa-chevron-right right grey-text darken-7"></i>
                                    </a>
                                <?php } ?>
                                <?php echo $posicion ?> de <?php echo $total ?> <?php echo $producto->nombre ?>
                            </div>
                            <div class="row">
                                <div class="col m6 s6">
                                    <?php if ($posicion - 2 >= 0 && $vestidos[$posicion - 2]) { ?>
                                        <a class="waves-effect"
                                           href="<?php echo site_url("tendencia/articulo/$disenador/".str_replace(" ", "-", $vestidos[$posicion - 2]->nombre)) ?>">
                                            <img class="responsive-img"
                                                 src="<?php echo base_url()."uploads/images/tendencia/".$vestidos[$posicion - 2]->image ?>">
                                        </a>
                                    <?php } else { ?>
                                        <a class="waves-effect"
                                           href="<?php echo site_url("tendencia/articulo/$disenador/".str_replace(" ", "-", $vestidos[$posicion - 1]->nombre)) ?>">
                                            <img class="responsive-img"
                                                 src="<?php echo base_url()."uploads/images/tendencia/".$vestidos[$posicion - 1]->image ?>">
                                        </a>
                                    <?php } ?>
                                </div>
                                <div class="col m6 s6">
                                    <?php if (count($vestidos) > $posicion && $vestidos[$posicion]) { ?>
                                        <a class="waves-effect"
                                           href="<?php echo site_url("tendencia/articulo/$disenador/".str_replace(" ", "-", $vestidos[$posicion]->nombre)) ?>">
                                            <img class="responsive-img"
                                                 src="<?php echo base_url()."uploads/images/tendencia/".$vestidos[$posicion]->image ?>">
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="card-action preview-dress-action">
                                <a style="font-size: 12px;  text-align: right;float: right" class="dorado-1-text"
                                   href="<?php echo site_url("tendencia/disenador/".str_replace(" ", "-", $producto->nombre)."/$disenador") ?>">
                                    <?php echo $producto->nombre." de ".$vestido->disenador ?> <i
                                            class="fa fa-chevron-right right"></i>
                                </a>
                            </div>

                        </div>
                    </div sty>
                </div>
                <div class="col m9 s12">
                    <section id="destacados">
                        <h5>Modelo: <?php echo $vestido->nombre ?> (<?php echo $vestido->disenador ?>)</h5>
                        <div class="divider"></div>
                        <div class="row" style="padding-top: 8px;">
                            <div class="col s12  m10 offset-m1">
                                <div id="dress" class="card horizontal">
                                    <div class="card-image img">
                                        <img style="max-height: initial" src="<?php echo base_url()."uploads/images/tendencia/".$vestido->image ?>" alt="">
                                    </div>
                                    <div class="card-stacked " style="padding: 3% 15px">
                                        <div class="card-content info-dress " style="padding: 0% 20px;">
                                            <p>
                                                <label>
                                                    Modelo:
                                                </label>
                                                <b><?php echo $vestido->nombre ?> (<?php echo $vestido->disenador ?>)</b>
                                            </p>
                                            <?php echo printDescriptor($vestido->temporada, "Temporda") ?>
                                            <?php echo printDescriptor($vestido->disenador, "Diseñador") ?>
                                            <?php echo printDescriptor($vestido->estilo, "Estilo") ?>
                                            <?php echo printDescriptor($vestido->corte, "Corte") ?>
                                            <?php echo printDescriptor($vestido->escote, "Escote") ?>
                                            <?php echo printDescriptor($vestido->largo, "Largo") ?>
                                            <?php echo printDescriptor($vestido->tipo, "Tipo") ?>
                                            <?php echo printDescriptor($vestido->categoria, "Categoria") ?>
                                            <?php echo printDescriptor($vestido->detalle, "Descripci&oacute;n") ?>
                                        </div>
                                    </div>
                                    <?php if ($this->checker->isLogin()): ?>
                                        <i class="material-icons  tooltipped favorite vestido red-text darken-5"
                                           data-vestido="<?php echo $vestido->id_vestido ?>" data-position="top" data-delay="5"
                                           data-tooltip="<?php echo $vestido->like ? "Eliminar" : "Guardar" ?>"><?php echo $vestido->like ? "favorite" : "favorite_border" ?></i>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>
                    </section>
                    <?php if ($this->checker->isLogin()) : ?>
                        <section id="comentar">
                            <h5>Deja tu comentario</h5>
                            <div class="divider"></div>
                            <div class="comentar">
                                <div class="card-panel" style="padding-bottom: 10px;padding-top: 5px">
                                    <div class="row" style="margin-bottom: 0px">
                                        <div class="col">
                                            <?php if ($this->session->userdata("usuario")) { ?>
                                                <img class="responsive-img circule-img border"
                                                     src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $this->session->userdata("usuario") ?>">
                                            <?php } else { ?>
                                                <img class="responsive-img circule-img border"
                                                     src="<?php echo $this->config->base_url() ?>dist/img/blog/perfil.png">
                                            <?php } ?>
                                        </div>
                                        <div class="input-field col s10 m10">
                                            <textarea id="text-comment" class="materialize-textarea " length="250"></textarea>
                                            <label for="icon_prefix2">Comentario</label>
                                        </div>
                                        <div class="col s12 m12">
                                            <a id="btn-comentar" data-vestido="<?php echo $vestido->id_vestido ?>"
                                               class="waves-effect waves-light btn dorado-2 right">
                                                <i class="material-icons right">send</i> Comentar
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php endif; ?>
                    <section class="comentarios">
                        <?php
                        if ($comentarios) {
                            foreach ($comentarios as $key => $c) {
                                ?>
                                <div id="<?php echo $c->id_vestido_comentario ?>"
                                     class="card-panel comentario  <?php echo $c->comentarios ? "has-comments" : "" ?>"
                                     style="padding-bottom: 10px;padding-top: 5px">
                                    <div class="row" style="margin-bottom: 5px">
                                        <div class="col">
                                            <img class="responsive-img circule-img"
                                                 src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $c->id_usuario ?>">
                                        </div>
                                        <div class="input-field col s10 m10">
                                            <h6><b><?php echo $c->usuario->nombre ?><?php echo $c->usuario->apellido ?></b>
                                                <small>
                                                    <?php echo relativeTimeFormat($c->fecha_creacion) ?></small>
                                            </h6>
                                            <p>
                                                <?php echo $c->mensaje ?>
                                            </p>
                                        </div>
                                        <?php if ($this->checker->isLogin()): ?>
                                            <div class="col s6">
                                                <a data-href="#comentar-<?php echo $c->id_vestido_comentario ?>"
                                                   class="btn-flat dorado-2-text waves-effect comment clickable">
                                                    Comentar
                                                </a>
                                            </div>
                                            <div class="col s6">
                                                <div class="container-icons-social">
                                                    <p style="float: right;">
                                                    <span class="icon-social comments total-comment"><text><?php echo $c->comentarios ?></text> 
                                                        <i class="fa fa-comment-o left tooltipped" data-position="top"
                                                           data-tooltip="Comentarios"></i>
                                                    </span>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php else: ?>
                                            <div class="col s12">
                                                <div class="container-icons-social">
                                                    <p style="float: right;">
                                                    <span class="icon-social comments total-comment"><text><?php echo $c->comentarios ?></text>
                                                        <i class="fa fa-comment-o left tooltipped" data-position="top"
                                                           data-tooltip="Comentarios"></i>
                                                    </span>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="commentarios-2 row">
                                        <div class="divider"></div>
                                        <div class="load-comentarios valign-wrapper <?php echo $c->comentarios ? "" : "hide" ?> "
                                             style="height: 50px">
                                            <div class="valign col s12 center">
                                                <i class="fa fa-spin fa-spinner fa-2x"></i>
                                            </div>
                                        </div>
                                        <?php if ($this->checker->isLogin()) { ?>
                                            <div id="comentar-<?php echo $c->id_vestido_comentario ?>" class="comment ">
                                                <div class="card-panel z-depth-0" style="padding-bottom: 10px;padding-top: 5px">
                                                    <div class="row" style="margin-bottom: 0px">
                                                        <div class="col">
                                                            <img class="responsive-img circule-img border"
                                                                 src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $this->session->userdata("usuario") ?>">
                                                        </div>
                                                        <div class="input-field col s10 m10">
                                                            <textarea class="materialize-textarea " length="250"></textarea>
                                                            <label for="icon_prefix2">Comentario</label>
                                                        </div>
                                                        <div class="col s12 m12">
                                                            <a class="waves-effect waves-light btn dorado-2 btn-comment"
                                                               data-vestido="<?php echo $vestido->id_vestido ?>"
                                                               data-comment='<?php echo $c->id_vestido_comentario ?>'
                                                               style="    float: right;"><i
                                                                        class="material-icons right">send</i>Comentar</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </section>
                </div>
            </div>
        </div>

        <script id="template-comentario" type="text/html">
            <div class="row">
                <div class="col">
                    <img class=" foto responsive-img circule-img"
                         src="<?php echo $this->config->base_url() ?>/dist/img/mujer.png">
                </div>
                <div class="input-field col s10 m10">
                    <h6><b class="nombre">Nombre de la persona</b>
                        <small class="fecha">fecha de publicacion</small>
                    </h6>
                    <p class="contenido">
                        Aqui el contenido del comentario
                    </p>
                </div>
                <div class="col s8">
                    <a class="btn-flat dorado-2-text waves-effect">
                        Comentar
                    </a>
                    <!--                    <a class="btn-flat dorado-2-text waves-effect">
                                            compartir
                                        </a>-->
                </div>
                <div class="col s4">
                    <div class="container-icons-social">
                        <p>
                            <span class="icon-social comments">14
                                <i class="fa fa-comment left tooltipped" data-position="top"
                                   data-tooltip="Comentarios"></i>
                            </span>
                            <!--<span class="icon-social comments">10 <i class="fa fa-share-alt left"></i></span>-->
                            <span class="icon-social comments "><text>0</text>
                                <a class="tooltipped clickable like-comment" data-comment="0" data-position="top"
                                   data-tooltip="Me gusta">
                                    <i class="fa fa-heart-o left"></i>
                                </a>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </script>
        <?php $this->view("general/footer");
        $this->view("principal/foot"); ?>
        <script src="<?php echo base_url() ?>dist/js/facebook.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/js/tendencia/vestidos.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                var login = "<?php echo site_url("cuenta") ?>";
                var fbc = new FBclub();
                fbc.initShareLinks();
                UIvestidos.login = login;
                vestidos.server = "<?php echo base_url() ?>";
                vestidos.initLike();
                $('#btn-comentar').on('click', UIvestidos.btn_principal_comment);
                $('a.comment').on('click', UIvestidos.btn_comment);
                $('.comment a.btn-comment').on('click', UIvestidos.btn_comment_comment);
                var comentarios = $('.comentario.has-comments');
                if (comentarios.length > 0) {
                    UIvestidos.load_comments(comentarios, 0);
                }

                $('.btn-share-link-tw').on('click', function() {
                    var $this = $(this);
                    var url = $this.data('href');
                    if (url) {
                        var titulo = $this.data('quote');
                        console.log('https://twitter.com/share?url=' + url + '&text=' + titulo);
                        window.open('https://twitter.com/share?url=' + url + '&text=' + encodeURIComponent(titulo), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                    }
                });

                $('#dress-save').on('click', function(e) {
                    e.preventDefault();
                    if (!$('#toast-container .toast').get(0)) {
                        Materialize.toast('Debes estar registrado para comentar<br> '
                            + " <a href='<?php echo site_url('Cuenta') ?>?callback=" + location.href + '\' class=\'dorado-2-text pull-right\'>Inicia Sesi&oacute;n <i class=\'material-icons right\'>keyboard_arrow_right</i></a>', 5000);
                        $('.toast').css({display: 'block'});
                    }
                });

                if (screen.width < 426) {
                    $('#dress').removeClass('horizontal');
                }
                else {
                    $('#dress').addClass('horizontal');
                }

            });
        </script>
    </body>
</html>

<?php

function printDescriptor($des, $label)
{
    if ($des) {
        return "<p><label>$label:</label> $des</p>";
    }

    return "";
}
