<html>
    <head>
        <?php $this->view("principal/head");
        $this->view("general/newheader"); ?>
        <style>
            #solicitar_info {
                display: none;
            }

            a.border {
                border: 1px solid #d0d0d0;
                box-shadow: none !important;
            }

        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("general/menu");
        }
        ?>
        <div class="body-container">
            <br>
            <?php
            $this->view("principal/breadcrumbs", array(
                "breadcrumbs" => array(
                    "Home"            => base_url(),
                    $producto->nombre => site_url("tendencia/index/".str_replace(" ", "-", $producto->nombre)),
                ),
            ));
            ?>
            <section id="label">
                <div class="card-panel">
                    <h5><?php echo $producto->nombre ?></h5>
                    <p>
                        <?php echo $producto->descripcion ?>
                    </p>
                </div>
            </section>
            <section id="menu">
                <?php $this->view("principal/vestidos/menu") ?>
            </section>
            <section id="catalogo">
                <h5>Catalogo de <?php echo $producto->nombre ?></h5>
                <div class="divider"></div>
                <div class="row">
                    <?php foreach ($catalogo as $key => $c) { ?>
                        <div class="col m20-porcent s10 ">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <a href="<?php echo site_url("tendencia/articulo/".str_replace(" ", "-", $c->disenador)."/".str_replace(" ", "-", $c->nombre)) ?>">
                                        <div class="image-product" style="background: url('data:<?php echo base_url()."uploads/images/tendencia/".$c->image ?>')
                                                no-repeat ;
                                                width: 100%;
                                                background-size: 100% 100%;
                                                "></div>
                                    </a>
                                    <i class="material-icons  tooltipped favorite vestido red-text darken-5"
                                       data-vestido="<?php echo $c->id_vestido ?>" data-position="top" data-delay="5"
                                       data-tooltip="<?php echo $c->like ? "Ya no me gusta" : "Me gusta" ?>"><?php echo $c->like ? "favorite" : "favorite_border" ?></i>
                                    <div class="social dorado-2-text">
                                        <span>
                                            <i class="material-icons">favorite</i> <?php echo $c->likes ?>
                                        </span>
                                        <span>
                                            <i class="material-icons">chat_bubble</i> <?php echo $c->comments ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="card-action center-align">
                                    <a href="<?php echo site_url("tendencia/articulo/".str_replace(" ", "-", $c->disenador)."/".str_replace(" ", "-", $c->nombre)) ?>" class="black-text darken-5">
                                        <?php echo $c->disenador ?> (<?php echo $c->temporada ?>)
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>
        </div>
        <?php $this->view("principal/footer");
        $this->view("general/newfooter"); ?>
        <script src="<?php echo base_url() ?>dist/js/tendencia/vestidos.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                var fbc = new FBclub();
                fbc.initShareLinks();
                vestidos.server = "<?php echo base_url() ?>";
                vestidos.initLike();
            });
        </script>
    </body>
</html>