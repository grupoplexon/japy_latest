<?php $producto_nombre = str_replace(" ", "-", $producto->nombre); ?>
<?php $dis = str_replace(" ", "-", $disenador); ?>
<html>
    <head>
        <?php $this->view("principal/head");
        $this->view("general/newheader"); ?>
        <style>
            #solicitar_info {
                display: none;
            }

            a.border {
                border: 1px solid #d0d0d0;
                box-shadow: none !important;
            }
        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("general/menu");
        }
        ?>
        <div class="body-container">
            <br>
            <section id="label">
            </section>
            <section id="menu">
                <?php $this->view("principal/vestidos/menu") ?>
            </section>
            <section id="breadcrumb">
                <?php
                $this->view("principal/breadcrumbs", array(
                    "breadcrumbs" => array(
                        "Home"                            => base_url(),
                        $producto->nombre                 => site_url("tendencia/index/$producto_nombre/$dis"),
                        "$producto->nombre de $disenador" => site_url("tendencia/disenador/$producto_nombre/$dis"),
                    ),
                ));
                ?>
            </section>
            <section id="catalogo">
                <h5><?php echo $producto->nombre ?> de <?php echo $disenador ?></h5>
                <div class="divider"></div>
                <div class="row">
                    <?php foreach ($vestidos as $key => $c) { ?>
                        <div class="col s10 offset-s1 m3 l2 ">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <a href="<?php echo site_url("tendencia/articulo/$dis/".str_replace(" ", "-", $c->nombre)) ?>">
                                        <div class="image-product" style="background: url('<?php  echo base_url()."uploads/images/tendencia/".$c->image ?>');no-repeat ;width: 100%;background-size: 100% 100%;"></div>
                                    </a>
                                    <i class="material-icons  tooltipped favorite vestido red-text darken-5"
                                       data-vestido="<?php echo $c->id_vestido ?>" data-position="top" data-delay="5"
                                       data-tooltip="<?php echo $c->like ? "Eliminar" : "Guardar" ?>"><?php echo $c->like ? "favorite" : "favorite_border" ?></i>
                                    <div class="social dorado-1-text">
                                        <span>
                                            <i class="material-icons">favorite</i> <?php echo $c->likes ?>
                                        </span>
                                        <span>
                                            <i class="material-icons">chat_bubble</i> <?php echo $c->comments ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="card-action center-align">
                                    <a href="<?php echo site_url("tendencia/articulo/$dis/".str_replace(" ", "-", $c->nombre)) ?>" class="black-text darken-5">
                                        <?php echo $c->disenador ?> (<?php echo $c->temporada ?>)
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <ul class="pagination">
                    <?php if ($pag == 1) { ?>
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="<?php echo site_url("tendencia/disenador/$producto_nombre/".str_replace(" ", "-", $disenador)) ?>?pagina=<?php echo($pag - 1); ?>"><i class="material-icons">chevron_left</i></a></li>
                    <?php } ?>
                    <?php $total_paginas = (int)(($total / 15) + 1); ?>
                    <?php for ($p = 1; $p <= $total_paginas; $p++) { ?>
                        <li class="waves-effect <?php echo($p == $pag ? "active" : "") ?>"><a href="#!"><?php echo $p ?></a></li>
                    <?php } ?>
                    <?php if ($pag == $total_paginas) { ?>
                        <li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                    <?php } else { ?>
                        <li class="waves-effect">
                            <a href="<?php echo site_url("tendencia/disenador/$producto_nombre/".str_replace(" ", "-", $disenador)) ?>?pagina=<?php echo($pag + 1); ?>">
                                <i class="material-icons">chevron_right</i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </section>
        </div>
        <?php $this->view("general/footer");
        $this->view("general/newfooter"); ?>
        <script src="<?php echo base_url() ?>dist/js/tendencia/vestidos.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                var fbc = new FBclub();
                fbc.initShareLinks();
                vestidos.server = "<?php echo base_url() ?>";
                vestidos.initLike();
            });
        </script>
    </body>
</html>