<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        <?php $this->view("principal/head");
              $this->view("general/newheader");?>
        <style>
            #solicitar_info{
                display: none;
            }
            a.border{
                border: 1px solid #d0d0d0;
                box-shadow: none !important;
            }

        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("general/menu");
        }
        ?>
        <div class="body-container">
            <br>
            <?php
            $this->view("principal/breadcrumbs", array("breadcrumbs" => array(
                    "Home" => base_url(),
                    $producto->nombre => site_url("tendencia/index/" . str_replace(" ", "-", $producto->nombre)),
        )))
            ?> 
            <section id="label">
                <div class="card-panel">
                    <h5><?php echo $producto->nombre ?></h5>
                    <p>
                        <?php echo $producto->descripcion ?>
                    </p>
                </div>
            </section>
            <section id="menu">
                <?php // $this->view("principal/vestidos/menu") ?>
            </section>
            <section id="destacados">
                <h5>Dise���adores de <?php echo $producto->nombre ?></h5>
                <div class="divider"></div>
                <div class="row">
                    <?php foreach ($destacados as $key => $dest) { ?>
                        <div class="col m20-porcent s12 ">
                            <a href="<?php echo site_url("tendencia/disenador/" . str_replace(" ", "-", $producto->nombre) . "/" . str_replace(" ", "-", $dest->disenador)) ?>">
                                <div class="card">
                                    <div class="card-image waves-effect waves-block waves-light">
                                        <img src="data:<?php echo base_url()."uploads/images/tendencia/".$dest->imagen ?>">
                                    </div>
                                    <div class="card-action">
                                        <text href="#" class="black-text darken-5">
                                        <?php echo $dest->disenador ?> (<?php echo $dest->temporada ?>)
                                        </text>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col s12">
                        <a href="<?php echo site_url("tendencia/disenadores/" . str_replace(" ", "-", $producto->nombre)) ?>"  class="btn-flat white waves-effect dorado-2-text right">
                            Ver todo los dise���adores
                            <i class="material-icons right">keyboard_arrow_right</i>
                        </a>
                    </div>
                </div>
            </section>
        </div>
        <?php $this->view("principal/footer");
              $this->view("general/newfooter");?>
        <script src="<?php echo base_url() ?>dist/js/tendencia/vestidos.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                var fbc = new FBclub();
                fbc.initShareLinks();
                vestidos.server = "<?php echo base_url() ?>";
                vestidos.initLike();
            });
        </script>
    </body>
</html>