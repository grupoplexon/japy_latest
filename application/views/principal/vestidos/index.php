<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        <?php $this->view("principal/head");
        $this->view("general/newheader"); ?>
        <style>
        .row .col.m2{
            height: 330px !important;
        }

        .card .card-action{
            height: 20% !important;
        }
        </style>
    </head>
    <body>
        <?php
        if ($this->checker->isProveedor()) {
            $this->view("proveedor/menu");
        } else {
            $this->view("general/menu");
        }
        ?>
        <div class="body-container">
            <br>
            <?php
            $this->view("principal/breadcrumbs", array(
                "breadcrumbs" => array(
                    "Home"            => base_url(),
                    $producto->nombre => site_url("tendencia/index/".str_replace(" ", "-", $producto->nombre)),
                ),
            ))
            ?>
            <section id="label">
                <div class="card-panel">
                    <h5><?php echo $producto->nombre ?></h5>
                    <p>
                        <?php echo $producto->descripcion ?>
                    </p>
                </div>
            </section>
            <section id="menu">
                <?php $this->view("principal/vestidos/menu") ?>
            </section>
            <section id="destacados">
                <div class="row section-title">
                    <div class="col s12 m6">
                        <h5>Diseñadores de <?php echo $producto->nombre ?> destacados</h5>
                    </div>
                    <div class="col s12 m6 section-title" style="margin-top: 12px">
                        <a href="<?php echo site_url("tendencia/disenadores/".str_replace(" ", "-", $producto->nombre)) ?>" class="btn-flat white waves-effect dorado-2-text right">
                            Ver todo los diseñdores
                            <i class="material-icons right">keyboard_arrow_right</i>
                        </a>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="row hide-on-small-only">
                    <?php foreach ($destacados as $key => $dest) { ?>
                        <div class="col s10 offset-s1 m2">
                            <a href="<?php echo site_url("tendencia/disenador/".str_replace(" ", "-", $producto->nombre)."/".str_replace(" ", "-", $dest->disenador)) ?>">
                                <div class="card">
                                    <div class="card-image waves-effect waves-block waves-light image-product" style="background: url('<?php echo base_url()."uploads/images/tendencia/".$dest->image ?>');no-repeat ;width: 100%;background-size: 100% 100%;">
                                    </div>
                                    <div class="card-action">
                                        <text href="#" class="black-text darken-5">
                                            <?php echo $dest->disenador ?> (<?php echo $dest->temporada ?>)
                                        </text>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <div class="center-slide hide-on-med-and-up">
                    <?php foreach ($destacados as $key => $dest) { ?>
                        <div class="col s10 offset-s1 m2">
                            <a href="<?php echo site_url("tendencia/disenador/".str_replace(" ", "-", $producto->nombre)."/".str_replace(" ", "-", $dest->disenador)) ?>">
                                <div class="card" style="padding: 20px;background: transparent;box-shadow: none">
                                    <div class="card-image waves-effect waves-block waves-light image-product" style="background: url('<?php echo base_url()."uploads/images/tendencia/".$dest->image ?>');no-repeat ;width: 100%;background-size: 100% 100%;">
                                    </div>
                                    <div class="card-action" style="background: white;box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);">
                                        <text href="#" class="black-text darken-5">
                                            <?php echo $dest->disenador ?> (<?php echo $dest->temporada ?>)
                                        </text>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </section>
            <section id="catalogo hide-on-small-only">
                <div class="row">
                    <div class="col s12 m6 section-title">
                        <h5>Catalogo de <?php echo $producto->nombre ?></h5>
                    </div>
                    <div class="col s12 m6 section-title" style="margin-top: 12px">
                        <a href="<?php echo site_url("tendencia/catalogo/".str_replace(" ", "-", $producto->nombre)) ?>" class="btn-flat white waves-effect dorado-2-text right">
                            Ver todo el catalogo
                            <i class="material-icons right">keyboard_arrow_right</i>
                        </a>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="row hide-on-small-only">
                    <?php foreach ($catalogo as $key => $c) { ?>
                        <div class="col s10 offset-s1 m2">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <a href="<?php echo site_url("tendencia/articulo/".str_replace(" ", "-", $c->disenador)."/".str_replace(" ", "-", $c->nombre)) ?>" class="image-product">
                                        <div class="image-product" style="background: url('<?php echo base_url()."uploads/images/tendencia/".$c->image ?>');no-repeat ;width: 100%;background-size: 100% 100%;"></div>
                                    </a>
                                    <i class="material-icons  tooltipped favorite vestido red-text darken-5"
                                       data-vestido="<?php echo $c->id_vestido ?>" data-position="top" data-delay="5"
                                       data-tooltip="<?php echo $c->like ? "Eliminar" : "Guardar" ?>"><?php echo $c->like ? "favorite" : "favorite_border" ?></i>
                                    <div class="social dorado-1-text">
                                    <span>
                                        <i class="material-icons">favorite</i> <?php echo $c->likes ?>
                                    </span>
                                        <span>
                                        <i class="material-icons">chat_bubble</i> <?php echo $c->comments ?>
                                    </span>
                                    </div>
                                </div>
                                <div class="card-action">
                                    <a href="<?php echo site_url("tendencia/articulo/".str_replace(" ", "-", $c->disenador)."/".str_replace(" ", "-", $c->nombre)) ?>" class="black-text darken-5" style="text-transform: inherit;">
                                        <?php echo $c->disenador ?> (<?php echo $c->temporada ?>)
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class=" center-slide hide-on-med-and-up">
                    <?php foreach ($catalogo as $key => $c) { ?>
                        <div class="card" style="padding: 20px;background: transparent;box-shadow: none">
                            <div class="card-image waves-effect waves-block waves-light" style="box-shadow: 2px 0px 2px 0 rgba(0, 0, 0, 0.2);">
                                <a href="<?php echo site_url("tendencia/articulo/".str_replace(" ", "-", $c->disenador)."/".str_replace(" ", "-", $c->nombre)) ?>"
                                   class="">
                                    <div class="image-product" style="'<?php echo base_url()."uploads/images/tendencia/".$c->image ?>');no-repeat ;width: 100%;background-size: 100% 100%;"></div>
                                </a>
                                <i class="material-icons  tooltipped favorite vestido red-text darken-5"
                                   data-vestido="<?php echo $c->id_vestido ?>" data-position="top" data-delay="5"
                                   data-tooltip="<?php echo $c->like ? "Eliminar" : "Guardar" ?>"><?php echo $c->like ? "favorite" : "favorite_border" ?></i>
                                <div class="social dorado-1-text">
                                <span>
                                    <i class="material-icons">favorite</i> <?php echo $c->likes ?>
                                </span>
                                    <span>
                                    <i class="material-icons">chat_bubble</i> <?php echo $c->comments ?>
                                </span>
                                </div>
                            </div>
                            <div class="card-action center-align" style="background: white;box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);">
                                <a href="<?php echo site_url("tendencia/articulo/".str_replace(" ", "-", $c->disenador)."/".str_replace(" ", "-", $c->nombre)) ?>" class="black-text darken-5" style="text-transform: inherit;">
                                    <?php echo $c->disenador ?> (<?php echo $c->temporada ?>)
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>
        </div>
        <?php
        $this->view("general/footer");
        $this->view("general/newfooter"); ?>
        <script src="<?php echo base_url() ?>dist/js/tendencia/vestidos.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                // var fbc = new FBclub();
                // fbc.initShareLinks();
                // vestidos.server = "<?php echo base_url() ?>";
                // vestidos.initLike();

            });
        </script>
    </body>
</html>