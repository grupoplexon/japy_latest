<?php

function has_nombre($nombre, $sortable) {
    foreach ($sortable as $key => $value) {
        if (is_object($value)) {
            if ($value->nombre == $nombre) {
                return TRUE;
            }
        } else {
            if ($value == $nombre) {
                return TRUE;
            }
        }
    }
    return false;
}

function generateQuery($param, $set = NULL) {
    $query = "";
    foreach ($param as $key => $value) {
        if ($set[0] != $key) {
            $query .= "$key=$value&";
        }
    }
    if ($set) {
        return $query . "" . $set[0] . "=" . $set[1];
    } else {
        return $query;
    }
}

function generateQueryWithout($param, $nombre) {
    $query = "";
    foreach ($param as $key => $value) {
        if ($nombre != $key) {
            $query .= "$key=$value&";
        }
    }
    return $query;
}
?>
<style>
    #solicitar_info{
        display: none;
    }
    a.border{
        border: 1px solid #d0d0d0;
        box-shadow: none !important;
    }
    .image-product{
        height: 250px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .center-slide a{
        padding: 0!important;
    }
    .details-slide-img{
        box-shadow: 2px 0px 2px 0 rgba(0, 0, 0, 0.2);
    }

    .details-slide-item{
        padding: 20px;
        background: transparent;
        box-shadow: none
    }

    .details-slide-action{
        background: white;
        box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    }

    @media only screen and (max-width: 425px) {
        .dropdown-content li a {
            font-size: 16px !important;
            text-align: center;
        }
        #dropdown-menu-temporada,#dropdown-menu-diseñador,#dropdown-menu-largo,
        #dropdown-menu-productos,#dropdown-menu-escote,#dropdown-menu-corte,
        #dropdown-menu-categoria{
            position: initial!important;
        }
        .center-slide{
            margin: 0!important;
        }
    }

    @media only screen and (max-width: 321px) {
        .center-slide{
            margin-bottom: 90px!important;
        }
    }
</style>
<div class="row" style="font-size: 12px;">
    <div class="col btn-block-on-small">
        <a class='dropdown-button btn dorado-2 btn-block-on-small' data-activates='dropdown-menu-productos' data-hover="true" data-beloworigin="true" >
            <?php echo $producto->nombre ?>
            <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <ul id='dropdown-menu-productos' class='dropdown-content' style="font-size: 12px;">
            <?php foreach (productos() as $key => $p) { ?>
                <li class="<?php echo ($producto->nombre) == ($p->nombre) ? "active" : "" ?>">
                    <a class="black-text darken-5 "  style="font-size: 12px;"  href="<?php echo site_url("tendencia/index/" . str_replace(" ", "-", $p->nombre)) ?>" >
                        <?php echo $p->nombre ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <?php foreach ($producto->sortable as $key => $value) { ?>
        <?php if ($value->nombre != "diseñador" && $value->nombre != "disenador" && $value->nombre != "temporada") { ?>
            <div class="col  btn-block-on-small">
                <a class='dropdown-button btn white dorado-2-text btn-block-on-small border' data-activates='dropdown-menu-<?php echo $value->nombre ?>' data-constrainwidth="false" data-hover="true" data-beloworigin="true" >
                    <?php echo array_key_exists($value->nombre, $param) ? $param[$value->nombre] : ucfirst($value->nombre); ?> 
                    <i class="material-icons right">keyboard_arrow_down</i>
                </a>
                <ul id='dropdown-menu-<?php echo $value->nombre ?>' class='dropdown-content'>
                    <li>
                        <a  href="<?php echo site_url("tendencia/catalogo/" . str_replace(" ", "-", $producto->nombre)) ?>?<?php echo generateQueryWithout($param, $value->nombre) ?>"  class="black-text title-item">
                            Todos los  <?php echo ucfirst($value->nombre) ?> 
                        </a>
                    </li>
                    <?php foreach ($value->valores as $key => $t) { ?>
                        <li>
                            <a  href="<?php echo site_url("tendencia/catalogo/" . str_replace(" ", "-", $producto->nombre)) ?>?<?php echo generateQuery($param, array($value->nombre, $t->nombre)) ?>"  class="black-text darken-5 item-filter"  style="font-size: 12px;">
                                <?php echo $t->nombre ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    <?php } ?>
    <div class="col  btn-block-on-small">
        <a class='dropdown-button btn white dorado-2-text btn-block-on-small border'data-activates='dropdown-menu-diseñador' data-constrainwidth="false" data-hover="true" data-beloworigin="true" >
            <?php echo array_key_exists("disenador", $param) ? str_replace("-", " ", $param["disenador"]) : "Diseñador" ?>
            <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <ul id='dropdown-menu-diseñador' class='dropdown-content' style="font-size: 12px;">
            <li>
                <a href="<?php echo site_url("tendencia/catalogo/" . str_replace(" ", "-", $producto->nombre)) ?>?<?php echo generateQueryWithout($param, "disenador") ?>" class="black-text">
                    Todos los diseñadores
                </a>
            </li>
            <?php foreach ($disenadores as $key => $d) { ?>
                <li>
                    <a href="<?php echo site_url("tendencia/catalogo/" . str_replace(" ", "-", $producto->nombre)) ?>?<?php echo generateQuery($param, array("disenador", str_replace(" ", "-", $d->disenador))) ?>" class="black-text darken-5"  style="font-size: 12px;">
                        <?php echo $d->disenador ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="col  btn-block-on-small">
        <a class='dropdown-button btn white dorado-2-text btn-block-on-small border' data-activates='dropdown-menu-temporada' data-constrainwidth="false" data-hover="true" data-beloworigin="true" >
            <?php echo array_key_exists("temporada", $param) ? $param["temporada"] : "Temporada"; ?>
            <i class="material-icons right">keyboard_arrow_down</i>
        </a>
        <ul id='dropdown-menu-temporada' class='dropdown-content'>
            <li>
                <a href="<?php echo site_url("tendencia/catalogo/" . str_replace(" ", "-", $producto->nombre)) ?>?<?php echo generateQueryWithout($param, "temporada") ?>"  class="black-text">
                    Todos las temporadas
                </a>
            </li>
            <?php foreach ($temporadas as $key => $t) { ?>
                <li>
                    <a href="<?php echo site_url("tendencia/catalogo/" . str_replace(" ", "-", $producto->nombre)) ?>?<?php echo generateQuery($param, array("temporada", $t->temporada)) ?>" class="black-text darken-5"  style="font-size: 12px;">
                        <?php echo $t->temporada ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>