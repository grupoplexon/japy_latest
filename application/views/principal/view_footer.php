<section style="display:none;">
    <div style="background: url(<?php echo base_url() ?>dist/img/textura_footer2.png); ">
        <div class="container">
            <div class="seccion-header">
                <div style="padding:22px">&nbsp;</div>
                <div class="row">
                    <div class="col s4">
                        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/vineta2.png" alt=""/>
                    </div>
                    <div class="col s4">
                        <p class="gris-1-text seccion-title" style="font-size: 13px;"><b>EMPRESAS ESPECIALIZADAS</b></p>
                    </div>
                    <div class="col s4">
                        <img class="responsive-img" src="<?php echo base_url() ?>dist/img/vineta3.png" alt=""/>
                    </div>
                </div>

                <div style="margin:8px">&nbsp;</div>
            </div>
            <div class="seccion-body">
                <div class="row nowrap" style="background:white; border: 1px solid rgba(0, 0, 0, 0.37);">
                    <div class="col m3 s6" style="border-right: 1px solid rgba(0, 0, 0, 0.37);">
                        <div class="col m12" style="padding: 0px;">
                            <img class="responsive-img" src="<?php echo base_url() ?>dist/img/empresas1.png" alt=""/>
                        </div>
                        <div class="col m12" style="min-height: 215px;padding: 20px;">
                            <h6 class="dorado-1-text"><b>PROVEEDORES</b></h6>
                            <ul class="lista">
                                <li>Invitaciones de boda</li>
                                <li>Recuerdos para boda</li>
                                <li>Fot&oacute;grafos de bodas</li>
                                <li>Video para bodas</li>
                                <li>M&uacute;sica para bodas</li>
                                <li>Autos para bodas</li>
                                <li>Renta de sillas y mesas</li>
                                <li>Carpas para bodas</li>
                                <li>Animaci&oacute;n Bodas</li>
                                <li>Arreglos florales boda</li>
                                <li>Mesa de regalos</li>
                                <li>Organizadores de bodas</li>
                                <li>Luna de miel</li>
                                <li>Decoraci&oacute;n para bodas</li>
                                <li>Pasteles para boda</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col  m3 s6" style="border-right: 1px solid rgba(0, 0, 0, 0.37); min-height: 538px;">
                        <div class="col m12" style="min-height: 268px;padding: 20px;">
                            <h6 class="dorado-1-text"><b>BANQUETES</b></h6>
                            <ul class="lista">
                                <li>Haciendas para bodas</li>
                                <li>Hoteles para bodas</li>
                                <li>Restaurantes para bodas</li>
                                <li>Salones para bodas</li>
                                <li>Catering para bodas</li>
                                <li>Bodas en la playa</li>
                                <li>Jardines para bodas</li>
                                <li>Hoteles Barcel&oacute;</li>
                                <li>Iberostar</li>
                                <li>Hoteles Bel Air</li>
                            </ul>
                        </div>
                        <div class="col m12" style="padding: 0px;">
                            <img class="responsive-img" src="<?php echo base_url() ?>dist/img/empresas3.png" alt=""/>
                        </div>

                    </div>
                    <div class="col  m3 s6" style="border-right: 1px solid rgba(0, 0, 0, 0.37);">
                        <div class="col m12" style="min-height: 268px;  padding: 0px;  ">
                            <img class="responsive-img" src="<?php echo base_url() ?>dist/img/empresas2.png" alt=""/>
                        </div>
                        <div class="col m12" style="padding: 20px;">
                            <h6 class="dorado-1-text"><b>NOVIAS</b></h6>
                            <ul class="lista">
                                <li>Tiendas de novia</li>
                                <li>Complementos para novias</li>
                                <li>Joyer&iacute;a</li>
                                <li>Belleza Novias</li>
                                <li>Vestidos de fiesta</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col  m3 s6">
                        <div class="col m12" style="min-height: 268px;padding: 20px;">
                            <h6 class="dorado-1-text"><b>NOVIOS</b></h6>
                            <ul class="lista">
                                <li>Trajes de novio</li>
                                <li>Accesorios novio</li>
                                <li>Cuidado masculino</li>
                            </ul>

                        </div>
                        <div class="col m12" style="padding: 0px;">
                            <img class="responsive-img" src="<?php echo base_url() ?>dist/img/empresas4.png" alt=""/>
                        </div>
                    </div>
                </div>

                <div style="padding: 40px">&nbsp;</div>
            </div>

        </div>
    </div>
</section>
<?php $home_section = getFooter(); ?>
<footer class="page-footer primary-background">
    <!--<img class="vineta" src="<?php echo base_url() ?>/dist/img/vineta.png" alt=""/>-->
    <div class="footer-copyright primary-background" style="height: 70px;"> <!-- dorado-1 -->
        <div class="row">
            <div class="col m4 offset-m1 white-text s4">© Derechos Reservados 2018.</div>
            <div class="col m3 offset-m3 s8">
                <img class="" src="<?php echo base_url() ?>dist/img/clubnupcial_logotype.png"
                     style="padding: 6px;z-index: 9;position: relative;height: 70px;    width: auto;"
                     alt="Japy"/>
            </div>
        </div>
    </div>
    <br>
    <div class="row" style="margin-bottom: 0px">
        <div class="col s1 offset-s1 hide-on-med-and-down">
            <small class="white-text"><b>HOME</b></small>
            <ul>
                <?php foreach ($home_section as $key => $value) { ?>
                    <?php if ($value->tipo == "HOME") { ?>
                        <li>
                            <a class="grey-text text-lighten-3"
                               href="<?php echo empty($value->url) ? '#!' : $value->url; ?>">
                                <small><?php echo $value->titulo ?></small>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <div class="col s1 hide-on-med-and-down">
            <small class="white-text"><b>EVENTOS</b></small>
            <ul>
                <?php foreach ($home_section as $key => $value) { ?>
                    <?php if ($value->tipo == "EVENTOS") { ?>
                        <li>
                            <a class="grey-text text-lighten-3"
                               href="<?php echo empty($value->url) ? '#!' : $value->url; ?>">
                                <small><?php echo $value->titulo ?></small>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <div class="col s1 hide-on-med-and-down">
            <small class="white-text"><b>PROVEEDORES</b></small>
            <ul>
                <?php foreach ($home_section as $key => $value) { ?>
                    <?php if ($value->tipo == "PROVEEDORES") { ?>
                        <li>
                            <a class="grey-text text-lighten-3"
                               href="<?php echo empty($value->url) ? '#!' : $value->url; ?>">
                                <small><?php echo $value->titulo ?></small>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <div class="col s1 hide-on-med-and-down">
            <small class="white-text"><b>EMPRESAS</b></small>
            <ul>
                <?php foreach ($home_section as $key => $value) { ?>
                    <?php if ($value->tipo == "EMPRESAS") { ?>
                        <li>
                            <a class="grey-text text-lighten-3"
                               href="<?php echo empty($value->url) ? '#!' : $value->url; ?>">
                                <small><?php echo $value->titulo ?></small>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <div class="col s1 hide-on-med-and-down">
            <small class="white-text"><b>BLOG</b></small>
        </div>
        <div class="col s1 hide-on-med-and-down">
            <small class="white-text"><b>GALER&Iacute;A</b></small>
        </div>
        <div class="col s1 hide-on-med-and-down">
            <small class="white-text"><b>MI BODA</b></small>
            <ul>
                <?php foreach ($home_section as $key => $value) { ?>
                    <?php if ($value->tipo == "MI BODA") { ?>
                        <li>
                            <a class="grey-text text-lighten-3"
                               href="<?php echo empty($value->url) ? '#!' : $value->url; ?>">
                                <small><?php echo $value->titulo ?></small>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <div class="col s12 m3">
            <div class="row">
                <div class="col m5 s6">
                    <small class="white-text"><b>REDES SOCIALES</b></small>
                    <ul>
                        <?php foreach ($home_section as $key => $value) { ?>
                            <?php if ($value->tipo == "REDES SOCIALES") { ?>
                                <li>
                                    <a class="white-text" href="<?php echo empty($value->url) ? '#!' : $value->url; ?>"
                                       target="_blank">
                                        <small>
                                            <span class="fa-stack fa-lg">
                                                <i class="fa fa-square fa-stack-2x white-text"></i>
                                                <i class="fa fa-<?php echo $value->icono ?> fa-stack-1x grey-text darken-5"></i>
                                            </span> <?php echo $value->titulo ?>
                                        </small>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col m5 s6">
                    <small class="white-text"><b>DESCARGA</b></small>
                    <ul>
                        <li>
                            <img src="<?php echo base_url() ?>/dist/img/google_play.png" alt=""/>
                        </li>
                        <li>
                            <img src="<?php echo base_url() ?>/dist/img/app_store.png" alt=""/>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>