<style>
    #promo-modal {
        background-image: url(<?php echo base_url()."dist/img/pop-up-mobile.png" ?>);
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 50% 50%;
        background-color: transparent;
        box-shadow: none;
        max-width: 100%;
        height: 100%;
        top: 15% !important;
    }

    #promo-modal a {
        display: block;
        width: 100%;
        height: 100%;
    }

    @media only screen and (min-width: 768px) {
        #promo-modal {
            background-image: url(<?php echo base_url()."dist/img/pop-up.png" ?>);
            width: 800px;
            height: 600px;
            max-width: 100%;
        }
    }

</style>
<div id="promo-modal" class="modal">
    <a href="<?php echo base_url()."registro" ?>"></a>
</div>
<input type="hidden" id="isAuthenticated" value="<?php echo $this->session->userdata("id_usuario") ?>">
<script>
    $(document).ready(function () {
        const isAuthenticated = $("#isAuthenticated").val();
        if (!isAuthenticated) {
            //$("#promo-modal").modal("open");
        }
    });
</script>