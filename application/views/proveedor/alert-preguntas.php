<div class="row">
    <div class="col s12">
        <div class="card alert-preguntas blue lighten-5">
            <div class="card-title">
                <a class="clickable pull-right black-text exit" style="padding: 10px" onclick="$('.alert-preguntas').hide(600);"><i class="fa fa-times" aria-hidden="true"></i></a> 
            </div>
            <div class="card-content">
                <h4 style="">&iexcl;No lo pienses m&aacute;s!</h4>
                <span>
                    Para mejorar la posibilidad de <b>aparecer en la busqueda de proveedores</b> te sugerimos llenar las preguntas frecuentes, mientras m&aacute;s llenes
                    mejores ser&aacute;n tus posibilidades.
                </span>
            </div>
            <div class="card-action">
                <a href="<?php echo base_url() ?>index.php/proveedor/escaparate/faq" class="btn btn-flat right secondary-text" style="margin: 0; margin-top: -20px; min-height: 36px; height: auto">Preguntas Frecuentes</a>
            </div>
        </div>
    </div>
</div>

