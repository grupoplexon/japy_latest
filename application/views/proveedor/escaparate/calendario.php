<?php

function hasDia($cal, $date) {
    if ($cal) {
        foreach ($cal as $key => $c) {
            if ($c->dia == date("Y-m-d", $date)) {
                $k = $key;
                break;
            }
        }
        if (isset($k)) {
            $e = $cal[$k]->estado == 1;
            unset($cal[$k]);
            return $e;
        }
    }
    return false;
}
?>


<html>
    <head>
        <?php $this->view("proveedor/header") ?>
        <style>
            .row-cal{
                width: 100%;
                padding: 0px;
                margin: 0px;
                display: flex;
            }
            .row-cal .dia{
                background: #C8E6C9;
                padding: 0px;
                border: 1px solid white;
                color: #1B5E20;
                border-radius: 4px;
                cursor: pointer;
                transition: 0.3s;
            }
            .row-cal .cell-cal{ 
                width: 14.285714%;
                float: left;
                text-align: center;
                font-size: 12px;
            }
            .row-cal .dia:hover{ 
                background: gray;
                color: white;
            }
            .row-cal .dia.ocupado{ 
                background: #FF5722;
                color: white;
            }

            .label {
                display: block;
                float: left;
                margin-left: 5px;
                padding: 2px;
                border: 1px solid lightgray;
                border-radius: 1px;
            }

            .row-cal .cell-cal.disabled{
                color: gray;
                background: white;
            }
            .cal{
                padding: 0px;
                min-height: 165px;
            }
            .month{
                text-align: center;
                padding: 2px;
                font-size: 14px;
            }
        </style>
    </head>
    <body >
        <?php $this->view("proveedor/menu") ?>
        <div class="body-container">
            <div class="row">
                <?php $this->view("proveedor/escaparate/menu") ?>
                <div class="col s12 m8 l9">
                    <?php $this->view("proveedor/mensajes") ?>
                    <section class="intro-section">
                        <h5>Calendario de Bodas</h5>
                        <div class="card-panel blue lighten-5">
                            Informe a los novi@s de los d&iacute;as que ya tiene comprometidos y en los que no podr&aacute; ofrecer sus servicios. <br>
                            De este manera recibir&aacute; solicitudes de mejor calidad y aumentar&aacute; sus posibilidades de venta.
                        </div>
                    </section>
                    <section class="row">
                        <div class="pull-left row row-cal" style="margin-bottom: 10px">
                            <div class="label">
                                <span class="cell-cal dia ocupado" style="    width: 20px;">x</span> No disponible
                            </div>
                            <div class="label">
                                <span class="cell-cal dia" style="    width: 20px;">x</span> Disponible
                            </div>
                        </div>
                        <div class="pull-right btn-block-on-small">
                            <a class="btn col s4 mInitial dorado-2" href="<?php echo base_url() ?>index.php/proveedor/escaparate/calendario?year=<?php echo $year - 1 ?>">
                                <i class="fa fa-chevron-left left"></i> <?php echo $year - 1 ?>
                            </a>
                            <button class="btn col s4 mInitial grey  lighten-4 z-depth-0 black-text">
                                <?php echo $year ?>
                            </button>
                            <a class="btn col s4 mInitial dorado-2" href="<?php echo base_url() ?>index.php/proveedor/escaparate/calendario?year=<?php echo $year + 1 ?>">
                                <i class="fa fa-chevron-right right"></i> <?php echo $year + 1 ?>
                            </a>
                        </div>
                    </section>
                    <section id="section-calendario" class="row">
                        <?php
                        $meses = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"];
                        foreach ($meses as $key => $value) {
                            if (isset($end)) {
                                $last_month_end = $end - (6 - $w2);
                            } else {
                                $last_month_end = 31;
                            }
                            $mes = $key + 1;
                            $fecha = strtotime("$year-" . $mes . "-01");
                            $t = date("t", $fecha);
                            $w = date("w", $fecha);
                            $w2 = date("w", strtotime("$year-" . $mes . "-$t"));
                            $end = $t + (6 - $w2);
                            ?>
                            <div class="col s12 m4 l3">
                                <div class="card-panel cal">
                                    <div class="month">
                                        <?php echo $value ?> <?php echo $year ?> 
                                    </div>
                                    <div class="row-cal header">
                                        <div class="cell-cal">
                                            Do
                                        </div>
                                        <div class="cell-cal">
                                            Lun
                                        </div>
                                        <div class="cell-cal">
                                            Ma
                                        </div>
                                        <div class="cell-cal">
                                            Mi
                                        </div>
                                        <div class="cell-cal">
                                            Ju
                                        </div>
                                        <div class="cell-cal">
                                            Vi
                                        </div>
                                        <div class="cell-cal">
                                            Sa
                                        </div>
                                    </div>
                                    <div>
                                        <?php
                                        $c = 7;
                                        $n = 1;
                                        for ($start = 0 - $w; $start < $end; $start++) {
                                            $c++;
                                            if ($c >= 7) {
                                                $c = 0;
                                                ?>
                                            </div>
                                            <div class="row-cal">
                                            <?php } ?>
                                            <div  data-date="<?php echo strtotime("$year-" . $mes . "-" . ($start + 1 )) ?>"
                                                  class="cell-cal <?php echo $start < 0 || $t <= $start ? "disabled" : " dia " ?>
                                                  <?php echo hasDia($calendario, strtotime("$year-" . $mes . "-" . ($start + 1 ))) ? "ocupado" : "" ?> "><?php
                                                  if ($start < 0) {
                                                      echo $last_month_end + ($start + 1);
                                                  } else {
                                                      if ($t <= $start) {
                                                          echo $n++;
                                                      } else {
                                                          echo $start + 1;
                                                      }
                                                  }
                                                  ?></div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </section>
                </div>
            </div>
        </div>
        <?php $this->view("proveedor/footer") ?>
        <script>
            $(document).ready(function () {
                $(".row-cal .dia").on("click", function (elem) {
                    var $dia = $(elem.currentTarget);
                    var d = {
                        "dia": $dia.data("date"),
                        "ocupado": !$dia.hasClass("ocupado")
                    };
                    change_dia(d, function () {
                        $dia.toggleClass("ocupado");
                    });
                });


                function change_dia(data, fn) {
                    $.ajax({
                        url: "<?php echo base_url() ?>index.php/proveedor/escaparate/calendario",
                        method: "POST",
                        data: data,
                        success: function (resp) {
                            fn(resp);
                        },
                        error: function () {
                            resp(null);
                        }
                    });
                }


            });
        </script>
    </body>

</html>
