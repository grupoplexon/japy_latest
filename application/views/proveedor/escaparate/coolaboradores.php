<html>
<head>
    <?php $this->view("proveedor/header") ?>
    <style>
        .ui-widget-content {
            border: 1px solid #E0E0E0;
            color: #222222;
            background-color: white;
        }

        .ui-widget {
            font-family: Verdana, Arial, sans-serif;
            font-size: 1.1em;
            box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .ui-menu {
            list-style: none;
            padding: 0;
            margin: 0;
            display: block;
            outline: none;
        }

        .ui-menu .ui-menu-item {
            width: 100%;
            position: relative;
            margin: 0;
            padding: 3px 1em 3px .4em;
            cursor: pointer;
            min-height: 0;
        }

        .ui-menu .ui-menu-item:hover {
            background-color: #F5E6DF;
        }

        .ui-helper-hidden-accessible {
            display: none;
        }

        .collection .collection-item.avatar .title {
            font-size: 16px;
            margin-left: 25px;
        }

        .collection .collection-item.avatar p {
            margin: 0;
            margin-left: 25px;
        }

    </style>
</head>
<body>
    <?php $this->view("proveedor/menu") ?>
    <div class="body-container">
        <div class="row">
            <?php $this->view("proveedor/escaparate/menu") ?>
            <div class="col s12 m8 l9">
                <?php $this->view("proveedor/mensajes") ?>
                <section class="intro-section">
                    <h5>Empresas Colaboradoras</h5>
                    <div class="card-panel blue lighten-5">
                        <b>Indique las empresas con las que acostumbra a trabajar</b>
                        para que los novios As&iacute; conseguir&aacute; ampliar el abanico de clientes
                        <b>y recibir&aacute; mejores solicitudes.</b>
                    </div>
                </section>
                <section>
                    <div class="card-panel">
                        <div class="row">
                            <form class="form-validate"
                                  action="<?php echo base_url() ?>index.php/proveedor/escaparate/colaboradores"
                                  method="POST">
                                <div class="col s12 m8">
                                    <input id="tags" class="form-control validate[required]" name="datos">
                                    <input id="empresa" type="hidden" name="id" class="form-control">
                                </div>
                                <div class="col s12 m4">
                                    <button class="btn dorado-2" type="submit">
                                        Guardar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <section>
                    <ul class="collection">
                        <?php foreach ($collaborators as $key => $collaborator) : ?>
                            <li class="collection-item avatar">
                                <img src="<?php echo($collaborator->imageLogo->first() != null ?
                                    'data:' . $collaborator->imageLogo()->first()->mime . ';base64, ' . base64_encode($collaborator->imageLogo()->first()->data) :
                                    base_url() . 'dist/img/blog/perfil.png') ?>"
                                     class="circle">
                                <span class="title"><?php echo $collaborator->nombre ?></span>
                                <p><?php echo $collaborator->nombre_tipo_proveedor ?><br>
                                    <?php echo $collaborator->localizacion_estado ?>
                                </p>
                                <a class="secondary-content delete-colaborador clickable"
                                   data-id="<?php echo $collaborator->id_proveedor ?>"><i
                                            class="material-icons">delete</i></a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <!--                    <ul class="collection"> -->
                    <!--                        --><?php //foreach ($colaboradores as $key => $value) { ?>
                    <!--                            <li class="collection-item avatar">-->
                    <!--                                                    <img src="data:-->
                    <!--                    --><?php //echo $value->logo->mime ?><!--;base64,-->
                    <!--                    --><?php //echo base64_encode($value->logo->data) ?><!--"-->
                    <!--                                     alt="" class="circle">-->
                    <!--                                <span class="title">-->
                    <?php //echo $value->nombre ?><!--</span>-->
                    <!--                                <p>--><?php //echo $value->nombre_tipo_proveedor ?><!--<br>-->
                    <!--                                    --><?php //echo $value->localizacion_estado ?>
                    <!--                                </p>-->
                    <!--                                <a class="secondary-content delete-colaborador clickable"-->
                    <!--                                   data-id="--><?php //echo $value->id_colaborador ?><!--"><i-->
                    <!--                                            class="material-icons">delete</i></a>-->
                    <!--                            </li>-->
                    <!--                        --><?php //} ?>
                    <!--                    </ul>-->
                </section>
            </div>
        </div>
    </div>
    <?php $this->view("proveedor/footer") ?>
    <script>
        $(document).ready(function () {
            var server = "<?php echo base_url() ?>";
            var auto = $("#tags").autocomplete({
                source: server + "index.php/proveedor/escaparate/colaboradores",
                minLength: 0,
                select: function (event, ui) {
                    $(this).val(ui.item.nombre + " | " + ui.item.tipo + " | " + ui.item.estado);
                    $("#empresa").val(ui.item.id);
                    return false;
                }
            });

            auto.autocomplete("instance")._resizeMenu = function (ul, item) {
                console.log("_resizeMenu", this.element.width());
                $(".ui-autocomplete ").get(0).style.width = this.element.width() + "px"
            };

            auto.autocomplete("instance")._renderItem = function (ul, item) {
                var term = $("#tags").val();
                var n = par(item.nombre, term);
                var e = par(item.estado, term);
                var t = par(item.tipo, term);
                return $("<li>")
                    .append(n + " | " + t + " | " + e)
                    .appendTo(ul);
            };

            $(".delete-colaborador").click(function (evt) {
                var self = $(this);
                var c = self.data("id");
                if (c) {
                    if (confirm("¿Estas seguro de eliminar el colaborador?")) {
                        $.ajax({
                            url: server + "index.php/proveedor/escaparate/colaboradores",
                            method: "POST",
                            data: {
                                action: "delete",
                                colaborador: c,
                            },
                            success: function () {
                                self.parent().remove();
                            }, error: function () {

                            }
                        });
                    }
                }
            });

            function par(n, term) {
                var i = n.indexOf(term);
                if (i === 0) {
                    return "<b>" + term + "</b>" + n.substring(term.length, n.length);
                } else {
                    return n;
                }
            }
        })
    </script>
</body>

</html>
