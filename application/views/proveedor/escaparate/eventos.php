<html>
<head>
    <?php $this->view("proveedor/header") ?>
    <style>
        .collection .collection-item.avatar .title {
            font-size: 16px;
            margin-left: 25px;
        }

        .collection .collection-item.avatar p {
            margin: 0;
            margin-left: 25px;
        }

        section#section-crear {
            height: inherit;
            transition: 0.8s;
            overflow: hidden;
        }

        section#section-crear.active {
            height: 0px;
            margin: 0px;
            padding: 0px;
        }

        .dz-image img {
            width: 200px;
            height: auto;
            margin: 0px auto;
            position: relative;
            display: block;
        }

        .collection .evento .collection-item.active {
            background-color: #9e9e9e !important;
            color: #FFFFFF;
        }

        .evento .collapsible-body p {
            margin: 0;
            padding: 2px 10px;
        }
        @media only screen and (max-width: 425px) {
            .space-top{
                margin-top: 20px !important;
            }

        }

    </style>
</head>
<body>
    <?php $this->view("proveedor/menu") ?>
    <div class="body-container">
        <div class="row">
            <?php $this->view("proveedor/escaparate/menu") ?>
            <div class="col s12 m8 l9">
                <?php $this->view("proveedor/mensajes") ?>
                <section class="intro-section">
                    <h5>Eventos</h5>
                    <div class="card-panel blue lighten-5">
                        Promocione y actualice todos los eventos relacionados con su empresa en su escaparate.
                        <b>Los eventos le ayudar&aacute;n a promocionar su negocio</b> y captar un mayor
                        inter&eacute;s de los novios cuando visiten su perfil de empresa.
                    </div>
                </section>
                <section class="row">
                    <button id="toggle-crear" class="col s12 m3 offset-m9 btn dorado-2 pull-right">
                        Crear evento
                    </button>
                </section>
                <section id="section-crear" class="card-panel active ">
                    <form class="form-validate" method="POST"
                          action="<?php echo base_url() ?>index.php/proveedor/escaparate/eventos/nuevo">
                        <h5>Crear eventos</h5>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col s12 m7">
                                <p>
                                    <label>Nombre del evento</label>
                                    <input name="nombre" class="form-control validate[required]">
                                </p>
                                <p>
                                    <label>Tipo de evento</label>
                                    <select name="tipo" class="form-control  validate[required]">
                                        <option value="">-- Seleccionar --</option>
                                        <option value="3">Expo boda</option>
                                        <option value="4">Demostraci&oacute;n</option>
                                        <option value="5">Degustaci&oacute;n</option>
                                        <option value="6">Inauguraci&oacute;n</option>
                                        <option value="7">Concurso</option>
                                        <option value="8">Desfile</option>
                                        <option value="9">Concierto</option>
                                        <option value="10">Curso</option>
                                        <option value="11">Exposici&oacute;n</option>
                                        <option value="12">Sorteo</option>
                                    </select>
                                </p>
                                <div class="row" style="    margin-bottom: 0px;    margin: 0px;    padding: 0px;">
                                    <p class="col s6" style="    margin: 0px;">
                                        <label>Empieza el d&iacute;a</label>
                                        <input name="fecha_inicio" class="form-control datepicker ">
                                    </p>
                                    <p class="col s6" style="    margin: 0px;">
                                        <label>a las </label>
                                        <select name="hora_inicio" class="form-control">
                                            <option value="23:59" selected="selected">-- Todo el d&iacute;a --</option>
                                            <option value="0:00">0:00</option>
                                            <option value="1:00">1:00</option>
                                            <option value="2:00">2:00</option>
                                            <option value="3:00">3:00</option>
                                            <option value="4:00">4:00</option>
                                            <option value="5:00">5:00</option>
                                            <option value="6:00">6:00</option>
                                            <option value="7:00">7:00</option>
                                            <option value="8:00">8:00</option>
                                            <option value="9:00">9:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                            <option value="12:00">12:00</option>
                                            <option value="13:00">13:00</option>
                                            <option value="14:00">14:00</option>
                                            <option value="15:00">15:00</option>
                                            <option value="16:00">16:00</option>
                                            <option value="17:00">17:00</option>
                                            <option value="18:00">18:00</option>
                                            <option value="19:00">19:00</option>
                                            <option value="20:00">20:00</option>
                                            <option value="21:00">21:00</option>
                                            <option value="22:00">22:00</option>
                                            <option value="23:00">23:00</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="row" style="    margin-bottom: 0px;    margin: 0px;    padding: 0px;">
                                    <p class="col s6" style="    margin: 0px;">
                                        <label>Y acaba el d&iacute;a</label>
                                        <input name="fecha_termino" class="form-control datepicker">
                                    </p>
                                    <p class="col s6" style="    margin: 0px;">
                                        <label>a las </label>
                                        <select name="hora_termino" class="form-control">
                                            <option value="23:59" selected="selected">-- Todo el d&iacute;a --</option>
                                            <option value="0:00">0:00</option>
                                            <option value="1:00">1:00</option>
                                            <option value="2:00">2:00</option>
                                            <option value="3:00">3:00</option>
                                            <option value="4:00">4:00</option>
                                            <option value="5:00">5:00</option>
                                            <option value="6:00">6:00</option>
                                            <option value="7:00">7:00</option>
                                            <option value="8:00">8:00</option>
                                            <option value="9:00">9:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                            <option value="12:00">12:00</option>
                                            <option value="13:00">13:00</option>
                                            <option value="14:00">14:00</option>
                                            <option value="15:00">15:00</option>
                                            <option value="16:00">16:00</option>
                                            <option value="17:00">17:00</option>
                                            <option value="18:00">18:00</option>
                                            <option value="19:00">19:00</option>
                                            <option value="20:00">20:00</option>
                                            <option value="21:00">21:00</option>
                                            <option value="22:00">22:00</option>
                                            <option value="23:00">23:00</option>
                                        </select>
                                    </p>
                                </div>
                                <p>
                                    <label>Descripci&oacute;n del Eventos</label>
                                    <textarea class="editable" name="descripcion"></textarea>
                                </p>
                            </div>
                            <div class="col s12 m5">
                                <select name="pais" class="countries hidden "
                                        data-default="<?php echo $empresa->localizacion_pais ?>">
                                    <option value="" disabled selected>-- Pa&iacute;s --</option>
                                </select>
                                <p>
                                    <label>Estado</label>
                                    <select name="estado" class="form-control states "
                                            data-default="<?php echo $empresa->localizacion_estado ?>"></select>
                                </p>
                                <p>
                                    <label>Poblaci&oacute;n</label>
                                    <select name="poblacion" class="form-control cities "
                                            data-default="<?php echo $empresa->localizacion_poblacion ?>"></select>
                                </p>
                                <p>
                                    <label>Direcci&oacute;n</label>
                                    <input name="direccion" class="form-control  validate[required]">
                                </p>
                                <div id="map" style="height: 300px;width: 100%" class="map align-center valign-wrapper">
                                    <i class="fa fa-spin fa-spinner fa-2x valign-wrapper" style="margin: auto;"></i>
                                </div>
                                <input name="latitud" id="latitud" type="hidden"
                                       value="<?php echo $empresa->localizacion_latitud ?>">
                                <input name="longitud" id="longitud" type="hidden"
                                       value="<?php echo $empresa->localizacion_longitud ?>">
                                <div class="col s12">
                                    <div class="card-panel z-depth-0 valign-wrapper upload ">
                                        <text style="width: 100%;text-align: center;color: gray;"><i
                                                    class="fa fa-upload fa-2x valign "></i><br>
                                            Suelte la imagen o haga clic aqu&iacute; para cargar.
                                        </text>
                                    </div>
                                    <input name="file" type="hidden" class="archivo">
                                    <p class="error-foto">

                                    </p>
                                    <button type="button" class="btn dorado-2 eliminar-foto hidden" style="width: 100%">
                                        Eliminar Foto
                                    </button>
                                </div>
                            </div>
                            <div class="col s12">
                                <div class="row">
                                    <button class="col s12 btn grey pull-right">
                                        Cancelar
                                    </button>
                                    <button class="col s12 btn dorado-2 pull-right space-top" style="margin-right: 20px">
                                        Publicar Evento
                                    </button>
                                </div>

                            </div>
                        </div>
                    </form>
                </section>
                <section>
                    <?php if ($eventos) { ?>
                        <ul class="collection collapsible" data-collapsible="accordion">
                            <?php foreach ($eventos as $key => $e) { ?>
                                <li class="evento" id="evento-<?php echo $e->id_evento ?>"
                                    data-id="<?php echo $e->id_evento ?>">
                                    <div class="collapsible-header collection-item avatar">
                                        <?php if ($e->mime_imagen) { ?>
                                            <img src="data:<?php echo $e->mime_imagen ?>;base64,<?php echo base64_encode($e->imagen) ?>"
                                                 alt="" class="circle">
                                        <?php } else { ?>
                                            <i class="circle material-icons dorado-2">event</i>
                                        <?php } ?>
                                        <span class="title"><?php echo $e->nombre ?></span>
                                        <p>
                                            <small>Del <?php echo dateMiniFormat($e->fecha_inicio) ?>
                                                a <?php echo dateMiniFormat($e->fecha_termino) ?></small>
                                            <br>
                                            <?php echo $e->direccion ?>  <?php echo $e->poblacion ?>
                                            , <?php echo $e->estado ?>
                                        </p>
                                    </div>
                                    <div class="collapsible-body" style="padding: 0px 20px;">
                                        <form class="form-validate" method="POST"
                                              action="<?php echo site_url("proveedor/escaparate/eventos/edit/$e->id_evento") ?>">
                                            <h5>Editar</h5>
                                            <div class="divider"></div>
                                            <div class="row">
                                                <div class="col s12 m7">
                                                    <p>
                                                        <label>Nombre del evento</label>
                                                        <input name="nombre" class="form-control validate[required]"
                                                               value="<?php echo $e->nombre ?>">
                                                    </p>
                                                    <p>
                                                        <label>Tipo de evento</label>
                                                        <select name="tipo" class="form-control  validate[required]">
                                                            <option value="">-- Seleccionar --</option>
                                                            <option value="3" <?php echo $e->tipo_evento == 3 ? "selected" : "" ?>>
                                                                Expo boda
                                                            </option>
                                                            <option value="4" <?php echo $e->tipo_evento == 4 ? "selected" : "" ?>>
                                                                Demostraci&oacute;n
                                                            </option>
                                                            <option value="5" <?php echo $e->tipo_evento == 5 ? "selected" : "" ?>>
                                                                Degustaci&oacute;n
                                                            </option>
                                                            <option value="6" <?php echo $e->tipo_evento == 6 ? "selected" : "" ?>>
                                                                Inauguraci&oacute;n
                                                            </option>
                                                            <option value="7" <?php echo $e->tipo_evento == 7 ? "selected" : "" ?>>
                                                                Concurso
                                                            </option>
                                                            <option value="8" <?php echo $e->tipo_evento == 8 ? "selected" : "" ?>>
                                                                Desfile
                                                            </option>
                                                            <option value="9" <?php echo $e->tipo_evento == 9 ? "selected" : "" ?>>
                                                                Concierto
                                                            </option>
                                                            <option value="10" <?php echo $e->tipo_evento == 10 ? "selected" : "" ?>>
                                                                Curso
                                                            </option>
                                                            <option value="11" <?php echo $e->tipo_evento == 11 ? "selected" : "" ?>>
                                                                Exposici&oacute;n
                                                            </option>
                                                            <option value="12" <?php echo $e->tipo_evento == 12 ? "selected" : "" ?>>
                                                                Sorteo
                                                            </option>
                                                        </select>
                                                    </p>
                                                    <div class="row"
                                                         style="    margin-bottom: 0px;    margin: 0px;    padding: 0px;">
                                                        <?php $fecha_inicio = strtotime($e->fecha_inicio) ?>
                                                        <p class="col s6" style="    margin: 0px;">
                                                            <label>Empieza el d&iacute;a</label>
                                                            <input name="fecha_inicio" class="form-control datepicker "
                                                                   data-value="<?php echo $fecha_inicio ?>">
                                                        </p>
                                                        <p class="col s6" style="    margin: 0px;">
                                                            <label>a las </label>
                                                            <select name="hora_inicio" class="form-control">
                                                                <?php $fecha_inicio = strftime("%k%M", $fecha_inicio) ?>
                                                                <option value="23:59" <?php echo $fecha_inicio == "2359" ? "selected" : "" ?>>
                                                                    -- Todo el d&iacute;a --
                                                                </option>
                                                                <option value="0:00" <?php echo $fecha_inicio == "000" ? "selected" : "" ?>>
                                                                    0:00
                                                                </option>
                                                                <option value="1:00" <?php echo $fecha_inicio == "100" ? "selected" : "" ?>>
                                                                    1:00
                                                                </option>
                                                                <option value="2:00" <?php echo $fecha_inicio == "200" ? "selected" : "" ?>>
                                                                    2:00
                                                                </option>
                                                                <option value="3:00" <?php echo $fecha_inicio == "300" ? "selected" : "" ?>>
                                                                    3:00
                                                                </option>
                                                                <option value="4:00" <?php echo $fecha_inicio == "400" ? "selected" : "" ?>>
                                                                    4:00
                                                                </option>
                                                                <option value="5:00" <?php echo $fecha_inicio == "500" ? "selected" : "" ?>>
                                                                    5:00
                                                                </option>
                                                                <option value="6:00" <?php echo $fecha_inicio == "600" ? "selected" : "" ?>>
                                                                    6:00
                                                                </option>
                                                                <option value="7:00" <?php echo $fecha_inicio == "700" ? "selected" : "" ?>>
                                                                    7:00
                                                                </option>
                                                                <option value="8:00" <?php echo $fecha_inicio == "800" ? "selected" : "" ?>>
                                                                    8:00
                                                                </option>
                                                                <option value="9:00" <?php echo $fecha_inicio == "900" ? "selected" : "" ?>>
                                                                    9:00
                                                                </option>
                                                                <option value="10:00" <?php echo $fecha_inicio == "1000" ? "selected" : "" ?>>
                                                                    10:00
                                                                </option>
                                                                <option value="11:00" <?php echo $fecha_inicio == "1100" ? "selected" : "" ?>>
                                                                    11:00
                                                                </option>
                                                                <option value="12:00" <?php echo $fecha_inicio == "1200" ? "selected" : "" ?>>
                                                                    12:00
                                                                </option>
                                                                <option value="13:00" <?php echo $fecha_inicio == "1300" ? "selected" : "" ?>>
                                                                    13:00
                                                                </option>
                                                                <option value="14:00" <?php echo $fecha_inicio == "1400" ? "selected" : "" ?>>
                                                                    14:00
                                                                </option>
                                                                <option value="15:00" <?php echo $fecha_inicio == "1500" ? "selected" : "" ?>>
                                                                    15:00
                                                                </option>
                                                                <option value="16:00" <?php echo $fecha_inicio == "1600" ? "selected" : "" ?>>
                                                                    16:00
                                                                </option>
                                                                <option value="17:00" <?php echo $fecha_inicio == "1700" ? "selected" : "" ?>>
                                                                    17:00
                                                                </option>
                                                                <option value="18:00" <?php echo $fecha_inicio == "1800" ? "selected" : "" ?>>
                                                                    18:00
                                                                </option>
                                                                <option value="19:00" <?php echo $fecha_inicio == "1900" ? "selected" : "" ?>>
                                                                    19:00
                                                                </option>
                                                                <option value="20:00" <?php echo $fecha_inicio == "2000" ? "selected" : "" ?>>
                                                                    20:00
                                                                </option>
                                                                <option value="21:00" <?php echo $fecha_inicio == "2100" ? "selected" : "" ?>>
                                                                    21:00
                                                                </option>
                                                                <option value="22:00" <?php echo $fecha_inicio == "2200" ? "selected" : "" ?>>
                                                                    22:00
                                                                </option>
                                                                <option value="23:00" <?php echo $fecha_inicio == "2300" ? "selected" : "" ?>>
                                                                    23:00
                                                                </option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                    <div class="row"
                                                         style="    margin-bottom: 0px;    margin: 0px;    padding: 0px;">
                                                        <p class="col s6" style="    margin: 0px;">
                                                            <label>Y acaba el d&iacute;a</label>
                                                            <input name="fecha_termino" class="form-control datepicker"
                                                                   data-value="<?php echo strtotime($e->fecha_termino) ?>">
                                                        </p>
                                                        <p class="col s6" style="    margin: 0px;">
                                                            <label>a las </label>
                                                            <select name="hora_termino" class="form-control">
                                                                <?php $fecha_inicio = strftime("%k%M", strtotime($e->fecha_termino)) ?>
                                                                <option value="23:59" <?php echo $fecha_inicio == "2359" ? "selected" : "" ?>>
                                                                    -- Todo el d&iacute;a --
                                                                </option>
                                                                <option value="0:00" <?php echo $fecha_inicio == "000" ? "selected" : "" ?>>
                                                                    0:00
                                                                </option>
                                                                <option value="1:00" <?php echo $fecha_inicio == "100" ? "selected" : "" ?>>
                                                                    1:00
                                                                </option>
                                                                <option value="2:00" <?php echo $fecha_inicio == "200" ? "selected" : "" ?>>
                                                                    2:00
                                                                </option>
                                                                <option value="3:00" <?php echo $fecha_inicio == "300" ? "selected" : "" ?>>
                                                                    3:00
                                                                </option>
                                                                <option value="4:00" <?php echo $fecha_inicio == "400" ? "selected" : "" ?>>
                                                                    4:00
                                                                </option>
                                                                <option value="5:00" <?php echo $fecha_inicio == "500" ? "selected" : "" ?>>
                                                                    5:00
                                                                </option>
                                                                <option value="6:00" <?php echo $fecha_inicio == "600" ? "selected" : "" ?>>
                                                                    6:00
                                                                </option>
                                                                <option value="7:00" <?php echo $fecha_inicio == "700" ? "selected" : "" ?>>
                                                                    7:00
                                                                </option>
                                                                <option value="8:00" <?php echo $fecha_inicio == "800" ? "selected" : "" ?>>
                                                                    8:00
                                                                </option>
                                                                <option value="9:00" <?php echo $fecha_inicio == "900" ? "selected" : "" ?>>
                                                                    9:00
                                                                </option>
                                                                <option value="10:00" <?php echo $fecha_inicio == "1000" ? "selected" : "" ?>>
                                                                    10:00
                                                                </option>
                                                                <option value="11:00" <?php echo $fecha_inicio == "1100" ? "selected" : "" ?>>
                                                                    11:00
                                                                </option>
                                                                <option value="12:00" <?php echo $fecha_inicio == "1200" ? "selected" : "" ?>>
                                                                    12:00
                                                                </option>
                                                                <option value="13:00" <?php echo $fecha_inicio == "1300" ? "selected" : "" ?>>
                                                                    13:00
                                                                </option>
                                                                <option value="14:00" <?php echo $fecha_inicio == "1400" ? "selected" : "" ?>>
                                                                    14:00
                                                                </option>
                                                                <option value="15:00" <?php echo $fecha_inicio == "1500" ? "selected" : "" ?>>
                                                                    15:00
                                                                </option>
                                                                <option value="16:00" <?php echo $fecha_inicio == "1600" ? "selected" : "" ?>>
                                                                    16:00
                                                                </option>
                                                                <option value="17:00" <?php echo $fecha_inicio == "1700" ? "selected" : "" ?>>
                                                                    17:00
                                                                </option>
                                                                <option value="18:00" <?php echo $fecha_inicio == "1800" ? "selected" : "" ?>>
                                                                    18:00
                                                                </option>
                                                                <option value="19:00" <?php echo $fecha_inicio == "1900" ? "selected" : "" ?>>
                                                                    19:00
                                                                </option>
                                                                <option value="20:00" <?php echo $fecha_inicio == "2000" ? "selected" : "" ?>>
                                                                    20:00
                                                                </option>
                                                                <option value="21:00" <?php echo $fecha_inicio == "2100" ? "selected" : "" ?>>
                                                                    21:00
                                                                </option>
                                                                <option value="22:00" <?php echo $fecha_inicio == "2200" ? "selected" : "" ?>>
                                                                    22:00
                                                                </option>
                                                                <option value="23:00" <?php echo $fecha_inicio == "2300" ? "selected" : "" ?>>
                                                                    23:00
                                                                </option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                    <p>
                                                        <label>Descripci&oacute;n del Eventos</label>
                                                        <textarea class="editable"
                                                                  name="descripcion"><?php echo $e->descripcion ?></textarea>
                                                    </p>
                                                </div>
                                                <div class="col s12 m5">
                                                    <select name="pais" class="countries hidden "
                                                            data-default="<?php echo $e->pais ?>">
                                                        <option value="" disabled selected>-- Pa&iacute;s --</option>
                                                    </select>
                                                    <p>
                                                        <label>Estado</label>
                                                        <select name="estado" class="form-control states "
                                                                data-default="<?php echo $e->estado ?>"></select>
                                                    </p>
                                                    <p>
                                                        <label>Poblaci&oacute;n</label>
                                                        <select name="poblacion" class="form-control cities "
                                                                data-default="<?php echo $e->poblacion ?>"></select>
                                                    </p>
                                                    <p>
                                                        <label>Direcci&oacute;n</label>
                                                        <input name="direccion" class="form-control  validate[required]"
                                                               value="<?php echo $e->direccion ?>">
                                                    </p>
                                                    <div id="map" style="height: 300px;width: 100%"
                                                         class="map align-center valign-wrapper">
                                                        <i class="fa fa-spin fa-spinner fa-2x valign-wrapper"
                                                           style="margin: auto;"></i>
                                                    </div>
                                                    <input name="latitud" id="latitud" type="hidden"
                                                           value="<?php echo $e->latitud ?>">
                                                    <input name="longitud" id="longitud" type="hidden"
                                                           value="<?php echo $e->longitud ?>">
                                                    <div class="col s12">
                                                        <div class="card-panel z-depth-0 valign-wrapper upload "
                                                             data-evento="evento-<?php echo $e->id_evento ?>">
                                                            <text style="width: 100%;text-align: center;color: gray;"><i
                                                                        class="fa fa-upload fa-2x valign "></i><br>
                                                                Suelte la imagen o haga clic aqu&iacute; para cargar.
                                                            </text>
                                                        </div>
                                                        <input name="file" type="hidden" class="archivo"
                                                               value="data:<?php echo $e->mime_imagen ?>;base64,<?php echo base64_encode($e->imagen) ?>">
                                                        <p class="error-foto">

                                                        </p>
                                                        <button type="button" class="btn dorado-2 eliminar-foto hidden"
                                                                style="width: 100%">
                                                            Eliminar Foto
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <button type="submit" class="btn grey pull-right" name="delete" value="1">
                                                        Eliminar
                                                    </button>
                                                    <button type="submit" class="btn dorado-2 pull-right"
                                                            style="margin-right: 20px" name="submit" value="1">
                                                        Guardar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <div class="valign-wrapper card-panel no-hay " style="height: 250px">
                            <div class="valign align-center" style="width: 100%">
                                <i class="fa fa-info fa-3x "></i><br>No hay eventos
                            </div>
                        </div>
                    <?php } ?>
                </section>
            </div>
        </div>
    </div>
    <?php $this->view("proveedor/footer") ?>
    <script>
        $(document).ready(function () {
            init_tinymce_mini(".editable");
            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 5, // Creates a dropdown of 15 years to control year
                formatSubmit: "yyyy-mm-dd "
            });
            $(".form-validate").validationEngine();
            $(".datepicker").each(function () {
                if ($(this).data("value")) {
                    $(this).pickadate("picker").set('select', $(this).data("value") * 1000);
                }
            });
            $(".delete").click(function (e) {
                e.preventDefault();
                var self = $(this);
                var evento = self.parent().data("id");
                if (evento) {
                    if (confirm("¿Estas seguro de eliminar el evento?")) {
                        $.ajax({
                            url: "<?php echo base_url() ?>index.php/proveedor/escaparate/eventos/borrar",
                            method: "POST",
                            data: {
                                evento: evento
                            },
                            success: function (resp) {
                                self.parent().remove();
                            },
                            error: function () {
                            }
                        });
                    }
                }

            });

            $(".evento").click(function () {
                if ($(this).find(".map .gm-style")) {
                    var center = getPosicion($(this).find(".map"));
                    var self = this;
                    setTimeout(function () {
                        drawMap($(self).find(".map").get(0), center);
                    }, 300);
                }
            });

            $("#toggle-crear").click(function () {
                $("#section-crear").toggleClass("active");
                $(".no-hay").toggleClass("hide");
            });
            $(".upload").each(function (i, elem) {
                var dropzone = new Dropzone(elem, {
                    url: "<?php echo base_url() ?>index.php/proveedor/escaparate/fotossad3",
                    method: "POST",
                    paramName: "files", // The name that will be used to transfer the file
                    maxFilesize: 1, // MB
                    uploadMultiple: false,
                    thumbnailWidth: 500,
                    thumbnailHeight: null,
                    createImageThumbnails: true,
                    accept: function (file, done) {
                        $(elem).find("text").hide();
                        var t = $(elem).find("div.dz-preview").length;
                        if (t > 1) {
                            $(elem).find("div.dz-preview").each(function (i, value) {
                                if (t - 1 != i) {
                                    value.remove();
                                }
                            });
                        }
                        setTimeout(function () {
                            $(elem).parent().find(".archivo").val($(file.previewElement).find("img").attr("src"));
                        }, 1000);
                    },
                    error: function (data) {
                        $(elem).find("text").show();
                        $(elem).parent().find(".error-foto").addClass("red-text");
                        $(elem).parent().find(".error-foto").html("La imagen es demasiado grande");
                        $(elem).parent().find(".dz-preview").remove();
                        setTimeout(function () {
                            $(elem).parent().find(".error-foto").removeClass("red-text");
                            $(elem).parent().find(".error-foto").html("");
                        }, 15000);
                    },
                    acceptedFiles: "image/*"
                });
                var prom = $(elem).data("evento");
                if (prom) {// Create the mock file:
                    var file = $("#" + prom).find(".archivo").attr("value");
                    if (file != null && file != "") {
                        var mockFile = {name: "Filename", size: 12345};
                        dropzone.emit("addedfile", mockFile);
                        dropzone.emit("thumbnail", mockFile, file);
                        dropzone.emit("complete", mockFile);
                        $(elem).find("text").hide();
                    }
                }
            });
            $(".upload text").on("click", function (e) {
                $(e.currentTarget.parentNode).trigger("click");
            });
            $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap", function (data, textStatus, jqxhr) {
            });
        });

        function initMap() {
            var elems = $("#map");
            elems.each(function () {
                var elem = this;
                var center = getPosicion(elem);
                if (center == null) {
                    center = {lat: 20.676580, lng: -103.34785};
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (pos) {
                            var center = {lat: pos.coords.latitude, lng: pos.coords.longitude};
                            drawMap(elem, center);
                            setPosicion(elem, center);
                        }, function () {
                            drawMap(elem, center);
                        });
                    } else {
                        drawMap(elem, center);
                    }
                } else {
                    drawMap(elem, center);
                }
            });
        }

        function drawMap(elem, center) {
            if (google) {
                var map = new google.maps.Map(elem, {
                    center: center,
                    zoom: 14
                });
                var marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    title: 'Mueve el mapa'
                });
                map.addListener('center_changed', function () {
                    var p = map.getCenter();
                    marker.setPosition({lat: p.lat(), lng: p.lng()});
                    setPosicion(elem, {lat: p.lat(), lng: p.lng()});
                });
            }
        }

        function getPosicion(elem) {
            if ($(elem).parent().find("input[name='latitud']").val() != "") {
                return {
                    lat: parseFloat($(elem).parent().find("input[name='latitud']").val()),
                    lng: parseFloat($(elem).parent().find("input[name='longitud']").val())
                }
            }
            return null;
        }

        function setPosicion(elem, center) {
            $(elem).parent().find("input[name='latitud']").val(center.lat);
            $(elem).parent().find("input[name='longitud']").val(center.lng);
        }
    </script>
</body>

</html>
