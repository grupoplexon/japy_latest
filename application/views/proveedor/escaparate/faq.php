<html>
<head>
    <?php $this->view("proveedor/header") ?>
</head>
<?php $this->view("proveedor/menu") ?>
<style>
    p{
        font-weight: 400 !important;
    }
    body{
        background-size: contain;
    }
</style>
<body>


<div class="body-container">
    <div class="row">
        <?php $this->view("proveedor/escaparate/menu") ?>
        <div class="col s12 m8 l9">
            <?php $this->view("proveedor/mensajes") ?>
            <section  class="intro-section">
                <h5>Modificar Preguntas Frecuentes</h5>
                <div class="divider"></div>
                <div class="card-panel blue lighten-5">
                    <i class="fa fa-info "></i>
                    A continuaci&oacute;n, puede contestar las Preguntas Frecuentes, esta informaci&oacute;n
                    aparecer&aacute; en su escaparate.<br>
                    Sus respuestas proporcionar&aacute;n a los novios un mayor conocimiento
                    de los servicios que ofrece su negocio.
                </div>
                <div class="col m12 s12">
                    <form method="POST" action="<?php echo base_url() ?>index.php/proveedor/escaparate/faq"
                          id="faq-form">
                        <?php foreach ($faqs as $key => $faq) { ?>
                            <div class="row">
                                <div class="card-panel z-depth-1">
                                    <h6 style="font-weight: bold">
                                        <i class="material-icons  <?php echo empty($faq->respuesta) ? "red-text" : "green-text" ?> ">info</i>
                                        <?php echo $faq->pregunta ?></h6>
                                    <div class="divider"></div>
                                    <div class="row">
                                        <?php
                                        switch ($faq->tipo) {
                                            case "RANGE":
                                                if ($faq->valores == null) {
                                                    range_money($faq->id_faq, null, $faq->respuesta);
                                                } else {
                                                    range_person($faq->id_faq, null, $faq->respuesta);
                                                }
                                                break;
                                            case "BOOLEAN":
                                                boolean($faq->id_faq, null, $faq->respuesta);
                                                break;
                                            case "TEXT":
                                                text($faq->id_faq, null, $faq->respuesta);
                                                break;
                                            case "CHECKBOX":
                                                checkbox($faq->id_faq, $faq->valores, null, $faq->respuesta);
                                                break;
                                            case "TIME":
                                                select_time($faq->id_faq, null, $faq->respuesta);
                                                break;
                                            case "SELECT":
                                                select($faq->id_faq, $faq->valores, null, $faq->respuesta);
                                                break;
                                            default:
                                                text($faq->id_faq, null, $faq->respuesta);
                                                break;
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <p>
                            <button class="btn btn-block-on-small dorado-2 pull-right">
                                Guardar
                            </button>
                        </p>
                    </form>
                </div>
                <div class="col m8 s12">

                </div>
            </section>

        </div>
    </div>
</div>
<?php $this->view("proveedor/footer") ?>
<script>
    $(document).ready(function () {
        $('input[data-type=\'money\']').inputFormat();
        $('input[data-toggle]').on('change', function (elem) {
            if (this.checked) {
                $(this.getAttribute('data-toggle')).removeClass('hide');
                $(this.getAttribute('data-toggle')).children().removeAttr('disabled');
            }
            else {
                $(this.getAttribute('data-toggle')).addClass('hide');
                $(this.getAttribute('data-toggle')).children().attr('disabled', 'true');
            }
        });

        var error = 0;

        $('#minPeople').on('change', function (e) {
            $(this).css('border', '');
            error = 0;

            if (parseInt($(this).val()) > parseInt($('#maxPeople').val())) {
                $(this).css('cssText', 'border:1px solid red !important;');
                error = 1;
            }
            else if (parseInt($(this).val()) < parseInt($('#maxPeople').val())) {
                $('#maxPeople').css('border', '');
            }
        });

        $('#maxPeople').on('change', function (e) {
            $(this).css('border', '');
            error = 0;

            if (parseInt($(this).val()) < parseInt($('#minPeople').val())) {
                $(this).css('cssText', 'border:1px solid red !important;');
                error = 1;
            }
            else if (parseInt($(this).val()) > parseInt($('#minPeople').val())) {
                $('#minPeople').css('border', '');
            }
        });

        $('#minPrice').on('keyup', function (e) {
            $(this).css('border', '');
            error = 0;
            const minPrice = parseFloat($(this).val().replace(/,\s?/g, ''));
            const maxPrice = parseFloat($('#maxPrice').val().replace(/,\s?/g, ''));

            if (minPrice > maxPrice) {
                $(this).css('cssText', 'border:1px solid red !important;');
                error = 1;
            }
            else if (minPrice < maxPrice) {
                $('#maxPrice').css('border', '');
            }
        });

        $('#maxPrice').on('keyup', function (e) {
            $(this).css('border', '');
            error = 0;
            const maxPrice = parseFloat($(this).val().replace(/,\s?/g, ''));
            const minPrice = parseFloat($('#minPrice').val().replace(/,\s?/g, ''));

            if (maxPrice < minPrice) {
                $(this).css('cssText', 'border:1px solid red !important;');
                error = 1;
            }
            else if (maxPrice > minPrice) {
                $('#minPrice').css('border', '');
            }
        });

        $('#faq-form').submit(function (e) {
            if (error) {
                swal('Oops', 'Al parecer tienes un error', 'error');
                return false;
            }
        });
    });
</script>
</body>
</html>

<?php

function range_money($name, $info = null, $valor = null)
{
    if ($valor != null) {
        $valores = explode("|", $valor);
    } else {
        $valores = [];
    }

    ?>
    <div class=" col m6 s12" style="margin-top: 20px">
        <div class="row">
            <div class="col m6 s12">
                Precio m&iacute;nimo:
            </div>
            <div class="col m6 s12">
                <div class="input-group">
                    <span>$</span>
                    <input class="form-control has-error" id="minPrice"
                           name="<?php echo $name."[]" ?>" <?php echo count($valores) > 0 ? "value='$valores[0]'" : "" ?>
                           data-type="money">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col m6 s12">
                Precio m&aacute;ximo:
            </div>
            <div class="col m6 s12">
                <div class="input-group">
                    <span>$</span>
                    <input class="form-control" id="maxPrice"
                           name="<?php echo $name."[]" ?>" <?php echo count($valores) >= 2 ? "value='$valores[1]'" : "" ?>
                           data-type="money">
                </div>
            </div>
        </div>
    </div>
    <div class="col m6 s12">
        <div class="card-panel white z-depth-0">
            <?php echo $info == null ? "" : $info ?>
        </div>
    </div>
<?php } ?>


<?php

function range_person($name, $info = null, $valor = null)
{
    if ($valor != null) {
        $valores = explode("|", $valor);
    } else {
        $valores = [];
    }

    ?>
    <div class=" col m6 s12" style="margin-top: 20px">
        <div class="row">
            <div class="col m6 s12">
                M&iacute;nimo de personas:
            </div>
            <div class="col m6 s12">
                <div class="input-group">
                    <span><i class="fa fa-child"></i></span>
                    <input class="form-control" id="minPeople"
                           name="<?php echo $name."[]" ?>" <?php echo count($valores) > 0 ? "value='$valores[0]'" : "" ?>
                           data-type="person">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col m6 s12">
                M&aacute;ximo de personas:
            </div>
            <div class="col m6 s12">
                <div class="input-group">
                    <span><i class="fa fa-child"></i></span>
                    <input class="form-control" id="maxPeople"
                           name="<?php echo $name."[]" ?>" <?php echo count($valores) >= 2 ? "value='$valores[1]'" : "" ?>
                           data-type="person">
                </div>
            </div>
        </div>
    </div>
    <div class="col m6 s12">
        <div class="card-panel white z-depth-0">
            <?php echo $info == null ? "" : $info ?>
        </div>
    </div>
<?php } ?>



<?php

function boolean($name, $info = null, $valor = null)
{ ?>
    <div class=" col m6 s12" style="margin-top: 20px">
        <div class="col m6 s12">
            <p>
                <input value="SI" name="<?php echo $name ?>" <?php echo $valor == "SI" ? "checked" : "" ?> type="radio"
                       id="SI_<?php echo $name ?>"/>
                <label for="SI_<?php echo $name ?>">SI</label>
            </p>
        </div>
        <div class="col m6 s12">
            <p>
                <input value="NO" name="<?php echo $name ?>" <?php echo $valor == "NO" ? "checked" : "" ?> type="radio"
                       id="NO_<?php echo $name ?>"/>
                <label for="NO_<?php echo $name ?>">NO</label>
            </p>
        </div>
    </div>
    <div class="col m6 s12">
        <div class="card-panel white z-depth-0">
            <?php echo $info == null ? "" : $info ?>
        </div>
    </div>
<?php } ?>
<?php

function text($name, $info = null, $valor = null)
{ ?>
    <div class=" col m12 s12" style="margin-top: 20px">
        <div class="input-field col s12">
            <textarea id="text_<?php echo $name ?>" name="<?php echo $name ?>"
                      class="materialize-textarea form-control"><?php echo $valor ?></textarea>
        </div>
    </div>
    <?php if ($info != null) { ?>
    <div class="col m6 s12">
        <div class="card-panel white z-depth-0">
            <?php echo $info ?>
        </div>
    </div>
<?php } ?>
<?php } ?>
<?php

function checkbox($name, $values, $info = null, $valor = "")
{
    $v = explode("|", $values);
    ?>
    <div class=" col m12 s12" style="margin-top: 20px">
        <?php
        foreach ($v as $key => $value) {
            $c   = 0;
            $pos = strpos($valor, $value);
            if ($pos === false) {

            } else {
                $valor = str_replace("$value", "", $valor);
                $c++;
            }
            ?>
            <p class="col m4 s12">
                <input type="checkbox"
                        <?php echo($pos === false ? "" : "checked") ?>
                       value="<?php echo $value ?>"
                       name="<?php echo $name ?>[]"
                       id="checkbox_<?php echo $name ?>_<?php echo $value ?>"/>
                <label for="checkbox_<?php echo $name ?>_<?php echo $value ?>"><?php echo $value ?></label>
            </p>
        <?php } ?>
        <?php
        $a = str_split($valor);
        $n = "";
        $b = true;
        foreach ($a as $key => $char) {
            if ( ! $b) {
                $n .= $char;
            } elseif (($char != "" && $char != ",")) {
                $b = false;
                $n .= $char;
            }
        }
        ?>
        <p class="col m4 s12">
            <input value="OTRO" type="checkbox" name="<?php echo $name ?>[]" <?php echo ! empty($n) ? "checked" : "" ?>
                   id="checkbox_<?php echo $name ?>_otro" data-check="otro" data-toggle="#otro_<?php echo $name ?>"/>
            <label for="checkbox_<?php echo $name ?>_otro">Otro</label>
        </p>
        <p class="col s12 <?php echo ! empty($n) ? "" : "hide" ?>  " id="otro_<?php echo $name ?>">
            <textarea class="form-control" name="<?php echo $name ?>[]" disabled="true"
                      style="height: 100px"><?php echo ! empty($n) ? "$n" : "" ?></textarea>
            <small>Ingresa los datos separados por comas</small>
        </p>
    </div>
    <?php if ($info != null) { ?>
    <div class="col m6 s12">
        <div class="card-panel white z-depth-0">
            <?php echo $info ?>
        </div>
    </div>
<?php } ?>
<?php } ?>

<?php

function select_time($name, $info = null, $valor = null)
{ ?>
    <div class=" col m6 s12" style="margin-top: 20px">
        <select class="browser-default" name="<?php echo $name ?>">
            <option value="Ninguno" <?php echo $valor == 'Ninguno' ? "selected" : "" ?> >Ninguno</option>
            <option value="18:00" <?php echo $valor == '18:00' ? "selected" : "" ?>>18:00</option>
            <option value="19:00" <?php echo $valor == '19:00' ? "selected" : "" ?>>19:00</option>
            <option value="20:00" <?php echo $valor == '20:00' ? "selected" : "" ?>>20:00</option>
            <option value="21:00" <?php echo $valor == '21:00' ? "selected" : "" ?>>21:00</option>
            <option value="22:00" <?php echo $valor == '22:00' ? "selected" : "" ?>>22:00</option>
            <option value="23:00" <?php echo $valor == '23:00' ? "selected" : "" ?>>23:00</option>
            <option value="00:00" <?php echo $valor == '00:00' ? "selected" : "" ?>>00:00</option>
            <option value="01:00" <?php echo $valor == '01:00' ? "selected" : "" ?>>01:00</option>
            <option value="02:00" <?php echo $valor == '02:00' ? "selected" : "" ?>>02:00</option>
            <option value="03:00" <?php echo $valor == '03:00' ? "selected" : "" ?>>03:00</option>
            <option value="04:00" <?php echo $valor == '04:00' ? "selected" : "" ?>>04:00</option>
            <option value="05:00" <?php echo $valor == '05:00' ? "selected" : "" ?>>05:00</option>
            <option value="06:00" <?php echo $valor == '06:00' ? "selected" : "" ?>>06:00</option>
            <option value="07:00" <?php echo $valor == '07:00' ? "selected" : "" ?>>07:00</option>
        </select>
    </div>
    <div class="col m6 s12">
        <div class="card-panel white z-depth-0">
            <?php echo $info == null ? "" : $info ?>
        </div>
    </div>
<?php } ?>

<?php

function select($name, $values, $info = null, $valor = null)
{
    $v = explode("|", $values);
    ?>
    <div class=" col m6 s12" style="margin-top: 20px">
        <select class="browser-default" name="<?php echo $name ?>">
            <option value="" disabled selected>Elige una opci&oacute;n</option>
            <?php foreach ($v as $key => $value) { ?>
                <option value="<?php echo $value ?>" <?php echo $valor == $value ? "selected" : "" ?> ><?php echo $value ?></option>
            <?php }
            ?>
        </select>
    </div>
    <div class="col m6 s12">
        <div class="card-panel white z-depth-0">
            <?php echo $info == null ? "" : $info ?>
        </div>
    </div>
<?php } ?>



