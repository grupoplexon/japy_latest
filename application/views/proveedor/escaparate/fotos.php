<html>
    <head>
        <?php $this->view("proveedor/header") ?>
            <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- ================== END BASE CSS STYLE ================== -->
        <style>
            .card-title {
                font-size: 14px !important;
            }

            .imagen img {
                height: 17.7rem;
                max-height: 17.7rem !important;
                margin: 0 auto;
            }

            .imagen div.card-image {
                background: #8C8D8F;
            }

            .imagen {
                transition: 1s;
            }

            .imagen.delete {
                opacity: 0;
                transform: scale(0.1, 0.1);
            }

            .imagen.dz-error {
                display: none;
            }

            .card-reveal {
                overflow: hidden;
            }

            @media only screen and (max-width: 320px) {
                .card-image {
                    height: 150px !important;
                }

                .card-image .image-card {
                    height: 100% !important;
                }

                #description-modal {
                    width: 20rem !important;
                }

                .card-reveal {
                    overflow: auto;
                }
            }

            @media only screen and (min-width: 321px) and (max-width: 425px) {
                .card-image {
                    height: 150px !important;
                }

                .card-image .image-card {
                    height: 100% !important;
                }

                #description-modal {
                    width: 25rem !important;
                }

                .card-reveal {
                    overflow: auto;
                }
            }

            .tab a {
                    color: #373737 !important;
                }
                .tabs .tab .active {
                    background: #373737  !important;
                    border-bottom: 5px solid #373737  !important;
                    transition: .2s ease-in-out;
                    color: #f9c9cb !important;
                }
                @media only screen and (min-width: 768px) {
                    input[type=text]:focus:not([readonly]), textarea.nombre:focus {
                        border-bottom: 1px solid #f8dadf  !important;
                        box-shadow: 0 1px 0 0 #f8dadf ;
                    }

                    input[type=text] {
                        border: 0px !important;
                        border-bottom: 0px !important;
                        margin-top: 17px;
                    }

                    .text-presupuestador {
                        font-size: 1.3rem !important;
                    }
                }
                html, body
            {
                height: 100%;
                width: 100%;
            }
            .modal {
                position: fixed !important;
                padding-top: 0px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
                max-width: 800px !important;
            }
            .modalImg {
                padding-top: 0px !important;
                padding-right: 0px !important;
                padding-bottom: 0px !important;
                padding-left: 0px !important;
            }
            .modalHead {
                padding-top: 10px !important;
                padding-right: 10px !important;
                padding-bottom: 10px !important;
                padding-left: 10px !important;
            }

            .tags{
                margin:0;
                padding:0;
                /* position:absolute; */
                right:24px;
                bottom:-12px;
                list-style:none;
            }
            .tags li, 
            .tags a{
                float:left;
                height:24px;
                line-height:24px;
                position:relative;
                font-size:11px;
                margin-block-end: 5px;
            }
            .tags a{
                margin-left:20px;
                padding:0 10px 0 12px;
                background:#0089e0;
                color:#fff;
                text-decoration:none;
                -moz-border-radius-bottomright:4px;
                -webkit-border-bottom-right-radius:4px; 
                border-bottom-right-radius:4px;
                -moz-border-radius-topright:4px;
                -webkit-border-top-right-radius:4px;    
                border-top-right-radius:4px;    
            }
            
            /* parte izquierda de cada etiqueta en forma de triangulo */
            .tags a:before{
                content:"";
                float:left;
                position:absolute;
                top:0;
                left:-12px;
                width:0;
                height:0;
                border-color:transparent #0089e0 transparent transparent;
                border-style:solid;
                border-width:12px 12px 12px 0;      
            }
            
            /* y por ultimo un :hover para darle algo mas de "estilo" */
            .tags a:hover{
                background:#555;
            }   
            .tags a:hover:before{
                border-color:transparent #555 transparent transparent;
            }
            .card-title {
                            font-size: 14px !important;
                        }
            .imagen img {
                height: 17.7rem;
                max-height: 17.7rem !important;
                margin: 0 auto;
                widtth: 100%;
            }

            .imagen div.card-image {
                background: #8C8D8F;
            }

            .imagen {
                transition: 1s;
            }

            .imagen.delete {
                opacity: 0;
                transform: scale(0.1, 0.1);
            }

            .imagen.dz-error {
                display: none;
            }

            .fotos {
                color: black;
            }

            p {
                margin: 6px !important;
            }
        </style>
    </head>
    <body>
        <?php $this->view("proveedor/menu") ?>
        <?php $provider = Provider::where("id_usuario", $this->session->userdata("id_usuario"))->first(); ?>

        <div class="body-container">
            <div class="row">
                <?php $this->view("proveedor/escaparate/menu") ?>
                <div class="row principal-content presupuesto-screen">
            <!-- <div class="col s12 m12 l3 right barra hide-on-small-only">
                 <ul class="collection with-header"
                style="box-shadow: 0 15px 10px 0px rgba(0,0,0,.5), 0 1px 4px 0 rgba(0,0,0,.3), 0 0 40px 0 rgba(0,0,0,.1) inset; height: 100%;"> 
                    <img src="<?php echo base_url() ?>/dist/img/prueba.jpg" style="width:auto;height:auto;">
                 </ul> 
            </div> -->
            <div class="col s12 m12 l9 pull-left" id="presupuesto-content">

                <div class="row" style="margin-top: 8px">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s6 primary-background secondary-text">
                                <a href="#test1" id="btn-tab-presu">Publicar nuevas fotos</a>
                            </li>
                            <li class="tab col s6 primary-background secondary-text">
                                <a href="#tab-pagos" id="btn-tab-pagos">Inspiraciones activas</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col s12" style="margin-bottom: 30px">
                        <h4 class="center-align primary-text text-presupuestador" style="font-weight: bold;">
                            NUEVAS OFERTAS
                        </h4>
                    </div> -->
                    <div id="test1" class="col s12">
                        <div class="row">
                        <div class="col s12 m8 l12">
                    <?php $this->view("proveedor/mensajes") ?>
                    <section class="intro-section">
                        <h5 class="fotos">Galeria Fotogr&aacute;fica</h5>
                        <div class="card-panel blue lighten-5 fotos">
                            Publique m&iacute;nimo 8 fotos de su empresa relacionadas con bodas y los servicios que ofrece.
                            Entre m&aacute;s fotos publique m&aacute;s f&aacute;cil ser&aacute; que los usuarios contacten con
                            usted para contratar sus servicios.
                        </div>
                        <ul>
                            <li class="check_total fotos"><i
                                        class="fa <?php echo count($galeria) >= 8 ? "fa-check green-text" : "fa-times red-text" ?> "></i>
                                A&ntilde;adir m&iacute;nimo 8 fotograf&iacute;as (en formato GIF y JPG de un m&aacute;ximo de
                                3MB cada una).
                            </li>
                            <li class="fotos"><i class="fa <?php echo $has_lp ? "fa-check green-text" : "fa-times red-text" ?> "></i> A&ntilde;adir
                                una imagen como logotipo de la empresa y seleccione la foto principal.
                            </li>
                        </ul>
                    </section>
                    <section>
                        <div class="card-panel z-depth-0 valign-wrapper upload " id="upload">
                            <text style="width: 100%;text-align: center;color: gray;"><i class="fa fa-upload fa-2x valign "></i><br>
                                Suelte los archivos o haga clic aqu&iacute; para cargar.
                            </text>
                        </div>
                    </section>
                    <section>
                        <div id="mensaje-error" class=" hide card-panel red lighten-2 white-text">
                            <i class="fa fa-warning"></i> Esta imagen no es valida, verifica que no sobrepase el tama&ntilde;o m&aacute;ximo
                            y su nombre no sea mayor a 8 caracteres.
                        </div>
                    </section>
                    <section class="row container-imagenes-insp">
                        <?php if ($galeria) { ?>
                            <?php foreach ($galeria as $key => $imagen) { ?>
                                <div id="<?php echo $imagen->id_galeria ?>"
                                     class="imagen col s12 m4 l3 <?php echo($key >= $provider->allowedImagesQuantity() ? 'hidden' : '') ?>">
                                    <div class="card">
                                        <div class="card-image waves-effect waves-block waves-light image">
                                            <img class="activator image-card" src="<?php echo $imagen->url ?>" style="object-fit: fill;height: 17.7rem;max-height: 17.7rem!important">
                                        </div>
                                        <div class="card-content center-align" style="display: none">
                                    <span class="card-title activator grey-text text-darken-4 truncate">
                                        <?php echo $imagen->nombre ?>
                                    </span>
                                        </div>
                                        <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4">Editar
                                        <i class="material-icons right">close</i></span>
                                            <p class="check-logo">
                                                <input name="logo" <?php echo $imagen->logo == 1 ? "checked" : "" ?>
                                                       type="radio" id="check-logo-<?php echo $imagen->id_galeria ?>"/>
                                                <label for="check-logo-<?php echo $imagen->id_galeria ?>">Logotipo</label>
                                            </p>
                                            <p class="check-principal">
                                                <input name="principal" <?php echo $imagen->principal == 1 ? "checked" : "" ?>
                                                       type="radio" id="check-foto-<?php echo $imagen->id_galeria ?>"/>
                                                <label for="check-foto-<?php echo $imagen->id_galeria ?>">Foto Principal</label>
                                            </p>
                                            <p>
                                                <button class="btn-flat btn-block green white-text modal-trigger inspiracion" data-id="<?php echo $imagen->id_galeria ?>" data-target="modal1" style="width: 104%">
                                                    Inspiración
                                                </button>
                                            </p>
                                            <p>
                                                <a class="waves-effect waves-light btn modal-trigger no-margin-element btn-block btnDescription"
                                                   href="#description-modal"
                                                   data-id="<?php echo $imagen->id_galeria ?>"
                                                   style="color: white;box-shadow: none">Descripción</a>
                                            </p>
                                            <p>
                                                <button data-id="<?php echo $imagen->id_galeria ?>"
                                                        class="btn-flat btn-block delete red white-text" style="width: 104%">
                                                    Eliminar
                                                </button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <div class="container-preview">
                        </div>

                        <!-- Modal Structure -->
                        <div id="modal1" class="modal modal-fixed-footer">
                            <div class="modal-content">
                            <h4>Publique su Inspiración</h4>
                            <div class="col s12 m12 l12 ">
                                <label style="font-size: 16px; color: black !important;">Título</label><br>
                                <input class="form-control" type="text" placeholder="Escribe el título de tu publicación" id="title" maxlength="70">
                                <label style="font-size: 16px; color: black !important;">Descripción</label><br>
                                <input class="form-control" type="text" placeholder="Escribe la descripción de tu publicación" id="description" maxlength="300">
                                <!-- <label style="font-size: 16px; color: black !important;">Etiquetas</label><br>
                                <input class="form-control" type="text" placeholder="Añade etiquetas a tu publicación" id="tags"> -->

                                <div class="form-group row">
                                    <!-- <label class="col-form-label col-md-2">Tags</label> -->
                                    <label style="font-size: 16px; color: black !important;">Tags</label>
                                    <div class="col-md-10">
                                        <select class="tag-selector" multiple="multiple"></select>
                                    </div>
                                </div>

                                <div class="col s12 m6 l3" id="publicar">
                                    <button class="btn btn-block waves-effect waves-light dorado-2 save" disabled="true" id="subir">PUBLICAR AHORA</button>
                                </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                                <!-- <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a> -->
                                <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                                >CERRAR</button>
                            </div>
                        </div>

                    </section>
                </div>
                        </div>
                    </div>

                    <div id="tab-pagos" class="tab2 col s12" style="display: none;">
                        <div class="row">
                            <section class="row container-imagenes">
                                <?php if ($inspiracion) { ?>
                                    <?php foreach ($inspiracion as $key => $imagen) { ?>
                                        <div id="<?php echo $imagen->id_inspiracion ?>"
                                            class="imagen col s8 m1 l3 offset-s2 <?php echo($key >= $provider->allowedImagesQuantity() ? 'hidden' : '') ?>">
                                            <div class="card">
                                                <div class="card-image waves-effect waves-block waves-light image">
                                                    <img class="activator image-card" src="<?php echo $imagen->url ?>" style="object-fit: fill;height: 17.7rem;max-height: 17.7rem!important">
                                                </div>
                                                <div class="card-content center-align" style="display: none">
                                            <span class="card-title activator grey-text text-darken-4 truncate">
                                                <?php echo $imagen->nombre ?>
                                            </span>
                                                </div>
                                                <div class="card-reveal">
                                            <span class="card-title grey-text text-darken-4">Editar
                                                <i class="material-icons right">close</i></span>
                                                <br>
                                                        <button class="btn modal-trigger mostrar" data-target="modal2" id="<?php echo $imagen->id_inspiracion ?>">Información</button>
                                                    <p>
                                                        <button data-id="<?php echo $imagen->id_inspiracion ?>"
                                                                class="btn-flat btn-block delete2 red white-text" style="width: 104%" id="deleteInsp">
                                                            Eliminar
                                                        </button>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                
                                <!-- Modal Structure -->
            <!-- height: 287px;
                height: 19em; -->
            <div id="modal2" class="modal" style="overflow-x: hidden;">
            

            <!-- <div class="col m6 s6 l4 modalImg"style="
            display: flex;
            align-items: center;
            max-width: 100%;
            overflow: hidden;">
                <img id="imgInfo" class="img-fluid" style="">
            </div> -->

            <!-- <div class="col m6 s6 l4 modalImg"style="
                    max-width: 100%;
                    height: auto;
                    width: auto\9;
        ">
                <img id="imgInfo" class="img-fluid" style="">
            </div> -->
                

                <!-- <div class="doc">
                <div class="box">
                    <img id="imgInfo" >
                </div>
                </div> -->

            <div class="col m6 s12 l4 modalImg">
                <div class="doc" style="    
                    flex-flow: column wrap;
                    width: 100%;
                    height: auto;
                    justify-content: center;
                    align-items: center;
                    background: rgb(51, 57, 68) none repeat scroll 0% 0%;
                    margin: 1em;
                    ">
                    <div class="box" style="
                   width: 100%;
                    height: auto;
                    overflow: hidden;
                    ">
                    <img id="imgInfo" style="
                    object-fit: cover;
                    object-position: center center;
                    height: 290px;
                    width: 250px;
                    " >
                    </div>
                </div>
                <!-- <img id="imgInfo" style="margin:5px; width: 100% !important; object-fit: fill !important;"> -->
            </div>



            <div class="col m6 s6 l8">
            
                <div class="col m12 s12 l12">
                <div class="modal-header">
                    <button class="modal-action modal-close waves-effect waves-green btn-flat" 
                    style="float: right !important;
                    position: absolute !important;
                    padding-left: 58% !important;">X</button>
                </div>
                    <div class="col m3 s12 l2 modalHead">
                        <img class="circule-img" style="background: white" id="logoProvider">
                    </div>
                    <div class="col m9 s12 l10 modalImg">
                        <h6 style="font-weight: bold; color: black;" id="nomproInfo"></h6>
                        <h6 id="titleInfo" style="color: black; font-size: 20px;"></h6>
                        <h6 id="fechaInfo" style="color: black;"></h6>
                    </div>
                </div>
                <div class="col m12 s12 l12">
                <!-- <button style="float: right;margin: 1em;">Guardar</button> -->
                
                <!-- <button class="" style="float: right;margin: .5em;" type="submit" name="action">Guardar
                    <i class="material-icons right">send</i>
                </button> -->
    
                    <p align="justify" id="descInfo" style="color: black;"></p>
                    <!-- <div id="divTags">
                    </div> -->
                    <ul class="tags col l12 m12 s12" id="divTags">
                        <li id="divTags">

                        </li>
                    </ul>
                    <div id="divComent">
                    </div>
                    <br>
                    <div class="col m12 s12">
                    <hr>
                        <i class="fa fa-eye" aria-hidden="true" style="color: black;"></i><strong style="color: black;" id="vistasInfo"></strong>
                        <i class="fa fa-commenting" aria-hidden="true" style="color: black;"></i><strong style="color: black;" id="contComent"></strong>
                    <hr>
                    </div>
                </div>
            </div>
        </div>

                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </div>

        <div id="template-imagen" class="template hide">
            <div class="imagen col s12 m4 l3 dz-preview dz-file-preview">
                <div class="card">
                    <div class=" dz-details waves-effect waves-block waves-light">
                        <img class="activator" data-dz-thumbnail>
                    </div>
                    <div class="card-content" style="display: none">
                        <span class="card-title activator grey-text text-darken-4 truncate">
                            <text data-dz-name></text>
                        </span>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">
                            Editar
                            <i class="material-icons right">close</i>
                        </span>
                        <p class="check-logo">
                            <input name="logo" type="radio" id="check-logo-"/>
                            <label for="check-logo-">Logotipo</label>
                        </p>
                        <p class="check-principal">
                            <input name="principal" type="radio" id="check-foto-"/>
                            <label for="check-foto-">Foto Principal</label>
                        </p>
                        <p>
                            <button class="btn-flat btn-block green white-text modal-trigger btninsp inspiracion" data-target="modal1" style="width: 104%">
                                Inspiración
                            </button>
                        </p>
                        <p>
                            <a class="waves-effect waves-light btn modal-trigger no-margin-element btn-block btnDescription"
                               href="#description-modal"
                               data-id=" "
                               style="color: white;box-shadow: none">Descripción</a>
                        </p>
                        <p>
                            <button class="btn-flat btn-block delete red white-text" style="width: 104%">
                                Eliminar
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div id="crop-modal" class="modal">
            <div class="modal-content">
                <h4>Cortar imagen</h4>
                <div class="image-container"></div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat crop-upload">Aceptar</a>
            </div>
        </div>

        <?php $this->view("proveedor/footer") ?>
        <input type="hidden" value="<?php echo base_url() ?>" id="baseURL"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.css"/>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.js"></script>


        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/i18n/es.js" type="text/javascript"></script>

        <script>
            $(document).ready(function() {

                $('.mostrar').click(function() {
            // document.getElementById('divComent').innerHTML='';
            let id = this.id;
            $.ajax({
                url: '<?php echo base_url() ?>'+'proveedor/inspiracion/modalInfo',
                method: 'post',
                //dataType: 'json',
                data: {
                    id_ins: id,
                },
                success: function(response) {
                    var ruta = '<?php echo base_url() ?>'+'/uploads/images/'+response.inspiracion.nombre;
                    document.getElementById("imgInfo").src = ruta;
                    var ruta2 = '<?php echo base_url() ?>'+'uploads/images/'+response.fotoInfo;
                    document.getElementById("logoProvider").src = ruta2;
                    document.getElementById("nomproInfo").innerHTML = response.nomproInfo;
                    document.getElementById("titleInfo").innerHTML = response.inspiracion.titulo;
                    let date = new Date(response.inspiracion.updated_at.replace(/-+/g, '/'));
                    let options = {
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric'
                    };
                    document.getElementById("fechaInfo").innerHTML = date.toLocaleDateString('es-MX', options);
                    document.getElementById("descInfo").innerHTML = response.inspiracion.descripcion;
                    if(response.inspiracion.vistas!=null) {
                        document.getElementById("vistasInfo").innerHTML = '&nbsp;'+response.inspiracion.vistas+'&nbsp;&nbsp;';
                    } else {
                        document.getElementById("vistasInfo").innerHTML = '&nbsp;'+0+'&nbsp;&nbsp;';
                    }
                    var tamaño = response.comentarios.length;
                    document.getElementById("contComent").innerHTML = '&nbsp;'+response.comentarios.length;
                    var i;
                    var comentario="";
                    var comentario2="";
                    for (i = 0; i < tamaño; i++) {
                        var nombre = '<strong style="font-weight: bold; color: black;">'; 
                        nombre += response.comentarios[i]['names']+': '; 
                        nombre += '</strong>'; 

                        var texto = '<strong style="color: black;">'; 
                        texto += response.comentarios[i]['texto']; 
                        texto += '</strong>'; 
                        
                        var salto = '<br>'
                        comentario = comentario + (nombre+texto)+salto;
                    }
                    var tamaño2 = response.tags.length;
                    var x=1;
                    for (i=0; i < tamaño2; i++) {
                        var tag = '<a style="font-weight: bold; color: white; font-size: 13px;">'; 
                        tag += response.tags[i]['tags']; 
                        tag += '</a>';
                        comentario2 = comentario2 + tag;
                    }
                    document.getElementById("divComent").innerHTML = comentario;
                    document.getElementById("divTags").innerHTML = comentario2;
                },
                error: function(e) {
                    console.log("ERROR");
                },
            });
        });

                $('.modal').modal();
                $('#upload').prop('disabled', true);
                var baseURL = $('#baseURL').val();
                var xhr = new XMLHttpRequest();

                $('#upload').dropzone({
                    url: baseURL + 'proveedor/escaparate/fotos',
                    previewTemplate: document.getElementById('template-imagen').innerHTML,
                    method: 'POST',
                    paramName: 'files', // The name that will be used to transfer the file
                    maxFilesize: 3, // MB
                    uploadMultiple: false,
                    createImageThumbnails: true,
                    acceptedFiles: 'image/*',
                    previewsContainer: '.container-preview',
                    dataType: 'json',
                    accept: function(file, done) {
                        $(file.previewElement).find('.delete').on('click', delete_img);
                        $(file.previewElement).find('.check-logo input').on('click', update_logo);
                        $(file.previewElement).find('.check-principal input').on('click', update_principal);
                        $(file.previewElement).find('.check-principal input').on('click', update_principal);
                        $(file.previewElement).find('.check-principal input').on('click', update_principal);
                        $(file.previewElement).find('.check-principal input').on('click', update_principal);

                        $('#mensaje-error').addClass('hide');
                        done();
                        update_check();
                    },
                    success:function (file) {
                        let resp = JSON.parse(file.xhr.response);
                        $('.btnDescription').attr('data-id',resp.data.id);
                        $('.btninsp').attr('data-id',resp.data.id);
                        validar();
                    },
                    error: function(data, xhr) {
                        $(data.previewElement).remove();
                        if (data.size > 3 * 1024 * 1024) {
                            swal('¡Error!', 'Solo puedes subir imagenes de maximo 3 MB', 'error');
                        }
                        else if (!data.type.match('image.*')) {
                            swal('¡Error!', 'Solo puedes subir imagenes', 'error');
                        }
                        else if (xhr.code == 403) {
                            swal('¡Advertencia!', 'Llegaste al máximo de imagenes permitidos, adquiere un nuevo paquete', 'warning');
                        }
                        else {
                            swal('¡Error!', 'Oops ocurrio un error, intentalo de nuevo mas tarde', 'error');
                        }
                    },
                    init: function() {
                        this.on('success', function(file, response) {
                            const mydropzone = this;
                            const id = response.data.id;
                            const imageURL = response.data.url;
                            mydropzone.emit('thumbnail', file, imageURL);

                            $(file.previewElement).attr('id', id);
                            $(file.previewElement).find('[data-dz-name]').html(response.data.file_name);
                            $(file.previewElement).find('.delete').attr('data-id', id);
                            $(file.previewElement).find('.check-logo input').attr('id', 'check-logo-' + id);
                            $(file.previewElement).find('.check-logo label').attr('for', 'check-logo-' + id);
                            $(file.previewElement).find('.check-principal input').attr('id', 'check-foto-' + id);
                            $(file.previewElement).find('.check-principal label').attr('for', 'check-foto-' + id);

                        });
                    },
                });

                $('#upload text').on('click', function(e) {
                    $(e.currentTarget.parentNode).trigger('click');
                });
                $('.imagen .delete').on('click', delete_img);
                $('.imagen .delete2').on('click', delete_insp);
                $('.imagen .check-logo input').on('click', update_logo);
                $('.imagen .check-principal input').on('click', update_principal);
                validar();
            });

            function validar() {
                $.ajax({
                    url: '<?php echo base_url() ?>'+'proveedor/escaparate/fotosCargadas',
                    method: 'post',
                    data: {
                        status: 1,
                        type: "foto",
                        msj: "El proveedor completó sus imágenes",
                    },
                    success: function(response) {

                    },
                    error: function() {

                    },
                });
            }

            function update_check() {
                var total = $('.imagen').length - 1;
                const $checkTotal = $('.check_total i');
                if (total < 8) {
                    $checkTotal.removeClass('fa-check').removeClass('green-text');
                    $checkTotal.addClass('fa-times').addClass('red-text');
                }
                else {
                    $checkTotal.addClass('fa-check').addClass('green-text');
                    $checkTotal.removeClass('fa-times').removeClass('red-text');
                }
            }

            function update_logo(elem) {
                const e = elem.currentTarget.parentElement.parentElement.parentElement.parentElement;
                const id = $(e).attr('id');
                const img = $(e).find('img').attr('src');
                create_miniature(id, 'UPDATE_LOGO', img);
            }

            function update_principal(elem) {
                const e = elem.currentTarget.parentElement.parentElement.parentElement.parentElement;
                const id = $(e).attr('id');
                const img = $(e).find('img').attr('src');
                create_miniature(id, 'UPDATE_PRINCIPAL', img);
            }

            function update_img(elem) {
                var $e = $(elem.currentTarget);
                var id = $e.attr('data-id');
                $.ajax({
                    url: "<?php echo base_url() ?>proveedor/escaparate/fotos",
                    method: 'POST',
                    data: {
                        id: id,
                        method: 'UPDATE',
                    },
                    success: function(resp) {
                        $e.parent().addClass('has-success');
                        setTimeout(function() {$e.parent().removeClass('has-success');}, 1500);
                        $('#' + id + ' .card-title.activator').html($e.val() + '<i class="material-icons right">more_vert</i>');
                    },
                    error: function() {
                        $e.parent().addClass('has-error');
                        setTimeout(function() {
                            $e.parent().removeClass('has-error');
                        }, 1500);
                    },
                });
            }

            function delete_insp(elem) {
                swal({
                    title: 'Estas seguro que quieres borrar esta inspiración?',
                    icon: 'error',
                    buttons: ['Cancelar', true],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var e = elem.currentTarget;
                        var id = $(e).data('id');
                        const $imagesContainer = $('.container-imagenes-insp');
                        $.ajax({
                            url: "<?php echo base_url() ?>proveedor/escaparate/deleteInsp",
                            method: 'POST',
                            data: {
                                id: id,
                            },
                            success: function(resp) {
                                var img = $('#' + id);
                                img.addClass('delete');
                                setTimeout(function() {
                                    $('#' + id).remove();
                                    if ($imagesContainer.find('.imagen.hidden').length) {
                                        $imagesContainer.find('.imagen.hidden').first().removeClass('hidden');
                                    }
                                }, 300);
                            },
                            error: function() {
                                // swal('¡Error!', 'Oops ocurrio un error, intentalo de nuevo mas tarde', 'error');
                            },
                        });
                    }
                });
            }

            function delete_img(elem) {
                swal({
                    title: 'Estas seguro que quieres borrar esta imagen?',
                    icon: 'error',
                    buttons: ['Cancelar', true],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var e = elem.currentTarget;
                        var id = $(e).data('id');
                        const $imagesContainer = $('.container-imagenes');
                        $.ajax({
                            url: "<?php echo base_url() ?>proveedor/escaparate/fotos",
                            method: 'POST',
                            data: {
                                id: id,
                                method: 'DELETE',
                            },
                            success: function(resp) {
                                var img = $('#' + id);
                                img.addClass('delete');
                                setTimeout(function() {
                                    $('#' + id).remove();
                                    if ($imagesContainer.find('.imagen.hidden').length) {
                                        $imagesContainer.find('.imagen.hidden').first().removeClass('hidden');
                                    }
                                    update_check();
                                }, 300);
                            },
                            error: function() {
                                swal('¡Error!', 'Oops ocurrio un error, intentalo de nuevo mas tarde', 'error');
                            },
                        });
                    }
                });
            }

            function create_miniature(id, method, image) {
                const $cropperModal = $('#crop-modal');
                const $uploadCrop = $cropperModal.find('.crop-upload');
                let $img = $('<img />');

                $img.attr('src', image);
                $cropperModal.find('.image-container').html($img);
                $img.cropper({
                    preview: '.image-preview',
                    aspectRatio: 1,
                    autoCrop: true,
                    autoCropArea: 1,
                    movable: false,
                    responsive: true,
                    cropBoxResizable: true,
                    viewMode: 2,
                });

                $cropperModal.modal('open');

                $uploadCrop.off().on('click', function(e) {
                    $.ajax({
                        url: "<?php echo base_url() ?>proveedor/escaparate/fotos",
                        method: 'POST',
                        data: {
                            id: id,
                            method: method,
                            data: $img.cropper('getCroppedCanvas').toDataURL('image/jpeg', '0.9'),
                        },
                        success: function(resp) {
                            $cropperModal.modal('close');
                        },
                        error: function() {
                            $cropperModal.modal('close');
                        },
                    });

                });
            }

        ////////    VARIABLES   ///////////////////////////////////

        setInterval('contador()',1000);
        function contador() {
            if($("#title").val()!=null && $("#description").val()!=null 
            && $tagSelector.select2('val') && $("#description").val()!="" && $("#title").val()!="") {
                document.getElementById("subir").disabled = false;
            } else {
                document.getElementById("subir").disabled = true;
            }
        }

        const $tagSelector = $('.tag-selector');
        const server = "<?php echo base_url() ?>";
        var descripcion;
        var titulo;

        cuentaPublicacion();

        /////// TAGS
            
        $tagSelector.select2({
            ajax: {
                url: server + 'blog/Post',
            },
        });

        $tagSelector.select2({
            tags: true,
            width: '100%',
            placeholder: 'Seleccione una etiqueta',
            allowClear: true,
            language: 'es',
            minimumInputLength: 2,
            ajax: {
                url: server + 'blog/tag/show',
                dataType: 'json',
                type: 'GET',
                quietMillis: 100,
                data: function(term) {
                    return {
                        term: term,
                    };
                },
                processResults: function(data) {
                    return {
                        results: data.map(function(item) {
                            return {
                                text: item.title,
                                id: item.id,
                            };
                        }),
                    };
                },
            },
        });

        $tagSelector.on('change.select2', function(e) {
            let data = {};

            data.tags = $tagSelector.select2('data').map(function(item) {
                return item.text;
            });

            // ESTAS LINEAS GUARDAN UNA NUEVA ETIQUETA SI ESTA NO EXISTIERA
            // $.ajax({
            //     url: server + 'blog/tag/store',
            //     data: data,
            //     method: 'POST',
            //     success: function(resp) {
            //         $('.tag-selector').html('');
            //         resp.forEach(function(item) {
            //             let newOption = new Option(item.title, item.id, true, true);
            //             $tagSelector.append(newOption);
            //         });
            //     },
            //     error: function() {
            //         $('.tag-selector').html('');
            //     },
            // });

        });

        $(".select-dropdown").css("display", "contents");

        ///// FIN TAGS

        var id_galeria;

        $('.inspiracion').on( 'click', function () {
            //alert($(this).data("id"));
            id_galeria = $(this).data("id");
        });

        $('#subir').on( 'click', function () {
            // alert(uploadImg);
            // if(uploadImg!=0) {
            
                titulo = $("#title").val();
                descripcion = $("#description").val();
                tags = $(".tag-selector").val();

                $.ajax({
                    url: '<?php echo base_url() ?>'+'proveedor/inspiracion/guardarDatos',
                    method: 'post',
                    data: {
                        Id: id_galeria,
                        Titulo: titulo,
                        Descripcion: descripcion,
                        Tags: $tagSelector.select2('val'),
                    },
                    success: function(response) {
                        cuentaPublicacion();
                        swal({
                            type: 'success',
                            title: 'Su inspiración fue publicada',
                            showConfirmButton: false,
                        });
                        setTimeout(function(){ location.reload();  }, 500);
                    },
                    error: function(response) {
                        
                        swal('¡Error!', 'Solo puede publicar un máximo de 10 inspiraciones', 'error');
                    },
                });
            // } else {
                // swal('¡Error!', 'Debe llenar todos los campos', 'error');
            // }
        })

        function cuentaPublicacion() {
        $.ajax({
            url: '<?php echo base_url() ?>'+'proveedor/inspiracion/cuentaPublicacion',
            method: 'post',
            data: {
                data: 'validar',
            },
            success: function(response) {
                $('#cuenta').html(response.publicaciones);
            },
            error: function() {
                console.log("ERROR");
            },
        });
    }

        </script>
    </body>
</html>