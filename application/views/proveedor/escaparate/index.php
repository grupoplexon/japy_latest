<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        <?php $this->view("proveedor/header") ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/intl-tel-input/build/css/intlTelInput.min.css">
        <style>
            .select2-container--default .select2-results__group {
                font-weight: bold !important;
            }

            .select2-search__field {
                border-color: transparent !important;
                box-shadow: none !important;
            }
        </style>
    </head>
    <body>
        <?php $this->view("proveedor/menu") ?>
        <style>
            .collection-item {
                padding-left: 10px !important;
            }

            .collection-item i {
                font-size: 20px;
            }
            p{
                font-weight: 400 !important;
            }
            .dorado-2 {
                background: #F8DADF !important;
            }

            @media screen and  (max-width: 425px) {
                .unpullright {
                    float: initial !important;
                }

                .p-justify {
                    text-align: justify;
                }
            }
        </style>
        <div class="body-container">
            <div class="row">
                <?php $this->view("proveedor/escaparate/menu") ?>
                <div class="col s12 m8 l9 " style="background-color: white">
                    <?php $this->view("proveedor/mensajes") ?>
                    <section class="intro-section">
                        <h5>Mis Datos</h5>
                        <div class="divider">
                        </div>
                        <div class="card-panel blue lighten-5">
                            <i class="fa fa-info"></i> Modifica la informaci&oacute;n general de tu empresa.
                            Es muy importante que toda la informaci&oacute;n publicada en tu
                            escaparate y tus datos de contacto est&eacute;n actualizados y sean veraces.
                        </div>
                        <h5>Datos de acceso</h5>
                        <div class="divider"></div>
                        <form method="POST" action="<?php echo base_url() ?>index.php/proveedor/escaparate/updateAcceso">
                            <div class="card-panel z-depth-0 col m6 s12">
                                <p>
                                    <label>
                                        Usuario:
                                    </label>
                                    <input class="form-control" readonly="" name="usuario" name="usuario"
                                           value="<?php echo $contacto->usuario ?>">
                                </p>
                            </div>
                            <div class="card-panel z-depth-0 col m6 s12">
                                <p>
                                    <label>
                                        Contraseña actual:
                                    </label>
                                    <input class="form-control" type="password" name="pass_actual">
                                </p>
                                <p>
                                    <label>
                                        Contraseña nueva:
                                    </label>
                                    <input class="form-control  validate[required,minSize[8],funcCall[checkPassword]]"
                                           type="password" name="pass_nueva" id="pass">
                                </p>
                                <p>
                                    <label>
                                        Confirmar contraseña:
                                    </label>
                                    <input class="form-control  validate[required,minSize[8],equals[pass]]" type="password"
                                           name="password">
                                </p>
                                <p class="center-align">
                                    <button type="submit" class="btn dorado-2 pull-right unpullright">
                                        Guardar
                                    </button>
                                </p>
                            </div>
                        </form>
                    </section>
                    <section class="row">
                        <form method="POST"
                              action="<?php echo base_url() ?>index.php/proveedor/escaparate/createVerification">
                            <h5>
                                Describe tu empresa
                            </h5>
                            <div class="divider"></div>
                            <div class="col s12 m8">
                                <p>
                                    <label>Nombre de Empresa</label>
                                    <input class="form-control " name="nombre_empresa" required="" maxlength="200"
                                           value="<?php echo $empresa->nombre ?>">
                                </p>
                                <p>
                                <label>Descripción</label>
                                <textarea class="descripcion editable"
                                          name="descripcion"><?php echo $empresa->descripcion; ?></textarea>
                                </p>
                                <p class="center-align">
                                    <button type="submit" class="btn dorado-2 pull-right unpullright">
                                        Guardar
                                    </button>
                                </p>
                            </div>
                            <div class="col s12 m4">
                                <p class="p-justify">
                                    Describe detalladamente tu empresa as&iacute; como los servicios o
                                    productos de bodas que ofreces con la m&aacute;xima informaci&oacute;n de
                                    inter&eacute;s para los novios.
                                </p>
                            </div>
                        </form>
                    </section>
                    <section class="row">
                        <h5>
                            Datos de contacto
                        </h5>
                        <div class="divider">
                        </div>
                        <div class="col s12 m8">
                            <form method="POST"
                                  action="<?php echo base_url() ?>proveedor/escaparate/updateContacto">

                                <p>
                                    <label>Persona de Contacto</label>
                                    <input class="form-control validate[required]" name="nombre"
                                           value="<?php echo $contacto->nombre; ?>">
                                </p>
                                <p>
                                    <label>Correo electr&oacute;nico</label>
                                    <input class="form-control" name="correo"
                                           value="<?php echo $contacto->correo; ?>">
                                </p>
                                <p>
                                    <label>Tel&eacute;fono</label>
                                    <input class="form-control" type="tel" id="telefono" name="telefono"
                                           value="<?php echo $empresa->contacto_telefono; ?>" style="padding-left: 42px!important;">
                                </p>

                                <p>
                                    <label>Celular</label>
                                    <input class="form-control" type="tel" id="celular" name="celular"
                                           value="<?php echo $empresa->contacto_celular; ?>" style="padding-left: 42px!important;">
                                </p>
                                <p>
                                    <label>P&aacute;gina Web</label>
                                    <input class="form-control validate[custom[url]]" name="pagina_web"
                                           value="<?php echo $empresa->contacto_pag_web; ?>">
                                </p>
                                <p class="center-align">
                                    <button type="submit" class="btn dorado-2 pull-right unpullright">
                                        Guardar
                                    </button>
                                </p>
                            </form>
                        </div>
                        <div class="col s12 m4">
                            <p class="p-justify">
                                Recibir&aacute;s las solicitudes de informaci&oacute;n
                                de usuarios interesados a la direcci&oacute;n
                                de correo elect&oacute;nico que hayas indicado,
                                as&iacute; como todas las novedades del portal
                                que puedan ser de tu inter&eacute;s.
                            </p>
                        </div>
                    </section>
                    <section class="row">
                        <h5>Sector de actividad</h5>
                        <div class="divider"></div>

                        <form method="POST" action="<?php echo base_url() ?>proveedor/escaparate/updateCategorias">
                            <p>
                                <select class="form-control" name="categories[]" style="display: unset !important;">
                                    <?php foreach ($categories as $category) : ?>
                                    <option value="<?= $category->id ?>"
                                    <?php if($categoryProvider->categoria_id==$category->id) 
                                    {?> selected <?php } ?>><?= $category->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </p>
                            <p class="center-align">
                                <button type="submit" class="btn dorado-2 pull-right unpullright center-align"
                                        style="margin-top: 10px;">
                                    Guardar
                                </button>
                            </p>
                        </form>


                    </section>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() ?>dist/tinymce/tinymce.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#telefono').intlTelInput();
                $('#celular').intlTelInput();
                $('form').validationEngine();
                init_tinymce_mini('textarea.editable');

                $('.category-selector').select2({
                    width: '100%',
                    maximumSelectionLength: 1,
                    placeholder: 'Seleccione su categoria',
                    allowClear: true,
                    language: 'es',
                });

            });
            function init_tinymce_mini(elem) {
                if (typeof tinymce != 'undefined') {
                    tinymce.init({
                        selector: elem,
                        relative_urls: false,
                        menubar: '',
                        plugins: [
                            'advlist autolink lists link  preview anchor',
                            'searchreplace code ',
                            'insertdatetime paste ',
                        ],
                        statusbar: false,
                        height: 350,
                        imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58', 'clubnupcial.com'],
                        browser_spellcheck: true,
                        toolbar: 'insertfile undo redo  | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist ',
                    });
                }
            }
        </script>

        <?php $this->view("japy/prueba/footer") ?>

        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.full.min.js"
                type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/i18n/es.js"
                type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/intl-tel-input/build/js/intlTelInput.min.js" type="text/javascript"></script>

    </body>
</html>