<html>
<head>
    <?php $this->view("proveedor/header") ?>
</head>
<style>
    label {
        color: black !important;
        font-size: 17px !important;
    }
    p{
        font-weight: 400 !important;
    }
</style>
<body>
    <?php $this->view("proveedor/menu") ?>
    <div class="body-container">
        <div class="row">
            <?php $this->view("proveedor/escaparate/menu") ?>
            <div class="col s12 m8 l9">
                <?php $this->view("proveedor/mensajes") ?>
                <section class="intro-section">
                    <h5>Modificar Localizaci&oacute;n y Mapa</h5>
                    <div class="divider"></div>
                    <div class="card-panel blue lighten-5">
                        <i class="fa fa-info "></i>
                        Puede modificar la ubicaci&oacute;n sobre el mapa arrastrando el mapa hasta el punto
                        deseado.<br>
                        La direcci&oacute;n debe constar del nombre de la calle seguido de una coma y el resto de datos.<br>
                        Ejemplo: Av Siempre Viva 412, Colonia La Calma
                    </div>
                    <div class="col m4 s12">
                        <form id="theForm" method="POST"
                              action="<?php echo base_url() ?>index.php/proveedor/escaparate/localizacion">

                            <p>
                                <label>
                                    Pais
                                </label>
                                <select id="selectCountry" name="pais" class="form-control  browser-default countries validate[required]"
                                        data-default="<?php echo $empresa->localizacion_pais ?>">
                                    <!-- <option value="" disabled selected>-- Pa&iacute;s --</option> -->
                                    <option value="142">Mexico</option>
                                    <?php foreach ($countries as $country) : ?>
                                        <option value= "<?= $country->id_pais; ?>"><?= $country->nombre; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </p>
                            <p>
                                <label>
                                    C&oacute;digo Postal
                                </label>
                                <input id="cp" name="cp" class="form-control validate[required,custom[integer]]"
                                       value="<?php echo $empresa->localizacion_cp ?>">
                            </p>
                            <p>
                                <label>
                                    Estado
                                </label>
                                <p style="font-weight: bold">Actual:&nbsp;<span><?php echo $empresa->localizacion_estado ?></span></p>
                                <select id="selectState" name="estado" class="form-control  browser-default states validate[required]"
                                        data-default="<?php echo $empresa->localizacion_estado ?>">
                                    <option value="" disabled selected>-- Estado --</option>
                                </select>
                            </p>
                            <p>
                                <label>
                                    Poblacion
                                </label>
                                <p style="font-weight: bold">Actual:&nbsp;<span><?php echo $empresa->localizacion_poblacion ?></span></p>
                                <select id="selectCity" name="poblacion" class="form-control validate[required]  browser-default cities"
                                        data-default="<?php echo $empresa->localizacion_poblacion ?>">
                                    <option value="" disabled selected>-- Poblaci&oacute;n --</option>
                                </select>
                            </p>
                            <input name="nameCountry" style="display: none;" id="nameCountry" type="text">
                            <input name="nameState" style="display: none;" id="nameState" type="text">
                            <p>
                                <label>
                                    Direccion
                                </label>
                                <input id="direccion" name="direccion" class="form-control validate[required]"
                                       value="<?php echo $empresa->localizacion_direccion ?>">
                            </p>
                            <input name="latitud" id="latitud" type="hidden"
                                   value="<?php echo $empresa->localizacion_latitud ?>">
                            <input name="longitud" id="longitud" type="hidden"
                                   value="<?php echo $empresa->localizacion_longitud ?>">
                            <p>
                                <button class="btn btn-block-on-small dorado-2 pull-right" type="submit"
                                        style="margin-bottom: 10px">
                                    Guardar
                                </button>
                            </p>
                        </form>
                    </div>
                    <div class="col m8 s12">
                        <div id="map" class="z-depth-1 center-align valign-wrapper" style="height: 350px;width: 100%">
                            <i class="fa fa-spin fa-spinner fa-2x valign-wrapper" style="margin: auto;"></i>
                        </div>
                        <div class="card-panel yellow lighten-5">
                            <i class="fa fa-info "></i> Arrastra el mapa hasta el punto deseado.<br>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <?php $this->view("proveedor/footer") ?>
    <script src="<?php echo base_url() ?>dist/js/location.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $("form").validationEngine();

            var location = {};
            $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    // location
                    if($('select[name=pais]').children("option:selected").val()){
                        console.log('entrando...');
                        $.ajax({
                            url: '<?= base_url().'/registro/getStates/' ?>'+$('select[name=pais]').children("option:selected").val()+'/'+location.state,
                            dataType: 'json',
                            timeout: 8000,
                            success: function(response) {
                                /////////////////////////
                                var myselect = $('<select>');
                                $.each(response.states, function(index, key) {
                                    myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                                });
                                $('#selectState').append(myselect.html());
                                /////////////////////////
                                var myselect = $('<select>');
                                $.each(response.cities, function(index, key) {
                                    myselect.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                                });
                                $('#selectCity').append(myselect.html());
                                /////////////////////////
                                // $('#selectCountry').html(location.country_name);
                                $('#selectState').val(response.state.id_estado);
                                $('#selectCity').val(location.city);

                                $( "#selectCountry" ).change(function() {
                                    $.ajax({
                                        url: '<?= base_url().'/registro/getStates/' ?>'+$('#selectCountry').val(),
                                        dataType: 'json',
                                        timeout: 8000,
                                        success: function(response) {
                                            // $('#selectCity').remove();
                                            $("#selectState").empty();
                                            var myselect = $('<select>');
                                            myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                                            $.each(response.states, function(index, key) {
                                                myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                                            });
                                            $('#selectState').append(myselect.html());
                                        }
                                    });
                                });

                                $( "#selectState" ).change(function() { /// obtiene ciudades
                                    $.ajax({
                                        url: '<?= base_url().'/registro/getCities/' ?>'+$('#selectState').val(),
                                        dataType: 'json',
                                        timeout: 8000,
                                        success: function(response) {
                                            // $('#selectCity').remove();
                                            $("#selectCity").empty();
                                            var myselect = $('<select>');
                                            myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                                            $.each(response.cities, function(index, key) {
                                                myselect.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                                            });
                                            $('#selectCity').append(myselect.html());
                                        }
                                    });
                                });

                                // $( "#selectCity" ).change(function() {
                                    
                                // });
                            },
                        });
                    }
                },
            });
            $('#theForm').submit(function() {
                $("#nameCountry").val($("#selectCountry option:selected").text());
                $("#nameState").val($("#selectState option:selected").text());
                return true;
            });
        });

        var map;
        var marker;
        var center;

        function initMap() {
            center = getPosicion();
            var elem = document.getElementById("map");
            if (center == null) {
                center = {lat: 20.676580, lng: -103.34785};
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (pos) {
                        center = {lat: pos.coords.latitude, lng: pos.coords.longitude};
                        drawMap(elem, center);
                        setPosicion(center);
                    }, function () {
                        drawMap(elem, center);
                    });
                } else {
                    drawMap(elem, center);
                }
            } else {
                drawMap(elem, center);
            }
        }

        function drawMap(elem, center) {
            map = new google.maps.Map(elem, {
                center: center,
                zoom: 14
            });
            marker = new google.maps.Marker({
                position: center,
                map: map,
                animation: google.maps.Animation.DROP,
                title: 'Mueve el mapa'
            });
            map.addListener('center_changed', function () {
                var p = map.getCenter();
                marker.setPosition({lat: p.lat(), lng: p.lng()});
                setPosicion({lat: p.lat(), lng: p.lng()});
            });
        }

        function setPosicion(center) {
            $("#latitud").val(center.lat);
            $("#longitud").val(center.lng);
        }

        function getPosicion() {
            if ($("#latitud").val() != "") {
                return {lat: parseFloat($("#latitud").val()), lng: parseFloat($("#longitud").val())};
            }
            return null;
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap"></script>
</body>

</html>