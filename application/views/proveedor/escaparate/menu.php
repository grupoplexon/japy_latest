<style>
    .menu-provider-esc {
        display: flex;
        flex-wrap: wrap;
        width: 100%;
        max-width: 100%;
        height: auto;
        background-color: white;
        justify-content: center;
        align-items: center;
        box-shadow: 0px -2px 11px 1px #323232;
    }

    .menu-provider-item {
        font-size: 20px;
        padding: 6px;
        color: #adacac;
        border-right: 1px solid #aaa2a2;
        width: 10%;
        display: flex;
        justify-content: center;
    }

    .active {
        color: #8dd7e0 !important;
    }
</style>
<?php
$controller = $_SERVER["REQUEST_URI"];
$controller = strtolower($controller);
?>

<div class="<?php echo preg_match("/(boda-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)/",
        $controller) ? "hide-on-med-and-up" : ""; ?>">
    <div class="col s2 m4 l3 sidebar hide-on-small-only">
        <div class="collection with-header">
            <a class="collection-item black-text "
               href="<?php echo base_url() ?>boda-<?php echo $this->session->userdata("slug") ?>">
                <h5>
                    <i class="fa fa-home"> </i>
                    <text class="hide-on-small-only">Mi escaparate <br>
                        <small style="font-size: 11px">Ver mi escaparate ></small>
                    </text>
                </h5>
            </a>
            <a class="collection-item black-text <?php echo preg_match("/proveedor\/escaparate$/",
                    $controller) ? "active" : ""; ?>"
               href="<?php echo base_url() ?>proveedor/escaparate">
                <i class="fa fa-edit "></i>
                <text class="hide-on-small-only">Datos de la Empresa</text>
            </a>
            <a class="collection-item black-text <?php echo (strpos($controller, "localizacion")) ? 'active' : ''; ?>"
               href="<?php echo base_url() ?>proveedor/escaparate/localizacion">
                <i class="fa fa-map-marker"> </i>
                <text class="hide-on-small-only">Localizaci&oacute;n y Mapa</text>
            </a>
            <a class="collection-item black-text <?php echo (strpos($controller, "faq")) ? 'active' : ''; ?> "
               href="<?php echo base_url() ?>proveedor/escaparate/faq">
                <i class="fa fa-check  "> </i>
                <text class="hide-on-small-only">Preguntas Frecuentes</text>
            </a>
            <a class="collection-item black-text <?php echo (strpos($controller, "promociones")) ? 'active' : ''; ?>  "
               href="<?php echo base_url() ?>proveedor/escaparate/promociones">
                <i class="fa fa-tags  "></i>
                <text class="hide-on-small-only">Promociones</text>
            </a>
            <a class="collection-item black-text <?php echo (strpos($controller, "fotos")) ? 'active' : ''; ?> "
               href="<?php echo base_url() ?>proveedor/escaparate/fotos">
                <i class="fa fa-camera-retro "> </i>
                <text class="hide-on-small-only">Fotos
                    <text>
            </a>
            <?php if (Provider::find($this->session->id_proveedor)->tipo_cuenta == 3) : ?>
                <a class="collection-item black-text <?php echo (strpos($controller, "videos")) ? 'active' : ''; ?>"
                   href="<?php echo base_url() ?>proveedor/escaparate/videos"> <i
                            class="fa fa-video-camera "> </i>
                    <text class="hide-on-small-only">Videos
                        <text>
                </a>
            <?php endif ?>
            <!--<a class="collection-item black-text ">    <i class="fa fa-genderless ">   </i> <text class="hide-on-small-only">Reportajes de boda<text></a>-->
            <!--            --><?php
            //            $p = $this->checker->tipoProveedor();
            //            if ($p->producto) {
            //                ?>
            <!--                <a class="collection-item black-text-->
            <!--            --><?php //echo (strpos($controller, "productos")) ? 'active' : ''; ?><!--"-->
            <!--                   href="--><?php //echo base_url() ?><!--proveedor/escaparate/productos">-->
            <!--                    --><?php //icon_tipo_articulo($p->grupo, $p->producto) ?>
            <!--                    <text class="hide-on-small-only">--><?php //echo ucfirst(strtolower($tipo->label_producto)) ?>
            <!--                        <text>-->
            <!--                </a>-->
            <!--            --><?php //} ?>
            <!--<a class="collection-item black-text <?php echo (strpos($controller, "calendario")) ? 'active' : ''; ?>"-->
            <!--   href="<?php echo base_url() ?>proveedor/escaparate/calendario"> <i-->
            <!--            class="fa fa-calendar-o"> </i>-->
            <!--    <text class="hide-on-small-only">Calendario de Bodas-->
            <!--        <text>-->
            <!--</a>-->
            <!--<a class="collection-item black-text <?php echo (strpos($controller, "eventos")) ? 'active' : ''; ?>"-->
            <!--   href="<?php echo base_url() ?>proveedor/escaparate/eventos">-->
            <!--    <i class="fa fa-calendar-check-o "></i>-->
            <!--    <text class="hide-on-small-only">Eventos-->
            <!--        <text>-->
            <!--</a>-->
            <!--<a class="collection-item black-text <?php echo (strpos($controller, "colaboradores")) ? 'active' : ''; ?>"-->
            <!--   href="<?php echo base_url() ?>proveedor/escaparate/colaboradores"> <i-->
            <!--            class="fa fa-exchange "> </i>-->
            <!--    <text class="hide-on-small-only">Empresas Colaboradoras-->
            <!--        <text>-->
            <!--</a>-->
        </div>
    </div>

    <div class="row menu-provider-esc hide-on-med-and-up">
        <a class="menu-provider-item <?php echo preg_match("/(boda-)([\p{L}0-9]+(?:-[\p{L}0-9]+)*)$/",
                $controller) ? "active" : ""; ?>"
           href="<?php echo base_url() ?>boda-<?php echo $this->session->userdata("slug") ?>">
            <i class="fa fa-home"> </i>
        </a>
        <a class="menu-provider-item <?php echo preg_match("/proveedor\/escaparate$/",
                $controller) ? "active" : ""; ?> "
           href="<?php echo base_url() ?>proveedor/escaparate">
            <i class="fa fa-edit "></i>
        </a>
        <a class="menu-provider-item  <?php echo (strpos($controller, "localizacion")) ? 'active' : ''; ?>"
           href="<?php echo base_url() ?>proveedor/escaparate/localizacion">
            <i class="fa fa-map-marker"> </i>
        </a>
        <a class="menu-provider-item <?php echo (strpos($controller, "faq")) ? 'active' : ''; ?>"
           href="<?php echo base_url() ?>proveedor/escaparate/faq">
            <i class="fa fa-check  "> </i>
        </a>
        <a class="menu-provider-item <?php echo (strpos($controller, "promociones")) ? 'active' : ''; ?>"
           href="<?php echo base_url() ?>proveedor/escaparate/promociones">
            <i class="fa fa-tags  "></i>
        </a>
        <a class="menu-provider-item <?php echo (strpos($controller, "fotos")) ? 'active' : ''; ?>"
           href="<?php echo base_url() ?>proveedor/escaparate/fotos">
            <i class="fa fa-camera-retro "> </i>
        </a>
        <?php if (Provider::find($this->session->id_proveedor)->tipo_cuenta == 3) : ?>
            <a class="menu-provider-item <?php echo (strpos($controller, "videos")) ? "active" : ""; ?>"
               href="<?php echo base_url() ?>proveedor/escaparate/videos"> <i
                        class="fa fa-video-camera "> </i>
            </a>
        <?php endif ?>
        <!--<a class="menu-provider-item <?php echo (strpos($controller, "calendario")) ? 'active' : ''; ?>"-->
        <!--   href="<?php echo base_url() ?>proveedor/escaparate/calendario"> <i-->
        <!--            class="fa fa-calendar-o"> </i>-->
        <!--</a>-->
        <!--<a class="menu-provider-item <?php echo (strpos($controller, "eventos")) ? 'active' : ''; ?>"-->
        <!--   href="<?php echo base_url() ?>proveedor/escaparate/eventos">-->
        <!--    <i class="fa fa-calendar-check-o "></i>-->
        <!--</a>-->
        <!--<a class="menu-provider-item <?php echo (strpos($controller, "colaboradores")) ? 'active' : ''; ?>"-->
        <!--   href="<?php echo base_url() ?>proveedor/escaparate/colaboradores">-->
        <!--    <i class="fa fa-exchange "> </i>-->
        <!--</a>-->
    </div>
</div>

<?php
function icon_tipo_articulo($grupo, $producto)
{
    if ($grupo == "BANQUETES") {
        echo '<i class="fa fa-cutlery "></i>';
    } else {
        if ($producto == "Servicios") {
            echo '<i class="fa fa-server" aria-hidden="true"></i>';
        } elseif ($producto == "Art&iacute;culos" || $producto == "VESTIDOS") {
            echo '<i class="fa fa-shopping-bag" aria-hidden="true"></i>';
        } elseif ($producto == "Autos") {
            echo '<i class="fa fa-car" aria-hidden="true"></i>';
        } elseif ($producto == "Mobiliario") {
            echo '<i class="fa fa-building-o" aria-hidden="true"></i>';
        } elseif ($producto == "Jayer&iacute;a") {
            echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
        }
    }
}

?>