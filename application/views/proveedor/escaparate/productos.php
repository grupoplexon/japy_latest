<html>
    <head>
        <?php $this->view("proveedor/header") ?>
        <style>
            .card-title{
                font-size: 12px
            }
            .dz-filename{
                display: none;
            }
            .dz-preview.dz-image-preview,.dz-image img  {
                width: 100%;
            }
            .desc{
                background: url('/clubnupcial/dist/img/tag.png');
                background-size: contain;
                background-position-x: 23px;
                background-repeat: no-repeat;
                width: 100%;
                color: white;
                font-size: 21px !important;
                text-shadow: 1px 0px 2px black;
                padding-top: 12px;
                padding-left: 46px !important;
                height: 50px !important;min-width: 115px;
                font-weight: bold;

            }
            .promociones .collection .collection-item.active {
                background-color: white !important;
                color: black;
            }
            [type="radio"]:not(:checked) + label.desc:before, [type="radio"]:not(:checked) + label.desc:after  {
                margin-top: 18px;
            }
            [type="radio"]:checked + label.desc:after {
                margin-top: 18px;
            }
        </style>
    </head>
    <body >
        <?php $this->view("proveedor/menu") ?>
        <div class="body-container">
            <div class="row">
                <?php $this->view("proveedor/escaparate/menu") ?>
                <div class="col s10 m8 l9">
                    <?php $this->view("proveedor/mensajes") ?>
                    <section>
                        <h5>Productos</h5>
                        <div class="card-panel blue lighten-5">
                            Publique los diferentes <?php echo ucfirst(strtolower($tipo->producto)) ?> para bodas de los que dispone.
                            De esta manera, los novios que se interesen por su escaparate
                            tendr&aacute;n una informaci&oacute;n m&aacute;s completa sobre sus servicios
                            y recibir&aacute; solicitudes de presupuesto de mejor calidad.
                        </div>
                        <button id="btn-crear" class="btn dorado-2">
                            Registrar <?php echo $tipo->label_producto ?>
                        </button>
                    </section>
                    <section id="section-crear">
                        <ul class="collapsible" data-collapsible="accordion">
                            <li>
                                <div  class=" hide collapsible-header"><i class="material-icons">filter_drama</i>First</div>
                                <div class="collapsible-body">
                                    <div class="row" id="nuevo">
                                        <form class="form-validate" action="<?php echo base_url() ?>index.php/proveedor/escaparate/productos/nuevo" method="POST">
                                            <div class=" col m10 offset-m1 s12 offset-s0">
                                                <h5>
                                                    Registrar <?php echo ucfirst(strtolower($tipo->label_producto)) ?>
                                                </h5>
                                                <div class="divider"> </div>
                                                <input type="hidden" id="tipo_producto" name="tipo_producto" value="<?php echo $tipo->label_producto ?>">
                                                <div>
                                                    <label style="margin-top: 10px">
                                                        Nombre
                                                    </label>
                                                    <input name="nombre" class="form-control validate[required]">
                                                </div>
                                                <label style="margin-top: 10px;width: 100%;display: block;">Precio</label>
                                                <div class="input-group" style="margin-bottom: 20px;">
                                                    <span>$</span>
                                                    <input data-type="money" name="precio" class="form-control validate[required]">
                                                </div>
                                                <div class="row">
                                                    <div class="col s12 <?php echo ($tipo->producto_imagen ? "m8" : "m12") ?>">
                                                        <label>
                                                            Detalle
                                                        </label>
                                                        <textarea name="detalle" class="editable"></textarea>
                                                    </div>
                                                    <?php if ($tipo->producto_imagen) { ?>
                                                        <div class="col s12 m4">
                                                            <div class="card-panel z-depth-0 valign-wrapper upload " id="upload" >
                                                                <text  style="width: 100%;text-align: center;color: gray;">
                                                                <i class="material-icons valign fa-4x">cloud_upload</i>
                                                                Suelte la imagen o haga clic aqu&iacute; para cargar.
                                                                </text>
                                                            </div>
                                                            <input name="file" type="hidden" class="archivo">
                                                            <div class="label-error hide red-text darken-5" >
                                                                La imagen es demasiado grande
                                                            </div>
                                                            <button type="button" class="btn dorado-2 eliminar-foto waves-effect" style="width: 100%" data-producto="nuevo">
                                                                Eliminar Foto
                                                            </button>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <?php if ($tipo->producto_imagen) { ?>
                                                    <div class="row detail <?php echo count($tipos_producto) == 1 ? "" : "hide" ?>">
                                                        <?php foreach ($tipos_producto as $key => $tipos_c) { ?>
                                                            <div class="form-tipo-producto <?php echo strtolower($tipos_c->nombre) ?> producto-<?php echo ($tipos_c->id_tipo_producto) ?>">
                                                                <?php foreach ($tipos_c->sortable as $key => $tp) { ?>
                                                                    <?php if (count($tp->valores)) { ?>
                                                                        <div>
                                                                            <label><?php echo ucfirst($tp->nombre) ?></label>
                                                                            <select class="browser-default" name="<?php echo $tp->nombre ?>"> 
                                                                                <option selected disabled >Selecciona una opci&oacute;n</option>
                                                                                <?php foreach ($tp->valores as $key => $valor) { ?>
                                                                                    <option value="<?php echo strtoupper($valor->nombre) ?>"><?php echo ucfirst($valor->nombre) ?></option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    <?php } else { ?>
                                                                        <div>
                                                                            <label><?php echo ucfirst($tp->nombre) ?></label>
                                                                            <input class="form-control autocomplate-<?php echo str_replace("ñ", "n", $tp->nombre) ?>" data-producto="<?php echo strtolower($tipos_c->nombre) ?>" name="<?php echo str_replace("ñ", "n", $tp->nombre) ?>" type="text">
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <p>
                                                <button type="submit" class="btn dorado-2 pull-right waves-effect" style="margin-right: 20px;">
                                                    Guardar
                                                </button>
                                                <button type="reset" class="btn-flat pull-right waves-effect white">
                                                    Cancelar
                                                </button>
                                            </p>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </section>
                    <section>
                        <?php if ($productos) { ?>
                            <ul class="collapsible" data-collapsible="accordion">
                                <?php
                                foreach ($productos as $key => $p) {
                                    ?>
                                    <li id="<?php echo $p->id_producto ?>">
                                        <div class="collapsible-header"><span class="chip green white-text ">publicado</span> <?php echo $p->nombre ?></div>
                                        <div class="collapsible-body">
                                            <form class=" form-validate" action="<?php echo base_url() ?>index.php/proveedor/escaparate/productos/editar/<?php echo $p->id_producto ?>" method="POST">
                                                <div class="row">
                                                    <div class=" col m10 offset-m1 s12 offset-s0">
                                                        <h5>
                                                            Editar Producto
                                                        </h5>
                                                        <div class="divider"> </div>
                                                        <input name="tipo_producto" type="hidden" value="<?php echo $p->tipo_producto ?>">
                                                        <label style="margin-top: 10px">
                                                            Nombre
                                                        </label>
                                                        <input name="nombre" value="<?php echo $p->nombre ?>" class="form-control validate[required]">
                                                        <input name="producto" type="hidden" value="<?php echo $p->id_producto ?>">
                                                        <label style="margin-top: 10px;width: 100%;display: block;">Precio</label>
                                                        <div class="input-group" style="margin-bottom: 20px;">
                                                            <span>$</span>
                                                            <input data-type="money" name="precio" value="<?php echo $p->precio ?>" class="form-control validate[required]">
                                                        </div>
                                                        <div class="row">
                                                            <div class="col s12 <?php echo ($tipo->producto_imagen ? "m8" : "m12") ?>">
                                                                <label>
                                                                    Detalle
                                                                </label>
                                                                <textarea name="detalle" class="editable"><?php echo $p->detalle ?></textarea>
                                                            </div>
                                                            <?php if ($tipo->producto_imagen) { ?>
                                                                <div class="col s12 m4">
                                                                    <div class="card-panel z-depth-0 valign-wrapper upload " id="upload" data-producto="<?php echo $p->id_producto ?>" >
                                                                        <text  style="width: 100%;text-align: center;color: gray;">
                                                                        <i class="material-icons valign fa-4x">cloud_upload</i>
                                                                        Suelte la imagen o haga clic aqu&iacute; para cargar.
                                                                        </text>
                                                                    </div>
                                                                    <input name="file" type="hidden" class="archivo" value="data:<?php echo $p->mime ?>;base64,<?php echo base64_encode($p->imagen) ?>">
                                                                    <div class="label-error hide red-text darken-5" >
                                                                        La imagen es demasiado grande
                                                                    </div>
                                                                    <button type="button" class="btn dorado-2 eliminar-foto waves-effect" style="width: 100%" data-producto="<?php echo $p->id_producto ?>">
                                                                        Eliminar Foto
                                                                    </button>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="detail-edit">
                                                            <?php if ($p->corte) { ?>
                                                                <label style="margin-top: 10px">
                                                                    Corte: 
                                                                </label>
                                                                <input name="corte" readonly="" value="<?php echo $p->corte ?>" class="form-control validate[required]">
                                                            <?php } ?>
                                                            <?php if ($p->escote) { ?>
                                                                <label style="margin-top: 10px">
                                                                    Escote: 
                                                                </label>
                                                                <input name="escote" readonly="" value="<?php echo $p->escote ?>" class="form-control validate[required]">
                                                            <?php } ?>
                                                            <?php if ($p->largo) { ?>
                                                                <label style="margin-top: 10px">
                                                                    Largo: 
                                                                </label>
                                                                <input name="largo" readonly="" value="<?php echo $p->largo ?>" class="form-control validate[required]">
                                                            <?php } ?>
                                                            <?php if ($p->categoria) { ?>
                                                                <label style="margin-top: 10px">
                                                                    Categoria: 
                                                                </label>
                                                                <input name="categoria" readonly="" value="<?php echo $p->categoria ?>" class="form-control validate[required]">
                                                            <?php } ?>
                                                            <?php if ($p->tipo) { ?>
                                                                <label style="margin-top: 10px">
                                                                    Tipo: 
                                                                </label>
                                                                <input name="tipo" readonly="" value="<?php echo $p->tipo ?>" class="form-control validate[required]">
                                                            <?php } ?>
                                                            <?php if ($p->disenador) { ?>
                                                                <label style="margin-top: 10px">
                                                                    Diseñador: 
                                                                </label>
                                                                <input name="disenador" readonly="" value="<?php echo $p->disenador ?>" class="form-control validate[required]">
                                                            <?php } ?>
                                                            <?php if ($p->temporada) { ?>
                                                                <label style="margin-top: 10px">
                                                                    Temporada: 
                                                                </label>
                                                                <input name="temporada" readonly="" value="<?php echo $p->temporada ?>" class="form-control validate[required]">
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn dorado-2 pull-right" style="margin-right: 20px;">
                                                    Guardar
                                                </button>
                                            </form>
                                            <form  action="<?php echo base_url() ?>index.php/proveedor/escaparate/productos/eliminar/<?php echo $p->id_producto ?>" method="POST">
                                                <input name="producto" type="hidden" value="<?php echo $p->id_producto ?>">
                                                <button type="submit" value="eliminar" class="btn grey pull-right" style="    margin-right: 30px;">
                                                    Eliminar
                                                </button>
                                            </form>
                                            <div class="row">
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <div class="card-panel valign-wrapper" style="min-height: 250px">
                                <div class="valign grey-text" style="    text-align: center;width: 100%;">
                                    <i class="fa fa-warning fa-4x"></i><br>
                                    No tienes productos o servicio registrados
                                </div>
                            </div>
                        <?php } ?>

                    </section>
                </div>
            </div>
        </div>
        <div class="detail-temp hide">

        </div>
        <?php $this->view("proveedor/footer") ?>
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function () {
                $("#section-crear .collapsible-header").hide();
                init_tinymce_mini("textarea.editable");
                $("input[data-type='money']").inputFormat();

                $("#btn-crear").on("click", function () {
                    $("#section-crear .collapsible-header").trigger("click");
                });

                $(".upload").each(function (i, elem) {
                    var dropzone = new Dropzone(elem, {
                        url: "<?php echo base_url() ?>index.php/proveedor/escaparate/fotossad3",
                        method: "POST",
                        paramName: "files", // The name that will be used to transfer the file
                        maxFilesize: 1, // MB
                        uploadMultiple: false,
                        thumbnailWidth: 500,
                        thumbnailHeight: null,
                        createImageThumbnails: true,
                        accept: function (file, done) {
                            $(elem).find("text").hide();
                            var t = $(elem).find("div.dz-preview").length;
                            if (t > 1) {
                                $(elem).find("div.dz-preview").each(function (i, value) {
                                    if (t - 1 != i) {
                                        value.remove();
                                    }
                                });
                            }
                            $(elem).parent().find(".label-error").addClass("hide");
                            setTimeout(function () {
                                $(elem).parent().find(".archivo").val($(file.previewElement).find("img").attr("src"))
                            }, 1000);
                        },
                        error: function (data) {
                            $(".eliminar-foto").trigger("click");
                            $(elem).parent().find(".archivo").val();
                            $(elem).parent().find(".label-error").removeClass("hide");
                        },
                        acceptedFiles: "image/*"
                    });

                    var prom = $(elem).data("producto");
                    if (prom) {// Create the mock file:
                        var file = $("#" + prom).find(".archivo").attr("value");
                        if (file != null && file != "") {
                            var mockFile = {name: "Filename", size: 12345};
                            dropzone.emit("addedfile", mockFile);
                            dropzone.emit("thumbnail", mockFile, file);
                            dropzone.emit("complete", mockFile);
                            $(elem).find("text").hide();
                        }
                    }
                });

                $(".upload text").on("click", function (e) {
                    $(e.currentTarget.parentNode).trigger("click");
                });

                $(".eliminar-foto").on("click", function (elm) {
                    var $btn = $(elm.currentTarget);
                    var $prom = $("#" + $btn.data("producto"));
                    $prom.find(".upload div.dz-preview").remove();
                    $prom.find("text").show();
                    $prom.find(".archivo").val("");
                    $prom.parent().find(".label-error").addClass("hide");
                });

                $(".detail-temp").html($(".detail").html());

                $("#tipo_producto").on("change", function () {
                    $(".detail").removeClass("hide");
                    $(".detail").html($(".detail-temp .form-tipo-producto.producto-" + $(this).val()).html());
                    $(".detail .autocomplate-disenador").autocomplete({
                        source: server + "index.php/proveedor/escaparate/auto/disenador/" + $(this).val(),
                        minLength: 2
                    });
                    $(".detail .autocomplate-temporada").autocomplete({
                        source: server + "index.php/proveedor/escaparate/auto/temporada/" + $(this).val(),
                        minLength: 2
                    });
                });

                $(".autocomplate-disenador").autocomplete({
                    source: server + "index.php/proveedor/escaparate/auto/disenador/" + $("#tipo_producto").val(),
                    minLength: 2
                });

                $(".autocomplate-temporada").autocomplete({
                    source: server + "index.php/proveedor/escaparate/auto/temporada/" + $("#tipo_producto").val(),
                    minLength: 2
                });

            });
        </script>
    </body>

</html>