<html>
<head>
    <?php $this->view("proveedor/header") ?>
    <style>
        .card-title {
            font-size: 12px
        }

        .promociones .collapsible .collapsible-header i {
            font-size: 2em
        }

        .collection-item.avatar {
            padding-left: 20px !important;
        }

        .collapsible-body p {
            padding: 10px;
        }

        .dz-image img {
            width: 200px;
            height: auto;
            margin: 0px auto;
            position: relative;
            display: block;
        }
        p{
            font-weight: 400 !important;
        }
}
    </style>
</head>
<body>
    <?php $this->view("proveedor/menu") ?>
    <div class="body-container">
        <div class="row">
            <?php $this->view("proveedor/escaparate/menu") ?>
            <div class="col s12 m8 l9">
                <?php $this->view("proveedor/mensajes") ?>
                <section class="intro-section">
                    <h5>Promociones</h5>
                    <div class="card-panel blue lighten-5">
                        Puede añadir promociones especiales al escaparate de su empresa.
                        Cuanto mejores sean las ofertas y descuentos que anuncie mayor
                        inter&eacute;s tendr&aacute;n los novios en sus servicios y recibir&aacute; m&aacute;s
                        solicitudes de presupuesto.
                    </div>

                    <div class="card-panel">
                        <div class="row">
                            <div class="col m4 s12">
                                <b> Descuento especial para novios en japybodas.com</b>
                                Ofrezca a los novios un descuento por contratar sus servicios a trav&eacute;s de japybodas.com
                            </div>
                            <div class="col m8 s12">
                                <form class="form-validate " method="POST"
                                      action="<?php echo base_url() ?>index.php/proveedor/escaparate/promociones/update_desc">
                                    <div class="col m4 s6">
                                        <p style="display:flex;align-items: center;">
                                            <input name="desc" <?php echo($empresa->descuento == 3 ? "checked" : "") ?>
                                                   value="3" type="radio" id="desc-3"/>
                                            <label style="display:flex;align-items: center;" for="desc-3">
                                                <i style="margin-right: 5px;color:#f6b268;"
                                                   class="material-icons md-48">style</i><span>3%</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col m4 s6">
                                        <p style="display:flex;align-items: center;">
                                            <input name="desc" <?php echo($empresa->descuento == 5 ? "checked" : "") ?>
                                                   value="5" type="radio" id="desc-5"/>
                                            <label style="display:flex;align-items: center;" for="desc-5">
                                                <i style="margin-right: 5px;color:#f6b268;"
                                                   class="material-icons md-48">style</i><span>5%</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col m4 s6">
                                        <p style="display:flex;align-items: center;">
                                            <input name="desc" <?php echo($empresa->descuento == 10 ? "checked" : "") ?>
                                                   value="10" type="radio" id="desc-10"/>
                                            <label style="display:flex;align-items: center;" for="desc-10">
                                                <i style="margin-right: 5px;color:#f6b268;"
                                                   class="material-icons md-48">style</i><span>10%</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col m4 s6">
                                        <p style="display:flex;align-items: center;">
                                            <input name="desc" <?php echo($empresa->descuento == 15 ? "checked" : "") ?>
                                                   value="15" type="radio" id="desc-15"/>
                                            <label style="display:flex;align-items: center;" for="desc-15">
                                                <i style="margin-right: 5px;color:#f6b268;"
                                                   class="material-icons md-48">style</i><span>15%</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col m4 s6">
                                        <p style="display:flex;align-items: center;">
                                            <input name="desc" <?php echo($empresa->descuento == 20 ? "checked" : "") ?>
                                                   value="20" type="radio" id="desc-20"/>
                                            <label style="display:flex;align-items: center;" for="desc-20">
                                                <i style="margin-right: 5px;color:#f6b268;"
                                                   class="material-icons md-48">style</i><span>20%</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col m4 s6">
                                        <p style="display:flex;align-items: center;">
                                            <input name="desc" <?php echo($empresa->descuento == 30 ? "checked" : "") ?>
                                                   value="30" type="radio" id="desc-30"/>
                                            <label style="display:flex;align-items: center;" for="desc-30">
                                                <i style="margin-right: 5px;color:#f6b268;"
                                                   class="material-icons">style</i><span>30%</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="col m12 ">
                                        <p style="display:flex;align-items: center;">
                                            <input name="desc" <?php echo($empresa->descuento == 0 ? "checked" : "") ?>
                                                   value="0" type="radio" id="desc-no"/>
                                            <label style="display:flex;align-items: center;" for="desc-no"
                                                   style="margin-bottom: 25px">
                                                No deseo hacer ningun descuento</label>
                                        </p>
                                    </div>
                                    <p>
                                        <button type="submit" class="btn btn-block-on-small dorado-2 pull-right">
                                            Guardar
                                        </button>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="">
                    <div class="row">
                        <h5 class="pull-left">Otras promociones</h5>
                        <p>
                            <button id="btn-crear-promocion" class="btn btn-block-on-small dorado-2 pull-right">
                                Crear promoci&oacute;n
                            </button>
                        </p>
                    </div>
                    <div class="divider"></div>
                </section>
                <section class="crear-promocion hide ">
                    <div class="card-panel" id="nuevo">
                        <form method="POST"
                              action="<?php echo base_url() ?>index.php/proveedor/escaparate/promociones/new">
                            <h5>Crear promoci&oacute;n</h5>
                            <div class="divider"></div>
                            <div class="row">
                                <div class=" col m4 s12">
                                    <p>
                                        <label>Tipo de promoci&oacute;n</label>
                                        <select name="tipo" class=" browser-default form-control">
                                            <option selected="" disabled="">Elige una opci&oacute;n</option>
                                            <option value="REGALO">Regalo</option>
                                            <option value="OFERTA">Oferta</option>
                                            <option value="DESCUENTO">Descuento</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col m4 s12" style="">
                                    <p>
                                        <label>Nombre de la promoci&oacute;n</label>
                                        <input name="nombre" class="form-control validate[required]">
                                    </p>
                                </div>
                                <div class="col m4 s12" style="">
                                    <p>
                                        <label>Fecha de Termino</label>
                                        <input name="fecha" class=" datepicker form-control validate[required]">
                                    </p>
                                </div>
                                <div class="col m4 s12" style="">
                                    <p>
                                        <label>Precio Original</label>
                                        <input name="precior"  class="form-control validate[required]">
                                    </p>
                                </div>
                                <div class="col m4 s12" style="">
                                    <p>
                                        <label>Precio con Descuento</label>
                                        <input name="preciod" class="form-control validate[required]">
                                    </p>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col m8 s12">
                                    <label>Descripci&oacute;n de la promoci&oacute;n</label>
                                    <textarea name="descripcion" class="editable"></textarea>
                                </div>
                                <div class="col m4 s12">
                                    <div class="card-panel z-depth-0 valign-wrapper upload " id="upload">
                                        <text style="width: 100%;text-align: center;color: gray;"><i
                                                    class="fa fa-upload fa-2x valign "></i><br>
                                            Suelte la imagen o haga clic aqu&iacute; para cargar.
                                        </text>
                                    </div>
                                    <input name="file" type="hidden" class="archivo">
                                    <p class="error-foto">

                                    </p>
                                    <button type="button" class="btn dorado-2 eliminar-foto" style="width: 100%"
                                            data-promocion="nuevo">
                                        Eliminar Foto
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn dorado-2 pull-right">
                                    Guardar
                                </button>
                            </div>
                        </form>
                    </div>
                </section>
                <section class="promociones">
                    <?php if ($promociones) { ?>
                        <ul class="collapsible collection" data-collapsible="accordion">
                            <?php foreach ($promociones as $key => $p) { ?>
                                <li id="<?php echo $p->id_promocion ?>">
                                    <div class="collapsible-header collection-item avatar">
                                        <?php echo icon_tipo_promocion($p->tipo) ?>
                                        <!--<i class="fa fa-tags" style="font-size: 2em"></i>-->
                                        <span class="title"
                                              style="    padding-left: 10px;"><b><?php echo $p->nombre ?></b></span>
                                        <p style="    padding-left: 10px;" class="truncate">
                                            <small><?php echo strip_tags($p->descripcion) ?></small>
                                        </p>
                                        <p style="padding-left: 78px;">
                                            <small>
                                                <?php echo $p->descargas ?> Descargas
                                            </small>
                                        </p>
                                        <span class="secondary-content"><?php echo estado_promocion($p->fecha_fin) ?></span>
                                    </div>
                                    <div class="collapsible-body" style="padding: 0px 20px;">
                                        <form method="POST"
                                              action="<?php echo base_url() ?>index.php/proveedor/escaparate/promociones/update/<?php echo $p->id_promocion ?>">
                                            <div class="row" style="margin-bottom: 0px">
                                                <div class=" col m4 s12">
                                                    <p>
                                                        <label>Tipo de promoci&oacute;n</label>
                                                        <select name="tipo" class=" browser-default form-control">
                                                            <option disabled="">Elige una opci&oacute;n</option>
                                                            <option value="REGALO" <?php echo $p->tipo == "REGALO" ? "selected" : "" ?> >
                                                                Regalo
                                                            </option>
                                                            <option value="OFERTA" <?php echo $p->tipo == "OFERTA" ? "selected" : "" ?> >
                                                                Oferta
                                                            </option>
                                                            <option value="DESCUENTO" <?php echo $p->tipo == "DESCUENTO" ? "selected" : "" ?>>
                                                                Descuento
                                                            </option>
                                                        </select>
                                                    </p>
                                                </div>
                                                <div class="col m4 s12" style="">
                                                    <p>
                                                        <label>Nombre de la promoci&oacute;n</label>
                                                        <input name="nombre" value="<?php echo $p->nombre ?>"
                                                               class="form-control validate[required]">
                                                    </p>
                                                </div>
                                                <div class="col m4 s12" style="">
                                                    <p>
                                                        <label>Fecha de Termino</label>
                                                        <input data-value="<?php echo $p->fecha_fin ?>" name="fecha"
                                                               class=" datepicker form-control validate[required]">
                                                    </p>
                                                </div>
                                                <div class="col m4 s12" style="">
                                                    <p>
                                                        <label>Precio Original</label>
                                                        <input name="precior" value="<?php echo $p->precio_original ?>"
                                                            class="form-control validate[required]">   
                                                    </p>
                                                </div>
                                                <div class="col m4 s12" style="">
                                                    <p>
                                                        <label>Precio con Descuento</label>
                                                        <input name="preciod" value="<?php echo $p->precio_desc ?>"
                                                               class="form-control validate[required]">
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col m8 s12">
                                                    <label>Descripci&oacute;n de la promoci&oacute;n</label>
                                                    <textarea name="descripcion"
                                                              class="editable"><?php echo $p->descripcion ?></textarea>
                                                </div>
                                                <div class="col m4 s12">
                                                    <div class="card-panel z-depth-0 valign-wrapper upload "
                                                         data-promocion="<?php echo $p->id_promocion ?>">
                                                        <text style="width: 100%;text-align: center;color: gray;"><i
                                                                    class="fa fa-upload fa-2x valign "></i><br>
                                                            Suelte la imagen o haga clic aqu&iacute; para cargar.
                                                        </text>
                                                    </div>
                                                    <input name="file" type="hidden" class="archivo"
                                                           value="data:<?php echo $p->mime_imagen ?>;base64,<?php echo base64_encode($p->imagen) ?>">
                                                    <p class="error-foto">
                                                    </p>       
                                                    <button type="button" class="btn dorado-2 eliminar-foto"
                                                            style="width: 100%"
                                                            data-promocion="<?php echo $p->id_promocion ?>">
                                                        Eliminar Foto
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <button type="submit"
                                                        class="btn btn-block-on-small dorado-2 pull-right">
                                                    Guardar
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <div id=" info-sin-promociones" class="card-panel valign-wrapper" style="height: 200px">
                            <div class=" valign center" style="width: 100%">
                                <i class="fa fa-info fa-3x"></i>
                                <br>
                                No tienes promociones registradas, te recomendamos registrar promociones para aumentar
                                el n&uacute;mero de solicitudes.
                            </div>
                        </div>
                    <?php } ?>
                </section>
            </div>
        </div>
    </div>
    <?php $this->view("proveedor/footer") ?>
    <script>
        $(document).ready(function () {
            init_tinymce_mini("textarea.editable");
            $("form").validationEngine();
            var options = es_datepicker;
            options.min = "now";
            $('.datepicker').pickadate(options);
            $('.datepicker').each(function (i, elem) {
                var val = $(elem).data("value");
                if (val) {
                    $(elem).pickadate("select", val, {format: 'yyyy-mm-dd'});
                }
            });
            $(".upload").each(function (i, elem) {
                var dropzone = new Dropzone(elem, {
                    url: "<?php echo base_url() ?>index.php/proveedor/escaparate/fotossad3",
                    method: "POST",
                    paramName: "files", // The name that will be used to transfer the file
                    maxFilesize: 1, // MB
                    thumbnailWidth: 500,
                    thumbnailHeight: null,
                    uploadMultiple: false,
                    createImageThumbnails: true,
                    accept: function (file, done) {
                        $(elem).find("text").hide();
                        var t = $(elem).find("div.dz-preview").length;
                        if (t > 1) {
                            $(elem).find("div.dz-preview").each(function (i, value) {
                                if (t - 1 != i) {
                                    value.remove();
                                }
                            });
                        }
                        setTimeout(function () {
                            $(elem).parent().find(".archivo").val($(file.previewElement).find("img").attr("src"));
                        }, 1000);
                    },
                    error: function (data) {
                        $(elem).find("text").show();
                        $(elem).parent().find(".error-foto").addClass("red-text");
                        $(elem).parent().find(".error-foto").html("La imagen es demasiado grande");
                        $(elem).parent().find(".dz-preview").remove();
                        setTimeout(function () {
                            $(elem).parent().find(".error-foto").removeClass("red-text");
                            $(elem).parent().find(".error-foto").html("");
                        }, 15000);
                    },
                    acceptedFiles: "image/*"
                });

                var prom = $(elem).data("promocion");
                if (prom) {// Create the mock file:
                    var file = $("#" + prom).find(".archivo").attr("value");
                    if (file != null && file != "") {
                        var mockFile = {name: "Filename", size: 12345};
                        dropzone.emit("addedfile", mockFile);
                        dropzone.emit("thumbnail", mockFile, file);
                        dropzone.emit("complete", mockFile);
                        $(elem).find("text").hide();
                    }
                }
            });


            $(".upload text").on("click", function (e) {
                $(e.currentTarget.parentNode).trigger("click");
            });


            $(".eliminar-foto").on("click", function (elm) {
                var $btn = $(elm.currentTarget);
                var $prom = $("#" + $btn.data("promocion"));
                $prom.find(".upload div.dz-preview").remove();
                $prom.find("text").show();
                $prom.find(".archivo").val("");
            });

            $("#btn-crear-promocion").on("click", function () {
                $("section.crear-promocion").toggleClass("hide");
            });

        });


    </script>
</body>

</html>