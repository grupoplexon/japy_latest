<html>
    <head>
        <?php $this->view("proveedor/header") ?>
        <style>
            .card-title {
                font-size: 14px !important;
            }

            .imagen img, .imagen video {
                width: auto !important;
                height: 12rem;
                margin: 0 auto;
            }

            .imagen div.card-image {
                background: #8C8D8F;
            }

            .imagen {
                transition: 1s;
            }

            .imagen.delete {
                opacity: 0;
                transform: scale(0.1, 0.1);
            }

            .imagen.dz-error {
                display: none;
            }

            @media screen and (max-width: 425px) {
                .imagen img, .imagen video {
                    width: 100% !important;
                }
            }
        </style>
    </head>
    <body>
        <?php $this->view("proveedor/menu") ?>
        <div class="body-container">
            <div class="row">
                <?php $this->view("proveedor/escaparate/menu") ?>
                <div class="col s12 m8 l9">
                    <?php $this->view("proveedor/mensajes") ?>
                    <section class="intro-section">
                        <h5>Galer&iacute;a de Videos</h5>
                        <ul>
                            <li>
                                <i class="fa fa-check green-text"></i>S&oacute;lo se permiten videos en formato MP4 O MOV.
                                Cada video debe tener un peso m&aacute;ximo de 500 Mb.
                            </li>
                        </ul>
                    </section>
                    <section>
                        <div class="card-panel z-depth-0 valign-wrapper upload " id="upload">
                            <text style="width: 100%;text-align: center;color: gray;"><i
                                        class="fa fa-upload fa-2x valign "></i><br>
                                Suelte los archivos o haga clic aqu&iacute; para cargar.
                            </text>
                        </div>
                        <h5 id="barra" style="display: none">Cargando archivo, por favor espere...</h5>
                        <div class="container-preview">
                        </div>
                    </section>
                    <section>
                        <div id="mensaje-error" class=" hide card-panel red lighten-2 white-text">
                            <i class="fa fa-warning"></i> Este video no es valido, verifica que no sobrepase el tamaño m&aacute;ximo
                            y su nombre no sea mayor a 8 caracteres.
                        </div>
                    </section>
                    <section class="container-imagenes">
                        <?php if ($galeria) { ?>
                            <?php foreach ($galeria as $key => $imagen) { ?>
                                <div id="<?php echo $imagen->id_galeria ?>" class="imagen col s12 m4 l3">
                                    <div class="card">
                                        <div class="card-image waves-effect waves-block waves-light">
                                            <video class="activator" src="<?php echo $imagen->url ?>" style="object-fit: fill;height: 15rem;max-height: 15rem!important"></video>
                                        </div>
                                        <div class="card-content"  style="display: none">
                                            <span class="card-title activator grey-text text-darken-4 truncate" style="line-height: initial">
                                                <?php echo $imagen->descripcion ? $imagen->descripcion : "" ?>
                                            </span>
                                        </div>
                                        <div class="card-reveal" style="overflow: hidden">
                                            <span class="card-title grey-text text-darken-4">Editar<i class="material-icons right">close</i></span>
                                            <p>
                                                <a class="btn-flat btn-block center-align" target="_blank"
                                                   href="<?php echo $imagen->url ?>">
                                                    Ver video
                                                </a>
                                            </p>
                                            <p>
                                                <a class="waves-effect waves-light btn modal-trigger no-margin-element btn-block btnDescription"
                                                   href="#description-modal"
                                                   data-id="<?php echo $imagen->id_galeria ?>"
                                                   style="color: white;box-shadow: none">Descripción</a>
                                            </p>
                                            <p>
                                                <button data-id="<?php echo $imagen->id_galeria ?>"
                                                        class="btn-flat btn-block delete red white-text"
                                                        style="width: 104%">
                                                    Eliminar
                                                </button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </section>
                </div>
            </div>
        </div>

        <div id="template-imagen" class="template hide">
            <div class="imagen col s12 m4 l3 dz-preview dz-file-preview">
                <div class="card">
                    <div class=" dz-details card-image waves-effect waves-block waves-light">
                        <img class="activator" data-dz-thumbnail>
                    </div>
                    <div class="card-content"  style="display: none">
                        <span class="card-title activator grey-text text-darken-4 truncate">
                            <text data-dz-name></text>
                        </span>
                    </div>
                    <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Editar<i
                                class="material-icons right">close</i></span>
                        <p>
                            <a target="_blank" class=" ver-video btn-flat btn-block center-align" href="">
                                Ver video
                            </a>
                        </p>
                        <p>
                            <a class="waves-effect waves-light btn modal-trigger no-margin-element btn-block btnDescription "
                               href="#description-modal"
                               data-id=" "
                               style="color: white;box-shadow: none">Descripción</a>
                        </p>
                        <p>
                            <button class="btn-flat btn-block delete red white-text" style="width: 104%">
                                Eliminar
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->view("proveedor/footer") ?>
        <script>
            var barra = document.getElementById("barra");
            $(document).ready(function() {
                $('#upload').dropzone({
                    url: "<?php echo base_url() ?>proveedor/escaparate/videos",
                    previewTemplate: document.getElementById('template-imagen').innerHTML,
                    method: 'POST',
                    paramName: 'files', // The name that will be used to transfer the file
                    maxFilesize: 500, // MB
                    uploadMultiple: false,
                    createImageThumbnails: true,
                    accept: function(file, done) {
                        $(file.previewElement).find('.delete').on('click', delete_img);
                        $(file.previewElement).find('.nombre').on('change', update_img);
                        $(file.previewElement).find('.nombre').val(file.name);
                        $('#mensaje-error').addClass('hide');
                        barra.style.display = "block";
                        if (file.type.indexOf('video') < 0) {
                            done();
                        }
                        else {
                            done();
                        }
                    },
                    success: function(file, done) {
                        let resp = JSON.parse(file.xhr.response);
                        $(file.previewElement).attr('id', resp.data.id);
                        $('.btnDescription').attr('data-id',resp.data.id);
                        $(file.previewElement).find('img').remove();

                        var video = document.createElement('video');
                        $(file.previewElement).find('.card-image').append(video);
                        $(file.previewElement).find('video').attr('src', resp.data.url);
                        $(file.previewElement).find('video').addClass('activator');
                        $(file.previewElement).find('video').css({'object-fit':'fill','height':'15rem','max-height':'15rem'});
                        $(file.previewElement).find('.ver-video').attr('href', resp.data.url);
                        $(file.previewElement).find('.delete').attr('data-id', resp.data.id);
                        $(file.previewElement).find('.nombre').attr('data-id', resp.data.id);
                        barra.style.display = "none";
                    },
                    error: function(data, xhr) {
                        $(data.previewElement).remove();
                        if (data.size > 500 * 1024 * 1024) {
                            swal('¡Error!', 'Solo puedes videos de maximo 500 MB', 'error');
                        }
                        else if (!data.type.match('video.*')) {
                            swal('¡Error!', 'Solo puedes subir videos', 'error');
                            barra.style.display = "none";
                        }
                        else if (xhr.code == 403) {
                            swal('¡Advertencia!', 'Llegaste al máximo de videos permitidos', 'warning');
                            barra.style.display = "none";
                        }
                        else {
                            swal('¡Error!', 'Oops ocurrio un error, intentalo de nuevo mas tarde', 'error');
                        }
                    },
                    acceptedFiles: 'video/mp4,video/mov',
                    init: function() {
                        $('#up');
                    },
                    previewsContainer: '.container-preview',
                });
                $('.dz-details').on('click', function(e) {
                    $(e.currentTarget.parentNode).trigger('click');
                });
                $('#upload text').on('click', function(e) {
                    $(e.currentTarget.parentNode).trigger('click');
                });
                $('.imagen .delete').on('click', delete_img);
                $('.imagen .nombre').on('change', update_img);
            });

            function update_img(elem) {
                var e = elem.currentTarget;
                var id = $(e).attr('data-id');
                $.ajax({
                    url: "<?php echo base_url() ?>proveedor/escaparate/videos",
                    method: 'POST',
                    data: {
                        id: id,
                        method: 'UPDATE',
                        nombre: $(e).val(),
                    },
                    success: function(resp) {
                        $('#' + id).find('span.card-title.truncate').text($(e).val());
                        $(e).parent().addClass('has-success');
                        setTimeout(function() {
                            $(e).parent().removeClass('has-success');
                        }, 1500);
                    },
                    error: function() {
                        $(e).parent().addClass('has-error');
                        setTimeout(function() {
                            $(e).parent().removeClass('has-error');
                        }, 1500);
                    },
                });
            }

            function delete_img(elem) {
                swal({
                    title: 'Estas seguro que quieres borrar el video?',
                    icon: 'error',
                    buttons: ['Cancelar', true],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var e = elem.currentTarget;
                        var id = $(e).data('id');
                        $.ajax({
                            url: "<?php echo base_url() ?>proveedor/escaparate/videos",
                            method: 'POST',
                            data: {
                                id: id,
                                method: 'DELETE',
                            },
                            success: function(resp) {
                                var img = $('#' + id);
                                img.addClass('delete');
                                setTimeout(function() {
                                    $('#' + id).remove();
                                }, 1050);
                            },
                            error: function() {
                                console.log('error');
                            },
                        });
                    }
                });

            }

        </script>
    </body>

</html>