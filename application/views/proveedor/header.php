<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php
$categoryTitle = "- Proveedores";
if ( ! isset($isSector)) {
    $categoryTitle = isset($category) ? "-  $category->name" : "";
}
?>
<title>BrideAdvisor <?php echo $categoryTitle ?></title>
<link rel="icon" href="<?php echo base_url() ?>dist/img/baIcon.png">
<link href="<?php echo base_url() ?>dist/img/clubnupcial.png" rel="icon" sizes="16x16">

<noscript id="deferred-styles">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons|Raleway:300,400,500,600,700">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/nouislider.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/materialize.min.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/ghpages-materialize.css"/> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/estilo.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/proveedor.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/presupuesto.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url() ?>dist/tinymce/skins/lightgray/skin.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url() ?>dist/tinymce/skins/lightgray/content.inline.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url() ?>dist/tinymce/skins/lightgray/content.inline.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url() ?>dist/css/jquery-ui/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url() ?>dist/css/jquery-ui/jquery-ui.theme.min.css"/>
    <link href="<?php echo $this->config->base_url() ?>dist/css/iconos-proveedor.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/brideadvisor/proveedores.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/swipebox/src/css/swipebox.min.css">
</noscript>

<style>
    .collection-item {
        padding-left: 10px !important;
    }

    .collection-item i {
        font-size: 20px;
    }

    .collection .collection-item.active {
        background-color: #F8DADF !important;
        color: #000 !important;
    }

    .starrange {
        display: flex;
        align-items: center;
        vertical-align: bottom;
    }

    .starrange h4 {
        margin: 0;
        margin-left: 1rem;

    }

    .starrange i {
        font-size: 1.3rem;
    }
</style>

<script src="<?php echo base_url() ?>dist/js/jquery-2.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/nouislider.min.js" type="text/javascript" async></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js" async></script>

<script type='text/javascript' data-cfasync='false'>
    window.purechatApi = {
        l: [], t: [], on: function() {
            this.l.push(arguments);
        },
    };
    window._mfq = window._mfq || [];

    (function() {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function(e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({c: 'f3f97b28-73ac-4929-a04d-527e409169b7', f: true});
                done = true;
            }
        };

        var mf = document.createElement('script');
        mf.type = 'text/javascript';
        mf.async = true;
        mf.src = '//cdn.mouseflow.com/projects/4315bf06-815c-4f86-b787-cb87d939a11e.js';
        document.getElementsByTagName('head')[0].appendChild(mf);

        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById('deferred-styles');
            var replacement = document.createElement('div');
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement);
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        if (raf) {
            raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        }
        else {
            window.addEventListener('load', loadDeferredStyles);
        }
    })();
</script>
<?php
setlocale(LC_MONETARY, 'en_US');
setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');
?>