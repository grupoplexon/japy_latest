<html lang="es" xml:lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<?php $this->view("proveedor/menu") ?>
    <?php $this->view("proveedor/header");
    $this->view("general/newheader"); ?>
</head>

<style>
    h5{
        color: #767676 !important;
        font-weight: 600 !important;
    }
    h4{
        color: #767676 !important;
    }
</style>
<body >

<img src="<?php echo base_url() ?>dist/img/proveedor.png" styl="" class="portada">
<div class="body-container">
    <?php// if ($porcentaje["porcentaje"] < 100) : ?>
        <?php // $this->load->view("proveedor/alert-preguntas.php"); ?>
    <?php // endif ?>
    <!--     SECCION INFORMACION       -->
    <br><br>
    <h4 style="text-align: center;margin: 30 34px;" id="indicatorsTitle">INDICADORES DE LOS ÚLTIMOS 7 DÍAS</h4>
    <div class="row container" id="indicators">

    </div>

    <!-- SECCION PANELES -->
    <div class="row">
        <!--            <h4>Proveedor: --><?php //echo $tipo->nombre_tipo_proveedor ?><!--</h4>-->
        <!-- <div class="divider"></div> -->
    </div>
    <hr>
    <div class="row center-align">
        
        <div class="col m7 l7 s12">
            <!--   ESTADISTICAS  -->
            <div class="col s12 ">
                <h5>ESTADÍSTICAS</h5>
                <div class="card-panel z-depth-0 left-align">
                    <b><i class="fa fa-eye"></i> Visitas a tu Escaparate
                        <small class="pull-right grey-text">&Uacute;ltimos 12 meses</small>
                    </b>
                    <div id="estadisticas" style="width: 100%; min-height: 350px">
                    </div>
                    <div class="col m6 s12 center-align">
                        <div class="col m6 s12">
                            <h5 class="total">0</h5>
                        </div>
                        <div class="col m6 s12">
                            Ultimos 12 meses
                        </div>
                    </div>
                    <div class="col m6 s12">
                        La gr&aacute;fica muestra el n&uacute;mero de visitas a tu escaparate de novios interesados
                        en tus servicios
                    </div>
                    <p>
                        <a class="dorado-2-text"
                           href="<?php echo base_url() ?>proveedor/escaparate/promociones">¿C&oacute;mo
                            puedo obtener m&aacute;s visitas?</a>
                    </p>
                </div>
            </div>
            <div class="col s12 ">
                <!-- <div class="divider"></div> -->
                <div class="card-panel z-depth-0 heigth-stat">
                    <div class="row left-align">
                        <b><i class="fa fa-envelope"></i> Solicitudes de presupuesto
                            <small class="pull-right grey-text">&Uacute;ltimos 12 meses</small>
                        </b>
                        <div id="estadisticas-solicitudes" style="width: 100%; min-height: 350px">
                        </div>
                        <div class="col m6 s12 center-align">
                            <div class="col m6 s12">
                                <h5 class="total">0</h5>
                            </div>
                            <div class="col m6 s12">
                                Ultimos 12 meses
                            </div>
                        </div>
                        <div class="col m6 s12 ">
                            La gr&aacute;fica muestra el n&uacute;mero de solicitudes realizadas por novios interesados
                            en tus servicio
                        </div>
                        <p>¿C&oacute;mo puedo recibir mas solicitudes? </p>
                        <div class="col m8 s12">
                            <a class="dorado-2-text"
                               href="<?php echo base_url() ?>proveedor/escaparate/fotos">Completa y mejora tu
                                galer&iacute;a de videos</a>
                            <a class="dorado-2-text"
                               href="<?php echo base_url() ?>proveedor/escaparate/promociones">Promociona tus
                                eventos</a>
                        </div>
                        <div class="col m4 s12">
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col s12 ">
                <div class="divider"></div>
                <div class="card-panel z-depth-0 ">
                    <b><i class="fa fa-eye"></i> Click en ver tel&eacute;fono
                        <small class="pull-right grey-text">&Uacute;ltimos 12 meses</small>
                    </b>
                    <div id="estadisticas-telefono" style="width: 100%; min-height: 350px">
                    </div>
                    <div class="col m6 s12 center-align">
                        <div class="col m6 s12">
                            <h5 class="total">0</h5>
                        </div>
                        <div class="col m6 s12">
                            Ultimos 12 meses
                        </div>
                    </div>
                    <div class="col m6 s12">
                        Esta gr&aacute;fica muestra el n&uacute;mero de veces que los novios interesados en tus
                        servicios vieron tu n&uacute;mero de tel&eacute;fono.
                    </div>
                    <p>
                        <a class="dorado-2-text" href="<?php echo base_url() ?>proveedor/escaparate/">¿C&oacute;mo
                            puedo obtener m&aacute;s llamadas?</a>
                    </p>
                </div>
            </div> -->
        </div>
        <div class="col l4 offset-l1 m4 s12">
            <!--   COMPLETAR PERFIL   -->
            <div class="col s12">
                <h5>COMPLETA TU PERFIL</h5>
                <!-- <div class="divider"></div> -->
                <div class="card-panel z-depth-0">
                    <div class="row">
                        <div class="col s12 l12">
                            <div data-por="<?php echo $porcentaje['porcentaje'] ?>" id="porcentaje-perfil">
                            </div>
                        </div>
                        <div class="col s12 l12 left-align">
                            <?php if ($porcentaje['porcentaje'] < 100) { ?>
                                <h6><b>Completa tu perfil para obtener mejores resultados</b></h6>
                            <?php } else { ?>
                                <h6><b>¡Haz Completado tu perfil !</b></h6>
                            <?php } ?>
                            <p style="font-weight: 400;">
                                Con un anuncio m&aacute;s completo tendras mayor posibilidades de que las
                                parejas te contacten para contratar tus servicio
                            </p>
                            <br>
                            <?php if ($porcentaje['porcentaje'] < 100) { ?>
                                <p class="center-align" style="font-weight: 400;">
                                <!-- /<?php echo($porcentaje['redireccionar'][0]); ?> -->
                                    <a href="<?php echo base_url() ?>proveedor/escaparate" 
                                       class="btn dorado-3 waves-effect ">
                                        Completar perfil
                                    </a>
                                </p>
                            <?php } else { ?>
                                <br>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--   Ultimas recomendaciones   -->
            <div class="col s12 recomendaciones" >
                <h5>ÚLTIMAS RECOMENDACIONES</h5>
                <div class="divider"></div>
                <?php if ($recomendaciones) { ?>
                    <?php foreach ($recomendaciones as $key => $r) { ?>
                        <div class="card-panel z-depth-1 ">
                            <div class="row">
                                <div class="col s2">
                                    <img src="<?php echo base_url() ?>perfil/foto/<?php echo $r->id_usuario ?>"
                                         class="responsive-img circle">
                                    <label style="font-size: 8px; text-transform: uppercase;padding: 2px; margin-top: 5px; width: 100%;display: block; border-radius: 3px"
                                           class=" white-text center <?php echo($r->visible ? "green" : "grey") ?>">
                                        <?php echo($r->visible ? "publicado" : "no publicado") ?>
                                    </label>
                                </div>
                                <div class="col s10">
                                    <div class="col s12 ">
                                        <?php echo $r->nombre ?>
                                        <small>
                                            <?php echo relativeTimeFormat($r->fecha_recomendacion) ?>
                                        </small>
                                        <small class="pull-right">
                                            <i class="fa fa-genderless"></i>
                                            <?php
                                            $now        = new DateTime("now");
                                            $fecha_boda = new DateTime($r->fecha_boda);
                                            ?>
                                            <?php if ($fecha_boda < $now) { ?>
                                                Se cas&oacute; el <?php echo $fecha_boda->format("d-m-Y") ?>
                                            <?php } else { ?>
                                                Se casa el <?php echo $fecha_boda->format("d-m-Y") ?>
                                            <?php } ?>
                                        </small>
                                    </div>
                                    <div class="col s6">
                                        <div class="row">
                                            <div class="col s12 m6 text-recomendacion ">
                                                Calidad del servicio:
                                            </div>
                                            <div class="col s12  m6">
                                                <div class="progress amber accent-2">
                                                    <div class="determinate dorado-2"
                                                         style="width: <?php echo($r->calidad_servicio * 20) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m6 text-recomendacion ">
                                                Respuesta:
                                            </div>
                                            <div class="col s12 m6">
                                                <div class="progress amber accent-2">
                                                    <div class="determinate dorado-2"
                                                         style="width: <?php echo($r->respuesta * 20) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m6 text-recomendacion ">
                                                Relaci&oacute;n calidad/precio:
                                            </div>
                                            <div class="col s12 m6">
                                                <div class="progress amber accent-2">
                                                    <div class="determinate dorado-2"
                                                         style="width: <?php echo($r->relacion_calidad * 20) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s6">
                                        <div class="row">
                                            <div class="col s12 m6 text-recomendacion ">
                                                Flexibilidad:
                                            </div>
                                            <div class="col s12  m6  ">
                                                <div class="progress amber accent-2">
                                                    <div class="determinate dorado-2"
                                                         style="width: <?php echo($r->flexibilidad * 20) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m6 text-recomendacion ">
                                                Profesionalismo:
                                            </div>
                                            <div class="col s12  m6  ">
                                                <div class="progress amber accent-2">
                                                    <div class="determinate dorado-2"
                                                         style="width: <?php echo($r->profesionalismo * 20) ?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="col s12 text-justify">
                                    <?php echo $r->resena ?>
                                </div>
                                <div class="divider"></div>
                                <div class="col s12">
                                    <p>
                                        <a class=" btn-flat waves-effect waves-pink pull-right">
                                            <i class="fa fa-facebook-official"> </i> Compartir
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <p>
                        <a href="<?php echo site_url("proveedor/recomendaciones") ?>"
                           class="btn dorado-2 pull-right  waves-effect ">
                            Revisar Todas mis recomendaciones
                        </a>
                    </p>
                <?php } else { ?>
                    <div class="card-panel z-depth-1">
                        <p class="center" style="color: #767676; font-weight:400">
                            <i class="material-icons fa-4x" style="font-size:30px;">info_outline</i><br>
                            A&uacute;n no tienes recomendaciones, Pide a tus clientes que te recomienden
                        </p>
                        
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <!-- Components -->
    <div class="panel-info col s12 m4" id="indicatorCard" style="display: none;">
        <div class="card-panel center-align">
            <div class="row">
                <div class="numero fa-2x">
                    <i class="fa"></i>
                    <text class="total-solicitudes"></text>
                </div>
                <div class="col s12">
                    <p>
                        Solicitudes recibidas en los ultimos 12 meses
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<input id="provider-id" type="hidden" value="<?php echo $proveedor->id_proveedor ?>">
<input id="provider-type" type="hidden" value="<?php echo $proveedor->tipo_cuenta ?>">
<input id="baseURL" type="hidden" value="<?php echo base_url() ?>">
<?php $this->view("japy/prueba/footer") ?>
<script>
    var server = $("#baseURL").val();
    var proveedor = $("#provider-id").val();
    const providerType = $("#provider-type").val();
    var meses = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DEC"];
    $(document).ready(function () {
        loadIndicators();
        if (providerType > 0) {
            setInterval(loadIndicators, 10000)
        }
        var c = grafica_porcentaje("porcentaje-perfil");
        c.tooltip.refresh(c.series[0].points[0]);
    });

    function loadIndicators() {
        $.ajax({
            "url": server + "proveedor/home/indicadores/" + proveedor,
            success: function (resp) {
                $("#indicators").empty();
                const week = resp.data.week;
                const year = resp.data.year;

                let indicator = {
                    class: '',
                    amount: '',
                    message: '',
                };

                for (const key of Object.keys(week)) {
                    switch (key) {
                        case 'contact_request':
                            indicator.class = 'fa-envelope green-text';
                            indicator.message = 'Solicitudes recibidas';
                            break;
                        case 'contact_pending_request':
                            indicator.class = 'fa-envelope orange-text';
                            indicator.message = 'Solicitudes pendientes';
                            break;
                        case 'contact_average_time_request':
                            indicator.class = 'fa-envelope yellow-text';
                            indicator.message = 'Tu tiempo promedio de respuesta (minutos)';
                            break;
                        case 'visit':
                            indicator.class = 'fa-users green-text';
                            indicator.message = 'Visitas';
                            break;
                        case 'phone':
                            indicator.class = 'fa-phone green-text';
                            indicator.message = 'Personas vieron tu telefono';
                            break;
                        case 'recommendation':
                            indicator.class = 'fa-thumbs-up green-text';
                            indicator.message = 'Recomendaciones';
                            break;
                        case 'rating':
                            indicator.class = 'fa-star green-text';
                            indicator.message = 'Personas te calificaron';
                            break;
                        case 'appearance_category':
                            indicator.class = 'fa-search green-text';
                            indicator.message = 'Apariciones en categoria';
                            break;
                        case 'appearance_search':
                            indicator.class = 'fa-search green-text';
                            indicator.message = 'Apariciones en busqueda';
                            break;
                    }
                    indicator.amount = week[key];
                    buildIndicator(indicator);
                }

                grafica_estadisticas("#estadisticas", Object.keys(year.visit).map(key => year.visit[key]), meses);
                grafica_estadisticas("#estadisticas-telefono", Object.keys(year.phone).map(key => year.phone[key]), meses);
                grafica_estadisticas("#estadisticas-solicitudes", Object.keys(year.contact_request).map(key => year.contact_request[key]), meses);

            },
            error: function () {
            }
        });
    }

    function buildIndicator(indicator) {
        $('#indicatorsTitle').show();

        $newIndicatorCard = $('#indicatorCard').clone().attr('id', '');

        $newIndicatorCard.find('i').addClass(indicator.class);
        $newIndicatorCard.find('.total-solicitudes').html(indicator.amount);
        $newIndicatorCard.find('p').html(indicator.message);

        $('#indicators').append($newIndicatorCard.show());
    }

    function grafica_porcentaje(elem) {

        var colores = {
            0: "#f44336",
            10: "#ff5722",
            20: "#ff9800",
            40: "#ffc107",
            60: "#cddc39",
            80: "#8bc34a",
            90: "#4caf50",
            100: "#009688",
        };

        var porcentaje = $("#" + elem).data("por");

        var color = "";

        for (var i in colores) {
            if (i < porcentaje) {
                color = colores[i];
            }
        }

        var chart = Highcharts.chart(elem, {
                chart: {
                    type: 'solidgauge',
                    marginTop: 50,
                    backgroundColor: "rgba(0,0,0,0)"
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: '',
                    floting: false,
                    style: {
                        fontSize: '0px'
                    }
                },
                tooltip: {
                    borderWidth: 0,
                    backgroundColor: 'none',
                    shadow: false,
                    style: {
                        fontSize: '16px',
                        opacity: 1,
                    },
                    hideDelay: 60000,
                    pointFormat: '<span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
                    positioner: function (labelWidth, labelHeight) {
                        return {
                            x: 100 - labelWidth / 2,
                            y: 110
                        };
                    }
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                    background: [{// Track for Move
                        outerRadius: '100%',
                        innerRadius: '95%',
                        backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.3).get(),
                        borderWidth: 0
                    }]
                },
                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: []
                },
                plotOptions: {
                    solidgauge: {
                        borderWidth: '10px',
                        dataLabels: {
                            enabled: false
                        },
                        linecap: 'round',
                        stickyTracking: false
                    },
                    series: {
                        events: {
                            show: function () {
                            }
                        }
                    }
                },
                series: [{
                    name: '',
                    borderColor: color,
                    data: [{
                        color: color,
                        radius: '100%',
                        innerRadius: '100%',
                        y: porcentaje
                    }],
                }]
            },
            function callback() {
            });

        return chart;

    }

    function grafica_estadisticas(elem, serie, labels) {
        $(elem).highcharts({
            title: {
                text: '',
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: '',
            },
            xAxis: {
                categories: labels,
            },
            yAxis: {
                title: {
                    text: 'Visitas'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: 'visitas'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: ' ',
                color: "#00BCDD",
                data: serie,
                dataLabels: {}
            }]
        });

        $(elem).parent().find('h5.total').html(serie.reduce((v, i) => (v + i)));
    }
</script>
</body>
</html>