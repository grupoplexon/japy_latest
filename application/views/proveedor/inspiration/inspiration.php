<html lang="es" xml:lang="es">
<head>
    <?php $this->view("proveedor/header") ?>
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- ================== END BASE CSS STYLE ================== -->
</head>
<style>
    .tab a {
        color: white !important;
    }
    .tabs .tab .active {
        background: #1c97b3 !important;
        border-bottom: 5px solid #00bcdd !important;
        transition: .2s ease-in-out;
    }
    @media only screen and (min-width: 768px) {
        input[type=text]:focus:not([readonly]), textarea.nombre:focus {
            border-bottom: 1px solid #00BCDD !important;
            box-shadow: 0 1px 0 0 #00BCDD;
        }

        input[type=text] {
            border: 0px !important;
            border-bottom: 0px !important;
            margin-top: 17px;
        }

        .text-presupuestador {
            font-size: 1.3rem !important;
        }
    }
    html, body
{
	height: 100%;
    width: 100%;
}
.modal {
    position: fixed !important;
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    max-width: 800px !important;
}
.modalImg {
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
}
.modalHead {
    padding-top: 10px !important;
    padding-right: 10px !important;
    padding-bottom: 10px !important;
    padding-left: 10px !important;
}

.tags{
    margin:0;
    padding:0;
    /* position:absolute; */
    right:24px;
    bottom:-12px;
    list-style:none;
}
.tags li, 
.tags a{
    float:left;
    height:24px;
    line-height:24px;
    position:relative;
    font-size:11px;
    margin-block-end: 5px;
}
.tags a{
    margin-left:20px;
    padding:0 10px 0 12px;
    background:#0089e0;
    color:#fff;
    text-decoration:none;
    -moz-border-radius-bottomright:4px;
    -webkit-border-bottom-right-radius:4px; 
    border-bottom-right-radius:4px;
    -moz-border-radius-topright:4px;
    -webkit-border-top-right-radius:4px;    
    border-top-right-radius:4px;    
}
 
/* parte izquierda de cada etiqueta en forma de triangulo */
.tags a:before{
    content:"";
    float:left;
    position:absolute;
    top:0;
    left:-12px;
    width:0;
    height:0;
    border-color:transparent #0089e0 transparent transparent;
    border-style:solid;
    border-width:12px 12px 12px 0;      
}
 
/* y por ultimo un :hover para darle algo mas de "estilo" */
.tags a:hover{
    background:#555;
}   
.tags a:hover:before{
    border-color:transparent #555 transparent transparent;
}
.card-title {
                font-size: 14px !important;
            }
.imagen img {
                height: 17.7rem;
                max-height: 17.7rem !important;
                margin: 0 auto;
                widtth: 100%;
            }

            .imagen div.card-image {
                background: #8C8D8F;
            }

            .imagen {
                transition: 1s;
            }

            .imagen.delete {
                opacity: 0;
                transform: scale(0.1, 0.1);
            }

            .imagen.dz-error {
                display: none;
            }
</style>
<body>
    <?php $this->view("proveedor/menu") ?>
    <?php $provider = Provider::where("id_usuario", $this->session->userdata("id_usuario"))->first(); ?>
    <div class="body-container" style="position: relative">
        <div class="row principal-content presupuesto-screen">
            <div class="col s12 m12 l3 right barra hide-on-small-only">
                <!-- <ul class="collection with-header"
                style="box-shadow: 0 15px 10px 0px rgba(0,0,0,.5), 0 1px 4px 0 rgba(0,0,0,.3), 0 0 40px 0 rgba(0,0,0,.1) inset; height: 100%;"> -->
                    <img src="<?php echo base_url() ?>/dist/img/prueba.jpg" style="width:auto;height:auto;">
                <!-- </ul> -->
            </div>
            <div class="col s12 m12 l9 pull-left" id="presupuesto-content">

                <div class="row" style="margin-top: 8px">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s6 primary-background secondary-text">
                                <a href="#test1" id="btn-tab-presu">Publicar nuevas fotos</a>
                            </li>
                            <li class="tab col s6 primary-background secondary-text">
                                <a href="#tab-pagos" id="btn-tab-pagos">Publicaciones activas</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col s12" style="margin-bottom: 30px">
                        <h4 class="center-align primary-text text-presupuestador" style="font-weight: bold;">
                            NUEVAS OFERTAS
                        </h4>
                    </div> -->
                    <div id="test1" class="col s12">
                        <div class="row">
                            <label style="font-size: 16px; color: black !important;">Sube fotos a tu publicación</label><br>
                            <label style="font-size: 16px; color: black !important;">Has subido:&nbsp;</label>
                            <label id="cuenta" style="font-size: 16px; color: black !important;"></label>
                            <label style="font-size: 16px; color: black !important;">&nbsp;de 10.</label><br>
                            <div class="col s12 m6 l6">
                            <section>
                                <div class="card-panel z-depth-0 valign-wrapper upload " id="upload">
                                    <text style="width: 100%;text-align: center;color: gray;" div="drop"><i class="fa fa-upload fa-2x valign "></i><br>
                                        Haga clic aqu&iacute; para cargar.
                                    </text>
                                </div>
                            </section>
                            <div class="container-preview" id="vistaPrevia">
                            </div>
                            </div>
                            <div class="col s12 m12 l12 ">
                                <label style="font-size: 16px; color: black !important;">Título</label><br>
                                <input class="form-control" type="text" placeholder="Escribe el título de tu publicación" id="title">
                                <label style="font-size: 16px; color: black !important;">Descripción</label><br>
                                <input class="form-control" type="text" placeholder="Escribe la descripción de tu publicación" id="description">
                                <!-- <label style="font-size: 16px; color: black !important;">Etiquetas</label><br>
                                <input class="form-control" type="text" placeholder="Añade etiquetas a tu publicación" id="tags"> -->

                                <div class="form-group row">
                                    <!-- <label class="col-form-label col-md-2">Tags</label> -->
                                    <label style="font-size: 16px; color: black !important;">Tags</label>
                                    <div class="col-md-10">
                                        <select class="tag-selector" multiple="multiple"></select>
                                    </div>
                                </div>

                                <div class="col s12 m6 l3" id="publicar">
                                    <button class="btn btn-block waves-effect waves-light dorado-2 save" disabled="true" id="subir">PUBLICAR AHORA</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="tab-pagos" class="tab2 col s12" style="display: none;">
                        <div class="row">
                            <section class="row container-imagenes">
                                <?php if ($inspiracion) { ?>
                                    <?php foreach ($inspiracion as $key => $imagen) { ?>
                                        <div id="<?php echo $imagen->id_inspiracion ?>"
                                            class="imagen col s8 m1 l3 offset-s2 <?php echo($key >= $provider->allowedImagesQuantity() ? 'hidden' : '') ?>">
                                            <div class="card">
                                                <div class="card-image waves-effect waves-block waves-light image">
                                                    <img class="activator image-card" src="<?php echo $imagen->url ?>" style="object-fit: fill;height: 17.7rem;max-height: 17.7rem!important">
                                                </div>
                                                <div class="card-content center-align" style="display: none">
                                            <span class="card-title activator grey-text text-darken-4 truncate">
                                                <?php echo $imagen->nombre ?>
                                            </span>
                                                </div>
                                                <div class="card-reveal">
                                            <span class="card-title grey-text text-darken-4">Editar
                                                <i class="material-icons right">close</i></span>
                                                <br>
                                                        <button class="btn modal-trigger mostrar" data-target="modal1" id="<?php echo $imagen->id_inspiracion ?>">Información</button>
                                                    <p>
                                                        <button data-id="<?php echo $imagen->id_inspiracion ?>"
                                                                class="btn-flat btn-block delete red white-text" style="width: 104%" id="deleteInsp">
                                                            Eliminar
                                                        </button>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                
                                <!-- Modal Structure -->
                                <div id="modal1" class="modal">
                                    <div class="col m6 s12 l4 modalImg">
                                        <img id="imgInfo" style="width: 100% !important;">
                                    </div>
                                    <div class="col m6 s12 l8">
                                        <div class="col m3 s12 l2 modalHead">
                                            <img class="circule-img" style="background: white" id="logoProvider">
                                        </div>
                                        <div class="col m9 s12 l10 modalImg">
                                            <h6 style="font-weight: bold; color: black;" id="nomproInfo"></h6>
                                            <h6 id="titleInfo" style="color: black; font-size: 20px;"></h6>
                                            <h6 id="fechaInfo" style="color: black;"></h6>
                                        </div>
                                    </div>
                                    <div class="col m6 s12 l8">
                                        <p align="justify" id="descInfo" style="color: black;"></p>
                                        <!-- <div id="divTags">
                                        </div> -->
                                        <ul class="tags col l12 m12 s12" id="divTags">
                                            <li id="divTags">

                                            </li>
                                        </ul>
                                        <div id="divComent">
                                        </div>
                                        <hr>
                                            <i class="fa fa-eye" aria-hidden="true" style="color: black;"></i><strong style="color: black;" id="vistasInfo"></strong>
                                            <i class="fa fa-commenting" aria-hidden="true" style="color: black;"></i><strong style="color: black;" id="contComent"></strong>
                                        <hr>
                                    </div>
                                </div>

                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="template-imagen" class="template hide">
            <div class="imagen col s12 m4 l5 dz-preview dz-file-preview">
                <div class="card">
                    <div class=" dz-details card-image waves-effect waves-block waves-light">
                        <img class="activator" data-dz-thumbnail>
                    </div>
                    <div class="card-content" style="display: none">
                        <span class="card-title activator grey-text text-darken-4 truncate">
                            <text data-dz-name></text>
                        </span>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">
                            Editar
                            <i class="material-icons right">close</i>
                        </span>
                        <p>
                            <button class="btn-flat btn-block delete red white-text" style="width: 104%" id="deletePreview">
                                Eliminar
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css"/>

        

    <?php $this->view("proveedor/footer") ?>
    <input type="hidden" value="<?php echo base_url() ?>" id="baseURL"/>
</body>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/i18n/es.js" type="text/javascript"></script>
<script>
var uploadImg = 0;
setInterval('contador()',1000);
function contador() {
    if(uploadImg!=0 && $("#title").val()!=null && $("#description").val()!=null 
    && $tagSelector.select2('val') && $("#description").val()!="" && $("#title").val()!="") {
        document.getElementById("subir").disabled = false;
    } else {
        document.getElementById("subir").disabled = true;
    }
}
// document.addEventListener('DOMContentLoaded', function() {
//     var elems = document.querySelectorAll('.modal');
//     var instances = M.Modal.init(elems);
//   });

const $tagSelector = $('.tag-selector');
const server = "<?php echo base_url() ?>";
var descripcion
var titulo

    $( document ).ready(function() {
        
        cuentaPublicacion();

        $('.mostrar').click(function() {
            // document.getElementById('divComent').innerHTML='';
            let id = this.id;
            $.ajax({
                url: '<?php echo base_url() ?>'+'proveedor/inspiracion/modalInfo',
                method: 'post',
                //dataType: 'json',
                data: {
                    id_ins: id,
                },
                success: function(response) {
                    var ruta = '<?php echo base_url() ?>'+'/uploads/images/inspiracion/'+response.inspiracion.nombre;
                    document.getElementById("imgInfo").src = ruta;
                    var ruta2 = '<?php echo base_url() ?>'+'uploads/images/'+response.fotoInfo;
                    document.getElementById("logoProvider").src = ruta2;
                    document.getElementById("nomproInfo").innerHTML = response.nomproInfo;
                    document.getElementById("titleInfo").innerHTML = response.inspiracion.titulo;
                    let date = new Date(response.inspiracion.updated_at.replace(/-+/g, '/'));
                    let options = {
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric'
                    };
                    document.getElementById("fechaInfo").innerHTML = date.toLocaleDateString('es-MX', options);
                    document.getElementById("descInfo").innerHTML = response.inspiracion.descripcion;
                    if(response.inspiracion.vistas!=null) {
                        document.getElementById("vistasInfo").innerHTML = '&nbsp;'+response.inspiracion.vistas+'&nbsp;&nbsp;';
                    } else {
                        document.getElementById("vistasInfo").innerHTML = '&nbsp;'+0+'&nbsp;&nbsp;';
                    }
                    var tamaño = response.comentarios.length;
                    document.getElementById("contComent").innerHTML = '&nbsp;'+response.comentarios.length;
                    var i;
                    var comentario="";
                    var comentario2="";
                    for (i = 0; i < tamaño; i++) {
                        var nombre = '<strong style="font-weight: bold; color: black;">'; 
                        nombre += response.comentarios[i]['names']+': '; 
                        nombre += '</strong>'; 

                        var texto = '<strong style="color: black;">'; 
                        texto += response.comentarios[i]['texto']; 
                        texto += '</strong>'; 
                        
                        var salto = '<br>'
                        comentario = comentario + (nombre+texto)+salto;
                    }
                    var tamaño2 = response.tags.length;
                    var x=1;
                    for (i=0; i < tamaño2; i++) {
                        var tag = '<a style="font-weight: bold; color: white; font-size: 13px;">'; 
                        tag += response.tags[i]['tags']; 
                        tag += '</a>';
                        comentario2 = comentario2 + tag;
                    }
                    document.getElementById("divComent").innerHTML = comentario;
                    document.getElementById("divTags").innerHTML = comentario2;
                },
                error: function(e) {
                    console.log("ERROR");
                },
            });
        });

        $('#publicar').on( 'click', 'button', function () {
            // alert(uploadImg);
            // if(uploadImg!=0) {
                let id = this.id;
                titulo = $("#title").val();
                descripcion = $("#description").val();
                tags = $(".tag-selector").val();

                var myDropzone = Dropzone.forElement(".upload");
                myDropzone.processQueue();
            // } else {
                // swal('¡Error!', 'Debe llenar todos los campos', 'error');
            // }
        });

        $('.container-preview').bind('DOMNodeInserted DOMNodeRemoved', function() {
            uploadImg = 1;
        });
        

        $tagSelector.select2({
            ajax: {
                url: server + 'blog/Post',
            },
        });

        $tagSelector.select2({
            tags: true,
            width: '100%',
            placeholder: 'Seleccione una etiqueta',
            allowClear: true,
            language: 'es',
            minimumInputLength: 2,
            ajax: {
                url: server + 'blog/tag/show',
                dataType: 'json',
                type: 'GET',
                quietMillis: 100,
                data: function(term) {
                    return {
                        term: term,
                    };
                },
                processResults: function(data) {
                    return {
                        results: data.map(function(item) {
                            return {
                                text: item.title,
                                id: item.id,
                            };
                        }),
                    };
                },
            },
        });

        $tagSelector.on('change.select2', function(e) {
            let data = {};

            data.tags = $tagSelector.select2('data').map(function(item) {
                return item.text;
            });

            // ESTAS LINEAS GUARDAN UNA NUEVA ETIQUETA SI ESTA NO EXISTIERA
            // $.ajax({
            //     url: server + 'blog/tag/store',
            //     data: data,
            //     method: 'POST',
            //     success: function(resp) {
            //         $('.tag-selector').html('');
            //         resp.forEach(function(item) {
            //             let newOption = new Option(item.title, item.id, true, true);
            //             $tagSelector.append(newOption);
            //         });
            //     },
            //     error: function() {
            //         $('.tag-selector').html('');
            //     },
            // });

        });

        $(".select-dropdown").css("display", "contents");

    });

    
    function cuentaPublicacion() {
        $.ajax({
            url: '<?php echo base_url() ?>'+'proveedor/inspiracion/cuentaPublicacion',
            method: 'post',
            data: {
                data: 'validar',
            },
            success: function(response) {
                $('#cuenta').html(response.publicaciones);
            },
            error: function() {
                console.log("ERROR");
            },
        });
    }

    $('#upload').prop('disabled', true);
    var baseURL = $('#baseURL').val();

    $('#upload').dropzone({
        url: baseURL + 'proveedor/inspiracion/fotos',
        previewTemplate: document.getElementById('template-imagen').innerHTML,
        method: 'POST',
        paramName: 'files', // The name that will be used to transfer the file
        maxFilesize: 6, // MB
        uploadMultiple: false,
        maxFiles: 1,
        createImageThumbnails: true,
        acceptedFiles: 'image/*',
        autoProcessQueue: false,
        previewsContainer: '.container-preview',
        dataType: 'json',
        accept: function(file, done) {
            $(file.previewElement).find('.delete').on('click', delete_img);
            done();
        },
        success:function (file) {
            // console.log("SUCCESS");
            let resp = JSON.parse(file.xhr.response);
            $(".save").attr("id", resp.data.id);
            // console.log(resp.data.id);
            // if(file!=null && titulo!=null && descripcion!=null && $tagSelector.select2('val')!=null) {
                $.ajax({
                    url: '<?php echo base_url() ?>'+'proveedor/inspiracion/guardarDatos',
                    method: 'post',
                    data: {
                        Id: resp.data.id,
                        Titulo: titulo,
                        Descripcion: descripcion,
                        Tags: $tagSelector.select2('val')
                    },
                    success: function(response) {
                        cuentaPublicacion();
                        swal({
                            type: 'success',
                            title: 'Su inspiración fue publicada',
                            showConfirmButton: false,
                        });
                        setTimeout(function(){ location.reload();  }, 1000);
                    },
                    error: function(response) {
                        var img = $('#' + resp.data.id);
                                    img.addClass('delete');
                                    setTimeout(function() {
                                        $('#' + resp.data.id).remove();
                                        if ($imagesContainer.find('.imagen.hidden').length) {
                                            $imagesContainer.find('.imagen.hidden').first().removeClass('hidden');
                                        }
                                        // update_check();
                                    }, 300);
                        swal('¡Error!', 'Solo puede publicar un máximo de 10 inspiraciones', 'error');
                    },
                });
            // } else {
            //     swal('¡Error!', 'Debe completar todos los campos', 'error');
            // }
        },
        error: function(data, xhr) {
            $(data.previewElement).remove();
            if (data.size > 3 * 1024 * 1024) {
                swal('¡Error!', 'Solo puedes subir imagenes de maximo 3 MB', 'error');
            }
            else if (!data.type.match('image.*')) {
                swal('¡Error!', 'Solo puedes subir imagenes', 'error');
            }
            else if (xhr.code == 403) {
                swal('¡Advertencia!', 'Llegaste al máximo de imagenes permitidos, adquiere un nuevo paquete', 'warning');
            }
            else {
                swal('¡Error!', 'Solo puede publicar una imagen a la vez', 'error');
            }
        },
        init: function() {
            this.on('success', function(file, response) {
                
            });
        },
    });

    $('#upload text').on('click', function(e) {
        $(e.currentTarget.parentNode).trigger('click');
    });
    $('.imagen .delete').on('click', delete_img);

    function delete_img(elem) {
                let iddelete = this.id;
                if(iddelete=='deleteInsp') {
                    swal({
                        title: 'Estas seguro que quieres borrar esta imagen?',
                        icon: 'error',
                        buttons: ['Cancelar', true],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            var e = elem.currentTarget;
                            var id = $(e).data('id');
                            const $imagesContainer = $('.container-imagenes');
                            $.ajax({
                                url: "<?php echo base_url() ?>proveedor/inspiracion/fotos",
                                method: 'POST',
                                data: {
                                    id: id,
                                    method: 'DELETE',
                                },
                                success: function(resp) {
                                    cuentaPublicacion();
                                    var img = $('#' + id);
                                    img.addClass('delete');
                                    setTimeout(function() {
                                        $('#' + id).remove();
                                        if ($imagesContainer.find('.imagen.hidden').length) {
                                            $imagesContainer.find('.imagen.hidden').first().removeClass('hidden');
                                        }
                                    }, 300);
                                },
                                error: function() {
                                    swal('¡Error!', 'Oops ocurrio un error, intentalo de nuevo mas tarde', 'error');
                                },
                            });
                            document.getElementById("vistaPrevia").innerHTML='';
                            // location.reload();
                        }
                    });
                } else {
                    swal({
                        title: 'Estas seguro que quieres borrar esta imagen?',
                        icon: 'error',
                        buttons: ['Cancelar', true],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            // document.getElementById("vistaPrevia").innerHTML='';
                            location.reload();
                        }
                    });
                }
            }
</script>

</html>