<?php $lbl_estado = $estado != "ALL" ? "en $estado" : "" ?>
<?php $verMapa = $ver == "map"; ?>
    <html lang="es" xml:lang="es" style="width: 100%;height: 100%">
    <head>
        <?php $this->view("proveedor/header");
        $this->view("general/newheader"); ?>
        <style>
            .gal-provi {
                object-fit: cover;
                width: 110px;
                height: 115px;
            }

            .btn-custom {
                padding: 0 13px !important;
            }

            .btn {
                padding: 0 13px !important;
            }

            .content-text {
                height: 100px !important;
            }
            .text-white {
                color: white;
            }
            body{
                background: white !important;
                min-height: calc(100vh - 85px);
                position: relative;
            }

            h5{
                font-size: 5vh !important;
            }

            .desc {
                top: 6px;
                right: 11px;
                position: absolut !important;
                width: 50px !important;
                height: 34px !important;
                font-size: 17px !important;
                padding-top: 3px;
                padding-left: 32px !important;
                z-index: 99;
            }

            .paginacion {
                justify-content:center;
                text-align:center;
            }

            .promo {
                font-size: 12px;
                padding: 2px;
                background: #ffab40;
                display: inline;
                border-radius: 3px;
                color: white;
            }

            .card-image .card-title {
                width: 100%;
                background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 1%, rgba(2, 0, 0, 0.7) 51%, rgba(2, 0, 0, 0.8) 98%);
                background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 1%, rgba(2, 0, 0, 0.7) 51%, rgba(2, 0, 0, 0.8) 98%);
                background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 1%, rgba(2, 0, 0, 0.7) 51%, rgba(2, 0, 0, 0.8) 98%);
            }

            .gm-style-iw {
                width: 350px !important;
                top: 15px !important;
                left: 0px !important;
                background-color: #fff;
                box-shadow: 0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                border-radius: 2px 2px 10px 10px;
            }

            #iw-container {
                margin-bottom: 10px;
                width: 100%;
            }

            #iw-container .iw-title {
                height: 125px;
                overflow: hidden;
            }

            #iw-container .iw-content {
                font-size: 13px;
                line-height: 18px;
                font-weight: 400;
                margin-right: 0px;
                padding: 5px 5px 0px 15px;
                max-height: 140px;
                overflow-y: auto;
                overflow-x: hidden;
                margin-bottom: 13px;
                overflow: hidden;
            }

            .iw-subTitle {
                font-size: 16px;
                font-weight: 700;
                padding: 5px 0;
            }

            .more-info {
                padding: 0 !important;
            }

            .card-image .card-title {
                background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 1%, rgba(44, 0, 0, 0.5) 100%, rgba(2, 0, 0, 0.8) 98%) !important;
            }

            .cover-screen {
                width: 100%;
                height: 2152px;
                max-width: 1903px;
                background-color: transparent !important;
                position: absolute;
                box-shadow: none;
            }

            .cover-screen:hover {
                box-shadow: none;
            }

            .banner_expo {
                object-fit: cover;
                width: 100%;
                height: auto;
            }

            @media only screen and (max-width: 320px) {
                .provider-image {
                    height: 200px !important;
                }

                .provider-info {
                    height: 45% !important;
                    margin-bottom: 0px;
                }

                .price, .more-info, .capacity {
                    text-align: center !important;
                }

                .provider-par {
                    text-align: justify !important;
                }
            }

            @media only screen and (max-width: 425px) and (min-width: 322px) {

                .card-image img {
                    height: 100% !important;
                }

                .card-action {
                    padding: 10px !important;
                }

                .more-info {
                    font-size: 12px !important;
                    padding: 0;
                }

                .provider-image {
                    max-width: 383px !important;
                }

                .provider-info {
                    height: 35% !important;
                    margin-bottom: 0px;
                }

                .price, .more-info, .capacity {
                    text-align: center !important;
                }

                .provider-par {
                    text-align: justify !important;
                }

            }

            @media only screen and (max-width: 768px) and (min-width: 650px) {
                .card-image {
                    height: 200px !important;
                }

                .card-image img {
                    height: 100% !important;
                }

                .price {
                    font-size: 12px !important;
                }

                .more-info {
                    font-size: 11px !important;
                    padding: 0;
                }

                .galeria a {
                    margin: 0 5px;
                }

            }

            @media only screen and (max-width: 1024px) and (min-width: 1000px) {
                .galeria {
                    overflow: auto !important;
                    justify-content: flex-start !important;
                }

                .galeria a {
                    margin: 0 5px;
                }

                .gal-provi {
                    width: 120px !important;
                    height: 100px !important;
                }

            }

            .descuentos {
                float: left;
                height: 120px!important;
                width: 70px!important;
                z-index: 5;
                position: absolute;
            }

            .btn {
                background-color: #86cad2 !important;
            }
            .btn:hover{
                background-color: #26a69a !important;
            }
        </style>
    </head>
    <body>
<?php
header('Access-Control-Allow-Origin: *');
if ($this->checker->isProveedor()) {
    //$this->view("proveedor/menu");
} else {
    //$this->view("general/menu");
   // $this->load->view("general/head");
    $this->view("principal/head");
    $this->view("general/newheader");
    $this->view("japy/header");
    
}
if ($this->session->userdata()) {
    $user = User::find($this->session->userdata("id_usuario"));
}
?>
    <!--- BANNER PROVEEDORES -->
    <?php if($state=="Jalisco") : ?>
    
    <script> 
        $(document).ready(function() {
        if (screen.width< 326) {
            $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider320 expos_JAPY-B-guadalajara.png');
        }
        else if (screen.width < 426) {
            $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider420 expos_JAPY-B-guadalajara.png');
        }
        else if (screen.width < 769) {
            $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider768 expos_JAPY-B-guadalajara.png');
        }
        else if (screen.width < 2100) {
            $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider2000x400 expos_JAPY-B-guadalajara.png');
        }
    }); </script>
    <div class="max-content">
        <img src="<?php echo base_url() ?>dist/img/slider_home/slider2000x400 expos_JAPY-B-guadalajara.png" alt="" class="banner_expo">
    </div>
    <?php elseif ($state=="Puebla") : ?>
    <script> 
        $(document).ready(function() {
            if (screen.width< 326) {
            if (screen.width< 326) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider320_expos_JAPY-B-puebla.png');
            }
            else if (screen.width < 426) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider420_expos_JAPY-B-puebla.png');
            }
            else if (screen.width < 769) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider768_expos_JAPY-B-puebla.png');
            }
            else if (screen.width < 2100) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider2000x400_expos_JAPY-B-puebla.png');
            }
    }); </script>
    <div class="max-content">
        <img src="<?php echo base_url() ?>dist/img/slider_home/slider2000x400_expos_JAPY-B-puebla.png" alt="" class="banner_expo">
    </div>
    <?php elseif ($state=="Queretaro") : ?>
    <script> 
        $(document).ready(function() {
            if (screen.width< 326) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider320_expos_JAPY-B-queretaro.png');
            }
            else if (screen.width < 426) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider420_expos_JAPY-B-queretaro.png');
            }
            else if (screen.width < 769) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider768_expos_JAPY-B-queretaro.png');
            }
            else if (screen.width < 2100) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider2000x400_expos_JAPY-B-queretaro.png');
            }
    }); </script>
    <div class="max-content">
        <img src="<?php echo base_url() ?>dist/img/slider_home/slider2000x400_expos_JAPY-B-queretaro.png" alt="" class="banner_expo">
    </div> 
    <?php else : ?>
    <script> 
        $(document).ready(function() {
            if (screen.width< 326) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider320_expos_JAPY-guadalajara.png');
            }
            else if (screen.width < 426) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider420_expos_JAPY-guadalajara.png');
            }
            else if (screen.width < 769) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider768_expos_JAPY-guadalajara.png');
            }
            else if (screen.width < 2100) {
                $('img.banner_expo').attr('src', '<?php echo base_url()?>dist/img/slider_home/slider2000x400_expos_JAPY-guadalajara.png');
            }
    }); </script>
    <div class="max-content">
        <img src="<?php echo base_url() ?>dist/img/slider_home/slider2000x400_expos_JAPY-guadalajara.png" alt="" class="banner_expo" >        
    </div>
    <?php endif; ?>

    <!--- FIN DE BANNER PROVEEDORES -->

    <div class="body-container">
        <?php $this->view("searchBar", $this->_ci_cached_vars); ?>
        
        <div class="row">
            <?php
            $breadcrumbs = [
                "Proveedores" => base_url()."proveedores",
            ];

            if ($servicio && $servicio != "ALL") {
                $breadcrumbs[$servicio->nombre_tipo_proveedor] = site_url("/proveedores/categoria/".strtolower($servicio->tag_categoria));
            }

            if ( ! isset($isSector) || ! $isSector) {
                if ($category->parent) {
                    $breadcrumbs[$category->parent->name] = base_url()."mx-".$category->parent->slug;
                }
                $breadcrumbs[$category->name] = base_url()."mx-".$category->slug;
            }

            $this->view("principal/breadcrumbs", ["breadcrumbs" => $breadcrumbs])
            ?>
        </div>
        <div class="row">
            <div class="col s12 m12 l12">
                <section id="menu-tipo">
                    <div class="card">
                        <div class="card-content">
                            <?php if (isset($isSector) && $isSector) : ?>
                                <h5>Bodas
                                    <span class="badge"><?php echo($providers->total()) ?> empresas</span>
                                </h5>
                                <p>
                                    <b>Gu&iacute;a para bodas:</b> Encuentra todo lo que necesitas para celebrar tu
                                    boda.
                                    Lugares para casarte como restaurantes, hoteles, haciendas, salones y jardines.
                                    Proveedores como fot&oacute;grafos, invitaciones, coches de bodas.
                                    Y tambi&eacute;n vestidos, trajes y complementos para los novios.
                                    Consulta precios y pide presupuesto.
                                </p>
                            <?php else : ?>
                                <h5>
                                    <?php echo $category->name ?>
                                    <span class="badge"><?php echo($providers->total()) ?> empresas</span>
                                </h5>
                                <p>
                                    <b style="font-weight: normal !important;"><?php echo $category->description ?></b>
                                </p>
                            <?php endif ?>
                        </div>
                        <div class="card-action ">
                            <!-- <?php if (isset($isSector)) : ?>
                                    <a href="<?php echo base_url() ?>proveedores?<?php echo((! empty($state) ? "&estado=".str_replace(" ",
                                        "-", $state) : '').(! empty($city) ? "&ciudad=".str_replace(" ",
                                        "-", $city) : '')."&ver=list") ?>"
                                       class="<?php echo $ver == "list" ? "dorado-2-text" : "grey-text " ?>"><i
                                                class="fa fa-list"></i> Lista</a>
                                    <a href="<?php echo base_url() ?>proveedores?<?php echo((! empty($state) ? "&estado=".str_replace(" ",
                                        "-", $state) : '').(! empty($city) ? "&ciudad=".str_replace(" ",
                                        "-", $city) : '')."&ver=img") ?>"
                                       class="<?php echo $ver == "img" ? "dorado-2-text" : "grey-text " ?> hide-on-small-only"><i
                                                class="fa fa-picture-o "></i> Imagenes</a> -->
                            <!--                            <a href="--><?php //echo base_url() ?><!--proveedores/sector/proveedores/?--><?php //echo((! empty($state) ? "&estado=".str_replace(" ",
                            //                                                    "-", $state) : '').(! empty($city) ? "&ciudad=".str_replace(" ",
                            //                                                    "-", $city) : '')."&ver=map") ?><!--"-->
                            <!--                               class="--><?php //echo $ver == "map" ? "dorado-2-text" : "grey-text " ?><!--"><i-->
                            <!--                                        class="fa fa-map-marker"></i> Mapa</a>-->
                            <?php else : ?>
                                <a href="<?php echo base_url()."mx-$category->slug" ?>?<?php echo((! empty($state) ? "&estado=".str_replace(" ",
                                            "-", $state) : '').(! empty($city) ? "&ciudad=".str_replace(" ",
                                            "-", $city) : '')."&ver=list") ?>"
                                   class="<?php echo $ver == "list" ? "dorado-2-text" : "grey-text " ?>"><i
                                            class="fa fa-list"></i> Lista</a>
                                <a href="<?php echo base_url()."mx-$category->slug" ?>?<?php echo((! empty($state) ? "&estado=".str_replace(" ",
                                            "-", $state) : '').(! empty($city) ? "&ciudad=".str_replace(" ",
                                            "-", $city) : '')."&ver=img") ?>"
                                   class="<?php echo $ver == "img" ? "dorado-2-text" : "grey-text " ?>  hide-on-small-only"><i
                                            class="fa fa-picture-o "></i> Imagenes</a>
                                <!--                            <a href="--><?php //echo base_url() ?><!--proveedores/categoria/?--><?php //echo("id=".$category->id.(! empty($state) ? "&estado=".str_replace(" ",
//                                                    "-", $state) : '').(! empty($city) ? "&ciudad=".str_replace(" ",
//                                                    "-", $city) : '')."&ver=map") ?><!--"-->
                                <!--                               class="--><?php //echo $ver == "map" ? "dorado-2-text" : "grey-text " ?><!--"><i-->
                                <!--                                        class="fa fa-map-marker"></i> Mapa</a>-->
                            <?php endif ?>
                        </div>
                    </div>
                </section>
                <section id="proveedores">
                    <?php if (isset($providers)) : ?>
                        <?php if ( ! isset($ver) || ! $ver || $ver == "list") { ?>
                            <?php foreach ($providers->items() as $key => $provider) : ?>
                                <div class="card hide-on-small-only ifs-lg-screen">
                                    <div class="card-content" style="padding: 10px;">
                                        <div class="row">
                                            <div class="col s12 m6 l4">
                                            <?php if($provider->videos->count()) : ?>
                                                        <div class="provider-image is-img-provider lazy"
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 100%" >
                                                        
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                        <?php if ($provider->descuento) : ?>
                                                        <?php if ($provider->descuento == 3) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/tres.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 5) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/cinco.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 10) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/diez.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 15) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/quince.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 20) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/veinte.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 30) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/treinta.png"
                                                                 alt="">
                                                            <?php } ?>
                                                        <?php endif ?>
                                                         <video 
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 250px;
                                                                     width: 100%;
                                                                     max-width: 100%" 
                                                         contextmenu="false" controls="true"
                                                            src="<?php echo(base_url().'uploads/videos/'.$provider->videos->first()->nombre) ?>">
                                                        </video>
                                                        
                                                    </div>
                                                <?php elseif ($provider->imagePrincipalMiniature->count()) : ?>
                                                    <div class="provider-image is-img-provider lazy"
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 100%;
                                                                     background-image: url('<?php echo(base_url().'uploads/images/'.$provider->imagePrincipalMiniature->first()->nombre) ?>')">
                                                         <?php if ($provider->descuento) : ?>
                                                         <?php if ($provider->descuento == 3) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/tres.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 5) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/cinco.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 10) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/diez.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 15) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/quince.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 20) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/veinte.png"
                                                                 alt="">
                                                            <?php } elseif ($provider->descuento == 30) { ?>
                                                                <img class="descuentos"
                                                                 src="<?php echo base_url() ?>dist/img/descuentos/treinta.png"
                                                                 alt="">
                                                            <?php } ?>
                                                        <?php endif ?>
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="provider-image is-img-provider lazy"
                                                         style="
                                                                     background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 100%"
                                                         data-src="<?php echo base_url() ?>/dist/img/slider1.png">
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col s12 m6 l8"
                                                 style="height: 350px;display: flex;justify-content: space-between;flex-direction: column;">
                                                <div class="row" style="margin: 0 !important;">
                                                    <span class="tooltip_c proveedor-favorit"><?php echo $provider->favorito ? "Guardado" : "Guardar" ?></span>
                                                    <i data-proveedor="<?php echo $provider->id_proveedor ?>"
                                                       class="fa fa-heart-o proveedor-favorite <?php echo $provider->favorito ? "active" : "" ?>  clickable  tooltippeds"
                                                       data-position="left" data-delay="5"
                                                       data-tooltip="<?php echo $provider->favorito ? "Guardado" : "Guardar" ?>"></i>
                                                    <!-- <div class="ifs-desc"></div> -->
                                                    <div class="ifs-provider-desc"></div>
                                                    
                                                    <h5>
                                                        <a class="dorado-2-text is-nombre"
                                                           href="<?php echo base_url()."boda-".$provider->slug ?>">
                                                            <?php echo $provider->nombre ?>
                                                        </a>
                                                    </h5>
                                                    <small class="grey-text lighten-3 "><?php echo $provider->nombre_tipo_proveedor ?>
                                                        <i class="fa fa-map-marker"></i> <?php echo $provider->localizacion_estado ?>
                                                    </small>
                                                    <div class="divider"></div>
                                                    <p class="provider-p" style="text-align: justify">
                                                        <?php echo strip_tags(substr($provider->descripcion, 0, 350)) ?>
                                                        ...
                                                    </p>
                                                </div>
                                                <div class="row ifs-promociones"
                                                     style="margin-bottom: 0px;margin-top:10px;margin-right: 0 !important;">
                                                    <?php if ($provider->promociones) : ?>
                                                        <div class="right promo">
                                                            <a href="<?php echo base_url() ?>index.php/escaparate/promociones/<?php echo $provider->id_proveedor ?>"
                                                               class="white-text"><i class="fa fa-tag "></i>
                                                                <?php echo $provider->promociones ?>
                                                                promocion<?php echo $provider->promociones > 1 ? "es" : "" ?>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="ifs-gallery"></div>
                                                <?php if ($provider->tipo_cuenta == 3 || $provider->tipo_cuenta == 2): ?>
                                                    <div class="row ifs-gallery-base"
                                                         style="height: auto; max-height: 120px;margin: 0!important;">
                                                        <div class="col s12">
                                                            <?php if ($provider->images->count()) : ?>
                                                                <div class="galeria"
                                                                     style="display: flex; justify-content: space-around;overflow: auto;">
                                                                    <?php foreach ($provider->images->take(6) as $g) { ?>
                                                                        <a rel="gallery-<?php echo $provider->id_proveedor ?>"
                                                                           class="swipebox"
                                                                           href="<?php echo base_url()."uploads/images/$g->nombre" ?>">
                                                                            <img class="gal-provi lazy" alt="" data-src="<?php echo base_url()."uploads/images/$g->nombre" ?>">
                                                                        </a>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>

                                                    </div>
                                                <?php endif; ?>
                                                <div class="row" style="padding-right: 0;margin: 0;">
                                                    <div class="divider"></div>
                                                    <div class="col m6 l4 price" style="padding-top: 5px">
                                                        <?php if (is_array($provider->precio)) : ?>
                                                            Precio desde: <br>
                                                            <i class="fa fa-money"></i> $ <?php echo $provider->precio[0] ?><?php echo count($provider->precio) > 1 ? "&nbsp;hasta $".$provider->precio[1] : "" ?>
                                                        <?php else : ?>
                                                            &nbsp;<br> &nbsp;
                                                        <?php endif ?>
                                                    </div>
                                                    <div class="col l4 capacity ">
                                                        <?php if (is_array($provider->capacidad)) : ?>
                                                            Capacidad: <br>
                                                            <i class="fa fa-users"></i>  <?php echo $provider->capacidad[0] ?><?php echo count($provider->capacidad) > 1 ? " a ".$provider->capacidad[1] : "" ?>
                                                        <?php else : ?>
                                                            &nbsp;<br> &nbsp;
                                                        <?php endif ?>
                                                    </div>
                                                    <div class="col m6 l4 " style="padding-right: 0">

                                                        <a href="<?php echo base_url()."boda-".$provider->slug ?>"
                                                           class="ifs-info btn-flat dorado-1-text right more-info truncate"
                                                           style="font-size: 0.8em;padding-right:0; ">
                                                            Quiero m&aacute;s informacion
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hide-on-med-and-up ifs-s-screen">
                                    <div class="col s12">
                                        <div class="card ">
                                            <div class="card-image">
                                            <?php if($provider->videos->count()) : ?>
                                                        <div class="provider-image is-img-provider lazy"
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 100%" >
                                                        
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                        <?php if ($provider->descuento) : ?>
                                                            <div class="desc">
                                                                - <?php echo $provider->descuento ?>%
                                                            </div>
                                                        <?php endif ?>
                                                         <video 
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 250px;
                                                                     width: 100%;
                                                                     max-width: 100%" 
                                                         contextmenu="false" controls="true"
                                                            src="<?php echo(base_url().'uploads/videos/'.$provider->videos->first()->nombre) ?>">
                                                        </video>
                                                        
                                                    </div>
                                                <?php elseif ($provider->imagePrincipalMiniature->count()) : ?>
                                                    <div class="provider-image is-img-provider lazy"
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 350px"
                                                         data-src="<?php echo(base_url().'uploads/images/'.$provider->imagePrincipalMiniature->first()->nombre) ?>">
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: left;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: left;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: left;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="provider-image is-img-provider lazy"
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 350px"
                                                         data-src="<?php echo base_url() ?>/dist/img/slider1.png">
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: left;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: left;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: left;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="card-content" style="padding: 10px;">
                                                <div class="row provider-info" style="height: 68%">
                                                    <i data-proveedor="<?php echo $provider->id_proveedor ?>"
                                                       class="fa fa-heart-o proveedor-favorite <?php echo $provider->favorito ? "active" : "" ?>  clickable  tooltipped"
                                                       data-position="left" data-delay="5"
                                                       data-tooltip="<?php echo $provider->favorito ? "Guardado" : "Guardar" ?>"></i>
                                                    <div class="ifs-provider-desc"></div>
                                                    
                                                    <h5>
                                                        <a class="dorado-2-text is-nombre"
                                                           href="<?php echo base_url() ?>boda-<?php echo $provider->slug ?>">
                                                            <?php echo $provider->nombre ?>
                                                        </a>
                                                    </h5>
                                                    <small class="grey-text lighten-3 "><?php echo $provider->nombre_tipo_proveedor ?>
                                                        <i class="fa fa-map-marker"></i> <?php echo $provider->localizacion_estado ?>
                                                    </small>
                                                    <div class="divider"></div>
                                                    <p class="provider-par">
                                                        <?php echo strip_tags(substr($provider->descripcion, 0, 350)) ?>
                                                        ...
                                                    </p>
                                                </div>
                                                <div class="row ifs-promociones" style="margin-bottom: 0px">
                                                    <?php if ($provider->promociones) : ?>
                                                        <div class="right promo">
                                                            <a href="<?php echo base_url() ?>index.php/escaparate/promociones/<?php echo $provider->id_proveedor ?>"
                                                               class="white-text"><i class="fa fa-tag "></i>
                                                                <?php echo $provider->promociones ?>
                                                                promocion<?php echo $provider->promociones > 1 ? "es" : "" ?>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="row" style="padding-right: 0;">
                                                    <div class="divider"></div>
                                                    <div id="price" class="col s12 price" style="padding-top: 5px">
                                                        <?php if (is_array($provider->precio)) : ?>
                                                            Precio desde: <br>
                                                            <i class="fa fa-money"></i> $ <?php echo $provider->precio[0] ?><?php echo count($provider->precio) > 1 ? "&nbsp;hasta $".$provider->precio[1] : "" ?>&nbsp;
                                                        <?php endif ?>
                                                    </div>
                                                    <div id="capacity" class="col s12 capacity ">
                                                        <?php if (is_array($provider->capacidad)) : ?>
                                                            Capacidad: <br>
                                                            <i class="fa fa-users"></i>  <?php echo $provider->capacidad[0] ?><?php echo count($provider->capacidad) > 1 ? " a ".$provider->capacidad[1] : "" ?>
                                                        <?php endif ?>
                                                    </div>
                                                    <div class="col s12 " style="padding-right: 0">

                                                        <a href="<?php echo base_url() ?>boda-<?php echo $provider->slug ?>"
                                                           class="ifs-info col s12 btn-flat dorado-1-text right more-info "
                                                           style="font-size: 0.8em;padding-right:0; ">
                                                            Quiero M&aacute;s informaci&oacute;n
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endforeach ?>

                        <?php } elseif ($ver == "img") { ?>
                            <div class="row">
                                <?php foreach ($providers as $provider) : ?>
                                    <div class="col s12 m6 l4">
                                        <div class="card">
                                            <i data-proveedor="<?php echo $provider->id_proveedor ?>"
                                               class="fa fa-heart-o proveedor-favorite <?php echo $provider->favorito ? "active" : "" ?>  clickable tooltipped"
                                               data-position="left" data-delay="5"
                                               data-tooltip="<?php echo $provider->favorito ? "Guardado" : "Guardar" ?>"></i>
                                            <?php if ($provider->descuento) : ?>
                                                <div class="desc" style="">
                                                    - <?php echo $provider->descuento ?>%
                                                </div>
                                            <?php endif ?>
                                            <style>
                                                i.proveedor-favorite {
                                                    left: 10px;
                                                    right: initial !important;
                                                }

                                                .desc {
                                                    left: 25px;
                                                }
                                            </style>
                                            <div class="card-image">
                                            <?php if($provider->videos->count()) : ?>
                                                        <div class="provider-image is-img-provider lazy"
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 100%" >
                                                        
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img class="cintillo"
                                                                 src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                         <video 
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 250px;
                                                                     width: 100%;
                                                                     max-width: 100%" 
                                                         contextmenu="false" controls="true"
                                                            src="<?php echo(base_url().'uploads/videos/'.$provider->videos->first()->nombre) ?>">
                                                        </video>
                                                        
                                                    </div>
                                                <?php elseif ($provider->imagePrincipalMiniature->count()) : ?>
                                                    <div class="provider-image is-img-provider lazy"
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 100%"
                                                         data-src="<?php echo(base_url().'uploads/images/'.$provider->imagePrincipalMiniature->first()->nombre) ?>">
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="provider-image lazy"
                                                         style="background-repeat: no-repeat;
                                                                     background-position: center;
                                                                     background-size:cover;
                                                                     height: 350px;
                                                                     width: 100%;
                                                                     max-width: 100%"
                                                         data-src="<?php echo base_url().'/dist/img/slider1.png' ?>">
                                                        <?php if ($provider->tipo_cuenta == 3) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintillod.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 2) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintilloo.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } elseif ($provider->tipo_cuenta == 1) { ?>
                                                            <img src="<?php echo base_url() ?>dist/img/cintillos/cintillop.png"
                                                                 alt=""
                                                                 style="float: right;height: 60px!important;width: 60px!important;">
                                                        <?php } ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="card-content content-tex">
                                                <p class="content-p" style="height: 12%">
                                                    <?php echo strip_tags(substr($provider->descripcion, 0, 200)) ?>...
                                                </p>
                                                <div class="row" style="margin: 10px 0px 0px 0px;">
                                                    <div class="grey-text darken-5 col s3" style="display: inline;">
                                                        <i class="fa fa-camera-retro "></i> <?php echo $provider->images_count ?>
                                                        <i class="fa fa-video-camera "></i> <?php echo $provider->videos_count ?>
                                                    </div>
                                                    <?php if ($provider->promociones) { ?>
                                                        <div class="promo dropdown-button"
                                                             data-activates='dropdown<?php echo $provider->id_proveedor ?>'
                                                             data-hover="true" data-beloworigin="true"
                                                             data-constrainwidth="false">
                                                            <a href="<?php echo base_url() ?>index.php/escaparate/promociones/<?php echo $provider->id_proveedor ?>"
                                                               class="white-text"><i
                                                                        class="fa fa-tag "></i> <?php echo $provider->promociones ?>
                                                                promocion<?php echo $provider->promociones > 1 ? "es" : "" ?>
                                                            </a>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="col s9  ">
                                                        <p class="pull-right">
                                                            <a href="<?php echo base_url()."boda-".$provider->slug ?>"
                                                               class="btn-flat dorado-1-text right"
                                                               style="font-size: 0.8em;padding-right:0; ">
                                                                Quiero m&aacute;s informaci&oacuten
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-action">
                                                <a class="black-text"
                                                   style="display: inline-table; font-size: 12px">
                                                    <?php if (is_array($provider->precio)) : ?>
                                                        Precio desde: <br>
                                                        <i class="fa fa-money"></i> $ <?php echo $provider->precio[0] ?><?php echo count($provider->precio) > 1 ? "&nbsp;hasta $".$provider->precio[1] : "" ?>
                                                    <?php else : ?>
                                                        &nbsp;<br> &nbsp;
                                                    <?php endif ?>
                                                </a>
                                                <a class="black-text"
                                                   style="display: inline-table; font-size: 12px">
                                                    <?php if (is_array($provider->capacidad)) : ?>
                                                        Capacidad: <br>
                                                        <i class="fa fa-users"></i>  <?php echo $provider->capacidad[0] ?><?php echo count($provider->capacidad) > 1 ? " a ".$provider->capacidad[1] : "" ?>
                                                    <?php else : ?>
                                                        &nbsp;<br> &nbsp;
                                                    <?php endif ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php } elseif ($ver == "map") { ?>
                            <div class="hide">
                                <?php foreach ($allProviders as $key => $p) { ?>
                                    <div class="proveedor" data-latitud="<?php echo $p->localizacion_latitud ?>"
                                         data-longitud="<?php echo $p->localizacion_longitud ?>"
                                         data-nombre="<?php echo $p->nombre ?>">
                                        <div id="iw-container">
                                            <div class="iw-title">
                                                <?php if ($p->imagePrincipal->first()) { ?>
                                                    <img style="width:100%" class="responsive-img"
                                                         src="<?php echo base_url() ?>uploads/images/<?php echo $p->imagePrincipal()->first()->nombre ?>">
                                                <?php } else { ?>
                                                    <img style="width:100%" class="responsive-img"
                                                         src="<?php echo base_url() ?>dist/img/slider1.png">
                                                <?php } ?>
                                            </div>
                                            <div class="iw-content">
                                                <div class="iw-subTitle dorado-1-text"><?php echo $p->nombre ?></div>
                                                <label><?php echo $p->localizacion_direccion ?></label><br>
                                                <label><?php echo $p->localizacion_poblacion ?>
                                                    , <?php echo $p->localizacion_estado ?></label>
                                                <div style="width:330px">
                                                    <div class="row" style="margin:0px">
                                                        <?php if ($p->images->count()) { ?>
                                                            <a class="col s4 dorado-2-text" title="Fotos"
                                                               href="<?php echo base_url() ?>index.php/escaparate/fotos/<?php echo $p->id_proveedor ?>">
                                                                <i class="fa fa-camera "></i> <?php echo $p->images->count() ?>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if ($p->videos->count()) { ?>
                                                            <a class="col s4 dorado-2-text" title="Videos"
                                                               href="<?php echo base_url() ?>index.php/escaparate/videos/<?php echo $p->id_proveedor ?>">
                                                                <i class="fa fa-video-camera"></i> <?php echo $p->videos->count() ?>
                                                            </a>
                                                        <?php } ?>
                                                        <?php if ($p->promotions->count() + ($p->descuento ? 1 : 0) > 0) { ?>
                                                            <a class="col s4 dorado-2-text" title="Promociones"
                                                               href="<?php echo base_url() ?>index.php/escaparate/promociones/<?php echo $p->id_proveedor ?>">
                                                                <i class="fa fa-tag"></i> <?php echo $p->promotions->count() + ($p->descuento ? 1 : 0) ?>
                                                            </a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <a href="<?php echo base_url() ?>boda-<?php echo $p->slug ?>"
                                                   class="btn dorado-2">Quiero m&aacute;s informaci&oacute;n</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div id="map" class="card-panel valign-wrapper center" style="height: 500px;padding: 0px;">
                                <i class="fa fa-spin fa-spinner valign fa-4x" style="width: 100%"></i>
                            </div>
                        <?php } ?>
                    <?php endif; ?>
                </section>

                <div class="paginacion">
                    <?php// $pag = 0; ?>
                    <?php// while ($pag < $lastpage): ?>
                    <?php// $pag += 1; ?>
                    <?php// dd($_SERVER); ?>
                    <?php// dd($_SERVER); ?>
                    <?php// dd(base_url().'?'.$_SERVER['REDIRECT_QUERY_STRING']) ?>
                        <?php// if(!empty($_SERVER['HTTP_REFERER']) && !empty($_SERVER['REDIRECT_QUERY_STRING'])): ?>
                            <!-- <a href="<?= $_SERVER['HTTP_REFERER'].'?'.$_SERVER['REDIRECT_QUERY_STRING'] ?>&pagina=<?= $pag ?>"><span class="btn"><?= $pag ?></span></a> -->
                        <?php// endif; ?>
                    <?php// endwhile; ?>
                    <div id="paginationContent" class="" style="margin-bottom: 5vh;"></div>
                </div>

                <?php //if ($providers->total() && $ver != 'map') : ?>
                    <!--<div class="isContainer" style="text-align: center;">
                        <span class="onLoad">Cargando...</span>
                    </div>-->
                <?php //endif ?>

            </div>
        </div>
    </div>
<?php //$this->view("principal/footer"); ?>
<?php $this->view("japy/footer"); ?>
    <script src="<?php echo base_url() ?>dist/swipebox/src/js/jquery.swipebox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.10/jquery.lazy.min.js"></script>

    <script async>
        var lastPage = parseInt("<?php echo $providers->lastPage() ?>");
        var page = 1;
        var img_url = "<?php echo(base_url()."uploads/images/") ?>";
        var img_url_default = "<?php echo(base_url()."/dist/img/slider1.png") ?>";
        var url_proveedor = "<?php echo(base_url()."boda-"); ?>";
        var detected = false;
        if (window.location.search != '') {
            var urlP = window.location.origin + window.location.pathname + window.location.search + '&pagina=';
        }
        else {
            var urlP = window.location.origin + window.location.pathname + '?pagina=';
        }

        //Detecta cuando el objeto entra en la vista
        /*$(window).scroll(function() {
            var top_of_element = $('.isContainer').offset().top;
            var bottom_of_element = $('.isContainer').offset().top + $('.isContainer').outerHeight();
            var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
            var top_of_screen = $(window).scrollTop();
            if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) && (!detected)) {
                detected = true;
                //this.lodMore();
            }
        });*/

        function lodMore() {
            page++;
            if (page <= lastPage) {
                $.when($.ajax({
                    url: urlP + (page),
                    headers: {'Accept': 'application/json'},
                    success: function(response) {
                    },
                })).done(function(dataArray) {
                    detected = false;
                    $.each(dataArray.data, function(i, dat) {
                        console.log(dat);
                        var content = dat.descripcion.replace(/(<([^>]+)>)/ig, '');
                        var newCard = $('.ifs-s-screen:last').clone(true);
                        var newLGCard = $('.ifs-lg-screen:last').clone(true);
                        var imgToDisplay = (dat.image_principal[0] !== undefined) ? (img_url + dat.image_principal[0].nombre) : img_url_default;
                        var cintillo = null;
                        var styles = {
                            'background-image': 'url(\'' + imgToDisplay + '\')',
                            'background-repeat': 'no-repeat',
                            'background-position': 'center',
                            'background-size': 'cover',
                            'height': '350px',
                            'width': '100%',
                            'max-width': '100%',
                        };
                        if (dat.tipo_cuenta === 3) {
                            cintillo = "<?php echo(base_url()."dist/img/cintillos/cintillod.png") ?>";
                        }
                        else if (dat.tipo_cuenta === 2) {
                            cintillo = "<?php echo(base_url()."dist/img/cintillos/cintilloo.png") ?>";
                        }
                        else if (dat.tipo_cuenta === 1) {
                            cintillo = "<?php echo(base_url()."dist/img/cintillos/cintillop.png") ?>";
                        }
                        else {
                            newCard.find('img.cintillo').remove();
                            newLGCard.find('img.cintillo').remove();
                        }

                        if (dat.descuento != null) {
                            newLGCard.find('div.ifs-provider-desc').html('<div class="desc"> -' + dat.descuento + '% </div>');
                            newCard.find('div.ifs-provider-desc').html('<div class="desc"> -' + dat.descuento + '% </div>');
                        }
                        else {
                            newLGCard.find('div.ifs-provider-desc').html('');
                            newCard.find('div.ifs-provider-desc').html('');
                        }

                        if (dat.promociones) {
                            var plurSing = (dat.promociones > 1) ? 'es' : '';
                            newLGCard.find('div.ifs-promociones').html('<div class="right promo"> <a href="<?php echo base_url() ?>index.php/escaparate/promociones/' + dat.id_proveedor + '" class="white-text"> <i class="fa fa-tag"></i>' + dat.promociones + plurSing + ' </a> </div>');
                            newCard.find('div.ifs-promociones').html('<div class="right promo"> <a href="<?php echo base_url() ?>index.php/escaparate/promociones/' + dat.id_proveedor + '" class="white-text"> <i class="fa fa-tag"></i>' + dat.promociones + plurSing + ' </a> </div>');
                        }
                        else {
                            newLGCard.find('div.ifs-promociones').html('');
                            newCard.find('div.ifs-promociones').html('');
                        }

                        if (dat.precio && (dat.precio != '')) {
                            newLGCard.find('div.price').html('Precio desde: <br> <i class="fa fa-money"></i> $' + dat.precio[0] + ((dat.precio.length > 1) ? (' hasta $' + dat.precio[1]) : '') + '');
                            newCard.find('div.price').html('Precio desde: <br> <i class="fa fa-money"></i> $' + dat.precio[0] + ((dat.precio.length > 1) ? (' hasta $' + dat.precio[1]) : '') + '');
                        }
                        else {
                            newLGCard.find('div.price').html('');
                            newCard.find('div.price').html('');
                        }

                        if (dat.capacidad && (dat.capacidad != '')) {
                            newLGCard.find('div.capacity').html('Capacidad: <br> <i class="fa fa-users"></i> ' + dat.capacidad[0] + ((dat.capacidad.length > 1) ? (' a ' + dat.capacidad[1]) : '') + '');
                            newCard.find('div.capacity').html('Capacidad: <br> <i class="fa fa-users"></i> ' + dat.capacidad[0] + ((dat.capacidad.length > 1) ? (' a ' + dat.capacidad[1]) : '') + '');
                        }
                        else {
                            newLGCard.find('div.capacity').html('');
                            newCard.find('div.capacity').html('');
                        }

                        if (dat.images && ((dat.tipo_cuenta == 3) || (dat.tipo_cuenta == 2))) {
                            newLGCard.find('div.ifs-gallery-base').remove();
                            var contImages = (dat.images.length > 6) ? 6 : dat.images.length;
                            newLGCard.find('div.ifs-gallery').html('<div class="row" style="height: auto; max-height: 120px;margin: 0!important;"><div class="col s12"><div class="ifs-gal galeria" style="display: flex; justify-content: space-around;overflow: hidden;"></div></div> </div>');
                            for (var i = 0; i < contImages; i++) {
                                newLGCard.find('div.ifs-gal').append('<a rel="gallery-' + dat.id_proveedor + '" class="swipebox" href="' + img_url + dat.images[i].nombre + '" > <img class="gal-provi" alt="" src="' + img_url + dat.images[i].nombre + '" /> </a>');
                            }
                        }
                        if (((!dat.images) && ((dat.tipo_cuenta == 3) || (dat.tipo_cuenta == 2))) || (dat.tipo_cuenta == 0) || (dat.tipo_cuenta == 1)) {
                            newLGCard.find('div.ifs-gallery-base').remove();
                            newLGCard.find('div.ifs-gal').remove();
                        }

                        if (dat.favorito) {
                            newCard.find('i.proveedor-favorite').attr({
                                'class': 'fa fa-heart-o proveedor-favorite ' + ((dat.favorito == 1) ? 'active' : '') + ' clickable  tooltipped',
                                'data-proveedor': dat.id_proveedor,
                                'data-position': 'left',
                                'data-delay': '5',
                                'data-tooltip': (dat.favorito == 1) ? 'Guardado' : 'Guardar',
                            });
                            newLGCard.find('i.proveedor-favorite').attr({
                                'class': 'fa fa-heart-o proveedor-favorite ' + ((dat.favorito == 1) ? 'active' : '') + ' clickable  tooltipped',
                                'data-proveedor': dat.id_proveedor,
                                'data-position': 'left',
                                'data-delay': '5',
                                'data-tooltip': (dat.favorito == 1) ? 'Guardado' : 'Guardar',
                            });
                        }
                        else {
                            newCard.find('i.proveedor-favorite').attr({
                                'class': 'fa fa-heart-o proveedor-favorite clickable  tooltipped',
                                'data-proveedor': dat.id_proveedor,
                                'data-position': 'left',
                                'data-delay': '5',
                                'data-tooltip': 'Guardar',
                            });
                            newLGCard.find('i.proveedor-favorite').attr({
                                'class': 'fa fa-heart-o proveedor-favorite clickable  tooltipped',
                                'data-proveedor': dat.id_proveedor,
                                'data-position': 'left',
                                'data-delay': '5',
                                'data-tooltip': 'Guardar',
                            });
                        }

                        // Pantalla pequeña
                        newCard.find('a.is-nombre').html(dat.nombre).attr('href', url_proveedor + dat.slug);
                        newCard.find('div.is-img-provider').css(styles);
                        newCard.find('img.cintillo').attr('src', cintillo);
                        newCard.find('small.lighten-3').html('<i class="fa fa-map-marker"></i>' + dat.localizacion_estado);
                        newCard.find('p.provider-par').html(content.substring(0, 300) + '...');
                        newCard.find('a.ifs-info').attr('href', url_proveedor + dat.slug);

                        // Pantalla grande
                        newLGCard.find('a.is-nombre').html(dat.nombre).attr('href', url_proveedor + dat.slug);
                        newLGCard.find('div.is-img-provider').css(styles);
                        newLGCard.find('img.cintillo').attr('src', cintillo);
                        newLGCard.find('small.lighten-3').html('<i class="fa fa-map-marker"></i>' + dat.localizacion_estado);
                        newLGCard.find('p.provider-p').html(content.substring(0, 350) + '...');
                        newLGCard.find('a.ifs-info').attr('href', url_proveedor + dat.slug);

                        newCard.insertBefore('.isContainer');
                        newLGCard.insertBefore('.isContainer');
                    });
                });
            }
            else {
                $('.onLoad').html('No quedan más elementos por mostrar');
            }
        }

        $(document).ready(function() {
            $('.lazy').Lazy();
            let estado = "<?php echo(isset($estado) ? $estado : "ALL") ?>";
            let server = "<?php echo base_url() ?>";
            const hasLocation = $('#hasLocation').val();
            const searchParams = new URLSearchParams(window.location.search);

            if (!hasLocation) {
                $('#opener_modal_ubi').trigger('click');
            }

            $('.proveedor-favorite').on('mouseover', function() {
                $(this).parent().find('.tooltip_c').addClass('tooltip_visible');
            });
            $('.proveedor-favorite').on('mouseout', function() {
                $(this).parent().find('.tooltip_c').removeClass('tooltip_visible');
            });

            $('.proveedor-favorite').on('click', function() {
                var $self = $(this);
                $.ajax({
                    url: server + 'index.php/escaparate/guardarlo/' + $self.data('proveedor'),
                    method: 'POST',
                    data: {
                        proveedor: $self.data('proveedor'),
                    },
                    success: function(resp) {
                        if (resp.data.favorito) {
                            $self.addClass('active').attr('data-tooltip', 'Guardado');
                            $('.tooltip_c').text('Guardado');
                        }
                        else {
                            $self.removeClass('active').attr('data-tooltip', 'Guardar');
                            $('.tooltip_c').text('Guardar');
                        }
                    },
                    error: function() {
                        if (!$('#toast-container .toast').get(0)) {
                            Materialize.toast(
                                "Debes estar registrado para guardar un proveedor<br> <a href='<?php echo site_url("cuenta") ?>' class='dorado-1-text pull-right'>Inicia Sesi&oacute;n <i class='material-icons right'>keyboard_arrow_right</i></a>",
                                5000);
                            $('.toast').css({display: 'block'});
                        }
                    },
                });
            });

            // if (Boolean(window.location.search)) {
            //     if (searchParams.has('page') || searchParams.has('ver')) {
            //         searchParams.set('page', currentPage == lastPage ? currentPage : currentPage + 1);
            //         searchParams.set('page', currentPage == 1 ? currentPage : currentPage - 1);
            //     }
            // }

            $('.swipebox').swipebox();

            var pages = <?= $lastpage ?>;
            if(pages > 1)
                for (let index = 0; index < pages; index++) {
                    var span = document.createElement("span");     // Create a <p> node
                    var t = document.createTextNode(index+1);      // Create a text node
                    span.appendChild(t); 
                    span.classList.add('btn');
                    var para = document.createElement("a");
                    para.style.paddingRight = "3px"; 
                    //link
                    var url = window.location.href;
                    var res = '';
                    if(url.indexOf('pagina=') != -1){
                        res = url.substr(0, url.indexOf('pagina=')+7);
                        res = res+(index+1);
                    }else{
                        if(url.indexOf('?') == -1){
                            res = url+'?&pagina='+(index+1);
                        }else
                            res = url+'&pagina='+(index+1);
                    }
                    //endlink
                    para.href=res;
                    para.appendChild(span); 
                    document.getElementById("paginationContent").appendChild(para);
                }
        });
    </script>

<?php if ($verMapa) { ?>
    <script>
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 5,
            });

            var t = decodeURIComponent(document.cookie);

            try {
                var geoip = JSON.parse(t.replace('geolocation=', ''));
            }
            catch (e) {
                var geoip = (t.replace('geolocation=', '').split(';')[1]);
            }

            var geocoder = new google.maps.Geocoder();
            var estado = $('.estado-selected').html();
            var zoom = 5;
            if (!estado) {
                estado = geoip.countryName && geoip.countryName != '-' ? geoip.countryName : 'Mexico';
            }
            else {
                zoom = 7;
            }
            geocoder.geocode({'address': estado}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    map.setZoom(zoom);
                }
                else {
                }
            });

            $('.proveedor').each(function() {
                var self = $(this);
                var self = $(this);
                var myLatLng = {lat: self.data('latitud'), lng: self.data('longitud')};
                if (myLatLng.lat == '' && myLatLng.lng == '') {
                    return;
                }
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: self.data('nombre'),
                });

                var infowindow = new google.maps.InfoWindow({
                    content: self.html(),
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker); // map e marker são as vari&aacute;veis definidas anteriormente
                });
                google.maps.event.addListener(map, 'click', function() {
                    infowindow.close();
                });
                google.maps.event.addListener(infowindow, 'domready', function() {
                    var iwOuter = $('.gm-style-iw');
                    var iwBackground = iwOuter.prev();
                    if ($('.iw-content').height() < 140) {
                        $('.iw-bottom-gradient').css({display: 'none'});
                    }
                    iwBackground.children(':nth-child(2)').css({'display': 'none'});
                    iwBackground.children(':nth-child(4)').css({'display': 'none'});
                    iwOuter.parent().parent().css({left: '0px'});
                    iwBackground.children(':nth-child(1)').attr('style', function(i, s) {
                        return s + 'left: 191px !important;';
                    });
                    iwBackground.children(':nth-child(3)').attr('style', function(i, s) {
                        return s + 'left: 191px !important;';
                    });
                    iwBackground.children(':nth-child(3)').find('div').children().css({
                        'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px',
                        'z-index': '1',
                    });
                    var iwCloseBtn = iwOuter.next();
                    iwCloseBtn.css({
                        opacity: '1',
                        right: '38px',
                        width: '27px',
                        height: '27px',
                        top: '3px',
                        border: '7px solid #FFFFFF',
                        background: 'white',
                        'border-radius': '13px',
                        'box-shadow': 'rgb(127, 127, 127) 0px 0px 5px',
                    });

                });
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBj6fY5sVLxsS7FswsQt_n6Oy1XRyTXxdA&callback=initMap"></script>
<?php } ?>