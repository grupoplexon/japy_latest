<?php
$controller    = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$controller    = strtolower($controller);
$provider      = User::find($this->session->userdata("id_usuario"))->provider;
$providerImage = $provider->imageLogoMiniature
    ? base_url()."uploads/images/".$provider->imageLogoMiniature->nombre
    :
    base_url()."/dist/img/iconos/proveedores/usuario-empresa.png";
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/css/brideAdvisor/proveedores.css"/>
<style>
    p {
        margin: 0;
        color: #3A3A3A;
        font-weight: bold;
        font-size: .9rem;
    }
    img{
        width: 40px;
    }
    body {
         background-image: url("https://localhost/japy/dist/img/fondo.jpg") !important;
         background-size: cover;
    }
    .alinear {
          padding-top: 0 !important;
          padding-left: 0 !important;
          padding-right: 0 !important;
          padding-bottom: 0 !important;
    }
    .menu-opcion{
        margin-top: 45px;
    }
    
    @media only screen and (min-width: 322px) and (max-width: 425px) {
        .menu {
            min-width: 267px !important;
        }
        
    }
    @media only screen and (max-width: 992px){
        .hide-on-med-and-down {
            display: block !important;
        }
    }
</style>
<div class="row alinear">
<div class="col l12 barra-menu" style="background: #ECEDED; ">
    <!-- <hr style="border: 4px solid; border-color: #F8DADF"> -->
    <div class="col l3 s6 center-align logo">
        <a href="<?php echo base_url() ?>">
        <img class="logo-p" src="<?php echo base_url() ?>/dist/img/japy_copy.png" style=" ">
        </a>
    </div>
    <div class="">
          <div class="hide-on-med-and-down"> 
            <div class="col l1 m1 center-align menu-opcion">
                    <a class="m" href="<?php echo base_url() ?>proveedor">
                    <img class="" style="" src="<?php echo base_url() ?>dist/img/iconos/proveedores/perfil.png" alt="">
                    <p>MI PERFIL</p>
                    </a>
            </div>
            <div class="col l1 m1 center-align menu-opcion">
                    <a class="" href="<?php echo base_url() ?>proveedor/escaparate">
                    <img class="" style="" src="<?php echo base_url() ?>dist/img/iconos/proveedores/escaparate.png" alt="">
                    <p>ESCAPARATE</p>
                    </a>
            </div>
            <div class="col l1 m1 center-align menu-opcion">
                    <a href="<?php echo base_url() ?>proveedor/solicitudes">
                    <img class="" style="" src="<?php echo base_url() ?>dist/img/iconos/proveedores/solicitudes.png" alt="">
                    <p>SOLICITUDES</p>
                    </a> 
            </div>
            <div class="col l2 m1 center-align menu-opcion" style="margin-left: -25px; margin-right: -25px;">
                    <a href="<?php echo site_url("proveedor/recomendaciones") ?>">
                    <img class="" style="" src="<?php echo base_url() ?>dist/img/iconos/proveedores/Recomendacion.png" alt="">
                    <p>RECOMENDACIONES</p>
                    </a>
            </div>
            <div class="col l1 m1 center-align menu-opcion">
                    <a href="<?php echo base_url() ?>proveedor/MiCuenta">
                    <img class="" style="" src="<?php echo base_url() ?>dist/img/iconos/proveedores/notificacion.png" alt="">
                    <p>NOTIFICACIONES</p>
                    </a>
            </div>
          </div>
          <div class="col l3 m4 s6 pull-right" style="float: right">
                <div class="vl2 hide-on-med-and-down"></div>
                <div id="menu-provider" class="row clickable dropdown-button waves-effect" data-beloworigin="true"
                     data-activates="dropdown-provider" style=" display: block; margin-bottom: 0;">
                     
                    <div class="col s6 img-user-provider" style="text-align: right;width: 80px;">
                        <img class="circule-img " style="background: white"
                             src="<?php echo $providerImage ?>"
                             alt="<?php echo $this->session->userdata("nombre_proveedor") ?>"/>
                    </div>
                    <div class="col s6 hide-on-med-and-down" style=" padding-left: 0px;width: calc( 100% - 80px )">
                        <p class="usuario" style="font-size: 17px;font-weight: bold;margin-bottom: -5px;">
                            <?php echo $this->session->userdata("nombre_proveedor") ?>
                        </p>
                        <p style="font-size: 12px; color: #6C6D6F;">
                            <?php echo $this->session->userdata("genero") ?>
                        </p>
                    </div>
                    <i id="burger" class="material-icons usuario hide-on-large-only"
                           style="position: absolute; top: 30%;display: block;right: 0px;">menu</i>
                    <!--<i class="fa fa-bars primary-text" data-activates="dropdown-menu"-->
                    <!--   style="position: absolute; top: 13px; display: block;right: 6px;"></i>-->
                </div>

                <ul id="dropdown-provider" class="dropdown-content">
                    <li class="hide-on-large-only" style="" >
                        <a class="grey-text darken-5" href="<?php echo base_url() ?>proveedor/MiCuenta">
                            <i class="material-icons left dorado-2-text">notifications</i>
                            Notificaciones
                        </a>
                    </li>
                    <li class="divider hide-on-large-only"></li>
                    <li class="hide-on-large-only" style="">
                        <a class="grey-text darken-5" href="<?php echo base_url() ?>proveedor">
                            <i class="material-icons left dorado-2-text">home</i>
                            Mi Perfil
                        </a>
                    </li>
                    <li class="divider hide-on-large-only" style=""></li>
                    <li class="hide-on-large-only" style="">
                        <a class="grey-text darken-5" href="<?php echo base_url() ?>proveedor/escaparate">
                            <i class="material-icons left dorado-2-text">art_track</i>
                            Escaparate
                        </a>
                    </li>
                    <li class="divider hide-on-large-only" style=""></li>
                    <li class="menu-provider hide-on-large-only">
                        <a class="grey-text darken-5"
                           href="<?php echo base_url() ?>proveedor/solicitudes">
                            <?php $sol = $this->checker->numSolicitudes(); ?>
                            <i class="material-icons left dorado-2-text<?php echo (strpos($controller,
                                "solicitudes")) ? '' : '-o'; ?>" <?php echo $sol ? "style='display: block;color: #8dd7e0' " : "" ?> >mail
                            </i>
                            <?php if ($sol > 99) { ?>
                                <span class="badge-news" style="font-size: 8px"><?php echo "99+" ?></span>
                            <?php } else { ?>
                                <span class="badge-news"><?php echo $sol ?></span>
                            <?php } ?>
                            Solicitudes
                        </a>
                    </li>
                    <li class="divider hide-on-large-only" style=""></li>
                    <li class="hide-on-large-only" style="">
                        <a class="grey-text darken-5" href="<?php echo site_url("proveedor/recomendaciones") ?>">
                            <i class="material-icons left dorado-2-text">assignment</i>
                            Recomendaciones
                        </a>
                    </li>
                    <li class="divider" style=""></li>
                    <!-- <li style="">
                        <a class="grey-text darken-5" href="<?php echo site_url("proveedor/ofertas") ?>">
                            <i class="material-icons left dorado-2-text">assignment</i>
                            Ofertas
                        </a>
                    </li>
                    <li class="divider" style=""></li> -->
                    <li style="">
                        <a class="grey-text darken-5" href="<?php echo base_url() ?>proveedor/MiCuenta">
                            <i class="material-icons left dorado-2-text">build</i>
                            Mi Cuenta
                        </a>
                    </li>
                    <li class="divider" style=""></li>
                    <li>
                        <a class="grey-text darken-5" href="<?php echo base_url()."cuenta/logout" ?>">
                            <i class="material-icons left dorado-2-text">close</i>
                            Cerrar Sesi&oacute;n
                        </a>
                    </li>
                </ul>
            </div>
     </div>

    <!-- <hr class="col l12 hide-on-small-only" style="border: 1px solid; border-color: #F8DADF"> -->
</div>
</div>