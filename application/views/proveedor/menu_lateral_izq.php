<?php $lbl_estado = $estado != "ALL" ? "en $estado" : "" ?>
    <section id="filtro">
        <h6>Filtrar por</h6>
        <div class="divider"></div>
        <ul class="collapsible" data-collapsible="expandable">
            <li>
                <div id="estadoCollapsible" class="collapsible-header">Estado:
                    <text class="dorado-2-text estado-selected"><?php echo($estado == "ALL" ? "" : $estado) ?></text>
                </div>
                <div class="collapsible-body" style="font-size: 0.8em;max-height: 250px;overflow-y: scroll">
                    <div class="collection ">
                        <?php if (isset($isSector)) : ?>
                            <a class="collection-item grey lighten-5 black-text clickable"
                               href="<?php echo base_url() ?>index.php/proveedores/sector/proveedores/?<?php echo((!empty($estado) && $estado != 'ALL' ? "&estado=" . str_replace(" ", "-", $estado) : '') . '&ver=' . $ver) ?>">
                                <?php echo(isset($estado) && $estado == 'ALL' ? '<b>Todos los estados</b>' : 'Todos los estados') ?>
                            </a>
                            <?php foreach ($estados as $value) { ?>
                                <a class="collection-item grey lighten-5 black-text clickable"
                                   href="<?php echo base_url() ?>index.php/proveedores/sector/proveedores/?<?php echo("estado=" . str_replace(" ", "-", $value->estado) . (!empty($city) ? "&ciudad=" . str_replace(" ", "-", $city) : '') . '&ver=' . $ver) ?>">
                                    <?php echo(!empty($state) && $state == $value->estado ? "<b>$value->estado</b>" : "$value->estado") ?>
                                </a>
                            <?php } ?>
                        <?php else : ?>
                            <a class="collection-item grey lighten-5 black-text clickable"
                               href="<?php echo base_url() ?>index.php/proveedores/categoria/?<?php echo("id=" . $category->id . (!empty($estado) && $estado != 'ALL' ? "&estado=" . str_replace(" ", "-", $estado) : '') . (!empty($city) ? "&ciudad=" . str_replace(" ", "-", $city) : '') . '&ver=' . $ver) ?>">
                                <?php echo(isset($estado) && $estado == "ALL" ? '<b>Todos los estados</b>' : 'Todos los estados') ?>
                            </a>

                            <?php foreach ($estados as $value) : ?>
                                <a class="collection-item grey lighten-5 black-text clickable"
                                   href="<?php echo base_url() ?>index.php/proveedores/categoria/?<?php echo("id=" . $category->id . "&estado=" . str_replace(" ", "-", $value->estado) . (!empty($city) ? "&ciudad=" . str_replace(" ", "-", $city) : '') . '&ver=' . $ver) ?>">
                                    <?php echo(!empty($state) && $state == $value->estado ? "<b>$value->estado</b>" : "$value->estado") ?>
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </li>
            <?php $estado_re = '/' . str_replace(' ', '-', $estado); ?>

            <?php if (isset($filtros)) { ?>
                <script>
                    function create_noUISlider(elem, min, max, select) {
                        if (!select) {
                            select = [min, max];
                        }
                        var slider = document.getElementById(elem);
                        noUiSlider.create(slider, {
                            start: select,
                            connect: true,
                            step: 10,
                            range: {
                                'min': min,
                                'max': max
                            },
                            format: wNumb({
                                decimals: 0
                            }),
                            pips: {
                                mode: 'range',
                                density: 5
                            }
                        });
                        slider.noUiSlider.on('change', function () {
                            var tipo = elem;
                            var params = location.getParams();
                            tipo = tipo.split(" ").join("_");
                            var d = new Array();
                            params[tipo] = this.get().join(",");
                            for (var i in params) {
                                d.push(i + '=' + params[i]);
                            }
                            setTimeout(function () {
                                location.href = location.href.split("?")[0] + "?" + d.join("&");
                            }, 300);
                        });
                    }

                    function getParams() {
                        var result = {};
                        var tmp = [];
                        if (location.search) {
                            location.search.substr(1).split("&")
                                .forEach(function (item) {
                                    tmp = item.split("=");
                                    result [tmp[0]] = decodeURIComponent(tmp[1]);
                                });
                        }
                        return result;
                    }

                    location.getParams = getParams;

                    function menu_lateral_change_check(elem, tipo) {
                        var search = new Array();
                        var params = location.getParams();
                        var d = new Array();
                        var ok = true;
                        tipo = tipo.split(" ").join("_");
                        $("#" + tipo + " input:checked").each(function () {
                            search.push(this.value.split(" ").join("_"));
                        });
                        for (var i in params) {
                            if (i == tipo) {
                                params[i] = search.join(",");
                                ok = false;
                            }
                        }
                        if (ok) {
                            params[tipo] = search.join(",");
                        }
                        if (params[tipo] == "") {
                            delete params[tipo];
                        }
                        for (var i in params) {
                            d.push(i + '=' + params[i]);
                        }
                        location.href = location.href.split("?")[0] + "?" + d.join("&");
                    }

                    function menu_lateral_change_boolean(elem, tipo) {
                        var params = location.getParams();
                        var d = new Array();
                        tipo = tipo.split(" ").join("_");
                        params[tipo] = $(elem).prop("checked") ? "si" : "";
                        if (params[tipo] == "") {
                            delete params[tipo];
                        }
                        for (var i in params) {
                            d.push(i + '=' + params[i]);
                        }
                        setTimeout(function () {
                            location.href = location.href.split("?")[0] + "?" + d.join("&");
                        }, 300);

                    }
                </script>
            <?php
            foreach ($filtros

            as $key => $value) {
            if ($value->tipo == 'RANGE' && $value->valores == null) {
                continue;
            }
            ?>
                <li>
                    <div class="collapsible-header active">
                        <?php echo $value->titulo ?>:
                        <text class="dorado-2-text"></text>
                    </div>
                    <div class="collapsible-body" style="font-size: 0.8em;max-height: 250px;overflow-y: scroll">
                        <div class="collection" id="<?php echo implode("_", explode(" ", $value->titulo)) ?>">
                            <?php if ($value->tipo == "BOOLEAN") { ?>
                                <div class="collection-item grey lighten-5 black-text ">
                                    <div class="switch">
                                        <label>
                                            No
                                            <input type="checkbox"
                                                   onchange="menu_lateral_change_boolean(this, '<?php echo $value->titulo ?>')" <?php echo in_array('si', $value->select) ? "checked" : "" ?>>
                                            <span class="lever"></span>
                                            SI
                                        </label>
                                    </div>
                                </div>
                            <?php } else if ($value->tipo == 'CHECKBOX' || $value->tipo == 'SELECT') { ?>
                                <?php $valores = explode('|', $value->valores) ?>
                                <?php foreach ($valores as $key => $val) { ?>
                                    <div class="collection-item grey lighten-5 black-text ">
                                        <input type="checkbox" id="<?php echo $value->titulo . $val ?>"
                                               value="<?php echo $val ?>"
                                               onchange="menu_lateral_change_check(this, '<?php echo $value->titulo ?>')" <?php echo in_array($val, $value->select) ? "checked" : "" ?> />
                                        <label for="<?php echo $value->titulo . $val ?>"><?php echo $val ?></label>
                                    </div>
                                <?php } ?>
                            <?php } else if ($value->tipo == 'RANGE') { ?>
                                <div class="collection-item grey lighten-5 black-text "
                                     style="padding: 60px 0px 60px 0px;" s>
                                    <div id="<?php echo $value->titulo ?>" class="ui-range" style="margin: 0px 15px;">
                                    </div>
                                    <script>create_noUISlider(<?php echo "'$value->titulo',$value->minimo,$value->maximo" . (count($value->select) >= 2 ? ',[' . $value->select[0] . ',' . $value->select[1] . "]" : "") ?>);</script>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </li>
                <?php
            }
            }
            ?>
        </ul>
    </section>
<?php if (isset($promociones) && count($promociones) > 0) { ?>
    <section id="promociones_en_tu_zona">
        <h6>Promociones en tu zona</h6>
        <div class="divider"></div>
        <div class="collection z-depth-1">
            <?php foreach ($promociones as $key => $promocion) { ?>
                <a href="<?php echo base_url() ?>index.php/escaparate/promociones/<?php echo $promocion->id_proveedor ?>/<?php echo $promocion->id_promocion ?>"
                   class="collection-item avatar black-text">
                    <i class="fa <?php echo $promocion->tipo == 'OFERTA' ? 'fa-tag  yellow' : $promocion->tipo == 'REGALO' ? 'fa-gift  yellow' : 'fa-star orange' ?>  fa-tag circle  darken-3"></i>
                    <span class="title"><?php echo $promocion->nombre ?></span>
                    <p>
                        <small>
                            <?php echo $promocion->nombre_tipo_proveedor ?>:
                            <b> <?php echo $promocion->nombre_proveedor ?></b>
                        </small>
                        <br>
                        <label>
                            <?php echo $promocion->localizacion_poblacion ?>
                            (<?php echo $promocion->localizacion_estado ?>)
                        </label>
                    </p>
                </a>
            <?php } ?>
        </div>
        <p>
            <a class="btn-flat dorado-2-text pull-right" style="font-size: 0.8em">
                Ver todas las promociones <i class="fa fa-chevron-right"></i>
            </a>
        </p>
    </section>
<?php } ?>