<html lang="es" xml:lang="es">
<head>
    <?php $this->view("proveedor/header") ?>
    <style>
        @media only screen and (max-width: 321px) {
            .titleplaced {
                font-size: 20px !important;
            }
        }

        @media only screen and (max-width: 425px) {
            .space-top {
                margin-top: 20px !important;
            }

            .menu-provider-item {
                font-size: 31px !important;
                width: 50% !important;
            }

            .titleplaced {
                text-align: center;
            }

        }
    </style>
</head>
<body>
<?php $this->view("proveedor/menu") ?>
<div class="body-container">
    <?php $this->view("proveedor/mensajes") ?>
    <div class="row">
        <div class="col s2 m4 l3 sidebar hide-on-small-only">
            <div class="collection with-header ">
                <a class="collection-item black-text "
                   href="<?php echo base_url() ?>boda-<?php echo $this->session->userdata("slug") ?>">
                    <h6>
                        <i class="fa fa-home"> </i>
                        <text class="hide-on-small-only">Mi escaparate <br>
                            <small style="font-size: 11px">Ver mi escaparate ></small>
                        </text>
                    </h6>
                </a>
                <a href="#notificaciones" class="collection-item black-text active toggle-show"
                   data-toggle="#notificaciones">
                    <i class="fa fa-bell"> </i>
                    <text class="hide-on-small-only">Notificaciones
                        <text>
                </a>
                <a href="#sello" class="collection-item black-text  toggle-show " data-toggle="#sello" style="display: none;">
                    <i class="fa fa-certificate  "> </i>
                    <text class="hide-on-small-only">Sello colaborador
                        <text>
                </a>
                <!--                        <a href="<?php echo site_url("proveedor/socialmedia") ?>" class="collection-item black-text" >
                            <i class="fa fa-share-alt  ">       </i> <text class="hide-on-small-only">Redes sociales<text>
                        </a>-->
            </div>
        </div>
        <div class="menu-provider-esc hide-on-med-and-up">

            <a class="menu-provider-item black-text "
               href="<?php echo base_url() ?>boda-<?php echo $this->session->userdata("slug") ?>">
                <i class="fa fa-home"></i>
            </a>
            <a href="#notificaciones" class="menu-provider-item black-text active toggle-show"
               data-toggle="#notificaciones">
                <i class="fa fa-bell"></i>
            </a>
            <a href="#sello" class="menu-provider-item black-text  toggle-show " data-toggle="#sello" style="display: none;">
                <i class="fa fa-certificate"></i>
            </a>
        </div>
        <div class="col s12 m8 l9">
            <section id="notificaciones">
                <h5 class="titleplaced">Notificaciones</h5>
                <div class="divider"></div>
                <div class="card-panel blue lighten-5">
                    Los emails que te enviamos son informaci&oacute;n importante para que tu anuncio tenga m&aacute;s
                    &eacute;xito.
                    No queremos que recibas emails que no te interesan, as&iacute; que aqu&iacute; puedes seleccionar
                    los que quieres recibir y los que no.
                </div>
                <div class="card-panel">
                    <form method="POST" action="<?php echo base_url() ?>proveedor/MiCuenta">
                        <p>
                            <input name="informacion"
                                   type="checkbox" <?php echo $proveedor->notificacion_informacion ? "checked" : "" ?>
                                   id="news"/>
                            <label for="news">Informacion mensual con datos de mi escaparate </label>
                        </p>
                        <p>
                            <input name="emails"
                                   type="checkbox" <?php echo $proveedor->notificacion_emails ? "checked" : "" ?>
                                   id="emails"/>
                            <label for="emails">Emails de formacion durante la primera semana</label>
                        </p>
                        <p>
                            <input name="alertas"
                                   type="checkbox" <?php echo $proveedor->notificacion_alertas ? "checked" : "" ?>
                                   id="alerts"/>
                            <label for="alerts">Alertas para saber qu&eacute; mejorar de mi escaparate</label>
                        </p>
                        <input name="clsr" value="1vbe23iv4fghhjg6jk4112sdcv" type="hidden">
                        <p>
                            <button value="guardar" type="submit" class="btn dorado-2 space-top">
                                Guardar
                            </button>
                        </p>
                    </form>

                </div>
            </section>
            <section class="hide" id="sello">
                <h5 class="titleplaced">Sello colaborador de japybodas.com</h5>
                <div class="divider"></div>
                <div class="card-panel blue lighten-5">
                    Estamos orgullosos de ayudarle a generar negocio.
                    Escoja el tamaño que mejor se adapte a su p&aacute;gina web
                    y pegue el c&oacute;digo html de su sello de colaborador
                    ¡Muchas gracias por colaborar con nosotros!
                </div>
                <div class="col m6 s12  center-align">
                    <a class="row" href="<?php echo site_url("boda-".$this->session->userdata("slug")) ?>">
                        <img width="150" src="<?php echo base_url() ?>dist/img/sello_colaborador.png" alt=""/>
                    </a>
                    <label class="row">
                        150 x 200
                    </label>
                    <textarea><a href="<?php echo site_url("boda-".$this->session->userdata("slug")) ?>"><img
                                    width="150" src="<?php echo base_url() ?>dist/img/sello_colaborador.png"
                                    alt=""/></a> </textarea>
                </div>
                <div class="col m6 s12 center-align">
                    <a class="row" href="<?php echo site_url("boda-".$this->session->userdata("slug")) ?>">
                        <img width="220" src="<?php echo base_url() ?>dist/img/sello_colaborador.png" alt=""/>
                    </a>
                    <label class="row">
                        220 x 290
                    </label>
                    <textarea><a href="<?php echo site_url("boda-".$this->session->userdata("slug")) ?>"><img
                                    width="220" src="<?php echo base_url() ?>dist/img/sello_colaborador.png"
                                    alt=""/></a> </textarea>
                </div>
            </section>
        </div>
    </div>
</div>
<script>
</script>
<?php $this->view("proveedor/footer") ?>
<script>
    $(document).ready(function () {
        var hash = window.location.hash;
        if (hash) {
            if (hash == "#sello" || hash == "#notificaciones") {
                $(hash).removeClass("hide");
                $(".toggle-show.active").removeClass("active");
                $("a[data-toggle='" + hash + "']").addClass("active");
                if (hash == "#sello") {
                    $("#notificaciones").addClass("hide");
                } else {
                    $("#sello").addClass("hide");
                }
            }
        }
        $(".toggle-show").on("click", function () {
            var $this = $(this);
            var $e = $($this.data("toggle"));
            if ($e.hasClass("hide")) {
                $($(".toggle-show.active").data("toggle")).addClass("hide");
                $(".toggle-show.active").removeClass("active");
                $this.addClass("active");
                $e.removeClass("hide");
            }
        });
    });
</script>

</body>

</html>