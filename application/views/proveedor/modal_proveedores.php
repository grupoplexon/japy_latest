<div id="modal-proveedor" class="modal">
    <div class="modal-content">
        <h4 id="modal-title" style="margin: 0;">Proveedor sugeridos</h4>
        <div class="row" style="margin: 0;">
        </div>
    </div>
    <div class="modal-footer">
        <ul class="pagination">
            <li class="waves-effect" id="previousPage">
                <a href="#!" style="color: black !important;"><i class="material-icons" >chevron_left</i></a>
            </li>
            <li>
                    <span>
                        <span id="currentPage"></span>
                        /
                        <span id="lastPage"></span>
                    </span>
            </li>
            <li class="waves-effect" id="nextPage">
                <a href="#!" style="color: black !important;"><i class="material-icons">chevron_right</i></a>
            </li>
        </ul>
    </div>
</div>

<div class="col s12 m4" id="card-proveedor">
    <div class="card">
        <a href="" target="_blank">
            <div class="card-image">
                <img src="http://materializecss.com/images/samplce-1.jpg" style="height: 120px;object-fit:cover;">
                <span class="card-title"
                      style="top:0;bottom:0;left:0;right:0;height:100px;margin:auto;padding: 0;display: flex;justify-content:center;align-items: center;text-align: center;">
                </span>
            </div>
        </a>
        <div class="card-content">
            <span></span>
        </div>
        <div class="card-action" style="display:flex;justify-content:center;">
            <a class="waves-effect waves-light btn save-provider" style="padding: 0; font-size: 0.6rem;">Guardar</a>
            <a class="waves-effect waves-light btn select-provider" style="padding: 0; font-size: 0.6rem;">Seleccionar</a>
        </div>
    </div>
</div>

<input id="base-url" type="hidden" value="<?php echo base_url() ?>"/>

