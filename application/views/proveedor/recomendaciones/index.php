<html>
    <head>
        <?php $this->view("proveedor/header") ?>
    </head>
    <body>
        <?php $this->view("proveedor/menu") ?>
        <div class="body-container">
            <div class="row">
                <?php // $this->view("proveedor/escaparate/menu") ?>
                <div class="col s12">
                    <?php $this->view("proveedor/mensajes") ?>
                    <section>
                        <h5>Recomendaciones</h5>
                        <div class="divider"></div>
                        <div class="card-panel blue lighten-5">
                            <i class="fa fa-info "></i>
                            Las buenas recomendaciones ayudan a que m&aacute;s clientes te contacten <br>
                        </div>
                        <div class="col m12 s12">
                            <?php if (($recomendaciones)) { ?>
                                <div class=" yellow-text text-darken-3 starrange">
                                    <?php $average  = 0;
                                    $currentReviews = 0 ?>
                                    <?php  $weddings = $provider->weddings->where("visible",0);
                                    foreach ($weddings as $wedding) :
                                        if ($wedding->pivot->calificacion > 0 ) {
                                            $average = $average + $wedding->pivot->calificacion;
                                            $currentReviews++;
                                        }
                                    endforeach;
                                    if ($currentReviews == 0):
                                        $average = $average / 1;
                                    else:
                                        $average = $average / $currentReviews;
                                    endif;
                                    ?>
                                    <?php if ($average > 0) : ?>
                                        <?php for ($i = 0; $i < 5; $i++) : ?>
                                            <i class="fa fa-star" <?php if ($i < round($average)) {
                                                echo("style='color:#f9a825;'");
                                            } else {
                                                echo("style='color:gray;'");
                                            } ?>></i>
                                        <?php endfor; ?>
                                    <?php else : ?>
                                        <?php for ($i = 0; $i < 5; $i++) : ?>
                                            <i class="fa fa-star" style="color:gray;"></i>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    <h4 class="primary-text">  <?php echo round($average) ?></h4><h6 class="primary-text">/ 5</h6>
                                </div>
                                <?php foreach ($recomendaciones as $key => $r) { ?>
                                    <div class="card-panel z-depth-1 hide" id="<?php echo $r->boda_id ?>">
                                        <div class="row">
                                            <div class="col s2">
                                                <img src="<?php echo base_url() ?>index.php/perfil/foto/<?php echo $r->usuario_id ?>" class="responsive-img circle">
                                                <button style="font-size: 8px; padding: 2px; margin-top: 5px; width: 100%;"
                                                        data-activates='dropdown-estado-<?php echo $r->boda_id ?>' data-beloworigin="true" data-constrainwidth="false"
                                                        class="dropdown-button btn white-text center <?php echo($r->activo ? "green" : "grey") ?>">
                                                    <?php echo($r->activo ? "publicado" : "no publicado") ?>
                                                    <i class="material-icons right">keyboard_arrow_down</i>
                                                </button>
                                                <ul id='dropdown-estado-<?php echo $r->boda_id ?>' class='dropdown-content'>
                                                    <li><a href="#!" class=" black-text change-estado" data-value="1" data-boda="<?php echo $r->boda_id ?>"> <i class="material-icons left">check</i> Publicar</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#!" class=" black-text change-estado" data-value="0" data-boda="<?php echo $r->boda_id ?>"><i class="material-icons left">close</i> No publicar</a></li>
                                                </ul>
                                            </div>
                                            <div class="col s10">
                                                <div class="col s12 ">
                                                    <?php echo $r->nombre ?>
                                                    <small>
                                                        <?php echo relativeTimeFormat($r->fecha_creacion) ?>
                                                    </small>
                                                    <small class="pull-right hide">
                                                        <i class="fa fa-genderless"></i>
                                                        <?php
                                                        //$now        = new DateTime("now");
                                                        //$fecha_boda = new DateTime($r->fecha_boda);
                                                        ?>
                                                        <?php// if ($fecha_boda < $now) { ?>
                                                            Se cas&oacute; el <?php// echo $fecha_boda->format("d-m-Y") ?>
                                                        <?php //} else { ?>
                                                            Se casa el <?php //echo $fecha_boda->format("d-m-Y") ?>
                                                        <?php //} ?>
                                                    </small>
                                                </div>
                                                <div class="col s6">
                                                    <div class="row">
                                                        <div class="col s12   m6 text-recomendacion ">
                                                            Calidad del servicio:
                                                        </div>
                                                        <div class="col s12  m6  ">
                                                            <div class="progress amber accent-2">
                                                                <div class="determinate dorado-2" style="width: <?php echo$r->servicio ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col s12   m6 text-recomendacion ">
                                                            Respuesta:
                                                        </div>
                                                        <div class="col s12  m6  ">
                                                            <div class="progress amber accent-2">
                                                                <div class="determinate dorado-2" style="width: <?php echo$r->respuesta ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col s12   m6 text-recomendacion ">
                                                            Relaci&oacute;n calidad/precio:
                                                        </div>
                                                        <div class="col s12  m6  ">
                                                            <div class="progress amber accent-2">
                                                                <div class="determinate dorado-2" style="width: <?php echo$r->calidad_precio ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s6">
                                                    <div class="row">
                                                        <div class="col s12 m6 text-recomendacion ">
                                                            Flexibilidad:<small><b><?php echo $r->profesionalidad ?>%</b></small>
                                                        </div>
                                                        <div class="col s12  m6  ">
                                                            <div class="progress amber accent-2">
                                                                <div class="determinate dorado-2" style="width: <?php echo$r->flexibilidad ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col s12 m6 text-recomendacion ">
                                                            Profesionalismo: <small><b><?php echo $r->profesionalidad ?>%</b></small>
                                                        </div>
                                                        <div class="col s12  m6  ">
                                                            <div class="progress amber accent-2">
                                                                <div class="determinate dorado-2" style="width: <?php echo$r->profesionalidad ?>%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="col s12 text-justify">
                                                <?php echo $r->mensaje ?>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="col s12">
                                                <p>
                                                    <a class=" btn-flat waves-effect waves-blue pull-right btn-share-link" data-href="<?php echo site_url("escaparate/recomendaciones/$r->proveedor_id/$r->boda_id") ?>" data-quote="<?php echo substr($r->mensaje, 0, 150) ?>...">
                                                        <i class="fa fa-facebook-official"> </i> Compartir
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-panel">
                                        <div class="row" style="margin: 0">
                                            <div class="col s6 m2 offset-s3 center-align" style="padding-top: 1rem">
                                                <img src="<?php echo $this->config->base_url() ?>perfil/foto/<?php echo $r->usuario_id?>" alt="" class="review-photo">
                                                <button style="font-size: 1rem; padding: 2px; margin-top: 5px; width: 100%;"
                                                        data-activates='dropdown-estado-<?php echo $r->boda_id ?>' data-beloworigin="true" data-constrainwidth="false"
                                                        class="dropdown-button btn white-text center <?php echo($r->activo ? "green" : "grey") ?> hide">
                                                    <?php echo($r->activo ? "publicado" : "no publicado") ?>
                                                    <i class="material-icons right">keyboard_arrow_down</i>
                                                </button>
                                            </div>
                                            <div class="col s12 m10">
                                                <h6>
                                                    <?php $novio = User::find($r->usuario_id);
                                                    echo $novio->nombre." ".$novio->apellido?>,
                                                    <small> <?php echo relativeTimeFormat($r->fecha_creacion) ?></small>
                                                </h6>
                                                <small class="pull-right hide" >
                                                    <?php
                                                    $now        = new DateTime("now");
                                                    $fecha_boda = new DateTime($r->fecha_boda);
                                                    ?>
                                                    <?php if ($fecha_boda < $now) { ?>
                                                        Se cas&oacute; el <?php echo $fecha_boda->format("d-m-Y") ?>
                                                    <?php } else { ?>
                                                        Se casa el <?php echo $fecha_boda->format("d-m-Y") ?>
                                                    <?php } ?>
                                                </small>
                                                <div class=" yellow-text text-darken-3">
                                                    <?php
                                                    $stars = 0;
                                                    foreach ($starsReviews as $s):
                                                        $id_boda = (int)$s->id_boda;
                                                        if($id_boda === $r->boda_id):
                                                            $stars = (int)$s->calificacion;
                                                        endif;
                                                    endforeach;
                                                    for ($i = 0; $i < 5; $i++) : ?>
                                                        <i class="fa fa-star" <?php if ($i < round($stars)) {
                                                            echo("style='color:#f9a825;'");
                                                        } else {
                                                            echo("style='color:gray;'");
                                                        } ?>></i>
                                                    <?php endfor; ?>
                                                    <a href="#" class="moreInfo">
                                                        <small >ver m&aacute;s</small>
                                                    </a>
                                                </div>
                                                <div  class="row extraInfo hide">
                                                    <div class="col s6">
                                                        <div class="row no-margin">
                                                            <div class="col s12   m6 no-padding 6 text-recomendacion ">
                                                                Servicio: <b><?php echo($r->servicio) ?>%</b>
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress " style="background-color: #c3c3c3">
                                                                    <div class="determinate dorado-1" style="width: <?php echo($r->servicio) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row no-margin">
                                                            <div class="col s12   m6 no-padding 6 text-recomendacion ">
                                                                Respuesta: <b><?php echo($r->respuesta) ?>%</b>
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress " style="background-color: #c3c3c3">
                                                                    <div class="determinate dorado-1" style="width: <?php echo($r->respuesta) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row no-margin">
                                                            <div class="col s12   m6 no-padding 6 text-recomendacion ">
                                                                Calidad/Precio:  <b><?php echo($r->calidad_precio) ?>%</b>
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress " style="background-color: #c3c3c3">
                                                                    <div class="determinate dorado-1" style="width: <?php echo($r->calidad_precio) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col s6">
                                                        <div class="row no-margin">
                                                            <div class="col s12 m6 no-padding text-recomendacion ">
                                                                Flexibilidad: <b><?php echo($r->flexibilidad) ?>%</b>
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress " style="background-color: #c3c3c3">
                                                                    <div class="determinate dorado-1" style="width: <?php echo($r->flexibilidad) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row no-margin">
                                                            <div class="col s12 m6 no-padding text-recomendacion ">
                                                                Profesionalismo: <b><?php echo($r->flexibilidad) ?>%</b>
                                                            </div>
                                                            <div class="col s12  m6  ">
                                                                <div class="progress " style="background-color: #c3c3c3">
                                                                    <div class="determinate dorado-1" style="width: <?php echo($r->profesionalidad) ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h5><b><?php echo $r->asunto?></b></h5>
                                                <p class="text-justify" style="">
                                                    <?php echo $r->mensaje ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="card-panel z-depth-1">
                                    <p class="center">
                                        <i class="material-icons fa-4x">info_outline</i><br>
                                        A&uacute;n no tienes recomendaciones, Pide a tus clientes que te recomienden
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col m8 s12">
                            <ul class="pagination">
                                <li class="<?php echo($page == 1 ? "disabled" : "waves-effect") ?>"><a href="<?php echo($page == 1 ? "#" : site_url("proveedor/escaparate/recomendaciones")."?pagina=".($page - 1)) ?>"><i class="material-icons">chevron_left</i></a></li>
                                <?php for ($i = 1; $i <= $total; $i++) { ?>
                                    <li class="<?php echo($i == $page ? "active" : "waves-effect") ?>"><a href="<?php echo site_url("proveedor/escaparate/recomendaciones")."?pagina=".$i ?>"><?php echo $i ?></a></li>
                                <?php } ?>
                                <li class="<?php echo($page >= ($total - 1) ? "disabled" : "waves-effect") ?>"><a href="<?php echo($page >= ($total - 1) ? "#" : site_url("proveedor/escaparate/recomendaciones")."?pagina=".($page + 1)) ?>"><i class="material-icons">chevron_right</i></a></li>
                            </ul>

                        </div>
                    </section>

                </div>
            </div>
        </div>
        <?php $this->view("proveedor/footer") ?>
        <script>
            var server = "<?php echo base_url() ?>";
            $(document).ready(function() {
                let $moreInfo = $('.moreInfo');
                let seeLessData = true;

                $moreInfo.on('click',function (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    $(this).parent().siblings('.extraInfo').toggleClass('hide',500);
                    if(seeLessData){
                        $(this).find('small').text("Ver menos");
                        seeLessData = false;
                    }else{
                        $(this).find('small').text("Ver más");
                        seeLessData = true;
                    }
                });

                $('.change-estado').on('click', function() {
                    var $this = $(this);
                    var estado = $this.data('value');
                    var boda = $this.data('boda');
                    $.ajax({
                        url: server + 'index.php/proveedor/escaparate/publicar_recomendacion',
                        method: 'POST',
                        data: {
                            estado: estado,
                            boda: boda,
                        },
                        success: function() {
                            var $btn = $('#' + boda + ' .dropdown-button ');
                            if (estado == 1) {
                                $btn.removeClass('grey');
                                $btn.addClass('green');
                                $btn.html('Publicado');
                            }
                            else {
                                $btn.addClass('grey');
                                $btn.removeClass('green');
                                $btn.html('No publicado');
                            }
                            $btn.append('<i class="material-icons right">keyboard_arrow_down</i>');
                        },
                    });
                });
                var fbclub = new FBclub("<?php echo $this->config->item("api_id", "facebook") ?>");
                fbclub.initShareLinks();
            });
        </script>

    </body>

</html>