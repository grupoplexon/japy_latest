<html>
    <head>
        <?php $this->view("proveedor/header") ?>
        <style>

            .bg-fb {
                background-color: #3b5998;
            }
            .bg-fb:hover{
                background-color: #1667b3;
            }
        </style>
    </head>
    <body >
        <?php $this->view("proveedor/menu") ?>
        <div class="body-container">
            <?php $this->view("proveedor/mensajes") ?>
            <div class="row">
                <div class="col s12">
                    <section id="notificaciones">
                        <h5>Redes Sociales</h5>
                        <div class="divider"></div>
                        <div class="card-panel blue lighten-5">
                            <i class="fa fa-info"></i> Vinculate con redes sociales y comparte directamente a tu FanPage tus recomendaciones, eventos y escaparate 
                        </div>
                        <div class="card-panel">
                            <?php if (!$this->checker->hasFb()) { ?>
                                <a href="<?php echo $this->fb->link_url() ?>" class="btn  btn-block-on-small  bg-fb">
                                    <i class="fa fa-facebook-official"></i> Facebook
                                </a>
                            <?php } else { ?>
                                <a href="<?php echo site_url("") ?>" class="btn  btn-block-on-small  bg-fb">
                                    <i class="fa fa-facebook-official"></i> Desvincular FB
                                </a>
                            <?php } ?>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <script>
        </script>
        <?php $this->view("proveedor/footer") ?>
    </body>

</html>