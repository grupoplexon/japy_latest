<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            td {padding:2em; border:1px dotted #000;}
            body {font-size:170%;}
            .f1 {font-family:helvetica;
                 text-align:center;
                 vertical-align:middle;
            }
            .f2 {font-family:arial;
                 background-color:yellow;
                 text-align:center;
                 font-weight:bold;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <td class="f2">DIA</td>
                <td class="f2">HORA</td>
                <td class="f2">NOMBRE</td>
                <td class="f2">TEL&Eacute;FONO</td>
                <td class="f2">MAIL</td>
                <td class="f2">FECHA EVENTO</td>
                <td class="f2">COMENSALES</td>
                <td class="f2">COMENTARIOS</td>
                <td class="f2">ESTADO</td>
            </tr>
            <?php if ($solicitudes) { ?>
                <?php foreach ($solicitudes as $key => $s) { ?>
                    <tr>
                        <td class="f1"><?php echo dateFormat($s->fecha_creacion, "%Y-%m-%d") ?></td>
                        <td class="f1"><?php echo dateFormat($s->fecha_creacion, "%R") ?></td>
                        <td class="f1"><?php echo $s->nombre ?> <?php echo $s->apellido ?></td>
                        <td class="f1">3310498825</td>
                        <td class="f1"><?php echo $s->correo ?></td>
                        <td class="f1"><?php echo dateFormat($s->fecha_boda, "%Y-%m-%d") ?></td>
                        <td class="f1"><?php echo $s->no_invitado ?></td>
                        <td class="f1"><?php echo replaceAcentos($s->mensaje) ?></td>
                        <td class="f1"><?php echo label_estado($s->estado) ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </table>
    </body>
</html>
<?php

function label_estado($e) {
    switch ($e) {
        case 0:
            return "PENDIENTE";
        case 1:
            return "ATENDIDA";
        case 2:
            return "CONTRATADA";
        case 3:
            return "DESCARTADA";
    }
}

function replaceAcentos($string) {
    $vowel = array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;");
    $str = array("&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;");
    return str_replace($vowel, $str, $string);
}
?>
