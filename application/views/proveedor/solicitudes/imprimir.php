<html>
    <head>
        <title>Imprimir Solicitud</title>
        <?php $this->view("proveedor/header") ?>
        <style>
            .collection .collection-item.active {
                font-weight: bold;
            }
            .collection .collection-item.avatar.remove {
                opacity: 0;
                height: 0px;
                display: block;
                overflow: hidden;
                background: #8C8D8F;
                min-height: 0px;
                padding: 0px;
            }
            .collection .collection-item.avatar {
                transition: 1s;
            }
            .btn-flat.border{
                border: 1px solid #CCCCCC;
            }

            .btn-flat.border.block-in-small{
            }
            @media only screen and (max-width: 600px){

                .block-in-small{
                    width: 100%;
                    display: block;
                    margin-bottom: 10px;
                }
                .block-in-small .btn-flat{
                    width: 100%
                }
            }
            .dz-preview .dz-image img {
                width: 100px;
                height: 100px;
                background-size: contain;
                background-repeat: no-repeat;
            }

            .row .col.m5 {
                width: 41.6666666667%;
                margin-left: auto;
                left: auto;
                right: auto;
            }
            .row .col.m2 {
                width: 16.6666666667%;
                margin-left: auto;
                left: auto;
                right: auto;
            }
        </style>
    </head>
    <body >
        <div class="body-container">
            <div class="row">
                <div class="col s12">
                    <div class="row" >
                        <div class="col">
                            <img class="responsive-img" src="http://<?php echo base_url()?>dist/img/logo.png" alt="">
                        </div>
                        <button class="btn dorado-2 waves-effect right " onclick="this.style.display = 'none';window.print();" style="    margin-top: 25px;" > <i class="fa fa-print"></i> Imprimir</button>
                    </div>
                    <div class="divider"></div><br>
                    <div class="row" >
                        <h5>Solicitud de presupuesto</h5>
                    </div>
                    <div class="divider"></div><br>
                    <div class="row imprimible">
                        <div class="card-panel">
                            <div class="row">
                                <div class="col s12 m2">
                                    <img class="responsive-img" src="<?php echo base_url() ?>index.php/perfil/foto/<?php echo $novio->id_usuario ?>">
                                </div>
                                <div class="col s12 m5 ">
                                    <h6 class="dorado-2-text"><b><?php echo $novio->nombre ?> <?php echo $novio->apellido ?></b></h6> 
                                    <label><i class="fa fa-phone"></i> Tel&eacute;fono</label> <?php echo $cliente->telefono ?> <br>
                                    <label><i class="fa fa-envelope"></i> Email</label> <?php echo $novio->correo ?><br>
                                </div>
                                <div class="col s12 m5">
                                    <label><i class="fa fa-calendar-o"></i> Fecha del evento</label> <?php echo dateFormat($boda->fecha_boda) ?><br>
                                    <label><i class="fa fa-users"></i> Invitados</label> <?php echo $boda->no_invitado ?><br>
                                </div>
                            </div>
                            <div class="row text-justify" >
                                <label>Mensaje recibido: <?php echo relativeTimeFormat($solicitud->fecha_creacion) ?></label><br>
                                <?php echo $solicitud->mensaje ?>
                            </div>

                            <div class="row">                            
                            </div>
                        </div>
                        <section id="solicitud-nota" class="<?php echo ($solicitud->nota ? "" : "hide") ?>">
                            <h5>Nota</h5>
                            <div class="divider" style="margin-bottom:  20px;"></div>
                            <div class="card-panel">
                                <?php echo $solicitud->nota ?>
                            </div>
                        </section>
                        <?php if ($mensajes) { ?>
                            <section id="conversacion">

                                <h5 style="margin-top: 50px;">Mensajes</h5>
                                <div class="divider" style="margin-bottom:  20px;"></div>
                                <?php foreach ($mensajes as $key => $msj) { ?>
                                    <div class="card-panel">
                                        <?php if ($msj->id_proveedor) { ?>
                                            <i class="fa fa-mail-forward blue-text"></i>Yo,
                                        <?php } else { ?>
                                            <i class="fa fa-mail-reply green-text"></i><?php echo ($novio->nombre . " " . $novio->apellido) ?>,
                                        <?php } ?>
                                        <label>el <?php echo dateFormat($msj->fecha_respuesta) ?> a las <?php echo dateFormat($msj->fecha_respuesta, "%H:%M") ?></label>
                                        <div class="divider"></div>
                                        <div>
                                            <h6 style="font-weight: bold"><?php echo $msj->asunto ?></h6>
                                            <?php echo $msj->mensaje ?>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <?php foreach ($msj->archivos as $key => $archivo) { ?>
                                                <a href="<?php echo base_url() ?>index.php/proveedores/archivo/<?php echo $archivo->id_archivo ?>/<?php echo $archivo->nombre ?>" class="btn-flat dorado-2-text right" download > <i class="fa fa-download"></i> <?php echo $archivo->nombre ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </section>
                        <?php } ?>


                    </div>

                </div>
            </div>
        </div>


        <div id="modal-nota" class="modal ">
            <div class="modal-content">
                <h4>Nota</h4>
                <div class="divider"></div>
                <br>
                <textarea name="nota" class="form-control nota" style="height: 250px"><?php echo $solicitud->nota ?></textarea>
                <p class="grey-text">Solo ser&aacute; visible para la empresa en la seccion de mensajes</p>
            </div>
            <div class="modal-footer">
                <a class="modal-action modal-close waves-effect waves-green btn-flat guardar "   >Guardar</a>
                <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancelar</a>
            </div>
        </div>


        <div id="modal-reenviar" class="modal ">
            <form method="POST" action="<?php echo base_url() ?>index.php/proveedor/solicitudes/reenviar/<?php echo $solicitud->id_solicitud ?>">
                <div class="modal-content">
                    <h4>Reenviar solicitud</h4>
                    <div class="divider"></div>
                    <br>
                    <textarea name="correos" class="form-control nota" style="height: 250px"><?php echo $contacto->correo ?></textarea>
                    <p class="grey-text">Introduce los e-mails a los que quieres reenviar esta solicitud, separados por una coma</p>
                </div>
                <div class="modal-footer">
                    <button class="modal-action modal-close waves-effect waves-green btn-flat guardar " type="submit"   >Reenviar</button>
                    <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancelar</a>
                </div>
            </form>
        </div>

    </body>

</html>