<html>
    <head>
        <title><?php echo $titulo ?> : <?php echo $page ? "pagina ".$page : "" ?></title>
        <?php $this->view("proveedor/header") ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/solicitudes.css">
    </head>
    <body data-tipo='<?php echo $estado === null ? "leido" : "estado" ?>'
          data-valor='<?php echo $estado === null ? "$leido" : "$estado" ?>'>
        <?php $this->view("proveedor/menu") ?>

        <div class="body-container">
            <!-- FLASH MESSAGES -->
            <?php $this->view("proveedor/mensajes") ?>
            <div class="row">
                <?php $this->view("proveedor/solicitudes/menu") ?>
                <div class="col s12 m8 l9">
                    <!-- TITLE -->
                    <h5><?php echo $titulo ?></h5>

                    <div class="divider">
                    </div>
                    <!-- CARD INDICATORS -->
                    <div class="row" style="margin-top: 23px">
                        <?php $this->view("proveedor/solicitudes/info") ?>
                    </div>
                    <!-- CONTROLS -->
                    <div class="row" style="font-size: 11px">
                        <div class="col  check-all-wrapper ">
                            <input type="checkbox" id="test5" class="checkAll" data-checks=".check"/>
                            <label for="test5"></label>
                        </div>
                        <div class="col select ">
                            <a class='dropdown-button btn-flat ' data-constrainwidth="false" data-beloworigin="true"
                               href='#'
                               data-activates='drop-mover-a'>
                                <i class="fa fa-chevron-down right dorado-2-text"></i>
                                Mover a
                            </a>
                            <ul id='drop-mover-a' class='dropdown-content' style="width: 170px">
                                <li><a href="#!" data-color="orange" data-estado="0" class="black-text btn-mover"><i
                                                class="fa fa-clock-o orange-text"></i> Pendientes</a></li>
                                <li><a href="#!" data-color="blue" data-estado="1" class="black-text btn-mover"><i
                                                class="fa fa-reply   blue-text"></i> Atendida</a></li>
                                <li><a href="#!" data-color="green" data-estado="2" class="black-text btn-mover"><i
                                                class="fa fa-check-square-o green-text"></i> Contratada</a></li>
                                <li><a href="#!" data-color="red" data-estado="3" class="black-text btn-mover"><i
                                                class="fa fa-clock-o red-text"></i> Descartada</a></li>
                            </ul>
                        </div>
                        <div class="col select ">
                            <a id="btn-marcar" class='dropdown-button btn-flat truncate' data-beloworigin="true" href='#'
                               data-activates='drop-movers-a'>
                                <i class="fa fa-chevron-down right dorado-2-text "></i>
                                Marcar como
                            </a>
                            <ul id='drop-movers-a' class='dropdown-content'>
                                <li><a href="#!" data-estado='0' class="black-text btn-marcar">No le&iacute;da</a></li>
                                <li><a href="#!" data-estado='1' class="black-text btn-marcar">Leida</a></li>
                            </ul>
                        </div>
                        <div class="col s12 right">
                            <form id="form-search" method="GET"
                                  action="<?php echo base_url() ?>proveedor/solicitudes">
                                <?php if ($estado || $estado === 0) { ?>
                                    <input name="estado" type="hidden" value="<?php echo $estado ?>">
                                <?php } ?>
                                <?php if ($leido || $leido === 0) { ?>
                                    <input name="leido" type="hidden" value="<?php echo $leido ?>">
                                <?php } ?>
                                <div class=" search">
                                    <input class="form-control " id="search" name="search">
                                    <span><i class="fa fa-search"></i></span>
                                </div>
                                <div id='busqueda-avanzada'>
                                    <div class="contenedor">
                                        <h6>Busqueda avanzada</h6>
                                        <div class="row">
                                            <p>
                                                <label>Desde:</label>
                                                <input type="text" name="inicio" class="datepicker form-control">
                                            </p>
                                            <p>
                                                <label>Hasta:</label>
                                                <input type="text" name="fin" class="datepicker form-control">
                                            </p>
                                            <p>
                                                <input name="tipo" type="radio" value="solicitud" id="test1"/>
                                                <label for="test1">Fecha solicitud</label>
                                            </p>
                                            <p>
                                                <input name="tipo" type="radio" value="boda" id="test2"/>
                                                <label for="test2">Fecha boda</label>
                                            </p>
                                            <input type="hidden" name="accion" value="search">
                                            <p>
                                                <button type="submit" style="width: 100%" class="btn btn-block dorado-2">
                                                    Buscar
                                                </button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- MAILS & PAGINATION  -->
                    <div class="row">
                        <ul id="solicitudes" class="collection z-depth-1 ">
                            <?php if (($solicitudes)) { ?>
                                <?php foreach ($solicitudes as $key => $solicitud) : ?>
                                    <li id="<?php echo $solicitud->id_correo ?>" class="solicitud collection-item avatar"
                                        style="padding-left: 87px !important; <?php echo $solicitud->leida_proveedor == 0 || $solicitud->hasUnseenReplies($this->session->id_usuario) ? "background-color:#fbe0a9;" : "" ?>">
                                        <div class="circle" style="border-radius: 0px; height: 100%;width: 67px">
                                            <input type="checkbox" data-estado='<?php echo $solicitud->estado ?>'
                                                   data-leida="<?php echo $solicitud->leida_proveedor ?>"
                                                   id="check<?php echo $solicitud->id_correo ?>"
                                                   class="check" name="solicitud[]"
                                                   value="<?php echo $solicitud->id_correo ?>"/>
                                            <label for="check<?php echo $solicitud->id_correo ?>" class=""
                                                   style="  display: block;float: left;"></label>
                                            <?php if ($solicitud->estado == "0") {//PENDIENTE?>
                                                <i class="fa fa-clock-o orange-text"></i>
                                            <?php } elseif ($solicitud->estado == 1) {//atendida  ?>
                                                <i class="fa fa-reply blue-text"></i>
                                            <?php } elseif ($solicitud->estado == 2) {//contratada  ?>
                                                <i class="fa fa-check-square-o green-text"></i>
                                            <?php } elseif ($solicitud->estado == 3) {//descartada  ?>
                                                <i class="fa fa-times-circle-o red-text"></i>
                                            <?php } ?>

                                        </div>
                                        <a href="<?php echo base_url() ?>proveedor/solicitudes/ver/<?php echo $solicitud->id_correo ?>"
                                           class="title dorado-2-text  " style="width: calc( 100% - 78px); display: block;">
                                            <?php echo $solicitud->user->nombre ?><?php echo $solicitud->user->apellido ?>
                                        </a>
                                        <p style="width: calc( 100% - 115px);margin-bottom: 10px;"
                                           class="grey-text darken-3 truncate-on-small ">
                                            <?php echo substr($solicitud->mensaje, 0, 250) ?>...
                                        </p>
                                        <div class="secondary-content black-text " style="font-size: 12px">
                                            <p style="font-size: 11px; font-weight: bold;margin-top: -15px;">
                                                Ultima actualizacion:
                                                <?php echo relativeTimeFormat($solicitud->fecha_creacion) ?>
                                            </p>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <div class="valign-wrapper" style="height: 150px">
                                    <div class="center valign" style="width: 100%">
                                        <i class="fa fa-info fa-3x"></i><br>
                                        No se han econtrado solicitudes en esta carpeta
                                    </div>
                                </div>
                            <?php } ?>
                        </ul>
                        <!-- PAGINATION -->
                        <?php if ($solicitudes->lastPage() > 1) { ?>

                            <ul class="pagination">
                                <li class="waves-effect <?php echo $solicitudes->currentPage() == 1 ? "disabled" : "" ?>">
                                    <a href="<?php echo base_url() ?>proveedor/solicitudes?pagina=<?php echo $solicitudes->currentPage() - 1 ?><?php echo isset($estado) ? "&estado=$estado" : '' ?><?php echo isset($leido) ? "&leido=$leido" : '' ?>">
                                        <i class="material-icons">chevron_left</i>
                                    </a>
                                </li>

                                <?php for ($i = 1; $i <= $solicitudes->lastPage(); $i++) : ?>
                                    <li class="waves-effect <?php echo $page == $i ? "active" : "" ?>">
                                        <a href="<?php echo base_url() ?>proveedor/solicitudes?pagina=<?php echo $i ?><?php echo isset($estado) ? "&estado=$estado" : '' ?><?php echo isset($leido) ? "&leido=$leido" : '' ?>">
                                            <?php echo $i ?>
                                        </a>
                                    </li>
                                <?php endfor; ?>

                                <li class="waves-effect <?php echo $solicitudes->currentPage() == $solicitudes->lastPage() ? "disabled" : "" ?>">
                                    <a href="<?php echo base_url() ?>proveedor/solicitudes?pagina=<?php echo $solicitudes->currentPage() + 1 ?><?php echo isset($estado) ? "&estado=$estado" : '' ?><?php echo isset($leido) ? "leido=$leido" : '' ?>">
                                        <i class="material-icons">chevron_right</i>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="hide" id="template-solicitud">
            <li id="{{ID}}" class=" solicitud collection-item avatar" style="padding-left: 87px !important;">
                <div class="circle" style="border-radius: 0px; height: 100%;width: 60px">
                    <input type="checkbox" data-estado="{{ESTADO}}" data-leida="{{LEIDA}}" id="check{{ID}}" class="check"
                           name="solicitud[]" value="{{ID}}"/>
                    <label for="check{{ID}}" class="" style="  display: block;float: left;"></label>
                    <i class="{{ICON}}"></i>
                    <p style="font-size: 11px; font-weight: bold">
                        {{FECHA}}<br>
                        {{HORA}}
                    </p>
                </div>
                <a href="<?php echo base_url() ?>proveedor/solicitudes/ver/{{ID}}" class="title dorado-2-text  ">{{NOMBRE}}</a>
                <p style="width: calc( 100% - 115px);    margin-bottom: 10px;" class="grey-text darken-3">
                    {{MENSAJE}}...
                </p>
                <div class="secondary-content black-text " style="font-size: 12px">
                    <label>Boda</label><br>
                    <i class="fa fa-calendar-o  dorado-2-text" style="font-size: 12px"></i> {{FECHA_BODA}} <br>
                    <i class="fa fa-users  dorado-2-text" style="font-size: 12px"></i> {{INVITADOS}}
                </div>
            </li>
        </div>
        <?php $this->view("proveedor/footer") ?>
        <script>
            $(document).ready(function() {
                var server = "<?php echo base_url() ?>";
                var t = $(document.body).data('tipo');
                var v = $(document.body).data('valor');
                const leido = $(document.body).data('leido');

                $('.modal-trigger').modal();

                $('.checkAll').on('change', function() {
                    var $o = $($(this).data('checks'));
                    $o.prop('checked', $(this).prop('checked'));
                });

                $('.btn-mover').on('click', function() {
                    var $c = $('#solicitudes').find('.check:checked');
                    var $self = $(this);
                    var data = getSelected();
                    if (data.length > 0) {
                        $.ajax({
                            url: server + 'proveedor/solicitudes/mover',
                            data: {
                                estado: $self.data('estado'),
                                solicitudes: data,
                            },
                            method: 'POST',
                            success: function() {
                                var totales = {
                                    '0': parseInt($('.total-0').html()),
                                    '1': parseInt($('.total-1').html()),
                                    '2': parseInt($('.total-2').html()),
                                    '3': parseInt($('.total-3').html()),
                                };

                                if (t == 'estado') {
                                    $c.each(function(index, value) {
                                        var $this = $(this);
                                        if ($this.data('estado') != $self.data('estado')) {
                                            totales[$this.data('estado')]--;
                                            totales[$self.data('estado')]++;
                                        }
                                        $this.parent().parent().addClass($self.data('color'));
                                        $this.parent().parent().addClass('remove');
                                        $this.remove();
                                    });
                                    getNext(function(resp) {
                                        for (var i in resp.data) {
                                            var s = resp.data[i];
                                            if ($('#' + s.id_correo + '.solicitud')) {
                                            }
                                            else {
                                                var template = template(s);
                                                $('#solicitudes').append(template);
                                            }
                                        }
                                    });
                                }
                                else {
                                    $c.each(function(index, value) {
                                        var $this = $(this);
                                        if ($this.data('estado') != $self.data('estado')) {
                                            totales[$this.data('estado')]--;
                                            totales[$self.data('estado')]++;
                                        }
                                        $this.parent().parent().find('input.check').data('estado', $self.data('estado'));
                                        $this.parent().parent().find('.circle i').attr('class', $self.find('i').attr('class'));
                                    });
                                }
                                for (var i in totales) {
                                    $('.total-' + i).html(totales[i]);
                                }
                                Materialize.toast('Solicitud' + (data.length > 1 ? 'es' : '') + ' movida' + (data.length > 1 ? 's' : ''), 4000);

                                if (t != 'estado') {
                                    $('.total-leido-' + v).html(parseInt($('.total-leido-' + v).html()) - data.length);
                                }

                                location.replace(server + 'proveedor/solicitudes');
                            }, error: function() {
                            },
                        });
                    }
                });

                $('.btn-marcar').on('click', function() {
                    var $c = $('#solicitudes').find('.check:checked');
                    var $self = $(this);
                    var data = getSelected();
                    var totales = 0;

                    $.ajax({
                        url: server + 'proveedor/solicitudes/marcar',
                        data: {
                            leida: $self.data('estado'),
                            solicitudes: data,
                        },
                        method: 'POST',
                        success: function() {

                            if (t != 'estado') {
                                $c.each(function(index, value) {
                                    if ($(value).data('leida') === $self.data('estado')) {
                                    }
                                    else {
                                        totales++;
                                        $(value).parent().parent().addClass($self.data('color'));
                                        $(value).parent().parent().addClass('remove');
                                        $(value).remove();
                                    }
                                });

                                getNext(function(resp) {
                                    for (var i in resp.data) {
                                        var s = resp.data[i];
                                        if ($('#' + s.id_correo + '.solicitud')) {
                                        }
                                        else {
                                            var template = template(s);
                                            $('#solicitudes').append(template);
                                        }
                                    }
                                });
                            }
                            else {
                                $c.each(function(index, value) {

                                    if ($(value).data('leida') != $self.data('estado')) {
                                        totales++;
                                        $(value).data('leida', !$(value).data('leida'));
                                    }
                                });

                                getNext(function(resp) {
                                    for (var i in resp.data) {
                                        var s = resp.data[i];
                                        if ($('#' + s.id_correo + '.solicitud')) {
                                        }
                                        else {
                                            var template = template(s);
                                            $('#solicitudes').append(template);
                                        }
                                    }
                                });
                            }

                            if ($self.data('estado') == '1') {
                                $('.total-leido-1').html(parseInt($('.total-leido-1').html()) + totales);
                                $('.total-leido-0').html(parseInt($('.total-leido-0').html()) - totales);
                            }
                            else {
                                $('.total-leido-0').html(parseInt($('.total-leido-0').html()) + totales);
                                $('.total-leido-1').html(parseInt($('.total-leido-1').html()) - totales);
                            }

                            location.replace(server + 'proveedor/solicitudes');
                        }, error: function() {
                        },
                    });
                });

                function getNext(fn) {
                    var data = {
                        page:<?php echo $page ?>
                    };
                    data["<?php echo $estado === null ? "leido" : "estado" ?>"] = "<?php echo $estado === null ? "$leido" : "$estado" ?>";
                    $.ajax({
                        url: server + 'proveedor/solicitudes/next',
                        method: 'GET', data: data,
                        success: function(resp) {
                            fn(resp);
                        }, erro: function() {
                            fn(null);
                        },

                    });
                }

                $('#search').on('focus click', function() {
                    $('#busqueda-avanzada').addClass('active');
                });

                salir = null;

                $('#busqueda-avanzada').mouseenter(function() {
                    if (salir) {
                        clearTimeout(salir);
                    }
                });

                $('#busqueda-avanzada').on('mouseleave', function() {
                    salir = setTimeout(function() {
                        $('#busqueda-avanzada').removeClass('active');
                    }, 2500);
                });

                $('.datepicker').pickadate({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 5, // Creates a dropdown of 15 years to control year
                    container: $('#form-search').get(0),
                    today: 'Hoy',
                    clear: 'Limpiar',
                    close: 'Aceptar',
                    labelMonthNext: 'Siguiente',
                    labelMonthPrev: 'Atras',
                    hiddenSuffix: '_submit',
                    formatSubmit: 'yyyy-mm-dd',
                    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
                    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Juv', 'Vie', 'Sab'],
                });

                var es = {
                    closeText: 'Cerrar',
                    prevText: 'Anterior',
                    nextText: 'Siguiente',
                    currentText: 'Aujourd\'hui',
                    monthNames: [
                        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: [
                        'Ene.', 'Feb.', 'Mar.', 'Abr.', 'May.', 'Jun.',
                        'Jul.', 'Ago.', 'Sept.', 'Oct.', 'Nov.', 'Dic.'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                    dayNamesShort: ['dom.', 'lun.', 'mar.', 'mier.', 'juev.', 'vien.', 'sab.'],
                    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                    weekHeader: 'Sem.',
                    dateFormat: 'dd-mm-yy',
                    firstDay: 1,
                    maxDate: 'now',
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: '',
                };

                $('#desde').datepicker(es);

                $('#desde').datepicker('option', 'onClose', function(selectedDate) {
                    $('#hasta').datepicker('option', 'minDate', selectedDate);
                });

                $('#hasta').datepicker(es);

                $('#hasta').datepicker('option', 'onClose', function(selectedDate) {
                    $('#desde').datepicker('option', 'maxDate', selectedDate);
                });

                $('#exportar-descargar').on('click', function(evt) {
                    var self = this;
                    $.ajax({
                        url: server + 'proveedor/solicitudes/exportar/' + $('#desde').val() + '/' + $('#hasta').val() + '/solicitudes.xls',
                        method: 'GET',
                        success: function(resp) {
                            saveDataLink.apply(self, [resp, 'solicitudes.xls']);
                        },
                    });
                });

                function saveDataLink(csv, filename) {
                    var csvData = 'data:application/xls;base64,' + btoa(csv);
                    var a = document.createElement('a');
                    $(a).attr({
                        'download': filename,
                        'href': csvData,
                        'target': '_blank',
                    });
                    $(a).on('click', function() {
                    });
                    a.click();
                }
            });

            function getSelected() {
                var $c = $('.check:checked');

                var data = new Array();

                $c.each(function(index, value) {
                    data.push($(value).val());
                });

                return data;
            }

            function plantilla(s) {
                var template = $('#template-solicitud').html();
                template = template.replace(/{{ID}}/g, s.id_correo);
                template = template.replace(/{{ESTADO}}/g, s.estado);
                template = template.replace(/{{LEIDA}}/g, s.leida);
                template = template.replace(/{{NOMBRE}}/g, s.nombre + ' ' + s.apellido);
                template = template.replace(/{{INVITADOS}}/g, s.no_invitado);
                template = template.replace(/{{MENSAJE}}/g, s.mensaje.substring(0, 250));
                template = template.replace(/{{FECHA_BODA}}/g, format(s.fecha_boda));
                template = template.replace(/{{FECHA}}/g, formatMIN(s.fecha_creacion));
                template = template.replace(/{{HORA}}/g, formatHora(s.fecha_creacion));
                if (s.leida == 0) {
                    template = template.replace(/{{ICON}}/g, 'fa fa-envelope orange-text');
                }
                else {
                    switch (s.estado) {
                        case 0:
                            template = template.replace(/{{ICON}}/g, 'fa fa-clock-o orange-text');
                            break;
                        case 1:
                            template = template.replace(/{{ICON}}/g, 'fa fa-reply blue-text');
                            break;
                        case 2:
                            template = template.replace(/{{ICON}}/g, 'fa fa-check-square-o green-text');
                            break;
                        case 3:
                            template = template.replace(/{{ICON}}/g, 'fa fa-times-circle-o green-text');
                            break;
                    }
                }
                return template;
            }

            function format(str) {
                var s = str.split(' ')[0].split('-');
                return s[2] + '-' + s[1] + '-' + s[0];
            }

            function formatMIN(str) {
                var s = str.split(' ')[0].split('-');
                return s[2] + '/' + s[1] + '/' + s[0].substring(2, 4);
            }

            function formatHora(str) {
                var s = str.split(' ')[1].split(':');
                return s[0] + ':' + s[1] + ' ' + (s[0] < 12 ? 'AM' : 'PM');
            }
        </script>
    </body>
</html>