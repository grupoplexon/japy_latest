<div class="col s12 m4 hide-on-small-only ">
    <a href="<?php echo site_url("proveedor/solicitudes?estado=1") ?>">
        <div class="row z-depth-1 z-depth-2-hover">
            <div class="col valign-wrapper blue white-text" style="height: 5rem;width: 45px">
                <div class="valign center" style="width: 100%">
                    <i class="fa fa-reply fa-2x"></i>
                </div>
            </div>
            <div class="col" style="width: calc( 100% - 45px )">
                <p class="" >
                    <strong style="font-size: 2rem " class="blue-text col left-align total-1">
                        <?php echo $totales->atendida ?>
                    </strong>
                    <span class=" col truncate black-text">
                        solicitudes <br>atendidas
                    </span>
                </p>
            </div>
        </div>
    </a>
</div>
<div class="col s12 m4 hide-on-small-only">
    <a href="<?php echo site_url("proveedor/solicitudes?estado=2") ?>">
        <div class="row z-depth-1">
            <div class="col valign-wrapper green white-text" style="height: 5rem;width: 45px">
                <div class="valign center" style="width: 100%">
                    <i class="fa fa-check-square-o fa-2x"></i>
                </div>
            </div>
            <div class="col" style="width: calc( 100% - 45px )">
                <p class="" >
                    <strong style="font-size: 2rem " class="green-text col left-align total-2">
                        <?php echo $totales->contratada ?>
                    </strong>
                    <span class=" col truncate black-text">
                        solicitudes <br>contratadas
                    </span>
                </p>
            </div>
        </div>
    </a>
</div>
<div class="col s12 m4 hide-on-small-only">
    <a href="<?php echo site_url("proveedor/solicitudes?estado=3") ?>">
        <div class="row z-depth-1">
            <div class="col valign-wrapper red white-text" style="height: 5rem;width: 45px">
                <div class="valign center" style="width: 100%">
                    <i class="fa fa-times-circle-o fa-2x"></i>
                </div>
            </div>
            <div class="col" style="width: calc( 100% - 45px )">
                <p class="" >
                    <strong style="font-size: 2rem " class="red-text col left-align total-3">
                        <?php echo $totales->descartada ?>
                    </strong>
                    <span class=" col truncate black-text">
                        solicitudes <br>descartadas
                    </span>
                </p>
            </div>
        </div>
    </a>

</div>