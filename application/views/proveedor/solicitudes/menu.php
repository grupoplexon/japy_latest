<?php
$controller = $_SERVER['REQUEST_URI'];
$controller = strtolower($controller);
if ( ! isset($estado)) {
    $estado = null;
}
if ( ! isset($leido)) {
    $leido = null;
}
?>
<style>

    @media only screen and (min-width: 279px) and (max-width: 426px) {

        #menu-upper .menu-provider-item {
            font-size: 30px !important;
            width: 33.3% !important;
        }

        #menu-lower {
            z-index: 10;
            width: 100%;
            height: 7%;
            box-shadow: 0px 0px 5px 0px #323232 !important;
        }

        #menu-lower .menu-provider-item {
            font-size: 26px !important;
            width: 16.65% !important;
        }

        .menu-provider-item i {
            position: relative;
            cursor: pointer;
        }

        .menu-provider-item .badge {
            position: absolute;
            font-size: 0.3em;
            top: 1px;
            color: #000;
            right: 1%;
        }

        .btn-flat {
            padding: 0 1.3rem !important;
        }

        p.btns {
            text-align: center !important;
        }

        .dorado-2, .red-text {
            width: 100%;
        }

        [type="checkbox"] + label {
            height: 70px;
        }

        .active i {
            color: #1c97b3 !important;
        }

        .modal-content h5 {
            text-align: center;
        }
    }
</style>
<div class="col s2 m4 l3 sidebar  hide-on-small-only">
    <div class="collection with-header">
        <a class="collection-item black-text "
           href="<?php echo base_url() ?>boda-<?php echo $this->session->userdata("slug") ?>">
            <h5>
                <i class="fa fa-home"> </i>
                <text class="hide-on-small-only">Mi buz&oacute;n <br>
                    <small style="font-size: 11px">Ver mi escaparate ></small>
                </text>
            </h5>
        </a>
        <div class="collection-header" style="text-transform: uppercase;font-weight: bold;">
            Solicitudes
        </div>
        <a class="collection-item black-text  "
           href="<?php echo base_url() ?>proveedor/solicitudes">
            <i class="fa fa-sign-in grey-text darken-5"></i>
            <text class="hide-on-small-only">
                Entrada
            </text>
            <span class="badge total-leida"><?php echo $totales->no_leidos + $totales->leidos ?></span>
        </a>

        <a class="collection-item black-text <?php echo $leido == 0 && ! isset($estado) && ! (strpos($controller, "configuracion")) ? 'active' : ''; ?>"
           style="padding-left: 32px !important"
           href="<?php echo base_url() ?>proveedor/solicitudes">
            <!--<i class="fa fa-envelope orange-text darken-5"></i>-->
            <i class="fa fa-envelope orange-text darken-5 "></i>
            <text class="hide-on-small-only">No leidos</text>
            <span class="badge total-leido-0"><?php echo $totales->no_leidos ?></span>
        </a>

        <a class="collection-item black-text <?php echo $leido == 1 && ! isset($estado) ? 'active' : ''; ?> "
           style="padding-left: 32px !important"
           href="<?php echo base_url() ?>proveedor/solicitudes?leido=1">
            <i class="fa fa-envelope grey-text darken-5"></i>
            <text class="hide-on-small-only">Leidos</text>
            <span class="badge total-leido-1"><?php echo $totales->leidos ?></span>
        </a>

        <a class="collection-item black-text <?php echo ($estado === 0) ? 'active' : ''.$estado; ?>  "
           href="<?php echo base_url() ?>proveedor/solicitudes?estado=0">
            <i class="fa fa-clock-o orange-text darken-5"></i>
            <text class="hide-on-small-only">Pendientes</text>
            <span class="badge  total-0"><?php echo $totales->pendiente ?></span>
        </a>
        <a class="collection-item black-text <?php echo ($estado == 1) ? 'active' : ''; ?> "
           href="<?php echo base_url() ?>proveedor/solicitudes?estado=1">
            <i class="fa fa-reply blue-text darken-5"></i>
            <text class="hide-on-small-only">Atendidos</text>
            <span class="badge total-1"><?php echo $totales->atendida ?></span>
        </a>
        <a class="collection-item black-text <?php echo ($estado == 2) ? 'active' : ''; ?>"
           href="<?php echo base_url() ?>proveedor/solicitudes?estado=2">
            <i class="fa fa-check-square-o green-text darken-5"></i>
            <text class="hide-on-small-only">Contratadas</text>
            <span class="badge total-2"><?php echo $totales->contratada ?></span>
        </a>
        <a class="collection-item black-text  <?php echo ($estado == 3) ? 'active' : ''; ?>"
           href="<?php echo base_url() ?>proveedor/solicitudes?estado=3">
            <i class="fa fa-times-circle-o red-text"></i>
            <text class="hide-on-small-only">Descartadas</text>
            <span class="badge total-3"><?php echo $totales->descartada ?></span>
        </a>
    </div>
</div>

<div id="menu-upper" class="row menu-provider-esc hide">
    <a class="menu-provider-item black-text " style="display: none">
        <i class="fa fa-ticket grey-text darken-5 "></i>
        <text class="hide-on-small-only">Boletos</text>
        <span class="badge">1</span>
    </a>
    <a class="menu-provider-item black-text  <?php echo (strpos($controller, "configuracion")) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>proveedor/solicitudes/configuracion">
        <i class="fa fa-gears grey-text "> </i>
    </a>

    <a class="menu-provider-item black-text <?php echo (strpos($controller, "plantillas")) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>proveedor/solicitudes/plantillas">
        <i class="fa fa-file-archive-o grey-text  "> </i>
    </a>
    <a class="menu-provider-item black-text waves-effect modal-trigger " href="#modal-exportar">
        <i class="fa fa-download grey-text  "> </i>
    </a>
</div>
<div id="menu-lower" class="row menu-provider-esc hide-on-med-and-up">

    <a class="menu-provider-item black-text <?php echo $leido == 0 && ! isset($estado) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>proveedor/solicitudes">
        <!--<i class="fa fa-envelope orange-text darken-5"></i>-->
        <i class="fa fa-envelope orange-text darken-5 "></i>
        <?php if ($totales->no_leidos > 99) { ?>
            <span class="badge-news total-leido-0" style="font-size: 8px"><?php echo "99+" ?></span>
        <?php } else { ?>
            <span class="badge-news total-leido-0"><?php echo $totales->no_leidos ?></span>
        <?php } ?>
    </a>

    <a class="menu-provider-item black-text <?php echo $leido == 1 && ! isset($estado) ? 'active' : ''; ?> "
       href="<?php echo base_url() ?>proveedor/solicitudes?leido=1">
        <i class="fa fa-envelope grey-text "></i>
        <?php if ($totales->leidos > 99) { ?>
            <span class="badge-news total-leido-1" style="font-size: 8px"><?php echo "99+" ?></span>
        <?php } else { ?>
            <span class="badge-news total-leido-1"><?php echo $totales->leidos ?></span>
        <?php } ?>
    </a>

    <a class="menu-provider-item black-text <?php echo ($estado === 0) ? 'active' : ''.$estado; ?>  "
       href="<?php echo base_url() ?>proveedor/solicitudes?estado=0">
        <i class="fa fa-clock-o orange-text darken-5"></i>
        <?php if ($totales->pendiente > 99) { ?>
            <span class="badge-news total-0" style="font-size: 8px"><?php echo "99+" ?></span>
        <?php } else { ?>
            <span class="badge-news  total-0"><?php echo $totales->pendiente ?></span>
        <?php } ?>
    </a>
    <a class="menu-provider-item black-text <?php echo ($estado == 1) ? 'active' : ''; ?> "
       href="<?php echo base_url() ?>proveedor/solicitudes?estado=1">
        <i class="fa fa-reply blue-text darken-5"></i>
        <?php if ($totales->atendida > 99) { ?>
            <span class="badge-news" style="font-size: 8px"><?php echo "99+" ?></span>
        <?php } else { ?>
            <span class="badge-news total-1"><?php echo $totales->atendida ?></span>
        <?php } ?>
    </a>
    <a class="menu-provider-item black-text <?php echo ($estado == 2) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>proveedor/solicitudes?estado=2">
        <i class="fa fa-check-square-o green-text darken-5"></i>
        <?php if ($totales->contratada > 99) { ?>
            <span class="badge-news" style="font-size: 8px"><?php echo "99+" ?></span>
        <?php } else { ?>
            <span class="badge-news total-2"><?php echo $totales->contratada ?></span>
        <?php } ?>
    </a>
    <a class="menu-provider-item black-text  <?php echo ($estado == 3) ? 'active' : ''; ?>"
       href="<?php echo base_url() ?>proveedor/solicitudes?estado=3">
        <i class="fa fa-times-circle-o red-text"></i>
        <?php if ($totales->descartada > 99) { ?>
            <span class="badge-news" style="font-size: 8px"><?php echo "99+" ?></span>
        <?php } else { ?>
            <span class="badge-news total-3"><?php echo $totales->descartada ?></span>
        <?php } ?>
    </a>
</div>

<div id="modal-exportar" class="modal">
    <div class="modal-content">
        <h5>Exportar Solicitudes</h5>
        <div class="divider"></div>
        <p>Seleccione las fechas que desea importar</p>
        <div class="row">
            <label>Desde :</label>
            <div class="input-group">
                <input id="desde" class="form-control " style="text-align: left">
                <span><i class="fa fa-calendar"></i></span>
            </div>
        </div>
        <div class="row">
            <label>Hasta :</label>
            <div class="input-group">
                <input id="hasta" class="form-control " style="text-align: left">
                <span><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" id="exportar-descargar" class=" modal-action modal-close waves-effect waves-green btn dorado-2"><i
                    class="fa fa-download left hide-on-small-only"></i> Exportar</a>
        <a href="#!" class=" modal-action modal-close waves-effect btn-flat red-text center-align ">Cancelar</a>
    </div>
</div>
