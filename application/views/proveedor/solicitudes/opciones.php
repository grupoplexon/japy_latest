<html>
<head>
    <title>Solicitudes Configuraci&oacute;n</title>
    <?php $this->view("proveedor/header") ?>
</head>
<body>
<?php $this->view("proveedor/menu") ?>
<div class="body-container">
    <?php $this->view("proveedor/mensajes") ?>
    <div class="row">
        <?php $this->view("proveedor/solicitudes/menu") ?>
        <div class="col s12 m8 l9">
            <form class="form-validate" method="POST"
                  action="<?php echo base_url() ?>index.php/proveedor/solicitudes/configuracion">

                <h5><?php echo $titulo ?></h5>
                <div class="divider"></div>
                <br>
                <div class="card-panel">
                    <h5>
                        Correo principal
                    </h5>
                    <div class="divider"></div>
                    <br>
                    <label>Correo principal</label>
                    <input name="correo_principal" value="<?php echo $contacto != false ? $contacto->correo : '' ?>"
                           class="form-control validate[required,custom[email]]">
                </div>
                <br>
            </form>
        </div>
    </div>
</div>
<div class="hide" id="template-nuevo">
    <div class="col s12">
        <label>Correo:</label>
    </div>
    <div class="col s10">
        <input class="form-control validate[custom[email]]" name="correo[]">
    </div>
    <div class="col s2">
        <button type="button" class="btn-flat red-text waves-effect white tooltipped eliminar-correo"
                data-position="top" data-delay="50" data-tooltip="Eliminar">
            <i class="fa fa-trash-o"></i>
        </button>
    </div>

</div>


<?php $this->view("proveedor/footer") ?>

<script>
    $(document).ready(function () {
        $(".form-validate").validationEngine();
        $("#agregar").on("click", agregar);
        $(".eliminar-correo").on("click", eliminar);
        $('.modal-trigger').modal();

        var es = {
            closeText: 'Cerrar',
            prevText: 'Anterior',
            nextText: 'Siguiente',
            currentText: 'Aujourd\'hui',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene.', 'Feb.', 'Mar.', 'Abr.', 'May.', 'Jun.',
                'Jul.', 'Ago.', 'Sept.', 'Oct.', 'Nov.', 'Dic.'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesShort: ['dom.', 'lun.', 'mar.', 'mier.', 'juev.', 'vien.', 'sab.'],
            dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            weekHeader: 'Sem.',
            dateFormat: 'dd-mm-yy',
            firstDay: 1,
            maxDate: "now",
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        }
        $("#desde").datepicker(es);
        $("#desde").datepicker("option", "onClose", function (selectedDate) {
            $("#hasta").datepicker("option", "minDate", selectedDate);
        });
        $("#hasta").datepicker(es);
        $("#hasta").datepicker("option", "onClose", function (selectedDate) {
            $("#desde").datepicker("option", "maxDate", selectedDate);
        });
        $("#exportar-descargar").on("click", function (evt) {
            var self = this;
            $.ajax({
                url: server + "index.php/proveedor/solicitudes/exportar/" + $("#desde").val() + "/" + $("#hasta").val() + "/solicitudes.xls",
                method: "GET",
                success: function (resp) {
                    saveDataLink.apply(self, [resp, "solicitudes.xls"]);
                }
            })
        });

        function saveDataLink(csv, filename) {
            var csvData = 'data:application/xls;base64,' + btoa(csv);
            var a = document.createElement("a");
            $(a).attr({
                'download': filename,
                'href': csvData,
                'target': '_blank'
            });
            $(a).on("click", function () {
            })
            a.click();
        }


    });

    function agregar() {
        var template = $("#template-nuevo").html();
        var div = document.createElement("div");
        var $div = $(div);
        $div.html(template);
        $div.addClass("row");
        $div.find(".eliminar-correo").on("click", eliminar);
        $div.find('.tooltipped').tooltip({delay: 50});
        $("#correos").append($div);
    }

    function eliminar() {
        $(this).parent().parent().find('.tooltipped').tooltip('remove');
        $(this).parent().parent().remove();
    }


</script>
</body>

</html>