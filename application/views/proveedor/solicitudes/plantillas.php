<html>
    <head>
        <title>Solicitudes Plantillas</title>
        <?php $this->view("proveedor/header") ?>
        <style>
            .collapsible-body p {
                margin: 3px 30px 0px 30px;
                padding: 0px;
            }

            .dz-preview .dz-image img {
                width: 100px;
                height: 100px;
                background-size: contain;
                background-repeat: no-repeat;
            }

            .template {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .clickable-li {
                cursor: pointer;
                pointer-events: auto;
            }
        </style>
    </head>
    <body>
        <?php $this->view("proveedor/menu") ?>
        <div class="body-container">
            <?php $this->view("proveedor/mensajes") ?>
            <div class="row">
                <?php $this->view("proveedor/solicitudes/menu") ?>
                <div class="col s12 m8 l9">
                    <h5>Plantillas</h5>
                    <div class="divider"></div>
                    <br>
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php foreach ($plantillas as $key => $plantilla) { ?>
                            <li id="<?php echo $plantilla->id_respuesta ?>" class="">

                                <div class="collapsible-header template ">
                                    <div class="">
                                        <i class="material-icons"><?php echo (count($plantilla->archivos)) ? "attachment" : "note" ?></i>
                                        <?php echo $plantilla->nombre_plantilla ?>
                                    </div>
                                    <div class="trash" style="margin: .5rem 0;cursor: pointer">
                                        <a class="delete-template" id="<?php echo $plantilla->id_respuesta ?>">
                                            <i class="material-icons primary-text">delete</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapsible-body">
                                    <form method="POST"
                                          action="<?php echo base_url() ?>proveedor/solicitudes/plantillas/<?php echo $plantilla->id_respuesta ?>">

                                        <div class="">
                                            <div class="row">
                                                <input name="plantilla" value="<?php echo $plantilla->id_respuesta ?>"
                                                       type="hidden">
                                                <label>Nombre de la plantilla</label>
                                                <input class="form-control " name="nombre_plantilla"
                                                       value="<?php echo $plantilla->nombre_plantilla ?>">
                                                <label>Asunto:</label>
                                                <input id="asunto" class="form-control" name="asunto"
                                                       value="<?php echo $plantilla->asunto ?>">
                                                <div class="col s12">
                                                    <label>Mensaje:</label>
                                                    <textarea id="mensaje" class="editable"
                                                              name="mensaje"> <?php echo $plantilla->mensaje ?></textarea>
                                                </div>
                                                <div class="col s12">
                                                    <div id="nuevo">
                                                        <div class="card-panel z-depth-0 valign-wrapper upload" id="upload" style="display: flex;justify-content: space-around;">
                                                            <text style="width: 100%;text-align: center;color: gray;"><i
                                                                        class="fa fa-upload fa-2x valign "></i><br>
                                                                Suelte el archivo o haga clic aqu&iacute; para cargar.
                                                            </text>
                                                        </div>
                                                        <input name="id_archivo" type="hidden"
                                                               value="<?php echo count($plantilla->archivos) ? $plantilla->archivos[0]->id_archivo : "" ?>">
                                                        <input name="file" type="hidden" class="archivo"
                                                               value="<?php echo count($plantilla->archivos) ? base_url()."proveedores/archivo_plantilla/".$plantilla->archivos[0]->id_archivo : "" ?>">
                                                        <input data-plantilla="<?php echo $plantilla->id_respuesta ?>"
                                                               name="file_name"
                                                               type="hidden"
                                                               class="archivo-name"
                                                               value="<?php echo (count($plantilla->archivos)) ? $plantilla->archivos[0]->nombre : "" ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <p>
                                                    <button type="submit" class="btn dorado-2 right">Guardar</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

        <?php $this->view("proveedor/footer") ?>

        <script>
            var server = "<?php echo base_url() ?>";

            $(document).ready(function() {
                init_tinymce_mini('.editable');

                $('.modal-trigger').modal();

                setupDropzone();
                let fileArrayUpload = 0;

                $('.upload').each(function(i, elem) {
                    var dropzone = new Dropzone(elem, {
                        url: "<?php echo base_url() ?>proveedor/escaparate/uploadFilesTemplate",
                        paramName: 'files',
                        maxFilesize: 3,
                        uploadMultiple: true,
                        maxFiles: 5,
                        createImageThumbnails: true,
                        parallelUploads: 5,
                        acceptedFiles: 'image/*,application/pdf',
                        init: function() {
                            this.on('success', function(file, responseText) {
                                responseText = JSON.parse(responseText);
                                file.fileId = responseText.data.fileIds[fileArrayUpload];
                                file.templateId = responseText.data.templateId;
                                fileArrayUpload++;
                                fileArrayUpload = fileArrayUpload == responseText.data.fileIds.length ? 0 : fileArrayUpload;

                            });
                            this.on('addedfile', function(file) {
                                let removeButton = Dropzone.createElement('<button class=\'btn waves-effect eliminar-foto\' style=\'margin-top:5px;padding:0 1rem;\'>Eliminar</button>');
                                let _this = this;
                                let thumbnail = $(file.previewElement).find('.dz-image img');

                                $('.dz-filename').addClass('truncate');

                                removeButton.addEventListener('click', function(e) {
                                    e.preventDefault();
                                    e.stopPropagation();

                                    _this.removeFile(file);
                                });

                                file.previewElement.appendChild(removeButton);
                            });
                            this.on('removedfile', function(file) {
                                $.ajax({
                                    url: server + 'proveedor/escaparate/deleteImagesTemplate',
                                    data: {
                                        'fileId': file.fileId,
                                        'templateId': file.templateId,
                                    },
                                    dataType: 'json',
                                    type: 'post',
                                    success: function(resp) {
                                    }, error: function(resp) {

                                    },
                                });

                                let fileQuantity = $('.upload').find('div.dz-preview').length;

                                if (fileQuantity == 0) {
                                    $('.upload').find('text').show();
                                }
                                else {
                                    $('.upload').find('text').hide();
                                }

                            });
                            this.on('sending', function(file, xhr, formData) {
                                const templateId = $('.template.active').parent().attr('id');

                                formData.append('templateId', templateId);
                            });
                        },
                        accept: function(file, done) {
                            $('.upload').parent().find('label.red-text').remove();
                            $('.upload').find('text').hide();

                            if (file.type == 'application/pdf') {
                                let thumbnail = $('.dz-preview .dz-image img');
                                thumbnail.attr('src', server + 'dist/img/pdf-icon.png');
                            }

                            done();
                        },
                        error: function(data, errorMessage) {
                            $('#nuevo .nombre').html('');
                            $('.upload').parent().find('label.red-text').remove();
                            $('.upload').parent().append(`<label class='red-text'>${errorMessage}</label>`);

                            $(elem).find('div.dz-preview').each(function(i, value) {
                                value.remove();
                            });
                        },
                        acceptedFiles: 'image/*,application/pdf',
                    });
                });

                $('.trash').on('click', function(e) {
                    e.stopPropagation();
                });

                $('.eliminar-foto').on('click', function(elm) {
                    let $btn = $(elm.currentTarget);
                    let $prom = $('#' + $btn.data('plantilla'));
                    let id = $(this).data('plantilla');
                    $prom.find('.upload div.dz-preview').remove();
                    $prom.find('.archivo').val('');
                    $('#nuevo .nombre').html('');

                    $.ajax({
                        url: server + 'proveedor/escaparate/deleteImagesTemplate',
                        data: {
                            'template-id': id,
                        },
                        dataType: 'json',
                        type: 'post',
                        success: function(resp) {
                        }, error: function(resp) {
                        },
                    });
                });

                $('.upload text').on('click', function(e) {
                    $(e.currentTarget.parentNode).trigger('click');
                });

                var es = {
                    closeText: 'Cerrar',
                    prevText: 'Anterior',
                    nextText: 'Siguiente',
                    currentText: 'Aujourd\'hui',
                    monthNames: [
                        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: [
                        'Ene.', 'Feb.', 'Mar.', 'Abr.', 'May.', 'Jun.',
                        'Jul.', 'Ago.', 'Sept.', 'Oct.', 'Nov.', 'Dic.'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                    dayNamesShort: ['dom.', 'lun.', 'mar.', 'mier.', 'juev.', 'vien.', 'sab.'],
                    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                    weekHeader: 'Sem.',
                    dateFormat: 'dd-mm-yy',
                    firstDay: 1,
                    maxDate: 'now',
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: '',
                };

                $('#desde').datepicker(es);

                $('#desde').datepicker('option', 'onClose', function(selectedDate) {
                    $('#hasta').datepicker('option', 'minDate', selectedDate);
                });

                $('#hasta').datepicker(es);

                $('#hasta').datepicker('option', 'onClose', function(selectedDate) {
                    $('#desde').datepicker('option', 'maxDate', selectedDate);
                });

                $('#exportar-descargar').on('click', function(evt) {
                    var self = this;
                    $.ajax({
                        url: server + 'index.php/proveedor/solicitudes/exportar/' + $('#desde').val() + '/' + $('#hasta').val() + '/solicitudes.xls',
                        method: 'GET',
                        success: function(resp) {
                            saveDataLink.apply(self, [resp, 'solicitudes.xls']);
                        },
                    });
                });

                $('.delete-template').on('click', function() {
                    let id = $(this).attr('id');
                    deleteTemplate(id);
                });

                $('.collapsible-header.template').on('click', function() {
                    const self = this;
                    let dropzone = Dropzone.forElement('.upload');
                    if (!$(self).hasClass('active')) {
                        $.ajax({
                            url: server + 'proveedor/solicitudes/plantilla/' + $(self).parent().attr('id'),
                            method: 'GET',
                            success: function(resp) {
                                if (resp.data.archivos.length) {

                                    $('.upload').find('div.dz-preview').each(function(i, value) {
                                        value.remove();
                                    });

                                    $('.upload').find('text').hide();

                                    resp.data.archivos.forEach(function(archivo) {
                                        let file;

                                        if (archivo.mime == 'application/pdf') {
                                            file = server + 'dist/img/pdf-icon.png';
                                        }
                                        else {
                                            file = server + 'proveedores/archivo_plantilla/' + archivo.id_archivo;
                                        }

                                        let mockFile = {name: archivo.nombre, fileId: archivo.id_archivo, templateId: $(self).parent().attr('id')};

                                        dropzone.emit('addedfile', mockFile);
                                        dropzone.emit('thumbnail', mockFile, file);
                                        dropzone.emit('complete', mockFile);
                                        dropzone.files.push(mockFile);

                                    });
                                }
                                else {
                                    dropzone.removeFile(dropzone.files[0]);
                                    $('.upload').find('text').show();
                                }
                            },
                            error: function() {

                            },
                        });
                    }
                });

            });

            function deleteTemplate(id) {
                $.ajax({
                    url: server + 'proveedor/solicitudes/disableTemplate',
                    data: {
                        idTemplate: id,
                    },
                    dataType: 'json',
                    type: 'post',
                    success: function(resp) {
                        $('#' + id).remove();
                    }, error: function(resp) {

                    },
                });
            }

            function saveDataLink(csv, filename) {
                var csvData = 'data:application/xls;base64,' + btoa(csv);
                var a = document.createElement('a');
                $(a).attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank',
                });
                $(a).on('click', function() {
                });
                a.click();
            }

            function setupDropzone() {
                Dropzone.prototype.defaultOptions.dictDefaultMessage = 'Tira tus archivos aqui';
                Dropzone.prototype.defaultOptions.dictFallbackMessage = 'Lo sentimos parece ser que tu navegador no es compatible';
                Dropzone.prototype.defaultOptions.dictFileTooBig = 'El archivo es muy grande ({{filesize}}MiB). Tamaño maximo: {{maxFilesize}}MiB.';
                Dropzone.prototype.defaultOptions.dictInvalidFileType = 'No se pueden subir archivos de este tipo';
                Dropzone.prototype.defaultOptions.dictResponseError = 'El servidor respondio con este codigo {{statusCode}}.';
                Dropzone.prototype.defaultOptions.dictCancelUpload = 'Cancelar subida';
                Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = 'Estas seguro que deseas cancelar?';
                Dropzone.prototype.defaultOptions.dictRemoveFile = 'Remover archivo';
                Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = 'Ya no puedes subir mas archivos.';
            }


        </script>
    </body>

</html>