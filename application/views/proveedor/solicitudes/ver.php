<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
        <title><?php echo $titulo ?> : <?php echo isset($page) ? "pagina ".$page : "" ?></title>
        <?php $this->view("proveedor/header") ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/solicitudes.css">
        <style>
            .nombre {
                margin: 0;
            }

            .upload {
                overflow: auto;
            }

            .nombre-elem {
                display: flex;
                flex-wrap: nowrap;
                align-items: center;
            }

            .nombre-elem i {
                font-size: 12px;
            }
        </style>

        <link rel="stylesheet" href="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/src/parsley.css">
    </head>
    <body data-tipo='<?php echo $estado === null ? "leido" : "estado" ?>'
          data-valor='<?php echo $estado === null ? "$leido" : "$estado" ?>'
          data-leida="<?php echo $solicitud->leida_proveedor ?>">
        <?php $this->view("proveedor/menu") ?>
        <div class="body-container">
            <?php $this->view("proveedor/mensajes") ?>
            <div class="row">
                <?php $this->view("proveedor/solicitudes/menu") ?>
                <div class="col s12 m8 l9">
                    <h5><?php echo $titulo ?></h5>
                    <div class="divider"></div>
                    <br>
                    <div id="informacion" class="card-panel z-depth-0 green  white-text hide">
                        <i class="fa fa-check"></i>
                        <text> Datos actualizados correctamente</text>
                    </div>
                    <div class="row" style="font-size: 11px">
                        <div class="col  btn-block-on-small ">
                            <a id="btn-mover" class='dropdown-button btn-flat border btn-block-on-small'
                               data-constrainwidth="false" data-beloworigin="true" href='#' data-activates='drop-mover-a'>
                                <?php if ($estado == "0") {//PENDIENTE    ?>
                                    <i class="fa fa-clock-o orange-text" style="font-size: 11px"></i>
                                <?php } else ?>
                                <?php if ($estado == 1) {//atendida     ?>
                                    <i class="fa fa-reply blue-text" style="font-size: 11px"></i>
                                <?php } else ?>
                                <?php if ($estado == 2) {//contratada     ?>
                                    <i class="fa fa-check-square-o green-text" style="font-size: 11px"></i>
                                <?php } else ?>
                                <?php if ($estado == 3) {//descartada     ?>
                                    <i class="fa fa-times-circle-o red-text " style="font-size: 11px"></i>
                                <?php } ?>
                                <text><?php echo Solicitudes::labelEstado($estado) ?></text>
                            </a>
                            <ul id='drop-mover-a' class='dropdown-content' style="width: 170px">
                                <li><a href="#!" data-color="orange" data-estado="0" class="black-text btn-mover"><i
                                                class="fa fa-clock-o orange-text"></i> Pendientes</a></li>
                                <li><a href="#!" data-color="blue" data-estado="1" class="black-text btn-mover"><i
                                                class="fa fa-reply   blue-text"></i> Atendida</a></li>
                                <li><a href="#!" data-color="green" data-estado="2" class="black-text btn-mover"><i
                                                class="fa fa-check-square-o green-text"></i> Contratada</a></li>
                                <li><a href="#!" data-color="red" data-estado="3" class="black-text btn-mover"><i
                                                class="fa fa-clock-o red-text"></i> Descartada</a></li>
                            </ul>
                        </div>
                        <div class="col btn-block-on-small ">
                            <a id="btn-marcar" class='dropdown-button btn-flat border btn-block-on-small'
                               data-beloworigin="true" href='#' data-activates='drop-movers-a'>
                                <i class="fa fa-chevron-down right dorado-2-text" style="font-size: 11px"></i>
                                Marcar como
                            </a>
                            <ul id='drop-movers-a' class='dropdown-content'>
                                <li><a href="#!" data-estado='0' class="black-text btn-marcar">No le&iacute;da</a></li>
                                <li><a href="#!" data-estado='1' class="black-text btn-marcar">Leida</a></li>
                            </ul>
                        </div>
                        <div class="col s12 right">
                            <div class="row">
                                <a class="col s6 btn-flat border waves-effect modal-trigger  " href="#modal-nota"
                                   data-target="#modal-nota"><i class="fa fa-edit" style="font-size: 11px;"></i>
                                    <text class="">Añadir Nota</text>
                                </a>
                            </div>
                            <button class="btn dorado-2 goto waves-effect " data-target="#responder"> Responder</button>
                        </div>
                    </div>
                    <div class="row imprimible">
                        <div class="card-panel">
                            <div class="row">
                                <div class="col s12 m2 center-align">
                                    <img class="responsive-img"
                                         src="<?php echo base_url() ?>perfil/foto/<?php echo $novio->id_usuario ?>">
                                </div>

                                <div class="col s12 m10 ">
                                    <div class="row">
                                        <div class="col s12">
                                            <h6 class="dorado-2-text">
                                                <b><?php echo $novio->nombre ?><?php echo $novio->apellido ?></b>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 m6">
                                            <label><i class="fa fa-phone"></i> Tel&eacute;fono</label> <?php echo $cliente->telefono ?> <br>
                                            <label><i class="fa fa-envelope"></i> Email</label> <?php echo $novio->correo ?><br>
                                        </div>
                                        <div class="col s12 m6">
                                            <label>
                                                <i class="fa fa-calendar-o"></i> Fecha del evento
                                            </label>
                                            <?php echo dateFormat($boda->fecha_boda, "%d de %B del %Y") ?>
                                            <br>
                                            <label><i class="fa fa-users"></i> Invitados</label>
                                            <?php echo $boda->no_invitado ?>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-justify">
                                <label>Mensaje recibido: <?php echo relativeTimeFormat($solicitud->fecha_creacion) ?></label><br>
                                <?php echo $solicitud->mensaje ?>
                            </div>

                            <div class="row">
                                <label class="blue-text"> Consiga m&aacute;s clientes: Responda lo antes posible. Las
                                    primeras 24h son 'claves'. </label>
                            </div>
                        </div>
                        <section id="solicitud-nota" class="<?php echo($solicitud->nota ? "" : "hide") ?>">
                            <h5>Nota</h5>
                            <div class="divider" style="margin-bottom:  20px;"></div>
                            <div class="card-panel orange lighten-3 ">
                                <?php echo $solicitud->nota ?>
                            </div>
                        </section>
                        <?php if ($mensajes) { ?>
                            <section id="conversacion">
                                <h5 style="margin-top: 50px;">Mensajes</h5>
                                <div class="divider" style="margin-bottom:  20px;"></div>
                                <?php foreach ($mensajes as $key => $msj) { ?>
                                    <div class="card-panel">
                                        <?php if ($msj->from == $this->session->id_usuario) { ?>
                                            <i class="fa fa-mail-reply blue-text"></i><b class="dorado-2-text">Yo</b>,
                                        <?php } else { ?>
                                            <i class="fa fa-mail-forward green-text"></i><b
                                                    class="dorado-2-text"><?php echo($novio->nombre." ".$novio->apellido) ?></b>,
                                        <?php } ?>
                                        <label>Enviado: <?php echo relativeTimeFormat($msj->fecha_creacion) ?></label>
                                        <div class="divider"></div>
                                        <div>
                                            <h6 style="font-weight: bold"><?php echo $msj->asunto ?></h6>
                                            <?php echo $msj->mensaje ?>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <?php foreach ($msj->archivos as $key => $archivo) { ?>
                                                <a href="<?php echo base_url() ?>proveedores/archivo/<?php echo $archivo->id_adjunto ?>" class="col s12 btn-flat dorado-2-text" style="overflow:hidden;" download>
                                                    <i class="fa fa-download"></i> <?php echo $archivo->nombre ?>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </section>
                        <?php } ?>
                    </div>
                    <div id="responder" class="responder">
                        <form id="answer-email"
                              enctype="multipart/form-data"
                              method="POST"
                              action="<?php echo base_url() ?>proveedor/solicitudes/ver/<?php echo $solicitud->id_correo ?>">
                            <div class="row">
                                <div class="col s12 12 m8">
                                    <h5 style="margin-top: 35px;">Responder</h5>
                                </div>
                                <div class=" col s12 m4 right">
                                    <select id="select-plantilla" class="form-control browser-default"
                                            style="top: 30px;position: relative;">
                                        <option value="nuevo" selected>No usar plantilla</option>
                                        <?php foreach ($plantillas as $key => $p) { ?>
                                            <option value="<?php echo $p->id_respuesta; ?>"><?php echo $p->nombre_plantilla; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="card-panel">
                                <div class="row">
                                    <p>
                                        <label>
                                            Asunto:
                                        </label>
                                        <input id="asunto" class="form-control" name="asunto"
                                               value="Te enviamos la informaci&oacute;n que solicitaste" data-parsley-length="[2,70]" data-parsley-required="">
                                    </p>
                                    <div class="col s12">
                                        <p>
                                            <label>
                                                Mensaje:
                                            </label>
                                            <textarea id="mensaje" name="mensaje" data-type="textarea" data-parsley-required=""></textarea>
                                        </p>
                                    </div>
                                    <div class="col s12">
                                        <div id="nuevo">
                                            <div class="card-panel z-depth-0 valign-wrapper upload " id="upload" enctype="multipart/form-data" style="display:flex;justify-content:space-around;overflow-x:auto;">
                                                <text style="width: 100%;text-align: center;color: gray;">
                                                    <i class="fa fa-upload fa-2x valign "></i><br>
                                                    Suelte el archivo o haga clic aqu&iacute; para cargar.
                                                </text>
                                            </div>
                                            <input name="files[]" type="hidden" class="archivo" value="">
                                            <input name="files_names[]" type="hidden" class="archivo-name" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <p id="check" class="">
                                        <input type="checkbox" id="guardar-mensaje" name="guardar"/>
                                        <label for="guardar-mensaje">Guardar este mensaje como una plantilla para utilizar
                                            despu&eacute;s en mensajes similares.</label>
                                    </p>
                                    <p class="hide" id="nombre_plantilla">
                                        <label>
                                            Nombre de la plantilla
                                        </label>
                                        <input class="form-control " name="nombre_plantilla">
                                    </p>
                                    <p class="right">
                                        <button id="update-template" class="btn dorado-2 hidden">Actualizar Plantilla</button>
                                        <button id="send-email" type="submit" class="btn dorado-2">Enviar</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-nota" class="modal ">
            <div class="modal-content">
                <h4>Nota</h4>
                <div class="divider"></div>
                <br>
                <textarea name="nota" class="form-control nota"
                          style="height: 250px"><?php echo $solicitud->nota ?></textarea>
                <p class="grey-text">Solo ser&aacute; visible para la empresa en la seccion de mensajes</p>
            </div>
            <div class="modal-footer">
                <a class="modal-action modal-close waves-effect waves-green btn-flat guardar ">Guardar</a>
                <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancelar</a>
            </div>
        </div>

        <div id="modal-reenviar" class="modal ">
            <form method="POST"
                  action="<?php echo base_url() ?>proveedor/solicitudes/reenviar/<?php echo $solicitud->id_correo ?>">
                <div class="modal-content">
                    <h4>Reenviar solicitud</h4>
                    <div class="divider"></div>
                    <br>
                    <?php
                    ?>
                    <textarea name="correos" class="form-control nota" style="height: 250px"><?php echo $contacto->correo ?>
                        ,<?php echo $proveedor->contacto_correos ?></textarea>
                    <p class="grey-text">Introduce los e-mails a los que quieres reenviar esta solicitud, separados por una
                        coma</p>
                </div>
                <div class="modal-footer">
                    <button class="modal-action modal-close waves-effect waves-green btn-flat guardar " type="submit">
                        Reenviar
                    </button>
                    <a class="modal-action modal-close waves-effect waves-red btn-flat ">Cancelar</a>
                </div>
            </form>
        </div>

        <input type="hidden" id="mail-id" value="<?php echo $solicitud->id_correo ?>">
        <?php $this->view("proveedor/footer") ?>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/parsley.js"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/parsley/dist/i18n/es.js"></script>

        <script>
            var server = "<?php echo base_url() ?>";

            $(document).ready(function() {
                init_tinymce_mini();

                var v = $(document.body).data('valor');
                var t = $(document.body).data('tipo');
                let leida = $(document.body).data('leida');
                let formData = new FormData();
                const mailId = $('#mail-id').val();
                const $mailForm = $('#answer-email').parsley();
                const $sendEmailBtn = $('#send-email');
                const $updateTemplateBtn = $('#update-template');
                let isTemplate = false;
                let isBeingSent = false;
                let templateId = null;

                $('.modal').modal();

                $('select').material_select();
                setupDropzone();

                tinymce.init({
                    selector: '#mensaje',
                    theme: 'modern',
                    menubar: 'false',
                    relative_urls: false,
                    plugins: [
                        'autolink link directionality',
                    ],
                    toolbar: 'undo redo | bold | italic | link |  alignleft aligncenter alignright alignjustify ',
                    imagetools_cors_hosts: ['localhost', '172.17.0.22', '127.0.0.1', '*', '172.17.0.58'],
                    setup: function(ed) {
                        ed.on('KeyUp', function(e) {
                            if (isTemplate) {
                                $updateTemplateBtn.removeClass('hidden');
                            }
                        });
                        ed.on('change', function(e) {
                            if (isTemplate && !isBeingSent) {
                                $updateTemplateBtn.removeClass('hidden');
                            }
                        });
                    },
                });

                $('#asunto').on('keyup', function(e) {
                    if (isTemplate) {
                        $updateTemplateBtn.removeClass('hidden');
                    }
                });

                $('.upload').each(function(i, elem) {
                    var dropzone = new Dropzone(elem, {
                        autoProcessQueue: false,
                        url: window.location.pathname,
                        paramName: 'files', // The name that will be used to transfer the file
                        maxFilesize: 3, // MB
                        uploadMultiple: true,
                        maxFiles: 5,
                        createImageThumbnails: true,
                        parallelUploads: 5,
                        acceptedFiles: 'image/*,application/pdf',
                        init: function() {
                            this.on('addedfile', function(file) {
                                let removeButton = Dropzone.createElement('<button class=\'btn waves-effect\' style=\'margin-top:5px;padding:0 1rem;\'>Eliminar</button>');
                                let _this = this;
                                let thumbnail = $(file.previewElement).find('.dz-image img');
                                let fileReader = new FileReader();
                                $inputName = $('.archivo-name').first();
                                $input = $('.archivo').first();
                                file.id = guid();

                                if (file instanceof File) {
                                    fileReader.onload = function(fileLoadedEvent) {
                                        if (file.status != 'error') {
                                            if ($input.val()) {
                                                $input.clone().attr('file-id', file.id).appendTo('#nuevo').val(fileLoadedEvent.target.result);
                                            }
                                            else {
                                                $input.val(fileLoadedEvent.target.result);
                                                $input.attr('file-id', file.id);
                                            }
                                        }
                                    };

                                    fileReader.readAsDataURL(file);
                                }
                                else if (file.data) {
                                    if ($input.val()) {
                                        $input.clone().attr('file-id', file.id).appendTo('#nuevo').val(file.data);
                                    }
                                    else {
                                        $input.val(file.data);
                                        $input.attr('file-id', file.id);
                                    }
                                }

                                if ($inputName.val()) {
                                    $inputName.clone().attr('file-id', file.id).appendTo('#nuevo').val(file.name);
                                }
                                else {
                                    $inputName.val(file.name);
                                    $inputName.attr('file-id', file.id);
                                }

                                if (file.type == 'application/pdf') {
                                    thumbnail.attr('src', server + 'dist/img/pdf-icon.png');
                                }

                                $('.dz-filename').addClass('truncate');

                                removeButton.addEventListener('click', function(e) {
                                    e.preventDefault();
                                    e.stopPropagation();

                                    _this.removeFile(file);
                                });

                                file.previewElement.appendChild(removeButton);

                                if (isTemplate) {
                                    $updateTemplateBtn.removeClass('hidden');
                                }

                            });
                            this.on('maxfilesexceeded', function(file) {
                                var _this = this;
                                _this.removeFile(file);

                                setTimeout(function() {
                                    $('.upload').parent().find('label.red-text').remove();
                                }, 3000);

                            });
                            this.on('removedfile', function(file) {
                                const $input = $('.archivo');

                                if ($input.length > 1) {
                                    $('input[file-id=' + file.id + ']').removeAttr('file-id').remove();
                                }
                                else {
                                    $('input[file-id=' + file.id + ']').removeAttr('file-id').val('');
                                }

                                let t = $(elem).find('div.dz-preview').length;

                                if (t == 0) {
                                    $(elem).find('text').show();
                                }
                                else {
                                    $(elem).find('text').hide();
                                }

                                if (isTemplate) {
                                    $updateTemplateBtn.removeClass('hidden');
                                }

                            });
                        },
                        accept: function(file, done) {
                            $(elem).find('text').hide();
                            done();
                        },
                        error: function(data, errorMessage) {
                            $('.upload').parent().append(`<label class='red-text'>${errorMessage}</label>`);

                            let _this = this;
                            _this.removeFile(data);
                            setTimeout(function() {
                                $('.upload').parent().find('label.red-text').remove();
                            }, 3000);
                        },
                    });
                });

                $('#send-email').on('click', function(e) {
                    isBeingSent = true;
                    $('#mensaje').val(tinyMCE.activeEditor.getContent());
                });

                $('#answer-email').on('submit', function(e) {
                    swal({
                        title: 'Cargando...',
                        closeOnClickOutside: false,
                        buttons: false,
                    });
                    // Make sure that the form isn't actually being sent.
                    e.preventDefault();
                    e.stopPropagation();
                    $mailForm.validate();
                    $sendEmailBtn.prop('disabled', true);

                    if ($mailForm.isValid()) {
                        formData = $('#answer-email :input[value!=\'\']').serialize();

                        $.ajax({
                            url: `${server}proveedor/solicitudes/sendMail/${mailId}`,
                            type: 'POST',
                            data: formData,
                            method: 'POST',
                            dataType: 'json',
                            success: function(response) {
                                swal('Exito', 'Tu mensaje fue enviado', 'success');
                                setTimeout(() => {
                                    location.reload();
                                }, 1500);
                            },
                            error: function(error) {
                                swal('Oops', 'Intentalo de nuevo mas tarde', 'error');
                                isBeingSent = false;
                                $sendEmailBtn.prop('disabled', false);
                            },
                        });
                    }
                });

                $('.btn-mover').on('click', function() {

                    var $self = $(this);
                    var data = new Array();

                    $.ajax({
                        url: server + 'proveedor/solicitudes/mover',
                        data: {
                            estado: $self.data('estado'),
                            solicitudes: [<?php echo $solicitud->id_correo ?>],
                        },
                        method: 'POST',
                        success: function() {
                            if (t == 'estado') {
                                $('#btn-mover i').removeAttr('class');
                                $('#btn-mover i').addClass($self.find('i').attr('class'));
                                switch ($self.data('estado')) {
                                    case 0:
                                        $('#btn-mover text').html('pendiente');
                                        break;
                                    case 1:
                                        $('#btn-mover text').html('atendida');
                                        break;
                                    case 2:
                                        $('#btn-mover text').html('contratada');
                                        break;
                                    case 3:
                                        $('#btn-mover text').html('descartada');
                                        break;
                                }
                            }

                            if (t == 'estado' && $self.data('estado') != v) {
                                $('.total-' + v).html(parseInt($('.total-' + v).html()) - 1);
                                $('.total-' + $self.data('estado')).html(parseInt($('.total-' + $self.data('estado')).html()) + 1);
                                v = $self.data('estado');
                            }
                            else {
                                $('.total-leido-' + v).html(parseInt($('.total-leido-' + v).html()) - data.length);
                            }
                        }, error: function() {
                        },
                    });
                });

                $('.btn-marcar').on('click', function() {
                    var $self = $(this);
                    var data = new Array();

                    $.ajax({
                        url: server + 'proveedor/solicitudes/marcar',
                        data: {
                            leida: $self.data('estado'),
                            solicitudes: [<?php echo $solicitud->id_correo ?>],
                        },
                        method: 'POST',
                        success: function() {
                            if ($self.data('estado') == 1 && leida == false) {
                                $('.total-leido-1').html(parseInt($('.total-leido-1').html()) + 1);
                                $('.total-leido-0').html(parseInt($('.total-leido-0').html()) - 1);
                                leida = !leida;
                            }
                            else if ($self.data('estado') == 0 && leida == true) {
                                $('.total-leido-0').html(parseInt($('.total-leido-0').html()) + 1);
                                $('.total-leido-1').html(parseInt($('.total-leido-1').html()) - 1);
                                leida = !leida;
                            }
                        }, error: function() {
                        },
                    });
                });

                $('.upload text').on('click', function(e) {
                    $(e.currentTarget.parentNode).trigger('click');
                });

                $('.delete-file').on('click', function(index, elemet) {
                    $(this).closest('.nombre-elem').remove();
                });

                $('.eliminar-foto').on('click', function(elm) {
                    var $btn = $(elm.currentTarget);
                    var $prom = $('#' + $btn.data('promocion'));
                    $prom.find('.upload div.dz-preview').remove();
                    $prom.find('text').show();
                    $prom.find('.archivo').val('');
                    $('#nuevo .nombre').html('');
                });

                $('#guardar-mensaje').on('change', function() {
                    $('#nombre_plantilla').toggleClass('hide');
                });

                $('.goto').on('click', function() {
                    var $self = $(this);
                    $('html,body').animate({
                        scrollTop: $($self.data('target')).offset().top,
                    }, 1000);
                });

                $('#select-plantilla').on('change', function() {

                    const $answerFormInputContainer = $('#answer-email').find('.card-panel > .row').first();
                    let valueSelect = $(this).val();
                    let dropzone = Dropzone.forElement('.upload');
                    let $self = $(this);
                    const $subject = $('#asunto');
                    $updateTemplateBtn.addClass('hidden');
                    isTemplate = false;

                    //Empty the form
                    $subject.val('');
                    tinyMCE.activeEditor.setContent('');
                    dropzone.removeAllFiles();

                    if (valueSelect != 'nuevo') {
                        swal({
                            title: 'Cargando...',
                            closeOnClickOutside: false,
                            buttons: false,
                        });
                        $inputName = $('.archivo-name').first();
                        $input = $('.archivo').first();

                        $.ajax({
                            url: server + 'proveedor/solicitudes/plantilla/' + $self.val(),
                            method: 'GET',
                            success: function(resp) {
                                $updateTemplateBtn.addClass('hidden');
                                $subject.val(resp.data.asunto);
                                templateId = resp.data.id_respuesta;
                                tinyMCE.activeEditor.setContent(resp.data.mensaje, {format: 'raw'});

                                if (resp.data.archivos.length) {
                                    $('.upload').find('div.dz-preview').each(function(i, value) {
                                        value.remove();
                                    });

                                    resp.data.archivos.forEach(function(archivo) {
                                            if (archivo.nombre.indexOf('pdf') != -1) {
                                                var file = server + 'dist/img/pdf-icon.png';
                                            }
                                            else {
                                                var file = server + 'proveedores/archivo_plantilla/' + archivo.id_archivo;
                                            }

                                            let mockFile = {name: archivo.nombre, data: archivo.data};

                                            dropzone.emit('addedfile', mockFile);
                                            dropzone.emit('thumbnail', mockFile, file);
                                            dropzone.emit('complete', mockFile);
                                            dropzone.files.push(mockFile);

                                            $('.upload').find('text').hide();
                                        },
                                    );
                                }
                                else {
                                    $('.upload').find('text').show();
                                }

                                isTemplate = true;
                                swal.close();
                            },
                            error: function() {
                                swal.close();
                            },
                        });
                    }

                });

                $('#modal-nota .guardar').on('click', function() {
                    $.ajax({
                        url: server + "proveedor/solicitudes/updatenota/<?php echo $solicitud->id_correo ?>",
                        data: {
                            nota: $('#modal-nota .nota').val(),
                        },
                        method: 'POST',
                        success: function(resp) {
                            $('#informacion').attr('class', 'card-panel green white-text');
                            $('#informacion i').attr('class', 'fa fa-check');
                            $('#informacion text').html('La nota se ha actualizado correctamente');
                            $('#solicitud-nota').removeClass('hide');
                            $('#solicitud-nota .card-panel').html($('#modal-nota .nota').val());
                        },
                        error: function() {
                            $('#informacion').attr('class', 'card-panel orange white-text');
                            $('#informacion i').attr('class', 'fa fa-times');
                            $('#informacion text').html('Ocurrio un error intete m&aacute;s tarde');
                        },
                    });
                });

                $updateTemplateBtn.on('click', function(e) {
                    swal({
                        title: 'Cargando...',
                        closeOnClickOutside: false,
                        buttons: false,
                    });
                    e.preventDefault();
                    $('#mensaje').val(tinyMCE.activeEditor.getContent());

                    if (isTemplate && templateId) {
                        $mailForm.validate();
                        $sendEmailBtn.prop('disabled', true);
                        $updateTemplateBtn.prop('disabled', true);

                        if ($mailForm.isValid()) {
                            formData = $('#answer-email :input[value!=\'\']').serialize();

                            $.ajax({
                                url: `${server}proveedor/solicitudes/updateTemplate/${templateId}`,
                                type: 'POST',
                                data: formData,
                                method: 'POST',
                                dataType: 'json',
                                success: function(response) {
                                    swal('Exito', 'Tu plantilla ha sido actualizada', 'success');
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1500);
                                },
                                error: function(error) {
                                    $sendEmailBtn.prop('disabled', false);
                                    $updateTemplateBtn.prop('disabled', false);
                                    swal('Oops', 'Intentalo de nuevo mas tarde', 'error');
                                },
                            });
                        }
                    }
                });

                var es = {
                    closeText: 'Cerrar',
                    prevText: 'Anterior',
                    nextText: 'Siguiente',
                    currentText: 'Aujourd\'hui',
                    monthNames: [
                        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: [
                        'Ene.', 'Feb.', 'Mar.', 'Abr.', 'May.', 'Jun.',
                        'Jul.', 'Ago.', 'Sept.', 'Oct.', 'Nov.', 'Dic.'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
                    dayNamesShort: ['dom.', 'lun.', 'mar.', 'mier.', 'juev.', 'vien.', 'sab.'],
                    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                    weekHeader: 'Sem.',
                    dateFormat: 'dd-mm-yy',
                    firstDay: 1,
                    maxDate: 'now',
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: '',
                };

                $('#desde').datepicker(es);

                $('#desde').datepicker('option', 'onClose', function(selectedDate) {
                    $('#hasta').datepicker('option', 'minDate', selectedDate);
                });

                $('#hasta').datepicker(es);

                $('#hasta').datepicker('option', 'onClose', function(selectedDate) {
                    $('#desde').datepicker('option', 'maxDate', selectedDate);
                });

                $('#exportar-descargar').on('click', function(evt) {
                    var self = this;
                    $.ajax({
                        url: server + 'proveedor/solicitudes/exportar/' + $('#desde').val() + '/' + $('#hasta').val() + '/solicitudes.xls',
                        method: 'GET',
                        success: function(resp) {
                            saveDataLink.apply(self, [resp, 'solicitudes.xls']);
                        },
                    });
                });

                function saveDataLink(csv, filename) {
                    var csvData = 'data:application/xls;base64,' + btoa(csv);
                    var a = document.createElement('a');
                    $(a).attr({
                        'download': filename,
                        'href': csvData,
                        'target': '_blank',
                    });
                    $(a).on('click', function() {});
                    a.click();
                }
            });

            function generarPDF() {
                window.open(server + "proveedor/solicitudes/imprimir/<?php echo $solicitud->id_correo ?>", 'ventana1', 'width=400,height=600,scrollbars=NO');
            }

            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                }

                return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
            }

            function setupDropzone() {
                Dropzone.prototype.defaultOptions.dictDefaultMessage = 'Tira tus archivos aqui';
                Dropzone.prototype.defaultOptions.dictFallbackMessage = 'Lo sentimos parece ser que tu navegador no es compatible';
                Dropzone.prototype.defaultOptions.dictFileTooBig = 'El archivo es muy grande ({{filesize}}MiB). Tamaño maximo: {{maxFilesize}}MiB.';
                Dropzone.prototype.defaultOptions.dictInvalidFileType = 'No se pueden subir archivos de este tipo';
                Dropzone.prototype.defaultOptions.dictResponseError = 'El servidor respondio con este codigo {{statusCode}}.';
                Dropzone.prototype.defaultOptions.dictCancelUpload = 'Cancelar subida';
                Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = 'Estas seguro que deseas cancelar?';
                Dropzone.prototype.defaultOptions.dictRemoveFile = 'Remover archivo';
                Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = 'Ya no puedes subir mas archivos.';
            }

        </script>
    </body>
</html>