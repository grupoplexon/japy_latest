<html>
    <head>
        <?php $this->view("register/header") ?>
    </head>
    <body>
        <?php $this->view("principal/menu") ?>
        <div class="body-container">
            <div class="hide-on-small-only">
                <div style="margin:10%;"></div>
            </div>
            <h5>Tu cuenta se encuentra activada</h5>
            <div class="divider"></div>
            <div>
                <div class="card-panel valign-wrapper">
                    <div class="valign col s12 center" style="width: 100%">
                        <i class="fa fa-check fa-5x green-text darken-5" style="    border-radius: 50%; border: 4px solid;padding: 13px;"></i>
                        <p style="font-size: 16px">
                            Tu cuenta ya se encuentra activada, disfruta de las ventajas de ser<b> miembro <a class="dorado-2-text" href="<?php echo base_url() ?>index.php/">japybodas.com</a></b><br>
                            En donde podras organizar tu boda, contactar proveedores y ganar premios con nuestros sorteos.
                        </p>
                        <p>
                            Accede a tu cuenta ingresando tu usuario y contraseña desde el portal 
                            <a class="dorado-2-text" href="<?php echo base_url() ?>index.php/">japybodas.com</a>
                            , te recomendamos cambiar tu contraseña
                        </p>
                        <a href="<?php echo base_url() ?>cuenta" class="btn dorado-2 waves-effect" style="width: 250px;float: right " >
                            iniciar sesi&oacute;n <i class="fa fa-chevron-right"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <?php $this->view("register/footer") ?>
    </body>

</html>