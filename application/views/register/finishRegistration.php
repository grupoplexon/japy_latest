<html>
<head>
    <?php $this->view("principal/newHeader") ?>
    <style>
        #countryId, #stateId, #cityId {
            background: #FFF !important;
            text-indent: 15px;
            font-size: 12px;
            height: 2rem;
            width: auto;
            border: 1px solid #f2f2f2;
        }

        video::-internal-media-controls-download-button {
            display: none;
        }

        video::-webkit-media-controls-enclosure {
            overflow: hidden;
        }

        video::-webkit-media-controls-panel {
            width: calc(100% + 30px); /* Adjust as needed */
        }

        .yellow.darken-5 {
            background: #00BCDD !important;
            color: #FFFFFF !important;
        }

        .yellow-text.darken-5 {
            color: #00BCDD !important;
        }

        input[type=email].formulario:focus:not([readonly]), input[type=text].formulario:focus:not([readonly]), input[type=tel].formulario:focus:not([readonly]), input[type=search].formulario:focus:not([readonly]), input[type=number].formulario:focus:not([readonly]), input[type=password].formulario:focus:not([readonly]) {
            border-bottom: 1px solid #00BCDD !important;
            box-shadow: 0 1px 0 0 #00BCDD !important
        }

        .select-dropdown li span {
            color: #514f50 !important;
        }

        @media only screen and (max-width: 425px) {
            .center-text {
                text-align: center;
            }

        }
    </style>
</head>
<body class="bg-malla-corazones">
<div class="body-container">
    <div class="row" style="margin-top: 20px;">
        <div class="col s2 m1">&nbsp;</div>
        <div class="col s12 m10">
            <form method="POST" id="registro" action="<?php echo base_url()."registro/finishRegistration" ?>">
                <div class="row">
                    <div class="col s12 m7 right">
                        <!--<img class="responsive-img" src="<?php echo base_url() ?>/dist/img/club.png" alt=""/>-->
                        <video class="video" style=" width: 100%; height: auto" autoplay="true" contextmenu="false"
                               controls="true">
                            <source src="<?php echo base_url() ?>uploads/videos/otros/CN_Corte1.mp4" type="video/mp4"/>
                        </video>
                    </div>
                    <div class="col s12 m5">
                        <h4 class="center-text">&iquest;TE ACUERDAS CUANDO SO&Ntilde;ABAS CON EL D&Iacute;A
                            DE TU BODA?</h4>
                        <p class="center-text"><b>Ya no es un sue&ntilde;o &iexcl;ES REAL!</b></p>
                        <p style="text-align: justify">

                            Te damos la bienvenida a este espacio dedicado a la creaci&oacute;n de momentos
                            inolvidables. Ya eres parte de Japy,
                            no tienes que preocuparte por nada, pues te daremos todas las herramientas para que
                            tengas todo bajo control.
                        </p>
                        <img class="responsive-img col s12"
                             src="<?php echo base_url() ?>/dist/img/vineta_dorada.png" alt=""/>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 z-depth-1" style="background: rgba(195,200,204,0.5)">
                        <div class="row" style="text-align: center">
                            <h5 class="center-align white-text">REG&Iacute;STRATE!</h5>
                        </div>
                        <div class="row">
                            <div class="col s12 m3">
                                <input class="col s12 formulario" type="text" name="nombre_novia"
                                       placeholder="Nombre(s) de la novia:" required
                                       value="<?php echo $user->nombre ?>">
                            </div>
                            <div class="col s12 m3">
                                <!-- DIVIDER -->
                                <input class="col s12 formulario" type="text" name="apellido_novia"
                                       placeholder="Apellido(s) de la novia:" required>
                            </div>
                            <div class="col s12 m3">
                                <!--<h6>Ingresa tus datos</h6>-->
                                <input class="col s12 formulario" type="text" name="nombre_novio"
                                       placeholder="Nombre(s) del novio:" required>
                            </div>
                            <div class="col s12 m3">
                                <!-- DIVIDER -->
                                <input class="col s12 formulario" type="text" name="apellido_novio"
                                       placeholder="Apellido(s) del novio:" required>
                            </div>
                            <div class="col s12 m6 ">
                                <select name="country" class="col s12 browser-default countries" id="countryId"
                                        style="width: 100%">
                                    <option value="" disabled selected>-- Pa&iacute;s --</option>
                                </select>
                            </div>
                            <div class="col s12 m6">
                                <select name="state" class="col s12 browser-default states" id="stateId"
                                        style="width: 100%">
                                    <option value="" disabled selected>-- Estado --</option>
                                </select>
                            </div>
                            <!--DIVIDER -->
                            <div class="col s12 m6">
                                <select name="city" id="cityId" class="col s12 browser-default cities"
                                        style="width: 100%">
                                    <option value="" disabled selected>-- Poblaci&oacute;n --</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6">
                                <input type="date" class="datepicker col s12 formulario" name="fecha_boda"
                                       placeholder="Fecha del evento:" required>
                            </div>
                            <div class="col s12 m6">
                                <input type="number" class="col s12 formulario" name="no_invitados"
                                       placeholder="N&uacute;mero aproximado de invidados:" maxlength="11" required>
                            </div>
                            <div class="col s12 m6">
                                <input type="text" class="col s12 formulario" name="presupuesto"
                                       placeholder="Presupuesto tentativo:" required>
                            </div>
                            <div class="col s12 m6">
                                <!-- DIVIDER -->
                                <input class="col s12 formulario" type="email" id="correo" name="correo"
                                       placeholder="Correo:" readonly="readonly" required value="<?php echo $user->correo ?>">
                            </div>
                            <div class="col s12 m6">
                                <input class="col s12 formulario" type="password" name="contrasena"
                                       placeholder="Contrase&ntilde;a:" required>
                                <!-- DIVIDER -->
                            </div>
                            <div class="col s12 m6 right">
                                <select class="col s12 formulario  right browser-default" name="tipo" required
                                        style="    margin-top: 0px;">
                                    <option value="" disabled>Yo soy*</option>
                                    <option value="1">Novio</option>
                                    <option value="2" selected>Novia</option>
                                </select>
                            </div>
                            <div class="col s12">
                                <button class="btn yellow darken-5" id="btn_enviar"
                                        type="button" style="margin-top: 10px; float: right!important; right: 0px">
                                    CONTINUAR
                                </button>
                            </div>
                            <?php $this->view("register/mensajes") ?>
                        </div>
                    </div>
                </div>
            </form>
            <p>Estos datos son muy importantes para poder ayudarte con la planeaci&oacute;n de tu boda. (sin estos
                datos no obtendr&aacute;n las ventajas)</p>
            <img class="responsive-img col s12" src="<?php echo base_url() ?>/dist/img/vineta_dorada.png" alt=""/>
            </p>
        </div>
        <div class="col s2 m1">&nbsp;</div>
    </div>
</div>
<?php // $this->view("register/footer") ?>
<?php $this->view("principal/newFooter") ?>
</body>
<?php $this->view("principal/footer") ?>
<script>
    $(document).ready(function () {
        $('select').material_select();
        $(".button-collapse").sideNav();
        $(".dropdown-button").dropdown();
        $('input[name="presupuesto"]').inputFormat().css('text-align', '');

        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 15,
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Aceptar',
            format: 'yyyy-mm-dd',
            formatSubmit: 'yyyy-mm-dd',
            min: new Date()
        });

        $('#btn_enviar').on('click', function () {
            var bandera = true;
            if (isEmpty($('[name="tipo"]').val())) {
                alert('Seleccione Yo soy');
                bandera = false;
            }

            if (bandera && $('[name="tipo"]').val() == 1) {
                if (isEmpty($('[name="correo"]').val()) || isEmpty($('[name="apellido_novio"]').val())
                    || isEmpty($('[name="fecha_boda"]').val()) || isEmpty($('[name="nombre_novio"]').val())
                    || isEmpty($('[name="contrasena"]').val())
                    || !isEmpty($('[name="no_invitados"]').val()) && !isNumeric($('[name="no_invitados"]').val())) {

                    isEmptyChangeStyle($('[name="fecha_boda"]'));
                    isEmptyChangeStyle($('[name="correo"]'));
                    isEmptyChangeStyle($('[name="apellido_novio"]'));
                    isEmptyChangeStyle($('[name="contrasena"]'));
                    isEmptyChangeStyle($('[name="nombre_novio"]'));
                    isEmptyChangeStyle($('[name="apellido_novia"]'));
                    isEmptyChangeStyle($('[name="nombre_novia"]'));
                    isNumericChangeStyle($('[name="no_invitados"]'));
                    bandera = false;
                }
            } else if (bandera && $('[name="tipo"]').val() == 2) {
                if (isEmpty($('[name="correo"]').val()) || isEmpty($('[name="apellido_novia"]').val())
                    || isEmpty($('[name="fecha_boda"]').val()) || isEmpty($('[name="nombre_novia"]').val())
                    || isEmpty($('[name="contrasena"]').val())
                    || !isEmpty($('[name="no_invitados"]').val()) && !isNumeric($('[name="no_invitados"]').val())) {

                    isEmptyChangeStyle($('[name="fecha_boda"]'));
                    isEmptyChangeStyle($('[name="correo"]'));
                    isEmptyChangeStyle($('[name="contrasena"]'));
                    isEmptyChangeStyle($('[name="apellido_novia"]'));
                    isEmptyChangeStyle($('[name="nombre_novia"]'));
                    isEmptyChangeStyle($('[name="apellido_novio"]'));
                    isEmptyChangeStyle($('[name="nombre_novio"]'));
                    isNumericChangeStyle($('[name="no_invitados"]'));
                    bandera = false;
                }
            }

            if (!isValidEmail($('[name="correo"').val())) {
                $('[name="correo"]').attr('class', $('[name="correo"]').attr('class') + ' invalid');
                bandera = false;
            } else {
                $('[name="correo"]').attr('class', ($('[name="correo"]').attr('class')).split('invalid')[0]);
            }

            if (bandera) {
                $('#btn_enviar').attr('disabled', '');
                $('#registro').submit();
            }
        });
    });

    function isEmpty(element) {
        if (element == '' || element == null || element.length == 0) {
            return true;
        }
        return false;
    }


    function isEmptyChangeStyle(element) {
        if (isEmpty(element.val())) {
            element.attr('class', element.attr('class') + ' invalid');
        } else {
            element.attr('class', (element.attr('class')).split('invalid')[0]);
        }
    }

    function noIsEmpty(element) {
        element.removeClass('invalid');
    }

    function isReal(element) {
        var rev = /(^[0-9][0-9]$)|(^[0-9]*(\.?[0-9]+)$)/;
        if (!rev.exec(element) && element.length <= 50) {
            return false;
        }
        return true;
    }

    function isNumeric(element) {
        var rev = /(^[0-9]*$)/;
        if (!rev.exec(element) && element.length <= 11) {
            return false;
        }
        return true;
    }

    function isNumericChangeStyle(element) {
        if (!isNumeric(element.val()) && !isEmpty(element.val())) {
            element.attr('class', element.attr('class') + ' invalid');
        } else {
            element.attr('class', (element.attr('class')).split('invalid')[0]);
        }
    }

    function isRealChangeStyle(element) {
        if (!isReal(element.val()) && !isEmpty(element.val())) {
            element.attr('class', element.attr('class') + ' invalid');
        } else {
            element.attr('class', (element.attr('class')).split('invalid')[0]);
        }
    }

    function isValidEmail(email) {
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>
</html>
