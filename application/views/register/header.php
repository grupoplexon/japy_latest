<?php if (isset($titulo)) { ?>
    <title><?php echo $titulo ?></title>
<?php } else { ?>
    <title>Japy</title>
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="theme-color" content="#f5e6df">
<meta name="msapplication-navbutton-color" content="#f5e6df">
<meta name="apple-mobile-web-app-status-bar-style" content="#f5e6df">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="icon" href="<?php echo base_url() ?>dist/img/favicon.png">
<link href="<?php echo base_url() ?>dist/img/clubnupcial.png" rel="icon" sizes="16x16">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo base_url() ?>dist/css/materialize.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/estilo.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>dist/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<style>
    footer {
        position: fixed;
    }

    .red-text.darken-5 {
        color: #92151b !important;
    }

    .red.darken-5 {
        background: #94151c !important;
    }

    select.countries, select.states, select.cities {
        background-color: rgba(255, 255, 255, 0.9);
        width: 100%;
        padding: 5px;
        border: 1px solid #f2f2f2;
        border-radius: 2px;
        height: 2rem;
    }

    .formulario > input.select-dropdown.invalid {
        border: 1px solid #F44336;
    }

    .formulario > input.select-dropdown {
        background: #FFF !important;
        border: 1px solid #f2f2f2;
        text-indent: 15px;
        font-size: 10px;
        margin-left: -10;
        height: 2rem;
        line-height: 2rem;
    }

    .formulario > span.caret {
        right: 7px;
        top: 12px;
    }

    input[type=date].formulario, input[type=text].formulario, input[type=email].formulario, input[type=search].formulario, input[type=password].formulario {
        background: #FFF !important;
        text-indent: 15px;
        font-size: 10px;
        height: 2rem;
        width: auto;
        border: 1px solid #f2f2f2;
    }

    input[type=date].formulario:focus:not([readonly]), input[type=email].formulario:focus:not([readonly]), input[type=text].formulario:focus:not([readonly]), input[type=search].formulario:focus:not([readonly]), input[type=password].formulario:focus:not([readonly]) {
        border-bottom: 1px solid #f9a797;
        box-shadow: 0 1px 0 0 #f2f2f2;
    }

    .btn {
        width: 100%;
    }

    footer.page-footer {
        margin-top: 0px;
        padding-top: 10px;
    }

    li {
        margin-bottom: 10px;
    }

    html {
        line-height: 1.5;
    }
</style>