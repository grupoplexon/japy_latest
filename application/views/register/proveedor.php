<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/intl-tel-input/build/css/intlTelInput.min.css">
        <?php
            $this->view("japy/prueba/header");
            $this->view("register/header");
        ?>
        <style>
            .select2-container--default .select2-results__group {
                font-weight: bold !important;
            }

            .select2-search__field {
                border-color: transparent !important;
                box-shadow: none !important;
            }

            p > label {
                display: block;
            }
            .intl-tel-input {
                padding-left: 50px !important;
            }
            .intl-tel-input.allow-dropdown input, .intl-tel-input.allow-dropdown input[type=tel], .intl-tel-input.allow-dropdown input[type=text], .intl-tel-input.separate-dial-code input, .intl-tel-input.separate-dial-code input[type=tel], .intl-tel-input.separate-dial-code input[type=text] {
                padding-right: 0px !important;
            }
            footer {
                position: unset !important;
            }
        </style>
    </head>
    <body class="bg-malla-corazones">
        <?php $this->view("register/mensajes") ?>
        <div class="body-container">
            <!-- <div class="hide-on-small-only">
                <div style="margin:10%;"></div>
            </div> -->
            <div class="row">
                <div class="col s2 m1">&nbsp;
                </div>
                <div class="col s12 m10" style="margin-top: 15%;">
                    <div class="card-panel primary-background">
                        <div class="row ">
                            <div class="col m8">
                                <img style="width: 50%; padding: 10px;"
                                     src="<?php echo base_url() ?>dist/img/BrideAdvisorBlack.png" alt=""/>
                            </div>
                            <div class="col m4">
                                <h5>¡Haga crecer su negocio con BrideAdvisor.mx!</h5>
                                <ul>
                                    <li>
                                        Reciba solicitudes de presupuesto de novios interesados.
                                    </li>
                                    <li>
                                        Consiga nuevos clientes y multiplique el &eacute;xito de su negocio.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-panel">
                        <form id="form-proveedor" method="POST"
                              action="<?php echo base_url() ?>registro/proveedor">
                            <section class="row">
                                <div class="divider"></div>
                                <h5 class="col m12">Datos de Contacto</h5>
                                <div class="col s12 m4 right">
                                    <div class="card-panel  gris-3 ">
                                        <p class="black-text  " style="text-align: justify">
                                            Recibir&aacute;s las solicitudes de informaci&oacute;n de usuarios interesados
                                            en la direcci&oacute;n de correo elect&oacute;nico que hayas indicado,
                                            as&iacute; como todas las novedades del portal que puedan ser de tu inter&eacute;s.
                                        </p>
                                    </div>
                                </div>
                                <div class="col s12 m8">
                                    <p class="">
                                        <label>Persona de Contacto</label>
                                        <input class="form-control validate[required,funcCall[checkSimbolos]]" name="nombre"
                                               value="<?php echo isset($nombre) ? $nombre : "" ?>">
                                    </p>
                                    <p>
                                        <label>Correo electr&oacute;nico</label>
                                        <input class="form-control validate[required,custom[email]]" name="correo"
                                               value="<?php echo isset($correo) ? $correo : "" ?>">
                                    </p>
                                    <p>
                                        <label>Celular</label>
                                        <input class="form-control" type="tel" id="celular" name="celular"
                                               value="<?php echo isset($celular) ? $celular : "" ?>" >
                                    </p>
                                    <p>
                                        <label>Tel&eacute;fono</label>
                                        <input class="form-control" type="tel" id="telefono" name="telefono"
                                               value="<?php echo isset($telefono) ? $telefono : "" ?>" >
                                    </p>
                                    <p>
                                        <label>P&aacute;gina Web</label>
                                        <input class="form-control validate[funcCall[checkURL]]" name="pagina_web"
                                               value="<?php echo isset($pagina_web) ? $pagina_web : "" ?>">
                                    </p>
                                </div>
                            </section>
                            <section class="row">
                                <div class="divider"></div>
                                <h5>Empresa</h5>
                                <div class="col s12 m4 right ">
                                    <div class="card-panel gris-3">
                                        <p class="black-text  " style="text-align: justify">
                                            Describa detalladamente su empresa as&iacute; como los servicios o productos de
                                            bodas
                                            que ofrece con la m&aacute;xima informaci&oacute;n de inter&eacute;s para los
                                            novios.
                                        </p>
                                        <p class="black-text  " style="text-align: justify">
                                            No se pueden incluir datos de contacto
                                            como correo electr&oacute;nico, tel&eacute;fono, p&aacute;gina web, direcci&oacute;n
                                            etc.
                                        </p>
                                        <p class="black-text  " style="text-align: justify">
                                            Introduzca la direcci&oacute;n de su empresa para que los
                                            usuarios interesados puedan ver su localizaci&oacute;n.
                                            Tenga en cuenta que el buscador y los mapas se basan en esta informaci&oacute;n.
                                        </p>
                                    </div>
                                </div>
                                <div class="col s12 m8">
                                    <p class="">
                                        <label>Nombre</label>
                                        <input class="form-control  validate[required,funcCall[checkSimbolos]]"
                                               name="nombre_empresa"
                                               value="<?php echo isset($nombre_empresa) ? $nombre_empresa : "" ?>">
                                    </p>
                                    <p class="">
                                        <label>Descripci&oacute;n de su empresa</label>
                                        <textarea class="form-control  validate[required]" name="descripcion_empresa"
                                                  style="height: 200px"><?php echo isset($descripcion_empresa) ? $descripcion_empresa : "" ?></textarea>
                                    </p>
                                    <p class="">
                                        <label>Pais</label>
                                        
                                        <select id="selectCountry" name="pais_empresa"
                                                data-default="<?php echo isset($pais_empresa) ? $pais_empresa : "Mexico" ?>"
                                                class="browser-default form-control validate[required] countries"
                                                style="height: 50px">
                                                <option value="142">Mexico</option>
                                                <?php foreach ($countries as $country) : ?>
                                                    <option value= "<?= $country->id_pais; ?>"><?= $country->nombre; ?></option>
                                                <?php endforeach; ?>
                                            
                                        </select>
                                        <?php //echo $countries ?>
                                        <!--<input  class="form-control validate[required]" name="pais_empresa" value="<?php echo isset($pais_empresa) ? $pais_empresa : "" ?>">-->
                                    </p>
                                    <p class="">
                                        <label>Estado</label>
                                        <select id="selectState" name="estado_empresa"
                                                data-default="<?php echo isset($estado_empresa) ? $estado_empresa : "" ?>"
                                                class="browser-default form-control validate[required] states"
                                                style="height: 50px">
                                                <option>Selecciona un estado...</option>
                                        </select>
                                        <!-- <input  class="form-control validate[required]" name="estado_empresa" value="<?php echo isset($estado_empresa) ? $estado_empresa : "" ?>">-->
                                    </p>
                                    <p class="">
                                        <label>Poblaci&oacute;n</label>
                                        <select id="selectCity" name="estado_poblacion"
                                                data-default="<?php echo isset($estado_poblacion) ? $estado_poblacion : "" ?>"
                                                class="browser-default form-control cities" style="height: 50px">
                                            <option>Selecciona una ciudad...</option>
                                        </select>
                                        <!--<input  class="form-control validate[required]" name="estado_poblacion" value="<?php echo isset($estado_poblacion) ? $estado_poblacion : "" ?>">-->
                                    </p>
                                    <p class="">
                                        <label>C&oacute;digo Postal</label>
                                        <input class="form-control validate[required,custom[integer]]" name="cp"
                                               value="<?php echo isset($cp) ? $cp : "" ?>">
                                    </p>
                                    <p class="">
                                        <label>Direcci&oacute;n</label>
                                        <input class="form-control  validate[required]" name="direccion"
                                               value="<?php echo isset($direccion) ? $direccion : "" ?>">
                                    </p>
                                </div>
                            </section>
                            <section class="row">
                                <div class="divider"></div>
                                <h5>Datos de acceso</h5>
                                <div class="col s12 m4 right">
                                    <div class="card-panel gris-3">
                                        <p class="black-text" style="text-align: justify">
                                            El Usuario debe tener por lo menos 6 caracteres.<br>
                                            La Contraseña debe tener por lo menos 8 caracteres.<br>
                                            Tome en cuenta may&uacute;sculas y min&uacute;sculas.
                                        </p>
                                    </div>
                                </div>
                                <div class="col s12 m8">
                                    <p class="">
                                        <label>Usuario</label>
                                        <input id="username"
                                               class="form-control  validate[required,minSize[6],ajax[ajaxNameCall]]"
                                               name="usuario" value="<?php echo isset($usuario) ? $usuario : "" ?>">
                                    </p>
                                    <p class="">
                                        <label>Contraseña</label>
                                        <input type="password" id="pass"
                                               class="form-control validate[required,minSize[8],funcCall[checkPassword]]"
                                               name="password">
                                    </p>
                                    <p class="">
                                        <label>Comfirmar Contraseña</label>
                                        <input type="password"
                                               class="form-control validate[required,minSize[8],equals[pass]]"
                                               name="repassword">
                                    </p>
                                </div>
                            </section>
                            <section class="row">
  q                            <div class="divider"></div>
                                <h5>Sector de actividad</h5>
                                <select class="category-selector" name="categories[]" multiple="multiple">
                                    <?php foreach ($categories as $category) : ?>
                                        <!-- <optgroup label="<?php echo $category->name ?>"> -->
                                            <?php //foreach ($category->subcategories as $subcategory) : ?>
                                                <option value="<?php echo $category->id ?>">
                                                    <?php echo $category->name ?>
                                                </option>
                                            <?php //endforeach; ?>
                                        <!-- </optgroup> -->
                                    <?php endforeach; ?>
                                </select>

                            </section>
                            <section class="row">
                                <div class="divider"></div>
                                <div class="col m12">
                                    <p>
                                        Al presionar "Aceptar" est&aacute;s aceptando expresamente nuestras <a
                                                class="dorado-2-text"
                                                href="<?php echo base_url() ?>terminos-japy.pdf" target="_blank">condiciones
                                            legales</a>.
                                    </p>
                                    <button type="submit" class="btn dorado-2">
                                        Aceptar
                                    </button>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
                <div class="col s2 m1">
                </div>
            </div>
        </div>
        <div style="margin-top: 150px;width: 100%">

        </div>
        <?php $this->view("japy/prueba/footer") ?>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/select2.full.min.js"
                type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/admin/assets/plugins/select2/dist/js/i18n/es.js"
                type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/intl-tel-input/build/js/intlTelInput.min.js"
                type="text/javascript"></script>
        <script src="<?php echo base_url() ?>dist/js/location.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                $('#telefono').intlTelInput();
                $('#celular').intlTelInput();

                $('#form-proveedor').submit(function(e) {
                    var countryDataTel = $('#telefono').intlTelInput('getSelectedCountryData');
                    var countryDataCel = $('#celular').intlTelInput('getSelectedCountryData');
                    var completeNumberTel = `+${countryDataTel.dialCode}${$('#telefono').val()}`;
                    var completeNumberCel = `+${countryDataCel.dialCode}${$('#celular').val()}`;
                    $('#telefono').val(completeNumberTel);
                    $('#celular').val(completeNumberCel);
                });

                $('#form-proveedor').validationEngine({
                    'ajaxUserCall': {
                        'url': 'ajaxValidateFieldUser',
                        'extraData': 'name=eric',
                        'extraDataDynamic': ['#user_id', '#user_email'],
                        'alertText': '* This user is already taken',
                        'alertTextOk': 'All good!',
                        'alertTextLoad': '* Validating, please wait',
                    },
                });

                $('.category-selector').select2({
                    width: '100%',
                    maximumSelectionLength: 1,
                    placeholder: 'Seleccione su categoria',
                    allowClear: true,
                    language: 'es',
                });

                var location = {};
                $.ajax({
                    url: 'https://geoip-db.com/jsonp/',
                    jsonpCallback: 'callback',
                    dataType: 'jsonp',
                    timeout: 8000,
                    success: function(location) {
                        // location
                        if($('select[name=pais_empresa]').children("option:selected").val()){
                            console.log('entrando...');
                            $.ajax({
                                url: '<?= base_url().'/registro/getStates/' ?>'+$('select[name=pais_empresa]').children("option:selected").val()+'/'+location.state,
                                dataType: 'json',
                                timeout: 8000,
                                success: function(response) {
                                    /////////////////////////
                                    var myselect = $('<select>');
                                    $.each(response.states, function(index, key) {
                                        myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                                    });
                                    $('#selectState').append(myselect.html());
                                    /////////////////////////
                                    var myselect = $('<select>');
                                    $.each(response.cities, function(index, key) {
                                        myselect.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                                    });
                                    $('#selectCity').append(myselect.html());
                                    /////////////////////////
                                    // $('#selectCountry').html(location.country_name);
                                    $('#selectState').val(response.state.id_estado);
                                    $('#selectCity').val(location.city);

                                    $( "#selectCountry" ).change(function() {
                                        $.ajax({
                                            url: '<?= base_url().'/registro/getStates/' ?>'+$('#selectCountry').val(),
                                            dataType: 'json',
                                            timeout: 8000,
                                            success: function(response) {
                                                // $('#selectCity').remove();
                                                $("#selectState").empty();
                                                var myselect = $('<select>');
                                                myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                                                $.each(response.states, function(index, key) {
                                                    myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                                                });
                                                $('#selectState').append(myselect.html());
                                            }
                                        });
                                    });

                                    $( "#selectState" ).change(function() { /// obtiene ciudades
                                        $.ajax({
                                            url: '<?= base_url().'/registro/getCities/' ?>'+$('#selectState').val(),
                                            dataType: 'json',
                                            timeout: 8000,
                                            success: function(response) {
                                                // $('#selectCity').remove();
                                                $("#selectCity").empty();
                                                var myselect = $('<select>');
                                                myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                                                $.each(response.cities, function(index, key) {
                                                    myselect.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                                                });
                                                $('#selectCity').append(myselect.html());
                                            }
                                        });
                                    });

                                    // $( "#selectCity" ).change(function() {
                                       
                                    // });
                                },
                            });
                        }
                    },
                    error: function(e){
                        console.log('ERROR localizacion');
                        $( "#selectCountry" ).change(function() {
                            $.ajax({
                                url: '<?= base_url().'/registro/getStates/' ?>'+$('#selectCountry').val(),
                                dataType: 'json',
                                timeout: 8000,
                                success: function(response) {
                                    // $('#selectCity').remove();
                                    $("#selectState").empty();
                                    var myselect = $('<select>');
                                    myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                                    $.each(response.states, function(index, key) {
                                        myselect.append( $('<option></option>').val(key.id_estado).html(key.estado) );
                                    });
                                    $('#selectState').append(myselect.html());
                                }
                            });
                        });

                        $( "#selectState" ).change(function() { /// obtiene ciudades
                            $.ajax({
                                url: '<?= base_url().'/registro/getCities/' ?>'+$('#selectState').val(),
                                dataType: 'json',
                                timeout: 8000,
                                success: function(response) {
                                    // $('#selectCity').remove();
                                    $("#selectCity").empty();
                                    var myselect = $('<select>');
                                    myselect.append( $('<option></option>').val(0).html('Selecciona una ciudad...') );
                                    $.each(response.cities, function(index, key) {
                                        myselect.append( $('<option></option>').val(key.ciudad).html(key.ciudad) );
                                    });
                                    $('#selectCity').append(myselect.html());
                                }
                            });
                        });
                    }
                });
            });

        </script>
    </body>
</html>
