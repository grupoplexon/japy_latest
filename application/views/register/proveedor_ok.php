<html>
    <head>
        <?php $this->view("register/header") ?>
    </head>
    <body class="bg-malla-corazones">
        <div class="container">
            <div class="hide-on-small-only">
                <div style="margin:10%;"></div>
            </div>
            <div class="row">
                <div class="col s2 m1">&nbsp;</div>
                <div class="col s12 m10">
                    <div class="card-panel">

                        <h4>Tus datos se han registrado</h4>
                        <p>
                            V&aacute;lidaremos la informaci&oacute;n y nos pondremos en contacto.
                            <a href="<?php echo base_url() ?>" class="btn dorado-2">
                                Aceptar
                            </a>
                        </p>

                    </div>
                </div>
                <div class="col s2 m1">&nbsp;</div>
            </div>
        </div>
        <?php $this->view("register/footer") ?>
    </body>

</html>