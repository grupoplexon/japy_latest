<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro - BrideAdvisor</title>
    <?php $this->view("japy/prueba/header"); ?>
    <link rel="stylesheet" href="<?= base_url() ?>dist/css/registro/registro.css">
</head>
<body>
    <div class="row padding-row" style="background-image: url('<?php echo base_url() ?>dist/img/texturaBa.jpg');
                            background-size: cover;">
        <div class="container">
            <div class="col l5 m6">
                <div class="col l12 s12 border-head paddings card-shadow"
                style="background-image: url('<?php echo base_url() ?>dist/img/brideadvisor/registerNovia.jpg');
                                background-size: cover; background-repeat: no-repeat; height: 250px">
                    <!-- <img class="border-head" src="<?= base_url() ?>dist/img/brideadvisor/registerNovia.jpg" style="width: 100%;"> -->
                    <div class="border-head background col l12 s12 center-align" style="height: 250px;">
                        <div class="col l12 s12 m12">
                            <h4 class="title-card">NOVIO / NOVIA</h4>
                        </div>
                    </div>
                </div>
                <div class="col l12 s12 m12 white center-align paddings card-shadow border-footer large-fcard">
                    <div class="col l8 offset-l2">
                        <p class="margin-desc color-text">Registra aquí tus datos para comenzar a organizar tu boda con las herramientas y proveedores que tenemos para ti.</p>
                        <a class="btn grey darken-4" href="<?= base_url() ?>cuenta/novia">Inicia sesión</a>
                    </div>
                    <div class="col l12 s12 center-align">
                        <br>
                        <p class="color-text">Si no tienes una cuenta, <a class="redirect" href="<?= base_url() ?>registro/registroNovia">haz click aquí.</a></p>
                        <br>
                    </div>
                </div>
            </div>
            <div class="col s12 hide-on-large-only hide-on-med-only space">

            </div>
            <div class="col l5 m6 offset-l1">
                <div class="col l12 s12 border-head paddings card-shadow"
                style="background-image: url('<?php echo base_url() ?>dist/img/brideadvisor/registerProveedor.jpg');
                                background-size: cover; background-repeat: no-repeat; height: 250px">
                    <!-- <img class="border-head" src="<?= base_url() ?>dist/img/brideadvisor/registerNovia.jpg" style="width: 100%;"> -->
                    <div class="border-head background col l12 s12 center-align" style="height: 250px;">
                        <div class="col l12 s12 m12">
                            <h4 class="title-card">EMPRESA</h4>
                        </div>
                    </div>
                </div>
                <div class="col l12 s12 m12 white center-align paddings card-shadow border-footer large-fcard">
                    <div class="col l8 offset-l2">
                        <p class="margin-desc color-text">Registra tu perfil como proveedor y ten acceso a todo el mercado de novios para que tu negocio crezca.</p>
                        <br>
                        <a class="btn grey darken-4" href="<?= base_url() ?>cuenta/empresa">Inicia sesión</a>
                    </div>
                    <div class="col l12 s12 center-align">
                        <br>
                        <p class="color-text">Si no tienes una cuenta, <a class="redirect" href="<?= base_url() ?>registro/proveedor">haz click aquí.</a></p>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php $this->view("japy/prueba/footer") ?>
</html>