<?php
$this->view("japy/prueba/header");
?>
    <style>
        #header {
            position: relative !important;
        }
        .registro-main {
            background-image: url(<?php echo base_url() ?>/dist/img/brideadvisor/registroNoviaNew.png);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: top;
            position: relative;
        }

        .registro-sec {
            top: 5%;
        }

        .logo {
            margin-bottom: 50px;
        }

        .logo img {
            height: 100px;
            width: 150px;
        }

        .formulario input {
            background-color: #FFF !important;
            color: black !important;
            box-shadow: 3px 3px 9px -3px grey !important;
            padding: 15px !important;
            border: none !important;
        }

        formulario input::placeholder {
            color: #696969;
        }

        .formulario select {
            margin-bottom: 15px;
            margin-top: 0;
            height: 50px;
            background-color: #FFF !important;
            color: black !important;
            box-shadow: 3px 3px 9px -3px grey !important;
            padding: 15px !important;
            border: none !important;
        }

        .basic-style {
            margin-top: 20px !important;
            margin-bottom: 0 !important;
        }

        .terminos {
            font-size: 12px;
            margin: 50px 0;
        }

        .swal-title {
            margin: 0 !important;
        }
        .row {
            margin-bottom: 0px !important;
        }
        .footer {
            margin-top: 0px !important;
        }
        @media only screen and (max-width: 321px) {
            .registro-main {
                background: url(<?php echo base_url() ?>/dist/img/textura_footer2.png) !important;
                height: 160% !important;
            }

            .registro-sec {
                top: 6%;
            }

            .logo {
                margin: 0;
            }
        }

        @media only screen and (min-width: 322px) and (max-width: 425px) {
            .registro-main {
                background: url(<?php echo base_url() ?>/dist/img/textura_footer2.png) !important;
            }

            .terminos {
                margin: 0;
            }

            .registro-sec {
                top: 10%;
            }

            .logo {
                margin: 0;
            }
        }
        .logo {
                margin-bottom: unset !important;
            }
    </style>
    <div class="registro-main max-content row">
        <div class="col s10 offset-s1 l3 offset-l8 registro-sec" style="">
            <div class=" row">
                <!-- <div class="col s12 logo center-align">
                    <img src="<?php echo base_url() ?>/dist/img/japy_nobg_noslogan.png" alt="">
                </div> -->
                <div class="col s12 center-align">
                    <br><br>
                    <h4 style="color: #767676; font-weight: bold;">REGÍSTRATE</h4>
                </div>
                <form action="<?php echo base_url()."/registro/registroNovia" ?>" method="POST" id="registro"
                      data-parsley-validate="true">
                    <input id="come_from" type="hidden" name="come_from" value="japy">
                    <div class="col s12 formulario ">
                        <div class="row">
                            <div class="col s12 ">
                                <input id="name_user" class=" basic-style" type="text" name="nombre" data-parsley-length="[2,30]"
                                       placeholder="Nombre(s)" required="" style="width: 90%; height: 20px;">
                            </div>
                            <div class="col s12 ">
                                <input id="lastname_user" class=" basic-style" type="text" name="last_nombre" data-parsley-length="[2,30]"
                                       placeholder="Apellidos" required="" style="width: 90%; height: 20px;">
                            </div>
                            <div class="col s12 ">
                                <select id="gender_register" class="basic-style browser-default basic-style" name="genero" required=""
                                        style="margin-top: 0px;-webkit-appearance: none;">
                                    <option value="" disabled selected>Novia/Novio</option>
                                    <option value="1">Novio</option>
                                    <option value="2">Novia</option>
                                </select>
                            </div>
                            <div class="col s12 ">
                                <input id="date" class=" basic-style" type="text" name="date" onfocus="(this.type='date')"
                                       placeholder="Fecha de la boda" required="" style="width: 90%; height: 20px;">
                            </div>
                            <div class="col s12  ">
                                <!-- DIVIDER -->
                                <input class=" basic-style" type="text" id="phone" name="phone" onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                       placeholder="Teléfono" required="" style="width: 90%; height: 20px;">
                            </div>
                            <div class="col s12  ">
                                <!-- DIVIDER -->
                                <input class=" basic-style" type="email" id="correo" name="correo"
                                       placeholder="Correo" data-parsley-type="email" required="" value="<?php if(!empty($_GET["mail"])) {echo $_GET["mail"];} ?>" style="width: 90%; height: 20px;">
                            </div>

                            <div class="col s12  bt-flabels__wrapper">
                                <input class="basic-style" id="password" type="password" name="contrasena"
                                       placeholder="Contrase&ntilde;a" data-parsley-length="[2,30]" required="" style="width: 90%; height: 20px;">
                                <!-- DIVIDER -->
                            </div>
                            <div class="col s12  bt-flabels__wrapper">
                                <input class="basic-style" id="password_2" type="password" name="contrasena_2"
                                       placeholder="Confirmar contrase&ntilde;a" data-parsley-length="[2,30]" required="" style="width: 90%; height: 20px;">
                                <!-- DIVIDER -->
                            </div>
                            <div class="col s12 center-align">
                                <br>
                                <button class="btn" id="btn_enviar" type="submit"
                                        style="margin-top: 10px; background: #F8DADF; color: #9B9B9B; font-weight: bold;">
                                    REGISTRARSE
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col s12 facebook-login center-align">
                    <!--<fb:login-button
                            size="medium"
                            scope="public_profile,email"
                            onlogin="checkLoginState($('#gender_register').val());">
                        Continuar con facebook
                    </fb:login-button>-->
                    <br>
                    <div class="fb-login-button" 
                    data-max-rows="1" 
                    data-size="medium" 
                    data-button-type="login_with" 
                    data-show-faces="false" 
                    data-auto-logout-link="false" 
                    data-use-continue-as="false"
                    scope="public_profile,email"
                    onlogin="checkLoginState($('#gender_register').val(),$('#come_from').val());">
                        Regístrate con Facebook
                    </div>
                </div>
                <div class="col s12 terminos center-align">
                    <p>Al dar clic en continuar estás aceptando los terminos , condiciones y nuestras políticas de
                        privacidad </p>
                    <a style="color: #9B9B9B;" href="<?php echo base_url()."terminos-japy.pdf" ?>" target="_blank">
                        <p style="color: #9B9B9B;">Consulta en políticas de privacidad</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <input id="base-url" type="hidden" value="<?php echo base_url() ?>">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   
    <!-- FACEBOOK SDK -->
    <script type="text/javascript" src="<?php echo base_url() . "dist/js/facebook/login.js" ?>"></script>
    <script>
    var STATE = "";
        $(document).ready(function() {

            $( "#name_user" ).focus();

            $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    if(location){
                        if(location.state){
                            STATE = location.state;
                            $("#come_from").val(STATE);
                        }
                    }
                },
            });
            
            const $registerForm = $('#registro').parsley();

            $('#btn_enviar').on('click', function(e) {

                e.preventDefault();

                $registerForm.validate();
                
                if ($registerForm.isValid()) {
                    if($('#password').val()==$('#password_2').val()) {
                        $.ajax({
                            url: '<?php echo base_url()."registro/checkUsuario" ?>',
                            method: 'post',
                            data: {
                                'correo': $('#correo').val(),
                            },
                            success: function(res) {
                                if (res.data == true) {
                                    swal('Error', 'Correo registrado anteriormente.', 'error');
                                    return false;
                                }

                                $('#registro').submit();
                            },
                            error: function() {
                                swal('Error', 'Lo sentimos ocurrio un error', 'error');
                            },
                        });
                    } else {
                        swal('Error', 'Las contraseñas no coinciden', 'error');
                        return false;
                    }
                }
            });
        });
    </script>
<?php
// $this->load->view("general/footer", $this->_ci_cached_vars);
$this->load->view("japy/prueba/footer");