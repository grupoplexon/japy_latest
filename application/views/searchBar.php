<?php
$search = isset($_GET["buscar"]) ? htmlspecialchars($_GET["buscar"]) : "";
$state  = isset($_GET["state"]) ? htmlspecialchars($_GET["state"]) : "";
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>



    //      $( function() {
    //         $( "#btn-search" ).autocomplete({
    //         source: categoria2
    //     } );
    //   });

</script>
<style>
    input.select-dropdown {
        background-color: white !important;
        border: 1px solid #ccc !important;
        padding-left: 20px !important;
        color: #ccc;
    }

    span.caret {
        color: #00bbcd !important;
        right: 19px !important;
        top: 5px !important;
        font-size: 15px !important;
        z-index: 5;
    }
</style>
<form class="row" method="GET"
      action="<?php echo base_url().("proveedores") ?>"
      style="padding: 10px 0;margin: 0">
    <div class="col s12 m5" style="margin:0;">
        <div style="display: flex;align-items: center;margin:0;">
            <input name="buscar" list="categoria"  id="btn-search" class="form-control dropdown-button" style="margin-top: 10px;border: #c1c1c1 solid 1px !important; border-radius: unset !important;" value="<?php echo $search ?>" placeholder="¿Qué estas buscando?"/>
            <datalist id="categoria">

            
            <?php // foreach (Category::with('subcategories')->orderBy('orden')->whereNull('parent_id')->get() as $key => $category): ?>
            <?php foreach (categories() as $category1) : ?>
            <?php foreach ($category1->subcategories as $subcategory) : ?>
                <option value="<?php echo $subcategory->name ?>" > <?php echo $subcategory->name ?> </option>
            <?php endforeach; ?>
            <?php endforeach; ?>
            
            <?php foreach ($providers as $pro) : ?>
                <option value="<?php echo $pro->nombre ?>">
            <?php endforeach; ?>
            
            </datalist>
        </div>
        <div>
            <?php if ( ! empty($search)) : ?>
                <span>Resultados de busqueda para: <?php echo $search ?></span>
            <?php endif ?>
        </div>
    </div>
    <div class="col s12 m5" style="display: flex;align-items: flex-start;margin: 0;">
        <div class="input-field col s12" style="margin: 0;">
            <select name="estado" id="stateId" style="width: 100%; margin-top:10px; margin-bottom: 0px; border: #c1c1c1 solid 1px !important; border-radius: unset !important;">
            </select>
        </div>
    </div>
    <div class="col s12 m2" style="justify-content: center;padding:10px 0;">
    <button type="submit" class="btn " style="font-size: small;height: 45px; width: 115px;background-color: #72d6e0;border: none;font-weight: bold; border-radius: unset !important;" >BUSCAR</button>
    </div>
</form>
<input type="hidden" id="url" value="<?php echo base_url() ?>">
<script>

    $(document).ready(function() {
        let currentState = '';
        let search = '';
        let url = '';
        const states = [
            {'name': 'Todos'},
            {'name': 'Jalisco'},
            {'name': 'Queretaro'},
            {'name': 'Puebla'},
            {'name': 'Sinaloa'},
            {'name': 'Estado de Mexico'},
            {'name': 'Ciudad de Mexico'},
        ];
        const baseURL = $('#url').val();
        url = new URL(window.location.href);
        currentState = (url.searchParams.get('estado') === null) ? '' : url.searchParams.get('estado');
        search = (url.searchParams.get('buscar') === null) ? '' : url.searchParams.get('buscar');
        let stateSelected = 'Todos';

        $('#stateId').on('change', function() {
            stateSelected = $('#stateId').val();
            if (stateSelected != null) {
                updateMenuUrl(baseURL, search, stateSelected);
            }
        });

        $('#btn-search').on('change', function() {
            search = $('#btn-search').val();
            updateMenuUrl(baseURL, search, stateSelected);
        });

        getUserLocation();

        $('#stateId').trigger('change');

        function updateMenuUrl(baseURL, search, state) {
            search = search == null ? '' : search;
            state = state == null ? '' : state;
            $('#dropdown-proveedores .sub-menu-item').each(function() {
                let categoryURL = $(this).attr('href');
                let categoryDirty = categoryURL.substr(categoryURL.lastIndexOf('/') + 1);
                let clearCategory = categoryDirty.split('?');
                $(this).attr('href', `${baseURL}${clearCategory[0]}?buscar=${search}&estado=${state}`);
            });

            $('#dropdown-proveedores .sub-sub-menu-item').each(function() {
                let categoryURL = $(this).attr('data-href');
                let categoryDirty = categoryURL.substr(categoryURL.lastIndexOf('/') + 1);
                let clearCategory = categoryDirty.split('?');
                $(this).attr('href', `${baseURL}${clearCategory[0]}?buscar=${search}&estado=${state}`);
            });
        }

        function updateSelect() {
            $('select[name="estado"]').empty();

            if (currentState != '') {
                $('select[name="estado"]').append($('<option>', {
                    value: currentState,
                    text: currentState,
                }));
            }

            for (let state of states) {
                if (currentState !== state.name) {
                    $('select[name="estado"]').append('<option value=\'' + state.name + '\'>' + state.name + '</option>');
                }
            }

            $('select[name="estado"]').material_select();
        }

        function updateUrl() {
            if (url.pathname != '/') {
                window.location.href = window.location.href + '?buscar=' + search + '&estado=' + currentState;
            }
        }

        function getUserLocation() {
            $.ajax({
                url: 'https://geoip-db.com/jsonp/',
                jsonpCallback: 'callback',
                dataType: 'jsonp',
                timeout: 8000,
                success: function(location) {
                    if (currentState == '') {
                        if (!states.includes(location.state) && location.state != 'Not Found') {
                            currentState = location.state;
                        }
                        else if (location.state == 'Mexico City') {
                            currentState = 'Ciudad de Mexico';
                        }
                        else if (location.state == 'Estado de Mexico') {
                            currentState = 'Ciudad de Mexico';
                        }
                        else {
                            currentState = 'Jalisco';
                        }
                        updateUrl();
                    }

                    updateSelect();
                    updateMenuUrl(baseURL, search, currentState);
                },
            });
            return '';
        }
    });

</script>

