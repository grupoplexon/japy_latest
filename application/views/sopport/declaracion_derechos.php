<!DOCTYPE html>
<head>
    <?php $this->view('principal/header'); ?>
    <title>Pol&iacute;tica de Datos</title>
    <style>
        .bs-callout-warning {
            border-left-color: #aa6708;
        }
        .bs-callout {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }
        .bs-callout-info {
            border-left-color: #1b809e;
        }
        .bs-callout-danger {
            border-left-color: #ce4844;
        }
        .bs-callout-warning {
            border-left-color: #aa6708;
        }
        .bs-callout-info {
            border-left-color: #1b809e;
        }

    </style>
</head>
<body>
    <div class="body-container">
        <div class="row">
            <div class="col s12 m12">
                <div class="card-panel z-depth-1">
                    <div class="row">
                        <h4>Pol&iacute;tica de Datos</h4>
                        <div class="divider"></div>
                        <div class="col l9 col offset-l1 col m10 col offset-m1">
                            <div class="post-preview">
                                <div style="text-align:justify;">Esta Declaraci&oacute;n de derechos y responsabilidades contiene las condiciones del servicio que rigen nuestra relaci&oacute;n con los usuarios y con todos aquellos que interactuar con la aplicaci&oacute;n  japybodas.com. Al usar los Servicios de  japybodas.com o al acceder a ellos, muestras tu conformidad con esta Declaraci&oacute;n, que se actualiza peri&oacute;dicamente seg&uacute;n se estipula en la secci&oacute;n 9 m&aacute;s adelante.</div>
                                <ol>
                                    <li><kbd><kbd>Privacidad.</kbd></kbd></li>
                                    <div style="text-align:justify;">Tu privacidad es muy importante para nosotros. Diseñamos nuestra Pol&iacute;tica de datos para ayudarte a comprender como recopilamos y usamos tu contenido e informaci&oacute;n. Te recomendamos que leas nuestra <a href="http://201.168.207.133/fict/politica.html">Pol&iacute;tica de datos.</a></div>
                                    <li><kbd><kbd>Compartir el contenido y la informaci&oacute;n.</kbd></kbd></li> 
                                    <div style="text-align:justify;">Cuando utilizas inicio de sesi&oacute;n con una aplicaci&oacute;n de terceros, esta puede solicitarte permiso para acceder a tu contenido e informaci&oacute;n, y al contenido y a la informaci&oacute;n que otros compartieron contigo.  Exigimos que las aplicaciones respeten tu privacidad, y tu acuerdo con la aplicaci&oacute;n controlar&aacute; el modo en el que esta use, almacene y transfiera dicho contenido e informaci&oacute;n.</div>
                                    <li><kbd><kbd>Seguridad.</kbd></kbd></li>
                                    <div style="text-align:justify;">Hacemos todo lo posible para que japy.com sea un sitio seguro, pero no podemos garantizarlo. Necesitamos tu ayuda para que as&iacute; sea, lo que implica los siguientes compromisos de tu parte:</div>
                                    <ul style="text-align:justify;">
                                        <li>No solicitaras informaci&oacute;n de inicio de sesi&oacute;n ni acceder&aacute;s a una cuenta perteneciente a otro usuario.</li>
                                        <li>No publicaras contenido que contenga lenguaje que incite al odio, resulte intimatorio, sea pornogr&aacute;fico, incite a la violencia o contenga desnudos o violencia gr&aacute;fica o injustificada.</li>
                                        <li>No realizar&aacute;s ninguna acci&oacute;n que pudiera inhabilitar, sobrecargar o afectar al funcionamiento correcto de  japy.com o a su aspecto, como un ataque de denegaci&oacute;n de servicio o la alteraci&oacute;n de la presentaci&oacute;n de p&aacute;ginas u otras funciones de  japy.com.</li>
                                        <li>No facilitar&aacute;s ni fomentar&aacute;s el incumplimiento de esta Declaraci&oacute;n ni de nuestras pol&iacute;ticas.</li>
                                    </ul>
                                    <li><kbd><kbd>Seguridad de la cuenta y registro</kbd></kbd></li>
                                    <div style="text-align:justify;">Los usuarios de japy.com proporcionan sus nombres y datos reales, y necesitamos tu colaboraci&oacute;n para que siga siendo as&iacute;. Estos son algunos de los compromisos que aceptas en relaci&oacute;n con el registro y el mantenimiento de la seguridad de tu cuenta:
                                        <ul>
                                            <li>No proporcionar&aacute;s informaci&oacute;n personal falsa en  japy.com, ni crear&aacute;s una cuenta para otras personas sin su autorizaci&oacute;n.
                                            </li><li>No crear&aacute;s m&aacute;s de una cuenta personal.</li>
                                            <li>Si inhabilitamos tu cuenta, no crear&aacute;s otra sin nuestro permiso.</li>
                                            <li>No compartir&aacute;s tu contraseña (o, en el caso de los desarrolladores, tu clave secreta), no dejar&aacute;s que otra persona acceda a tu cuenta, ni har&aacute;s nada que pueda poner en peligro la seguridad de tu cuenta.</li>
                                            <li>No transferir&aacute;s la cuenta (incluida cualquier p&aacute;gina o aplicaci&oacute;n que administres) a nadie sin nuestro consentimiento previo por escrito.</li>
                                        </ul>    
                                        <li><kbd><kbd>Protecci&oacute;n de los derechos de otras personas.</kbd></kbd></li>
                                        <div style="text-align:justify;">Respetamos los derechos de otras personas y esperamos que t&uacute; hagas lo mismo.
                                            <ul>
                                                <li>No publicar&aacute;s contenido ni realizar&aacute;s ninguna acci&oacute;n en  japy.com que infrinja o vulnere los derechos de terceros o que vulnere la ley de alg&uacute;n modo.</li>
                                            </ul>
                                            <li><kbd><kbd>Dispositivos m&oacute;viles y de otros tipos.</kbd></kbd></li>
                                            <ul>
                                                <li>Actualmente ofrecemos nuestros servicios para dispositivos m&oacute;viles de forma gratuita, pero ten en cuenta que se aplicar&aacute;n las tarifas normales de tu operador, por ejemplo, para mensajes de texto y datos.</li>
                                            </ul>
                                            <li><kbd><kbd>Acerca de los anuncios u otro contenido comercial publicado.</kbd></kbd></li>
                                            <div style="text-align:justify;">Nuestro objetivo es publicar anuncios y otro contenido comercial o patrocinado que sea valioso para nuestros usuarios y anunciantes. Para ayudarnos a lograrlo, aceptas lo siguiente:</div>
                                            <ul>
                                                <li>Nos concedes permiso para usar tu nombre, foto del perfil, contenido e informaci&oacute;n en relaci&oacute;n con contenido comercial, patrocinado o asociado.</li>
                                                <li>No proporcionamos tu contenido o informaci&oacute;n a anunciantes sin tu consentimiento.</li>
                                            </ul>
                                            <li><kbd><kbd>Disposiciones especiales aplicables a anunciantes.</kbd></kbd></li>
                                            <ul>
                                                <li>Si utilizas nuestras interfaces de creaci&oacute;n de anuncios de autoservicio para crear, presentar y/o entregar anuncios u otra actividad o contenido de car&aacute;cter comercial o patrocinado, aceptas nuestras Condiciones de publicidad de autoservicio. Asimismo, dichos anuncios u otra actividad o contenido de car&aacute;cter comercial o patrocinado publicados en  japy.com o en nuestra red de editores deben cumplir nuestras Pol&iacute;ticas de publicidad. </li>
                                            </ul>
                                            <li><kbd><kbd>Enmiendas</kbd></kbd></li>
                                            <ul>
                                                <li>Te notificaremos antes de realizar cambios en estas condiciones y te daremos la oportunidad de revisar y comentar las condiciones modificadas antes de seguir usando nuestros Servicios.</li>
                                                <li>Si realizamos cambios en las pol&iacute;ticas, normas u otras condiciones a las que hace referencia esta Declaraci&oacute;n o que est&aacute;n incorporadas en ella.</li>
                                                <li>Tu uso continuado de los Servicios de  japy.com despu&eacute;s de recibir la notificaci&oacute;n sobre los cambios en nuestras condiciones, pol&iacute;ticas o normas supone la aceptaci&oacute;n de las enmiendas.</li>
                                            </ul>
                                            <li><kbd><kbd>Terminaci&oacute;n</kbd></kbd></li>
                                            <div style="text-align:justify;">Si infringes la esencia o el esp&iacute;ritu de esta Declaraci&oacute;n, creas riesgos de cualquier tipo para  japy.com o nos expones a posibles responsabilidades jur&iacute;dicas, podr&iacute;amos impedirte el acceso a  japy.com total o parcialmente.<p></p>
                                                <li><kbd><kbd>Conflictos</kbd></kbd></li>
                                                <div style="text-align:justify;">INTENTAMOS MANTENER APP japy.com EN FUNCIONAMIENTO, SIN ERRORES Y SEGURO, PERO LO UTILIZAS BAJO TU PROPIA RESPONSABILIDAD. PROPORCIONAMOS APP japy.com TAL CUAL, SIN GARANT&Iacute;A ALGUNA EXPRESA O IMPL&Iacute;CITA, INCLUIDAS, ENTRE OTRAS, LAS GARANT&Iacute;AS DE COMERCIABILIDAD, ADECUACI&Oacute;N A UN FIN PARTICULAR Y NO INCUMPLIMIENTO. NO GARANTIZAMOS QUE APP japy.com SEA SIEMPRE SEGURO O EST&Eacute; LIBRE DE ERRORES, NI QUE FUNCIONE SIEMPRE SIN INTERRUPCIONES, RETRASOS O IMPERFECCIONES. APP japy.com NO SE RESPONSABILIZA DE LAS ACCIONES, EL CONTENIDO, LA INFORMACI&Oacute;N O LOS DATOS DE TERCEROS, Y POR LA PRESENTE NOS DISPENSAS A NOSOTROS, NUESTROS DIRECTIVOS, EMPLEADOS Y AGENTES DE CUALQUIER DEMANDA O DAÑOS, CONOCIDOS O DESCONOCIDOS, DERIVADOS DE CUALQUIER DEMANDA QUE TENGAS INTERPUESTA CONTRA TALES TERCEROS O DE ALG&Uacute;N MODO RELACIONADOS CON ESTA. SI ERES RESIDENTE DE CALIFORNIA, RENUNCIAS A LOS DERECHOS DE LA SECCI&Oacute;N 1542 DEL C&Oacute;DIGO CIVIL DE CALIFORNIA, QUE ESTIPULA LO SIGUIENTE: UNA RENUNCIA GENERAL NO INCLUYE LAS DEMANDAS QUE EL ACREEDOR DESCONOCE O NO SOSPECHA QUE EXISTEN EN SU FAVOR EN EL MOMENTO DE LA EJECUCI&Oacute;N DE LA RENUNCIA, LA CUAL, SI FUERA CONOCIDA POR &Eacute;L, DEBER&Aacute; HABER AFECTADO MATERIALMENTE A SU RELACI&Oacute;N CON EL DEUDOR. NO SEREMOS RESPONSABLES DE NINGUNA P&Eacute;RDIDA DE BENEFICIOS, AS&Iacute; COMO DE OTROS DAÑOS RESULTANTES, ESPECIALES, INDIRECTOS O INCIDENTALES DERIVADOS DE ESTA DECLARACI&Oacute;N O DE APP japy.com O RELACIONADOS CON ESTOS, INCLUSO EN EL CASO DE QUE SE HAYA AVISADO DE LA POSIBILIDAD DE QUE SE PRODUZCAN DICHOS DAÑOS. NUESTRA RESPONSABILIDAD CONJUNTA DERIVADA DE LA PRESENTE DECLARACI&Oacute;N O DE APP japy.com NO PODR&Aacute; SOBREPASAR EL VALOR DE CIEN D&Oacute;LARES (100 USD) O EL IMPORTE QUE NOS HAYAS PAGADO EN LOS &Uacute;LTIMOS DOCE MESES, LO QUE SEA M&Aacute;S ALTO. LAS LEYES APLICABLES PODR&Iacute;AN NO PERMITIR LA LIMITACI&Oacute;N O EXCLUSI&Oacute;N DE LA RESPONSABILIDAD POR DAÑOS INCIDENTALES O DERIVADOS, POR LO QUE LA LIMITACI&Oacute;N O EXCLUSI&Oacute;N ANTERIOR PODR&Iacute;A NO SER APLICABLE EN TU CASO. EN TALES CASOS, LA RESPONSABILIDAD DE APP japy.com SE LIMITAR&Aacute; AL GRADO M&Aacute;XIMO PERMITIDO POR LA LEY APLICABLE.</div>
                                            </div></div></div></ol>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php $this->view('principal/footer'); ?>


