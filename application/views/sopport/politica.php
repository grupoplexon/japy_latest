<!DOCTYPE html>
<head>
    <?php $this->view('principal/header'); ?>
    <title>Pol&iacute;tica de Datos</title>
    <style>
        .bs-callout-warning {
            border-left-color: #aa6708;
        }
        .bs-callout {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }
        .bs-callout-info {
            border-left-color: #1b809e;
        }
        .bs-callout-danger {
            border-left-color: #ce4844;
        }
        .bs-callout-warning {
            border-left-color: #aa6708;
        }
        .bs-callout-info {
            border-left-color: #1b809e;
        }

    </style>
</head>
<body>
    <div class="body-container">
        <div class="row">
            <div class="col s12 m12">
                <div class="card-panel z-depth-1">
                    <div class="row">
                        <h4>Pol&iacute;tica de Datos</h4>
                        <div class="divider"></div>
                        <div class="col l9 col offset-l1 col m10 col offset-m1">
                            <div class="post-preview">
                                <div style="text-align:justify;">
                                    <center><h5>¿Qu&eacute; tipo de informaci&oacute;n recopilamos?</h5></center>
                                    Se recopilan diferentes tipos de informaci&oacute;n relacionada contigo en funci&oacute;n de los Servicios que uses.<br>
                                    <b>Tus acciones y la informaci&oacute;n que proporcionas.</b>
                                    <div class="bs-callout bs-callout-warning"> 
                                        Recopilamos el contenido y otros datos que proporcionas cuando usas nuestros Servicios, por ejemplo, al abrir una cuenta. 
                                    </div>
                                    <b>Informaci&oacute;n sobre el dispositivo.</b>
                                    <div class="bs-callout bs-callout-info">
                                        Recopilamos informaci&oacute;n acerca de las computadoras, los tel&eacute;fonos u otros dispositivos en los que instalas o desde los que accedes a nuestros Servicios, as&iacute; como los datos generados por dichos dispositivos, en funci&oacute;n de los permisos que les hayas concedido. Podemos asociar la informaci&oacute;n que recopilamos de tus diferentes dispositivos, lo que nos ayuda a ofrecer Servicios coherentes en todos ellos. Estos son algunos ejemplos de la informaci&oacute;n de dispositivos que recopilamos: 
                                        <ul>
                                            <li>Atributos, como el sistema operativo, la versi&oacute;n de hardware, la configuraci&oacute;n del dispositivo, los nombres y tipos de software y de archivos, la carga de la bater&iacute;a, la intensidad de la señal y los identificadores de dispositivos.</li>
                                            <li>Ubicaciones del dispositivo, incluida la posici&oacute;n geogr&aacute;fica espec&iacute;fica obtenida a trav&eacute;s de señales de GPS, Bluetooth o wifi.</li>
                                            <li>Informaci&oacute;n sobre la conexi&oacute;n, como el nombre del operador de telefon&iacute;a celular o proveedor de servicios de internet, el tipo de navegador, el idioma y la zona horaria, el n&uacute;mero de celular y la direcci&oacute;n IP.</li>
                                        </ul>
                                    </div>  
                                    <center><h5>¿C&oacute;mo usamos esta informaci&oacute;n?</h5></center>  
                                    Nos apasiona crear experiencias atractivas y personalizadas para las personas. Usamos toda la informaci&oacute;n que tenemos para poder ofrecer nuestros Servicios y mantenerlos. El procedimiento es el siguiente:<br>
                                    <b>Proporcionar, mejorar y desarrollar los Servicios.</b>
                                    <div class="bs-callout bs-callout-danger">
                                        Lo que nos permite ofrecerte nuestros Servicios, personalizar el contenido y proporcionarte sugerencias es el uso que hacemos de esta informaci&oacute;n. Nos permite comprender c&oacute;mo usas nuestros Servicios e interact&uacute;as con ellos y las personas o las cosas a las que est&aacute;s conectado y en las que te interesas, tanto dentro de nuestros Servicios como fuera de ellos. 
                                        Realizamos encuestas e investigaciones, probamos funciones en fase de desarrollo y analizamos la informaci&oacute;n de la que disponemos para evaluar y mejorar los productos y servicios, desarrollar nuevos productos o funciones, y llevar a cabo auditor&iacute;as y actividades de soluci&oacute;n de problemas.
                                    </div>
                                    <b>Comunicarnos contigo.</b>
                                    <div class="bs-callout bs-callout-warning"> 
                                        Usamos tu informaci&oacute;n para enviarte mensajes de marketing, darte a conocer nuestros Servicios e informarte acerca de nuestras pol&iacute;ticas y condiciones. Tambi&eacute;n usamos tu informaci&oacute;n para responderte cuando te pones en contacto con nosotros.
                                    </div>
                                    <b>Mostrar y medir anuncios y servicios.</b>
                                    <div class="bs-callout bs-callout-info">
                                        Usamos la informaci&oacute;n que tenemos para mejorar nuestros sistemas de publicidad y de medici&oacute;n con el fin de mostrarte anuncios relevantes tanto dentro de nuestros Servicios como fuera de ellos, y para medir la eficacia y el alcance de los anuncios y los servicios. Obt&eacute;n m&aacute;s informaci&oacute;n sobre c&oacute;mo anunciarte en nuestros Servicios y c&oacute;mo controlar el modo en que se usa tu informaci&oacute;n para personalizar los anuncios que ves.
                                    </div>
                                </div>

                            </div>
                            <hr>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php $this->view('principal/footer'); ?>


