<?php
$this->load->view("general/head");
?>
<style>
    .content {
        background-image: url('<?php echo(base_url()."dist/img/textura_footer2.jpg") ?>');
        background-repeat: repeat;
    }

    .valign-wrapper {
        justify-content: center !important;
    }
</style>
<div class="max-content content valign-wrapper">
    <section class="row" style="margin: 0">
        <div class="col s12 m8 offset-m2">
            <div class="card-panel center-align">
                <?php if (isset($error)) : ?>
                    <style>
                        .card-panel {
                            width: 360px;
                        }
                    </style>
                    <i class="fa fa-exclamation-circle fa-5x red-text darken-5"
                       style="border-radius: 50%; border: 4px solid;padding: 13px;"></i>
                    <p style="font-size: 16px">
                        <?php echo $error; ?>
                    </p>
                    <a href="<?php echo base_url()."soporte/contrasena" ?>" class="btn dorado-2 waves-effect"
                       style="width: 250px;">
                        Regresar <i class="fa fa-chevron-right"></i>
                    </a>
                <?php else: ?>
                    <i class="fa fa-check fa-5x green-text darken-5"
                       style="border-radius: 50%; border: 4px solid;padding: 13px;"></i>
                    <p style="font-size: 16px">
                        Te enviamos un correo para que puedas recuperar tu contraseña
                    </p>
                    <p>
                        Accede a tu cuenta ingresando tu usuario y contraseña nueva desde el portal
                        <a class="dorado-2-text" href="<?php echo base_url() ?>">japybodas.com</a>
                        , te recomendamos cambiar tu contraseña
                    </p>
                    <a href="<?php echo base_url() ?>cuenta/" class="btn dorado-2 waves-effect"
                       style="width: 250px;">
                        iniciar sesi&oacute;n <i class="fa fa-chevron-right"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function() {
        let screenHeight = screen.height;
        let menuHeight = $('.nav-style').height();
        let footerHeight = $('footer').height();
        let moduleHeight = 0;

        moduleHeight = screenHeight - (menuHeight + footerHeight);

        $('.content').height(moduleHeight);

        console.log(screenHeight, menuHeight, footerHeight, moduleHeight);
    });
</script>
<?php
$this->load->view("general/footer", $this->_ci_cached_vars);
$this->load->view("general/newfooter");
?>
