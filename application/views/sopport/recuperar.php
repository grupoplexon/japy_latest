<?php
$this->load->view("japy/prueba/header");
?>
<style>
    .content{
        background-image: url('<?php echo(base_url()."dist/img/texturaBa.jpg") ?>');
        background-repeat: repeat;
    }
    .valign-wrapper{
        justify-content: center!important;
    }
    input::placeholder{
        color: grey;
    }
</style>
<div class="max-content content valign-wrapper">
    <section class="row" style="margin: 0">
        <div class="col s12">
            <div class="card-panel center-align">
                <form method="POST" id="form-recuperar" action="<?php echo base_url() . "soporte/contrasena" ?>" data-parsley-validate="true">
                    <div class="row" >
                        <p>
                            Ingresa tu correo electr&oacute;nico y te enviaremos los pasos a seguir para recuperar
                            tu contrase&ntilde;a
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s12 ">
                                <input name="correo" class=" validate[custom[email]]"  data-parsley-type="email" required="" placeholder="Correo electrónico">
                            </div>
                        </div>
                        <button class="btn dorado-2 white-text" type="submit">
                            Enviar
                        </button>
                        <?php $this->view("register/mensajes") ?>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        let screenHeight =  screen.height;
        let menuHeight   =  $('.nav-style').height();
        let footerHeight =  $('footer').height();
        let moduleHeight = 0;

        moduleHeight = screenHeight - (menuHeight + footerHeight);
        console.log(moduleHeight);
        $('.content').height(moduleHeight);

        $('#form-recuperar').validationEngine();
    });
</script>
<?php
//$this->load->view("general/footer", $this->_ci_cached_vars);
$this->load->view("japy/prueba/footer");
?>
