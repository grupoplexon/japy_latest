<html>
<head>
    <?php $this->view("register/header");
    $this->view("general/newheader");?>
    <style>
        .btn{
            width: initial!important;
        }
    </style>
</head>
<body>
<?php $this->view("general/menu") ?>
<div class="body-container">
    <div class="hide-on-small-only">
        <div style="margin:10%;"></div>
    </div>
    <h5>Recuperaci&oacute;n de contraseña</h5>
    <div class="divider"></div>
    <div>
        <div class="card-panel valign-wrapper">
            <div class="valign col s12 center" style="width: 100%">
                <?php if (isset($error)) : ?>
                    <i class="fa fa-exclamation-circle fa-5x red-text darken-5"
                       style="border-radius: 50%; border: 4px solid;padding: 13px;"></i>
                    <p style="font-size: 16px">
                        <?php echo $error; ?>
                    </p>
                    <a href="<?php echo base_url() ?>soporte/contrasena" class="btn dorado-2 waves-effect"
                       style="width: 250px;float: right ">
                        Regresar <i class="fa fa-chevron-right"></i>
                    </a>
                <?php else: ?>
                    <i class="fa fa-check fa-5x green-text darken-5"
                       style="border-radius: 50%; border: 4px solid;padding: 13px;"></i>
                    <p style="font-size: 16px">
                        Te enviamos un correo para que puedas recuperar tu contraseña
                    </p>
                    <p>
                        Accede a tu cuenta ingresando tu usuario y contraseña nueva desde el portal
                        <a class="dorado-2-text" href="<?php echo base_url() ?>">japybodas.com</a>
                        , te recomendamos cambiar tu contraseña
                    </p>
                    <a href="<?php echo base_url() ?>cuenta/" class="btn dorado-2 waves-effect"
                       style="width: 250px;float: right ">
                        iniciar sesi&oacute;n <i class="fa fa-chevron-right"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php $this->view("principal/footer");
      $this->view("register/footer");
      ?>
</body>
</html>