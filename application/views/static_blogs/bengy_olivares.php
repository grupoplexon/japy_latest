<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Japy</title>
    <?php $this->view("japy/header"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
</head>
<style>
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.slides{
    height: calc(100vh - 85px) !important;
}
.slider .indicators {
    margin-bottom: -14vh;
    z-index: 9;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px){
    .corch {
        background-image: url('<?php echo base_url() ?>dist/img/blog/corchetes_mov.png') !important;
        background-repeat: no-repeat;
        /* background-position: center; */
        background-size:cover;
        height: unset !important;
    }
}


@font-face{
    font-family: MyFont;
    src:url('Gentleman1000.otf') format('opentype');
}

p {
    color: black;
    text-align: justify;
}

.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}

</style>
<body>
<div class="row">
    <!-- <div class="col s12 m12 l12" style="height: calc(100vh - 85px) !important;"> -->
    <div class="responsive-img col l12">
            <video src="<?php echo base_url() ?>dist/img/blog/Principal_bengy.mp4" width="100%" autoplay muted loop controls></video>
    </div>
    <div class="col l12">
        <div class="container center-align">
            <p style="font-size: 9vh; text-align: center;">Cinco minutos con Bengy Olivares de True Moments Films</p>
        </div>
    </div>
    <!-- </div> -->
    <div class="container" >
        <div class="col l12">
            <p>Bengy Olivares camina con seguridad por la calle Lerdo de Tejada, nos sentamos a tomar un café donde realizamos la entrevista. Su actitud es relajada y transmite confianza en quien lo saluda, y en quienes lo dejan ser el testigo de un día tan importante para las parejas como el día de su boda;  Su objetivo es transmitir emociones a través de videos artísticos en True Moments, e impregnar de su estilo como fotógrafo en su cuenta de Instagram que cuenta con más de 39 mil seguidores.</p>
            <hr style="width: 40%; border: solid 1px; color: #72D6E0;">
            <p>El éxito que ha tenido Bengy se basa en la constancia, en tener claro lo que quiere, cómo va a lograrlo, y en haber escuchado a su padre, también fotógrafo de bodas que insistía en que fuera videografo nupcial y así hacer equipo, <strong style="color: black; font-style: italic;">“En ese entonces yo estaba en otras cosas, yo tenía mi agencia de publicidad bien puesta, me dedicaba al diseño, llegué a ir dos o tres veces a bodas con mi padre, pero no me llamaba la atención; hasta que hablé con él y le di una oportunidad, le dije enséñame algo para ver qué o cómo se hace. Me sentó un día, me puso los vídeos de Joe Simon, vi sus tráilers y en ese momento dije: si quiero hacer esto, conecté con muchas cosas que me gustaban que nunca había hecho y vi ahí una oportunidad muy buena”.</strong></p>
            <p>La primera boda que realizó fue junto a su padre, <strong style="color: black; font-style: italic;">“Un día me dijo tengo boda el sábado, y fui con una cámara que me prestaron; toda la boda la hice con cámara en mano sin nada más y pensé, ahí está la clave, en hacer las cosas sin pensarlas mucho, porque cuando pones trabas no acabas, y la verdad es que al final les entregamos el vídeo a los novios y estaban encantados con la boda”.</strong></p>
            <br>
        </div>
        <div class="col l12">
            <img src="<?php echo base_url() ?>dist/img/blog/Bengy04.jpg" style="width: 100%;">
        </div>
        <div class="col l12" style="margin-top: 50px;">
            <div class="col l6 s12">
                <p style="margin: 0;"><strong style="background: #72D6E0; font-weight: normal">Cuando Bengy iniciaba en el arte audiovisual, hacía vídeos corporativos,cobertura de eventos y más productos que le iban solicitando en el camino.</strong> Era muy genérico y eso era algo que no le agradaba del todo <strong style="color: black; font-style: italic;">“a mi me gustaban las bodas...le agarré mucho el gusto a hacerlas, editarlas, a todo el feeling que había alrededor, y dije es el momento de especializarse, era una parte importante, si quería que me ubicaran por algo en específico lo mejor era enfocarte y pegarle de lleno a eso”.</strong></p>
                <p><strong style="background: #72D6E0; font-weight: normal">Bengy comenzó hace 6 años con True Moments Films,</strong> lo que lo hace distinto de los demás videografos es que busca transmitir en sus vídeos toda esa vibra que hay alrededor de las bodas pero que a la vez va más allá de una emoción <strong style="color: black; font-style: italic;">“Un vídeo de True Moments tiene que transmitir sentimientos 100%, eso es lo que tienen mis vídeos y lo que nos hace diferentes, a veces edito el vídeo y digo quedó bien, pero cuando lo entrego o cuando se lo enseño a alguien y me dice yo lloro cuando veo tus videos, siento que tocan fibras sensibles y digo funciona, digo, esa es la esencia, yo prefiero mil veces que la gente llore a que le gente me diga qué está padre mi video, yo lo veo de esa forma, prefiero que mis videos se queden en la parte emotiva, sentimental, a que me digan, qué chidos efectos le pusiste”</strong></p>
                <div><img style="width: 100%" class="responsive-img" src="<?php echo base_url() ?>dist/img/blog/corchetes.png"></div>
            </div>
            <div class="col l6 s12">
                <img src="<?php echo base_url() ?>dist/img/blog/Bengy03.jpg" style="width: 100%;">
            </div>
        </div>
        <div style="margin-top: 40px;" class="col l12">
            <hr style="width: 40%; border: solid 1px; color: #72D6E0;"><br>
            <div class="col l6">
                <img src="<?php echo base_url() ?>dist/img/blog/Bengy01.jpg" style="width: 100%;">
            </div>
            <div class="col l6">
                <p>Entre emocionado y con una sonrisa que se le escapó, Bengy recordó una de las bodas que más le ha movido; la de Juan Pablo y Luis <strong style="color: black; font-style: italic;">“... fue una boda súper emotiva, de casi estar grabando a casi soltarme a llorar ahí, fueron una serie de factores que se estuvieron propiciando en el momento y creo que no era el único que estaba en ese rollo, la música, el vals, y literal se me puso la piel chinita”.</strong> </p>
                <p><strong style="background: #72D6E0; font-weight: normal; color: black;">Bengy es más que True Moments, creó todo un concepto en su cuenta personal de Instagram muy aparte de la marca que había creado, </strong><strong style="color: black; font-style: italic;">“lo quería hacer por un tipo de ejercicio visual, por un tema de constancia, quería darle un estilo a mi Instagram, quería que la gente se metiera a mi instagram y no les quedara duda de que soy fotógrafo… y al día de hoy si la gente pregunta por videos de bodas la gente dice háblale a Bengy, es muy rara la gente que dice ve con True Moments Films, me he hecho de un nombre, y eso me ha ayudado mucho”.</strong></p>
            </div>
        </div>
    </div>
</div>
</body>
<?php $this->view("japy/footer"); ?>
<script>
    $(document).ready(function(){

    });
</script>
</html>