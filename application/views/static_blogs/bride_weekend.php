<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Japy</title>
    <?php $this->view("japy/header"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta property="og:title" content="Planea tu boda en Bride Weekend"/>
    <meta property="og:image" content="<?php echo base_url() ?>dist/img/blog/bannerprincipal.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:url" content="<?php echo base_url()."bride-w" ?>"/>
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:site_name" content="Japy" />
    <!-- <meta property="og:description" content="Todo lo que nbición." /> -->
    <meta property="og:type" content="website" />
    
</head>
<style>
body{
    text-align: justify;
}
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.slides{
    height: calc(100vh - 85px) !important;
}
.slider .indicators {
    margin-bottom: -14vh;
    z-index: 9;
}
hr {
    border: 0;
    height: 3px;
    width: 50%;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(114, 214, 224, 0.75), rgba(0, 0, 0, 0));
}

@media only screen and  (min-width: 300px) and  (max-width: 500px){
    h1{
        font-size: 2rem !important;
    }
    .titulo{
        margin-top: -150px !important;
    }
    .video{
        height: 200px !important;
    }
}


@font-face{
    font-family: MyFont;
    src:url('Gentleman1000.otf') format('opentype');
}
.titulo{
    background: rgba(114, 214, 224, 0.60);;
    position: absolute;
    color: white;
    text-align: center;
    margin-top: -200px;
    width:100%;
    font-weight: bold;
}
.negrita{
    font-weight: bold;
}
.cursiva{
    font-style:italic;
    font-weight:normal !important;
}
.sombreado{
    background: #72D6E0;
    color: white;
    font-weight:normal !important;
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}
</style>
<body>
    <div class="responsive-img col l12">
            <img src="<?php echo base_url() ?>dist/img/blog/bannerprincipal.png" width="100%">
    </div>
    <div class="col l12">
        <div class="container center-align">
            <p style="font-size: 9vh; text-align: center;">Planea tu boda en Bride Weekend </p>
        </div>
    </div>
    <div class="valign-wrapper" style="width:100%;height:100%;position: relative;">
        <div class="valign" style="width:100%;">
            <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">

                    <p>
                    Si no sabes por dónde empezar con los preparativos para tu boda, llega para ti Bride Weekend, un concepto vanguardista que más que ser una expo, es un evento interactivo y divertido donde podrás encontrar tu vestido soñado dentro de los más de dos mil vestidos que estarán en exhibición e incluso podrás probártelos, además habrá cursos de maquillaje con profesionales, y todo en general será muy moderno, totalmente nuevo y hecho para la novia actual.
                    </p>
                    <p>
                    Bride Weekend, revoluciona las expos de bodas y surge para cubrir la necesidad que existía dentro de la industria nupcial, para que los proveedores encontraran a sus novias y las novias a sus proveedores en un mismo lugar, que va más allá de un solo fin de semana. En esta primera edición, las novias deberán registrarse en la App de Japy, uno de los patrocinadores oficiales, y a través de ella podrán interactuar con los expositores, saber sobre los eventos que se están llevando a cabo y recibir las notificaciones de sus tickets de venta en la misma aplicación.
                    </p>
                    <hr>
                </div>
                <!-- <div class="col s12 m12 l12">
                    <div class="responsive-img">
                        <img style="width: 100%;" src="<?php echo base_url() ?>dist/img/static_blogs/melissa1.jpeg"> 
                    </div>
                </div> -->
                <div class="col s12 m12 l12 alinear">
                    <div class="col s12 m6 l6">
                    <p>
                    Resalta también que se contará con espacios como Japy Spot donde junto con tu pareja prepararás sushi, serás parte de catas de vino y tequila, y también recibirás asesoría de imagen. Asimismo, pensaron en tu futuro esposo, y para ellos está instalada la Groom Zone donde podrán hacerse un buen corte en la barbería que estará establecida dentro de este espacio.  
                    </p>
                    <p>
                    Los organizadores de Bride Weekend esperan la asistencia de más de 5 mil personas, y manifiestan que son “especialistas en reducir el estrés y calmar a los novios en esos momentos de nervios durante la organización de su boda, logrando que las vivencias durante todo este proceso resulten divertidas, excitantes, con ilusión y sobre todo con un concepto que incluye las últimas tendencias de moda”.
                    </p>
                    <p>
                    El paso más importante ya lo diste, decir SÍ, ahora deja que los expertos se encarguen de hacer tu boda más fácil y encuentra todo lo que buscas en Bride Weekend este 23 y 24 de febrero en Expo Guadalajara, en un horario de 11:00A.M a 8:00P.M.
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:440px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/staticblogbw2.png"> 
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
<script>
$(document).ready(function(){

});

</script>
</body>
<?php $this->view("japy/footer"); ?>
</html>