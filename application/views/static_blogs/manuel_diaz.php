<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Japy</title>
    <?php $this->view("japy/header"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta property="og:title" content="Cinco Minutos con Manuel Díaz"/>
    <meta property="og:image" content="<?php echo base_url() ?>dist/img/blog/bannerprincipal.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:url" content="<?php echo base_url()."manuel_diaz" ?>"/>
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:site_name" content="Japy" />
    <!-- <meta property="og:description" content="Todo lo que nbición." /> -->
    <meta property="og:type" content="website" />
    
</head>
<style>
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.slides{
    height: calc(100vh - 85px) !important;
}
.slider .indicators {
    margin-bottom: -14vh;
    z-index: 9;
}
@media only screen and  (min-width: 300px) and  (max-width: 500px){
    .corch {
        background-image: url('<?php echo base_url() ?>dist/img/blog/corchetes_mov.png') !important;
        background-repeat: no-repeat;
        /* background-position: center; */
        background-size:cover;
        height: unset !important;
    }
}


@font-face{
    font-family: MyFont;
    src:url('Gentleman1000.otf') format('opentype');
}

p {
    color: black;
    text-align: justify;
}

.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}

</style>
<body>
<div class="row">
    <!-- <div class="col s12 m12 l12" style="height: calc(100vh - 85px) !important;"> -->
    <div class="responsive-img col l12">
            <!-- <video src="<?php echo base_url() ?>dist/img/blog/Principal_bengy.mp4" width="100%" autoplay muted loop controls></video> -->
            <img src="<?php echo base_url() ?>dist/img/blog/bannerprincipal.png" style="width: 100%">
    </div>
    <div class="col l12">
        <div class="container center-align">
            <p style="font-size: 4vh; text-align: center;">Bride Weekend es más que una expo, Manuel Díaz nos cuenta de qué va
            este innovador concepto, que reúne todo lo que se necesita para la
            celebración de tu boda en un solo lugar.</p>
        </div>
    </div>
    <!-- </div> -->
    <div class="container" >
        <div class="col l12">
            <p>Manuel Díaz, fundador y director de Grupo Plexon comienza una aventura más en su
            camino, creando un nuevo e innovador concepto llamado Bride Weekend en el que
            busca reinventar la industria nupcial en México al unir a proveedores y futuras parejas,
            para que disfruten no solo de un increíble fin de semana sino también de <i>“un evento
            muy interactivo, dinámico, festivo… una fiesta donde nos encontramos, donde las
            parejas pueden tocar un vestido, probar un pastel, y experimentar todo el glamour que
            se vive en las pasarelas. Es muy emocionante crear cosas y hacer algo dentro de lo
            que hemos trabajando durante 28 años, pero es todavía más gratificante encontrar los
            detalles para reinventarte”.</i></p>
        </div>
        <!-- <div class="col l12">
            <img src="<?php echo base_url() ?>dist/img/blog/Bengy04.jpg" style="width: 100%;">
        </div> -->
        <div class="col l12" style="margin-top: 50px;">
            <div class="col l7 s12 alinear">
                <p style="margin: 0; margin-right: 4vh;">El primer negocio que inició Manuel
                estuvo enfocado en la renta de
                limosinas, “de hecho fueron las
                primeras limosinas en Guadalajara, y
                nos enfocábamos al turismo, sin
                embargo, resultó que nuestros
                principales clientes eran las novias,
                pero era muy difícil encontrarlas... ya
                que es un mercado muy disperso,
                entonces se nos ocurrió hacer una
                expo. Logramos que las novias
                llegaran a nosotros y eso fue mucho
                más de lo que alguien pudiera haber
                conseguido”. <br><br>
                <strong>La pasión que Manuel impregna en
                cada concepto, lo hace no por el
                sentido de dinero, sino por el
                sentido de funcionalidad,</strong> <i>“puedes
                crear cosas hermosísimas pero que
                no son necesarias, y eso no sirve, se
                tiene que crear un concepto que sea
                funcional para que tenga realmente
                una razón de ser.”</i></p> <br>
                
            </div>
            <div class="col l5 s12 alinear">
                <img src="<?php echo base_url() ?>dist/img/blog/Manuel01.jpg" style="width: 100%">
            </div>
        </div>
        <div style="margin-top: 40px;" class="col l12">
            <div class="col l4 hide-on-small-only">
                <img src="<?php echo base_url() ?>dist/img/blog/Manuel02.jpg" style="width: 100%;">
            </div>
            <div class="col l8">
                <p>En Grupo Plexon, <strong>“Nosotros somos
                el enlace entre las novias y los
                proveedores, lo que hacemos
                nosotros es aglutinar el mercado,  la
                novia al encontrar todos sus
                proveedores juntos, resuelve todo
                en un mismo lugar y en un fin de
                semana, y los expositores
                encuentran a las novias juntas y sus
                ventas son bastante interesantes
                siempre y cuando ellos estén
                creando. Esto es como si fueras a
                pescar, una expo es así, tú vas a
                pescar, ahí están los pescados, tú
                sabrás que anzuelo usar, si usas
                una caña, tiras cinco líneas, o
                lanzas una red, es decisión de cada
                quién”.</strong> <br><br>
                <strong style="background: #72D6E0; font-weight: normal; color: black;">Más que dedicarse al negocio nupcial,
                la naturaleza de Manuel es crear
                conceptos</strong> que vayan más allá de lo
                cotidiano, como lo hizo con Teatro
                Vallarta, donde presentó el
                espectáculo fandango, el museo de
                Tequila; además de las diferentes
                expos que ha realizado a lo largo de
                los años como Expo tu Auto, Expo XV,
                Expo tu boda, Expo Chivas, y ahora
                sigue con este proyecto totalmente
                distinto a lo que ha hecho llamado
                Bride Weekend.</p>
            </div>
            <div class="col l4 show-on-small hide-on-large-only">
                <img src="<?php echo base_url() ?>dist/img/blog/Manuel02.jpg" style="width: 100%;">
            </div>
        </div>
        <div class="col l12" style="margin-top: 50px;">
            <div class="col l7 s12 alinear">
                <p style="margin: 0; margin-right: 4vh;"><i>“Cuando haces algo que no existe y
                captas la necesidad, eso es lo que vale, lo
                que hacemos en Grupo Plexon es crear
                conceptos, ahora con Bride Weekend, nos
                estamos reinventando, esto es totalmente
                nuevo adaptado al siglo XXI, adaptado a la
                novia actual. Es una exposición rica en
                interactividad, estamos creando cosas
                como groom zone, donde vamos a
                dedicarle algo especial a los novios, que
                no existe en ningún lado, contamos
                también con la zona Japy, que es parte del
                nuevo concepto que tenemos en internet,
                lo que estamos haciendo en Bride
                Weekend es todo un concepto donde no
                solo somos una expo de un fin de semana,
                sino que tenemos un portal donde
                estaremos presentes las 24 horas del día,
                con mucha interacción, premios, tips…
                estamos logrando muchas cosas, tanto
                para beneficio de la industria como de las
                novias”.</i></p> <br>
                
            </div>
            <div class="col l5 s12 alinear">
                <img src="<?php echo base_url() ?>dist/img/blog/Manuel03.png" style="width: 100%">
            </div>
        </div>
        <div class="col l12" style="margin-top: 30px;">
            <p>Lo que más disfruta Manuel Díaz sobre el mercado nupcial es que <i><strong>“es padrísimo
            vender alegría e ilusión, ya que el evento más importante en la vida de una mujer
            es el matrimonio, porque son el núcleo, son las dadoras de vida, una boda no es
            nada más la realización de un nuevo hogar, es la realización de dos personas que
            luego se multiplican, son parte de la naturaleza, la mujer es la que realmente da la
            vida.“</strong></i></p>
        </div>
    </div>
</div>
</body>
<?php $this->view("japy/footer"); ?>
<script>
    $(document).ready(function(){

    });
</script>
</html>