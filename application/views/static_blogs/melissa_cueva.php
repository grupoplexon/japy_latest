<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Japy</title>
    <?php $this->view("japy/header"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta property="og:title" content="Cinco Minutos con Melissa Cueva"/>
    <meta property="og:image" content="<?php echo base_url() ?>dist/img/static_blogs/melissaPrincipal.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:url" content="<?php echo base_url()."melissa_cueva" ?>"/>
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:site_name" content="Japy" />
    <!-- <meta property="og:description" content="Todo lo que nbición." /> -->
    <meta property="og:type" content="website" />
    
</head>
<style>
body{
    text-align: justify;
}
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.slides{
    height: calc(100vh - 85px) !important;
}
.slider .indicators {
    margin-bottom: -14vh;
    z-index: 9;
}
hr {
    border: 0;
    height: 3px;
    width: 50%;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(114, 214, 224, 0.75), rgba(0, 0, 0, 0));
}
.movil{
    display: none;
}

@media only screen and  (min-width: 300px) and  (max-width: 500px){
    h1{
        font-size: 2rem !important;
    }
    .titulo{
        margin-top: -150px !important;
    }
    img{
        height: 100%;
    }
    .cuerpo{
        margin-top: 50px;
    }
    .movil{
        display: block;
    }
    .web{
        display: none;
    }
}


@font-face{
    font-family: MyFont;
    src:url('Gentleman1000.otf') format('opentype');
}
.titulo{
    background: rgba(114, 214, 224, 0.60);;
    position: absolute;
    color: white;
    text-align: center;
    margin-top: -200px;
    width:100%;
    font-weight: bold;
}
.negrita{
    font-weight: bold;
}
.cursiva{
    font-style:italic;
    font-weight:normal !important;
}
.sombreado{
    background: #72D6E0;
    color: white;
    font-weight:normal !important;
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}

</style>
<body>
    <div class="col s12 m12 l12" style="height: calc(100vh - 85px) !important; position: relative;">
        <div class="responsive-img web" style="height: 550px;">
                <img style="width: 100%; height:550px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/melissaPrincipal.png">               
        </div>
        <div class="responsive-img movil" style="height: 550px;">
                <img style="width: 100%; height:550px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/melissaPrincipal-responsive.png">               
        </div>
        <div class="titulo">
                    <h1> Cinco Minutos con Melissa Cueva </h1>
        </div>
        
    </div> 
    <div class="valign-wrapper" style="width:100%;height:100%;position: relative;">
        <div class="valign" style="width:100%;">
            <div class="container">
            <div class="row">
                <div class="col s12 m12 l12 cuerpo">
                    <br>
                    <p class="negrita">
                    Teniendo como primicia, la importancia de la mantelería en tu boda, dedicamos
                    este espacio para hablar cinco minutos con Melissa Cueva, creadora de Catalina
                    Tableware, empresa dedicada al diseño de mantelería personalizada para bodas,
                    creada hace dos años.
                    </p>
                    <hr>
                    <p>
                    Estar dentro de la industria de la moda desde los 19 años de edad, tener una hermana
                    como diseñadora de moda, y haber trabajado como decoradora de eventos con Marita,
                    una empresaria tapatía jóven dedicada a la organización y decoración de eventos Elite en
                    Guadalajara, fueron momentos que marcaron la decisión de Melissa para desarrollar sin
                    límite su creatividad en la creación de manteles con diseños clásicos y brillantes.
                    </p>
                    <p>
                    Para desarrollar su idea con éxito analizó por dónde convenía empezar, observó que <strong class="cursiva">
                    “hay mucha competencia, hay gente muy talentosa, muy buena, y yo tenía que ver dónde
                    había escasez en todo el medio para poder entrar, y me di cuenta que era Mantelería, no
                    había mantelería bonita, ya era muy antigua, clásica, era aburrida. Se me ocurrió,el
                    cambio de mantelería, texturas, combinaciones de colores, y así comenzó Catalina
                    Tableware”</strong>,  una propuesta única para darle vida a las bodas.
                    </p>
                </div>
                <div class="col s12 m12 l12">
                    <div class="responsive-img">
                        <img style="width: 100%;" src="<?php echo base_url() ?>dist/img/static_blogs/melissa1.jpeg"> 
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                <br>
                <hr>
                    <div class="col s12 m6 l6">
                    
                    <p>
                    <strong class="sombreado" >Mucha de la inspiración que surge para los estampados están basados en su marca
                    favorita:</strong> Dolce &amp; Gabbana, <strong class="cursiva" >“A partir de aquí, empiezo a ver que Catalina puede
                    ser tendencia sobre la mesa, no moda, porque la moda pasa rápido, y puede
                    durar una semana, seis meses, y ahora si; combinamos lo que es la pasarela con la
                    moda, pero también en la mesa… Las novias de hoy en día son muy arriesgadas
                    y les gusta jugar un poquito más”. </strong>.
                    </p>
                    <p>
                    <strong class="negrita" >Los elementos que definen a Melissa como diseñadora son</strong> <strong class="cursiva" >“lo clásico, soy muy cursi, me gusta el 
                    virreinato, lo dorado, lo brillante, las flores, esa parte combinada con la actual, y con un poquito
                    más de lo que va saliendo en tendencia; Combino mi gusto, mi personalidad, con lo
                    que va saliendo en tendencia”</strong>.
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:300px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/melissa3.jpeg"> 
                            
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <br>
                    <hr>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/melissa2.jpeg"> 
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                    <p>
                    Para Catalina Tableware lo más importante  es el servicio que ofrecen y la asesoría que
                    brindan a las novias, entre los decoradores y la novia se puede tardar en la realización del
                    diseño aproximadamente una semana en realizarlo y si se quiere un diseño desde
                    cero, se platica entre el artista, la novia y Melissa para ver cuánto tiempo se tardarían
                    en tenerlo listo.
                    </p>
                    <p>
                    <strong class="negrita" >La visión a futuro que Melissa tiene para
                    Catalina Tableware es</strong> <strong class="cursiva" >
                    “seguir haciendo diseños, pero ahora con arte, ya que hay
                    mucho talento de artistas en Guadalajara que pintan increíble, y ahora más que
                    tendencia tendríamos también arte en la mesa, un detalle que la va a alzar con algo
                    más personalizado y original”</strong>.
                    </p>
                    </div>
                </div>
                <div class="col s12 m12 l12">
                    <p >
                    Los invitados a una boda lo primero que buscan es la mesa donde se sentarán, y como
                    sabes la primera y la última impresión es la que cuenta así que elegir una mantelería
                    única y original, será parte fundamental para darle vida a tu boda ya que al final, el mantel
                    siempre estará ahí.

                    </p>
                </div>
                <div class="col s12 m12 l12">
                    <div class="responsive-img">
                        <img style="width: 100%;" src="<?php echo base_url() ?>dist/img/static_blogs/melissa4.jpeg"> 
                    </div>
                </div>
                <div class="col s12 m12 l12">
                    <p >
                    Sigue a Melissa Cueva en su Instagram: <a href="https://www.instagram.com/catalinatableware/"> https://www.instagram.com/catalinatableware/ </a>
                    </p>
                </div>
            </div>
            </div>
        </div>
    </div>
<script>
$(document).ready(function(){

});

</script>
</body>
<?php $this->view("japy/footer"); ?>
</html>