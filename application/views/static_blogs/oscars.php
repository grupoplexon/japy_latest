<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Japy</title>
    <?php $this->view("japy/header"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta property="og:title" content="La tendencia que marcaron los Óscar en 2019"/>
    <meta property="og:image" content="<?php echo base_url() ?>dist/img/static_blogs/oscar/BrieLarsonVestido.jpg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:url" content="<?php echo base_url()."tendencia-oscars" ?>"/>
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:site_name" content="Japy" />
    <!-- <meta property="og:description" content="Todo lo que nbición." /> -->
    <meta property="og:type" content="website" />
    
</head>
<style>
body{
    text-align: justify;
}
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.slides{
    height: calc(100vh - 85px) !important;
}
.slider .indicators {
    margin-bottom: -14vh;
    z-index: 9;
}
h1{
        font-size: 3.5rem !important;
    }
hr {
    border: 0;
    height: 3px;
    width: 50%;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(114, 214, 224, 0.75), rgba(0, 0, 0, 0));
}
.corchete p{
    margin-top:-155px; 
    margin-bottom: 30px; 
    width:auto; 
    margin-left: 10%; 
    margin-right: 10%;
}
.corchete img{
    width: 100%; 
    height: 170px;
}
.movil{
    display: none;
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}

@media only screen and  (min-width: 300px) and  (max-width: 500px){
    h1{
        font-size: 1.5rem !important;
    }
    .titulo p{
        font-size: 2rem !important;
        margin-top: 15vh;
    }
    img{
        height: 100% !important;
    }
    .cuerpo{
        margin-top: 10px;
    }
    .texto {
    margin-top:0px !important;
    }
    .corchete p{
        margin-top: -190px;
        margin-bottom: 30px;
        width: auto;
        margin-left: 10%;
        margin-right: 10%;
    }
    .corchete img{
        width: 100%;
        height: 210px !important;
    }
    .movil{
        display: block;
    }
    .web{
        display: none;
    }
}


@font-face{
    font-family: MyFont;
    src:url('Gentleman1000.otf') format('opentype');
}

.negrita{
    font-weight: bold;
}
.cursiva{
    font-style:italic;
    font-weight:normal !important;
}
.sombreado{
    background: #72D6E0;
    color: white;
    font-weight:normal !important;
}
.texto {
    margin-top:50px;
}
</style>
<body>
    <div class="col s12 m12 l12 " style="height: calc(100vh - 85px) !important; position: relative;">
        <div class="responsive-img web" style="height: 550px;">
                <img style="width: 100%; height:550px; object-fit:cover; object-position:top;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/BrieLarsonVestido.jpg">         
        </div>
        <div class="responsive-img movil" style="height: 550px;">
                <img style="width: 100%; height:550px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/BrieLarsonVestido.jpg">         
        </div>
        <!-- <div class="titulo">
                    <h1> Los Metálicos ¡La tendencia que marcaron los Óscar en 2019!  </h1>
        </div>
         -->
    </div> 
    <!-- <div class="responsive-img col l12">
            <video src="<?php echo base_url() ?>dist/img/blog/novias_espana.mp4" width="100%" autoplay muted loop controls></video>
    </div> -->
    <div class="col l12">
        <div class="container center-align titulo">
            <p style="font-size: 9vh; text-align: center;">Los Metálicos ¡La tendencia que marcaron los Óscar en 2019! </p>
        </div>
    </div>
    <div class="valign-wrapper" style="width:100%;height:100%;position: relative;">
        <div class="valign" style="width:100%;">
            <div class="container">
            <div class="row">
                <div class="col s12 m12 l12 cuerpo">
                    <p class="negrita" style="font-style: italic">
                    ¿Aún No sabes qué vestido ponerte para la boda de tu amiga? Rescatamos la tendencia de las 
                    actrices que acudieron a los premios Óscar y los vestidos más vanguardistas para esta temporada 
                    2019, donde los diseños metálicos en colores plata, lavanda y rosados, en cortes de tubo y 
                    trompeta fueron los protagonistas de la alfombra roja. 
                    </p>
                    <hr>
                    <p>
                    <strong class="sombreado" >Te compartimos nuestros looks favoritos de la premiación. </strong>
                    </p>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <div class="col s12 m6 l6">
                    <br> 
                    <p class="negrita" > Brie Larson  </p>
                    <p>
                    La capitana marvel llegó luciendo un vestido con diseño de Céline que resaltaba su espectacular
                     figura; Un vestido creado a modo de malla metálica; con una abertura en la parte de la pierna, 
                     cuello alto halter, y la espalda descubierta;<strong class="negrita" >Un diseño sexy,  
                     deslumbrante, que le da un toque de frescura a su look.   </strong> </p>
                    <p>
                    Estamos seguras que si imitas este vestido en la boda de tu amiga, será un gran acierto. 
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <img style="width: 100%; height:400px; object-fit:contain;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/BrieLarsonVestido.jpg"> 
                            
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <br>
                    <hr>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:400px; object-fit:contain;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/emiliaclarkevestido.jpg"> 
                        </div>
                    </div>
                    <div class="col s12 m6 l6 texto" >
                    <p>
                    <strong class="negrita" > Emilia Clarke</strong>.
                    </p>
                    <p>
                    La reina de los dragones llegó en un diseño de Balmain metalizado en lila que envolvía su figura con un escote
                     asimétrico. El largo del vestido se ajusta a su figura en la zona de la cintura, que destaca sus curvas, y 
                     termina con una cauda escueta en un corte de sirena. 
                    </p>
                    <p>
                    <strong class="negrita">Un vestido por demás vanguardista con el que te verás súper coqueta</strong>, moderna y  a la vez elegante.
                    </p>
                    
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <div class="col s12 m6 l6 texto">
                    <p class="negrita" > Amy Adams </p>
                    <p>
                    Amy llegó a la gala en un vestido strapless blanco con <strong class="negrita" >detalles de bordados metálicos en plata de 
                    Versace</strong>  en estilo de sirena que se ajustaba a su cuerpo con el que lucía sus atributos
                    </p>
                    <p>
                    Si eres dama de honor, y tu amiga decide que quiere que vayan de blanco, destacarás de las otras damas con este sexy diseño.  
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:400px; object-fit:contain;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/amyadams.jpg"> 
                            
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <br>
                    <hr>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:400px; object-fit:contain;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/Jenniferlopezvestido.jpg"> 
                        </div>
                    </div>
                    <div class="col s12 m6 l6 texto">
                    <p>
                    <strong class="negrita" > Jennifer López</strong>.
                    </p>
                    <p>
                    JLo, resplandeció con un Tom Ford, cubierto en un mosaico que <strong class="negrita">simula espejos, queda complementado
                     con un cuello alto y mangas largas</strong>. El vestido se ciñe perfectamente a su figura, y brilla en cada 
                     reflejo de luz.  
                    </p>
                    <p>
                    Con este vestido resplandecerás en la boda de tu amiga, y  <strong class="negrita">atraerás las miradas de 
                    los invitados presentes. </strong>
                    </p>
                    
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <div class="col s12 m6 l6 texto">
                    <p class="negrita" > Glenn Close </p>
                    <p>
                    Como la diva que es, Glenn lució un increíble vestido de  <strong class="negrita" >Carolina Herrera en tono dorado
                     metálico, con capa y cauda bordado completamente a mano</strong>   con más de 4 millones de chaquiras 
                     generando un efecto visual luminoso;un vestido pensado en el triunfo. 
                    </p>
                    <p>
                    ¡Si eres la madre, o la abuela del novio o la novia, que se note! Este estilo en un tono dorado como el que usó 
                    Glenn te hará ver impactante en ese día tan especial para ti.
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:400px; object-fit:contain;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/GlennCloseVestido.jpg"> 
                            
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <br>
                    <hr>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:400px; object-fit:contain;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/michelleyeohvestido.jpg"> 
                        </div>
                    </div>
                    <div class="col s12 m6 l6 texto">
                    <p>
                    <strong class="negrita" > Michelle Yeoh </strong>.
                    </p>
                    <p>
                    La actriz y modelo Michelle Yeoh, se une a la tendencia en un ventoso vestido de corte princesa 
                    con escote metálico de Eliee Saab que <strong class="negrita">rescata la elegancia y el romanticismo 
                    en sus encajes y bordados perfectamente detallados a lo largo de él. </strong>. 
                    </p>
                    <p>
                    Este tipo de corte busca estilizar tu figura y disimular las caderas, si eres deseas verte un poco más delgada, 
                    no dudes en conseguir un vestido como el de Michelle.
                    </p>
                    
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <div class="col s12 m6 l6 texto">
                    <p class="negrita" > Emma Stone  </p>
                    <p>
                    Emma, marca tendencia cada vez que se presenta en una alfombra roja, esta vez llegó con un vestido de 
                    hombros levantados, de Louis Vuitton. <strong class="negrita" > Cada detalle de este modelo está elaborado manualmente 
                    lo que destaca su alta costura; 
                    </strong>  todo el bordado del vestido fue creado con la técnica Lunéville, y está tapizado por más de 30 mil cristales 
                    cosidos Swarovski de 3mm. Con este estilo Emma logró un look bastante futurista. Así que toma nota, si quieres 
                    lucir igual que Emma, en la boda de tu amiga.
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:400px; object-fit:contain;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/EmmaStoneVestido.jpg"> 
                            
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <br>
                    <hr>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:400px; object-fit:contain;" src="<?php echo base_url() ?>dist/img/static_blogs/oscar/MarieKondeVestido.jpg"> 
                        </div>
                    </div>
                    <div class="col s12 m6 l6 texto">
                    <p>
                    <strong class="negrita" > Marie Kondo</strong>.
                    </p>
                    <p>
                    Representada por su orden, el vestido de Marie Kondo, diseño de Jenny Packham,  transmite tranquilidad. 
                    Es un modelo de color rosa pálido, <strong class="negrita">  cubierto con flores repletas de lentejuelas 
                    encarna perfectamente su delicadeza .  </strong>. 
                    </p>
                    <p>
                    Si eres fan del minimalismo, de lo sencillo, y consideras que menos es más, este estilo es el indicado para ti.
                    </p>
                    
                    </div>
                </div> 
                <div class="col s12 m12 l12">
                <br> <br>
                    <p style="text-align:center;">
                    <strong class="negrita"> Como ves, los diseños metálicos van ganando terreno en el mundo de la gala de los premios 
                    más importantes de la Academia, por la luz que reflectan y el efecto que generan en todas las personas que los 
                    admiran.</strong>.
                    </p>
                    <p style="text-align:center;">
                    <strong class="negrita">  Esperamos te inspires en alguno de estos looks, para la próxima boda a la que acudas en este 2019. </strong>.
                    </p>
                </div>
            </div>
            </div>
        </div>
    </div>
<script>
$(document).ready(function(){

});

</script>
</body>
<?php $this->view("japy/footer"); ?>
</html>