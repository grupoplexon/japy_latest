<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Japy</title>
    <?php $this->view("japy/header"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta property="og:title" content="Cinco minutos con Pablo Saracho"/>
    <meta property="og:image" content="<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PSboda4.jpg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:url" content="<?php echo base_url()."pablo_saracho" ?>"/>
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:site_name" content="Japy" />
    <!-- <meta property="og:description" content="Todo lo que nbición." /> -->
    <meta property="og:type" content="website" />
    
</head>
<style>
body{
    text-align: justify;
}
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.slides{
    height: calc(100vh - 85px) !important;
}
.slider .indicators {
    margin-bottom: -14vh;
    z-index: 9;
}
hr {
    border: 0;
    height: 3px;
    width: 50%;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(114, 214, 224, 0.75), rgba(0, 0, 0, 0));
}

@media only screen and  (min-width: 300px) and  (max-width: 500px){
    h1{
        font-size: 2rem !important;
    }
    .titulo{
        margin-top: -150px !important;
    }
    .video{
        height: 200px !important;
    }
}


@font-face{
    font-family: MyFont;
    src:url('Gentleman1000.otf') format('opentype');
}
.titulo{
    background: rgba(114, 214, 224, 0.60);;
    position: absolute;
    color: white;
    text-align: center;
    margin-top: -200px;
    width:100%;
    font-weight: bold;
}
.negrita{
    font-weight: bold;
}
.cursiva{
    font-style:italic;
    font-weight:normal !important;
}
.sombreado{
    background: #72D6E0;
    color: white;
    font-weight:normal !important;
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}
</style>
<body>
    <!-- <div class="col s12 m12 l12" style="position: relative;">
        <div class="video" style="height: 550px;">
                 <img style="width: 100%; height:550px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/melissaPrincipal.png">  
            <video autoplay muted loop controls style="max-height: calc(100vh - 85px);" src="<?= base_url()."dist/img/static_blogs/PabloSaracho/PSvideo.mp4"?>" width="100%"></video>
        </div> -->
        <!-- <div class="titulo">
                    <h1> Cinco Minutos con Melissa Cueva </h1>
        </div> 
        
    </div> -->
    <div class="responsive-img col l12">
            <video src="<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PSvideo.mp4" width="100%" autoplay muted loop controls></video>
    </div>
    <div class="col l12">
        <div class="container center-align">
            <h1 style=" text-align: center;">Cinco minutos con Pablo Saracho</h1>
        </div>
    </div>
    <div class="valign-wrapper" style="width:100%;height:100%;position: relative;">
        <div class="valign" style="width:100%;">
            <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">

                    <p class="negrita">
                    Descubre cómo ha sido el camino de Pablo Saracho en la fotografía de boda,
                    desde sus inicios hasta la actualidad, lo que lo motiva a hacer click y lo que le
                    inspira en sus Storytellings.
                    </p>
                    
                    <p>
                    Cuando visitamos a Pablo Saracho, uno de los fotógrafos de bodas más creativos y
                    artísticos de Guadalajara, lo encontramos con su cabello alborotado, alejado de la vanidad
                    y el glamour, se mostró con toda su esencia, usaba Jeans, vestía una playera del
                    Barcelona, como buen fanático del fútbol, que no pasa un domingo por la mañana sin ver
                    un juego y encima traía puesta una sudadera negra.
                    </p>
                    <p>
                    Carmen, su perrita, corría por toda su casa, los dos fueron a recibirnos a la puerta y lo
                    primero que vimos fueron las fotografías de Pablo; documentales, de viaje, o de calle,
                    enmarcadas por él mismo. Nos mostró sus favoritas y comenzó a contarnos la historia de
                    cómo fue su acercamiento con el mundo fotográfico, .<strong class="cursiva">
                    “ Desde los 15 años, tenía una réflex
                    porque mi madre estudió comunicación; Mi hermano llevaba foto en la universidad,
                    compró una Nikon D50 pero no le interesaba tanto, así que me regaló la cámara porque
                    vio que a mí sí me interesaba, me regaló también una laptop, entonces desde esa edad
                    ya le sabía mover al photoshop”</strong>.
                    </p>
                    <hr>
                </div>
                <!-- <div class="col s12 m12 l12">
                    <div class="responsive-img">
                        <img style="width: 100%;" src="<?php echo base_url() ?>dist/img/static_blogs/melissa1.jpeg"> 
                    </div>
                </div> -->
                <div class="col s12 m12 l12 alinear">
                    <div class="col s12 m6 l6">
                    <p>
                    <strong class="sombreado" >Pablo sabía que sería fotógrafo desde que trabajaba 
                    en una imprenta</strong>
                    ,donde cada que hacía cambio de tinta, metía sus fotos en la prueba de impresión, 
                    las dejaba ahí, y cuando los clientes llegaban, preguntaban de quiénes eran, y 
                    él se emocionaba al escuchar el interés de los clientes por ellas, “y ahí en la 
                    imprenta fue donde me impulsaron a ser fotógrafo”.  
                    </p>
                    <p>
                    Mientras estaba en la universidad, recuerda
                    que una de sus maestras les dejó hacer un
                    ensayo sobre lo que menos les gustara y lo
                    que más les gustara, “entonces dije lo que
                    más me gusta es la foto, y lo que menos me
                    gusta son las bodas, y me puse a
                    investigar”.
                    </p>
                    <p>
                    “Empiezo a ver propuestas diferentes, fotos
                    bonitas, y en ese momento yo hacía fotos
                    de todo, de lo que fuera pero mal, no tenía
                    educación, <strong class="negrita"> había tomado cursos de foto,
                    había leído dos libros, había buscado a
                    muchos fotógrafos para ser asistente
                    pero todos me rechazaban, me acercaba
                    a muchas puertas y no se abría nada,
                    entonces yo hacía lo que podía</strong>, foto de
                    producto, foto de bandas, foto a mis
                    amigos, me la pasaba experimentando con
                    mi cámara.”
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:440px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PSboda1.jpg"> 
                            
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <br>
                    <hr>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:440px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PSboda2.jpg"> 
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                    <p>
                    Pablo Saracho hablaba con la misma seguridad con la que le aceptó tomar las 
                    fotografías de la boda civil de su vecina, con la misma emoción que cuando 
                    se ganó sus primeros 500 pesos que le pagaron por esa boda, con el romanticismo 
                    que se requiere para ser fotógrafo de bodas y con la nostalgia de recordar el 
                    cumpleaños número 70 de su abuela. 
                    </p>
                    <p class="cursiva">
                    “Mi padre siempre fue súper crítico con mis fotos, cuando fueron los 70 años de 
                    mi abuela, me dijo que tomara las fotos, él esperaba la típica foto de reportaje 
                    de señora rica, y yo me llevé un 50mm, hice todo a blanco y negro, las imprimí, 
                    se las enseñé a mi papá y me dijo que eran una porquería, y yo en vez de ofenderme, 
                    me dio para arriba... Ahorita mi papá ya me dice que están muy bonitas mis fotos”
                    </p>
                    <p>
                    Las primeras bodas que realizaba eran junto a sus amigos “El Sheriff! y Sabino, 
                    se divertían bastante y más que un trabajo, para ellos era una fiesta, se volvían 
                    parte de la boda y eso hacía que sus fotos fueran más reales y menos posadas, 
                    <strong class="negrita" >“para que tu trabajo refleje lo que estás haciendo, tienes que vivirlo”.</strong>.
                    </p>
                    </div>
                </div>
                <div class="col s12 m12 l12">
                <br>
                    <hr>
                    <p>
                    Para Pablo, la boda que marcó un antes y un después en su camino fue cuando conoció 
                    a David Bustos, <strong class="cursiva">“ yo todavía no me quitaba la escuela de foto que traía, seguía 
                    fotografiando con flash, a todos lados llevaba softbox, dirigía, buscaba la luz 
                    perfecta, era muy posado y todavía no estaba tan contento porque no sentía haber 
                    encontrado que había dado un gran salto, pero el dinero era bueno y había trabajo 
                    constante” Después de conocer a David, “Cambié mi estilo de boda por completo, de 
                    entregar 30 fotos de alto impacto, a tener una historia completa con las 800 fotos 
                    que ahora entrego, a contar un relato, que es lo chido de las bodas, que te abren las 
                    puertas, y la cámara se vuelve lo que tú quieras”</strong>.
                    </p>
                    <p>
                    Después de terminar de contarnos esta anécdota, sucedió algo curioso; Pablo recibió una 
                    llamada, y era justamente David. Llegó a visitarlo para darle su abrazo de cumpleaños que 
                    había sido un día anterior, se sentó con nosotros y seguimos platicando. 
                    </p>
                </div>
                <div class="col s12 m12 l12">
                    <div class="responsive-img">
                        <img style="width: 100%;" src="<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PSboda3.jpg"> 
                    </div>
                    <br>
                    <hr>
                </div>
                <div class="col s12 m12 l12">
                    <div class="col s12 m6 l6">
                    <p>
                    <strong>La autenticidad de Pablo Saracho lo ha llevado a ganarse la confianza de sus 
                    clientes, </strong>
                    <strong class="cursiva">“mis papás desde que era pequeño, me enseñaron a ver a la gente a 
                    los ojos cuando hablo con ellos, a aprenderme su nombre, a saludar, para mí 
                    es bien importante saludar a la gente y decir hola soy Pablo, trato de conocer 
                    a la pareja antes, los agrego a Instagram, a Facebook, veo lo que les gusta, 
                    trato de hacer click con ellos y eso me hace sentir parte de la boda, llego a 
                    empatizar hasta de más y llorar en los vals. Tengo que estar atento a lo que 
                    está pasando, pero si trato de ser parte de para poder sentir y reflejarlo”.</strong>
                    </p>
                    <p>
                    Pablo tomaba agua, mordía un plátano, lanzaba la pelota de Carmen mientras 
                    ella corría por ella, y siguió: 
                    </p>
                    <p>
                    <strong class="cursiva">“Se nota en la foto cuando no está contenta la gente, 
                    cuando no está agusto contigo, se siente en la tensión muscular, en que no respira, 
                    entonces si uno está nervioso, se nota y ellos están igual, uno tiene que estar muy 
                    tranquilo, ser amigo de ellos, tienes que verlos, para que te sientas un familiar 
                    más”</strong>
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:440px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PSboda4.jpg"> 
                            
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12">
                    <br>
                    <hr>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:602px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PS01.jpg"> 
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                    <p>
                    Las fotografías de bodas que Pablo hace, tienen 50% su esencia y 50% la esencia de la 
                    pareja, <strong>“lleva mi esencia porque es lo que ellos me hacen sentir, lo que me transmiten. 
                    Es la esencia a través de mis ojos”</strong>. 
                    </p>
                    <p class="">
                    Inspirado por las fotografías documentales de los libros que lee, de sus caminatas 
                    por la ciudad y de escribir lo que sentía en el día de la boda, se enfocó en un 
                    estilo para contar la historia de la boda de una manera distinta: Timeless Storytelling. 
                    <strong class="cursiva">“Que los hijos de los novios vean las fotos, y puedan contarles la historia, puedan 
                    saber lo que sucedió sin necesidad de un vídeo, porque el vídeo te lo da servido, te 
                    da el producto tal cual, y con las fotos, tienes la capacidad de imaginarte qué pasó, 
                    de representar qué sigue, y para mi es importante la cronología de los eventos, por 
                    eso me gusta empezar desde el arreglo de la novia, para que comience a sentir y tener 
                    una conexión de historia”.</strong>
                    </p>
                    <p>
                    <strong class="sombreado">El momento favorito en la boda, para Pablo Saracho,</strong> 
                    <strong class="cursiva">“es cuando salen los novios 
                    de la iglesia, porque en ese momento ya para ellos es prueba superada, ya de ahí 
                    en adelante es puro gozar, antes de que caminen me les acerco, los toco para que 
                    me sientan, y les digo caminen lento, disfrútenlo, este es su momento, sonrían, 
                    vean a la gente; entonces van prestando más atención al momento, y están súper 
                    presentes. A pesar de que la salida de la iglesia no es la mejor luz, a pesar de 
                    que no es mi foto favorita, es mi momento favorito porque es cuando más contentos 
                    los veo.” </strong>
                    </p>
                    </div>
                </div>
                <div class="col s12 m12 l12">
                <br>
                    <hr>
                    <p>
                    Finalizamos la entrevista, ojeamos algunos libros de fotografías de Alex Webb y 
                    Martin Parr de los que tenía Pablo en su casa, nos enseñó una fotografía que hizo 
                    con su celular donde vimos a dos niños que se estaban mojando con una manguera, 
                    una que imprimió para su cuarto de una vaca en medio del campo en China y nos 
                    contó que esas fotos nos las suele mostrar en sus redes sociales, porque las 
                    toma para él, y no para nadie más. Nos despedimos, y dejamos a Pablo terminando 
                    de editar unas fotografías.
                    </p>
                </div>
                <div class="col s12 m12 l12">
                    <div class="responsive-img">
                        <img style="width: 100%;" src="<?php echo base_url() ?>dist/img/static_blogs/PabloSaracho/PS02.jpg"> 
                    </div>
                    <br>
                    <!-- <hr> -->
                </div>
                <div class="col s12 m12 l12">
                    <p >
                    Sigue a Pablo Saracho 
                    en su Instagram: 
                    <a href="https://www.instagram.com/pablosaracho/" target="_blank"> https://www.instagram.com/pablosaracho/ </a>
                    </p>
                </div>
            </div>
            </div>
        </div>
    </div>
<script>
$(document).ready(function(){

});

</script>
</body>
<?php $this->view("japy/footer"); ?>
</html>