<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Japy</title>
    <?php $this->view("japy/header"); 
    $url_img     = $this->config->base_url()."blog/resource/imagen/";
    $url_default = $this->config->base_url()."/dist/img/blog/default.png";
    ?>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta property="og:title" content="Cinco Minutos con Victoria Sámano. Novias de España"/>
    <meta property="og:image" content="<?php echo base_url() ?>dist/img/static_blogs/victoria1.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:url" content="<?php echo base_url()."victoria_samano" ?>"/>
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="476">
    <meta property="og:site_name" content="Japy" />
    <!-- <meta property="og:description" content="Todo lo que nbición." /> -->
    <meta property="og:type" content="website" />
    
</head>
<style>
body{
    text-align: justify;
}
.content{
    min-height: calc(100vh - 85px);
    position: relative;
}
.slides{
    height: calc(100vh - 85px) !important;
}
.slider .indicators {
    margin-bottom: -14vh;
    z-index: 9;
}
h1{
        font-size: 3.5rem !important;
    }
hr {
    border: 0;
    height: 3px;
    width: 50%;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(114, 214, 224, 0.75), rgba(0, 0, 0, 0));
}
.corchete p{
    margin-top:-155px; 
    margin-bottom: 30px; 
    width:auto; 
    margin-left: 10%; 
    margin-right: 10%;
}
.corchete img{
    width: 100%; 
    height: 170px;
}
.movil{
    display: none;
}
.alinear {
    /* position: fixed !important; */
    padding-top: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
    padding-left: 0px !important;
    /* max-width: 800px !important; */
}

@media only screen and  (min-width: 300px) and  (max-width: 500px){
    h1{
        font-size: 1.5rem !important;
    }
    .titulo{
        margin-top: -150px !important;
    }
    img{
        height: 100% !important;
    }
    .cuerpo{
        margin-top: 50px;
    }
    .corchete p{
        margin-top: -190px;
        margin-bottom: 30px;
        width: auto;
        margin-left: 10%;
        margin-right: 10%;
    }
    .corchete img{
        width: 100%;
        height: 210px !important;
    }
    .movil{
        display: block;
    }
    .web{
        display: none;
    }
}


@font-face{
    font-family: MyFont;
    src:url('Gentleman1000.otf') format('opentype');
}
.titulo{
    background: rgba(114, 214, 224, 0.60);;
    position: absolute;
    color: white;
    text-align: center;
    margin-top: -200px;
    width:100%;
    font-weight: bold;
}
.negrita{
    font-weight: bold;
}
.cursiva{
    font-style:italic;
    font-weight:normal !important;
}
.sombreado{
    background: #72D6E0;
    color: white;
    font-weight:normal !important;
}

</style>
<body>
    <!-- <div class="col s12 m12 l12 " style="height: calc(100vh - 85px) !important; position: relative;">
        <div class="responsive-img web" style="height: 550px;">
                <img style="width: 100%; height:550px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/victoria2.png">         
        </div>
        <div class="responsive-img movil" style="height: 550px;">
                <img style="width: 100%; height:550px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/victoria2-responsive.png">         
        </div>
        <div class="titulo">
                    <h1> Cinco Minutos con Victoria Sámano. Novias de España </h1>
        </div>
        
    </div>  -->
    <div class="responsive-img col l12">
            <video src="<?php echo base_url() ?>dist/img/blog/novias_espana.mp4" width="100%" autoplay muted loop controls></video>
    </div>
    <div class="col l12">
        <div class="container center-align">
            <h1 style=" text-align: center;">Cinco Minutos con Victoria Sámano. Novias de España</h1>
        </div>
    </div> 
    <div class="valign-wrapper" style="width:100%;height:100%;position: relative;">
        <div class="valign" style="width:100%;">
            <div class="container">
            <div class="row">
                <div class="col s12 m12 l12 cuerpo">
                    <br>
                    <p class="negrita">
                    El vestido de novia, es la prenda más importante que vas a vestir en tu vida, y para que sepas
                    más sobre el proceso creativo para elegir tu vestido, entrevistamos a Victoria Sámano de
                    Novias de España .
                    </p>
                    <hr>
                    <p>
                    Victoria Sámano es mucho más que solo una persona de interés en la industria nupcial,
                    es la cara, la mente y la personificación misma de Novias de España;  La mayoría de
                    los vestidos que encontramos en esta importante y exitosa casa de vestidos de novias,
                    tienen la visión y creatividad de Victoria, <strong class="cursiva">“al trabajar en esto, una busca complacer a la
                    gente, pero cómo lo buscas es a través de lo que tú quisieras, yo como mujer también
                    comparto estas ilusiones, de boda, del amor, y de la familia, siento que en mi proceso
                    de edición, siempre estoy pensando en ese ideal”</strong>.
                    </p>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <div class="col s12 m6 l6">
                    <p>
                    <strong class="sombreado" >Victoria recibe las propuestas de las  distintas líneas, ve el modelaje que le
                    presentan basado en diseños europeos  de vanguardia y de ahí elige lo que cree
                    que es lo mejor para su público</strong> , adaptado a la figura latina. Por el lado creativo,
                    decide la tela que lleva, el corte, y hace distintas pruebas junto con su equipo
                    para ver qué es lo que se acomoda más a su tipo de mercado.
                    <p>
                    Más que ser la creadora creativa de Novias de España, para Victoria esto implica algo más: 
                    <strong class="cursiva" >“Es un proceso muy bonito, hablamos siempre de bodas que eso implica hablar de amor e ilusiones;
                    pero lo mejor es cuando las novias al  finalizar su evento nos comparten sus
                    fotografías, su experiencia y muchas veces eso llega con un agradecimiento que en realidad nosotros lo hacemos con
                    mucho gusto. Es lindo incidir en un momento tan importante para otras personas, para ellas es el día de su boda,
                    es algo que no van a olvidar y para nosotros es bonito ser parte de eso”</strong>.
                    </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:400px; object-fit:cover;" src="<?php echo base_url() ?>dist/img/static_blogs/victoria1.png"> 
                            
                        </div>
                    </div>
                </div>
                <div class="col s12 m12 l12 alinear">
                    <br>
                    <hr>
                    <div class="col s12 m6 l6">
                        <div class="responsive-img">
                            <br>
                            <img style="width: 100%; height:660px;" src="<?php echo base_url() ?>dist/img/static_blogs/victoria3.jpg"> 
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                    <p>
                    <strong class="negrita" > La esencia de su marca está en combinar la alta calidad de los productos con un alto estándar de servicio,</strong> <strong class="cursiva" >
                     “Durante el proceso de la venta del vestido se pueden dar muchas cosas, muchas alegrías, algunas
                    angustias, y como equipo de ventas, es nuestra responsabilidad siempre estar brindando nuestro apoyo a la novia, yo diría
                    que esa es la esencia de nosotros, tenemos no solo productos de alta calidad y diseñados
                    para el mercado latino sino que también tenemos el apoyo y la vocación de servicio”</strong>.
                    </p>
                    <p>
                    <strong class="sombreado" >La novia al probarse su vestido se dará cuenta de por qué Novias de España es distinto</strong>, ya que <strong class="cursiva" >“una puede ver los vestidos
                    en percha o maniquí y en general todos tienen algo lindo, algo deseable, sin embargo,
                    a la hora de ponérselo toda mujer sabe que un vestido que te queda bien, vale por mil, al
                    momento de probarse el vestido la novia se da cuenta que la horma es distinta, que le
                    queda a su cuerpo y que le da un soporte extra que nunca está de más y menos en un día tan especial”</strong>.
                    </p>
                    <p>
                    Después del proceso que requiere pensar en el vestido ideal, para Victoria, la entrega del
                    vestido es uno de los momentos más emotivos porque
                    </p>
                    <div class="corchete"> 
                        <img style=" " src="<?php echo base_url() ?>dist/img/static_blogs/corchetes.jpg"> 
                        <p style="">
                        “en general la novia viene acompañada de su mamá, amigas o hermanas, muchas veces
                        hay algo de lágrimas, además, es el momento en el que más recibimos agradecimiento, y
                        nos sentimos satisfechos de ser parte para crear la ilusión de esa persona”.
                        </p>
                    </div>
                    </div>
                </div>
                <div class="col s12 m12 l12">
                    <p >
                    <strong class="negrita"> Al final, no se trata solo de una prenda de vestir, para Sámano es mucho más que
                    un vestido de novia</strong>,<strong class="cursiva"> “es una prenda que se usa en un día especial, entonces de cierta
                    forma, es como la novia se presenta ante su familia, ante la sociedad y ante su futuro
                    esposo, es un momento en el que hay que deslumbrar, ser ese ideal, el vestido de la
                    novia es muy importante antes, durante y después, en las memorias de su evento,
                    entonces es más que una prenda de vestir, es una prenda que ella usa en ese momento
                    en el que su vida cambia, representa un momento de cambio y esta es la prenda que lo
                    simboliza”</strong>.

                    </p>
                </div>
                <div class="col s12 m12 l12">
                    <div class="responsive-img">
                        <img style="width: 100%;" src="<?php echo base_url() ?>dist/img/static_blogs/victoria4.png"> 
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
<script>
$(document).ready(function(){

});

</script>
</body>
<?php $this->view("japy/footer"); ?>
</html>