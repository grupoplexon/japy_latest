<!DOCTYPE html>
<!-- saved from url=(0050)http://bigelephant.mx/_sitios/japy/bienvenido.html -->
<html lang="en" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>.:: Bienvenido ::.</title>
        <link href="<?php echo base_url(); ?>dist/css/reminders/reminders.css" rel="stylesheet">
    </head>
    <body style=" margin: 0; padding: 0;">
        <table style="height: 100vh;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tbody>
            <tr>
                <!-- MARCO -->
                <td align="center" style="background-color: #ffffff;">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;max-width:600px">
                        <!-- ENCABEZADO -->
                        <tbody>
                        <tr>
                        <p>
                            <strong><?= !empty($email)? $email: 'Error consulta a servicio' ?></strong> gracias por registrarte en nuestra actvidad.<br>
                                 </p>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#fff" style="padding:0px 0px 0px 0px;">
                                            <img src="<?php echo base_url(); ?>dist/img/brideweekend/REGISTROACTIVIDAD.png" alt="JAPY" width="100%"  style="display: block;">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#ffffff" width="100">
                                        </td>
                                        <td bgcolor="#fff" style=" padding: 10px 30px 10px 30px; background: repeating-linear-gradient(-45deg, #ffffff 0, #ffffff 5%, #fff 0, #fff 100%)">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                <tr>
                                                    <td bgcolor="#fff" style=" font-family: &#39;Raleway&#39;, sans-serif; text-align: justify;font-size: 14px; color:#444242; ">
                                                        <p>
                                                            Te esperamos el dia del evento
                                                        </p>
                                                        <!-- <p>Nota: El apartado 'Mi Cuenta' esta ubicado al presionar el menu superior derecho, despues de iniciar sesion, es la penultima opcion.</p> -->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td bgcolor="#ffffff" width="100">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </body>
</html>