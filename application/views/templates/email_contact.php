<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <h2>Te a contactado  la siguiente persona:</h2>
    <div>El usuario de nombre: <strong><?= $name ?></strong></div>
    <br>
    <div>Correo: <strong><?= $email ?></strong></div>
    <br>
    <div>Teléfono: <strong><?= $telefono ?></strong></div>
    <br>
    <div>Te a enviado por medio de contacto el siguiente mensaje:</div>
    <p><strong><?= $msg ?></strong></p>
</body>
</html>