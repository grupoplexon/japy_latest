<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <h2>Te ha contactado  la siguiente persona</h2>
    <div>La persona de nombre: <strong><?= !empty($name)? $name: 'Error consulta a servicio' ?></strong></div>
    <br>
    <div>Correo: <strong><?= !empty($email)? $email: 'Error consulta a servicio' ?></strong></div>
    <br>
    <div>Telefono: <strong><?= !empty($telefono)? $telefono: 'Error consulta a servicio' ?></strong></div>
    <br>
    <div>Te ha enviado por medio de BrideWeekend el siguiente mensaje:</div>
    <p><strong><?= !empty($msg)? $msg: 'Error consulta a servicio' ?></strong></p>
</body>
</html>