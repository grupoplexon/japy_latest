<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 30px 0; background-color:#e6e3e2; ">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td>
            <img src="<?php echo base_url() ?>dist/img/japy_nobg_cut.png" height="155" alt='Logo'
                 data-default="placeholder"/>
        </td>
    </tr>
    <tr>
        <td>
            <h1 style="text-align: center; color: #000000;">Indicadores de los ultimos 7 dias</h1>
        </td>
    </tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
    <?php foreach (array_chunk($indicators, 3) as $indicatorsChunk) : ?>
        <tr>
            <?php foreach ($indicatorsChunk as $indicator) : ?>
                <td width="33%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="text-align: center;padding: 10px 0;">
                                <img src="<?php echo base_url().'dist/img/image_email/'.$indicator['image'] ?>"
                                     height="60" alt=''
                                     data-default="placeholder"/>
                                <span style="font-size: 30px;"><?php echo $indicator['amount'] ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;font-size: 13px;">
                                <?php echo $indicator['message'] ?>
                            </td>
                        </tr>
                    </table>
                </td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>