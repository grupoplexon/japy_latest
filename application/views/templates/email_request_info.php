<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style>

            table {
                width: 100%;
                height: auto;
                font-size: 16px;
                font-family: sans-serif !important;
            }

            tfoot {
                text-align: center;
                background: white;
            }

            p {
                margin: 0;
                font-family: sans-serif !important;
            }

            img {
                width: 100%;
            }

            table, .info, .user_data {
                background-color: #f6f6f6;
            }

            .user_data, .extras {
                padding: 1.5rem 3rem;
            }

            .user_data p:first-child {
                font-size: 1.5rem;
            }

            .user_data, .info {
                font-size: 1.2rem;
            }

            .info {
                padding: 4rem 3rem;
            }

            .table-info {
                text-align: center;
                color: #a1a1a1;
            }

            .table-row {
                text-align: justify;
                font-size: 1.2rem;
                padding: .5rem;
            }

            .btn {
                background-color: white;
                padding: 2rem 0;
            }

            .btn a {
                width: 10rem;
                padding: 1rem;
                background-color: #00bcdd;
                text-decoration: none;
                color: white;
                font-size: 1.2rem;
                font-weight: 500;
            }

            .extras {
                background-color: #d4d4d4;
            }

            .extras p {
                color: #858585;
            }

            @media only screen and (max-width: 321px) {
                .table-row {
                    width: 100%;
                    text-align: center;
                }
            }

            @media only screen and (max-width: 322px) and (max-width: 425px) {
                .table-row {
                    width: 100%;
                    text-align: center;
                }
            }

        </style>
    </head>
    <body>
        <table>
            <thead>
            <tr>
                <td>
                    <img src="<?php echo base_url() ?>dist/img/image_email/top.png" alt="">
                </td>
            </tr>
            </thead>
            <tbody>
            <tr style="font-size: 16px;">
                <td class="user_data">
                    <p><b><?php echo $name ?></b> ha visitado el perfil de tu empresa <b><?php echo $providerName ?></b>
                        en <b>japybodas.com</b> y te ha enviado un mensaje solicitando m&aacute;s informaci&oacute;n sobre tus servicios.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="<?php echo base_url() ?>dist/img/image_email/rings.png" alt="">
                </td>
            </tr>
            <tr class="info">
                <td>
                    <table class="table-info">
                        <tr style="font-size: 16px;">
                            <td>
                                <p class="table-row"><b>Fecha de evento: </b>
                                    <?php if ($date !== '0000-00-00 00:00:00') {
                                        $date = date_create($date);
                                        echo date_format($date, "Y-m-j");
                                    } else {
                                        echo " ";
                                    }
                                    ?></p>
                            </td>
                            <td>
                                <p class="table-row"><b>Correo: </b><?php echo ($email) ? $email : " " ?></p>
                            </td>
                        </tr>
                        <tr style="font-size: 16px;">
                            <td>
                                <p class="table-row"><b>Invitados: </b><?php echo ($guests) ? $guests : " " ?></p>
                            </td>
                            <td>
                                <p class="table-row"><b>Tel&eacute;fono: </b><?php echo ($phone) ? $phone : " " ?></p>
                            </td>
                        </tr>
                        <tr style="font-size: 16px;">
                            <td>
                                <p class="table-row"><b>Ciudad: </b><?php echo ($city) ? $city : " " ?></p>
                            </td>
                            <td>
                                <p><b>Presupuesto:</b> <span><?php echo ($budget)? $budget : ""?></span></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td class="btn">
                    <a href="<?php echo base_url() ?>cuenta?callback=<?php base_url() ?>proveedor/solicitudes/ver/<?php echo (is_null($mail->parent)) ? $mail->id_correo : $mail->parent ?>">Responder</a>
                </td>
            </tr>
            <tr style="font-size: 16px;">
                <td>
                    <p style="color: #3c3c3c;font-size: .8rem">Nota: Los proveedores que responden en las primeras horas despu&eacute;s del <br> contacto tienen un porcentaje mayor de cierre de ventas.</p>
                </td>
            </tr>
            <tr style="font-size: 16px;">
                <td class="extras">
                    <p>Si no te has registrado y recibes este correo por error puedes ignorarlo y disculpa cualquier molestia ocasionada.</p>
                </td>
            </tr>
            </tfoot>
        </table>
    </body>

</html>