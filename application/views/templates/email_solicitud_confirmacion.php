<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="font-family: Century Gothic, CenturyGothic, AppleGothic, sans-serif;">
<div style="margin: 0.5rem 0 1rem 0;border: 1px solid #e0e0e0;border-radius: 2px;">
    <div style="position: relative;">
        <img style="width: 100%; height:auto;" id="imagen"
             src="<?php echo "data:$mime;base64," . base64_encode($base) ?>"/>
    </div>
    <div style="padding: 0px 90px 0px 90px;">
        <br/>
        <br/>
        <h5 style="font-size: 1.64rem;line-height: 110%;margin: 0.82rem 0 0.656rem 0; text-align:center;">
            <b><?php echo $novia . ' & ' . $novio ?></b></h5>
        <br/>
        <h6 style="font-size: 1rem;line-height: 110%;margin: 0.5rem 0 0.4rem 0; text-align:center;"><?php echo strftime("%A %d de %B del %Y ", strtotime($fecha)) ?></h6>
        <br/>
        <h6 style="font-size: 1rem;line-height: 110%;margin: 0.5rem 0 0.4rem 0; text-align:center;"><?php echo $lugar ?></h6>
    </div>
    <hr style="height: 6px; background: url(http://ibrahimjabbari.com/english/images/hr-12.png) repeat-x 0 0; border: 0; margin: 50px 30px 50px 30px;"/>
    <div style="padding: 0px 90px 0px 90px;">
        <br/>
        <br/>
        <h6 style="font-size: 1rem;line-height: 110%; margin: 0.5rem 0 0.4rem 0; text-align:center;">
            <img style="float: left;margin-right: 15px; width: 24px; heigth: auto;"
                 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAJmklEQVR4Xu2d+49dVRXH19pnaMNjZu553LnDUF5aEBQURRqiMQYVghREEN+v1EeMIaRqCNiADaRAKg2CpCExRiHG+MZHK1Ta0pkaNRj/An/1h/7Ss/epSkem9969zJ4MsU7n9qzz2Hfm9K776/nu7177uz85M3ffc/ZGkM9IJ4AjPXoZPAgAIw6BACAAjHgCIz58uQMIACOewIgPX+4AAsCIJzDiw5c7gAAw4gmM+PDlDiAAjHgCIz58uQMIACOewIgPX+4AAsCIJzDiw5c7gAAw4gmM+PDlDiAAjHgCdQx/48b1YZZtJ2u3KKWm+9b+PUDcZYz5YR32Pj3kDlA9XYyiaC8gbj7FishBcF/1Lvw5CAAVsw2T5FYk2jvIBgG+rbX+ZsVuvDUXACpGG8XxUwCw9XQ2BPBYpvUDFbvy0lwAqBhrGMdPI8A9DJtHjNbfYuiGKhEAKsYdx/GdBPA8ywbxYZOmD7G0QxIJANWDVmEYHkSlbmBZEW03xuxgaYcgEgBqCDmKook+4ksBwPUcOwR4UGv9KEfrWyMA1JSwg8AiHlAAmziWBLAt03onR+tTIwDUmG4YhpNEdEAFwXUcWwK4P9P6cY7Wl0YAqDnZVqvVUkodBMRrWdaI95o0fYKl9SASADyEOjk5GY6Njb1MAG9n2RN93Rjj1hOG/hEAPEU+MTEROQgA8RpOFwiwVWv9NEdbp0YAqDPNZV7j4+PxWevXHwKit3K6cQtKWuvdHG1dGgGgriQH+IyPjydnrVt3CACu5nRFAHdnWj/D0dahEQDqSDHH47zp6fa6bncWAN7C6I4I8atZmn6Poa0sEQAqR8gz6HQ6Uwu93qwCeDOjBQHiV0yafp+hrSQRACrFV6zx1NRUZ6Hfnw0ArmS0JCD6ku+HSgQAxkzUKWm329O9Xm8OlXoTw5cQ4Ata6+cY2lISAaBUbNUaJUlyvoNABcHlDCeLAFu01j9iaAtLBIDCkdXTIEmSGWvtHCBexnC0BPD5TOsfM7SFJAJAobjqFcdxfIGDAJXayHDuE+JnszT9KUPLlggA7Kj8CKMo2oCIcwTwRkYPfSD6tDHm5wwtSyIAsGLyK4qi6EJQ6jAQXcroyUHwSWPMLxnaXIkAkBvRcAStVutiFQRzAHAJo8ceEH3CGMN7FO00hgIAI+1hSVqt1iVLEFzM6LOHAB/TWv+GoR0oEQCqpOeh7eTk5KXB2NhhALiQYd9FgI9qrX/H0K4oEQDKJuex3WSn8wbs9Q4rgA2Mbk4Q4l1Zmg58OeV0HgIAI+HVkLTb7Y1da+cUwAWM/h0Ed2Zp+gJD+38SAaBoYkPUt9vty7puxVCpmbxuEWDBWntHlmX78rQnXxcAiqS1CtokSS7v9fsOgvPzuncQENHtxpiX8rSvXxcAuEmtoi6O4yt61s4GSk3nldG39rUA8UPGmAN5WnddAOCktAY0cRxf2e/3Z1UQdPLKsdb+J1DqNq31y3laASAvoTV0PYoi90TRIUCcyivLQTAWBJvTNHVPIg38CAB5Sa6x62EYXqWUOkQA7bzSrLXzEAS3HEtTt66w4kcAyEtxDV4Pw/BqDAL3tHGSV5619jiMjd1y7OjRP66kFQDyElyj11tTU29T/b77Gx/nlWitfRUBPphl2Z+WawWAvPTW8PVWu32NstZBEOWVaa39t0K82Rjzl5O1AkBecmv8epIk77BEBwEgzCt1CYKbjDGvvK5dCYCxPCO5vrYSiOP4WgJwiz+TeZVZgH8FADdprf/qtIsALH69UGoXEr2PANbnmcj1xifwTwS4UWv9N1xcYAB4RQFMNH5YMoAiCRwLlLrOAbCHAG4r0lK0Z0YCFuB5bIXhq0qpc8+MIckoiiTg/il0AMwrpc4u0lC0Z0YCi+sDUZLsA6Kbz4whySiKJEDW7kG3ogTd7p/lz0CR6JqvdUvECnHT4tdAt5hARN8hgPcAgGr+8GQEp0vg5B+Jli8ErWu1WudIfM1KABGvQqV+z1oIWvYzsSwFN2uuT6nWLeIh4izz5+FTHhQRABoMgFvEIyL3hnHuAyKDHhUTABoKQInnBN3DovuXD1cAaCAAJZ4U/rAx5g8rDVUAaBgARd4VAIATpNQd2dGjLw4apgDQIABKvC30kSxN3beDgR8BoCEAFHxfsKsQ70rTdE/e8ASAvITWwPWlN4bd3gEXMcpxbwy718Z/y9DKiyGckFZTs7RxhHusm7tnwMe11r/m1ix3AG5Sq6ALw/AidFvH8HcNcVvH/KpIqQJAkbSGqC2xb9CnjDG/KFqiAFA0sSHoi+4cRoifydL0Z2VKEwDKpOaxTYm9Az+XpelPypYkAJRNzkM7t3toz+0oztxCto7dQwUADxNZxrLgJtK17R8sAJSZrZrbLG4j3+3OBUpdwbB228h/0RjzLEObKxEAciPyK3AHSbzW680VOEPgy8aYH9RVlQBQV5IlfIoeJePjFBEBoMTE1dGk4GFS3s4REgDqmM2CHkWPk/N5kpgAUHDyqspLHCjp9SxBAaDqjBZoX/RI2WGcJioAFJjAKtLCh0oP6TxhAaDKrDLbumPlEdGdKP5OVpMhniguALBmpLwoiqIJi3hAAWziuBDifVma7uJo69AIAHWkOMAjSZLxLtH+AOB6TjcEsC3TeidHW5dGAKgryWU+bvItkXsU+12cLhDgAa31YxxtnRoBoM40l7za7fZ5fTf5RO9m2RNtN8bsYGlrFgkANQfa6XTO7Xa7+wDRvWmd/0F82KTpQ/lCPwoBoMZcZ2ZmzllYWHiRAN7LskXcYdJ0O0vrSSQA1BTshg0bzj5+/PgLqNQNHEsCeDTT+kGO1qdGAKghXTf58/PzewHx/Rw7BNiptd7G0frWCADVE8Yoitzkb+ZYIcDjWuv7OdphaASAiimHSXIrEvGObCN6whhzb8Uua20uAFSMM0qSJ4Hoa3k2FvHJY2n6jTzdsK8LABUTj+L4KQDYmmPzXaN1LiQVSynVXAAoFdv/GsVxfDsBDHwRkwB2Z1rfU7Ebb80FgOrRYiuO9yuADyy3QoBntNZ3V+/Cn4MAUEO2bgFo/sSJRxTRFgBoAcA/EGCX1np3DfZeLQSAmuN1MBw5cmS+ZltvdgKAt2ibYSwANGOevFUpAHiLthnGAkAz5slblQKAt2ibYSwANGOevFUpAHiLthnGAkAz5slblQKAt2ibYSwANGOevFUpAHiLthnGAkAz5slblQKAt2ibYSwANGOevFUpAHiLthnGAkAz5slblQKAt2ibYSwANGOevFUpAHiLthnG/wU1y+pxXhLIIQAAAABJRU5ErkJggg=="/>
            <?php echo $titulo ?>
            <img style="float: right;margin-left: 15px; width: 24px; heigth: auto;"
                 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAHYUlEQVR4Xu2dS4wVRRSGT/WFUQzBXDqzIRhZsHDvyqUMhGhISHyh4gMfqIAbEhfEaFR8hQVqImiigi984QN8hIUkRhe6UBMWmkgMi4kQzHC7Tt/hIjOO01WmYMYgj7l1uru6q6nDuvrUOf/3dTO3b0+PAP4XdAIi6Ol5eGABApeABWABAk8g8PH5CsACBJ5A4OPzFYAFCDyBwMfnKwALEHgCgY/PVwAWIPAEAh+frwAsQOAJBD4+XwFYgMATCHx8vgKwAIEnEPj4fAVgAfxPoB3H64TWG0GIxQrgaKT164j4HAD843/3fnfo/RWgHccvC4CHz45RAXzalfImv+P1vzuvBWi329tEFG24YIxaL0fEr/2P2d8OvRWgL3wA80TrFinlJn/j9b8zLwWwgW+i1QDPp1I+6n/M/nbonQC28E2kAmBISvmNv/H635lXAlDggxAfYZLc6n/EfnfojQAU+Fqpz9M0vZk/BhaXywsBGH5xkHkr1C4AEf7eNE1v4TM/L+5zj6tVAAp8ANiDUq5i+OXBn/pButyCttWI8D9DKc0PfHzr1zZgy3W1XAHiON6uAdZb9mjgmzN/0nI9LyMkULkAFPhT9/vNmc/wCVApSysVgOFT0FSztjIBiPA/6Up5G5/57iWoRAAKfBDiY0yS2xm+e/iVfAogwt+NSbKa4VcD37kADL86kHl3cvZfABG++WLHnPlZ3kH4uHwJuBBAxHG8zfpz/ulv9Rh+Pn6FjypbABp8rT9ExDv4zC/MMXeBMgWgwv8AEe9k+LnZlXJgWQKQ4AuA96WUdzH8UhgWKlKGAFT470kp72b4hbiVdnBRARh+aSjqKVREACr8XVNnvqpnVN71fAnkFYDhXyQ+5RGABF8r9W6apmsAgM98D6WhCkCDD/BOKuU9DN9D8lMtUQQw8M2TPOssx3kbpbyX4VumVdMyWwGo8N9CKe9j+DVRJWxrIwAV/pso5f0Mn0ChxqX9BGD4NcKpYuuZBGD4VRCoeY8LCUCCrwB2dk9f9nXN8/D2xATOJwANvhA7ukmyluETk/dk+dkCkOCDEG9gkjzA8D2hmaONMwWgwT/9pq4HGX6O1D06ZFoAhu8RlCpbOSVAHMevEO7wVdkf71V+AipT6vdWq/UiJslroh3H6wXA9vL34Yq+J6ABnhWXt9u/taLoKt+b5f6cJJCJ+XFsfud+lpPyXNT7BIwAfwDAFd53yg06ScAI8DQAPOakOhf1PYFMwOLFl8xH3AdCLPG9W+6v5ASE2HzqY+CCBQsuGx8f/5IlKDlgP8spBXAw0voFRNzx351AigRTX/6Y+//8nJ+fkK27+t93ARQJBIB5zNs87Mm/0Wsdt38Lz/k2kCIBCMEvdPCPKamj8z4PQJFAK2Xe3mle4zZB2pkXe5HABZ8IokgAWu+bN2/ejcPDw+NeTMVNWCcw4zOBFAkUwP65c+asPHLkyJj17ryw9gT6PRRK+oiolfp2YGBgxcjIyF+1T8YNWCXQVwDyfQIhvgelrkfE41Yd8KJaE7ASwHS4cOHCOSdPnvzK5maRAvgRsmx5t9vt1jodb943AWsBqBIIgAMTExPLer2e7NsFL6gtAZIAVAkA4NfZs2YNjYyMHKttQt54xgTIAlAlyJQ6OLvVGkqS5Ciz8C+BXAJQJdBKHRJCLEHEw/5FEHZHuQWgSgAAwyrLru12u8NhR+7X9IUEyCHB4VYULel0Oof8iiHcbgoLQJVAKfWnkUBKeTDc2P2ZvBQBqBKA1se01kvTNP3FnyjC7KQ0AcgSAEgVRcu6nc6BMKP3Y+pSBThDAvN42ZDFiKkAWC6l/MliLS9xkEDpAlAlUADHI62vQ8QfHMzHJfsk4EQAsgRKnYBWa0U3Sb5jYtUm4EyAHBKMRUKsRMT91UYQ9m5OBaBKIAD+VlF0Q9rp7AsbS3XTOxeAKoF5tlAArJJS7q0uhnB3qkSAHBJMgtarEXF3uGiqmbwyAXJIkGmANamUu6qJIsxdKhUghwQKtF6LiDvDxON+6soFyCGB1gAbUilfdR9HeDvUIsC0BCfGxr6IAJZaxa71RkR8yWotL7JOoDYB8kigATalUm6xno4X9k2gVgHySABaP4GIm/tOxgusEqhdgDwSmLdbpVLyW02sEM+8yAsB8kgAAM+glI+XkEHQJbwRIIcEWkXR1fw8QTF/vRKALIEQT2GSPFksgrCP9k4AkgRab0XER8JGWGx6LwWwlkDrVfx9wUUqQF8JtP4ZEa8BgMliEYR9tLdXgGksixYturTX623VAOZP0gyYN5MpgD3ZxMRDvV4vCRtf8em9F2B6xMHBwblZll2ZZdnR0dHRtPjoXMEk0BgBGJebBFgAN7k2pioL0BhUbhplAdzk2piqLEBjULlplAVwk2tjqrIAjUHlplEWwE2ujanKAjQGlZtGWQA3uTamKgvQGFRuGmUB3OTamKosQGNQuWmUBXCTa2OqsgCNQeWmURbATa6NqcoCNAaVm0ZZADe5NqYqC9AYVG4aZQHc5NqYqv8Clxv462giLg0AAAAASUVORK5CYII="/>
        </h6>
        <br/>
        <p><?php echo $descripcion ?></p>
    </div>
    <?php if ($confirmacion == 1) { ?>
        <div>
            <hr style="height: 6px; background: url(http://ibrahimjabbari.com/english/images/hr-12.png) repeat-x 0 0; border: 0; margin: 50px 30px 50px 30px;"/>
            <div style="padding: 0px 90px 0px 90px; text-align:center;">
                <p>Por favor,confirma tu asistencia aqu&iacute;</p>
                <?php if (isset($data['codigo'])) : ?>
                    <a style="background-color: #eeeeee !important; box-shadow: none; background-color: transparent; color: #343434; cursor: pointer; border: none; border-radius: 2px; display: inline-block; height: 36px; line-height: 36px; outline: 0; padding: 0 2rem; text-transform: uppercase; vertical-align: middle; -webkit-tap-highlight-color: transparent;"
                       href="<?php echo base_url() ?>index.php/Extr/Confirmacion/index/<?php echo $data['codigo'] ?>"
                       target="_blank">Confirmar</a>
                <?php else : ?>
                    <a style="background-color: #eeeeee !important; box-shadow: none; background-color: transparent; color: #343434; cursor: pointer; border: none; border-radius: 2px; display: inline-block; height: 36px; line-height: 36px; outline: 0; padding: 0 2rem; text-transform: uppercase; vertical-align: middle; -webkit-tap-highlight-color: transparent;"
                       href="<?php echo base_url() ?>index.php/Extr/Confirmacion/index/<?php echo $codigo ?>"
                       target="_blank">Confirmar</a>
                <?php endif ?>
            </div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
        </div>
    <?php } ?>
</div>
</body>
</html>