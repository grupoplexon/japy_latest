<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style>
            table {
                text-align: center;
                width: 100%;
                height: auto;
                font-size: 16px;
                font-family: sans-serif !important;
            }

            p {
                margin: 0;
                font-family: sans-serif !important;
            }

            img {
                width: 100%;
            }

            table, .info, .user_data {
                background-color: #f6f6f6;
            }

            .user_data, .extras {
                padding: 1.5rem 3rem;
            }

            .user_data p:first-child {
                font-size: 2.5rem;
            }

            .user_data, .info {
                font-size: 1.2rem;
            }

            .info {
                text-align: justify;
                padding: 4rem 3rem;
            }

            .btn {
                background-color: white;
                padding: 3rem 0;
            }

            .btn a {
                width: 10rem;
                padding: 1rem;
                background-color: #00bcdd;
                text-decoration: none;
                color: white;
                font-size: 1.2rem;
                font-weight: 500;
            }

            .extras {
                background-color: #d4d4d4;
            }

            .extras p {
                color: #858585;
            }
        </style>
    </head>
    <body>
        <table>
            <thead>
            <tr>
                <td>
                    <img src="<?php echo base_url() ?>dist/img/image_email/top.png" alt="">
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="user_data">
                    <p>Hola <span id="user_name"><?php echo ($rol == 3) ? $bride : $user["name"] ?></span></p>
                    <p>Tenemos buenas noticias para ti.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="<?php echo base_url() ?>dist/img/image_email/rings.png" alt="">
                </td>
            </tr>
            <tr>
                <td class="info"
                <p><span id="provider_name"><?php echo ($rol == 3) ? $user["name"] : $bride ?></span> ha respondido a tu mensaje y quiere
                    seguir en comunicaci&oacute;n contigo, te invitamos a ingresar a tu
                    perfil para consultar la informaci&oacute;n</p>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td class="btn">
                    <?php if ($rol == 3): ?>
                        <a href="<?php echo base_url() ?>cuenta?callback=<?php base_url() ?>novios/buzon/ver/<?php echo $user["id"] ?>/<?php echo $mail["parent"] ?>">Ver Mensaje </a>
                    <?php else: ?>
                        <a href="<?php echo base_url() ?>cuenta?callback=<?php echo base_url() ?>proveedor/solicitudes/ver/<?php echo $mail["parent"] ?>">Ver Mensaje </a>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td class="extras">
                    <p>Si no te has registrado y recibes este correo por error puedes ignorarlo y disculpa cualquier molestia ocasionada.</p>
                </td>
            </tr>
            </tfoot>
        </table>
    </body>

</html>