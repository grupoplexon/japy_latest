<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body style="font-family: Century Gothic, CenturyGothic, AppleGothic, sans-serif; background-color: #f6f3f2;">
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td align="center" valign="top">
                        <table style="width:580px;border-radius:4px">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                            <tbody>
                                                <tr>
                                                    <td height="30" style="padding:20px 0;height:30px" align="center" valign="top"> 
                                                        <center style="width:100%;min-width:580px">
                                                            <small style="font-size: 8px;">Ve la web de Rosa Cabrera</small>
                                                            <br/><br/>
                                                            <a border="0" href="<?php echo base_url() ?>"> 
                                                                <img height="30" src="<?php echo base_url() ?>dist/img/japy_nobg_cut.png" style="max-width:100%;margin:0px auto 0px;min-height:30px!important;max-height:30px!important" border="0" align="center" class="CToWUd" />
                                                            </a> 
                                                        </center> 
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table bgcolor="#FFFFFF">
                                            <tbody>
                                                <tr>
                                                    <td align="center" valign="top">
                                                        <table width="100%" style="border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center" style="text-align:center;font-size:30px;color:#92151b;font-weight:600;padding:10px 30px 10px"> 
                                                                        <?php echo $miportal->nosotros ?>
                                                                    </td>
                                                                </tr>
                                                                <tr> 
                                                                    <td align="center" style="text-align:center;font-size:16px;color:#2b2b2b;font-weight:500;padding:10px 30px 0"> 
                                                                        <?php echo strftime("%A %d de %B del %Y ", strtotime($miportal->fecha_boda)) ?> 
                                                                    </td> 
                                                                </tr>
                                                                <tr> 
                                                                    <td style="text-align:center;padding:30px 10px 20px" align="center"> 
                                                                        <a style="margin:0 auto;max-width:580px" href="<?php echo base_url() ?>web/index/<?php echo $this->session->userdata('id_miportal') ?>" target="_blank">
                                                                            <img width="90" height="90" border="0" src="https://ci3.googleusercontent.com/proxy/ILOdq-FtT7N1mG0pzu2pTB4lJQNKUVOTtXkZeNVMO254RcKaXq8LQiv1yXoSzYz-V94oEktanEwqTOT6dx9H0PdizOJAXfKuwGI1ox1d6bwJLIhE01iU4MYm2w=s0-d-e1-ft#https://cdn1.bodas.com.mx/assets/img/user/120x120/user-female-120.jpg" class="CToWUd" /> 
                                                                        </a>
                                                                    </td> 
                                                                </tr>
                                                                <tr> 
                                                                    <td style="text-align:center;padding:0 20px 30px 20px;border-bottom:1px solid #e5e5e5;color:#2b2b2b;font-family:'Avenir Next',Avenir,Arial,sans-serif;font-size:16px;font-weight:500" align="center"> 
                                                                        <?php echo $asunto ?>
                                                                    </td> 
                                                                </tr>
                                                                <tr> 
                                                                    <td> 
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-bottom:1px solid #e5e5e5"> 
                                                                            <tbody>
                                                                                <tr> 
                                                                                    <td valign="top" align="left" style="padding:25px 0 25px 25px;width:42px"> 
                                                                                        <img src="<?php echo base_url() ?>dist/img/image_email/comillas1.png" width="42" height="30" class="CToWUd" /> 
                                                                                    </td> 
                                                                                    <td style="padding:20px 25px"> 
                                                                                        <?php echo $mensaje ?>
                                                                                    </td> 
                                                                                    <td valign="bottom" align="right" style="padding:25px 25px 25px 0;width:42px"> 
                                                                                        <img src="<?php echo base_url() ?>dist/img/image_email/comillas2.png" width="42" height="30" class="CToWUd"/> 
                                                                                    </td> 
                                                                                </tr> 
                                                                            </tbody>
                                                                        </table> 
                                                                    </td>
                                                                </tr>
                                                                <tr> 
                                                                    <td style="text-align:center;padding:30px 0 30px 0" align="center"> 
                                                                        <a style="display:inline-block;color:#ffffff;text-decoration:none;padding:0;border-radius:3px" href="<?php echo base_url() ?>web/index/<?php echo $this->session->userdata('id_miportal') ?>" target="_blank">
                                                                            <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;display:inline-block;padding:0"> 
                                                                                <tbody>
                                                                                    <tr> 
                                                                                        <td style="border-collapse:collapse!important;vertical-align:top;color:#ffffff;display:block;width:auto!important;margin:0;padding:10px 15px;border-radius:3px" valign="top"> 
                                                                                            <a style="background-color: #eeeeee !important; box-shadow: none; background-color: transparent; color: #343434; cursor: pointer; border: none; border-radius: 2px; display: inline-block; height: 36px; line-height: 36px; outline: 0; padding: 0 2rem; text-decoration:none; vertical-align: middle; -webkit-tap-highlight-color: transparent;" href="<?php echo base_url() ?>web/index/<?php echo $this->session->userdata('id_miportal') ?>" target="_blank" >
                                                                                                Ver la web de Rosa
                                                                                            </a>
                                                                                        </td> 
                                                                                    </tr> 
                                                                                </tbody>
                                                                            </table> 
                                                                        </a> 
                                                                    </td> 
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <br/>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <small style="font-size: 8px;">Copyright</small>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <br/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>