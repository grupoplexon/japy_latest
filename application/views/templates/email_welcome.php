<!DOCTYPE html>
<!-- saved from url=(0050)http://bigelephant.mx/_sitios/japy/bienvenido.html -->
<html lang="en" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>.:: Bienvenido ::.</title>
        <link href="<?php echo base_url(); ?>dist/css/reminders/reminders.css" rel="stylesheet">
    </head>
    <body style=" margin: 0; padding: 0;">
        <table style="height: 100vh;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tbody>
            <tr>
                <!-- MARCO -->
                <td align="center" style="background-color: white !important;">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;max-width:600px">
                        <!-- ENCABEZADO -->
                        <tbody>
                        <!-- <tr>
                            <td bgcolor="#fff" height="25px" width="40">
                            </td>
                        </tr> -->
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#ffff" style="padding:0px 0px 0px 0px;">
                                            <img src="<?php echo base_url() ?>dist/img/email/header1.png" alt="JAPY" width="100%"  style="display: block;">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- <tr>
                            <td bgcolor="#fff" height="15px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#white" height="30px" width="40">
                            </td>
                        </tr> -->
                        <!-- FIN ENCABEZADO -->


                        <tr style="background: #ffffff !important;">
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#ffffff" width="100">
                                        </td>
                                        <td bgcolor="#ffffff" style=" padding: 10px 30px 10px 30px; background: #ffffff">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                <tr>
                                                    <td bgcolor="#ffffff" style=" font-family: &#39;Raleway&#39;, sans-serif; text-align: center;font-size: 14px; color:#444242; ">
                                                        <h2>
                                                            Te invitamos a completar tu perfil para que obtengas grandes beneficios.
                                                        </h2>
                                                        <a href="<?php echo base_url() ?>novios/perfil">
                                                            <img src="<?php echo base_url() ?>dist/img/email/button1.png" alt="JAPY">
                                                        </a>
                                                        <!-- <p>Nota: El apartado 'Mi Cuenta' esta ubicado al presionar el menu superior derecho, despues de iniciar sesion, es la penultima opcion.</p> -->
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td bgcolor="#ffffff" width="100">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>


                        
                        <!-- CUERPO CONTENIDO -->

                        <!-- <tr>
                            <td bgcolor="#f0a5c7" height="10px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#f0a5c7" height="25px" width="40" valing="center" style=" font-family: &#39;Raleway&#39;, sans-serif; font-weight: 300;text-align: center; font-size: 30px; color:#fff; margin-top:50;">
                                <b>¡Gracias por registrarte!</b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#f0a5c7" height="10px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#white" height="15px" width="40">
                            </td>
                        </tr> -->
                        
                        <!-- <tr>
                            <td bgcolor="#white" height="30px" width="40">
                            </td>
                        </tr> -->
                        <!-- <tr bgcolor="#white" valing="center" style="font-family: &#39;Raleway&#39;, sans-serif; font-weight: 300;text-align: center; font-size: 14px; color:#fff; margin-top:50;">
                            <td>
                                <b>Accede y cambia tu contrase&ntilde;a aqui:</b>
                            </td>
                        </tr> -->
                        <!-- <tr>
                            <td bgcolor="#white" height="10px" width="40">
                            </td>
                        </tr> -->
                        <!-- <tr bgcolor="#white">
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td width="290">
                                        </td>
                                        <td>
                                            <form action="https://japybodas.com/novios/perfil/Home/configuracion"> 
                                            <a href="https://japybodas.com/novios/perfil/Home/configuracion">
                                                <button type="submit" style="outline: none; font-family: &#39;Raleway&#39;, sans-serif; text-align: center; background-color: #cf1e66; border:none; border-radius: 50px; height:30px; width:160px; font-size: 14px; color:#fff; ">
                                                    <b>Ingresa ahora</b>
                                                </button>
                                            </a>
                                            </form>
                                        </td>
                                        <td width="290">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr> -->
                        <!-- <tr>
                            <td bgcolor="#00b4d9" height="10px" width="40">
                            </td>
                        </tr> -->
                        <tr bgcolor="#ffffff" style="font-family: &#39;Raleway&#39;, sans-serif; text-align: center; font-size: 12px; color:#444242; margin-top:50;">
                            <!-- <td>
                                Continua con tu planificación de ensueño!
                            </td> -->
                        </tr>
                        <!-- FIN CUERPO CONTENIDO -->
                        <!-- FOOTER-->
                        <tr>
                            <td bgcolor="#ffffff" height="20px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff" style=" font-family: &#39;Raleway&#39;, sans-serif; text-align: center; font-size: 12px; color:#white; margin-top:50;">
                            <!-- <td>
                                <a href="https://www.facebook.com/Japy-177297236307569/" style="color:#cf1e66;">Facebook</a> | <a href="https://twitter.com/japymx/" style="color:#cf1e66;">Twitter</a> |
                                <a href="https://www.instagram.com/japymx/" style="color:#cf1e66;">Instagram</a> | <a href="https://co.pinterest.com/japymx/" style="color:#cf1e66;">Pinterest</a>
                            </td> -->
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff" height="10px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff" style=" font-family: &#39;Raleway&#39;, sans-serif; text-align: center; font-size: 12px; color:#white; margin-top:50;">
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- FIN FOOTER-->
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#white" style="padding:0px 0px 0px 0px;">
                                            <img src="<?php echo base_url() ?>dist/img/email/footer1.png" alt="JAPY" width="100%" height="135" style="display: block;">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>

                        </tr>
                        <!-- <tr>
                            <td bgcolor="#white" height="20px">
                            </td>
                        </tr> -->
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </body>
</html>