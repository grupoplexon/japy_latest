<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Contacto</title>

    <link href="http://fonts.googleapis.com/icon?family=Raleway:300,500,700" rel="stylesheet">

    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .contain {
                width: 90%;
                padding: 15px
            }

            h1 {
                font-size: 25px;
                line-height: 27px;
            }

            p {
                width: 90%;
            }

            h2 {
                font-size: 20px;
                line-height: 22px;
            }
        }

        .template {
            width: 100%;
            font-family: "Raleway", sans-serif;
        }

        tr {
            width: 100%;
            max-width: 100%;
        }

        td {
            display: flex;
        }

        .logo {
            width: 15%;
            display: flex;
            justify-content: center;
            align-items: center;
            background-image: url("<?php echo base_url() ?>/dist/img/japy_nobg_cut.png");
            -moz-background-size: 100% 100%;
            -webkit-background-size: 100% 100%;
            background-size: 100% 100%;
        }

        .menu {
            display: flex;
            justify-content: center;
            width: 85%;
            font-size: 25px;
        }

        .menu ul {
            display: flex;
            list-style: none;
            padding: 0;
        }

        .menu ul li {
            color: grey;
            font-size: 30px;
        }

        .footer img {
            width: 40px;
            height: 40px;
        }

        .footer {
            display: flex;
            padding: 5px;
        }

        .right {
            display: flex;
            justify-content: flex-end;
            width: 55%;
        }

        .left {
            display: flex;
            justify-content: center;
            width: 40%;
        }

        .left img {
            width: 60px;
            height: auto;
        }

        .left span {
            font-size: 12px;
            color: white;
        }

        tfoot {
            background-color: #00bcdd;
        }

        center {
            margin: auto;
            text-align: center;
        }
    </style>
</head>
<body>
<table class="template">
    <thead>
    <tr style="padding: 10px 35px">
        <td class="logo">
        </td>
        <td class="menu">
            <ul>
                <li>
                    <span>MI BODA</span>
                </li>
                <li>
                    <span>|</span>
                </li>
                <li>
                    <span>IDEAS</span>
                </li>
                <li>
                    <span>|</span>
                </li>
                <li>
                    <span>VESTIDOS</span>
                </li>
            </ul>
        </td>
    </tr>
    </thead>
