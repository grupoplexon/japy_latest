<!DOCTYPE html>
<!-- saved from url=(0046)http://bigelephant.mx/_sitios/japy/2meses.html -->
<html lang="en" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>.:: Japy 2 meses ::.</title>
        <link href="<?php echo base_url(); ?>dist/css/reminders/reminders.css" rel="stylesheet">
    </head>
    <body style=" margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tbody>
            <tr>
                <!-- MARCO -->
                <td align="center" style="background-color: #EDEFF8;">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;max-width:600px">
                        <!-- ENCABEZADO -->
                        <tbody>
                        <tr>
                            <td bgcolor="#fff" height="25px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#fff" width="290">
                                        </td>
                                        <td bgcolor="#fff" style="padding:0px 0px 0px 0px;">
                                            <img src="<?php echo base_url(); ?>dist/img/japy_nobg.png" alt="JAPY" width="196" height="77" style="display: block;">
                                        </td>
                                        <td bgcolor="#fff" width="290">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#fff" height="15px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#f0a5c7" height="40px" width="40">
                            </td>
                        </tr>
                        <!-- FIN ENCABEZADO -->
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr bgcolor="#f0a5c7">
                                        <td width="250">
                                        </td>
                                        <td bgcolor="#f0a5c7" style="padding: 0px 0px 0px 0px;">
                                            <img src="<?php echo base_url(); ?>dist/img/reminder/titulo2meses.png" alt="JAPY" width="400" height="120" style="display: block;">
                                        </td>
                                        <td width="250">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#f0a5c7" height="40px">
                            </td>
                        </tr>
                        <!-- IMAGEN CONTENIDO -->
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#f0a5c7" width="190">
                                        </td>
                                        <td bgcolor="#f0a5c7" style="padding: 0px 0px 0px 0px;">
                                            <img src="<?php echo base_url(); ?>dist/img/reminder/novios.png" alt="JAPY" width="550" height="375" style="display: block;">
                                        </td>
                                        <td bgcolor="#f0a5c7" width="190">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- FIN IMAGEN CONTENIDO -->
                        <!-- CUERPO CONTENIDO -->

                        <tr>
                            <td bgcolor="#00b4d9" height="20px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="25px" width="40" valing="center" style="line-height: 15px; font-family: &#39;Raleway&#39;, sans-serif; font-weight: 300;text-align: center; font-size: 30px; color:#fff; margin-top:50;">
                                <b>¡Estás tan solo a</b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="25px" width="40" valing="center" style="line-height: 28px; font-family: &#39;Raleway&#39;, sans-serif; font-weight: 300;text-align: center; font-size: 30px; color:#fff; margin-top:50;">
                                <b>unos pasos!</b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="15px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#00b4d9" width="20px">
                                        </td>
                                        <td bgcolor="#00b4d9" style=" padding: 10px 10px 10px 10px;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                <tr>
                                                    <td height="80px" width="40px">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                            <tr>
                                                                <td width="10px" height="100px">
                                                                    <table bgcolor="#00b4d9" border="0" cellpadding="0" cellspacing="0" width="100%" height="100px">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td bgcolor="#00b4d9" width="100px">
                                                                            </td>
                                                                            <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 3px;">
                                                                                <img src="<?php echo base_url(); ?>dist/img/reminder/renta_coche.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                            </td>
                                                                            <td width="5px">
                                                                            </td>
                                                                            <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 3px;">
                                                                                <img src="<?php echo base_url(); ?>dist/img/reminder/menu.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                            </td>
                                                                            <td width="5px">
                                                                            </td>
                                                                            <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 3px;">
                                                                                <img src="<?php echo base_url(); ?>dist/img/reminder/noche_boda.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                            </td>
                                                                            <td bgcolor="#00b4d9" width="100px">
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#00b4d9" height="10px" width="40px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="80px" width="40">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                            <tr>
                                                                <td width="10px" height="100px">
                                                                    <table bgcolor="#00b4d9" border="0" cellpadding="0" cellspacing="0" width="100%" height="100px">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td bgcolor="#00b4d9" width="100px">
                                                                            </td>
                                                                            <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 3px;">
                                                                                <img src="<?php echo base_url(); ?>dist/img/reminder/despedida.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                            </td>
                                                                            <td width="5px">
                                                                            </td>
                                                                            <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 3px;">
                                                                                <img src="<?php echo base_url(); ?>dist/img/reminder/musica.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                            </td>
                                                                            <td width="5px">
                                                                            </td>
                                                                            <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 3px;">
                                                                                <img src="<?php echo base_url(); ?>dist/img/reminder/recuerdos.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                            </td>
                                                                            <td bgcolor="#00b4d9" width="100px">
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td bgcolor="#00b4d9" width="20px">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="30px" width="40px">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9" valing="center" style="font-family: &#39;Raleway&#39;, sans-serif; font-weight: 300;text-align: center; font-size: 14px; color:#fff; margin-top:50;">
                            <td>
                                <b>Todo esto y más tenemos para ti en japybodas.com</b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="10px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9">
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td width="290">
                                        </td>
                                        <td>
                                            <form action="https://japybodas.com/cuenta">
                                                <button type="submit" style="outline: none; font-family: &#39;Raleway&#39;, sans-serif; text-align: center; background-color: #cf1e66; border:none; border-radius: 50px; height:30px; width:160px; font-size: 14px; color:#fff; ">
                                                    <b>Ingresa ahora</b>
                                                </button>
                                            </form>
                                        </td>
                                        <td width="290">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="10px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9" style="font-family: &#39;Raleway&#39;, sans-serif; text-align: center; font-size: 12px; color:#444242; margin-top:50;">
                            <td>
                                Continua con tu planificación de ensueño!
                            </td>
                        </tr>
                        <!-- FIN CUERPO CONTENIDO -->
                        <!-- FOOTER-->
                        <tr>
                            <td bgcolor="#00b4d9" height="20px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9" style=" font-family: &#39;Raleway&#39;, sans-serif; text-align: center; font-size: 12px; color:#00b4d9; margin-top:50;">
                            <td>
                                <a href="https://www.facebook.com/Japy-177297236307569/" style="color:#cf1e66;">Facebook</a> | <a href="https://twitter.com/japymx/" style="color:#cf1e66;">Twitter</a> |
                                <a href="https://www.instagram.com/japymx/" style="color:#cf1e66;">Instagram</a> | <a href="https://co.pinterest.com/japymx/" style="color:#cf1e66;">Pinterest</a>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="10px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="10px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="30px" width="40">
                            </td>
                        </tr>
                        <!-- FIN FOOTER-->
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>


    </body>
</html>