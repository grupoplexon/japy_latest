<!DOCTYPE html>
<!-- saved from url=(0048)http://bigelephant.mx/_sitios/japy/2semanas.html -->
<html lang="en" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>.:: Japy 2 semanas ::.</title>
        <link href="<?php echo base_url(); ?>dist/css/reminders/reminders.css" rel="stylesheet">
    </head>
    <body style=" margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tbody>
            <tr>
                <!-- MARCO -->
                <td align="center" style="background-color: #EDEFF8;">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;max-width:600px">
                        <!-- ENCABEZADO -->
                        <tbody>
                        <tr>
                            <td bgcolor="#fff" height="25px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#fff" width="290">
                                        </td>
                                        <td bgcolor="#fff" style="padding:0px 0px 0px 0px;">
                                            <img src="<?php echo base_url(); ?>dist/img/japy_nobg.png" alt="JAPY" width="196" height="77" style="display: block;">
                                        </td>
                                        <td bgcolor="#fff" width="290">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#fff" height="15px" width="40">
                            </td>
                        </tr>
                        <!-- FIN ENCABEZADO -->
                        <tr>
                            <td bgcolor="#00b4d9" height="10px">
                            </td>
                        </tr>
                        <!-- IMAGEN CONTENIDO -->
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#00b4d9" width="190">
                                        </td>
                                        <td bgcolor="#00b4d9" style="padding: 0px 0px 0px 0px;">
                                            <img src="<?php echo base_url(); ?>dist/img/reminder/2semanas.png" alt="JAPY" width="500" height="500" style="display: block;">
                                        </td>
                                        <td bgcolor="#00b4d9" width="190">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="30px" width="40">
                            </td>
                        </tr>
                        <!-- FIN IMAGEN CONTENIDO -->
                        <!-- CUERPO CONTENIDO -->

                        <tr>
                            <td>
                                <table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td bgcolor="#00b4d9" width="30px">
                                        </td>
                                        <td bgcolor="#00b4d9" style=" padding: 10px 10px 10px 10px;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                <tr>
                                                    <td width="10px" height="100px">
                                                        <table bgcolor="#00b4d9" border="0" cellpadding="0" cellspacing="0" width="100%" height="100px">
                                                            <tbody>
                                                            <tr>
                                                                <td bgcolor="#00b4d9" width="80px">
                                                                </td>
                                                                <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 5px;">
                                                                    <img src="<?php echo base_url(); ?>dist/img/reminder/spa.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                </td>
                                                                <td width="5px">
                                                                </td>
                                                                <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 5px;">
                                                                    <img src="<?php echo base_url(); ?>dist/img/reminder/novio.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                </td>
                                                                <td width="5px">
                                                                </td>
                                                                <td bgcolor="#00b4d9" width="10px" style="border-radius:10px; padding: 0px 0px 0px 5px;">
                                                                    <img src="<?php echo base_url(); ?>dist/img/reminder/mesas.png" alt="JAPY" width="90px" height="90px" style="display: block;">
                                                                </td>
                                                                <td bgcolor="#00b4d9" width="80px">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td bgcolor="#00b4d9" width="30px">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="20px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr bgcolor="#00b4d9" valing="center" style="font-family: &#39;Raleway&#39;, sans-serif; font-weight: 400;text-align: center; font-size: 14px; color:#fff; margin-top:50;">
                                        <td width="100">
                                        </td>
                                        <td>
                                            Están a un cerrar de ojos, todo va de maravilla y en
                                            unos días más, harán de su sueño una realidad.
                                        </td>
                                        <td width="100">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="10px" width="40">
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="25px" width="40" valing="center" style="font-family: &#39;Raleway&#39;, sans-serif; font-weight: 300;text-align: center; font-size: 35px; color:#ffe2ef; margin-top:50;">
                                <b>¡Felicitaciones!</b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="20px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9" valing="center" style="font-family: &#39;Raleway&#39;, sans-serif; font-weight: 300;text-align: center; font-size: 14px; color:#fff; margin-top:50;">
                            <td>
                                <b>Todo esto y más tenemos para ti en japybodas.com</b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="5px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9">
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td width="290">
                                        </td>
                                        <td>
                                            <form action="https://japybodas.com/cuenta">
                                                <button type="submit" style="outline: none; font-family: &#39;Raleway&#39;, sans-serif; text-align: center; background-color: #cf1e66; border:none; border-radius: 50px; height:30px; width:160px; font-size: 14px; color:#fff; ">
                                                    <b>Ingresa ahora</b>
                                                </button>
                                            </form>
                                        </td>
                                        <td width="290">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="5px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9" style="font-family: &#39;Raleway&#39;, sans-serif; text-align: center; font-size: 12px; color:#444242; margin-top:50;">
                            <td>
                                Continua con tu planificación de ensueño!
                            </td>
                        </tr>
                        <!-- FIN CUERPO CONTENIDO -->
                        <!-- FOOTER-->
                        <tr>
                            <td bgcolor="#00b4d9" height="20px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9" style=" font-family: &#39;Raleway&#39;, sans-serif; text-align: center; font-size: 12px; color:#00b4d9; margin-top:50;">
                            <td>
                                <a href="https://www.facebook.com/Japy-177297236307569/" style="color:#cf1e66;">Facebook</a> | <a href="https://twitter.com/japymx/" style="color:#cf1e66;">Twitter</a> |
                                <a href="https://www.instagram.com/japymx/" style="color:#cf1e66;">Instagram</a> | <a href="https://co.pinterest.com/japymx/" style="color:#cf1e66;">Pinterest</a>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#00b4d9" height="10px" width="40">
                            </td>
                        </tr>
                        <tr bgcolor="#00b4d9" style=" font-family: &#39;Raleway&#39;, sans-serif; text-align: center; font-size: 12px; color:#00b4d9; margin-top:50;">
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td width="200">
                                        </td>
                                        <td width="259">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                <tr>
                                                    <td height="10">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="30">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td width="20">
                                        </td>
                                        <td width="80">
                                        </td>
                                        <td width="10" style="padding:0px 0px 0px 0px;">
                                            <img src="<?php echo base_url(); ?>dist/img/reminder/textura2.png" alt="JAPY" width="80" height="80" style="display: block;">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- FIN FOOTER-->
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>


    </body>
</html>