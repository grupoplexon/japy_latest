<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Vestidos de Novia - BrideAdvisor</title>
    <?php $this->view("japy/prueba/header"); ?>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script> -->
</head>
<style>
     #header { 
        position: relative !important;
    }
    body{
        background-image: url('<?php echo base_url()?>dist/img/fondo.jpg'); 
        background-size: contain;
        font-family: 'Raleway', sans-serif !important;s
        color: #757575 !important;
    }
    hr{
        color: #F8DADF ;
        border:2px solid;
        background-color: #f8dadf;
    }
    .portada{
        margin-top: 10vh !important;
    }
    .tendencia{
        margin-left: 5% !important;
        margin-right: 5% !important;
    }
    .post{
        height: 400px !important;
        margin-bottom: 4vh;

        width: 288px;
        height: 200px;
        overflow: hidden;
        margin: 10px;
        position: relative;
    }
    .post > .img-home{
        position:absolute;
        left: -100%;
        right: -100%;
        top: -100%;
        bottom: -100%;
        margin: auto;
        min-height: 100%;
        min-width: 100%;
        object-fit: cover !important;
    }
    .img-home{
        /* object-fit: cover !important; */
    }
    select {
        background: white !important;
        border-radius: 10px !important;
        padding-left: 10px !important;
        opacity: inherit !important;
        /*width: 100% !important;*/
        position: relative !important;
        height: 50px !important;
        pointer-events: all !important;
    }
    .select-wrapper .caret{
        z-index: 1 !important;
    }
    .card{
        margin: 0 !important;
    }
    @media only screen and (max-width: 900px) and (min-width: 300px){
        .button-movil2 {
            position: relative !important;
            margin: 0 0px;
            line-height: 90px;
            top: 10px;
        }
        .img-home{
            height: 250px;
        }
    }
</style>
<body>
    <div class="row container portada " style="background: white !important; " >
        <div class=" hide-on-small-only card"> 
            <img class="img-home " style="width:100%;" src="<?php echo base_url() ?>dist/img/TENDENCIA.png">
        </div>
        <div class=" hide-on-large-only card"> 
            <img class="img-home " style="width:100%;" src="<?php echo base_url() ?>dist/img/TENDENCIA_vm.png?v=1"
            <br>
            <h5 style="text-align: center">VESTIDOS DE NOVIA</h5>
            <h6 style="text-align:justify;">Tu vestido de novia es el más bonito que te pondrás en tu vida. Busca dentro de nuestro catálogo, es el más grande en moda nupcial y seguro
            que encuentras ese diseño único que te hace brillar con luz propia. Tenemos más de 6.000 vestidos de más de 50 diseñadores. Podrás filtrar por 
            diseñador, corte, temporada y/o escote.</h6>
            </br>
        </div>
    </div>
    <div class="row container">
        <?php  $this->view("tendencia/menu"); ?>
    </div>
    <div class="row tendencia">
        <h3>ÚLTIMAS COLECCIONES</h3>
        <hr>
        <?php foreach ($disenadores as $d) : ?>
            <h4>Diseñador <?= $d->disenador ?></h4>
            <br>
            <div class="row jcarousel-articulos center">
                <?php foreach ($tendencias as $t) : ?>
                    <?php if($t->disenador == $d->disenador) { ?>
                        <div class="col m2-l3-jcaroul post">
                             <a style="" href="<?php echo base_url() ?>tendencia/post?modelo=<?php echo $t->nombre ?>"><img class="img-home " style="width:288px; height:400px;" src="<?php echo base_url() ?>uploads/images/tendencia/<?= $t->image ?>"></a>
                        </div> 
                    <?php } ?>
               <?php endforeach; ?>
           </div>
       <?php endforeach; ?>
    </div>
</body>
<?php $this->view("japy/prueba/footer"); ?>
<script>
    $(document).ready(function(){
        $('.dropdown-trigger').dropdown();
        
        $('.jcarousel-articulos').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            // speed: 300,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left flecha" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right flecha" aria-hidden="true"></i></button>',
            // autoplay: true,
            lazyLoad: 'ondemand',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ],
        });
    });
</script>
</html>