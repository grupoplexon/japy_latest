<style>
    select {
        background: white !important;
        border-radius: 10px !important;
        padding-left: 10px !important;
        opacity: inherit !important;
        /*width: 100% !important;*/
        position: relative !important;
        height: 50px !important;
        pointer-events: all !important;
        border: 0px #757575 solid !important;
        box-shadow: 0px 0px 3px 0px;
        margin-bottom: 10px;
    }
    
</style>
<div class="row">
   <div class="col l3 s12">
    <select id="categoria" class="select browser-default">
      <option value="" disabled selected>Categoria</option>
      <?php foreach ($vestido as $v) : ?>
        <?php if($value==$v->id_tipo_vestido) {?>
            <option value="<?= $v->slug ?>" selected> <?= $v->nombre ?></option>
        <?php }else{ ?>
            <option value="<?= $v->slug ?>"> <?= $v->nombre ?></option>
        <?php } ?>
       <?php endforeach; ?>
    </select>
  </div>
  <div class="col l2 s12">
    <select id="disenador" class="select browser-default">
      <option value="" selected>Diseñador</option>
      <option value=""  > Todos los Diseñadores</option>
      <?php if($disenador){ foreach ($disenador as $dis) : ?>
        <?php if($value_dis!=null & $value_dis ==$dis->disenador) {?>
            <option value="<?= $dis->disenador ?>" selected> <?= $dis->disenador ?></option>
        <?php }else{ ?>
            <option value="<?= $dis->disenador ?>" > <?= $dis->disenador ?></option>
        <?php } ?>
       <?php endforeach; } ?>
    </select>
  </div>
  <?php if($corte[0]->corte != null){ ?>
  <div class="col l2 s12">
    <select id="corte" class="select browser-default">
      <option value=""  selected>Corte</option>
      <option value=""  > Todos los Cortes</option>
      <?php  foreach ($corte as $c) : ?>
        <?php if($value_corte!=null & $value_corte ==$c->corte) {?>
           <option value="<?= $c->corte ?>" selected> <?= $c->corte ?></option>
        <?php }else{ ?>
            <option value="<?= $c->corte ?>"> <?= $c->corte ?></option>
        <?php } ?>
            
       <?php endforeach;  ?>
    </select>
  </div>
  <?php } ?>
  <?php if($escote[0]->escote != null){ ?>
  <div class="col l2 s12">
    <select id="escote" class="select browser-default">
      <option value=""  selected>Escote</option>
      <option value="" > Todos los Escotes</option>
      <?php  foreach ($escote as $e) : ?> 
        <?php if($value_escote!=null & $value_escote ==$e->escote) {?>
           <option value="<?= $e->escote ?>" selected> <?= $e->escote ?></option>
        <?php }else{ ?>
            <option value="<?= $e->escote ?>"> <?= $e->escote ?></option>
        <?php } ?>
            
       <?php endforeach;  ?>
    </select>
  </div>
  <?php } ?>
  <?php if($largo[0]->largo != null){ ?>
  <div class="col l2 s12">
    <select id="largo" class="select browser-default">
      <option value=""  selected>Largo</option>
      <option value="" > Todos los Largos</option>
      <?php  foreach ($largo as $l) : ?>
        <?php if($value_largo!=null & $value_largo ==$l->largo) {?>
           <option value="<?= $l->largo ?>" selected> <?= $l->largo ?></option>
        <?php }else{ ?>
            <option value="<?= $l->largo ?>"> <?= $l->largo ?></option>
        <?php } ?>
            
       <?php endforeach;  ?>
    </select>
  </div>
  <?php } ?>
  <?php if($tipo[0]->tipo != null){ ?>
  <div class="col l2 s12">
    <select id="tipo" class="select browser-default">
      <option value=""  selected>Tipo</option>
      <option value="" > Todos los Tipos</option>
      <?php  foreach ($tipo as $t) : ?>
        <?php if($value_tipo!=null & $value_tipo ==$t->tipo) {?>
           <option value="<?= $t->tipo ?>" selected> <?= $t->tipo ?></option>
        <?php }else{ ?>
            <option value="<?= $t->tipo ?>"> <?= $t->tipo ?></option>
        <?php } ?>
            
       <?php endforeach;  ?>
    </select>
  </div>
  <?php } ?>
  <?php if($estilo[0]->estilo != null){ ?>
  <div class="col l2 s12">
    <select id="estilo" class="select browser-default">
      <option value=""  selected>Estilo</option>
      <option value="" > Todos los Estilos</option>
      <?php  foreach ($estilo as $c) : ?>
        <?php if($value_estilo!=null & $value_estilo ==$c->estilo) {?>
           <option value="<?= $c->estilo ?>" selected> <?= $c->estilo ?></option>
        <?php }else{ ?>
            <option value="<?= $c->estilo ?>"> <?= $c->estilo ?></option>
        <?php } ?>
            
       <?php endforeach;  ?>
    </select>
  </div>
  <?php } ?>
  <?php if($categoria[0]->categoria != null){ ?>
  <div class="col l2 s12">
    <select id="category" class="select browser-default">
      <option value=""  selected>Categoria</option>
      <option value="" > Todos las Categorias</option>
      <?php  foreach ($categoria as $c) : ?>
        <?php if($value_category!=null & $value_category ==$c->categoria) {?>
           <option value="<?= $c->categoria ?>" selected> <?= $c->categoria ?></option>
        <?php }else{ ?>
            <option value="<?= $c->categoria ?>"> <?= $c->categoria ?></option>
        <?php } ?>
            
       <?php endforeach;  ?>
    </select>
  </div>
  <?php } ?>
   <div class="col l2 s12">
    <select id="temporada" class="select browser-default">
      <option value=""  selected>Temporada</option>
      <option value="" > Todos las Temporadas</option>
      <?php if($temporada){ foreach ($temporada as $t) : ?>
        <?php if($value_temporada!=null & $value_temporada == $t->temporada) {?>
           <option value="<?= $t->temporada ?>" selected> <?= $t->temporada ?></option>
        <?php }else{ ?>
            <option value="<?= $t->temporada ?>"> <?= $t->temporada ?></option>
        <?php } ?>
            
       <?php endforeach; } ?>
    </select>
  </div>
  <input class="" id="url" type="hidden" value="<?php echo base_url('tendencia') ?>"> 
</div> 
<script src="<?php echo base_url() ?>dist/js/brideadvisor/tendencia.js"></script>
<script>
    
</script>