
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:url"           content="<?php echo base_url() ?>tendencia/post/<?php echo $post->id_vestido ?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Modelo: <?php echo $post->nombre ?> (<?php echo $post->disenador ?>)" />
    <meta property="og:description"   content='<?php echo $post->detalle ?>' />
    <meta property="og:image"         content="<?php echo base_url() ?>uploads/images/tendencia/<?= $post->image ?>" />
    
    <title>Modelo: <?php echo $post->nombre ?> (<?php echo $post->disenador ?>) - Tendencia</title>
    <?php $this->view("japy/prueba/header"); ?>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script> -->
</head>
<style>
    #header {
        position: relative !important;
    }
    body{
        background-image: url('<?php echo base_url()?>dist/img/fondo.jpg'); 
        background-size: cover;
    }
    .name{ 
        margin: 10vh !important;
    }
    hr{
        color: #F8DADF ;
        border:2px solid;
        background-color: #f8dadf;
    }
    h5{
        font-size: 1.3rem !important;
    }
    .post{
        padding: 5vh !important;
        text-align: justify !important;
        margin: 0 !important;
        padding-top: 0 !important;
    }
    .img-post, .post{
        height: 600px !important;
    }
    .like{
        width: 60px;
        float: right;
    }
    .like2{
        display: none;
    } 
    .like, .like2{
        cursor: pointer;
    }
    .inicio{
        font-size: 1.15rem !important;
        color: #757575 !important;
        font-weight: bold !important;
    }
    #comentario{
        margin: 10px 0 8px 0 !important;
        border: 1px solid black !important;
    }
    #label-coment{
        font-size:1.1rem !important;
    }
    @media only screen and (max-width: 900px) and (min-width: 300px){
        .button-movil2 {
            position: relative !important;
            margin: 0 0px;
            line-height: 90px;
            top: 10px;
        }
            .name{ 
            margin: 4vh !important;
        }
        .img-post{
            margin-bottom: 10px;
        }
    }
		/* these styles are for the demo, but are not required for the plugin */
		.zoom {
			display:inline-block;
			position: relative;
		}
		/* magnifying glass icon */
		.zoom:after {
			content:'';
			display:block; 
			width:33px; 
			height:33px; 
			position:absolute; 
			top:0;
			right:0;
			background:url(icon.png);
            z-index: 100;
		}
		.zoom img {
			display: block;
		}
		.zoom img::selection { background-color: transparent; }

        .fas{
            position: absolute;
            right: 0px;
            top: 0px;
            z-index: 1;
            background: white;
        }
</style>
<body>
    <div class="row"> 
        <div class="name row">
            <h3 >Modelo: <?php echo $post->nombre ?> (<?php echo $post->disenador ?>)</h3>
            <hr>
        </div>
        <div class="row container">
            <div class="col l6 s12 img-post">
                <span class='zoom' id='ex1' style="cursor: zoom-in;">
                    <i class="fas fa-search-plus"></i>
                    <img class="img-home "  style=" z-index: -1;width:100%; height:100%; object-fit: cover;" src="<?php echo base_url() ?>uploads/images/tendencia/<?= $post->image ?>">
                </span>
            </div>
            <div class="col l6 s12 card post">
                <?php if ( $this->checker->isLogin()) { ?>
                    <img id="like" onclick="M.toast({html: 'Post Guardado (:'})" class="like tooltipped" data-tooltip="Guardar" src="<?php echo base_url() ?>dist/img/brideadvisor/megusta.png">  
                    <img id="like2" onclick="M.toast({html: 'Post Eliminado :('})"  class="like like2" src="<?php echo base_url() ?>dist/img/brideadvisor/megusta02.png">  
                 <?php } else { ?>
                    <img onclick="M.toast({html: 'Inicia sesion para guardar'})" class="like" src="<?php echo base_url() ?>dist/img/brideadvisor/megusta.png">
                 <?php } ?>
                <br><br>
                <h5><b>Modelo: </b><?php echo $post->nombre ?></h5>
                <h5><b>Temporada: </b><?php echo $post->temporada ?></h5>
                <h5><b>Diseñador: </b><?php echo $post->disenador ?></h5>
                <?php if($post->corte!=null){ ?> 
                    <h5><b>Corte: </b><?php echo $post->corte ?></h5>
                <?php } ?> 
                <?php if($post->escote!=null){ ?> 
                    <h5><b>Escote: </b><?php echo $post->escote ?></h5>
                <?php } ?> 
                <?php if($post->largo!=null){ ?> 
                    <h5><b>Largo: </b><?php echo $post->largo ?></h5>
                <?php } ?> 
                <?php if($post->tipo!=null){ ?> 
                    <h5><b>Tipo: </b><?php echo $post->tipo ?></h5>
                <?php } ?> 
                <?php if($post->estilo!=null){ ?> 
                    <h5><b>Estilo: </b><?php echo $post->estilo ?></h5>
                <?php } ?> 
                <h5><b>Descripcion: </b><?php echo $post->detalle ?></h5>
                <br>
                <div id="fb-root"></div>
                  <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.0";
                    fjs.parentNode.insertBefore(js, fjs);
                  }(document, 'script', 'facebook-jssdk'));</script>
                
                  <!-- Your share button code -->
                  <div class="row"> 
                    <div class="fb-share-button" 
                        data-href="<?php echo base_url() ?>tendencia/post/<?php echo $post->id_vestido ?>" 
                        data-layout="button" data-size="small"> 
                    </div>
                    <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                  </div>
                </div>
        </div>
        <div class="name row">
            <hr>
            <br>
            <h4 >Deja un comentario</h4>
            <?php if ( $this->checker->isLogin()) { ?>
                <div class="row card  center-align" >
                    <br><br>
                    <div class="row container">
                        <form method="POST" id="sendComment" >
                            <div class="input-field col l12 s12 center-align ">
                                <input placeholder="" id="comentario" name="comentario" type="text" class="validate">
                                <label for="comentario" id="label-coment"> Comentario </label>
                            </div>
                            <div class="col l12 s12 center-align">
                                <button class="btn grey darken-3" id="" type="submit">Comentar</button>
                                <br> <br>
                            </div>
                        </form>  
                    </div>   
                </div>
            <?php } else { ?>
                <div class="row card  center-align">
                    <br>
                    <h6 class=""><a class="inicio" href="<?php echo base_url() ?>cuenta/novia">Inicia sesion</a> para dejar tu comentario</h6>
                    <br>
                </div>
            <?php } ?>
        </div>
    </div>
    <input type="hidden" value="<?php echo $post->id_vestido ?>" id="vestido">
    <input type="hidden" value="<?php echo base_url() ?>" id="base-url">
</body>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<?php $this->view("japy/prueba/footer"); ?>
<!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script> -->
	<script src='<?php echo base_url() ?>dist/zoom-master/jquery.zoom.js'></script>
<script>
const baseUrl = $('#base-url').val();
    $( document ).ready(function() {
    
        $('.tooltipped').tooltip();

        $('#ex1').zoom();
        
        like();
});
    function like(){
        $.ajax({
            url: baseUrl + 'tendencia/like_vestido',
            dataType: 'json',
            method: 'post',
            data: {
                vestido: $('#vestido').val(),
            },
            success: function(response) {
                if(response.data=='ok') {
                    $('#like').hide();
                    $('#like2').show();
                    console.log('entro');
                } else {
                    $('#like2').hide();
                    $('#like').show();
                }
            },
            error: function() {
                console.log('error');
            }
        });
    }
    $( "#like" ).click(function() {
        $.ajax({
            url: baseUrl + 'tendencia/create_like',
            dataType: 'json',
            method: 'post',
            data: {
                vestido: $('#vestido').val(),
            },
            success: function(response) {
                if(response.data=='ok') {
                    $('#like').hide();
                    $('#like2').show();
                }
            },
            error: function() {
                console.log('error');
            }
        });
    });
    $( "#like2" ).click(function() {
        $.ajax({
            url: baseUrl + 'tendencia/delete_like',
            dataType: 'json',
            method: 'post',
            data: {
                vestido: $('#vestido').val(),
            },
            success: function(response) {
                if(response.data=='ok') {
                    $('#like2').hide();
                    $('#like').show();
                }
            },
            error: function() {
                console.log('error');
            }
        });
    });
    $('#sendComment').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: baseUrl + 'tendencia/saveComment',
            dataType: 'json',
            method: 'post',
            data: {
                comment: $('#comentario').val(),
                vestido: $('#vestido').val(),
            },
            success: function(response) {
                if(response.data=='ok') {
                    swal('Correcto', 'Tu comentario fue publicado.', 'success');
                    // setTimeout(function(){
                    //     location.reload();
                    // },2000);
                }
            },
            error: function() {
                console.log('error');
            }
        });
    });
</script>
</html>