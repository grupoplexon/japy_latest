<style>
    .slider-bodas {
        margin-bottom: 0px;
    }

    @media only screen and (min-width: 601px) {
        .slider-bodas {
            margin-bottom: -200px;
        }
    }

    .yellow.darken-5 {
        background: #00BCDD !important;
        color: #FFFFFF !important;
    }

    .yellow-text.darken-5 {
        color: #00BCDD !important;
    }

    body {
        height: 100% !important;
    }

    .portada {
        background-image: url('<?php echo base_url() ?>dist/img/club.png');
        background-size: cover;
        position: relative;
        background-repeat: no-repeat;
        height: 80%;
    }

    .portada .portada-button {
        position: absolute;
        left: 0px;
        right: 0px;
        bottom: 170px;
        text-align: center;
    }
</style>
<div class="portada">
    <div class="portada-button">
        <a class="btn waves-effect waves-light yellow darken-5" href="<?php echo base_url() . "registro" ?>">reg&iacute;strate</a>
    </div>
</div>
<script type='text/javascript' data-cfasync='false'>window.purechatApi = {
        l: [], t: [], on: function () {
            this.l.push(arguments);
        }
    };
    (function () {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function (e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({c: 'f3f97b28-73ac-4929-a04d-527e409169b7', f: true});
                done = true;
            }
        };
    })();
</script>

<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/4315bf06-815c-4f86-b787-cb87d939a11e.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>

<script src="<?php echo base_url() ?>dist/js/searchBarClubnupcial.js" type="text/javascript"></script>

<?php $this->view("principal/foot") ?>